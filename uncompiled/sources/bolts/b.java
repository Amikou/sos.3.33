package bolts;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

/* compiled from: Task.java */
/* loaded from: classes.dex */
public class b<TResult> {
    public static volatile d l;
    public boolean b;
    public boolean c;
    public TResult d;
    public Exception e;
    public boolean f;
    public bolts.c g;
    public static final ExecutorService i = br.a();
    public static final Executor j = br.b();
    public static final Executor k = uc.c();
    public static b<?> m = new b<>((Object) null);
    public static b<Boolean> n = new b<>(Boolean.TRUE);
    public static b<Boolean> o = new b<>(Boolean.FALSE);
    public final Object a = new Object();
    public List<bolts.a<TResult, Void>> h = new ArrayList();

    /* compiled from: Task.java */
    /* loaded from: classes.dex */
    public class a implements bolts.a<TResult, Void> {
        public final /* synthetic */ o34 a;
        public final /* synthetic */ bolts.a b;
        public final /* synthetic */ Executor c;
        public final /* synthetic */ vv d;

        public a(b bVar, o34 o34Var, bolts.a aVar, Executor executor, vv vvVar) {
            this.a = o34Var;
            this.b = aVar;
            this.c = executor;
        }

        @Override // bolts.a
        /* renamed from: b */
        public Void a(b<TResult> bVar) {
            b.d(this.a, this.b, bVar, this.c, this.d);
            return null;
        }
    }

    /* compiled from: Task.java */
    /* renamed from: bolts.b$b  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static class RunnableC0073b implements Runnable {
        public final /* synthetic */ o34 a;
        public final /* synthetic */ bolts.a f0;
        public final /* synthetic */ b g0;

        public RunnableC0073b(vv vvVar, o34 o34Var, bolts.a aVar, b bVar) {
            this.a = o34Var;
            this.f0 = aVar;
            this.g0 = bVar;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Runnable
        public void run() {
            try {
                this.a.d(this.f0.a(this.g0));
            } catch (CancellationException unused) {
                this.a.b();
            } catch (Exception e) {
                this.a.c(e);
            }
        }
    }

    /* compiled from: Task.java */
    /* loaded from: classes.dex */
    public static class c implements Runnable {
        public final /* synthetic */ o34 a;
        public final /* synthetic */ Callable f0;

        public c(vv vvVar, o34 o34Var, Callable callable) {
            this.a = o34Var;
            this.f0 = callable;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Runnable
        public void run() {
            try {
                this.a.d(this.f0.call());
            } catch (CancellationException unused) {
                this.a.b();
            } catch (Exception e) {
                this.a.c(e);
            }
        }
    }

    /* compiled from: Task.java */
    /* loaded from: classes.dex */
    public interface d {
        void a(b<?> bVar, UnobservedTaskException unobservedTaskException);
    }

    static {
        new b(true);
    }

    public b() {
    }

    public static <TResult> b<TResult> b(Callable<TResult> callable, Executor executor) {
        return c(callable, executor, null);
    }

    public static <TResult> b<TResult> c(Callable<TResult> callable, Executor executor, vv vvVar) {
        o34 o34Var = new o34();
        try {
            executor.execute(new c(vvVar, o34Var, callable));
        } catch (Exception e) {
            o34Var.c(new ExecutorException(e));
        }
        return o34Var.a();
    }

    public static <TContinuationResult, TResult> void d(o34<TContinuationResult> o34Var, bolts.a<TResult, TContinuationResult> aVar, b<TResult> bVar, Executor executor, vv vvVar) {
        try {
            executor.execute(new RunnableC0073b(vvVar, o34Var, aVar, bVar));
        } catch (Exception e) {
            o34Var.c(new ExecutorException(e));
        }
    }

    public static <TResult> b<TResult> g(Exception exc) {
        o34 o34Var = new o34();
        o34Var.c(exc);
        return o34Var.a();
    }

    public static <TResult> b<TResult> h(TResult tresult) {
        if (tresult == null) {
            return (b<TResult>) m;
        }
        if (tresult instanceof Boolean) {
            return ((Boolean) tresult).booleanValue() ? (b<TResult>) n : (b<TResult>) o;
        }
        o34 o34Var = new o34();
        o34Var.d(tresult);
        return o34Var.a();
    }

    public static d k() {
        return l;
    }

    public <TContinuationResult> b<TContinuationResult> e(bolts.a<TResult, TContinuationResult> aVar) {
        return f(aVar, j, null);
    }

    public <TContinuationResult> b<TContinuationResult> f(bolts.a<TResult, TContinuationResult> aVar, Executor executor, vv vvVar) {
        boolean m2;
        o34 o34Var = new o34();
        synchronized (this.a) {
            m2 = m();
            if (!m2) {
                this.h.add(new a(this, o34Var, aVar, executor, vvVar));
            }
        }
        if (m2) {
            d(o34Var, aVar, this, executor, vvVar);
        }
        return o34Var.a();
    }

    public Exception i() {
        Exception exc;
        synchronized (this.a) {
            if (this.e != null) {
                this.f = true;
                bolts.c cVar = this.g;
                if (cVar != null) {
                    cVar.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    public TResult j() {
        TResult tresult;
        synchronized (this.a) {
            tresult = this.d;
        }
        return tresult;
    }

    public boolean l() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    public boolean m() {
        boolean z;
        synchronized (this.a) {
            z = this.b;
        }
        return z;
    }

    public boolean n() {
        boolean z;
        synchronized (this.a) {
            z = i() != null;
        }
        return z;
    }

    public final void o() {
        synchronized (this.a) {
            for (bolts.a<TResult, Void> aVar : this.h) {
                try {
                    aVar.a(this);
                } catch (RuntimeException e) {
                    throw e;
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
            this.h = null;
        }
    }

    public boolean p() {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.a.notifyAll();
            o();
            return true;
        }
    }

    public boolean q(Exception exc) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.a.notifyAll();
            o();
            if (!this.f && k() != null) {
                this.g = new bolts.c(this);
            }
            return true;
        }
    }

    public boolean r(TResult tresult) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.a.notifyAll();
            o();
            return true;
        }
    }

    public b(TResult tresult) {
        r(tresult);
    }

    public b(boolean z) {
        if (z) {
            p();
        } else {
            r(null);
        }
    }
}
