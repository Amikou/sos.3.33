package bolts;

import bolts.b;

/* compiled from: UnobservedErrorNotifier.java */
/* loaded from: classes.dex */
public class c {
    public b<?> a;

    public c(b<?> bVar) {
        this.a = bVar;
    }

    public void a() {
        this.a = null;
    }

    public void finalize() throws Throwable {
        b.d k;
        try {
            b<?> bVar = this.a;
            if (bVar != null && (k = b.k()) != null) {
                k.a(bVar, new UnobservedTaskException(bVar.i()));
            }
        } finally {
            super.finalize();
        }
    }
}
