package info.hoang8f.android.segmented.font;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

/* loaded from: classes2.dex */
public class AwesomeTypefaceSpan extends TypefaceSpan {
    public final Context a;
    public final fn1 f0;

    public AwesomeTypefaceSpan(Context context, fn1 fn1Var) {
        super(fn1Var.b().toString());
        this.a = context.getApplicationContext();
        this.f0 = fn1Var;
    }

    @Override // android.text.style.TypefaceSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setTypeface(ge4.b(this.a, this.f0));
    }

    @Override // android.text.style.TypefaceSpan, android.text.style.MetricAffectingSpan
    public void updateMeasureState(TextPaint textPaint) {
        textPaint.setTypeface(ge4.b(this.a, this.f0));
    }
}
