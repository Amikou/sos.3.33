package info.hoang8f.android.segmented.utils;

import android.content.Context;
import info.hoang8f.android.segmented.utils.BootstrapText;

/* compiled from: IconResolver.java */
/* loaded from: classes2.dex */
public class a {
    public static fn1 a(String str) {
        for (fn1 fn1Var : ge4.a()) {
            if (!fn1Var.b().equals("fontawesome-webfont.ttf") && fn1Var.a(str) != null) {
                return fn1Var;
            }
        }
        throw new IllegalArgumentException(String.format("Could not find FontIcon value for '%s', please ensure that it is mapped to a valid font", str));
    }

    public static BootstrapText b(Context context, String str, boolean z) {
        if (str == null) {
            return null;
        }
        BootstrapText.b bVar = new BootstrapText.b(context, z);
        int i = 0;
        int i2 = 0;
        int i3 = -1;
        int i4 = -1;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt == '\\') {
                i += 2;
            } else {
                if (charAt == '{') {
                    i3 = i;
                } else if (charAt == '}') {
                    i4 = i;
                }
                if (i3 != -1 && i4 != -1) {
                    if (i3 >= 0 && i4 < str.length()) {
                        String replaceAll = str.substring(i3 + 1, i4).replaceAll("\\-", "_");
                        bVar.b(str.substring(i2, i3));
                        if (replaceAll.matches("(fa_|fa-)[a-z_0-9]+")) {
                            if (z) {
                                bVar.b("?");
                            } else {
                                bVar.a(replaceAll, ge4.c("fontawesome-webfont.ttf", false));
                            }
                        } else if (z) {
                            bVar.b("?");
                        } else {
                            bVar.a(replaceAll, a(replaceAll));
                        }
                        i2 = i4 + 1;
                    }
                    i3 = -1;
                    i4 = -1;
                }
            }
            i++;
        }
        return bVar.b(str.substring(i2, str.length())).c();
    }
}
