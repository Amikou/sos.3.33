package info.hoang8f.android.segmented.utils;

import android.content.Context;
import android.text.SpannableString;
import info.hoang8f.android.segmented.font.AwesomeTypefaceSpan;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes2.dex */
public class BootstrapText extends SpannableString implements Serializable {

    /* loaded from: classes2.dex */
    public static class b {
        public final Context b;
        public final Map<Integer, fn1> c = new HashMap();
        public final StringBuilder a = new StringBuilder();

        public b(Context context, boolean z) {
            this.b = context.getApplicationContext();
        }

        public b a(CharSequence charSequence, fn1 fn1Var) {
            this.a.append(fn1Var.a(charSequence.toString().replaceAll("\\-", "_")));
            this.c.put(Integer.valueOf(this.a.length()), fn1Var);
            return this;
        }

        public b b(CharSequence charSequence) {
            this.a.append(charSequence);
            return this;
        }

        public BootstrapText c() {
            BootstrapText bootstrapText = new BootstrapText(this.a.toString());
            for (Map.Entry<Integer, fn1> entry : this.c.entrySet()) {
                int intValue = entry.getKey().intValue();
                bootstrapText.setSpan(new AwesomeTypefaceSpan(this.b, entry.getValue()), intValue - 1, intValue, 18);
            }
            return bootstrapText;
        }
    }

    public BootstrapText(CharSequence charSequence) {
        super(charSequence);
    }
}
