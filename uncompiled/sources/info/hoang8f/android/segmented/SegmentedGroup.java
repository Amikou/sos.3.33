package info.hoang8f.android.segmented;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;
import android.util.StateSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class SegmentedGroup extends RadioGroup {
    public int a;
    public int f0;
    public int g0;
    public int h0;
    public b i0;
    public Float j0;
    public RadioGroup.OnCheckedChangeListener k0;
    public HashMap<Integer, TransitionDrawable> l0;
    public int m0;

    /* loaded from: classes2.dex */
    public class a implements RadioGroup.OnCheckedChangeListener {
        public a() {
        }

        @Override // android.widget.RadioGroup.OnCheckedChangeListener
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            TransitionDrawable transitionDrawable;
            ((TransitionDrawable) SegmentedGroup.this.l0.get(Integer.valueOf(i))).reverseTransition(200);
            if (SegmentedGroup.this.m0 != 0 && (transitionDrawable = (TransitionDrawable) SegmentedGroup.this.l0.get(Integer.valueOf(SegmentedGroup.this.m0))) != null) {
                transitionDrawable.reverseTransition(200);
            }
            SegmentedGroup.this.m0 = i;
            if (SegmentedGroup.this.k0 != null) {
                SegmentedGroup.this.k0.onCheckedChanged(radioGroup, i);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class b {
        public int a;
        public int b;
        public final int c = tz2.radio_checked;
        public final int d = tz2.radio_unchecked;
        public float e;
        public final float f;
        public final float[] g;
        public final float[] h;
        public final float[] i;
        public final float[] j;
        public final float[] k;
        public final float[] l;
        public float[] m;

        public b(float f) {
            float applyDimension = TypedValue.applyDimension(1, 0.1f, SegmentedGroup.this.getResources().getDisplayMetrics());
            this.f = applyDimension;
            this.a = -1;
            this.b = -1;
            this.e = f;
            this.g = new float[]{f, f, applyDimension, applyDimension, applyDimension, applyDimension, f, f};
            this.h = new float[]{applyDimension, applyDimension, f, f, f, f, applyDimension, applyDimension};
            this.i = new float[]{applyDimension, applyDimension, applyDimension, applyDimension, applyDimension, applyDimension, applyDimension, applyDimension};
            this.j = new float[]{f, f, f, f, f, f, f, f};
            this.k = new float[]{f, f, f, f, applyDimension, applyDimension, applyDimension, applyDimension};
            this.l = new float[]{applyDimension, applyDimension, applyDimension, applyDimension, f, f, f, f};
        }

        public final int a(View view) {
            return SegmentedGroup.this.indexOfChild(view);
        }

        public float[] b(View view) {
            f(c(), a(view));
            return this.m;
        }

        public final int c() {
            return SegmentedGroup.this.getChildCount();
        }

        public int d() {
            return this.c;
        }

        public int e() {
            return this.d;
        }

        public final void f(int i, int i2) {
            if (this.a == i && this.b == i2) {
                return;
            }
            this.a = i;
            this.b = i2;
            if (i == 1) {
                this.m = this.j;
            } else if (i2 == 0) {
                this.m = SegmentedGroup.this.getOrientation() == 0 ? this.g : this.k;
            } else if (i2 == i - 1) {
                this.m = SegmentedGroup.this.getOrientation() == 0 ? this.h : this.l;
            } else {
                this.m = this.i;
            }
        }
    }

    public SegmentedGroup(Context context) {
        super(context);
        this.h0 = -1;
        getResources();
        this.f0 = m70.d(getContext(), wy2.radio_button_selected_color);
        this.g0 = m70.d(getContext(), wy2.radio_button_unselected_color);
        this.a = (int) getResources().getDimension(zy2.radio_button_stroke_border);
        Float valueOf = Float.valueOf(getResources().getDimension(zy2.radio_button_conner_radius));
        this.j0 = valueOf;
        this.i0 = new b(valueOf.floatValue());
    }

    public final void e(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, t23.SegmentedGroup, 0, 0);
        try {
            this.a = (int) obtainStyledAttributes.getDimension(t23.SegmentedGroup_sc_border_width, getResources().getDimension(zy2.radio_button_stroke_border));
            this.j0 = Float.valueOf(obtainStyledAttributes.getDimension(t23.SegmentedGroup_sc_corner_radius, getResources().getDimension(zy2.radio_button_conner_radius)));
            this.f0 = obtainStyledAttributes.getColor(t23.SegmentedGroup_sc_tint_color, m70.d(getContext(), wy2.radio_button_selected_color));
            this.h0 = obtainStyledAttributes.getColor(t23.SegmentedGroup_sc_checked_text_color, m70.d(getContext(), 17170443));
            this.g0 = obtainStyledAttributes.getColor(t23.SegmentedGroup_sc_unchecked_tint_color, m70.d(getContext(), wy2.radio_button_unselected_color));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public void f() {
        this.l0 = new HashMap<>();
        int childCount = super.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            g(childAt);
            if (i == childCount - 1) {
                return;
            }
            RadioGroup.LayoutParams layoutParams = (RadioGroup.LayoutParams) childAt.getLayoutParams();
            RadioGroup.LayoutParams layoutParams2 = new RadioGroup.LayoutParams(layoutParams.width, layoutParams.height, layoutParams.weight);
            if (getOrientation() == 0) {
                layoutParams2.setMargins(0, 0, -this.a, 0);
            } else {
                layoutParams2.setMargins(0, 0, 0, -this.a);
            }
            childAt.setLayoutParams(layoutParams2);
        }
    }

    public final void g(View view) {
        int d = this.i0.d();
        int e = this.i0.e();
        ((Button) view).setTextColor(new ColorStateList(new int[][]{new int[]{-16842912}, new int[]{16842912}}, new int[]{this.f0, this.h0}));
        Drawable mutate = m70.f(getContext(), d).mutate();
        Drawable mutate2 = m70.f(getContext(), e).mutate();
        GradientDrawable gradientDrawable = (GradientDrawable) mutate;
        gradientDrawable.setColor(this.f0);
        gradientDrawable.setStroke(this.a, this.f0);
        GradientDrawable gradientDrawable2 = (GradientDrawable) mutate2;
        gradientDrawable2.setStroke(this.a, this.f0);
        gradientDrawable2.setColor(this.g0);
        gradientDrawable.setCornerRadii(this.i0.b(view));
        gradientDrawable2.setCornerRadii(this.i0.b(view));
        GradientDrawable gradientDrawable3 = (GradientDrawable) m70.f(getContext(), e).mutate();
        gradientDrawable3.setStroke(this.a, this.f0);
        gradientDrawable3.setColor(this.g0);
        gradientDrawable3.setCornerRadii(this.i0.b(view));
        gradientDrawable3.setColor(Color.argb(50, Color.red(this.f0), Color.green(this.f0), Color.blue(this.f0)));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{mutate2, gradientDrawable3});
        TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{mutate2, mutate});
        if (((RadioButton) view).isChecked()) {
            transitionDrawable.reverseTransition(0);
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{-16842912, 16842919}, layerDrawable);
        stateListDrawable.addState(StateSet.WILD_CARD, transitionDrawable);
        this.l0.put(Integer.valueOf(view.getId()), transitionDrawable);
        view.setBackground(stateListDrawable);
        super.setOnCheckedChangeListener(new a());
    }

    @Override // android.widget.RadioGroup, android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        f();
    }

    @Override // android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        this.l0.remove(Integer.valueOf(view.getId()));
    }

    @Override // android.widget.RadioGroup
    public void setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener onCheckedChangeListener) {
        this.k0 = onCheckedChangeListener;
    }

    public void setTintColor(int i) {
        this.f0 = i;
        f();
    }

    public void setUnCheckedTintColor(int i, int i2) {
        this.g0 = i;
        f();
    }

    public void setTintColor(int i, int i2) {
        this.f0 = i;
        this.h0 = i2;
        f();
    }

    public SegmentedGroup(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h0 = -1;
        getResources();
        this.f0 = m70.d(getContext(), wy2.radio_button_selected_color);
        this.g0 = m70.d(getContext(), wy2.radio_button_unselected_color);
        this.a = (int) getResources().getDimension(zy2.radio_button_stroke_border);
        this.j0 = Float.valueOf(getResources().getDimension(zy2.radio_button_conner_radius));
        e(attributeSet);
        this.i0 = new b(this.j0.floatValue());
    }
}
