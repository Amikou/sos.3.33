package info.hoang8f.android.segmented;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import info.hoang8f.android.segmented.utils.BootstrapText;
import info.hoang8f.android.segmented.utils.a;

/* loaded from: classes2.dex */
public class AwesomeRadioButton extends AppCompatRadioButton {
    public BootstrapText h0;

    public AwesomeRadioButton(Context context) {
        super(context);
    }

    public final void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, t23.AwesomeRadioButton);
        try {
            String string = obtainStyledAttributes.getString(t23.AwesomeRadioButton_awesome_text);
            if (string != null) {
                setMarkdownText(string);
            }
            b();
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public void b() {
        BootstrapText bootstrapText = this.h0;
        if (bootstrapText != null) {
            setText(bootstrapText);
        }
    }

    public BootstrapText getBootstrapText() {
        return this.h0;
    }

    public void setBootstrapText(BootstrapText bootstrapText) {
        this.h0 = bootstrapText;
        b();
    }

    public void setMarkdownText(String str) {
        setBootstrapText(a.b(getContext(), str + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, isInEditMode()));
    }

    public AwesomeRadioButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    public AwesomeRadioButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(attributeSet);
    }
}
