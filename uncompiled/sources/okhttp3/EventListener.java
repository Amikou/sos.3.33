package okhttp3;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;

/* compiled from: EventListener.kt */
/* loaded from: classes2.dex */
public abstract class EventListener {
    public static final Companion Companion = new Companion(null);
    public static final EventListener NONE = new EventListener() { // from class: okhttp3.EventListener$Companion$NONE$1
    };

    /* compiled from: EventListener.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: EventListener.kt */
    /* loaded from: classes2.dex */
    public interface Factory {
        EventListener create(Call call);
    }

    public void cacheConditionalHit(Call call, Response response) {
        fs1.f(call, "call");
        fs1.f(response, "cachedResponse");
    }

    public void cacheHit(Call call, Response response) {
        fs1.f(call, "call");
        fs1.f(response, "response");
    }

    public void cacheMiss(Call call) {
        fs1.f(call, "call");
    }

    public void callEnd(Call call) {
        fs1.f(call, "call");
    }

    public void callFailed(Call call, IOException iOException) {
        fs1.f(call, "call");
        fs1.f(iOException, "ioe");
    }

    public void callStart(Call call) {
        fs1.f(call, "call");
    }

    public void canceled(Call call) {
        fs1.f(call, "call");
    }

    public void connectEnd(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol) {
        fs1.f(call, "call");
        fs1.f(inetSocketAddress, "inetSocketAddress");
        fs1.f(proxy, "proxy");
    }

    public void connectFailed(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol, IOException iOException) {
        fs1.f(call, "call");
        fs1.f(inetSocketAddress, "inetSocketAddress");
        fs1.f(proxy, "proxy");
        fs1.f(iOException, "ioe");
    }

    public void connectStart(Call call, InetSocketAddress inetSocketAddress, Proxy proxy) {
        fs1.f(call, "call");
        fs1.f(inetSocketAddress, "inetSocketAddress");
        fs1.f(proxy, "proxy");
    }

    public void connectionAcquired(Call call, Connection connection) {
        fs1.f(call, "call");
        fs1.f(connection, "connection");
    }

    public void connectionReleased(Call call, Connection connection) {
        fs1.f(call, "call");
        fs1.f(connection, "connection");
    }

    public void dnsEnd(Call call, String str, List<InetAddress> list) {
        fs1.f(call, "call");
        fs1.f(str, "domainName");
        fs1.f(list, "inetAddressList");
    }

    public void dnsStart(Call call, String str) {
        fs1.f(call, "call");
        fs1.f(str, "domainName");
    }

    public void proxySelectEnd(Call call, HttpUrl httpUrl, List<Proxy> list) {
        fs1.f(call, "call");
        fs1.f(httpUrl, "url");
        fs1.f(list, "proxies");
    }

    public void proxySelectStart(Call call, HttpUrl httpUrl) {
        fs1.f(call, "call");
        fs1.f(httpUrl, "url");
    }

    public void requestBodyEnd(Call call, long j) {
        fs1.f(call, "call");
    }

    public void requestBodyStart(Call call) {
        fs1.f(call, "call");
    }

    public void requestFailed(Call call, IOException iOException) {
        fs1.f(call, "call");
        fs1.f(iOException, "ioe");
    }

    public void requestHeadersEnd(Call call, Request request) {
        fs1.f(call, "call");
        fs1.f(request, "request");
    }

    public void requestHeadersStart(Call call) {
        fs1.f(call, "call");
    }

    public void responseBodyEnd(Call call, long j) {
        fs1.f(call, "call");
    }

    public void responseBodyStart(Call call) {
        fs1.f(call, "call");
    }

    public void responseFailed(Call call, IOException iOException) {
        fs1.f(call, "call");
        fs1.f(iOException, "ioe");
    }

    public void responseHeadersEnd(Call call, Response response) {
        fs1.f(call, "call");
        fs1.f(response, "response");
    }

    public void responseHeadersStart(Call call) {
        fs1.f(call, "call");
    }

    public void satisfactionFailure(Call call, Response response) {
        fs1.f(call, "call");
        fs1.f(response, "response");
    }

    public void secureConnectEnd(Call call, Handshake handshake) {
        fs1.f(call, "call");
    }

    public void secureConnectStart(Call call) {
        fs1.f(call, "call");
    }
}
