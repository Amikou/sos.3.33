package okhttp3;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.jvm.internal.Lambda;

/* compiled from: Handshake.kt */
/* loaded from: classes2.dex */
public final class Handshake$peerCertificates$2 extends Lambda implements rc1<List<? extends Certificate>> {
    public final /* synthetic */ rc1 $peerCertificatesFn;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Handshake$peerCertificates$2(rc1 rc1Var) {
        super(0);
        this.$peerCertificatesFn = rc1Var;
    }

    @Override // defpackage.rc1
    public final List<? extends Certificate> invoke() {
        try {
            return (List) this.$peerCertificatesFn.invoke();
        } catch (SSLPeerUnverifiedException unused) {
            return b20.g();
        }
    }
}
