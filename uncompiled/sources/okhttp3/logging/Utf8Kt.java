package okhttp3.logging;

import java.io.EOFException;
import okio.b;

/* compiled from: utf8.kt */
/* loaded from: classes2.dex */
public final class Utf8Kt {
    public static final boolean isProbablyUtf8(b bVar) {
        fs1.f(bVar, "$this$isProbablyUtf8");
        try {
            b bVar2 = new b();
            bVar.f(bVar2, 0L, u33.e(bVar.a0(), 64L));
            for (int i = 0; i < 16; i++) {
                if (bVar2.c0()) {
                    return true;
                }
                int W = bVar2.W();
                if (Character.isISOControl(W) && !Character.isWhitespace(W)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }
}
