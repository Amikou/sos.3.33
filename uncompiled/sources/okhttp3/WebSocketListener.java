package okhttp3;

import okio.ByteString;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: WebSocketListener.kt */
/* loaded from: classes2.dex */
public abstract class WebSocketListener {
    public void onClosed(WebSocket webSocket, int i, String str) {
        fs1.f(webSocket, "webSocket");
        fs1.f(str, "reason");
    }

    public void onClosing(WebSocket webSocket, int i, String str) {
        fs1.f(webSocket, "webSocket");
        fs1.f(str, "reason");
    }

    public void onFailure(WebSocket webSocket, Throwable th, Response response) {
        fs1.f(webSocket, "webSocket");
        fs1.f(th, "t");
    }

    public void onMessage(WebSocket webSocket, String str) {
        fs1.f(webSocket, "webSocket");
        fs1.f(str, PublicResolver.FUNC_TEXT);
    }

    public void onMessage(WebSocket webSocket, ByteString byteString) {
        fs1.f(webSocket, "webSocket");
        fs1.f(byteString, "bytes");
    }

    public void onOpen(WebSocket webSocket, Response response) {
        fs1.f(webSocket, "webSocket");
        fs1.f(response, "response");
    }
}
