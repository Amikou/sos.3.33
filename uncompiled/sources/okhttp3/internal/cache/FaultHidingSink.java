package okhttp3.internal.cache;

import java.io.IOException;
import okio.b;
import okio.f;
import okio.m;

/* compiled from: FaultHidingSink.kt */
/* loaded from: classes2.dex */
public class FaultHidingSink extends f {
    private boolean hasErrors;
    private final tc1<IOException, te4> onException;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public FaultHidingSink(m mVar, tc1<? super IOException, te4> tc1Var) {
        super(mVar);
        fs1.f(mVar, "delegate");
        fs1.f(tc1Var, "onException");
        this.onException = tc1Var;
    }

    @Override // okio.f, okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (this.hasErrors) {
            return;
        }
        try {
            super.close();
        } catch (IOException e) {
            this.hasErrors = true;
            this.onException.invoke(e);
        }
    }

    @Override // okio.f, okio.m, java.io.Flushable
    public void flush() {
        if (this.hasErrors) {
            return;
        }
        try {
            super.flush();
        } catch (IOException e) {
            this.hasErrors = true;
            this.onException.invoke(e);
        }
    }

    public final tc1<IOException, te4> getOnException() {
        return this.onException;
    }

    @Override // okio.f, okio.m
    public void write(b bVar, long j) {
        fs1.f(bVar, "source");
        if (this.hasErrors) {
            bVar.skip(j);
            return;
        }
        try {
            super.write(bVar, j);
        } catch (IOException e) {
            this.hasErrors = true;
            this.onException.invoke(e);
        }
    }
}
