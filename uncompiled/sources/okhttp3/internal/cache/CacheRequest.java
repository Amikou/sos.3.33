package okhttp3.internal.cache;

import java.io.IOException;
import okio.m;

/* compiled from: CacheRequest.kt */
/* loaded from: classes2.dex */
public interface CacheRequest {
    void abort();

    m body() throws IOException;
}
