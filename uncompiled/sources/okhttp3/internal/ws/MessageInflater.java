package okhttp3.internal.ws;

import java.io.Closeable;
import java.io.IOException;
import java.util.zip.Inflater;
import okio.b;
import okio.j;
import okio.n;

/* compiled from: MessageInflater.kt */
/* loaded from: classes2.dex */
public final class MessageInflater implements Closeable {
    private final b deflatedBytes;
    private final Inflater inflater;
    private final j inflaterSource;
    private final boolean noContextTakeover;

    public MessageInflater(boolean z) {
        this.noContextTakeover = z;
        b bVar = new b();
        this.deflatedBytes = bVar;
        Inflater inflater = new Inflater(true);
        this.inflater = inflater;
        this.inflaterSource = new j((n) bVar, inflater);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.inflaterSource.close();
    }

    public final void inflate(b bVar) throws IOException {
        fs1.f(bVar, "buffer");
        if (this.deflatedBytes.a0() == 0) {
            if (this.noContextTakeover) {
                this.inflater.reset();
            }
            this.deflatedBytes.P0(bVar);
            this.deflatedBytes.V(65535);
            long bytesRead = this.inflater.getBytesRead() + this.deflatedBytes.a0();
            do {
                this.inflaterSource.a(bVar, Long.MAX_VALUE);
            } while (this.inflater.getBytesRead() < bytesRead);
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
