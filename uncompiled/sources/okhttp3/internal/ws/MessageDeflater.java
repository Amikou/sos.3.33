package okhttp3.internal.ws;

import java.io.Closeable;
import java.io.IOException;
import java.util.zip.Deflater;
import okio.ByteString;
import okio.b;
import okio.e;
import okio.m;

/* compiled from: MessageDeflater.kt */
/* loaded from: classes2.dex */
public final class MessageDeflater implements Closeable {
    private final b deflatedBytes;
    private final Deflater deflater;
    private final e deflaterSink;
    private final boolean noContextTakeover;

    public MessageDeflater(boolean z) {
        this.noContextTakeover = z;
        b bVar = new b();
        this.deflatedBytes = bVar;
        Deflater deflater = new Deflater(-1, true);
        this.deflater = deflater;
        this.deflaterSink = new e((m) bVar, deflater);
    }

    private final boolean endsWith(b bVar, ByteString byteString) {
        return bVar.G0(bVar.a0() - byteString.size(), byteString);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.deflaterSink.close();
    }

    public final void deflate(b bVar) throws IOException {
        ByteString byteString;
        fs1.f(bVar, "buffer");
        if (this.deflatedBytes.a0() == 0) {
            if (this.noContextTakeover) {
                this.deflater.reset();
            }
            this.deflaterSink.write(bVar, bVar.a0());
            this.deflaterSink.flush();
            b bVar2 = this.deflatedBytes;
            byteString = MessageDeflaterKt.EMPTY_DEFLATE_BLOCK;
            if (endsWith(bVar2, byteString)) {
                long a0 = this.deflatedBytes.a0() - 4;
                b.a M = b.M(this.deflatedBytes, null, 1, null);
                try {
                    M.b(a0);
                    yz.a(M, null);
                } finally {
                }
            } else {
                this.deflatedBytes.d0(0);
            }
            b bVar3 = this.deflatedBytes;
            bVar.write(bVar3, bVar3.a0());
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
