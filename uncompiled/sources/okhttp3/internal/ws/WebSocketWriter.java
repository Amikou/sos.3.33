package okhttp3.internal.ws;

import java.io.Closeable;
import java.io.IOException;
import java.util.Random;
import okio.ByteString;
import okio.b;
import okio.c;

/* compiled from: WebSocketWriter.kt */
/* loaded from: classes2.dex */
public final class WebSocketWriter implements Closeable {
    private final boolean isClient;
    private final b.a maskCursor;
    private final byte[] maskKey;
    private final b messageBuffer;
    private MessageDeflater messageDeflater;
    private final long minimumDeflateSize;
    private final boolean noContextTakeover;
    private final boolean perMessageDeflate;
    private final Random random;
    private final c sink;
    private final b sinkBuffer;
    private boolean writerClosed;

    public WebSocketWriter(boolean z, c cVar, Random random, boolean z2, boolean z3, long j) {
        fs1.f(cVar, "sink");
        fs1.f(random, "random");
        this.isClient = z;
        this.sink = cVar;
        this.random = random;
        this.perMessageDeflate = z2;
        this.noContextTakeover = z3;
        this.minimumDeflateSize = j;
        this.messageBuffer = new b();
        this.sinkBuffer = cVar.o();
        this.maskKey = z ? new byte[4] : null;
        this.maskCursor = z ? new b.a() : null;
    }

    private final void writeControlFrame(int i, ByteString byteString) throws IOException {
        if (!this.writerClosed) {
            int size = byteString.size();
            if (((long) size) <= 125) {
                this.sinkBuffer.d0(i | 128);
                if (this.isClient) {
                    this.sinkBuffer.d0(size | 128);
                    Random random = this.random;
                    byte[] bArr = this.maskKey;
                    fs1.d(bArr);
                    random.nextBytes(bArr);
                    this.sinkBuffer.l1(this.maskKey);
                    if (size > 0) {
                        long a0 = this.sinkBuffer.a0();
                        this.sinkBuffer.n1(byteString);
                        b bVar = this.sinkBuffer;
                        b.a aVar = this.maskCursor;
                        fs1.d(aVar);
                        bVar.C(aVar);
                        this.maskCursor.c(a0);
                        WebSocketProtocol.INSTANCE.toggleMask(this.maskCursor, this.maskKey);
                        this.maskCursor.close();
                    }
                } else {
                    this.sinkBuffer.d0(size);
                    this.sinkBuffer.n1(byteString);
                }
                this.sink.flush();
                return;
            }
            throw new IllegalArgumentException("Payload size must be less than or equal to 125".toString());
        }
        throw new IOException("closed");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        MessageDeflater messageDeflater = this.messageDeflater;
        if (messageDeflater != null) {
            messageDeflater.close();
        }
    }

    public final Random getRandom() {
        return this.random;
    }

    public final c getSink() {
        return this.sink;
    }

    public final void writeClose(int i, ByteString byteString) throws IOException {
        ByteString byteString2 = ByteString.EMPTY;
        if (i != 0 || byteString != null) {
            if (i != 0) {
                WebSocketProtocol.INSTANCE.validateCloseCode(i);
            }
            b bVar = new b();
            bVar.P(i);
            if (byteString != null) {
                bVar.n1(byteString);
            }
            byteString2 = bVar.R0();
        }
        try {
            writeControlFrame(8, byteString2);
        } finally {
            this.writerClosed = true;
        }
    }

    public final void writeMessageFrame(int i, ByteString byteString) throws IOException {
        fs1.f(byteString, "data");
        if (!this.writerClosed) {
            this.messageBuffer.n1(byteString);
            int i2 = i | 128;
            if (this.perMessageDeflate && byteString.size() >= this.minimumDeflateSize) {
                MessageDeflater messageDeflater = this.messageDeflater;
                if (messageDeflater == null) {
                    messageDeflater = new MessageDeflater(this.noContextTakeover);
                    this.messageDeflater = messageDeflater;
                }
                messageDeflater.deflate(this.messageBuffer);
                i2 |= 64;
            }
            long a0 = this.messageBuffer.a0();
            this.sinkBuffer.d0(i2);
            int i3 = this.isClient ? 128 : 0;
            if (a0 <= 125) {
                this.sinkBuffer.d0(((int) a0) | i3);
            } else if (a0 <= WebSocketProtocol.PAYLOAD_SHORT_MAX) {
                this.sinkBuffer.d0(i3 | 126);
                this.sinkBuffer.P((int) a0);
            } else {
                this.sinkBuffer.d0(i3 | 127);
                this.sinkBuffer.L0(a0);
            }
            if (this.isClient) {
                Random random = this.random;
                byte[] bArr = this.maskKey;
                fs1.d(bArr);
                random.nextBytes(bArr);
                this.sinkBuffer.l1(this.maskKey);
                if (a0 > 0) {
                    b bVar = this.messageBuffer;
                    b.a aVar = this.maskCursor;
                    fs1.d(aVar);
                    bVar.C(aVar);
                    this.maskCursor.c(0L);
                    WebSocketProtocol.INSTANCE.toggleMask(this.maskCursor, this.maskKey);
                    this.maskCursor.close();
                }
            }
            this.sinkBuffer.write(this.messageBuffer, a0);
            this.sink.O();
            return;
        }
        throw new IOException("closed");
    }

    public final void writePing(ByteString byteString) throws IOException {
        fs1.f(byteString, "payload");
        writeControlFrame(9, byteString);
    }

    public final void writePong(ByteString byteString) throws IOException {
        fs1.f(byteString, "payload");
        writeControlFrame(10, byteString);
    }
}
