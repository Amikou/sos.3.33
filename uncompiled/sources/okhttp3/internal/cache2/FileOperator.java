package okhttp3.internal.cache2;

import java.io.IOException;
import java.nio.channels.FileChannel;
import okio.b;

/* compiled from: FileOperator.kt */
/* loaded from: classes2.dex */
public final class FileOperator {
    private final FileChannel fileChannel;

    public FileOperator(FileChannel fileChannel) {
        fs1.f(fileChannel, "fileChannel");
        this.fileChannel = fileChannel;
    }

    public final void read(long j, b bVar, long j2) {
        fs1.f(bVar, "sink");
        if (j2 < 0) {
            throw new IndexOutOfBoundsException();
        }
        while (j2 > 0) {
            long transferTo = this.fileChannel.transferTo(j, j2, bVar);
            j += transferTo;
            j2 -= transferTo;
        }
    }

    public final void write(long j, b bVar, long j2) throws IOException {
        fs1.f(bVar, "source");
        if (j2 < 0 || j2 > bVar.a0()) {
            throw new IndexOutOfBoundsException();
        }
        while (j2 > 0) {
            long transferFrom = this.fileChannel.transferFrom(bVar, j, j2);
            j += transferFrom;
            j2 -= transferFrom;
        }
    }
}
