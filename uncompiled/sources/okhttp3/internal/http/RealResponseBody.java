package okhttp3.internal.http;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.d;

/* compiled from: RealResponseBody.kt */
/* loaded from: classes2.dex */
public final class RealResponseBody extends ResponseBody {
    private final long contentLength;
    private final String contentTypeString;
    private final d source;

    public RealResponseBody(String str, long j, d dVar) {
        fs1.f(dVar, "source");
        this.contentTypeString = str;
        this.contentLength = j;
        this.source = dVar;
    }

    @Override // okhttp3.ResponseBody
    public long contentLength() {
        return this.contentLength;
    }

    @Override // okhttp3.ResponseBody
    public MediaType contentType() {
        String str = this.contentTypeString;
        if (str != null) {
            return MediaType.Companion.parse(str);
        }
        return null;
    }

    @Override // okhttp3.ResponseBody
    public d source() {
        return this.source;
    }
}
