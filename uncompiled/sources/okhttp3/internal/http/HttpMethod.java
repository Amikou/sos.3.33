package okhttp3.internal.http;

/* compiled from: HttpMethod.kt */
/* loaded from: classes2.dex */
public final class HttpMethod {
    public static final HttpMethod INSTANCE = new HttpMethod();

    private HttpMethod() {
    }

    public static final boolean permitsRequestBody(String str) {
        fs1.f(str, "method");
        return (fs1.b(str, "GET") || fs1.b(str, "HEAD")) ? false : true;
    }

    public static final boolean requiresRequestBody(String str) {
        fs1.f(str, "method");
        return fs1.b(str, "POST") || fs1.b(str, "PUT") || fs1.b(str, "PATCH") || fs1.b(str, "PROPPATCH") || fs1.b(str, "REPORT");
    }

    public final boolean invalidatesCache(String str) {
        fs1.f(str, "method");
        return fs1.b(str, "POST") || fs1.b(str, "PATCH") || fs1.b(str, "PUT") || fs1.b(str, "DELETE") || fs1.b(str, "MOVE");
    }

    public final boolean redirectsToGet(String str) {
        fs1.f(str, "method");
        return !fs1.b(str, "PROPFIND");
    }

    public final boolean redirectsWithBody(String str) {
        fs1.f(str, "method");
        return fs1.b(str, "PROPFIND");
    }
}
