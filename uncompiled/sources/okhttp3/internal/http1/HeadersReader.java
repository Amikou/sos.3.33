package okhttp3.internal.http1;

import okhttp3.Headers;
import okio.d;

/* compiled from: HeadersReader.kt */
/* loaded from: classes2.dex */
public final class HeadersReader {
    public static final Companion Companion = new Companion(null);
    private static final int HEADER_LIMIT = 262144;
    private long headerLimit;
    private final d source;

    /* compiled from: HeadersReader.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    public HeadersReader(d dVar) {
        fs1.f(dVar, "source");
        this.source = dVar;
        this.headerLimit = (long) HEADER_LIMIT;
    }

    public final d getSource() {
        return this.source;
    }

    public final Headers readHeaders() {
        Headers.Builder builder = new Headers.Builder();
        while (true) {
            String readLine = readLine();
            if (readLine.length() == 0) {
                return builder.build();
            }
            builder.addLenient$okhttp(readLine);
        }
    }

    public final String readLine() {
        String p0 = this.source.p0(this.headerLimit);
        this.headerLimit -= p0.length();
        return p0;
    }
}
