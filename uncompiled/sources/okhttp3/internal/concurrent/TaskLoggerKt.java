package okhttp3.internal.concurrent;

import androidx.media3.common.PlaybackException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.http2.Http2Connection;

/* compiled from: TaskLogger.kt */
/* loaded from: classes2.dex */
public final class TaskLoggerKt {
    public static final /* synthetic */ void access$log(Task task, TaskQueue taskQueue, String str) {
        log(task, taskQueue, str);
    }

    public static final String formatDuration(long j) {
        String str;
        if (j <= -999500000) {
            str = ((j - 500000000) / ((long) Http2Connection.DEGRADED_PONG_TIMEOUT_NS)) + " s ";
        } else if (j <= -999500) {
            str = ((j - 500000) / ((long) PlaybackException.CUSTOM_ERROR_CODE_BASE)) + " ms";
        } else if (j <= 0) {
            str = ((j - 500) / 1000) + " µs";
        } else if (j < 999500) {
            str = ((j + 500) / 1000) + " µs";
        } else if (j < 999500000) {
            str = ((j + 500000) / ((long) PlaybackException.CUSTOM_ERROR_CODE_BASE)) + " ms";
        } else {
            str = ((j + 500000000) / ((long) Http2Connection.DEGRADED_PONG_TIMEOUT_NS)) + " s ";
        }
        lu3 lu3Var = lu3.a;
        String format = String.format("%6s", Arrays.copyOf(new Object[]{str}, 1));
        fs1.e(format, "java.lang.String.format(format, *args)");
        return format;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static final void log(Task task, TaskQueue taskQueue, String str) {
        Logger logger = TaskRunner.Companion.getLogger();
        StringBuilder sb = new StringBuilder();
        sb.append(taskQueue.getName$okhttp());
        sb.append(' ');
        lu3 lu3Var = lu3.a;
        String format = String.format("%-22s", Arrays.copyOf(new Object[]{str}, 1));
        fs1.e(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(": ");
        sb.append(task.getName());
        logger.fine(sb.toString());
    }

    public static final <T> T logElapsed(Task task, TaskQueue taskQueue, rc1<? extends T> rc1Var) {
        long j;
        fs1.f(task, "task");
        fs1.f(taskQueue, "queue");
        fs1.f(rc1Var, "block");
        boolean isLoggable = TaskRunner.Companion.getLogger().isLoggable(Level.FINE);
        if (isLoggable) {
            j = taskQueue.getTaskRunner$okhttp().getBackend().nanoTime();
            log(task, taskQueue, "starting");
        } else {
            j = -1;
        }
        try {
            T invoke = rc1Var.invoke();
            uq1.b(1);
            if (isLoggable) {
                log(task, taskQueue, "finished run in " + formatDuration(taskQueue.getTaskRunner$okhttp().getBackend().nanoTime() - j));
            }
            uq1.a(1);
            return invoke;
        } catch (Throwable th) {
            uq1.b(1);
            if (isLoggable) {
                log(task, taskQueue, "failed a run in " + formatDuration(taskQueue.getTaskRunner$okhttp().getBackend().nanoTime() - j));
            }
            uq1.a(1);
            throw th;
        }
    }

    public static final void taskLog(Task task, TaskQueue taskQueue, rc1<String> rc1Var) {
        fs1.f(task, "task");
        fs1.f(taskQueue, "queue");
        fs1.f(rc1Var, "messageBlock");
        if (TaskRunner.Companion.getLogger().isLoggable(Level.FINE)) {
            log(task, taskQueue, rc1Var.invoke());
        }
    }
}
