package okhttp3.internal.concurrent;

/* compiled from: TaskQueue.kt */
/* loaded from: classes2.dex */
public final class TaskQueue$schedule$2 extends Task {
    public final /* synthetic */ rc1 $block;
    public final /* synthetic */ String $name;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TaskQueue$schedule$2(rc1 rc1Var, String str, String str2) {
        super(str2, false, 2, null);
        this.$block = rc1Var;
        this.$name = str;
    }

    @Override // okhttp3.internal.concurrent.Task
    public long runOnce() {
        return ((Number) this.$block.invoke()).longValue();
    }
}
