package okhttp3.internal.concurrent;

/* compiled from: TaskQueue.kt */
/* loaded from: classes2.dex */
public final class TaskQueue$execute$1 extends Task {
    public final /* synthetic */ rc1 $block;
    public final /* synthetic */ boolean $cancelable;
    public final /* synthetic */ String $name;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public TaskQueue$execute$1(rc1 rc1Var, String str, boolean z, String str2, boolean z2) {
        super(str2, z2);
        this.$block = rc1Var;
        this.$name = str;
        this.$cancelable = z;
    }

    @Override // okhttp3.internal.concurrent.Task
    public long runOnce() {
        this.$block.invoke();
        return -1L;
    }
}
