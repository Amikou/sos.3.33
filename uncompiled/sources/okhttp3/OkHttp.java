package okhttp3;

/* compiled from: OkHttp.kt */
/* loaded from: classes2.dex */
public final class OkHttp {
    public static final OkHttp INSTANCE = new OkHttp();
    public static final String VERSION = "4.9.2";

    private OkHttp() {
    }
}
