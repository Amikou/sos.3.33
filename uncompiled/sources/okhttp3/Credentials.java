package okhttp3;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import okio.ByteString;

/* compiled from: Credentials.kt */
/* loaded from: classes2.dex */
public final class Credentials {
    public static final Credentials INSTANCE = new Credentials();

    private Credentials() {
    }

    public static final String basic(String str, String str2) {
        return basic$default(str, str2, null, 4, null);
    }

    public static final String basic(String str, String str2, Charset charset) {
        fs1.f(str, "username");
        fs1.f(str2, "password");
        fs1.f(charset, "charset");
        String base64 = ByteString.Companion.c(str + ':' + str2, charset).base64();
        return "Basic " + base64;
    }

    public static /* synthetic */ String basic$default(String str, String str2, Charset charset, int i, Object obj) {
        if ((i & 4) != 0) {
            charset = StandardCharsets.ISO_8859_1;
            fs1.e(charset, "ISO_8859_1");
        }
        return basic(str, str2, charset);
    }
}
