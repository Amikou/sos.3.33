package okhttp3;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import defpackage.un2;
import java.io.Closeable;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.http1.HeadersReader;
import okio.ByteString;
import okio.b;
import okio.d;
import okio.k;
import okio.n;
import okio.o;

/* compiled from: MultipartReader.kt */
/* loaded from: classes2.dex */
public final class MultipartReader implements Closeable {
    public static final Companion Companion = new Companion(null);
    private static final un2 afterBoundaryOptions;
    private final String boundary;
    private boolean closed;
    private final ByteString crlfDashDashBoundary;
    private PartSource currentPart;
    private final ByteString dashDashBoundary;
    private boolean noMoreParts;
    private int partCount;
    private final d source;

    /* compiled from: MultipartReader.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final un2 getAfterBoundaryOptions() {
            return MultipartReader.afterBoundaryOptions;
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: MultipartReader.kt */
    /* loaded from: classes2.dex */
    public static final class Part implements Closeable {
        private final d body;
        private final Headers headers;

        public Part(Headers headers, d dVar) {
            fs1.f(headers, "headers");
            fs1.f(dVar, "body");
            this.headers = headers;
            this.body = dVar;
        }

        public final d body() {
            return this.body;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.body.close();
        }

        public final Headers headers() {
            return this.headers;
        }
    }

    /* compiled from: MultipartReader.kt */
    /* loaded from: classes2.dex */
    public final class PartSource implements n {
        private final o timeout = new o();

        public PartSource() {
        }

        @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            if (fs1.b(MultipartReader.this.currentPart, this)) {
                MultipartReader.this.currentPart = null;
            }
        }

        @Override // okio.n
        public long read(b bVar, long j) {
            fs1.f(bVar, "sink");
            if (j >= 0) {
                if (fs1.b(MultipartReader.this.currentPart, this)) {
                    o timeout = MultipartReader.this.source.timeout();
                    o oVar = this.timeout;
                    long timeoutNanos = timeout.timeoutNanos();
                    long a = o.Companion.a(oVar.timeoutNanos(), timeout.timeoutNanos());
                    TimeUnit timeUnit = TimeUnit.NANOSECONDS;
                    timeout.timeout(a, timeUnit);
                    if (timeout.hasDeadline()) {
                        long deadlineNanoTime = timeout.deadlineNanoTime();
                        if (oVar.hasDeadline()) {
                            timeout.deadlineNanoTime(Math.min(timeout.deadlineNanoTime(), oVar.deadlineNanoTime()));
                        }
                        try {
                            long currentPartBytesRemaining = MultipartReader.this.currentPartBytesRemaining(j);
                            long read = currentPartBytesRemaining == 0 ? -1L : MultipartReader.this.source.read(bVar, currentPartBytesRemaining);
                            timeout.timeout(timeoutNanos, timeUnit);
                            if (oVar.hasDeadline()) {
                                timeout.deadlineNanoTime(deadlineNanoTime);
                            }
                            return read;
                        } catch (Throwable th) {
                            timeout.timeout(timeoutNanos, TimeUnit.NANOSECONDS);
                            if (oVar.hasDeadline()) {
                                timeout.deadlineNanoTime(deadlineNanoTime);
                            }
                            throw th;
                        }
                    }
                    if (oVar.hasDeadline()) {
                        timeout.deadlineNanoTime(oVar.deadlineNanoTime());
                    }
                    try {
                        long currentPartBytesRemaining2 = MultipartReader.this.currentPartBytesRemaining(j);
                        long read2 = currentPartBytesRemaining2 == 0 ? -1L : MultipartReader.this.source.read(bVar, currentPartBytesRemaining2);
                        timeout.timeout(timeoutNanos, timeUnit);
                        if (oVar.hasDeadline()) {
                            timeout.clearDeadline();
                        }
                        return read2;
                    } catch (Throwable th2) {
                        timeout.timeout(timeoutNanos, TimeUnit.NANOSECONDS);
                        if (oVar.hasDeadline()) {
                            timeout.clearDeadline();
                        }
                        throw th2;
                    }
                }
                throw new IllegalStateException("closed".toString());
            }
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        }

        @Override // okio.n
        public o timeout() {
            return this.timeout;
        }
    }

    static {
        un2.a aVar = un2.h0;
        ByteString.a aVar2 = ByteString.Companion;
        afterBoundaryOptions = aVar.d(aVar2.d("\r\n"), aVar2.d("--"), aVar2.d(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR), aVar2.d("\t"));
    }

    public MultipartReader(d dVar, String str) throws IOException {
        fs1.f(dVar, "source");
        fs1.f(str, "boundary");
        this.source = dVar;
        this.boundary = str;
        this.dashDashBoundary = new b().C0("--").C0(str).R0();
        this.crlfDashDashBoundary = new b().C0("\r\n--").C0(str).R0();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long currentPartBytesRemaining(long j) {
        this.source.A1(this.crlfDashDashBoundary.size());
        long r = this.source.o().r(this.crlfDashDashBoundary);
        if (r == -1) {
            return Math.min(j, (this.source.o().a0() - this.crlfDashDashBoundary.size()) + 1);
        }
        return Math.min(j, r);
    }

    public final String boundary() {
        return this.boundary;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.closed) {
            return;
        }
        this.closed = true;
        this.currentPart = null;
        this.source.close();
    }

    public final Part nextPart() throws IOException {
        if (!this.closed) {
            if (this.noMoreParts) {
                return null;
            }
            if (this.partCount == 0 && this.source.G0(0L, this.dashDashBoundary)) {
                this.source.skip(this.dashDashBoundary.size());
            } else {
                while (true) {
                    long currentPartBytesRemaining = currentPartBytesRemaining(8192L);
                    if (currentPartBytesRemaining == 0) {
                        break;
                    }
                    this.source.skip(currentPartBytesRemaining);
                }
                this.source.skip(this.crlfDashDashBoundary.size());
            }
            boolean z = false;
            while (true) {
                int h0 = this.source.h0(afterBoundaryOptions);
                if (h0 == -1) {
                    throw new ProtocolException("unexpected characters after boundary");
                }
                if (h0 == 0) {
                    this.partCount++;
                    Headers readHeaders = new HeadersReader(this.source).readHeaders();
                    PartSource partSource = new PartSource();
                    this.currentPart = partSource;
                    return new Part(readHeaders, k.d(partSource));
                } else if (h0 == 1) {
                    if (!z) {
                        if (this.partCount != 0) {
                            this.noMoreParts = true;
                            return null;
                        }
                        throw new ProtocolException("expected at least 1 part");
                    }
                    throw new ProtocolException("unexpected characters after boundary");
                } else if (h0 == 2 || h0 == 3) {
                    z = true;
                }
            }
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public MultipartReader(okhttp3.ResponseBody r3) throws java.io.IOException {
        /*
            r2 = this;
            java.lang.String r0 = "response"
            defpackage.fs1.f(r3, r0)
            okio.d r0 = r3.source()
            okhttp3.MediaType r3 = r3.contentType()
            if (r3 == 0) goto L1b
            java.lang.String r1 = "boundary"
            java.lang.String r3 = r3.parameter(r1)
            if (r3 == 0) goto L1b
            r2.<init>(r0, r3)
            return
        L1b:
            java.net.ProtocolException r3 = new java.net.ProtocolException
            java.lang.String r0 = "expected the Content-Type to have a boundary parameter"
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.MultipartReader.<init>(okhttp3.ResponseBody):void");
    }
}
