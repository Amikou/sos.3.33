package okhttp3;

import java.io.IOException;

/* compiled from: Protocol.kt */
/* loaded from: classes2.dex */
public enum Protocol {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2"),
    H2_PRIOR_KNOWLEDGE("h2_prior_knowledge"),
    QUIC("quic");
    
    public static final Companion Companion = new Companion(null);
    private final String protocol;

    /* compiled from: Protocol.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Protocol get(String str) throws IOException {
            fs1.f(str, "protocol");
            Protocol protocol = Protocol.HTTP_1_0;
            if (!fs1.b(str, protocol.protocol)) {
                protocol = Protocol.HTTP_1_1;
                if (!fs1.b(str, protocol.protocol)) {
                    protocol = Protocol.H2_PRIOR_KNOWLEDGE;
                    if (!fs1.b(str, protocol.protocol)) {
                        protocol = Protocol.HTTP_2;
                        if (!fs1.b(str, protocol.protocol)) {
                            protocol = Protocol.SPDY_3;
                            if (!fs1.b(str, protocol.protocol)) {
                                protocol = Protocol.QUIC;
                                if (!fs1.b(str, protocol.protocol)) {
                                    throw new IOException("Unexpected protocol: " + str);
                                }
                            }
                        }
                    }
                }
            }
            return protocol;
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    Protocol(String str) {
        this.protocol = str;
    }

    public static final Protocol get(String str) throws IOException {
        return Companion.get(str);
    }

    @Override // java.lang.Enum
    public String toString() {
        return this.protocol;
    }
}
