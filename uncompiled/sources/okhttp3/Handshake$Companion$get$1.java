package okhttp3;

import java.security.cert.Certificate;
import java.util.List;
import kotlin.jvm.internal.Lambda;

/* compiled from: Handshake.kt */
/* loaded from: classes2.dex */
public final class Handshake$Companion$get$1 extends Lambda implements rc1<List<? extends Certificate>> {
    public final /* synthetic */ List $peerCertificatesCopy;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public Handshake$Companion$get$1(List list) {
        super(0);
        this.$peerCertificatesCopy = list;
    }

    @Override // defpackage.rc1
    public final List<? extends Certificate> invoke() {
        return this.$peerCertificatesCopy;
    }
}
