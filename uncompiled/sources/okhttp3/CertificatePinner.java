package okhttp3;

import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.text.StringsKt__StringsKt;
import okhttp3.internal.HostnamesKt;
import okhttp3.internal.tls.CertificateChainCleaner;
import okio.ByteString;
import org.slf4j.Marker;

/* compiled from: CertificatePinner.kt */
/* loaded from: classes2.dex */
public final class CertificatePinner {
    public static final Companion Companion = new Companion(null);
    public static final CertificatePinner DEFAULT = new Builder().build();
    private final CertificateChainCleaner certificateChainCleaner;
    private final Set<Pin> pins;

    /* compiled from: CertificatePinner.kt */
    /* loaded from: classes2.dex */
    public static final class Builder {
        private final List<Pin> pins = new ArrayList();

        public final Builder add(String str, String... strArr) {
            fs1.f(str, "pattern");
            fs1.f(strArr, "pins");
            for (String str2 : strArr) {
                this.pins.add(new Pin(str, str2));
            }
            return this;
        }

        public final CertificatePinner build() {
            return new CertificatePinner(j20.o0(this.pins), null, 2, null);
        }

        public final List<Pin> getPins() {
            return this.pins;
        }
    }

    /* compiled from: CertificatePinner.kt */
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final String pin(Certificate certificate) {
            fs1.f(certificate, "certificate");
            if (certificate instanceof X509Certificate) {
                return "sha256/" + sha256Hash((X509Certificate) certificate).base64();
            }
            throw new IllegalArgumentException("Certificate pinning requires X509 certificates".toString());
        }

        public final ByteString sha1Hash(X509Certificate x509Certificate) {
            fs1.f(x509Certificate, "$this$sha1Hash");
            ByteString.a aVar = ByteString.Companion;
            PublicKey publicKey = x509Certificate.getPublicKey();
            fs1.e(publicKey, "publicKey");
            byte[] encoded = publicKey.getEncoded();
            fs1.e(encoded, "publicKey.encoded");
            return ByteString.a.h(aVar, encoded, 0, 0, 3, null).sha1();
        }

        public final ByteString sha256Hash(X509Certificate x509Certificate) {
            fs1.f(x509Certificate, "$this$sha256Hash");
            ByteString.a aVar = ByteString.Companion;
            PublicKey publicKey = x509Certificate.getPublicKey();
            fs1.e(publicKey, "publicKey");
            byte[] encoded = publicKey.getEncoded();
            fs1.e(encoded, "publicKey.encoded");
            return ByteString.a.h(aVar, encoded, 0, 0, 3, null).sha256();
        }

        public /* synthetic */ Companion(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: CertificatePinner.kt */
    /* loaded from: classes2.dex */
    public static final class Pin {
        private final ByteString hash;
        private final String hashAlgorithm;
        private final String pattern;

        public Pin(String str, String str2) {
            fs1.f(str, "pattern");
            fs1.f(str2, "pin");
            if ((dv3.H(str, "*.", false, 2, null) && StringsKt__StringsKt.Z(str, Marker.ANY_MARKER, 1, false, 4, null) == -1) || (dv3.H(str, "**.", false, 2, null) && StringsKt__StringsKt.Z(str, Marker.ANY_MARKER, 2, false, 4, null) == -1) || StringsKt__StringsKt.Z(str, Marker.ANY_MARKER, 0, false, 6, null) == -1) {
                String canonicalHost = HostnamesKt.toCanonicalHost(str);
                if (canonicalHost != null) {
                    this.pattern = canonicalHost;
                    if (dv3.H(str2, "sha1/", false, 2, null)) {
                        this.hashAlgorithm = "sha1";
                        ByteString.a aVar = ByteString.Companion;
                        String substring = str2.substring(5);
                        fs1.e(substring, "(this as java.lang.String).substring(startIndex)");
                        ByteString a = aVar.a(substring);
                        if (a != null) {
                            this.hash = a;
                            return;
                        }
                        throw new IllegalArgumentException("Invalid pin hash: " + str2);
                    } else if (dv3.H(str2, "sha256/", false, 2, null)) {
                        this.hashAlgorithm = "sha256";
                        ByteString.a aVar2 = ByteString.Companion;
                        String substring2 = str2.substring(7);
                        fs1.e(substring2, "(this as java.lang.String).substring(startIndex)");
                        ByteString a2 = aVar2.a(substring2);
                        if (a2 != null) {
                            this.hash = a2;
                            return;
                        }
                        throw new IllegalArgumentException("Invalid pin hash: " + str2);
                    } else {
                        throw new IllegalArgumentException("pins must start with 'sha256/' or 'sha1/': " + str2);
                    }
                }
                throw new IllegalArgumentException("Invalid pattern: " + str);
            }
            throw new IllegalArgumentException(("Unexpected pattern: " + str).toString());
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Pin) {
                Pin pin = (Pin) obj;
                return ((fs1.b(this.pattern, pin.pattern) ^ true) || (fs1.b(this.hashAlgorithm, pin.hashAlgorithm) ^ true) || (fs1.b(this.hash, pin.hash) ^ true)) ? false : true;
            }
            return false;
        }

        public final ByteString getHash() {
            return this.hash;
        }

        public final String getHashAlgorithm() {
            return this.hashAlgorithm;
        }

        public final String getPattern() {
            return this.pattern;
        }

        public int hashCode() {
            return (((this.pattern.hashCode() * 31) + this.hashAlgorithm.hashCode()) * 31) + this.hash.hashCode();
        }

        public final boolean matchesCertificate(X509Certificate x509Certificate) {
            fs1.f(x509Certificate, "certificate");
            String str = this.hashAlgorithm;
            int hashCode = str.hashCode();
            if (hashCode != -903629273) {
                if (hashCode == 3528965 && str.equals("sha1")) {
                    return fs1.b(this.hash, CertificatePinner.Companion.sha1Hash(x509Certificate));
                }
            } else if (str.equals("sha256")) {
                return fs1.b(this.hash, CertificatePinner.Companion.sha256Hash(x509Certificate));
            }
            return false;
        }

        public final boolean matchesHostname(String str) {
            fs1.f(str, "hostname");
            if (dv3.H(this.pattern, "**.", false, 2, null)) {
                int length = this.pattern.length() - 3;
                int length2 = str.length() - length;
                if (!dv3.y(str, str.length() - length, this.pattern, 3, length, false, 16, null)) {
                    return false;
                }
                if (length2 != 0 && str.charAt(length2 - 1) != '.') {
                    return false;
                }
            } else if (dv3.H(this.pattern, "*.", false, 2, null)) {
                int length3 = this.pattern.length() - 1;
                int length4 = str.length() - length3;
                if (!dv3.y(str, str.length() - length3, this.pattern, 1, length3, false, 16, null) || StringsKt__StringsKt.d0(str, '.', length4 - 1, false, 4, null) != -1) {
                    return false;
                }
            } else {
                return fs1.b(str, this.pattern);
            }
            return true;
        }

        public String toString() {
            return this.hashAlgorithm + '/' + this.hash.base64();
        }
    }

    public CertificatePinner(Set<Pin> set, CertificateChainCleaner certificateChainCleaner) {
        fs1.f(set, "pins");
        this.pins = set;
        this.certificateChainCleaner = certificateChainCleaner;
    }

    public static final String pin(Certificate certificate) {
        return Companion.pin(certificate);
    }

    public static final ByteString sha1Hash(X509Certificate x509Certificate) {
        return Companion.sha1Hash(x509Certificate);
    }

    public static final ByteString sha256Hash(X509Certificate x509Certificate) {
        return Companion.sha256Hash(x509Certificate);
    }

    public final void check(String str, List<? extends Certificate> list) throws SSLPeerUnverifiedException {
        fs1.f(str, "hostname");
        fs1.f(list, "peerCertificates");
        check$okhttp(str, new CertificatePinner$check$1(this, list, str));
    }

    public final void check$okhttp(String str, rc1<? extends List<? extends X509Certificate>> rc1Var) {
        fs1.f(str, "hostname");
        fs1.f(rc1Var, "cleanedPeerCertificatesFn");
        List<Pin> findMatchingPins = findMatchingPins(str);
        if (findMatchingPins.isEmpty()) {
            return;
        }
        List<? extends X509Certificate> invoke = rc1Var.invoke();
        for (X509Certificate x509Certificate : invoke) {
            ByteString byteString = null;
            ByteString byteString2 = null;
            for (Pin pin : findMatchingPins) {
                String hashAlgorithm = pin.getHashAlgorithm();
                int hashCode = hashAlgorithm.hashCode();
                if (hashCode != -903629273) {
                    if (hashCode == 3528965 && hashAlgorithm.equals("sha1")) {
                        if (byteString2 == null) {
                            byteString2 = Companion.sha1Hash(x509Certificate);
                        }
                        if (fs1.b(pin.getHash(), byteString2)) {
                            return;
                        }
                    }
                    throw new AssertionError("unsupported hashAlgorithm: " + pin.getHashAlgorithm());
                } else if (hashAlgorithm.equals("sha256")) {
                    if (byteString == null) {
                        byteString = Companion.sha256Hash(x509Certificate);
                    }
                    if (fs1.b(pin.getHash(), byteString)) {
                        return;
                    }
                } else {
                    throw new AssertionError("unsupported hashAlgorithm: " + pin.getHashAlgorithm());
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Certificate pinning failure!");
        sb.append("\n  Peer certificate chain:");
        for (X509Certificate x509Certificate2 : invoke) {
            sb.append("\n    ");
            sb.append(Companion.pin(x509Certificate2));
            sb.append(": ");
            Principal subjectDN = x509Certificate2.getSubjectDN();
            fs1.e(subjectDN, "element.subjectDN");
            sb.append(subjectDN.getName());
        }
        sb.append("\n  Pinned certificates for ");
        sb.append(str);
        sb.append(":");
        for (Pin pin2 : findMatchingPins) {
            sb.append("\n    ");
            sb.append(pin2);
        }
        String sb2 = sb.toString();
        fs1.e(sb2, "StringBuilder().apply(builderAction).toString()");
        throw new SSLPeerUnverifiedException(sb2);
    }

    public boolean equals(Object obj) {
        if (obj instanceof CertificatePinner) {
            CertificatePinner certificatePinner = (CertificatePinner) obj;
            if (fs1.b(certificatePinner.pins, this.pins) && fs1.b(certificatePinner.certificateChainCleaner, this.certificateChainCleaner)) {
                return true;
            }
        }
        return false;
    }

    public final List<Pin> findMatchingPins(String str) {
        fs1.f(str, "hostname");
        Set<Pin> set = this.pins;
        List<Pin> g = b20.g();
        for (Object obj : set) {
            if (((Pin) obj).matchesHostname(str)) {
                if (g.isEmpty()) {
                    g = new ArrayList<>();
                }
                qd4.b(g).add(obj);
            }
        }
        return g;
    }

    public final CertificateChainCleaner getCertificateChainCleaner$okhttp() {
        return this.certificateChainCleaner;
    }

    public final Set<Pin> getPins() {
        return this.pins;
    }

    public int hashCode() {
        int hashCode = (1517 + this.pins.hashCode()) * 41;
        CertificateChainCleaner certificateChainCleaner = this.certificateChainCleaner;
        return hashCode + (certificateChainCleaner != null ? certificateChainCleaner.hashCode() : 0);
    }

    public final CertificatePinner withCertificateChainCleaner$okhttp(CertificateChainCleaner certificateChainCleaner) {
        fs1.f(certificateChainCleaner, "certificateChainCleaner");
        return fs1.b(this.certificateChainCleaner, certificateChainCleaner) ? this : new CertificatePinner(this.pins, certificateChainCleaner);
    }

    public /* synthetic */ CertificatePinner(Set set, CertificateChainCleaner certificateChainCleaner, int i, qi0 qi0Var) {
        this(set, (i & 2) != 0 ? null : certificateChainCleaner);
    }

    public final void check(String str, Certificate... certificateArr) throws SSLPeerUnverifiedException {
        fs1.f(str, "hostname");
        fs1.f(certificateArr, "peerCertificates");
        check(str, ai.K(certificateArr));
    }
}
