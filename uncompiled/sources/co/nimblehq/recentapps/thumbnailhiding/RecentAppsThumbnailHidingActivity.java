package co.nimblehq.recentapps.thumbnailhiding;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import defpackage.p43;

/* compiled from: RecentAppsThumbnailHidingActivity.kt */
/* loaded from: classes.dex */
public abstract class RecentAppsThumbnailHidingActivity extends AppCompatActivity implements p43 {
    public final boolean a;
    public final boolean f0;

    @Override // defpackage.p43
    public void o(Activity activity, boolean z) {
        p43.a.a(this, activity, z);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (w() || v()) {
            m43.a(this, true);
        }
    }

    public boolean t() {
        return this.f0;
    }

    public boolean u() {
        return this.a;
    }

    public final boolean v() {
        return t() && je2.e(this);
    }

    public final boolean w() {
        return u() && Build.VERSION.SDK_INT < 26;
    }
}
