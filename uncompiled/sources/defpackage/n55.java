package defpackage;

import android.os.Handler;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: n55  reason: default package */
/* loaded from: classes.dex */
public abstract class n55 {
    public static volatile Handler d;
    public final tl5 a;
    public final Runnable b;
    public volatile long c;

    public n55(tl5 tl5Var) {
        zt2.j(tl5Var);
        this.a = tl5Var;
        this.b = new k55(this, tl5Var);
    }

    public abstract void a();

    public final void b(long j) {
        d();
        if (j >= 0) {
            this.c = this.a.a().a();
            if (f().postDelayed(this.b, j)) {
                return;
            }
            this.a.w().l().b("Failed to schedule delayed post. time", Long.valueOf(j));
        }
    }

    public final boolean c() {
        return this.c != 0;
    }

    public final void d() {
        this.c = 0L;
        f().removeCallbacks(this.b);
    }

    public final Handler f() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (n55.class) {
            if (d == null) {
                d = new z95(this.a.m().getMainLooper());
            }
            handler = d;
        }
        return handler;
    }
}
