package defpackage;

import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: qk5  reason: default package */
/* loaded from: classes.dex */
public final class qk5 extends vc5 {
    public final byte[] b;

    public qk5(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.b = bArr;
    }

    @Override // defpackage.vc5
    public final byte[] G1() {
        return this.b;
    }
}
