package defpackage;

/* compiled from: SystemProps.kt */
/* renamed from: b34  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class b34 {
    public static final int a = Runtime.getRuntime().availableProcessors();

    public static final int a() {
        return a;
    }

    public static final String b(String str) {
        try {
            return System.getProperty(str);
        } catch (SecurityException unused) {
            return null;
        }
    }
}
