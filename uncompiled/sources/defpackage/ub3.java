package defpackage;

import kotlinx.coroutines.flow.AbstractFlow;

/* compiled from: Builders.kt */
/* renamed from: ub3  reason: default package */
/* loaded from: classes2.dex */
public final class ub3<T> extends AbstractFlow<T> {
    public final hd1<k71<? super T>, q70<? super te4>, Object> a;

    /* JADX WARN: Multi-variable type inference failed */
    public ub3(hd1<? super k71<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        this.a = hd1Var;
    }

    @Override // kotlinx.coroutines.flow.AbstractFlow
    public Object e(k71<? super T> k71Var, q70<? super te4> q70Var) {
        Object invoke = this.a.invoke(k71Var, q70Var);
        return invoke == gs1.d() ? invoke : te4.a;
    }
}
