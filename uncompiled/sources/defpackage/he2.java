package defpackage;

import android.app.Activity;
import android.view.View;
import android.view.ViewParent;
import androidx.core.app.a;
import androidx.navigation.NavController;
import java.lang.ref.WeakReference;

/* compiled from: Navigation.java */
/* renamed from: he2  reason: default package */
/* loaded from: classes.dex */
public final class he2 {
    public static NavController a(Activity activity, int i) {
        NavController c = c(a.r(activity, i));
        if (c != null) {
            return c;
        }
        throw new IllegalStateException("Activity " + activity + " does not have a NavController set on " + i);
    }

    public static NavController b(View view) {
        NavController c = c(view);
        if (c != null) {
            return c;
        }
        throw new IllegalStateException("View " + view + " does not have a NavController set");
    }

    public static NavController c(View view) {
        while (view != null) {
            NavController d = d(view);
            if (d != null) {
                return d;
            }
            ViewParent parent = view.getParent();
            view = parent instanceof View ? (View) parent : null;
        }
        return null;
    }

    public static NavController d(View view) {
        Object tag = view.getTag(o03.nav_controller_view_tag);
        if (tag instanceof WeakReference) {
            return (NavController) ((WeakReference) tag).get();
        }
        if (tag instanceof NavController) {
            return (NavController) tag;
        }
        return null;
    }

    public static void e(View view, NavController navController) {
        view.setTag(o03.nav_controller_view_tag, navController);
    }
}
