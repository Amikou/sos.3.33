package defpackage;

import androidx.lifecycle.LiveData;

/* compiled from: LiveData.kt */
/* renamed from: q02  reason: default package */
/* loaded from: classes2.dex */
public final class q02 {

    /* compiled from: LiveData.kt */
    /* renamed from: q02$a */
    /* loaded from: classes2.dex */
    public static final class a implements tl2<T> {
        public final /* synthetic */ tl2<T> a;
        public final /* synthetic */ LiveData<T> b;

        public a(tl2<T> tl2Var, LiveData<T> liveData) {
            this.a = tl2Var;
            this.b = liveData;
        }

        @Override // defpackage.tl2
        public void onChanged(T t) {
            this.a.onChanged(t);
            this.b.removeObserver(this);
        }
    }

    public static final <T> void a(LiveData<T> liveData, rz1 rz1Var, tl2<T> tl2Var) {
        fs1.f(liveData, "<this>");
        fs1.f(rz1Var, "lifecycleOwner");
        fs1.f(tl2Var, "observer");
        liveData.observe(rz1Var, new a(tl2Var, liveData));
    }
}
