package defpackage;

import defpackage.yw1;

/* compiled from: KProperty.kt */
/* renamed from: zw1  reason: default package */
/* loaded from: classes2.dex */
public interface zw1<V> extends yw1<V>, rc1<V> {

    /* compiled from: KProperty.kt */
    /* renamed from: zw1$a */
    /* loaded from: classes2.dex */
    public interface a<V> extends yw1.a<V>, rc1<V> {
    }

    Object getDelegate();

    a<V> getGetter();
}
