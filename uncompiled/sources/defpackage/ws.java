package defpackage;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.format.MatchStrength;
import com.fasterxml.jackson.core.format.a;
import com.fasterxml.jackson.core.io.f;
import java.io.ByteArrayInputStream;
import java.io.CharConversionException;
import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/* compiled from: ByteSourceJsonBootstrapper.java */
/* renamed from: ws  reason: default package */
/* loaded from: classes.dex */
public final class ws {
    public final jm1 a;
    public final InputStream b;
    public final byte[] c;
    public int d;
    public int e;
    public final boolean f;
    public boolean g;
    public int h;

    public ws(jm1 jm1Var, InputStream inputStream) {
        this.g = true;
        this.a = jm1Var;
        this.b = inputStream;
        this.c = jm1Var.g();
        this.d = 0;
        this.e = 0;
        this.f = true;
    }

    public static MatchStrength h(a aVar) throws IOException {
        if (!aVar.f()) {
            return MatchStrength.INCONCLUSIVE;
        }
        byte e = aVar.e();
        if (e == -17) {
            if (!aVar.f()) {
                return MatchStrength.INCONCLUSIVE;
            }
            if (aVar.e() != -69) {
                return MatchStrength.NO_MATCH;
            }
            if (!aVar.f()) {
                return MatchStrength.INCONCLUSIVE;
            }
            if (aVar.e() != -65) {
                return MatchStrength.NO_MATCH;
            }
            if (!aVar.f()) {
                return MatchStrength.INCONCLUSIVE;
            }
            e = aVar.e();
        }
        int k = k(aVar, e);
        if (k < 0) {
            return MatchStrength.INCONCLUSIVE;
        }
        if (k == 123) {
            int j = j(aVar);
            if (j < 0) {
                return MatchStrength.INCONCLUSIVE;
            }
            if (j != 34 && j != 125) {
                return MatchStrength.NO_MATCH;
            }
            return MatchStrength.SOLID_MATCH;
        } else if (k == 91) {
            int j2 = j(aVar);
            if (j2 < 0) {
                return MatchStrength.INCONCLUSIVE;
            }
            if (j2 != 93 && j2 != 91) {
                return MatchStrength.SOLID_MATCH;
            }
            return MatchStrength.SOLID_MATCH;
        } else {
            MatchStrength matchStrength = MatchStrength.WEAK_MATCH;
            if (k == 34) {
                return matchStrength;
            }
            if (k > 57 || k < 48) {
                if (k == 45) {
                    int j3 = j(aVar);
                    if (j3 < 0) {
                        return MatchStrength.INCONCLUSIVE;
                    }
                    return (j3 > 57 || j3 < 48) ? MatchStrength.NO_MATCH : matchStrength;
                } else if (k == 110) {
                    return m(aVar, "ull", matchStrength);
                } else {
                    if (k == 116) {
                        return m(aVar, "rue", matchStrength);
                    }
                    if (k == 102) {
                        return m(aVar, "alse", matchStrength);
                    }
                    return MatchStrength.NO_MATCH;
                }
            }
            return matchStrength;
        }
    }

    public static int j(a aVar) throws IOException {
        if (aVar.f()) {
            return k(aVar, aVar.e());
        }
        return -1;
    }

    public static int k(a aVar, byte b) throws IOException {
        while (true) {
            int i = b & 255;
            if (i != 32 && i != 13 && i != 10 && i != 9) {
                return i;
            }
            if (!aVar.f()) {
                return -1;
            }
            b = aVar.e();
        }
    }

    public static int l(DataInput dataInput) throws IOException {
        int readUnsignedByte = dataInput.readUnsignedByte();
        if (readUnsignedByte != 239) {
            return readUnsignedByte;
        }
        int readUnsignedByte2 = dataInput.readUnsignedByte();
        if (readUnsignedByte2 == 187) {
            int readUnsignedByte3 = dataInput.readUnsignedByte();
            if (readUnsignedByte3 == 191) {
                return dataInput.readUnsignedByte();
            }
            throw new IOException("Unexpected byte 0x" + Integer.toHexString(readUnsignedByte3) + " following 0xEF 0xBB; should get 0xBF as part of UTF-8 BOM");
        }
        throw new IOException("Unexpected byte 0x" + Integer.toHexString(readUnsignedByte2) + " following 0xEF; should get 0xBB as part of UTF-8 BOM");
    }

    public static MatchStrength m(a aVar, String str, MatchStrength matchStrength) throws IOException {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!aVar.f()) {
                return MatchStrength.INCONCLUSIVE;
            }
            if (aVar.e() != str.charAt(i)) {
                return MatchStrength.NO_MATCH;
            }
        }
        return matchStrength;
    }

    public final boolean a(int i) {
        if ((65280 & i) == 0) {
            this.g = true;
        } else if ((i & 255) != 0) {
            return false;
        } else {
            this.g = false;
        }
        this.h = 2;
        return true;
    }

    public final boolean b(int i) throws IOException {
        if ((i >> 8) == 0) {
            this.g = true;
        } else if ((16777215 & i) == 0) {
            this.g = false;
        } else if (((-16711681) & i) == 0) {
            i("3412");
        } else if ((i & (-65281)) != 0) {
            return false;
        } else {
            i("2143");
        }
        this.h = 4;
        return true;
    }

    public JsonParser c(int i, c cVar, vs vsVar, zx zxVar, int i2) throws IOException {
        if (e() == JsonEncoding.UTF8 && JsonFactory.Feature.CANONICALIZE_FIELD_NAMES.enabledIn(i2)) {
            return new ke4(this.a, i, this.b, cVar, vsVar.G(i2), this.c, this.d, this.e, this.f);
        }
        return new b43(this.a, i, d(), cVar, zxVar.q(i2));
    }

    public Reader d() throws IOException {
        JsonEncoding l = this.a.l();
        int bits = l.bits();
        if (bits != 8 && bits != 16) {
            if (bits == 32) {
                jm1 jm1Var = this.a;
                return new f(jm1Var, this.b, this.c, this.d, this.e, jm1Var.l().isBigEndian());
            }
            throw new RuntimeException("Internal error");
        }
        InputStream inputStream = this.b;
        if (inputStream == null) {
            inputStream = new ByteArrayInputStream(this.c, this.d, this.e);
        } else if (this.d < this.e) {
            inputStream = new com.fasterxml.jackson.core.io.c(this.a, inputStream, this.c, this.d, this.e);
        }
        return new InputStreamReader(inputStream, l.getJavaName());
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x003f, code lost:
        if (a(r1 >>> 16) != false) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x005c, code lost:
        if (a((r1[r5 + 1] & 255) | ((r1[r5] & 255) << 8)) != false) goto L29;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.core.JsonEncoding e() throws java.io.IOException {
        /*
            r8 = this;
            r0 = 4
            boolean r1 = r8.f(r0)
            r2 = 2
            r3 = 1
            r4 = 0
            if (r1 == 0) goto L42
            byte[] r1 = r8.c
            int r5 = r8.d
            r6 = r1[r5]
            int r6 = r6 << 24
            int r7 = r5 + 1
            r7 = r1[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 16
            r6 = r6 | r7
            int r7 = r5 + 2
            r7 = r1[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            int r5 = r5 + 3
            r1 = r1[r5]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r1 = r1 | r6
            boolean r5 = r8.g(r1)
            if (r5 == 0) goto L32
            goto L5e
        L32:
            boolean r5 = r8.b(r1)
            if (r5 == 0) goto L39
            goto L5e
        L39:
            int r1 = r1 >>> 16
            boolean r1 = r8.a(r1)
            if (r1 == 0) goto L5f
            goto L5e
        L42:
            boolean r1 = r8.f(r2)
            if (r1 == 0) goto L5f
            byte[] r1 = r8.c
            int r5 = r8.d
            r6 = r1[r5]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 8
            int r5 = r5 + r3
            r1 = r1[r5]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r1 = r1 | r6
            boolean r1 = r8.a(r1)
            if (r1 == 0) goto L5f
        L5e:
            r4 = r3
        L5f:
            if (r4 != 0) goto L64
            com.fasterxml.jackson.core.JsonEncoding r0 = com.fasterxml.jackson.core.JsonEncoding.UTF8
            goto L8a
        L64:
            int r1 = r8.h
            if (r1 == r3) goto L88
            if (r1 == r2) goto L7e
            if (r1 != r0) goto L76
            boolean r0 = r8.g
            if (r0 == 0) goto L73
            com.fasterxml.jackson.core.JsonEncoding r0 = com.fasterxml.jackson.core.JsonEncoding.UTF32_BE
            goto L8a
        L73:
            com.fasterxml.jackson.core.JsonEncoding r0 = com.fasterxml.jackson.core.JsonEncoding.UTF32_LE
            goto L8a
        L76:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Internal error"
            r0.<init>(r1)
            throw r0
        L7e:
            boolean r0 = r8.g
            if (r0 == 0) goto L85
            com.fasterxml.jackson.core.JsonEncoding r0 = com.fasterxml.jackson.core.JsonEncoding.UTF16_BE
            goto L8a
        L85:
            com.fasterxml.jackson.core.JsonEncoding r0 = com.fasterxml.jackson.core.JsonEncoding.UTF16_LE
            goto L8a
        L88:
            com.fasterxml.jackson.core.JsonEncoding r0 = com.fasterxml.jackson.core.JsonEncoding.UTF8
        L8a:
            jm1 r1 = r8.a
            r1.u(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ws.e():com.fasterxml.jackson.core.JsonEncoding");
    }

    public boolean f(int i) throws IOException {
        int read;
        int i2 = this.e - this.d;
        while (i2 < i) {
            InputStream inputStream = this.b;
            if (inputStream == null) {
                read = -1;
            } else {
                byte[] bArr = this.c;
                int i3 = this.e;
                read = inputStream.read(bArr, i3, bArr.length - i3);
            }
            if (read < 1) {
                return false;
            }
            this.e += read;
            i2 += read;
        }
        return true;
    }

    public final boolean g(int i) throws IOException {
        if (i == -16842752) {
            i("3412");
        } else if (i == -131072) {
            this.d += 4;
            this.h = 4;
            this.g = false;
            return true;
        } else if (i == 65279) {
            this.g = true;
            this.d += 4;
            this.h = 4;
            return true;
        } else if (i == 65534) {
            i("2143");
        }
        int i2 = i >>> 16;
        if (i2 == 65279) {
            this.d += 2;
            this.h = 2;
            this.g = true;
            return true;
        } else if (i2 == 65534) {
            this.d += 2;
            this.h = 2;
            this.g = false;
            return true;
        } else if ((i >>> 8) == 15711167) {
            this.d += 3;
            this.h = 1;
            this.g = true;
            return true;
        } else {
            return false;
        }
    }

    public final void i(String str) throws IOException {
        throw new CharConversionException("Unsupported UCS-4 endianness (" + str + ") detected");
    }

    public ws(jm1 jm1Var, byte[] bArr, int i, int i2) {
        this.g = true;
        this.a = jm1Var;
        this.b = null;
        this.c = bArr;
        this.d = i;
        this.e = i + i2;
        this.f = false;
    }
}
