package defpackage;

import android.util.SparseArray;
import androidx.media3.common.g;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import defpackage.f84;
import defpackage.ny;
import java.io.IOException;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: BundledChunkExtractor.java */
/* renamed from: ks  reason: default package */
/* loaded from: classes.dex */
public final class ks implements r11, ny {
    public static final ny.a n0 = js.a;
    public static final ot2 o0 = new ot2();
    public final p11 a;
    public final int f0;
    public final j g0;
    public final SparseArray<a> h0 = new SparseArray<>();
    public boolean i0;
    public ny.b j0;
    public long k0;
    public wi3 l0;
    public j[] m0;

    /* compiled from: BundledChunkExtractor.java */
    /* renamed from: ks$a */
    /* loaded from: classes.dex */
    public static final class a implements f84 {
        public final int a;
        public final int b;
        public final j c;
        public final ks0 d = new ks0();
        public j e;
        public f84 f;
        public long g;

        public a(int i, int i2, j jVar) {
            this.a = i;
            this.b = i2;
            this.c = jVar;
        }

        @Override // defpackage.f84
        public /* synthetic */ void a(op2 op2Var, int i) {
            e84.b(this, op2Var, i);
        }

        @Override // defpackage.f84
        public void b(long j, int i, int i2, int i3, f84.a aVar) {
            long j2 = this.g;
            if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j >= j2) {
                this.f = this.d;
            }
            ((f84) b.j(this.f)).b(j, i, i2, i3, aVar);
        }

        @Override // defpackage.f84
        public void c(op2 op2Var, int i, int i2) {
            ((f84) b.j(this.f)).a(op2Var, i);
        }

        @Override // defpackage.f84
        public /* synthetic */ int d(g gVar, int i, boolean z) {
            return e84.a(this, gVar, i, z);
        }

        @Override // defpackage.f84
        public int e(g gVar, int i, boolean z, int i2) throws IOException {
            return ((f84) b.j(this.f)).d(gVar, i, z);
        }

        @Override // defpackage.f84
        public void f(j jVar) {
            j jVar2 = this.c;
            if (jVar2 != null) {
                jVar = jVar.j(jVar2);
            }
            this.e = jVar;
            ((f84) b.j(this.f)).f(this.e);
        }

        public void g(ny.b bVar, long j) {
            if (bVar == null) {
                this.f = this.d;
                return;
            }
            this.g = j;
            f84 f = bVar.f(this.a, this.b);
            this.f = f;
            j jVar = this.e;
            if (jVar != null) {
                f.f(jVar);
            }
        }
    }

    public ks(p11 p11Var, int i, j jVar) {
        this.a = p11Var;
        this.f0 = i;
        this.g0 = jVar;
    }

    public static /* synthetic */ ny h(int i, j jVar, boolean z, List list, f84 f84Var, ks2 ks2Var) {
        p11 xb1Var;
        String str = jVar.o0;
        if (y82.p(str)) {
            return null;
        }
        if (y82.o(str)) {
            xb1Var = new d52(1);
        } else {
            xb1Var = new xb1(z ? 4 : 0, null, null, list, f84Var);
        }
        return new ks(xb1Var, i, jVar);
    }

    @Override // defpackage.ny
    public void a() {
        this.a.a();
    }

    @Override // defpackage.ny
    public boolean b(q11 q11Var) throws IOException {
        int f = this.a.f(q11Var, o0);
        ii.g(f != 1);
        return f == 0;
    }

    @Override // defpackage.ny
    public void c(ny.b bVar, long j, long j2) {
        this.j0 = bVar;
        this.k0 = j2;
        if (!this.i0) {
            this.a.j(this);
            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                this.a.c(0L, j);
            }
            this.i0 = true;
            return;
        }
        p11 p11Var = this.a;
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j = 0;
        }
        p11Var.c(0L, j);
        for (int i = 0; i < this.h0.size(); i++) {
            this.h0.valueAt(i).g(bVar, j2);
        }
    }

    @Override // defpackage.ny
    public j[] d() {
        return this.m0;
    }

    @Override // defpackage.ny
    public py e() {
        wi3 wi3Var = this.l0;
        if (wi3Var instanceof py) {
            return (py) wi3Var;
        }
        return null;
    }

    @Override // defpackage.r11
    public f84 f(int i, int i2) {
        a aVar = this.h0.get(i);
        if (aVar == null) {
            ii.g(this.m0 == null);
            aVar = new a(i, i2, i2 == this.f0 ? this.g0 : null);
            aVar.g(this.j0, this.k0);
            this.h0.put(i, aVar);
        }
        return aVar;
    }

    @Override // defpackage.r11
    public void m() {
        j[] jVarArr = new j[this.h0.size()];
        for (int i = 0; i < this.h0.size(); i++) {
            jVarArr[i] = (j) ii.i(this.h0.valueAt(i).e);
        }
        this.m0 = jVarArr;
    }

    @Override // defpackage.r11
    public void p(wi3 wi3Var) {
        this.l0 = wi3Var;
    }
}
