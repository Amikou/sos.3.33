package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: ItemSimpleCheckboxBinding.java */
/* renamed from: gt1  reason: default package */
/* loaded from: classes2.dex */
public final class gt1 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final TextView c;

    public gt1(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, TextView textView, View view) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = textView;
    }

    public static gt1 a(View view) {
        int i = R.id.cbSelect;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbSelect);
        if (materialCheckBox != null) {
            i = R.id.tvName;
            TextView textView = (TextView) ai4.a(view, R.id.tvName);
            if (textView != null) {
                i = R.id.vDivider;
                View a = ai4.a(view, R.id.vDivider);
                if (a != null) {
                    return new gt1((ConstraintLayout) view, materialCheckBox, textView, a);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static gt1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_simple_checkbox, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
