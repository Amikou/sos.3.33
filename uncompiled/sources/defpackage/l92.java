package defpackage;

import com.bumptech.glide.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ModelLoaderRegistry.java */
/* renamed from: l92  reason: default package */
/* loaded from: classes.dex */
public class l92 {
    public final qa2 a;
    public final a b;

    /* compiled from: ModelLoaderRegistry.java */
    /* renamed from: l92$a */
    /* loaded from: classes.dex */
    public static class a {
        public final Map<Class<?>, C0198a<?>> a = new HashMap();

        /* compiled from: ModelLoaderRegistry.java */
        /* renamed from: l92$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0198a<Model> {
            public final List<j92<Model, ?>> a;

            public C0198a(List<j92<Model, ?>> list) {
                this.a = list;
            }
        }

        public void a() {
            this.a.clear();
        }

        public <Model> List<j92<Model, ?>> b(Class<Model> cls) {
            C0198a<?> c0198a = this.a.get(cls);
            if (c0198a == null) {
                return null;
            }
            return (List<j92<Model, ?>>) c0198a.a;
        }

        public <Model> void c(Class<Model> cls, List<j92<Model, ?>> list) {
            if (this.a.put(cls, new C0198a<>(list)) == null) {
                return;
            }
            throw new IllegalStateException("Already cached loaders for model: " + cls);
        }
    }

    public l92(et2<List<Throwable>> et2Var) {
        this(new qa2(et2Var));
    }

    public static <A> Class<A> b(A a2) {
        return (Class<A>) a2.getClass();
    }

    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var) {
        this.a.b(cls, cls2, k92Var);
        this.b.a();
    }

    public synchronized List<Class<?>> c(Class<?> cls) {
        return this.a.g(cls);
    }

    public <A> List<j92<A, ?>> d(A a2) {
        List<j92<A, ?>> e = e(b(a2));
        if (!e.isEmpty()) {
            int size = e.size();
            List<j92<A, ?>> emptyList = Collections.emptyList();
            boolean z = true;
            for (int i = 0; i < size; i++) {
                j92<A, ?> j92Var = e.get(i);
                if (j92Var.a(a2)) {
                    if (z) {
                        emptyList = new ArrayList<>(size - i);
                        z = false;
                    }
                    emptyList.add(j92Var);
                }
            }
            if (emptyList.isEmpty()) {
                throw new Registry.NoModelLoaderAvailableException(a2, e);
            }
            return emptyList;
        }
        throw new Registry.NoModelLoaderAvailableException(a2);
    }

    public final synchronized <A> List<j92<A, ?>> e(Class<A> cls) {
        List<j92<A, ?>> b;
        b = this.b.b(cls);
        if (b == null) {
            b = Collections.unmodifiableList(this.a.e(cls));
            this.b.c(cls, b);
        }
        return b;
    }

    public synchronized <Model, Data> void f(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var) {
        g(this.a.j(cls, cls2, k92Var));
        this.b.a();
    }

    public final <Model, Data> void g(List<k92<? extends Model, ? extends Data>> list) {
        for (k92<? extends Model, ? extends Data> k92Var : list) {
            k92Var.a();
        }
    }

    public l92(qa2 qa2Var) {
        this.b = new a();
        this.a = qa2Var;
    }
}
