package defpackage;

import java.util.concurrent.locks.LockSupport;
import kotlin.coroutines.CoroutineContext;

/* compiled from: Builders.kt */
/* renamed from: yq  reason: default package */
/* loaded from: classes2.dex */
public final class yq<T> extends t4<T> {
    public final Thread g0;
    public final xx0 h0;

    public yq(CoroutineContext coroutineContext, Thread thread, xx0 xx0Var) {
        super(coroutineContext, true, true);
        this.g0 = thread;
        this.h0 = xx0Var;
    }

    @Override // defpackage.bu1
    public void E(Object obj) {
        if (fs1.b(Thread.currentThread(), this.g0)) {
            return;
        }
        Thread thread = this.g0;
        n5.a();
        LockSupport.unpark(thread);
    }

    public final T L0() {
        n5.a();
        try {
            xx0 xx0Var = this.h0;
            if (xx0Var != null) {
                xx0.S(xx0Var, false, 1, null);
            }
            while (!Thread.interrupted()) {
                xx0 xx0Var2 = this.h0;
                long a0 = xx0Var2 == null ? Long.MAX_VALUE : xx0Var2.a0();
                if (e0()) {
                    xx0 xx0Var3 = this.h0;
                    if (xx0Var3 != null) {
                        xx0.m(xx0Var3, false, 1, null);
                    }
                    n5.a();
                    T t = (T) cu1.h(a0());
                    t30 t30Var = t instanceof t30 ? (t30) t : null;
                    if (t30Var == null) {
                        return t;
                    }
                    throw t30Var.a;
                }
                n5.a();
                LockSupport.parkNanos(this, a0);
            }
            InterruptedException interruptedException = new InterruptedException();
            I(interruptedException);
            throw interruptedException;
        } catch (Throwable th) {
            n5.a();
            throw th;
        }
    }

    @Override // defpackage.bu1
    public boolean f0() {
        return true;
    }
}
