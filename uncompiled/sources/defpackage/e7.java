package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityGetEmailBinding.java */
/* renamed from: e7  reason: default package */
/* loaded from: classes2.dex */
public final class e7 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final ConstraintLayout c;
    public final AppCompatImageView d;
    public final TextInputLayout e;
    public final View f;
    public final TextView g;
    public final sp3 h;
    public final TextView i;

    public e7(ConstraintLayout constraintLayout, MaterialButton materialButton, ConstraintLayout constraintLayout2, AppCompatImageView appCompatImageView, TextInputLayout textInputLayout, View view, TextView textView, sp3 sp3Var, TextView textView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = constraintLayout2;
        this.d = appCompatImageView;
        this.e = textInputLayout;
        this.f = view;
        this.g = textView;
        this.h = sp3Var;
        this.i = textView2;
    }

    public static e7 a(View view) {
        int i = R.id.btnEmail;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnEmail);
        if (materialButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.img_logo;
            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
            if (appCompatImageView != null) {
                i = R.id.tilEmail;
                TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilEmail);
                if (textInputLayout != null) {
                    i = R.id.tilEmailDivider;
                    View a = ai4.a(view, R.id.tilEmailDivider);
                    if (a != null) {
                        i = R.id.tilEmailTxt;
                        TextView textView = (TextView) ai4.a(view, R.id.tilEmailTxt);
                        if (textView != null) {
                            i = R.id.toolbar;
                            View a2 = ai4.a(view, R.id.toolbar);
                            if (a2 != null) {
                                sp3 a3 = sp3.a(a2);
                                i = R.id.tvVerifyNotice;
                                TextView textView2 = (TextView) ai4.a(view, R.id.tvVerifyNotice);
                                if (textView2 != null) {
                                    return new e7(constraintLayout, materialButton, constraintLayout, appCompatImageView, textInputLayout, a, textView, a3, textView2);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static e7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static e7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_get_email, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
