package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.widget.TextView;

/* compiled from: CalendarItemStyle.java */
/* renamed from: su  reason: default package */
/* loaded from: classes2.dex */
public final class su {
    public final Rect a;
    public final ColorStateList b;
    public final ColorStateList c;
    public final ColorStateList d;
    public final int e;
    public final pn3 f;

    public su(ColorStateList colorStateList, ColorStateList colorStateList2, ColorStateList colorStateList3, int i, pn3 pn3Var, Rect rect) {
        du2.c(rect.left);
        du2.c(rect.top);
        du2.c(rect.right);
        du2.c(rect.bottom);
        this.a = rect;
        this.b = colorStateList2;
        this.c = colorStateList;
        this.d = colorStateList3;
        this.e = i;
        this.f = pn3Var;
    }

    public static su a(Context context, int i) {
        du2.a(i != 0, "Cannot create a CalendarItemStyle with a styleResId of 0");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, o23.MaterialCalendarItem);
        Rect rect = new Rect(obtainStyledAttributes.getDimensionPixelOffset(o23.MaterialCalendarItem_android_insetLeft, 0), obtainStyledAttributes.getDimensionPixelOffset(o23.MaterialCalendarItem_android_insetTop, 0), obtainStyledAttributes.getDimensionPixelOffset(o23.MaterialCalendarItem_android_insetRight, 0), obtainStyledAttributes.getDimensionPixelOffset(o23.MaterialCalendarItem_android_insetBottom, 0));
        ColorStateList b = n42.b(context, obtainStyledAttributes, o23.MaterialCalendarItem_itemFillColor);
        ColorStateList b2 = n42.b(context, obtainStyledAttributes, o23.MaterialCalendarItem_itemTextColor);
        ColorStateList b3 = n42.b(context, obtainStyledAttributes, o23.MaterialCalendarItem_itemStrokeColor);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(o23.MaterialCalendarItem_itemStrokeWidth, 0);
        pn3 m = pn3.b(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendarItem_itemShapeAppearance, 0), obtainStyledAttributes.getResourceId(o23.MaterialCalendarItem_itemShapeAppearanceOverlay, 0)).m();
        obtainStyledAttributes.recycle();
        return new su(b, b2, b3, dimensionPixelSize, m, rect);
    }

    public int b() {
        return this.a.bottom;
    }

    public int c() {
        return this.a.top;
    }

    public void d(TextView textView) {
        o42 o42Var = new o42();
        o42 o42Var2 = new o42();
        o42Var.setShapeAppearanceModel(this.f);
        o42Var2.setShapeAppearanceModel(this.f);
        o42Var.a0(this.c);
        o42Var.k0(this.e, this.d);
        textView.setTextColor(this.b);
        Drawable rippleDrawable = Build.VERSION.SDK_INT >= 21 ? new RippleDrawable(this.b.withAlpha(30), o42Var, o42Var2) : o42Var;
        Rect rect = this.a;
        ei4.w0(textView, new InsetDrawable(rippleDrawable, rect.left, rect.top, rect.right, rect.bottom));
    }
}
