package defpackage;

import java.math.BigInteger;

/* renamed from: me1  reason: default package */
/* loaded from: classes2.dex */
public class me1 implements ke1 {
    public final ne1 a;
    public final qt0 b;

    public me1(xs0 xs0Var, ne1 ne1Var) {
        this.a = ne1Var;
        this.b = new pc3(xs0Var.n(ne1Var.b()));
    }

    @Override // defpackage.bt0
    public qt0 a() {
        return this.b;
    }

    @Override // defpackage.bt0
    public boolean b() {
        return true;
    }

    @Override // defpackage.ke1
    public BigInteger[] c(BigInteger bigInteger) {
        int c = this.a.c();
        BigInteger d = d(bigInteger, this.a.d(), c);
        BigInteger d2 = d(bigInteger, this.a.e(), c);
        ne1 ne1Var = this.a;
        return new BigInteger[]{bigInteger.subtract(d.multiply(ne1Var.f()).add(d2.multiply(ne1Var.h()))), d.multiply(ne1Var.g()).add(d2.multiply(ne1Var.i())).negate()};
    }

    public BigInteger d(BigInteger bigInteger, BigInteger bigInteger2, int i) {
        boolean z = bigInteger2.signum() < 0;
        BigInteger multiply = bigInteger.multiply(bigInteger2.abs());
        boolean testBit = multiply.testBit(i - 1);
        BigInteger shiftRight = multiply.shiftRight(i);
        if (testBit) {
            shiftRight = shiftRight.add(ws0.b);
        }
        return z ? shiftRight.negate() : shiftRight;
    }
}
