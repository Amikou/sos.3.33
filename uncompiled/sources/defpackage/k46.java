package defpackage;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: k46  reason: default package */
/* loaded from: classes.dex */
public final class k46 {
    public final ck5 a;

    public k46(ck5 ck5Var) {
        this.a = ck5Var;
    }

    public final void a() {
        this.a.q().e();
        if (e()) {
            if (d()) {
                this.a.A().u.b(null);
                Bundle bundle = new Bundle();
                bundle.putString("source", "(not set)");
                bundle.putString("medium", "(not set)");
                bundle.putString("_cis", "intent");
                bundle.putLong("_cc", 1L);
                this.a.F().X("auto", "_cmpx", bundle);
            } else {
                String a = this.a.A().u.a();
                if (TextUtils.isEmpty(a)) {
                    this.a.w().n().a("Cache still valid but referrer not found");
                } else {
                    long a2 = ((this.a.A().v.a() / 3600000) - 1) * 3600000;
                    Uri parse = Uri.parse(a);
                    Bundle bundle2 = new Bundle();
                    Pair pair = new Pair(parse.getPath(), bundle2);
                    for (String str : parse.getQueryParameterNames()) {
                        bundle2.putString(str, parse.getQueryParameter(str));
                    }
                    ((Bundle) pair.second).putLong("_cc", a2);
                    this.a.F().X((String) pair.first, "_cmp", (Bundle) pair.second);
                }
                this.a.A().u.b(null);
            }
            this.a.A().v.b(0L);
        }
    }

    public final void b(String str, Bundle bundle) {
        String uri;
        this.a.q().e();
        if (this.a.h()) {
            return;
        }
        if (bundle.isEmpty()) {
            uri = null;
        } else {
            if (true == str.isEmpty()) {
                str = "auto";
            }
            Uri.Builder builder = new Uri.Builder();
            builder.path(str);
            for (String str2 : bundle.keySet()) {
                builder.appendQueryParameter(str2, bundle.getString(str2));
            }
            uri = builder.build().toString();
        }
        if (TextUtils.isEmpty(uri)) {
            return;
        }
        this.a.A().u.b(uri);
        this.a.A().v.b(this.a.a().a());
    }

    public final void c() {
        if (e() && d()) {
            this.a.A().u.b(null);
        }
    }

    public final boolean d() {
        return e() && this.a.a().a() - this.a.A().v.a() > this.a.z().r(null, qf5.R);
    }

    public final boolean e() {
        return this.a.A().v.a() > 0;
    }
}
