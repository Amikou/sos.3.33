package defpackage;

import com.fasterxml.jackson.core.JsonLocation;

/* compiled from: UnresolvedId.java */
/* renamed from: ef4  reason: default package */
/* loaded from: classes.dex */
public class ef4 {
    public final Object a;
    public final JsonLocation b;
    public final Class<?> c;

    public ef4(Object obj, Class<?> cls, JsonLocation jsonLocation) {
        this.a = obj;
        this.c = cls;
        this.b = jsonLocation;
    }

    public String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = this.a;
        Class<?> cls = this.c;
        objArr[1] = cls == null ? "NULL" : cls.getName();
        objArr[2] = this.b;
        return String.format("Object id [%s] (for %s) at %s", objArr);
    }
}
