package defpackage;

/* compiled from: CharJVM.kt */
/* renamed from: xx  reason: default package */
/* loaded from: classes2.dex */
public class xx {
    public static final int a(int i) {
        if (2 > i || 36 < i) {
            throw new IllegalArgumentException("radix " + i + " was not in valid range " + new sr1(2, 36));
        }
        return i;
    }

    public static final int b(char c, int i) {
        return Character.digit((int) c, i);
    }

    public static final boolean c(char c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c);
    }
}
