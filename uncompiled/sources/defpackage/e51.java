package defpackage;

import android.content.Context;
import com.google.firebase.FirebaseCommonRegistrar;
import defpackage.jz1;

/* renamed from: e51  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class e51 implements jz1.a {
    public static final /* synthetic */ e51 a = new e51();

    @Override // defpackage.jz1.a
    public final String extract(Object obj) {
        String e;
        e = FirebaseCommonRegistrar.e((Context) obj);
        return e;
    }
}
