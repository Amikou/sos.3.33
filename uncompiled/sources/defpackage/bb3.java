package defpackage;

import android.database.Cursor;
import defpackage.qb3;

/* renamed from: bb3  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class bb3 implements qb3.b {
    public static final /* synthetic */ bb3 a = new bb3();

    @Override // defpackage.qb3.b
    public final Object apply(Object obj) {
        return Boolean.valueOf(((Cursor) obj).moveToNext());
    }
}
