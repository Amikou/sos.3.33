package defpackage;

import android.content.ContentValues;
import com.onesignal.influence.domain.OSInfluenceChannel;
import com.onesignal.influence.domain.OSInfluenceType;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: OSOutcomeEventsCache.kt */
/* renamed from: fk2  reason: default package */
/* loaded from: classes2.dex */
public final class fk2 {
    public final yj2 a;
    public final bn2 b;
    public final vk2 c;

    public fk2(yj2 yj2Var, bn2 bn2Var, vk2 vk2Var) {
        fs1.f(yj2Var, "logger");
        fs1.f(bn2Var, "dbHelper");
        fs1.f(vk2Var, "preferences");
        this.a = yj2Var;
        this.b = bn2Var;
        this.c = vk2Var;
    }

    public final void a(List<lj2> list, JSONArray jSONArray, OSInfluenceChannel oSInfluenceChannel) {
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    String string = jSONArray.getString(i);
                    fs1.e(string, "influenceId");
                    list.add(new lj2(string, oSInfluenceChannel));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final void b(List<lj2> list, qk2 qk2Var) {
        if (qk2Var != null) {
            JSONArray a = qk2Var.a();
            JSONArray b = qk2Var.b();
            a(list, a, OSInfluenceChannel.IAM);
            a(list, b, OSInfluenceChannel.NOTIFICATION);
        }
    }

    public final synchronized void c(String str, String str2) {
        fs1.f(str, "notificationTableName");
        fs1.f(str2, "notificationIdColumnName");
        StringBuilder sb = new StringBuilder();
        sb.append("NOT EXISTS(SELECT NULL FROM ");
        sb.append(str);
        sb.append(" n ");
        sb.append("WHERE");
        sb.append(" n.");
        sb.append(str2);
        sb.append(" = ");
        sb.append("channel_influence_id");
        sb.append(" AND ");
        sb.append("channel_type");
        sb.append(" = \"");
        String oSInfluenceChannel = OSInfluenceChannel.NOTIFICATION.toString();
        Locale locale = Locale.ROOT;
        fs1.e(locale, "Locale.ROOT");
        if (oSInfluenceChannel != null) {
            String lowerCase = oSInfluenceChannel.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            sb.append(lowerCase);
            sb.append("\")");
            this.b.d("cached_unique_outcome", sb.toString(), null);
        } else {
            throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
        }
    }

    public final synchronized void d(dk2 dk2Var) {
        fs1.f(dk2Var, "event");
        this.b.d("outcome", "timestamp = ?", new String[]{String.valueOf(dk2Var.c())});
    }

    /* JADX WARN: Removed duplicated region for block: B:30:0x00bd A[Catch: all -> 0x00d2, TRY_LEAVE, TryCatch #1 {, blocks: (B:4:0x0003, B:28:0x00b7, B:30:0x00bd, B:37:0x00c8, B:39:0x00ce, B:40:0x00d1), top: B:46:0x0003 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final synchronized java.util.List<defpackage.dk2> e() {
        /*
            r18 = this;
            r7 = r18
            monitor-enter(r18)
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch: java.lang.Throwable -> Ld2
            r8.<init>()     // Catch: java.lang.Throwable -> Ld2
            r9 = 0
            bn2 r10 = r7.b     // Catch: java.lang.Throwable -> Lc5
            java.lang.String r11 = "outcome"
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            android.database.Cursor r10 = r10.c(r11, r12, r13, r14, r15, r16, r17)     // Catch: java.lang.Throwable -> Lc5
            boolean r0 = r10.moveToFirst()     // Catch: java.lang.Throwable -> Lc2
            if (r0 == 0) goto Lb7
        L1f:
            java.lang.String r0 = "notification_influence_type"
            int r0 = r10.getColumnIndex(r0)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r0 = r10.getString(r0)     // Catch: java.lang.Throwable -> Lc2
            com.onesignal.influence.domain.OSInfluenceType$a r1 = com.onesignal.influence.domain.OSInfluenceType.Companion     // Catch: java.lang.Throwable -> Lc2
            com.onesignal.influence.domain.OSInfluenceType r0 = r1.a(r0)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r2 = "iam_influence_type"
            int r2 = r10.getColumnIndex(r2)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r2 = r10.getString(r2)     // Catch: java.lang.Throwable -> Lc2
            com.onesignal.influence.domain.OSInfluenceType r2 = r1.a(r2)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r1 = "notification_ids"
            int r1 = r10.getColumnIndex(r1)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r1 = r10.getString(r1)     // Catch: java.lang.Throwable -> Lc2
            if (r1 == 0) goto L4a
            goto L4c
        L4a:
            java.lang.String r1 = "[]"
        L4c:
            java.lang.String r3 = "iam_ids"
            int r3 = r10.getColumnIndex(r3)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r3 = r10.getString(r3)     // Catch: java.lang.Throwable -> Lc2
            if (r3 == 0) goto L59
            goto L5b
        L59:
            java.lang.String r3 = "[]"
        L5b:
            r5 = r3
            java.lang.String r3 = "name"
            int r3 = r10.getColumnIndex(r3)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r12 = r10.getString(r3)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r3 = "weight"
            int r3 = r10.getColumnIndex(r3)     // Catch: java.lang.Throwable -> Lc2
            float r14 = r10.getFloat(r3)     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r3 = "timestamp"
            int r3 = r10.getColumnIndex(r3)     // Catch: java.lang.Throwable -> Lc2
            long r15 = r10.getLong(r3)     // Catch: java.lang.Throwable -> Lc2
            qk2 r3 = new qk2     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            r4 = 3
            r3.<init>(r9, r9, r4, r9)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            qk2 r6 = new qk2     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            r6.<init>(r9, r9, r4, r9)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            pk2 r0 = r7.h(r0, r3, r6, r1)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            r1 = r18
            r4 = r6
            r6 = r0
            r1.f(r2, r3, r4, r5, r6)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            if (r0 == 0) goto L94
        L92:
            r13 = r0
            goto L9a
        L94:
            pk2 r0 = new pk2     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            r0.<init>(r9, r9)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            goto L92
        L9a:
            dk2 r0 = new dk2     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            java.lang.String r1 = "name"
            defpackage.fs1.e(r12, r1)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            r11 = r0
            r11.<init>(r12, r13, r14, r15)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            r8.add(r0)     // Catch: org.json.JSONException -> La9 java.lang.Throwable -> Lc2
            goto Lb1
        La9:
            r0 = move-exception
            yj2 r1 = r7.a     // Catch: java.lang.Throwable -> Lc2
            java.lang.String r2 = "Generating JSONArray from notifications ids outcome:JSON Failed."
            r1.error(r2, r0)     // Catch: java.lang.Throwable -> Lc2
        Lb1:
            boolean r0 = r10.moveToNext()     // Catch: java.lang.Throwable -> Lc2
            if (r0 != 0) goto L1f
        Lb7:
            boolean r0 = r10.isClosed()     // Catch: java.lang.Throwable -> Ld2
            if (r0 != 0) goto Lc0
            r10.close()     // Catch: java.lang.Throwable -> Ld2
        Lc0:
            monitor-exit(r18)
            return r8
        Lc2:
            r0 = move-exception
            r9 = r10
            goto Lc6
        Lc5:
            r0 = move-exception
        Lc6:
            if (r9 == 0) goto Ld1
            boolean r1 = r9.isClosed()     // Catch: java.lang.Throwable -> Ld2
            if (r1 != 0) goto Ld1
            r9.close()     // Catch: java.lang.Throwable -> Ld2
        Ld1:
            throw r0     // Catch: java.lang.Throwable -> Ld2
        Ld2:
            r0 = move-exception
            monitor-exit(r18)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.fk2.e():java.util.List");
    }

    public final pk2 f(OSInfluenceType oSInfluenceType, qk2 qk2Var, qk2 qk2Var2, String str, pk2 pk2Var) {
        pk2 c;
        pk2 d;
        int i = ek2.b[oSInfluenceType.ordinal()];
        if (i == 1) {
            qk2Var.c(new JSONArray(str));
            return (pk2Var == null || (c = pk2Var.c(qk2Var)) == null) ? new pk2(qk2Var, null) : c;
        } else if (i != 2) {
            return pk2Var;
        } else {
            qk2Var2.c(new JSONArray(str));
            return (pk2Var == null || (d = pk2Var.d(qk2Var2)) == null) ? new pk2(null, qk2Var2) : d;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:23:0x0090, code lost:
        if (r4.isClosed() == false) goto L34;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x0092, code lost:
        r4.close();
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x00a2, code lost:
        if (r4.isClosed() == false) goto L34;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final synchronized java.util.List<com.onesignal.influence.domain.a> g(java.lang.String r23, java.util.List<com.onesignal.influence.domain.a> r24) {
        /*
            r22 = this;
            r1 = r22
            r0 = r23
            monitor-enter(r22)
            java.lang.String r2 = "name"
            defpackage.fs1.f(r0, r2)     // Catch: java.lang.Throwable -> Lb3
            java.lang.String r2 = "influences"
            r3 = r24
            defpackage.fs1.f(r3, r2)     // Catch: java.lang.Throwable -> Lb3
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch: java.lang.Throwable -> Lb3
            r2.<init>()     // Catch: java.lang.Throwable -> Lb3
            r4 = 0
            java.util.Iterator r3 = r24.iterator()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
        L1b:
            boolean r5 = r3.hasNext()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            if (r5 == 0) goto L8a
            java.lang.Object r5 = r3.next()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            com.onesignal.influence.domain.a r5 = (com.onesignal.influence.domain.a) r5     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            org.json.JSONArray r6 = new org.json.JSONArray     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r6.<init>()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            org.json.JSONArray r7 = r5.b()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            if (r7 == 0) goto L1b
            int r8 = r7.length()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r9 = 0
            r10 = r9
        L38:
            if (r10 >= r8) goto L79
            java.lang.String r11 = r7.getString(r10)     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            com.onesignal.influence.domain.OSInfluenceChannel r12 = r5.c()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            java.lang.String[] r15 = new java.lang.String[r9]     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            java.lang.String r16 = "channel_influence_id = ? AND channel_type = ? AND name = ?"
            r13 = 3
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r14[r9] = r11     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            java.lang.String r12 = r12.toString()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r13 = 1
            r14[r13] = r12     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r12 = 2
            r14[r12] = r0     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            bn2 r13 = r1.b     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            java.lang.String r12 = "cached_unique_outcome"
            r18 = 0
            r19 = 0
            r20 = 0
            java.lang.String r21 = "1"
            r17 = r14
            r14 = r12
            android.database.Cursor r4 = r13.b(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            java.lang.String r12 = "cursor"
            defpackage.fs1.e(r4, r12)     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            int r12 = r4.getCount()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            if (r12 != 0) goto L76
            r6.put(r11)     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
        L76:
            int r10 = r10 + 1
            goto L38
        L79:
            int r7 = r6.length()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            if (r7 <= 0) goto L1b
            com.onesignal.influence.domain.a r5 = r5.a()     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r5.e(r6)     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            r2.add(r5)     // Catch: java.lang.Throwable -> L96 org.json.JSONException -> L98
            goto L1b
        L8a:
            if (r4 == 0) goto La5
            boolean r0 = r4.isClosed()     // Catch: java.lang.Throwable -> Lb3
            if (r0 != 0) goto La5
        L92:
            r4.close()     // Catch: java.lang.Throwable -> Lb3
            goto La5
        L96:
            r0 = move-exception
            goto La7
        L98:
            r0 = move-exception
            r0.printStackTrace()     // Catch: java.lang.Throwable -> L96
            if (r4 == 0) goto La5
            boolean r0 = r4.isClosed()     // Catch: java.lang.Throwable -> Lb3
            if (r0 != 0) goto La5
            goto L92
        La5:
            monitor-exit(r22)
            return r2
        La7:
            if (r4 == 0) goto Lb2
            boolean r2 = r4.isClosed()     // Catch: java.lang.Throwable -> Lb3
            if (r2 != 0) goto Lb2
            r4.close()     // Catch: java.lang.Throwable -> Lb3
        Lb2:
            throw r0     // Catch: java.lang.Throwable -> Lb3
        Lb3:
            r0 = move-exception
            monitor-exit(r22)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.fk2.g(java.lang.String, java.util.List):java.util.List");
    }

    public final pk2 h(OSInfluenceType oSInfluenceType, qk2 qk2Var, qk2 qk2Var2, String str) {
        pk2 pk2Var;
        int i = ek2.a[oSInfluenceType.ordinal()];
        if (i == 1) {
            qk2Var.d(new JSONArray(str));
            pk2Var = new pk2(qk2Var, null);
        } else if (i != 2) {
            return null;
        } else {
            qk2Var2.d(new JSONArray(str));
            pk2Var = new pk2(null, qk2Var2);
        }
        return pk2Var;
    }

    public final Set<String> i() {
        vk2 vk2Var = this.c;
        return vk2Var.c(vk2Var.f(), "PREFS_OS_UNATTRIBUTED_UNIQUE_OUTCOME_EVENTS_SENT", null);
    }

    public final boolean j() {
        vk2 vk2Var = this.c;
        return vk2Var.j(vk2Var.f(), this.c.h(), false);
    }

    public final synchronized void k(dk2 dk2Var) {
        OSInfluenceType oSInfluenceType;
        qk2 b;
        qk2 a;
        OSInfluenceType oSInfluenceType2;
        fs1.f(dk2Var, "eventParams");
        JSONArray jSONArray = new JSONArray();
        JSONArray jSONArray2 = new JSONArray();
        OSInfluenceType oSInfluenceType3 = OSInfluenceType.UNATTRIBUTED;
        pk2 b2 = dk2Var.b();
        if (b2 == null || (a = b2.a()) == null) {
            oSInfluenceType = oSInfluenceType3;
        } else {
            JSONArray b3 = a.b();
            if (b3 == null || b3.length() <= 0) {
                oSInfluenceType2 = oSInfluenceType3;
            } else {
                oSInfluenceType2 = OSInfluenceType.DIRECT;
                jSONArray = b3;
            }
            JSONArray a2 = a.a();
            if (a2 != null && a2.length() > 0) {
                oSInfluenceType3 = OSInfluenceType.DIRECT;
                jSONArray2 = a2;
            }
            oSInfluenceType = oSInfluenceType3;
            oSInfluenceType3 = oSInfluenceType2;
        }
        pk2 b4 = dk2Var.b();
        if (b4 != null && (b = b4.b()) != null) {
            JSONArray b5 = b.b();
            if (b5 != null && b5.length() > 0) {
                oSInfluenceType3 = OSInfluenceType.INDIRECT;
                jSONArray = b5;
            }
            JSONArray a3 = b.a();
            if (a3 != null && a3.length() > 0) {
                oSInfluenceType = OSInfluenceType.INDIRECT;
                jSONArray2 = a3;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("notification_ids", jSONArray.toString());
        contentValues.put("iam_ids", jSONArray2.toString());
        String str = oSInfluenceType3.toString();
        if (str != null) {
            String lowerCase = str.toLowerCase();
            fs1.e(lowerCase, "(this as java.lang.String).toLowerCase()");
            contentValues.put("notification_influence_type", lowerCase);
            String str2 = oSInfluenceType.toString();
            if (str2 != null) {
                String lowerCase2 = str2.toLowerCase();
                fs1.e(lowerCase2, "(this as java.lang.String).toLowerCase()");
                contentValues.put("iam_influence_type", lowerCase2);
                contentValues.put(PublicResolver.FUNC_NAME, dk2Var.a());
                contentValues.put("weight", Float.valueOf(dk2Var.d()));
                contentValues.put("timestamp", Long.valueOf(dk2Var.c()));
                this.b.e("outcome", null, contentValues);
            } else {
                throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
            }
        } else {
            throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
        }
    }

    public final void l(Set<String> set) {
        vk2 vk2Var = this.c;
        String f = vk2Var.f();
        fs1.d(set);
        vk2Var.g(f, "PREFS_OS_UNATTRIBUTED_UNIQUE_OUTCOME_EVENTS_SENT", set);
    }

    public final synchronized void m(dk2 dk2Var) {
        fs1.f(dk2Var, "eventParams");
        yj2 yj2Var = this.a;
        yj2Var.debug("OneSignal saveUniqueOutcomeEventParams: " + dk2Var);
        String a = dk2Var.a();
        ArrayList arrayList = new ArrayList();
        pk2 b = dk2Var.b();
        qk2 a2 = b != null ? b.a() : null;
        pk2 b2 = dk2Var.b();
        qk2 b3 = b2 != null ? b2.b() : null;
        b(arrayList, a2);
        b(arrayList, b3);
        for (lj2 lj2Var : arrayList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("channel_influence_id", lj2Var.b());
            contentValues.put("channel_type", lj2Var.a().toString());
            contentValues.put(PublicResolver.FUNC_NAME, a);
            this.b.e("cached_unique_outcome", null, contentValues);
        }
    }
}
