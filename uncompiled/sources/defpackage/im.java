package defpackage;

/* compiled from: Base32.java */
/* renamed from: im  reason: default package */
/* loaded from: classes2.dex */
public class im {
    public static final int[] a = {255, 255, 26, 27, 28, 29, 30, 31, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 255, 255, 255, 255, 255, 255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 255, 255, 255, 255, 255};

    public static byte[] a(String str) {
        int i;
        int length = (str.length() * 5) / 8;
        byte[] bArr = new byte[length];
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < str.length(); i4++) {
            int charAt = str.charAt(i4) - '0';
            if (charAt >= 0) {
                int[] iArr = a;
                if (charAt < iArr.length && (i = iArr[charAt]) != 255) {
                    if (i2 <= 3) {
                        i2 = (i2 + 5) % 8;
                        if (i2 == 0) {
                            bArr[i3] = (byte) (i | bArr[i3]);
                            i3++;
                            if (i3 >= length) {
                                break;
                            }
                        } else {
                            bArr[i3] = (byte) ((i << (8 - i2)) | bArr[i3]);
                        }
                    } else {
                        i2 = (i2 + 5) % 8;
                        bArr[i3] = (byte) (bArr[i3] | (i >>> i2));
                        i3++;
                        if (i3 >= length) {
                            break;
                        }
                        bArr[i3] = (byte) ((i << (8 - i2)) | bArr[i3]);
                    }
                }
            }
        }
        return bArr;
    }

    public static String b(byte[] bArr) {
        int i;
        int i2;
        StringBuffer stringBuffer = new StringBuffer(((bArr.length + 7) * 8) / 5);
        int i3 = 0;
        int i4 = 0;
        while (i3 < bArr.length) {
            int i5 = bArr[i3] >= 0 ? bArr[i3] : bArr[i3] + 256;
            if (i4 > 3) {
                i3++;
                if (i3 < bArr.length) {
                    i2 = bArr[i3] >= 0 ? bArr[i3] : bArr[i3] + 256;
                } else {
                    i2 = 0;
                }
                i4 = (i4 + 5) % 8;
                i = ((i5 & (255 >> i4)) << i4) | (i2 >> (8 - i4));
            } else {
                int i6 = i4 + 5;
                i = (i5 >> (8 - i6)) & 31;
                i4 = i6 % 8;
                if (i4 == 0) {
                    i3++;
                }
            }
            stringBuffer.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".charAt(i));
        }
        return stringBuffer.toString();
    }
}
