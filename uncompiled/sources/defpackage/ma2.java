package defpackage;

import java.util.List;

/* compiled from: MultiCacheKey.java */
/* renamed from: ma2  reason: default package */
/* loaded from: classes.dex */
public class ma2 implements wt {
    public final List<wt> a;

    @Override // defpackage.wt
    public boolean a() {
        return false;
    }

    @Override // defpackage.wt
    public String b() {
        return this.a.get(0).b();
    }

    public List<wt> c() {
        return this.a;
    }

    @Override // defpackage.wt
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ma2) {
            return this.a.equals(((ma2) obj).a);
        }
        return false;
    }

    @Override // defpackage.wt
    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return "MultiCacheKey:" + this.a.toString();
    }
}
