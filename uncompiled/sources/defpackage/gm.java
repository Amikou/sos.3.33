package defpackage;

import android.os.Handler;
import defpackage.gm;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: BandwidthMeter.java */
/* renamed from: gm  reason: default package */
/* loaded from: classes.dex */
public interface gm {

    /* compiled from: BandwidthMeter.java */
    /* renamed from: gm$a */
    /* loaded from: classes.dex */
    public interface a {

        /* compiled from: BandwidthMeter.java */
        /* renamed from: gm$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0176a {
            public final CopyOnWriteArrayList<C0177a> a = new CopyOnWriteArrayList<>();

            /* compiled from: BandwidthMeter.java */
            /* renamed from: gm$a$a$a  reason: collision with other inner class name */
            /* loaded from: classes.dex */
            public static final class C0177a {
                public final Handler a;
                public final a b;
                public boolean c;

                public C0177a(Handler handler, a aVar) {
                    this.a = handler;
                    this.b = aVar;
                }

                public void d() {
                    this.c = true;
                }
            }

            public static /* synthetic */ void d(C0177a c0177a, int i, long j, long j2) {
                c0177a.b.A(i, j, j2);
            }

            public void b(Handler handler, a aVar) {
                ii.e(handler);
                ii.e(aVar);
                e(aVar);
                this.a.add(new C0177a(handler, aVar));
            }

            public void c(final int i, final long j, final long j2) {
                Iterator<C0177a> it = this.a.iterator();
                while (it.hasNext()) {
                    final C0177a next = it.next();
                    if (!next.c) {
                        next.a.post(new Runnable() { // from class: fm
                            @Override // java.lang.Runnable
                            public final void run() {
                                gm.a.C0176a.d(gm.a.C0176a.C0177a.this, i, j, j2);
                            }
                        });
                    }
                }
            }

            public void e(a aVar) {
                Iterator<C0177a> it = this.a.iterator();
                while (it.hasNext()) {
                    C0177a next = it.next();
                    if (next.b == aVar) {
                        next.d();
                        this.a.remove(next);
                    }
                }
            }
        }

        void A(int i, long j, long j2);
    }

    long b();

    void c(Handler handler, a aVar);

    fa4 f();

    void g(a aVar);

    long h();
}
