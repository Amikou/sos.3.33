package defpackage;

import androidx.recyclerview.widget.RecyclerView;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.Random;

/* compiled from: pingpongping.java */
/* renamed from: qy4  reason: default package */
/* loaded from: classes.dex */
public class qy4 {
    public static final BigInteger k = BigInteger.valueOf(0);
    public static final BigInteger l = BigInteger.valueOf(1);
    public static final BigInteger m = BigInteger.valueOf(4);
    public static final BigInteger n = BigInteger.valueOf(5);
    public static final BigInteger o = BigInteger.valueOf(6);
    public static String p;
    public vq2 c;
    public byte[] f;
    public int a = 0;
    public bz4 b = new bz4(true);
    public int d = RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
    public int e = 768;
    public BigInteger g = BigInteger.valueOf(0);
    public BigInteger h = BigInteger.valueOf(0);
    public BigInteger i = BigInteger.valueOf(0);
    public BigInteger j = BigInteger.valueOf(0);

    static {
        BigInteger.valueOf(0L);
        p = "3331826350523954630311241967640951239858294736445361608086460265235096225983933203968500061419285926981287171155562181811253311132720944069446752355550722072540646541099242367017695428891868144180426342560303673050047649325575536678356628163268853184301619108551489143799507230749751313284539962710451153012503469991732518919004376111560492715132988352979816403246299836154209306136382046119628637054794112808035581653351864682935672170538730939963307751519414959213140044324492527775639241487923054459219166863271398472665208668730894374266515826156351847767160333379109315703307795489630821937186650430805236414320111877443611461276760182135837058775318162183952960557414502646346523814650817308641653094213272224663864175066958788046576178422969598329655544232966314206325330912385257831278348375483446261763187682794158803356489788013774805195010271059727551198993486030318095007967113710812986240362715215376495780841639";
    }

    public qy4() {
        this.c = new vq2();
        this.c = new vq2();
    }

    public BigInteger[] a() {
        BigInteger bigInteger;
        BigInteger[] bigIntegerArr = new BigInteger[2];
        this.g = b();
        if (this.a == 0) {
            BigInteger bigInteger2 = new BigInteger(this.e, new Random(new Date().getTime()));
            BigInteger remainder = bigInteger2.remainder(o);
            BigInteger bigInteger3 = l;
            BigInteger subtract = bigInteger2.subtract(remainder.subtract(bigInteger3));
            if (subtract.remainder(n).compareTo(k) == 0) {
                subtract = subtract.add(m);
            }
            this.h = subtract;
            bigInteger = this.h.modInverse(this.g.subtract(bigInteger3));
            f(this.h);
            f(bigInteger);
        } else {
            this.h = new BigInteger("9775946708954075513105450550018683278099002657566007032591109087139915126019509814779461256980775392304629751276791567952192046464423375586795491105751843");
            bigInteger = new BigInteger("50964855539960795901098958607004994025117615179372442973433536261416681314198551964402895305772357403748460032129176867663643586072061211882772054838536476598450232003253410400372227625010273806272462551734434888237388313469925208461646171086915766921109875451538270711018680655277456326315213259015672816949");
        }
        f(this.h);
        f(bigInteger);
        this.i = bigInteger;
        bigIntegerArr[0] = bigInteger;
        bigIntegerArr[1] = this.h;
        return bigIntegerArr;
    }

    public BigInteger b() {
        BigInteger valueOf = BigInteger.valueOf(0L);
        int i = this.d;
        if (i == 1024) {
            valueOf = new BigInteger("155315526351482395991155996351231807220169644828378937433223838972232518351958838087073321845624756550146945246003790108045940383194773439496051917019892370102341378990113959561895891019716873290512815434724157588460613638202017020672756091067223336194394910765309830876066246480156617492164140095427773547319");
            this.e = 768;
        } else if (i == 2048) {
            valueOf = new BigInteger("23305194876708628772942683712029936077909669703685601536444639373642192961312057628536252556993613535085814978074159499472186948366786511073521042149496833091653927921051971991878417514244523141840390929407035897870051133798325148487490127609979590754317732309227672185636344887595480613978815978023799438712228855016292399124518070073985663909563703587906667969897977595160213407719977739503277175679754624803077227852082230001597232751102744845455251210318774263448932578893084926219730828456570578353097506847447616887558397334732907988224984263418791672177125610795030874376815488104459799035590942083438936294203");
            this.e = 1009;
        } else if (i == 3072) {
            valueOf = new BigInteger(p);
            this.e = 1857;
        }
        this.g = valueOf;
        return valueOf;
    }

    public int c(String str, String str2) {
        return this.c.a(str, str2);
    }

    public BigInteger d(String str) {
        byte[] bArr;
        BigInteger valueOf = BigInteger.valueOf(0L);
        short charAt = (short) ((((byte) str.charAt(0)) >>> 4) & 15);
        try {
            byte[] a = ay1.a(lm.a(str));
            int i = 1;
            if (charAt > 0) {
                bArr = new byte[a.length + 1];
                bArr[0] = 0;
            } else {
                bArr = new byte[a.length];
                i = 0;
            }
            System.arraycopy(a, 0, bArr, i, a.length);
            return new BigInteger(bArr);
        } catch (Exception e) {
            System.out.println("cotstr(1): " + e.toString());
            return valueOf;
        }
    }

    public String e(BigInteger bigInteger) {
        StringBuffer stringBuffer = new StringBuffer("");
        byte[] byteArray = bigInteger.toByteArray();
        for (int i = byteArray[0] == 0 ? 1 : 0; i < byteArray.length; i++) {
            try {
                stringBuffer.append((char) byteArray[i]);
            } catch (Exception e) {
                stringBuffer.append("cotstr(1): " + e.toString());
            }
        }
        return stringBuffer.toString();
    }

    public String f(BigInteger bigInteger) {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] byteArray = bigInteger.toByteArray();
        for (int i = byteArray[0] == 0 ? 1 : 0; i < byteArray.length; i++) {
            try {
                stringBuffer.append((char) byteArray[i]);
            } catch (Exception e) {
                stringBuffer.append("cotstr(1): " + e.toString());
                return stringBuffer.toString();
            }
        }
        return lm.d(stringBuffer.toString());
    }

    public String g(String str) {
        return this.c.b(str);
    }

    public String h() {
        return e(this.j);
    }

    public final String i() {
        if (this.a == 0) {
            this.f = ay1.c(32);
        } else {
            this.f = new byte[32];
            for (int i = 0; i < 32; i++) {
                this.f[i] = (byte) "abcdefghijklmnopqrstuvwxyz123456".charAt(i);
            }
            PrintStream printStream = System.out;
            printStream.println("getpingData: using test key=abcdefghijklmnopqrstuvwxyz123456");
        }
        if (this.g.compareTo(k) != 0) {
            try {
                BigInteger bigInteger = new BigInteger(this.f);
                this.j = bigInteger;
                return f(bigInteger.modPow(this.h, this.g));
            } catch (Exception e) {
                return " error 3: " + e.toString();
            }
        }
        return " error 4: ";
    }

    public String j(String str) {
        return f(d(str).modPow(this.i, this.g));
    }

    public String k() {
        try {
            String i = i();
            if (i.compareTo("error") != 0) {
                this.b.g(this.f);
            }
            return i;
        } catch (Exception e) {
            return "Error State " + e.toString();
        }
    }

    public String l(String str) {
        return this.b.d(str);
    }
}
