package defpackage;

import android.net.Uri;
import androidx.media3.common.j;
import androidx.media3.datasource.b;
import androidx.media3.datasource.f;
import androidx.media3.exoplayer.upstream.Loader;
import java.util.List;
import java.util.Map;

/* compiled from: Chunk.java */
/* renamed from: my  reason: default package */
/* loaded from: classes.dex */
public abstract class my implements Loader.e {
    public final long a = u02.a();
    public final je0 b;
    public final int c;
    public final j d;
    public final int e;
    public final Object f;
    public final long g;
    public final long h;
    public final f i;

    public my(b bVar, je0 je0Var, int i, j jVar, int i2, Object obj, long j, long j2) {
        this.i = new f(bVar);
        this.b = (je0) ii.e(je0Var);
        this.c = i;
        this.d = jVar;
        this.e = i2;
        this.f = obj;
        this.g = j;
        this.h = j2;
    }

    public final long b() {
        return this.i.o();
    }

    public final Map<String, List<String>> d() {
        return this.i.q();
    }

    public final Uri e() {
        return this.i.p();
    }
}
