package defpackage;

import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: el5  reason: default package */
/* loaded from: classes.dex */
public final class el5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ pl5 f0;

    public el5(pl5 pl5Var, zzp zzpVar) {
        this.f0 = pl5Var;
        this.a = zzpVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        fw5 fw5Var2;
        fw5Var = this.f0.a;
        fw5Var.i();
        fw5Var2 = this.f0.a;
        zzp zzpVar = this.a;
        fw5Var2.q().e();
        fw5Var2.d0();
        zt2.f(zzpVar.a);
        t45 c = t45.c(zzpVar.z0);
        t45 f0 = fw5Var2.f0(zzpVar.a);
        fw5Var2.w().v().c("Setting consent, package, consent", zzpVar.a, c);
        fw5Var2.e0(zzpVar.a, c);
        if (c.i(f0)) {
            fw5Var2.k(zzpVar);
        }
    }
}
