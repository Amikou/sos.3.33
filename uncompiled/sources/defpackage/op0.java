package defpackage;

import java.util.concurrent.CancellationException;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: DispatchedContinuation.kt */
/* renamed from: op0 */
/* loaded from: classes2.dex */
public final class op0 {
    public static final k24 a = new k24("UNDEFINED");
    public static final k24 b = new k24("REUSABLE_CLAIMED");

    public static final <T> void b(q70<? super T> q70Var, Object obj, tc1<? super Throwable, te4> tc1Var) {
        boolean z;
        if (q70Var instanceof np0) {
            np0 np0Var = (np0) q70Var;
            Object c = w30.c(obj, tc1Var);
            if (np0Var.h0.j(np0Var.getContext())) {
                np0Var.j0 = c;
                np0Var.g0 = 1;
                np0Var.h0.h(np0Var.getContext(), np0Var);
                return;
            }
            ze0.a();
            xx0 b2 = j54.a.b();
            if (b2.W()) {
                np0Var.j0 = c;
                np0Var.g0 = 1;
                b2.N(np0Var);
                return;
            }
            b2.R(true);
            try {
                st1 st1Var = (st1) np0Var.getContext().get(st1.f);
                if (st1Var == null || st1Var.b()) {
                    z = false;
                } else {
                    CancellationException g = st1Var.g();
                    np0Var.a(c, g);
                    Result.a aVar = Result.Companion;
                    np0Var.resumeWith(Result.m52constructorimpl(o83.a(g)));
                    z = true;
                }
                if (!z) {
                    q70<T> q70Var2 = np0Var.i0;
                    Object obj2 = np0Var.k0;
                    CoroutineContext context = q70Var2.getContext();
                    Object c2 = ThreadContextKt.c(context, obj2);
                    qe4<?> e = c2 != ThreadContextKt.a ? x80.e(q70Var2, context, c2) : null;
                    np0Var.i0.resumeWith(obj);
                    te4 te4Var = te4.a;
                    if (e == null || e.M0()) {
                        ThreadContextKt.a(context, c2);
                    }
                }
                do {
                } while (b2.b0());
            } finally {
                try {
                    return;
                } finally {
                }
            }
            return;
        }
        q70Var.resumeWith(obj);
    }

    public static /* synthetic */ void c(q70 q70Var, Object obj, tc1 tc1Var, int i, Object obj2) {
        if ((i & 2) != 0) {
            tc1Var = null;
        }
        b(q70Var, obj, tc1Var);
    }

    public static final boolean d(np0<? super te4> np0Var) {
        te4 te4Var = te4.a;
        ze0.a();
        xx0 b2 = j54.a.b();
        if (b2.X()) {
            return false;
        }
        if (b2.W()) {
            np0Var.j0 = te4Var;
            np0Var.g0 = 1;
            b2.N(np0Var);
            return true;
        }
        b2.R(true);
        try {
            np0Var.run();
            do {
            } while (b2.b0());
        } finally {
            try {
                return false;
            } finally {
            }
        }
        return false;
    }
}
