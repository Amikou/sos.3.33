package defpackage;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.o;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: oe5  reason: default package */
/* loaded from: classes.dex */
public final class oe5 extends o {
    public final em5 a;

    public oe5(em5 em5Var) {
        this.a = em5Var;
    }

    @Override // com.google.android.gms.internal.measurement.p
    public final int c() {
        return System.identityHashCode(this.a);
    }

    @Override // com.google.android.gms.internal.measurement.p
    public final void y(String str, String str2, Bundle bundle, long j) {
        this.a.a(str, str2, bundle, j);
    }
}
