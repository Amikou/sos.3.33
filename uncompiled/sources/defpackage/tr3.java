package defpackage;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import java.util.List;

/* renamed from: tr3  reason: default package */
/* loaded from: classes2.dex */
public abstract class tr3 {
    public static tr3 e(Bundle bundle) {
        return new bt4(bundle.getInt("session_id"), bundle.getInt("status"), bundle.getInt("error_code"), bundle.getLong("bytes_downloaded"), bundle.getLong("total_bytes_to_download"), bundle.getStringArrayList("module_names"), bundle.getStringArrayList("languages"), (PendingIntent) bundle.getParcelable("user_confirmation_intent"), bundle.getParcelableArrayList("split_file_intents"));
    }

    public abstract List<String> a();

    public abstract List<String> b();

    public abstract long c();

    public abstract List<Intent> d();

    public abstract int f();

    @Deprecated
    public abstract PendingIntent g();

    public abstract int h();

    public abstract int i();

    public abstract long j();
}
