package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;
import java.io.File;
import java.io.FileNotFoundException;

/* compiled from: MediaStoreFileLoader.java */
/* renamed from: b72  reason: default package */
/* loaded from: classes.dex */
public final class b72 implements j92<Uri, File> {
    public final Context a;

    /* compiled from: MediaStoreFileLoader.java */
    /* renamed from: b72$a */
    /* loaded from: classes.dex */
    public static final class a implements k92<Uri, File> {
        public final Context a;

        public a(Context context) {
            this.a = context;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Uri, File> c(qa2 qa2Var) {
            return new b72(this.a);
        }
    }

    /* compiled from: MediaStoreFileLoader.java */
    /* renamed from: b72$b */
    /* loaded from: classes.dex */
    public static class b implements d<File> {
        public static final String[] g0 = {"_data"};
        public final Context a;
        public final Uri f0;

        public b(Context context, Uri uri) {
            this.a = context;
            this.f0 = uri;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<File> a() {
            return File.class;
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super File> aVar) {
            Cursor query = this.a.getContentResolver().query(this.f0, g0, null, null, null);
            if (query != null) {
                try {
                    r0 = query.moveToFirst() ? query.getString(query.getColumnIndexOrThrow("_data")) : null;
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(r0)) {
                aVar.c(new FileNotFoundException("Failed to find file path for: " + this.f0));
                return;
            }
            aVar.f(new File(r0));
        }
    }

    public b72(Context context) {
        this.a = context;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<File> b(Uri uri, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(uri), new b(this.a, uri));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(Uri uri) {
        return d72.b(uri);
    }
}
