package defpackage;

import java.security.MessageDigest;

/* renamed from: wl  reason: default package */
/* loaded from: classes2.dex */
public class wl extends MessageDigest {
    public qo0 a;

    public wl(qo0 qo0Var) {
        super(qo0Var.g());
        this.a = qo0Var;
    }

    @Override // java.security.MessageDigestSpi
    public byte[] engineDigest() {
        byte[] bArr = new byte[this.a.h()];
        this.a.a(bArr, 0);
        return bArr;
    }

    @Override // java.security.MessageDigestSpi
    public void engineReset() {
        this.a.reset();
    }

    @Override // java.security.MessageDigestSpi
    public void engineUpdate(byte b) {
        this.a.c(b);
    }

    @Override // java.security.MessageDigestSpi
    public void engineUpdate(byte[] bArr, int i, int i2) {
        this.a.b(bArr, i, i2);
    }
}
