package defpackage;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.List;

/* compiled from: SupportSQLiteOpenHelper.java */
/* renamed from: tw3  reason: default package */
/* loaded from: classes.dex */
public interface tw3 extends Closeable {

    /* compiled from: SupportSQLiteOpenHelper.java */
    /* renamed from: tw3$a */
    /* loaded from: classes.dex */
    public static abstract class a {
        public final int a;

        public a(int i) {
            this.a = i;
        }

        public final void a(String str) {
            if (str.equalsIgnoreCase(":memory:") || str.trim().length() == 0) {
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("deleting the database file: ");
            sb.append(str);
            try {
                if (Build.VERSION.SDK_INT >= 16) {
                    nw3.c(new File(str));
                } else if (!new File(str).delete()) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not delete the database file ");
                    sb2.append(str);
                }
            } catch (Exception unused) {
            }
        }

        public void b(sw3 sw3Var) {
        }

        public void c(sw3 sw3Var) {
            StringBuilder sb = new StringBuilder();
            sb.append("Corruption reported by sqlite on database: ");
            sb.append(sw3Var.getPath());
            if (!sw3Var.isOpen()) {
                a(sw3Var.getPath());
                return;
            }
            List<Pair<String, String>> list = null;
            try {
                try {
                    list = sw3Var.G();
                } finally {
                    if (list != null) {
                        for (Pair<String, String> next : list) {
                            a((String) next.second);
                        }
                    } else {
                        a(sw3Var.getPath());
                    }
                }
            } catch (SQLiteException unused) {
            }
            try {
                sw3Var.close();
            } catch (IOException unused2) {
            }
        }

        public abstract void d(sw3 sw3Var);

        public abstract void e(sw3 sw3Var, int i, int i2);

        public void f(sw3 sw3Var) {
        }

        public abstract void g(sw3 sw3Var, int i, int i2);
    }

    /* compiled from: SupportSQLiteOpenHelper.java */
    /* renamed from: tw3$b */
    /* loaded from: classes.dex */
    public static class b {
        public final Context a;
        public final String b;
        public final a c;
        public final boolean d;

        /* compiled from: SupportSQLiteOpenHelper.java */
        /* renamed from: tw3$b$a */
        /* loaded from: classes.dex */
        public static class a {
            public Context a;
            public String b;
            public a c;
            public boolean d;

            public a(Context context) {
                this.a = context;
            }

            public b a() {
                if (this.c != null) {
                    if (this.a != null) {
                        if (this.d && TextUtils.isEmpty(this.b)) {
                            throw new IllegalArgumentException("Must set a non-null database name to a configuration that uses the no backup directory.");
                        }
                        return new b(this.a, this.b, this.c, this.d);
                    }
                    throw new IllegalArgumentException("Must set a non-null context to create the configuration.");
                }
                throw new IllegalArgumentException("Must set a callback to create the configuration.");
            }

            public a b(a aVar) {
                this.c = aVar;
                return this;
            }

            public a c(String str) {
                this.b = str;
                return this;
            }

            public a d(boolean z) {
                this.d = z;
                return this;
            }
        }

        public b(Context context, String str, a aVar, boolean z) {
            this.a = context;
            this.b = str;
            this.c = aVar;
            this.d = z;
        }

        public static a a(Context context) {
            return new a(context);
        }
    }

    /* compiled from: SupportSQLiteOpenHelper.java */
    /* renamed from: tw3$c */
    /* loaded from: classes.dex */
    public interface c {
        tw3 a(b bVar);
    }

    sw3 D0();

    @Override // java.io.Closeable, java.lang.AutoCloseable
    void close();

    String getDatabaseName();

    void setWriteAheadLoggingEnabled(boolean z);
}
