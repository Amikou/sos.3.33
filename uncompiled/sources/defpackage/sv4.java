package defpackage;

import java.util.Objects;

/* renamed from: sv4  reason: default package */
/* loaded from: classes2.dex */
public final class sv4 extends mv4 {
    public final qv4 a = new qv4();

    @Override // defpackage.mv4
    public final void a(Throwable th, Throwable th2) {
        if (th2 == th) {
            throw new IllegalArgumentException("Self suppression is not allowed.", th2);
        }
        Objects.requireNonNull(th2, "The suppressed exception cannot be null.");
        this.a.a(th).add(th2);
    }
}
