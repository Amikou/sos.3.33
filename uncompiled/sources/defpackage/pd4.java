package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.b;
import com.fasterxml.jackson.databind.type.TypeFactory;

/* compiled from: TypeIdResolverBase.java */
/* renamed from: pd4  reason: default package */
/* loaded from: classes.dex */
public abstract class pd4 implements b {
    public final TypeFactory a;
    public final JavaType b;

    public pd4(JavaType javaType, TypeFactory typeFactory) {
        this.b = javaType;
        this.a = typeFactory;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public void c(JavaType javaType) {
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String f() {
        return e(null, this.b.getRawClass());
    }
}
