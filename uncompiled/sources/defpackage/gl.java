package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_User.java */
/* renamed from: gl  reason: default package */
/* loaded from: classes2.dex */
public final class gl extends r90.e.f {
    public final String a;

    /* compiled from: AutoValue_CrashlyticsReport_Session_User.java */
    /* renamed from: gl$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.f.a {
        public String a;

        @Override // defpackage.r90.e.f.a
        public r90.e.f a() {
            String str = "";
            if (this.a == null) {
                str = " identifier";
            }
            if (str.isEmpty()) {
                return new gl(this.a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.f.a
        public r90.e.f.a b(String str) {
            Objects.requireNonNull(str, "Null identifier");
            this.a = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.f
    public String b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.f) {
            return this.a.equals(((r90.e.f) obj).b());
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    public String toString() {
        return "User{identifier=" + this.a + "}";
    }

    public gl(String str) {
        this.a = str;
    }
}
