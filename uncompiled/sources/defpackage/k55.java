package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: k55  reason: default package */
/* loaded from: classes.dex */
public final class k55 implements Runnable {
    public final /* synthetic */ tl5 a;
    public final /* synthetic */ n55 f0;

    public k55(n55 n55Var, tl5 tl5Var) {
        this.f0 = n55Var;
        this.a = tl5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.a.b();
        if (c66.a()) {
            this.a.q().p(this);
            return;
        }
        boolean c = this.f0.c();
        this.f0.c = 0L;
        if (c) {
            this.f0.a();
        }
    }
}
