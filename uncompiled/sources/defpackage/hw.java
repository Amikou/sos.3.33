package defpackage;

import android.view.View;
import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: CarouselOffset.java */
/* renamed from: hw  reason: default package */
/* loaded from: classes2.dex */
public class hw {

    /* compiled from: CarouselOffset.java */
    /* renamed from: hw$a */
    /* loaded from: classes2.dex */
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public final /* synthetic */ RecyclerView a;
        public final /* synthetic */ boolean f0;
        public final /* synthetic */ View g0;
        public final /* synthetic */ int h0;

        public a(hw hwVar, RecyclerView recyclerView, boolean z, View view, int i) {
            this.a = recyclerView;
            this.f0 = z;
            this.g0 = view;
            this.h0 = i;
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (this.a.getItemDecorationCount() > 0) {
                this.a.c1(0);
            }
            if (this.f0) {
                this.a.i(new gw(this.g0.getWidth(), this.h0), 0);
            } else {
                this.a.i(new gw(0, this.h0), 0);
            }
            this.g0.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    public void a(RecyclerView recyclerView, View view, int i, boolean z) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new a(this, recyclerView, z, view, i));
    }
}
