package defpackage;

import android.text.TextUtils;
import com.google.firebase.crashlytics.internal.common.d;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import zendesk.core.Constants;

/* compiled from: DefaultSettingsSpiCall.java */
/* renamed from: vk0  reason: default package */
/* loaded from: classes2.dex */
public class vk0 implements mn3 {
    public final String a;
    public final jl1 b;
    public final w12 c;

    public vk0(String str, jl1 jl1Var) {
        this(str, jl1Var, w12.f());
    }

    @Override // defpackage.mn3
    public JSONObject a(ln3 ln3Var, boolean z) {
        if (z) {
            try {
                Map<String, String> f = f(ln3Var);
                hl1 b = b(d(f), ln3Var);
                w12 w12Var = this.c;
                w12Var.b("Requesting settings from " + this.a);
                w12 w12Var2 = this.c;
                w12Var2.i("Settings query params were: " + f);
                return g(b.c());
            } catch (IOException e) {
                this.c.e("Settings request failed.", e);
                return null;
            }
        }
        throw new RuntimeException("An invalid data collection token was used.");
    }

    public final hl1 b(hl1 hl1Var, ln3 ln3Var) {
        c(hl1Var, "X-CRASHLYTICS-GOOGLE-APP-ID", ln3Var.a);
        c(hl1Var, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        c(hl1Var, "X-CRASHLYTICS-API-CLIENT-VERSION", d.i());
        c(hl1Var, Constants.ACCEPT_HEADER, Constants.APPLICATION_JSON);
        c(hl1Var, "X-CRASHLYTICS-DEVICE-MODEL", ln3Var.b);
        c(hl1Var, "X-CRASHLYTICS-OS-BUILD-VERSION", ln3Var.c);
        c(hl1Var, "X-CRASHLYTICS-OS-DISPLAY-VERSION", ln3Var.d);
        c(hl1Var, "X-CRASHLYTICS-INSTALLATION-ID", ln3Var.e.a());
        return hl1Var;
    }

    public final void c(hl1 hl1Var, String str, String str2) {
        if (str2 != null) {
            hl1Var.d(str, str2);
        }
    }

    public hl1 d(Map<String, String> map) {
        hl1 a = this.b.a(this.a, map);
        return a.d(Constants.USER_AGENT_HEADER_KEY, "Crashlytics Android SDK/" + d.i()).d("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
    }

    public final JSONObject e(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            w12 w12Var = this.c;
            w12Var.l("Failed to parse settings JSON from " + this.a, e);
            w12 w12Var2 = this.c;
            w12Var2.k("Settings response " + str);
            return null;
        }
    }

    public final Map<String, String> f(ln3 ln3Var) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", ln3Var.h);
        hashMap.put("display_version", ln3Var.g);
        hashMap.put("source", Integer.toString(ln3Var.i));
        String str = ln3Var.f;
        if (!TextUtils.isEmpty(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    public JSONObject g(kl1 kl1Var) {
        int b = kl1Var.b();
        w12 w12Var = this.c;
        w12Var.i("Settings response code was: " + b);
        if (h(b)) {
            return e(kl1Var.a());
        }
        w12 w12Var2 = this.c;
        w12Var2.d("Settings request failed; (status: " + b + ") from " + this.a);
        return null;
    }

    public boolean h(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }

    public vk0(String str, jl1 jl1Var, w12 w12Var) {
        if (str != null) {
            this.c = w12Var;
            this.b = jl1Var;
            this.a = str;
            return;
        }
        throw new IllegalArgumentException("url must not be null.");
    }
}
