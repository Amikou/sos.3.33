package defpackage;

import android.os.RemoteException;
import com.google.android.gms.common.internal.n;
import com.google.android.gms.common.internal.p;
import com.google.zxing.qrcode.encoder.Encoder;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: vc5  reason: default package */
/* loaded from: classes.dex */
public abstract class vc5 extends p {
    public int a;

    public vc5(byte[] bArr) {
        zt2.a(bArr.length == 25);
        this.a = Arrays.hashCode(bArr);
    }

    public static byte[] H1(String str) {
        try {
            return str.getBytes(Encoder.DEFAULT_BYTE_MODE_ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public abstract byte[] G1();

    public boolean equals(Object obj) {
        lm1 zzb;
        if (obj != null && (obj instanceof n)) {
            try {
                n nVar = (n) obj;
                if (nVar.zzc() == hashCode() && (zzb = nVar.zzb()) != null) {
                    return Arrays.equals(G1(), (byte[]) nl2.G1(zzb));
                }
                return false;
            } catch (RemoteException unused) {
            }
        }
        return false;
    }

    public int hashCode() {
        return this.a;
    }

    @Override // com.google.android.gms.common.internal.n
    public final lm1 zzb() {
        return nl2.H1(G1());
    }

    @Override // com.google.android.gms.common.internal.n
    public final int zzc() {
        return hashCode();
    }
}
