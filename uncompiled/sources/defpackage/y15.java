package defpackage;

import android.content.Context;
import android.util.SparseIntArray;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: y15  reason: default package */
/* loaded from: classes.dex */
public final class y15 {
    public final SparseIntArray a = new SparseIntArray();
    public eh1 b;

    public y15(eh1 eh1Var) {
        zt2.j(eh1Var);
        this.b = eh1Var;
    }

    public final int a(Context context, a.f fVar) {
        zt2.j(context);
        zt2.j(fVar);
        int i = 0;
        if (fVar.p()) {
            int q = fVar.q();
            int i2 = this.a.get(q, -1);
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            while (true) {
                if (i3 >= this.a.size()) {
                    i = i2;
                    break;
                }
                int keyAt = this.a.keyAt(i3);
                if (keyAt > q && this.a.get(keyAt) == 0) {
                    break;
                }
                i3++;
            }
            if (i == -1) {
                i = this.b.j(context, q);
            }
            this.a.put(q, i);
            return i;
        }
        return 0;
    }

    public final void b() {
        this.a.clear();
    }
}
