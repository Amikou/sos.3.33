package defpackage;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.internal.zar;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: w25  reason: default package */
/* loaded from: classes.dex */
public final class w25 implements Parcelable.Creator<zar> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zar createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        Account account = null;
        int i = 0;
        int i2 = 0;
        GoogleSignInAccount googleSignInAccount = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 1) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 2) {
                account = (Account) SafeParcelReader.o(parcel, C, Account.CREATOR);
            } else if (v == 3) {
                i2 = SafeParcelReader.E(parcel, C);
            } else if (v != 4) {
                SafeParcelReader.I(parcel, C);
            } else {
                googleSignInAccount = (GoogleSignInAccount) SafeParcelReader.o(parcel, C, GoogleSignInAccount.CREATOR);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zar(i, account, i2, googleSignInAccount);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zar[] newArray(int i) {
        return new zar[i];
    }
}
