package defpackage;

import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: Executors.kt */
/* renamed from: t83  reason: default package */
/* loaded from: classes2.dex */
public final class t83 implements Runnable {
    public final CoroutineDispatcher a;
    public final ov<te4> f0;

    /* JADX WARN: Multi-variable type inference failed */
    public t83(CoroutineDispatcher coroutineDispatcher, ov<? super te4> ovVar) {
        this.a = coroutineDispatcher;
        this.f0 = ovVar;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.f0.k(this.a, te4.a);
    }
}
