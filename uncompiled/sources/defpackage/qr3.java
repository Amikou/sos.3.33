package defpackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java8.util.i;
import java8.util.s;
import java8.util.t;

/* compiled from: SpinedBuffer.java */
/* renamed from: qr3  reason: default package */
/* loaded from: classes2.dex */
public class qr3<E> extends l5 implements m60<E> {
    public E[] e = (E[]) new Object[1 << this.a];
    public E[][] f;

    /* compiled from: SpinedBuffer.java */
    /* renamed from: qr3$a */
    /* loaded from: classes2.dex */
    public class a implements s<E> {
        public int a;
        public final int b;
        public int c;
        public final int d;
        public E[] e;

        public a(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            E[][] eArr = qr3.this.f;
            this.e = eArr == null ? qr3.this.e : eArr[i];
        }

        @Override // java8.util.s
        public void a(m60<? super E> m60Var) {
            int i;
            rl2.f(m60Var);
            int i2 = this.a;
            int i3 = this.b;
            if (i2 < i3 || (i2 == i3 && this.c < this.d)) {
                int i4 = this.c;
                while (true) {
                    i = this.b;
                    if (i2 >= i) {
                        break;
                    }
                    E[] eArr = qr3.this.f[i2];
                    while (i4 < eArr.length) {
                        m60Var.accept((Object) eArr[i4]);
                        i4++;
                    }
                    i4 = 0;
                    i2++;
                }
                E[] eArr2 = this.a == i ? this.e : qr3.this.f[i];
                int i5 = this.d;
                while (i4 < i5) {
                    m60Var.accept((Object) eArr2[i4]);
                    i4++;
                }
                this.a = this.b;
                this.c = this.d;
            }
        }

        @Override // java8.util.s
        public int b() {
            return 16464;
        }

        @Override // java8.util.s
        public s<E> c() {
            int i = this.a;
            int i2 = this.b;
            if (i < i2) {
                qr3 qr3Var = qr3.this;
                a aVar = new a(i, i2 - 1, this.c, qr3Var.f[i2 - 1].length);
                int i3 = this.b;
                this.a = i3;
                this.c = 0;
                this.e = qr3.this.f[i3];
                return aVar;
            } else if (i == i2) {
                int i4 = this.d;
                int i5 = this.c;
                int i6 = (i4 - i5) / 2;
                if (i6 == 0) {
                    return null;
                }
                s<E> a = i.a(this.e, i5, i5 + i6);
                this.c += i6;
                return a;
            } else {
                return null;
            }
        }

        @Override // java8.util.s
        public boolean d(m60<? super E> m60Var) {
            rl2.f(m60Var);
            int i = this.a;
            int i2 = this.b;
            if (i < i2 || (i == i2 && this.c < this.d)) {
                int i3 = this.c;
                this.c = i3 + 1;
                m60Var.accept((Object) this.e[i3]);
                if (this.c == this.e.length) {
                    this.c = 0;
                    int i4 = this.a + 1;
                    this.a = i4;
                    E[][] eArr = qr3.this.f;
                    if (eArr != null && i4 <= this.b) {
                        this.e = eArr[i4];
                    }
                }
                return true;
            }
            return false;
        }

        @Override // java8.util.s
        public Comparator<? super E> f() {
            return t.h(this);
        }

        @Override // java8.util.s
        public boolean h(int i) {
            return t.k(this, i);
        }

        @Override // java8.util.s
        public long i() {
            return t.i(this);
        }

        @Override // java8.util.s
        public long l() {
            int i = this.a;
            int i2 = this.b;
            if (i == i2) {
                return this.d - this.c;
            }
            long[] jArr = qr3.this.d;
            return ((jArr[i2] + this.d) - jArr[i]) - this.c;
        }
    }

    public void accept(E e) {
        if (this.b == this.e.length) {
            l();
            int i = this.c;
            int i2 = i + 1;
            E[][] eArr = this.f;
            if (i2 >= eArr.length || eArr[i + 1] == null) {
                k();
            }
            this.b = 0;
            int i3 = this.c + 1;
            this.c = i3;
            this.e = this.f[i3];
        }
        E[] eArr2 = this.e;
        int i4 = this.b;
        this.b = i4 + 1;
        eArr2[i4] = e;
    }

    public void e(m60<? super E> m60Var) {
        for (int i = 0; i < this.c; i++) {
            for (E e : this.f[i]) {
                m60Var.accept((Object) e);
            }
        }
        for (int i2 = 0; i2 < this.b; i2++) {
            m60Var.accept((Object) this.e[i2]);
        }
    }

    public long h() {
        int i = this.c;
        if (i == 0) {
            return this.e.length;
        }
        return this.f[i].length + this.d[i];
    }

    public void i() {
        E[][] eArr = this.f;
        if (eArr != null) {
            this.e = eArr[0];
            int i = 0;
            while (true) {
                E[] eArr2 = this.e;
                if (i >= eArr2.length) {
                    break;
                }
                eArr2[i] = null;
                i++;
            }
            this.f = null;
            this.d = null;
        } else {
            for (int i2 = 0; i2 < this.b; i2++) {
                this.e[i2] = null;
            }
        }
        this.b = 0;
        this.c = 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void j(long j) {
        Object[][] objArr;
        int i;
        long h = h();
        if (j <= h) {
            return;
        }
        l();
        int i2 = this.c;
        while (true) {
            i2++;
            if (j <= h) {
                return;
            }
            E[][] eArr = this.f;
            if (i2 >= eArr.length) {
                int length = eArr.length * 2;
                this.f = (E[][]) ((Object[][]) Arrays.copyOf(eArr, length));
                this.d = Arrays.copyOf(this.d, length);
            }
            int f = f(i2);
            ((E[][]) this.f)[i2] = new Object[f];
            long[] jArr = this.d;
            jArr[i2] = jArr[i2 - 1] + objArr[i].length;
            h += f;
        }
    }

    public void k() {
        j(h() + 1);
    }

    public final void l() {
        if (this.f == null) {
            E[][] eArr = (E[][]) new Object[8];
            this.f = eArr;
            this.d = new long[8];
            eArr[0] = this.e;
        }
    }

    public s<E> spliterator() {
        return new a(0, this.c, 0, this.b);
    }

    public String toString() {
        ArrayList arrayList = new ArrayList();
        e(pr3.a(arrayList));
        return "SpinedBuffer:" + arrayList.toString();
    }
}
