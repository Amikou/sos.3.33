package defpackage;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java8.util.concurrent.CompletableFuture;

/* compiled from: Async.java */
/* renamed from: ti  reason: default package */
/* loaded from: classes3.dex */
public class ti {
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    static {
        Runtime.getRuntime().addShutdownHook(new Thread(si.a));
    }

    public static ScheduledExecutorService defaultExecutorService() {
        final ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(getCpuCount());
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() { // from class: qi
            @Override // java.lang.Runnable
            public final void run() {
                ti.shutdown(newScheduledThreadPool);
            }
        }));
        return newScheduledThreadPool;
    }

    private static int getCpuCount() {
        return Runtime.getRuntime().availableProcessors();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ void lambda$run$1(CompletableFuture completableFuture, Callable callable) {
        try {
            completableFuture.f(callable.call());
        } catch (Throwable th) {
            completableFuture.g(th);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ void lambda$static$0() {
        shutdown(executor);
    }

    public static <T> CompletableFuture<T> run(final Callable<T> callable) {
        final CompletableFuture<T> completableFuture = new CompletableFuture<>();
        CompletableFuture.r(new Runnable() { // from class: ri
            @Override // java.lang.Runnable
            public final void run() {
                ti.lambda$run$1(CompletableFuture.this, callable);
            }
        }, executor);
        return completableFuture;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void shutdown(ExecutorService executorService) {
        executorService.shutdown();
        try {
            TimeUnit timeUnit = TimeUnit.SECONDS;
            if (executorService.awaitTermination(60L, timeUnit)) {
                return;
            }
            executorService.shutdownNow();
            if (executorService.awaitTermination(60L, timeUnit)) {
                return;
            }
            System.err.println("Thread pool did not terminate");
        } catch (InterruptedException unused) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
