package defpackage;

import java.util.concurrent.ThreadFactory;

/* renamed from: ou4  reason: default package */
/* loaded from: classes2.dex */
public final class ou4 implements ThreadFactory {
    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, "SplitCompatBackgroundThread");
    }
}
