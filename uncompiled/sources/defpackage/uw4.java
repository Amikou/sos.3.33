package defpackage;

import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.c;
import java.io.File;
import java.util.concurrent.Executor;

/* renamed from: uw4  reason: default package */
/* loaded from: classes2.dex */
public final class uw4 {
    public final c a;
    public final cw4<zy4> b;
    public final zv4 c;
    public final cw4<Executor> d;
    public final fv4 e;
    public final ws4 f;

    public uw4(c cVar, cw4<zy4> cw4Var, zv4 zv4Var, cw4<Executor> cw4Var2, fv4 fv4Var, ws4 ws4Var) {
        this.a = cVar;
        this.b = cw4Var;
        this.c = zv4Var;
        this.d = cw4Var2;
        this.e = fv4Var;
        this.f = ws4Var;
    }

    public final void a(final rw4 rw4Var) {
        Executor a;
        Runnable a2;
        File x = this.a.x(rw4Var.b, rw4Var.c, rw4Var.d);
        File D = this.a.D(rw4Var.b, rw4Var.c, rw4Var.d);
        if (!x.exists() || !D.exists()) {
            throw new bj(String.format("Cannot find pack files to move for pack %s.", rw4Var.b), rw4Var.a);
        }
        File t = this.a.t(rw4Var.b, rw4Var.c, rw4Var.d);
        t.mkdirs();
        if (!x.renameTo(t)) {
            throw new bj("Cannot move merged pack files to final location.", rw4Var.a);
        }
        new File(this.a.t(rw4Var.b, rw4Var.c, rw4Var.d), "merge.tmp").delete();
        File u = this.a.u(rw4Var.b, rw4Var.c, rw4Var.d);
        u.mkdirs();
        if (!D.renameTo(u)) {
            throw new bj("Cannot move metadata files to final location.", rw4Var.a);
        }
        if (this.f.a()) {
            a = this.d.a();
            a2 = new Runnable(this, rw4Var) { // from class: sw4
                public final uw4 a;
                public final rw4 f0;

                {
                    this.a = this;
                    this.f0 = rw4Var;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.a.b(this.f0);
                }
            };
        } else {
            a = this.d.a();
            c cVar = this.a;
            cVar.getClass();
            a2 = tw4.a(cVar);
        }
        a.execute(a2);
        this.c.f(rw4Var.b, rw4Var.c, rw4Var.d);
        this.e.a(rw4Var.b);
        this.b.a().d(rw4Var.a, rw4Var.b);
    }

    public final /* synthetic */ void b(rw4 rw4Var) {
        this.a.E(rw4Var.b, rw4Var.c, rw4Var.d);
    }
}
