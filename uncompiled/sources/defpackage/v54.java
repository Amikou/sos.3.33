package defpackage;

import android.content.ContentResolver;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.a;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* compiled from: ThumbnailStreamOpener.java */
/* renamed from: v54  reason: default package */
/* loaded from: classes.dex */
public class v54 {
    public static final r31 f = new r31();
    public final r31 a;
    public final t54 b;
    public final sh c;
    public final ContentResolver d;
    public final List<ImageHeaderParser> e;

    public v54(List<ImageHeaderParser> list, t54 t54Var, sh shVar, ContentResolver contentResolver) {
        this(list, f, t54Var, shVar, contentResolver);
    }

    public int a(Uri uri) {
        InputStream inputStream = null;
        try {
            try {
                inputStream = this.d.openInputStream(uri);
                int b = a.b(this.e, inputStream, this.c);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                    }
                }
                return b;
            } catch (IOException | NullPointerException unused2) {
                if (Log.isLoggable("ThumbStreamOpener", 3)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to open uri: ");
                    sb.append(uri);
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                        return -1;
                    } catch (IOException unused3) {
                        return -1;
                    }
                }
                return -1;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused4) {
                }
            }
            throw th;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0041  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.String b(android.net.Uri r5) {
        /*
            r4 = this;
            r0 = 0
            t54 r1 = r4.b     // Catch: java.lang.Throwable -> L1e java.lang.SecurityException -> L20
            android.database.Cursor r1 = r1.a(r5)     // Catch: java.lang.Throwable -> L1e java.lang.SecurityException -> L20
            if (r1 == 0) goto L18
            boolean r2 = r1.moveToFirst()     // Catch: java.lang.SecurityException -> L21 java.lang.Throwable -> L3d
            if (r2 == 0) goto L18
            r2 = 0
            java.lang.String r5 = r1.getString(r2)     // Catch: java.lang.SecurityException -> L21 java.lang.Throwable -> L3d
            r1.close()
            return r5
        L18:
            if (r1 == 0) goto L1d
            r1.close()
        L1d:
            return r0
        L1e:
            r5 = move-exception
            goto L3f
        L20:
            r1 = r0
        L21:
            java.lang.String r2 = "ThumbStreamOpener"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch: java.lang.Throwable -> L3d
            if (r2 == 0) goto L37
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L3d
            r2.<init>()     // Catch: java.lang.Throwable -> L3d
            java.lang.String r3 = "Failed to query for thumbnail for Uri: "
            r2.append(r3)     // Catch: java.lang.Throwable -> L3d
            r2.append(r5)     // Catch: java.lang.Throwable -> L3d
        L37:
            if (r1 == 0) goto L3c
            r1.close()
        L3c:
            return r0
        L3d:
            r5 = move-exception
            r0 = r1
        L3f:
            if (r0 == 0) goto L44
            r0.close()
        L44:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.v54.b(android.net.Uri):java.lang.String");
    }

    public final boolean c(File file) {
        return this.a.a(file) && 0 < this.a.c(file);
    }

    public InputStream d(Uri uri) throws FileNotFoundException {
        String b = b(uri);
        if (TextUtils.isEmpty(b)) {
            return null;
        }
        File b2 = this.a.b(b);
        if (c(b2)) {
            Uri fromFile = Uri.fromFile(b2);
            try {
                return this.d.openInputStream(fromFile);
            } catch (NullPointerException e) {
                throw ((FileNotFoundException) new FileNotFoundException("NPE opening uri: " + uri + " -> " + fromFile).initCause(e));
            }
        }
        return null;
    }

    public v54(List<ImageHeaderParser> list, r31 r31Var, t54 t54Var, sh shVar, ContentResolver contentResolver) {
        this.a = r31Var;
        this.b = t54Var;
        this.c = shVar;
        this.d = contentResolver;
        this.e = list;
    }
}
