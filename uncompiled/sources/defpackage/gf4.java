package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.LocaleList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;
import kotlin.TypeCastException;

/* compiled from: UpdateLocaleDelegate.kt */
/* renamed from: gf4  reason: default package */
/* loaded from: classes2.dex */
public final class gf4 {
    public final void a(Context context, Locale locale) {
        fs1.g(context, "context");
        fs1.g(locale, "locale");
        c(context, locale);
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != context) {
            fs1.c(applicationContext, "appContext");
            c(applicationContext, locale);
        }
    }

    @SuppressLint({"NewApi"})
    public final void b(Configuration configuration, Locale locale) {
        LinkedHashSet c = tm3.c(locale);
        LocaleList localeList = LocaleList.getDefault();
        fs1.c(localeList, "LocaleList.getDefault()");
        int size = localeList.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            Locale locale2 = localeList.get(i);
            fs1.c(locale2, "defaultLocales[it]");
            arrayList.add(locale2);
        }
        c.addAll(arrayList);
        Object[] array = c.toArray(new Locale[0]);
        if (array != null) {
            Locale[] localeArr = (Locale[]) array;
            configuration.setLocales(new LocaleList((Locale[]) Arrays.copyOf(localeArr, localeArr.length)));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public final void c(Context context, Locale locale) {
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        fs1.c(resources, "res");
        Configuration configuration = resources.getConfiguration();
        fs1.c(configuration, "res.configuration");
        if (fs1.b(m11.a(configuration), locale)) {
            return;
        }
        Configuration configuration2 = new Configuration(resources.getConfiguration());
        if (m11.b(24)) {
            b(configuration2, locale);
        } else if (m11.b(17)) {
            configuration2.setLocale(locale);
        } else {
            configuration2.locale = locale;
        }
        resources.updateConfiguration(configuration2, resources.getDisplayMetrics());
    }
}
