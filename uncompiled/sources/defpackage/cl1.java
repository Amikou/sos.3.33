package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.a;

/* compiled from: HoneycombBitmapFactory.java */
@TargetApi(11)
/* renamed from: cl1  reason: default package */
/* loaded from: classes.dex */
public class cl1 extends br2 {
    public static final String e = "cl1";
    public final ru0 a;
    public final dr2 b;
    public final a00 c;
    public boolean d;

    public cl1(ru0 ru0Var, dr2 dr2Var, a00 a00Var) {
        this.a = ru0Var;
        this.b = dr2Var;
        this.c = a00Var;
    }

    @Override // defpackage.br2
    @TargetApi(12)
    public a<Bitmap> c(int i, int i2, Bitmap.Config config) {
        if (this.d) {
            return d(i, i2, config);
        }
        a<PooledByteBuffer> a = this.a.a((short) i, (short) i2);
        try {
            zu0 zu0Var = new zu0(a);
            zu0Var.a0(wj0.a);
            a<Bitmap> a2 = this.b.a(zu0Var, config, null, a.j().size());
            if (!a2.j().isMutable()) {
                a.g(a2);
                this.d = true;
                v11.z(e, "Immutable bitmap returned by decoder");
                a<Bitmap> d = d(i, i2, config);
                zu0.c(zu0Var);
                return d;
            }
            a2.j().setHasAlpha(true);
            a2.j().eraseColor(0);
            zu0.c(zu0Var);
            return a2;
        } finally {
            a.close();
        }
    }

    public final a<Bitmap> d(int i, int i2, Bitmap.Config config) {
        return this.c.c(Bitmap.createBitmap(i, i2, config), yo3.b());
    }
}
