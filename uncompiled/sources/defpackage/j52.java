package defpackage;

/* renamed from: j52  reason: default package */
/* loaded from: classes2.dex */
public class j52 extends f52 {
    public int f0;
    public int g0;
    public he1 h0;

    public j52(int i, int i2, he1 he1Var, String str) {
        super(false, str);
        this.f0 = i;
        this.g0 = i2;
        this.h0 = new he1(he1Var);
    }

    public he1 b() {
        return this.h0;
    }

    public int c() {
        return this.h0.b();
    }

    public int d() {
        return this.f0;
    }

    public int e() {
        return this.g0;
    }
}
