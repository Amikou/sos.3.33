package defpackage;

import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.rd.animation.type.AnimationType;
import com.rd.draw.data.Orientation;

/* compiled from: CoordinatesUtils.java */
/* renamed from: r80  reason: default package */
/* loaded from: classes2.dex */
public class r80 {
    public static int a(mq1 mq1Var, int i) {
        if (mq1Var == null) {
            return 0;
        }
        if (mq1Var.g() == Orientation.HORIZONTAL) {
            return g(mq1Var, i);
        }
        return h(mq1Var, i);
    }

    public static int b(mq1 mq1Var, float f, float f2) {
        int c = mq1Var.c();
        int m = mq1Var.m();
        int s = mq1Var.s();
        int h = mq1Var.h();
        int d = mq1Var.g() == Orientation.HORIZONTAL ? mq1Var.d() : mq1Var.v();
        int i = 0;
        int i2 = 0;
        while (i < c) {
            int i3 = (m * 2) + (s / 2) + (i > 0 ? h : h / 2) + i2;
            boolean z = true;
            boolean z2 = f >= ((float) i2) && f <= ((float) i3);
            if (f2 < Utils.FLOAT_EPSILON || f2 > d) {
                z = false;
            }
            if (z2 && z) {
                return i;
            }
            i++;
            i2 = i3;
        }
        return -1;
    }

    public static int c(mq1 mq1Var, int i) {
        int c = mq1Var.c();
        int m = mq1Var.m();
        int s = mq1Var.s();
        int h = mq1Var.h();
        int i2 = 0;
        for (int i3 = 0; i3 < c; i3++) {
            int i4 = s / 2;
            int i5 = i2 + m + i4;
            if (i == i3) {
                return i5;
            }
            i2 = i5 + m + h + i4;
        }
        return mq1Var.b() == AnimationType.DROP ? i2 + (m * 2) : i2;
    }

    public static int d(mq1 mq1Var, float f, float f2) {
        if (mq1Var == null) {
            return -1;
        }
        if (mq1Var.g() != Orientation.HORIZONTAL) {
            f2 = f;
            f = f2;
        }
        return b(mq1Var, f, f2);
    }

    public static Pair<Integer, Float> e(mq1 mq1Var, int i, float f, boolean z) {
        int c = mq1Var.c();
        int q = mq1Var.q();
        if (z) {
            i = (c - 1) - i;
        }
        boolean z2 = false;
        if (i < 0) {
            i = 0;
        } else {
            int i2 = c - 1;
            if (i > i2) {
                i = i2;
            }
        }
        boolean z3 = i > q;
        boolean z4 = !z ? i + 1 >= q : i + (-1) >= q;
        if (z3 || z4) {
            mq1Var.V(i);
            q = i;
        }
        float f2 = Utils.FLOAT_EPSILON;
        if (q == i && f != Utils.FLOAT_EPSILON) {
            z2 = true;
        }
        if (z2) {
            i = z ? i - 1 : i + 1;
        } else {
            f = 1.0f - f;
        }
        if (f > 1.0f) {
            f2 = 1.0f;
        } else if (f >= Utils.FLOAT_EPSILON) {
            f2 = f;
        }
        return new Pair<>(Integer.valueOf(i), Float.valueOf(f2));
    }

    public static int f(mq1 mq1Var) {
        int m = mq1Var.m();
        return mq1Var.b() == AnimationType.DROP ? m * 3 : m;
    }

    public static int g(mq1 mq1Var, int i) {
        int f;
        if (mq1Var == null) {
            return 0;
        }
        if (mq1Var.g() == Orientation.HORIZONTAL) {
            f = c(mq1Var, i);
        } else {
            f = f(mq1Var);
        }
        return f + mq1Var.j();
    }

    public static int h(mq1 mq1Var, int i) {
        int c;
        if (mq1Var == null) {
            return 0;
        }
        if (mq1Var.g() == Orientation.HORIZONTAL) {
            c = f(mq1Var);
        } else {
            c = c(mq1Var, i);
        }
        return c + mq1Var.l();
    }
}
