package defpackage;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: u25  reason: default package */
/* loaded from: classes.dex */
public interface u25 extends GoogleApiClient.b {
    void b(ConnectionResult connectionResult, a<?> aVar, boolean z);
}
