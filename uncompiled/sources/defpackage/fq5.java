package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: fq5  reason: default package */
/* loaded from: classes.dex */
public final class fq5 implements Runnable {
    public final /* synthetic */ bq5 a;
    public final /* synthetic */ bq5 f0;
    public final /* synthetic */ long g0;
    public final /* synthetic */ boolean h0;
    public final /* synthetic */ qq5 i0;

    public fq5(qq5 qq5Var, bq5 bq5Var, bq5 bq5Var2, long j, boolean z) {
        this.i0 = qq5Var;
        this.a = bq5Var;
        this.f0 = bq5Var2;
        this.g0 = j;
        this.h0 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.i0.n(this.a, this.f0, this.g0, this.h0, null);
    }
}
