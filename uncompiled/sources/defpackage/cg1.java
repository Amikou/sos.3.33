package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: GifHeader.java */
/* renamed from: cg1  reason: default package */
/* loaded from: classes.dex */
public class cg1 {
    public zf1 d;
    public int f;
    public int g;
    public boolean h;
    public int i;
    public int j;
    public int k;
    public int l;
    public int m;
    public int[] a = null;
    public int b = 0;
    public int c = 0;
    public final List<zf1> e = new ArrayList();

    public int a() {
        return this.g;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.f;
    }
}
