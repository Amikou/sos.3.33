package defpackage;

import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: GetIsUserHasTokenUseCase.kt */
/* renamed from: kf1  reason: default package */
/* loaded from: classes2.dex */
public final class kf1 implements dm1 {
    public final bn1 a;

    public kf1(bn1 bn1Var) {
        fs1.f(bn1Var, "userRepository");
        this.a = bn1Var;
    }

    @Override // defpackage.dm1
    public Object a(IToken iToken, TokenType tokenType, q70<? super Boolean> q70Var) {
        return hr.a(this.a.g(iToken, tokenType));
    }
}
