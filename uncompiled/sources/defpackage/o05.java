package defpackage;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: o05  reason: default package */
/* loaded from: classes.dex */
public final class o05 extends a.AbstractC0106a<so3, s15> {
    @Override // com.google.android.gms.common.api.a.AbstractC0106a
    public final /* synthetic */ so3 d(Context context, Looper looper, kz kzVar, s15 s15Var, GoogleApiClient.b bVar, GoogleApiClient.c cVar) {
        return new so3(context, looper, false, kzVar, s15.a(), bVar, cVar);
    }
}
