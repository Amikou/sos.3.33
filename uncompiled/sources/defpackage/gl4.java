package defpackage;

/* renamed from: gl4  reason: default package */
/* loaded from: classes2.dex */
public class gl4 implements ut2 {
    public pt0[] a = null;
    public pt0[] b = null;
    public pt0 c = null;

    public pt0[] a() {
        return this.a;
    }

    public pt0[] b() {
        return this.b;
    }

    public pt0 c() {
        return this.c;
    }

    public void d(pt0[] pt0VarArr) {
        this.a = pt0VarArr;
    }

    public void e(pt0[] pt0VarArr) {
        this.b = pt0VarArr;
    }

    public void f(pt0 pt0Var) {
        this.c = pt0Var;
    }
}
