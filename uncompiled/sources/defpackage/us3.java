package defpackage;

import android.graphics.Matrix;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: State.java */
/* renamed from: us3  reason: default package */
/* loaded from: classes.dex */
public class us3 {
    public float c;
    public float d;
    public float f;
    public final Matrix a = new Matrix();
    public final float[] b = new float[9];
    public float e = 1.0f;

    public static int a(float f, float f2) {
        if (f > f2 + 0.001f) {
            return 1;
        }
        return f < f2 - 0.001f ? -1 : 0;
    }

    public static boolean c(float f, float f2) {
        return f >= f2 - 0.001f && f <= f2 + 0.001f;
    }

    public static float i(float f) {
        if (Float.isNaN(f)) {
            throw new IllegalArgumentException("Provided float is NaN");
        }
        return f;
    }

    public us3 b() {
        us3 us3Var = new us3();
        us3Var.m(this);
        return us3Var;
    }

    public void d(Matrix matrix) {
        matrix.set(this.a);
    }

    public float e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || us3.class != obj.getClass()) {
            return false;
        }
        us3 us3Var = (us3) obj;
        return c(us3Var.c, this.c) && c(us3Var.d, this.d) && c(us3Var.e, this.e) && c(us3Var.f, this.f);
    }

    public float f() {
        return this.c;
    }

    public float g() {
        return this.d;
    }

    public float h() {
        return this.e;
    }

    public int hashCode() {
        float f = this.c;
        int floatToIntBits = (f != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f) : 0) * 31;
        float f2 = this.d;
        int floatToIntBits2 = (floatToIntBits + (f2 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f2) : 0)) * 31;
        float f3 = this.e;
        int floatToIntBits3 = (floatToIntBits2 + (f3 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f3) : 0)) * 31;
        float f4 = this.f;
        return floatToIntBits3 + (f4 != Utils.FLOAT_EPSILON ? Float.floatToIntBits(f4) : 0);
    }

    public void j(float f, float f2, float f3) {
        this.a.postRotate(i(f), i(f2), i(f3));
        p(false, true);
    }

    public void k(float f, float f2, float f3) {
        this.a.postRotate((-this.f) + i(f), i(f2), i(f3));
        p(false, true);
    }

    public void l(float f, float f2, float f3, float f4) {
        while (f4 < -180.0f) {
            f4 += 360.0f;
        }
        while (f4 > 180.0f) {
            f4 -= 360.0f;
        }
        this.c = i(f);
        this.d = i(f2);
        this.e = i(f3);
        this.f = i(f4);
        this.a.reset();
        if (f3 != 1.0f) {
            this.a.postScale(f3, f3);
        }
        if (f4 != Utils.FLOAT_EPSILON) {
            this.a.postRotate(f4);
        }
        this.a.postTranslate(f, f2);
    }

    public void m(us3 us3Var) {
        this.c = us3Var.c;
        this.d = us3Var.d;
        this.e = us3Var.e;
        this.f = us3Var.f;
        this.a.set(us3Var.a);
    }

    public void n(float f, float f2) {
        this.a.postTranslate(i(f), i(f2));
        p(false, false);
    }

    public void o(float f, float f2) {
        this.a.postTranslate((-this.c) + i(f), (-this.d) + i(f2));
        p(false, false);
    }

    public final void p(boolean z, boolean z2) {
        this.a.getValues(this.b);
        this.c = i(this.b[2]);
        this.d = i(this.b[5]);
        if (z) {
            float[] fArr = this.b;
            this.e = i((float) Math.hypot(fArr[1], fArr[4]));
        }
        if (z2) {
            float[] fArr2 = this.b;
            this.f = i((float) Math.toDegrees(Math.atan2(fArr2[3], fArr2[4])));
        }
    }

    public void q(float f, float f2, float f3) {
        i(f);
        this.a.postScale(f, f, i(f2), i(f3));
        p(true, false);
    }

    public void r(float f, float f2, float f3) {
        i(f);
        Matrix matrix = this.a;
        float f4 = this.e;
        matrix.postScale(f / f4, f / f4, i(f2), i(f3));
        p(true, false);
    }

    public String toString() {
        return "{x=" + this.c + ",y=" + this.d + ",zoom=" + this.e + ",rotation=" + this.f + "}";
    }
}
