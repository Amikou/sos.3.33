package defpackage;

import com.facebook.imagepipeline.image.a;

/* compiled from: BitmapMemoryCacheGetProducer.java */
/* renamed from: dq  reason: default package */
/* loaded from: classes.dex */
public class dq extends gq {
    public dq(l72<wt, a> l72Var, xt xtVar, dv2<com.facebook.common.references.a<a>> dv2Var) {
        super(l72Var, xtVar, dv2Var);
    }

    @Override // defpackage.gq
    public String c() {
        return "pipe_ui";
    }

    @Override // defpackage.gq
    public String d() {
        return "BitmapMemoryCacheGetProducer";
    }

    @Override // defpackage.gq
    public l60<com.facebook.common.references.a<a>> f(l60<com.facebook.common.references.a<a>> l60Var, wt wtVar, boolean z) {
        return l60Var;
    }
}
