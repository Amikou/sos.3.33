package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.facebook.imagepipeline.animated.base.AnimatedDrawableFrameInfo;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: AnimatedDrawableBackendImpl.java */
/* renamed from: nd  reason: default package */
/* loaded from: classes.dex */
public class nd implements kd {
    public final pd a;
    public final yd b;
    public final td c;
    public final Rect d;
    public final int[] e;
    public final AnimatedDrawableFrameInfo[] f;
    public final Rect g = new Rect();
    public final Rect h = new Rect();
    public final boolean i;
    public Bitmap j;

    public nd(pd pdVar, yd ydVar, Rect rect, boolean z) {
        this.a = pdVar;
        this.b = ydVar;
        td d = ydVar.d();
        this.c = d;
        int[] i = d.i();
        this.e = i;
        pdVar.a(i);
        pdVar.c(i);
        pdVar.b(i);
        this.d = k(d, rect);
        this.i = z;
        this.f = new AnimatedDrawableFrameInfo[d.a()];
        for (int i2 = 0; i2 < this.c.a(); i2++) {
            this.f[i2] = this.c.c(i2);
        }
    }

    public static Rect k(td tdVar, Rect rect) {
        if (rect == null) {
            return new Rect(0, 0, tdVar.getWidth(), tdVar.getHeight());
        }
        return new Rect(0, 0, Math.min(rect.width(), tdVar.getWidth()), Math.min(rect.height(), tdVar.getHeight()));
    }

    @Override // defpackage.kd
    public int a() {
        return this.c.a();
    }

    @Override // defpackage.kd
    public int b() {
        return this.c.b();
    }

    @Override // defpackage.kd
    public AnimatedDrawableFrameInfo c(int i) {
        return this.f[i];
    }

    @Override // defpackage.kd
    public void d(int i, Canvas canvas) {
        xd g = this.c.g(i);
        try {
            if (g.getWidth() > 0 && g.getHeight() > 0) {
                if (this.c.h()) {
                    n(canvas, g);
                } else {
                    m(canvas, g);
                }
            }
        } finally {
            g.a();
        }
    }

    @Override // defpackage.kd
    public int e(int i) {
        return this.e[i];
    }

    @Override // defpackage.kd
    public kd f(Rect rect) {
        return k(this.c, rect).equals(this.d) ? this : new nd(this.a, this.b, rect, this.i);
    }

    @Override // defpackage.kd
    public int g() {
        return this.d.height();
    }

    @Override // defpackage.kd
    public int getHeight() {
        return this.c.getHeight();
    }

    @Override // defpackage.kd
    public int getWidth() {
        return this.c.getWidth();
    }

    @Override // defpackage.kd
    public int h() {
        return this.d.width();
    }

    @Override // defpackage.kd
    public yd i() {
        return this.b;
    }

    public final synchronized void j() {
        Bitmap bitmap = this.j;
        if (bitmap != null) {
            bitmap.recycle();
            this.j = null;
        }
    }

    public final synchronized Bitmap l(int i, int i2) {
        Bitmap bitmap = this.j;
        if (bitmap != null && (bitmap.getWidth() < i || this.j.getHeight() < i2)) {
            j();
        }
        if (this.j == null) {
            this.j = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        }
        this.j.eraseColor(0);
        return this.j;
    }

    public final void m(Canvas canvas, xd xdVar) {
        int width;
        int height;
        int c;
        int d;
        if (this.i) {
            float max = Math.max(xdVar.getWidth() / Math.min(xdVar.getWidth(), canvas.getWidth()), xdVar.getHeight() / Math.min(xdVar.getHeight(), canvas.getHeight()));
            width = (int) (xdVar.getWidth() / max);
            height = (int) (xdVar.getHeight() / max);
            c = (int) (xdVar.c() / max);
            d = (int) (xdVar.d() / max);
        } else {
            width = xdVar.getWidth();
            height = xdVar.getHeight();
            c = xdVar.c();
            d = xdVar.d();
        }
        synchronized (this) {
            Bitmap l = l(width, height);
            this.j = l;
            xdVar.b(width, height, l);
            canvas.save();
            canvas.translate(c, d);
            canvas.drawBitmap(this.j, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, (Paint) null);
            canvas.restore();
        }
    }

    public final void n(Canvas canvas, xd xdVar) {
        double width = this.d.width() / this.c.getWidth();
        double height = this.d.height() / this.c.getHeight();
        int round = (int) Math.round(xdVar.getWidth() * width);
        int round2 = (int) Math.round(xdVar.getHeight() * height);
        int c = (int) (xdVar.c() * width);
        int d = (int) (xdVar.d() * height);
        synchronized (this) {
            int width2 = this.d.width();
            int height2 = this.d.height();
            l(width2, height2);
            Bitmap bitmap = this.j;
            if (bitmap != null) {
                xdVar.b(round, round2, bitmap);
            }
            this.g.set(0, 0, width2, height2);
            this.h.set(c, d, width2 + c, height2 + d);
            Bitmap bitmap2 = this.j;
            if (bitmap2 != null) {
                canvas.drawBitmap(bitmap2, this.g, this.h, (Paint) null);
            }
        }
    }
}
