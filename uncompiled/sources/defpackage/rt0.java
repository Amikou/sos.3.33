package defpackage;

import java.math.BigInteger;
import java.util.Enumeration;
import org.bouncycastle.asn1.a0;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;
import org.bouncycastle.asn1.s0;

/* renamed from: rt0  reason: default package */
/* loaded from: classes2.dex */
public class rt0 extends h {
    public h4 a;

    public rt0(int i, BigInteger bigInteger, c4 c4Var) {
        this(i, bigInteger, null, c4Var);
    }

    public rt0(int i, BigInteger bigInteger, a0 a0Var, c4 c4Var) {
        byte[] a = ip.a((i + 7) / 8, bigInteger);
        d4 d4Var = new d4();
        d4Var.a(new g(1L));
        d4Var.a(new j0(a));
        if (c4Var != null) {
            d4Var.a(new s0(true, 0, c4Var));
        }
        if (a0Var != null) {
            d4Var.a(new s0(true, 1, a0Var));
        }
        this.a = new n0(d4Var);
    }

    public rt0(h4 h4Var) {
        this.a = h4Var;
    }

    public static rt0 o(Object obj) {
        if (obj instanceof rt0) {
            return (rt0) obj;
        }
        if (obj != null) {
            return new rt0(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        return this.a;
    }

    public BigInteger p() {
        return new BigInteger(1, ((f4) this.a.D(1)).D());
    }

    public final k q(int i) {
        Enumeration E = this.a.E();
        while (E.hasMoreElements()) {
            c4 c4Var = (c4) E.nextElement();
            if (c4Var instanceof k4) {
                k4 k4Var = (k4) c4Var;
                if (k4Var.D() == i) {
                    return k4Var.B().i();
                }
            }
        }
        return null;
    }

    public a0 s() {
        return (a0) q(1);
    }
}
