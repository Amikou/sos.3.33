package defpackage;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: Undispatched.kt */
/* renamed from: re4  reason: default package */
/* loaded from: classes2.dex */
public final class re4 {
    public static final <T> void a(tc1<? super q70<? super T>, ? extends Object> tc1Var, q70<? super T> q70Var) {
        q70 a = ef0.a(q70Var);
        try {
            CoroutineContext context = q70Var.getContext();
            Object c = ThreadContextKt.c(context, null);
            if (tc1Var != null) {
                Object invoke = ((tc1) qd4.c(tc1Var, 1)).invoke(a);
                ThreadContextKt.a(context, c);
                if (invoke != gs1.d()) {
                    Result.a aVar = Result.Companion;
                    a.resumeWith(Result.m52constructorimpl(invoke));
                    return;
                }
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            a.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }
    }

    public static final <R, T> void b(hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var, R r, q70<? super T> q70Var) {
        q70 a = ef0.a(q70Var);
        try {
            CoroutineContext context = q70Var.getContext();
            Object c = ThreadContextKt.c(context, null);
            if (hd1Var != null) {
                Object invoke = ((hd1) qd4.c(hd1Var, 2)).invoke(r, a);
                ThreadContextKt.a(context, c);
                if (invoke != gs1.d()) {
                    Result.a aVar = Result.Companion;
                    a.resumeWith(Result.m52constructorimpl(invoke));
                    return;
                }
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            a.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }
    }

    public static final <T, R> Object c(vd3<? super T> vd3Var, R r, hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var) {
        Object t30Var;
        Object k0;
        Throwable j;
        try {
        } catch (Throwable th) {
            t30Var = new t30(th, false, 2, null);
        }
        if (hd1Var != null) {
            t30Var = ((hd1) qd4.c(hd1Var, 2)).invoke(r, vd3Var);
            if (t30Var != gs1.d() && (k0 = vd3Var.k0(t30Var)) != cu1.b) {
                if (k0 instanceof t30) {
                    Throwable th2 = ((t30) k0).a;
                    q70<? super T> q70Var = vd3Var.g0;
                    if (ze0.d() && (q70Var instanceof e90)) {
                        j = hs3.j(th2, (e90) q70Var);
                        throw j;
                    }
                    throw th2;
                }
                return cu1.h(k0);
            }
            return gs1.d();
        }
        throw new NullPointerException("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
    }
}
