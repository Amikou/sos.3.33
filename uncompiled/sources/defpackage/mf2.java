package defpackage;

import io.reactivex.internal.schedulers.RxThreadFactory;
import java.util.concurrent.ThreadFactory;

/* compiled from: NewThreadScheduler.java */
/* renamed from: mf2  reason: default package */
/* loaded from: classes2.dex */
public final class mf2 extends bd3 {
    public static final RxThreadFactory a = new RxThreadFactory("RxNewThreadScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.newthread-priority", 5).intValue())));

    public mf2() {
        this(a);
    }

    public mf2(ThreadFactory threadFactory) {
    }
}
