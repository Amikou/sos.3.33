package defpackage;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.measurement.internal.zzaq;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: s55  reason: default package */
/* loaded from: classes.dex */
public final class s55 {
    public final String a;
    public final String b;
    public final String c;
    public final long d;
    public final long e;
    public final zzaq f;

    public s55(ck5 ck5Var, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        zzaq zzaqVar;
        zt2.f(str2);
        zt2.f(str3);
        this.a = str2;
        this.b = str3;
        this.c = true == TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        if (j2 != 0 && j2 > j) {
            ck5Var.w().p().b("Event created with reverse previous/current timestamps. appId", og5.x(str2));
        }
        if (bundle != null && !bundle.isEmpty()) {
            Bundle bundle2 = new Bundle(bundle);
            Iterator<String> it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (next == null) {
                    ck5Var.w().l().a("Param name can't be null");
                    it.remove();
                } else {
                    Object r = ck5Var.G().r(next, bundle2.get(next));
                    if (r == null) {
                        ck5Var.w().p().b("Param value can't be null", ck5Var.H().o(next));
                        it.remove();
                    } else {
                        ck5Var.G().z(bundle2, next, r);
                    }
                }
            }
            zzaqVar = new zzaq(bundle2);
        } else {
            zzaqVar = new zzaq(new Bundle());
        }
        this.f = zzaqVar;
    }

    public final s55 a(ck5 ck5Var, long j) {
        return new s55(ck5Var, this.c, this.a, this.b, this.d, j, this.f);
    }

    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f);
        int length = String.valueOf(str).length();
        StringBuilder sb = new StringBuilder(length + 33 + String.valueOf(str2).length() + valueOf.length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    public s55(ck5 ck5Var, String str, String str2, String str3, long j, long j2, zzaq zzaqVar) {
        zt2.f(str2);
        zt2.f(str3);
        zt2.j(zzaqVar);
        this.a = str2;
        this.b = str3;
        this.c = true == TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        if (j2 != 0 && j2 > j) {
            ck5Var.w().p().c("Event created with reverse previous/current timestamps. appId, name", og5.x(str2), og5.x(str3));
        }
        this.f = zzaqVar;
    }
}
