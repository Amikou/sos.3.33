package defpackage;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ez5  reason: default package */
/* loaded from: classes.dex */
public final class ez5 implements Iterator<String> {
    public Iterator<String> a;
    public final /* synthetic */ uy5 f0;

    public ez5(uy5 uy5Var) {
        gu5 gu5Var;
        this.f0 = uy5Var;
        gu5Var = uy5Var.a;
        this.a = gu5Var.iterator();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.a.next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
