package defpackage;

import android.os.Build;
import com.facebook.imagepipeline.platform.GingerbreadPurgeableDecoder;
import com.facebook.imagepipeline.platform.KitKatPurgeableDecoder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: PlatformDecoderFactory.java */
/* renamed from: er2  reason: default package */
/* loaded from: classes.dex */
public class er2 {
    public static dr2 a(xs2 xs2Var, boolean z) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            int e = xs2Var.e();
            return new yn2(xs2Var.b(), e, new it2(e));
        } else if (i < 21 && ld2.a()) {
            try {
                if (!z || i >= 19) {
                    int i2 = KitKatPurgeableDecoder.d;
                    return (dr2) KitKatPurgeableDecoder.class.getConstructor(y61.class).newInstance(xs2Var.d());
                }
                Method method = GingerbreadPurgeableDecoder.d;
                return (dr2) GingerbreadPurgeableDecoder.class.getConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (ClassNotFoundException e2) {
                throw new RuntimeException("Wrong Native code setup, reflection failed.", e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException("Wrong Native code setup, reflection failed.", e3);
            } catch (InstantiationException e4) {
                throw new RuntimeException("Wrong Native code setup, reflection failed.", e4);
            } catch (NoSuchMethodException e5) {
                throw new RuntimeException("Wrong Native code setup, reflection failed.", e5);
            } catch (InvocationTargetException e6) {
                throw new RuntimeException("Wrong Native code setup, reflection failed.", e6);
            }
        } else {
            int e7 = xs2Var.e();
            return new di(xs2Var.b(), e7, new it2(e7));
        }
    }
}
