package defpackage;

import android.app.Notification;
import android.app.RemoteInput;
import android.content.Context;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.dh2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: NotificationCompatBuilder.java */
/* renamed from: eh2  reason: default package */
/* loaded from: classes.dex */
public class eh2 implements ch2 {
    public final Context a;
    public final Notification.Builder b;
    public final dh2.e c;
    public RemoteViews d;
    public RemoteViews e;
    public final List<Bundle> f = new ArrayList();
    public final Bundle g = new Bundle();
    public int h;
    public RemoteViews i;

    public eh2(dh2.e eVar) {
        int i;
        Icon icon;
        List<String> list;
        List<String> e;
        this.c = eVar;
        this.a = eVar.a;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 26) {
            this.b = new Notification.Builder(eVar.a, eVar.K);
        } else {
            this.b = new Notification.Builder(eVar.a);
        }
        Notification notification = eVar.S;
        this.b.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, eVar.i).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(eVar.e).setContentText(eVar.f).setContentInfo(eVar.k).setContentIntent(eVar.g).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(eVar.h, (notification.flags & 128) != 0).setLargeIcon(eVar.j).setNumber(eVar.l).setProgress(eVar.t, eVar.u, eVar.v);
        if (i2 < 21) {
            this.b.setSound(notification.sound, notification.audioStreamType);
        }
        if (i2 >= 16) {
            this.b.setSubText(eVar.q).setUsesChronometer(eVar.o).setPriority(eVar.m);
            Iterator<dh2.a> it = eVar.b.iterator();
            while (it.hasNext()) {
                b(it.next());
            }
            Bundle bundle = eVar.D;
            if (bundle != null) {
                this.g.putAll(bundle);
            }
            if (Build.VERSION.SDK_INT < 20) {
                if (eVar.z) {
                    this.g.putBoolean("android.support.localOnly", true);
                }
                String str = eVar.w;
                if (str != null) {
                    this.g.putString("android.support.groupKey", str);
                    if (eVar.x) {
                        this.g.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.g.putBoolean("android.support.useSideChannel", true);
                    }
                }
                String str2 = eVar.y;
                if (str2 != null) {
                    this.g.putString("android.support.sortKey", str2);
                }
            }
            this.d = eVar.H;
            this.e = eVar.I;
        }
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 17) {
            this.b.setShowWhen(eVar.n);
        }
        if (i3 >= 19 && i3 < 21 && (e = e(g(eVar.c), eVar.V)) != null && !e.isEmpty()) {
            this.g.putStringArray("android.people", (String[]) e.toArray(new String[e.size()]));
        }
        if (i3 >= 20) {
            this.b.setLocalOnly(eVar.z).setGroup(eVar.w).setGroupSummary(eVar.x).setSortKey(eVar.y);
            this.h = eVar.O;
        }
        if (i3 >= 21) {
            this.b.setCategory(eVar.C).setColor(eVar.E).setVisibility(eVar.F).setPublicVersion(eVar.G).setSound(notification.sound, notification.audioAttributes);
            if (i3 < 28) {
                list = e(g(eVar.c), eVar.V);
            } else {
                list = eVar.V;
            }
            if (list != null && !list.isEmpty()) {
                for (String str3 : list) {
                    this.b.addPerson(str3);
                }
            }
            this.i = eVar.J;
            if (eVar.d.size() > 0) {
                Bundle bundle2 = eVar.d().getBundle("android.car.EXTENSIONS");
                bundle2 = bundle2 == null ? new Bundle() : bundle2;
                Bundle bundle3 = new Bundle(bundle2);
                Bundle bundle4 = new Bundle();
                for (int i4 = 0; i4 < eVar.d.size(); i4++) {
                    bundle4.putBundle(Integer.toString(i4), fh2.b(eVar.d.get(i4)));
                }
                bundle2.putBundle("invisible_actions", bundle4);
                bundle3.putBundle("invisible_actions", bundle4);
                eVar.d().putBundle("android.car.EXTENSIONS", bundle2);
                this.g.putBundle("android.car.EXTENSIONS", bundle3);
            }
        }
        int i5 = Build.VERSION.SDK_INT;
        if (i5 >= 23 && (icon = eVar.U) != null) {
            this.b.setSmallIcon(icon);
        }
        if (i5 >= 24) {
            this.b.setExtras(eVar.D).setRemoteInputHistory(eVar.s);
            RemoteViews remoteViews = eVar.H;
            if (remoteViews != null) {
                this.b.setCustomContentView(remoteViews);
            }
            RemoteViews remoteViews2 = eVar.I;
            if (remoteViews2 != null) {
                this.b.setCustomBigContentView(remoteViews2);
            }
            RemoteViews remoteViews3 = eVar.J;
            if (remoteViews3 != null) {
                this.b.setCustomHeadsUpContentView(remoteViews3);
            }
        }
        if (i5 >= 26) {
            this.b.setBadgeIconType(eVar.L).setSettingsText(eVar.r).setShortcutId(eVar.M).setTimeoutAfter(eVar.N).setGroupAlertBehavior(eVar.O);
            if (eVar.B) {
                this.b.setColorized(eVar.A);
            }
            if (!TextUtils.isEmpty(eVar.K)) {
                this.b.setSound(null).setDefaults(0).setLights(0, 0, 0).setVibrate(null);
            }
        }
        if (i5 >= 28) {
            Iterator<oq2> it2 = eVar.c.iterator();
            while (it2.hasNext()) {
                this.b.addPerson(it2.next().h());
            }
        }
        int i6 = Build.VERSION.SDK_INT;
        if (i6 >= 29) {
            this.b.setAllowSystemGeneratedContextualActions(eVar.Q);
            this.b.setBubbleMetadata(dh2.d.a(eVar.R));
        }
        if (yr.c() && (i = eVar.P) != 0) {
            this.b.setForegroundServiceBehavior(i);
        }
        if (eVar.T) {
            if (this.c.x) {
                this.h = 2;
            } else {
                this.h = 1;
            }
            this.b.setVibrate(null);
            this.b.setSound(null);
            int i7 = notification.defaults & (-2);
            notification.defaults = i7;
            int i8 = i7 & (-3);
            notification.defaults = i8;
            this.b.setDefaults(i8);
            if (i6 >= 26) {
                if (TextUtils.isEmpty(this.c.w)) {
                    this.b.setGroup("silent");
                }
                this.b.setGroupAlertBehavior(this.h);
            }
        }
    }

    public static List<String> e(List<String> list, List<String> list2) {
        if (list == null) {
            return list2;
        }
        if (list2 == null) {
            return list;
        }
        uh uhVar = new uh(list.size() + list2.size());
        uhVar.addAll(list);
        uhVar.addAll(list2);
        return new ArrayList(uhVar);
    }

    public static List<String> g(List<oq2> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (oq2 oq2Var : list) {
            arrayList.add(oq2Var.g());
        }
        return arrayList;
    }

    @Override // defpackage.ch2
    public Notification.Builder a() {
        return this.b;
    }

    public final void b(dh2.a aVar) {
        Notification.Action.Builder builder;
        Bundle bundle;
        int i = Build.VERSION.SDK_INT;
        if (i < 20) {
            if (i >= 16) {
                this.f.add(fh2.f(this.b, aVar));
                return;
            }
            return;
        }
        IconCompat e = aVar.e();
        if (i >= 23) {
            builder = new Notification.Action.Builder(e != null ? e.p() : null, aVar.i(), aVar.a());
        } else {
            builder = new Notification.Action.Builder(e != null ? e.e() : 0, aVar.i(), aVar.a());
        }
        if (aVar.f() != null) {
            for (RemoteInput remoteInput : n63.b(aVar.f())) {
                builder.addRemoteInput(remoteInput);
            }
        }
        if (aVar.d() != null) {
            bundle = new Bundle(aVar.d());
        } else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.b());
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 24) {
            builder.setAllowGeneratedReplies(aVar.b());
        }
        bundle.putInt("android.support.action.semanticAction", aVar.g());
        if (i2 >= 28) {
            builder.setSemanticAction(aVar.g());
        }
        if (i2 >= 29) {
            builder.setContextual(aVar.j());
        }
        bundle.putBoolean("android.support.action.showsUserInterface", aVar.h());
        builder.addExtras(bundle);
        this.b.addAction(builder.build());
    }

    public Notification c() {
        Bundle a;
        RemoteViews f;
        RemoteViews d;
        dh2.h hVar = this.c.p;
        if (hVar != null) {
            hVar.b(this);
        }
        RemoteViews e = hVar != null ? hVar.e(this) : null;
        Notification d2 = d();
        if (e != null) {
            d2.contentView = e;
        } else {
            RemoteViews remoteViews = this.c.H;
            if (remoteViews != null) {
                d2.contentView = remoteViews;
            }
        }
        int i = Build.VERSION.SDK_INT;
        if (i >= 16 && hVar != null && (d = hVar.d(this)) != null) {
            d2.bigContentView = d;
        }
        if (i >= 21 && hVar != null && (f = this.c.p.f(this)) != null) {
            d2.headsUpContentView = f;
        }
        if (i >= 16 && hVar != null && (a = dh2.a(d2)) != null) {
            hVar.a(a);
        }
        return d2;
    }

    public Notification d() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            return this.b.build();
        }
        if (i >= 24) {
            Notification build = this.b.build();
            if (this.h != 0) {
                if (build.getGroup() != null && (build.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 && this.h == 2) {
                    h(build);
                }
                if (build.getGroup() != null && (build.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 && this.h == 1) {
                    h(build);
                }
            }
            return build;
        } else if (i >= 21) {
            this.b.setExtras(this.g);
            Notification build2 = this.b.build();
            RemoteViews remoteViews = this.d;
            if (remoteViews != null) {
                build2.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.e;
            if (remoteViews2 != null) {
                build2.bigContentView = remoteViews2;
            }
            RemoteViews remoteViews3 = this.i;
            if (remoteViews3 != null) {
                build2.headsUpContentView = remoteViews3;
            }
            if (this.h != 0) {
                if (build2.getGroup() != null && (build2.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 && this.h == 2) {
                    h(build2);
                }
                if (build2.getGroup() != null && (build2.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 && this.h == 1) {
                    h(build2);
                }
            }
            return build2;
        } else if (i >= 20) {
            this.b.setExtras(this.g);
            Notification build3 = this.b.build();
            RemoteViews remoteViews4 = this.d;
            if (remoteViews4 != null) {
                build3.contentView = remoteViews4;
            }
            RemoteViews remoteViews5 = this.e;
            if (remoteViews5 != null) {
                build3.bigContentView = remoteViews5;
            }
            if (this.h != 0) {
                if (build3.getGroup() != null && (build3.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 && this.h == 2) {
                    h(build3);
                }
                if (build3.getGroup() != null && (build3.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 && this.h == 1) {
                    h(build3);
                }
            }
            return build3;
        } else if (i >= 19) {
            SparseArray<Bundle> a = fh2.a(this.f);
            if (a != null) {
                this.g.putSparseParcelableArray("android.support.actionExtras", a);
            }
            this.b.setExtras(this.g);
            Notification build4 = this.b.build();
            RemoteViews remoteViews6 = this.d;
            if (remoteViews6 != null) {
                build4.contentView = remoteViews6;
            }
            RemoteViews remoteViews7 = this.e;
            if (remoteViews7 != null) {
                build4.bigContentView = remoteViews7;
            }
            return build4;
        } else if (i >= 16) {
            Notification build5 = this.b.build();
            Bundle a2 = dh2.a(build5);
            Bundle bundle = new Bundle(this.g);
            for (String str : this.g.keySet()) {
                if (a2.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            a2.putAll(bundle);
            SparseArray<Bundle> a3 = fh2.a(this.f);
            if (a3 != null) {
                dh2.a(build5).putSparseParcelableArray("android.support.actionExtras", a3);
            }
            RemoteViews remoteViews8 = this.d;
            if (remoteViews8 != null) {
                build5.contentView = remoteViews8;
            }
            RemoteViews remoteViews9 = this.e;
            if (remoteViews9 != null) {
                build5.bigContentView = remoteViews9;
            }
            return build5;
        } else {
            return this.b.getNotification();
        }
    }

    public Context f() {
        return this.a;
    }

    public final void h(Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        int i = notification.defaults & (-2);
        notification.defaults = i;
        notification.defaults = i & (-3);
    }
}
