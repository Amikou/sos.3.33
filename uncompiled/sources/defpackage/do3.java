package defpackage;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import kotlin.Pair;
import net.safemoon.androidwallet.R;

/* compiled from: SharedUtility.kt */
/* renamed from: do3  reason: default package */
/* loaded from: classes2.dex */
public final class do3 {
    public static final do3 a = new do3();

    public final String a(Context context) {
        fs1.f(context, "context");
        String j = bo3.j(context, "DEFAULT_LANGUAGE", "en");
        fs1.e(j, "getString(context, Share…GUAGE, Languages.English)");
        return j;
    }

    public final int b(Context context) {
        boolean z;
        fs1.f(context, "context");
        int f = bo3.f(context, "DEFAULT_SCREEN", R.id.navigation_wallet);
        ArrayList<Pair<Integer, Integer>> a2 = jd0.a.a();
        boolean z2 = true;
        if (!(a2 instanceof Collection) || !a2.isEmpty()) {
            Iterator<T> it = a2.iterator();
            while (it.hasNext()) {
                if (((Number) ((Pair) it.next()).getFirst()).intValue() == f) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
        }
        z2 = false;
        return z2 ? bo3.f(context, "DEFAULT_SCREEN", R.id.navigation_wallet) : R.id.navigation_wallet;
    }

    public final boolean c(Context context) {
        fs1.f(context, "context");
        return bo3.e(context, "DISPLAY_MASTER_ACCOUNT", false);
    }

    public final boolean d(Context context) {
        fs1.f(context, "context");
        return bo3.e(context, "SAFEMOON_LINKED_ALL_WALLET", false);
    }

    public final void e(Context context, String str) {
        fs1.f(context, "context");
        fs1.f(str, "defaultLanguage");
        bo3.o(context, "DEFAULT_LANGUAGE", str);
    }

    public final void f(Context context, int i) {
        fs1.f(context, "context");
        bo3.l(context, "DEFAULT_SCREEN", i);
    }

    public final void g(Context context, boolean z) {
        fs1.f(context, "context");
        bo3.n(context, "DISPLAY_MASTER_ACCOUNT", Boolean.valueOf(z));
    }

    public final void h(Context context, boolean z) {
        fs1.f(context, "context");
        bo3.n(context, "SAFEMOON_LINKED_ALL_WALLET", Boolean.valueOf(z));
    }
}
