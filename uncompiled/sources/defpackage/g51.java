package defpackage;

import android.content.Context;
import com.google.firebase.FirebaseCommonRegistrar;
import defpackage.jz1;

/* renamed from: g51  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class g51 implements jz1.a {
    public static final /* synthetic */ g51 a = new g51();

    @Override // defpackage.jz1.a
    public final String extract(Object obj) {
        String g;
        g = FirebaseCommonRegistrar.g((Context) obj);
        return g;
    }
}
