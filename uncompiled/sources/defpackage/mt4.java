package defpackage;

import android.os.Bundle;
import com.google.android.play.core.assetpacks.a;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: mt4  reason: default package */
/* loaded from: classes2.dex */
public final class mt4 extends a<Void> {
    public final /* synthetic */ st4 c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public mt4(st4 st4Var, tx4<Void> tx4Var) {
        super(st4Var, tx4Var);
        this.c = st4Var;
    }

    @Override // com.google.android.play.core.assetpacks.a, com.google.android.play.core.internal.r
    public final void Q0(Bundle bundle, Bundle bundle2) {
        AtomicBoolean atomicBoolean;
        it4 it4Var;
        super.Q0(bundle, bundle2);
        atomicBoolean = this.c.e;
        if (!atomicBoolean.compareAndSet(true, false)) {
            it4Var = st4.f;
            it4Var.e("Expected keepingAlive to be true, but was false.", new Object[0]);
        }
        if (bundle.getBoolean("keep_alive")) {
            this.c.a();
        }
    }
}
