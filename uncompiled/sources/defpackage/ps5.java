package defpackage;

import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ps5  reason: default package */
/* loaded from: classes.dex */
public final class ps5 implements Runnable {
    public final /* synthetic */ AtomicReference a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ zzp h0;
    public final /* synthetic */ boolean i0;
    public final /* synthetic */ p j0;

    public ps5(p pVar, AtomicReference atomicReference, String str, String str2, String str3, zzp zzpVar, boolean z) {
        this.j0 = pVar;
        this.a = atomicReference;
        this.f0 = str2;
        this.g0 = str3;
        this.h0 = zzpVar;
        this.i0 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AtomicReference atomicReference;
        d dVar;
        synchronized (this.a) {
            try {
                dVar = this.j0.d;
            } catch (RemoteException e) {
                this.j0.a.w().l().d("(legacy) Failed to get user properties; remote exception", null, this.f0, e);
                this.a.set(Collections.emptyList());
                atomicReference = this.a;
            }
            if (dVar == null) {
                this.j0.a.w().l().d("(legacy) Failed to get user properties; not connected to service", null, this.f0, this.g0);
                this.a.set(Collections.emptyList());
                this.a.notify();
                return;
            }
            if (TextUtils.isEmpty(null)) {
                zt2.j(this.h0);
                this.a.set(dVar.X(this.f0, this.g0, this.i0, this.h0));
            } else {
                this.a.set(dVar.B1(null, this.f0, this.g0, this.i0));
            }
            this.j0.D();
            atomicReference = this.a;
            atomicReference.notify();
        }
    }
}
