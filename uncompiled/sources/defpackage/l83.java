package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Status;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: l83  reason: default package */
/* loaded from: classes.dex */
public interface l83 {
    @RecentlyNonNull
    Status e();
}
