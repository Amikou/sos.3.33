package defpackage;

import android.os.SystemClock;

/* compiled from: Fps.java */
/* renamed from: m91  reason: default package */
/* loaded from: classes.dex */
public class m91 {
    public long a;
    public long b;
    public int c;

    public void a() {
        if (ef1.b()) {
            long uptimeMillis = SystemClock.uptimeMillis();
            this.a = uptimeMillis;
            this.b = uptimeMillis;
            this.c = 0;
        }
    }

    public void b() {
        if (ef1.b()) {
            long uptimeMillis = SystemClock.uptimeMillis() - this.a;
            if (uptimeMillis > 40) {
                StringBuilder sb = new StringBuilder();
                sb.append("Frame time: ");
                sb.append(uptimeMillis);
            } else if (uptimeMillis > 20) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Frame time: ");
                sb2.append(uptimeMillis);
            }
            this.c++;
            this.a = SystemClock.uptimeMillis();
        }
    }

    public void c() {
        if (!ef1.b() || this.c <= 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Average FPS: ");
        sb.append(Math.round((this.c * 1000.0f) / ((int) (SystemClock.uptimeMillis() - this.b))));
    }
}
