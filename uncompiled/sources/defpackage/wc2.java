package defpackage;

import java.nio.ByteBuffer;
import java.util.Arrays;

/* compiled from: NalUnitUtil.java */
/* renamed from: wc2  reason: default package */
/* loaded from: classes.dex */
public final class wc2 {
    public static final byte[] a = {0, 0, 0, 1};
    public static final float[] b = {1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 2.1818182f, 1.8181819f, 2.909091f, 2.4242425f, 1.6363636f, 1.3636364f, 1.939394f, 1.6161616f, 1.3333334f, 1.5f, 2.0f};
    public static final Object c = new Object();
    public static int[] d = new int[10];

    /* compiled from: NalUnitUtil.java */
    /* renamed from: wc2$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final boolean b;
        public final int c;
        public final int d;
        public final int[] e;
        public final int f;
        public final int g;
        public final int h;
        public final float i;

        public a(int i, boolean z, int i2, int i3, int[] iArr, int i4, int i5, int i6, int i7, float f) {
            this.a = i;
            this.b = z;
            this.c = i2;
            this.d = i3;
            this.e = iArr;
            this.f = i4;
            this.g = i6;
            this.h = i7;
            this.i = f;
        }
    }

    /* compiled from: NalUnitUtil.java */
    /* renamed from: wc2$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final int b;
        public final boolean c;

        public b(int i, int i2, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = z;
        }
    }

    /* compiled from: NalUnitUtil.java */
    /* renamed from: wc2$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final int a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final float g;
        public final boolean h;
        public final boolean i;
        public final int j;
        public final int k;
        public final int l;
        public final boolean m;

        public c(int i, int i2, int i3, int i4, int i5, int i6, int i7, float f, boolean z, boolean z2, int i8, int i9, int i10, boolean z3) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i6;
            this.f = i7;
            this.g = f;
            this.h = z;
            this.i = z2;
            this.j = i8;
            this.k = i9;
            this.l = i10;
            this.m = z3;
        }
    }

    public static void a(boolean[] zArr) {
        zArr[0] = false;
        zArr[1] = false;
        zArr[2] = false;
    }

    public static void b(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i + 1;
            if (i3 < position) {
                int i4 = byteBuffer.get(i) & 255;
                if (i2 == 3) {
                    if (i4 == 1 && (byteBuffer.get(i3) & 31) == 7) {
                        ByteBuffer duplicate = byteBuffer.duplicate();
                        duplicate.position(i - 3);
                        duplicate.limit(position);
                        byteBuffer.position(0);
                        byteBuffer.put(duplicate);
                        return;
                    }
                } else if (i4 == 0) {
                    i2++;
                }
                if (i4 != 0) {
                    i2 = 0;
                }
                i = i3;
            } else {
                byteBuffer.clear();
                return;
            }
        }
    }

    public static int c(byte[] bArr, int i, int i2, boolean[] zArr) {
        int i3 = i2 - i;
        ii.g(i3 >= 0);
        if (i3 == 0) {
            return i2;
        }
        if (zArr[0]) {
            a(zArr);
            return i - 3;
        } else if (i3 > 1 && zArr[1] && bArr[i] == 1) {
            a(zArr);
            return i - 2;
        } else if (i3 > 2 && zArr[2] && bArr[i] == 0 && bArr[i + 1] == 1) {
            a(zArr);
            return i - 1;
        } else {
            int i4 = i2 - 1;
            int i5 = i + 2;
            while (i5 < i4) {
                if ((bArr[i5] & 254) == 0) {
                    int i6 = i5 - 2;
                    if (bArr[i6] == 0 && bArr[i5 - 1] == 0 && bArr[i5] == 1) {
                        a(zArr);
                        return i6;
                    }
                    i5 -= 2;
                }
                i5 += 3;
            }
            zArr[0] = i3 <= 2 ? !(i3 != 2 ? !(zArr[1] && bArr[i4] == 1) : !(zArr[2] && bArr[i2 + (-2)] == 0 && bArr[i4] == 1)) : bArr[i2 + (-3)] == 0 && bArr[i2 + (-2)] == 0 && bArr[i4] == 1;
            zArr[1] = i3 <= 1 ? zArr[2] && bArr[i4] == 0 : bArr[i2 + (-2)] == 0 && bArr[i4] == 0;
            zArr[2] = bArr[i4] == 0;
            return i2;
        }
    }

    public static int d(byte[] bArr, int i, int i2) {
        while (i < i2 - 2) {
            if (bArr[i] == 0 && bArr[i + 1] == 0 && bArr[i + 2] == 3) {
                return i;
            }
            i++;
        }
        return i2;
    }

    public static int e(byte[] bArr, int i) {
        return (bArr[i + 3] & 126) >> 1;
    }

    public static int f(byte[] bArr, int i) {
        return bArr[i + 3] & 31;
    }

    public static boolean g(String str, byte b2) {
        if ("video/avc".equals(str) && (b2 & 31) == 6) {
            return true;
        }
        return "video/hevc".equals(str) && ((b2 & 126) >> 1) == 39;
    }

    public static a h(byte[] bArr, int i, int i2) {
        return i(bArr, i + 2, i2);
    }

    public static a i(byte[] bArr, int i, int i2) {
        pp2 pp2Var = new pp2(bArr, i, i2);
        pp2Var.l(4);
        int e = pp2Var.e(3);
        pp2Var.k();
        int e2 = pp2Var.e(2);
        boolean d2 = pp2Var.d();
        int e3 = pp2Var.e(5);
        int i3 = 0;
        for (int i4 = 0; i4 < 32; i4++) {
            if (pp2Var.d()) {
                i3 |= 1 << i4;
            }
        }
        int[] iArr = new int[6];
        for (int i5 = 0; i5 < 6; i5++) {
            iArr[i5] = pp2Var.e(8);
        }
        int e4 = pp2Var.e(8);
        int i6 = 0;
        for (int i7 = 0; i7 < e; i7++) {
            if (pp2Var.d()) {
                i6 += 89;
            }
            if (pp2Var.d()) {
                i6 += 8;
            }
        }
        pp2Var.l(i6);
        if (e > 0) {
            pp2Var.l((8 - e) * 2);
        }
        int h = pp2Var.h();
        int h2 = pp2Var.h();
        if (h2 == 3) {
            pp2Var.k();
        }
        int h3 = pp2Var.h();
        int h4 = pp2Var.h();
        if (pp2Var.d()) {
            int h5 = pp2Var.h();
            int h6 = pp2Var.h();
            int h7 = pp2Var.h();
            int h8 = pp2Var.h();
            h3 -= ((h2 == 1 || h2 == 2) ? 2 : 1) * (h5 + h6);
            h4 -= (h2 == 1 ? 2 : 1) * (h7 + h8);
        }
        pp2Var.h();
        pp2Var.h();
        int h9 = pp2Var.h();
        for (int i8 = pp2Var.d() ? 0 : e; i8 <= e; i8++) {
            pp2Var.h();
            pp2Var.h();
            pp2Var.h();
        }
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        if (pp2Var.d() && pp2Var.d()) {
            n(pp2Var);
        }
        pp2Var.l(2);
        if (pp2Var.d()) {
            pp2Var.l(8);
            pp2Var.h();
            pp2Var.h();
            pp2Var.k();
        }
        p(pp2Var);
        if (pp2Var.d()) {
            for (int i9 = 0; i9 < pp2Var.h(); i9++) {
                pp2Var.l(h9 + 4 + 1);
            }
        }
        pp2Var.l(2);
        float f = 1.0f;
        if (pp2Var.d()) {
            if (pp2Var.d()) {
                int e5 = pp2Var.e(8);
                if (e5 == 255) {
                    int e6 = pp2Var.e(16);
                    int e7 = pp2Var.e(16);
                    if (e6 != 0 && e7 != 0) {
                        f = e6 / e7;
                    }
                } else {
                    float[] fArr = b;
                    if (e5 < fArr.length) {
                        f = fArr[e5];
                    } else {
                        p12.i("NalUnitUtil", "Unexpected aspect_ratio_idc value: " + e5);
                    }
                }
            }
            if (pp2Var.d()) {
                pp2Var.k();
            }
            if (pp2Var.d()) {
                pp2Var.l(4);
                if (pp2Var.d()) {
                    pp2Var.l(24);
                }
            }
            if (pp2Var.d()) {
                pp2Var.h();
                pp2Var.h();
            }
            pp2Var.k();
            if (pp2Var.d()) {
                h4 *= 2;
            }
        }
        return new a(e2, d2, e3, i3, iArr, e4, h, h3, h4, f);
    }

    public static b j(byte[] bArr, int i, int i2) {
        return k(bArr, i + 1, i2);
    }

    public static b k(byte[] bArr, int i, int i2) {
        pp2 pp2Var = new pp2(bArr, i, i2);
        int h = pp2Var.h();
        int h2 = pp2Var.h();
        pp2Var.k();
        return new b(h, h2, pp2Var.d());
    }

    public static c l(byte[] bArr, int i, int i2) {
        return m(bArr, i + 1, i2);
    }

    /* JADX WARN: Removed duplicated region for block: B:56:0x00d5  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:76:0x0133  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x0145  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.wc2.c m(byte[] r22, int r23, int r24) {
        /*
            Method dump skipped, instructions count: 364
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.wc2.m(byte[], int, int):wc2$c");
    }

    public static void n(pp2 pp2Var) {
        for (int i = 0; i < 4; i++) {
            int i2 = 0;
            while (i2 < 6) {
                int i3 = 1;
                if (!pp2Var.d()) {
                    pp2Var.h();
                } else {
                    int min = Math.min(64, 1 << ((i << 1) + 4));
                    if (i > 1) {
                        pp2Var.g();
                    }
                    for (int i4 = 0; i4 < min; i4++) {
                        pp2Var.g();
                    }
                }
                if (i == 3) {
                    i3 = 3;
                }
                i2 += i3;
            }
        }
    }

    public static void o(pp2 pp2Var, int i) {
        int i2 = 8;
        int i3 = 8;
        for (int i4 = 0; i4 < i; i4++) {
            if (i2 != 0) {
                i2 = ((pp2Var.g() + i3) + 256) % 256;
            }
            if (i2 != 0) {
                i3 = i2;
            }
        }
    }

    public static void p(pp2 pp2Var) {
        int h = pp2Var.h();
        int[] iArr = new int[0];
        int[] iArr2 = new int[0];
        int i = -1;
        int i2 = 0;
        int i3 = -1;
        while (i2 < h) {
            if (((i2 == 0 || !pp2Var.d()) ? (byte) 0 : (byte) 1) != 0) {
                int i4 = i + i3;
                int h2 = (1 - ((pp2Var.d() ? 1 : 0) * 2)) * (pp2Var.h() + 1);
                int i5 = i4 + 1;
                boolean[] zArr = new boolean[i5];
                for (int i6 = 0; i6 <= i4; i6++) {
                    if (!pp2Var.d()) {
                        zArr[i6] = pp2Var.d();
                    } else {
                        zArr[i6] = true;
                    }
                }
                int[] iArr3 = new int[i5];
                int[] iArr4 = new int[i5];
                int i7 = 0;
                for (int i8 = i3 - 1; i8 >= 0; i8--) {
                    int i9 = iArr2[i8] + h2;
                    if (i9 < 0 && zArr[i + i8]) {
                        iArr3[i7] = i9;
                        i7++;
                    }
                }
                if (h2 < 0 && zArr[i4]) {
                    iArr3[i7] = h2;
                    i7++;
                }
                for (int i10 = 0; i10 < i; i10++) {
                    int i11 = iArr[i10] + h2;
                    if (i11 < 0 && zArr[i10]) {
                        iArr3[i7] = i11;
                        i7++;
                    }
                }
                int[] copyOf = Arrays.copyOf(iArr3, i7);
                int i12 = 0;
                for (int i13 = i - 1; i13 >= 0; i13--) {
                    int i14 = iArr[i13] + h2;
                    if (i14 > 0 && zArr[i13]) {
                        iArr4[i12] = i14;
                        i12++;
                    }
                }
                if (h2 > 0 && zArr[i4]) {
                    iArr4[i12] = h2;
                    i12++;
                }
                for (int i15 = 0; i15 < i3; i15++) {
                    int i16 = iArr2[i15] + h2;
                    if (i16 > 0 && zArr[i + i15]) {
                        iArr4[i12] = i16;
                        i12++;
                    }
                }
                iArr2 = Arrays.copyOf(iArr4, i12);
                iArr = copyOf;
                i = i7;
                i3 = i12;
            } else {
                int h3 = pp2Var.h();
                int h4 = pp2Var.h();
                int[] iArr5 = new int[h3];
                for (int i17 = 0; i17 < h3; i17++) {
                    iArr5[i17] = pp2Var.h() + 1;
                    pp2Var.k();
                }
                int[] iArr6 = new int[h4];
                for (int i18 = 0; i18 < h4; i18++) {
                    iArr6[i18] = pp2Var.h() + 1;
                    pp2Var.k();
                }
                i = h3;
                iArr = iArr5;
                i3 = h4;
                iArr2 = iArr6;
            }
            i2++;
        }
    }

    public static int q(byte[] bArr, int i) {
        int i2;
        synchronized (c) {
            int i3 = 0;
            int i4 = 0;
            while (i3 < i) {
                try {
                    i3 = d(bArr, i3, i);
                    if (i3 < i) {
                        int[] iArr = d;
                        if (iArr.length <= i4) {
                            d = Arrays.copyOf(iArr, iArr.length * 2);
                        }
                        d[i4] = i3;
                        i3 += 3;
                        i4++;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
            i2 = i - i4;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < i4; i7++) {
                int i8 = d[i7] - i6;
                System.arraycopy(bArr, i6, bArr, i5, i8);
                int i9 = i5 + i8;
                int i10 = i9 + 1;
                bArr[i9] = 0;
                i5 = i10 + 1;
                bArr[i10] = 0;
                i6 += i8 + 3;
            }
            System.arraycopy(bArr, i6, bArr, i5, i2 - i5);
        }
        return i2;
    }
}
