package defpackage;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSOutcomeSource.kt */
/* renamed from: pk2  reason: default package */
/* loaded from: classes2.dex */
public final class pk2 {
    public qk2 a;
    public qk2 b;

    public pk2(qk2 qk2Var, qk2 qk2Var2) {
        this.a = qk2Var;
        this.b = qk2Var2;
    }

    public final qk2 a() {
        return this.a;
    }

    public final qk2 b() {
        return this.b;
    }

    public final pk2 c(qk2 qk2Var) {
        this.a = qk2Var;
        return this;
    }

    public final pk2 d(qk2 qk2Var) {
        this.b = qk2Var;
        return this;
    }

    public final JSONObject e() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        qk2 qk2Var = this.a;
        if (qk2Var != null) {
            jSONObject.put("direct", qk2Var.e());
        }
        qk2 qk2Var2 = this.b;
        if (qk2Var2 != null) {
            jSONObject.put("indirect", qk2Var2.e());
        }
        return jSONObject;
    }

    public String toString() {
        return "OSOutcomeSource{directBody=" + this.a + ", indirectBody=" + this.b + '}';
    }
}
