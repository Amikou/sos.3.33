package defpackage;

import com.google.android.play.core.assetpacks.c;
import java.util.concurrent.Executor;

/* renamed from: wx4  reason: default package */
/* loaded from: classes2.dex */
public final class wx4 implements jw4<ux4> {
    public final jw4<c> a;
    public final jw4<zy4> b;
    public final jw4<au4> c;
    public final jw4<oy4> d;
    public final jw4<zv4> e;
    public final jw4<fv4> f;
    public final jw4<vu4> g;
    public final jw4<Executor> h;
    public final jw4<ws4> i;

    public wx4(jw4<c> jw4Var, jw4<zy4> jw4Var2, jw4<au4> jw4Var3, jw4<oy4> jw4Var4, jw4<zv4> jw4Var5, jw4<fv4> jw4Var6, jw4<vu4> jw4Var7, jw4<Executor> jw4Var8, jw4<ws4> jw4Var9) {
        this.a = jw4Var;
        this.b = jw4Var2;
        this.c = jw4Var3;
        this.d = jw4Var4;
        this.e = jw4Var5;
        this.f = jw4Var6;
        this.g = jw4Var7;
        this.h = jw4Var8;
        this.i = jw4Var9;
    }

    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ ux4 a() {
        return new ux4(this.a.a(), gw4.c(this.b), this.c.a(), this.d.a(), this.e.a(), this.f.a(), this.g.a(), gw4.c(this.h), this.i.a());
    }
}
