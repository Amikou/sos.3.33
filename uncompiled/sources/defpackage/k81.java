package defpackage;

import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import defpackage.o81;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: FontProvider.java */
/* renamed from: k81  reason: default package */
/* loaded from: classes.dex */
public class k81 {
    public static final Comparator<byte[]> a = new a();

    /* compiled from: FontProvider.java */
    /* renamed from: k81$a */
    /* loaded from: classes.dex */
    public class a implements Comparator<byte[]> {
        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            int i;
            int i2;
            if (bArr.length != bArr2.length) {
                i = bArr.length;
                i2 = bArr2.length;
            } else {
                for (int i3 = 0; i3 < bArr.length; i3++) {
                    if (bArr[i3] != bArr2[i3]) {
                        i = bArr[i3];
                        i2 = bArr2[i3];
                    }
                }
                return 0;
            }
            return i - i2;
        }
    }

    public static List<byte[]> a(Signature[] signatureArr) {
        ArrayList arrayList = new ArrayList();
        for (Signature signature : signatureArr) {
            arrayList.add(signature.toByteArray());
        }
        return arrayList;
    }

    public static boolean b(List<byte[]> list, List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!Arrays.equals(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static List<List<byte[]>> c(l81 l81Var, Resources resources) {
        if (l81Var.b() != null) {
            return l81Var.b();
        }
        return n81.c(resources, l81Var.c());
    }

    public static o81.a d(Context context, l81 l81Var, CancellationSignal cancellationSignal) throws PackageManager.NameNotFoundException {
        ProviderInfo e = e(context.getPackageManager(), l81Var, context.getResources());
        if (e == null) {
            return o81.a.a(1, null);
        }
        return o81.a.a(0, f(context, l81Var, e.authority, cancellationSignal));
    }

    public static ProviderInfo e(PackageManager packageManager, l81 l81Var, Resources resources) throws PackageManager.NameNotFoundException {
        String e = l81Var.e();
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(e, 0);
        if (resolveContentProvider != null) {
            if (resolveContentProvider.packageName.equals(l81Var.f())) {
                List<byte[]> a2 = a(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
                Collections.sort(a2, a);
                List<List<byte[]>> c = c(l81Var, resources);
                for (int i = 0; i < c.size(); i++) {
                    ArrayList arrayList = new ArrayList(c.get(i));
                    Collections.sort(arrayList, a);
                    if (b(a2, arrayList)) {
                        return resolveContentProvider;
                    }
                }
                return null;
            }
            throw new PackageManager.NameNotFoundException("Found content provider " + e + ", but package was not " + l81Var.f());
        }
        throw new PackageManager.NameNotFoundException("No package found for authority: " + e);
    }

    public static o81.b[] f(Context context, l81 l81Var, String str, CancellationSignal cancellationSignal) {
        int i;
        Uri withAppendedId;
        boolean z;
        int i2;
        ArrayList arrayList = new ArrayList();
        Uri build = new Uri.Builder().scheme(PublicResolver.FUNC_CONTENT).authority(str).build();
        Uri build2 = new Uri.Builder().scheme(PublicResolver.FUNC_CONTENT).authority(str).appendPath("file").build();
        Cursor cursor = null;
        try {
            String[] strArr = {"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"};
            int i3 = 0;
            if (Build.VERSION.SDK_INT > 16) {
                cursor = context.getContentResolver().query(build, strArr, "query = ?", new String[]{l81Var.g()}, null, cancellationSignal);
            } else {
                cursor = context.getContentResolver().query(build, strArr, "query = ?", new String[]{l81Var.g()}, null);
            }
            if (cursor != null && cursor.getCount() > 0) {
                int columnIndex = cursor.getColumnIndex("result_code");
                ArrayList arrayList2 = new ArrayList();
                int columnIndex2 = cursor.getColumnIndex("_id");
                int columnIndex3 = cursor.getColumnIndex("file_id");
                int columnIndex4 = cursor.getColumnIndex("font_ttc_index");
                int columnIndex5 = cursor.getColumnIndex("font_weight");
                int columnIndex6 = cursor.getColumnIndex("font_italic");
                while (cursor.moveToNext()) {
                    int i4 = columnIndex != -1 ? cursor.getInt(columnIndex) : i3;
                    int i5 = columnIndex4 != -1 ? cursor.getInt(columnIndex4) : i3;
                    if (columnIndex3 == -1) {
                        i = i4;
                        withAppendedId = ContentUris.withAppendedId(build, cursor.getLong(columnIndex2));
                    } else {
                        i = i4;
                        withAppendedId = ContentUris.withAppendedId(build2, cursor.getLong(columnIndex3));
                    }
                    int i6 = columnIndex5 != -1 ? cursor.getInt(columnIndex5) : 400;
                    if (columnIndex6 == -1 || cursor.getInt(columnIndex6) != 1) {
                        z = false;
                        i2 = i;
                    } else {
                        i2 = i;
                        z = true;
                    }
                    arrayList2.add(o81.b.a(withAppendedId, i5, i6, z, i2));
                    i3 = 0;
                }
                arrayList = arrayList2;
            }
            return (o81.b[]) arrayList.toArray(new o81.b[0]);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
