package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: Callback.java */
/* renamed from: uu  reason: default package */
/* loaded from: classes3.dex */
public abstract class uu<E> {
    private boolean canceled = false;

    /* compiled from: Callback.java */
    /* renamed from: uu$a */
    /* loaded from: classes3.dex */
    public class a implements Runnable {
        public final /* synthetic */ Object a;

        public a(Object obj) {
            this.a = obj;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Runnable
        public void run() {
            uu.this.success(this.a);
        }
    }

    public void cancel() {
        this.canceled = true;
    }

    public void internalSuccess(E e) {
        if (this.canceled) {
            return;
        }
        new Handler(Looper.getMainLooper()).post(new a(e));
    }

    public abstract void success(E e);
}
