package defpackage;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: TextWithDelayAfterTextChangedWatcher.kt */
/* renamed from: z44  reason: default package */
/* loaded from: classes2.dex */
public final class z44 implements TextWatcher {
    public final TextWatcher a;
    public final long f0;
    public final Handler g0;
    public Runnable h0;

    public z44(TextWatcher textWatcher, long j) {
        fs1.f(textWatcher, "origin");
        this.a = textWatcher;
        this.f0 = j;
        this.g0 = new Handler(Looper.getMainLooper());
        this.h0 = y44.a;
    }

    public static final void c(z44 z44Var, Editable editable) {
        fs1.f(z44Var, "this$0");
        z44Var.a.afterTextChanged(editable);
    }

    public static final void d() {
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(final Editable editable) {
        this.g0.removeCallbacks(this.h0);
        Runnable runnable = new Runnable() { // from class: x44
            @Override // java.lang.Runnable
            public final void run() {
                z44.c(z44.this, editable);
            }
        };
        this.h0 = runnable;
        this.g0.postDelayed(runnable, this.f0);
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        fs1.f(charSequence, "s");
        this.a.beforeTextChanged(charSequence, i, i2, i3);
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        fs1.f(charSequence, "s");
        this.a.onTextChanged(charSequence, i, i2, i3);
    }

    public /* synthetic */ z44(TextWatcher textWatcher, long j, int i, qi0 qi0Var) {
        this(textWatcher, (i & 2) != 0 ? 100L : j);
    }
}
