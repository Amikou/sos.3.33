package defpackage;

import android.content.Context;
import android.util.Log;
import defpackage.x50;

/* compiled from: DefaultConnectivityMonitorFactory.java */
/* renamed from: pi0  reason: default package */
/* loaded from: classes.dex */
public class pi0 implements y50 {
    @Override // defpackage.y50
    public x50 a(Context context, x50.a aVar) {
        boolean z = m70.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        Log.isLoggable("ConnectivityMonitor", 3);
        if (z) {
            return new oi0(context, aVar);
        }
        return new ti2();
    }
}
