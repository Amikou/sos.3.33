package defpackage;

import android.view.View;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogV2HelpBinding.java */
/* renamed from: oo0  reason: default package */
/* loaded from: classes2.dex */
public final class oo0 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final MaterialButton c;

    public oo0(MaterialCardView materialCardView, MaterialButton materialButton, MaterialButton materialButton2, MaterialCardView materialCardView2, MaterialButton materialButton3, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5, MaterialTextView materialTextView6, MaterialTextView materialTextView7, MaterialTextView materialTextView8, MaterialTextView materialTextView9, MaterialTextView materialTextView10, MaterialTextView materialTextView11, MaterialTextView materialTextView12) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = materialButton3;
    }

    public static oo0 a(View view) {
        int i = R.id.btnConsolidateNow;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConsolidateNow);
        if (materialButton != null) {
            i = R.id.btnDontShowMe;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnDontShowMe);
            if (materialButton2 != null) {
                MaterialCardView materialCardView = (MaterialCardView) view;
                i = R.id.dialogCross;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.dialogCross);
                if (materialButton3 != null) {
                    i = R.id.txtAboutV2Describe;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAboutV2Describe);
                    if (materialTextView != null) {
                        i = R.id.txtAboutV2Note;
                        MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtAboutV2Note);
                        if (materialTextView2 != null) {
                            i = R.id.txtAboutV2Title;
                            MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtAboutV2Title);
                            if (materialTextView3 != null) {
                                i = R.id.txtAllSet;
                                MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtAllSet);
                                if (materialTextView4 != null) {
                                    i = R.id.txtAllSetDescribe;
                                    MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txtAllSetDescribe);
                                    if (materialTextView5 != null) {
                                        i = R.id.txtFooterTitle;
                                        MaterialTextView materialTextView6 = (MaterialTextView) ai4.a(view, R.id.txtFooterTitle);
                                        if (materialTextView6 != null) {
                                            i = R.id.txtHeaderDescribe;
                                            MaterialTextView materialTextView7 = (MaterialTextView) ai4.a(view, R.id.txtHeaderDescribe);
                                            if (materialTextView7 != null) {
                                                i = R.id.txtHeaderTitle;
                                                MaterialTextView materialTextView8 = (MaterialTextView) ai4.a(view, R.id.txtHeaderTitle);
                                                if (materialTextView8 != null) {
                                                    i = R.id.txtStep1V2Title;
                                                    MaterialTextView materialTextView9 = (MaterialTextView) ai4.a(view, R.id.txtStep1V2Title);
                                                    if (materialTextView9 != null) {
                                                        i = R.id.txtStepV1Describe;
                                                        MaterialTextView materialTextView10 = (MaterialTextView) ai4.a(view, R.id.txtStepV1Describe);
                                                        if (materialTextView10 != null) {
                                                            i = R.id.txtStepV2Describe;
                                                            MaterialTextView materialTextView11 = (MaterialTextView) ai4.a(view, R.id.txtStepV2Describe);
                                                            if (materialTextView11 != null) {
                                                                i = R.id.txtStepV3Describe;
                                                                MaterialTextView materialTextView12 = (MaterialTextView) ai4.a(view, R.id.txtStepV3Describe);
                                                                if (materialTextView12 != null) {
                                                                    return new oo0(materialCardView, materialButton, materialButton2, materialCardView, materialButton3, materialTextView, materialTextView2, materialTextView3, materialTextView4, materialTextView5, materialTextView6, materialTextView7, materialTextView8, materialTextView9, materialTextView10, materialTextView11, materialTextView12);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
