package defpackage;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: vy5  reason: default package */
/* loaded from: classes.dex */
public final class vy5 {
    public static final Iterator<Object> a = new ry5();
    public static final Iterable<Object> b = new ty5();

    public static <T> Iterable<T> a() {
        return (Iterable<T>) b;
    }
}
