package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.button.MaterialButton;
import java.util.Objects;
import net.safemoon.androidwallet.R;

/* compiled from: ItemSimpleTextButtonBinding.java */
/* renamed from: ht1  reason: default package */
/* loaded from: classes2.dex */
public final class ht1 {
    public final MaterialButton a;
    public final MaterialButton b;

    public ht1(MaterialButton materialButton, MaterialButton materialButton2) {
        this.a = materialButton;
        this.b = materialButton2;
    }

    public static ht1 a(View view) {
        Objects.requireNonNull(view, "rootView");
        MaterialButton materialButton = (MaterialButton) view;
        return new ht1(materialButton, materialButton);
    }

    public static ht1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_simple_text_button, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public MaterialButton b() {
        return this.a;
    }
}
