package defpackage;

import com.google.android.gms.internal.clearcut.zzdi;
import java.util.Iterator;
import java.util.Map;

/* renamed from: yd5  reason: default package */
/* loaded from: classes.dex */
public final class yd5 implements wd5 {
    @Override // defpackage.wd5
    public final sd5<?, ?> a(Object obj) {
        throw new NoSuchMethodError();
    }

    @Override // defpackage.wd5
    public final Object b(Object obj) {
        return zzdi.zzbz().zzca();
    }

    @Override // defpackage.wd5
    public final Object c(Object obj, Object obj2) {
        zzdi zzdiVar = (zzdi) obj;
        zzdi zzdiVar2 = (zzdi) obj2;
        if (!zzdiVar2.isEmpty()) {
            if (!zzdiVar.isMutable()) {
                zzdiVar = zzdiVar.zzca();
            }
            zzdiVar.zza(zzdiVar2);
        }
        return zzdiVar;
    }

    @Override // defpackage.wd5
    public final int d(int i, Object obj, Object obj2) {
        zzdi zzdiVar = (zzdi) obj;
        if (zzdiVar.isEmpty()) {
            return 0;
        }
        Iterator it = zzdiVar.entrySet().iterator();
        if (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            entry.getKey();
            entry.getValue();
            throw new NoSuchMethodError();
        }
        return 0;
    }

    @Override // defpackage.wd5
    public final Object e(Object obj) {
        ((zzdi) obj).zzv();
        return obj;
    }

    @Override // defpackage.wd5
    public final boolean f(Object obj) {
        return !((zzdi) obj).isMutable();
    }

    @Override // defpackage.wd5
    public final Map<?, ?> g(Object obj) {
        return (zzdi) obj;
    }

    @Override // defpackage.wd5
    public final Map<?, ?> h(Object obj) {
        return (zzdi) obj;
    }
}
