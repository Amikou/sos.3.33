package defpackage;

import android.os.Bundle;
import com.google.android.gms.tasks.b;
import com.google.android.gms.tasks.c;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: p56  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class p56 implements b {
    public static final b a = new p56();

    @Override // com.google.android.gms.tasks.b
    public final c a(Object obj) {
        return com.google.android.gms.cloudmessaging.b.c((Bundle) obj);
    }
}
