package defpackage;

import android.content.Context;
import com.google.android.datatransport.runtime.backends.d;

/* compiled from: CreationContextFactory.java */
/* renamed from: ka0  reason: default package */
/* loaded from: classes.dex */
public class ka0 {
    public final Context a;
    public final qz b;
    public final qz c;

    public ka0(Context context, qz qzVar, qz qzVar2) {
        this.a = context;
        this.b = qzVar;
        this.c = qzVar2;
    }

    public d a(String str) {
        return d.a(this.a, this.b, this.c, str);
    }
}
