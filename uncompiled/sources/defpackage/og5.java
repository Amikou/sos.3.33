package defpackage;

import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.measurement.internal.h;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: og5  reason: default package */
/* loaded from: classes.dex */
public final class og5 extends sl5 {
    public char c;
    public long d;
    public String e;
    public final jg5 f;
    public final jg5 g;
    public final jg5 h;
    public final jg5 i;
    public final jg5 j;
    public final jg5 k;
    public final jg5 l;
    public final jg5 m;
    public final jg5 n;

    public og5(ck5 ck5Var) {
        super(ck5Var);
        this.c = (char) 0;
        this.d = -1L;
        this.f = new jg5(this, 6, false, false);
        this.g = new jg5(this, 6, true, false);
        this.h = new jg5(this, 6, false, true);
        this.i = new jg5(this, 5, false, false);
        this.j = new jg5(this, 5, true, false);
        this.k = new jg5(this, 5, false, true);
        this.l = new jg5(this, 4, false, false);
        this.m = new jg5(this, 3, false, false);
        this.n = new jg5(this, 2, false, false);
    }

    public static String A(boolean z, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = "";
        }
        String B = B(z, obj);
        String B2 = B(z, obj2);
        String B3 = B(z, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        String str3 = ", ";
        if (!TextUtils.isEmpty(B)) {
            sb.append(str2);
            sb.append(B);
            str2 = ", ";
        }
        if (TextUtils.isEmpty(B2)) {
            str3 = str2;
        } else {
            sb.append(str2);
            sb.append(B2);
        }
        if (!TextUtils.isEmpty(B3)) {
            sb.append(str3);
            sb.append(B3);
        }
        return sb.toString();
    }

    public static String B(boolean z, Object obj) {
        String str;
        String className;
        if (obj == null) {
            return "";
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf(((Integer) obj).intValue());
        }
        int i = 0;
        if (obj instanceof Long) {
            if (!z) {
                return String.valueOf(obj);
            }
            Long l = (Long) obj;
            if (Math.abs(l.longValue()) < 100) {
                return String.valueOf(obj);
            }
            String str2 = String.valueOf(obj).charAt(0) == '-' ? "-" : "";
            String valueOf = String.valueOf(Math.abs(l.longValue()));
            long round = Math.round(Math.pow(10.0d, valueOf.length() - 1));
            long round2 = Math.round(Math.pow(10.0d, valueOf.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(str2.length() + 43 + str2.length());
            sb.append(str2);
            sb.append(round);
            sb.append("...");
            sb.append(str2);
            sb.append(round2);
            return sb.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (obj instanceof Throwable) {
                Throwable th = (Throwable) obj;
                StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
                String G = G(ck5.class.getCanonicalName());
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i];
                    if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null && G(className).equals(G)) {
                        sb2.append(": ");
                        sb2.append(stackTraceElement);
                        break;
                    }
                    i++;
                }
                return sb2.toString();
            } else if (!(obj instanceof lg5)) {
                return z ? "-" : String.valueOf(obj);
            } else {
                str = ((lg5) obj).a;
                return str;
            }
        }
    }

    public static String G(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf == -1 ? str : str.substring(0, lastIndexOf);
    }

    public static Object x(String str) {
        if (str == null) {
            return null;
        }
        return new lg5(str);
    }

    @Override // defpackage.sl5
    public final boolean f() {
        return false;
    }

    public final jg5 l() {
        return this.f;
    }

    public final jg5 n() {
        return this.g;
    }

    public final jg5 o() {
        return this.h;
    }

    public final jg5 p() {
        return this.i;
    }

    public final jg5 r() {
        return this.j;
    }

    public final jg5 s() {
        return this.k;
    }

    public final jg5 t() {
        return this.l;
    }

    public final jg5 u() {
        return this.m;
    }

    public final jg5 v() {
        return this.n;
    }

    public final void y(int i, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && Log.isLoggable(z(), i)) {
            Log.println(i, z(), A(false, str, obj, obj2, obj3));
        }
        if (z2 || i < 5) {
            return;
        }
        zt2.j(str);
        h E = this.a.E();
        if (E == null) {
            Log.println(6, z(), "Scheduler not set. Not logging error/warn");
        } else if (!E.h()) {
            Log.println(6, z(), "Scheduler not initialized. Not logging error/warn");
        } else {
            if (i >= 9) {
                i = 8;
            }
            E.p(new gg5(this, i, str, obj, obj2, obj3));
        }
    }

    public final String z() {
        String str;
        synchronized (this) {
            if (this.e == null) {
                if (this.a.N() != null) {
                    this.e = this.a.N();
                } else {
                    this.e = this.a.z().i();
                }
            }
            zt2.j(this.e);
            str = this.e;
        }
        return str;
    }
}
