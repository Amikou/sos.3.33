package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.HashMap;
import java.util.Map;

/* renamed from: fv4  reason: default package */
/* loaded from: classes2.dex */
public final class fv4 {
    public final Map<String, Double> a = new HashMap();

    public final synchronized void a(String str) {
        this.a.put(str, Double.valueOf((double) Utils.DOUBLE_EPSILON));
    }

    public final synchronized double b(String str) {
        Double d = this.a.get(str);
        if (d == null) {
            return Utils.DOUBLE_EPSILON;
        }
        return d.doubleValue();
    }

    public final synchronized double c(String str, fw4 fw4Var) {
        double d;
        d = (((bv4) fw4Var).g + 1.0d) / ((bv4) fw4Var).h;
        this.a.put(str, Double.valueOf(d));
        return d;
    }
}
