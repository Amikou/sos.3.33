package defpackage;

import java.util.Collections;
import java.util.List;

/* compiled from: Period.java */
/* renamed from: jq2  reason: default package */
/* loaded from: classes.dex */
public class jq2 {
    public final String a;
    public final long b;
    public final List<i8> c;
    public final List<jy0> d;

    public jq2(String str, long j, List<i8> list, List<jy0> list2) {
        this(str, j, list, list2, null);
    }

    public int a(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.c.get(i2).b == i) {
                return i2;
            }
        }
        return -1;
    }

    public jq2(String str, long j, List<i8> list, List<jy0> list2, nm0 nm0Var) {
        this.a = str;
        this.b = j;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
    }
}
