package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.n;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.d;
import com.google.crypto.tink.proto.e;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import java.security.GeneralSecurityException;

/* compiled from: AesCtrHmacAeadKeyManager.java */
/* renamed from: fa  reason: default package */
/* loaded from: classes2.dex */
public final class fa extends g<d> {

    /* compiled from: AesCtrHmacAeadKeyManager.java */
    /* renamed from: fa$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, d> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(d dVar) throws GeneralSecurityException {
            return new iv0((gq1) new ha().d(dVar.H(), gq1.class), (n) new rk1().d(dVar.I(), n.class), dVar.I().J().G());
        }
    }

    /* compiled from: AesCtrHmacAeadKeyManager.java */
    /* renamed from: fa$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<e, d> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public d a(e eVar) throws GeneralSecurityException {
            return d.K().s(new ha().e().a(eVar.D())).t(new rk1().e().a(eVar.E())).u(fa.this.j()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public e c(ByteString byteString) throws InvalidProtocolBufferException {
            return e.G(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(e eVar) throws GeneralSecurityException {
            new ha().e().d(eVar.D());
            new rk1().e().d(eVar.E());
            ug4.a(eVar.D().E());
        }
    }

    public fa() {
        super(d.class, new a(com.google.crypto.tink.a.class));
    }

    public static void l(boolean z) throws GeneralSecurityException {
        q.q(new fa(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, d> e() {
        return new b(e.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int j() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: k */
    public d g(ByteString byteString) throws InvalidProtocolBufferException {
        return d.L(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(d dVar) throws GeneralSecurityException {
        ug4.c(dVar.J(), j());
        new ha().i(dVar.H());
        new rk1().i(dVar.I());
    }
}
