package defpackage;

import com.google.android.gms.internal.clearcut.zzcd;
import com.google.android.gms.internal.clearcut.zzcq;

/* renamed from: wa5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class wa5 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[zzcq.values().length];
        b = iArr;
        try {
            iArr[zzcq.zzle.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            b[zzcq.zzlg.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            b[zzcq.zzld.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        int[] iArr2 = new int[zzcd.values().length];
        a = iArr2;
        try {
            iArr2[zzcd.MAP.ordinal()] = 1;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            a[zzcd.VECTOR.ordinal()] = 2;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            a[zzcd.SCALAR.ordinal()] = 3;
        } catch (NoSuchFieldError unused6) {
        }
    }
}
