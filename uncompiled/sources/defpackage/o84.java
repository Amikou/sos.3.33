package defpackage;

import java.math.BigInteger;

/* compiled from: Transaction.java */
/* renamed from: o84  reason: default package */
/* loaded from: classes3.dex */
public class o84 {
    private static final int CHAIN_ID_INC = 35;
    private static final int LOWER_REAL_V = 27;
    private String blockHash;
    private String blockNumber;
    private String creates;
    private String from;
    private String gas;
    private String gasPrice;
    private String hash;
    private String input;
    private String nonce;
    private String publicKey;
    private String r;
    private String raw;
    private String s;
    private String to;
    private String transactionIndex;
    private long v;
    private String value;

    public o84() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof o84) {
            o84 o84Var = (o84) obj;
            if (getV() != o84Var.getV()) {
                return false;
            }
            if (getHash() == null ? o84Var.getHash() == null : getHash().equals(o84Var.getHash())) {
                if (getNonceRaw() == null ? o84Var.getNonceRaw() == null : getNonceRaw().equals(o84Var.getNonceRaw())) {
                    if (getBlockHash() == null ? o84Var.getBlockHash() == null : getBlockHash().equals(o84Var.getBlockHash())) {
                        if (getBlockNumberRaw() == null ? o84Var.getBlockNumberRaw() == null : getBlockNumberRaw().equals(o84Var.getBlockNumberRaw())) {
                            if (getTransactionIndexRaw() == null ? o84Var.getTransactionIndexRaw() == null : getTransactionIndexRaw().equals(o84Var.getTransactionIndexRaw())) {
                                if (getFrom() == null ? o84Var.getFrom() == null : getFrom().equals(o84Var.getFrom())) {
                                    if (getTo() == null ? o84Var.getTo() == null : getTo().equals(o84Var.getTo())) {
                                        if (getValueRaw() == null ? o84Var.getValueRaw() == null : getValueRaw().equals(o84Var.getValueRaw())) {
                                            if (getGasPriceRaw() == null ? o84Var.getGasPriceRaw() == null : getGasPriceRaw().equals(o84Var.getGasPriceRaw())) {
                                                if (getGasRaw() == null ? o84Var.getGasRaw() == null : getGasRaw().equals(o84Var.getGasRaw())) {
                                                    if (getInput() == null ? o84Var.getInput() == null : getInput().equals(o84Var.getInput())) {
                                                        if (getCreates() == null ? o84Var.getCreates() == null : getCreates().equals(o84Var.getCreates())) {
                                                            if (getPublicKey() == null ? o84Var.getPublicKey() == null : getPublicKey().equals(o84Var.getPublicKey())) {
                                                                if (getRaw() == null ? o84Var.getRaw() == null : getRaw().equals(o84Var.getRaw())) {
                                                                    if (getR() == null ? o84Var.getR() == null : getR().equals(o84Var.getR())) {
                                                                        return getS() != null ? getS().equals(o84Var.getS()) : o84Var.getS() == null;
                                                                    }
                                                                    return false;
                                                                }
                                                                return false;
                                                            }
                                                            return false;
                                                        }
                                                        return false;
                                                    }
                                                    return false;
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public String getBlockHash() {
        return this.blockHash;
    }

    public BigInteger getBlockNumber() {
        return ej2.decodeQuantity(this.blockNumber);
    }

    public String getBlockNumberRaw() {
        return this.blockNumber;
    }

    public Long getChainId() {
        long j = this.v;
        if (j == 27 || j == 28) {
            return null;
        }
        return Long.valueOf((j - 35) / 2);
    }

    public String getCreates() {
        return this.creates;
    }

    public String getFrom() {
        return this.from;
    }

    public BigInteger getGas() {
        return ej2.decodeQuantity(this.gas);
    }

    public BigInteger getGasPrice() {
        return ej2.decodeQuantity(this.gasPrice);
    }

    public String getGasPriceRaw() {
        return this.gasPrice;
    }

    public String getGasRaw() {
        return this.gas;
    }

    public String getHash() {
        return this.hash;
    }

    public String getInput() {
        return this.input;
    }

    public BigInteger getNonce() {
        return ej2.decodeQuantity(this.nonce);
    }

    public String getNonceRaw() {
        return this.nonce;
    }

    public String getPublicKey() {
        return this.publicKey;
    }

    public String getR() {
        return this.r;
    }

    public String getRaw() {
        return this.raw;
    }

    public String getS() {
        return this.s;
    }

    public String getTo() {
        return this.to;
    }

    public BigInteger getTransactionIndex() {
        return ej2.decodeQuantity(this.transactionIndex);
    }

    public String getTransactionIndexRaw() {
        return this.transactionIndex;
    }

    public long getV() {
        return this.v;
    }

    public BigInteger getValue() {
        return ej2.decodeQuantity(this.value);
    }

    public String getValueRaw() {
        return this.value;
    }

    public int hashCode() {
        return ((((((((((((((((((((((((((((((((getHash() != null ? getHash().hashCode() : 0) * 31) + (getNonceRaw() != null ? getNonceRaw().hashCode() : 0)) * 31) + (getBlockHash() != null ? getBlockHash().hashCode() : 0)) * 31) + (getBlockNumberRaw() != null ? getBlockNumberRaw().hashCode() : 0)) * 31) + (getTransactionIndexRaw() != null ? getTransactionIndexRaw().hashCode() : 0)) * 31) + (getFrom() != null ? getFrom().hashCode() : 0)) * 31) + (getTo() != null ? getTo().hashCode() : 0)) * 31) + (getValueRaw() != null ? getValueRaw().hashCode() : 0)) * 31) + (getGasPriceRaw() != null ? getGasPriceRaw().hashCode() : 0)) * 31) + (getGasRaw() != null ? getGasRaw().hashCode() : 0)) * 31) + (getInput() != null ? getInput().hashCode() : 0)) * 31) + (getCreates() != null ? getCreates().hashCode() : 0)) * 31) + (getPublicKey() != null ? getPublicKey().hashCode() : 0)) * 31) + (getRaw() != null ? getRaw().hashCode() : 0)) * 31) + (getR() != null ? getR().hashCode() : 0)) * 31) + (getS() != null ? getS().hashCode() : 0)) * 31) + BigInteger.valueOf(getV()).hashCode();
    }

    public void setBlockHash(String str) {
        this.blockHash = str;
    }

    public void setBlockNumber(String str) {
        this.blockNumber = str;
    }

    public void setCreates(String str) {
        this.creates = str;
    }

    public void setFrom(String str) {
        this.from = str;
    }

    public void setGas(String str) {
        this.gas = str;
    }

    public void setGasPrice(String str) {
        this.gasPrice = str;
    }

    public void setHash(String str) {
        this.hash = str;
    }

    public void setInput(String str) {
        this.input = str;
    }

    public void setNonce(String str) {
        this.nonce = str;
    }

    public void setPublicKey(String str) {
        this.publicKey = str;
    }

    public void setR(String str) {
        this.r = str;
    }

    public void setRaw(String str) {
        this.raw = str;
    }

    public void setS(String str) {
        this.s = str;
    }

    public void setTo(String str) {
        this.to = str;
    }

    public void setTransactionIndex(String str) {
        this.transactionIndex = str;
    }

    public void setV(Object obj) {
        if (obj instanceof String) {
            this.v = m30.longValueExact(ej2.toBigInt((String) obj));
        } else if (obj instanceof Integer) {
            this.v = ((Integer) obj).longValue();
        } else {
            this.v = ((Long) obj).longValue();
        }
    }

    public void setValue(String str) {
        this.value = str;
    }

    public o84(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, long j) {
        this.hash = str;
        this.nonce = str2;
        this.blockHash = str3;
        this.blockNumber = str4;
        this.transactionIndex = str5;
        this.from = str6;
        this.to = str7;
        this.value = str8;
        this.gasPrice = str10;
        this.gas = str9;
        this.input = str11;
        this.creates = str12;
        this.publicKey = str13;
        this.raw = str14;
        this.r = str15;
        this.s = str16;
        this.v = j;
    }
}
