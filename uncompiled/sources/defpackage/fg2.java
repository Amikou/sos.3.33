package defpackage;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.d;
import androidx.recyclerview.widget.o;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.collectible.RoomNFT;
import net.safemoon.androidwallet.views.CustomVideoPlayer;

/* compiled from: NftsAdapter.kt */
/* renamed from: fg2  reason: default package */
/* loaded from: classes2.dex */
public final class fg2 extends o<RoomNFT, a> {
    public final tc1<RoomNFT, te4> a;
    public final d<RoomNFT> b;

    /* compiled from: NftsAdapter.kt */
    /* renamed from: fg2$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final ws1 a;
        public final /* synthetic */ fg2 b;

        /* compiled from: NftsAdapter.kt */
        /* renamed from: fg2$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0172a extends en<ao1> {
            public final /* synthetic */ ws1 b;
            public final /* synthetic */ RoomNFT c;

            /* compiled from: NftsAdapter.kt */
            /* renamed from: fg2$a$a$a  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C0173a implements j73<Drawable> {
                public final /* synthetic */ ws1 a;
                public final /* synthetic */ RoomNFT f0;

                public C0173a(ws1 ws1Var, RoomNFT roomNFT) {
                    this.a = ws1Var;
                    this.f0 = roomNFT;
                }

                @Override // defpackage.j73
                /* renamed from: a */
                public boolean n(Drawable drawable, Object obj, i34<Drawable> i34Var, DataSource dataSource, boolean z) {
                    return false;
                }

                @Override // defpackage.j73
                public boolean i(GlideException glideException, Object obj, i34<Drawable> i34Var, boolean z) {
                    CustomVideoPlayer customVideoPlayer = this.a.g;
                    fs1.e(customVideoPlayer, "videoPlayer");
                    customVideoPlayer.setVisibility(0);
                    this.a.g.setPlayURL(this.f0.getImage_preview_url());
                    return false;
                }
            }

            public C0172a(ws1 ws1Var, RoomNFT roomNFT) {
                this.b = ws1Var;
                this.c = roomNFT;
            }

            @Override // defpackage.en, defpackage.m80
            public void c(String str, Throwable th) {
                super.c(str, th);
                ImageView imageView = this.b.d;
                fs1.e(imageView, "nftIcon");
                imageView.setVisibility(0);
                SimpleDraweeView simpleDraweeView = this.b.f;
                fs1.e(simpleDraweeView, "sdvImage");
                simpleDraweeView.setVisibility(4);
                k73 u = com.bumptech.glide.a.u(this.b.d);
                String image_preview_url = this.c.getImage_preview_url();
                fs1.d(image_preview_url);
                Objects.requireNonNull(image_preview_url, "null cannot be cast to non-null type kotlin.CharSequence");
                u.y(StringsKt__StringsKt.K0(image_preview_url).toString()).l(R.drawable.safemoon).d().u0(new C0173a(this.b, this.c)).I0(this.b.d);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(fg2 fg2Var, ws1 ws1Var) {
            super(ws1Var.b());
            fs1.f(fg2Var, "this$0");
            fs1.f(ws1Var, "itemCollectible");
            this.b = fg2Var;
            this.a = ws1Var;
        }

        public static final void c(fg2 fg2Var, RoomNFT roomNFT, View view) {
            fs1.f(fg2Var, "this$0");
            fs1.f(roomNFT, "$item");
            fg2Var.a().invoke(roomNFT);
        }

        /* JADX WARN: Removed duplicated region for block: B:13:0x0026  */
        /* JADX WARN: Removed duplicated region for block: B:14:0x0073  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final void b(final net.safemoon.androidwallet.model.collectible.RoomNFT r9) {
            /*
                r8 = this;
                java.lang.String r0 = "item"
                defpackage.fs1.f(r9, r0)
                ws1 r0 = r8.a
                fg2 r1 = r8.b
                java.lang.String r2 = r9.getImage_preview_url()
                r3 = 1
                r4 = 0
                if (r2 != 0) goto L13
            L11:
                r2 = r4
                goto L1f
            L13:
                int r2 = r2.length()
                if (r2 <= 0) goto L1b
                r2 = r3
                goto L1c
            L1b:
                r2 = r4
            L1c:
                if (r2 != r3) goto L11
                r2 = r3
            L1f:
                java.lang.String r5 = "nftIcon"
                r6 = 4
                java.lang.String r7 = "sdvImage"
                if (r2 == 0) goto L73
                android.widget.ImageView r2 = r0.d
                defpackage.fs1.e(r2, r5)
                r2.setVisibility(r6)
                com.facebook.drawee.view.SimpleDraweeView r2 = r0.f
                defpackage.fs1.e(r2, r7)
                r2.setVisibility(r4)
                com.facebook.drawee.view.SimpleDraweeView r2 = r0.f
                xq2 r4 = defpackage.kc1.e()
                java.lang.String r5 = r9.getImage_preview_url()
                defpackage.fs1.d(r5)
                java.lang.String r6 = "null cannot be cast to non-null type kotlin.CharSequence"
                java.util.Objects.requireNonNull(r5, r6)
                java.lang.CharSequence r5 = kotlin.text.StringsKt__StringsKt.K0(r5)
                java.lang.String r5 = r5.toString()
                com.facebook.imagepipeline.request.ImageRequest r5 = com.facebook.imagepipeline.request.ImageRequest.b(r5)
                com.facebook.drawee.controller.AbstractDraweeControllerBuilder r4 = r4.B(r5)
                xq2 r4 = (defpackage.xq2) r4
                com.facebook.drawee.controller.AbstractDraweeControllerBuilder r3 = r4.y(r3)
                xq2 r3 = (defpackage.xq2) r3
                fg2$a$a r4 = new fg2$a$a
                r4.<init>(r0, r9)
                com.facebook.drawee.controller.AbstractDraweeControllerBuilder r3 = r3.A(r4)
                xq2 r3 = (defpackage.xq2) r3
                w4 r3 = r3.build()
                r2.setController(r3)
                goto Lc0
            L73:
                com.facebook.drawee.view.SimpleDraweeView r2 = r0.f
                defpackage.fs1.e(r2, r7)
                r2.setVisibility(r6)
                android.widget.ImageView r2 = r0.d
                defpackage.fs1.e(r2, r5)
                r2.setVisibility(r4)
                java.lang.String r2 = r9.getImage_data()
                r3 = 2131231295(0x7f08023f, float:1.8078667E38)
                if (r2 == 0) goto Lad
                android.widget.ImageView r2 = r0.d
                k73 r2 = com.bumptech.glide.a.u(r2)
                java.lang.String r4 = r9.getImage_data()
                defpackage.fs1.d(r4)
                byte[] r4 = defpackage.mu3.a(r4)
                com.bumptech.glide.e r2 = r2.z(r4)
                com.bumptech.glide.request.a r2 = r2.l(r3)
                com.bumptech.glide.e r2 = (com.bumptech.glide.e) r2
                android.widget.ImageView r3 = r0.d
                r2.I0(r3)
                goto Lc0
            Lad:
                android.widget.ImageView r2 = r0.d
                k73 r2 = com.bumptech.glide.a.u(r2)
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
                com.bumptech.glide.e r2 = r2.w(r3)
                android.widget.ImageView r3 = r0.d
                r2.I0(r3)
            Lc0:
                android.widget.TextView r0 = r0.e
                java.lang.String r2 = r9.getName()
                r0.setText(r2)
                ws1 r0 = r8.a
                androidx.constraintlayout.widget.ConstraintLayout r0 = r0.b()
                eg2 r2 = new eg2
                r2.<init>()
                r0.setOnClickListener(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.fg2.a.b(net.safemoon.androidwallet.model.collectible.RoomNFT):void");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public fg2(defpackage.tc1<? super net.safemoon.androidwallet.model.collectible.RoomNFT, defpackage.te4> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "callBack"
            defpackage.fs1.f(r2, r0)
            gg2$a r0 = defpackage.gg2.a()
            r1.<init>(r0)
            r1.a = r2
            androidx.recyclerview.widget.d r2 = new androidx.recyclerview.widget.d
            gg2$a r0 = defpackage.gg2.a()
            r2.<init>(r1, r0)
            r1.b = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.fg2.<init>(tc1):void");
    }

    public final tc1<RoomNFT, te4> a() {
        return this.a;
    }

    public final d<RoomNFT> b() {
        return this.b;
    }

    public RoomNFT c(int i) {
        RoomNFT roomNFT = this.b.b().get(i);
        fs1.e(roomNFT, "differ.currentList[position]");
        return roomNFT;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        aVar.b(c(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        ws1 c = ws1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        fs1.e(c, "inflate(LayoutInflater.f…arent,\n            false)");
        return new a(this, c);
    }

    @Override // androidx.recyclerview.widget.o, androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.b.b().size();
    }
}
