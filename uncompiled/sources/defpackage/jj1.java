package defpackage;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/* compiled from: HTTP.java */
@Target({ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: jj1  reason: default package */
/* loaded from: classes3.dex */
public @interface jj1 {
    boolean hasBody() default false;

    String method();

    String path() default "";
}
