package defpackage;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;

/* compiled from: ViewUtilsApi23.java */
/* renamed from: sk4  reason: default package */
/* loaded from: classes.dex */
public class sk4 extends rk4 {
    public static boolean k = true;

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public void h(View view, int i) {
        if (Build.VERSION.SDK_INT == 28) {
            super.h(view, i);
        } else if (k) {
            try {
                view.setTransitionVisibility(i);
            } catch (NoSuchMethodError unused) {
                k = false;
            }
        }
    }
}
