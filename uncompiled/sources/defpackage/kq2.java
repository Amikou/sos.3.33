package defpackage;

import android.content.Context;
import android.os.Binder;
import android.os.Process;

/* compiled from: PermissionChecker.java */
/* renamed from: kq2  reason: default package */
/* loaded from: classes.dex */
public final class kq2 {
    public static int a(Context context, String str) {
        return b(context, str, Binder.getCallingPid(), Binder.getCallingUid(), Binder.getCallingPid() == Process.myPid() ? context.getPackageName() : null);
    }

    public static int b(Context context, String str, int i, int i2, String str2) {
        int b;
        if (context.checkPermission(str, i, i2) == -1) {
            return -1;
        }
        String c = uf.c(str);
        if (c == null) {
            return 0;
        }
        if (str2 == null) {
            String[] packagesForUid = context.getPackageManager().getPackagesForUid(i2);
            if (packagesForUid == null || packagesForUid.length <= 0) {
                return -1;
            }
            str2 = packagesForUid[0];
        }
        if (Process.myUid() == i2 && sl2.a(context.getPackageName(), str2)) {
            b = uf.a(context, i2, c, str2);
        } else {
            b = uf.b(context, c, str2);
        }
        return b == 0 ? 0 : -2;
    }

    public static int c(Context context, String str) {
        return b(context, str, Process.myPid(), Process.myUid(), context.getPackageName());
    }
}
