package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: nu5  reason: default package */
/* loaded from: classes.dex */
public final class nu5 extends wn5<Long> implements gt5<Long>, vw5 {
    public long[] f0;
    public int g0;

    static {
        new nu5(new long[0], 0).zzb();
    }

    public nu5() {
        this(new long[10], 0);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        long longValue = ((Long) obj).longValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            long[] jArr = this.f0;
            if (i2 < jArr.length) {
                System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
            } else {
                long[] jArr2 = new long[((i2 * 3) / 2) + 1];
                System.arraycopy(jArr, 0, jArr2, 0, i);
                System.arraycopy(this.f0, i, jArr2, i + 1, this.g0 - i);
                this.f0 = jArr2;
            }
            this.f0[i] = longValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(n(i));
    }

    @Override // defpackage.wn5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Long> collection) {
        e();
        vs5.d(collection);
        if (!(collection instanceof nu5)) {
            return super.addAll(collection);
        }
        nu5 nu5Var = (nu5) collection;
        int i = nu5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.f0;
            if (i3 > jArr.length) {
                this.f0 = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(nu5Var.f0, 0, this.f0, this.g0, nu5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.gt5
    public final /* synthetic */ gt5<Long> d(int i) {
        if (i >= this.g0) {
            return new nu5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof nu5)) {
            return super.equals(obj);
        }
        nu5 nu5Var = (nu5) obj;
        if (this.g0 != nu5Var.g0) {
            return false;
        }
        long[] jArr = nu5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(k(i));
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + vs5.b(this.f0[i2]);
        }
        return i;
    }

    public final void i(long j) {
        e();
        int i = this.g0;
        long[] jArr = this.f0;
        if (i == jArr.length) {
            long[] jArr2 = new long[((i * 3) / 2) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.f0 = jArr2;
        }
        long[] jArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        jArr3[i2] = j;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Long) {
            long longValue = ((Long) obj).longValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == longValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    public final long k(int i) {
        m(i);
        return this.f0[i];
    }

    public final void m(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(n(i));
        }
    }

    public final String n(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        e();
        m(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            long[] jArr = this.f0;
            System.arraycopy(jArr, i2, jArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        e();
        m(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    public nu5(long[] jArr, int i) {
        this.f0 = jArr;
        this.g0 = i;
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* synthetic */ boolean add(Object obj) {
        i(((Long) obj).longValue());
        return true;
    }
}
