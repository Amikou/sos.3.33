package defpackage;

import android.content.Context;

/* compiled from: AllTokenListRepositoryProvider.kt */
/* renamed from: za  reason: default package */
/* loaded from: classes2.dex */
public final class za implements rm1 {
    public static final za a = new za();
    public static pl1 b;

    @Override // defpackage.rm1
    public void a() {
        b = null;
    }

    public final pl1 b(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new ya(new ni(context), new k41());
                }
                te4 te4Var = te4.a;
            }
        }
        pl1 pl1Var = b;
        fs1.d(pl1Var);
        return pl1Var;
    }
}
