package defpackage;

import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: GlideExecutor.java */
/* renamed from: ig1  reason: default package */
/* loaded from: classes.dex */
public final class ig1 implements ExecutorService {
    public static final long f0 = TimeUnit.SECONDS.toMillis(10);
    public static volatile int g0;
    public final ExecutorService a;

    /* compiled from: GlideExecutor.java */
    /* renamed from: ig1$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final boolean a;
        public int b;
        public int c;
        public final ThreadFactory d = new c();
        public e e = e.b;
        public String f;
        public long g;

        public b(boolean z) {
            this.a = z;
        }

        public ig1 a() {
            if (!TextUtils.isEmpty(this.f)) {
                ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(this.b, this.c, this.g, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new d(this.d, this.f, this.e, this.a));
                if (this.g != 0) {
                    threadPoolExecutor.allowCoreThreadTimeOut(true);
                }
                return new ig1(threadPoolExecutor);
            }
            throw new IllegalArgumentException("Name must be non-null and non-empty, but given: " + this.f);
        }

        public b b(String str) {
            this.f = str;
            return this;
        }

        public b c(int i) {
            this.b = i;
            this.c = i;
            return this;
        }
    }

    /* compiled from: GlideExecutor.java */
    /* renamed from: ig1$c */
    /* loaded from: classes.dex */
    public static final class c implements ThreadFactory {

        /* compiled from: GlideExecutor.java */
        /* renamed from: ig1$c$a */
        /* loaded from: classes.dex */
        public class a extends Thread {
            public a(c cVar, Runnable runnable) {
                super(runnable);
            }

            @Override // java.lang.Thread, java.lang.Runnable
            public void run() {
                Process.setThreadPriority(9);
                super.run();
            }
        }

        public c() {
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new a(this, runnable);
        }
    }

    /* compiled from: GlideExecutor.java */
    /* renamed from: ig1$d */
    /* loaded from: classes.dex */
    public static final class d implements ThreadFactory {
        public final ThreadFactory a;
        public final String f0;
        public final e g0;
        public final boolean h0;
        public final AtomicInteger i0 = new AtomicInteger();

        /* compiled from: GlideExecutor.java */
        /* renamed from: ig1$d$a */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public final /* synthetic */ Runnable a;

            public a(Runnable runnable) {
                this.a = runnable;
            }

            @Override // java.lang.Runnable
            public void run() {
                if (d.this.h0) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    this.a.run();
                } catch (Throwable th) {
                    d.this.g0.a(th);
                }
            }
        }

        public d(ThreadFactory threadFactory, String str, e eVar, boolean z) {
            this.a = threadFactory;
            this.f0 = str;
            this.g0 = eVar;
            this.h0 = z;
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            Thread newThread = this.a.newThread(new a(runnable));
            newThread.setName("glide-" + this.f0 + "-thread-" + this.i0.getAndIncrement());
            return newThread;
        }
    }

    /* compiled from: GlideExecutor.java */
    /* renamed from: ig1$e */
    /* loaded from: classes.dex */
    public interface e {
        public static final e a;
        public static final e b;

        /* compiled from: GlideExecutor.java */
        /* renamed from: ig1$e$a */
        /* loaded from: classes.dex */
        public class a implements e {
            @Override // defpackage.ig1.e
            public void a(Throwable th) {
            }
        }

        /* compiled from: GlideExecutor.java */
        /* renamed from: ig1$e$b */
        /* loaded from: classes.dex */
        public class b implements e {
            @Override // defpackage.ig1.e
            public void a(Throwable th) {
            }
        }

        /* compiled from: GlideExecutor.java */
        /* renamed from: ig1$e$c */
        /* loaded from: classes.dex */
        public class c implements e {
            @Override // defpackage.ig1.e
            public void a(Throwable th) {
                if (th != null) {
                    throw new RuntimeException("Request threw uncaught throwable", th);
                }
            }
        }

        static {
            new a();
            b bVar = new b();
            a = bVar;
            new c();
            b = bVar;
        }

        void a(Throwable th);
    }

    public ig1(ExecutorService executorService) {
        this.a = executorService;
    }

    public static int a() {
        if (g0 == 0) {
            g0 = Math.min(4, ca3.a());
        }
        return g0;
    }

    public static b b() {
        return new b(true).c(a() >= 4 ? 2 : 1).b("animation");
    }

    public static ig1 c() {
        return b().a();
    }

    public static b d() {
        return new b(true).c(1).b("disk-cache");
    }

    public static ig1 e() {
        return d().a();
    }

    public static b f() {
        return new b(false).c(a()).b("source");
    }

    public static ig1 g() {
        return f().a();
    }

    public static ig1 h() {
        return new ig1(new ThreadPoolExecutor(0, Integer.MAX_VALUE, f0, TimeUnit.MILLISECONDS, new SynchronousQueue(), new d(new c(), "source-unlimited", e.b, false)));
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.a.awaitTermination(j, timeUnit);
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.a.execute(runnable);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.a.invokeAll(collection);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return (T) this.a.invokeAny(collection);
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isShutdown() {
        return this.a.isShutdown();
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isTerminated() {
        return this.a.isTerminated();
    }

    @Override // java.util.concurrent.ExecutorService
    public void shutdown() {
        this.a.shutdown();
    }

    @Override // java.util.concurrent.ExecutorService
    public List<Runnable> shutdownNow() {
        return this.a.shutdownNow();
    }

    @Override // java.util.concurrent.ExecutorService
    public Future<?> submit(Runnable runnable) {
        return this.a.submit(runnable);
    }

    public String toString() {
        return this.a.toString();
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException {
        return this.a.invokeAll(collection, j, timeUnit);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return (T) this.a.invokeAny(collection, j, timeUnit);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.a.submit(runnable, t);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Callable<T> callable) {
        return this.a.submit(callable);
    }
}
