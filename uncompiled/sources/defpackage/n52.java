package defpackage;

/* renamed from: n52  reason: default package */
/* loaded from: classes2.dex */
public class n52 extends k52 {
    public int a;
    public int f0;
    public je1 g0;
    public rs2 h0;
    public he1 i0;
    public mq2 j0;
    public mq2 k0;
    public he1 l0;
    public rs2[] m0;

    public n52(int i, int i2, je1 je1Var, rs2 rs2Var, mq2 mq2Var, mq2 mq2Var2, he1 he1Var) {
        super(true, null);
        this.f0 = i2;
        this.a = i;
        this.g0 = je1Var;
        this.h0 = rs2Var;
        this.i0 = he1Var;
        this.j0 = mq2Var;
        this.k0 = mq2Var2;
        this.l0 = sh1.a(je1Var, rs2Var);
        this.m0 = new ts2(je1Var, rs2Var).c();
    }

    public je1 a() {
        return this.g0;
    }

    public rs2 b() {
        return this.h0;
    }

    public he1 c() {
        return this.l0;
    }

    public int d() {
        return this.f0;
    }

    public int e() {
        return this.a;
    }

    public mq2 f() {
        return this.j0;
    }

    public mq2 g() {
        return this.k0;
    }

    public rs2[] h() {
        return this.m0;
    }

    public he1 i() {
        return this.i0;
    }
}
