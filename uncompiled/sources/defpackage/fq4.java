package defpackage;

import java.util.concurrent.Executor;

/* compiled from: WorkInitializer_Factory.java */
/* renamed from: fq4  reason: default package */
/* loaded from: classes.dex */
public final class fq4 implements z11<eq4> {
    public final ew2<Executor> a;
    public final ew2<dy0> b;
    public final ew2<rq4> c;
    public final ew2<l24> d;

    public fq4(ew2<Executor> ew2Var, ew2<dy0> ew2Var2, ew2<rq4> ew2Var3, ew2<l24> ew2Var4) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
        this.d = ew2Var4;
    }

    public static fq4 a(ew2<Executor> ew2Var, ew2<dy0> ew2Var2, ew2<rq4> ew2Var3, ew2<l24> ew2Var4) {
        return new fq4(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static eq4 c(Executor executor, dy0 dy0Var, rq4 rq4Var, l24 l24Var) {
        return new eq4(executor, dy0Var, rq4Var, l24Var);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public eq4 get() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
