package defpackage;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.MotionEvent;
import com.facebook.common.internal.ImmutableMap;
import com.facebook.drawee.components.DraweeEventTracker;
import com.github.mikephil.charting.utils.Utils;
import defpackage.ff1;
import defpackage.l80;
import defpackage.tl0;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: AbstractDraweeController.java */
/* renamed from: w4  reason: default package */
/* loaded from: classes.dex */
public abstract class w4<T, INFO> implements ir0, tl0.a, ff1.a {
    public static final Map<String, Object> v = ImmutableMap.of("component_tag", "drawee");
    public static final Map<String, Object> w = ImmutableMap.of("origin", "memory_bitmap", "origin_sub", "shortcut");
    public static final Class<?> x = w4.class;
    public final tl0 b;
    public final Executor c;
    public x83 d;
    public ff1 e;
    public m80<INFO> f;
    public c22 h;
    public vm3 i;
    public Drawable j;
    public String k;
    public Object l;
    public boolean m;
    public boolean n;
    public boolean o;
    public boolean p;
    public String q;
    public ge0<T> r;
    public T s;
    public Drawable u;
    public final DraweeEventTracker a = DraweeEventTracker.a();
    public b91<INFO> g = new b91<>();
    public boolean t = true;

    /* compiled from: AbstractDraweeController.java */
    /* renamed from: w4$a */
    /* loaded from: classes.dex */
    public class a implements lm2 {
        public a() {
        }

        @Override // defpackage.lm2
        public void a() {
            w4 w4Var = w4.this;
            c22 c22Var = w4Var.h;
            if (c22Var != null) {
                c22Var.b(w4Var.k);
            }
        }

        @Override // defpackage.lm2
        public void b() {
        }

        @Override // defpackage.lm2
        public void c() {
            w4 w4Var = w4.this;
            c22 c22Var = w4Var.h;
            if (c22Var != null) {
                c22Var.a(w4Var.k);
            }
        }
    }

    /* compiled from: AbstractDraweeController.java */
    /* renamed from: w4$b */
    /* loaded from: classes.dex */
    public class b extends gn<T> {
        public final /* synthetic */ String a;
        public final /* synthetic */ boolean b;

        public b(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @Override // defpackage.ke0
        public void a(ge0<T> ge0Var) {
            boolean b = ge0Var.b();
            w4.this.O(this.a, ge0Var, ge0Var.d(), b);
        }

        @Override // defpackage.gn
        public void e(ge0<T> ge0Var) {
            w4.this.L(this.a, ge0Var, ge0Var.c(), true);
        }

        @Override // defpackage.gn
        public void f(ge0<T> ge0Var) {
            boolean b = ge0Var.b();
            boolean f = ge0Var.f();
            float d = ge0Var.d();
            T g = ge0Var.g();
            if (g != null) {
                w4.this.N(this.a, ge0Var, g, d, b, this.b, f);
            } else if (b) {
                w4.this.L(this.a, ge0Var, new NullPointerException(), true);
            }
        }
    }

    /* compiled from: AbstractDraweeController.java */
    /* renamed from: w4$c */
    /* loaded from: classes.dex */
    public static class c<INFO> extends c91<INFO> {
        public static <INFO> c<INFO> j(m80<? super INFO> m80Var, m80<? super INFO> m80Var2) {
            if (nc1.d()) {
                nc1.a("AbstractDraweeController#createInternal");
            }
            c<INFO> cVar = new c<>();
            cVar.g(m80Var);
            cVar.g(m80Var2);
            if (nc1.d()) {
                nc1.b();
            }
            return cVar;
        }
    }

    public w4(tl0 tl0Var, Executor executor, String str, Object obj) {
        this.b = tl0Var;
        this.c = executor;
        D(str, obj);
    }

    public abstract INFO A(T t);

    public Uri B() {
        return null;
    }

    public x83 C() {
        if (this.d == null) {
            this.d = new x83();
        }
        return this.d;
    }

    public final synchronized void D(String str, Object obj) {
        tl0 tl0Var;
        if (nc1.d()) {
            nc1.a("AbstractDraweeController#init");
        }
        this.a.b(DraweeEventTracker.Event.ON_INIT_CONTROLLER);
        if (!this.t && (tl0Var = this.b) != null) {
            tl0Var.a(this);
        }
        this.m = false;
        Q();
        this.p = false;
        x83 x83Var = this.d;
        if (x83Var != null) {
            x83Var.a();
        }
        ff1 ff1Var = this.e;
        if (ff1Var != null) {
            ff1Var.a();
            this.e.f(this);
        }
        m80<INFO> m80Var = this.f;
        if (m80Var instanceof c) {
            ((c) m80Var).h();
        } else {
            this.f = null;
        }
        vm3 vm3Var = this.i;
        if (vm3Var != null) {
            vm3Var.reset();
            this.i.b(null);
            this.i = null;
        }
        this.j = null;
        if (v11.m(2)) {
            v11.q(x, "controller %x %s -> %s: initialize", Integer.valueOf(System.identityHashCode(this)), this.k, str);
        }
        this.k = str;
        this.l = obj;
        if (nc1.d()) {
            nc1.b();
        }
        if (this.h != null) {
            e0();
        }
    }

    public void E(String str, Object obj) {
        D(str, obj);
        this.t = false;
    }

    public final boolean F(String str, ge0<T> ge0Var) {
        if (ge0Var == null && this.r == null) {
            return true;
        }
        return str.equals(this.k) && ge0Var == this.r && this.n;
    }

    public final void G(String str, Throwable th) {
        if (v11.m(2)) {
            v11.r(x, "controller %x %s: %s: failure: %s", Integer.valueOf(System.identityHashCode(this)), this.k, str, th);
        }
    }

    public final void H(String str, T t) {
        if (v11.m(2)) {
            v11.s(x, "controller %x %s: %s: image: %s %x", Integer.valueOf(System.identityHashCode(this)), this.k, str, y(t), Integer.valueOf(z(t)));
        }
    }

    public final l80.a I(ge0<T> ge0Var, INFO info2, Uri uri) {
        return J(ge0Var == null ? null : ge0Var.getExtras(), K(info2), uri);
    }

    public final l80.a J(Map<String, Object> map, Map<String, Object> map2, Uri uri) {
        String str;
        PointF pointF;
        vm3 vm3Var = this.i;
        if (vm3Var instanceof ye1) {
            ye1 ye1Var = (ye1) vm3Var;
            String valueOf = String.valueOf(ye1Var.n());
            pointF = ye1Var.m();
            str = valueOf;
        } else {
            str = null;
            pointF = null;
        }
        return v82.a(v, w, map, v(), str, pointF, map2, q(), uri);
    }

    public abstract Map<String, Object> K(INFO info2);

    public final void L(String str, ge0<T> ge0Var, Throwable th, boolean z) {
        Drawable drawable;
        if (nc1.d()) {
            nc1.a("AbstractDraweeController#onFailureInternal");
        }
        if (!F(str, ge0Var)) {
            G("ignore_old_datasource @ onFailure", th);
            ge0Var.close();
            if (nc1.d()) {
                nc1.b();
                return;
            }
            return;
        }
        this.a.b(z ? DraweeEventTracker.Event.ON_DATASOURCE_FAILURE : DraweeEventTracker.Event.ON_DATASOURCE_FAILURE_INT);
        if (z) {
            G("final_failed @ onFailure", th);
            this.r = null;
            this.o = true;
            vm3 vm3Var = this.i;
            if (vm3Var != null) {
                if (this.p && (drawable = this.u) != null) {
                    vm3Var.g(drawable, 1.0f, true);
                } else if (g0()) {
                    vm3Var.c(th);
                } else {
                    vm3Var.d(th);
                }
            }
            T(th, ge0Var);
        } else {
            G("intermediate_failed @ onFailure", th);
            U(th);
        }
        if (nc1.d()) {
            nc1.b();
        }
    }

    public void M(String str, T t) {
    }

    public final void N(String str, ge0<T> ge0Var, T t, float f, boolean z, boolean z2, boolean z3) {
        try {
            if (nc1.d()) {
                nc1.a("AbstractDraweeController#onNewResultInternal");
            }
            if (!F(str, ge0Var)) {
                H("ignore_old_datasource @ onNewResult", t);
                R(t);
                ge0Var.close();
                if (nc1.d()) {
                    nc1.b();
                    return;
                }
                return;
            }
            this.a.b(z ? DraweeEventTracker.Event.ON_DATASOURCE_RESULT : DraweeEventTracker.Event.ON_DATASOURCE_RESULT_INT);
            try {
                Drawable n = n(t);
                T t2 = this.s;
                Drawable drawable = this.u;
                this.s = t;
                this.u = n;
                if (z) {
                    H("set_final_result @ onNewResult", t);
                    this.r = null;
                    this.i.g(n, 1.0f, z2);
                    Y(str, t, ge0Var);
                } else if (z3) {
                    H("set_temporary_result @ onNewResult", t);
                    this.i.g(n, 1.0f, z2);
                    Y(str, t, ge0Var);
                } else {
                    H("set_intermediate_result @ onNewResult", t);
                    this.i.g(n, f, z2);
                    V(str, t);
                }
                if (drawable != null && drawable != n) {
                    P(drawable);
                }
                if (t2 != null && t2 != t) {
                    H("release_previous_result @ onNewResult", t2);
                    R(t2);
                }
                if (nc1.d()) {
                    nc1.b();
                }
            } catch (Exception e) {
                H("drawable_failed @ onNewResult", t);
                R(t);
                L(str, ge0Var, e, z);
                if (nc1.d()) {
                    nc1.b();
                }
            }
        } catch (Throwable th) {
            if (nc1.d()) {
                nc1.b();
            }
            throw th;
        }
    }

    public final void O(String str, ge0<T> ge0Var, float f, boolean z) {
        if (!F(str, ge0Var)) {
            G("ignore_old_datasource @ onProgress", null);
            ge0Var.close();
        } else if (z) {
        } else {
            this.i.e(f, false);
        }
    }

    public abstract void P(Drawable drawable);

    public final void Q() {
        Map<String, Object> map;
        boolean z = this.n;
        this.n = false;
        this.o = false;
        ge0<T> ge0Var = this.r;
        Map<String, Object> map2 = null;
        if (ge0Var != null) {
            map = ge0Var.getExtras();
            this.r.close();
            this.r = null;
        } else {
            map = null;
        }
        Drawable drawable = this.u;
        if (drawable != null) {
            P(drawable);
        }
        if (this.q != null) {
            this.q = null;
        }
        this.u = null;
        T t = this.s;
        if (t != null) {
            Map<String, Object> K = K(A(t));
            H("release", this.s);
            R(this.s);
            this.s = null;
            map2 = K;
        }
        if (z) {
            W(map, map2);
        }
    }

    public abstract void R(T t);

    public void S(l80<INFO> l80Var) {
        this.g.i(l80Var);
    }

    public final void T(Throwable th, ge0<T> ge0Var) {
        l80.a I = I(ge0Var, null, null);
        r().c(this.k, th);
        s().d(this.k, th, I);
    }

    public final void U(Throwable th) {
        r().f(this.k, th);
        s().f(this.k);
    }

    public final void V(String str, T t) {
        INFO A = A(t);
        r().a(str, A);
        s().a(str, A);
    }

    public final void W(Map<String, Object> map, Map<String, Object> map2) {
        r().d(this.k);
        s().c(this.k, J(map, map2, null));
    }

    public void X(ge0<T> ge0Var, INFO info2) {
        r().e(this.k, this.l);
        s().e(this.k, this.l, I(ge0Var, info2, B()));
    }

    public final void Y(String str, T t, ge0<T> ge0Var) {
        INFO A = A(t);
        r().b(str, A, o());
        s().b(str, A, I(ge0Var, A, null));
    }

    public void Z(String str) {
        this.q = str;
    }

    @Override // defpackage.tl0.a
    public void a() {
        this.a.b(DraweeEventTracker.Event.ON_RELEASE_CONTROLLER);
        x83 x83Var = this.d;
        if (x83Var != null) {
            x83Var.c();
        }
        ff1 ff1Var = this.e;
        if (ff1Var != null) {
            ff1Var.e();
        }
        vm3 vm3Var = this.i;
        if (vm3Var != null) {
            vm3Var.reset();
        }
        Q();
    }

    public void a0(Drawable drawable) {
        this.j = drawable;
        vm3 vm3Var = this.i;
        if (vm3Var != null) {
            vm3Var.b(drawable);
        }
    }

    @Override // defpackage.ir0
    public boolean b(MotionEvent motionEvent) {
        if (v11.m(2)) {
            v11.q(x, "controller %x %s: onTouchEvent %s", Integer.valueOf(System.identityHashCode(this)), this.k, motionEvent);
        }
        ff1 ff1Var = this.e;
        if (ff1Var == null) {
            return false;
        }
        if (ff1Var.b() || f0()) {
            this.e.d(motionEvent);
            return true;
        }
        return false;
    }

    public void b0(n80 n80Var) {
    }

    @Override // defpackage.ir0
    public void c() {
        if (nc1.d()) {
            nc1.a("AbstractDraweeController#onDetach");
        }
        if (v11.m(2)) {
            v11.p(x, "controller %x %s: onDetach", Integer.valueOf(System.identityHashCode(this)), this.k);
        }
        this.a.b(DraweeEventTracker.Event.ON_DETACH_CONTROLLER);
        this.m = false;
        this.b.d(this);
        if (nc1.d()) {
            nc1.b();
        }
    }

    public void c0(ff1 ff1Var) {
        this.e = ff1Var;
        if (ff1Var != null) {
            ff1Var.f(this);
        }
    }

    @Override // defpackage.ir0
    public jr0 d() {
        return this.i;
    }

    public void d0(boolean z) {
        this.p = z;
    }

    @Override // defpackage.ir0
    public void e(jr0 jr0Var) {
        if (v11.m(2)) {
            v11.q(x, "controller %x %s: setHierarchy: %s", Integer.valueOf(System.identityHashCode(this)), this.k, jr0Var);
        }
        this.a.b(jr0Var != null ? DraweeEventTracker.Event.ON_SET_HIERARCHY : DraweeEventTracker.Event.ON_CLEAR_HIERARCHY);
        if (this.n) {
            this.b.a(this);
            a();
        }
        vm3 vm3Var = this.i;
        if (vm3Var != null) {
            vm3Var.b(null);
            this.i = null;
        }
        if (jr0Var != null) {
            xt2.b(Boolean.valueOf(jr0Var instanceof vm3));
            vm3 vm3Var2 = (vm3) jr0Var;
            this.i = vm3Var2;
            vm3Var2.b(this.j);
        }
        if (this.h != null) {
            e0();
        }
    }

    public final void e0() {
        vm3 vm3Var = this.i;
        if (vm3Var instanceof ye1) {
            ((ye1) vm3Var).t(new a());
        }
    }

    @Override // defpackage.ff1.a
    public boolean f() {
        if (v11.m(2)) {
            v11.p(x, "controller %x %s: onClick", Integer.valueOf(System.identityHashCode(this)), this.k);
        }
        if (g0()) {
            this.d.b();
            this.i.reset();
            h0();
            return true;
        }
        return false;
    }

    public boolean f0() {
        return g0();
    }

    @Override // defpackage.ir0
    public void g() {
        if (nc1.d()) {
            nc1.a("AbstractDraweeController#onAttach");
        }
        if (v11.m(2)) {
            v11.q(x, "controller %x %s: onAttach: %s", Integer.valueOf(System.identityHashCode(this)), this.k, this.n ? "request already submitted" : "request needs submit");
        }
        this.a.b(DraweeEventTracker.Event.ON_ATTACH_CONTROLLER);
        xt2.g(this.i);
        this.b.a(this);
        this.m = true;
        if (!this.n) {
            h0();
        }
        if (nc1.d()) {
            nc1.b();
        }
    }

    public final boolean g0() {
        x83 x83Var;
        return this.o && (x83Var = this.d) != null && x83Var.e();
    }

    public void h0() {
        if (nc1.d()) {
            nc1.a("AbstractDraweeController#submitRequest");
        }
        T p = p();
        if (p != null) {
            if (nc1.d()) {
                nc1.a("AbstractDraweeController#submitRequest->cache");
            }
            this.r = null;
            this.n = true;
            this.o = false;
            this.a.b(DraweeEventTracker.Event.ON_SUBMIT_CACHE_HIT);
            X(this.r, A(p));
            M(this.k, p);
            N(this.k, this.r, p, 1.0f, true, true, true);
            if (nc1.d()) {
                nc1.b();
            }
            if (nc1.d()) {
                nc1.b();
                return;
            }
            return;
        }
        this.a.b(DraweeEventTracker.Event.ON_DATASOURCE_SUBMIT);
        this.i.e(Utils.FLOAT_EPSILON, true);
        this.n = true;
        this.o = false;
        ge0<T> u = u();
        this.r = u;
        X(u, null);
        if (v11.m(2)) {
            v11.q(x, "controller %x %s: submitRequest: dataSource: %x", Integer.valueOf(System.identityHashCode(this)), this.k, Integer.valueOf(System.identityHashCode(this.r)));
        }
        this.r.e(new b(this.k, this.r.a()), this.c);
        if (nc1.d()) {
            nc1.b();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public void l(m80<? super INFO> m80Var) {
        xt2.g(m80Var);
        m80<INFO> m80Var2 = this.f;
        if (m80Var2 instanceof c) {
            ((c) m80Var2).g(m80Var);
        } else if (m80Var2 != null) {
            this.f = c.j(m80Var2, m80Var);
        } else {
            this.f = m80Var;
        }
    }

    public void m(l80<INFO> l80Var) {
        this.g.g(l80Var);
    }

    public abstract Drawable n(T t);

    public Animatable o() {
        Drawable drawable = this.u;
        if (drawable instanceof Animatable) {
            return (Animatable) drawable;
        }
        return null;
    }

    public T p() {
        return null;
    }

    public Object q() {
        return this.l;
    }

    public m80<INFO> r() {
        m80<INFO> m80Var = this.f;
        return m80Var == null ? en.g() : m80Var;
    }

    public l80<INFO> s() {
        return this.g;
    }

    public Drawable t() {
        return this.j;
    }

    public String toString() {
        return ol2.c(this).c("isAttached", this.m).c("isRequestSubmitted", this.n).c("hasFetchFailed", this.o).a("fetchedImage", z(this.s)).b("events", this.a.toString()).toString();
    }

    public abstract ge0<T> u();

    public final Rect v() {
        vm3 vm3Var = this.i;
        if (vm3Var == null) {
            return null;
        }
        return vm3Var.a();
    }

    public ff1 w() {
        return this.e;
    }

    public String x() {
        return this.k;
    }

    public String y(T t) {
        return t != null ? t.getClass().getSimpleName() : "<null>";
    }

    public int z(T t) {
        return System.identityHashCode(t);
    }
}
