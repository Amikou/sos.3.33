package defpackage;

import com.onesignal.influence.domain.OSInfluenceType;

/* renamed from: kk2  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class kk2 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[OSInfluenceType.values().length];
        a = iArr;
        iArr[OSInfluenceType.DIRECT.ordinal()] = 1;
        iArr[OSInfluenceType.INDIRECT.ordinal()] = 2;
        iArr[OSInfluenceType.UNATTRIBUTED.ordinal()] = 3;
    }
}
