package defpackage;

import android.content.Context;
import com.google.android.play.core.assetpacks.c;

/* renamed from: qw4  reason: default package */
/* loaded from: classes2.dex */
public final class qw4 implements jw4<pw4> {
    public final jw4 a;
    public final /* synthetic */ int b = 0;

    public qw4(jw4<c> jw4Var) {
        this.a = jw4Var;
    }

    public qw4(jw4<zy4> jw4Var, byte[] bArr) {
        this.a = jw4Var;
    }

    public qw4(jw4<Context> jw4Var, char[] cArr) {
        this.a = jw4Var;
    }

    public qw4(jw4<c> jw4Var, short[] sArr) {
        this.a = jw4Var;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [su4, pw4] */
    /* JADX WARN: Type inference failed for: r1v4, types: [ww4, pw4] */
    /* JADX WARN: Type inference failed for: r1v5, types: [fx4, pw4] */
    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ pw4 a() {
        int i = this.b;
        return i != 0 ? i != 1 ? i != 2 ? new fx4((c) this.a.a()) : new ww4(((my4) this.a).a()) : new su4(gw4.c(this.a)) : new pw4((c) this.a.a());
    }
}
