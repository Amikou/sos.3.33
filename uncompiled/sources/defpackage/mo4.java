package defpackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;

/* compiled from: WebSocketClient.java */
/* renamed from: mo4  reason: default package */
/* loaded from: classes2.dex */
public abstract class mo4 extends p5 implements Runnable, WebSocket {
    public int connectTimeout;
    public Draft draft;
    public org.java_websocket.b engine;
    public Map<String, String> headers;
    public OutputStream ostream;
    public URI uri;
    public Thread writeThread;
    public Socket socket = null;
    public Proxy proxy = Proxy.NO_PROXY;
    public CountDownLatch connectLatch = new CountDownLatch(1);
    public CountDownLatch closeLatch = new CountDownLatch(1);

    /* compiled from: WebSocketClient.java */
    /* renamed from: mo4$b */
    /* loaded from: classes2.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            Thread currentThread = Thread.currentThread();
            currentThread.setName("WebSocketWriteThread-" + Thread.currentThread().getId());
            while (true) {
                try {
                    try {
                        try {
                            if (Thread.interrupted()) {
                                break;
                            }
                            ByteBuffer take = mo4.this.engine.a.take();
                            mo4.this.ostream.write(take.array(), 0, take.limit());
                            mo4.this.ostream.flush();
                        } catch (IOException e) {
                            mo4.this.handleIOException(e);
                        }
                    } finally {
                        mo4.this.closeSocket();
                        mo4.this.writeThread = null;
                    }
                } catch (InterruptedException unused) {
                    for (ByteBuffer byteBuffer : mo4.this.engine.a) {
                        mo4.this.ostream.write(byteBuffer.array(), 0, byteBuffer.limit());
                        mo4.this.ostream.flush();
                    }
                }
            }
        }
    }

    public mo4(URI uri, Draft draft, Map<String, String> map, int i) {
        this.uri = null;
        this.engine = null;
        this.connectTimeout = 0;
        if (uri == null) {
            throw new IllegalArgumentException();
        }
        if (draft != null) {
            this.uri = uri;
            this.draft = draft;
            this.headers = map;
            this.connectTimeout = i;
            setTcpNoDelay(false);
            setReuseAddr(false);
            this.engine = new org.java_websocket.b(this, draft);
            return;
        }
        throw new IllegalArgumentException("null as draft is permitted for `WebSocketServer` only!");
    }

    public void close() {
        if (this.writeThread != null) {
            this.engine.a(1000);
        }
    }

    public void closeBlocking() throws InterruptedException {
        close();
        this.closeLatch.await();
    }

    public final void closeSocket() {
        try {
            Socket socket = this.socket;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            onWebsocketError(this, e);
        }
    }

    public void connect() {
        if (this.writeThread == null) {
            Thread thread = new Thread(this);
            this.writeThread = thread;
            thread.setName("WebSocketConnectReadThread-" + this.writeThread.getId());
            this.writeThread.start();
            return;
        }
        throw new IllegalStateException("WebSocketClient objects are not reuseable");
    }

    @Override // defpackage.p5
    public Collection<WebSocket> getConnections() {
        return Collections.singletonList(this.engine);
    }

    public final int getPort() {
        int port = this.uri.getPort();
        if (port == -1) {
            String scheme = this.uri.getScheme();
            if ("wss".equals(scheme)) {
                return 443;
            }
            if ("ws".equals(scheme)) {
                return 80;
            }
            throw new IllegalArgumentException("unknown scheme: " + scheme);
        }
        return port;
    }

    public final void handleIOException(IOException iOException) {
        if (iOException instanceof SSLException) {
            onError(iOException);
        }
        this.engine.m();
    }

    public boolean isClosed() {
        return this.engine.s();
    }

    public boolean isClosing() {
        return this.engine.t();
    }

    public boolean isOpen() {
        return this.engine.u();
    }

    public abstract void onClose(int i, String str, boolean z);

    public void onCloseInitiated(int i, String str) {
    }

    public void onClosing(int i, String str, boolean z) {
    }

    public abstract void onError(Exception exc);

    public abstract void onMessage(String str);

    public void onMessage(ByteBuffer byteBuffer) {
    }

    public abstract void onOpen(bm3 bm3Var);

    @Override // org.java_websocket.c
    public final void onWebsocketClose(WebSocket webSocket, int i, String str, boolean z) {
        stopConnectionLostTimer();
        Thread thread = this.writeThread;
        if (thread != null) {
            thread.interrupt();
        }
        onClose(i, str, z);
        this.connectLatch.countDown();
        this.closeLatch.countDown();
    }

    @Override // org.java_websocket.c
    public void onWebsocketCloseInitiated(WebSocket webSocket, int i, String str) {
        onCloseInitiated(i, str);
    }

    @Override // org.java_websocket.c
    public void onWebsocketClosing(WebSocket webSocket, int i, String str, boolean z) {
        onClosing(i, str, z);
    }

    @Override // org.java_websocket.c
    public final void onWebsocketError(WebSocket webSocket, Exception exc) {
        onError(exc);
    }

    @Override // org.java_websocket.c
    public final void onWebsocketMessage(WebSocket webSocket, String str) {
        onMessage(str);
    }

    @Override // org.java_websocket.c
    public final void onWebsocketOpen(WebSocket webSocket, tj1 tj1Var) {
        startConnectionLostTimer();
        onOpen((bm3) tj1Var);
        this.connectLatch.countDown();
    }

    @Override // org.java_websocket.c
    public final void onWriteDemand(WebSocket webSocket) {
    }

    public void reconnect() {
        reset();
        connect();
    }

    public final void reset() {
        try {
            closeBlocking();
            Thread thread = this.writeThread;
            if (thread != null) {
                thread.interrupt();
                this.writeThread = null;
            }
            this.draft.q();
            Socket socket = this.socket;
            if (socket != null) {
                socket.close();
                this.socket = null;
            }
            this.connectLatch = new CountDownLatch(1);
            this.closeLatch = new CountDownLatch(1);
            this.engine = new org.java_websocket.b(this, this.draft);
        } catch (Exception e) {
            onError(e);
            this.engine.e(1006, e.getMessage());
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean z;
        int read;
        try {
            Socket socket = this.socket;
            if (socket == null) {
                this.socket = new Socket(this.proxy);
                z = true;
            } else if (socket.isClosed()) {
                throw new IOException();
            } else {
                z = false;
            }
            this.socket.setTcpNoDelay(isTcpNoDelay());
            this.socket.setReuseAddress(isReuseAddr());
            if (!this.socket.isBound()) {
                this.socket.connect(new InetSocketAddress(this.uri.getHost(), getPort()), this.connectTimeout);
            }
            if (z && "wss".equals(this.uri.getScheme())) {
                SSLContext sSLContext = SSLContext.getInstance("TLS");
                sSLContext.init(null, null, null);
                this.socket = sSLContext.getSocketFactory().createSocket(this.socket, this.uri.getHost(), getPort(), true);
            }
            InputStream inputStream = this.socket.getInputStream();
            this.ostream = this.socket.getOutputStream();
            sendHandshake();
            Thread thread = new Thread(new b());
            this.writeThread = thread;
            thread.start();
            byte[] bArr = new byte[org.java_websocket.b.v0];
            while (!isClosing() && !isClosed() && (read = inputStream.read(bArr)) != -1) {
                try {
                    this.engine.j(ByteBuffer.wrap(bArr, 0, read));
                } catch (IOException e) {
                    handleIOException(e);
                    return;
                } catch (RuntimeException e2) {
                    onError(e2);
                    this.engine.e(1006, e2.getMessage());
                    return;
                }
            }
            this.engine.m();
        } catch (Exception e3) {
            onWebsocketError(this.engine, e3);
            this.engine.e(-1, e3.getMessage());
        }
    }

    public void send(String str) throws NotYetConnectedException {
        this.engine.w(str);
    }

    @Override // org.java_websocket.WebSocket
    public void sendFrame(Framedata framedata) {
        this.engine.sendFrame(framedata);
    }

    public final void sendHandshake() throws InvalidHandshakeException {
        String rawPath = this.uri.getRawPath();
        String rawQuery = this.uri.getRawQuery();
        rawPath = (rawPath == null || rawPath.length() == 0) ? "/" : "/";
        if (rawQuery != null) {
            rawPath = rawPath + '?' + rawQuery;
        }
        int port = getPort();
        StringBuilder sb = new StringBuilder();
        sb.append(this.uri.getHost());
        sb.append(port != 80 ? ":" + port : "");
        String sb2 = sb.toString();
        rj1 rj1Var = new rj1();
        rj1Var.f(rawPath);
        rj1Var.put("Host", sb2);
        Map<String, String> map = this.headers;
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                rj1Var.put(entry.getKey(), entry.getValue());
            }
        }
        this.engine.A(rj1Var);
    }

    @Override // org.java_websocket.c
    public final void onWebsocketMessage(WebSocket webSocket, ByteBuffer byteBuffer) {
        onMessage(byteBuffer);
    }
}
