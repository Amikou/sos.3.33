package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textview.MaterialTextView;
import defpackage.n14;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.swap.Swap;

/* compiled from: SwapTokenAdapter.kt */
/* renamed from: n14  reason: default package */
/* loaded from: classes2.dex */
public final class n14 extends RecyclerView.Adapter<a> implements Filterable {
    public List<Swap> a = new ArrayList();
    public final List<Swap> f0 = new ArrayList();
    public tc1<? super Swap, te4> g0;

    /* compiled from: SwapTokenAdapter.kt */
    /* renamed from: n14$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final ti4 a;
        public final /* synthetic */ n14 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(n14 n14Var, ti4 ti4Var) {
            super(ti4Var.b());
            fs1.f(n14Var, "this$0");
            fs1.f(ti4Var, "binding");
            this.b = n14Var;
            this.a = ti4Var;
        }

        public static final void c(n14 n14Var, Swap swap, View view) {
            fs1.f(n14Var, "this$0");
            fs1.f(swap, "$item");
            tc1 tc1Var = n14Var.g0;
            if (tc1Var == null) {
                return;
            }
            tc1Var.invoke(swap);
        }

        public final void b(int i) {
            final Swap swap = (Swap) this.b.a.get(i);
            ti4 ti4Var = this.a;
            final n14 n14Var = this.b;
            String str = swap.logoURI;
            fs1.e(str, "item.logoURI");
            if ((str.length() > 0) || swap.imageResource > 0) {
                k73 u = com.bumptech.glide.a.u(ti4Var.b);
                String str2 = swap.logoURI;
                fs1.e(str2, "item.logoURI");
                u.x(str2.length() > 0 ? swap.logoURI : Integer.valueOf(swap.imageResource)).d0(150, 150).a(n73.v0()).I0(ti4Var.b);
            }
            MaterialTextView materialTextView = ti4Var.d;
            materialTextView.setText(((Object) swap.name) + " (" + ((Object) swap.symbol) + ')');
            MaterialTextView materialTextView2 = ti4Var.c;
            fs1.e(materialTextView2, "txtTokenBal");
            Double d = swap.nativeBalance;
            fs1.e(d, "it.nativeBalance");
            materialTextView2.setVisibility(d.doubleValue() > Utils.DOUBLE_EPSILON ? 0 : 8);
            MaterialTextView materialTextView3 = ti4Var.c;
            Double d2 = swap.nativeBalance;
            fs1.e(d2, "it.nativeBalance");
            materialTextView3.setText(e30.p(d2.doubleValue(), 0, null, false, 7, null));
            ti4Var.b().setOnClickListener(new View.OnClickListener() { // from class: m14
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    n14.a.c(n14.this, swap, view);
                }
            });
        }
    }

    /* compiled from: SwapTokenAdapter.kt */
    /* renamed from: n14$b */
    /* loaded from: classes2.dex */
    public final class b extends Filter {
        public final /* synthetic */ n14 a;

        public b(n14 n14Var) {
            fs1.f(n14Var, "this$0");
            this.a = n14Var;
        }

        @Override // android.widget.Filter
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String obj;
            String obj2;
            Filter.FilterResults filterResults = new Filter.FilterResults();
            filterResults.values = this.a.f0;
            if (charSequence != null && (obj = charSequence.toString()) != null && (obj2 = StringsKt__StringsKt.K0(obj).toString()) != null) {
                String lowerCase = obj2.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (lowerCase != null) {
                    List list = this.a.f0;
                    ArrayList arrayList = new ArrayList();
                    for (Object obj3 : list) {
                        Swap swap = (Swap) obj3;
                        boolean z = true;
                        if (!(lowerCase.length() == 0)) {
                            String str = swap.name;
                            fs1.e(str, "it.name");
                            Locale locale = Locale.ROOT;
                            String lowerCase2 = str.toLowerCase(locale);
                            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            if (!StringsKt__StringsKt.M(lowerCase2, lowerCase, false, 2, null)) {
                                String str2 = swap.symbol;
                                fs1.e(str2, "it.symbol");
                                String lowerCase3 = str2.toLowerCase(locale);
                                fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                                if (!StringsKt__StringsKt.M(lowerCase3, lowerCase, false, 2, null)) {
                                    z = false;
                                }
                            }
                        }
                        if (z) {
                            arrayList.add(obj3);
                        }
                    }
                    filterResults.values = arrayList;
                }
            }
            return filterResults;
        }

        @Override // android.widget.Filter
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.values == null) {
                return;
            }
            n14 n14Var = this.a;
            n14Var.a.clear();
            List list = n14Var.a;
            Object obj = filterResults.values;
            Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.collections.Collection<net.safemoon.androidwallet.model.swap.Swap>");
            list.addAll((Collection) obj);
            n14Var.notifyDataSetChanged();
        }
    }

    @Override // android.widget.Filterable
    /* renamed from: d */
    public b getFilter() {
        return new b(this);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        aVar.b(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        ti4 a2 = ti4.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_holder_swap, viewGroup, false));
        fs1.e(a2, "bind(LayoutInflater.from…der_swap, parent, false))");
        return new a(this, a2);
    }

    public final void g(List<? extends Swap> list) {
        fs1.f(list, "tokens");
        this.f0.clear();
        this.f0.addAll(list);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.size();
    }

    public final void h(tc1<? super Swap, te4> tc1Var) {
        this.g0 = tc1Var;
    }
}
