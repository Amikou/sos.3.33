package defpackage;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.m;

/* compiled from: JsonSchema.java */
@Deprecated
/* renamed from: aw1  reason: default package */
/* loaded from: classes.dex */
public class aw1 {
    public final m a;

    @JsonCreator
    public aw1(m mVar) {
        this.a = mVar;
    }

    public static d a() {
        m objectNode = JsonNodeFactory.instance.objectNode();
        objectNode.T("type", "any");
        return objectNode;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof aw1)) {
            aw1 aw1Var = (aw1) obj;
            m mVar = this.a;
            if (mVar == null) {
                return aw1Var.a == null;
            }
            return mVar.equals(aw1Var.a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.a.toString();
    }
}
