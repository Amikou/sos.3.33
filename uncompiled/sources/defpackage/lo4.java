package defpackage;

import java.io.IOException;
import java8.util.concurrent.CompletableFuture;
import org.web3j.protocol.core.a;
import org.web3j.protocol.core.c;

/* compiled from: Web3jService.java */
/* renamed from: lo4  reason: default package */
/* loaded from: classes3.dex */
public interface lo4 {
    void close() throws IOException;

    <T extends i83> T send(c cVar, Class<T> cls) throws IOException;

    <T extends i83> CompletableFuture<T> sendAsync(c cVar, Class<T> cls);

    lo sendBatch(a aVar) throws IOException;

    CompletableFuture<lo> sendBatchAsync(a aVar);

    <T extends zg2<?>> q71<T> subscribe(c cVar, String str, Class<T> cls);
}
