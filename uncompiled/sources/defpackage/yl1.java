package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: IFiatTokenDataSource.kt */
/* renamed from: yl1  reason: default package */
/* loaded from: classes2.dex */
public interface yl1 {
    LiveData<List<RoomFiat>> a();

    Object b(Fiat[] fiatArr, q70<? super te4> q70Var);
}
