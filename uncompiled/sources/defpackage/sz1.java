package defpackage;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleCoroutineScope;

/* compiled from: LifecycleOwner.kt */
/* renamed from: sz1  reason: default package */
/* loaded from: classes.dex */
public final class sz1 {
    public static final LifecycleCoroutineScope a(rz1 rz1Var) {
        fs1.f(rz1Var, "$this$lifecycleScope");
        Lifecycle lifecycle = rz1Var.getLifecycle();
        fs1.e(lifecycle, "lifecycle");
        return oz1.a(lifecycle);
    }
}
