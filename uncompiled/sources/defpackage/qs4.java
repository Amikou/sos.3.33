package defpackage;

import java.math.BigInteger;

/* renamed from: qs4  reason: default package */
/* loaded from: classes2.dex */
public class qs4 {
    public final BigInteger a;
    public final BigInteger b;

    public qs4(BigInteger bigInteger, BigInteger bigInteger2) {
        this.a = bigInteger;
        this.b = bigInteger2;
    }
}
