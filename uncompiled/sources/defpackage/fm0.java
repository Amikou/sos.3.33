package defpackage;

import android.content.res.Resources;
import android.util.TypedValue;

/* compiled from: DensityUtils.java */
/* renamed from: fm0  reason: default package */
/* loaded from: classes2.dex */
public class fm0 {
    public static int a(int i) {
        return (int) TypedValue.applyDimension(1, i, Resources.getSystem().getDisplayMetrics());
    }
}
