package defpackage;

import android.os.Handler;
import android.os.Process;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: RequestExecutor.java */
/* renamed from: f73  reason: default package */
/* loaded from: classes.dex */
public class f73 {

    /* compiled from: RequestExecutor.java */
    /* renamed from: f73$a */
    /* loaded from: classes.dex */
    public static class a implements ThreadFactory {
        public String a;
        public int f0;

        /* compiled from: RequestExecutor.java */
        /* renamed from: f73$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0170a extends Thread {
            public final int a;

            public C0170a(Runnable runnable, String str, int i) {
                super(runnable, str);
                this.a = i;
            }

            @Override // java.lang.Thread, java.lang.Runnable
            public void run() {
                Process.setThreadPriority(this.a);
                super.run();
            }
        }

        public a(String str, int i) {
            this.a = str;
            this.f0 = i;
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new C0170a(runnable, this.a, this.f0);
        }
    }

    /* compiled from: RequestExecutor.java */
    /* renamed from: f73$b */
    /* loaded from: classes.dex */
    public static class b<T> implements Runnable {
        public Callable<T> a;
        public n60<T> f0;
        public Handler g0;

        /* compiled from: RequestExecutor.java */
        /* renamed from: f73$b$a */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public final /* synthetic */ n60 a;
            public final /* synthetic */ Object f0;

            public a(b bVar, n60 n60Var, Object obj) {
                this.a = n60Var;
                this.f0 = obj;
            }

            /* JADX WARN: Multi-variable type inference failed */
            @Override // java.lang.Runnable
            public void run() {
                this.a.accept(this.f0);
            }
        }

        public b(Handler handler, Callable<T> callable, n60<T> n60Var) {
            this.a = callable;
            this.f0 = n60Var;
            this.g0 = handler;
        }

        @Override // java.lang.Runnable
        public void run() {
            T t;
            try {
                t = this.a.call();
            } catch (Exception unused) {
                t = null;
            }
            this.g0.post(new a(this, this.f0, t));
        }
    }

    public static ThreadPoolExecutor a(String str, int i, int i2) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, i2, TimeUnit.MILLISECONDS, new LinkedBlockingDeque(), new a(str, i));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }

    public static <T> void b(Executor executor, Callable<T> callable, n60<T> n60Var) {
        executor.execute(new b(zu.a(), callable, n60Var));
    }

    public static <T> T c(ExecutorService executorService, Callable<T> callable, int i) throws InterruptedException {
        try {
            return executorService.submit(callable).get(i, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw e;
        } catch (ExecutionException e2) {
            throw new RuntimeException(e2);
        } catch (TimeoutException unused) {
            throw new InterruptedException("timeout");
        }
    }
}
