package defpackage;

import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.asn1.a0;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: jv3  reason: default package */
/* loaded from: classes2.dex */
public class jv3 extends h {
    public va a;
    public a0 f0;

    public jv3(h4 h4Var) {
        if (h4Var.size() == 2) {
            Enumeration E = h4Var.E();
            this.a = va.p(E.nextElement());
            this.f0 = a0.H(E.nextElement());
            return;
        }
        throw new IllegalArgumentException("Bad sequence size: " + h4Var.size());
    }

    public jv3(va vaVar, c4 c4Var) throws IOException {
        this.f0 = new a0(c4Var);
        this.a = vaVar;
    }

    public jv3(va vaVar, byte[] bArr) {
        this.f0 = new a0(bArr);
        this.a = vaVar;
    }

    public static jv3 p(Object obj) {
        if (obj instanceof jv3) {
            return (jv3) obj;
        }
        if (obj != null) {
            return new jv3(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(this.a);
        d4Var.a(this.f0);
        return new n0(d4Var);
    }

    public va o() {
        return this.a;
    }

    public a0 q() {
        return this.f0;
    }

    public k s() throws IOException {
        return k.s(this.f0.E());
    }
}
