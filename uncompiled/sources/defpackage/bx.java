package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.r;
import com.google.crypto.tink.proto.s;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: ChaCha20Poly1305KeyManager.java */
/* renamed from: bx  reason: default package */
/* loaded from: classes2.dex */
public class bx extends g<r> {

    /* compiled from: ChaCha20Poly1305KeyManager.java */
    /* renamed from: bx$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, r> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(r rVar) throws GeneralSecurityException {
            return new zw(rVar.G().toByteArray());
        }
    }

    /* compiled from: ChaCha20Poly1305KeyManager.java */
    /* renamed from: bx$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<s, r> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public r a(s sVar) throws GeneralSecurityException {
            return r.I().t(bx.this.j()).s(ByteString.copyFrom(p33.c(32))).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public s c(ByteString byteString) throws InvalidProtocolBufferException {
            return s.D(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(s sVar) throws GeneralSecurityException {
        }
    }

    public bx() {
        super(r.class, new a(com.google.crypto.tink.a.class));
    }

    public static void l(boolean z) throws GeneralSecurityException {
        q.q(new bx(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, r> e() {
        return new b(s.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int j() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: k */
    public r g(ByteString byteString) throws InvalidProtocolBufferException {
        return r.J(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(r rVar) throws GeneralSecurityException {
        ug4.c(rVar.H(), j());
        if (rVar.G().size() != 32) {
            throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
        }
    }
}
