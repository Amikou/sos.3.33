package defpackage;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;

/* compiled from: TextDrawableHelper.java */
/* renamed from: i44  reason: default package */
/* loaded from: classes2.dex */
public class i44 {
    public float c;
    public d44 f;
    public final TextPaint a = new TextPaint(1);
    public final f44 b = new a();
    public boolean d = true;
    public WeakReference<b> e = new WeakReference<>(null);

    /* compiled from: TextDrawableHelper.java */
    /* renamed from: i44$a */
    /* loaded from: classes2.dex */
    public class a extends f44 {
        public a() {
        }

        @Override // defpackage.f44
        public void a(int i) {
            i44.this.d = true;
            b bVar = (b) i44.this.e.get();
            if (bVar != null) {
                bVar.a();
            }
        }

        @Override // defpackage.f44
        public void b(Typeface typeface, boolean z) {
            if (z) {
                return;
            }
            i44.this.d = true;
            b bVar = (b) i44.this.e.get();
            if (bVar != null) {
                bVar.a();
            }
        }
    }

    /* compiled from: TextDrawableHelper.java */
    /* renamed from: i44$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a();

        int[] getState();

        boolean onStateChange(int[] iArr);
    }

    public i44(b bVar) {
        g(bVar);
    }

    public final float c(CharSequence charSequence) {
        return charSequence == null ? Utils.FLOAT_EPSILON : this.a.measureText(charSequence, 0, charSequence.length());
    }

    public d44 d() {
        return this.f;
    }

    public TextPaint e() {
        return this.a;
    }

    public float f(String str) {
        if (!this.d) {
            return this.c;
        }
        float c = c(str);
        this.c = c;
        this.d = false;
        return c;
    }

    public void g(b bVar) {
        this.e = new WeakReference<>(bVar);
    }

    public void h(d44 d44Var, Context context) {
        if (this.f != d44Var) {
            this.f = d44Var;
            if (d44Var != null) {
                d44Var.k(context, this.a, this.b);
                b bVar = this.e.get();
                if (bVar != null) {
                    this.a.drawableState = bVar.getState();
                }
                d44Var.j(context, this.a, this.b);
                this.d = true;
            }
            b bVar2 = this.e.get();
            if (bVar2 != null) {
                bVar2.a();
                bVar2.onStateChange(bVar2.getState());
            }
        }
    }

    public void i(boolean z) {
        this.d = z;
    }

    public void j(Context context) {
        this.f.j(context, this.a, this.b);
    }
}
