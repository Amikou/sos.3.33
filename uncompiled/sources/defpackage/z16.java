package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: z16  reason: default package */
/* loaded from: classes.dex */
public final class z16 implements yp5<a26> {
    public static final z16 f0 = new z16();
    public final yp5<a26> a = gq5.a(gq5.b(new b26()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final a26 zza() {
        return this.a.zza();
    }
}
