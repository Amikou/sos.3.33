package defpackage;

import android.net.Uri;
import defpackage.j92;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: UrlUriLoader.java */
/* renamed from: vf4  reason: default package */
/* loaded from: classes.dex */
public class vf4<Data> implements j92<Uri, Data> {
    public static final Set<String> b = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", "https")));
    public final j92<ng1, Data> a;

    /* compiled from: UrlUriLoader.java */
    /* renamed from: vf4$a */
    /* loaded from: classes.dex */
    public static class a implements k92<Uri, InputStream> {
        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Uri, InputStream> c(qa2 qa2Var) {
            return new vf4(qa2Var.d(ng1.class, InputStream.class));
        }
    }

    public vf4(j92<ng1, Data> j92Var) {
        this.a = j92Var;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(Uri uri, int i, int i2, vn2 vn2Var) {
        return this.a.b(new ng1(uri.toString()), i, i2, vn2Var);
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
