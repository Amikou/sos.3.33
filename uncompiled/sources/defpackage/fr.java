package defpackage;

import java.security.Permission;
import java.security.spec.ECParameterSpec;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.crypto.spec.DHParameterSpec;
import org.bouncycastle.jcajce.provider.config.ProviderConfigurationPermission;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/* renamed from: fr  reason: default package */
/* loaded from: classes2.dex */
public class fr implements gw2 {
    public static Permission g = new ProviderConfigurationPermission(BouncyCastleProvider.PROVIDER_NAME, "threadLocalEcImplicitlyCa");
    public static Permission h = new ProviderConfigurationPermission(BouncyCastleProvider.PROVIDER_NAME, "ecImplicitlyCa");
    public static Permission i = new ProviderConfigurationPermission(BouncyCastleProvider.PROVIDER_NAME, "threadLocalDhDefaultParams");
    public static Permission j = new ProviderConfigurationPermission(BouncyCastleProvider.PROVIDER_NAME, "DhDefaultParams");
    public static Permission k = new ProviderConfigurationPermission(BouncyCastleProvider.PROVIDER_NAME, "acceptableEcCurves");
    public static Permission l = new ProviderConfigurationPermission(BouncyCastleProvider.PROVIDER_NAME, "additionalEcParameters");
    public volatile ot0 c;
    public volatile Object d;
    public ThreadLocal a = new ThreadLocal();
    public ThreadLocal b = new ThreadLocal();
    public volatile Set e = new HashSet();
    public volatile Map f = new HashMap();

    @Override // defpackage.gw2
    public Map a() {
        return Collections.unmodifiableMap(this.f);
    }

    @Override // defpackage.gw2
    public ot0 b() {
        ot0 ot0Var = (ot0) this.a.get();
        return ot0Var != null ? ot0Var : this.c;
    }

    @Override // defpackage.gw2
    public Set c() {
        return Collections.unmodifiableSet(this.e);
    }

    public void d(String str, Object obj) {
        ThreadLocal threadLocal;
        SecurityManager securityManager = System.getSecurityManager();
        if (str.equals("threadLocalEcImplicitlyCa")) {
            if (securityManager != null) {
                securityManager.checkPermission(g);
            }
            ot0 g2 = ((obj instanceof ot0) || obj == null) ? (ot0) obj : us0.g((ECParameterSpec) obj, false);
            if (g2 != null) {
                this.a.set(g2);
                return;
            }
            threadLocal = this.a;
        } else if (str.equals("ecImplicitlyCa")) {
            if (securityManager != null) {
                securityManager.checkPermission(h);
            }
            if ((obj instanceof ot0) || obj == null) {
                this.c = (ot0) obj;
                return;
            } else {
                this.c = us0.g((ECParameterSpec) obj, false);
                return;
            }
        } else if (!str.equals("threadLocalDhDefaultParams")) {
            if (str.equals("DhDefaultParams")) {
                if (securityManager != null) {
                    securityManager.checkPermission(j);
                }
                if (!(obj instanceof DHParameterSpec) && !(obj instanceof DHParameterSpec[]) && obj != null) {
                    throw new IllegalArgumentException("not a valid DHParameterSpec or DHParameterSpec[]");
                }
                this.d = obj;
                return;
            } else if (str.equals("acceptableEcCurves")) {
                if (securityManager != null) {
                    securityManager.checkPermission(k);
                }
                this.e = (Set) obj;
                return;
            } else if (str.equals("additionalEcParameters")) {
                if (securityManager != null) {
                    securityManager.checkPermission(l);
                }
                this.f = (Map) obj;
                return;
            } else {
                return;
            }
        } else {
            if (securityManager != null) {
                securityManager.checkPermission(i);
            }
            if (!(obj instanceof DHParameterSpec) && !(obj instanceof DHParameterSpec[]) && obj != null) {
                throw new IllegalArgumentException("not a valid DHParameterSpec");
            }
            threadLocal = this.b;
            if (obj != null) {
                threadLocal.set(obj);
                return;
            }
        }
        threadLocal.remove();
    }
}
