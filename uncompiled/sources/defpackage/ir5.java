package defpackage;

import com.google.android.gms.internal.vision.zzml;
import com.google.android.gms.internal.vision.zzmo;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ir5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class ir5 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[zzml.values().length];
        b = iArr;
        try {
            iArr[zzml.zza.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            b[zzml.zzb.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            b[zzml.zzc.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            b[zzml.zzd.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            b[zzml.zze.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            b[zzml.zzf.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            b[zzml.zzg.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            b[zzml.zzh.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        try {
            b[zzml.zzj.ordinal()] = 9;
        } catch (NoSuchFieldError unused9) {
        }
        try {
            b[zzml.zzk.ordinal()] = 10;
        } catch (NoSuchFieldError unused10) {
        }
        try {
            b[zzml.zzi.ordinal()] = 11;
        } catch (NoSuchFieldError unused11) {
        }
        try {
            b[zzml.zzl.ordinal()] = 12;
        } catch (NoSuchFieldError unused12) {
        }
        try {
            b[zzml.zzm.ordinal()] = 13;
        } catch (NoSuchFieldError unused13) {
        }
        try {
            b[zzml.zzo.ordinal()] = 14;
        } catch (NoSuchFieldError unused14) {
        }
        try {
            b[zzml.zzp.ordinal()] = 15;
        } catch (NoSuchFieldError unused15) {
        }
        try {
            b[zzml.zzq.ordinal()] = 16;
        } catch (NoSuchFieldError unused16) {
        }
        try {
            b[zzml.zzr.ordinal()] = 17;
        } catch (NoSuchFieldError unused17) {
        }
        try {
            b[zzml.zzn.ordinal()] = 18;
        } catch (NoSuchFieldError unused18) {
        }
        int[] iArr2 = new int[zzmo.values().length];
        a = iArr2;
        try {
            iArr2[zzmo.INT.ordinal()] = 1;
        } catch (NoSuchFieldError unused19) {
        }
        try {
            a[zzmo.LONG.ordinal()] = 2;
        } catch (NoSuchFieldError unused20) {
        }
        try {
            a[zzmo.FLOAT.ordinal()] = 3;
        } catch (NoSuchFieldError unused21) {
        }
        try {
            a[zzmo.DOUBLE.ordinal()] = 4;
        } catch (NoSuchFieldError unused22) {
        }
        try {
            a[zzmo.BOOLEAN.ordinal()] = 5;
        } catch (NoSuchFieldError unused23) {
        }
        try {
            a[zzmo.STRING.ordinal()] = 6;
        } catch (NoSuchFieldError unused24) {
        }
        try {
            a[zzmo.BYTE_STRING.ordinal()] = 7;
        } catch (NoSuchFieldError unused25) {
        }
        try {
            a[zzmo.ENUM.ordinal()] = 8;
        } catch (NoSuchFieldError unused26) {
        }
        try {
            a[zzmo.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError unused27) {
        }
    }
}
