package defpackage;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/* compiled from: TransactionExecutor.java */
/* renamed from: t84  reason: default package */
/* loaded from: classes.dex */
public class t84 implements Executor {
    public final Executor a;
    public final ArrayDeque<Runnable> f0 = new ArrayDeque<>();
    public Runnable g0;

    /* compiled from: TransactionExecutor.java */
    /* renamed from: t84$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ Runnable a;

        public a(Runnable runnable) {
            this.a = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                this.a.run();
            } finally {
                t84.this.a();
            }
        }
    }

    public t84(Executor executor) {
        this.a = executor;
    }

    public synchronized void a() {
        Runnable poll = this.f0.poll();
        this.g0 = poll;
        if (poll != null) {
            this.a.execute(poll);
        }
    }

    @Override // java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        this.f0.offer(new a(runnable));
        if (this.g0 == null) {
            a();
        }
    }
}
