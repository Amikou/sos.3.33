package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: DefaultRunnableScheduler.java */
/* renamed from: lk0  reason: default package */
/* loaded from: classes.dex */
public class lk0 implements ba3 {
    public final Handler a = kj1.a(Looper.getMainLooper());

    @Override // defpackage.ba3
    public void a(long j, Runnable runnable) {
        this.a.postDelayed(runnable, j);
    }

    @Override // defpackage.ba3
    public void b(Runnable runnable) {
        this.a.removeCallbacks(runnable);
    }
}
