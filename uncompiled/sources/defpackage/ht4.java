package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.play.core.internal.p;

/* renamed from: ht4  reason: default package */
/* loaded from: classes2.dex */
public final class ht4 extends jt4 {
    public final /* synthetic */ tx4 f0;
    public final /* synthetic */ st4 g0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ht4(st4 st4Var, tx4 tx4Var, tx4 tx4Var2) {
        super(tx4Var);
        this.g0 = st4Var;
        this.f0 = tx4Var2;
    }

    @Override // defpackage.jt4
    public final void a() {
        it4 it4Var;
        zt4 zt4Var;
        String str;
        Bundle j;
        try {
            zt4Var = this.g0.d;
            str = this.g0.a;
            j = st4.j();
            ((p) zt4Var.c()).U(str, j, new mt4(this.g0, this.f0));
        } catch (RemoteException e) {
            it4Var = st4.f;
            it4Var.c(e, "keepAlive", new Object[0]);
        }
    }
}
