package defpackage;

import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import androidx.room.RoomDatabase;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/* compiled from: DBUtil.java */
/* renamed from: id0  reason: default package */
/* loaded from: classes.dex */
public class id0 {
    public static CancellationSignal a() {
        if (Build.VERSION.SDK_INT >= 16) {
            return nw3.b();
        }
        return null;
    }

    public static void b(sw3 sw3Var) {
        ArrayList<String> arrayList = new ArrayList();
        Cursor E0 = sw3Var.E0("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        while (E0.moveToNext()) {
            try {
                arrayList.add(E0.getString(0));
            } catch (Throwable th) {
                E0.close();
                throw th;
            }
        }
        E0.close();
        for (String str : arrayList) {
            if (str.startsWith("room_fts_content_sync_")) {
                sw3Var.K("DROP TRIGGER IF EXISTS " + str);
            }
        }
    }

    public static Cursor c(RoomDatabase roomDatabase, vw3 vw3Var, boolean z, CancellationSignal cancellationSignal) {
        Cursor D = roomDatabase.D(vw3Var, cancellationSignal);
        if (z && (D instanceof AbstractWindowedCursor)) {
            AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor) D;
            int count = abstractWindowedCursor.getCount();
            return (Build.VERSION.SDK_INT < 23 || (abstractWindowedCursor.hasWindow() ? abstractWindowedCursor.getWindow().getNumRows() : count) < count) ? wb0.a(abstractWindowedCursor) : D;
        }
        return D;
    }

    public static int d(File file) throws IOException {
        FileChannel fileChannel = null;
        try {
            ByteBuffer allocate = ByteBuffer.allocate(4);
            FileChannel channel = new FileInputStream(file).getChannel();
            channel.tryLock(60L, 4L, true);
            channel.position(60L);
            if (channel.read(allocate) == 4) {
                allocate.rewind();
                int i = allocate.getInt();
                channel.close();
                return i;
            }
            throw new IOException("Bad database header, unable to read 4 bytes at offset 60");
        } catch (Throwable th) {
            if (0 != 0) {
                fileChannel.close();
            }
            throw th;
        }
    }
}
