package defpackage;

import android.content.Context;
import android.net.Uri;
import com.facebook.common.internal.ImmutableList;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.util.Set;

/* compiled from: PipelineDraweeControllerBuilder.java */
/* renamed from: xq2  reason: default package */
/* loaded from: classes.dex */
public class xq2 extends AbstractDraweeControllerBuilder<xq2, ImageRequest, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, ao1> {
    public final qo1 t;
    public final zq2 u;
    public ImmutableList<uq0> v;
    public fo1 w;
    public ko1 x;

    /* compiled from: PipelineDraweeControllerBuilder.java */
    /* renamed from: xq2$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[AbstractDraweeControllerBuilder.CacheLevel.values().length];
            a = iArr;
            try {
                iArr[AbstractDraweeControllerBuilder.CacheLevel.FULL_FETCH.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[AbstractDraweeControllerBuilder.CacheLevel.DISK_CACHE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[AbstractDraweeControllerBuilder.CacheLevel.BITMAP_MEMORY_CACHE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public xq2(Context context, zq2 zq2Var, qo1 qo1Var, Set<m80> set, Set<l80> set2) {
        super(context, set, set2);
        this.t = qo1Var;
        this.u = zq2Var;
    }

    public static ImageRequest.RequestLevel E(AbstractDraweeControllerBuilder.CacheLevel cacheLevel) {
        int i = a.a[cacheLevel.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return ImageRequest.RequestLevel.BITMAP_MEMORY_CACHE;
                }
                throw new RuntimeException("Cache level" + cacheLevel + "is not supported. ");
            }
            return ImageRequest.RequestLevel.DISK_CACHE;
        }
        return ImageRequest.RequestLevel.FULL_FETCH;
    }

    public final wt F() {
        ImageRequest n = n();
        xt d = this.t.d();
        if (d == null || n == null) {
            return null;
        }
        if (n.k() != null) {
            return d.c(n, f());
        }
        return d.a(n, f());
    }

    @Override // com.facebook.drawee.controller.AbstractDraweeControllerBuilder
    /* renamed from: G */
    public ge0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> i(ir0 ir0Var, String str, ImageRequest imageRequest, Object obj, AbstractDraweeControllerBuilder.CacheLevel cacheLevel) {
        return this.t.a(imageRequest, obj, E(cacheLevel), H(ir0Var), str);
    }

    public h73 H(ir0 ir0Var) {
        if (ir0Var instanceof wq2) {
            return ((wq2) ir0Var).p0();
        }
        return null;
    }

    @Override // com.facebook.drawee.controller.AbstractDraweeControllerBuilder
    /* renamed from: I */
    public wq2 w() {
        wq2 c;
        if (nc1.d()) {
            nc1.a("PipelineDraweeControllerBuilder#obtainController");
        }
        try {
            ir0 p = p();
            String e = AbstractDraweeControllerBuilder.e();
            if (p instanceof wq2) {
                c = (wq2) p;
            } else {
                c = this.u.c();
            }
            c.r0(x(c, e), e, F(), f(), this.v, this.w);
            c.s0(this.x, this, gw3.a);
            return c;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public xq2 J(ko1 ko1Var) {
        this.x = ko1Var;
        return r();
    }

    @Override // defpackage.bp3
    /* renamed from: K */
    public xq2 a(Uri uri) {
        if (uri == null) {
            return (xq2) super.B(null);
        }
        return (xq2) super.B(ImageRequestBuilder.u(uri).I(p93.b()).a());
    }
}
