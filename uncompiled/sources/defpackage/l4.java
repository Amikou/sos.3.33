package defpackage;

import androidx.media3.common.ParserException;

/* compiled from: AacUtil.java */
/* renamed from: l4  reason: default package */
/* loaded from: classes.dex */
public final class l4 {
    public static final int[] a = {96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350};
    public static final int[] b = {0, 1, 2, 3, 4, 5, 6, 8, -1, -1, -1, 7, 8, -1, 8, -1};

    /* compiled from: AacUtil.java */
    /* renamed from: l4$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final int b;
        public final String c;

        public b(int i, int i2, String str) {
            this.a = i;
            this.b = i2;
            this.c = str;
        }
    }

    public static byte[] a(int i, int i2, int i3) {
        return new byte[]{(byte) (((i << 3) & 248) | ((i2 >> 1) & 7)), (byte) (((i2 << 7) & 128) | ((i3 << 3) & 120))};
    }

    public static int b(np2 np2Var) {
        int h = np2Var.h(5);
        return h == 31 ? np2Var.h(6) + 32 : h;
    }

    public static int c(np2 np2Var) throws ParserException {
        int h = np2Var.h(4);
        if (h == 15) {
            return np2Var.h(24);
        }
        if (h < 13) {
            return a[h];
        }
        throw ParserException.createForMalformedContainer(null, null);
    }

    public static b d(np2 np2Var, boolean z) throws ParserException {
        int b2 = b(np2Var);
        int c = c(np2Var);
        int h = np2Var.h(4);
        String str = "mp4a.40." + b2;
        if (b2 == 5 || b2 == 29) {
            c = c(np2Var);
            b2 = b(np2Var);
            if (b2 == 22) {
                h = np2Var.h(4);
            }
        }
        if (z) {
            if (b2 != 1 && b2 != 2 && b2 != 3 && b2 != 4 && b2 != 6 && b2 != 7 && b2 != 17) {
                switch (b2) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                        break;
                    default:
                        throw ParserException.createForUnsupportedContainerFeature("Unsupported audio object type: " + b2);
                }
            }
            f(np2Var, b2, h);
            switch (b2) {
                case 17:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    int h2 = np2Var.h(2);
                    if (h2 == 2 || h2 == 3) {
                        throw ParserException.createForUnsupportedContainerFeature("Unsupported epConfig: " + h2);
                    }
            }
        }
        int i = b[h];
        if (i != -1) {
            return new b(c, i, str);
        }
        throw ParserException.createForMalformedContainer(null, null);
    }

    public static b e(byte[] bArr) throws ParserException {
        return d(new np2(bArr), false);
    }

    public static void f(np2 np2Var, int i, int i2) {
        if (np2Var.g()) {
            p12.i("AacUtil", "Unexpected frameLengthFlag = 1");
        }
        if (np2Var.g()) {
            np2Var.r(14);
        }
        boolean g = np2Var.g();
        if (i2 == 0) {
            throw new UnsupportedOperationException();
        }
        if (i == 6 || i == 20) {
            np2Var.r(3);
        }
        if (g) {
            if (i == 22) {
                np2Var.r(16);
            }
            if (i == 17 || i == 19 || i == 20 || i == 23) {
                np2Var.r(3);
            }
            np2Var.r(1);
        }
    }
}
