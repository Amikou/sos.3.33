package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: ig3  reason: default package */
/* loaded from: classes2.dex */
public class ig3 extends xs0.b {
    public jg3 j;

    /* renamed from: ig3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] h = cd2.h();
            long[] h2 = cd2.h();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                long j = ((i3 ^ i) - 1) >> 31;
                for (int i4 = 0; i4 < 3; i4++) {
                    long j2 = h[i4];
                    long[] jArr = this.b;
                    h[i4] = j2 ^ (jArr[i2 + i4] & j);
                    h2[i4] = h2[i4] ^ (jArr[(i2 + 3) + i4] & j);
                }
                i2 += 6;
            }
            return ig3.this.i(new hg3(h), new hg3(h2), false);
        }
    }

    public ig3() {
        super(163, 3, 6, 7);
        this.j = new jg3(this, null, null);
        ct0 n = n(BigInteger.valueOf(1L));
        this.b = n;
        this.c = n;
        this.d = new BigInteger(1, pk1.a("04000000000000000000020108A2E0CC0D99F8A5EF"));
        this.e = BigInteger.valueOf(2L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return true;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new ig3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 3 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            cd2.f(((hg3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 3;
            cd2.f(((hg3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 3;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public it0 f() {
        return new ll4();
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new jg3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new jg3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new hg3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 163;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
