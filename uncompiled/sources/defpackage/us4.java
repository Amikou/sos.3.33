package defpackage;

import com.google.android.play.core.assetpacks.AssetPackExtractionService;
import com.google.android.play.core.assetpacks.ExtractionForegroundService;

/* renamed from: us4  reason: default package */
/* loaded from: classes2.dex */
public interface us4 {
    void a(ExtractionForegroundService extractionForegroundService);

    void b(AssetPackExtractionService assetPackExtractionService);
}
