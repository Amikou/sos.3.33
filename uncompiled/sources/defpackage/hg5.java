package defpackage;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: hg5  reason: default package */
/* loaded from: classes.dex */
public final class hg5 extends fg5<FieldDescriptorType, Object> {
    public hg5(int i) {
        super(i, null);
    }

    @Override // defpackage.fg5
    public final void q() {
        if (!a()) {
            for (int i = 0; i < l(); i++) {
                Map.Entry<FieldDescriptorType, Object> g = g(i);
                if (((ta5) g.getKey()).J0()) {
                    g.setValue(Collections.unmodifiableList((List) g.getValue()));
                }
            }
            Iterator it = m().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (((ta5) entry.getKey()).J0()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.q();
    }
}
