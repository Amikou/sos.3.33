package defpackage;

import android.os.Bundle;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: ga5  reason: default package */
/* loaded from: classes2.dex */
public final class ga5 implements fp5 {
    public final /* synthetic */ wf5 a;

    public ga5(wf5 wf5Var) {
        this.a = wf5Var;
    }

    @Override // defpackage.fp5
    public final long a() {
        return this.a.G();
    }

    @Override // defpackage.fp5
    public final String f() {
        return this.a.a();
    }

    @Override // defpackage.fp5
    public final String g() {
        return this.a.F();
    }

    @Override // defpackage.fp5
    public final String h() {
        return this.a.H();
    }

    @Override // defpackage.fp5
    public final void i(String str, String str2, Bundle bundle) {
        this.a.w(str, str2, bundle);
    }

    @Override // defpackage.fp5
    public final String l() {
        return this.a.E();
    }

    @Override // defpackage.fp5
    public final void m(String str) {
        this.a.C(str);
    }

    @Override // defpackage.fp5
    public final List<Bundle> n(String str, String str2) {
        return this.a.A(str, str2);
    }

    @Override // defpackage.fp5
    public final void o(Bundle bundle) {
        this.a.y(bundle);
    }

    @Override // defpackage.fp5
    public final void p(String str) {
        this.a.D(str);
    }

    @Override // defpackage.fp5
    public final void q(String str, String str2, Bundle bundle) {
        this.a.z(str, str2, bundle);
    }

    @Override // defpackage.fp5
    public final int r(String str) {
        return this.a.d(str);
    }

    @Override // defpackage.fp5
    public final Map<String, Object> s(String str, String str2, boolean z) {
        return this.a.b(str, str2, z);
    }
}
