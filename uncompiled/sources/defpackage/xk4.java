package defpackage;

import kotlin.text.StringsKt__IndentKt;

/* compiled from: ViewportHint.kt */
/* renamed from: xk4  reason: default package */
/* loaded from: classes.dex */
public abstract class xk4 {
    public final int a;
    public final int b;
    public final int c;
    public final int d;

    /* compiled from: ViewportHint.kt */
    /* renamed from: xk4$a */
    /* loaded from: classes.dex */
    public static final class a extends xk4 {
        public final int e;
        public final int f;

        public a(int i, int i2, int i3, int i4, int i5, int i6) {
            super(i3, i4, i5, i6, null);
            this.e = i;
            this.f = i2;
        }

        public final int e() {
            return this.f;
        }

        @Override // defpackage.xk4
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.e == aVar.e && this.f == aVar.f && d() == aVar.d() && c() == aVar.c() && a() == aVar.a() && b() == aVar.b();
            }
            return false;
        }

        public final int f() {
            return this.e;
        }

        @Override // defpackage.xk4
        public int hashCode() {
            return super.hashCode() + this.e + this.f;
        }

        public String toString() {
            return StringsKt__IndentKt.h("ViewportHint.Access(\n            |    pageOffset=" + this.e + ",\n            |    indexInPage=" + this.f + ",\n            |    presentedItemsBefore=" + d() + ",\n            |    presentedItemsAfter=" + c() + ",\n            |    originalPageOffsetFirst=" + a() + ",\n            |    originalPageOffsetLast=" + b() + ",\n            |)", null, 1, null);
        }
    }

    /* compiled from: ViewportHint.kt */
    /* renamed from: xk4$b */
    /* loaded from: classes.dex */
    public static final class b extends xk4 {
        public b(int i, int i2, int i3, int i4) {
            super(i, i2, i3, i4, null);
        }

        public String toString() {
            return StringsKt__IndentKt.h("ViewportHint.Initial(\n            |    presentedItemsBefore=" + d() + ",\n            |    presentedItemsAfter=" + c() + ",\n            |    originalPageOffsetFirst=" + a() + ",\n            |    originalPageOffsetLast=" + b() + ",\n            |)", null, 1, null);
        }
    }

    public xk4(int i, int i2, int i3, int i4) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public final int a() {
        return this.c;
    }

    public final int b() {
        return this.d;
    }

    public final int c() {
        return this.b;
    }

    public final int d() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof xk4) {
            xk4 xk4Var = (xk4) obj;
            return this.a == xk4Var.a && this.b == xk4Var.b && this.c == xk4Var.c && this.d == xk4Var.d;
        }
        return false;
    }

    public int hashCode() {
        return this.a + this.b + this.c + this.d;
    }

    public /* synthetic */ xk4(int i, int i2, int i3, int i4, qi0 qi0Var) {
        this(i, i2, i3, i4);
    }
}
