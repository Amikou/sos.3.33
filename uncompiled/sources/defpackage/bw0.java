package defpackage;

import android.util.Pair;
import java.lang.Throwable;

/* compiled from: ErrorMessageProvider.java */
/* renamed from: bw0  reason: default package */
/* loaded from: classes.dex */
public interface bw0<T extends Throwable> {
    Pair<Integer, String> a(T t);
}
