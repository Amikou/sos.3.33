package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: l33  reason: default package */
/* loaded from: classes2.dex */
public class l33 extends h {
    public g a;
    public i f0;
    public g g0;
    public byte[][] h0;
    public byte[][] i0;
    public byte[] j0;

    public l33(int i, short[][] sArr, short[][] sArr2, short[] sArr3) {
        this.a = new g(0L);
        this.g0 = new g(i);
        this.h0 = o33.c(sArr);
        this.i0 = o33.c(sArr2);
        this.j0 = o33.a(sArr3);
    }

    public l33(h4 h4Var) {
        if (h4Var.D(0) instanceof g) {
            this.a = g.z(h4Var.D(0));
        } else {
            this.f0 = i.G(h4Var.D(0));
        }
        this.g0 = g.z(h4Var.D(1));
        h4 z = h4.z(h4Var.D(2));
        this.h0 = new byte[z.size()];
        for (int i = 0; i < z.size(); i++) {
            this.h0[i] = f4.B(z.D(i)).D();
        }
        h4 h4Var2 = (h4) h4Var.D(3);
        this.i0 = new byte[h4Var2.size()];
        for (int i2 = 0; i2 < h4Var2.size(); i2++) {
            this.i0[i2] = f4.B(h4Var2.D(i2)).D();
        }
        this.j0 = f4.B(((h4) h4Var.D(4)).D(0)).D();
    }

    public static l33 t(Object obj) {
        if (obj instanceof l33) {
            return (l33) obj;
        }
        if (obj != null) {
            return new l33(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        c4 c4Var = this.a;
        if (c4Var == null) {
            c4Var = this.f0;
        }
        d4Var.a(c4Var);
        d4Var.a(this.g0);
        d4 d4Var2 = new d4();
        int i = 0;
        int i2 = 0;
        while (true) {
            byte[][] bArr = this.h0;
            if (i2 >= bArr.length) {
                break;
            }
            d4Var2.a(new j0(bArr[i2]));
            i2++;
        }
        d4Var.a(new n0(d4Var2));
        d4 d4Var3 = new d4();
        while (true) {
            byte[][] bArr2 = this.i0;
            if (i >= bArr2.length) {
                d4Var.a(new n0(d4Var3));
                d4 d4Var4 = new d4();
                d4Var4.a(new j0(this.j0));
                d4Var.a(new n0(d4Var4));
                return new n0(d4Var);
            }
            d4Var3.a(new j0(bArr2[i]));
            i++;
        }
    }

    public short[][] o() {
        return o33.d(this.h0);
    }

    public short[] p() {
        return o33.b(this.j0);
    }

    public short[][] q() {
        return o33.d(this.i0);
    }

    public int s() {
        return this.g0.B().intValue();
    }
}
