package defpackage;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.os.Binder;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.internal.measurement.a;
import com.google.android.gms.internal.measurement.g;
import com.google.android.gms.internal.measurement.zzd;
import com.google.android.gms.measurement.internal.c;
import com.google.android.gms.measurement.internal.i;
import com.google.android.gms.measurement.internal.j;
import com.google.android.gms.measurement.internal.k;
import com.google.android.gms.measurement.internal.l;
import com.google.android.gms.measurement.internal.m;
import com.google.android.gms.measurement.internal.n;
import com.google.android.gms.measurement.internal.r;
import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzas;
import com.google.android.gms.measurement.internal.zzkq;
import com.google.android.gms.measurement.internal.zzp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: pl5  reason: default package */
/* loaded from: classes.dex */
public final class pl5 extends c {
    public final fw5 a;
    public Boolean b;
    public String c;

    public pl5(fw5 fw5Var, String str) {
        zt2.j(fw5Var);
        this.a = fw5Var;
        this.c = null;
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzkq> B1(String str, String str2, String str3, boolean z) {
        G1(str, true);
        try {
            List<mw5> list = (List) this.a.q().n(new j(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (mw5 mw5Var : list) {
                if (z || !sw5.F(mw5Var.c)) {
                    arrayList.add(new zzkq(mw5Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.w().l().c("Failed to get user properties as. appId", og5.x(str), e);
            return Collections.emptyList();
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void F0(zzp zzpVar) {
        F1(zzpVar, false);
        J1(new dl5(this, zzpVar));
    }

    public final void F1(zzp zzpVar, boolean z) {
        zt2.j(zzpVar);
        zt2.f(zzpVar.a);
        G1(zzpVar.a, false);
        this.a.c0().l(zzpVar.f0, zzpVar.u0, zzpVar.y0);
    }

    public final void G1(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.b == null) {
                        if (!"com.google.android.gms".equals(this.c) && !oe4.a(this.a.m(), Binder.getCallingUid()) && !rh1.a(this.a.m()).c(Binder.getCallingUid())) {
                            z2 = false;
                            this.b = Boolean.valueOf(z2);
                        }
                        z2 = true;
                        this.b = Boolean.valueOf(z2);
                    }
                    if (this.b.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e) {
                    this.a.w().l().b("Measurement Service called with invalid calling package. appId", og5.x(str));
                    throw e;
                }
            }
            if (this.c == null && qh1.k(this.a.m(), Binder.getCallingUid(), str)) {
                this.c = str;
            }
            if (str.equals(this.c)) {
                return;
            }
            throw new SecurityException(String.format("Unknown calling package name '%s'.", str));
        }
        this.a.w().l().a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    public final void H1(zzas zzasVar, zzp zzpVar) {
        if (!this.a.T().p(zzpVar.a)) {
            N1(zzasVar, zzpVar);
            return;
        }
        this.a.w().v().b("EES config found for", zzpVar.a);
        qj5 T = this.a.T();
        String str = zzpVar.a;
        a36.a();
        g gVar = null;
        if (T.a.z().v(null, qf5.B0) && !TextUtils.isEmpty(str)) {
            gVar = T.i.c(str);
        }
        if (gVar != null) {
            try {
                Bundle N1 = zzasVar.f0.N1();
                HashMap hashMap = new HashMap();
                for (String str2 : N1.keySet()) {
                    Object obj = N1.get(str2);
                    if (obj != null) {
                        hashMap.put(str2, obj);
                    }
                }
                String a = yl5.a(zzasVar.a);
                if (a == null) {
                    a = zzasVar.a;
                }
                if (gVar.b(new a(a, zzasVar.h0, hashMap))) {
                    if (gVar.c()) {
                        this.a.w().v().b("EES edited event", zzasVar.a);
                        N1(r.M(gVar.e().c()), zzpVar);
                    } else {
                        N1(zzasVar, zzpVar);
                    }
                    if (gVar.d()) {
                        for (a aVar : gVar.e().f()) {
                            this.a.w().v().b("EES logging created event", aVar.b());
                            N1(r.M(aVar), zzpVar);
                        }
                        return;
                    }
                    return;
                }
            } catch (zzd unused) {
                this.a.w().l().c("EES error. appId, eventName", zzpVar.f0, zzasVar.a);
            }
            this.a.w().v().b("EES was not applied to event", zzasVar.a);
            N1(zzasVar, zzpVar);
            return;
        }
        this.a.w().v().b("EES not loaded for", zzpVar.a);
        N1(zzasVar, zzpVar);
    }

    public final zzas I1(zzas zzasVar, zzp zzpVar) {
        zzaq zzaqVar;
        if ("_cmp".equals(zzasVar.a) && (zzaqVar = zzasVar.f0) != null && zzaqVar.M1() != 0) {
            String L1 = zzasVar.f0.L1("_cis");
            if ("referrer broadcast".equals(L1) || "referrer API".equals(L1)) {
                this.a.w().t().b("Event has been filtered ", zzasVar.toString());
                return new zzas("_cmpx", zzasVar.f0, zzasVar.g0, zzasVar.h0);
            }
        }
        return zzasVar;
    }

    public final void J1(Runnable runnable) {
        zt2.j(runnable);
        if (this.a.q().l()) {
            runnable.run();
        } else {
            this.a.q().p(runnable);
        }
    }

    public final /* synthetic */ void K1(String str, Bundle bundle) {
        e55 V = this.a.V();
        V.e();
        V.g();
        byte[] c = V.b.Z().x(new s55(V.a, "", str, "dep", 0L, 0L, bundle)).c();
        V.a.w().v().c("Saving default event parameters, appId, data size", V.a.H().n(str), Integer.valueOf(c.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("parameters", c);
        try {
            if (V.P().insertWithOnConflict("default_event_params", null, contentValues, 5) == -1) {
                V.a.w().l().b("Failed to insert default event parameters (got -1). appId", og5.x(str));
            }
        } catch (SQLiteException e) {
            V.a.w().l().c("Error storing default event parameters. appId", og5.x(str), e);
        }
    }

    public final void N1(zzas zzasVar, zzp zzpVar) {
        this.a.i();
        this.a.j0(zzasVar, zzpVar);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void P(zzaa zzaaVar, zzp zzpVar) {
        zt2.j(zzaaVar);
        zt2.j(zzaaVar.g0);
        F1(zzpVar, false);
        zzaa zzaaVar2 = new zzaa(zzaaVar);
        zzaaVar2.a = zzpVar.a;
        J1(new gk5(this, zzaaVar2, zzpVar));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void Q(long j, String str, String str2, String str3) {
        J1(new nl5(this, str2, str3, str, j));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzkq> T(zzp zzpVar, boolean z) {
        F1(zzpVar, false);
        String str = zzpVar.a;
        zt2.j(str);
        try {
            List<mw5> list = (List) this.a.q().n(new n(this, str)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (mw5 mw5Var : list) {
                if (z || !sw5.F(mw5Var.c)) {
                    arrayList.add(new zzkq(mw5Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.w().l().c("Failed to get user properties. appId", og5.x(zzpVar.a), e);
            return null;
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzkq> X(String str, String str2, boolean z, zzp zzpVar) {
        F1(zzpVar, false);
        String str3 = zzpVar.a;
        zt2.j(str3);
        try {
            List<mw5> list = (List) this.a.q().n(new i(this, str3, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (mw5 mw5Var : list) {
                if (z || !sw5.F(mw5Var.c)) {
                    arrayList.add(new zzkq(mw5Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.a.w().l().c("Failed to query user properties. appId", og5.x(zzpVar.a), e);
            return Collections.emptyList();
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void X0(zzkq zzkqVar, zzp zzpVar) {
        zt2.j(zzkqVar);
        F1(zzpVar, false);
        J1(new jl5(this, zzkqVar, zzpVar));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzaa> Y(String str, String str2, String str3) {
        G1(str, true);
        try {
            return (List) this.a.q().n(new l(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.a.w().l().b("Failed to get conditional user properties as", e);
            return Collections.emptyList();
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void e0(zzp zzpVar) {
        zt2.f(zzpVar.a);
        G1(zzpVar.a, false);
        J1(new bl5(this, zzpVar));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void f1(zzp zzpVar) {
        F1(zzpVar, false);
        J1(new ml5(this, zzpVar));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final List<zzaa> g(String str, String str2, zzp zzpVar) {
        F1(zzpVar, false);
        String str3 = zzpVar.a;
        zt2.j(str3);
        try {
            return (List) this.a.q().n(new k(this, str3, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.a.w().l().b("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void h0(final Bundle bundle, zzp zzpVar) {
        F1(zzpVar, false);
        final String str = zzpVar.a;
        zt2.j(str);
        J1(new Runnable(this, str, bundle) { // from class: ek5
            public final pl5 a;
            public final String f0;
            public final Bundle g0;

            {
                this.a = this;
                this.f0 = str;
                this.g0 = bundle;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.K1(this.f0, this.g0);
            }
        });
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void i0(zzaa zzaaVar) {
        zt2.j(zzaaVar);
        zt2.j(zzaaVar.g0);
        zt2.f(zzaaVar.a);
        G1(zzaaVar.a, true);
        J1(new hk5(this, new zzaa(zzaaVar)));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void j0(zzas zzasVar, String str, String str2) {
        zt2.j(zzasVar);
        zt2.f(str);
        G1(str, true);
        J1(new hl5(this, zzasVar, str));
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final byte[] l0(zzas zzasVar, String str) {
        zt2.f(str);
        zt2.j(zzasVar);
        G1(str, true);
        this.a.w().u().b("Log and bundle. event", this.a.b0().n(zzasVar.a));
        long nanoTime = this.a.a().nanoTime() / 1000000;
        try {
            byte[] bArr = (byte[]) this.a.q().o(new m(this, zzasVar, str)).get();
            if (bArr == null) {
                this.a.w().l().b("Log and bundle returned null. appId", og5.x(str));
                bArr = new byte[0];
            }
            this.a.w().u().d("Log and bundle processed. event, size, time_ms", this.a.b0().n(zzasVar.a), Integer.valueOf(bArr.length), Long.valueOf((this.a.a().nanoTime() / 1000000) - nanoTime));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.a.w().l().d("Failed to log and bundle. appId, event, error", og5.x(str), this.a.b0().n(zzasVar.a), e);
            return null;
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void o(zzp zzpVar) {
        zt2.f(zzpVar.a);
        zt2.j(zzpVar.z0);
        el5 el5Var = new el5(this, zzpVar);
        zt2.j(el5Var);
        if (this.a.q().l()) {
            el5Var.run();
        } else {
            this.a.q().s(el5Var);
        }
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final String u(zzp zzpVar) {
        F1(zzpVar, false);
        return this.a.z(zzpVar);
    }

    @Override // com.google.android.gms.measurement.internal.d
    public final void w1(zzas zzasVar, zzp zzpVar) {
        zt2.j(zzasVar);
        F1(zzpVar, false);
        J1(new gl5(this, zzasVar, zzpVar));
    }
}
