package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;

/* compiled from: SequenceBuilder.kt */
/* renamed from: pl3  reason: default package */
/* loaded from: classes2.dex */
public final class pl3<T> extends ql3<T> implements Iterator<T>, q70<te4>, tw1 {
    public int a;
    public T f0;
    public Iterator<? extends T> g0;
    public q70<? super te4> h0;

    @Override // defpackage.ql3
    public Object a(T t, q70<? super te4> q70Var) {
        this.f0 = t;
        this.a = 3;
        this.h0 = q70Var;
        Object d = gs1.d();
        if (d == gs1.d()) {
            ef0.c(q70Var);
        }
        return d == gs1.d() ? d : te4.a;
    }

    public final Throwable b() {
        int i = this.a;
        if (i != 4) {
            if (i != 5) {
                return new IllegalStateException("Unexpected state of the iterator: " + this.a);
            }
            return new IllegalStateException("Iterator has failed.");
        }
        return new NoSuchElementException();
    }

    public final T d() {
        if (hasNext()) {
            return next();
        }
        throw new NoSuchElementException();
    }

    public final void g(q70<? super te4> q70Var) {
        this.h0 = q70Var;
    }

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        return EmptyCoroutineContext.INSTANCE;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        while (true) {
            int i = this.a;
            if (i != 0) {
                if (i != 1) {
                    if (i == 2 || i == 3) {
                        return true;
                    }
                    if (i == 4) {
                        return false;
                    }
                    throw b();
                }
                Iterator<? extends T> it = this.g0;
                fs1.d(it);
                if (it.hasNext()) {
                    this.a = 2;
                    return true;
                }
                this.g0 = null;
            }
            this.a = 5;
            q70<? super te4> q70Var = this.h0;
            fs1.d(q70Var);
            this.h0 = null;
            te4 te4Var = te4.a;
            Result.a aVar = Result.Companion;
            q70Var.resumeWith(Result.m52constructorimpl(te4Var));
        }
    }

    @Override // java.util.Iterator
    public T next() {
        int i = this.a;
        if (i == 0 || i == 1) {
            return d();
        }
        if (i == 2) {
            this.a = 1;
            Iterator<? extends T> it = this.g0;
            fs1.d(it);
            return it.next();
        } else if (i == 3) {
            this.a = 0;
            T t = this.f0;
            this.f0 = null;
            return t;
        } else {
            throw b();
        }
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
        o83.b(obj);
        this.a = 4;
    }
}
