package defpackage;

import androidx.media3.common.util.b;

/* compiled from: AviStreamHeaderChunk.java */
/* renamed from: vl  reason: default package */
/* loaded from: classes.dex */
public final class vl implements sl {
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;

    public vl(int i, int i2, int i3, int i4, int i5, int i6) {
        this.a = i;
        this.b = i3;
        this.c = i4;
        this.d = i5;
        this.e = i6;
    }

    public static vl c(op2 op2Var) {
        int q = op2Var.q();
        op2Var.Q(12);
        int q2 = op2Var.q();
        int q3 = op2Var.q();
        int q4 = op2Var.q();
        op2Var.Q(4);
        int q5 = op2Var.q();
        int q6 = op2Var.q();
        op2Var.Q(8);
        return new vl(q, q2, q3, q4, q5, q6);
    }

    public long a() {
        return b.J0(this.d, this.b * 1000000, this.c);
    }

    public int b() {
        int i = this.a;
        if (i != 1935960438) {
            if (i != 1935963489) {
                if (i != 1937012852) {
                    p12.i("AviStreamHeaderChunk", "Found unsupported streamType fourCC: " + Integer.toHexString(this.a));
                    return -1;
                }
                return 3;
            }
            return 1;
        }
        return 2;
    }

    @Override // defpackage.sl
    public int getType() {
        return 1752331379;
    }
}
