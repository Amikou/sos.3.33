package defpackage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSwapTransaction.kt */
/* renamed from: fo0  reason: default package */
/* loaded from: classes2.dex */
public final class fo0 {
    public static final fo0 a = new fo0();

    public static final void e(WeakReference<Activity> weakReference, final String str, DialogInterface.OnDismissListener onDismissListener) {
        fs1.f(weakReference, "activityReference");
        fs1.f(str, "transactionId");
        fs1.f(onDismissListener, "dismissListner");
        Activity activity = weakReference.get();
        fs1.d(activity);
        fs1.e(activity, "activityReference.get()!!");
        final Activity activity2 = activity;
        final Dialog d = a.d(activity2);
        go0 a2 = go0.a(activity2.getLayoutInflater().inflate(R.layout.dialog_swap_transaction, (ViewGroup) null));
        fs1.e(a2, "bind(activity.layoutInfl…_swap_transaction, null))");
        a2.e.setText(activity2.getString(R.string.title_track_transaction));
        a2.c.setText(str);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: co0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                fo0.f(activity2, str, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: do0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                fo0.g(activity2, str, view);
            }
        });
        a2.d.setOnClickListener(new View.OnClickListener() { // from class: eo0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                fo0.h(d, view);
            }
        });
        d.setContentView(a2.b());
        d.setOnDismissListener(onDismissListener);
        d.show();
    }

    public static final void f(Activity activity, String str, View view) {
        fs1.f(activity, "$activity");
        fs1.f(str, "$transactionId");
        activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public static final void g(Activity activity, String str, View view) {
        fs1.f(activity, "$activity");
        fs1.f(str, "$transactionId");
        e30.g(activity, str);
    }

    public static final void h(Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        dialog.dismiss();
    }

    public final Dialog d(Context context) {
        Dialog dialog = new Dialog(context, 2132017235);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.getAttributes().gravity = 17;
            window.getAttributes().width = -1;
        }
        return dialog;
    }
}
