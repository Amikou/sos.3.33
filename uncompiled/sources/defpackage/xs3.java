package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;
import kotlinx.coroutines.flow.StateFlowImpl;

/* compiled from: StateFlow.kt */
/* renamed from: xs3  reason: default package */
/* loaded from: classes2.dex */
public final class xs3 {
    public static final k24 a = new k24("NONE");
    public static final k24 b = new k24("PENDING");

    public static final <T> jb2<T> a(T t) {
        if (t == null) {
            t = (T) xi2.a;
        }
        return new StateFlowImpl(t);
    }

    public static final <T> j71<T> d(ws3<? extends T> ws3Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        boolean z = true;
        if (ze0.a()) {
            if (!(i != -1)) {
                throw new AssertionError();
            }
        }
        if (i < 0 || i > 1) {
            z = false;
        }
        return ((z || i == -2) && bufferOverflow == BufferOverflow.DROP_OLDEST) ? ws3Var : vn3.e(ws3Var, coroutineContext, i, bufferOverflow);
    }

    public static final void e(jb2<Integer> jb2Var, int i) {
        int intValue;
        do {
            intValue = jb2Var.getValue().intValue();
        } while (!jb2Var.b(Integer.valueOf(intValue), Integer.valueOf(intValue + i)));
    }
}
