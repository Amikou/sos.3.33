package defpackage;

import java.nio.charset.Charset;
import java.security.MessageDigest;

/* compiled from: Key.java */
/* renamed from: fx1  reason: default package */
/* loaded from: classes.dex */
public interface fx1 {
    public static final Charset a = Charset.forName("UTF-8");

    void b(MessageDigest messageDigest);

    boolean equals(Object obj);

    int hashCode();
}
