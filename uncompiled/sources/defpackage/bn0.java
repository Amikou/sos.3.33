package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAnchorChainBalanceBinding.java */
/* renamed from: bn0  reason: default package */
/* loaded from: classes2.dex */
public final class bn0 {
    public final MaterialButton a;
    public final MaterialButton b;

    public bn0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, AppCompatTextView appCompatTextView) {
        this.a = materialButton;
        this.b = materialButton2;
    }

    public static bn0 a(View view) {
        int i = R.id.btnAllChain;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAllChain);
        if (materialButton != null) {
            i = R.id.btnSelectedChain;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnSelectedChain);
            if (materialButton2 != null) {
                i = R.id.txtTitle;
                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtTitle);
                if (appCompatTextView != null) {
                    return new bn0((ConstraintLayout) view, materialButton, materialButton2, appCompatTextView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
