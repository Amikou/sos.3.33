package defpackage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/* compiled from: NotificationParams.java */
@JsonIgnoreProperties(ignoreUnknown = true)
/* renamed from: ri2  reason: default package */
/* loaded from: classes3.dex */
public class ri2<T> {
    private T result;
    private String subsciption;

    public T getResult() {
        return this.result;
    }

    public String getSubsciption() {
        return this.subsciption;
    }
}
