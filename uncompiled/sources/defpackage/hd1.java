package defpackage;

/* compiled from: Functions.kt */
/* renamed from: hd1  reason: default package */
/* loaded from: classes2.dex */
public interface hd1<P1, P2, R> extends pd1<R> {
    R invoke(P1 p1, P2 p2);
}
