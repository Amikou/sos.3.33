package defpackage;

import defpackage.st1;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;

/* compiled from: Job.kt */
/* renamed from: zt1 */
/* loaded from: classes2.dex */
public final /* synthetic */ class zt1 {
    public static final q30 a(st1 st1Var) {
        return new ut1(st1Var);
    }

    public static /* synthetic */ q30 b(st1 st1Var, int i, Object obj) {
        if ((i & 1) != 0) {
            st1Var = null;
        }
        return xt1.a(st1Var);
    }

    public static final void c(st1 st1Var, String str, Throwable th) {
        st1Var.a(ny0.a(str, th));
    }

    public static final void d(CoroutineContext coroutineContext, CancellationException cancellationException) {
        st1 st1Var = (st1) coroutineContext.get(st1.f);
        if (st1Var == null) {
            return;
        }
        st1Var.a(cancellationException);
    }

    public static /* synthetic */ void e(st1 st1Var, String str, Throwable th, int i, Object obj) {
        if ((i & 2) != 0) {
            th = null;
        }
        xt1.c(st1Var, str, th);
    }

    public static /* synthetic */ void f(CoroutineContext coroutineContext, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        xt1.d(coroutineContext, cancellationException);
    }

    public static final Object g(st1 st1Var, q70<? super te4> q70Var) {
        st1.a.a(st1Var, null, 1, null);
        Object F = st1Var.F(q70Var);
        return F == gs1.d() ? F : te4.a;
    }

    public static final void h(st1 st1Var) {
        if (!st1Var.b()) {
            throw st1Var.g();
        }
    }

    public static final void i(CoroutineContext coroutineContext) {
        st1 st1Var = (st1) coroutineContext.get(st1.f);
        if (st1Var == null) {
            return;
        }
        xt1.i(st1Var);
    }
}
