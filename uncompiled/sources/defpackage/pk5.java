package defpackage;

import defpackage.kb;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: pk5  reason: default package */
/* loaded from: classes2.dex */
public final class pk5 {
    public final kb.b a;
    public final tf b;
    public final yh5 c;

    public pk5(tf tfVar, kb.b bVar) {
        this.a = bVar;
        this.b = tfVar;
        yh5 yh5Var = new yh5(this);
        this.c = yh5Var;
        tfVar.b(yh5Var);
    }
}
