package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.os.SystemClock;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* renamed from: ba  reason: default package */
/* loaded from: classes.dex */
public class ba {
    public com.google.android.gms.common.a a;
    public jf5 b;
    public boolean c;
    public final Object d = new Object();
    public b e;
    public final Context f;
    public final boolean g;
    public final long h;

    /* renamed from: ba$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final String a;
        public final boolean b;

        public a(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        public final String a() {
            return this.a;
        }

        public final boolean b() {
            return this.b;
        }

        public final String toString() {
            String str = this.a;
            boolean z = this.b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
            sb.append("{");
            sb.append(str);
            sb.append("}");
            sb.append(z);
            return sb.toString();
        }
    }

    /* renamed from: ba$b */
    /* loaded from: classes.dex */
    public static class b extends Thread {
        public WeakReference<ba> a;
        public long f0;
        public CountDownLatch g0 = new CountDownLatch(1);
        public boolean h0 = false;

        public b(ba baVar, long j) {
            this.a = new WeakReference<>(baVar);
            this.f0 = j;
            start();
        }

        public final void a() {
            ba baVar = this.a.get();
            if (baVar != null) {
                baVar.a();
                this.h0 = true;
            }
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public final void run() {
            try {
                if (this.g0.await(this.f0, TimeUnit.MILLISECONDS)) {
                    return;
                }
                a();
            } catch (InterruptedException unused) {
                a();
            }
        }
    }

    public ba(Context context, long j, boolean z, boolean z2) {
        Context applicationContext;
        zt2.j(context);
        if (z && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.f = context;
        this.c = false;
        this.h = j;
        this.g = z2;
    }

    public static a b(Context context) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        u75 u75Var = new u75(context);
        boolean a2 = u75Var.a("gads:ad_id_app_context:enabled", false);
        float b2 = u75Var.b("gads:ad_id_app_context:ping_ratio", Utils.FLOAT_EPSILON);
        String c = u75Var.c("gads:ad_id_use_shared_preference:experiment_id", "");
        ba baVar = new ba(context, -1L, a2, u75Var.a("gads:ad_id_use_persistent_service:enabled", false));
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            baVar.h(false);
            a c2 = baVar.c();
            baVar.i(c2, a2, b2, SystemClock.elapsedRealtime() - elapsedRealtime, c, null);
            return c2;
        } finally {
        }
    }

    public static void d(boolean z) {
    }

    public static com.google.android.gms.common.a e(Context context, boolean z) throws IOException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            int j = eh1.h().j(context, qh1.a);
            if (j == 0 || j == 2) {
                String str = z ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                com.google.android.gms.common.a aVar = new com.google.android.gms.common.a();
                Intent intent = new Intent(str);
                intent.setPackage("com.google.android.gms");
                try {
                    if (v50.b().a(context, intent, aVar, 1)) {
                        return aVar;
                    }
                    throw new IOException("Connection failure");
                } catch (Throwable th) {
                    throw new IOException(th);
                }
            }
            throw new IOException("Google Play services not available");
        } catch (PackageManager.NameNotFoundException unused) {
            throw new GooglePlayServicesNotAvailableException(9);
        }
    }

    public static jf5 f(Context context, com.google.android.gms.common.a aVar) throws IOException {
        try {
            return di5.b(aVar.a(10000L, TimeUnit.MILLISECONDS));
        } catch (InterruptedException unused) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    public final void a() {
        zt2.i("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.f == null || this.a == null) {
                return;
            }
            try {
                if (this.c) {
                    v50.b().c(this.f, this.a);
                }
            } catch (Throwable unused) {
            }
            this.c = false;
            this.b = null;
            this.a = null;
        }
    }

    public a c() throws IOException {
        a aVar;
        zt2.i("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (!this.c) {
                synchronized (this.d) {
                    b bVar = this.e;
                    if (bVar == null || !bVar.h0) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                try {
                    h(false);
                    if (!this.c) {
                        throw new IOException("AdvertisingIdClient cannot reconnect.");
                    }
                } catch (Exception e) {
                    throw new IOException("AdvertisingIdClient cannot reconnect.", e);
                }
            }
            zt2.j(this.a);
            zt2.j(this.b);
            try {
                aVar = new a(this.b.getId(), this.b.O(true));
            } catch (RemoteException unused) {
                throw new IOException("Remote exception");
            }
        }
        g();
        return aVar;
    }

    public void finalize() throws Throwable {
        a();
        super.finalize();
    }

    public final void g() {
        synchronized (this.d) {
            b bVar = this.e;
            if (bVar != null) {
                bVar.g0.countDown();
                try {
                    this.e.join();
                } catch (InterruptedException unused) {
                }
            }
            if (this.h > 0) {
                this.e = new b(this, this.h);
            }
        }
    }

    public final void h(boolean z) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        zt2.i("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.c) {
                a();
            }
            com.google.android.gms.common.a e = e(this.f, this.g);
            this.a = e;
            this.b = f(this.f, e);
            this.c = true;
            if (z) {
                g();
            }
        }
    }

    public final boolean i(a aVar, boolean z, float f, long j, String str, Throwable th) {
        if (Math.random() > f) {
            return false;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("app_context", z ? "1" : "0");
        if (aVar != null) {
            hashMap.put("limit_ad_tracking", aVar.b() ? "1" : "0");
        }
        if (aVar != null && aVar.a() != null) {
            hashMap.put("ad_id_size", Integer.toString(aVar.a().length()));
        }
        if (th != null) {
            hashMap.put("error", th.getClass().getName());
        }
        if (str != null && !str.isEmpty()) {
            hashMap.put("experiment_id", str);
        }
        hashMap.put("tag", "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(j));
        new b45(this, hashMap).start();
        return true;
    }
}
