package defpackage;

import java.util.concurrent.Executor;

/* renamed from: l34  reason: default package */
/* loaded from: classes2.dex */
public abstract class l34<ResultT> {
    public abstract l34<ResultT> a(Executor executor, mm2 mm2Var);

    public abstract l34<ResultT> b(Executor executor, tm2<? super ResultT> tm2Var);

    public abstract Exception c();

    public abstract ResultT d();

    public abstract boolean e();

    public abstract boolean f();
}
