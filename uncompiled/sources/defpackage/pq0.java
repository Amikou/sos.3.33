package defpackage;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import defpackage.oq0;

/* compiled from: DrawManager.java */
/* renamed from: pq0  reason: default package */
/* loaded from: classes2.dex */
public class pq0 {
    public mq1 a;
    public oq0 b;
    public q52 c;
    public dj d;

    public pq0() {
        mq1 mq1Var = new mq1();
        this.a = mq1Var;
        this.b = new oq0(mq1Var);
        this.c = new q52();
        this.d = new dj(this.a);
    }

    public void a(Canvas canvas) {
        this.b.a(canvas);
    }

    public mq1 b() {
        if (this.a == null) {
            this.a = new mq1();
        }
        return this.a;
    }

    public void c(Context context, AttributeSet attributeSet) {
        this.d.c(context, attributeSet);
    }

    public Pair<Integer, Integer> d(int i, int i2) {
        return this.c.a(this.a, i, i2);
    }

    public void e(oq0.b bVar) {
        this.b.e(bVar);
    }

    public void f(MotionEvent motionEvent) {
        this.b.f(motionEvent);
    }

    public void g(wg4 wg4Var) {
        this.b.g(wg4Var);
    }
}
