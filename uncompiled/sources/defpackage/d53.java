package defpackage;

import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.MutablePropertyReference0;

/* compiled from: Reflection.java */
/* renamed from: d53  reason: default package */
/* loaded from: classes2.dex */
public class d53 {
    public static final f53 a;
    public static final qw1[] b;

    static {
        f53 f53Var = null;
        try {
            f53Var = (f53) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (f53Var == null) {
            f53Var = new f53();
        }
        a = f53Var;
        b = new qw1[0];
    }

    public static sw1 a(FunctionReference functionReference) {
        return a.a(functionReference);
    }

    public static qw1 b(Class cls) {
        return a.b(cls);
    }

    public static rw1 c(Class cls) {
        return a.c(cls, "");
    }

    public static xw1 d(MutablePropertyReference0 mutablePropertyReference0) {
        return a.d(mutablePropertyReference0);
    }

    public static String e(vd1 vd1Var) {
        return a.e(vd1Var);
    }

    public static String f(Lambda lambda) {
        return a.f(lambda);
    }
}
