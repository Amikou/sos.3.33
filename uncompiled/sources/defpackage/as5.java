package defpackage;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.c;
import com.google.android.gms.internal.clearcut.u0;
import com.google.android.gms.internal.clearcut.v0;

/* renamed from: as5  reason: default package */
/* loaded from: classes.dex */
public final class as5 extends c<u0> {
    public as5(Context context, Looper looper, kz kzVar, GoogleApiClient.b bVar, GoogleApiClient.c cVar) {
        super(context, looper, 40, kzVar, bVar, cVar);
    }

    @Override // com.google.android.gms.common.internal.b
    public final String H() {
        return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
    }

    @Override // com.google.android.gms.common.internal.b
    public final String I() {
        return "com.google.android.gms.clearcut.service.START";
    }

    @Override // com.google.android.gms.common.internal.b, com.google.android.gms.common.api.a.f
    public final int q() {
        return 11925000;
    }

    @Override // com.google.android.gms.common.internal.b
    public final /* synthetic */ IInterface y(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.clearcut.internal.IClearcutLoggerService");
        return queryLocalInterface instanceof u0 ? (u0) queryLocalInterface : new v0(iBinder);
    }
}
