package defpackage;

import java.math.BigInteger;

/* compiled from: EthGetBlockTransactionCountByHash.java */
/* renamed from: tw0  reason: default package */
/* loaded from: classes3.dex */
public class tw0 extends i83<String> {
    public BigInteger getTransactionCount() {
        return ej2.decodeQuantity(getResult());
    }
}
