package defpackage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: CollectionsJVM.kt */
/* renamed from: a20  reason: default package */
/* loaded from: classes2.dex */
public class a20 {
    public static final <T> Object[] a(T[] tArr, boolean z) {
        fs1.f(tArr, "$this$copyToArrayOfAny");
        if (z && fs1.b(tArr.getClass(), Object[].class)) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length, Object[].class);
        fs1.e(copyOf, "java.util.Arrays.copyOf(… Array<Any?>::class.java)");
        return copyOf;
    }

    public static final <T> List<T> b(T t) {
        List<T> singletonList = Collections.singletonList(t);
        fs1.e(singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }
}
