package defpackage;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* compiled from: ArrayDrawable.java */
/* renamed from: nh  reason: default package */
/* loaded from: classes.dex */
public class nh extends Drawable implements Drawable.Callback, wa4, va4 {
    public wa4 a;
    public final Drawable[] g0;
    public final wq0[] h0;
    public final xq0 f0 = new xq0();
    public final Rect i0 = new Rect();
    public boolean j0 = false;
    public boolean k0 = false;
    public boolean l0 = false;

    /* compiled from: ArrayDrawable.java */
    /* renamed from: nh$a */
    /* loaded from: classes.dex */
    public class a implements wq0 {
        public final /* synthetic */ int a;

        public a(int i) {
            this.a = i;
        }

        @Override // defpackage.wq0
        public Drawable f(Drawable drawable) {
            return nh.this.f(this.a, drawable);
        }

        @Override // defpackage.wq0
        public Drawable i() {
            return nh.this.b(this.a);
        }
    }

    public nh(Drawable[] drawableArr) {
        int i = 0;
        xt2.g(drawableArr);
        this.g0 = drawableArr;
        while (true) {
            Drawable[] drawableArr2 = this.g0;
            if (i < drawableArr2.length) {
                br0.d(drawableArr2[i], this, this);
                i++;
            } else {
                this.h0 = new wq0[drawableArr2.length];
                return;
            }
        }
    }

    public final wq0 a(int i) {
        return new a(i);
    }

    public Drawable b(int i) {
        xt2.b(Boolean.valueOf(i >= 0));
        xt2.b(Boolean.valueOf(i < this.g0.length));
        return this.g0[i];
    }

    @Override // defpackage.wa4
    public void c(Matrix matrix) {
        wa4 wa4Var = this.a;
        if (wa4Var != null) {
            wa4Var.c(matrix);
        } else {
            matrix.reset();
        }
    }

    public wq0 d(int i) {
        xt2.b(Boolean.valueOf(i >= 0));
        xt2.b(Boolean.valueOf(i < this.h0.length));
        wq0[] wq0VarArr = this.h0;
        if (wq0VarArr[i] == null) {
            wq0VarArr[i] = a(i);
        }
        return this.h0[i];
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.draw(canvas);
            }
            i++;
        }
    }

    public int e() {
        return this.g0.length;
    }

    public Drawable f(int i, Drawable drawable) {
        xt2.b(Boolean.valueOf(i >= 0));
        xt2.b(Boolean.valueOf(i < this.g0.length));
        Drawable drawable2 = this.g0[i];
        if (drawable != drawable2) {
            if (drawable != null && this.l0) {
                drawable.mutate();
            }
            br0.d(this.g0[i], null, null);
            br0.d(drawable, null, null);
            br0.e(drawable, this.f0);
            br0.a(drawable, this);
            br0.d(drawable, this, this);
            this.k0 = false;
            this.g0[i] = drawable;
            invalidateSelf();
        }
        return drawable2;
    }

    @Override // defpackage.wa4
    public void g(RectF rectF) {
        wa4 wa4Var = this.a;
        if (wa4Var != null) {
            wa4Var.g(rectF);
        } else {
            rectF.set(getBounds());
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        int i = 0;
        int i2 = -1;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                break;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                i2 = Math.max(i2, drawable.getIntrinsicHeight());
            }
            i++;
        }
        if (i2 > 0) {
            return i2;
        }
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        int i = 0;
        int i2 = -1;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                break;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                i2 = Math.max(i2, drawable.getIntrinsicWidth());
            }
            i++;
        }
        if (i2 > 0) {
            return i2;
        }
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        if (this.g0.length == 0) {
            return -2;
        }
        int i = -1;
        int i2 = 1;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i2 >= drawableArr.length) {
                return i;
            }
            Drawable drawable = drawableArr[i2];
            if (drawable != null) {
                i = Drawable.resolveOpacity(i, drawable.getOpacity());
            }
            i2++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        int i = 0;
        rect.left = 0;
        rect.top = 0;
        rect.right = 0;
        rect.bottom = 0;
        Rect rect2 = this.i0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return true;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.getPadding(rect2);
                rect.left = Math.max(rect.left, rect2.left);
                rect.top = Math.max(rect.top, rect2.top);
                rect.right = Math.max(rect.right, rect2.right);
                rect.bottom = Math.max(rect.bottom, rect2.bottom);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        if (!this.k0) {
            this.j0 = false;
            int i = 0;
            while (true) {
                Drawable[] drawableArr = this.g0;
                boolean z = true;
                if (i >= drawableArr.length) {
                    break;
                }
                Drawable drawable = drawableArr[i];
                boolean z2 = this.j0;
                if (drawable == null || !drawable.isStateful()) {
                    z = false;
                }
                this.j0 = z2 | z;
                i++;
            }
            this.k0 = true;
        }
        return this.j0;
    }

    @Override // defpackage.va4
    public void j(wa4 wa4Var) {
        this.a = wa4Var;
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i < drawableArr.length) {
                Drawable drawable = drawableArr[i];
                if (drawable != null) {
                    drawable.mutate();
                }
                i++;
            } else {
                this.l0 = true;
                return this;
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.setBounds(rect);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        int i2 = 0;
        boolean z = false;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i2 >= drawableArr.length) {
                return z;
            }
            Drawable drawable = drawableArr[i2];
            if (drawable != null && drawable.setLevel(i)) {
                z = true;
            }
            i2++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        int i = 0;
        boolean z = false;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return z;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null && drawable.setState(iArr)) {
                z = true;
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.f0.b(i);
        int i2 = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i2 >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i2];
            if (drawable != null) {
                drawable.setAlpha(i);
            }
            i2++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.f0.c(colorFilter);
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.setColorFilter(colorFilter);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setDither(boolean z) {
        this.f0.d(z);
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.setDither(z);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setFilterBitmap(boolean z) {
        this.f0.e(z);
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.setFilterBitmap(z);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    @TargetApi(21)
    public void setHotspot(float f, float f2) {
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.setHotspot(f, f2);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        int i = 0;
        while (true) {
            Drawable[] drawableArr = this.g0;
            if (i >= drawableArr.length) {
                return visible;
            }
            Drawable drawable = drawableArr[i];
            if (drawable != null) {
                drawable.setVisible(z, z2);
            }
            i++;
        }
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
