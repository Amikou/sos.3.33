package defpackage;

import android.content.Context;
import android.net.wifi.WifiManager;

/* compiled from: WifiLockManager.java */
/* renamed from: ep4  reason: default package */
/* loaded from: classes.dex */
public final class ep4 {
    public final WifiManager a;
    public WifiManager.WifiLock b;
    public boolean c;
    public boolean d;

    public ep4(Context context) {
        this.a = (WifiManager) context.getApplicationContext().getSystemService("wifi");
    }

    public void a(boolean z) {
        if (z && this.b == null) {
            WifiManager wifiManager = this.a;
            if (wifiManager == null) {
                p12.i("WifiLockManager", "WifiManager is null, therefore not creating the WifiLock.");
                return;
            }
            WifiManager.WifiLock createWifiLock = wifiManager.createWifiLock(3, "ExoPlayer:WifiLockManager");
            this.b = createWifiLock;
            createWifiLock.setReferenceCounted(false);
        }
        this.c = z;
        c();
    }

    public void b(boolean z) {
        this.d = z;
        c();
    }

    public final void c() {
        WifiManager.WifiLock wifiLock = this.b;
        if (wifiLock == null) {
            return;
        }
        if (this.c && this.d) {
            wifiLock.acquire();
        } else {
            wifiLock.release();
        }
    }
}
