package defpackage;

import com.facebook.common.internal.ImmutableMap;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: PostprocessedBitmapMemoryCacheProducer.java */
/* renamed from: qt2  reason: default package */
/* loaded from: classes.dex */
public class qt2 implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final l72<wt, com.facebook.imagepipeline.image.a> a;
    public final xt b;
    public final dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> c;

    /* compiled from: PostprocessedBitmapMemoryCacheProducer.java */
    /* renamed from: qt2$a */
    /* loaded from: classes.dex */
    public static class a extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final wt c;
        public final boolean d;
        public final l72<wt, com.facebook.imagepipeline.image.a> e;
        public final boolean f;

        public a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, wt wtVar, boolean z, l72<wt, com.facebook.imagepipeline.image.a> l72Var, boolean z2) {
            super(l60Var);
            this.c = wtVar;
            this.d = z;
            this.e = l72Var;
            this.f = z2;
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            if (aVar == null) {
                if (qm.e(i)) {
                    p().d(null, i);
                }
            } else if (!qm.f(i) || this.d) {
                com.facebook.common.references.a<com.facebook.imagepipeline.image.a> c = this.f ? this.e.c(this.c, aVar) : null;
                try {
                    p().c(1.0f);
                    l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> p = p();
                    if (c != null) {
                        aVar = c;
                    }
                    p.d(aVar, i);
                } finally {
                    com.facebook.common.references.a.g(c);
                }
            }
        }
    }

    public qt2(l72<wt, com.facebook.imagepipeline.image.a> l72Var, xt xtVar, dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dv2Var) {
        this.a = l72Var;
        this.b = xtVar;
        this.c = dv2Var;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        iv2 l = ev2Var.l();
        ImageRequest c = ev2Var.c();
        Object a2 = ev2Var.a();
        rt2 k = c.k();
        if (k != null && k.c() != null) {
            l.k(ev2Var, b());
            wt c2 = this.b.c(c, a2);
            com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar = ev2Var.c().x(1) ? this.a.get(c2) : null;
            if (aVar != null) {
                l.a(ev2Var, b(), l.j(ev2Var, b()) ? ImmutableMap.of("cached_value_found", "true") : null);
                l.e(ev2Var, "PostprocessedBitmapMemoryCacheProducer", true);
                ev2Var.f("memory_bitmap", "postprocessed");
                l60Var.c(1.0f);
                l60Var.d(aVar, 1);
                aVar.close();
                return;
            }
            a aVar2 = new a(l60Var, c2, k instanceof z63, this.a, ev2Var.c().x(2));
            l.a(ev2Var, b(), l.j(ev2Var, b()) ? ImmutableMap.of("cached_value_found", "false") : null);
            this.c.a(aVar2, ev2Var);
            return;
        }
        this.c.a(l60Var, ev2Var);
    }

    public String b() {
        return "PostprocessedBitmapMemoryCacheProducer";
    }
}
