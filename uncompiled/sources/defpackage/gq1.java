package defpackage;

import java.security.GeneralSecurityException;

/* compiled from: IndCpaCipher.java */
/* renamed from: gq1  reason: default package */
/* loaded from: classes2.dex */
public interface gq1 {
    byte[] a(byte[] bArr) throws GeneralSecurityException;

    byte[] b(byte[] bArr) throws GeneralSecurityException;
}
