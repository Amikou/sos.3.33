package defpackage;

import com.google.android.gms.internal.measurement.e1;
import com.google.android.gms.internal.measurement.f1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: bk5  reason: default package */
/* loaded from: classes.dex */
public final class bk5 extends w1<e1, bk5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public bk5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.e1 r0 = com.google.android.gms.internal.measurement.e1.A()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bk5.<init>():void");
    }

    public final f1 v(int i) {
        return ((e1) this.f0).y(0);
    }

    public final bk5 x(dk5 dk5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        e1.B((e1) this.f0, dk5Var.o());
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ bk5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.e1 r1 = com.google.android.gms.internal.measurement.e1.A()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bk5.<init>(wi5):void");
    }
}
