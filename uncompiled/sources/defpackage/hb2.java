package defpackage;

import androidx.paging.LoadType;
import defpackage.w02;

/* compiled from: MutableLoadStateCollection.kt */
/* renamed from: hb2  reason: default package */
/* loaded from: classes.dex */
public final class hb2 {
    public w02 a;
    public w02 b;
    public w02 c;
    public x02 d;
    public x02 e;

    public hb2() {
        w02.c.a aVar = w02.c.d;
        this.a = aVar.b();
        this.b = aVar.b();
        this.c = aVar.b();
        this.d = x02.e.a();
    }

    public final w02 c(w02 w02Var, w02 w02Var2, w02 w02Var3, w02 w02Var4) {
        return w02Var4 == null ? w02Var3 : (!(w02Var instanceof w02.b) || ((w02Var2 instanceof w02.c) && (w02Var4 instanceof w02.c)) || (w02Var4 instanceof w02.a)) ? w02Var4 : w02Var;
    }

    public final w02 d(LoadType loadType, boolean z) {
        fs1.f(loadType, "type");
        x02 x02Var = z ? this.e : this.d;
        if (x02Var != null) {
            return x02Var.d(loadType);
        }
        return null;
    }

    public final void e(a30 a30Var) {
        fs1.f(a30Var, "combinedLoadStates");
        this.a = a30Var.e();
        this.b = a30Var.d();
        this.c = a30Var.b();
        this.d = a30Var.f();
        this.e = a30Var.c();
    }

    public final void f(x02 x02Var, x02 x02Var2) {
        fs1.f(x02Var, "sourceLoadStates");
        this.d = x02Var;
        this.e = x02Var2;
        i();
    }

    public final boolean g(LoadType loadType, boolean z, w02 w02Var) {
        boolean b;
        fs1.f(loadType, "type");
        fs1.f(w02Var, "state");
        if (z) {
            x02 x02Var = this.e;
            x02 h = (x02Var != null ? x02Var : x02.e.a()).h(loadType, w02Var);
            this.e = h;
            b = fs1.b(h, x02Var);
        } else {
            x02 x02Var2 = this.d;
            x02 h2 = x02Var2.h(loadType, w02Var);
            this.d = h2;
            b = fs1.b(h2, x02Var2);
        }
        boolean z2 = !b;
        i();
        return z2;
    }

    public final a30 h() {
        return new a30(this.a, this.b, this.c, this.d, this.e);
    }

    public final void i() {
        w02 w02Var = this.a;
        w02 g = this.d.g();
        w02 g2 = this.d.g();
        x02 x02Var = this.e;
        this.a = c(w02Var, g, g2, x02Var != null ? x02Var.g() : null);
        w02 w02Var2 = this.b;
        w02 g3 = this.d.g();
        w02 f = this.d.f();
        x02 x02Var2 = this.e;
        this.b = c(w02Var2, g3, f, x02Var2 != null ? x02Var2.f() : null);
        w02 w02Var3 = this.c;
        w02 g4 = this.d.g();
        w02 e = this.d.e();
        x02 x02Var3 = this.e;
        this.c = c(w02Var3, g4, e, x02Var3 != null ? x02Var3.e() : null);
    }
}
