package defpackage;

import android.graphics.Canvas;
import android.view.MotionEvent;
import com.rd.animation.type.AnimationType;

/* compiled from: DrawController.java */
/* renamed from: oq0  reason: default package */
/* loaded from: classes2.dex */
public class oq0 {
    public wg4 a;
    public lr0 b;
    public mq1 c;
    public b d;

    /* compiled from: DrawController.java */
    /* renamed from: oq0$a */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[AnimationType.values().length];
            a = iArr;
            try {
                iArr[AnimationType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[AnimationType.COLOR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[AnimationType.SCALE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[AnimationType.WORM.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[AnimationType.SLIDE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[AnimationType.FILL.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[AnimationType.THIN_WORM.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[AnimationType.DROP.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[AnimationType.SWAP.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[AnimationType.SCALE_DOWN.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    /* compiled from: DrawController.java */
    /* renamed from: oq0$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a(int i);
    }

    public oq0(mq1 mq1Var) {
        this.c = mq1Var;
        this.b = new lr0(mq1Var);
    }

    public void a(Canvas canvas) {
        int c = this.c.c();
        for (int i = 0; i < c; i++) {
            b(canvas, i, r80.g(this.c, i), r80.h(this.c, i));
        }
    }

    public final void b(Canvas canvas, int i, int i2, int i3) {
        boolean z = this.c.z();
        int q = this.c.q();
        int r = this.c.r();
        boolean z2 = true;
        boolean z3 = !z && (i == q || i == this.c.f());
        if (!z || (i != q && i != r)) {
            z2 = false;
        }
        boolean z4 = z3 | z2;
        this.b.k(i, i2, i3);
        if (this.a != null && z4) {
            c(canvas);
        } else {
            this.b.a(canvas, z4);
        }
    }

    public final void c(Canvas canvas) {
        switch (a.a[this.c.b().ordinal()]) {
            case 1:
                this.b.a(canvas, true);
                return;
            case 2:
                this.b.b(canvas, this.a);
                return;
            case 3:
                this.b.e(canvas, this.a);
                return;
            case 4:
                this.b.j(canvas, this.a);
                return;
            case 5:
                this.b.g(canvas, this.a);
                return;
            case 6:
                this.b.d(canvas, this.a);
                return;
            case 7:
                this.b.i(canvas, this.a);
                return;
            case 8:
                this.b.c(canvas, this.a);
                return;
            case 9:
                this.b.h(canvas, this.a);
                return;
            case 10:
                this.b.f(canvas, this.a);
                return;
            default:
                return;
        }
    }

    public final void d(float f, float f2) {
        int d;
        if (this.d == null || (d = r80.d(this.c, f, f2)) < 0) {
            return;
        }
        this.d.a(d);
    }

    public void e(b bVar) {
        this.d = bVar;
    }

    public void f(MotionEvent motionEvent) {
        if (motionEvent != null && motionEvent.getAction() == 1) {
            d(motionEvent.getX(), motionEvent.getY());
        }
    }

    public void g(wg4 wg4Var) {
        this.a = wg4Var;
    }
}
