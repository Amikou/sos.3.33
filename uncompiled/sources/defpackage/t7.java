package defpackage;

import android.content.Context;
import android.content.Intent;
import defpackage.s7;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ActivityResultContracts.java */
/* renamed from: t7  reason: default package */
/* loaded from: classes.dex */
public final class t7 extends s7<String[], Map<String, Boolean>> {
    public static Intent e(String[] strArr) {
        return new Intent("androidx.activity.result.contract.action.REQUEST_PERMISSIONS").putExtra("androidx.activity.result.contract.extra.PERMISSIONS", strArr);
    }

    @Override // defpackage.s7
    /* renamed from: d */
    public Intent a(Context context, String[] strArr) {
        return e(strArr);
    }

    @Override // defpackage.s7
    /* renamed from: f */
    public s7.a<Map<String, Boolean>> b(Context context, String[] strArr) {
        if (strArr != null && strArr.length != 0) {
            rh rhVar = new rh();
            boolean z = true;
            for (String str : strArr) {
                boolean z2 = m70.a(context, str) == 0;
                rhVar.put(str, Boolean.valueOf(z2));
                if (!z2) {
                    z = false;
                }
            }
            if (z) {
                return new s7.a<>(rhVar);
            }
            return null;
        }
        return new s7.a<>(Collections.emptyMap());
    }

    @Override // defpackage.s7
    /* renamed from: g */
    public Map<String, Boolean> c(int i, Intent intent) {
        if (i != -1) {
            return Collections.emptyMap();
        }
        if (intent == null) {
            return Collections.emptyMap();
        }
        String[] stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS");
        int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
        if (intArrayExtra != null && stringArrayExtra != null) {
            HashMap hashMap = new HashMap();
            int length = stringArrayExtra.length;
            for (int i2 = 0; i2 < length; i2++) {
                hashMap.put(stringArrayExtra[i2], Boolean.valueOf(intArrayExtra[i2] == 0));
            }
            return hashMap;
        }
        return Collections.emptyMap();
    }
}
