package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: ga3  reason: default package */
/* loaded from: classes2.dex */
public interface ga3 {
    public static final i A;
    public static final i B;
    public static final i C;
    public static final i D;
    public static final i E;
    public static final i F;
    public static final i G;
    public static final i H;
    public static final i I;
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;
    public static final i g;
    public static final i h;
    public static final i i;
    public static final i j;
    public static final i k;
    public static final i l;
    public static final i m;
    public static final i n;
    public static final i o;
    public static final i p;
    public static final i q;
    public static final i r;
    public static final i s;
    public static final i t;
    public static final i u;
    public static final i v;
    public static final i w;
    public static final i x;
    public static final i y;
    public static final i z;

    static {
        i iVar = new i("1.3.132.0");
        a = iVar;
        b = iVar.z("1");
        c = iVar.z("2");
        d = iVar.z("3");
        e = iVar.z("4");
        f = iVar.z("5");
        g = iVar.z("6");
        h = iVar.z("7");
        i = iVar.z("8");
        j = iVar.z("9");
        k = iVar.z("10");
        l = iVar.z("15");
        m = iVar.z("16");
        n = iVar.z("17");
        o = iVar.z("22");
        p = iVar.z("23");
        q = iVar.z("24");
        r = iVar.z("25");
        s = iVar.z("26");
        t = iVar.z("27");
        u = iVar.z("28");
        v = iVar.z("29");
        w = iVar.z("30");
        x = iVar.z("31");
        y = iVar.z("32");
        z = iVar.z("33");
        A = iVar.z("34");
        B = iVar.z("35");
        C = iVar.z("36");
        D = iVar.z("37");
        E = iVar.z("38");
        F = iVar.z("39");
        G = vr4.J;
        H = vr4.P;
        i iVar2 = new i("1.3.132.1");
        I = iVar2;
        iVar2.z("11.0");
        iVar2.z("11.1");
        iVar2.z("11.2");
        iVar2.z("11.3");
        iVar2.z("14.0");
        iVar2.z("14.1");
        iVar2.z("14.2");
        iVar2.z("14.3");
        iVar2.z("15.0");
        iVar2.z("15.1");
        iVar2.z("15.2");
        iVar2.z("15.3");
        iVar2.z("16.0");
        iVar2.z("16.1");
        iVar2.z("16.2");
        iVar2.z("16.3");
    }
}
