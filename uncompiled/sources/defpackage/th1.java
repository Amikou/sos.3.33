package defpackage;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: GradientColorInflaterCompat.java */
/* renamed from: th1  reason: default package */
/* loaded from: classes.dex */
public final class th1 {
    public static a a(a aVar, int i, int i2, boolean z, int i3) {
        if (aVar != null) {
            return aVar;
        }
        if (z) {
            return new a(i, i3, i2);
        }
        return new a(i, i2);
    }

    public static Shader b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            TypedArray k = xd4.k(resources, theme, attributeSet, y23.GradientColor);
            float f = xd4.f(k, xmlPullParser, "startX", y23.GradientColor_android_startX, Utils.FLOAT_EPSILON);
            float f2 = xd4.f(k, xmlPullParser, "startY", y23.GradientColor_android_startY, Utils.FLOAT_EPSILON);
            float f3 = xd4.f(k, xmlPullParser, "endX", y23.GradientColor_android_endX, Utils.FLOAT_EPSILON);
            float f4 = xd4.f(k, xmlPullParser, "endY", y23.GradientColor_android_endY, Utils.FLOAT_EPSILON);
            float f5 = xd4.f(k, xmlPullParser, "centerX", y23.GradientColor_android_centerX, Utils.FLOAT_EPSILON);
            float f6 = xd4.f(k, xmlPullParser, "centerY", y23.GradientColor_android_centerY, Utils.FLOAT_EPSILON);
            int g = xd4.g(k, xmlPullParser, "type", y23.GradientColor_android_type, 0);
            int b = xd4.b(k, xmlPullParser, "startColor", y23.GradientColor_android_startColor, 0);
            boolean j = xd4.j(xmlPullParser, "centerColor");
            int b2 = xd4.b(k, xmlPullParser, "centerColor", y23.GradientColor_android_centerColor, 0);
            int b3 = xd4.b(k, xmlPullParser, "endColor", y23.GradientColor_android_endColor, 0);
            int g2 = xd4.g(k, xmlPullParser, "tileMode", y23.GradientColor_android_tileMode, 0);
            float f7 = xd4.f(k, xmlPullParser, "gradientRadius", y23.GradientColor_android_gradientRadius, Utils.FLOAT_EPSILON);
            k.recycle();
            a a2 = a(c(resources, xmlPullParser, attributeSet, theme), b, b3, j, b2);
            if (g != 1) {
                if (g != 2) {
                    return new LinearGradient(f, f2, f3, f4, a2.a, a2.b, d(g2));
                }
                return new SweepGradient(f5, f6, a2.a, a2.b);
            } else if (f7 > Utils.FLOAT_EPSILON) {
                return new RadialGradient(f5, f6, f7, a2.a, a2.b, d(g2));
            } else {
                throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
            }
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0080, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r10.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' attribute!");
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.th1.a c(android.content.res.Resources r9, org.xmlpull.v1.XmlPullParser r10, android.util.AttributeSet r11, android.content.res.Resources.Theme r12) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            int r0 = r10.getDepth()
            r1 = 1
            int r0 = r0 + r1
            java.util.ArrayList r2 = new java.util.ArrayList
            r3 = 20
            r2.<init>(r3)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>(r3)
        L12:
            int r3 = r10.next()
            if (r3 == r1) goto L81
            int r5 = r10.getDepth()
            if (r5 >= r0) goto L21
            r6 = 3
            if (r3 == r6) goto L81
        L21:
            r6 = 2
            if (r3 == r6) goto L25
            goto L12
        L25:
            if (r5 > r0) goto L12
            java.lang.String r3 = r10.getName()
            java.lang.String r5 = "item"
            boolean r3 = r3.equals(r5)
            if (r3 != 0) goto L34
            goto L12
        L34:
            int[] r3 = defpackage.y23.GradientColorItem
            android.content.res.TypedArray r3 = defpackage.xd4.k(r9, r12, r11, r3)
            int r5 = defpackage.y23.GradientColorItem_android_color
            boolean r6 = r3.hasValue(r5)
            int r7 = defpackage.y23.GradientColorItem_android_offset
            boolean r8 = r3.hasValue(r7)
            if (r6 == 0) goto L66
            if (r8 == 0) goto L66
            r6 = 0
            int r5 = r3.getColor(r5, r6)
            r6 = 0
            float r6 = r3.getFloat(r7, r6)
            r3.recycle()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)
            r4.add(r3)
            java.lang.Float r3 = java.lang.Float.valueOf(r6)
            r2.add(r3)
            goto L12
        L66:
            org.xmlpull.v1.XmlPullParserException r9 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r10 = r10.getPositionDescription()
            r11.append(r10)
            java.lang.String r10 = ": <item> tag requires a 'color' attribute and a 'offset' attribute!"
            r11.append(r10)
            java.lang.String r10 = r11.toString()
            r9.<init>(r10)
            throw r9
        L81:
            int r9 = r4.size()
            if (r9 <= 0) goto L8d
            th1$a r9 = new th1$a
            r9.<init>(r4, r2)
            return r9
        L8d:
            r9 = 0
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.th1.c(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):th1$a");
    }

    public static Shader.TileMode d(int i) {
        if (i != 1) {
            if (i != 2) {
                return Shader.TileMode.CLAMP;
            }
            return Shader.TileMode.MIRROR;
        }
        return Shader.TileMode.REPEAT;
    }

    /* compiled from: GradientColorInflaterCompat.java */
    /* renamed from: th1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int[] a;
        public final float[] b;

        public a(List<Integer> list, List<Float> list2) {
            int size = list.size();
            this.a = new int[size];
            this.b = new float[size];
            for (int i = 0; i < size; i++) {
                this.a[i] = list.get(i).intValue();
                this.b[i] = list2.get(i).floatValue();
            }
        }

        public a(int i, int i2) {
            this.a = new int[]{i, i2};
            this.b = new float[]{Utils.FLOAT_EPSILON, 1.0f};
        }

        public a(int i, int i2, int i3) {
            this.a = new int[]{i, i2, i3};
            this.b = new float[]{Utils.FLOAT_EPSILON, 0.5f, 1.0f};
        }
    }
}
