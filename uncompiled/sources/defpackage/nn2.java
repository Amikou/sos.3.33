package defpackage;

/* compiled from: OptionalDouble.java */
/* renamed from: nn2  reason: default package */
/* loaded from: classes2.dex */
public final class nn2 {
    public static final nn2 c = new nn2();
    public final boolean a;
    public final double b;

    public nn2() {
        this.a = false;
        this.b = Double.NaN;
    }

    public static nn2 a() {
        return c;
    }

    public static nn2 c(double d) {
        return new nn2(d);
    }

    public boolean b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof nn2) {
            nn2 nn2Var = (nn2) obj;
            boolean z = this.a;
            if (z && nn2Var.a) {
                if (Double.compare(this.b, nn2Var.b) == 0) {
                    return true;
                }
            } else if (z == nn2Var.a) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        if (this.a) {
            return kq0.a(this.b);
        }
        return 0;
    }

    public String toString() {
        return this.a ? String.format("OptionalDouble[%s]", Double.valueOf(this.b)) : "OptionalDouble.empty";
    }

    public nn2(double d) {
        this.a = true;
        this.b = d;
    }
}
