package defpackage;

import com.facebook.datasource.AbstractDataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: IncreasingQualityDataSourceSupplier.java */
/* renamed from: fq1  reason: default package */
/* loaded from: classes.dex */
public class fq1<T> implements fw3<ge0<T>> {
    public final List<fw3<ge0<T>>> a;
    public final boolean b;

    /* compiled from: IncreasingQualityDataSourceSupplier.java */
    /* renamed from: fq1$a */
    /* loaded from: classes.dex */
    public class a extends AbstractDataSource<T> {
        public ArrayList<ge0<T>> i;
        public int j;
        public int k;
        public AtomicInteger l;
        public Throwable m;
        public Map<String, Object> n;

        /* compiled from: IncreasingQualityDataSourceSupplier.java */
        /* renamed from: fq1$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0174a implements ke0<T> {
            public int a;

            public C0174a(int i) {
                this.a = i;
            }

            @Override // defpackage.ke0
            public void a(ge0<T> ge0Var) {
                if (this.a == 0) {
                    a.this.s(ge0Var.d());
                }
            }

            @Override // defpackage.ke0
            public void b(ge0<T> ge0Var) {
                a.this.G(this.a, ge0Var);
            }

            @Override // defpackage.ke0
            public void c(ge0<T> ge0Var) {
            }

            @Override // defpackage.ke0
            public void d(ge0<T> ge0Var) {
                if (ge0Var.a()) {
                    a.this.H(this.a, ge0Var);
                } else if (ge0Var.b()) {
                    a.this.G(this.a, ge0Var);
                }
            }
        }

        public a() {
            if (fq1.this.b) {
                return;
            }
            A();
        }

        public final void A() {
            if (this.l != null) {
                return;
            }
            synchronized (this) {
                if (this.l == null) {
                    this.l = new AtomicInteger(0);
                    int size = fq1.this.a.size();
                    this.k = size;
                    this.j = size;
                    this.i = new ArrayList<>(size);
                    for (int i = 0; i < size; i++) {
                        ge0<T> ge0Var = (ge0) ((fw3) fq1.this.a.get(i)).get();
                        this.i.add(ge0Var);
                        ge0Var.e(new C0174a(i), bv.a());
                        if (ge0Var.a()) {
                            break;
                        }
                    }
                }
            }
        }

        public final synchronized ge0<T> B(int i) {
            ge0<T> ge0Var;
            ArrayList<ge0<T>> arrayList = this.i;
            ge0Var = null;
            if (arrayList != null && i < arrayList.size()) {
                ge0Var = this.i.set(i, null);
            }
            return ge0Var;
        }

        public final synchronized ge0<T> C(int i) {
            ArrayList<ge0<T>> arrayList;
            arrayList = this.i;
            return (arrayList == null || i >= arrayList.size()) ? null : this.i.get(i);
        }

        public final synchronized ge0<T> D() {
            return C(this.j);
        }

        public final void E() {
            Throwable th;
            if (this.l.incrementAndGet() != this.k || (th = this.m) == null) {
                return;
            }
            q(th, this.n);
        }

        /* JADX WARN: Removed duplicated region for block: B:18:0x0022 A[LOOP:0: B:17:0x0020->B:18:0x0022, LOOP_END] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final void F(int r3, defpackage.ge0<T> r4, boolean r5) {
            /*
                r2 = this;
                monitor-enter(r2)
                int r0 = r2.j     // Catch: java.lang.Throwable -> L2f
                ge0 r1 = r2.C(r3)     // Catch: java.lang.Throwable -> L2f
                if (r4 != r1) goto L2d
                int r4 = r2.j     // Catch: java.lang.Throwable -> L2f
                if (r3 != r4) goto Le
                goto L2d
            Le:
                ge0 r4 = r2.D()     // Catch: java.lang.Throwable -> L2f
                if (r4 == 0) goto L1d
                if (r5 == 0) goto L1b
                int r4 = r2.j     // Catch: java.lang.Throwable -> L2f
                if (r3 >= r4) goto L1b
                goto L1d
            L1b:
                r3 = r0
                goto L1f
            L1d:
                r2.j = r3     // Catch: java.lang.Throwable -> L2f
            L1f:
                monitor-exit(r2)     // Catch: java.lang.Throwable -> L2f
            L20:
                if (r0 <= r3) goto L2c
                ge0 r4 = r2.B(r0)
                r2.z(r4)
                int r0 = r0 + (-1)
                goto L20
            L2c:
                return
            L2d:
                monitor-exit(r2)     // Catch: java.lang.Throwable -> L2f
                return
            L2f:
                r3 = move-exception
                monitor-exit(r2)     // Catch: java.lang.Throwable -> L2f
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.fq1.a.F(int, ge0, boolean):void");
        }

        public final void G(int i, ge0<T> ge0Var) {
            z(I(i, ge0Var));
            if (i == 0) {
                this.m = ge0Var.c();
                this.n = ge0Var.getExtras();
            }
            E();
        }

        public final void H(int i, ge0<T> ge0Var) {
            F(i, ge0Var, ge0Var.b());
            if (ge0Var == D()) {
                u(null, i == 0 && ge0Var.b(), ge0Var.getExtras());
            }
            E();
        }

        public final synchronized ge0<T> I(int i, ge0<T> ge0Var) {
            if (ge0Var == D()) {
                return null;
            }
            if (ge0Var == C(i)) {
                return B(i);
            }
            return ge0Var;
        }

        @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
        public synchronized boolean a() {
            boolean z;
            if (fq1.this.b) {
                A();
            }
            ge0<T> D = D();
            if (D != null) {
                z = D.a();
            }
            return z;
        }

        @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
        public boolean close() {
            if (fq1.this.b) {
                A();
            }
            synchronized (this) {
                if (super.close()) {
                    ArrayList<ge0<T>> arrayList = this.i;
                    this.i = null;
                    if (arrayList != null) {
                        for (int i = 0; i < arrayList.size(); i++) {
                            z(arrayList.get(i));
                        }
                        return true;
                    }
                    return true;
                }
                return false;
            }
        }

        @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
        public synchronized T g() {
            ge0<T> D;
            if (fq1.this.b) {
                A();
            }
            D = D();
            return D != null ? D.g() : null;
        }

        public final void z(ge0<T> ge0Var) {
            if (ge0Var != null) {
                ge0Var.close();
            }
        }
    }

    public fq1(List<fw3<ge0<T>>> list, boolean z) {
        xt2.c(!list.isEmpty(), "List of suppliers is empty!");
        this.a = list;
        this.b = z;
    }

    public static <T> fq1<T> c(List<fw3<ge0<T>>> list, boolean z) {
        return new fq1<>(list, z);
    }

    @Override // defpackage.fw3
    /* renamed from: d */
    public ge0<T> get() {
        return new a();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof fq1) {
            return ol2.a(this.a, ((fq1) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return ol2.c(this).b("list", this.a).toString();
    }
}
