package defpackage;

import android.annotation.SuppressLint;

/* compiled from: RendererCapabilities.java */
/* renamed from: u63  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class u63 {
    public static int a(int i) {
        return b(i, 0, 0);
    }

    public static int b(int i, int i2, int i3) {
        return c(i, i2, i3, 0, 128);
    }

    @SuppressLint({"WrongConstant"})
    public static int c(int i, int i2, int i3, int i4, int i5) {
        return i | i2 | i3 | i4 | i5;
    }

    @SuppressLint({"WrongConstant"})
    public static int d(int i) {
        return i & 24;
    }

    @SuppressLint({"WrongConstant"})
    public static int e(int i) {
        return i & 128;
    }

    @SuppressLint({"WrongConstant"})
    public static int f(int i) {
        return i & 7;
    }

    @SuppressLint({"WrongConstant"})
    public static int g(int i) {
        return i & 64;
    }

    @SuppressLint({"WrongConstant"})
    public static int h(int i) {
        return i & 32;
    }
}
