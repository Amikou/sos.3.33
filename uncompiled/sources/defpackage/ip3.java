package defpackage;

/* compiled from: SimpleImageTranscoderFactory.java */
/* renamed from: ip3  reason: default package */
/* loaded from: classes.dex */
public class ip3 implements bp1 {
    public final int a;

    public ip3(int i) {
        this.a = i;
    }

    @Override // defpackage.bp1
    public ap1 createImageTranscoder(wn1 wn1Var, boolean z) {
        return new hp3(z, this.a);
    }
}
