package defpackage;

import java.util.List;
import net.safemoon.androidwallet.model.collectible.Assets;
import net.safemoon.androidwallet.model.collectible.Collectible;
import retrofit2.b;

/* compiled from: CollectibleAPIInterface.java */
/* renamed from: p00  reason: default package */
/* loaded from: classes2.dex */
public interface p00 {
    @ge1("/api/v1/collections")
    b<List<Collectible>> a(@yw2("limit") Integer num, @yw2("offset") Integer num2, @yw2("asset_owner") String str);

    @ge1("/api/v1/assets")
    b<Assets> b(@yw2("limit") Integer num, @yw2("offset") Integer num2, @yw2("collection") String str, @yw2("owner") String str2);
}
