package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import com.github.mikephil.charting.utils.Utils;

/* renamed from: u75  reason: default package */
/* loaded from: classes.dex */
public final class u75 {
    public SharedPreferences a;

    public u75(Context context) {
        try {
            Context d = qh1.d(context);
            this.a = d == null ? null : d.getSharedPreferences("google_ads_flags", 0);
        } catch (Throwable unused) {
            this.a = null;
        }
    }

    public final boolean a(String str, boolean z) {
        try {
            SharedPreferences sharedPreferences = this.a;
            if (sharedPreferences == null) {
                return false;
            }
            return sharedPreferences.getBoolean(str, false);
        } catch (Throwable unused) {
            return false;
        }
    }

    public final float b(String str, float f) {
        try {
            SharedPreferences sharedPreferences = this.a;
            return sharedPreferences == null ? Utils.FLOAT_EPSILON : sharedPreferences.getFloat(str, Utils.FLOAT_EPSILON);
        } catch (Throwable unused) {
            return Utils.FLOAT_EPSILON;
        }
    }

    public final String c(String str, String str2) {
        try {
            SharedPreferences sharedPreferences = this.a;
            return sharedPreferences == null ? str2 : sharedPreferences.getString(str, str2);
        } catch (Throwable unused) {
            return str2;
        }
    }
}
