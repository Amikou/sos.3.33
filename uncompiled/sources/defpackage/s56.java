package defpackage;

import com.google.android.gms.internal.measurement.j1;
import com.google.android.gms.internal.measurement.s0;
import com.google.android.gms.internal.measurement.u0;
import com.google.android.gms.measurement.internal.r;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: s56  reason: default package */
/* loaded from: classes.dex */
public final class s56 extends n56 {
    public final u0 g;
    public final /* synthetic */ x56 h;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public s56(x56 x56Var, String str, int i, u0 u0Var) {
        super(str, i);
        this.h = x56Var;
        this.g = u0Var;
    }

    @Override // defpackage.n56
    public final int a() {
        return this.g.y();
    }

    @Override // defpackage.n56
    public final boolean b() {
        return true;
    }

    @Override // defpackage.n56
    public final boolean c() {
        return false;
    }

    public final boolean k(Long l, Long l2, j1 j1Var, boolean z) {
        k16.a();
        boolean v = this.h.a.z().v(this.a, qf5.Y);
        boolean B = this.g.B();
        boolean C = this.g.C();
        boolean E = this.g.E();
        byte b = (B || C || E) ? (byte) 1 : (byte) 0;
        Boolean bool = null;
        bool = null;
        bool = null;
        bool = null;
        bool = null;
        if (z && b == 0) {
            this.h.a.w().v().c("Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID", Integer.valueOf(this.b), this.g.x() ? Integer.valueOf(this.g.y()) : null);
            return true;
        }
        s0 A = this.g.A();
        boolean C2 = A.C();
        if (j1Var.C()) {
            if (!A.z()) {
                this.h.a.w().p().b("No number filter for long property. property", this.h.a.H().p(j1Var.z()));
            } else {
                bool = n56.e(n56.g(j1Var.D(), A.A()), C2);
            }
        } else if (j1Var.E()) {
            if (!A.z()) {
                this.h.a.w().p().b("No number filter for double property. property", this.h.a.H().p(j1Var.z()));
            } else {
                bool = n56.e(n56.h(j1Var.F(), A.A()), C2);
            }
        } else if (j1Var.A()) {
            if (!A.x()) {
                if (!A.z()) {
                    this.h.a.w().p().b("No string or number filter defined. property", this.h.a.H().p(j1Var.z()));
                } else if (r.C(j1Var.B())) {
                    bool = n56.e(n56.i(j1Var.B(), A.A()), C2);
                } else {
                    this.h.a.w().p().c("Invalid user property value for Numeric number filter. property, value", this.h.a.H().p(j1Var.z()), j1Var.B());
                }
            } else {
                bool = n56.e(n56.f(j1Var.B(), A.y(), this.h.a.w()), C2);
            }
        } else {
            this.h.a.w().p().b("User property has no value, property", this.h.a.H().p(j1Var.z()));
        }
        this.h.a.w().v().b("Property filter result", bool == null ? "null" : bool);
        if (bool == null) {
            return false;
        }
        this.c = Boolean.TRUE;
        if (!E || bool.booleanValue()) {
            if (!z || this.g.B()) {
                this.d = bool;
            }
            if (bool.booleanValue() && b != 0 && j1Var.x()) {
                long y = j1Var.y();
                if (l != null) {
                    y = l.longValue();
                }
                if (v && this.g.B() && !this.g.C() && l2 != null) {
                    y = l2.longValue();
                }
                if (this.g.C()) {
                    this.f = Long.valueOf(y);
                } else {
                    this.e = Long.valueOf(y);
                }
            }
            return true;
        }
        return true;
    }
}
