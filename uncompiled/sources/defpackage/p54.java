package defpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import com.bumptech.glide.load.data.g;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ThumbFetcher.java */
/* renamed from: p54  reason: default package */
/* loaded from: classes.dex */
public class p54 implements d<InputStream> {
    public final Uri a;
    public final v54 f0;
    public InputStream g0;

    /* compiled from: ThumbFetcher.java */
    /* renamed from: p54$a */
    /* loaded from: classes.dex */
    public static class a implements t54 {
        public static final String[] b = {"_data"};
        public final ContentResolver a;

        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @Override // defpackage.t54
        public Cursor a(Uri uri) {
            return this.a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{uri.getLastPathSegment()}, null);
        }
    }

    /* compiled from: ThumbFetcher.java */
    /* renamed from: p54$b */
    /* loaded from: classes.dex */
    public static class b implements t54 {
        public static final String[] b = {"_data"};
        public final ContentResolver a;

        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @Override // defpackage.t54
        public Cursor a(Uri uri) {
            return this.a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{uri.getLastPathSegment()}, null);
        }
    }

    public p54(Uri uri, v54 v54Var) {
        this.a = uri;
        this.f0 = v54Var;
    }

    public static p54 c(Context context, Uri uri, t54 t54Var) {
        return new p54(uri, new v54(com.bumptech.glide.a.c(context).j().g(), t54Var, com.bumptech.glide.a.c(context).e(), context.getContentResolver()));
    }

    public static p54 f(Context context, Uri uri) {
        return c(context, uri, new a(context.getContentResolver()));
    }

    public static p54 g(Context context, Uri uri) {
        return c(context, uri, new b(context.getContentResolver()));
    }

    @Override // com.bumptech.glide.load.data.d
    public Class<InputStream> a() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.d
    public void b() {
        InputStream inputStream = this.g0;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }

    @Override // com.bumptech.glide.load.data.d
    public void cancel() {
    }

    @Override // com.bumptech.glide.load.data.d
    public DataSource d() {
        return DataSource.LOCAL;
    }

    @Override // com.bumptech.glide.load.data.d
    public void e(Priority priority, d.a<? super InputStream> aVar) {
        try {
            InputStream h = h();
            this.g0 = h;
            aVar.f(h);
        } catch (FileNotFoundException e) {
            aVar.c(e);
        }
    }

    public final InputStream h() throws FileNotFoundException {
        InputStream d = this.f0.d(this.a);
        int a2 = d != null ? this.f0.a(this.a) : -1;
        return a2 != -1 ? new g(d, a2) : d;
    }
}
