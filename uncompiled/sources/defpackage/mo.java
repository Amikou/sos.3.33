package defpackage;

/* compiled from: BatchingListUpdateCallback.java */
/* renamed from: mo  reason: default package */
/* loaded from: classes.dex */
public class mo implements i02 {
    public final i02 a;
    public int f0 = 0;
    public int g0 = -1;
    public int h0 = -1;
    public Object i0 = null;

    public mo(i02 i02Var) {
        this.a = i02Var;
    }

    public void a() {
        int i = this.f0;
        if (i == 0) {
            return;
        }
        if (i == 1) {
            this.a.onInserted(this.g0, this.h0);
        } else if (i == 2) {
            this.a.onRemoved(this.g0, this.h0);
        } else if (i == 3) {
            this.a.onChanged(this.g0, this.h0, this.i0);
        }
        this.i0 = null;
        this.f0 = 0;
    }

    @Override // defpackage.i02
    public void onChanged(int i, int i2, Object obj) {
        int i3;
        if (this.f0 == 3) {
            int i4 = this.g0;
            int i5 = this.h0;
            if (i <= i4 + i5 && (i3 = i + i2) >= i4 && this.i0 == obj) {
                this.g0 = Math.min(i, i4);
                this.h0 = Math.max(i5 + i4, i3) - this.g0;
                return;
            }
        }
        a();
        this.g0 = i;
        this.h0 = i2;
        this.i0 = obj;
        this.f0 = 3;
    }

    @Override // defpackage.i02
    public void onInserted(int i, int i2) {
        int i3;
        if (this.f0 == 1 && i >= (i3 = this.g0)) {
            int i4 = this.h0;
            if (i <= i3 + i4) {
                this.h0 = i4 + i2;
                this.g0 = Math.min(i, i3);
                return;
            }
        }
        a();
        this.g0 = i;
        this.h0 = i2;
        this.f0 = 1;
    }

    @Override // defpackage.i02
    public void onMoved(int i, int i2) {
        a();
        this.a.onMoved(i, i2);
    }

    @Override // defpackage.i02
    public void onRemoved(int i, int i2) {
        int i3;
        if (this.f0 == 2 && (i3 = this.g0) >= i && i3 <= i + i2) {
            this.h0 += i2;
            this.g0 = i;
            return;
        }
        a();
        this.g0 = i;
        this.h0 = i2;
        this.f0 = 2;
    }
}
