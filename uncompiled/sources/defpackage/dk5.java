package defpackage;

import com.google.android.gms.internal.measurement.b1;
import com.google.android.gms.internal.measurement.f1;
import com.google.android.gms.internal.measurement.j1;
import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.x1;
import com.google.android.gms.internal.measurement.z0;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: dk5  reason: default package */
/* loaded from: classes.dex */
public final class dk5 extends w1<f1, dk5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public dk5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.f1 r0 = com.google.android.gms.internal.measurement.f1.L0()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dk5.<init>():void");
    }

    public final dk5 A(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.d0((f1) this.f0, str);
        return this;
    }

    public final int A0() {
        return ((f1) this.f0).w1();
    }

    public final dk5 B(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.e0((f1) this.f0, i);
        return this;
    }

    public final j1 B0(int i) {
        return ((f1) this.f0).x1(i);
    }

    public final dk5 C(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.f0((f1) this.f0, str);
        return this;
    }

    public final dk5 C0(int i, j1 j1Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.S0((f1) this.f0, i, j1Var);
        return this;
    }

    public final String D() {
        return ((f1) this.f0).x();
    }

    public final dk5 D0(j1 j1Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.T0((f1) this.f0, j1Var);
        return this;
    }

    public final dk5 E(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.g0((f1) this.f0, str);
        return this;
    }

    public final dk5 E0(il5 il5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.T0((f1) this.f0, il5Var.o());
        return this;
    }

    public final dk5 G(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.h0((f1) this.f0, str);
        return this;
    }

    public final dk5 G0(Iterable<? extends j1> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.U0((f1) this.f0, iterable);
        return this;
    }

    public final dk5 H(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.j0((f1) this.f0, j);
        return this;
    }

    public final dk5 I(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.k0((f1) this.f0, 42004L);
        return this;
    }

    public final dk5 I0(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.V0((f1) this.f0, i);
        return this;
    }

    public final dk5 J(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.l0((f1) this.f0, str);
        return this;
    }

    public final dk5 K() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.m0((f1) this.f0);
        return this;
    }

    public final dk5 K0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.W0((f1) this.f0, j);
        return this;
    }

    public final dk5 L(boolean z) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.n0((f1) this.f0, z);
        return this;
    }

    public final long L0() {
        return ((f1) this.f0).B1();
    }

    public final dk5 M() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.o0((f1) this.f0);
        return this;
    }

    public final dk5 M0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.X0((f1) this.f0, j);
        return this;
    }

    public final dk5 N(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.p0((f1) this.f0, str);
        return this;
    }

    public final long N0() {
        return ((f1) this.f0).D1();
    }

    public final dk5 O() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.q0((f1) this.f0);
        return this;
    }

    public final dk5 O0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.Y0((f1) this.f0, j);
        return this;
    }

    public final dk5 P(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.r0((f1) this.f0, j);
        return this;
    }

    public final dk5 P0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.Z0((f1) this.f0, j);
        return this;
    }

    public final dk5 Q(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.s0((f1) this.f0, i);
        return this;
    }

    public final dk5 Q0() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.a1((f1) this.f0);
        return this;
    }

    public final dk5 R(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.t0((f1) this.f0, str);
        return this;
    }

    public final dk5 R0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.Y((f1) this.f0, j);
        return this;
    }

    public final dk5 S() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.u0((f1) this.f0);
        return this;
    }

    public final dk5 S0() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.Z((f1) this.f0);
        return this;
    }

    public final String T() {
        return ((f1) this.f0).M();
    }

    public final dk5 U(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.v0((f1) this.f0, str);
        return this;
    }

    public final dk5 V(boolean z) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.w0((f1) this.f0, z);
        return this;
    }

    public final dk5 W(Iterable<? extends z0> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.x0((f1) this.f0, iterable);
        return this;
    }

    public final dk5 X() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        ((f1) this.f0).zzG = x1.o();
        return this;
    }

    public final dk5 Y(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.M0((f1) this.f0, 1);
        return this;
    }

    public final dk5 Z(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.e1((f1) this.f0, str);
        return this;
    }

    public final dk5 a0(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.f1((f1) this.f0, i);
        return this;
    }

    public final dk5 b0() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.g1((f1) this.f0);
        return this;
    }

    public final dk5 c0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.h1((f1) this.f0, j);
        return this;
    }

    public final dk5 d0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.i1((f1) this.f0, j);
        return this;
    }

    public final dk5 e0(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1 f1Var = (f1) this.f0;
        int i = f1.zza;
        throw null;
    }

    public final dk5 f0() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.j1((f1) this.f0);
        return this;
    }

    public final dk5 g0(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.k1((f1) this.f0, i);
        return this;
    }

    public final dk5 h0(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.l1((f1) this.f0, str);
        return this;
    }

    public final dk5 j0(ik5 ik5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.m1((f1) this.f0, ik5Var.o());
        return this;
    }

    public final dk5 k0(Iterable<? extends Integer> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.n1((f1) this.f0, iterable);
        return this;
    }

    public final dk5 l0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.o1((f1) this.f0, j);
        return this;
    }

    public final dk5 m0(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.p1((f1) this.f0, j);
        return this;
    }

    public final String n0() {
        return ((f1) this.f0).F0();
    }

    public final dk5 o0(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.q1((f1) this.f0, str);
        return this;
    }

    public final dk5 p0(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.r1((f1) this.f0, str);
        return this;
    }

    public final List<b1> q0() {
        return Collections.unmodifiableList(((f1) this.f0).s1());
    }

    public final int r0() {
        return ((f1) this.f0).t1();
    }

    public final b1 s0(int i) {
        return ((f1) this.f0).u1(i);
    }

    public final dk5 t0(int i, tj5 tj5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.N0((f1) this.f0, i, tj5Var.o());
        return this;
    }

    public final dk5 u0(tj5 tj5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.O0((f1) this.f0, tj5Var.o());
        return this;
    }

    public final dk5 v(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.a0((f1) this.f0, "android");
        return this;
    }

    public final dk5 v0(Iterable<? extends b1> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.P0((f1) this.f0, iterable);
        return this;
    }

    public final dk5 w0() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        ((f1) this.f0).zzh = x1.o();
        return this;
    }

    public final dk5 x(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.b0((f1) this.f0, str);
        return this;
    }

    public final dk5 x0(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.R0((f1) this.f0, i);
        return this;
    }

    public final dk5 y(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        f1.c0((f1) this.f0, str);
        return this;
    }

    public final List<j1> z0() {
        return Collections.unmodifiableList(((f1) this.f0).v1());
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ dk5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.f1 r1 = com.google.android.gms.internal.measurement.f1.L0()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dk5.<init>(wi5):void");
    }
}
