package defpackage;

import com.google.android.gms.internal.measurement.a1;
import com.google.android.gms.internal.measurement.h1;
import com.google.android.gms.internal.measurement.i1;
import com.google.android.gms.internal.measurement.z0;
import com.google.android.gms.measurement.internal.r;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: v46  reason: default package */
/* loaded from: classes.dex */
public final class v46 {
    public String a;
    public boolean b;
    public h1 c;
    public BitSet d;
    public BitSet e;
    public Map<Integer, Long> f;
    public Map<Integer, List<Long>> g;
    public final /* synthetic */ x56 h;

    public /* synthetic */ v46(x56 x56Var, String str, q46 q46Var) {
        this.h = x56Var;
        this.a = str;
        this.b = true;
        this.d = new BitSet();
        this.e = new BitSet();
        this.f = new rh();
        this.g = new rh();
    }

    public static /* synthetic */ BitSet c(v46 v46Var) {
        return v46Var.d;
    }

    public final void a(n56 n56Var) {
        int a = n56Var.a();
        Boolean bool = n56Var.c;
        if (bool != null) {
            this.e.set(a, bool.booleanValue());
        }
        Boolean bool2 = n56Var.d;
        if (bool2 != null) {
            this.d.set(a, bool2.booleanValue());
        }
        if (n56Var.e != null) {
            Map<Integer, Long> map = this.f;
            Integer valueOf = Integer.valueOf(a);
            Long l = map.get(valueOf);
            long longValue = n56Var.e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.f.put(valueOf, Long.valueOf(longValue));
            }
        }
        if (n56Var.f != null) {
            Map<Integer, List<Long>> map2 = this.g;
            Integer valueOf2 = Integer.valueOf(a);
            List<Long> list = map2.get(valueOf2);
            if (list == null) {
                list = new ArrayList<>();
                this.g.put(valueOf2, list);
            }
            if (n56Var.b()) {
                list.clear();
            }
            k16.a();
            q45 z = this.h.a.z();
            String str = this.a;
            we5<Boolean> we5Var = qf5.a0;
            if (z.v(str, we5Var) && n56Var.c()) {
                list.clear();
            }
            k16.a();
            if (this.h.a.z().v(this.a, we5Var)) {
                Long valueOf3 = Long.valueOf(n56Var.f.longValue() / 1000);
                if (list.contains(valueOf3)) {
                    return;
                }
                list.add(valueOf3);
                return;
            }
            list.add(Long.valueOf(n56Var.f.longValue() / 1000));
        }
    }

    public final z0 b(int i) {
        ArrayList arrayList;
        List list;
        jj5 E = z0.E();
        E.v(i);
        E.A(this.b);
        h1 h1Var = this.c;
        if (h1Var != null) {
            E.y(h1Var);
        }
        cl5 H = h1.H();
        H.y(r.E(this.d));
        H.v(r.E(this.e));
        Map<Integer, Long> map = this.f;
        if (map == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList(map.size());
            for (Integer num : this.f.keySet()) {
                int intValue = num.intValue();
                Long l = this.f.get(Integer.valueOf(intValue));
                if (l != null) {
                    rj5 B = a1.B();
                    B.v(intValue);
                    B.x(l.longValue());
                    arrayList2.add(B.o());
                }
            }
            arrayList = arrayList2;
        }
        if (arrayList != null) {
            H.B(arrayList);
        }
        Map<Integer, List<Long>> map2 = this.g;
        if (map2 == null) {
            list = Collections.emptyList();
        } else {
            ArrayList arrayList3 = new ArrayList(map2.size());
            for (Integer num2 : this.g.keySet()) {
                fl5 C = i1.C();
                C.v(num2.intValue());
                List<Long> list2 = this.g.get(num2);
                if (list2 != null) {
                    Collections.sort(list2);
                    C.x(list2);
                }
                arrayList3.add((i1) C.o());
            }
            list = arrayList3;
        }
        H.D(list);
        E.x(H);
        return E.o();
    }

    public /* synthetic */ v46(x56 x56Var, String str, h1 h1Var, BitSet bitSet, BitSet bitSet2, Map map, Map map2, q46 q46Var) {
        this.h = x56Var;
        this.a = str;
        this.d = bitSet;
        this.e = bitSet2;
        this.f = map;
        this.g = new rh();
        for (Integer num : map2.keySet()) {
            ArrayList arrayList = new ArrayList();
            arrayList.add((Long) map2.get(num));
            this.g.put(num, arrayList);
        }
        this.b = false;
        this.c = h1Var;
    }
}
