package defpackage;

import android.net.Uri;

/* compiled from: SimpleDraweeControllerBuilder.java */
/* renamed from: bp3  reason: default package */
/* loaded from: classes.dex */
public interface bp3 {
    bp3 a(Uri uri);

    bp3 b(ir0 ir0Var);

    ir0 build();
}
