package defpackage;

import android.os.Bundle;
import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: co5  reason: default package */
/* loaded from: classes.dex */
public final class co5 implements pw5 {
    public final /* synthetic */ dp5 a;

    public co5(dp5 dp5Var) {
        this.a = dp5Var;
    }

    @Override // defpackage.pw5
    public final void i(String str, String str2, Bundle bundle) {
        if (!TextUtils.isEmpty(str)) {
            dp5 dp5Var = this.a;
            ck5.t();
            dp5Var.b0("auto", "_err", dp5Var.a.a().a(), bundle, false, true, false, str);
            return;
        }
        this.a.X("auto", "_err", bundle);
    }
}
