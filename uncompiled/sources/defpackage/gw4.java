package defpackage;

import com.google.android.play.core.internal.d;

/* renamed from: gw4  reason: default package */
/* loaded from: classes2.dex */
public final class gw4<T> implements jw4, cw4 {
    public static final Object c = new Object();
    public volatile jw4<T> a;
    public volatile Object b = c;

    public gw4(jw4<T> jw4Var) {
        this.a = jw4Var;
    }

    public static <P extends jw4<T>, T> jw4<T> b(P p) {
        d.j(p);
        return p instanceof gw4 ? p : new gw4(p);
    }

    public static <P extends jw4<T>, T> cw4<T> c(P p) {
        if (p instanceof cw4) {
            return (cw4) p;
        }
        d.j(p);
        return new gw4(p);
    }

    @Override // defpackage.jw4
    public final T a() {
        T t = (T) this.b;
        Object obj = c;
        if (t == obj) {
            synchronized (this) {
                t = this.b;
                if (t == obj) {
                    t = this.a.a();
                    Object obj2 = this.b;
                    if (obj2 != obj && obj2 != t) {
                        String valueOf = String.valueOf(obj2);
                        String valueOf2 = String.valueOf(t);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 118 + valueOf2.length());
                        sb.append("Scoped provider was invoked recursively returning different results: ");
                        sb.append(valueOf);
                        sb.append(" & ");
                        sb.append(valueOf2);
                        sb.append(". This is likely due to a circular dependency.");
                        throw new IllegalStateException(sb.toString());
                    }
                    this.b = t;
                    this.a = null;
                }
            }
        }
        return t;
    }
}
