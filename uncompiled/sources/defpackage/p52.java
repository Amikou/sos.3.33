package defpackage;

/* renamed from: p52  reason: default package */
/* loaded from: classes2.dex */
public class p52 extends k52 {
    public int a;
    public int f0;
    public he1 g0;

    public p52(int i, int i2, he1 he1Var) {
        super(false, null);
        this.a = i;
        this.f0 = i2;
        this.g0 = new he1(he1Var);
    }

    public he1 a() {
        return this.g0;
    }

    public int b() {
        return this.g0.b();
    }

    public int c() {
        return this.a;
    }

    public int d() {
        return this.f0;
    }
}
