package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: d36  reason: default package */
/* loaded from: classes.dex */
public final class d36 implements yp5<e36> {
    public static final d36 f0 = new d36();
    public final yp5<e36> a = gq5.a(gq5.b(new f36()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final e36 zza() {
        return this.a.zza();
    }
}
