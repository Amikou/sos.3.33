package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: po2  reason: default package */
/* loaded from: classes2.dex */
public interface po2 {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;
    public static final i g;
    public static final i h;

    static {
        i iVar = new i("1.3.6.1.4.1.8301.3.1.3.5.3.2");
        a = iVar;
        iVar.z("1");
        iVar.z("2");
        iVar.z("3");
        iVar.z("4");
        iVar.z("5");
        i iVar2 = new i("1.3.6.1.4.1.8301.3.1.3.3");
        b = iVar2;
        iVar2.z("1");
        iVar2.z("2");
        iVar2.z("3");
        iVar2.z("4");
        iVar2.z("5");
        c = new i("1.3.6.1.4.1.8301.3.1.3.4.1");
        d = new i("1.3.6.1.4.1.8301.3.1.3.4.2");
        new i("1.3.6.1.4.1.8301.3.1.3.4.2.1");
        new i("1.3.6.1.4.1.8301.3.1.3.4.2.2");
        new i("1.3.6.1.4.1.8301.3.1.3.4.2.3");
        e = xl.h;
        i iVar3 = xl.i;
        i iVar4 = xl.j;
        i iVar5 = xl.k;
        f = xl.w;
        g = xl.l;
        i iVar6 = xl.m;
        i iVar7 = xl.n;
        i iVar8 = xl.o;
        i iVar9 = xl.p;
        h = xl.q;
        i iVar10 = xl.r;
        i iVar11 = xl.s;
        i iVar12 = xl.t;
        i iVar13 = xl.u;
    }
}
