package defpackage;

import com.google.common.primitives.Ints;
import java.util.Comparator;

/* compiled from: ComparisonChain.java */
/* renamed from: k30  reason: default package */
/* loaded from: classes2.dex */
public abstract class k30 {
    public static final k30 a = new a();
    public static final k30 b = new b(-1);
    public static final k30 c = new b(1);

    /* compiled from: ComparisonChain.java */
    /* renamed from: k30$a */
    /* loaded from: classes2.dex */
    public class a extends k30 {
        public a() {
            super(null);
        }

        @Override // defpackage.k30
        public k30 d(int i, int i2) {
            return j(Ints.e(i, i2));
        }

        @Override // defpackage.k30
        public <T> k30 e(T t, T t2, Comparator<T> comparator) {
            return j(comparator.compare(t, t2));
        }

        @Override // defpackage.k30
        public k30 f(boolean z, boolean z2) {
            return j(cr.a(z, z2));
        }

        @Override // defpackage.k30
        public k30 g(boolean z, boolean z2) {
            return j(cr.a(z2, z));
        }

        @Override // defpackage.k30
        public int h() {
            return 0;
        }

        public k30 j(int i) {
            if (i < 0) {
                return k30.b;
            }
            return i > 0 ? k30.c : k30.a;
        }
    }

    /* compiled from: ComparisonChain.java */
    /* renamed from: k30$b */
    /* loaded from: classes2.dex */
    public static final class b extends k30 {
        public final int d;

        public b(int i) {
            super(null);
            this.d = i;
        }

        @Override // defpackage.k30
        public k30 d(int i, int i2) {
            return this;
        }

        @Override // defpackage.k30
        public <T> k30 e(T t, T t2, Comparator<T> comparator) {
            return this;
        }

        @Override // defpackage.k30
        public k30 f(boolean z, boolean z2) {
            return this;
        }

        @Override // defpackage.k30
        public k30 g(boolean z, boolean z2) {
            return this;
        }

        @Override // defpackage.k30
        public int h() {
            return this.d;
        }
    }

    public /* synthetic */ k30(a aVar) {
        this();
    }

    public static k30 i() {
        return a;
    }

    public abstract k30 d(int i, int i2);

    public abstract <T> k30 e(T t, T t2, Comparator<T> comparator);

    public abstract k30 f(boolean z, boolean z2);

    public abstract k30 g(boolean z, boolean z2);

    public abstract int h();

    public k30() {
    }
}
