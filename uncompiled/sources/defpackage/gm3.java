package defpackage;

/* compiled from: ServiceDescriptionElement.java */
/* renamed from: gm3  reason: default package */
/* loaded from: classes.dex */
public final class gm3 {
    public final long a;
    public final long b;
    public final long c;
    public final float d;
    public final float e;

    public gm3(long j, long j2, long j3, float f, float f2) {
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = f;
        this.e = f2;
    }
}
