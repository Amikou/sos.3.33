package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: jd5  reason: default package */
/* loaded from: classes.dex */
public final class jd5 extends n65<Long> implements rb5<Long> {
    public long[] f0;
    public int g0;

    static {
        new jd5().v();
    }

    public jd5() {
        this(new long[10], 0);
    }

    public jd5(long[] jArr, int i) {
        this.f0 = jArr;
        this.g0 = i;
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5<Long> T0(int i) {
        if (i >= this.g0) {
            return new jd5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        n(i, ((Long) obj).longValue());
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Long> collection) {
        e();
        gb5.a(collection);
        if (collection instanceof jd5) {
            jd5 jd5Var = (jd5) collection;
            int i = jd5Var.g0;
            if (i == 0) {
                return false;
            }
            int i2 = this.g0;
            if (Integer.MAX_VALUE - i2 >= i) {
                int i3 = i2 + i;
                long[] jArr = this.f0;
                if (i3 > jArr.length) {
                    this.f0 = Arrays.copyOf(jArr, i3);
                }
                System.arraycopy(jd5Var.f0, 0, this.f0, this.g0, jd5Var.g0);
                this.g0 = i3;
                ((AbstractList) this).modCount++;
                return true;
            }
            throw new OutOfMemoryError();
        }
        return super.addAll(collection);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof jd5) {
            jd5 jd5Var = (jd5) obj;
            if (this.g0 != jd5Var.g0) {
                return false;
            }
            long[] jArr = jd5Var.f0;
            for (int i = 0; i < this.g0; i++) {
                if (this.f0[i] != jArr[i]) {
                    return false;
                }
            }
            return true;
        }
        return super.equals(obj);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(i(i));
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + gb5.j(this.f0[i2]);
        }
        return i;
    }

    public final long i(int i) {
        k(i);
        return this.f0[i];
    }

    public final void k(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(m(i));
        }
    }

    public final String m(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final void n(int i, long j) {
        int i2;
        e();
        if (i < 0 || i > (i2 = this.g0)) {
            throw new IndexOutOfBoundsException(m(i));
        }
        long[] jArr = this.f0;
        if (i2 < jArr.length) {
            System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
        } else {
            long[] jArr2 = new long[((i2 * 3) / 2) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.f0, i, jArr2, i + 1, this.g0 - i);
            this.f0 = jArr2;
        }
        this.f0[i] = j;
        this.g0++;
        ((AbstractList) this).modCount++;
    }

    public final void o(long j) {
        n(this.g0, j);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        k(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        int i2 = this.g0;
        if (i < i2 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, i2 - i);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Long.valueOf(this.f0[i]))) {
                long[] jArr = this.f0;
                System.arraycopy(jArr, i + 1, jArr, i, this.g0 - i);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        long[] jArr = this.f0;
        System.arraycopy(jArr, i2, jArr, i, this.g0 - i2);
        this.g0 -= i2 - i;
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        e();
        k(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }
}
