package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.phenotype.Configuration;
import com.google.android.gms.phenotype.zzi;

/* renamed from: qa5  reason: default package */
/* loaded from: classes.dex */
public final class qa5 implements Parcelable.Creator<Configuration> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Configuration createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        zzi[] zziVarArr = null;
        int i = 0;
        String[] strArr = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 2) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 3) {
                zziVarArr = (zzi[]) SafeParcelReader.s(parcel, C, zzi.CREATOR);
            } else if (v != 4) {
                SafeParcelReader.I(parcel, C);
            } else {
                strArr = SafeParcelReader.q(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new Configuration(i, zziVarArr, strArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Configuration[] newArray(int i) {
        return new Configuration[i];
    }
}
