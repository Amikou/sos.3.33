package defpackage;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

/* compiled from: Schedulers.java */
/* renamed from: fd3  reason: default package */
/* loaded from: classes2.dex */
public final class fd3 {
    public static final bd3 a = da3.g(new h());

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static final bd3 a = new s40();
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$b */
    /* loaded from: classes2.dex */
    public static final class b implements Callable<bd3> {
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public bd3 call() throws Exception {
            return a.a;
        }
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$c */
    /* loaded from: classes2.dex */
    public static final class c implements Callable<bd3> {
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public bd3 call() throws Exception {
            return d.a;
        }
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$d */
    /* loaded from: classes2.dex */
    public static final class d {
        public static final bd3 a = new ps1();
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$e */
    /* loaded from: classes2.dex */
    public static final class e {
        public static final bd3 a = new mf2();
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$f */
    /* loaded from: classes2.dex */
    public static final class f implements Callable<bd3> {
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public bd3 call() throws Exception {
            return e.a;
        }
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$g */
    /* loaded from: classes2.dex */
    public static final class g {
        public static final bd3 a = new xp3();
    }

    /* compiled from: Schedulers.java */
    /* renamed from: fd3$h */
    /* loaded from: classes2.dex */
    public static final class h implements Callable<bd3> {
        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public bd3 call() throws Exception {
            return g.a;
        }
    }

    static {
        da3.d(new b());
        da3.e(new c());
        n84.a();
        da3.f(new f());
    }

    public static bd3 a(Executor executor) {
        return new vy0(executor);
    }

    public static bd3 b() {
        return da3.i(a);
    }
}
