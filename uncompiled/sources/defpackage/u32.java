package defpackage;

import com.google.protobuf.i0;
import com.google.protobuf.j0;
import com.google.protobuf.k0;

/* compiled from: MapFieldSchemas.java */
/* renamed from: u32  reason: default package */
/* loaded from: classes2.dex */
public final class u32 {
    public static final i0 a = c();
    public static final i0 b = new k0();

    public static i0 a() {
        return a;
    }

    public static i0 b() {
        return b;
    }

    public static i0 c() {
        try {
            return (i0) j0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
