package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: y25  reason: default package */
/* loaded from: classes.dex */
public final class y25 implements u05 {
    public final /* synthetic */ t25 a;

    public y25(t25 t25Var) {
        this.a = t25Var;
    }

    @Override // defpackage.u05
    public final void a(ConnectionResult connectionResult) {
        Lock lock;
        Lock lock2;
        lock = this.a.l;
        lock.lock();
        try {
            this.a.j = connectionResult;
            this.a.x();
        } finally {
            lock2 = this.a.l;
            lock2.unlock();
        }
    }

    @Override // defpackage.u05
    public final void b(int i, boolean z) {
        Lock lock;
        Lock lock2;
        boolean z2;
        i05 i05Var;
        lock = this.a.l;
        lock.lock();
        try {
            z2 = this.a.k;
            if (z2) {
                this.a.k = false;
                this.a.j(i, z);
                return;
            }
            this.a.k = true;
            i05Var = this.a.c;
            i05Var.onConnectionSuspended(i);
        } finally {
            lock2 = this.a.l;
            lock2.unlock();
        }
    }

    @Override // defpackage.u05
    public final void d(Bundle bundle) {
        Lock lock;
        Lock lock2;
        lock = this.a.l;
        lock.lock();
        try {
            this.a.j = ConnectionResult.i0;
            this.a.x();
        } finally {
            lock2 = this.a.l;
            lock2.unlock();
        }
    }

    public /* synthetic */ y25(t25 t25Var, x25 x25Var) {
        this(t25Var);
    }
}
