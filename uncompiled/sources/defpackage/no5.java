package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: no5  reason: default package */
/* loaded from: classes.dex */
public final class no5 implements Runnable {
    public final /* synthetic */ Boolean a;
    public final /* synthetic */ dp5 f0;

    public no5(dp5 dp5Var, Boolean bool) {
        this.f0 = dp5Var;
        this.a = bool;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.L(this.a, true);
    }
}
