package defpackage;

/* compiled from: NativeLoader.java */
/* renamed from: pd2  reason: default package */
/* loaded from: classes.dex */
public class pd2 {
    public static qd2 a;

    public static synchronized void a(qd2 qd2Var) {
        synchronized (pd2.class) {
            if (a == null) {
                a = qd2Var;
            } else {
                throw new IllegalStateException("Cannot re-initialize NativeLoader.");
            }
        }
    }

    public static synchronized void b(qd2 qd2Var) {
        synchronized (pd2.class) {
            if (!c()) {
                a(qd2Var);
            }
        }
    }

    public static synchronized boolean c() {
        boolean z;
        synchronized (pd2.class) {
            z = a != null;
        }
        return z;
    }

    public static boolean d(String str) {
        return e(str, 0);
    }

    public static boolean e(String str, int i) {
        qd2 qd2Var;
        synchronized (pd2.class) {
            qd2Var = a;
            if (qd2Var == null) {
                throw new IllegalStateException("NativeLoader has not been initialized.  To use standard native library loading, call NativeLoader.init(new SystemDelegate()).");
            }
        }
        return qd2Var.a(str, i);
    }
}
