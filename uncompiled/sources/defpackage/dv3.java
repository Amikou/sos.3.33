package defpackage;

import java.nio.ByteBuffer;
import java.nio.charset.CodingErrorAction;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: StringsJVM.kt */
/* renamed from: dv3 */
/* loaded from: classes2.dex */
public class dv3 extends cv3 {
    public static final String A(String str, char c, char c2, boolean z) {
        fs1.f(str, "$this$replace");
        if (!z) {
            String replace = str.replace(c, c2);
            fs1.e(replace, "(this as java.lang.Strin…replace(oldChar, newChar)");
            return replace;
        }
        StringBuilder sb = new StringBuilder(str.length());
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (yx.d(charAt, c, z)) {
                charAt = c2;
            }
            sb.append(charAt);
        }
        String sb2 = sb.toString();
        fs1.e(sb2, "StringBuilder(capacity).…builderAction).toString()");
        return sb2;
    }

    public static final String B(String str, String str2, String str3, boolean z) {
        fs1.f(str, "$this$replace");
        fs1.f(str2, "oldValue");
        fs1.f(str3, "newValue");
        int i = 0;
        int V = StringsKt__StringsKt.V(str, str2, 0, z);
        if (V < 0) {
            return str;
        }
        int length = str2.length();
        int b = u33.b(length, 1);
        int length2 = (str.length() - length) + str3.length();
        if (length2 >= 0) {
            StringBuilder sb = new StringBuilder(length2);
            do {
                sb.append((CharSequence) str, i, V);
                sb.append(str3);
                i = V + length;
                if (V >= str.length()) {
                    break;
                }
                V = StringsKt__StringsKt.V(str, str2, V + b, z);
            } while (V > 0);
            sb.append((CharSequence) str, i, str.length());
            String sb2 = sb.toString();
            fs1.e(sb2, "stringBuilder.append(this, i, length).toString()");
            return sb2;
        }
        throw new OutOfMemoryError();
    }

    public static /* synthetic */ String C(String str, char c, char c2, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return A(str, c, c2, z);
    }

    public static /* synthetic */ String D(String str, String str2, String str3, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return B(str, str2, str3, z);
    }

    public static final boolean E(String str, String str2, int i, boolean z) {
        fs1.f(str, "$this$startsWith");
        fs1.f(str2, "prefix");
        if (!z) {
            return str.startsWith(str2, i);
        }
        return x(str, i, str2, 0, str2.length(), z);
    }

    public static final boolean F(String str, String str2, boolean z) {
        fs1.f(str, "$this$startsWith");
        fs1.f(str2, "prefix");
        if (!z) {
            return str.startsWith(str2);
        }
        return x(str, 0, str2, 0, str2.length(), z);
    }

    public static /* synthetic */ boolean G(String str, String str2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        return E(str, str2, i, z);
    }

    public static /* synthetic */ boolean H(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return F(str, str2, z);
    }

    public static final String p(byte[] bArr, int i, int i2, boolean z) {
        fs1.f(bArr, "$this$decodeToString");
        c5.a.a(i, i2, bArr.length);
        if (!z) {
            return new String(bArr, i, i2 - i, by.a);
        }
        String charBuffer = by.a.newDecoder().onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT).decode(ByteBuffer.wrap(bArr, i, i2 - i)).toString();
        fs1.e(charBuffer, "decoder.decode(ByteBuffe…- startIndex)).toString()");
        return charBuffer;
    }

    public static /* synthetic */ String q(byte[] bArr, int i, int i2, boolean z, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = 0;
        }
        if ((i3 & 2) != 0) {
            i2 = bArr.length;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        return p(bArr, i, i2, z);
    }

    public static final boolean r(String str, String str2, boolean z) {
        fs1.f(str, "$this$endsWith");
        fs1.f(str2, "suffix");
        if (!z) {
            return str.endsWith(str2);
        }
        return x(str, str.length() - str2.length(), str2, 0, str2.length(), true);
    }

    public static /* synthetic */ boolean s(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return r(str, str2, z);
    }

    public static final boolean t(String str, String str2, boolean z) {
        if (str == null) {
            return str2 == null;
        } else if (!z) {
            return str.equals(str2);
        } else {
            return str.equalsIgnoreCase(str2);
        }
    }

    public static /* synthetic */ boolean u(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return t(str, str2, z);
    }

    public static final Comparator<String> v(lu3 lu3Var) {
        fs1.f(lu3Var, "$this$CASE_INSENSITIVE_ORDER");
        Comparator<String> comparator = String.CASE_INSENSITIVE_ORDER;
        fs1.e(comparator, "java.lang.String.CASE_INSENSITIVE_ORDER");
        return comparator;
    }

    public static final boolean w(CharSequence charSequence) {
        boolean z;
        fs1.f(charSequence, "$this$isBlank");
        if (charSequence.length() != 0) {
            sr1 S = StringsKt__StringsKt.S(charSequence);
            if (!(S instanceof Collection) || !((Collection) S).isEmpty()) {
                Iterator<Integer> it = S.iterator();
                while (it.hasNext()) {
                    if (!xx.c(charSequence.charAt(((or1) it).b()))) {
                        z = false;
                        break;
                    }
                }
            }
            z = true;
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public static final boolean x(String str, int i, String str2, int i2, int i3, boolean z) {
        fs1.f(str, "$this$regionMatches");
        fs1.f(str2, "other");
        if (!z) {
            return str.regionMatches(i, str2, i2, i3);
        }
        return str.regionMatches(z, i, str2, i2, i3);
    }

    public static /* synthetic */ boolean y(String str, int i, String str2, int i2, int i3, boolean z, int i4, Object obj) {
        if ((i4 & 16) != 0) {
            z = false;
        }
        return x(str, i, str2, i2, i3, z);
    }

    public static final String z(CharSequence charSequence, int i) {
        fs1.f(charSequence, "$this$repeat");
        int i2 = 1;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Count 'n' must be non-negative, but was " + i + '.').toString());
        } else if (i != 0) {
            if (i != 1) {
                int length = charSequence.length();
                if (length != 0) {
                    if (length != 1) {
                        StringBuilder sb = new StringBuilder(charSequence.length() * i);
                        if (1 <= i) {
                            while (true) {
                                sb.append(charSequence);
                                if (i2 == i) {
                                    break;
                                }
                                i2++;
                            }
                        }
                        String sb2 = sb.toString();
                        fs1.e(sb2, "sb.toString()");
                        return sb2;
                    }
                    char charAt = charSequence.charAt(0);
                    char[] cArr = new char[i];
                    for (int i3 = 0; i3 < i; i3++) {
                        cArr[i3] = charAt;
                    }
                    return new String(cArr);
                }
                return "";
            }
            return charSequence.toString();
        } else {
            return "";
        }
    }
}
