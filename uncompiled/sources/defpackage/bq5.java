package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: bq5  reason: default package */
/* loaded from: classes.dex */
public final class bq5 {
    public final String a;
    public final String b;
    public final long c;
    public boolean d;
    public final boolean e;
    public final long f;

    public bq5(String str, String str2, long j) {
        this(str, str2, j, false, 0L);
    }

    public bq5(String str, String str2, long j, boolean z, long j2) {
        this.a = str;
        this.b = str2;
        this.c = j;
        this.d = false;
        this.e = z;
        this.f = j2;
    }
}
