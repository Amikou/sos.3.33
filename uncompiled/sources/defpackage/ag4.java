package defpackage;

import com.onesignal.n1;

/* compiled from: UserStateEmail.java */
/* renamed from: ag4  reason: default package */
/* loaded from: classes2.dex */
public class ag4 extends n1 {
    public ag4(String str, boolean z) {
        super("email" + str, z);
    }

    @Override // com.onesignal.n1
    public void a() {
    }

    @Override // com.onesignal.n1
    public n1 p(String str) {
        return new ag4(str, false);
    }
}
