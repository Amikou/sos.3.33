package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.extractor.metadata.flac.PictureFrame;
import com.google.common.collect.ImmutableList;
import defpackage.s61;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/* compiled from: FlacMetadataReader.java */
/* renamed from: p61  reason: default package */
/* loaded from: classes.dex */
public final class p61 {

    /* compiled from: FlacMetadataReader.java */
    /* renamed from: p61$a */
    /* loaded from: classes.dex */
    public static final class a {
        public s61 a;

        public a(s61 s61Var) {
            this.a = s61Var;
        }
    }

    public static boolean a(q11 q11Var) throws IOException {
        op2 op2Var = new op2(4);
        q11Var.n(op2Var.d(), 0, 4);
        return op2Var.F() == 1716281667;
    }

    public static int b(q11 q11Var) throws IOException {
        q11Var.j();
        op2 op2Var = new op2(2);
        q11Var.n(op2Var.d(), 0, 2);
        int J = op2Var.J();
        if ((J >> 2) == 16382) {
            q11Var.j();
            return J;
        }
        q11Var.j();
        throw ParserException.createForMalformedContainer("First frame does not start with sync code.", null);
    }

    public static Metadata c(q11 q11Var, boolean z) throws IOException {
        Metadata a2 = new in1().a(q11Var, z ? null : androidx.media3.extractor.metadata.id3.a.b);
        if (a2 == null || a2.d() == 0) {
            return null;
        }
        return a2;
    }

    public static Metadata d(q11 q11Var, boolean z) throws IOException {
        q11Var.j();
        long e = q11Var.e();
        Metadata c = c(q11Var, z);
        q11Var.k((int) (q11Var.e() - e));
        return c;
    }

    public static boolean e(q11 q11Var, a aVar) throws IOException {
        q11Var.j();
        np2 np2Var = new np2(new byte[4]);
        q11Var.n(np2Var.a, 0, 4);
        boolean g = np2Var.g();
        int h = np2Var.h(7);
        int h2 = np2Var.h(24) + 4;
        if (h == 0) {
            aVar.a = h(q11Var);
        } else {
            s61 s61Var = aVar.a;
            if (s61Var == null) {
                throw new IllegalArgumentException();
            }
            if (h == 3) {
                aVar.a = s61Var.b(f(q11Var, h2));
            } else if (h == 4) {
                aVar.a = s61Var.c(j(q11Var, h2));
            } else if (h == 6) {
                op2 op2Var = new op2(h2);
                q11Var.readFully(op2Var.d(), 0, h2);
                op2Var.Q(4);
                aVar.a = s61Var.a(ImmutableList.of(PictureFrame.a(op2Var)));
            } else {
                q11Var.k(h2);
            }
        }
        return g;
    }

    public static s61.a f(q11 q11Var, int i) throws IOException {
        op2 op2Var = new op2(i);
        q11Var.readFully(op2Var.d(), 0, i);
        return g(op2Var);
    }

    public static s61.a g(op2 op2Var) {
        op2Var.Q(1);
        int G = op2Var.G();
        long e = op2Var.e() + G;
        int i = G / 18;
        long[] jArr = new long[i];
        long[] jArr2 = new long[i];
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                break;
            }
            long w = op2Var.w();
            if (w == -1) {
                jArr = Arrays.copyOf(jArr, i2);
                jArr2 = Arrays.copyOf(jArr2, i2);
                break;
            }
            jArr[i2] = w;
            jArr2[i2] = op2Var.w();
            op2Var.Q(2);
            i2++;
        }
        op2Var.Q((int) (e - op2Var.e()));
        return new s61.a(jArr, jArr2);
    }

    public static s61 h(q11 q11Var) throws IOException {
        byte[] bArr = new byte[38];
        q11Var.readFully(bArr, 0, 38);
        return new s61(bArr, 4);
    }

    public static void i(q11 q11Var) throws IOException {
        op2 op2Var = new op2(4);
        q11Var.readFully(op2Var.d(), 0, 4);
        if (op2Var.F() != 1716281667) {
            throw ParserException.createForMalformedContainer("Failed to read FLAC stream marker.", null);
        }
    }

    public static List<String> j(q11 q11Var, int i) throws IOException {
        op2 op2Var = new op2(i);
        q11Var.readFully(op2Var.d(), 0, i);
        op2Var.Q(4);
        return Arrays.asList(dl4.j(op2Var, false, false).a);
    }
}
