package defpackage;

import defpackage.lp;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: PsBinarySearchSeeker.java */
/* renamed from: iw2  reason: default package */
/* loaded from: classes.dex */
public final class iw2 extends lp {

    /* compiled from: PsBinarySearchSeeker.java */
    /* renamed from: iw2$b */
    /* loaded from: classes.dex */
    public static final class b implements lp.f {
        public final h64 a;
        public final op2 b;

        public static void d(op2 op2Var) {
            int k;
            int f = op2Var.f();
            if (op2Var.a() < 10) {
                op2Var.P(f);
                return;
            }
            op2Var.Q(9);
            int D = op2Var.D() & 7;
            if (op2Var.a() < D) {
                op2Var.P(f);
                return;
            }
            op2Var.Q(D);
            if (op2Var.a() >= 4) {
                if (iw2.k(op2Var.d(), op2Var.e()) == 443) {
                    op2Var.Q(4);
                    int J = op2Var.J();
                    if (op2Var.a() < J) {
                        op2Var.P(f);
                        return;
                    }
                    op2Var.Q(J);
                }
                while (op2Var.a() >= 4 && (k = iw2.k(op2Var.d(), op2Var.e())) != 442 && k != 441 && (k >>> 8) == 1) {
                    op2Var.Q(4);
                    if (op2Var.a() < 2) {
                        op2Var.P(f);
                        return;
                    }
                    op2Var.P(Math.min(op2Var.f(), op2Var.e() + op2Var.J()));
                }
                return;
            }
            op2Var.P(f);
        }

        @Override // defpackage.lp.f
        public lp.e a(q11 q11Var, long j) throws IOException {
            long position = q11Var.getPosition();
            int min = (int) Math.min(20000L, q11Var.getLength() - position);
            this.b.L(min);
            q11Var.n(this.b.d(), 0, min);
            return c(this.b, j, position);
        }

        @Override // defpackage.lp.f
        public void b() {
            this.b.M(androidx.media3.common.util.b.f);
        }

        public final lp.e c(op2 op2Var, long j, long j2) {
            int i = -1;
            int i2 = -1;
            long j3 = -9223372036854775807L;
            while (op2Var.a() >= 4) {
                if (iw2.k(op2Var.d(), op2Var.e()) != 442) {
                    op2Var.Q(1);
                } else {
                    op2Var.Q(4);
                    long l = jw2.l(op2Var);
                    if (l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                        long b = this.a.b(l);
                        if (b > j) {
                            if (j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                return lp.e.d(b, j2);
                            }
                            return lp.e.e(j2 + i2);
                        } else if (100000 + b > j) {
                            return lp.e.e(j2 + op2Var.e());
                        } else {
                            i2 = op2Var.e();
                            j3 = b;
                        }
                    }
                    d(op2Var);
                    i = op2Var.e();
                }
            }
            if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return lp.e.f(j3, j2 + i);
            }
            return lp.e.d;
        }

        public b(h64 h64Var) {
            this.a = h64Var;
            this.b = new op2();
        }
    }

    public iw2(h64 h64Var, long j, long j2) {
        super(new lp.b(), new b(h64Var), j, 0L, j + 1, 0L, j2, 188L, 1000);
    }

    public static int k(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }
}
