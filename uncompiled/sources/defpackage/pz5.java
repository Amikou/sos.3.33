package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: pz5  reason: default package */
/* loaded from: classes.dex */
public abstract class pz5 {
    public final boolean a(byte[] bArr, int i, int i2) {
        return b(0, bArr, i, i2) == 0;
    }

    public abstract int b(int i, byte[] bArr, int i2, int i3);
}
