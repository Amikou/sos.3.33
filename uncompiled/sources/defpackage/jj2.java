package defpackage;

import java.util.LinkedList;

/* compiled from: OOMSoftReferenceBucket.java */
/* renamed from: jj2  reason: default package */
/* loaded from: classes.dex */
public class jj2<V> extends or<V> {
    public LinkedList<ij2<V>> f;

    public jj2(int i, int i2, int i3) {
        super(i, i2, i3, false);
        this.f = new LinkedList<>();
    }

    @Override // defpackage.or
    public void a(V v) {
        ij2<V> poll = this.f.poll();
        if (poll == null) {
            poll = new ij2<>();
        }
        poll.c(v);
        this.c.add(poll);
    }

    @Override // defpackage.or
    public V g() {
        ij2<V> ij2Var = (ij2) this.c.poll();
        xt2.g(ij2Var);
        V b = ij2Var.b();
        ij2Var.a();
        this.f.add(ij2Var);
        return b;
    }
}
