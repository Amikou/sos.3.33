package defpackage;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: FileLoader.java */
/* renamed from: n31  reason: default package */
/* loaded from: classes.dex */
public class n31<Data> implements j92<File, Data> {
    public final d<Data> a;

    /* compiled from: FileLoader.java */
    /* renamed from: n31$a */
    /* loaded from: classes.dex */
    public static class a<Data> implements k92<File, Data> {
        public final d<Data> a;

        public a(d<Data> dVar) {
            this.a = dVar;
        }

        @Override // defpackage.k92
        public final void a() {
        }

        @Override // defpackage.k92
        public final j92<File, Data> c(qa2 qa2Var) {
            return new n31(this.a);
        }
    }

    /* compiled from: FileLoader.java */
    /* renamed from: n31$b */
    /* loaded from: classes.dex */
    public static class b extends a<ParcelFileDescriptor> {

        /* compiled from: FileLoader.java */
        /* renamed from: n31$b$a */
        /* loaded from: classes.dex */
        public class a implements d<ParcelFileDescriptor> {
            @Override // defpackage.n31.d
            public Class<ParcelFileDescriptor> a() {
                return ParcelFileDescriptor.class;
            }

            @Override // defpackage.n31.d
            /* renamed from: d */
            public void b(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                parcelFileDescriptor.close();
            }

            @Override // defpackage.n31.d
            /* renamed from: e */
            public ParcelFileDescriptor c(File file) throws FileNotFoundException {
                return ParcelFileDescriptor.open(file, 268435456);
            }
        }

        public b() {
            super(new a());
        }
    }

    /* compiled from: FileLoader.java */
    /* renamed from: n31$c */
    /* loaded from: classes.dex */
    public static final class c<Data> implements com.bumptech.glide.load.data.d<Data> {
        public final File a;
        public final d<Data> f0;
        public Data g0;

        public c(File file, d<Data> dVar) {
            this.a = file;
            this.f0 = dVar;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<Data> a() {
            return this.f0.a();
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
            Data data = this.g0;
            if (data != null) {
                try {
                    this.f0.b(data);
                } catch (IOException unused) {
                }
            }
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.Object, Data] */
        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super Data> aVar) {
            try {
                Data c = this.f0.c(this.a);
                this.g0 = c;
                aVar.f(c);
            } catch (FileNotFoundException e) {
                aVar.c(e);
            }
        }
    }

    /* compiled from: FileLoader.java */
    /* renamed from: n31$d */
    /* loaded from: classes.dex */
    public interface d<Data> {
        Class<Data> a();

        void b(Data data) throws IOException;

        Data c(File file) throws FileNotFoundException;
    }

    /* compiled from: FileLoader.java */
    /* renamed from: n31$e */
    /* loaded from: classes.dex */
    public static class e extends a<InputStream> {

        /* compiled from: FileLoader.java */
        /* renamed from: n31$e$a */
        /* loaded from: classes.dex */
        public class a implements d<InputStream> {
            @Override // defpackage.n31.d
            public Class<InputStream> a() {
                return InputStream.class;
            }

            @Override // defpackage.n31.d
            /* renamed from: d */
            public void b(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            @Override // defpackage.n31.d
            /* renamed from: e */
            public InputStream c(File file) throws FileNotFoundException {
                return new FileInputStream(file);
            }
        }

        public e() {
            super(new a());
        }
    }

    public n31(d<Data> dVar) {
        this.a = dVar;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(File file, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(file), new c(file, this.a));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(File file) {
        return true;
    }
}
