package defpackage;

import com.onesignal.influence.domain.a;
import java.util.List;
import java.util.Set;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: OSOutcomeEventsRepository.kt */
/* renamed from: jk2  reason: default package */
/* loaded from: classes2.dex */
public abstract class jk2 implements ik2 {
    public final yj2 a;
    public final fk2 b;
    public final co2 c;

    public jk2(yj2 yj2Var, fk2 fk2Var, co2 co2Var) {
        fs1.f(yj2Var, "logger");
        fs1.f(fk2Var, "outcomeEventsCache");
        fs1.f(co2Var, "outcomeEventsService");
        this.a = yj2Var;
        this.b = fk2Var;
        this.c = co2Var;
    }

    @Override // defpackage.ik2
    public List<a> a(String str, List<a> list) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        fs1.f(list, "influences");
        List<a> g = this.b.g(str, list);
        yj2 yj2Var = this.a;
        yj2Var.debug("OneSignal getNotCachedUniqueOutcome influences: " + g);
        return g;
    }

    @Override // defpackage.ik2
    public void b(dk2 dk2Var) {
        fs1.f(dk2Var, "event");
        this.b.k(dk2Var);
    }

    @Override // defpackage.ik2
    public List<dk2> c() {
        return this.b.e();
    }

    @Override // defpackage.ik2
    public void d(Set<String> set) {
        fs1.f(set, "unattributedUniqueOutcomeEvents");
        yj2 yj2Var = this.a;
        yj2Var.debug("OneSignal save unattributedUniqueOutcomeEvents: " + set);
        this.b.l(set);
    }

    @Override // defpackage.ik2
    public void e(dk2 dk2Var) {
        fs1.f(dk2Var, "outcomeEvent");
        this.b.d(dk2Var);
    }

    @Override // defpackage.ik2
    public void f(String str, String str2) {
        fs1.f(str, "notificationTableName");
        fs1.f(str2, "notificationIdColumnName");
        this.b.c(str, str2);
    }

    @Override // defpackage.ik2
    public Set<String> h() {
        Set<String> i = this.b.i();
        yj2 yj2Var = this.a;
        yj2Var.debug("OneSignal getUnattributedUniqueOutcomeEventsSentByChannel: " + i);
        return i;
    }

    @Override // defpackage.ik2
    public void i(dk2 dk2Var) {
        fs1.f(dk2Var, "eventParams");
        this.b.m(dk2Var);
    }

    public final yj2 j() {
        return this.a;
    }

    public final co2 k() {
        return this.c;
    }
}
