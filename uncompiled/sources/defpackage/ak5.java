package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ak5  reason: default package */
/* loaded from: classes.dex */
public final class ak5 implements Runnable {
    public final /* synthetic */ gm5 a;
    public final /* synthetic */ ck5 f0;

    public ak5(ck5 ck5Var, gm5 gm5Var) {
        this.f0 = ck5Var;
        this.a = gm5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ck5.s(this.f0, this.a);
        this.f0.y(this.a.g);
    }
}
