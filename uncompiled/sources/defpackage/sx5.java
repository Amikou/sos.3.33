package defpackage;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: sx5  reason: default package */
/* loaded from: classes.dex */
public final class sx5 extends ux5<FieldDescriptorType, Object> {
    public sx5(int i) {
        super(i, null);
    }

    @Override // defpackage.ux5
    public final void e() {
        if (!i()) {
            for (int i = 0; i < j(); i++) {
                Map.Entry<FieldDescriptorType, Object> h = h(i);
                if (((pr5) h.getKey()).b()) {
                    h.setValue(Collections.unmodifiableList((List) h.getValue()));
                }
            }
            Iterator it = m().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (((pr5) entry.getKey()).b()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.e();
    }
}
