package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktSelectLanguageBinding.java */
/* renamed from: x6  reason: default package */
/* loaded from: classes2.dex */
public final class x6 {
    public final ConstraintLayout a;
    public final EditText b;
    public final RecyclerView c;
    public final sp3 d;
    public final TextView e;

    public x6(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, EditText editText, RecyclerView recyclerView, sp3 sp3Var, TextView textView, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = editText;
        this.c = recyclerView;
        this.d = sp3Var;
        this.e = textView;
    }

    public static x6 a(View view) {
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        int i = R.id.etSearch;
        EditText editText = (EditText) ai4.a(view, R.id.etSearch);
        if (editText != null) {
            i = R.id.rvRealLanguage;
            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvRealLanguage);
            if (recyclerView != null) {
                i = R.id.toolbar;
                View a = ai4.a(view, R.id.toolbar);
                if (a != null) {
                    sp3 a2 = sp3.a(a);
                    i = R.id.tv_not_found;
                    TextView textView = (TextView) ai4.a(view, R.id.tv_not_found);
                    if (textView != null) {
                        i = R.id.vSearchContainer;
                        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vSearchContainer);
                        if (linearLayout != null) {
                            return new x6(constraintLayout, constraintLayout, editText, recyclerView, a2, textView, linearLayout);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static x6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static x6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_select_language, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
