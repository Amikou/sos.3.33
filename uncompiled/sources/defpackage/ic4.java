package defpackage;

import android.text.Layout;
import androidx.media3.extractor.text.SubtitleDecoderException;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import zendesk.support.request.CellBase;

/* compiled from: TtmlDecoder.java */
/* renamed from: ic4  reason: default package */
/* loaded from: classes.dex */
public final class ic4 extends androidx.media3.extractor.text.a {
    public static final Pattern o = Pattern.compile("^([0-9][0-9]+):([0-9][0-9]):([0-9][0-9])(?:(\\.[0-9]+)|:([0-9][0-9])(?:\\.([0-9]+))?)?$");
    public static final Pattern p = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?)(h|m|s|ms|f|t)$");
    public static final Pattern q = Pattern.compile("^(([0-9]*.)?[0-9]+)(px|em|%)$");
    public static final Pattern r = Pattern.compile("^([-+]?\\d+\\.?\\d*?)%$");
    public static final Pattern s = Pattern.compile("^(\\d+\\.?\\d*?)% (\\d+\\.?\\d*?)%$");
    public static final Pattern t = Pattern.compile("^(\\d+\\.?\\d*?)px (\\d+\\.?\\d*?)px$");
    public static final Pattern u = Pattern.compile("^(\\d+) (\\d+)$");
    public static final b v = new b(30.0f, 1, 1);
    public static final a w = new a(32, 15);
    public final XmlPullParserFactory n;

    /* compiled from: TtmlDecoder.java */
    /* renamed from: ic4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;

        public a(int i, int i2) {
            this.a = i2;
        }
    }

    /* compiled from: TtmlDecoder.java */
    /* renamed from: ic4$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final float a;
        public final int b;
        public final int c;

        public b(float f, int i, int i2) {
            this.a = f;
            this.b = i;
            this.c = i2;
        }
    }

    /* compiled from: TtmlDecoder.java */
    /* renamed from: ic4$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final int a;
        public final int b;

        public c(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    public ic4() {
        super("TtmlDecoder");
        try {
            XmlPullParserFactory newInstance = XmlPullParserFactory.newInstance();
            this.n = newInstance;
            newInstance.setNamespaceAware(true);
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    public static mc4 C(mc4 mc4Var) {
        return mc4Var == null ? new mc4() : mc4Var;
    }

    public static boolean D(String str) {
        return str.equals("tt") || str.equals("head") || str.equals("body") || str.equals("div") || str.equals("p") || str.equals("span") || str.equals("br") || str.equals("style") || str.equals("styling") || str.equals("layout") || str.equals("region") || str.equals("metadata") || str.equals("image") || str.equals("data") || str.equals("information");
    }

    public static Layout.Alignment E(String str) {
        String e = ei.e(str);
        e.hashCode();
        char c2 = 65535;
        switch (e.hashCode()) {
            case -1364013995:
                if (e.equals("center")) {
                    c2 = 0;
                    break;
                }
                break;
            case 100571:
                if (e.equals("end")) {
                    c2 = 1;
                    break;
                }
                break;
            case 3317767:
                if (e.equals("left")) {
                    c2 = 2;
                    break;
                }
                break;
            case 108511772:
                if (e.equals("right")) {
                    c2 = 3;
                    break;
                }
                break;
            case 109757538:
                if (e.equals("start")) {
                    c2 = 4;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return Layout.Alignment.ALIGN_CENTER;
            case 1:
            case 3:
                return Layout.Alignment.ALIGN_OPPOSITE;
            case 2:
            case 4:
                return Layout.Alignment.ALIGN_NORMAL;
            default:
                return null;
        }
    }

    public static a F(XmlPullParser xmlPullParser, a aVar) throws SubtitleDecoderException {
        String attributeValue = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "cellResolution");
        if (attributeValue == null) {
            return aVar;
        }
        Matcher matcher = u.matcher(attributeValue);
        if (!matcher.matches()) {
            p12.i("TtmlDecoder", "Ignoring malformed cell resolution: " + attributeValue);
            return aVar;
        }
        try {
            int parseInt = Integer.parseInt((String) ii.e(matcher.group(1)));
            int parseInt2 = Integer.parseInt((String) ii.e(matcher.group(2)));
            if (parseInt != 0 && parseInt2 != 0) {
                return new a(parseInt, parseInt2);
            }
            throw new SubtitleDecoderException("Invalid cell resolution " + parseInt + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + parseInt2);
        } catch (NumberFormatException unused) {
            p12.i("TtmlDecoder", "Ignoring malformed cell resolution: " + attributeValue);
            return aVar;
        }
    }

    public static void G(String str, mc4 mc4Var) throws SubtitleDecoderException {
        Matcher matcher;
        String[] L0 = androidx.media3.common.util.b.L0(str, "\\s+");
        if (L0.length == 1) {
            matcher = q.matcher(str);
        } else if (L0.length == 2) {
            matcher = q.matcher(L0[1]);
            p12.i("TtmlDecoder", "Multiple values in fontSize attribute. Picking the second value for vertical font size and ignoring the first.");
        } else {
            throw new SubtitleDecoderException("Invalid number of entries for fontSize: " + L0.length + ".");
        }
        if (matcher.matches()) {
            String str2 = (String) ii.e(matcher.group(3));
            str2.hashCode();
            char c2 = 65535;
            switch (str2.hashCode()) {
                case 37:
                    if (str2.equals("%")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 3240:
                    if (str2.equals("em")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 3592:
                    if (str2.equals("px")) {
                        c2 = 2;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    mc4Var.z(3);
                    break;
                case 1:
                    mc4Var.z(2);
                    break;
                case 2:
                    mc4Var.z(1);
                    break;
                default:
                    throw new SubtitleDecoderException("Invalid unit for fontSize: '" + str2 + "'.");
            }
            mc4Var.y(Float.parseFloat((String) ii.e(matcher.group(1))));
            return;
        }
        throw new SubtitleDecoderException("Invalid expression for fontSize: '" + str + "'.");
    }

    public static b H(XmlPullParser xmlPullParser) throws SubtitleDecoderException {
        String attributeValue = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "frameRate");
        int parseInt = attributeValue != null ? Integer.parseInt(attributeValue) : 30;
        float f = 1.0f;
        String attributeValue2 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "frameRateMultiplier");
        if (attributeValue2 != null) {
            String[] L0 = androidx.media3.common.util.b.L0(attributeValue2, MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            if (L0.length == 2) {
                f = Integer.parseInt(L0[0]) / Integer.parseInt(L0[1]);
            } else {
                throw new SubtitleDecoderException("frameRateMultiplier doesn't have 2 parts");
            }
        }
        b bVar = v;
        int i = bVar.b;
        String attributeValue3 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "subFrameRate");
        if (attributeValue3 != null) {
            i = Integer.parseInt(attributeValue3);
        }
        int i2 = bVar.c;
        String attributeValue4 = xmlPullParser.getAttributeValue("http://www.w3.org/ns/ttml#parameter", "tickRate");
        if (attributeValue4 != null) {
            i2 = Integer.parseInt(attributeValue4);
        }
        return new b(parseInt * f, i, i2);
    }

    public static Map<String, mc4> I(XmlPullParser xmlPullParser, Map<String, mc4> map, a aVar, c cVar, Map<String, kc4> map2, Map<String, String> map3) throws IOException, XmlPullParserException {
        do {
            xmlPullParser.next();
            if (androidx.media3.common.util.c.f(xmlPullParser, "style")) {
                String a2 = androidx.media3.common.util.c.a(xmlPullParser, "style");
                mc4 N = N(xmlPullParser, new mc4());
                if (a2 != null) {
                    for (String str : O(a2)) {
                        N.a(map.get(str));
                    }
                }
                String g = N.g();
                if (g != null) {
                    map.put(g, N);
                }
            } else if (androidx.media3.common.util.c.f(xmlPullParser, "region")) {
                kc4 L = L(xmlPullParser, aVar, cVar);
                if (L != null) {
                    map2.put(L.a, L);
                }
            } else if (androidx.media3.common.util.c.f(xmlPullParser, "metadata")) {
                J(xmlPullParser, map3);
            }
        } while (!androidx.media3.common.util.c.d(xmlPullParser, "head"));
        return map;
    }

    public static void J(XmlPullParser xmlPullParser, Map<String, String> map) throws IOException, XmlPullParserException {
        String a2;
        do {
            xmlPullParser.next();
            if (androidx.media3.common.util.c.f(xmlPullParser, "image") && (a2 = androidx.media3.common.util.c.a(xmlPullParser, "id")) != null) {
                map.put(a2, xmlPullParser.nextText());
            }
        } while (!androidx.media3.common.util.c.d(xmlPullParser, "metadata"));
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static jc4 K(XmlPullParser xmlPullParser, jc4 jc4Var, Map<String, kc4> map, b bVar) throws SubtitleDecoderException {
        long j;
        long j2;
        char c2;
        int attributeCount = xmlPullParser.getAttributeCount();
        mc4 N = N(xmlPullParser, null);
        String str = null;
        String str2 = "";
        long j3 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        long j4 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        long j5 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        String[] strArr = null;
        for (int i = 0; i < attributeCount; i++) {
            String attributeName = xmlPullParser.getAttributeName(i);
            String attributeValue = xmlPullParser.getAttributeValue(i);
            attributeName.hashCode();
            switch (attributeName.hashCode()) {
                case -934795532:
                    if (attributeName.equals("region")) {
                        c2 = 0;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 99841:
                    if (attributeName.equals("dur")) {
                        c2 = 1;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 100571:
                    if (attributeName.equals("end")) {
                        c2 = 2;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 93616297:
                    if (attributeName.equals("begin")) {
                        c2 = 3;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 109780401:
                    if (attributeName.equals("style")) {
                        c2 = 4;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1292595405:
                    if (attributeName.equals("backgroundImage")) {
                        c2 = 5;
                        break;
                    }
                    c2 = 65535;
                    break;
                default:
                    c2 = 65535;
                    break;
            }
            switch (c2) {
                case 0:
                    if (map.containsKey(attributeValue)) {
                        str2 = attributeValue;
                        continue;
                    }
                case 1:
                    j5 = P(attributeValue, bVar);
                    break;
                case 2:
                    j4 = P(attributeValue, bVar);
                    break;
                case 3:
                    j3 = P(attributeValue, bVar);
                    break;
                case 4:
                    String[] O = O(attributeValue);
                    if (O.length > 0) {
                        strArr = O;
                        break;
                    }
                    break;
                case 5:
                    if (attributeValue.startsWith("#")) {
                        str = attributeValue.substring(1);
                        break;
                    }
                    break;
            }
        }
        if (jc4Var != null) {
            long j6 = jc4Var.d;
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            if (j6 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    j3 += j6;
                }
                if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    j4 += j6;
                }
            }
        } else {
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        long j7 = j3;
        if (j4 == j) {
            if (j5 != j) {
                j2 = j7 + j5;
            } else if (jc4Var != null) {
                long j8 = jc4Var.e;
                if (j8 != j) {
                    j2 = j8;
                }
            }
            return jc4.c(xmlPullParser.getName(), j7, j2, N, strArr, str2, str, jc4Var);
        }
        j2 = j4;
        return jc4.c(xmlPullParser.getName(), j7, j2, N, strArr, str2, str, jc4Var);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:59:0x01a7, code lost:
        if (r0.equals("tb") == false) goto L40;
     */
    /* JADX WARN: Removed duplicated region for block: B:47:0x017a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.kc4 L(org.xmlpull.v1.XmlPullParser r17, defpackage.ic4.a r18, defpackage.ic4.c r19) {
        /*
            Method dump skipped, instructions count: 564
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ic4.L(org.xmlpull.v1.XmlPullParser, ic4$a, ic4$c):kc4");
    }

    public static float M(String str) {
        Matcher matcher = r.matcher(str);
        if (!matcher.matches()) {
            p12.i("TtmlDecoder", "Invalid value for shear: " + str);
            return Float.MAX_VALUE;
        }
        try {
            return Math.min(100.0f, Math.max(-100.0f, Float.parseFloat((String) ii.e(matcher.group(1)))));
        } catch (NumberFormatException e) {
            p12.j("TtmlDecoder", "Failed to parse shear: " + str, e);
            return Float.MAX_VALUE;
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:102:0x01e0, code lost:
        if (r3.equals(org.web3j.ens.contracts.generated.PublicResolver.FUNC_TEXT) == false) goto L49;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.mc4 N(org.xmlpull.v1.XmlPullParser r12, defpackage.mc4 r13) {
        /*
            Method dump skipped, instructions count: 928
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ic4.N(org.xmlpull.v1.XmlPullParser, mc4):mc4");
    }

    public static String[] O(String str) {
        String trim = str.trim();
        return trim.isEmpty() ? new String[0] : androidx.media3.common.util.b.L0(trim, "\\s+");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x00bc, code lost:
        if (r13.equals("ms") == false) goto L21;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static long P(java.lang.String r13, defpackage.ic4.b r14) throws androidx.media3.extractor.text.SubtitleDecoderException {
        /*
            Method dump skipped, instructions count: 326
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ic4.P(java.lang.String, ic4$b):long");
    }

    public static c Q(XmlPullParser xmlPullParser) {
        String a2 = androidx.media3.common.util.c.a(xmlPullParser, "extent");
        if (a2 == null) {
            return null;
        }
        Matcher matcher = t.matcher(a2);
        if (!matcher.matches()) {
            p12.i("TtmlDecoder", "Ignoring non-pixel tts extent: " + a2);
            return null;
        }
        try {
            return new c(Integer.parseInt((String) ii.e(matcher.group(1))), Integer.parseInt((String) ii.e(matcher.group(2))));
        } catch (NumberFormatException unused) {
            p12.i("TtmlDecoder", "Ignoring malformed tts extent: " + a2);
            return null;
        }
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        b bVar;
        try {
            XmlPullParser newPullParser = this.n.newPullParser();
            HashMap hashMap = new HashMap();
            HashMap hashMap2 = new HashMap();
            HashMap hashMap3 = new HashMap();
            hashMap2.put("", new kc4(""));
            c cVar = null;
            newPullParser.setInput(new ByteArrayInputStream(bArr, 0, i), null);
            ArrayDeque arrayDeque = new ArrayDeque();
            b bVar2 = v;
            a aVar = w;
            int i2 = 0;
            nc4 nc4Var = null;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.getEventType()) {
                jc4 jc4Var = (jc4) arrayDeque.peek();
                if (i2 == 0) {
                    String name = newPullParser.getName();
                    if (eventType == 2) {
                        if ("tt".equals(name)) {
                            bVar2 = H(newPullParser);
                            aVar = F(newPullParser, w);
                            cVar = Q(newPullParser);
                        }
                        c cVar2 = cVar;
                        b bVar3 = bVar2;
                        a aVar2 = aVar;
                        if (D(name)) {
                            if ("head".equals(name)) {
                                bVar = bVar3;
                                I(newPullParser, hashMap, aVar2, cVar2, hashMap2, hashMap3);
                            } else {
                                bVar = bVar3;
                                try {
                                    jc4 K = K(newPullParser, jc4Var, hashMap2, bVar);
                                    arrayDeque.push(K);
                                    if (jc4Var != null) {
                                        jc4Var.a(K);
                                    }
                                } catch (SubtitleDecoderException e) {
                                    p12.j("TtmlDecoder", "Suppressing parser error", e);
                                    i2++;
                                }
                            }
                            bVar2 = bVar;
                        } else {
                            p12.f("TtmlDecoder", "Ignoring unsupported tag: " + newPullParser.getName());
                            i2++;
                            bVar2 = bVar3;
                        }
                        cVar = cVar2;
                        aVar = aVar2;
                    } else if (eventType == 4) {
                        ((jc4) ii.e(jc4Var)).a(jc4.d(newPullParser.getText()));
                    } else if (eventType == 3) {
                        if (newPullParser.getName().equals("tt")) {
                            nc4Var = new nc4((jc4) ii.e((jc4) arrayDeque.peek()), hashMap, hashMap2, hashMap3);
                        }
                        arrayDeque.pop();
                    }
                } else if (eventType == 2) {
                    i2++;
                } else if (eventType == 3) {
                    i2--;
                }
                newPullParser.next();
            }
            if (nc4Var != null) {
                return nc4Var;
            }
            throw new SubtitleDecoderException("No TTML subtitles found");
        } catch (IOException e2) {
            throw new IllegalStateException("Unexpected error when reading input.", e2);
        } catch (XmlPullParserException e3) {
            throw new SubtitleDecoderException("Unable to decode source", e3);
        }
    }
}
