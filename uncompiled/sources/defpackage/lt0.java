package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: lt0  reason: default package */
/* loaded from: classes2.dex */
public class lt0 {
    public static pr4 a(at0 at0Var) {
        if (at0Var == null) {
            return null;
        }
        return new pr4(at0Var.a(), at0Var.b(), at0Var.d(), at0Var.c(), at0Var.e());
    }

    public static pr4 b(String str) {
        pr4 b = mr4.b(str);
        if (b == null) {
            b = fa3.h(str);
        }
        if (b == null) {
            b = sc2.b(str);
        }
        if (b == null) {
            b = x34.d(str);
        }
        if (b == null) {
            b = u3.f(str);
        }
        if (b == null) {
            b = a(dt0.a(str));
        }
        return b == null ? oe1.f(str) : b;
    }

    public static pr4 c(i iVar) {
        pr4 c = mr4.c(iVar);
        if (c == null) {
            c = fa3.i(iVar);
        }
        if (c == null) {
            c = x34.e(iVar);
        }
        if (c == null) {
            c = u3.g(iVar);
        }
        if (c == null) {
            c = a(dt0.b(iVar));
        }
        return c == null ? oe1.g(iVar) : c;
    }

    public static String d(i iVar) {
        String d = mr4.d(iVar);
        if (d == null) {
            d = fa3.j(iVar);
        }
        if (d == null) {
            d = sc2.d(iVar);
        }
        if (d == null) {
            d = x34.f(iVar);
        }
        if (d == null) {
            d = u3.h(iVar);
        }
        if (d == null) {
            d = dt0.c(iVar);
        }
        return d == null ? oe1.h(iVar) : d;
    }

    public static i e(String str) {
        i e = mr4.e(str);
        if (e == null) {
            e = fa3.k(str);
        }
        if (e == null) {
            e = sc2.e(str);
        }
        if (e == null) {
            e = x34.g(str);
        }
        if (e == null) {
            e = u3.i(str);
        }
        if (e == null) {
            e = dt0.d(str);
        }
        return e == null ? oe1.i(str) : e;
    }
}
