package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;

/* compiled from: CoinPriceAlertDao.kt */
/* renamed from: l00  reason: default package */
/* loaded from: classes2.dex */
public interface l00 {
    LiveData<List<RoomCoinPriceAlert>> a();

    Object b(q70<? super List<RoomCoinPriceAlert>> q70Var);

    Object c(int i, q70<? super te4> q70Var);

    Object d(RoomCoinPriceAlert roomCoinPriceAlert, q70<? super te4> q70Var);
}
