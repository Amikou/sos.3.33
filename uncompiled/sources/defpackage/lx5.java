package defpackage;

import com.google.android.gms.internal.measurement.a2;
import com.google.android.gms.internal.measurement.b2;
import com.google.android.gms.internal.measurement.c2;
import com.google.android.gms.internal.measurement.d2;
import com.google.android.gms.internal.measurement.x1;
import com.google.protobuf.m;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: lx5  reason: default package */
/* loaded from: classes.dex */
public final class lx5 implements py5 {
    public static final tx5 b = new ax5();
    public final tx5 a;

    public lx5() {
        tx5 tx5Var;
        tx5[] tx5VarArr = new tx5[2];
        tx5VarArr[0] = pu5.c();
        try {
            Set<String> set = m.a;
            tx5Var = (tx5) m.class.getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            tx5Var = b;
        }
        tx5VarArr[1] = tx5Var;
        cx5 cx5Var = new cx5(tx5VarArr);
        cw5.b(cx5Var, "messageInfoFactory");
        this.a = cx5Var;
    }

    public static boolean b(rx5 rx5Var) {
        return rx5Var.zzc() == 1;
    }

    @Override // defpackage.py5
    public final <T> c2<T> a(Class<T> cls) {
        d2.A(cls);
        rx5 a = this.a.a(cls);
        if (a.zza()) {
            if (x1.class.isAssignableFrom(cls)) {
                return b2.b(d2.c(), zt5.a(), a.zzb());
            }
            return b2.b(d2.a(), zt5.b(), a.zzb());
        } else if (x1.class.isAssignableFrom(cls)) {
            if (b(a)) {
                return a2.E(cls, a, fy5.b(), ww5.d(), d2.c(), zt5.a(), px5.b());
            }
            return a2.E(cls, a, fy5.b(), ww5.d(), d2.c(), null, px5.b());
        } else if (b(a)) {
            return a2.E(cls, a, fy5.a(), ww5.c(), d2.a(), zt5.b(), px5.a());
        } else {
            return a2.E(cls, a, fy5.a(), ww5.c(), d2.b(), null, px5.a());
        }
    }
}
