package defpackage;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import net.safemoon.androidwallet.R;

/* compiled from: DialogV2Help.kt */
/* renamed from: no0  reason: default package */
/* loaded from: classes2.dex */
public final class no0 extends sn0 {
    public static final a x0 = new a(null);
    public oo0 u0;
    public rc1<te4> v0;
    public rc1<te4> w0;

    /* compiled from: DialogV2Help.kt */
    /* renamed from: no0$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final no0 a(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
            no0 no0Var = new no0();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            no0Var.setArguments(bundle);
            no0Var.v0 = rc1Var;
            no0Var.w0 = rc1Var2;
            return no0Var;
        }
    }

    public static final void A(no0 no0Var, View view) {
        fs1.f(no0Var, "this$0");
        no0Var.h();
    }

    public static final void B(no0 no0Var, View view) {
        fs1.f(no0Var, "this$0");
        no0Var.h();
        rc1<te4> rc1Var = no0Var.v0;
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public static final void C(no0 no0Var, View view) {
        fs1.f(no0Var, "this$0");
        no0Var.h();
        rc1<te4> rc1Var = no0Var.w0;
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public final void D(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, no0.class.getCanonicalName());
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_v2_help, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        oo0 a2 = oo0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        oo0 oo0Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: lo0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                no0.A(no0.this, view2);
            }
        });
        oo0 oo0Var2 = this.u0;
        if (oo0Var2 == null) {
            fs1.r("binding");
            oo0Var2 = null;
        }
        oo0Var2.a.setOnClickListener(new View.OnClickListener() { // from class: ko0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                no0.B(no0.this, view2);
            }
        });
        oo0 oo0Var3 = this.u0;
        if (oo0Var3 == null) {
            fs1.r("binding");
            oo0Var3 = null;
        }
        oo0Var3.b.setOnClickListener(new View.OnClickListener() { // from class: mo0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                no0.C(no0.this, view2);
            }
        });
        oo0 oo0Var4 = this.u0;
        if (oo0Var4 == null) {
            fs1.r("binding");
        } else {
            oo0Var = oo0Var4;
        }
        oo0Var.b.setVisibility(e30.m0(this.w0 != null));
    }
}
