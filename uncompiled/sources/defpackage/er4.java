package defpackage;

/* compiled from: WormAnimationValue.java */
/* renamed from: er4  reason: default package */
/* loaded from: classes2.dex */
public class er4 implements wg4 {
    public int a;
    public int b;

    public int a() {
        return this.b;
    }

    public int b() {
        return this.a;
    }

    public void c(int i) {
        this.b = i;
    }

    public void d(int i) {
        this.a = i;
    }
}
