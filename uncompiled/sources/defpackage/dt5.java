package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: dt5  reason: default package */
/* loaded from: classes.dex */
public final class dt5 implements ServiceConnection, b.a, b.InterfaceC0111b {
    public volatile boolean a;
    public volatile eg5 f0;
    public final /* synthetic */ p g0;

    public dt5(p pVar) {
        this.g0 = pVar;
    }

    public final void a(Intent intent) {
        dt5 dt5Var;
        this.g0.e();
        Context m = this.g0.a.m();
        v50 b = v50.b();
        synchronized (this) {
            if (this.a) {
                this.g0.a.w().v().a("Connection attempt already in progress");
                return;
            }
            this.g0.a.w().v().a("Using local app measurement service");
            this.a = true;
            dt5Var = this.g0.c;
            b.a(m, intent, dt5Var, 129);
        }
    }

    public final void b() {
        if (this.f0 != null && (this.f0.c() || this.f0.k())) {
            this.f0.b();
        }
        this.f0 = null;
    }

    public final void c() {
        this.g0.e();
        Context m = this.g0.a.m();
        synchronized (this) {
            if (this.a) {
                this.g0.a.w().v().a("Connection attempt already in progress");
            } else if (this.f0 != null && (this.f0.k() || this.f0.c())) {
                this.g0.a.w().v().a("Already awaiting connection attempt");
            } else {
                this.f0 = new eg5(m, Looper.getMainLooper(), this, this);
                this.g0.a.w().v().a("Connecting to remote service");
                this.a = true;
                zt2.j(this.f0);
                this.f0.w();
            }
        }
    }

    @Override // com.google.android.gms.common.internal.b.a
    public final void onConnected(Bundle bundle) {
        zt2.e("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                zt2.j(this.f0);
                this.g0.a.q().p(new xs5(this, this.f0.G()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.f0 = null;
                this.a = false;
            }
        }
    }

    @Override // com.google.android.gms.common.internal.b.InterfaceC0111b
    public final void onConnectionFailed(ConnectionResult connectionResult) {
        zt2.e("MeasurementServiceConnection.onConnectionFailed");
        og5 B = this.g0.a.B();
        if (B != null) {
            B.p().b("Service connection failed", connectionResult);
        }
        synchronized (this) {
            this.a = false;
            this.f0 = null;
        }
        this.g0.a.q().p(new bt5(this));
    }

    @Override // com.google.android.gms.common.internal.b.a
    public final void onConnectionSuspended(int i) {
        zt2.e("MeasurementServiceConnection.onConnectionSuspended");
        this.g0.a.w().u().a("Service connection suspended");
        this.g0.a.q().p(new zs5(this));
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        dt5 dt5Var;
        d bVar;
        zt2.e("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.a = false;
                this.g0.a.w().l().a("Service connected with null binder");
                return;
            }
            d dVar = null;
            try {
                String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (queryLocalInterface instanceof d) {
                        bVar = (d) queryLocalInterface;
                    } else {
                        bVar = new com.google.android.gms.measurement.internal.b(iBinder);
                    }
                    dVar = bVar;
                    this.g0.a.w().v().a("Bound to IMeasurementService interface");
                } else {
                    this.g0.a.w().l().b("Got binder with a wrong descriptor", interfaceDescriptor);
                }
            } catch (RemoteException unused) {
                this.g0.a.w().l().a("Service connect failed to get IMeasurementService");
            }
            if (dVar == null) {
                this.a = false;
                try {
                    v50 b = v50.b();
                    Context m = this.g0.a.m();
                    dt5Var = this.g0.c;
                    b.c(m, dt5Var);
                } catch (IllegalArgumentException unused2) {
                }
            } else {
                this.g0.a.q().p(new ss5(this, dVar));
            }
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        zt2.e("MeasurementServiceConnection.onServiceDisconnected");
        this.g0.a.w().u().a("Service disconnected");
        this.g0.a.q().p(new us5(this, componentName));
    }
}
