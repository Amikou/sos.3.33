package defpackage;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: ov4  reason: default package */
/* loaded from: classes2.dex */
public final class ov4 extends WeakReference<Throwable> {
    public final int a;

    public ov4(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        this.a = System.identityHashCode(th);
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == ov4.class) {
            if (this == obj) {
                return true;
            }
            ov4 ov4Var = (ov4) obj;
            if (this.a == ov4Var.a && get() == ov4Var.get()) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return this.a;
    }
}
