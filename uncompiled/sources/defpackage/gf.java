package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import defpackage.b83;

/* compiled from: AppCompatDrawableManager.java */
/* renamed from: gf  reason: default package */
/* loaded from: classes.dex */
public final class gf {
    public static final PorterDuff.Mode b = PorterDuff.Mode.SRC_IN;
    public static gf c;
    public b83 a;

    /* compiled from: AppCompatDrawableManager.java */
    /* renamed from: gf$a */
    /* loaded from: classes.dex */
    public class a implements b83.f {
        public final int[] a = {vz2.abc_textfield_search_default_mtrl_alpha, vz2.abc_textfield_default_mtrl_alpha, vz2.abc_ab_share_pack_mtrl_alpha};
        public final int[] b = {vz2.abc_ic_commit_search_api_mtrl_alpha, vz2.abc_seekbar_tick_mark_material, vz2.abc_ic_menu_share_mtrl_alpha, vz2.abc_ic_menu_copy_mtrl_am_alpha, vz2.abc_ic_menu_cut_mtrl_alpha, vz2.abc_ic_menu_selectall_mtrl_alpha, vz2.abc_ic_menu_paste_mtrl_am_alpha};
        public final int[] c = {vz2.abc_textfield_activated_mtrl_alpha, vz2.abc_textfield_search_activated_mtrl_alpha, vz2.abc_cab_background_top_mtrl_alpha, vz2.abc_text_cursor_material, vz2.abc_text_select_handle_left_mtrl, vz2.abc_text_select_handle_middle_mtrl, vz2.abc_text_select_handle_right_mtrl};
        public final int[] d = {vz2.abc_popup_background_mtrl_mult, vz2.abc_cab_background_internal_bg, vz2.abc_menu_hardkey_panel_mtrl_mult};
        public final int[] e = {vz2.abc_tab_indicator_material, vz2.abc_textfield_search_material};
        public final int[] f = {vz2.abc_btn_check_material, vz2.abc_btn_radio_material, vz2.abc_btn_check_material_anim, vz2.abc_btn_radio_material_anim};

        @Override // defpackage.b83.f
        public Drawable a(b83 b83Var, Context context, int i) {
            if (i == vz2.abc_cab_background_top_material) {
                return new LayerDrawable(new Drawable[]{b83Var.j(context, vz2.abc_cab_background_internal_bg), b83Var.j(context, vz2.abc_cab_background_top_mtrl_alpha)});
            }
            if (i == vz2.abc_ratingbar_material) {
                return l(b83Var, context, lz2.abc_star_big);
            }
            if (i == vz2.abc_ratingbar_indicator_material) {
                return l(b83Var, context, lz2.abc_star_medium);
            }
            if (i == vz2.abc_ratingbar_small_material) {
                return l(b83Var, context, lz2.abc_star_small);
            }
            return null;
        }

        /* JADX WARN: Removed duplicated region for block: B:21:0x0046  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x0061 A[RETURN] */
        @Override // defpackage.b83.f
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public boolean b(android.content.Context r7, int r8, android.graphics.drawable.Drawable r9) {
            /*
                r6 = this;
                android.graphics.PorterDuff$Mode r0 = defpackage.gf.a()
                int[] r1 = r6.a
                boolean r1 = r6.f(r1, r8)
                r2 = 16842801(0x1010031, float:2.3693695E-38)
                r3 = -1
                r4 = 0
                r5 = 1
                if (r1 == 0) goto L17
                int r2 = defpackage.jy2.colorControlNormal
            L14:
                r8 = r3
            L15:
                r1 = r5
                goto L44
            L17:
                int[] r1 = r6.c
                boolean r1 = r6.f(r1, r8)
                if (r1 == 0) goto L22
                int r2 = defpackage.jy2.colorControlActivated
                goto L14
            L22:
                int[] r1 = r6.d
                boolean r1 = r6.f(r1, r8)
                if (r1 == 0) goto L2d
                android.graphics.PorterDuff$Mode r0 = android.graphics.PorterDuff.Mode.MULTIPLY
                goto L14
            L2d:
                int r1 = defpackage.vz2.abc_list_divider_mtrl_alpha
                if (r8 != r1) goto L3c
                r2 = 16842800(0x1010030, float:2.3693693E-38)
                r8 = 1109603123(0x42233333, float:40.8)
                int r8 = java.lang.Math.round(r8)
                goto L15
            L3c:
                int r1 = defpackage.vz2.abc_dialog_material_background
                if (r8 != r1) goto L41
                goto L14
            L41:
                r8 = r3
                r1 = r4
                r2 = r1
            L44:
                if (r1 == 0) goto L61
                boolean r1 = defpackage.dr0.a(r9)
                if (r1 == 0) goto L50
                android.graphics.drawable.Drawable r9 = r9.mutate()
            L50:
                int r7 = defpackage.b54.c(r7, r2)
                android.graphics.PorterDuffColorFilter r7 = defpackage.gf.e(r7, r0)
                r9.setColorFilter(r7)
                if (r8 == r3) goto L60
                r9.setAlpha(r8)
            L60:
                return r5
            L61:
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.gf.a.b(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
        }

        @Override // defpackage.b83.f
        public PorterDuff.Mode c(int i) {
            if (i == vz2.abc_switch_thumb_material) {
                return PorterDuff.Mode.MULTIPLY;
            }
            return null;
        }

        @Override // defpackage.b83.f
        public ColorStateList d(Context context, int i) {
            if (i == vz2.abc_edit_text_material) {
                return mf.c(context, xy2.abc_tint_edittext);
            }
            if (i == vz2.abc_switch_track_mtrl_alpha) {
                return mf.c(context, xy2.abc_tint_switch_track);
            }
            if (i == vz2.abc_switch_thumb_material) {
                return k(context);
            }
            if (i == vz2.abc_btn_default_mtrl_shape) {
                return j(context);
            }
            if (i == vz2.abc_btn_borderless_material) {
                return g(context);
            }
            if (i == vz2.abc_btn_colored_material) {
                return i(context);
            }
            if (i != vz2.abc_spinner_mtrl_am_alpha && i != vz2.abc_spinner_textfield_background_material) {
                if (f(this.b, i)) {
                    return b54.e(context, jy2.colorControlNormal);
                }
                if (f(this.e, i)) {
                    return mf.c(context, xy2.abc_tint_default);
                }
                if (f(this.f, i)) {
                    return mf.c(context, xy2.abc_tint_btn_checkable);
                }
                if (i == vz2.abc_seekbar_thumb_material) {
                    return mf.c(context, xy2.abc_tint_seek_thumb);
                }
                return null;
            }
            return mf.c(context, xy2.abc_tint_spinner);
        }

        @Override // defpackage.b83.f
        public boolean e(Context context, int i, Drawable drawable) {
            if (i == vz2.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                Drawable findDrawableByLayerId = layerDrawable.findDrawableByLayerId(16908288);
                int i2 = jy2.colorControlNormal;
                m(findDrawableByLayerId, b54.c(context, i2), gf.b);
                m(layerDrawable.findDrawableByLayerId(16908303), b54.c(context, i2), gf.b);
                m(layerDrawable.findDrawableByLayerId(16908301), b54.c(context, jy2.colorControlActivated), gf.b);
                return true;
            } else if (i == vz2.abc_ratingbar_material || i == vz2.abc_ratingbar_indicator_material || i == vz2.abc_ratingbar_small_material) {
                LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                m(layerDrawable2.findDrawableByLayerId(16908288), b54.b(context, jy2.colorControlNormal), gf.b);
                Drawable findDrawableByLayerId2 = layerDrawable2.findDrawableByLayerId(16908303);
                int i3 = jy2.colorControlActivated;
                m(findDrawableByLayerId2, b54.c(context, i3), gf.b);
                m(layerDrawable2.findDrawableByLayerId(16908301), b54.c(context, i3), gf.b);
                return true;
            } else {
                return false;
            }
        }

        public final boolean f(int[] iArr, int i) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }

        public final ColorStateList g(Context context) {
            return h(context, 0);
        }

        public final ColorStateList h(Context context, int i) {
            int c = b54.c(context, jy2.colorControlHighlight);
            return new ColorStateList(new int[][]{b54.b, b54.d, b54.c, b54.f}, new int[]{b54.b(context, jy2.colorButtonNormal), z20.f(c, i), z20.f(c, i), i});
        }

        public final ColorStateList i(Context context) {
            return h(context, b54.c(context, jy2.colorAccent));
        }

        public final ColorStateList j(Context context) {
            return h(context, b54.c(context, jy2.colorButtonNormal));
        }

        public final ColorStateList k(Context context) {
            int[][] iArr = new int[3];
            int[] iArr2 = new int[3];
            int i = jy2.colorSwitchThumbNormal;
            ColorStateList e = b54.e(context, i);
            if (e != null && e.isStateful()) {
                iArr[0] = b54.b;
                iArr2[0] = e.getColorForState(iArr[0], 0);
                iArr[1] = b54.e;
                iArr2[1] = b54.c(context, jy2.colorControlActivated);
                iArr[2] = b54.f;
                iArr2[2] = e.getDefaultColor();
            } else {
                iArr[0] = b54.b;
                iArr2[0] = b54.b(context, i);
                iArr[1] = b54.e;
                iArr2[1] = b54.c(context, jy2.colorControlActivated);
                iArr[2] = b54.f;
                iArr2[2] = b54.c(context, i);
            }
            return new ColorStateList(iArr, iArr2);
        }

        public final LayerDrawable l(b83 b83Var, Context context, int i) {
            BitmapDrawable bitmapDrawable;
            BitmapDrawable bitmapDrawable2;
            BitmapDrawable bitmapDrawable3;
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(i);
            Drawable j = b83Var.j(context, vz2.abc_star_black_48dp);
            Drawable j2 = b83Var.j(context, vz2.abc_star_half_black_48dp);
            if ((j instanceof BitmapDrawable) && j.getIntrinsicWidth() == dimensionPixelSize && j.getIntrinsicHeight() == dimensionPixelSize) {
                bitmapDrawable = (BitmapDrawable) j;
                bitmapDrawable2 = new BitmapDrawable(bitmapDrawable.getBitmap());
            } else {
                Bitmap createBitmap = Bitmap.createBitmap(dimensionPixelSize, dimensionPixelSize, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(createBitmap);
                j.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
                j.draw(canvas);
                bitmapDrawable = new BitmapDrawable(createBitmap);
                bitmapDrawable2 = new BitmapDrawable(createBitmap);
            }
            bitmapDrawable2.setTileModeX(Shader.TileMode.REPEAT);
            if ((j2 instanceof BitmapDrawable) && j2.getIntrinsicWidth() == dimensionPixelSize && j2.getIntrinsicHeight() == dimensionPixelSize) {
                bitmapDrawable3 = (BitmapDrawable) j2;
            } else {
                Bitmap createBitmap2 = Bitmap.createBitmap(dimensionPixelSize, dimensionPixelSize, Bitmap.Config.ARGB_8888);
                Canvas canvas2 = new Canvas(createBitmap2);
                j2.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
                j2.draw(canvas2);
                bitmapDrawable3 = new BitmapDrawable(createBitmap2);
            }
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{bitmapDrawable, bitmapDrawable3, bitmapDrawable2});
            layerDrawable.setId(0, 16908288);
            layerDrawable.setId(1, 16908303);
            layerDrawable.setId(2, 16908301);
            return layerDrawable;
        }

        public final void m(Drawable drawable, int i, PorterDuff.Mode mode) {
            if (dr0.a(drawable)) {
                drawable = drawable.mutate();
            }
            if (mode == null) {
                mode = gf.b;
            }
            drawable.setColorFilter(gf.e(i, mode));
        }
    }

    public static synchronized gf b() {
        gf gfVar;
        synchronized (gf.class) {
            if (c == null) {
                h();
            }
            gfVar = c;
        }
        return gfVar;
    }

    public static synchronized PorterDuffColorFilter e(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter l;
        synchronized (gf.class) {
            l = b83.l(i, mode);
        }
        return l;
    }

    public static synchronized void h() {
        synchronized (gf.class) {
            if (c == null) {
                gf gfVar = new gf();
                c = gfVar;
                gfVar.a = b83.h();
                c.a.u(new a());
            }
        }
    }

    public static void i(Drawable drawable, k64 k64Var, int[] iArr) {
        b83.w(drawable, k64Var, iArr);
    }

    public synchronized Drawable c(Context context, int i) {
        return this.a.j(context, i);
    }

    public synchronized Drawable d(Context context, int i, boolean z) {
        return this.a.k(context, i, z);
    }

    public synchronized ColorStateList f(Context context, int i) {
        return this.a.m(context, i);
    }

    public synchronized void g(Context context) {
        this.a.s(context);
    }
}
