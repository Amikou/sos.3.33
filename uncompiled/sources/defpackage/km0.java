package defpackage;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DependencyDao_Impl.java */
/* renamed from: km0  reason: default package */
/* loaded from: classes.dex */
public final class km0 implements jm0 {
    public final RoomDatabase a;
    public final zv0<gm0> b;

    /* compiled from: DependencyDao_Impl.java */
    /* renamed from: km0$a */
    /* loaded from: classes.dex */
    public class a extends zv0<gm0> {
        public a(km0 km0Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, gm0 gm0Var) {
            String str = gm0Var.a;
            if (str == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, str);
            }
            String str2 = gm0Var.b;
            if (str2 == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, str2);
            }
        }
    }

    public km0(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @Override // defpackage.jm0
    public List<String> a(String str) {
        k93 c = k93.c("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        this.a.d();
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            ArrayList arrayList = new ArrayList(c2.getCount());
            while (c2.moveToNext()) {
                arrayList.add(c2.getString(0));
            }
            return arrayList;
        } finally {
            c2.close();
            c.f();
        }
    }

    @Override // defpackage.jm0
    public boolean b(String str) {
        k93 c = k93.c("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        this.a.d();
        boolean z = false;
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            if (c2.moveToFirst()) {
                z = c2.getInt(0) != 0;
            }
            return z;
        } finally {
            c2.close();
            c.f();
        }
    }

    @Override // defpackage.jm0
    public void c(gm0 gm0Var) {
        this.a.d();
        this.a.e();
        try {
            this.b.h(gm0Var);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.jm0
    public boolean d(String str) {
        k93 c = k93.c("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        this.a.d();
        boolean z = false;
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            if (c2.moveToFirst()) {
                z = c2.getInt(0) != 0;
            }
            return z;
        } finally {
            c2.close();
            c.f();
        }
    }
}
