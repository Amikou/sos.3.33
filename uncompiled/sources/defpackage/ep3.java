package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.o;

/* compiled from: SimpleITokenAdapter.kt */
/* renamed from: ep3  reason: default package */
/* loaded from: classes2.dex */
public final class ep3 extends o<q9, gp3> {
    public final Context a;
    public final String b;
    public final tc1<q9, te4> c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public ep3(Context context, String str, tc1<? super q9, te4> tc1Var) {
        super(fp3.a);
        fs1.f(context, "context");
        fs1.f(tc1Var, "onItemClick");
        this.a = context;
        this.b = str;
        this.c = tc1Var;
    }

    public static final void d(ep3 ep3Var, q9 q9Var, View view) {
        fs1.f(ep3Var, "this$0");
        tc1<q9, te4> tc1Var = ep3Var.c;
        fs1.e(q9Var, "model");
        tc1Var.invoke(q9Var);
    }

    public static final void e(ep3 ep3Var, q9 q9Var, View view) {
        fs1.f(ep3Var, "this$0");
        tc1<q9, te4> tc1Var = ep3Var.c;
        fs1.e(q9Var, "model");
        tc1Var.invoke(q9Var);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public void onBindViewHolder(gp3 gp3Var, int i) {
        fs1.f(gp3Var, "holder");
        final q9 item = getItem(i);
        fs1.e(item, "model");
        gp3Var.a(item);
        gp3Var.d().setOnClickListener(new View.OnClickListener() { // from class: cp3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ep3.d(ep3.this, item, view);
            }
        });
        gp3Var.b().setOnClickListener(new View.OnClickListener() { // from class: dp3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ep3.e(ep3.this, item, view);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public gp3 onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        qs1 c = qs1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        fs1.e(c, "inflate(\n               …      false\n            )");
        return new gp3(c, this.a, this.b);
    }
}
