package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/* compiled from: HelperWidget.java */
/* renamed from: mk1  reason: default package */
/* loaded from: classes.dex */
public class mk1 extends ConstraintWidget implements lk1 {
    public ConstraintWidget[] P0 = new ConstraintWidget[4];
    public int Q0 = 0;

    @Override // defpackage.lk1
    public void a() {
        this.Q0 = 0;
        Arrays.fill(this.P0, (Object) null);
    }

    @Override // defpackage.lk1
    public void b(ConstraintWidget constraintWidget) {
        if (constraintWidget == this || constraintWidget == null) {
            return;
        }
        int i = this.Q0 + 1;
        ConstraintWidget[] constraintWidgetArr = this.P0;
        if (i > constraintWidgetArr.length) {
            this.P0 = (ConstraintWidget[]) Arrays.copyOf(constraintWidgetArr, constraintWidgetArr.length * 2);
        }
        ConstraintWidget[] constraintWidgetArr2 = this.P0;
        int i2 = this.Q0;
        constraintWidgetArr2[i2] = constraintWidget;
        this.Q0 = i2 + 1;
    }

    public void c(d dVar) {
    }

    @Override // androidx.constraintlayout.core.widgets.ConstraintWidget
    public void n(ConstraintWidget constraintWidget, HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.n(constraintWidget, hashMap);
        mk1 mk1Var = (mk1) constraintWidget;
        this.Q0 = 0;
        int i = mk1Var.Q0;
        for (int i2 = 0; i2 < i; i2++) {
            b(hashMap.get(mk1Var.P0[i2]));
        }
    }

    public void o1(ArrayList<dp4> arrayList, int i, dp4 dp4Var) {
        for (int i2 = 0; i2 < this.Q0; i2++) {
            dp4Var.a(this.P0[i2]);
        }
        for (int i3 = 0; i3 < this.Q0; i3++) {
            vi1.a(this.P0[i3], i, arrayList, dp4Var);
        }
    }

    public int p1(int i) {
        int i2;
        int i3;
        for (int i4 = 0; i4 < this.Q0; i4++) {
            ConstraintWidget constraintWidget = this.P0[i4];
            if (i == 0 && (i3 = constraintWidget.M0) != -1) {
                return i3;
            }
            if (i == 1 && (i2 = constraintWidget.N0) != -1) {
                return i2;
            }
        }
        return -1;
    }
}
