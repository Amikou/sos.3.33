package defpackage;

import android.view.View;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAutoSlippageBinding.java */
/* renamed from: kn0  reason: default package */
/* loaded from: classes2.dex */
public final class kn0 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final bi4 d;

    public kn0(MaterialCardView materialCardView, MaterialButton materialButton, MaterialCardView materialCardView2, MaterialButton materialButton2, View view, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5, MaterialTextView materialTextView6, MaterialTextView materialTextView7, MaterialTextView materialTextView8, MaterialButton materialButton3, bi4 bi4Var) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = materialButton3;
        this.d = bi4Var;
    }

    public static kn0 a(View view) {
        int i = R.id.btnDontShowMe;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDontShowMe);
        if (materialButton != null) {
            MaterialCardView materialCardView = (MaterialCardView) view;
            i = R.id.dialogCross;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialogCross);
            if (materialButton2 != null) {
                i = R.id.paddingView;
                View a = ai4.a(view, R.id.paddingView);
                if (a != null) {
                    i = R.id.txtAboutV2Describe;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAboutV2Describe);
                    if (materialTextView != null) {
                        i = R.id.txtAboutV2Note;
                        MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtAboutV2Note);
                        if (materialTextView2 != null) {
                            i = R.id.txtAboutV2Title;
                            MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtAboutV2Title);
                            if (materialTextView3 != null) {
                                i = R.id.txtHeaderDescribe;
                                MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtHeaderDescribe);
                                if (materialTextView4 != null) {
                                    i = R.id.txtHeaderTitle;
                                    MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txtHeaderTitle);
                                    if (materialTextView5 != null) {
                                        i = R.id.txtStep1V2Title;
                                        MaterialTextView materialTextView6 = (MaterialTextView) ai4.a(view, R.id.txtStep1V2Title);
                                        if (materialTextView6 != null) {
                                            i = R.id.txtStepV1Describe;
                                            MaterialTextView materialTextView7 = (MaterialTextView) ai4.a(view, R.id.txtStepV1Describe);
                                            if (materialTextView7 != null) {
                                                i = R.id.txtStepV2Describe;
                                                MaterialTextView materialTextView8 = (MaterialTextView) ai4.a(view, R.id.txtStepV2Describe);
                                                if (materialTextView8 != null) {
                                                    i = R.id.txtTextSlippage;
                                                    MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.txtTextSlippage);
                                                    if (materialButton3 != null) {
                                                        i = R.id.wrapperBtnConsolidateNow;
                                                        View a2 = ai4.a(view, R.id.wrapperBtnConsolidateNow);
                                                        if (a2 != null) {
                                                            return new kn0(materialCardView, materialButton, materialCardView, materialButton2, a, materialTextView, materialTextView2, materialTextView3, materialTextView4, materialTextView5, materialTextView6, materialTextView7, materialTextView8, materialButton3, bi4.a(a2));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
