package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: c46  reason: default package */
/* loaded from: classes.dex */
public final class c46 implements yp5<d46> {
    public static final c46 f0 = new c46();
    public final yp5<d46> a = gq5.a(gq5.b(new e46()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final d46 zza() {
        return this.a.zza();
    }
}
