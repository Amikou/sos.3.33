package defpackage;

import android.net.Uri;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: DataSpec.java */
/* renamed from: je0  reason: default package */
/* loaded from: classes.dex */
public final class je0 {
    public final Uri a;
    public final long b;
    public final int c;
    public final byte[] d;
    public final Map<String, String> e;
    public final long f;
    public final long g;
    public final String h;
    public final int i;
    public final Object j;

    /* compiled from: DataSpec.java */
    /* renamed from: je0$b */
    /* loaded from: classes.dex */
    public static final class b {
        public Uri a;
        public long b;
        public int c;
        public byte[] d;
        public Map<String, String> e;
        public long f;
        public long g;
        public String h;
        public int i;
        public Object j;

        public je0 a() {
            ii.j(this.a, "The uri must be set.");
            return new je0(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j);
        }

        public b b(int i) {
            this.i = i;
            return this;
        }

        public b c(byte[] bArr) {
            this.d = bArr;
            return this;
        }

        public b d(int i) {
            this.c = i;
            return this;
        }

        public b e(Map<String, String> map) {
            this.e = map;
            return this;
        }

        public b f(String str) {
            this.h = str;
            return this;
        }

        public b g(long j) {
            this.g = j;
            return this;
        }

        public b h(long j) {
            this.f = j;
            return this;
        }

        public b i(Uri uri) {
            this.a = uri;
            return this;
        }

        public b j(String str) {
            this.a = Uri.parse(str);
            return this;
        }

        public b() {
            this.c = 1;
            this.e = Collections.emptyMap();
            this.g = -1L;
        }

        public b(je0 je0Var) {
            this.a = je0Var.a;
            this.b = je0Var.b;
            this.c = je0Var.c;
            this.d = je0Var.d;
            this.e = je0Var.e;
            this.f = je0Var.f;
            this.g = je0Var.g;
            this.h = je0Var.h;
            this.i = je0Var.i;
            this.j = je0Var.j;
        }
    }

    static {
        f62.a("media3.datasource");
    }

    public static String c(int i) {
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return "HEAD";
                }
                throw new IllegalStateException();
            }
            return "POST";
        }
        return "GET";
    }

    public b a() {
        return new b();
    }

    public final String b() {
        return c(this.c);
    }

    public boolean d(int i) {
        return (this.i & i) == i;
    }

    public je0 e(long j) {
        long j2 = this.g;
        return f(j, j2 != -1 ? j2 - j : -1L);
    }

    public je0 f(long j, long j2) {
        return (j == 0 && this.g == j2) ? this : new je0(this.a, this.b, this.c, this.d, this.e, this.f + j, j2, this.h, this.i, this.j);
    }

    public String toString() {
        return "DataSpec[" + b() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.a + ", " + this.f + ", " + this.g + ", " + this.h + ", " + this.i + "]";
    }

    public je0(Uri uri, long j, int i, byte[] bArr, Map<String, String> map, long j2, long j3, String str, int i2, Object obj) {
        byte[] bArr2 = bArr;
        boolean z = true;
        ii.a(j + j2 >= 0);
        ii.a(j2 >= 0);
        if (j3 <= 0 && j3 != -1) {
            z = false;
        }
        ii.a(z);
        this.a = uri;
        this.b = j;
        this.c = i;
        this.d = (bArr2 == null || bArr2.length == 0) ? null : bArr2;
        this.e = Collections.unmodifiableMap(new HashMap(map));
        this.f = j2;
        this.g = j3;
        this.h = str;
        this.i = i2;
        this.j = obj;
    }
}
