package defpackage;

import java.math.BigInteger;
import java.security.SecureRandom;

/* renamed from: ij1  reason: default package */
/* loaded from: classes2.dex */
public class ij1 implements od0 {
    public static final BigInteger e = BigInteger.valueOf(0);
    public final hj1 a;
    public final byte[] b;
    public final byte[] c;
    public BigInteger d;

    public ij1(qo0 qo0Var) {
        hj1 hj1Var = new hj1(qo0Var);
        this.a = hj1Var;
        this.c = new byte[hj1Var.c()];
        this.b = new byte[hj1Var.c()];
    }

    @Override // defpackage.od0
    public BigInteger a() {
        int bitLength = (this.d.bitLength() + 7) / 8;
        byte[] bArr = new byte[bitLength];
        while (true) {
            int i = 0;
            while (i < bitLength) {
                hj1 hj1Var = this.a;
                byte[] bArr2 = this.c;
                hj1Var.b(bArr2, 0, bArr2.length);
                this.a.a(this.c, 0);
                int min = Math.min(bitLength - i, this.c.length);
                System.arraycopy(this.c, 0, bArr, i, min);
                i += min;
            }
            BigInteger e2 = e(bArr);
            if (e2.compareTo(e) > 0 && e2.compareTo(this.d) < 0) {
                return e2;
            }
            hj1 hj1Var2 = this.a;
            byte[] bArr3 = this.c;
            hj1Var2.b(bArr3, 0, bArr3.length);
            this.a.f((byte) 0);
            this.a.a(this.b, 0);
            this.a.d(new kx1(this.b));
            hj1 hj1Var3 = this.a;
            byte[] bArr4 = this.c;
            hj1Var3.b(bArr4, 0, bArr4.length);
            this.a.a(this.c, 0);
        }
    }

    @Override // defpackage.od0
    public boolean b() {
        return true;
    }

    @Override // defpackage.od0
    public void c(BigInteger bigInteger, SecureRandom secureRandom) {
        throw new IllegalStateException("Operation not supported");
    }

    @Override // defpackage.od0
    public void d(BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        this.d = bigInteger;
        wh.n(this.c, (byte) 1);
        wh.n(this.b, (byte) 0);
        int bitLength = (bigInteger.bitLength() + 7) / 8;
        byte[] bArr2 = new byte[bitLength];
        byte[] b = ip.b(bigInteger2);
        System.arraycopy(b, 0, bArr2, bitLength - b.length, b.length);
        int bitLength2 = (bigInteger.bitLength() + 7) / 8;
        byte[] bArr3 = new byte[bitLength2];
        BigInteger e2 = e(bArr);
        if (e2.compareTo(bigInteger) >= 0) {
            e2 = e2.subtract(bigInteger);
        }
        byte[] b2 = ip.b(e2);
        System.arraycopy(b2, 0, bArr3, bitLength2 - b2.length, b2.length);
        this.a.d(new kx1(this.b));
        hj1 hj1Var = this.a;
        byte[] bArr4 = this.c;
        hj1Var.b(bArr4, 0, bArr4.length);
        this.a.f((byte) 0);
        this.a.b(bArr2, 0, bitLength);
        this.a.b(bArr3, 0, bitLength2);
        this.a.a(this.b, 0);
        this.a.d(new kx1(this.b));
        hj1 hj1Var2 = this.a;
        byte[] bArr5 = this.c;
        hj1Var2.b(bArr5, 0, bArr5.length);
        this.a.a(this.c, 0);
        hj1 hj1Var3 = this.a;
        byte[] bArr6 = this.c;
        hj1Var3.b(bArr6, 0, bArr6.length);
        this.a.f((byte) 1);
        this.a.b(bArr2, 0, bitLength);
        this.a.b(bArr3, 0, bitLength2);
        this.a.a(this.b, 0);
        this.a.d(new kx1(this.b));
        hj1 hj1Var4 = this.a;
        byte[] bArr7 = this.c;
        hj1Var4.b(bArr7, 0, bArr7.length);
        this.a.a(this.c, 0);
    }

    public final BigInteger e(byte[] bArr) {
        BigInteger bigInteger = new BigInteger(1, bArr);
        return bArr.length * 8 > this.d.bitLength() ? bigInteger.shiftRight((bArr.length * 8) - this.d.bitLength()) : bigInteger;
    }
}
