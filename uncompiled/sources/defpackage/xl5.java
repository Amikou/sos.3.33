package defpackage;

import com.google.android.gms.internal.measurement.zzgs;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: xl5  reason: default package */
/* loaded from: classes.dex */
public final class xl5 implements sv5 {
    public static final sv5 a = new xl5();

    @Override // defpackage.sv5
    public final boolean d(int i) {
        return zzgs.zza(i) != null;
    }
}
