package defpackage;

import com.google.android.gms.internal.clearcut.m;

/* renamed from: bb5  reason: default package */
/* loaded from: classes.dex */
public final class bb5 implements ee5 {
    public static final bb5 a = new bb5();

    public static bb5 c() {
        return a;
    }

    @Override // defpackage.ee5
    public final boolean a(Class<?> cls) {
        return m.class.isAssignableFrom(cls);
    }

    @Override // defpackage.ee5
    public final ce5 b(Class<?> cls) {
        if (!m.class.isAssignableFrom(cls)) {
            String name = cls.getName();
            throw new IllegalArgumentException(name.length() != 0 ? "Unsupported message type: ".concat(name) : new String("Unsupported message type: "));
        }
        try {
            return (ce5) m.q(cls.asSubclass(m.class)).i(m.e.c, null, null);
        } catch (Exception e) {
            String name2 = cls.getName();
            throw new RuntimeException(name2.length() != 0 ? "Unable to get message info for ".concat(name2) : new String("Unable to get message info for "), e);
        }
    }
}
