package defpackage;

import defpackage.wi3;
import java.io.EOFException;
import java.io.IOException;

/* compiled from: DefaultOggSeeker.java */
/* renamed from: gk0  reason: default package */
/* loaded from: classes.dex */
public final class gk0 implements zl2 {
    public final yl2 a;
    public final long b;
    public final long c;
    public final fu3 d;
    public int e;
    public long f;
    public long g;
    public long h;
    public long i;
    public long j;
    public long k;
    public long l;

    /* compiled from: DefaultOggSeeker.java */
    /* renamed from: gk0$b */
    /* loaded from: classes.dex */
    public final class b implements wi3 {
        public b() {
        }

        @Override // defpackage.wi3
        public boolean e() {
            return true;
        }

        @Override // defpackage.wi3
        public wi3.a h(long j) {
            return new wi3.a(new yi3(j, androidx.media3.common.util.b.r((gk0.this.b + ((gk0.this.d.c(j) * (gk0.this.c - gk0.this.b)) / gk0.this.f)) - 30000, gk0.this.b, gk0.this.c - 1)));
        }

        @Override // defpackage.wi3
        public long i() {
            return gk0.this.d.b(gk0.this.f);
        }
    }

    public gk0(fu3 fu3Var, long j, long j2, long j3, long j4, boolean z) {
        ii.a(j >= 0 && j2 > j);
        this.d = fu3Var;
        this.b = j;
        this.c = j2;
        if (j3 != j2 - j && !z) {
            this.e = 0;
        } else {
            this.f = j4;
            this.e = 4;
        }
        this.a = new yl2();
    }

    @Override // defpackage.zl2
    public long b(q11 q11Var) throws IOException {
        int i = this.e;
        if (i == 0) {
            long position = q11Var.getPosition();
            this.g = position;
            this.e = 1;
            long j = this.c - 65307;
            if (j > position) {
                return j;
            }
        } else if (i != 1) {
            if (i == 2) {
                long i2 = i(q11Var);
                if (i2 != -1) {
                    return i2;
                }
                this.e = 3;
            } else if (i != 3) {
                if (i == 4) {
                    return -1L;
                }
                throw new IllegalStateException();
            }
            k(q11Var);
            this.e = 4;
            return -(this.k + 2);
        }
        this.f = j(q11Var);
        this.e = 4;
        return this.g;
    }

    @Override // defpackage.zl2
    public void c(long j) {
        this.h = androidx.media3.common.util.b.r(j, 0L, this.f - 1);
        this.e = 2;
        this.i = this.b;
        this.j = this.c;
        this.k = 0L;
        this.l = this.f;
    }

    @Override // defpackage.zl2
    /* renamed from: h */
    public b a() {
        if (this.f != 0) {
            return new b();
        }
        return null;
    }

    public final long i(q11 q11Var) throws IOException {
        if (this.i == this.j) {
            return -1L;
        }
        long position = q11Var.getPosition();
        if (!this.a.d(q11Var, this.j)) {
            long j = this.i;
            if (j != position) {
                return j;
            }
            throw new IOException("No ogg page can be found.");
        }
        this.a.a(q11Var, false);
        q11Var.j();
        long j2 = this.h;
        yl2 yl2Var = this.a;
        long j3 = yl2Var.c;
        long j4 = j2 - j3;
        int i = yl2Var.e + yl2Var.f;
        if (0 > j4 || j4 >= 72000) {
            int i2 = (j4 > 0L ? 1 : (j4 == 0L ? 0 : -1));
            if (i2 < 0) {
                this.j = position;
                this.l = j3;
            } else {
                this.i = q11Var.getPosition() + i;
                this.k = this.a.c;
            }
            long j5 = this.j;
            long j6 = this.i;
            if (j5 - j6 < 100000) {
                this.j = j6;
                return j6;
            }
            long position2 = q11Var.getPosition() - (i * (i2 <= 0 ? 2L : 1L));
            long j7 = this.j;
            long j8 = this.i;
            return androidx.media3.common.util.b.r(position2 + ((j4 * (j7 - j8)) / (this.l - this.k)), j8, j7 - 1);
        }
        return -1L;
    }

    public long j(q11 q11Var) throws IOException {
        this.a.b();
        if (this.a.c(q11Var)) {
            this.a.a(q11Var, false);
            yl2 yl2Var = this.a;
            q11Var.k(yl2Var.e + yl2Var.f);
            long j = this.a.c;
            while (true) {
                yl2 yl2Var2 = this.a;
                if ((yl2Var2.b & 4) == 4 || !yl2Var2.c(q11Var) || q11Var.getPosition() >= this.c || !this.a.a(q11Var, true)) {
                    break;
                }
                yl2 yl2Var3 = this.a;
                if (!s11.e(q11Var, yl2Var3.e + yl2Var3.f)) {
                    break;
                }
                j = this.a.c;
            }
            return j;
        }
        throw new EOFException();
    }

    public final void k(q11 q11Var) throws IOException {
        while (true) {
            this.a.c(q11Var);
            this.a.a(q11Var, false);
            yl2 yl2Var = this.a;
            if (yl2Var.c > this.h) {
                q11Var.j();
                return;
            }
            q11Var.k(yl2Var.e + yl2Var.f);
            this.i = q11Var.getPosition();
            this.k = this.a.c;
        }
    }
}
