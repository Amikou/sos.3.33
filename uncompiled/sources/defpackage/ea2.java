package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.extractor.metadata.id3.MlltFrame;
import androidx.media3.extractor.metadata.id3.TextInformationFrame;
import androidx.media3.extractor.metadata.id3.a;
import defpackage.la2;
import defpackage.zi3;
import java.io.EOFException;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: Mp3Extractor.java */
/* renamed from: ea2  reason: default package */
/* loaded from: classes.dex */
public final class ea2 implements p11 {
    public static final a.InterfaceC0044a u;
    public final int a;
    public final long b;
    public final op2 c;
    public final la2.a d;
    public final qe1 e;
    public final in1 f;
    public final f84 g;
    public r11 h;
    public f84 i;
    public f84 j;
    public int k;
    public Metadata l;
    public long m;
    public long n;
    public long o;
    public int p;
    public zi3 q;
    public boolean r;
    public boolean s;
    public long t;

    static {
        ca2 ca2Var = ca2.b;
        u = da2.a;
    }

    public ea2() {
        this(0);
    }

    public static long m(Metadata metadata) {
        if (metadata != null) {
            int d = metadata.d();
            for (int i = 0; i < d; i++) {
                Metadata.Entry c = metadata.c(i);
                if (c instanceof TextInformationFrame) {
                    TextInformationFrame textInformationFrame = (TextInformationFrame) c;
                    if (textInformationFrame.a.equals("TLEN")) {
                        return b.y0(Long.parseLong(textInformationFrame.g0));
                    }
                }
            }
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static int n(op2 op2Var, int i) {
        if (op2Var.f() >= i + 4) {
            op2Var.P(i);
            int n = op2Var.n();
            if (n == 1483304551 || n == 1231971951) {
                return n;
            }
        }
        if (op2Var.f() >= 40) {
            op2Var.P(36);
            return op2Var.n() == 1447187017 ? 1447187017 : 0;
        }
        return 0;
    }

    public static boolean o(int i, long j) {
        return ((long) (i & (-128000))) == (j & (-128000));
    }

    public static /* synthetic */ p11[] p() {
        return new p11[]{new ea2()};
    }

    public static /* synthetic */ boolean q(int i, int i2, int i3, int i4, int i5) {
        return (i2 == 67 && i3 == 79 && i4 == 77 && (i5 == 77 || i == 2)) || (i2 == 77 && i3 == 76 && i4 == 76 && (i5 == 84 || i == 2));
    }

    public static d92 r(Metadata metadata, long j) {
        if (metadata != null) {
            int d = metadata.d();
            for (int i = 0; i < d; i++) {
                Metadata.Entry c = metadata.c(i);
                if (c instanceof MlltFrame) {
                    return d92.a(j, (MlltFrame) c, m(metadata));
                }
            }
            return null;
        }
        return null;
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.k = 0;
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.n = 0L;
        this.p = 0;
        this.t = j2;
        zi3 zi3Var = this.q;
        if (!(zi3Var instanceof kq1) || ((kq1) zi3Var).a(j2)) {
            return;
        }
        this.s = true;
        this.j = this.g;
    }

    public final void e() {
        ii.i(this.i);
        b.j(this.h);
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        e();
        int u2 = u(q11Var);
        if (u2 == -1 && (this.q instanceof kq1)) {
            long i = i(this.n);
            if (this.q.i() != i) {
                ((kq1) this.q).f(i);
                this.h.p(this.q);
            }
        }
        return u2;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        return w(q11Var, true);
    }

    public final zi3 h(q11 q11Var) throws IOException {
        long m;
        long j;
        long i;
        long d;
        zi3 s = s(q11Var);
        d92 r = r(this.l, q11Var.getPosition());
        if (this.r) {
            return new zi3.a();
        }
        if ((this.a & 4) != 0) {
            if (r != null) {
                i = r.i();
                d = r.d();
            } else if (s != null) {
                i = s.i();
                d = s.d();
            } else {
                m = m(this.l);
                j = -1;
                s = new kq1(m, q11Var.getPosition(), j);
            }
            j = d;
            m = i;
            s = new kq1(m, q11Var.getPosition(), j);
        } else if (r != null) {
            s = r;
        } else if (s == null) {
            s = null;
        }
        if (s == null || !(s.e() || (this.a & 1) == 0)) {
            return l(q11Var, (this.a & 2) != 0);
        }
        return s;
    }

    public final long i(long j) {
        return this.m + ((j * 1000000) / this.d.d);
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.h = r11Var;
        f84 f = r11Var.f(0, 1);
        this.i = f;
        this.j = f;
        this.h.m();
    }

    public void k() {
        this.r = true;
    }

    public final zi3 l(q11 q11Var, boolean z) throws IOException {
        q11Var.n(this.c.d(), 0, 4);
        this.c.P(0);
        this.d.a(this.c.n());
        return new a60(q11Var.getLength(), q11Var.getPosition(), this.d, z);
    }

    public final zi3 s(q11 q11Var) throws IOException {
        op2 op2Var = new op2(this.d.c);
        q11Var.n(op2Var.d(), 0, this.d.c);
        la2.a aVar = this.d;
        int i = 21;
        if ((aVar.a & 1) != 0) {
            if (aVar.e != 1) {
                i = 36;
            }
        } else if (aVar.e == 1) {
            i = 13;
        }
        int i2 = i;
        int n = n(op2Var, i2);
        if (n != 1483304551 && n != 1231971951) {
            if (n == 1447187017) {
                ch4 a = ch4.a(q11Var.getLength(), q11Var.getPosition(), this.d, op2Var);
                q11Var.k(this.d.c);
                return a;
            }
            q11Var.j();
            return null;
        }
        js4 a2 = js4.a(q11Var.getLength(), q11Var.getPosition(), this.d, op2Var);
        if (a2 != null && !this.e.a()) {
            q11Var.j();
            q11Var.f(i2 + 141);
            q11Var.n(this.c.d(), 0, 3);
            this.c.P(0);
            this.e.d(this.c.G());
        }
        q11Var.k(this.d.c);
        return (a2 == null || a2.e() || n != 1231971951) ? a2 : l(q11Var, false);
    }

    public final boolean t(q11 q11Var) throws IOException {
        zi3 zi3Var = this.q;
        if (zi3Var != null) {
            long d = zi3Var.d();
            if (d != -1 && q11Var.e() > d - 4) {
                return true;
            }
        }
        try {
            return !q11Var.d(this.c.d(), 0, 4, true);
        } catch (EOFException unused) {
            return true;
        }
    }

    public final int u(q11 q11Var) throws IOException {
        if (this.k == 0) {
            try {
                w(q11Var, false);
            } catch (EOFException unused) {
                return -1;
            }
        }
        if (this.q == null) {
            zi3 h = h(q11Var);
            this.q = h;
            this.h.p(h);
            this.j.f(new j.b().e0(this.d.b).W(4096).H(this.d.e).f0(this.d.d).N(this.e.a).O(this.e.b).X((this.a & 8) != 0 ? null : this.l).E());
            this.o = q11Var.getPosition();
        } else if (this.o != 0) {
            long position = q11Var.getPosition();
            long j = this.o;
            if (position < j) {
                q11Var.k((int) (j - position));
            }
        }
        return v(q11Var);
    }

    public final int v(q11 q11Var) throws IOException {
        la2.a aVar;
        if (this.p == 0) {
            q11Var.j();
            if (t(q11Var)) {
                return -1;
            }
            this.c.P(0);
            int n = this.c.n();
            if (o(n, this.k) && la2.j(n) != -1) {
                this.d.a(n);
                if (this.m == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    this.m = this.q.b(q11Var.getPosition());
                    if (this.b != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                        this.m += this.b - this.q.b(0L);
                    }
                }
                this.p = this.d.c;
                zi3 zi3Var = this.q;
                if (zi3Var instanceof kq1) {
                    kq1 kq1Var = (kq1) zi3Var;
                    kq1Var.c(i(this.n + aVar.g), q11Var.getPosition() + this.d.c);
                    if (this.s && kq1Var.a(this.t)) {
                        this.s = false;
                        this.j = this.i;
                    }
                }
            } else {
                q11Var.k(1);
                this.k = 0;
                return 0;
            }
        }
        int d = this.j.d(q11Var, this.p, true);
        if (d == -1) {
            return -1;
        }
        int i = this.p - d;
        this.p = i;
        if (i > 0) {
            return 0;
        }
        this.j.b(i(this.n), 1, this.d.c, 0, null);
        this.n += this.d.g;
        this.p = 0;
        return 0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:50:0x009e, code lost:
        if (r13 == false) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00a0, code lost:
        r12.k(r1 + r7);
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00a5, code lost:
        r12.j();
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00a8, code lost:
        r11.k = r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x00aa, code lost:
        return true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean w(defpackage.q11 r12, boolean r13) throws java.io.IOException {
        /*
            r11 = this;
            if (r13 == 0) goto L6
            r0 = 32768(0x8000, float:4.5918E-41)
            goto L8
        L6:
            r0 = 131072(0x20000, float:1.83671E-40)
        L8:
            r12.j()
            long r1 = r12.getPosition()
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            r2 = 0
            r3 = 1
            r4 = 0
            if (r1 != 0) goto L42
            int r1 = r11.a
            r1 = r1 & 8
            if (r1 != 0) goto L20
            r1 = r3
            goto L21
        L20:
            r1 = r4
        L21:
            if (r1 == 0) goto L25
            r1 = r2
            goto L27
        L25:
            androidx.media3.extractor.metadata.id3.a$a r1 = defpackage.ea2.u
        L27:
            in1 r5 = r11.f
            androidx.media3.common.Metadata r1 = r5.a(r12, r1)
            r11.l = r1
            if (r1 == 0) goto L36
            qe1 r5 = r11.e
            r5.c(r1)
        L36:
            long r5 = r12.e()
            int r1 = (int) r5
            if (r13 != 0) goto L40
            r12.k(r1)
        L40:
            r5 = r4
            goto L44
        L42:
            r1 = r4
            r5 = r1
        L44:
            r6 = r5
            r7 = r6
        L46:
            boolean r8 = r11.t(r12)
            if (r8 == 0) goto L55
            if (r6 <= 0) goto L4f
            goto L9e
        L4f:
            java.io.EOFException r12 = new java.io.EOFException
            r12.<init>()
            throw r12
        L55:
            op2 r8 = r11.c
            r8.P(r4)
            op2 r8 = r11.c
            int r8 = r8.n()
            if (r5 == 0) goto L69
            long r9 = (long) r5
            boolean r9 = o(r8, r9)
            if (r9 == 0) goto L70
        L69:
            int r9 = defpackage.la2.j(r8)
            r10 = -1
            if (r9 != r10) goto L90
        L70:
            int r5 = r7 + 1
            if (r7 != r0) goto L7e
            if (r13 == 0) goto L77
            return r4
        L77:
            java.lang.String r12 = "Searched too many bytes."
            androidx.media3.common.ParserException r12 = androidx.media3.common.ParserException.createForMalformedContainer(r12, r2)
            throw r12
        L7e:
            if (r13 == 0) goto L89
            r12.j()
            int r6 = r1 + r5
            r12.f(r6)
            goto L8c
        L89:
            r12.k(r3)
        L8c:
            r6 = r4
            r7 = r5
            r5 = r6
            goto L46
        L90:
            int r6 = r6 + 1
            if (r6 != r3) goto L9b
            la2$a r5 = r11.d
            r5.a(r8)
            r5 = r8
            goto Lab
        L9b:
            r8 = 4
            if (r6 != r8) goto Lab
        L9e:
            if (r13 == 0) goto La5
            int r1 = r1 + r7
            r12.k(r1)
            goto La8
        La5:
            r12.j()
        La8:
            r11.k = r5
            return r3
        Lab:
            int r9 = r9 + (-4)
            r12.f(r9)
            goto L46
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ea2.w(q11, boolean):boolean");
    }

    public ea2(int i) {
        this(i, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
    }

    public ea2(int i, long j) {
        this.a = (i & 2) != 0 ? i | 1 : i;
        this.b = j;
        this.c = new op2(10);
        this.d = new la2.a();
        this.e = new qe1();
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.f = new in1();
        ks0 ks0Var = new ks0();
        this.g = ks0Var;
        this.j = ks0Var;
    }
}
