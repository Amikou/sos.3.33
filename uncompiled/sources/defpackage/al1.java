package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: HolderWalletBinding.java */
/* renamed from: al1  reason: default package */
/* loaded from: classes2.dex */
public final class al1 {
    public final ConstraintLayout a;
    public final AppCompatImageView b;
    public final ConstraintLayout c;
    public final AppCompatImageView d;
    public final MaterialCardView e;
    public final TextView f;

    public al1(ConstraintLayout constraintLayout, MaterialButton materialButton, AppCompatImageView appCompatImageView, ConstraintLayout constraintLayout2, AppCompatImageView appCompatImageView2, MaterialCardView materialCardView, MaterialCardView materialCardView2, TextView textView) {
        this.a = constraintLayout;
        this.b = appCompatImageView;
        this.c = constraintLayout2;
        this.d = appCompatImageView2;
        this.e = materialCardView2;
        this.f = textView;
    }

    public static al1 a(View view) {
        int i = R.id.btnDelete;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDelete);
        if (materialButton != null) {
            i = R.id.cbAddToken;
            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.cbAddToken);
            if (appCompatImageView != null) {
                i = R.id.fgContentLayout;
                ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.fgContentLayout);
                if (constraintLayout != null) {
                    i = R.id.imWalletLink;
                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imWalletLink);
                    if (appCompatImageView2 != null) {
                        i = R.id.rowBG;
                        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.rowBG);
                        if (materialCardView != null) {
                            i = R.id.rowFG;
                            MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.rowFG);
                            if (materialCardView2 != null) {
                                i = R.id.txtName;
                                TextView textView = (TextView) ai4.a(view, R.id.txtName);
                                if (textView != null) {
                                    return new al1((ConstraintLayout) view, materialButton, appCompatImageView, constraintLayout, appCompatImageView2, materialCardView, materialCardView2, textView);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
