package defpackage;

import android.text.TextUtils;
import androidx.work.b;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import defpackage.kn2;
import java.util.List;

/* compiled from: EnqueueRunnable.java */
/* renamed from: tv0  reason: default package */
/* loaded from: classes.dex */
public class tv0 implements Runnable {
    public static final String g0 = v12.f("EnqueueRunnable");
    public final yp4 a;
    public final ln2 f0 = new ln2();

    public tv0(yp4 yp4Var) {
        this.a = yp4Var;
    }

    public static boolean b(yp4 yp4Var) {
        boolean c = c(yp4Var.g(), yp4Var.f(), (String[]) yp4.l(yp4Var).toArray(new String[0]), yp4Var.d(), yp4Var.b());
        yp4Var.k();
        return c;
    }

    /* JADX WARN: Removed duplicated region for block: B:118:0x01ae  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x01b8  */
    /* JADX WARN: Removed duplicated region for block: B:127:0x01e1 A[LOOP:5: B:125:0x01db->B:127:0x01e1, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:129:0x01fa  */
    /* JADX WARN: Removed duplicated region for block: B:143:0x020a A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0093  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0159  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean c(defpackage.hq4 r19, java.util.List<? extends androidx.work.d> r20, java.lang.String[] r21, java.lang.String r22, androidx.work.ExistingWorkPolicy r23) {
        /*
            Method dump skipped, instructions count: 529
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.tv0.c(hq4, java.util.List, java.lang.String[], java.lang.String, androidx.work.ExistingWorkPolicy):boolean");
    }

    public static boolean e(yp4 yp4Var) {
        List<yp4> e = yp4Var.e();
        boolean z = false;
        if (e != null) {
            boolean z2 = false;
            for (yp4 yp4Var2 : e) {
                if (!yp4Var2.j()) {
                    z2 |= e(yp4Var2);
                } else {
                    v12.c().h(g0, String.format("Already enqueued work ids (%s).", TextUtils.join(", ", yp4Var2.c())), new Throwable[0]);
                }
            }
            z = z2;
        }
        return b(yp4Var) | z;
    }

    public static void g(tq4 tq4Var) {
        h60 h60Var = tq4Var.j;
        String str = tq4Var.c;
        if (str.equals(ConstraintTrackingWorker.class.getName())) {
            return;
        }
        if (h60Var.f() || h60Var.i()) {
            b.a aVar = new b.a();
            aVar.c(tq4Var.e).h("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            tq4Var.c = ConstraintTrackingWorker.class.getName();
            tq4Var.e = aVar.a();
        }
    }

    public static boolean h(hq4 hq4Var, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (cd3 cd3Var : hq4Var.p()) {
                if (cls.isAssignableFrom(cd3Var.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException unused) {
        }
        return false;
    }

    public boolean a() {
        WorkDatabase q = this.a.g().q();
        q.e();
        try {
            boolean e = e(this.a);
            q.E();
            return e;
        } finally {
            q.j();
        }
    }

    public kn2 d() {
        return this.f0;
    }

    public void f() {
        hq4 g = this.a.g();
        gd3.b(g.k(), g.q(), g.p());
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            if (!this.a.h()) {
                if (a()) {
                    so2.a(this.a.g().j(), RescheduleReceiver.class, true);
                    f();
                }
                this.f0.a(kn2.a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", this.a));
        } catch (Throwable th) {
            this.f0.a(new kn2.b.a(th));
        }
    }
}
