package defpackage;

import java.math.BigInteger;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: hs4  reason: default package */
/* loaded from: classes2.dex */
public class hs4 extends h {
    public final byte[] a;
    public final byte[] f0;

    public hs4(h4 h4Var) {
        if (!g.z(h4Var.D(0)).B().equals(BigInteger.valueOf(0L))) {
            throw new IllegalArgumentException("unknown version of sequence");
        }
        this.a = wh.e(f4.B(h4Var.D(1)).D());
        this.f0 = wh.e(f4.B(h4Var.D(2)).D());
    }

    public hs4(byte[] bArr, byte[] bArr2) {
        this.a = wh.e(bArr);
        this.f0 = wh.e(bArr2);
    }

    public static hs4 o(Object obj) {
        if (obj instanceof hs4) {
            return (hs4) obj;
        }
        if (obj != null) {
            return new hs4(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(0L));
        d4Var.a(new j0(this.a));
        d4Var.a(new j0(this.f0));
        return new n0(d4Var);
    }

    public byte[] p() {
        return wh.e(this.a);
    }

    public byte[] q() {
        return wh.e(this.f0);
    }
}
