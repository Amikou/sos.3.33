package defpackage;

import defpackage.pt0;

/* renamed from: de3  reason: default package */
/* loaded from: classes2.dex */
public class de3 extends pt0.c {
    public de3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public de3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public de3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        ce3 ce3Var = (ce3) this.c;
        if (ce3Var.i()) {
            return i.v();
        }
        ce3 ce3Var2 = (ce3) this.b;
        ce3 ce3Var3 = (ce3) this.d[0];
        int[] e = ad2.e();
        int[] e2 = ad2.e();
        int[] e3 = ad2.e();
        be3.j(ce3Var.f, e3);
        int[] e4 = ad2.e();
        be3.j(e3, e4);
        boolean h = ce3Var3.h();
        int[] iArr = ce3Var3.f;
        if (!h) {
            be3.j(iArr, e2);
            iArr = e2;
        }
        be3.m(ce3Var2.f, iArr, e);
        be3.a(ce3Var2.f, iArr, e2);
        be3.e(e2, e, e2);
        be3.i(ad2.b(e2, e2, e2), e2);
        be3.e(e3, ce3Var2.f, e3);
        be3.i(kd2.G(4, e3, 2, 0), e3);
        be3.i(kd2.H(4, e4, 3, 0, e), e);
        ce3 ce3Var4 = new ce3(e4);
        be3.j(e2, ce3Var4.f);
        int[] iArr2 = ce3Var4.f;
        be3.m(iArr2, e3, iArr2);
        int[] iArr3 = ce3Var4.f;
        be3.m(iArr3, e3, iArr3);
        ce3 ce3Var5 = new ce3(e3);
        be3.m(e3, ce3Var4.f, ce3Var5.f);
        int[] iArr4 = ce3Var5.f;
        be3.e(iArr4, e2, iArr4);
        int[] iArr5 = ce3Var5.f;
        be3.m(iArr5, e, iArr5);
        ce3 ce3Var6 = new ce3(e2);
        be3.n(ce3Var.f, ce3Var6.f);
        if (!h) {
            int[] iArr6 = ce3Var6.f;
            be3.e(iArr6, ce3Var3.f, iArr6);
        }
        return new de3(i, ce3Var4, ce3Var5, new ct0[]{ce3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        ce3 ce3Var = (ce3) this.b;
        ce3 ce3Var2 = (ce3) this.c;
        ce3 ce3Var3 = (ce3) pt0Var.q();
        ce3 ce3Var4 = (ce3) pt0Var.r();
        ce3 ce3Var5 = (ce3) this.d[0];
        ce3 ce3Var6 = (ce3) pt0Var.s(0);
        int[] g = ad2.g();
        int[] e = ad2.e();
        int[] e2 = ad2.e();
        int[] e3 = ad2.e();
        boolean h = ce3Var5.h();
        if (h) {
            iArr = ce3Var3.f;
            iArr2 = ce3Var4.f;
        } else {
            be3.j(ce3Var5.f, e2);
            be3.e(e2, ce3Var3.f, e);
            be3.e(e2, ce3Var5.f, e2);
            be3.e(e2, ce3Var4.f, e2);
            iArr = e;
            iArr2 = e2;
        }
        boolean h2 = ce3Var6.h();
        if (h2) {
            iArr3 = ce3Var.f;
            iArr4 = ce3Var2.f;
        } else {
            be3.j(ce3Var6.f, e3);
            be3.e(e3, ce3Var.f, g);
            be3.e(e3, ce3Var6.f, e3);
            be3.e(e3, ce3Var2.f, e3);
            iArr3 = g;
            iArr4 = e3;
        }
        int[] e4 = ad2.e();
        be3.m(iArr3, iArr, e4);
        be3.m(iArr4, iArr2, e);
        if (ad2.q(e4)) {
            return ad2.q(e) ? J() : i.v();
        }
        be3.j(e4, e2);
        int[] e5 = ad2.e();
        be3.e(e2, e4, e5);
        be3.e(e2, iArr3, e2);
        be3.g(e5, e5);
        ad2.s(iArr4, e5, g);
        be3.i(ad2.b(e2, e2, e5), e5);
        ce3 ce3Var7 = new ce3(e3);
        be3.j(e, ce3Var7.f);
        int[] iArr5 = ce3Var7.f;
        be3.m(iArr5, e5, iArr5);
        ce3 ce3Var8 = new ce3(e5);
        be3.m(e2, ce3Var7.f, ce3Var8.f);
        be3.f(ce3Var8.f, e, g);
        be3.h(g, ce3Var8.f);
        ce3 ce3Var9 = new ce3(e4);
        if (!h) {
            int[] iArr6 = ce3Var9.f;
            be3.e(iArr6, ce3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = ce3Var9.f;
            be3.e(iArr7, ce3Var6.f, iArr7);
        }
        return new de3(i, ce3Var7, ce3Var8, new ct0[]{ce3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new de3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new de3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
