package defpackage;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzaj;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: d55  reason: default package */
/* loaded from: classes.dex */
public final class d55 implements Parcelable.Creator<zzaj> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzaj createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        Rect rect = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            if (SafeParcelReader.v(C) != 2) {
                SafeParcelReader.I(parcel, C);
            } else {
                rect = (Rect) SafeParcelReader.o(parcel, C, Rect.CREATOR);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzaj(rect);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzaj[] newArray(int i) {
        return new zzaj[i];
    }
}
