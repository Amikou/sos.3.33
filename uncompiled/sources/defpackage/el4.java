package defpackage;

import android.app.Activity;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumTransaction;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.viewmodels.wc.WalletConnect;

/* compiled from: WC.kt */
/* renamed from: el4  reason: default package */
/* loaded from: classes2.dex */
public class el4 extends WalletConnect {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public el4(Activity activity, Wallet wallet2) {
        super(activity, wallet2);
        fs1.f(activity, "activity");
        fs1.f(wallet2, "wallet");
    }

    @Override // net.safemoon.androidwallet.viewmodels.wc.WalletConnect
    public void q(int i, String str) {
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
    }

    @Override // net.safemoon.androidwallet.viewmodels.wc.WalletConnect
    public void r(long j, WCEthereumSignMessage wCEthereumSignMessage) {
        fs1.f(wCEthereumSignMessage, ThrowableDeserializer.PROP_NAME_MESSAGE);
    }

    @Override // net.safemoon.androidwallet.viewmodels.wc.WalletConnect
    public void s(long j, WCEthereumTransaction wCEthereumTransaction) {
        fs1.f(wCEthereumTransaction, "transaction");
    }

    @Override // net.safemoon.androidwallet.viewmodels.wc.WalletConnect
    public void t(WCPeerMeta wCPeerMeta) {
        fs1.f(wCPeerMeta, "peer");
    }
}
