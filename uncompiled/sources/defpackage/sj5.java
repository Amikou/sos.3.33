package defpackage;

import android.content.Context;
import android.content.res.Resources;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: sj5  reason: default package */
/* loaded from: classes.dex */
public final class sj5 {
    public static String a(Context context) {
        try {
            return context.getResources().getResourcePackageName(q13.common_google_play_services_unknown_issue);
        } catch (Resources.NotFoundException unused) {
            return context.getPackageName();
        }
    }

    public static final String b(String str, Resources resources, String str2) {
        int identifier = resources.getIdentifier(str, Utf8String.TYPE_NAME, str2);
        if (identifier != 0) {
            try {
            } catch (Resources.NotFoundException unused) {
                return null;
            }
        }
        return resources.getString(identifier);
    }
}
