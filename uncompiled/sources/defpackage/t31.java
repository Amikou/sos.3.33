package defpackage;

import android.content.Context;
import java.io.File;

/* compiled from: FileStoreImpl.java */
/* renamed from: t31  reason: default package */
/* loaded from: classes2.dex */
public class t31 implements s31 {
    public final Context a;

    public t31(Context context) {
        this.a = context;
    }

    @Override // defpackage.s31
    public String a() {
        return new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics").getPath();
    }

    @Override // defpackage.s31
    public File b() {
        return c(new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics"));
    }

    public File c(File file) {
        if (file != null) {
            if (file.exists() || file.mkdirs()) {
                return file;
            }
            w12.f().k("Couldn't create file");
            return null;
        }
        w12.f().k("Null File");
        return null;
    }
}
