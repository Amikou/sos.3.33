package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewParent;
import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ViewUtilsBase.java */
/* renamed from: uk4  reason: default package */
/* loaded from: classes.dex */
public class uk4 {
    public static Method b;
    public static boolean c;
    public static Field d;
    public static boolean e;
    public float[] a;

    public void a(View view) {
        if (view.getVisibility() == 0) {
            view.setTag(zz2.save_non_transition_alpha, null);
        }
    }

    @SuppressLint({"PrivateApi"})
    public final void b() {
        if (c) {
            return;
        }
        try {
            Class cls = Integer.TYPE;
            Method declaredMethod = View.class.getDeclaredMethod("setFrame", cls, cls, cls, cls);
            b = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (NoSuchMethodException unused) {
        }
        c = true;
    }

    public float c(View view) {
        Float f = (Float) view.getTag(zz2.save_non_transition_alpha);
        if (f != null) {
            return view.getAlpha() / f.floatValue();
        }
        return view.getAlpha();
    }

    public void d(View view) {
        int i = zz2.save_non_transition_alpha;
        if (view.getTag(i) == null) {
            view.setTag(i, Float.valueOf(view.getAlpha()));
        }
    }

    public void e(View view, Matrix matrix) {
        if (matrix != null && !matrix.isIdentity()) {
            float[] fArr = this.a;
            if (fArr == null) {
                fArr = new float[9];
                this.a = fArr;
            }
            matrix.getValues(fArr);
            float f = fArr[3];
            float sqrt = ((float) Math.sqrt(1.0f - (f * f))) * (fArr[0] < Utils.FLOAT_EPSILON ? -1 : 1);
            float degrees = (float) Math.toDegrees(Math.atan2(f, sqrt));
            float f2 = fArr[0] / sqrt;
            float f3 = fArr[4] / sqrt;
            float f4 = fArr[2];
            float f5 = fArr[5];
            view.setPivotX(Utils.FLOAT_EPSILON);
            view.setPivotY(Utils.FLOAT_EPSILON);
            view.setTranslationX(f4);
            view.setTranslationY(f5);
            view.setRotation(degrees);
            view.setScaleX(f2);
            view.setScaleY(f3);
            return;
        }
        view.setPivotX(view.getWidth() / 2);
        view.setPivotY(view.getHeight() / 2);
        view.setTranslationX(Utils.FLOAT_EPSILON);
        view.setTranslationY(Utils.FLOAT_EPSILON);
        view.setScaleX(1.0f);
        view.setScaleY(1.0f);
        view.setRotation(Utils.FLOAT_EPSILON);
    }

    public void f(View view, int i, int i2, int i3, int i4) {
        b();
        Method method = b;
        if (method != null) {
            try {
                method.invoke(view, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    public void g(View view, float f) {
        Float f2 = (Float) view.getTag(zz2.save_non_transition_alpha);
        if (f2 != null) {
            view.setAlpha(f2.floatValue() * f);
        } else {
            view.setAlpha(f);
        }
    }

    public void h(View view, int i) {
        if (!e) {
            try {
                Field declaredField = View.class.getDeclaredField("mViewFlags");
                d = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            e = true;
        }
        Field field = d;
        if (field != null) {
            try {
                d.setInt(view, i | (field.getInt(view) & (-13)));
            } catch (IllegalAccessException unused2) {
            }
        }
    }

    public void i(View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            i(view2, matrix);
            matrix.preTranslate(-view2.getScrollX(), -view2.getScrollY());
        }
        matrix.preTranslate(view.getLeft(), view.getTop());
        Matrix matrix2 = view.getMatrix();
        if (matrix2.isIdentity()) {
            return;
        }
        matrix.preConcat(matrix2);
    }

    public void j(View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            j(view2, matrix);
            matrix.postTranslate(view2.getScrollX(), view2.getScrollY());
        }
        matrix.postTranslate(-view.getLeft(), -view.getTop());
        Matrix matrix2 = view.getMatrix();
        if (matrix2.isIdentity()) {
            return;
        }
        Matrix matrix3 = new Matrix();
        if (matrix2.invert(matrix3)) {
            matrix.postConcat(matrix3);
        }
    }
}
