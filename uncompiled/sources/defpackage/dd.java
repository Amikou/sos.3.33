package defpackage;

import android.annotation.TargetApi;
import android.app.Activity;
import androidx.core.app.a;

/* compiled from: AndroidSupportV4Compat.java */
@TargetApi(23)
/* renamed from: dd  reason: default package */
/* loaded from: classes2.dex */
public class dd {
    public static void a(Activity activity, String[] strArr, int i) {
        if (activity instanceof fd) {
            ((fd) activity).validateRequestPermissionsRequestCode(i);
        }
        activity.requestPermissions(strArr, i);
    }

    public static boolean b(Activity activity, String str) {
        return a.u(activity, str);
    }
}
