package defpackage;

import java.util.concurrent.CancellationException;

/* compiled from: Channels.common.kt */
/* renamed from: vx  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class vx {
    public static final void a(f43<?> f43Var, Throwable th) {
        if (th != null) {
            r0 = th instanceof CancellationException ? (CancellationException) th : null;
            if (r0 == null) {
                r0 = ny0.a("Channel was consumed, consumer had failed", th);
            }
        }
        f43Var.a(r0);
    }
}
