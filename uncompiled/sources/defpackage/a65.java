package defpackage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: a65  reason: default package */
/* loaded from: classes.dex */
public final class a65 implements z55 {
    public final String a;
    public final ArrayList<z55> f0;

    public a65(String str, List<z55> list) {
        this.a = str;
        ArrayList<z55> arrayList = new ArrayList<>();
        this.f0 = arrayList;
        arrayList.addAll(list);
    }

    public final String a() {
        return this.a;
    }

    @Override // defpackage.z55
    public final Double b() {
        throw new IllegalStateException("Statement cannot be cast as Double");
    }

    @Override // defpackage.z55
    public final Boolean c() {
        throw new IllegalStateException("Statement cannot be cast as Boolean");
    }

    public final ArrayList<z55> d() {
        return this.f0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof a65) {
            a65 a65Var = (a65) obj;
            String str = this.a;
            if (str == null ? a65Var.a == null : str.equals(a65Var.a)) {
                return this.f0.equals(a65Var.f0);
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        String str = this.a;
        return ((str != null ? str.hashCode() : 0) * 31) + this.f0.hashCode();
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return null;
    }

    @Override // defpackage.z55
    public final z55 m() {
        return this;
    }

    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        throw new IllegalStateException("Statement is not an evaluated entity");
    }

    @Override // defpackage.z55
    public final String zzc() {
        throw new IllegalStateException("Statement cannot be cast as String");
    }
}
