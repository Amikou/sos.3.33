package defpackage;

import android.graphics.PointF;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import androidx.media3.common.util.b;
import androidx.media3.extractor.text.a;
import defpackage.es3;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import zendesk.support.request.CellBase;

/* compiled from: SsaDecoder.java */
/* renamed from: cs3  reason: default package */
/* loaded from: classes.dex */
public final class cs3 extends a {
    public static final Pattern s = Pattern.compile("(?:(\\d+):)?(\\d+):(\\d+)[:.](\\d+)");
    public final boolean n;
    public final ds3 o;
    public Map<String, es3> p;
    public float q;
    public float r;

    public cs3(List<byte[]> list) {
        super("SsaDecoder");
        this.q = -3.4028235E38f;
        this.r = -3.4028235E38f;
        if (list != null && !list.isEmpty()) {
            this.n = true;
            String B = b.B(list.get(0));
            ii.a(B.startsWith("Format:"));
            this.o = (ds3) ii.e(ds3.a(B));
            H(new op2(list.get(1)));
            return;
        }
        this.n = false;
        this.o = null;
    }

    public static int C(long j, List<Long> list, List<List<kb0>> list2) {
        int i;
        int size = list.size() - 1;
        while (true) {
            if (size < 0) {
                i = 0;
                break;
            } else if (list.get(size).longValue() == j) {
                return size;
            } else {
                if (list.get(size).longValue() < j) {
                    i = size + 1;
                    break;
                }
                size--;
            }
        }
        list.add(i, Long.valueOf(j));
        list2.add(i, i == 0 ? new ArrayList() : new ArrayList(list2.get(i - 1)));
        return i;
    }

    public static float D(int i) {
        if (i != 0) {
            if (i != 1) {
                return i != 2 ? -3.4028235E38f : 0.95f;
            }
            return 0.5f;
        }
        return 0.05f;
    }

    public static kb0 E(String str, es3 es3Var, es3.b bVar, float f, float f2) {
        SpannableString spannableString = new SpannableString(str);
        kb0.b o = new kb0.b().o(spannableString);
        if (es3Var != null) {
            if (es3Var.c != null) {
                spannableString.setSpan(new ForegroundColorSpan(es3Var.c.intValue()), 0, spannableString.length(), 33);
            }
            if (es3Var.j == 3 && es3Var.d != null) {
                spannableString.setSpan(new BackgroundColorSpan(es3Var.d.intValue()), 0, spannableString.length(), 33);
            }
            float f3 = es3Var.e;
            if (f3 != -3.4028235E38f && f2 != -3.4028235E38f) {
                o.q(f3 / f2, 1);
            }
            boolean z = es3Var.f;
            if (z && es3Var.g) {
                spannableString.setSpan(new StyleSpan(3), 0, spannableString.length(), 33);
            } else if (z) {
                spannableString.setSpan(new StyleSpan(1), 0, spannableString.length(), 33);
            } else if (es3Var.g) {
                spannableString.setSpan(new StyleSpan(2), 0, spannableString.length(), 33);
            }
            if (es3Var.h) {
                spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 33);
            }
            if (es3Var.i) {
                spannableString.setSpan(new StrikethroughSpan(), 0, spannableString.length(), 33);
            }
        }
        int i = bVar.a;
        if (i == -1) {
            i = es3Var != null ? es3Var.b : -1;
        }
        o.p(N(i)).l(M(i)).i(L(i));
        PointF pointF = bVar.b;
        if (pointF != null && f2 != -3.4028235E38f && f != -3.4028235E38f) {
            o.k(pointF.x / f);
            o.h(bVar.b.y / f2, 0);
        } else {
            o.k(D(o.d()));
            o.h(D(o.c()), 0);
        }
        return o.a();
    }

    public static Map<String, es3> J(op2 op2Var) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        es3.a aVar = null;
        while (true) {
            String p = op2Var.p();
            if (p == null || (op2Var.a() != 0 && op2Var.h() == 91)) {
                break;
            } else if (p.startsWith("Format:")) {
                aVar = es3.a.a(p);
            } else if (p.startsWith("Style:")) {
                if (aVar == null) {
                    p12.i("SsaDecoder", "Skipping 'Style:' line before 'Format:' line: " + p);
                } else {
                    es3 b = es3.b(p, aVar);
                    if (b != null) {
                        linkedHashMap.put(b.a, b);
                    }
                }
            }
        }
        return linkedHashMap;
    }

    public static long K(String str) {
        Matcher matcher = s.matcher(str.trim());
        return !matcher.matches() ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : (Long.parseLong((String) b.j(matcher.group(1))) * 60 * 60 * 1000000) + (Long.parseLong((String) b.j(matcher.group(2))) * 60 * 1000000) + (Long.parseLong((String) b.j(matcher.group(3))) * 1000000) + (Long.parseLong((String) b.j(matcher.group(4))) * 10000);
    }

    public static int L(int i) {
        switch (i) {
            case -1:
                return Integer.MIN_VALUE;
            case 0:
            default:
                p12.i("SsaDecoder", "Unknown alignment: " + i);
                return Integer.MIN_VALUE;
            case 1:
            case 2:
            case 3:
                return 2;
            case 4:
            case 5:
            case 6:
                return 1;
            case 7:
            case 8:
            case 9:
                return 0;
        }
    }

    public static int M(int i) {
        switch (i) {
            case -1:
                return Integer.MIN_VALUE;
            case 0:
            default:
                p12.i("SsaDecoder", "Unknown alignment: " + i);
                return Integer.MIN_VALUE;
            case 1:
            case 4:
            case 7:
                return 0;
            case 2:
            case 5:
            case 8:
                return 1;
            case 3:
            case 6:
            case 9:
                return 2;
        }
    }

    public static Layout.Alignment N(int i) {
        switch (i) {
            case -1:
                return null;
            case 0:
            default:
                p12.i("SsaDecoder", "Unknown alignment: " + i);
                return null;
            case 1:
            case 4:
            case 7:
                return Layout.Alignment.ALIGN_NORMAL;
            case 2:
            case 5:
            case 8:
                return Layout.Alignment.ALIGN_CENTER;
            case 3:
            case 6:
            case 9:
                return Layout.Alignment.ALIGN_OPPOSITE;
        }
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        op2 op2Var = new op2(bArr, i);
        if (!this.n) {
            H(op2Var);
        }
        G(op2Var, arrayList, arrayList2);
        return new fs3(arrayList, arrayList2);
    }

    public final void F(String str, ds3 ds3Var, List<List<kb0>> list, List<Long> list2) {
        int i;
        ii.a(str.startsWith("Dialogue:"));
        String[] split = str.substring(9).split(",", ds3Var.e);
        if (split.length != ds3Var.e) {
            p12.i("SsaDecoder", "Skipping dialogue line with fewer columns than format: " + str);
            return;
        }
        long K = K(split[ds3Var.a]);
        if (K == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            p12.i("SsaDecoder", "Skipping invalid timing: " + str);
            return;
        }
        long K2 = K(split[ds3Var.b]);
        if (K2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            p12.i("SsaDecoder", "Skipping invalid timing: " + str);
            return;
        }
        Map<String, es3> map = this.p;
        es3 es3Var = (map == null || (i = ds3Var.c) == -1) ? null : map.get(split[i].trim());
        String str2 = split[ds3Var.d];
        kb0 E = E(es3.b.d(str2).replace("\\N", "\n").replace("\\n", "\n").replace("\\h", " "), es3Var, es3.b.b(str2), this.q, this.r);
        int C = C(K2, list2, list);
        for (int C2 = C(K, list2, list); C2 < C; C2++) {
            list.get(C2).add(E);
        }
    }

    public final void G(op2 op2Var, List<List<kb0>> list, List<Long> list2) {
        ds3 ds3Var = this.n ? this.o : null;
        while (true) {
            String p = op2Var.p();
            if (p == null) {
                return;
            }
            if (p.startsWith("Format:")) {
                ds3Var = ds3.a(p);
            } else if (p.startsWith("Dialogue:")) {
                if (ds3Var == null) {
                    p12.i("SsaDecoder", "Skipping dialogue line before complete format: " + p);
                } else {
                    F(p, ds3Var, list, list2);
                }
            }
        }
    }

    public final void H(op2 op2Var) {
        while (true) {
            String p = op2Var.p();
            if (p == null) {
                return;
            }
            if ("[Script Info]".equalsIgnoreCase(p)) {
                I(op2Var);
            } else if ("[V4+ Styles]".equalsIgnoreCase(p)) {
                this.p = J(op2Var);
            } else if ("[V4 Styles]".equalsIgnoreCase(p)) {
                p12.f("SsaDecoder", "[V4 Styles] are not supported");
            } else if ("[Events]".equalsIgnoreCase(p)) {
                return;
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0059 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:4:0x0006  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void I(defpackage.op2 r5) {
        /*
            r4 = this;
        L0:
            java.lang.String r0 = r5.p()
            if (r0 == 0) goto L59
            int r1 = r5.a()
            if (r1 == 0) goto L14
            int r1 = r5.h()
            r2 = 91
            if (r1 == r2) goto L59
        L14:
            java.lang.String r1 = ":"
            java.lang.String[] r0 = r0.split(r1)
            int r1 = r0.length
            r2 = 2
            if (r1 == r2) goto L1f
            goto L0
        L1f:
            r1 = 0
            r1 = r0[r1]
            java.lang.String r1 = r1.trim()
            java.lang.String r1 = defpackage.ei.e(r1)
            r1.hashCode()
            java.lang.String r2 = "playresx"
            boolean r2 = r1.equals(r2)
            r3 = 1
            if (r2 != 0) goto L4c
            java.lang.String r2 = "playresy"
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L3f
            goto L0
        L3f:
            r0 = r0[r3]     // Catch: java.lang.NumberFormatException -> L0
            java.lang.String r0 = r0.trim()     // Catch: java.lang.NumberFormatException -> L0
            float r0 = java.lang.Float.parseFloat(r0)     // Catch: java.lang.NumberFormatException -> L0
            r4.r = r0     // Catch: java.lang.NumberFormatException -> L0
            goto L0
        L4c:
            r0 = r0[r3]     // Catch: java.lang.NumberFormatException -> L0
            java.lang.String r0 = r0.trim()     // Catch: java.lang.NumberFormatException -> L0
            float r0 = java.lang.Float.parseFloat(r0)     // Catch: java.lang.NumberFormatException -> L0
            r4.q = r0     // Catch: java.lang.NumberFormatException -> L0
            goto L0
        L59:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cs3.I(op2):void");
    }
}
