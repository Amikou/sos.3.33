package defpackage;

import com.google.android.gms.internal.firebase_messaging.a;
import com.google.android.gms.internal.firebase_messaging.e;
import com.google.android.gms.internal.firebase_messaging.f;
import com.google.firebase.messaging.reporting.MessagingClientEvent;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: bd5  reason: default package */
/* loaded from: classes.dex */
public final class bd5 implements b50 {
    public static final b50 a = new bd5();

    @Override // defpackage.b50
    public final void a(fv0<?> fv0Var) {
        fv0Var.a(nf5.class, f.a);
        fv0Var.a(g82.class, e.a);
        fv0Var.a(MessagingClientEvent.class, a.a);
    }
}
