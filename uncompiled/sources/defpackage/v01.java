package defpackage;

import androidx.media3.extractor.text.SubtitleDecoderException;
import com.google.common.collect.ImmutableList;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

/* compiled from: ExoplayerCuesDecoder.java */
/* renamed from: v01  reason: default package */
/* loaded from: classes.dex */
public final class v01 implements rv3 {
    public final lb0 a = new lb0();
    public final tv3 b = new tv3();
    public final Deque<uv3> c = new ArrayDeque();
    public int d;
    public boolean e;

    /* compiled from: ExoplayerCuesDecoder.java */
    /* renamed from: v01$a */
    /* loaded from: classes.dex */
    public class a extends uv3 {
        public a() {
        }

        @Override // defpackage.if0
        public void u() {
            v01.this.j(this);
        }
    }

    /* compiled from: ExoplayerCuesDecoder.java */
    /* renamed from: v01$b */
    /* loaded from: classes.dex */
    public static final class b implements qv3 {
        public final long a;
        public final ImmutableList<kb0> f0;

        public b(long j, ImmutableList<kb0> immutableList) {
            this.a = j;
            this.f0 = immutableList;
        }

        @Override // defpackage.qv3
        public int a(long j) {
            return this.a > j ? 0 : -1;
        }

        @Override // defpackage.qv3
        public long d(int i) {
            ii.a(i == 0);
            return this.a;
        }

        @Override // defpackage.qv3
        public List<kb0> e(long j) {
            return j >= this.a ? this.f0 : ImmutableList.of();
        }

        @Override // defpackage.qv3
        public int f() {
            return 1;
        }
    }

    public v01() {
        for (int i = 0; i < 2; i++) {
            this.c.addFirst(new a());
        }
        this.d = 0;
    }

    @Override // androidx.media3.decoder.a
    public void a() {
        this.e = true;
    }

    @Override // defpackage.rv3
    public void b(long j) {
    }

    @Override // androidx.media3.decoder.a
    public void flush() {
        ii.g(!this.e);
        this.b.h();
        this.d = 0;
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: g */
    public tv3 d() throws SubtitleDecoderException {
        ii.g(!this.e);
        if (this.d != 0) {
            return null;
        }
        this.d = 1;
        return this.b;
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: h */
    public uv3 c() throws SubtitleDecoderException {
        ii.g(!this.e);
        if (this.d != 2 || this.c.isEmpty()) {
            return null;
        }
        uv3 removeFirst = this.c.removeFirst();
        if (this.b.p()) {
            removeFirst.g(4);
        } else {
            tv3 tv3Var = this.b;
            removeFirst.v(this.b.i0, new b(tv3Var.i0, this.a.a(((ByteBuffer) ii.e(tv3Var.g0)).array())), 0L);
        }
        this.b.h();
        this.d = 0;
        return removeFirst;
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: i */
    public void e(tv3 tv3Var) throws SubtitleDecoderException {
        ii.g(!this.e);
        ii.g(this.d == 1);
        ii.a(this.b == tv3Var);
        this.d = 2;
    }

    public final void j(uv3 uv3Var) {
        ii.g(this.c.size() < 2);
        ii.a(!this.c.contains(uv3Var));
        uv3Var.h();
        this.c.addFirst(uv3Var);
    }
}
