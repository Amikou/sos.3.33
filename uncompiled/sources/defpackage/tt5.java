package defpackage;

import com.google.android.gms.internal.measurement.v1;
import com.google.android.gms.internal.measurement.z1;
import defpackage.cu5;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: tt5  reason: default package */
/* loaded from: classes.dex */
public abstract class tt5<T extends cu5<T>> {
    public abstract boolean a(z1 z1Var);

    public abstract v1<T> b(Object obj);

    public abstract void c(Object obj);
}
