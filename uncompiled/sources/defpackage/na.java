package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.n;
import com.google.crypto.tink.proto.o;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

/* compiled from: AesGcmSivKeyManager.java */
/* renamed from: na  reason: default package */
/* loaded from: classes2.dex */
public final class na extends g<n> {

    /* compiled from: AesGcmSivKeyManager.java */
    /* renamed from: na$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, n> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(n nVar) throws GeneralSecurityException {
            return new ma(nVar.G().toByteArray());
        }
    }

    /* compiled from: AesGcmSivKeyManager.java */
    /* renamed from: na$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<o, n> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public n a(o oVar) {
            return n.I().s(ByteString.copyFrom(p33.c(oVar.D()))).t(na.this.k()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public o c(ByteString byteString) throws InvalidProtocolBufferException {
            return o.E(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(o oVar) throws GeneralSecurityException {
            ug4.a(oVar.D());
        }
    }

    public na() {
        super(n.class, new a(com.google.crypto.tink.a.class));
    }

    public static boolean j() {
        try {
            Cipher.getInstance("AES/GCM-SIV/NoPadding");
            return true;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException unused) {
            return false;
        }
    }

    public static void m(boolean z) throws GeneralSecurityException {
        if (j()) {
            q.q(new na(), z);
        }
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesGcmSivKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, n> e() {
        return new b(o.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int k() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: l */
    public n g(ByteString byteString) throws InvalidProtocolBufferException {
        return n.J(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: n */
    public void i(n nVar) throws GeneralSecurityException {
        ug4.c(nVar.H(), k());
        ug4.a(nVar.G().size());
    }
}
