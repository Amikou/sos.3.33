package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ManageContactsAdapter.kt */
/* renamed from: i32  reason: default package */
/* loaded from: classes2.dex */
public final class i32 extends RecyclerView.Adapter<a> {
    public final sl1 a;
    public final List<IContact> b;
    public String c;
    public final List<IContact> d;
    public ct1 e;

    /* compiled from: ManageContactsAdapter.kt */
    /* renamed from: i32$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final ImageView a;
        public final TextView b;
        public final TextView c;
        public final ConstraintLayout d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(i32 i32Var, ct1 ct1Var) {
            super(ct1Var.b());
            fs1.f(i32Var, "this$0");
            fs1.f(ct1Var, "binding");
            ImageView imageView = ct1Var.c;
            fs1.e(imageView, "binding.ivContactIcon");
            this.a = imageView;
            TextView textView = ct1Var.e;
            fs1.e(textView, "binding.tvContactName");
            this.b = textView;
            TextView textView2 = ct1Var.d;
            fs1.e(textView2, "binding.tvContactAddress");
            this.c = textView2;
            ConstraintLayout constraintLayout = ct1Var.b;
            fs1.e(constraintLayout, "binding.container");
            this.d = constraintLayout;
        }

        public final TextView a() {
            return this.c;
        }

        public final ConstraintLayout b() {
            return this.d;
        }

        public final ImageView c() {
            return this.a;
        }

        public final TextView d() {
            return this.b;
        }
    }

    /* compiled from: Comparisons.kt */
    /* renamed from: i32$b */
    /* loaded from: classes2.dex */
    public static final class b<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            String name = ((IContact) t).getName();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = name.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String name2 = ((IContact) t2).getName();
            Objects.requireNonNull(name2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = name2.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            return l30.a(lowerCase, lowerCase2);
        }
    }

    public i32(sl1 sl1Var) {
        fs1.f(sl1Var, "clickListener");
        this.a = sl1Var;
        this.b = new ArrayList();
        this.c = "";
        this.d = new ArrayList();
    }

    public static final void c(i32 i32Var, IContact iContact, View view) {
        fs1.f(i32Var, "this$0");
        fs1.f(iContact, "$model");
        i32Var.a.a(iContact);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        final IContact iContact = this.d.get(i);
        aVar.d().setText(iContact.getName());
        aVar.a().setText(iContact.getAddress());
        com.bumptech.glide.a.u(aVar.c()).v(new File(iContact.getProfilePath())).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).d0(250, 250).a(n73.v0()).I0(aVar.c());
        aVar.b().setOnClickListener(new View.OnClickListener() { // from class: h32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                i32.c(i32.this, iContact, view);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        ct1 a2 = ct1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_manage_contact, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        this.e = a2;
        ct1 ct1Var = this.e;
        if (ct1Var == null) {
            fs1.r("binding");
            ct1Var = null;
        }
        return new a(this, ct1Var);
    }

    public final void e() {
        this.d.clear();
        List<IContact> list = this.d;
        List<IContact> list2 = this.b;
        ArrayList arrayList = new ArrayList();
        for (Object obj : list2) {
            IContact iContact = (IContact) obj;
            boolean z = true;
            if (!(this.c.length() == 0)) {
                String name = iContact.getName();
                Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
                Locale locale = Locale.ROOT;
                String lowerCase = name.toLowerCase(locale);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                Objects.requireNonNull(lowerCase, "null cannot be cast to non-null type kotlin.CharSequence");
                String obj2 = StringsKt__StringsKt.K0(lowerCase).toString();
                String str = this.c;
                Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
                String lowerCase2 = str.toLowerCase(locale);
                fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                Objects.requireNonNull(lowerCase2, "null cannot be cast to non-null type kotlin.CharSequence");
                if (!StringsKt__StringsKt.M(obj2, StringsKt__StringsKt.K0(lowerCase2).toString(), false, 2, null)) {
                    z = false;
                }
            }
            if (z) {
                arrayList.add(obj);
            }
        }
        list.addAll(arrayList);
        notifyDataSetChanged();
    }

    public final void f(List<? extends IContact> list) {
        fs1.f(list, "items");
        this.b.clear();
        this.b.addAll(j20.e0(list, new b()));
        e();
    }

    public final void g(String str) {
        fs1.f(str, "search");
        this.c = str;
        e();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.d.size();
    }
}
