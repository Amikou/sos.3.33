package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: CoroutineScope.kt */
/* renamed from: d90  reason: default package */
/* loaded from: classes2.dex */
public final class d90 {
    public static final c90 a(CoroutineContext coroutineContext) {
        q30 b;
        if (coroutineContext.get(st1.f) == null) {
            b = zt1.b(null, 1, null);
            coroutineContext = coroutineContext.plus(b);
        }
        return new n70(coroutineContext);
    }

    public static final <R> Object b(hd1<? super c90, ? super q70<? super R>, ? extends Object> hd1Var, q70<? super R> q70Var) {
        vd3 vd3Var = new vd3(q70Var.getContext(), q70Var);
        Object c = re4.c(vd3Var, vd3Var, hd1Var);
        if (c == gs1.d()) {
            ef0.c(q70Var);
        }
        return c;
    }
}
