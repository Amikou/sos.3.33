package defpackage;

import java.io.IOException;
import java.util.Arrays;

/* compiled from: OggPacket.java */
/* renamed from: xl2  reason: default package */
/* loaded from: classes.dex */
public final class xl2 {
    public final yl2 a = new yl2();
    public final op2 b = new op2(new byte[65025], 0);
    public int c = -1;
    public int d;
    public boolean e;

    public final int a(int i) {
        int i2;
        int i3 = 0;
        this.d = 0;
        do {
            int i4 = this.d;
            int i5 = i + i4;
            yl2 yl2Var = this.a;
            if (i5 >= yl2Var.d) {
                break;
            }
            int[] iArr = yl2Var.g;
            this.d = i4 + 1;
            i2 = iArr[i4 + i];
            i3 += i2;
        } while (i2 == 255);
        return i3;
    }

    public yl2 b() {
        return this.a;
    }

    public op2 c() {
        return this.b;
    }

    public boolean d(q11 q11Var) throws IOException {
        int i;
        ii.g(q11Var != null);
        if (this.e) {
            this.e = false;
            this.b.L(0);
        }
        while (!this.e) {
            if (this.c < 0) {
                if (!this.a.c(q11Var) || !this.a.a(q11Var, true)) {
                    return false;
                }
                yl2 yl2Var = this.a;
                int i2 = yl2Var.e;
                if ((yl2Var.b & 1) == 1 && this.b.f() == 0) {
                    i2 += a(0);
                    i = this.d + 0;
                } else {
                    i = 0;
                }
                if (!s11.e(q11Var, i2)) {
                    return false;
                }
                this.c = i;
            }
            int a = a(this.c);
            int i3 = this.c + this.d;
            if (a > 0) {
                op2 op2Var = this.b;
                op2Var.c(op2Var.f() + a);
                if (!s11.d(q11Var, this.b.d(), this.b.f(), a)) {
                    return false;
                }
                op2 op2Var2 = this.b;
                op2Var2.O(op2Var2.f() + a);
                this.e = this.a.g[i3 + (-1)] != 255;
            }
            if (i3 == this.a.d) {
                i3 = -1;
            }
            this.c = i3;
        }
        return true;
    }

    public void e() {
        this.a.b();
        this.b.L(0);
        this.c = -1;
        this.e = false;
    }

    public void f() {
        if (this.b.d().length == 65025) {
            return;
        }
        op2 op2Var = this.b;
        op2Var.N(Arrays.copyOf(op2Var.d(), Math.max(65025, this.b.f())), this.b.f());
    }
}
