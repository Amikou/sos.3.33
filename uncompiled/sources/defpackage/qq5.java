package defpackage;

import android.app.Activity;
import android.os.Bundle;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: qq5  reason: default package */
/* loaded from: classes.dex */
public final class qq5 extends vh5 {
    public volatile bq5 c;
    public bq5 d;
    public bq5 e;
    public final Map<Activity, bq5> f;
    public Activity g;
    public volatile boolean h;
    public volatile bq5 i;
    public bq5 j;
    public boolean k;
    public final Object l;
    public String m;

    public qq5(ck5 ck5Var) {
        super(ck5Var);
        this.l = new Object();
        this.f = new ConcurrentHashMap();
    }

    public static /* synthetic */ void E(qq5 qq5Var, Bundle bundle, bq5 bq5Var, bq5 bq5Var2, long j) {
        bundle.remove("screen_name");
        bundle.remove("screen_class");
        qq5Var.n(bq5Var, bq5Var2, j, true, qq5Var.a.G().s(null, "screen_view", bundle, null, true));
    }

    public static void x(bq5 bq5Var, Bundle bundle, boolean z) {
        if (bq5Var != null) {
            if (!bundle.containsKey("_sc") || z) {
                String str = bq5Var.a;
                if (str != null) {
                    bundle.putString("_sn", str);
                } else {
                    bundle.remove("_sn");
                }
                String str2 = bq5Var.b;
                if (str2 != null) {
                    bundle.putString("_sc", str2);
                } else {
                    bundle.remove("_sc");
                }
                bundle.putLong("_si", bq5Var.c);
                return;
            }
            z = false;
        }
        if (bq5Var == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    public final void A(Activity activity) {
        if (this.a.z().v(null, qf5.s0)) {
            synchronized (this.l) {
                this.k = true;
                if (activity != this.g) {
                    synchronized (this.l) {
                        this.g = activity;
                        this.h = false;
                    }
                    if (this.a.z().v(null, qf5.r0) && this.a.z().C()) {
                        this.i = null;
                        this.a.q().p(new nq5(this));
                    }
                }
            }
        }
        if (this.a.z().v(null, qf5.r0) && !this.a.z().C()) {
            this.c = this.i;
            this.a.q().p(new hq5(this));
            return;
        }
        l(activity, p(activity), false);
        pc5 d = this.a.d();
        d.a.q().p(new pa5(d, d.a.a().b()));
    }

    public final void B(Activity activity) {
        if (this.a.z().v(null, qf5.s0)) {
            synchronized (this.l) {
                this.k = false;
                this.h = true;
            }
        }
        long b = this.a.a().b();
        if (this.a.z().v(null, qf5.r0) && !this.a.z().C()) {
            this.c = null;
            this.a.q().p(new jq5(this, b));
            return;
        }
        bq5 p = p(activity);
        this.d = this.c;
        this.c = null;
        this.a.q().p(new lq5(this, p, b));
    }

    public final void C(Activity activity, Bundle bundle) {
        bq5 bq5Var;
        if (!this.a.z().C() || bundle == null || (bq5Var = this.f.get(activity)) == null) {
            return;
        }
        Bundle bundle2 = new Bundle();
        bundle2.putLong("id", bq5Var.c);
        bundle2.putString(PublicResolver.FUNC_NAME, bq5Var.a);
        bundle2.putString("referrer_name", bq5Var.b);
        bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
    }

    public final void D(Activity activity) {
        synchronized (this.l) {
            if (activity == this.g) {
                this.g = null;
            }
        }
        if (this.a.z().C()) {
            this.f.remove(activity);
        }
    }

    @Override // defpackage.vh5
    public final boolean j() {
        return false;
    }

    public final void l(Activity activity, bq5 bq5Var, boolean z) {
        bq5 bq5Var2;
        bq5 bq5Var3 = this.c == null ? this.d : this.c;
        if (bq5Var.b == null) {
            bq5Var2 = new bq5(bq5Var.a, activity != null ? s(activity.getClass(), "Activity") : null, bq5Var.c, bq5Var.e, bq5Var.f);
        } else {
            bq5Var2 = bq5Var;
        }
        this.d = this.c;
        this.c = bq5Var2;
        this.a.q().p(new fq5(this, bq5Var2, bq5Var3, this.a.a().b(), z));
    }

    public final void n(bq5 bq5Var, bq5 bq5Var2, long j, boolean z, Bundle bundle) {
        long j2;
        e();
        boolean z2 = false;
        if (z && this.e != null) {
            z2 = true;
        }
        if (z2) {
            o(this.e, true, j);
        }
        if (bq5Var2 == null || bq5Var2.c != bq5Var.c || !sw5.G(bq5Var2.b, bq5Var.b) || !sw5.G(bq5Var2.a, bq5Var.a)) {
            Bundle bundle2 = new Bundle();
            q45 z3 = this.a.z();
            we5<Boolean> we5Var = qf5.s0;
            if (z3.v(null, we5Var)) {
                if (bundle != null) {
                    bundle2 = new Bundle(bundle);
                } else {
                    bundle2 = new Bundle();
                }
            }
            x(bq5Var, bundle2, true);
            if (bq5Var2 != null) {
                String str = bq5Var2.a;
                if (str != null) {
                    bundle2.putString("_pn", str);
                }
                String str2 = bq5Var2.b;
                if (str2 != null) {
                    bundle2.putString("_pc", str2);
                }
                bundle2.putLong("_pi", bq5Var2.c);
            }
            if (z2) {
                lu5 lu5Var = this.a.C().e;
                long j3 = j - lu5Var.b;
                lu5Var.b = j;
                if (j3 > 0) {
                    this.a.G().Q(bundle2, j3);
                }
            }
            String str3 = "auto";
            if (this.a.z().v(null, we5Var)) {
                if (!this.a.z().C()) {
                    bundle2.putLong("_mst", 1L);
                }
                if (true == bq5Var.e) {
                    str3 = "app";
                }
            }
            if (this.a.z().v(null, we5Var)) {
                long a = this.a.a().a();
                if (bq5Var.e) {
                    long j4 = bq5Var.f;
                    if (j4 != 0) {
                        j2 = j4;
                        this.a.F().Y(str3, "_vs", j2, bundle2);
                    }
                }
                j2 = a;
                this.a.F().Y(str3, "_vs", j2, bundle2);
            } else {
                dp5 F = this.a.F();
                ck5 ck5Var = F.a;
                F.e();
                F.Y(str3, "_vs", F.a.a().a(), bundle2);
            }
        }
        this.e = bq5Var;
        if (this.a.z().v(null, qf5.s0) && bq5Var.e) {
            this.j = bq5Var;
        }
        this.a.R().W(bq5Var);
    }

    public final void o(bq5 bq5Var, boolean z, long j) {
        this.a.d().h(this.a.a().b());
        if (!this.a.C().e.d(bq5Var != null && bq5Var.d, z, j) || bq5Var == null) {
            return;
        }
        bq5Var.d = false;
    }

    public final bq5 p(Activity activity) {
        zt2.j(activity);
        bq5 bq5Var = this.f.get(activity);
        if (bq5Var == null) {
            bq5 bq5Var2 = new bq5(null, s(activity.getClass(), "Activity"), this.a.G().h0());
            this.f.put(activity, bq5Var2);
            bq5Var = bq5Var2;
        }
        return (this.a.z().v(null, qf5.s0) && this.i != null) ? this.i : bq5Var;
    }

    public final bq5 r(boolean z) {
        g();
        e();
        if (this.a.z().v(null, qf5.s0) && z) {
            bq5 bq5Var = this.e;
            return bq5Var != null ? bq5Var : this.j;
        }
        return this.e;
    }

    public final String s(Class<?> cls, String str) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName == null) {
            return "Activity";
        }
        String[] split = canonicalName.split("\\.");
        int length = split.length;
        String str2 = length > 0 ? split[length - 1] : "";
        int length2 = str2.length();
        this.a.z();
        if (length2 > 100) {
            this.a.z();
            return str2.substring(0, 100);
        }
        return str2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:18:0x0050, code lost:
        if (r2 > 100) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0082, code lost:
        if (r4 > 100) goto L29;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void t(android.os.Bundle r13, long r14) {
        /*
            Method dump skipped, instructions count: 317
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.qq5.t(android.os.Bundle, long):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x0088, code lost:
        if (r1 <= 100) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x00b4, code lost:
        if (r1 <= 100) goto L36;
     */
    @java.lang.Deprecated
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void u(android.app.Activity r4, java.lang.String r5, java.lang.String r6) {
        /*
            r3 = this;
            ck5 r0 = r3.a
            q45 r0 = r0.z()
            boolean r0 = r0.C()
            if (r0 != 0) goto L1c
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.s()
            java.lang.String r5 = "setCurrentScreen cannot be called while screen reporting is disabled."
            r4.a(r5)
            return
        L1c:
            bq5 r0 = r3.c
            if (r0 != 0) goto L30
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.s()
            java.lang.String r5 = "setCurrentScreen cannot be called while no activity active"
            r4.a(r5)
            return
        L30:
            java.util.Map<android.app.Activity, bq5> r1 = r3.f
            java.lang.Object r1 = r1.get(r4)
            if (r1 != 0) goto L48
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.s()
            java.lang.String r5 = "setCurrentScreen must be called with an activity in the activity lifecycle"
            r4.a(r5)
            return
        L48:
            if (r6 != 0) goto L54
            java.lang.Class r6 = r4.getClass()
            java.lang.String r1 = "Activity"
            java.lang.String r6 = r3.s(r6, r1)
        L54:
            java.lang.String r1 = r0.b
            boolean r1 = defpackage.sw5.G(r1, r6)
            java.lang.String r0 = r0.a
            boolean r0 = defpackage.sw5.G(r0, r5)
            if (r1 == 0) goto L75
            if (r0 != 0) goto L65
            goto L75
        L65:
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.s()
            java.lang.String r5 = "setCurrentScreen cannot be called with the same class and name"
            r4.a(r5)
            return
        L75:
            r0 = 100
            if (r5 == 0) goto La3
            int r1 = r5.length()
            if (r1 <= 0) goto L8b
            int r1 = r5.length()
            ck5 r2 = r3.a
            r2.z()
            if (r1 > r0) goto L8b
            goto La3
        L8b:
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.s()
            int r5 = r5.length()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            java.lang.String r6 = "Invalid screen name length in setCurrentScreen. Length"
            r4.b(r6, r5)
            return
        La3:
            if (r6 == 0) goto Lcf
            int r1 = r6.length()
            if (r1 <= 0) goto Lb7
            int r1 = r6.length()
            ck5 r2 = r3.a
            r2.z()
            if (r1 > r0) goto Lb7
            goto Lcf
        Lb7:
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.s()
            int r5 = r6.length()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            java.lang.String r6 = "Invalid class name length in setCurrentScreen. Length"
            r4.b(r6, r5)
            return
        Lcf:
            ck5 r0 = r3.a
            og5 r0 = r0.w()
            jg5 r0 = r0.v()
            if (r5 != 0) goto Lde
            java.lang.String r1 = "null"
            goto Ldf
        Lde:
            r1 = r5
        Ldf:
            java.lang.String r2 = "Setting current screen to name, class"
            r0.c(r2, r1, r6)
            bq5 r0 = new bq5
            ck5 r1 = r3.a
            sw5 r1 = r1.G()
            long r1 = r1.h0()
            r0.<init>(r5, r6, r1)
            java.util.Map<android.app.Activity, bq5> r5 = r3.f
            r5.put(r4, r0)
            r5 = 1
            r3.l(r4, r0, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.qq5.u(android.app.Activity, java.lang.String, java.lang.String):void");
    }

    public final bq5 v() {
        return this.c;
    }

    public final void y(String str, bq5 bq5Var) {
        e();
        synchronized (this) {
            String str2 = this.m;
            if (str2 == null || str2.equals(str) || bq5Var != null) {
                this.m = str;
            }
        }
    }

    public final void z(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (!this.a.z().C() || bundle == null || (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) == null) {
            return;
        }
        this.f.put(activity, new bq5(bundle2.getString(PublicResolver.FUNC_NAME), bundle2.getString("referrer_name"), bundle2.getLong("id")));
    }
}
