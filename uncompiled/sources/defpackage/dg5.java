package defpackage;

import com.google.android.gms.internal.measurement.r0;
import com.google.android.gms.internal.measurement.s0;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: dg5  reason: default package */
/* loaded from: classes.dex */
public final class dg5 extends w1<r0, dg5> implements xx5 {
    public dg5() {
        super(r0.K());
    }

    public final s0 A(int i) {
        return ((r0) this.f0).C(i);
    }

    public final dg5 B(int i, s0 s0Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        r0.M((r0) this.f0, i, s0Var);
        return this;
    }

    public final String v() {
        return ((r0) this.f0).z();
    }

    public final dg5 x(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        r0.L((r0) this.f0, str);
        return this;
    }

    public final int y() {
        return ((r0) this.f0).B();
    }

    public /* synthetic */ dg5(zf5 zf5Var) {
        super(r0.K());
    }
}
