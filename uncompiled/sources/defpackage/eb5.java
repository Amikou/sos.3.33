package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: eb5  reason: default package */
/* loaded from: classes.dex */
public final class eb5 extends n65<Integer> implements rb5<Integer> {
    public int[] f0;
    public int g0;

    static {
        new eb5().v();
    }

    public eb5() {
        this(new int[10], 0);
    }

    public eb5(int[] iArr, int i) {
        this.f0 = iArr;
        this.g0 = i;
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5<Integer> T0(int i) {
        if (i >= this.g0) {
            return new eb5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        n(i, ((Integer) obj).intValue());
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Integer> collection) {
        e();
        gb5.a(collection);
        if (collection instanceof eb5) {
            eb5 eb5Var = (eb5) collection;
            int i = eb5Var.g0;
            if (i == 0) {
                return false;
            }
            int i2 = this.g0;
            if (Integer.MAX_VALUE - i2 >= i) {
                int i3 = i2 + i;
                int[] iArr = this.f0;
                if (i3 > iArr.length) {
                    this.f0 = Arrays.copyOf(iArr, i3);
                }
                System.arraycopy(eb5Var.f0, 0, this.f0, this.g0, eb5Var.g0);
                this.g0 = i3;
                ((AbstractList) this).modCount++;
                return true;
            }
            throw new OutOfMemoryError();
        }
        return super.addAll(collection);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof eb5) {
            eb5 eb5Var = (eb5) obj;
            if (this.g0 != eb5Var.g0) {
                return false;
            }
            int[] iArr = eb5Var.f0;
            for (int i = 0; i < this.g0; i++) {
                if (this.f0[i] != iArr[i]) {
                    return false;
                }
            }
            return true;
        }
        return super.equals(obj);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(getInt(i));
    }

    public final int getInt(int i) {
        k(i);
        return this.f0[i];
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + this.f0[i2];
        }
        return i;
    }

    public final void i(int i) {
        n(this.g0, i);
    }

    public final void k(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(m(i));
        }
    }

    public final String m(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final void n(int i, int i2) {
        int i3;
        e();
        if (i < 0 || i > (i3 = this.g0)) {
            throw new IndexOutOfBoundsException(m(i));
        }
        int[] iArr = this.f0;
        if (i3 < iArr.length) {
            System.arraycopy(iArr, i, iArr, i + 1, i3 - i);
        } else {
            int[] iArr2 = new int[((i3 * 3) / 2) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.f0, i, iArr2, i + 1, this.g0 - i);
            this.f0 = iArr2;
        }
        this.f0[i] = i2;
        this.g0++;
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        k(i);
        int[] iArr = this.f0;
        int i2 = iArr[i];
        int i3 = this.g0;
        if (i < i3 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, i3 - i);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i2);
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Integer.valueOf(this.f0[i]))) {
                int[] iArr = this.f0;
                System.arraycopy(iArr, i + 1, iArr, i, this.g0 - i);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        int[] iArr = this.f0;
        System.arraycopy(iArr, i2, iArr, i, this.g0 - i2);
        this.g0 -= i2 - i;
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        e();
        k(i);
        int[] iArr = this.f0;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }
}
