package defpackage;

import android.opengl.GLES20;
import androidx.media3.common.util.GlUtil;
import com.github.mikephil.charting.utils.Utils;
import defpackage.xv2;
import java.nio.Buffer;
import java.nio.FloatBuffer;

/* compiled from: ProjectionRenderer.java */
/* renamed from: zv2  reason: default package */
/* loaded from: classes.dex */
public final class zv2 {
    public static final float[] j = {1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, -1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 1.0f, 1.0f};
    public static final float[] k = {1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, -0.5f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0.5f, 1.0f};
    public static final float[] l = {1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, -0.5f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 1.0f, 1.0f};
    public static final float[] m = {0.5f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, -1.0f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 1.0f, 1.0f};
    public static final float[] n = {0.5f, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, -1.0f, Utils.FLOAT_EPSILON, 0.5f, 1.0f, 1.0f};
    public int a;
    public a b;
    public a c;
    public hg1 d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;

    /* compiled from: ProjectionRenderer.java */
    /* renamed from: zv2$a */
    /* loaded from: classes.dex */
    public static class a {
        public final int a;
        public final FloatBuffer b;
        public final FloatBuffer c;
        public final int d;

        public a(xv2.b bVar) {
            this.a = bVar.a();
            this.b = GlUtil.e(bVar.c);
            this.c = GlUtil.e(bVar.d);
            int i = bVar.b;
            if (i == 1) {
                this.d = 5;
            } else if (i != 2) {
                this.d = 4;
            } else {
                this.d = 6;
            }
        }
    }

    public static boolean c(xv2 xv2Var) {
        xv2.a aVar = xv2Var.a;
        xv2.a aVar2 = xv2Var.b;
        return aVar.b() == 1 && aVar.a(0).a == 0 && aVar2.b() == 1 && aVar2.a(0).a == 0;
    }

    public void a(int i, float[] fArr, boolean z) {
        float[] fArr2;
        a aVar = z ? this.c : this.b;
        if (aVar == null) {
            return;
        }
        int i2 = this.a;
        if (i2 == 1) {
            fArr2 = z ? l : k;
        } else if (i2 == 2) {
            fArr2 = z ? n : m;
        } else {
            fArr2 = j;
        }
        GLES20.glUniformMatrix3fv(this.f, 1, false, fArr2, 0);
        GLES20.glUniformMatrix4fv(this.e, 1, false, fArr, 0);
        GLES20.glActiveTexture(33984);
        GLES20.glBindTexture(36197, i);
        GLES20.glUniform1i(this.i, 0);
        GlUtil.c();
        GLES20.glVertexAttribPointer(this.g, 3, 5126, false, 12, (Buffer) aVar.b);
        GlUtil.c();
        GLES20.glVertexAttribPointer(this.h, 2, 5126, false, 8, (Buffer) aVar.c);
        GlUtil.c();
        GLES20.glDrawArrays(aVar.d, 0, aVar.a);
        GlUtil.c();
    }

    public void b() {
        hg1 hg1Var = new hg1("uniform mat4 uMvpMatrix;\nuniform mat3 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTexCoords;\nvarying vec2 vTexCoords;\n// Standard transformation.\nvoid main() {\n  gl_Position = uMvpMatrix * aPosition;\n  vTexCoords = (uTexMatrix * vec3(aTexCoords, 1)).xy;\n}\n", "// This is required since the texture data is GL_TEXTURE_EXTERNAL_OES.\n#extension GL_OES_EGL_image_external : require\nprecision mediump float;\n// Standard texture rendering shader.\nuniform samplerExternalOES uTexture;\nvarying vec2 vTexCoords;\nvoid main() {\n  gl_FragColor = texture2D(uTexture, vTexCoords);\n}\n");
        this.d = hg1Var;
        this.e = hg1Var.j("uMvpMatrix");
        this.f = this.d.j("uTexMatrix");
        this.g = this.d.e("aPosition");
        this.h = this.d.e("aTexCoords");
        this.i = this.d.j("uTexture");
    }

    public void d(xv2 xv2Var) {
        if (c(xv2Var)) {
            this.a = xv2Var.c;
            a aVar = new a(xv2Var.a.a(0));
            this.b = aVar;
            if (!xv2Var.d) {
                aVar = new a(xv2Var.b.a(0));
            }
            this.c = aVar;
        }
    }
}
