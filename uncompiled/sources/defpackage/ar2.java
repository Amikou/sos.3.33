package defpackage;

import com.google.common.collect.CompactHashMap;
import com.google.common.collect.CompactHashSet;
import com.google.common.collect.MapMaker;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/* compiled from: Platform.java */
/* renamed from: ar2  reason: default package */
/* loaded from: classes2.dex */
public final class ar2 {
    public static <T> T[] a(Object[] objArr, int i, int i2, T[] tArr) {
        return (T[]) Arrays.copyOfRange(objArr, i, i2, tArr.getClass());
    }

    public static <T> T[] b(T[] tArr, int i) {
        return (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), i));
    }

    public static <K, V> Map<K, V> c(int i) {
        return CompactHashMap.createWithExpectedSize(i);
    }

    public static <E> Set<E> d() {
        return CompactHashSet.create();
    }

    public static <K, V> Map<K, V> e() {
        return CompactHashMap.create();
    }

    public static MapMaker f(MapMaker mapMaker) {
        return mapMaker.l();
    }
}
