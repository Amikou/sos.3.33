package defpackage;

import android.os.Bundle;
import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CueEncoder.java */
/* renamed from: mb0  reason: default package */
/* loaded from: classes.dex */
public final class mb0 {
    public byte[] a(List<kb0> list) {
        ArrayList<Bundle> c = is.c(list);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("c", c);
        Parcel obtain = Parcel.obtain();
        obtain.writeBundle(bundle);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        return marshall;
    }
}
