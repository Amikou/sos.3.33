package defpackage;

import com.google.android.gms.tasks.c;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: xz5  reason: default package */
/* loaded from: classes.dex */
public final class xz5 implements Runnable {
    public final /* synthetic */ c a;
    public final /* synthetic */ az5 f0;

    public xz5(az5 az5Var, c cVar) {
        this.f0 = az5Var;
        this.a = cVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        um2 um2Var;
        um2 um2Var2;
        obj = this.f0.b;
        synchronized (obj) {
            um2Var = this.f0.c;
            if (um2Var != null) {
                um2Var2 = this.f0.c;
                um2Var2.a(this.a.l());
            }
        }
    }
}
