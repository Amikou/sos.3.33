package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityChooseNetworkBinding.java */
/* renamed from: y6  reason: default package */
/* loaded from: classes2.dex */
public final class y6 {
    public final ConstraintLayout a;
    public final RecyclerView b;
    public final zd3 c;
    public final rp3 d;

    public y6(ConstraintLayout constraintLayout, RecyclerView recyclerView, zd3 zd3Var, rp3 rp3Var) {
        this.a = constraintLayout;
        this.b = recyclerView;
        this.c = zd3Var;
        this.d = rp3Var;
    }

    public static y6 a(View view) {
        int i = R.id.rvChainList;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvChainList);
        if (recyclerView != null) {
            i = R.id.searchBar;
            View a = ai4.a(view, R.id.searchBar);
            if (a != null) {
                zd3 a2 = zd3.a(a);
                View a3 = ai4.a(view, R.id.toolbar);
                if (a3 != null) {
                    return new y6((ConstraintLayout) view, recyclerView, a2, rp3.a(a3));
                }
                i = R.id.toolbar;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static y6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static y6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_choose_network, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
