package defpackage;

import java.math.BigInteger;

/* renamed from: f61  reason: default package */
/* loaded from: classes2.dex */
public class f61 extends x4 {
    @Override // defpackage.x4
    public pt0 c(pt0 pt0Var, BigInteger bigInteger) {
        int a;
        xs0 i = pt0Var.i();
        if (bigInteger.bitLength() <= h61.a(i)) {
            g61 b = h61.b(pt0Var);
            ht0 a2 = b.a();
            int c = b.c();
            int i2 = ((a + c) - 1) / c;
            pt0 v = i.v();
            int i3 = c * i2;
            int[] o = kd2.o(i3, bigInteger);
            int i4 = i3 - 1;
            for (int i5 = 0; i5 < i2; i5++) {
                int i6 = 0;
                for (int i7 = i4 - i5; i7 >= 0; i7 -= i2) {
                    i6 = (i6 << 1) | kd2.p(o, i7);
                }
                v = v.K(a2.b(i6));
            }
            return v.a(b.b());
        }
        throw new IllegalStateException("fixed-point comb doesn't support scalars larger than the curve order");
    }
}
