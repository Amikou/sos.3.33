package defpackage;

import android.graphics.drawable.Animatable;

/* compiled from: BaseControllerListener.java */
/* renamed from: en  reason: default package */
/* loaded from: classes.dex */
public class en<INFO> implements m80<INFO> {
    public static final m80<Object> a = new en();

    public static <INFO> m80<INFO> g() {
        return (m80<INFO>) a;
    }

    @Override // defpackage.m80
    public void a(String str, INFO info2) {
    }

    @Override // defpackage.m80
    public void b(String str, INFO info2, Animatable animatable) {
    }

    @Override // defpackage.m80
    public void c(String str, Throwable th) {
    }

    @Override // defpackage.m80
    public void d(String str) {
    }

    @Override // defpackage.m80
    public void e(String str, Object obj) {
    }

    @Override // defpackage.m80
    public void f(String str, Throwable th) {
    }
}
