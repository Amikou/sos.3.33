package defpackage;

import android.content.Context;
import android.os.Build;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;

/* compiled from: SchedulingModule.java */
/* renamed from: jd3  reason: default package */
/* loaded from: classes.dex */
public abstract class jd3 {
    public static rq4 a(Context context, dy0 dy0Var, SchedulerConfig schedulerConfig, qz qzVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new vt1(context, dy0Var, schedulerConfig);
        }
        return new ra(context, dy0Var, qzVar, schedulerConfig);
    }
}
