package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: n35  reason: default package */
/* loaded from: classes.dex */
public final class n35 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ long f0;
    public final /* synthetic */ pc5 g0;

    public n35(pc5 pc5Var, String str, long j) {
        this.g0 = pc5Var;
        this.a = str;
        this.f0 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        pc5.i(this.g0, this.a, this.f0);
    }
}
