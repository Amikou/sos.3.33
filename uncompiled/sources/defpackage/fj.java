package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.provider.Settings;
import android.util.Pair;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Ints;
import java.util.Arrays;

/* compiled from: AudioCapabilities.java */
/* renamed from: fj  reason: default package */
/* loaded from: classes.dex */
public final class fj {
    public static final fj c = new fj(new int[]{2}, 8);
    public static final fj d = new fj(new int[]{2, 5, 6}, 8);
    public static final ImmutableMap<Integer, Integer> e = new ImmutableMap.b().d(5, 6).d(17, 6).d(7, 6).d(18, 6).d(6, 8).d(8, 8).d(14, 8).b();
    public final int[] a;
    public final int b;

    /* compiled from: AudioCapabilities.java */
    /* renamed from: fj$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final AudioAttributes a = new AudioAttributes.Builder().setUsage(1).setContentType(3).setFlags(0).build();

        public static int[] a() {
            ImmutableList.a builder = ImmutableList.builder();
            af4 it = fj.e.keySet().iterator();
            while (it.hasNext()) {
                int intValue = ((Integer) it.next()).intValue();
                if (AudioTrack.isDirectPlaybackSupported(new AudioFormat.Builder().setChannelMask(12).setEncoding(intValue).setSampleRate(48000).build(), a)) {
                    builder.a(Integer.valueOf(intValue));
                }
            }
            builder.a(2);
            return Ints.k(builder.l());
        }

        public static int b(int i, int i2) {
            for (int i3 = 8; i3 > 0; i3--) {
                if (AudioTrack.isDirectPlaybackSupported(new AudioFormat.Builder().setEncoding(i).setSampleRate(i2).setChannelMask(b.E(i3)).build(), a)) {
                    return i3;
                }
            }
            return 0;
        }
    }

    public fj(int[] iArr, int i) {
        if (iArr != null) {
            int[] copyOf = Arrays.copyOf(iArr, iArr.length);
            this.a = copyOf;
            Arrays.sort(copyOf);
        } else {
            this.a = new int[0];
        }
        this.b = i;
    }

    public static boolean b() {
        if (b.a >= 17) {
            String str = b.c;
            if ("Amazon".equals(str) || "Xiaomi".equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static fj c(Context context) {
        return d(context, context.registerReceiver(null, new IntentFilter("android.media.action.HDMI_AUDIO_PLUG")));
    }

    @SuppressLint({"InlinedApi"})
    public static fj d(Context context, Intent intent) {
        if (b() && Settings.Global.getInt(context.getContentResolver(), "external_surround_sound_enabled", 0) == 1) {
            return d;
        }
        if (b.a >= 29 && (b.t0(context) || b.o0(context))) {
            return new fj(a.a(), 8);
        }
        if (intent != null && intent.getIntExtra("android.media.extra.AUDIO_PLUG_STATE", 0) != 0) {
            return new fj(intent.getIntArrayExtra("android.media.extra.ENCODINGS"), intent.getIntExtra("android.media.extra.MAX_CHANNEL_COUNT", 8));
        }
        return c;
    }

    public static int e(int i) {
        int i2 = b.a;
        if (i2 <= 28) {
            if (i == 7) {
                i = 8;
            } else if (i == 3 || i == 4 || i == 5) {
                i = 6;
            }
        }
        if (i2 <= 26 && "fugu".equals(b.b) && i == 1) {
            i = 2;
        }
        return b.E(i);
    }

    public static int g(int i, int i2) {
        if (b.a >= 29) {
            return a.b(i, i2);
        }
        return ((Integer) ii.e(e.getOrDefault(Integer.valueOf(i), 0))).intValue();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof fj) {
            fj fjVar = (fj) obj;
            return Arrays.equals(this.a, fjVar.a) && this.b == fjVar.b;
        }
        return false;
    }

    public Pair<Integer, Integer> f(j jVar) {
        int d2 = y82.d((String) ii.e(jVar.p0), jVar.m0);
        if (e.containsKey(Integer.valueOf(d2))) {
            if (d2 == 18 && !i(18)) {
                d2 = 6;
            } else if (d2 == 8 && !i(8)) {
                d2 = 7;
            }
            if (i(d2)) {
                int i = jVar.C0;
                if (i != -1 && d2 != 18) {
                    if (i > this.b) {
                        return null;
                    }
                } else {
                    int i2 = jVar.D0;
                    if (i2 == -1) {
                        i2 = 48000;
                    }
                    i = g(d2, i2);
                }
                int e2 = e(i);
                if (e2 == 0) {
                    return null;
                }
                return Pair.create(Integer.valueOf(d2), Integer.valueOf(e2));
            }
            return null;
        }
        return null;
    }

    public boolean h(j jVar) {
        return f(jVar) != null;
    }

    public int hashCode() {
        return this.b + (Arrays.hashCode(this.a) * 31);
    }

    public boolean i(int i) {
        return Arrays.binarySearch(this.a, i) >= 0;
    }

    public String toString() {
        return "AudioCapabilities[maxChannelCount=" + this.b + ", supportedEncodings=" + Arrays.toString(this.a) + "]";
    }
}
