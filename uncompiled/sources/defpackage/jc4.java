package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.SpannableStringBuilder;
import android.util.Base64;
import android.util.Pair;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import zendesk.support.request.CellBase;

/* compiled from: TtmlNode.java */
/* renamed from: jc4  reason: default package */
/* loaded from: classes.dex */
public final class jc4 {
    public final String a;
    public final String b;
    public final boolean c;
    public final long d;
    public final long e;
    public final mc4 f;
    public final String[] g;
    public final String h;
    public final String i;
    public final jc4 j;
    public final HashMap<String, Integer> k;
    public final HashMap<String, Integer> l;
    public List<jc4> m;

    public jc4(String str, String str2, long j, long j2, mc4 mc4Var, String[] strArr, String str3, String str4, jc4 jc4Var) {
        this.a = str;
        this.b = str2;
        this.i = str4;
        this.f = mc4Var;
        this.g = strArr;
        this.c = str2 != null;
        this.d = j;
        this.e = j2;
        this.h = (String) ii.e(str3);
        this.j = jc4Var;
        this.k = new HashMap<>();
        this.l = new HashMap<>();
    }

    public static jc4 c(String str, long j, long j2, mc4 mc4Var, String[] strArr, String str2, String str3, jc4 jc4Var) {
        return new jc4(str, null, j, j2, mc4Var, strArr, str2, str3, jc4Var);
    }

    public static jc4 d(String str) {
        return new jc4(null, lc4.b(str), CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, null, null, "", null, null);
    }

    public static void e(SpannableStringBuilder spannableStringBuilder) {
        dm0[] dm0VarArr;
        for (dm0 dm0Var : (dm0[]) spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), dm0.class)) {
            spannableStringBuilder.replace(spannableStringBuilder.getSpanStart(dm0Var), spannableStringBuilder.getSpanEnd(dm0Var), "");
        }
        for (int i = 0; i < spannableStringBuilder.length(); i++) {
            if (spannableStringBuilder.charAt(i) == ' ') {
                int i2 = i + 1;
                int i3 = i2;
                while (i3 < spannableStringBuilder.length() && spannableStringBuilder.charAt(i3) == ' ') {
                    i3++;
                }
                int i4 = i3 - i2;
                if (i4 > 0) {
                    spannableStringBuilder.delete(i, i4 + i);
                }
            }
        }
        if (spannableStringBuilder.length() > 0 && spannableStringBuilder.charAt(0) == ' ') {
            spannableStringBuilder.delete(0, 1);
        }
        for (int i5 = 0; i5 < spannableStringBuilder.length() - 1; i5++) {
            if (spannableStringBuilder.charAt(i5) == '\n') {
                int i6 = i5 + 1;
                if (spannableStringBuilder.charAt(i6) == ' ') {
                    spannableStringBuilder.delete(i6, i5 + 2);
                }
            }
        }
        if (spannableStringBuilder.length() > 0 && spannableStringBuilder.charAt(spannableStringBuilder.length() - 1) == ' ') {
            spannableStringBuilder.delete(spannableStringBuilder.length() - 1, spannableStringBuilder.length());
        }
        for (int i7 = 0; i7 < spannableStringBuilder.length() - 1; i7++) {
            if (spannableStringBuilder.charAt(i7) == ' ') {
                int i8 = i7 + 1;
                if (spannableStringBuilder.charAt(i8) == '\n') {
                    spannableStringBuilder.delete(i7, i8);
                }
            }
        }
        if (spannableStringBuilder.length() <= 0 || spannableStringBuilder.charAt(spannableStringBuilder.length() - 1) != '\n') {
            return;
        }
        spannableStringBuilder.delete(spannableStringBuilder.length() - 1, spannableStringBuilder.length());
    }

    public static SpannableStringBuilder k(String str, Map<String, kb0.b> map) {
        if (!map.containsKey(str)) {
            kb0.b bVar = new kb0.b();
            bVar.o(new SpannableStringBuilder());
            map.put(str, bVar);
        }
        return (SpannableStringBuilder) ii.e(map.get(str).e());
    }

    public void a(jc4 jc4Var) {
        if (this.m == null) {
            this.m = new ArrayList();
        }
        this.m.add(jc4Var);
    }

    public final void b(Map<String, mc4> map, kb0.b bVar, int i, int i2, int i3) {
        mc4 f = lc4.f(this.f, this.g, map);
        SpannableStringBuilder spannableStringBuilder = (SpannableStringBuilder) bVar.e();
        if (spannableStringBuilder == null) {
            spannableStringBuilder = new SpannableStringBuilder();
            bVar.o(spannableStringBuilder);
        }
        SpannableStringBuilder spannableStringBuilder2 = spannableStringBuilder;
        if (f != null) {
            lc4.a(spannableStringBuilder2, i, i2, f, this.j, map, i3);
            if ("p".equals(this.a)) {
                if (f.k() != Float.MAX_VALUE) {
                    bVar.m((f.k() * (-90.0f)) / 100.0f);
                }
                if (f.m() != null) {
                    bVar.p(f.m());
                }
                if (f.h() != null) {
                    bVar.j(f.h());
                }
            }
        }
    }

    public jc4 f(int i) {
        List<jc4> list = this.m;
        if (list != null) {
            return list.get(i);
        }
        throw new IndexOutOfBoundsException();
    }

    public int g() {
        List<jc4> list = this.m;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public List<kb0> h(long j, Map<String, mc4> map, Map<String, kc4> map2, Map<String, String> map3) {
        List<Pair<String, String>> arrayList = new ArrayList<>();
        n(j, this.h, arrayList);
        TreeMap treeMap = new TreeMap();
        p(j, false, this.h, treeMap);
        o(j, map, map2, this.h, treeMap);
        ArrayList arrayList2 = new ArrayList();
        for (Pair<String, String> pair : arrayList) {
            String str = map3.get(pair.second);
            if (str != null) {
                byte[] decode = Base64.decode(str, 0);
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(decode, 0, decode.length);
                kc4 kc4Var = (kc4) ii.e(map2.get(pair.first));
                arrayList2.add(new kb0.b().f(decodeByteArray).k(kc4Var.b).l(0).h(kc4Var.c, 0).i(kc4Var.e).n(kc4Var.f).g(kc4Var.g).r(kc4Var.j).a());
            }
        }
        for (Map.Entry entry : treeMap.entrySet()) {
            kc4 kc4Var2 = (kc4) ii.e(map2.get(entry.getKey()));
            kb0.b bVar = (kb0.b) entry.getValue();
            e((SpannableStringBuilder) ii.e(bVar.e()));
            bVar.h(kc4Var2.c, kc4Var2.d);
            bVar.i(kc4Var2.e);
            bVar.k(kc4Var2.b);
            bVar.n(kc4Var2.f);
            bVar.q(kc4Var2.i, kc4Var2.h);
            bVar.r(kc4Var2.j);
            arrayList2.add(bVar.a());
        }
        return arrayList2;
    }

    public final void i(TreeSet<Long> treeSet, boolean z) {
        boolean equals = "p".equals(this.a);
        boolean equals2 = "div".equals(this.a);
        if (z || equals || (equals2 && this.i != null)) {
            long j = this.d;
            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                treeSet.add(Long.valueOf(j));
            }
            long j2 = this.e;
            if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                treeSet.add(Long.valueOf(j2));
            }
        }
        if (this.m == null) {
            return;
        }
        for (int i = 0; i < this.m.size(); i++) {
            this.m.get(i).i(treeSet, z || equals);
        }
    }

    public long[] j() {
        TreeSet<Long> treeSet = new TreeSet<>();
        int i = 0;
        i(treeSet, false);
        long[] jArr = new long[treeSet.size()];
        Iterator<Long> it = treeSet.iterator();
        while (it.hasNext()) {
            jArr[i] = it.next().longValue();
            i++;
        }
        return jArr;
    }

    public String[] l() {
        return this.g;
    }

    public boolean m(long j) {
        long j2 = this.d;
        return (j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && this.e == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) || (j2 <= j && this.e == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) || ((j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j < this.e) || (j2 <= j && j < this.e));
    }

    public final void n(long j, String str, List<Pair<String, String>> list) {
        if (!"".equals(this.h)) {
            str = this.h;
        }
        if (m(j) && "div".equals(this.a) && this.i != null) {
            list.add(new Pair<>(str, this.i));
            return;
        }
        for (int i = 0; i < g(); i++) {
            f(i).n(j, str, list);
        }
    }

    public final void o(long j, Map<String, mc4> map, Map<String, kc4> map2, String str, Map<String, kb0.b> map3) {
        int i;
        if (m(j)) {
            String str2 = "".equals(this.h) ? str : this.h;
            Iterator<Map.Entry<String, Integer>> it = this.l.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry<String, Integer> next = it.next();
                String key = next.getKey();
                int intValue = this.k.containsKey(key) ? this.k.get(key).intValue() : 0;
                int intValue2 = next.getValue().intValue();
                if (intValue != intValue2) {
                    b(map, (kb0.b) ii.e(map3.get(key)), intValue, intValue2, ((kc4) ii.e(map2.get(str2))).j);
                }
            }
            while (i < g()) {
                f(i).o(j, map, map2, str2, map3);
                i++;
            }
        }
    }

    public final void p(long j, boolean z, String str, Map<String, kb0.b> map) {
        this.k.clear();
        this.l.clear();
        if ("metadata".equals(this.a)) {
            return;
        }
        if (!"".equals(this.h)) {
            str = this.h;
        }
        if (this.c && z) {
            k(str, map).append((CharSequence) ii.e(this.b));
        } else if ("br".equals(this.a) && z) {
            k(str, map).append('\n');
        } else if (m(j)) {
            for (Map.Entry<String, kb0.b> entry : map.entrySet()) {
                this.k.put(entry.getKey(), Integer.valueOf(((CharSequence) ii.e(entry.getValue().e())).length()));
            }
            boolean equals = "p".equals(this.a);
            for (int i = 0; i < g(); i++) {
                f(i).p(j, z || equals, str, map);
            }
            if (equals) {
                lc4.c(k(str, map));
            }
            for (Map.Entry<String, kb0.b> entry2 : map.entrySet()) {
                this.l.put(entry2.getKey(), Integer.valueOf(((CharSequence) ii.e(entry2.getValue().e())).length()));
            }
        }
    }
}
