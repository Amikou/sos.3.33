package defpackage;

import com.google.android.gms.internal.clearcut.o;
import com.google.android.gms.internal.clearcut.zzfl;
import com.google.android.gms.internal.clearcut.zzfq;
import defpackage.ta5;

/* renamed from: ta5  reason: default package */
/* loaded from: classes.dex */
public interface ta5<T extends ta5<T>> extends Comparable<T> {
    he5 F(he5 he5Var, o oVar);

    qe5 H1(qe5 qe5Var, qe5 qe5Var2);

    boolean J0();

    boolean m();

    zzfq q();

    zzfl w();

    int zzc();
}
