package defpackage;

/* compiled from: AnimatedDrawableBackendAnimationInformation.java */
/* renamed from: ld  reason: default package */
/* loaded from: classes.dex */
public class ld implements ke {
    public final kd a;

    public ld(kd kdVar) {
        this.a = kdVar;
    }

    @Override // defpackage.ke
    public int a() {
        return this.a.a();
    }

    @Override // defpackage.ke
    public int b() {
        return this.a.b();
    }

    @Override // defpackage.ke
    public int h(int i) {
        return this.a.e(i);
    }
}
