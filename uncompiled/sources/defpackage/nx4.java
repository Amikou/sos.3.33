package defpackage;

import com.google.android.play.core.assetpacks.c;
import java.util.List;

/* renamed from: nx4  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class nx4 implements tm2 {
    public final c a;

    public nx4(c cVar) {
        this.a = cVar;
    }

    public static tm2 b(c cVar) {
        return new nx4(cVar);
    }

    @Override // defpackage.tm2
    public final void a(Object obj) {
        this.a.a((List) obj);
    }
}
