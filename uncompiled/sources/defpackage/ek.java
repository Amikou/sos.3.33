package defpackage;

import defpackage.r90;
import java.io.IOException;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;
import zendesk.core.ZendeskIdentityStorage;

/* compiled from: AutoCrashlyticsReportEncoder.java */
/* renamed from: ek  reason: default package */
/* loaded from: classes2.dex */
public final class ek implements b50 {
    public static final b50 a = new ek();

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$a */
    /* loaded from: classes2.dex */
    public static final class a implements hl2<r90.a> {
        public static final a a = new a();
        public static final h31 b = h31.d("pid");
        public static final h31 c = h31.d("processName");
        public static final h31 d = h31.d("reasonCode");
        public static final h31 e = h31.d("importance");
        public static final h31 f = h31.d("pss");
        public static final h31 g = h31.d("rss");
        public static final h31 h = h31.d("timestamp");
        public static final h31 i = h31.d("traceFile");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.a aVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.e(b, aVar.c());
            cVar.a(c, aVar.d());
            cVar.e(d, aVar.f());
            cVar.e(e, aVar.b());
            cVar.f(f, aVar.e());
            cVar.f(g, aVar.g());
            cVar.f(h, aVar.h());
            cVar.a(i, aVar.i());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$b */
    /* loaded from: classes2.dex */
    public static final class b implements hl2<r90.c> {
        public static final b a = new b();
        public static final h31 b = h31.d("key");
        public static final h31 c = h31.d("value");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.c cVar, com.google.firebase.encoders.c cVar2) throws IOException {
            cVar2.a(b, cVar.b());
            cVar2.a(c, cVar.c());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$c */
    /* loaded from: classes2.dex */
    public static final class c implements hl2<r90> {
        public static final c a = new c();
        public static final h31 b = h31.d("sdkVersion");
        public static final h31 c = h31.d("gmpAppId");
        public static final h31 d = h31.d("platform");
        public static final h31 e = h31.d("installationUuid");
        public static final h31 f = h31.d("buildVersion");
        public static final h31 g = h31.d("displayVersion");
        public static final h31 h = h31.d("session");
        public static final h31 i = h31.d("ndkPayload");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90 r90Var, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, r90Var.i());
            cVar.a(c, r90Var.e());
            cVar.e(d, r90Var.h());
            cVar.a(e, r90Var.f());
            cVar.a(f, r90Var.c());
            cVar.a(g, r90Var.d());
            cVar.a(h, r90Var.j());
            cVar.a(i, r90Var.g());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$d */
    /* loaded from: classes2.dex */
    public static final class d implements hl2<r90.d> {
        public static final d a = new d();
        public static final h31 b = h31.d("files");
        public static final h31 c = h31.d("orgId");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.d dVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, dVar.b());
            cVar.a(c, dVar.c());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$e */
    /* loaded from: classes2.dex */
    public static final class e implements hl2<r90.d.b> {
        public static final e a = new e();
        public static final h31 b = h31.d("filename");
        public static final h31 c = h31.d("contents");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.d.b bVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, bVar.c());
            cVar.a(c, bVar.b());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$f */
    /* loaded from: classes2.dex */
    public static final class f implements hl2<r90.e.a> {
        public static final f a = new f();
        public static final h31 b = h31.d("identifier");
        public static final h31 c = h31.d("version");
        public static final h31 d = h31.d("displayVersion");
        public static final h31 e = h31.d("organization");
        public static final h31 f = h31.d("installationUuid");
        public static final h31 g = h31.d("developmentPlatform");
        public static final h31 h = h31.d("developmentPlatformVersion");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.a aVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, aVar.e());
            cVar.a(c, aVar.h());
            cVar.a(d, aVar.d());
            cVar.a(e, aVar.g());
            cVar.a(f, aVar.f());
            cVar.a(g, aVar.b());
            cVar.a(h, aVar.c());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$g */
    /* loaded from: classes2.dex */
    public static final class g implements hl2<r90.e.a.b> {
        public static final g a = new g();
        public static final h31 b = h31.d("clsId");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.a.b bVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, bVar.a());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$h */
    /* loaded from: classes2.dex */
    public static final class h implements hl2<r90.e.c> {
        public static final h a = new h();
        public static final h31 b = h31.d("arch");
        public static final h31 c = h31.d("model");
        public static final h31 d = h31.d("cores");
        public static final h31 e = h31.d("ram");
        public static final h31 f = h31.d("diskSpace");
        public static final h31 g = h31.d("simulator");
        public static final h31 h = h31.d("state");
        public static final h31 i = h31.d("manufacturer");
        public static final h31 j = h31.d("modelClass");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.c cVar, com.google.firebase.encoders.c cVar2) throws IOException {
            cVar2.e(b, cVar.b());
            cVar2.a(c, cVar.f());
            cVar2.e(d, cVar.c());
            cVar2.f(e, cVar.h());
            cVar2.f(f, cVar.d());
            cVar2.d(g, cVar.j());
            cVar2.e(h, cVar.i());
            cVar2.a(i, cVar.e());
            cVar2.a(j, cVar.g());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$i */
    /* loaded from: classes2.dex */
    public static final class i implements hl2<r90.e> {
        public static final i a = new i();
        public static final h31 b = h31.d("generator");
        public static final h31 c = h31.d("identifier");
        public static final h31 d = h31.d("startedAt");
        public static final h31 e = h31.d("endedAt");
        public static final h31 f = h31.d("crashed");
        public static final h31 g = h31.d("app");
        public static final h31 h = h31.d("user");
        public static final h31 i = h31.d("os");
        public static final h31 j = h31.d("device");
        public static final h31 k = h31.d("events");
        public static final h31 l = h31.d("generatorType");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e eVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, eVar.f());
            cVar.a(c, eVar.i());
            cVar.f(d, eVar.k());
            cVar.a(e, eVar.d());
            cVar.d(f, eVar.m());
            cVar.a(g, eVar.b());
            cVar.a(h, eVar.l());
            cVar.a(i, eVar.j());
            cVar.a(j, eVar.c());
            cVar.a(k, eVar.e());
            cVar.e(l, eVar.g());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$j */
    /* loaded from: classes2.dex */
    public static final class j implements hl2<r90.e.d.a> {
        public static final j a = new j();
        public static final h31 b = h31.d("execution");
        public static final h31 c = h31.d("customAttributes");
        public static final h31 d = h31.d("internalKeys");
        public static final h31 e = h31.d("background");
        public static final h31 f = h31.d("uiOrientation");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a aVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, aVar.d());
            cVar.a(c, aVar.c());
            cVar.a(d, aVar.e());
            cVar.a(e, aVar.b());
            cVar.e(f, aVar.f());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$k */
    /* loaded from: classes2.dex */
    public static final class k implements hl2<r90.e.d.a.b.AbstractC0266a> {
        public static final k a = new k();
        public static final h31 b = h31.d("baseAddress");
        public static final h31 c = h31.d("size");
        public static final h31 d = h31.d(PublicResolver.FUNC_NAME);
        public static final h31 e = h31.d(ZendeskIdentityStorage.UUID_KEY);

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a.b.AbstractC0266a abstractC0266a, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.f(b, abstractC0266a.b());
            cVar.f(c, abstractC0266a.d());
            cVar.a(d, abstractC0266a.c());
            cVar.a(e, abstractC0266a.f());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$l */
    /* loaded from: classes2.dex */
    public static final class l implements hl2<r90.e.d.a.b> {
        public static final l a = new l();
        public static final h31 b = h31.d("threads");
        public static final h31 c = h31.d("exception");
        public static final h31 d = h31.d("appExitInfo");
        public static final h31 e = h31.d("signal");
        public static final h31 f = h31.d("binaries");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a.b bVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, bVar.f());
            cVar.a(c, bVar.d());
            cVar.a(d, bVar.b());
            cVar.a(e, bVar.e());
            cVar.a(f, bVar.c());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$m */
    /* loaded from: classes2.dex */
    public static final class m implements hl2<r90.e.d.a.b.c> {
        public static final m a = new m();
        public static final h31 b = h31.d("type");
        public static final h31 c = h31.d("reason");
        public static final h31 d = h31.d("frames");
        public static final h31 e = h31.d("causedBy");
        public static final h31 f = h31.d("overflowCount");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a.b.c cVar, com.google.firebase.encoders.c cVar2) throws IOException {
            cVar2.a(b, cVar.f());
            cVar2.a(c, cVar.e());
            cVar2.a(d, cVar.c());
            cVar2.a(e, cVar.b());
            cVar2.e(f, cVar.d());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$n */
    /* loaded from: classes2.dex */
    public static final class n implements hl2<r90.e.d.a.b.AbstractC0270d> {
        public static final n a = new n();
        public static final h31 b = h31.d(PublicResolver.FUNC_NAME);
        public static final h31 c = h31.d("code");
        public static final h31 d = h31.d(Address.TYPE_NAME);

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a.b.AbstractC0270d abstractC0270d, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, abstractC0270d.d());
            cVar.a(c, abstractC0270d.c());
            cVar.f(d, abstractC0270d.b());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$o */
    /* loaded from: classes2.dex */
    public static final class o implements hl2<r90.e.d.a.b.AbstractC0272e> {
        public static final o a = new o();
        public static final h31 b = h31.d(PublicResolver.FUNC_NAME);
        public static final h31 c = h31.d("importance");
        public static final h31 d = h31.d("frames");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a.b.AbstractC0272e abstractC0272e, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, abstractC0272e.d());
            cVar.e(c, abstractC0272e.c());
            cVar.a(d, abstractC0272e.b());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$p */
    /* loaded from: classes2.dex */
    public static final class p implements hl2<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> {
        public static final p a = new p();
        public static final h31 b = h31.d("pc");
        public static final h31 c = h31.d("symbol");
        public static final h31 d = h31.d("file");
        public static final h31 e = h31.d("offset");
        public static final h31 f = h31.d("importance");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.a.b.AbstractC0272e.AbstractC0274b abstractC0274b, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.f(b, abstractC0274b.e());
            cVar.a(c, abstractC0274b.f());
            cVar.a(d, abstractC0274b.b());
            cVar.f(e, abstractC0274b.d());
            cVar.e(f, abstractC0274b.c());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$q */
    /* loaded from: classes2.dex */
    public static final class q implements hl2<r90.e.d.c> {
        public static final q a = new q();
        public static final h31 b = h31.d("batteryLevel");
        public static final h31 c = h31.d("batteryVelocity");
        public static final h31 d = h31.d("proximityOn");
        public static final h31 e = h31.d("orientation");
        public static final h31 f = h31.d("ramUsed");
        public static final h31 g = h31.d("diskUsed");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.c cVar, com.google.firebase.encoders.c cVar2) throws IOException {
            cVar2.a(b, cVar.b());
            cVar2.e(c, cVar.c());
            cVar2.d(d, cVar.g());
            cVar2.e(e, cVar.e());
            cVar2.f(f, cVar.f());
            cVar2.f(g, cVar.d());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$r */
    /* loaded from: classes2.dex */
    public static final class r implements hl2<r90.e.d> {
        public static final r a = new r();
        public static final h31 b = h31.d("timestamp");
        public static final h31 c = h31.d("type");
        public static final h31 d = h31.d("app");
        public static final h31 e = h31.d("device");
        public static final h31 f = h31.d("log");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d dVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.f(b, dVar.e());
            cVar.a(c, dVar.f());
            cVar.a(d, dVar.b());
            cVar.a(e, dVar.c());
            cVar.a(f, dVar.d());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$s */
    /* loaded from: classes2.dex */
    public static final class s implements hl2<r90.e.d.AbstractC0276d> {
        public static final s a = new s();
        public static final h31 b = h31.d(PublicResolver.FUNC_CONTENT);

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.d.AbstractC0276d abstractC0276d, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, abstractC0276d.b());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$t */
    /* loaded from: classes2.dex */
    public static final class t implements hl2<r90.e.AbstractC0277e> {
        public static final t a = new t();
        public static final h31 b = h31.d("platform");
        public static final h31 c = h31.d("version");
        public static final h31 d = h31.d("buildVersion");
        public static final h31 e = h31.d("jailbroken");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.AbstractC0277e abstractC0277e, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.e(b, abstractC0277e.c());
            cVar.a(c, abstractC0277e.d());
            cVar.a(d, abstractC0277e.b());
            cVar.d(e, abstractC0277e.e());
        }
    }

    /* compiled from: AutoCrashlyticsReportEncoder.java */
    /* renamed from: ek$u */
    /* loaded from: classes2.dex */
    public static final class u implements hl2<r90.e.f> {
        public static final u a = new u();
        public static final h31 b = h31.d("identifier");

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(r90.e.f fVar, com.google.firebase.encoders.c cVar) throws IOException {
            cVar.a(b, fVar.b());
        }
    }

    @Override // defpackage.b50
    public void a(fv0<?> fv0Var) {
        c cVar = c.a;
        fv0Var.a(r90.class, cVar);
        fv0Var.a(lk.class, cVar);
        i iVar = i.a;
        fv0Var.a(r90.e.class, iVar);
        fv0Var.a(rk.class, iVar);
        f fVar = f.a;
        fv0Var.a(r90.e.a.class, fVar);
        fv0Var.a(sk.class, fVar);
        g gVar = g.a;
        fv0Var.a(r90.e.a.b.class, gVar);
        fv0Var.a(tk.class, gVar);
        u uVar = u.a;
        fv0Var.a(r90.e.f.class, uVar);
        fv0Var.a(gl.class, uVar);
        t tVar = t.a;
        fv0Var.a(r90.e.AbstractC0277e.class, tVar);
        fv0Var.a(fl.class, tVar);
        h hVar = h.a;
        fv0Var.a(r90.e.c.class, hVar);
        fv0Var.a(uk.class, hVar);
        r rVar = r.a;
        fv0Var.a(r90.e.d.class, rVar);
        fv0Var.a(vk.class, rVar);
        j jVar = j.a;
        fv0Var.a(r90.e.d.a.class, jVar);
        fv0Var.a(wk.class, jVar);
        l lVar = l.a;
        fv0Var.a(r90.e.d.a.b.class, lVar);
        fv0Var.a(xk.class, lVar);
        o oVar = o.a;
        fv0Var.a(r90.e.d.a.b.AbstractC0272e.class, oVar);
        fv0Var.a(bl.class, oVar);
        p pVar = p.a;
        fv0Var.a(r90.e.d.a.b.AbstractC0272e.AbstractC0274b.class, pVar);
        fv0Var.a(cl.class, pVar);
        m mVar = m.a;
        fv0Var.a(r90.e.d.a.b.c.class, mVar);
        fv0Var.a(zk.class, mVar);
        a aVar = a.a;
        fv0Var.a(r90.a.class, aVar);
        fv0Var.a(nk.class, aVar);
        n nVar = n.a;
        fv0Var.a(r90.e.d.a.b.AbstractC0270d.class, nVar);
        fv0Var.a(al.class, nVar);
        k kVar = k.a;
        fv0Var.a(r90.e.d.a.b.AbstractC0266a.class, kVar);
        fv0Var.a(yk.class, kVar);
        b bVar = b.a;
        fv0Var.a(r90.c.class, bVar);
        fv0Var.a(ok.class, bVar);
        q qVar = q.a;
        fv0Var.a(r90.e.d.c.class, qVar);
        fv0Var.a(dl.class, qVar);
        s sVar = s.a;
        fv0Var.a(r90.e.d.AbstractC0276d.class, sVar);
        fv0Var.a(el.class, sVar);
        d dVar = d.a;
        fv0Var.a(r90.d.class, dVar);
        fv0Var.a(pk.class, dVar);
        e eVar = e.a;
        fv0Var.a(r90.d.b.class, eVar);
        fv0Var.a(qk.class, eVar);
    }
}
