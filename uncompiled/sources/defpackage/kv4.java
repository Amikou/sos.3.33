package defpackage;

/* renamed from: kv4  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class kv4 implements xv4 {
    public final zv4 a;
    public final int b;
    public final /* synthetic */ int c = 0;

    public kv4(zv4 zv4Var, int i) {
        this.a = zv4Var;
        this.b = i;
    }

    public kv4(zv4 zv4Var, int i, byte[] bArr) {
        this.a = zv4Var;
        this.b = i;
    }

    @Override // defpackage.xv4
    public final Object a() {
        if (this.c != 0) {
            this.a.m(this.b);
            return null;
        }
        this.a.n(this.b);
        return null;
    }
}
