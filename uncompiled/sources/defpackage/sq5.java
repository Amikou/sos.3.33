package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzkq;
import com.google.android.gms.measurement.internal.zzp;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: sq5  reason: default package */
/* loaded from: classes.dex */
public final class sq5 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ zzp g0;
    public final /* synthetic */ boolean h0;
    public final /* synthetic */ m i0;
    public final /* synthetic */ p j0;

    public sq5(p pVar, String str, String str2, zzp zzpVar, boolean z, m mVar) {
        this.j0 = pVar;
        this.a = str;
        this.f0 = str2;
        this.g0 = zzpVar;
        this.h0 = z;
        this.i0 = mVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Bundle bundle;
        RemoteException e;
        d dVar;
        Bundle bundle2 = new Bundle();
        try {
            dVar = this.j0.d;
            if (dVar == null) {
                this.j0.a.w().l().c("Failed to get user properties; not connected to service", this.a, this.f0);
                this.j0.a.G().W(this.i0, bundle2);
                return;
            }
            zt2.j(this.g0);
            List<zzkq> X = dVar.X(this.a, this.f0, this.h0, this.g0);
            bundle = new Bundle();
            if (X != null) {
                for (zzkq zzkqVar : X) {
                    String str = zzkqVar.i0;
                    if (str != null) {
                        bundle.putString(zzkqVar.f0, str);
                    } else {
                        Long l = zzkqVar.h0;
                        if (l != null) {
                            bundle.putLong(zzkqVar.f0, l.longValue());
                        } else {
                            Double d = zzkqVar.k0;
                            if (d != null) {
                                bundle.putDouble(zzkqVar.f0, d.doubleValue());
                            }
                        }
                    }
                }
            }
            try {
                try {
                    this.j0.D();
                    this.j0.a.G().W(this.i0, bundle);
                } catch (Throwable th) {
                    th = th;
                    bundle2 = bundle;
                    this.j0.a.G().W(this.i0, bundle2);
                    throw th;
                }
            } catch (RemoteException e2) {
                e = e2;
                this.j0.a.w().l().c("Failed to get user properties; remote exception", this.a, e);
                this.j0.a.G().W(this.i0, bundle);
            }
        } catch (RemoteException e3) {
            bundle = bundle2;
            e = e3;
        } catch (Throwable th2) {
            th = th2;
            this.j0.a.G().W(this.i0, bundle2);
            throw th;
        }
    }
}
