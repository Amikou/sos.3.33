package defpackage;

import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: js5  reason: default package */
/* loaded from: classes.dex */
public final class js5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ boolean f0;
    public final /* synthetic */ zzaa g0;
    public final /* synthetic */ p h0;

    public js5(p pVar, boolean z, zzp zzpVar, boolean z2, zzaa zzaaVar, zzaa zzaaVar2) {
        this.h0 = pVar;
        this.a = zzpVar;
        this.f0 = z2;
        this.g0 = zzaaVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d dVar;
        dVar = this.h0.d;
        if (dVar == null) {
            this.h0.a.w().l().a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        zt2.j(this.a);
        this.h0.K(dVar, this.f0 ? null : this.g0, this.a);
        this.h0.D();
    }
}
