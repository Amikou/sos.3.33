package defpackage;

import defpackage.wh;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import org.bouncycastle.asn1.a1;
import org.bouncycastle.asn1.h0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.p0;
import org.bouncycastle.asn1.t;
import org.bouncycastle.asn1.v;

/* renamed from: j4  reason: default package */
/* loaded from: classes2.dex */
public abstract class j4 extends k implements Iterable {
    public Vector a;
    public boolean f0;

    public j4() {
        this.a = new Vector();
        this.f0 = false;
    }

    public j4(c4 c4Var) {
        Vector vector = new Vector();
        this.a = vector;
        this.f0 = false;
        vector.addElement(c4Var);
    }

    public j4(d4 d4Var, boolean z) {
        this.a = new Vector();
        this.f0 = false;
        for (int i = 0; i != d4Var.c(); i++) {
            this.a.addElement(d4Var.b(i));
        }
        if (z) {
            H();
        }
    }

    public j4(c4[] c4VarArr, boolean z) {
        this.a = new Vector();
        this.f0 = false;
        for (int i = 0; i != c4VarArr.length; i++) {
            this.a.addElement(c4VarArr[i]);
        }
        if (z) {
            H();
        }
    }

    public static j4 B(k4 k4Var, boolean z) {
        if (z) {
            if (k4Var.E()) {
                return (j4) k4Var.B();
            }
            throw new IllegalArgumentException("object implicit - explicit expected.");
        }
        k B = k4Var.B();
        if (k4Var.E()) {
            return k4Var instanceof v ? new t(B) : new a1(B);
        } else if (B instanceof j4) {
            return (j4) B;
        } else {
            if (B instanceof h4) {
                h4 h4Var = (h4) B;
                return k4Var instanceof v ? new t(h4Var.F()) : new a1(h4Var.F());
            }
            throw new IllegalArgumentException("unknown object in getInstance: " + k4Var.getClass().getName());
        }
    }

    public final c4 D(Enumeration enumeration) {
        c4 c4Var = (c4) enumeration.nextElement();
        return c4Var == null ? h0.a : c4Var;
    }

    public c4 E(int i) {
        return (c4) this.a.elementAt(i);
    }

    public Enumeration F() {
        return this.a.elements();
    }

    public final boolean G(byte[] bArr, byte[] bArr2) {
        int min = Math.min(bArr.length, bArr2.length);
        for (int i = 0; i != min; i++) {
            if (bArr[i] != bArr2[i]) {
                return (bArr[i] & 255) < (bArr2[i] & 255);
            }
        }
        return min == bArr.length;
    }

    public void H() {
        if (this.f0) {
            return;
        }
        this.f0 = true;
        if (this.a.size() > 1) {
            int size = this.a.size() - 1;
            boolean z = true;
            while (z) {
                int i = 0;
                byte[] z2 = z((c4) this.a.elementAt(0));
                z = false;
                int i2 = 0;
                while (i2 != size) {
                    int i3 = i2 + 1;
                    byte[] z3 = z((c4) this.a.elementAt(i3));
                    if (G(z2, z3)) {
                        z2 = z3;
                    } else {
                        Object elementAt = this.a.elementAt(i2);
                        Vector vector = this.a;
                        vector.setElementAt(vector.elementAt(i3), i2);
                        this.a.setElementAt(elementAt, i3);
                        z = true;
                        i = i2;
                    }
                    i2 = i3;
                }
                size = i;
            }
        }
    }

    public c4[] I() {
        c4[] c4VarArr = new c4[size()];
        for (int i = 0; i != size(); i++) {
            c4VarArr[i] = E(i);
        }
        return c4VarArr;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        Enumeration F = F();
        int size = size();
        while (F.hasMoreElements()) {
            size = (size * 17) ^ D(F).hashCode();
        }
        return size;
    }

    @Override // java.lang.Iterable
    public Iterator<c4> iterator() {
        return new wh.a(I());
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof j4) {
            j4 j4Var = (j4) kVar;
            if (size() != j4Var.size()) {
                return false;
            }
            Enumeration F = F();
            Enumeration F2 = j4Var.F();
            while (F.hasMoreElements()) {
                c4 D = D(F);
                c4 D2 = D(F2);
                k i = D.i();
                k i2 = D2.i();
                if (i != i2 && !i.equals(i2)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public int size() {
        return this.a.size();
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return true;
    }

    public String toString() {
        return this.a.toString();
    }

    @Override // org.bouncycastle.asn1.k
    public k w() {
        if (this.f0) {
            p0 p0Var = new p0();
            p0Var.a = this.a;
            return p0Var;
        }
        Vector vector = new Vector();
        for (int i = 0; i != this.a.size(); i++) {
            vector.addElement(this.a.elementAt(i));
        }
        p0 p0Var2 = new p0();
        p0Var2.a = vector;
        p0Var2.H();
        return p0Var2;
    }

    @Override // org.bouncycastle.asn1.k
    public k y() {
        a1 a1Var = new a1();
        a1Var.a = this.a;
        return a1Var;
    }

    public final byte[] z(c4 c4Var) {
        try {
            return c4Var.i().n("DER");
        } catch (IOException unused) {
            throw new IllegalArgumentException("cannot encode object added to SET");
        }
    }
}
