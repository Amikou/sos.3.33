package defpackage;

import android.net.Uri;

/* compiled from: RangedUri.java */
/* renamed from: s33  reason: default package */
/* loaded from: classes.dex */
public final class s33 {
    public final long a;
    public final long b;
    public final String c;
    public int d;

    public s33(String str, long j, long j2) {
        this.c = str == null ? "" : str;
        this.a = j;
        this.b = j2;
    }

    public s33 a(s33 s33Var, String str) {
        String c = c(str);
        if (s33Var != null && c.equals(s33Var.c(str))) {
            long j = this.b;
            if (j != -1) {
                long j2 = this.a;
                if (j2 + j == s33Var.a) {
                    long j3 = s33Var.b;
                    return new s33(c, j2, j3 != -1 ? j + j3 : -1L);
                }
            }
            long j4 = s33Var.b;
            if (j4 != -1) {
                long j5 = s33Var.a;
                if (j5 + j4 == this.a) {
                    return new s33(c, j5, j != -1 ? j4 + j : -1L);
                }
            }
        }
        return null;
    }

    public Uri b(String str) {
        return rf4.e(str, this.c);
    }

    public String c(String str) {
        return rf4.d(str, this.c);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || s33.class != obj.getClass()) {
            return false;
        }
        s33 s33Var = (s33) obj;
        return this.a == s33Var.a && this.b == s33Var.b && this.c.equals(s33Var.c);
    }

    public int hashCode() {
        if (this.d == 0) {
            this.d = ((((527 + ((int) this.a)) * 31) + ((int) this.b)) * 31) + this.c.hashCode();
        }
        return this.d;
    }

    public String toString() {
        return "RangedUri(referenceUri=" + this.c + ", start=" + this.a + ", length=" + this.b + ")";
    }
}
