package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSettingBinding.java */
/* renamed from: cb1  reason: default package */
/* loaded from: classes2.dex */
public final class cb1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final MaterialButton f;
    public final MaterialButton g;
    public final AppCompatImageView h;
    public final MaterialButton i;
    public final MaterialButton j;
    public final MaterialButton k;
    public final SwitchMaterial l;
    public final SwitchMaterial m;
    public final MaterialButton n;
    public final rp3 o;
    public final MaterialButton p;
    public final MaterialButton q;
    public final MaterialButton r;
    public final MaterialButton s;
    public final MaterialButton t;
    public final MaterialButton u;
    public final MaterialButton v;

    public cb1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, MaterialButton materialButton5, MaterialButton materialButton6, MaterialButton materialButton7, ConstraintLayout constraintLayout2, CardView cardView, CardView cardView2, CardView cardView3, CardView cardView4, CardView cardView5, CardView cardView6, CardView cardView7, CardView cardView8, CardView cardView9, View view, View view2, View view3, View view4, View view5, View view6, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, AppCompatImageView appCompatImageView, MaterialButton materialButton8, MaterialButton materialButton9, MaterialButton materialButton10, SwitchMaterial switchMaterial, SwitchMaterial switchMaterial2, MaterialButton materialButton11, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, rp3 rp3Var, MaterialButton materialButton12, MaterialButton materialButton13, MaterialButton materialButton14, MaterialButton materialButton15, MaterialButton materialButton16, TextView textView9, MaterialButton materialButton17, TextView textView10, View view7, View view8, View view9, View view10, View view11, View view12, View view13, View view14, View view15, View view16, View view17, View view18, MaterialButton materialButton18) {
        this.a = constraintLayout;
        this.b = materialButton2;
        this.c = materialButton3;
        this.d = materialButton4;
        this.e = materialButton5;
        this.f = materialButton6;
        this.g = materialButton7;
        this.h = appCompatImageView;
        this.i = materialButton8;
        this.j = materialButton9;
        this.k = materialButton10;
        this.l = switchMaterial;
        this.m = switchMaterial2;
        this.n = materialButton11;
        this.o = rp3Var;
        this.p = materialButton12;
        this.q = materialButton13;
        this.r = materialButton14;
        this.s = materialButton15;
        this.t = materialButton16;
        this.u = materialButton17;
        this.v = materialButton18;
    }

    public static cb1 a(View view) {
        int i = R.id.btnAppearance;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAppearance);
        if (materialButton != null) {
            i = R.id.btnDefaultCurrency;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnDefaultCurrency);
            if (materialButton2 != null) {
                i = R.id.btnDefaultDateFormat;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnDefaultDateFormat);
                if (materialButton3 != null) {
                    i = R.id.btnDefaultLanguage;
                    MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.btnDefaultLanguage);
                    if (materialButton4 != null) {
                        i = R.id.btnDefaultScreen;
                        MaterialButton materialButton5 = (MaterialButton) ai4.a(view, R.id.btnDefaultScreen);
                        if (materialButton5 != null) {
                            i = R.id.btnDisplayMaster;
                            MaterialButton materialButton6 = (MaterialButton) ai4.a(view, R.id.btnDisplayMaster);
                            if (materialButton6 != null) {
                                i = R.id.btn_wallet_connect;
                                MaterialButton materialButton7 = (MaterialButton) ai4.a(view, R.id.btn_wallet_connect);
                                if (materialButton7 != null) {
                                    ConstraintLayout constraintLayout = (ConstraintLayout) view;
                                    i = R.id.cv_1;
                                    CardView cardView = (CardView) ai4.a(view, R.id.cv_1);
                                    if (cardView != null) {
                                        i = R.id.cv_2;
                                        CardView cardView2 = (CardView) ai4.a(view, R.id.cv_2);
                                        if (cardView2 != null) {
                                            i = R.id.cv_3;
                                            CardView cardView3 = (CardView) ai4.a(view, R.id.cv_3);
                                            if (cardView3 != null) {
                                                i = R.id.cv4;
                                                CardView cardView4 = (CardView) ai4.a(view, R.id.cv4);
                                                if (cardView4 != null) {
                                                    i = R.id.cv5;
                                                    CardView cardView5 = (CardView) ai4.a(view, R.id.cv5);
                                                    if (cardView5 != null) {
                                                        i = R.id.cv6;
                                                        CardView cardView6 = (CardView) ai4.a(view, R.id.cv6);
                                                        if (cardView6 != null) {
                                                            i = R.id.cv7;
                                                            CardView cardView7 = (CardView) ai4.a(view, R.id.cv7);
                                                            if (cardView7 != null) {
                                                                i = R.id.cv_appearance;
                                                                CardView cardView8 = (CardView) ai4.a(view, R.id.cv_appearance);
                                                                if (cardView8 != null) {
                                                                    i = R.id.cv_currency;
                                                                    CardView cardView9 = (CardView) ai4.a(view, R.id.cv_currency);
                                                                    if (cardView9 != null) {
                                                                        i = R.id.dividerDefaultCurrency;
                                                                        View a = ai4.a(view, R.id.dividerDefaultCurrency);
                                                                        if (a != null) {
                                                                            i = R.id.dividerDefaultDateFormat;
                                                                            View a2 = ai4.a(view, R.id.dividerDefaultDateFormat);
                                                                            if (a2 != null) {
                                                                                i = R.id.dividerDefaultScreen;
                                                                                View a3 = ai4.a(view, R.id.dividerDefaultScreen);
                                                                                if (a3 != null) {
                                                                                    i = R.id.divider__notification;
                                                                                    View a4 = ai4.a(view, R.id.divider__notification);
                                                                                    if (a4 != null) {
                                                                                        i = R.id.dividerSwitchWallet;
                                                                                        View a5 = ai4.a(view, R.id.dividerSwitchWallet);
                                                                                        if (a5 != null) {
                                                                                            i = R.id.dividerTextViewPref;
                                                                                            View a6 = ai4.a(view, R.id.dividerTextViewPref);
                                                                                            if (a6 != null) {
                                                                                                i = R.id.imageView6;
                                                                                                ImageView imageView = (ImageView) ai4.a(view, R.id.imageView6);
                                                                                                if (imageView != null) {
                                                                                                    i = R.id.imageView7;
                                                                                                    ImageView imageView2 = (ImageView) ai4.a(view, R.id.imageView7);
                                                                                                    if (imageView2 != null) {
                                                                                                        i = R.id.imageView8;
                                                                                                        ImageView imageView3 = (ImageView) ai4.a(view, R.id.imageView8);
                                                                                                        if (imageView3 != null) {
                                                                                                            i = R.id.imageView9;
                                                                                                            ImageView imageView4 = (ImageView) ai4.a(view, R.id.imageView9);
                                                                                                            if (imageView4 != null) {
                                                                                                                i = R.id.imgIndicator;
                                                                                                                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgIndicator);
                                                                                                                if (appCompatImageView != null) {
                                                                                                                    i = R.id.joinCommunityButton;
                                                                                                                    MaterialButton materialButton8 = (MaterialButton) ai4.a(view, R.id.joinCommunityButton);
                                                                                                                    if (materialButton8 != null) {
                                                                                                                        i = R.id.logout;
                                                                                                                        MaterialButton materialButton9 = (MaterialButton) ai4.a(view, R.id.logout);
                                                                                                                        if (materialButton9 != null) {
                                                                                                                            i = R.id.privacy;
                                                                                                                            MaterialButton materialButton10 = (MaterialButton) ai4.a(view, R.id.privacy);
                                                                                                                            if (materialButton10 != null) {
                                                                                                                                i = R.id.switchDisplayMaster;
                                                                                                                                SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchDisplayMaster);
                                                                                                                                if (switchMaterial != null) {
                                                                                                                                    i = R.id.switchTheme;
                                                                                                                                    SwitchMaterial switchMaterial2 = (SwitchMaterial) ai4.a(view, R.id.switchTheme);
                                                                                                                                    if (switchMaterial2 != null) {
                                                                                                                                        i = R.id.terms;
                                                                                                                                        MaterialButton materialButton11 = (MaterialButton) ai4.a(view, R.id.terms);
                                                                                                                                        if (materialButton11 != null) {
                                                                                                                                            i = R.id.textView;
                                                                                                                                            TextView textView = (TextView) ai4.a(view, R.id.textView);
                                                                                                                                            if (textView != null) {
                                                                                                                                                i = R.id.textView11;
                                                                                                                                                TextView textView2 = (TextView) ai4.a(view, R.id.textView11);
                                                                                                                                                if (textView2 != null) {
                                                                                                                                                    i = R.id.textView12;
                                                                                                                                                    TextView textView3 = (TextView) ai4.a(view, R.id.textView12);
                                                                                                                                                    if (textView3 != null) {
                                                                                                                                                        i = R.id.textView13;
                                                                                                                                                        TextView textView4 = (TextView) ai4.a(view, R.id.textView13);
                                                                                                                                                        if (textView4 != null) {
                                                                                                                                                            i = R.id.textView14;
                                                                                                                                                            TextView textView5 = (TextView) ai4.a(view, R.id.textView14);
                                                                                                                                                            if (textView5 != null) {
                                                                                                                                                                i = R.id.textView31;
                                                                                                                                                                TextView textView6 = (TextView) ai4.a(view, R.id.textView31);
                                                                                                                                                                if (textView6 != null) {
                                                                                                                                                                    i = R.id.textViewPref;
                                                                                                                                                                    TextView textView7 = (TextView) ai4.a(view, R.id.textViewPref);
                                                                                                                                                                    if (textView7 != null) {
                                                                                                                                                                        i = R.id.textViewPrefAppearance;
                                                                                                                                                                        TextView textView8 = (TextView) ai4.a(view, R.id.textViewPrefAppearance);
                                                                                                                                                                        if (textView8 != null) {
                                                                                                                                                                            i = R.id.toolbar;
                                                                                                                                                                            View a7 = ai4.a(view, R.id.toolbar);
                                                                                                                                                                            if (a7 != null) {
                                                                                                                                                                                rp3 a8 = rp3.a(a7);
                                                                                                                                                                                i = R.id.tvApplicationVersion;
                                                                                                                                                                                MaterialButton materialButton12 = (MaterialButton) ai4.a(view, R.id.tvApplicationVersion);
                                                                                                                                                                                if (materialButton12 != null) {
                                                                                                                                                                                    i = R.id.tv_my_contacts;
                                                                                                                                                                                    MaterialButton materialButton13 = (MaterialButton) ai4.a(view, R.id.tv_my_contacts);
                                                                                                                                                                                    if (materialButton13 != null) {
                                                                                                                                                                                        i = R.id.tv_notification;
                                                                                                                                                                                        MaterialButton materialButton14 = (MaterialButton) ai4.a(view, R.id.tv_notification);
                                                                                                                                                                                        if (materialButton14 != null) {
                                                                                                                                                                                            i = R.id.tv_security;
                                                                                                                                                                                            MaterialButton materialButton15 = (MaterialButton) ai4.a(view, R.id.tv_security);
                                                                                                                                                                                            if (materialButton15 != null) {
                                                                                                                                                                                                i = R.id.tv_switch_wallet;
                                                                                                                                                                                                MaterialButton materialButton16 = (MaterialButton) ai4.a(view, R.id.tv_switch_wallet);
                                                                                                                                                                                                if (materialButton16 != null) {
                                                                                                                                                                                                    i = R.id.tv_switch_wallet_label;
                                                                                                                                                                                                    TextView textView9 = (TextView) ai4.a(view, R.id.tv_switch_wallet_label);
                                                                                                                                                                                                    if (textView9 != null) {
                                                                                                                                                                                                        i = R.id.tv_transaction_history;
                                                                                                                                                                                                        MaterialButton materialButton17 = (MaterialButton) ai4.a(view, R.id.tv_transaction_history);
                                                                                                                                                                                                        if (materialButton17 != null) {
                                                                                                                                                                                                            i = R.id.txtSwitchWalletLabel;
                                                                                                                                                                                                            TextView textView10 = (TextView) ai4.a(view, R.id.txtSwitchWalletLabel);
                                                                                                                                                                                                            if (textView10 != null) {
                                                                                                                                                                                                                i = R.id.view;
                                                                                                                                                                                                                View a9 = ai4.a(view, R.id.view);
                                                                                                                                                                                                                if (a9 != null) {
                                                                                                                                                                                                                    i = R.id.view1;
                                                                                                                                                                                                                    View a10 = ai4.a(view, R.id.view1);
                                                                                                                                                                                                                    if (a10 != null) {
                                                                                                                                                                                                                        i = R.id.view11;
                                                                                                                                                                                                                        View a11 = ai4.a(view, R.id.view11);
                                                                                                                                                                                                                        if (a11 != null) {
                                                                                                                                                                                                                            i = R.id.view12;
                                                                                                                                                                                                                            View a12 = ai4.a(view, R.id.view12);
                                                                                                                                                                                                                            if (a12 != null) {
                                                                                                                                                                                                                                i = R.id.view2;
                                                                                                                                                                                                                                View a13 = ai4.a(view, R.id.view2);
                                                                                                                                                                                                                                if (a13 != null) {
                                                                                                                                                                                                                                    i = R.id.view3;
                                                                                                                                                                                                                                    View a14 = ai4.a(view, R.id.view3);
                                                                                                                                                                                                                                    if (a14 != null) {
                                                                                                                                                                                                                                        i = R.id.view4;
                                                                                                                                                                                                                                        View a15 = ai4.a(view, R.id.view4);
                                                                                                                                                                                                                                        if (a15 != null) {
                                                                                                                                                                                                                                            i = R.id.view5;
                                                                                                                                                                                                                                            View a16 = ai4.a(view, R.id.view5);
                                                                                                                                                                                                                                            if (a16 != null) {
                                                                                                                                                                                                                                                i = R.id.view6;
                                                                                                                                                                                                                                                View a17 = ai4.a(view, R.id.view6);
                                                                                                                                                                                                                                                if (a17 != null) {
                                                                                                                                                                                                                                                    i = R.id.view7;
                                                                                                                                                                                                                                                    View a18 = ai4.a(view, R.id.view7);
                                                                                                                                                                                                                                                    if (a18 != null) {
                                                                                                                                                                                                                                                        i = R.id.view8;
                                                                                                                                                                                                                                                        View a19 = ai4.a(view, R.id.view8);
                                                                                                                                                                                                                                                        if (a19 != null) {
                                                                                                                                                                                                                                                            i = R.id.view9;
                                                                                                                                                                                                                                                            View a20 = ai4.a(view, R.id.view9);
                                                                                                                                                                                                                                                            if (a20 != null) {
                                                                                                                                                                                                                                                                i = R.id.zendeskSupport;
                                                                                                                                                                                                                                                                MaterialButton materialButton18 = (MaterialButton) ai4.a(view, R.id.zendeskSupport);
                                                                                                                                                                                                                                                                if (materialButton18 != null) {
                                                                                                                                                                                                                                                                    return new cb1(constraintLayout, materialButton, materialButton2, materialButton3, materialButton4, materialButton5, materialButton6, materialButton7, constraintLayout, cardView, cardView2, cardView3, cardView4, cardView5, cardView6, cardView7, cardView8, cardView9, a, a2, a3, a4, a5, a6, imageView, imageView2, imageView3, imageView4, appCompatImageView, materialButton8, materialButton9, materialButton10, switchMaterial, switchMaterial2, materialButton11, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, a8, materialButton12, materialButton13, materialButton14, materialButton15, materialButton16, textView9, materialButton17, textView10, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, materialButton18);
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
