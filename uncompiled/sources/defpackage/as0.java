package defpackage;

/* compiled from: DropAnimationValue.java */
/* renamed from: as0  reason: default package */
/* loaded from: classes2.dex */
public class as0 implements wg4 {
    public int a;
    public int b;
    public int c;

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.a;
    }

    public void d(int i) {
        this.b = i;
    }

    public void e(int i) {
        this.c = i;
    }

    public void f(int i) {
        this.a = i;
    }
}
