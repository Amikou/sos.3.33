package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.fragments.ReflectionsFragment;

/* renamed from: x53  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class x53 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ x53 a = new x53();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        ReflectionsFragment.N(dialogInterface);
    }
}
