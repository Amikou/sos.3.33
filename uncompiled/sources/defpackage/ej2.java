package defpackage;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import org.web3j.exceptions.MessageDecodingException;
import org.web3j.exceptions.MessageEncodingException;

/* compiled from: Numeric.java */
/* renamed from: ej2  reason: default package */
/* loaded from: classes3.dex */
public final class ej2 {
    private static final String HEX_PREFIX = "0x";

    private ej2() {
    }

    public static byte asByte(int i, int i2) {
        return (byte) ((i << 4) | i2);
    }

    public static String cleanHexPrefix(String str) {
        return containsHexPrefix(str) ? str.substring(2) : str;
    }

    public static boolean containsHexPrefix(String str) {
        return !uu3.isEmpty(str) && str.length() > 1 && str.charAt(0) == '0' && str.charAt(1) == 'x';
    }

    public static BigInteger decodeQuantity(String str) {
        if (isLongValue(str)) {
            return BigInteger.valueOf(Long.parseLong(str));
        }
        if (isValidHexQuantity(str)) {
            try {
                return new BigInteger(str.substring(2), 16);
            } catch (NumberFormatException e) {
                throw new MessageDecodingException("Negative ", e);
            }
        }
        throw new MessageDecodingException("Value must be in format 0x[1-9]+[0-9]* or 0x0");
    }

    public static String encodeQuantity(BigInteger bigInteger) {
        if (bigInteger.signum() != -1) {
            return HEX_PREFIX + bigInteger.toString(16);
        }
        throw new MessageEncodingException("Negative values are not supported");
    }

    public static byte[] hexStringToByteArray(String str) {
        byte[] bArr;
        String cleanHexPrefix = cleanHexPrefix(str);
        int length = cleanHexPrefix.length();
        int i = 0;
        if (length == 0) {
            return new byte[0];
        }
        if (length % 2 != 0) {
            bArr = new byte[(length / 2) + 1];
            bArr[0] = (byte) Character.digit(cleanHexPrefix.charAt(0), 16);
            i = 1;
        } else {
            bArr = new byte[length / 2];
        }
        while (i < length) {
            int i2 = i + 1;
            bArr[i2 / 2] = (byte) ((Character.digit(cleanHexPrefix.charAt(i), 16) << 4) + Character.digit(cleanHexPrefix.charAt(i2), 16));
            i += 2;
        }
        return bArr;
    }

    public static boolean isIntegerValue(BigDecimal bigDecimal) {
        return bigDecimal.signum() == 0 || bigDecimal.scale() <= 0 || bigDecimal.stripTrailingZeros().scale() <= 0;
    }

    private static boolean isLongValue(String str) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    private static boolean isValidHexQuantity(String str) {
        return str != null && str.length() >= 3 && str.startsWith(HEX_PREFIX);
    }

    public static String prependHexPrefix(String str) {
        if (containsHexPrefix(str)) {
            return str;
        }
        return HEX_PREFIX + str;
    }

    public static BigInteger toBigInt(byte[] bArr, int i, int i2) {
        return toBigInt(Arrays.copyOfRange(bArr, i, i2 + i));
    }

    public static BigInteger toBigIntNoPrefix(String str) {
        return new BigInteger(str, 16);
    }

    public static byte[] toBytesPadded(BigInteger bigInteger, int i) {
        int length;
        byte[] bArr = new byte[i];
        byte[] byteArray = bigInteger.toByteArray();
        int i2 = 1;
        if (byteArray[0] == 0) {
            length = byteArray.length - 1;
        } else {
            i2 = 0;
            length = byteArray.length;
        }
        if (length <= i) {
            System.arraycopy(byteArray, i2, bArr, i - length, length);
            return bArr;
        }
        throw new RuntimeException("Input is too large to put in byte array of size " + i);
    }

    public static String toHexString(byte[] bArr, int i, int i2, boolean z) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append(HEX_PREFIX);
        }
        for (int i3 = i; i3 < i + i2; i3++) {
            sb.append(String.format("%02x", Integer.valueOf(bArr[i3] & 255)));
        }
        return sb.toString();
    }

    public static String toHexStringNoPrefix(BigInteger bigInteger) {
        return bigInteger.toString(16);
    }

    public static String toHexStringNoPrefixZeroPadded(BigInteger bigInteger, int i) {
        return toHexStringZeroPadded(bigInteger, i, false);
    }

    public static String toHexStringWithPrefix(BigInteger bigInteger) {
        return HEX_PREFIX + bigInteger.toString(16);
    }

    public static String toHexStringWithPrefixSafe(BigInteger bigInteger) {
        String hexStringNoPrefix = toHexStringNoPrefix(bigInteger);
        if (hexStringNoPrefix.length() < 2) {
            hexStringNoPrefix = uu3.zeros(1) + hexStringNoPrefix;
        }
        return HEX_PREFIX + hexStringNoPrefix;
    }

    public static String toHexStringWithPrefixZeroPadded(BigInteger bigInteger, int i) {
        return toHexStringZeroPadded(bigInteger, i, true);
    }

    private static String toHexStringZeroPadded(BigInteger bigInteger, int i, boolean z) {
        String hexStringNoPrefix = toHexStringNoPrefix(bigInteger);
        int length = hexStringNoPrefix.length();
        if (length <= i) {
            if (bigInteger.signum() >= 0) {
                if (length < i) {
                    hexStringNoPrefix = uu3.zeros(i - length) + hexStringNoPrefix;
                }
                if (z) {
                    return HEX_PREFIX + hexStringNoPrefix;
                }
                return hexStringNoPrefix;
            }
            throw new UnsupportedOperationException("Value cannot be negative");
        }
        throw new UnsupportedOperationException("Value " + hexStringNoPrefix + "is larger then length " + i);
    }

    public static BigInteger toBigInt(byte[] bArr) {
        return new BigInteger(1, bArr);
    }

    public static String toHexStringNoPrefix(byte[] bArr) {
        return toHexString(bArr, 0, bArr.length, false);
    }

    public static BigInteger toBigInt(String str) {
        return toBigIntNoPrefix(cleanHexPrefix(str));
    }

    public static String toHexString(byte[] bArr) {
        return toHexString(bArr, 0, bArr.length, true);
    }
}
