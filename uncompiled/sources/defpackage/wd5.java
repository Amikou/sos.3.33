package defpackage;

import java.util.Map;

/* renamed from: wd5  reason: default package */
/* loaded from: classes.dex */
public interface wd5 {
    sd5<?, ?> a(Object obj);

    Object b(Object obj);

    Object c(Object obj, Object obj2);

    int d(int i, Object obj, Object obj2);

    Object e(Object obj);

    boolean f(Object obj);

    Map<?, ?> g(Object obj);

    Map<?, ?> h(Object obj);
}
