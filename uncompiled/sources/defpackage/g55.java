package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: g55  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class g55 {
    public static z55 a(m55 m55Var, z55 z55Var, wk5 wk5Var, List<z55> list) {
        if (m55Var.o(z55Var.zzc())) {
            z55 e = m55Var.e(z55Var.zzc());
            if (e instanceof c55) {
                return ((c55) e).a(wk5Var, list);
            }
            throw new IllegalArgumentException(String.format("%s is not a function", z55Var.zzc()));
        } else if ("hasOwnProperty".equals(z55Var.zzc())) {
            vm5.a("hasOwnProperty", 1, list);
            return m55Var.o(wk5Var.a(list.get(0)).zzc()) ? z55.c0 : z55.d0;
        } else {
            throw new IllegalArgumentException(String.format("Object has no function %s", z55Var.zzc()));
        }
    }

    public static Iterator<z55> b(Map<String, z55> map) {
        return new i55(map.keySet().iterator());
    }
}
