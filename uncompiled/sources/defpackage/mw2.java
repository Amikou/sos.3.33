package defpackage;

import java.nio.ByteBuffer;
import java.util.UUID;
import okhttp3.internal.http2.Http2Connection;

/* compiled from: PsshAtomUtil.java */
/* renamed from: mw2  reason: default package */
/* loaded from: classes.dex */
public final class mw2 {

    /* compiled from: PsshAtomUtil.java */
    /* renamed from: mw2$a */
    /* loaded from: classes.dex */
    public static class a {
        public final UUID a;
        public final int b;
        public final byte[] c;

        public a(UUID uuid, int i, byte[] bArr) {
            this.a = uuid;
            this.b = i;
            this.c = bArr;
        }
    }

    public static byte[] a(UUID uuid, byte[] bArr) {
        return b(uuid, null, bArr);
    }

    public static byte[] b(UUID uuid, UUID[] uuidArr, byte[] bArr) {
        int length = (bArr != null ? bArr.length : 0) + 32;
        if (uuidArr != null) {
            length += (uuidArr.length * 16) + 4;
        }
        ByteBuffer allocate = ByteBuffer.allocate(length);
        allocate.putInt(length);
        allocate.putInt(1886614376);
        allocate.putInt(uuidArr != null ? Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE : 0);
        allocate.putLong(uuid.getMostSignificantBits());
        allocate.putLong(uuid.getLeastSignificantBits());
        if (uuidArr != null) {
            allocate.putInt(uuidArr.length);
            for (UUID uuid2 : uuidArr) {
                allocate.putLong(uuid2.getMostSignificantBits());
                allocate.putLong(uuid2.getLeastSignificantBits());
            }
        }
        if (bArr != null && bArr.length != 0) {
            allocate.putInt(bArr.length);
            allocate.put(bArr);
        }
        return allocate.array();
    }

    public static boolean c(byte[] bArr) {
        return d(bArr) != null;
    }

    public static a d(byte[] bArr) {
        op2 op2Var = new op2(bArr);
        if (op2Var.f() < 32) {
            return null;
        }
        op2Var.P(0);
        if (op2Var.n() == op2Var.a() + 4 && op2Var.n() == 1886614376) {
            int c = zi.c(op2Var.n());
            if (c > 1) {
                p12.i("PsshAtomUtil", "Unsupported pssh version: " + c);
                return null;
            }
            UUID uuid = new UUID(op2Var.w(), op2Var.w());
            if (c == 1) {
                op2Var.Q(op2Var.H() * 16);
            }
            int H = op2Var.H();
            if (H != op2Var.a()) {
                return null;
            }
            byte[] bArr2 = new byte[H];
            op2Var.j(bArr2, 0, H);
            return new a(uuid, c, bArr2);
        }
        return null;
    }

    public static byte[] e(byte[] bArr, UUID uuid) {
        a d = d(bArr);
        if (d == null) {
            return null;
        }
        if (uuid.equals(d.a)) {
            return d.c;
        }
        p12.i("PsshAtomUtil", "UUID mismatch. Expected: " + uuid + ", got: " + d.a + ".");
        return null;
    }

    public static UUID f(byte[] bArr) {
        a d = d(bArr);
        if (d == null) {
            return null;
        }
        return d.a;
    }

    public static int g(byte[] bArr) {
        a d = d(bArr);
        if (d == null) {
            return -1;
        }
        return d.b;
    }
}
