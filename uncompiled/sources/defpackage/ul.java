package defpackage;

/* compiled from: AviMainHeaderChunk.java */
/* renamed from: ul  reason: default package */
/* loaded from: classes.dex */
public final class ul implements sl {
    public final int a;
    public final int b;
    public final int c;

    public ul(int i, int i2, int i3, int i4) {
        this.a = i;
        this.b = i2;
        this.c = i3;
    }

    public static ul b(op2 op2Var) {
        int q = op2Var.q();
        op2Var.Q(8);
        int q2 = op2Var.q();
        int q3 = op2Var.q();
        op2Var.Q(4);
        int q4 = op2Var.q();
        op2Var.Q(12);
        return new ul(q, q2, q3, q4);
    }

    public boolean a() {
        return (this.b & 16) == 16;
    }

    @Override // defpackage.sl
    public int getType() {
        return 1751742049;
    }
}
