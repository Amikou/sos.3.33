package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: g52  reason: default package */
/* loaded from: classes2.dex */
public class g52 extends h {
    public int a;
    public int f0;
    public byte[] g0;
    public byte[] h0;
    public byte[] i0;
    public va j0;

    public g52(int i, int i2, je1 je1Var, rs2 rs2Var, mq2 mq2Var, va vaVar) {
        this.a = i;
        this.f0 = i2;
        this.g0 = je1Var.e();
        this.h0 = rs2Var.h();
        this.i0 = mq2Var.a();
        this.j0 = vaVar;
    }

    public g52(h4 h4Var) {
        this.a = ((g) h4Var.D(0)).B().intValue();
        this.f0 = ((g) h4Var.D(1)).B().intValue();
        this.g0 = ((f4) h4Var.D(2)).D();
        this.h0 = ((f4) h4Var.D(3)).D();
        this.i0 = ((f4) h4Var.D(4)).D();
        this.j0 = va.p(h4Var.D(5));
    }

    public static g52 s(Object obj) {
        if (obj instanceof g52) {
            return (g52) obj;
        }
        if (obj != null) {
            return new g52(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(this.a));
        d4Var.a(new g(this.f0));
        d4Var.a(new j0(this.g0));
        d4Var.a(new j0(this.h0));
        d4Var.a(new j0(this.i0));
        d4Var.a(this.j0);
        return new n0(d4Var);
    }

    public va o() {
        return this.j0;
    }

    public je1 p() {
        return new je1(this.g0);
    }

    public rs2 q() {
        return new rs2(p(), this.h0);
    }

    public int t() {
        return this.f0;
    }

    public int w() {
        return this.a;
    }

    public mq2 y() {
        return new mq2(this.i0);
    }
}
