package defpackage;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentDefaultLanguageBinding.java */
/* renamed from: ba1  reason: default package */
/* loaded from: classes2.dex */
public final class ba1 {
    public final ConstraintLayout a;
    public final EditText b;
    public final RecyclerView c;
    public final rp3 d;
    public final TextView e;

    public ba1(ConstraintLayout constraintLayout, EditText editText, RecyclerView recyclerView, rp3 rp3Var, TextView textView, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = editText;
        this.c = recyclerView;
        this.d = rp3Var;
        this.e = textView;
    }

    public static ba1 a(View view) {
        int i = R.id.etSearch;
        EditText editText = (EditText) ai4.a(view, R.id.etSearch);
        if (editText != null) {
            i = R.id.rvRealLanguage;
            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvRealLanguage);
            if (recyclerView != null) {
                i = R.id.toolbar;
                View a = ai4.a(view, R.id.toolbar);
                if (a != null) {
                    rp3 a2 = rp3.a(a);
                    i = R.id.tv_not_fount;
                    TextView textView = (TextView) ai4.a(view, R.id.tv_not_fount);
                    if (textView != null) {
                        i = R.id.vSearchContainer;
                        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vSearchContainer);
                        if (linearLayout != null) {
                            return new ba1((ConstraintLayout) view, editText, recyclerView, a2, textView, linearLayout);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
