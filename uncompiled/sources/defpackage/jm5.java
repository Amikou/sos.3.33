package defpackage;

import android.content.Context;
import com.google.android.gms.internal.measurement.zzhz;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jm5  reason: default package */
/* loaded from: classes.dex */
public final class jm5 extends uo5 {
    public final Context a;
    public final yp5<zzhz<vn5>> b;

    public jm5(Context context, yp5<zzhz<vn5>> yp5Var) {
        Objects.requireNonNull(context, "Null context");
        this.a = context;
        this.b = yp5Var;
    }

    @Override // defpackage.uo5
    public final Context a() {
        return this.a;
    }

    @Override // defpackage.uo5
    public final yp5<zzhz<vn5>> b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        yp5<zzhz<vn5>> yp5Var;
        if (obj == this) {
            return true;
        }
        if (obj instanceof uo5) {
            uo5 uo5Var = (uo5) obj;
            if (this.a.equals(uo5Var.a()) && ((yp5Var = this.b) != null ? yp5Var.equals(uo5Var.b()) : uo5Var.b() == null)) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        yp5<zzhz<vn5>> yp5Var = this.b;
        return hashCode ^ (yp5Var == null ? 0 : yp5Var.hashCode());
    }

    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(valueOf.length() + 46 + valueOf2.length());
        sb.append("FlagsContext{context=");
        sb.append(valueOf);
        sb.append(", hermeticFileOverrides=");
        sb.append(valueOf2);
        sb.append("}");
        return sb.toString();
    }
}
