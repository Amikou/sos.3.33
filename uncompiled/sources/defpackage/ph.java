package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ArrayIterator.java */
/* renamed from: ph  reason: default package */
/* loaded from: classes.dex */
public class ph<T> implements Iterator<T>, Iterable<T> {
    public final T[] a;
    public int f0 = 0;

    public ph(T[] tArr) {
        this.a = tArr;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.f0 < this.a.length;
    }

    @Override // java.lang.Iterable
    public Iterator<T> iterator() {
        return this;
    }

    @Override // java.util.Iterator
    public T next() {
        int i = this.f0;
        T[] tArr = this.a;
        if (i < tArr.length) {
            this.f0 = i + 1;
            return tArr[i];
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
