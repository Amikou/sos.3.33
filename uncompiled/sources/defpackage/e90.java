package defpackage;

/* compiled from: CoroutineStackFrame.kt */
/* renamed from: e90  reason: default package */
/* loaded from: classes2.dex */
public interface e90 {
    e90 getCallerFrame();

    StackTraceElement getStackTraceElement();
}
