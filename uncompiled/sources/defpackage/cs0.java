package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.rd.draw.data.Orientation;

/* compiled from: DropDrawer.java */
/* renamed from: cs0  reason: default package */
/* loaded from: classes2.dex */
public class cs0 extends hn {
    public cs0(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2) {
        if (wg4Var instanceof as0) {
            as0 as0Var = (as0) wg4Var;
            int t = this.b.t();
            int p = this.b.p();
            this.a.setColor(t);
            canvas.drawCircle(i, i2, this.b.m(), this.a);
            this.a.setColor(p);
            if (this.b.g() == Orientation.HORIZONTAL) {
                canvas.drawCircle(as0Var.c(), as0Var.a(), as0Var.b(), this.a);
            } else {
                canvas.drawCircle(as0Var.a(), as0Var.c(), as0Var.b(), this.a);
            }
        }
    }
}
