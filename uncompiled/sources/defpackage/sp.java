package defpackage;

/* compiled from: BitmapCounterProvider.java */
/* renamed from: sp  reason: default package */
/* loaded from: classes.dex */
public class sp {
    public static final int a = b();
    public static int b = 384;
    public static volatile rp c;

    public static rp a() {
        if (c == null) {
            synchronized (sp.class) {
                if (c == null) {
                    c = new rp(b, a);
                }
            }
        }
        return c;
    }

    public static int b() {
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min > 16777216) {
            return (min / 4) * 3;
        }
        return min / 2;
    }
}
