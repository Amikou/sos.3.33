package defpackage;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.UserManager;
import com.google.android.gms.internal.clearcut.b;

/* renamed from: r45 */
/* loaded from: classes.dex */
public abstract class r45<T> {
    public static final Object g = new Object();
    @SuppressLint({"StaticFieldLeak"})
    public static Context h;
    public static volatile Boolean i;
    public static volatile Boolean j;
    public final w55 a;
    public final String b;
    public final String c;
    public final T d;
    public volatile g45 e;
    public volatile SharedPreferences f;

    public r45(w55 w55Var, String str, T t) {
        String str2;
        String str3;
        String str4;
        String str5;
        Uri uri;
        Uri uri2;
        this.e = null;
        this.f = null;
        str2 = w55Var.a;
        if (str2 == null) {
            uri2 = w55Var.b;
            if (uri2 == null) {
                throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
            }
        }
        str3 = w55Var.a;
        if (str3 != null) {
            uri = w55Var.b;
            if (uri != null) {
                throw new IllegalArgumentException("Must pass one of SharedPreferences file name or ContentProvider URI");
            }
        }
        this.a = w55Var;
        str4 = w55Var.c;
        String valueOf = String.valueOf(str4);
        String valueOf2 = String.valueOf(str);
        this.c = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        str5 = w55Var.d;
        String valueOf3 = String.valueOf(str5);
        String valueOf4 = String.valueOf(str);
        this.b = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
        this.d = t;
    }

    public /* synthetic */ r45(w55 w55Var, String str, Object obj, f55 f55Var) {
        this(w55Var, str, obj);
    }

    public static void b(Context context) {
        Context applicationContext;
        if (h == null) {
            synchronized (g) {
                if ((Build.VERSION.SDK_INT < 24 || !context.isDeviceProtectedStorage()) && (applicationContext = context.getApplicationContext()) != null) {
                    context = applicationContext;
                }
                if (h != context) {
                    i = null;
                }
                h = context;
            }
        }
    }

    public static <V> V d(r55<V> r55Var) {
        try {
            return r55Var.d();
        } catch (SecurityException unused) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return r55Var.d();
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }

    public static <T> r45<T> e(w55 w55Var, String str, T t, b<T> bVar) {
        return new o55(w55Var, str, t, bVar);
    }

    public static r45<String> f(w55 w55Var, String str, String str2) {
        return new l55(w55Var, str, str2);
    }

    public static r45<Boolean> g(w55 w55Var, String str, boolean z) {
        return new h55(w55Var, str, Boolean.valueOf(z));
    }

    public static boolean h(String str, boolean z) {
        if (p()) {
            return ((Boolean) d(new r55(str, false) { // from class: b55
                public final String a;
                public final boolean b = false;

                {
                    this.a = str;
                }

                @Override // defpackage.r55
                public final Object d() {
                    Boolean valueOf;
                    valueOf = Boolean.valueOf(v56.h(r45.h.getContentResolver(), this.a, this.b));
                    return valueOf;
                }
            })).booleanValue();
        }
        return false;
    }

    public static boolean p() {
        if (i == null) {
            Context context = h;
            if (context == null) {
                return false;
            }
            i = Boolean.valueOf(kq2.a(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0);
        }
        return i.booleanValue();
    }

    public final T a() {
        boolean z;
        if (h != null) {
            z = this.a.f;
            if (z) {
                T o = o();
                if (o != null) {
                    return o;
                }
                T n = n();
                if (n != null) {
                    return n;
                }
            } else {
                T n2 = n();
                if (n2 != null) {
                    return n2;
                }
                T o2 = o();
                if (o2 != null) {
                    return o2;
                }
            }
            return this.d;
        }
        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
    }

    public abstract T c(SharedPreferences sharedPreferences);

    public abstract T j(String str);

    @TargetApi(24)
    public final T n() {
        Uri uri;
        String str;
        boolean z;
        String str2;
        Uri uri2;
        if (h("gms:phenotype:phenotype_flag:debug_bypass_phenotype", false)) {
            String valueOf = String.valueOf(this.b);
            if (valueOf.length() != 0) {
                "Bypass reading Phenotype values for flag: ".concat(valueOf);
            }
        } else {
            uri = this.a.b;
            if (uri != null) {
                if (this.e == null) {
                    ContentResolver contentResolver = h.getContentResolver();
                    uri2 = this.a.b;
                    this.e = g45.a(contentResolver, uri2);
                }
                String str3 = (String) d(new r55(this, this.e) { // from class: u45
                    public final r45 a;
                    public final g45 b;

                    {
                        this.a = this;
                        this.b = r2;
                    }

                    @Override // defpackage.r55
                    public final Object d() {
                        return this.b.c().get(this.a.b);
                    }
                });
                if (str3 != null) {
                    return j(str3);
                }
            } else {
                str = this.a.a;
                if (str != null) {
                    if (Build.VERSION.SDK_INT < 24 || h.isDeviceProtectedStorage()) {
                        z = true;
                    } else {
                        if (j == null || !j.booleanValue()) {
                            j = Boolean.valueOf(((UserManager) h.getSystemService(UserManager.class)).isUserUnlocked());
                        }
                        z = j.booleanValue();
                    }
                    if (!z) {
                        return null;
                    }
                    if (this.f == null) {
                        Context context = h;
                        str2 = this.a.a;
                        this.f = context.getSharedPreferences(str2, 0);
                    }
                    SharedPreferences sharedPreferences = this.f;
                    if (sharedPreferences.contains(this.b)) {
                        return c(sharedPreferences);
                    }
                }
            }
        }
        return null;
    }

    public final T o() {
        boolean z;
        String str;
        z = this.a.e;
        if (z || !p() || (str = (String) d(new r55(this) { // from class: y45
            public final r45 a;

            {
                this.a = this;
            }

            @Override // defpackage.r55
            public final Object d() {
                return this.a.q();
            }
        })) == null) {
            return null;
        }
        return j(str);
    }

    public final /* synthetic */ String q() {
        return v56.c(h.getContentResolver(), this.c, null);
    }
}
