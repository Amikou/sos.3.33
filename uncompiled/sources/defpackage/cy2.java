package defpackage;

/* renamed from: cy2  reason: default package */
/* loaded from: classes.dex */
public final class cy2 {
    public static final int alpha = 2130968628;
    public static final int coordinatorLayoutStyle = 2130968859;
    public static final int font = 2130969021;
    public static final int fontProviderAuthority = 2130969023;
    public static final int fontProviderCerts = 2130969024;
    public static final int fontProviderFetchStrategy = 2130969025;
    public static final int fontProviderFetchTimeout = 2130969026;
    public static final int fontProviderPackage = 2130969027;
    public static final int fontProviderQuery = 2130969028;
    public static final int fontStyle = 2130969030;
    public static final int fontVariationSettings = 2130969031;
    public static final int fontWeight = 2130969032;
    public static final int keylines = 2130969159;
    public static final int layout_anchor = 2130969171;
    public static final int layout_anchorGravity = 2130969172;
    public static final int layout_behavior = 2130969173;
    public static final int layout_dodgeInsetEdges = 2130969222;
    public static final int layout_insetEdge = 2130969235;
    public static final int layout_keyline = 2130969236;
    public static final int statusBarBackground = 2130969609;
    public static final int ttcIndex = 2130969779;
}
