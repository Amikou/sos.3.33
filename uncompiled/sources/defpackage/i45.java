package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: i45  reason: default package */
/* loaded from: classes.dex */
public final class i45 implements Iterator<z55> {
    public final /* synthetic */ Iterator a;
    public final /* synthetic */ Iterator f0;

    public i45(p45 p45Var, Iterator it, Iterator it2) {
        this.a = it;
        this.f0 = it2;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        if (this.a.hasNext()) {
            return true;
        }
        return this.f0.hasNext();
    }

    @Override // java.util.Iterator
    public final /* bridge */ /* synthetic */ z55 next() {
        if (this.a.hasNext()) {
            return new f65(((Integer) this.a.next()).toString());
        }
        if (this.f0.hasNext()) {
            return new f65((String) this.f0.next());
        }
        throw new NoSuchElementException();
    }
}
