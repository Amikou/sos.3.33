package defpackage;

/* compiled from: Regex.kt */
/* renamed from: f42  reason: default package */
/* loaded from: classes2.dex */
public final class f42 {
    public final String a;
    public final sr1 b;

    public f42(String str, sr1 sr1Var) {
        fs1.f(str, "value");
        fs1.f(sr1Var, "range");
        this.a = str;
        this.b = sr1Var;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof f42) {
                f42 f42Var = (f42) obj;
                return fs1.b(this.a, f42Var.a) && fs1.b(this.b, f42Var.b);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        sr1 sr1Var = this.b;
        return hashCode + (sr1Var != null ? sr1Var.hashCode() : 0);
    }

    public String toString() {
        return "MatchGroup(value=" + this.a + ", range=" + this.b + ")";
    }
}
