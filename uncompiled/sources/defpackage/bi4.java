package defpackage;

import android.view.View;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ViewButtonShadowBinding.java */
/* renamed from: bi4  reason: default package */
/* loaded from: classes2.dex */
public final class bi4 {
    public final MaterialButton a;
    public final View b;

    public bi4(CardView cardView, MaterialButton materialButton, View view) {
        this.a = materialButton;
        this.b = view;
    }

    public static bi4 a(View view) {
        int i = R.id.button;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.button);
        if (materialButton != null) {
            i = R.id.viewButton;
            View a = ai4.a(view, R.id.viewButton);
            if (a != null) {
                return new bi4((CardView) view, materialButton, a);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
