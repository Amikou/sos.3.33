package defpackage;

import androidx.media3.common.util.b;
import java.util.UUID;

/* compiled from: FrameworkCryptoConfig.java */
/* renamed from: cc1  reason: default package */
/* loaded from: classes.dex */
public final class cc1 implements qa0 {
    public static final boolean d;
    public final UUID a;
    public final byte[] b;
    public final boolean c;

    static {
        boolean z;
        if ("Amazon".equals(b.c)) {
            String str = b.d;
            if ("AFTM".equals(str) || "AFTB".equals(str)) {
                z = true;
                d = z;
            }
        }
        z = false;
        d = z;
    }

    public cc1(UUID uuid, byte[] bArr, boolean z) {
        this.a = uuid;
        this.b = bArr;
        this.c = z;
    }
}
