package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;
import defpackage.xp;

/* compiled from: KeepLastFrameCache.java */
/* renamed from: ex1  reason: default package */
/* loaded from: classes.dex */
public class ex1 implements xp {
    public int a = -1;
    public xp.a b;
    public a<Bitmap> c;

    @Override // defpackage.xp
    public synchronized a<Bitmap> a(int i, int i2, int i3) {
        a<Bitmap> e;
        e = a.e(this.c);
        g();
        return e;
    }

    @Override // defpackage.xp
    public void b(int i, a<Bitmap> aVar, int i2) {
    }

    @Override // defpackage.xp
    public synchronized boolean c(int i) {
        boolean z;
        if (i == this.a) {
            z = a.u(this.c);
        }
        return z;
    }

    @Override // defpackage.xp
    public synchronized void clear() {
        g();
    }

    @Override // defpackage.xp
    public synchronized a<Bitmap> d(int i) {
        if (this.a == i) {
            return a.e(this.c);
        }
        return null;
    }

    @Override // defpackage.xp
    public synchronized void e(int i, a<Bitmap> aVar, int i2) {
        int i3;
        if (aVar != null) {
            if (this.c != null && aVar.j().equals(this.c.j())) {
                return;
            }
        }
        a.g(this.c);
        xp.a aVar2 = this.b;
        if (aVar2 != null && (i3 = this.a) != -1) {
            aVar2.a(this, i3);
        }
        this.c = a.e(aVar);
        xp.a aVar3 = this.b;
        if (aVar3 != null) {
            aVar3.b(this, i);
        }
        this.a = i;
    }

    @Override // defpackage.xp
    public synchronized a<Bitmap> f(int i) {
        return a.e(this.c);
    }

    public final synchronized void g() {
        int i;
        xp.a aVar = this.b;
        if (aVar != null && (i = this.a) != -1) {
            aVar.a(this, i);
        }
        a.g(this.c);
        this.c = null;
        this.a = -1;
    }
}
