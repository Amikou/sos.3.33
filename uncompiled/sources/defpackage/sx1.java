package defpackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: KeysMap.java */
/* renamed from: sx1  reason: default package */
/* loaded from: classes2.dex */
public class sx1 {
    public final Map<String, String> a = new HashMap();
    public int b;
    public int c;

    public sx1(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.a);
    }

    public String b(String str) {
        if (str != null) {
            String trim = str.trim();
            int length = trim.length();
            int i = this.c;
            return length > i ? trim.substring(0, i) : trim;
        }
        return str;
    }

    public final String c(String str) {
        if (str != null) {
            return b(str);
        }
        throw new IllegalArgumentException("Custom attribute key must not be null.");
    }

    public void d(Map<String, String> map) {
        e(map);
    }

    public final synchronized void e(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String c = c(entry.getKey());
            String b = entry.getValue() == null ? "" : b(entry.getValue());
            if (this.a.containsKey(c)) {
                hashMap.put(c, b);
            } else {
                hashMap2.put(c, b);
            }
        }
        this.a.putAll(hashMap);
        int size = this.a.size() + hashMap2.size();
        int i = this.b;
        if (size > i) {
            int size2 = i - this.a.size();
            w12 f = w12.f();
            f.i("Exceeded maximum number of custom attributes (" + this.b + ").");
            hashMap2.keySet().retainAll(new ArrayList(hashMap2.keySet()).subList(0, size2));
        }
        this.a.putAll(hashMap2);
    }
}
