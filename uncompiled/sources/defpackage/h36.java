package defpackage;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.common.internal.q;
import com.google.android.gms.common.internal.r;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: h36  reason: default package */
/* loaded from: classes.dex */
public abstract class h36 extends j35 implements q {
    public static q F1(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
        if (queryLocalInterface instanceof q) {
            return (q) queryLocalInterface;
        }
        return new r(iBinder);
    }
}
