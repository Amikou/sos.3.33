package defpackage;

import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import retrofit2.b;
import retrofit2.n;

/* compiled from: GetSafeMoonNotificationHistoryUseCase.kt */
/* renamed from: lf1  reason: default package */
/* loaded from: classes2.dex */
public final class lf1 {
    public final ac3 a;

    /* compiled from: GetSafeMoonNotificationHistoryUseCase.kt */
    /* renamed from: lf1$a */
    /* loaded from: classes2.dex */
    public static final class a implements wu<NotificationHistory> {
        public final /* synthetic */ q70<NotificationHistory> a;

        /* JADX WARN: Multi-variable type inference failed */
        public a(q70<? super NotificationHistory> q70Var) {
            this.a = q70Var;
        }

        @Override // defpackage.wu
        public void a(b<NotificationHistory> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            q70<NotificationHistory> q70Var = this.a;
            Result.a aVar = Result.Companion;
            q70Var.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }

        @Override // defpackage.wu
        public void b(b<NotificationHistory> bVar, n<NotificationHistory> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                q70<NotificationHistory> q70Var = this.a;
                NotificationHistory a = nVar.a();
                Result.a aVar = Result.Companion;
                q70Var.resumeWith(Result.m52constructorimpl(a));
                return;
            }
            q70<NotificationHistory> q70Var2 = this.a;
            Result.a aVar2 = Result.Companion;
            q70Var2.resumeWith(Result.m52constructorimpl(null));
        }
    }

    public lf1(ac3 ac3Var) {
        fs1.f(ac3Var, "api");
        this.a = ac3Var;
    }

    public Object a(String str, String str2, q70<? super NotificationHistory> q70Var) {
        tb3 tb3Var = new tb3(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var));
        this.a.a(str2, str, hr.d(100), hr.d(0)).n(new a(tb3Var));
        Object a2 = tb3Var.a();
        if (a2 == gs1.d()) {
            ef0.c(q70Var);
        }
        return a2;
    }
}
