package defpackage;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.navigation.NavController;
import androidx.navigation.d;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: AbstractAppBarOnDestinationChangedListener.java */
/* renamed from: p4  reason: default package */
/* loaded from: classes.dex */
public abstract class p4 implements NavController.b {
    public final Context a;
    public final Set<Integer> b;
    public final WeakReference<jn2> c;
    public mr0 d;
    public ValueAnimator e;

    public p4(Context context, af afVar) {
        this.a = context;
        this.b = afVar.b();
        jn2 a = afVar.a();
        if (a != null) {
            this.c = new WeakReference<>(a);
        } else {
            this.c = null;
        }
    }

    @Override // androidx.navigation.NavController.b
    public void a(NavController navController, d dVar, Bundle bundle) {
        if (dVar instanceof g71) {
            return;
        }
        WeakReference<jn2> weakReference = this.c;
        jn2 jn2Var = weakReference != null ? weakReference.get() : null;
        if (this.c != null && jn2Var == null) {
            navController.B(this);
            return;
        }
        CharSequence t = dVar.t();
        boolean z = true;
        if (t != null) {
            StringBuffer stringBuffer = new StringBuffer();
            Matcher matcher = Pattern.compile("\\{(.+?)\\}").matcher(t);
            while (matcher.find()) {
                String group = matcher.group(1);
                if (bundle != null && bundle.containsKey(group)) {
                    matcher.appendReplacement(stringBuffer, "");
                    stringBuffer.append(bundle.get(group).toString());
                } else {
                    throw new IllegalArgumentException("Could not find " + group + " in " + bundle + " to fill label " + ((Object) t));
                }
            }
            matcher.appendTail(stringBuffer);
            d(stringBuffer);
        }
        boolean c = oe2.c(dVar, this.b);
        if (jn2Var == null && c) {
            c(null, 0);
            return;
        }
        if (jn2Var == null || !c) {
            z = false;
        }
        b(z);
    }

    public final void b(boolean z) {
        boolean z2;
        int i;
        if (this.d == null) {
            this.d = new mr0(this.a);
            z2 = false;
        } else {
            z2 = true;
        }
        mr0 mr0Var = this.d;
        if (z) {
            i = n13.nav_app_bar_open_drawer_description;
        } else {
            i = n13.nav_app_bar_navigate_up_description;
        }
        c(mr0Var, i);
        float f = z ? Utils.FLOAT_EPSILON : 1.0f;
        if (z2) {
            float a = this.d.a();
            ValueAnimator valueAnimator = this.e;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.d, "progress", a, f);
            this.e = ofFloat;
            ofFloat.start();
            return;
        }
        this.d.setProgress(f);
    }

    public abstract void c(Drawable drawable, int i);

    public abstract void d(CharSequence charSequence);
}
