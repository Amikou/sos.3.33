package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: r56  reason: default package */
/* loaded from: classes.dex */
public final class r56 extends c55 {
    public r56(w56 w56Var, String str) {
        super("getVersion");
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        return new z45(Double.valueOf((double) Utils.DOUBLE_EPSILON));
    }
}
