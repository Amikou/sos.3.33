package defpackage;

import com.facebook.cache.disk.b;
import com.facebook.cache.disk.c;
import com.facebook.cache.disk.f;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* compiled from: DiskStorageCacheFactory.java */
/* renamed from: ip0  reason: default package */
/* loaded from: classes.dex */
public class ip0 implements l31 {
    public jp0 a;

    public ip0(jp0 jp0Var) {
        this.a = jp0Var;
    }

    public static c b(ap0 ap0Var, b bVar) {
        return c(ap0Var, bVar, Executors.newSingleThreadExecutor());
    }

    public static c c(ap0 ap0Var, b bVar, Executor executor) {
        return new c(bVar, ap0Var.h(), new c.C0083c(ap0Var.k(), ap0Var.j(), ap0Var.f()), ap0Var.e(), ap0Var.d(), ap0Var.g(), executor, ap0Var.i());
    }

    @Override // defpackage.l31
    public f a(ap0 ap0Var) {
        return b(ap0Var, this.a.a(ap0Var));
    }
}
