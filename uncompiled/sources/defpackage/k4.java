package defpackage;

import java.io.IOException;
import org.bouncycastle.asn1.b1;
import org.bouncycastle.asn1.d1;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.s0;

/* renamed from: k4  reason: default package */
/* loaded from: classes2.dex */
public abstract class k4 extends k implements d1 {
    public int a;
    public boolean f0 = false;
    public boolean g0;
    public c4 h0;

    public k4(boolean z, int i, c4 c4Var) {
        this.g0 = true;
        this.h0 = null;
        if (c4Var instanceof b4) {
            this.g0 = true;
        } else {
            this.g0 = z;
        }
        this.a = i;
        if (!this.g0) {
            boolean z2 = c4Var.i() instanceof j4;
        }
        this.h0 = c4Var;
    }

    public static k4 z(Object obj) {
        if (obj == null || (obj instanceof k4)) {
            return (k4) obj;
        }
        if (!(obj instanceof byte[])) {
            throw new IllegalArgumentException("unknown object in getInstance: " + obj.getClass().getName());
        }
        try {
            return z(k.s((byte[]) obj));
        } catch (IOException e) {
            throw new IllegalArgumentException("failed to construct tagged object from byte[]: " + e.getMessage());
        }
    }

    public k B() {
        c4 c4Var = this.h0;
        if (c4Var != null) {
            return c4Var.i();
        }
        return null;
    }

    public int D() {
        return this.a;
    }

    public boolean E() {
        return this.g0;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        int i = this.a;
        c4 c4Var = this.h0;
        return c4Var != null ? i ^ c4Var.hashCode() : i;
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() {
        return i();
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof k4) {
            k4 k4Var = (k4) kVar;
            if (this.a == k4Var.a && this.f0 == k4Var.f0 && this.g0 == k4Var.g0) {
                c4 c4Var = this.h0;
                return c4Var == null ? k4Var.h0 == null : c4Var.i().equals(k4Var.h0.i());
            }
            return false;
        }
        return false;
    }

    public String toString() {
        return "[" + this.a + "]" + this.h0;
    }

    @Override // org.bouncycastle.asn1.k
    public k w() {
        return new s0(this.g0, this.a, this.h0);
    }

    @Override // org.bouncycastle.asn1.k
    public k y() {
        return new b1(this.g0, this.a, this.h0);
    }
}
