package defpackage;

import com.google.android.gms.internal.measurement.zzjd;
import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: rr5  reason: default package */
/* loaded from: classes.dex */
public final class rr5 implements Comparator<zzjd> {
    @Override // java.util.Comparator
    public final /* bridge */ /* synthetic */ int compare(zzjd zzjdVar, zzjd zzjdVar2) {
        zzjd zzjdVar3 = zzjdVar;
        zzjd zzjdVar4 = zzjdVar2;
        or5 or5Var = new or5(zzjdVar3);
        or5 or5Var2 = new or5(zzjdVar4);
        while (or5Var.hasNext() && or5Var2.hasNext()) {
            int compare = Integer.compare(or5Var.zza() & 255, or5Var2.zza() & 255);
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzjdVar3.zzc(), zzjdVar4.zzc());
    }
}
