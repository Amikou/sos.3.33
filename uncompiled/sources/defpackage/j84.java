package defpackage;

import android.content.Context;

/* compiled from: Trackers.java */
/* renamed from: j84  reason: default package */
/* loaded from: classes.dex */
public class j84 {
    public static j84 e;
    public oo a;
    public qo b;
    public df2 c;
    public yt3 d;

    public j84(Context context, q34 q34Var) {
        Context applicationContext = context.getApplicationContext();
        this.a = new oo(applicationContext, q34Var);
        this.b = new qo(applicationContext, q34Var);
        this.c = new df2(applicationContext, q34Var);
        this.d = new yt3(applicationContext, q34Var);
    }

    public static synchronized j84 c(Context context, q34 q34Var) {
        j84 j84Var;
        synchronized (j84.class) {
            if (e == null) {
                e = new j84(context, q34Var);
            }
            j84Var = e;
        }
        return j84Var;
    }

    public oo a() {
        return this.a;
    }

    public qo b() {
        return this.b;
    }

    public df2 d() {
        return this.c;
    }

    public yt3 e() {
        return this.d;
    }
}
