package defpackage;

import android.os.RemoteException;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: yq5  reason: default package */
/* loaded from: classes.dex */
public final class yq5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ p f0;

    public yq5(p pVar, zzp zzpVar) {
        this.f0 = pVar;
        this.a = zzpVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d dVar;
        dVar = this.f0.d;
        if (dVar == null) {
            this.f0.a.w().l().a("Failed to reset data on the service: not connected to service");
            return;
        }
        try {
            zt2.j(this.a);
            dVar.e0(this.a);
        } catch (RemoteException e) {
            this.f0.a.w().l().b("Failed to reset data on the service: remote exception", e);
        }
        this.f0.D();
    }
}
