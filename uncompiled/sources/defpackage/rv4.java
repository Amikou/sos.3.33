package defpackage;

import java.util.List;

/* renamed from: rv4  reason: default package */
/* loaded from: classes2.dex */
public final class rv4 {
    public final String a;
    public final long b;
    public int c;
    public final long d;
    public final List<vv4> e;

    public rv4(String str, long j, int i, long j2, List<vv4> list) {
        this.a = str;
        this.b = j;
        this.c = i;
        this.d = j2;
        this.e = list;
    }
}
