package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.e0;
import com.google.crypto.tink.proto.f0;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: XChaCha20Poly1305KeyManager.java */
/* renamed from: yr4  reason: default package */
/* loaded from: classes2.dex */
public class yr4 extends g<e0> {

    /* compiled from: XChaCha20Poly1305KeyManager.java */
    /* renamed from: yr4$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, e0> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(e0 e0Var) throws GeneralSecurityException {
            return new xr4(e0Var.G().toByteArray());
        }
    }

    /* compiled from: XChaCha20Poly1305KeyManager.java */
    /* renamed from: yr4$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<f0, e0> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public e0 a(f0 f0Var) throws GeneralSecurityException {
            return e0.I().t(yr4.this.j()).s(ByteString.copyFrom(p33.c(32))).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public f0 c(ByteString byteString) throws InvalidProtocolBufferException {
            return f0.D(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(f0 f0Var) throws GeneralSecurityException {
        }
    }

    public yr4() {
        super(e0.class, new a(com.google.crypto.tink.a.class));
    }

    public static void l(boolean z) throws GeneralSecurityException {
        q.q(new yr4(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.XChaCha20Poly1305Key";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, e0> e() {
        return new b(f0.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int j() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: k */
    public e0 g(ByteString byteString) throws InvalidProtocolBufferException {
        return e0.J(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(e0 e0Var) throws GeneralSecurityException {
        ug4.c(e0Var.H(), j());
        if (e0Var.G().size() != 32) {
            throw new GeneralSecurityException("invalid XChaCha20Poly1305Key: incorrect key length");
        }
    }
}
