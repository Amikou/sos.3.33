package defpackage;

import androidx.paging.LoadType;

/* renamed from: zo2  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class zo2 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;
    public static final /* synthetic */ int[] c;
    public static final /* synthetic */ int[] d;
    public static final /* synthetic */ int[] e;

    static {
        int[] iArr = new int[LoadType.values().length];
        a = iArr;
        LoadType loadType = LoadType.REFRESH;
        iArr[loadType.ordinal()] = 1;
        int[] iArr2 = new int[LoadType.values().length];
        b = iArr2;
        iArr2[loadType.ordinal()] = 1;
        int[] iArr3 = new int[LoadType.values().length];
        c = iArr3;
        LoadType loadType2 = LoadType.PREPEND;
        iArr3[loadType2.ordinal()] = 1;
        LoadType loadType3 = LoadType.APPEND;
        iArr3[loadType3.ordinal()] = 2;
        iArr3[loadType.ordinal()] = 3;
        int[] iArr4 = new int[LoadType.values().length];
        d = iArr4;
        iArr4[loadType2.ordinal()] = 1;
        iArr4[loadType3.ordinal()] = 2;
        int[] iArr5 = new int[LoadType.values().length];
        e = iArr5;
        iArr5[loadType2.ordinal()] = 1;
    }
}
