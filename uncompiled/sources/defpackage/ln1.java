package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.crashlytics.internal.common.j;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;

/* compiled from: IdManager.java */
/* renamed from: ln1  reason: default package */
/* loaded from: classes2.dex */
public class ln1 implements dr1 {
    public static final Pattern g = Pattern.compile("[^\\p{Alnum}]");
    public static final String h = Pattern.quote("/");
    public final er1 a;
    public final Context b;
    public final String c;
    public final k51 d;
    public final be0 e;
    public String f;

    public ln1(Context context, String str, k51 k51Var, be0 be0Var) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        }
        if (str != null) {
            this.b = context;
            this.c = str;
            this.d = k51Var;
            this.e = be0Var;
            this.a = new er1();
            return;
        }
        throw new IllegalArgumentException("appIdentifier must not be null");
    }

    public static String c() {
        return "SYN_" + UUID.randomUUID().toString();
    }

    public static String e(String str) {
        if (str == null) {
            return null;
        }
        return g.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    public static boolean k(String str) {
        return str != null && str.startsWith("SYN_");
    }

    @Override // defpackage.dr1
    public synchronized String a() {
        String str = this.f;
        if (str != null) {
            return str;
        }
        w12.f().i("Determining Crashlytics installation ID...");
        SharedPreferences r = CommonUtils.r(this.b);
        String string = r.getString("firebase.installation.id", null);
        w12 f = w12.f();
        f.i("Cached Firebase Installation ID: " + string);
        if (this.e.d()) {
            String d = d();
            w12 f2 = w12.f();
            f2.i("Fetched Firebase Installation ID: " + d);
            if (d == null) {
                d = string == null ? c() : string;
            }
            if (d.equals(string)) {
                this.f = l(r);
            } else {
                this.f = b(d, r);
            }
        } else if (k(string)) {
            this.f = l(r);
        } else {
            this.f = b(c(), r);
        }
        if (this.f == null) {
            w12.f().k("Unable to determine Crashlytics Install Id, creating a new one.");
            this.f = b(c(), r);
        }
        w12 f3 = w12.f();
        f3.i("Crashlytics installation ID: " + this.f);
        return this.f;
    }

    public final synchronized String b(String str, SharedPreferences sharedPreferences) {
        String e;
        e = e(UUID.randomUUID().toString());
        w12 f = w12.f();
        f.i("Created new Crashlytics installation ID: " + e + " for FID: " + str);
        sharedPreferences.edit().putString("crashlytics.installation.id", e).putString("firebase.installation.id", str).apply();
        return e;
    }

    public final String d() {
        try {
            return (String) j.b(this.d.getId());
        } catch (Exception e) {
            w12.f().l("Failed to retrieve Firebase Installations ID.", e);
            return null;
        }
    }

    public String f() {
        return this.c;
    }

    public String g() {
        return this.a.a(this.b);
    }

    public String h() {
        return String.format(Locale.US, "%s/%s", m(Build.MANUFACTURER), m(Build.MODEL));
    }

    public String i() {
        return m(Build.VERSION.INCREMENTAL);
    }

    public String j() {
        return m(Build.VERSION.RELEASE);
    }

    public final String l(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString("crashlytics.installation.id", null);
    }

    public final String m(String str) {
        return str.replaceAll(h, "");
    }
}
