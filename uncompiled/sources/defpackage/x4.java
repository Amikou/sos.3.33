package defpackage;

import java.math.BigInteger;

/* renamed from: x4  reason: default package */
/* loaded from: classes2.dex */
public abstract class x4 implements it0 {
    @Override // defpackage.it0
    public pt0 a(pt0 pt0Var, BigInteger bigInteger) {
        int signum = bigInteger.signum();
        if (signum == 0 || pt0Var.u()) {
            return pt0Var.i().v();
        }
        pt0 c = c(pt0Var, bigInteger.abs());
        if (signum <= 0) {
            c = c.z();
        }
        return b(c);
    }

    public pt0 b(pt0 pt0Var) {
        return vs0.a(pt0Var);
    }

    public abstract pt0 c(pt0 pt0Var, BigInteger bigInteger);
}
