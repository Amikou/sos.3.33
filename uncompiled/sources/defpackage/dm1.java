package defpackage;

import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: IGetIsUserHasTokenUseCase.kt */
/* renamed from: dm1  reason: default package */
/* loaded from: classes2.dex */
public interface dm1 {
    Object a(IToken iToken, TokenType tokenType, q70<? super Boolean> q70Var);
}
