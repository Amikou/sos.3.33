package defpackage;

import java.math.BigInteger;

/* compiled from: StaticGasProvider.java */
/* renamed from: it3  reason: default package */
/* loaded from: classes3.dex */
public class it3 implements j80 {
    private BigInteger gasLimit;
    private BigInteger gasPrice;

    public it3(BigInteger bigInteger, BigInteger bigInteger2) {
        this.gasPrice = bigInteger;
        this.gasLimit = bigInteger2;
    }

    @Override // defpackage.j80
    public BigInteger getGasLimit(String str) {
        return this.gasLimit;
    }

    @Override // defpackage.j80
    public BigInteger getGasPrice(String str) {
        return this.gasPrice;
    }

    @Override // defpackage.j80
    public BigInteger getGasLimit() {
        return this.gasLimit;
    }

    @Override // defpackage.j80
    public BigInteger getGasPrice() {
        return this.gasPrice;
    }
}
