package defpackage;

import java8.util.stream.MatchOps;

/* renamed from: g42  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class g42 implements ew3 {
    public final MatchOps.MatchKind a;
    public final fu2 b;

    public g42(MatchOps.MatchKind matchKind, fu2 fu2Var) {
        this.a = matchKind;
        this.b = fu2Var;
    }

    public static ew3 a(MatchOps.MatchKind matchKind, fu2 fu2Var) {
        return new g42(matchKind, fu2Var);
    }

    @Override // defpackage.ew3
    public Object get() {
        return MatchOps.a(this.a, this.b);
    }
}
