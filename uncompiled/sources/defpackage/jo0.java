package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogUnlinkConfirmationBinding.java */
/* renamed from: jo0  reason: default package */
/* loaded from: classes2.dex */
public final class jo0 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialTextView d;
    public final MaterialTextView e;

    public jo0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialTextView;
        this.e = materialTextView2;
    }

    public static jo0 a(View view) {
        int i = R.id.btnNegative;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnNegative);
        if (materialButton != null) {
            i = R.id.btnPositive;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPositive);
            if (materialButton2 != null) {
                i = R.id.tvDialogContent;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tvDialogContent);
                if (materialTextView != null) {
                    i = R.id.tvDialogTitle;
                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.tvDialogTitle);
                    if (materialTextView2 != null) {
                        return new jo0((ConstraintLayout) view, materialButton, materialButton2, materialTextView, materialTextView2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
