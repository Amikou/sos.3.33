package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import com.google.common.collect.ImmutableList;
import defpackage.fu3;
import java.util.Arrays;
import java.util.List;

/* compiled from: OpusReader.java */
/* renamed from: wn2  reason: default package */
/* loaded from: classes.dex */
public final class wn2 extends fu3 {
    public static final byte[] o = {79, 112, 117, 115, 72, 101, 97, 100};
    public static final byte[] p = {79, 112, 117, 115, 84, 97, 103, 115};
    public boolean n;

    public static boolean o(op2 op2Var, byte[] bArr) {
        if (op2Var.a() < bArr.length) {
            return false;
        }
        int e = op2Var.e();
        byte[] bArr2 = new byte[bArr.length];
        op2Var.j(bArr2, 0, bArr.length);
        op2Var.P(e);
        return Arrays.equals(bArr2, bArr);
    }

    public static boolean p(op2 op2Var) {
        return o(op2Var, o);
    }

    @Override // defpackage.fu3
    public long f(op2 op2Var) {
        return c(n(op2Var.d()));
    }

    @Override // defpackage.fu3
    public boolean i(op2 op2Var, long j, fu3.b bVar) throws ParserException {
        if (o(op2Var, o)) {
            byte[] copyOf = Arrays.copyOf(op2Var.d(), op2Var.f());
            int c = xn2.c(copyOf);
            List<byte[]> a = xn2.a(copyOf);
            if (bVar.a != null) {
                return true;
            }
            bVar.a = new j.b().e0("audio/opus").H(c).f0(48000).T(a).E();
            return true;
        }
        byte[] bArr = p;
        if (o(op2Var, bArr)) {
            ii.i(bVar.a);
            if (this.n) {
                return true;
            }
            this.n = true;
            op2Var.Q(bArr.length);
            Metadata c2 = dl4.c(ImmutableList.copyOf(dl4.j(op2Var, false, false).a));
            if (c2 == null) {
                return true;
            }
            bVar.a = bVar.a.b().X(c2.b(bVar.a.n0)).E();
            return true;
        }
        ii.i(bVar.a);
        return false;
    }

    @Override // defpackage.fu3
    public void l(boolean z) {
        super.l(z);
        if (z) {
            this.n = false;
        }
    }

    public final long n(byte[] bArr) {
        int i;
        int i2 = bArr[0] & 255;
        int i3 = i2 & 3;
        int i4 = 2;
        if (i3 == 0) {
            i4 = 1;
        } else if (i3 != 1 && i3 != 2) {
            i4 = bArr[1] & 63;
        }
        int i5 = i2 >> 3;
        return i4 * (i5 >= 16 ? 2500 << i : i5 >= 12 ? 10000 << (i & 1) : (i5 & 3) == 3 ? 60000 : 10000 << i);
    }
}
