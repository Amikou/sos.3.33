package defpackage;

/* renamed from: h52  reason: default package */
/* loaded from: classes2.dex */
public class h52 extends f52 {
    public int f0;
    public int g0;
    public je1 h0;
    public rs2 i0;
    public mq2 j0;
    public he1 k0;
    public rs2[] l0;

    public h52(int i, int i2, je1 je1Var, rs2 rs2Var, he1 he1Var, mq2 mq2Var, String str) {
        super(true, str);
        this.f0 = i;
        this.g0 = i2;
        this.h0 = je1Var;
        this.i0 = rs2Var;
        this.k0 = he1Var;
        this.j0 = mq2Var;
        this.l0 = new ts2(je1Var, rs2Var).c();
    }

    public h52(int i, int i2, je1 je1Var, rs2 rs2Var, mq2 mq2Var, String str) {
        this(i, i2, je1Var, rs2Var, sh1.a(je1Var, rs2Var), mq2Var, str);
    }

    public je1 b() {
        return this.h0;
    }

    public rs2 c() {
        return this.i0;
    }

    public he1 d() {
        return this.k0;
    }

    public int e() {
        return this.g0;
    }

    public int f() {
        return this.f0;
    }

    public mq2 g() {
        return this.j0;
    }

    public rs2[] h() {
        return this.l0;
    }
}
