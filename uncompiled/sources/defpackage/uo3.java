package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: uo3  reason: default package */
/* loaded from: classes.dex */
public final class uo3 implements a.d {
    @RecentlyNonNull
    public static final uo3 a;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: uo3$a */
    /* loaded from: classes.dex */
    public static final class a {
    }

    static {
        new a();
        a = new uo3(false, false, null, false, null, null, false, null, null);
    }

    public uo3(boolean z, boolean z2, String str, boolean z3, String str2, String str3, boolean z4, Long l, Long l2) {
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof uo3) {
            uo3 uo3Var = (uo3) obj;
            return pl2.a(null, null) && pl2.a(null, null) && pl2.a(null, null) && pl2.a(null, null) && pl2.a(null, null);
        }
        return false;
    }

    @RecentlyNonNull
    public final int hashCode() {
        Boolean bool = Boolean.FALSE;
        return pl2.b(bool, bool, null, bool, bool, null, null, null, null);
    }
}
