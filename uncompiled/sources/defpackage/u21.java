package defpackage;

import java.util.Map;
import kotlin.Pair;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;

/* compiled from: FiatData.kt */
/* renamed from: u21  reason: default package */
/* loaded from: classes2.dex */
public final class u21 {
    public static final a a = new a(null);
    public static final Map<String, String> b = z32.e(new Pair("USD", "$"), new Pair("EUR", "€"), new Pair("JPY", "¥"), new Pair("GBP", "£"));
    public static Fiat c = Fiat.Companion.getDEFAULT_CURRENCY();

    /* compiled from: FiatData.kt */
    /* renamed from: u21$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final Fiat a() {
            return u21.c;
        }

        public final String b() {
            String str = d().get(a().getSymbol());
            return str == null ? a().getSymbol() : str;
        }

        public final String c(String str) {
            fs1.f(str, "symbol");
            String str2 = d().get(str);
            return str2 == null ? str : str2;
        }

        public final Map<String, String> d() {
            return u21.b;
        }

        public final void e(Fiat fiat) {
            fs1.f(fiat, "<set-?>");
            u21.c = fiat;
        }
    }
}
