package defpackage;

import com.google.android.play.core.assetpacks.c;
import java.util.concurrent.Executor;

/* renamed from: dw4  reason: default package */
/* loaded from: classes2.dex */
public final class dw4 implements jw4<zv4> {
    public final jw4 a;
    public final jw4 b;
    public final jw4 c;
    public final jw4 d;
    public final /* synthetic */ int e = 0;

    public dw4(jw4<c> jw4Var, jw4<zy4> jw4Var2, jw4<fv4> jw4Var3, jw4<Executor> jw4Var4) {
        this.a = jw4Var;
        this.b = jw4Var2;
        this.c = jw4Var3;
        this.d = jw4Var4;
    }

    public dw4(jw4<c> jw4Var, jw4<zy4> jw4Var2, jw4<au4> jw4Var3, jw4<fv4> jw4Var4, byte[] bArr) {
        this.a = jw4Var;
        this.b = jw4Var2;
        this.c = jw4Var3;
        this.d = jw4Var4;
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [cv4, zv4] */
    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ zv4 a() {
        if (this.e != 0) {
            return new cv4((c) this.a.a(), gw4.c(this.b), gw4.c(this.c), (fv4) this.d.a());
        }
        Object a = this.a.a();
        return new zv4((c) a, gw4.c(this.b), (fv4) this.c.a(), gw4.c(this.d));
    }
}
