package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityEnterPasswordBinding.java */
/* renamed from: c7  reason: default package */
/* loaded from: classes2.dex */
public final class c7 {
    public final ConstraintLayout a;
    public final AppCompatImageView b;
    public final SwitchMaterial c;
    public final TextInputLayout d;
    public final TextView e;
    public final TextView f;

    public c7(ConstraintLayout constraintLayout, AppCompatImageView appCompatImageView, LinearLayoutCompat linearLayoutCompat, bq1 bq1Var, SwitchMaterial switchMaterial, TextInputLayout textInputLayout, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view) {
        this.a = constraintLayout;
        this.b = appCompatImageView;
        this.c = switchMaterial;
        this.d = textInputLayout;
        this.e = textView;
        this.f = textView4;
    }

    public static c7 a(View view) {
        int i = R.id.btnBiometrics;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnBiometrics);
        if (appCompatImageView != null) {
            i = R.id.layoutPassword;
            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.layoutPassword);
            if (linearLayoutCompat != null) {
                i = R.id.layoutSafeMoonBrand;
                View a = ai4.a(view, R.id.layoutSafeMoonBrand);
                if (a != null) {
                    bq1 a2 = bq1.a(a);
                    i = R.id.switchTwoLayerSignInBio;
                    SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchTwoLayerSignInBio);
                    if (switchMaterial != null) {
                        i = R.id.tilPassword;
                        TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilPassword);
                        if (textInputLayout != null) {
                            i = R.id.tv_not;
                            TextView textView = (TextView) ai4.a(view, R.id.tv_not);
                            if (textView != null) {
                                i = R.id.tvSignInBioText;
                                TextView textView2 = (TextView) ai4.a(view, R.id.tvSignInBioText);
                                if (textView2 != null) {
                                    i = R.id.tv_sign_in_header;
                                    TextView textView3 = (TextView) ai4.a(view, R.id.tv_sign_in_header);
                                    if (textView3 != null) {
                                        i = R.id.tvWipeData;
                                        TextView textView4 = (TextView) ai4.a(view, R.id.tvWipeData);
                                        if (textView4 != null) {
                                            i = R.id.tv_wipe_notice;
                                            TextView textView5 = (TextView) ai4.a(view, R.id.tv_wipe_notice);
                                            if (textView5 != null) {
                                                i = R.id.viewPasswordDivider;
                                                View a3 = ai4.a(view, R.id.viewPasswordDivider);
                                                if (a3 != null) {
                                                    return new c7((ConstraintLayout) view, appCompatImageView, linearLayoutCompat, a2, switchMaterial, textInputLayout, textView, textView2, textView3, textView4, textView5, a3);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static c7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static c7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_enter_password, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
