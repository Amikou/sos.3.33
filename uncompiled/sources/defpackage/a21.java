package defpackage;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/* compiled from: FactoryPools.java */
/* renamed from: a21  reason: default package */
/* loaded from: classes.dex */
public final class a21 {
    public static final g<Object> a = new a();

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$a */
    /* loaded from: classes.dex */
    public class a implements g<Object> {
        @Override // defpackage.a21.g
        public void a(Object obj) {
        }
    }

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$b */
    /* loaded from: classes.dex */
    public class b implements d<List<T>> {
        @Override // defpackage.a21.d
        /* renamed from: b */
        public List<T> a() {
            return new ArrayList();
        }
    }

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$c */
    /* loaded from: classes.dex */
    public class c implements g<List<T>> {
        @Override // defpackage.a21.g
        /* renamed from: b */
        public void a(List<T> list) {
            list.clear();
        }
    }

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$d */
    /* loaded from: classes.dex */
    public interface d<T> {
        T a();
    }

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$e */
    /* loaded from: classes.dex */
    public static final class e<T> implements et2<T> {
        public final d<T> a;
        public final g<T> b;
        public final et2<T> c;

        public e(et2<T> et2Var, d<T> dVar, g<T> gVar) {
            this.c = et2Var;
            this.a = dVar;
            this.b = gVar;
        }

        @Override // defpackage.et2
        public boolean a(T t) {
            if (t instanceof f) {
                ((f) t).g().b(true);
            }
            this.b.a(t);
            return this.c.a(t);
        }

        @Override // defpackage.et2
        public T b() {
            T b = this.c.b();
            if (b == null) {
                b = this.a.a();
                if (Log.isLoggable("FactoryPools", 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Created new ");
                    sb.append(b.getClass());
                }
            }
            if (b instanceof f) {
                ((f) b).g().b(false);
            }
            return b;
        }
    }

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$f */
    /* loaded from: classes.dex */
    public interface f {
        et3 g();
    }

    /* compiled from: FactoryPools.java */
    /* renamed from: a21$g */
    /* loaded from: classes.dex */
    public interface g<T> {
        void a(T t);
    }

    public static <T extends f> et2<T> a(et2<T> et2Var, d<T> dVar) {
        return b(et2Var, dVar, c());
    }

    public static <T> et2<T> b(et2<T> et2Var, d<T> dVar, g<T> gVar) {
        return new e(et2Var, dVar, gVar);
    }

    public static <T> g<T> c() {
        return (g<T>) a;
    }

    public static <T extends f> et2<T> d(int i, d<T> dVar) {
        return a(new it2(i), dVar);
    }

    public static <T> et2<List<T>> e() {
        return f(20);
    }

    public static <T> et2<List<T>> f(int i) {
        return b(new it2(i), new b(), new c());
    }
}
