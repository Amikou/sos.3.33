package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: b26  reason: default package */
/* loaded from: classes.dex */
public final class b26 implements a26 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.ga.ga_app_id", false);

    @Override // defpackage.a26
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.a26
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
