package defpackage;

import java.net.IDN;
import java.util.Arrays;
import org.web3j.ens.EnsResolutionException;

/* compiled from: NameHash.java */
/* renamed from: xc2  reason: default package */
/* loaded from: classes3.dex */
public class xc2 {
    private static final byte[] EMPTY = new byte[32];

    public static String nameHash(String str) {
        return ej2.toHexString(nameHash(normalise(str).split("\\.")));
    }

    public static byte[] nameHashAsBytes(String str) {
        return ej2.hexStringToByteArray(nameHash(str));
    }

    public static String normalise(String str) {
        try {
            return IDN.toASCII(str, 2).toLowerCase();
        } catch (IllegalArgumentException unused) {
            throw new EnsResolutionException("Invalid ENS name provided: " + str);
        }
    }

    private static byte[] nameHash(String[] strArr) {
        if (strArr.length != 0 && !strArr[0].equals("")) {
            byte[] copyOf = Arrays.copyOf(nameHash(strArr.length == 1 ? new String[0] : (String[]) Arrays.copyOfRange(strArr, 1, strArr.length)), 64);
            byte[] sha3 = ak1.sha3(strArr[0].getBytes(m30.UTF_8));
            System.arraycopy(sha3, 0, copyOf, 32, sha3.length);
            return ak1.sha3(copyOf);
        }
        return EMPTY;
    }
}
