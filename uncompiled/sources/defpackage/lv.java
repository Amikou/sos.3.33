package defpackage;

import android.graphics.Typeface;

/* compiled from: CancelableFontCallback.java */
/* renamed from: lv  reason: default package */
/* loaded from: classes2.dex */
public final class lv extends f44 {
    public final Typeface a;
    public final a b;
    public boolean c;

    /* compiled from: CancelableFontCallback.java */
    /* renamed from: lv$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(Typeface typeface);
    }

    public lv(a aVar, Typeface typeface) {
        this.a = typeface;
        this.b = aVar;
    }

    @Override // defpackage.f44
    public void a(int i) {
        d(this.a);
    }

    @Override // defpackage.f44
    public void b(Typeface typeface, boolean z) {
        d(typeface);
    }

    public void c() {
        this.c = true;
    }

    public final void d(Typeface typeface) {
        if (this.c) {
            return;
        }
        this.b.a(typeface);
    }
}
