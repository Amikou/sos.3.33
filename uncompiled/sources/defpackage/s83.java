package defpackage;

import kotlin.Result;

/* compiled from: JobSupport.kt */
/* renamed from: s83  reason: default package */
/* loaded from: classes2.dex */
public final class s83 extends au1 {
    public final q70<te4> i0;

    /* JADX WARN: Multi-variable type inference failed */
    public s83(q70<? super te4> q70Var) {
        this.i0 = q70Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        y(th);
        return te4.a;
    }

    @Override // defpackage.v30
    public void y(Throwable th) {
        q70<te4> q70Var = this.i0;
        te4 te4Var = te4.a;
        Result.a aVar = Result.Companion;
        q70Var.resumeWith(Result.m52constructorimpl(te4Var));
    }
}
