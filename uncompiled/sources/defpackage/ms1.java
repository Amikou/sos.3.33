package defpackage;

/* compiled from: CancellableContinuationImpl.kt */
/* renamed from: ms1  reason: default package */
/* loaded from: classes2.dex */
public final class ms1 extends iv {
    public final tc1<Throwable, te4> a;

    /* JADX WARN: Multi-variable type inference failed */
    public ms1(tc1<? super Throwable, te4> tc1Var) {
        this.a = tc1Var;
    }

    @Override // defpackage.jv
    public void a(Throwable th) {
        this.a.invoke(th);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        a(th);
        return te4.a;
    }

    public String toString() {
        return "InvokeOnCancel[" + ff0.a(this.a) + '@' + ff0.b(this) + ']';
    }
}
