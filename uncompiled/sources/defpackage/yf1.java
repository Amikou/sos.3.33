package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.a;
import java.security.MessageDigest;

/* compiled from: GifDrawableTransformation.java */
/* renamed from: yf1  reason: default package */
/* loaded from: classes.dex */
public class yf1 implements za4<uf1> {
    public final za4<Bitmap> b;

    public yf1(za4<Bitmap> za4Var) {
        this.b = (za4) wt2.d(za4Var);
    }

    @Override // defpackage.za4
    public s73<uf1> a(Context context, s73<uf1> s73Var, int i, int i2) {
        uf1 uf1Var = s73Var.get();
        s73<Bitmap> oqVar = new oq(uf1Var.e(), a.c(context).f());
        s73<Bitmap> a = this.b.a(context, oqVar, i, i2);
        if (!oqVar.equals(a)) {
            oqVar.b();
        }
        uf1Var.m(this.b, a.get());
        return s73Var;
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        this.b.b(messageDigest);
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof yf1) {
            return this.b.equals(((yf1) obj).b);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return this.b.hashCode();
    }
}
