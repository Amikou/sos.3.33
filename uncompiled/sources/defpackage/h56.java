package defpackage;

import com.google.android.gms.internal.measurement.r0;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: h56  reason: default package */
/* loaded from: classes.dex */
public final class h56 extends n56 {
    public final r0 g;
    public final /* synthetic */ x56 h;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h56(x56 x56Var, String str, int i, r0 r0Var) {
        super(str, i);
        this.h = x56Var;
        this.g = r0Var;
    }

    @Override // defpackage.n56
    public final int a() {
        return this.g.y();
    }

    @Override // defpackage.n56
    public final boolean b() {
        return false;
    }

    @Override // defpackage.n56
    public final boolean c() {
        return this.g.D();
    }

    /* JADX WARN: Removed duplicated region for block: B:127:0x03ef  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x03f2  */
    /* JADX WARN: Removed duplicated region for block: B:131:0x03fa A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:132:0x03fb  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean k(java.lang.Long r16, java.lang.Long r17, com.google.android.gms.internal.measurement.b1 r18, long r19, defpackage.v55 r21, boolean r22) {
        /*
            Method dump skipped, instructions count: 1133
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.h56.k(java.lang.Long, java.lang.Long, com.google.android.gms.internal.measurement.b1, long, v55, boolean):boolean");
    }
}
