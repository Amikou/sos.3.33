package defpackage;

import android.net.Uri;
import androidx.media3.common.StreamKey;
import androidx.media3.common.util.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: DashManifest.java */
/* renamed from: sd0  reason: default package */
/* loaded from: classes.dex */
public class sd0 implements l41<sd0> {
    public final long a;
    public final long b;
    public final long c;
    public final boolean d;
    public final long e;
    public final long f;
    public final long g;
    public final long h;
    public final ig4 i;
    public final gm3 j;
    public final Uri k;
    public final mv2 l;
    public final List<jq2> m;

    public sd0(long j, long j2, long j3, boolean z, long j4, long j5, long j6, long j7, mv2 mv2Var, ig4 ig4Var, gm3 gm3Var, Uri uri, List<jq2> list) {
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = z;
        this.e = j4;
        this.f = j5;
        this.g = j6;
        this.h = j7;
        this.l = mv2Var;
        this.i = ig4Var;
        this.k = uri;
        this.j = gm3Var;
        this.m = list == null ? Collections.emptyList() : list;
    }

    public static ArrayList<i8> c(List<i8> list, LinkedList<StreamKey> linkedList) {
        StreamKey poll = linkedList.poll();
        int i = poll.a;
        ArrayList<i8> arrayList = new ArrayList<>();
        do {
            int i2 = poll.f0;
            i8 i8Var = list.get(i2);
            List<b73> list2 = i8Var.c;
            ArrayList arrayList2 = new ArrayList();
            do {
                arrayList2.add(list2.get(poll.g0));
                poll = linkedList.poll();
                if (poll.a != i) {
                    break;
                }
            } while (poll.f0 == i2);
            arrayList.add(new i8(i8Var.a, i8Var.b, arrayList2, i8Var.d, i8Var.e, i8Var.f));
        } while (poll.a == i);
        linkedList.addFirst(poll);
        return arrayList;
    }

    @Override // defpackage.l41
    /* renamed from: b */
    public final sd0 a(List<StreamKey> list) {
        long j;
        LinkedList linkedList = new LinkedList(list);
        Collections.sort(linkedList);
        linkedList.add(new StreamKey(-1, -1, -1));
        ArrayList arrayList = new ArrayList();
        long j2 = 0;
        int i = 0;
        while (true) {
            int e = e();
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            if (i >= e) {
                break;
            }
            if (((StreamKey) linkedList.peek()).a != i) {
                long f = f(i);
                if (f != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    j2 += f;
                }
            } else {
                jq2 d = d(i);
                arrayList.add(new jq2(d.a, d.b - j2, c(d.c, linkedList), d.d));
            }
            i++;
        }
        long j3 = this.b;
        if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j = j3 - j2;
        }
        return new sd0(this.a, j, this.c, this.d, this.e, this.f, this.g, this.h, this.l, this.i, this.j, this.k, arrayList);
    }

    public final jq2 d(int i) {
        return this.m.get(i);
    }

    public final int e() {
        return this.m.size();
    }

    public final long f(int i) {
        if (i == this.m.size() - 1) {
            long j = this.b;
            return j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : j - this.m.get(i).b;
        }
        return this.m.get(i + 1).b - this.m.get(i).b;
    }

    public final long g(int i) {
        return b.y0(f(i));
    }
}
