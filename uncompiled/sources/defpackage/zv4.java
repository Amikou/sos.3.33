package defpackage;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.k;
import com.google.android.play.core.internal.l;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: zv4  reason: default package */
/* loaded from: classes2.dex */
public final class zv4 {
    public static final it4 g = new it4("ExtractorSessionStoreView");
    public final c a;
    public final cw4<zy4> b;
    public final fv4 c;
    public final cw4<Executor> d;
    public final Map<Integer, tv4> e = new HashMap();
    public final ReentrantLock f = new ReentrantLock();

    public zv4(c cVar, cw4<zy4> cw4Var, fv4 fv4Var, cw4<Executor> cw4Var2) {
        this.a = cVar;
        this.b = cw4Var;
        this.c = fv4Var;
        this.d = cw4Var2;
    }

    public static String s(Bundle bundle) {
        ArrayList<String> stringArrayList = bundle.getStringArrayList("pack_names");
        if (stringArrayList == null || stringArrayList.isEmpty()) {
            throw new bj("Session without pack received.");
        }
        return stringArrayList.get(0);
    }

    public static <T> List<T> t(List<T> list) {
        return list == null ? Collections.emptyList() : list;
    }

    public final void a() {
        this.f.lock();
    }

    public final void b() {
        this.f.unlock();
    }

    public final Map<Integer, tv4> c() {
        return this.e;
    }

    public final boolean d(Bundle bundle) {
        return ((Boolean) q(new iv4(this, bundle, null))).booleanValue();
    }

    public final boolean e(Bundle bundle) {
        return ((Boolean) q(new iv4(this, bundle))).booleanValue();
    }

    public final void f(final String str, final int i, final long j) {
        q(new xv4(this, str, i, j) { // from class: jv4
            public final zv4 a;
            public final String b;
            public final int c;
            public final long d;

            {
                this.a = this;
                this.b = str;
                this.c = i;
                this.d = j;
            }

            @Override // defpackage.xv4
            public final Object a() {
                this.a.l(this.b, this.c, this.d);
                return null;
            }
        });
    }

    public final void g(int i) {
        q(new kv4(this, i));
    }

    public final /* synthetic */ Map h(List list) {
        int i;
        Map<String, tv4> p = p(list);
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            final tv4 tv4Var = p.get(str);
            if (tv4Var == null) {
                i = 8;
            } else {
                if (k.d(tv4Var.c.c)) {
                    try {
                        tv4Var.c.c = 6;
                        this.d.a().execute(new Runnable(this, tv4Var) { // from class: nv4
                            public final zv4 a;
                            public final tv4 f0;

                            {
                                this.a = this;
                                this.f0 = tv4Var;
                            }

                            @Override // java.lang.Runnable
                            public final void run() {
                                this.a.g(this.f0.a);
                            }
                        });
                        this.c.a(str);
                    } catch (bj unused) {
                        g.d("Session %d with pack %s does not exist, no need to cancel.", Integer.valueOf(tv4Var.a), str);
                    }
                }
                i = tv4Var.c.c;
            }
            hashMap.put(str, Integer.valueOf(i));
        }
        return hashMap;
    }

    public final /* synthetic */ Map i(List list) {
        HashMap hashMap = new HashMap();
        for (tv4 tv4Var : this.e.values()) {
            String str = tv4Var.c.a;
            if (list.contains(str)) {
                tv4 tv4Var2 = (tv4) hashMap.get(str);
                if ((tv4Var2 == null ? -1 : tv4Var2.a) < tv4Var.a) {
                    hashMap.put(str, tv4Var);
                }
            }
        }
        return hashMap;
    }

    public final /* synthetic */ Boolean j(Bundle bundle) {
        int i = bundle.getInt("session_id");
        if (i == 0) {
            return Boolean.TRUE;
        }
        Map<Integer, tv4> map = this.e;
        Integer valueOf = Integer.valueOf(i);
        if (map.containsKey(valueOf)) {
            tv4 tv4Var = this.e.get(valueOf);
            if (tv4Var.c.c == 6) {
                return Boolean.FALSE;
            }
            return Boolean.valueOf(!k.g(tv4Var.c.c, bundle.getInt(l.e("status", s(bundle)))));
        }
        return Boolean.TRUE;
    }

    public final /* synthetic */ Boolean k(Bundle bundle) {
        vv4 vv4Var;
        int i = bundle.getInt("session_id");
        if (i == 0) {
            return Boolean.FALSE;
        }
        Map<Integer, tv4> map = this.e;
        Integer valueOf = Integer.valueOf(i);
        boolean z = true;
        boolean z2 = false;
        if (map.containsKey(valueOf)) {
            tv4 r = r(i);
            int i2 = bundle.getInt(l.e("status", r.c.a));
            if (k.g(r.c.c, i2)) {
                g.a("Found stale update for session %s with status %d.", valueOf, Integer.valueOf(r.c.c));
                rv4 rv4Var = r.c;
                String str = rv4Var.a;
                int i3 = rv4Var.c;
                if (i3 == 4) {
                    this.b.a().d(i, str);
                } else if (i3 == 5) {
                    this.b.a().b(i);
                } else if (i3 == 6) {
                    this.b.a().g(Arrays.asList(str));
                }
            } else {
                r.c.c = i2;
                if (k.e(i2)) {
                    g(i);
                    this.c.a(r.c.a);
                } else {
                    for (vv4 vv4Var2 : r.c.e) {
                        ArrayList parcelableArrayList = bundle.getParcelableArrayList(l.f("chunk_intents", r.c.a, vv4Var2.a));
                        if (parcelableArrayList != null) {
                            for (int i4 = 0; i4 < parcelableArrayList.size(); i4++) {
                                if (parcelableArrayList.get(i4) != null && ((Intent) parcelableArrayList.get(i4)).getData() != null) {
                                    vv4Var2.d.get(i4).a = true;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            String s = s(bundle);
            long j = bundle.getLong(l.e("pack_version", s));
            int i5 = bundle.getInt(l.e("status", s));
            long j2 = bundle.getLong(l.e("total_bytes_to_download", s));
            ArrayList<String> stringArrayList = bundle.getStringArrayList(l.e("slice_ids", s));
            ArrayList arrayList = new ArrayList();
            for (String str2 : t(stringArrayList)) {
                ArrayList parcelableArrayList2 = bundle.getParcelableArrayList(l.f("chunk_intents", s, str2));
                ArrayList arrayList2 = new ArrayList();
                for (Intent intent : t(parcelableArrayList2)) {
                    if (intent == null) {
                        z = z2;
                    }
                    arrayList2.add(new pv4(z));
                    z = true;
                    z2 = false;
                }
                String string = bundle.getString(l.f("uncompressed_hash_sha256", s, str2));
                long j3 = bundle.getLong(l.f("uncompressed_size", s, str2));
                int i6 = bundle.getInt(l.f("patch_format", s, str2), 0);
                if (i6 != 0) {
                    vv4Var = new vv4(str2, string, j3, arrayList2, 0, i6);
                    z2 = false;
                } else {
                    z2 = false;
                    vv4Var = new vv4(str2, string, j3, arrayList2, bundle.getInt(l.f("compression_format", s, str2), 0), 0);
                }
                arrayList.add(vv4Var);
                z = true;
            }
            this.e.put(Integer.valueOf(i), new tv4(i, bundle.getInt("app_version_code"), new rv4(s, j, i5, j2, arrayList)));
        }
        return Boolean.TRUE;
    }

    public final /* synthetic */ void l(String str, int i, long j) {
        tv4 tv4Var = p(Arrays.asList(str)).get(str);
        if (tv4Var == null || k.e(tv4Var.c.c)) {
            g.b(String.format("Could not find pack %s while trying to complete it", str), new Object[0]);
        }
        this.a.b(str, i, j);
        tv4Var.c.c = 4;
    }

    public final /* synthetic */ void m(int i) {
        r(i).c.c = 5;
    }

    public final /* synthetic */ void n(int i) {
        tv4 r = r(i);
        if (!k.e(r.c.c)) {
            throw new bj(String.format("Could not safely delete session %d because it is not in a terminal state.", Integer.valueOf(i)), i);
        }
        c cVar = this.a;
        rv4 rv4Var = r.c;
        cVar.b(rv4Var.a, r.b, rv4Var.b);
        rv4 rv4Var2 = r.c;
        int i2 = rv4Var2.c;
        if (i2 == 5 || i2 == 6) {
            this.a.c(rv4Var2.a, r.b, rv4Var2.b);
        }
    }

    public final void o(int i) {
        q(new kv4(this, i, null));
    }

    public final Map<String, tv4> p(final List<String> list) {
        return (Map) q(new xv4(this, list, null) { // from class: lv4
            public final zv4 a;
            public final List b;
            public final /* synthetic */ int c = 1;

            {
                this.a = this;
                this.b = list;
            }

            @Override // defpackage.xv4
            public final Object a() {
                return this.c != 0 ? this.a.i(this.b) : this.a.h(this.b);
            }
        });
    }

    public final <T> T q(xv4<T> xv4Var) {
        try {
            a();
            return xv4Var.a();
        } finally {
            b();
        }
    }

    public final tv4 r(int i) {
        Map<Integer, tv4> map = this.e;
        Integer valueOf = Integer.valueOf(i);
        tv4 tv4Var = map.get(valueOf);
        if (tv4Var != null) {
            return tv4Var;
        }
        throw new bj(String.format("Could not find session %d while trying to get it", valueOf), i);
    }
}
