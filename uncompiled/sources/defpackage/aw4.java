package defpackage;

/* renamed from: aw4  reason: default package */
/* loaded from: classes2.dex */
public final class aw4 implements Runnable {
    public final /* synthetic */ l34 a;
    public final /* synthetic */ bx4 f0;

    public aw4(bx4 bx4Var, l34 l34Var) {
        this.f0 = bx4Var;
        this.a = l34Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        mm2 mm2Var;
        mm2 mm2Var2;
        obj = this.f0.b;
        synchronized (obj) {
            mm2Var = this.f0.c;
            if (mm2Var != null) {
                mm2Var2 = this.f0.c;
                mm2Var2.b(this.a.c());
            }
        }
    }
}
