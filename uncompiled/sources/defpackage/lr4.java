package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.d;
import com.fasterxml.jackson.core.io.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: WriterBasedJsonGenerator.java */
/* renamed from: lr4  reason: default package */
/* loaded from: classes.dex */
public final class lr4 extends bv1 {
    public static final char[] z0 = a.d();
    public final Writer r0;
    public char s0;
    public char[] t0;
    public int u0;
    public int v0;
    public int w0;
    public char[] x0;
    public yl3 y0;

    public lr4(jm1 jm1Var, int i, c cVar, Writer writer) {
        super(jm1Var, i, cVar);
        this.s0 = '\"';
        this.r0 = writer;
        char[] e = jm1Var.e();
        this.t0 = e;
        this.w0 = e.length;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void B0(BigDecimal bigDecimal) throws IOException {
        z1("write a number");
        if (bigDecimal == null) {
            W1();
        } else if (this.h0) {
            b2(v1(bigDecimal));
        } else {
            W0(v1(bigDecimal));
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void F0(BigInteger bigInteger) throws IOException {
        z1("write a number");
        if (bigInteger == null) {
            W1();
        } else if (this.h0) {
            b2(bigInteger.toString());
        } else {
            W0(bigInteger.toString());
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void H0(short s) throws IOException {
        z1("write a number");
        if (this.h0) {
            c2(s);
            return;
        }
        if (this.v0 + 6 >= this.w0) {
            L1();
        }
        this.v0 = aj2.p(s, this.t0, this.v0);
    }

    public final char[] J1() {
        char[] cArr = {'\\', 0, '\\', 'u', '0', '0', 0, 0, '\\', 'u'};
        this.x0 = cArr;
        return cArr;
    }

    public final void K1(char c, int i) throws IOException, JsonGenerationException {
        String value;
        int i2;
        if (i >= 0) {
            if (this.v0 + 2 > this.w0) {
                L1();
            }
            char[] cArr = this.t0;
            int i3 = this.v0;
            int i4 = i3 + 1;
            this.v0 = i4;
            cArr[i3] = '\\';
            this.v0 = i4 + 1;
            cArr[i4] = (char) i;
        } else if (i != -2) {
            if (this.v0 + 5 >= this.w0) {
                L1();
            }
            int i5 = this.v0;
            char[] cArr2 = this.t0;
            int i6 = i5 + 1;
            cArr2[i5] = '\\';
            int i7 = i6 + 1;
            cArr2[i6] = 'u';
            if (c > 255) {
                int i8 = 255 & (c >> '\b');
                int i9 = i7 + 1;
                char[] cArr3 = z0;
                cArr2[i7] = cArr3[i8 >> 4];
                i2 = i9 + 1;
                cArr2[i9] = cArr3[i8 & 15];
                c = (char) (c & 255);
            } else {
                int i10 = i7 + 1;
                cArr2[i7] = '0';
                i2 = i10 + 1;
                cArr2[i10] = '0';
            }
            int i11 = i2 + 1;
            char[] cArr4 = z0;
            cArr2[i2] = cArr4[c >> 4];
            cArr2[i11] = cArr4[c & 15];
            this.v0 = i11 + 1;
        } else {
            yl3 yl3Var = this.y0;
            if (yl3Var == null) {
                value = this.n0.getEscapeSequence(c).getValue();
            } else {
                value = yl3Var.getValue();
                this.y0 = null;
            }
            int length = value.length();
            if (this.v0 + length > this.w0) {
                L1();
                if (length > this.w0) {
                    this.r0.write(value);
                    return;
                }
            }
            value.getChars(0, length, this.t0, this.v0);
            this.v0 += length;
        }
    }

    public void L1() throws IOException {
        int i = this.v0;
        int i2 = this.u0;
        int i3 = i - i2;
        if (i3 > 0) {
            this.u0 = 0;
            this.v0 = 0;
            this.r0.write(this.t0, i2, i3);
        }
    }

    public final int M1(char[] cArr, int i, int i2, char c, int i3) throws IOException, JsonGenerationException {
        String value;
        int i4;
        if (i3 >= 0) {
            if (i > 1 && i < i2) {
                int i5 = i - 2;
                cArr[i5] = '\\';
                cArr[i5 + 1] = (char) i3;
                return i5;
            }
            char[] cArr2 = this.x0;
            if (cArr2 == null) {
                cArr2 = J1();
            }
            cArr2[1] = (char) i3;
            this.r0.write(cArr2, 0, 2);
            return i;
        } else if (i3 == -2) {
            yl3 yl3Var = this.y0;
            if (yl3Var == null) {
                value = this.n0.getEscapeSequence(c).getValue();
            } else {
                value = yl3Var.getValue();
                this.y0 = null;
            }
            int length = value.length();
            if (i >= length && i < i2) {
                int i6 = i - length;
                value.getChars(0, length, cArr, i6);
                return i6;
            }
            this.r0.write(value);
            return i;
        } else if (i > 5 && i < i2) {
            int i7 = i - 6;
            int i8 = i7 + 1;
            cArr[i7] = '\\';
            int i9 = i8 + 1;
            cArr[i8] = 'u';
            if (c > 255) {
                int i10 = (c >> '\b') & 255;
                int i11 = i9 + 1;
                char[] cArr3 = z0;
                cArr[i9] = cArr3[i10 >> 4];
                i4 = i11 + 1;
                cArr[i11] = cArr3[i10 & 15];
                c = (char) (c & 255);
            } else {
                int i12 = i9 + 1;
                cArr[i9] = '0';
                i4 = i12 + 1;
                cArr[i12] = '0';
            }
            int i13 = i4 + 1;
            char[] cArr4 = z0;
            cArr[i4] = cArr4[c >> 4];
            cArr[i13] = cArr4[c & 15];
            return i13 - 5;
        } else {
            char[] cArr5 = this.x0;
            if (cArr5 == null) {
                cArr5 = J1();
            }
            this.u0 = this.v0;
            if (c > 255) {
                int i14 = (c >> '\b') & 255;
                int i15 = c & 255;
                char[] cArr6 = z0;
                cArr5[10] = cArr6[i14 >> 4];
                cArr5[11] = cArr6[i14 & 15];
                cArr5[12] = cArr6[i15 >> 4];
                cArr5[13] = cArr6[i15 & 15];
                this.r0.write(cArr5, 8, 6);
                return i;
            }
            char[] cArr7 = z0;
            cArr5[6] = cArr7[c >> 4];
            cArr5[7] = cArr7[c & 15];
            this.r0.write(cArr5, 2, 6);
            return i;
        }
    }

    public final void N1(char c, int i) throws IOException, JsonGenerationException {
        String value;
        int i2;
        if (i >= 0) {
            int i3 = this.v0;
            if (i3 >= 2) {
                int i4 = i3 - 2;
                this.u0 = i4;
                char[] cArr = this.t0;
                cArr[i4] = '\\';
                cArr[i4 + 1] = (char) i;
                return;
            }
            char[] cArr2 = this.x0;
            if (cArr2 == null) {
                cArr2 = J1();
            }
            this.u0 = this.v0;
            cArr2[1] = (char) i;
            this.r0.write(cArr2, 0, 2);
        } else if (i != -2) {
            int i5 = this.v0;
            if (i5 >= 6) {
                char[] cArr3 = this.t0;
                int i6 = i5 - 6;
                this.u0 = i6;
                cArr3[i6] = '\\';
                int i7 = i6 + 1;
                cArr3[i7] = 'u';
                if (c > 255) {
                    int i8 = (c >> '\b') & 255;
                    int i9 = i7 + 1;
                    char[] cArr4 = z0;
                    cArr3[i9] = cArr4[i8 >> 4];
                    i2 = i9 + 1;
                    cArr3[i2] = cArr4[i8 & 15];
                    c = (char) (c & 255);
                } else {
                    int i10 = i7 + 1;
                    cArr3[i10] = '0';
                    i2 = i10 + 1;
                    cArr3[i2] = '0';
                }
                int i11 = i2 + 1;
                char[] cArr5 = z0;
                cArr3[i11] = cArr5[c >> 4];
                cArr3[i11 + 1] = cArr5[c & 15];
                return;
            }
            char[] cArr6 = this.x0;
            if (cArr6 == null) {
                cArr6 = J1();
            }
            this.u0 = this.v0;
            if (c > 255) {
                int i12 = (c >> '\b') & 255;
                int i13 = c & 255;
                char[] cArr7 = z0;
                cArr6[10] = cArr7[i12 >> 4];
                cArr6[11] = cArr7[i12 & 15];
                cArr6[12] = cArr7[i13 >> 4];
                cArr6[13] = cArr7[i13 & 15];
                this.r0.write(cArr6, 8, 6);
                return;
            }
            char[] cArr8 = z0;
            cArr6[6] = cArr8[c >> 4];
            cArr6[7] = cArr8[c & 15];
            this.r0.write(cArr6, 2, 6);
        } else {
            yl3 yl3Var = this.y0;
            if (yl3Var == null) {
                value = this.n0.getEscapeSequence(c).getValue();
            } else {
                value = yl3Var.getValue();
                this.y0 = null;
            }
            int length = value.length();
            int i14 = this.v0;
            if (i14 >= length) {
                int i15 = i14 - length;
                this.u0 = i15;
                value.getChars(0, length, this.t0, i15);
                return;
            }
            this.u0 = i14;
            this.r0.write(value);
        }
    }

    public final int O1(InputStream inputStream, byte[] bArr, int i, int i2, int i3) throws IOException {
        int i4 = 0;
        while (i < i2) {
            bArr[i4] = bArr[i];
            i4++;
            i++;
        }
        int min = Math.min(i3, bArr.length);
        do {
            int i5 = min - i4;
            if (i5 == 0) {
                break;
            }
            int read = inputStream.read(bArr, i4, i5);
            if (read < 0) {
                return i4;
            }
            i4 += read;
        } while (i4 < 3);
        return i4;
    }

    public void P1() {
        char[] cArr = this.t0;
        if (cArr != null) {
            this.t0 = null;
            this.k0.p(cArr);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public int Q(Base64Variant base64Variant, InputStream inputStream, int i) throws IOException, JsonGenerationException {
        z1("write a binary value");
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr[i2] = this.s0;
        byte[] d = this.k0.d();
        try {
            if (i < 0) {
                i = Q1(base64Variant, inputStream, d);
            } else {
                int R1 = R1(base64Variant, inputStream, d, i);
                if (R1 > 0) {
                    a("Too few bytes available: missing " + R1 + " bytes (out of " + i + ")");
                }
            }
            this.k0.o(d);
            if (this.v0 >= this.w0) {
                L1();
            }
            char[] cArr2 = this.t0;
            int i3 = this.v0;
            this.v0 = i3 + 1;
            cArr2[i3] = this.s0;
            return i;
        } catch (Throwable th) {
            this.k0.o(d);
            throw th;
        }
    }

    public int Q1(Base64Variant base64Variant, InputStream inputStream, byte[] bArr) throws IOException, JsonGenerationException {
        int i = this.w0 - 6;
        int i2 = 2;
        int maxLineLength = base64Variant.getMaxLineLength() >> 2;
        int i3 = -3;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i4 > i3) {
                i5 = O1(inputStream, bArr, i4, i5, bArr.length);
                if (i5 < 3) {
                    break;
                }
                i3 = i5 - 3;
                i4 = 0;
            }
            if (this.v0 > i) {
                L1();
            }
            int i7 = i4 + 1;
            int i8 = i7 + 1;
            i4 = i8 + 1;
            i6 += 3;
            int encodeBase64Chunk = base64Variant.encodeBase64Chunk((((bArr[i7] & 255) | (bArr[i4] << 8)) << 8) | (bArr[i8] & 255), this.t0, this.v0);
            this.v0 = encodeBase64Chunk;
            maxLineLength--;
            if (maxLineLength <= 0) {
                char[] cArr = this.t0;
                int i9 = encodeBase64Chunk + 1;
                this.v0 = i9;
                cArr[encodeBase64Chunk] = '\\';
                this.v0 = i9 + 1;
                cArr[i9] = 'n';
                maxLineLength = base64Variant.getMaxLineLength() >> 2;
            }
        }
        if (i5 > 0) {
            if (this.v0 > i) {
                L1();
            }
            int i10 = bArr[0] << 16;
            if (1 < i5) {
                i10 |= (bArr[1] & 255) << 8;
            } else {
                i2 = 1;
            }
            int i11 = i6 + i2;
            this.v0 = base64Variant.encodeBase64Partial(i10, i2, this.t0, this.v0);
            return i11;
        }
        return i6;
    }

    public int R1(Base64Variant base64Variant, InputStream inputStream, byte[] bArr, int i) throws IOException, JsonGenerationException {
        int O1;
        int i2 = this.w0 - 6;
        int i3 = 2;
        int maxLineLength = base64Variant.getMaxLineLength() >> 2;
        int i4 = -3;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i <= 2) {
                break;
            }
            if (i5 > i4) {
                i6 = O1(inputStream, bArr, i5, i6, i);
                if (i6 < 3) {
                    i5 = 0;
                    break;
                }
                i4 = i6 - 3;
                i5 = 0;
            }
            if (this.v0 > i2) {
                L1();
            }
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            i5 = i8 + 1;
            i -= 3;
            int encodeBase64Chunk = base64Variant.encodeBase64Chunk((((bArr[i7] & 255) | (bArr[i5] << 8)) << 8) | (bArr[i8] & 255), this.t0, this.v0);
            this.v0 = encodeBase64Chunk;
            maxLineLength--;
            if (maxLineLength <= 0) {
                char[] cArr = this.t0;
                int i9 = encodeBase64Chunk + 1;
                this.v0 = i9;
                cArr[encodeBase64Chunk] = '\\';
                this.v0 = i9 + 1;
                cArr[i9] = 'n';
                maxLineLength = base64Variant.getMaxLineLength() >> 2;
            }
        }
        if (i <= 0 || (O1 = O1(inputStream, bArr, i5, i6, i)) <= 0) {
            return i;
        }
        if (this.v0 > i2) {
            L1();
        }
        int i10 = bArr[0] << 16;
        if (1 < O1) {
            i10 |= (bArr[1] & 255) << 8;
        } else {
            i3 = 1;
        }
        this.v0 = base64Variant.encodeBase64Partial(i10, i3, this.t0, this.v0);
        return i - i3;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void S(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        z1("write a binary value");
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i3 = this.v0;
        this.v0 = i3 + 1;
        cArr[i3] = this.s0;
        S1(base64Variant, bArr, i, i2 + i);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i4 = this.v0;
        this.v0 = i4 + 1;
        cArr2[i4] = this.s0;
    }

    public void S1(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        int i3 = i2 - 3;
        int i4 = this.w0 - 6;
        int maxLineLength = base64Variant.getMaxLineLength() >> 2;
        while (i <= i3) {
            if (this.v0 > i4) {
                L1();
            }
            int i5 = i + 1;
            int i6 = i5 + 1;
            int i7 = i6 + 1;
            int encodeBase64Chunk = base64Variant.encodeBase64Chunk((((bArr[i] << 8) | (bArr[i5] & 255)) << 8) | (bArr[i6] & 255), this.t0, this.v0);
            this.v0 = encodeBase64Chunk;
            maxLineLength--;
            if (maxLineLength <= 0) {
                char[] cArr = this.t0;
                int i8 = encodeBase64Chunk + 1;
                this.v0 = i8;
                cArr[encodeBase64Chunk] = '\\';
                this.v0 = i8 + 1;
                cArr[i8] = 'n';
                maxLineLength = base64Variant.getMaxLineLength() >> 2;
            }
            i = i7;
        }
        int i9 = i2 - i;
        if (i9 > 0) {
            if (this.v0 > i4) {
                L1();
            }
            int i10 = i + 1;
            int i11 = bArr[i] << 16;
            if (i9 == 2) {
                i11 |= (bArr[i10] & 255) << 8;
            }
            this.v0 = base64Variant.encodeBase64Partial(i11, i9, this.t0, this.v0);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void T0(char c) throws IOException {
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = c;
    }

    public void T1(yl3 yl3Var, boolean z) throws IOException {
        if (this.a != null) {
            X1(yl3Var, z);
            return;
        }
        if (this.v0 + 1 >= this.w0) {
            L1();
        }
        if (z) {
            char[] cArr = this.t0;
            int i = this.v0;
            this.v0 = i + 1;
            cArr[i] = ',';
        }
        char[] asQuotedChars = yl3Var.asQuotedChars();
        if (this.p0) {
            X0(asQuotedChars, 0, asQuotedChars.length);
            return;
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        int i3 = i2 + 1;
        this.v0 = i3;
        cArr2[i2] = this.s0;
        int length = asQuotedChars.length;
        if (i3 + length + 1 >= this.w0) {
            X0(asQuotedChars, 0, length);
            if (this.v0 >= this.w0) {
                L1();
            }
            char[] cArr3 = this.t0;
            int i4 = this.v0;
            this.v0 = i4 + 1;
            cArr3[i4] = this.s0;
            return;
        }
        System.arraycopy(asQuotedChars, 0, cArr2, i3, length);
        int i5 = this.v0 + length;
        this.v0 = i5;
        char[] cArr4 = this.t0;
        this.v0 = i5 + 1;
        cArr4[i5] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void U0(yl3 yl3Var) throws IOException {
        W0(yl3Var.getValue());
    }

    public void U1(String str, boolean z) throws IOException {
        if (this.a != null) {
            Y1(str, z);
            return;
        }
        if (this.v0 + 1 >= this.w0) {
            L1();
        }
        if (z) {
            char[] cArr = this.t0;
            int i = this.v0;
            this.v0 = i + 1;
            cArr[i] = ',';
        }
        if (this.p0) {
            g2(str);
            return;
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr2[i2] = this.s0;
        g2(str);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr3 = this.t0;
        int i3 = this.v0;
        this.v0 = i3 + 1;
        cArr3[i3] = this.s0;
    }

    public final void V1(String str) throws IOException {
        L1();
        int length = str.length();
        int i = 0;
        while (true) {
            int i2 = this.w0;
            if (i + i2 > length) {
                i2 = length - i;
            }
            int i3 = i + i2;
            str.getChars(i, i3, this.t0, 0);
            if (this.n0 != null) {
                f2(i2);
            } else {
                int i4 = this.m0;
                if (i4 != 0) {
                    e2(i2, i4);
                } else {
                    d2(i2);
                }
            }
            if (i3 >= length) {
                return;
            }
            i = i3;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void W0(String str) throws IOException {
        int length = str.length();
        int i = this.w0 - this.v0;
        if (i == 0) {
            L1();
            i = this.w0 - this.v0;
        }
        if (i >= length) {
            str.getChars(0, length, this.t0, this.v0);
            this.v0 += length;
            return;
        }
        n2(str);
    }

    public final void W1() throws IOException {
        if (this.v0 + 4 >= this.w0) {
            L1();
        }
        int i = this.v0;
        char[] cArr = this.t0;
        cArr[i] = 'n';
        int i2 = i + 1;
        cArr[i2] = 'u';
        int i3 = i2 + 1;
        cArr[i3] = 'l';
        int i4 = i3 + 1;
        cArr[i4] = 'l';
        this.v0 = i4 + 1;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void X0(char[] cArr, int i, int i2) throws IOException {
        if (i2 < 32) {
            if (i2 > this.w0 - this.v0) {
                L1();
            }
            System.arraycopy(cArr, i, this.t0, this.v0, i2);
            this.v0 += i2;
            return;
        }
        L1();
        this.r0.write(cArr, i, i2);
    }

    public void X1(yl3 yl3Var, boolean z) throws IOException {
        if (z) {
            this.a.writeObjectEntrySeparator(this);
        } else {
            this.a.beforeObjectEntries(this);
        }
        char[] asQuotedChars = yl3Var.asQuotedChars();
        if (this.p0) {
            X0(asQuotedChars, 0, asQuotedChars.length);
            return;
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = this.s0;
        X0(asQuotedChars, 0, asQuotedChars.length);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr2[i2] = this.s0;
    }

    public void Y1(String str, boolean z) throws IOException {
        if (z) {
            this.a.writeObjectEntrySeparator(this);
        } else {
            this.a.beforeObjectEntries(this);
        }
        if (this.p0) {
            g2(str);
            return;
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = this.s0;
        g2(str);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr2[i2] = this.s0;
    }

    public final void Z1(int i) throws IOException {
        if (this.v0 + 13 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i2 = this.v0;
        int i3 = i2 + 1;
        this.v0 = i3;
        cArr[i2] = this.s0;
        int p = aj2.p(i, cArr, i3);
        this.v0 = p;
        char[] cArr2 = this.t0;
        this.v0 = p + 1;
        cArr2[p] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void a0(boolean z) throws IOException {
        int i;
        z1("write a boolean value");
        if (this.v0 + 5 >= this.w0) {
            L1();
        }
        int i2 = this.v0;
        char[] cArr = this.t0;
        if (z) {
            cArr[i2] = 't';
            int i3 = i2 + 1;
            cArr[i3] = 'r';
            int i4 = i3 + 1;
            cArr[i4] = 'u';
            i = i4 + 1;
            cArr[i] = 'e';
        } else {
            cArr[i2] = 'f';
            int i5 = i2 + 1;
            cArr[i5] = 'a';
            int i6 = i5 + 1;
            cArr[i6] = 'l';
            int i7 = i6 + 1;
            cArr[i7] = 's';
            i = i7 + 1;
            cArr[i] = 'e';
        }
        this.v0 = i + 1;
    }

    public final void a2(long j) throws IOException {
        if (this.v0 + 23 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        int i2 = i + 1;
        this.v0 = i2;
        cArr[i] = this.s0;
        int r = aj2.r(j, cArr, i2);
        this.v0 = r;
        char[] cArr2 = this.t0;
        this.v0 = r + 1;
        cArr2[r] = this.s0;
    }

    public final void b2(String str) throws IOException {
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = this.s0;
        W0(str);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr2[i2] = this.s0;
    }

    public final void c2(short s) throws IOException {
        if (this.v0 + 8 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        int i2 = i + 1;
        this.v0 = i2;
        cArr[i] = this.s0;
        int p = aj2.p(s, cArr, i2);
        this.v0 = p;
        char[] cArr2 = this.t0;
        this.v0 = p + 1;
        cArr2[p] = this.s0;
    }

    @Override // defpackage.we1, com.fasterxml.jackson.core.JsonGenerator, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        super.close();
        if (this.t0 != null && F1(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT)) {
            while (true) {
                cw1 l = l();
                if (l.e()) {
                    e0();
                } else if (!l.f()) {
                    break;
                } else {
                    f0();
                }
            }
        }
        L1();
        this.u0 = 0;
        this.v0 = 0;
        if (this.r0 != null) {
            if (!this.k0.n() && !F1(JsonGenerator.Feature.AUTO_CLOSE_TARGET)) {
                if (F1(JsonGenerator.Feature.FLUSH_PASSED_TO_STREAM)) {
                    this.r0.flush();
                }
            } else {
                this.r0.close();
            }
        }
        P1();
    }

    public final void d2(int i) throws IOException {
        char[] cArr;
        char c;
        int[] iArr = this.l0;
        int length = iArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < i) {
            do {
                cArr = this.t0;
                c = cArr[i2];
                if (c < length && iArr[c] != 0) {
                    break;
                }
                i2++;
            } while (i2 < i);
            int i4 = i2 - i3;
            if (i4 > 0) {
                this.r0.write(cArr, i3, i4);
                if (i2 >= i) {
                    return;
                }
            }
            i2++;
            i3 = M1(this.t0, i2, i, c, iArr[c]);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void e0() throws IOException {
        if (!this.i0.e()) {
            a("Current context not Array but " + this.i0.i());
        }
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeEndArray(this, this.i0.c());
        } else {
            if (this.v0 >= this.w0) {
                L1();
            }
            char[] cArr = this.t0;
            int i = this.v0;
            this.v0 = i + 1;
            cArr[i] = ']';
        }
        this.i0 = this.i0.l();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void e1() throws IOException {
        z1("start an array");
        this.i0 = this.i0.m();
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeStartArray(this);
            return;
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = '[';
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x0021 A[EDGE_INSN: B:26:0x0021->B:13:0x0021 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void e2(int r13, int r14) throws java.io.IOException, com.fasterxml.jackson.core.JsonGenerationException {
        /*
            r12 = this;
            int[] r0 = r12.l0
            int r1 = r0.length
            int r2 = r14 + 1
            int r1 = java.lang.Math.min(r1, r2)
            r2 = 0
            r3 = r2
            r4 = r3
        Lc:
            if (r2 >= r13) goto L3a
        Le:
            char[] r5 = r12.t0
            char r10 = r5[r2]
            if (r10 >= r1) goto L19
            r4 = r0[r10]
            if (r4 == 0) goto L1d
            goto L21
        L19:
            if (r10 <= r14) goto L1d
            r4 = -1
            goto L21
        L1d:
            int r2 = r2 + 1
            if (r2 < r13) goto Le
        L21:
            int r6 = r2 - r3
            if (r6 <= 0) goto L2d
            java.io.Writer r7 = r12.r0
            r7.write(r5, r3, r6)
            if (r2 < r13) goto L2d
            goto L3a
        L2d:
            int r2 = r2 + 1
            char[] r7 = r12.t0
            r6 = r12
            r8 = r2
            r9 = r13
            r11 = r4
            int r3 = r6.M1(r7, r8, r9, r10, r11)
            goto Lc
        L3a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.e2(int, int):void");
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void f0() throws IOException {
        if (!this.i0.f()) {
            a("Current context not Object but " + this.i0.i());
        }
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeEndObject(this, this.i0.c());
        } else {
            if (this.v0 >= this.w0) {
                L1();
            }
            char[] cArr = this.t0;
            int i = this.v0;
            this.v0 = i + 1;
            cArr[i] = '}';
        }
        this.i0 = this.i0.l();
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x0035 A[EDGE_INSN: B:31:0x0035->B:19:0x0035 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void f2(int r15) throws java.io.IOException, com.fasterxml.jackson.core.JsonGenerationException {
        /*
            r14 = this;
            int[] r0 = r14.l0
            int r1 = r14.m0
            r2 = 1
            if (r1 >= r2) goto La
            r1 = 65535(0xffff, float:9.1834E-41)
        La:
            int r2 = r0.length
            int r3 = r1 + 1
            int r2 = java.lang.Math.min(r2, r3)
            com.fasterxml.jackson.core.io.CharacterEscapes r3 = r14.n0
            r4 = 0
            r5 = r4
            r6 = r5
        L16:
            if (r4 >= r15) goto L50
        L18:
            char[] r7 = r14.t0
            char r12 = r7[r4]
            if (r12 >= r2) goto L23
            r6 = r0[r12]
            if (r6 == 0) goto L31
            goto L35
        L23:
            if (r12 <= r1) goto L27
            r6 = -1
            goto L35
        L27:
            yl3 r7 = r3.getEscapeSequence(r12)
            r14.y0 = r7
            if (r7 == 0) goto L31
            r6 = -2
            goto L35
        L31:
            int r4 = r4 + 1
            if (r4 < r15) goto L18
        L35:
            int r7 = r4 - r5
            if (r7 <= 0) goto L43
            java.io.Writer r8 = r14.r0
            char[] r9 = r14.t0
            r8.write(r9, r5, r7)
            if (r4 < r15) goto L43
            goto L50
        L43:
            int r4 = r4 + 1
            char[] r9 = r14.t0
            r8 = r14
            r10 = r4
            r11 = r15
            r13 = r6
            int r5 = r8.M1(r9, r10, r11, r12, r13)
            goto L16
        L50:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.f2(int):void");
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator, java.io.Flushable
    public void flush() throws IOException {
        L1();
        if (this.r0 == null || !F1(JsonGenerator.Feature.FLUSH_PASSED_TO_STREAM)) {
            return;
        }
        this.r0.flush();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void g0(yl3 yl3Var) throws IOException {
        int t = this.i0.t(yl3Var.getValue());
        if (t == 4) {
            a("Can not write a field name, expecting a value");
        }
        T1(yl3Var, t == 1);
    }

    public final void g2(String str) throws IOException {
        int length = str.length();
        int i = this.w0;
        if (length > i) {
            V1(str);
            return;
        }
        if (this.v0 + length > i) {
            L1();
        }
        str.getChars(0, length, this.t0, this.v0);
        if (this.n0 != null) {
            l2(length);
            return;
        }
        int i2 = this.m0;
        if (i2 != 0) {
            j2(length, i2);
        } else {
            i2(length);
        }
    }

    public final void h2(char[] cArr, int i, int i2) throws IOException {
        if (this.n0 != null) {
            m2(cArr, i, i2);
            return;
        }
        int i3 = this.m0;
        if (i3 != 0) {
            k2(cArr, i, i2, i3);
            return;
        }
        int i4 = i2 + i;
        int[] iArr = this.l0;
        int length = iArr.length;
        while (i < i4) {
            int i5 = i;
            do {
                char c = cArr[i5];
                if (c < length && iArr[c] != 0) {
                    break;
                }
                i5++;
            } while (i5 < i4);
            int i6 = i5 - i;
            if (i6 < 32) {
                if (this.v0 + i6 > this.w0) {
                    L1();
                }
                if (i6 > 0) {
                    System.arraycopy(cArr, i, this.t0, this.v0, i6);
                    this.v0 += i6;
                }
            } else {
                L1();
                this.r0.write(cArr, i, i6);
            }
            if (i5 >= i4) {
                return;
            }
            i = i5 + 1;
            char c2 = cArr[i5];
            K1(c2, iArr[c2]);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void i0(String str) throws IOException {
        int t = this.i0.t(str);
        if (t == 4) {
            a("Can not write a field name, expecting a value");
        }
        U1(str, t == 1);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void i1() throws IOException {
        z1("start an object");
        this.i0 = this.i0.n();
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeStartObject(this);
            return;
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = '{';
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (r3 <= 0) goto L14;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x001b, code lost:
        r6.r0.write(r2, r4, r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x0020, code lost:
        r2 = r6.t0;
        r3 = r6.v0;
        r6.v0 = r3 + 1;
        r2 = r2[r3];
        N1(r2, r7[r2]);
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0016, code lost:
        r4 = r6.u0;
        r3 = r3 - r4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void i2(int r7) throws java.io.IOException {
        /*
            r6 = this;
            int r0 = r6.v0
            int r0 = r0 + r7
            int[] r7 = r6.l0
            int r1 = r7.length
        L6:
            int r2 = r6.v0
            if (r2 >= r0) goto L36
        La:
            char[] r2 = r6.t0
            int r3 = r6.v0
            char r4 = r2[r3]
            if (r4 >= r1) goto L30
            r4 = r7[r4]
            if (r4 == 0) goto L30
            int r4 = r6.u0
            int r3 = r3 - r4
            if (r3 <= 0) goto L20
            java.io.Writer r5 = r6.r0
            r5.write(r2, r4, r3)
        L20:
            char[] r2 = r6.t0
            int r3 = r6.v0
            int r4 = r3 + 1
            r6.v0 = r4
            char r2 = r2[r3]
            r3 = r7[r2]
            r6.N1(r2, r3)
            goto L6
        L30:
            int r3 = r3 + 1
            r6.v0 = r3
            if (r3 < r0) goto La
        L36:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.i2(int):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x003a A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void j2(int r9, int r10) throws java.io.IOException, com.fasterxml.jackson.core.JsonGenerationException {
        /*
            r8 = this;
            int r0 = r8.v0
            int r0 = r0 + r9
            int[] r9 = r8.l0
            int r1 = r9.length
            int r2 = r10 + 1
            int r1 = java.lang.Math.min(r1, r2)
        Lc:
            int r2 = r8.v0
            if (r2 >= r0) goto L3a
        L10:
            char[] r2 = r8.t0
            int r3 = r8.v0
            char r4 = r2[r3]
            if (r4 >= r1) goto L1d
            r5 = r9[r4]
            if (r5 == 0) goto L34
            goto L20
        L1d:
            if (r4 <= r10) goto L34
            r5 = -1
        L20:
            int r6 = r8.u0
            int r3 = r3 - r6
            if (r3 <= 0) goto L2a
            java.io.Writer r7 = r8.r0
            r7.write(r2, r6, r3)
        L2a:
            int r2 = r8.v0
            int r2 = r2 + 1
            r8.v0 = r2
            r8.N1(r4, r5)
            goto Lc
        L34:
            int r3 = r3 + 1
            r8.v0 = r3
            if (r3 < r0) goto L10
        L3a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.j2(int, int):void");
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void k1(Object obj) throws IOException {
        z1("start an object");
        mw1 n = this.i0.n();
        this.i0 = n;
        if (obj != null) {
            n.h(obj);
        }
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeStartObject(this);
            return;
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = '{';
    }

    /* JADX WARN: Removed duplicated region for block: B:30:0x001f A[EDGE_INSN: B:30:0x001f->B:14:0x001f ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void k2(char[] r9, int r10, int r11, int r12) throws java.io.IOException, com.fasterxml.jackson.core.JsonGenerationException {
        /*
            r8 = this;
            int r11 = r11 + r10
            int[] r0 = r8.l0
            int r1 = r0.length
            int r2 = r12 + 1
            int r1 = java.lang.Math.min(r1, r2)
            r2 = 0
        Lb:
            if (r10 >= r11) goto L4f
            r3 = r10
        Le:
            char r4 = r9[r3]
            if (r4 >= r1) goto L17
            r2 = r0[r4]
            if (r2 == 0) goto L1b
            goto L1f
        L17:
            if (r4 <= r12) goto L1b
            r2 = -1
            goto L1f
        L1b:
            int r3 = r3 + 1
            if (r3 < r11) goto Le
        L1f:
            int r5 = r3 - r10
            r6 = 32
            if (r5 >= r6) goto L3e
            int r6 = r8.v0
            int r6 = r6 + r5
            int r7 = r8.w0
            if (r6 <= r7) goto L2f
            r8.L1()
        L2f:
            if (r5 <= 0) goto L46
            char[] r6 = r8.t0
            int r7 = r8.v0
            java.lang.System.arraycopy(r9, r10, r6, r7, r5)
            int r10 = r8.v0
            int r10 = r10 + r5
            r8.v0 = r10
            goto L46
        L3e:
            r8.L1()
            java.io.Writer r6 = r8.r0
            r6.write(r9, r10, r5)
        L46:
            if (r3 < r11) goto L49
            goto L4f
        L49:
            int r10 = r3 + 1
            r8.K1(r4, r2)
            goto Lb
        L4f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.k2(char[], int, int, int):void");
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0052 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void l2(int r12) throws java.io.IOException, com.fasterxml.jackson.core.JsonGenerationException {
        /*
            r11 = this;
            int r0 = r11.v0
            int r0 = r0 + r12
            int[] r12 = r11.l0
            int r1 = r11.m0
            r2 = 1
            if (r1 >= r2) goto Ld
            r1 = 65535(0xffff, float:9.1834E-41)
        Ld:
            int r3 = r12.length
            int r4 = r1 + 1
            int r3 = java.lang.Math.min(r3, r4)
            com.fasterxml.jackson.core.io.CharacterEscapes r4 = r11.n0
        L16:
            int r5 = r11.v0
            if (r5 >= r0) goto L52
        L1a:
            char[] r5 = r11.t0
            int r6 = r11.v0
            char r5 = r5[r6]
            if (r5 >= r3) goto L27
            r6 = r12[r5]
            if (r6 == 0) goto L4b
            goto L34
        L27:
            if (r5 <= r1) goto L2b
            r6 = -1
            goto L34
        L2b:
            yl3 r6 = r4.getEscapeSequence(r5)
            r11.y0 = r6
            if (r6 == 0) goto L4b
            r6 = -2
        L34:
            int r7 = r11.v0
            int r8 = r11.u0
            int r7 = r7 - r8
            if (r7 <= 0) goto L42
            java.io.Writer r9 = r11.r0
            char[] r10 = r11.t0
            r9.write(r10, r8, r7)
        L42:
            int r7 = r11.v0
            int r7 = r7 + r2
            r11.v0 = r7
            r11.N1(r5, r6)
            goto L16
        L4b:
            int r5 = r11.v0
            int r5 = r5 + r2
            r11.v0 = r5
            if (r5 < r0) goto L1a
        L52:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.l2(int):void");
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void m0() throws IOException {
        z1("write a null");
        W1();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void m1(yl3 yl3Var) throws IOException {
        z1("write a string");
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = this.s0;
        char[] asQuotedChars = yl3Var.asQuotedChars();
        int length = asQuotedChars.length;
        if (length < 32) {
            if (length > this.w0 - this.v0) {
                L1();
            }
            System.arraycopy(asQuotedChars, 0, this.t0, this.v0, length);
            this.v0 += length;
        } else {
            L1();
            this.r0.write(asQuotedChars, 0, length);
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr2[i2] = this.s0;
    }

    /* JADX WARN: Removed duplicated region for block: B:34:0x0033 A[EDGE_INSN: B:34:0x0033->B:20:0x0033 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void m2(char[] r11, int r12, int r13) throws java.io.IOException, com.fasterxml.jackson.core.JsonGenerationException {
        /*
            r10 = this;
            int r13 = r13 + r12
            int[] r0 = r10.l0
            int r1 = r10.m0
            r2 = 1
            if (r1 >= r2) goto Lb
            r1 = 65535(0xffff, float:9.1834E-41)
        Lb:
            int r2 = r0.length
            int r3 = r1 + 1
            int r2 = java.lang.Math.min(r2, r3)
            com.fasterxml.jackson.core.io.CharacterEscapes r3 = r10.n0
            r4 = 0
        L15:
            if (r12 >= r13) goto L63
            r5 = r12
        L18:
            char r6 = r11[r5]
            if (r6 >= r2) goto L21
            r4 = r0[r6]
            if (r4 == 0) goto L2f
            goto L33
        L21:
            if (r6 <= r1) goto L25
            r4 = -1
            goto L33
        L25:
            yl3 r7 = r3.getEscapeSequence(r6)
            r10.y0 = r7
            if (r7 == 0) goto L2f
            r4 = -2
            goto L33
        L2f:
            int r5 = r5 + 1
            if (r5 < r13) goto L18
        L33:
            int r7 = r5 - r12
            r8 = 32
            if (r7 >= r8) goto L52
            int r8 = r10.v0
            int r8 = r8 + r7
            int r9 = r10.w0
            if (r8 <= r9) goto L43
            r10.L1()
        L43:
            if (r7 <= 0) goto L5a
            char[] r8 = r10.t0
            int r9 = r10.v0
            java.lang.System.arraycopy(r11, r12, r8, r9, r7)
            int r12 = r10.v0
            int r12 = r12 + r7
            r10.v0 = r12
            goto L5a
        L52:
            r10.L1()
            java.io.Writer r8 = r10.r0
            r8.write(r11, r12, r7)
        L5a:
            if (r5 < r13) goto L5d
            goto L63
        L5d:
            int r12 = r5 + 1
            r10.K1(r6, r4)
            goto L15
        L63:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.lr4.m2(char[], int, int):void");
    }

    public final void n2(String str) throws IOException {
        int i = this.w0;
        int i2 = this.v0;
        int i3 = i - i2;
        str.getChars(0, i3, this.t0, i2);
        this.v0 += i3;
        L1();
        int length = str.length() - i3;
        while (true) {
            int i4 = this.w0;
            if (length > i4) {
                int i5 = i3 + i4;
                str.getChars(i3, i5, this.t0, 0);
                this.u0 = 0;
                this.v0 = i4;
                L1();
                length -= i4;
                i3 = i5;
            } else {
                str.getChars(i3, i3 + length, this.t0, 0);
                this.u0 = 0;
                this.v0 = length;
                return;
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void o1(String str) throws IOException {
        z1("write a string");
        if (str == null) {
            W1();
            return;
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = this.s0;
        g2(str);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i2 = this.v0;
        this.v0 = i2 + 1;
        cArr2[i2] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void p1(char[] cArr, int i, int i2) throws IOException {
        z1("write a string");
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr2 = this.t0;
        int i3 = this.v0;
        this.v0 = i3 + 1;
        cArr2[i3] = this.s0;
        h2(cArr, i, i2);
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr3 = this.t0;
        int i4 = this.v0;
        this.v0 = i4 + 1;
        cArr3[i4] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void r0(double d) throws IOException {
        if (!this.h0 && (!F1(JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS) || (!Double.isNaN(d) && !Double.isInfinite(d)))) {
            z1("write a number");
            W0(String.valueOf(d));
            return;
        }
        o1(String.valueOf(d));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void w0(float f) throws IOException {
        if (!this.h0 && (!F1(JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS) || (!Float.isNaN(f) && !Float.isInfinite(f)))) {
            z1("write a number");
            W0(String.valueOf(f));
            return;
        }
        o1(String.valueOf(f));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void x0(int i) throws IOException {
        z1("write a number");
        if (this.h0) {
            Z1(i);
            return;
        }
        if (this.v0 + 11 >= this.w0) {
            L1();
        }
        this.v0 = aj2.p(i, this.t0, this.v0);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void y0(long j) throws IOException {
        z1("write a number");
        if (this.h0) {
            a2(j);
            return;
        }
        if (this.v0 + 21 >= this.w0) {
            L1();
        }
        this.v0 = aj2.r(j, this.t0, this.v0);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void z0(String str) throws IOException {
        z1("write a number");
        if (this.h0) {
            b2(str);
        } else {
            W0(str);
        }
    }

    @Override // defpackage.we1
    public void z1(String str) throws IOException {
        char c;
        int u = this.i0.u();
        if (this.a != null) {
            I1(str, u);
            return;
        }
        if (u == 1) {
            c = ',';
        } else if (u != 2) {
            if (u != 3) {
                if (u != 5) {
                    return;
                }
                H1(str);
                return;
            }
            yl3 yl3Var = this.o0;
            if (yl3Var != null) {
                W0(yl3Var.getValue());
                return;
            }
            return;
        } else {
            c = ':';
        }
        if (this.v0 >= this.w0) {
            L1();
        }
        char[] cArr = this.t0;
        int i = this.v0;
        this.v0 = i + 1;
        cArr[i] = c;
    }
}
