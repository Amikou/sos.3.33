package defpackage;

import android.content.SharedPreferences;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ph5  reason: default package */
/* loaded from: classes.dex */
public final class ph5 {
    public final String a;
    public final long b;
    public boolean c;
    public long d;
    public final /* synthetic */ mi5 e;

    public ph5(mi5 mi5Var, String str, long j) {
        this.e = mi5Var;
        zt2.f(str);
        this.a = str;
        this.b = j;
    }

    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.n().getLong(this.a, this.b);
        }
        return this.d;
    }

    public final void b(long j) {
        SharedPreferences.Editor edit = this.e.n().edit();
        edit.putLong(this.a, j);
        edit.apply();
        this.d = j;
    }
}
