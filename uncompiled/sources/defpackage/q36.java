package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: q36  reason: default package */
/* loaded from: classes.dex */
public final class q36 implements yp5<r36> {
    public static final q36 f0 = new q36();
    public final yp5<r36> a = gq5.a(gq5.b(new s36()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final r36 zza() {
        return this.a.zza();
    }
}
