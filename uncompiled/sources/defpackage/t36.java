package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: t36  reason: default package */
/* loaded from: classes.dex */
public final class t36 implements yp5<u36> {
    public static final t36 f0 = new t36();
    public final yp5<u36> a = gq5.a(gq5.b(new v36()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final u36 zza() {
        return this.a.zza();
    }
}
