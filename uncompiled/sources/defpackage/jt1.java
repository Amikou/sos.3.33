package defpackage;

import android.graphics.Canvas;
import android.os.Build;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ItemTouchUIUtilImpl.java */
/* renamed from: jt1  reason: default package */
/* loaded from: classes.dex */
public class jt1 implements it1 {
    public static final it1 a = new jt1();

    public static float e(RecyclerView recyclerView, View view) {
        int childCount = recyclerView.getChildCount();
        float f = Utils.FLOAT_EPSILON;
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (childAt != view) {
                float y = ei4.y(childAt);
                if (y > f) {
                    f = y;
                }
            }
        }
        return f;
    }

    @Override // defpackage.it1
    public void a(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            int i = xz2.item_touch_helper_previous_elevation;
            Object tag = view.getTag(i);
            if (tag instanceof Float) {
                ei4.A0(view, ((Float) tag).floatValue());
            }
            view.setTag(i, null);
        }
        view.setTranslationX(Utils.FLOAT_EPSILON);
        view.setTranslationY(Utils.FLOAT_EPSILON);
    }

    @Override // defpackage.it1
    public void b(View view) {
    }

    @Override // defpackage.it1
    public void c(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
    }

    @Override // defpackage.it1
    public void d(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        if (Build.VERSION.SDK_INT >= 21 && z) {
            int i2 = xz2.item_touch_helper_previous_elevation;
            if (view.getTag(i2) == null) {
                Float valueOf = Float.valueOf(ei4.y(view));
                ei4.A0(view, e(recyclerView, view) + 1.0f);
                view.setTag(i2, valueOf);
            }
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }
}
