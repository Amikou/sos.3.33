package defpackage;

import android.os.Looper;
import android.os.Message;
import com.google.android.gms.cloudmessaging.b;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: z56  reason: default package */
/* loaded from: classes.dex */
public final class z56 extends lf5 {
    public final /* synthetic */ b a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public z56(b bVar, Looper looper) {
        super(looper);
        this.a = bVar;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        this.a.h(message);
    }
}
