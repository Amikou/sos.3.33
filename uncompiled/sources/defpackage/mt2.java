package defpackage;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.h;

/* compiled from: PopupMenu.java */
/* renamed from: mt2  reason: default package */
/* loaded from: classes.dex */
public class mt2 {
    public final Context a;
    public final e b;
    public final h c;
    public d d;
    public c e;

    /* compiled from: PopupMenu.java */
    /* renamed from: mt2$a */
    /* loaded from: classes.dex */
    public class a implements e.a {
        public a() {
        }

        @Override // androidx.appcompat.view.menu.e.a
        public boolean a(e eVar, MenuItem menuItem) {
            d dVar = mt2.this.d;
            if (dVar != null) {
                return dVar.onMenuItemClick(menuItem);
            }
            return false;
        }

        @Override // androidx.appcompat.view.menu.e.a
        public void b(e eVar) {
        }
    }

    /* compiled from: PopupMenu.java */
    /* renamed from: mt2$b */
    /* loaded from: classes.dex */
    public class b implements PopupWindow.OnDismissListener {
        public b() {
        }

        @Override // android.widget.PopupWindow.OnDismissListener
        public void onDismiss() {
            mt2 mt2Var = mt2.this;
            c cVar = mt2Var.e;
            if (cVar != null) {
                cVar.a(mt2Var);
            }
        }
    }

    /* compiled from: PopupMenu.java */
    /* renamed from: mt2$c */
    /* loaded from: classes.dex */
    public interface c {
        void a(mt2 mt2Var);
    }

    /* compiled from: PopupMenu.java */
    /* renamed from: mt2$d */
    /* loaded from: classes.dex */
    public interface d {
        boolean onMenuItemClick(MenuItem menuItem);
    }

    public mt2(Context context, View view) {
        this(context, view, 0);
    }

    public Menu a() {
        return this.b;
    }

    public MenuInflater b() {
        return new kw3(this.a);
    }

    public void c(int i) {
        b().inflate(i, this.b);
    }

    public void d(int i) {
        this.c.h(i);
    }

    public void e(d dVar) {
        this.d = dVar;
    }

    public void f() {
        this.c.k();
    }

    public mt2(Context context, View view, int i) {
        this(context, view, i, jy2.popupMenuStyle, 0);
    }

    public mt2(Context context, View view, int i, int i2, int i3) {
        this.a = context;
        e eVar = new e(context);
        this.b = eVar;
        eVar.V(new a());
        h hVar = new h(context, eVar, view, false, i2, i3);
        this.c = hVar;
        hVar.h(i);
        hVar.i(new b());
    }
}
