package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: j06  reason: default package */
/* loaded from: classes.dex */
public final class j06 implements i06 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.client.click_identifier_control.dev", false);

    @Override // defpackage.i06
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
