package defpackage;

import android.util.SparseArray;
import android.view.View;

/* compiled from: TransitionValuesMaps.java */
/* renamed from: lb4  reason: default package */
/* loaded from: classes.dex */
public class lb4 {
    public final rh<View, kb4> a = new rh<>();
    public final SparseArray<View> b = new SparseArray<>();
    public final i22<View> c = new i22<>();
    public final rh<String, View> d = new rh<>();
}
