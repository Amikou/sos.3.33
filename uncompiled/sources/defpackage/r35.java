package defpackage;

import com.google.android.gms.internal.vision.l;
import com.google.android.gms.vision.clearcut.DynamiteClearcutLogger;
import com.google.android.gms.vision.clearcut.VisionClearcutLogger;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: r35  reason: default package */
/* loaded from: classes.dex */
public final class r35 implements Runnable {
    public final /* synthetic */ int a;
    public final /* synthetic */ l f0;
    public final /* synthetic */ DynamiteClearcutLogger g0;

    public r35(DynamiteClearcutLogger dynamiteClearcutLogger, int i, l lVar) {
        this.g0 = dynamiteClearcutLogger;
        this.a = i;
        this.f0 = lVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        VisionClearcutLogger visionClearcutLogger;
        visionClearcutLogger = this.g0.zzc;
        visionClearcutLogger.zza(this.a, this.f0);
    }
}
