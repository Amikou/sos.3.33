package defpackage;

import com.facebook.imagepipeline.image.a;

/* compiled from: CloseableAnimatedImage.java */
/* renamed from: vz  reason: default package */
/* loaded from: classes.dex */
public class vz extends a {
    public yd g0;
    public boolean h0;

    public vz(yd ydVar) {
        this(ydVar, true);
    }

    @Override // com.facebook.imagepipeline.image.a
    public synchronized int b() {
        yd ydVar;
        ydVar = this.g0;
        return ydVar == null ? 0 : ydVar.d().j();
    }

    @Override // com.facebook.imagepipeline.image.a
    public boolean c() {
        return this.h0;
    }

    @Override // com.facebook.imagepipeline.image.a, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            yd ydVar = this.g0;
            if (ydVar == null) {
                return;
            }
            this.g0 = null;
            ydVar.a();
        }
    }

    public synchronized td f() {
        yd ydVar;
        ydVar = this.g0;
        return ydVar == null ? null : ydVar.d();
    }

    public synchronized yd g() {
        return this.g0;
    }

    @Override // defpackage.ao1
    public synchronized int getHeight() {
        yd ydVar;
        ydVar = this.g0;
        return ydVar == null ? 0 : ydVar.d().getHeight();
    }

    @Override // defpackage.ao1
    public synchronized int getWidth() {
        yd ydVar;
        ydVar = this.g0;
        return ydVar == null ? 0 : ydVar.d().getWidth();
    }

    @Override // com.facebook.imagepipeline.image.a
    public synchronized boolean isClosed() {
        return this.g0 == null;
    }

    public vz(yd ydVar, boolean z) {
        this.g0 = ydVar;
        this.h0 = z;
    }
}
