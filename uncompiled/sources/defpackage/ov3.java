package defpackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: SubstituteLoggerFactory.java */
/* renamed from: ov3  reason: default package */
/* loaded from: classes2.dex */
public class ov3 implements hm1 {
    public boolean a = false;
    public final Map<String, nv3> b = new HashMap();
    public final LinkedBlockingQueue<pv3> c = new LinkedBlockingQueue<>();

    @Override // defpackage.hm1
    public synchronized x12 a(String str) {
        nv3 nv3Var;
        nv3Var = this.b.get(str);
        if (nv3Var == null) {
            nv3Var = new nv3(str, this.c, this.a);
            this.b.put(str, nv3Var);
        }
        return nv3Var;
    }

    public void b() {
        this.b.clear();
        this.c.clear();
    }

    public LinkedBlockingQueue<pv3> c() {
        return this.c;
    }

    public List<nv3> d() {
        return new ArrayList(this.b.values());
    }

    public void e() {
        this.a = true;
    }
}
