package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: w56  reason: default package */
/* loaded from: classes.dex */
public final class w56 extends c55 {
    public w56() {
        super("internal.platform");
        this.f0.put("isAndroid", new m56(this, "isAndroid"));
        this.f0.put("getVersion", new r56(this, "getVersion"));
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        return z55.X;
    }
}
