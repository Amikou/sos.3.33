package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: w06  reason: default package */
/* loaded from: classes.dex */
public final class w06 implements yp5<x06> {
    public static final w06 f0 = new w06();
    public final yp5<x06> a = gq5.a(gq5.b(new y06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final x06 zza() {
        return this.a.zza();
    }
}
