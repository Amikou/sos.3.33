package defpackage;

import defpackage.u21;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;

/* compiled from: FiatData.kt */
/* renamed from: v21  reason: default package */
/* loaded from: classes2.dex */
public final class v21 {
    public static final double a(double d) {
        u21.a aVar = u21.a;
        if (aVar.a().getRate() != null) {
            Double rate = aVar.a().getRate();
            fs1.d(rate);
            return d * rate.doubleValue();
        }
        return d;
    }

    public static final double b(double d, Fiat fiat) {
        fs1.f(fiat, "defaultFiat");
        return fiat.getRate() != null ? d * fiat.getRate().doubleValue() : d;
    }

    public static final double c(double d) {
        u21.a aVar = u21.a;
        if (aVar.a().getRate() != null) {
            Double rate = aVar.a().getRate();
            fs1.d(rate);
            return d / rate.doubleValue();
        }
        return d;
    }
}
