package defpackage;

import androidx.media3.common.j;
import androidx.media3.common.util.b;
import defpackage.gc4;
import zendesk.support.request.CellBase;

/* compiled from: PassthroughSectionPayloadReader.java */
/* renamed from: up2  reason: default package */
/* loaded from: classes.dex */
public final class up2 implements wh3 {
    public j a;
    public h64 b;
    public f84 c;

    public up2(String str) {
        this.a = new j.b().e0(str).E();
    }

    @Override // defpackage.wh3
    public void a(op2 op2Var) {
        c();
        long d = this.b.d();
        long e = this.b.e();
        if (d == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || e == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return;
        }
        j jVar = this.a;
        if (e != jVar.t0) {
            j E = jVar.b().i0(e).E();
            this.a = E;
            this.c.f(E);
        }
        int a = op2Var.a();
        this.c.a(op2Var, a);
        this.c.b(d, 1, a, 0, null);
    }

    @Override // defpackage.wh3
    public void b(h64 h64Var, r11 r11Var, gc4.d dVar) {
        this.b = h64Var;
        dVar.a();
        f84 f = r11Var.f(dVar.c(), 5);
        this.c = f;
        f.f(this.a);
    }

    public final void c() {
        ii.i(this.b);
        b.j(this.c);
    }
}
