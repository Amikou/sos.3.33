package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.helper.widget.Flow;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityImportWordBinding.java */
/* renamed from: i7  reason: default package */
/* loaded from: classes2.dex */
public final class i7 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final AppCompatImageView e;
    public final ConstraintLayout f;
    public final Flow g;

    public i7(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, AppCompatImageView appCompatImageView, ConstraintLayout constraintLayout2, Flow flow, LinearLayout linearLayout, ImageView imageView, ScrollView scrollView) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = appCompatImageView;
        this.f = constraintLayout2;
        this.g = flow;
    }

    public static i7 a(View view) {
        int i = R.id.btnBack;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBack);
        if (materialButton != null) {
            i = R.id.btnConfirm;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnConfirm);
            if (materialButton2 != null) {
                i = R.id.btnPaste;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnPaste);
                if (materialButton3 != null) {
                    i = R.id.btnScanQr;
                    AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnScanQr);
                    if (appCompatImageView != null) {
                        i = R.id.clWordParent;
                        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.clWordParent);
                        if (constraintLayout != null) {
                            i = R.id.flowWidget;
                            Flow flow = (Flow) ai4.a(view, R.id.flowWidget);
                            if (flow != null) {
                                i = R.id.headerParent;
                                LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.headerParent);
                                if (linearLayout != null) {
                                    i = R.id.imageView4;
                                    ImageView imageView = (ImageView) ai4.a(view, R.id.imageView4);
                                    if (imageView != null) {
                                        i = R.id.scrollView4;
                                        ScrollView scrollView = (ScrollView) ai4.a(view, R.id.scrollView4);
                                        if (scrollView != null) {
                                            return new i7((ConstraintLayout) view, materialButton, materialButton2, materialButton3, appCompatImageView, constraintLayout, flow, linearLayout, imageView, scrollView);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static i7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static i7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_import_word, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
