package defpackage;

import com.google.android.gms.tasks.c;
import java.util.ArrayDeque;
import java.util.Queue;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: m36  reason: default package */
/* loaded from: classes.dex */
public final class m36<TResult> {
    public final Object a = new Object();
    public Queue<l46<TResult>> b;
    public boolean c;

    public final void a(c<TResult> cVar) {
        l46<TResult> poll;
        synchronized (this.a) {
            if (this.b != null && !this.c) {
                this.c = true;
                while (true) {
                    synchronized (this.a) {
                        poll = this.b.poll();
                        if (poll == null) {
                            this.c = false;
                            return;
                        }
                    }
                    poll.c(cVar);
                }
            }
        }
    }

    public final void b(l46<TResult> l46Var) {
        synchronized (this.a) {
            if (this.b == null) {
                this.b = new ArrayDeque();
            }
            this.b.add(l46Var);
        }
    }
}
