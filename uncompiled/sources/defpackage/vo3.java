package defpackage;

import java.util.ConcurrentModificationException;
import java.util.Map;

/* compiled from: SimpleArrayMap.java */
/* renamed from: vo3  reason: default package */
/* loaded from: classes.dex */
public class vo3<K, V> {
    public static Object[] h0;
    public static int i0;
    public static Object[] j0;
    public static int k0;
    public int[] a;
    public Object[] f0;
    public int g0;

    public vo3() {
        this.a = c70.a;
        this.f0 = c70.c;
        this.g0 = 0;
    }

    private void a(int i) {
        if (i == 8) {
            synchronized (vo3.class) {
                Object[] objArr = j0;
                if (objArr != null) {
                    this.f0 = objArr;
                    j0 = (Object[]) objArr[0];
                    this.a = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    k0--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (vo3.class) {
                Object[] objArr2 = h0;
                if (objArr2 != null) {
                    this.f0 = objArr2;
                    h0 = (Object[]) objArr2[0];
                    this.a = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    i0--;
                    return;
                }
            }
        }
        this.a = new int[i];
        this.f0 = new Object[i << 1];
    }

    public static int b(int[] iArr, int i, int i2) {
        try {
            return c70.a(iArr, i, i2);
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    public static void d(int[] iArr, Object[] objArr, int i) {
        if (iArr.length == 8) {
            synchronized (vo3.class) {
                if (k0 < 10) {
                    objArr[0] = j0;
                    objArr[1] = iArr;
                    for (int i2 = (i << 1) - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    j0 = objArr;
                    k0++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (vo3.class) {
                if (i0 < 10) {
                    objArr[0] = h0;
                    objArr[1] = iArr;
                    for (int i3 = (i << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    h0 = objArr;
                    i0++;
                }
            }
        }
    }

    public void c(int i) {
        int i2 = this.g0;
        int[] iArr = this.a;
        if (iArr.length < i) {
            Object[] objArr = this.f0;
            a(i);
            if (this.g0 > 0) {
                System.arraycopy(iArr, 0, this.a, 0, i2);
                System.arraycopy(objArr, 0, this.f0, 0, i2 << 1);
            }
            d(iArr, objArr, i2);
        }
        if (this.g0 != i2) {
            throw new ConcurrentModificationException();
        }
    }

    public void clear() {
        int i = this.g0;
        if (i > 0) {
            int[] iArr = this.a;
            Object[] objArr = this.f0;
            this.a = c70.a;
            this.f0 = c70.c;
            this.g0 = 0;
            d(iArr, objArr, i);
        }
        if (this.g0 > 0) {
            throw new ConcurrentModificationException();
        }
    }

    public boolean containsKey(Object obj) {
        return f(obj) >= 0;
    }

    public boolean containsValue(Object obj) {
        return h(obj) >= 0;
    }

    public int e(Object obj, int i) {
        int i2 = this.g0;
        if (i2 == 0) {
            return -1;
        }
        int b = b(this.a, i2, i);
        if (b >= 0 && !obj.equals(this.f0[b << 1])) {
            int i3 = b + 1;
            while (i3 < i2 && this.a[i3] == i) {
                if (obj.equals(this.f0[i3 << 1])) {
                    return i3;
                }
                i3++;
            }
            for (int i4 = b - 1; i4 >= 0 && this.a[i4] == i; i4--) {
                if (obj.equals(this.f0[i4 << 1])) {
                    return i4;
                }
            }
            return ~i3;
        }
        return b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof vo3) {
            vo3 vo3Var = (vo3) obj;
            if (size() != vo3Var.size()) {
                return false;
            }
            for (int i = 0; i < this.g0; i++) {
                try {
                    K i2 = i(i);
                    V m = m(i);
                    Object obj2 = vo3Var.get(i2);
                    if (m == null) {
                        if (obj2 != null || !vo3Var.containsKey(i2)) {
                            return false;
                        }
                    } else if (!m.equals(obj2)) {
                        return false;
                    }
                } catch (ClassCastException | NullPointerException unused) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (size() != map.size()) {
                return false;
            }
            for (int i3 = 0; i3 < this.g0; i3++) {
                try {
                    K i4 = i(i3);
                    V m2 = m(i3);
                    Object obj3 = map.get(i4);
                    if (m2 == null) {
                        if (obj3 != null || !map.containsKey(i4)) {
                            return false;
                        }
                    } else if (!m2.equals(obj3)) {
                        return false;
                    }
                } catch (ClassCastException | NullPointerException unused2) {
                }
            }
            return true;
        }
        return false;
    }

    public int f(Object obj) {
        return obj == null ? g() : e(obj, obj.hashCode());
    }

    public int g() {
        int i = this.g0;
        if (i == 0) {
            return -1;
        }
        int b = b(this.a, i, 0);
        if (b >= 0 && this.f0[b << 1] != null) {
            int i2 = b + 1;
            while (i2 < i && this.a[i2] == 0) {
                if (this.f0[i2 << 1] == null) {
                    return i2;
                }
                i2++;
            }
            for (int i3 = b - 1; i3 >= 0 && this.a[i3] == 0; i3--) {
                if (this.f0[i3 << 1] == null) {
                    return i3;
                }
            }
            return ~i2;
        }
        return b;
    }

    public V get(Object obj) {
        return getOrDefault(obj, null);
    }

    public V getOrDefault(Object obj, V v) {
        int f = f(obj);
        return f >= 0 ? (V) this.f0[(f << 1) + 1] : v;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public int h(Object obj) {
        int i = this.g0 * 2;
        Object[] objArr = this.f0;
        if (obj == null) {
            for (int i2 = 1; i2 < i; i2 += 2) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
            }
            return -1;
        }
        for (int i3 = 1; i3 < i; i3 += 2) {
            if (obj.equals(objArr[i3])) {
                return i3 >> 1;
            }
        }
        return -1;
    }

    public int hashCode() {
        int[] iArr = this.a;
        Object[] objArr = this.f0;
        int i = this.g0;
        int i2 = 1;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            Object obj = objArr[i2];
            i4 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i3];
            i3++;
            i2 += 2;
        }
        return i4;
    }

    public K i(int i) {
        return (K) this.f0[i << 1];
    }

    public boolean isEmpty() {
        return this.g0 <= 0;
    }

    public void j(vo3<? extends K, ? extends V> vo3Var) {
        int i = vo3Var.g0;
        c(this.g0 + i);
        if (this.g0 != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                put(vo3Var.i(i2), vo3Var.m(i2));
            }
        } else if (i > 0) {
            System.arraycopy(vo3Var.a, 0, this.a, 0, i);
            System.arraycopy(vo3Var.f0, 0, this.f0, 0, i << 1);
            this.g0 = i;
        }
    }

    public V k(int i) {
        Object[] objArr = this.f0;
        int i2 = i << 1;
        V v = (V) objArr[i2 + 1];
        int i3 = this.g0;
        int i4 = 0;
        if (i3 <= 1) {
            d(this.a, objArr, i3);
            this.a = c70.a;
            this.f0 = c70.c;
        } else {
            int i5 = i3 - 1;
            int[] iArr = this.a;
            if (iArr.length > 8 && i3 < iArr.length / 3) {
                a(i3 > 8 ? i3 + (i3 >> 1) : 8);
                if (i3 != this.g0) {
                    throw new ConcurrentModificationException();
                }
                if (i > 0) {
                    System.arraycopy(iArr, 0, this.a, 0, i);
                    System.arraycopy(objArr, 0, this.f0, 0, i2);
                }
                if (i < i5) {
                    int i6 = i + 1;
                    int i7 = i5 - i;
                    System.arraycopy(iArr, i6, this.a, i, i7);
                    System.arraycopy(objArr, i6 << 1, this.f0, i2, i7 << 1);
                }
            } else {
                if (i < i5) {
                    int i8 = i + 1;
                    int i9 = i5 - i;
                    System.arraycopy(iArr, i8, iArr, i, i9);
                    Object[] objArr2 = this.f0;
                    System.arraycopy(objArr2, i8 << 1, objArr2, i2, i9 << 1);
                }
                Object[] objArr3 = this.f0;
                int i10 = i5 << 1;
                objArr3[i10] = null;
                objArr3[i10 + 1] = null;
            }
            i4 = i5;
        }
        if (i3 == this.g0) {
            this.g0 = i4;
            return v;
        }
        throw new ConcurrentModificationException();
    }

    public V l(int i, V v) {
        int i2 = (i << 1) + 1;
        Object[] objArr = this.f0;
        V v2 = (V) objArr[i2];
        objArr[i2] = v;
        return v2;
    }

    public V m(int i) {
        return (V) this.f0[(i << 1) + 1];
    }

    public V put(K k, V v) {
        int i;
        int e;
        int i2 = this.g0;
        if (k == null) {
            e = g();
            i = 0;
        } else {
            int hashCode = k.hashCode();
            i = hashCode;
            e = e(k, hashCode);
        }
        if (e >= 0) {
            int i3 = (e << 1) + 1;
            Object[] objArr = this.f0;
            V v2 = (V) objArr[i3];
            objArr[i3] = v;
            return v2;
        }
        int i4 = ~e;
        int[] iArr = this.a;
        if (i2 >= iArr.length) {
            int i5 = 4;
            if (i2 >= 8) {
                i5 = (i2 >> 1) + i2;
            } else if (i2 >= 4) {
                i5 = 8;
            }
            Object[] objArr2 = this.f0;
            a(i5);
            if (i2 == this.g0) {
                int[] iArr2 = this.a;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr2, 0, this.f0, 0, objArr2.length);
                }
                d(iArr, objArr2, i2);
            } else {
                throw new ConcurrentModificationException();
            }
        }
        if (i4 < i2) {
            int[] iArr3 = this.a;
            int i6 = i4 + 1;
            System.arraycopy(iArr3, i4, iArr3, i6, i2 - i4);
            Object[] objArr3 = this.f0;
            System.arraycopy(objArr3, i4 << 1, objArr3, i6 << 1, (this.g0 - i4) << 1);
        }
        int i7 = this.g0;
        if (i2 == i7) {
            int[] iArr4 = this.a;
            if (i4 < iArr4.length) {
                iArr4[i4] = i;
                Object[] objArr4 = this.f0;
                int i8 = i4 << 1;
                objArr4[i8] = k;
                objArr4[i8 + 1] = v;
                this.g0 = i7 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public V putIfAbsent(K k, V v) {
        V v2 = get(k);
        return v2 == null ? put(k, v) : v2;
    }

    public V remove(Object obj) {
        int f = f(obj);
        if (f >= 0) {
            return k(f);
        }
        return null;
    }

    public V replace(K k, V v) {
        int f = f(k);
        if (f >= 0) {
            return l(f, v);
        }
        return null;
    }

    public int size() {
        return this.g0;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.g0 * 28);
        sb.append('{');
        for (int i = 0; i < this.g0; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            K i2 = i(i);
            if (i2 != this) {
                sb.append(i2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            V m = m(i);
            if (m != this) {
                sb.append(m);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public boolean remove(Object obj, Object obj2) {
        int f = f(obj);
        if (f >= 0) {
            V m = m(f);
            if (obj2 == m || (obj2 != null && obj2.equals(m))) {
                k(f);
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean replace(K k, V v, V v2) {
        int f = f(k);
        if (f >= 0) {
            V m = m(f);
            if (m == v || (v != null && v.equals(m))) {
                l(f, v2);
                return true;
            }
            return false;
        }
        return false;
    }

    public vo3(int i) {
        if (i == 0) {
            this.a = c70.a;
            this.f0 = c70.c;
        } else {
            a(i);
        }
        this.g0 = 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public vo3(vo3<K, V> vo3Var) {
        this();
        if (vo3Var != 0) {
            j(vo3Var);
        }
    }
}
