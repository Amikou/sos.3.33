package defpackage;

import androidx.media3.common.util.b;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ClearKeyUtil.java */
/* renamed from: gz  reason: default package */
/* loaded from: classes.dex */
public final class gz {
    public static byte[] a(byte[] bArr) {
        return b.a >= 27 ? bArr : b.j0(c(b.B(bArr)));
    }

    public static byte[] b(byte[] bArr) {
        if (b.a >= 27) {
            return bArr;
        }
        try {
            JSONObject jSONObject = new JSONObject(b.B(bArr));
            StringBuilder sb = new StringBuilder("{\"keys\":[");
            JSONArray jSONArray = jSONObject.getJSONArray("keys");
            for (int i = 0; i < jSONArray.length(); i++) {
                if (i != 0) {
                    sb.append(",");
                }
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                sb.append("{\"k\":\"");
                sb.append(d(jSONObject2.getString("k")));
                sb.append("\",\"kid\":\"");
                sb.append(d(jSONObject2.getString("kid")));
                sb.append("\",\"kty\":\"");
                sb.append(jSONObject2.getString("kty"));
                sb.append("\"}");
            }
            sb.append("]}");
            return b.j0(sb.toString());
        } catch (JSONException e) {
            p12.d("ClearKeyUtil", "Failed to adjust response data: " + b.B(bArr), e);
            return bArr;
        }
    }

    public static String c(String str) {
        return str.replace('+', '-').replace('/', '_');
    }

    public static String d(String str) {
        return str.replace('-', '+').replace('_', '/');
    }
}
