package defpackage;

import android.content.Context;

/* compiled from: SchemaManager_Factory.java */
/* renamed from: td3  reason: default package */
/* loaded from: classes.dex */
public final class td3 implements z11<sd3> {
    public final ew2<Context> a;
    public final ew2<String> b;
    public final ew2<Integer> c;

    public td3(ew2<Context> ew2Var, ew2<String> ew2Var2, ew2<Integer> ew2Var3) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
    }

    public static td3 a(ew2<Context> ew2Var, ew2<String> ew2Var2, ew2<Integer> ew2Var3) {
        return new td3(ew2Var, ew2Var2, ew2Var3);
    }

    public static sd3 c(Context context, String str, int i) {
        return new sd3(context, str, i);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public sd3 get() {
        return c(this.a.get(), this.b.get(), this.c.get().intValue());
    }
}
