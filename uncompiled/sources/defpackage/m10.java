package defpackage;

import androidx.annotation.RecentlyNonNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: m10  reason: default package */
/* loaded from: classes.dex */
public final class m10 {
    @RecentlyNonNull
    @Deprecated
    public static <T> List<T> a() {
        return Collections.emptyList();
    }

    @RecentlyNonNull
    @Deprecated
    public static <T> List<T> b(@RecentlyNonNull T t) {
        return Collections.singletonList(t);
    }

    @RecentlyNonNull
    @Deprecated
    public static <T> List<T> c(@RecentlyNonNull T... tArr) {
        int length = tArr.length;
        if (length != 0) {
            if (length != 1) {
                return Collections.unmodifiableList(Arrays.asList(tArr));
            }
            return b(tArr[0]);
        }
        return a();
    }
}
