package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.extractor.metadata.scte35.PrivateCommand;
import androidx.media3.extractor.metadata.scte35.SpliceInsertCommand;
import androidx.media3.extractor.metadata.scte35.SpliceNullCommand;
import androidx.media3.extractor.metadata.scte35.SpliceScheduleCommand;
import androidx.media3.extractor.metadata.scte35.TimeSignalCommand;
import java.nio.ByteBuffer;

/* compiled from: SpliceInfoDecoder.java */
/* renamed from: rr3  reason: default package */
/* loaded from: classes.dex */
public final class rr3 extends kp3 {
    public final op2 a = new op2();
    public final np2 b = new np2();
    public h64 c;

    @Override // defpackage.kp3
    public Metadata b(n82 n82Var, ByteBuffer byteBuffer) {
        h64 h64Var = this.c;
        if (h64Var == null || n82Var.m0 != h64Var.e()) {
            h64 h64Var2 = new h64(n82Var.i0);
            this.c = h64Var2;
            h64Var2.a(n82Var.i0 - n82Var.m0);
        }
        byte[] array = byteBuffer.array();
        int limit = byteBuffer.limit();
        this.a.N(array, limit);
        this.b.o(array, limit);
        this.b.r(39);
        long h = (this.b.h(1) << 32) | this.b.h(32);
        this.b.r(20);
        int h2 = this.b.h(12);
        int h3 = this.b.h(8);
        Metadata.Entry entry = null;
        this.a.Q(14);
        if (h3 == 0) {
            entry = new SpliceNullCommand();
        } else if (h3 == 255) {
            entry = PrivateCommand.a(this.a, h2, h);
        } else if (h3 == 4) {
            entry = SpliceScheduleCommand.a(this.a);
        } else if (h3 == 5) {
            entry = SpliceInsertCommand.a(this.a, h, this.c);
        } else if (h3 == 6) {
            entry = TimeSignalCommand.a(this.a, h, this.c);
        }
        return entry == null ? new Metadata(new Metadata.Entry[0]) : new Metadata(entry);
    }
}
