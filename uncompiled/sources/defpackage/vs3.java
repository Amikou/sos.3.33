package defpackage;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: StateController.java */
/* renamed from: vs3  reason: default package */
/* loaded from: classes.dex */
public class vs3 {
    public static final us3 f = new us3();
    public static final Rect g = new Rect();
    public static final RectF h = new RectF();
    public static final PointF i = new PointF();
    public final Settings a;
    public final ts4 b;
    public final ba2 c;
    public boolean d = true;
    public float e;

    public vs3(Settings settings) {
        this.a = settings;
        this.b = new ts4(settings);
        this.c = new ba2(settings);
    }

    public final float a(float f2, float f3, float f4, float f5, float f6) {
        if (f6 == Utils.FLOAT_EPSILON) {
            return f2;
        }
        float f7 = (f2 + f3) * 0.5f;
        float f8 = (f7 >= f4 || f2 >= f3) ? (f7 <= f5 || f2 <= f3) ? 0.0f : (f7 - f5) / f6 : (f4 - f7) / f6;
        if (f8 == Utils.FLOAT_EPSILON) {
            return f2;
        }
        if (f8 > 1.0f) {
            f8 = 1.0f;
        }
        return f2 - (((float) Math.sqrt(f8)) * (f2 - f3));
    }

    public float b(float f2) {
        float f3 = this.e;
        return f3 > Utils.FLOAT_EPSILON ? f2 * f3 : f2;
    }

    public void c(us3 us3Var) {
        if (this.e > Utils.FLOAT_EPSILON) {
            us3Var.l(us3Var.f(), us3Var.g(), us3Var.h() * this.e, us3Var.e());
        }
    }

    public final float d(float f2, float f3, float f4, float f5, float f6) {
        if (f6 == 1.0f) {
            return f2;
        }
        float f7 = (f2 >= f4 || f2 >= f3) ? (f2 <= f5 || f2 <= f3) ? 0.0f : (f2 - f5) / ((f6 * f5) - f5) : (f4 - f2) / (f4 - (f4 / f6));
        return f7 == Utils.FLOAT_EPSILON ? f2 : f2 + (((float) Math.sqrt(f7)) * (f3 - f2));
    }

    public float e(us3 us3Var) {
        return this.b.e(us3Var).a();
    }

    public float f(us3 us3Var) {
        return this.b.e(us3Var).c();
    }

    public void g(us3 us3Var, RectF rectF) {
        this.c.i(us3Var).f(rectF);
    }

    public boolean h(us3 us3Var) {
        this.d = true;
        return m(us3Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x006f  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x007c  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0092  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00a1  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00a4  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00f2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean i(defpackage.us3 r22, defpackage.us3 r23, float r24, float r25, boolean r26, boolean r27, boolean r28) {
        /*
            Method dump skipped, instructions count: 314
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.vs3.i(us3, us3, float, float, boolean, boolean, boolean):boolean");
    }

    public us3 j(us3 us3Var, us3 us3Var2, float f2, float f3, boolean z, boolean z2, boolean z3) {
        us3 us3Var3 = f;
        us3Var3.m(us3Var);
        if (i(us3Var3, us3Var2, f2, f3, z, z2, z3)) {
            return us3Var3.b();
        }
        return null;
    }

    public void k(float f2) {
        this.e = f2;
    }

    public us3 l(us3 us3Var, float f2, float f3) {
        this.b.e(us3Var);
        float a = this.b.a();
        float g2 = this.a.g() > Utils.FLOAT_EPSILON ? this.a.g() : this.b.b();
        if (us3Var.h() < (a + g2) * 0.5f) {
            a = g2;
        }
        us3 b = us3Var.b();
        b.r(a, f2, f3);
        return b;
    }

    public boolean m(us3 us3Var) {
        Rect rect;
        boolean z = false;
        if (this.d) {
            us3Var.l(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.b.e(us3Var).a(), Utils.FLOAT_EPSILON);
            si1.b(us3Var, this.a, g);
            us3Var.o(rect.left, rect.top);
            if (!this.a.v() || !this.a.w()) {
                z = true;
            }
            this.d = z;
            return !z;
        }
        i(us3Var, us3Var, Float.NaN, Float.NaN, false, false, true);
        return false;
    }
}
