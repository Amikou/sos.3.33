package defpackage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.c;
import java.io.IOException;

/* compiled from: AdminNodeInfo.java */
/* renamed from: w9  reason: default package */
/* loaded from: classes3.dex */
public class w9 extends i83<a> {

    /* compiled from: AdminNodeInfo.java */
    /* renamed from: w9$a */
    /* loaded from: classes3.dex */
    public static class a {
        private String enode;
        private String id;
        private String ip;
        private String listenAddr;
        private String name;

        public a() {
        }

        public String getEnode() {
            return this.enode;
        }

        public String getId() {
            return this.id;
        }

        public String getIp() {
            return this.ip;
        }

        public String getListenAddr() {
            return this.listenAddr;
        }

        public String getName() {
            return this.name;
        }

        public a(String str, String str2, String str3, String str4, String str5) {
            this.enode = str;
            this.id = str2;
            this.ip = str3;
            this.listenAddr = str4;
            this.name = str5;
        }
    }

    /* compiled from: AdminNodeInfo.java */
    /* renamed from: w9$b */
    /* loaded from: classes3.dex */
    public static class b extends c<a> {
        private ObjectReader objectReader = ml2.getObjectReader();

        @Override // com.fasterxml.jackson.databind.c
        public a deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            if (jsonParser.u() != JsonToken.VALUE_NULL) {
                return (a) this.objectReader.readValue(jsonParser, a.class);
            }
            return null;
        }
    }

    @Override // defpackage.i83
    @JsonIgnoreProperties(ignoreUnknown = true)
    @com.fasterxml.jackson.databind.annotation.b(using = b.class)
    public void setResult(a aVar) {
        super.setResult((w9) aVar);
    }
}
