package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.extractor.metadata.dvbsi.AppInfoTable;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/* compiled from: AppInfoTableDecoder.java */
/* renamed from: qf  reason: default package */
/* loaded from: classes.dex */
public final class qf extends kp3 {
    public static Metadata c(np2 np2Var) {
        np2Var.r(12);
        int d = (np2Var.d() + np2Var.h(12)) - 4;
        np2Var.r(44);
        np2Var.s(np2Var.h(12));
        np2Var.r(16);
        ArrayList arrayList = new ArrayList();
        while (true) {
            String str = null;
            if (np2Var.d() >= d) {
                break;
            }
            np2Var.r(48);
            int h = np2Var.h(8);
            np2Var.r(4);
            int d2 = np2Var.d() + np2Var.h(12);
            String str2 = null;
            while (np2Var.d() < d2) {
                int h2 = np2Var.h(8);
                int h3 = np2Var.h(8);
                int d3 = np2Var.d() + h3;
                if (h2 == 2) {
                    int h4 = np2Var.h(16);
                    np2Var.r(8);
                    if (h4 != 3) {
                    }
                    while (np2Var.d() < d3) {
                        str = np2Var.l(np2Var.h(8), cy.a);
                        int h5 = np2Var.h(8);
                        for (int i = 0; i < h5; i++) {
                            np2Var.s(np2Var.h(8));
                        }
                    }
                } else if (h2 == 21) {
                    str2 = np2Var.l(h3, cy.a);
                }
                np2Var.p(d3 * 8);
            }
            np2Var.p(d2 * 8);
            if (str != null && str2 != null) {
                arrayList.add(new AppInfoTable(h, str + str2));
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata(arrayList);
    }

    @Override // defpackage.kp3
    public Metadata b(n82 n82Var, ByteBuffer byteBuffer) {
        if (byteBuffer.get() == 116) {
            return c(new np2(byteBuffer.array(), byteBuffer.limit()));
        }
        return null;
    }
}
