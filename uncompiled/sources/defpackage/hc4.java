package defpackage;

import zendesk.support.request.CellBase;

/* compiled from: TsUtil.java */
/* renamed from: hc4  reason: default package */
/* loaded from: classes.dex */
public final class hc4 {
    public static int a(byte[] bArr, int i, int i2) {
        while (i < i2 && bArr[i] != 71) {
            i++;
        }
        return i;
    }

    public static boolean b(byte[] bArr, int i, int i2, int i3) {
        int i4 = 0;
        for (int i5 = -4; i5 <= 4; i5++) {
            int i6 = (i5 * 188) + i3;
            if (i6 < i || i6 >= i2 || bArr[i6] != 71) {
                i4 = 0;
            } else {
                i4++;
                if (i4 == 5) {
                    return true;
                }
            }
        }
        return false;
    }

    public static long c(op2 op2Var, int i, int i2) {
        op2Var.P(i);
        if (op2Var.a() < 5) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        int n = op2Var.n();
        if ((8388608 & n) == 0 && ((2096896 & n) >> 8) == i2) {
            if (((n & 32) != 0) && op2Var.D() >= 7 && op2Var.a() >= 7) {
                if ((op2Var.D() & 16) == 16) {
                    byte[] bArr = new byte[6];
                    op2Var.j(bArr, 0, 6);
                    return d(bArr);
                }
            }
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static long d(byte[] bArr) {
        return ((bArr[0] & 255) << 25) | ((bArr[1] & 255) << 17) | ((bArr[2] & 255) << 9) | ((bArr[3] & 255) << 1) | ((255 & bArr[4]) >> 7);
    }
}
