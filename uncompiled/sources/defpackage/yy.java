package defpackage;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: CircularProgressDrawable.java */
/* renamed from: yy  reason: default package */
/* loaded from: classes.dex */
public class yy extends Drawable implements Animatable {
    public static final Interpolator k0 = new LinearInterpolator();
    public static final Interpolator l0 = new f21();
    public static final int[] m0 = {-16777216};
    public final c a;
    public float f0;
    public Resources g0;
    public Animator h0;
    public float i0;
    public boolean j0;

    /* compiled from: CircularProgressDrawable.java */
    /* renamed from: yy$a */
    /* loaded from: classes.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ c a;

        public a(c cVar) {
            this.a = cVar;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            yy.this.n(floatValue, this.a);
            yy.this.b(floatValue, this.a, false);
            yy.this.invalidateSelf();
        }
    }

    /* compiled from: CircularProgressDrawable.java */
    /* renamed from: yy$b */
    /* loaded from: classes.dex */
    public class b implements Animator.AnimatorListener {
        public final /* synthetic */ c a;

        public b(c cVar) {
            this.a = cVar;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
            yy.this.b(1.0f, this.a, true);
            this.a.A();
            this.a.l();
            yy yyVar = yy.this;
            if (yyVar.j0) {
                yyVar.j0 = false;
                animator.cancel();
                animator.setDuration(1332L);
                animator.start();
                this.a.x(false);
                return;
            }
            yyVar.i0 += 1.0f;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            yy.this.i0 = Utils.FLOAT_EPSILON;
        }
    }

    /* compiled from: CircularProgressDrawable.java */
    /* renamed from: yy$c */
    /* loaded from: classes.dex */
    public static class c {
        public final RectF a = new RectF();
        public final Paint b;
        public final Paint c;
        public final Paint d;
        public float e;
        public float f;
        public float g;
        public float h;
        public int[] i;
        public int j;
        public float k;
        public float l;
        public float m;
        public boolean n;
        public Path o;
        public float p;
        public float q;
        public int r;
        public int s;
        public int t;
        public int u;

        public c() {
            Paint paint = new Paint();
            this.b = paint;
            Paint paint2 = new Paint();
            this.c = paint2;
            Paint paint3 = new Paint();
            this.d = paint3;
            this.e = Utils.FLOAT_EPSILON;
            this.f = Utils.FLOAT_EPSILON;
            this.g = Utils.FLOAT_EPSILON;
            this.h = 5.0f;
            this.p = 1.0f;
            this.t = 255;
            paint.setStrokeCap(Paint.Cap.SQUARE);
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint2.setStyle(Paint.Style.FILL);
            paint2.setAntiAlias(true);
            paint3.setColor(0);
        }

        public void A() {
            this.k = this.e;
            this.l = this.f;
            this.m = this.g;
        }

        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.a;
            float f = this.q;
            float f2 = (this.h / 2.0f) + f;
            if (f <= Utils.FLOAT_EPSILON) {
                f2 = (Math.min(rect.width(), rect.height()) / 2.0f) - Math.max((this.r * this.p) / 2.0f, this.h / 2.0f);
            }
            rectF.set(rect.centerX() - f2, rect.centerY() - f2, rect.centerX() + f2, rect.centerY() + f2);
            float f3 = this.e;
            float f4 = this.g;
            float f5 = (f3 + f4) * 360.0f;
            float f6 = ((this.f + f4) * 360.0f) - f5;
            this.b.setColor(this.u);
            this.b.setAlpha(this.t);
            float f7 = this.h / 2.0f;
            rectF.inset(f7, f7);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.d);
            float f8 = -f7;
            rectF.inset(f8, f8);
            canvas.drawArc(rectF, f5, f6, false, this.b);
            b(canvas, f5, f6, rectF);
        }

        public void b(Canvas canvas, float f, float f2, RectF rectF) {
            if (this.n) {
                Path path = this.o;
                if (path == null) {
                    Path path2 = new Path();
                    this.o = path2;
                    path2.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    path.reset();
                }
                this.o.moveTo(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
                this.o.lineTo(this.r * this.p, Utils.FLOAT_EPSILON);
                Path path3 = this.o;
                float f3 = this.p;
                path3.lineTo((this.r * f3) / 2.0f, this.s * f3);
                this.o.offset(((Math.min(rectF.width(), rectF.height()) / 2.0f) + rectF.centerX()) - ((this.r * this.p) / 2.0f), rectF.centerY() + (this.h / 2.0f));
                this.o.close();
                this.c.setColor(this.u);
                this.c.setAlpha(this.t);
                canvas.save();
                canvas.rotate(f + f2, rectF.centerX(), rectF.centerY());
                canvas.drawPath(this.o, this.c);
                canvas.restore();
            }
        }

        public int c() {
            return this.t;
        }

        public float d() {
            return this.f;
        }

        public int e() {
            return this.i[f()];
        }

        public int f() {
            return (this.j + 1) % this.i.length;
        }

        public float g() {
            return this.e;
        }

        public int h() {
            return this.i[this.j];
        }

        public float i() {
            return this.l;
        }

        public float j() {
            return this.m;
        }

        public float k() {
            return this.k;
        }

        public void l() {
            t(f());
        }

        public void m() {
            this.k = Utils.FLOAT_EPSILON;
            this.l = Utils.FLOAT_EPSILON;
            this.m = Utils.FLOAT_EPSILON;
            y(Utils.FLOAT_EPSILON);
            v(Utils.FLOAT_EPSILON);
            w(Utils.FLOAT_EPSILON);
        }

        public void n(int i) {
            this.t = i;
        }

        public void o(float f, float f2) {
            this.r = (int) f;
            this.s = (int) f2;
        }

        public void p(float f) {
            if (f != this.p) {
                this.p = f;
            }
        }

        public void q(float f) {
            this.q = f;
        }

        public void r(int i) {
            this.u = i;
        }

        public void s(ColorFilter colorFilter) {
            this.b.setColorFilter(colorFilter);
        }

        public void t(int i) {
            this.j = i;
            this.u = this.i[i];
        }

        public void u(int[] iArr) {
            this.i = iArr;
            t(0);
        }

        public void v(float f) {
            this.f = f;
        }

        public void w(float f) {
            this.g = f;
        }

        public void x(boolean z) {
            if (this.n != z) {
                this.n = z;
            }
        }

        public void y(float f) {
            this.e = f;
        }

        public void z(float f) {
            this.h = f;
            this.b.setStrokeWidth(f);
        }
    }

    public yy(Context context) {
        this.g0 = ((Context) du2.e(context)).getResources();
        c cVar = new c();
        this.a = cVar;
        cVar.u(m0);
        k(2.5f);
        m();
    }

    public final void a(float f, c cVar) {
        n(f, cVar);
        cVar.y(cVar.k() + (((cVar.i() - 0.01f) - cVar.k()) * f));
        cVar.v(cVar.i());
        cVar.w(cVar.j() + ((((float) (Math.floor(cVar.j() / 0.8f) + 1.0d)) - cVar.j()) * f));
    }

    public void b(float f, c cVar, boolean z) {
        float interpolation;
        float f2;
        if (this.j0) {
            a(f, cVar);
        } else if (f != 1.0f || z) {
            float j = cVar.j();
            if (f < 0.5f) {
                interpolation = cVar.k();
                f2 = (l0.getInterpolation(f / 0.5f) * 0.79f) + 0.01f + interpolation;
            } else {
                float k = cVar.k() + 0.79f;
                interpolation = k - (((1.0f - l0.getInterpolation((f - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
                f2 = k;
            }
            cVar.y(interpolation);
            cVar.v(f2);
            cVar.w(j + (0.20999998f * f));
            h((f + this.i0) * 216.0f);
        }
    }

    public final int c(float f, int i, int i2) {
        int i3 = (i >> 24) & 255;
        int i4 = (i >> 16) & 255;
        int i5 = (i >> 8) & 255;
        int i6 = i & 255;
        return ((i3 + ((int) ((((i2 >> 24) & 255) - i3) * f))) << 24) | ((i4 + ((int) ((((i2 >> 16) & 255) - i4) * f))) << 16) | ((i5 + ((int) ((((i2 >> 8) & 255) - i5) * f))) << 8) | (i6 + ((int) (f * ((i2 & 255) - i6))));
    }

    public void d(boolean z) {
        this.a.x(z);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.f0, bounds.exactCenterX(), bounds.exactCenterY());
        this.a.a(canvas, bounds);
        canvas.restore();
    }

    public void e(float f) {
        this.a.p(f);
        invalidateSelf();
    }

    public void f(int... iArr) {
        this.a.u(iArr);
        this.a.t(0);
        invalidateSelf();
    }

    public void g(float f) {
        this.a.w(f);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.a.c();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public final void h(float f) {
        this.f0 = f;
    }

    public final void i(float f, float f2, float f3, float f4) {
        c cVar = this.a;
        float f5 = this.g0.getDisplayMetrics().density;
        cVar.z(f2 * f5);
        cVar.q(f * f5);
        cVar.t(0);
        cVar.o(f3 * f5, f4 * f5);
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.h0.isRunning();
    }

    public void j(float f, float f2) {
        this.a.y(f);
        this.a.v(f2);
        invalidateSelf();
    }

    public void k(float f) {
        this.a.z(f);
        invalidateSelf();
    }

    public void l(int i) {
        if (i == 0) {
            i(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            i(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    public final void m() {
        c cVar = this.a;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(Utils.FLOAT_EPSILON, 1.0f);
        ofFloat.addUpdateListener(new a(cVar));
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(k0);
        ofFloat.addListener(new b(cVar));
        this.h0 = ofFloat;
    }

    public void n(float f, c cVar) {
        if (f > 0.75f) {
            cVar.r(c((f - 0.75f) / 0.25f, cVar.h(), cVar.e()));
        } else {
            cVar.r(cVar.h());
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.a.n(i);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.s(colorFilter);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        this.h0.cancel();
        this.a.A();
        if (this.a.d() != this.a.g()) {
            this.j0 = true;
            this.h0.setDuration(666L);
            this.h0.start();
            return;
        }
        this.a.t(0);
        this.a.m();
        this.h0.setDuration(1332L);
        this.h0.start();
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        this.h0.cancel();
        h(Utils.FLOAT_EPSILON);
        this.a.x(false);
        this.a.t(0);
        this.a.m();
        invalidateSelf();
    }
}
