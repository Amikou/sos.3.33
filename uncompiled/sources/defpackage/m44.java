package defpackage;

import com.google.crypto.tink.shaded.protobuf.ByteString;

/* compiled from: TextFormatEscaper.java */
/* renamed from: m44  reason: default package */
/* loaded from: classes2.dex */
public final class m44 {

    /* compiled from: TextFormatEscaper.java */
    /* renamed from: m44$a */
    /* loaded from: classes2.dex */
    public class a implements b {
        public final /* synthetic */ ByteString a;

        public a(ByteString byteString) {
            this.a = byteString;
        }

        @Override // defpackage.m44.b
        public byte a(int i) {
            return this.a.byteAt(i);
        }

        @Override // defpackage.m44.b
        public int size() {
            return this.a.size();
        }
    }

    /* compiled from: TextFormatEscaper.java */
    /* renamed from: m44$b */
    /* loaded from: classes2.dex */
    public interface b {
        byte a(int i);

        int size();
    }

    public static String a(b bVar) {
        StringBuilder sb = new StringBuilder(bVar.size());
        for (int i = 0; i < bVar.size(); i++) {
            byte a2 = bVar.a(i);
            if (a2 == 34) {
                sb.append("\\\"");
            } else if (a2 == 39) {
                sb.append("\\'");
            } else if (a2 != 92) {
                switch (a2) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (a2 >= 32 && a2 <= 126) {
                            sb.append((char) a2);
                            continue;
                        } else {
                            sb.append('\\');
                            sb.append((char) (((a2 >>> 6) & 3) + 48));
                            sb.append((char) (((a2 >>> 3) & 7) + 48));
                            sb.append((char) ((a2 & 7) + 48));
                            break;
                        }
                        break;
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }

    public static String b(ByteString byteString) {
        return a(new a(byteString));
    }

    public static String c(String str) {
        return b(ByteString.copyFromUtf8(str));
    }
}
