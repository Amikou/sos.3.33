package defpackage;

import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Cancellable.kt */
/* renamed from: sv */
/* loaded from: classes2.dex */
public final class sv {
    public static final void a(q70<? super te4> q70Var, q70<?> q70Var2) {
        try {
            q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(q70Var);
            Result.a aVar = Result.Companion;
            op0.c(c, Result.m52constructorimpl(te4.a), null, 2, null);
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            q70Var2.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }
    }

    public static final <T> void b(tc1<? super q70<? super T>, ? extends Object> tc1Var, q70<? super T> q70Var) {
        try {
            q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(IntrinsicsKt__IntrinsicsJvmKt.a(tc1Var, q70Var));
            Result.a aVar = Result.Companion;
            op0.c(c, Result.m52constructorimpl(te4.a), null, 2, null);
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            q70Var.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }
    }

    public static final <R, T> void c(hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var, R r, q70<? super T> q70Var, tc1<? super Throwable, te4> tc1Var) {
        try {
            q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(IntrinsicsKt__IntrinsicsJvmKt.b(hd1Var, r, q70Var));
            Result.a aVar = Result.Companion;
            op0.b(c, Result.m52constructorimpl(te4.a), tc1Var);
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            q70Var.resumeWith(Result.m52constructorimpl(o83.a(th)));
        }
    }

    public static /* synthetic */ void d(hd1 hd1Var, Object obj, q70 q70Var, tc1 tc1Var, int i, Object obj2) {
        if ((i & 4) != 0) {
            tc1Var = null;
        }
        c(hd1Var, obj, q70Var, tc1Var);
    }
}
