package defpackage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.io.AccessDeniedException;
import kotlin.io.FileWalkDirection;

/* compiled from: FileTreeWalk.kt */
/* renamed from: w31  reason: default package */
/* loaded from: classes2.dex */
public final class w31 implements ol3<File> {
    public final File a;
    public final FileWalkDirection b;
    public final tc1<File, Boolean> c;
    public final tc1<File, te4> d;
    public final hd1<File, IOException, te4> e;
    public final int f;

    /* compiled from: FileTreeWalk.kt */
    /* renamed from: w31$a */
    /* loaded from: classes2.dex */
    public static abstract class a extends c {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(File file) {
            super(file);
            fs1.f(file, "rootDir");
        }
    }

    /* compiled from: FileTreeWalk.kt */
    /* renamed from: w31$b */
    /* loaded from: classes2.dex */
    public final class b extends b5<File> {
        public final ArrayDeque<c> g0;

        /* compiled from: FileTreeWalk.kt */
        /* renamed from: w31$b$a */
        /* loaded from: classes2.dex */
        public final class a extends a {
            public boolean b;
            public File[] c;
            public int d;
            public boolean e;
            public final /* synthetic */ b f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(b bVar, File file) {
                super(file);
                fs1.f(file, "rootDir");
                this.f = bVar;
            }

            @Override // defpackage.w31.c
            public File b() {
                if (!this.e && this.c == null) {
                    tc1 tc1Var = w31.this.c;
                    if (tc1Var != null && !((Boolean) tc1Var.invoke(a())).booleanValue()) {
                        return null;
                    }
                    File[] listFiles = a().listFiles();
                    this.c = listFiles;
                    if (listFiles == null) {
                        hd1 hd1Var = w31.this.e;
                        if (hd1Var != null) {
                            te4 te4Var = (te4) hd1Var.invoke(a(), new AccessDeniedException(a(), null, "Cannot list files in a directory", 2, null));
                        }
                        this.e = true;
                    }
                }
                File[] fileArr = this.c;
                if (fileArr != null) {
                    int i = this.d;
                    fs1.d(fileArr);
                    if (i < fileArr.length) {
                        File[] fileArr2 = this.c;
                        fs1.d(fileArr2);
                        int i2 = this.d;
                        this.d = i2 + 1;
                        return fileArr2[i2];
                    }
                }
                if (this.b) {
                    tc1 tc1Var2 = w31.this.d;
                    if (tc1Var2 != null) {
                        te4 te4Var2 = (te4) tc1Var2.invoke(a());
                    }
                    return null;
                }
                this.b = true;
                return a();
            }
        }

        /* compiled from: FileTreeWalk.kt */
        /* renamed from: w31$b$b  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public final class C0288b extends c {
            public boolean b;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0288b(b bVar, File file) {
                super(file);
                fs1.f(file, "rootFile");
            }

            @Override // defpackage.w31.c
            public File b() {
                if (this.b) {
                    return null;
                }
                this.b = true;
                return a();
            }
        }

        /* compiled from: FileTreeWalk.kt */
        /* renamed from: w31$b$c */
        /* loaded from: classes2.dex */
        public final class c extends a {
            public boolean b;
            public File[] c;
            public int d;
            public final /* synthetic */ b e;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(b bVar, File file) {
                super(file);
                fs1.f(file, "rootDir");
                this.e = bVar;
            }

            /* JADX WARN: Code restructure failed: missing block: B:30:0x0085, code lost:
                if (r0.length == 0) goto L30;
             */
            @Override // defpackage.w31.c
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public java.io.File b() {
                /*
                    r10 = this;
                    boolean r0 = r10.b
                    r1 = 0
                    if (r0 != 0) goto L28
                    w31$b r0 = r10.e
                    w31 r0 = defpackage.w31.this
                    tc1 r0 = defpackage.w31.d(r0)
                    if (r0 == 0) goto L20
                    java.io.File r2 = r10.a()
                    java.lang.Object r0 = r0.invoke(r2)
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L20
                    return r1
                L20:
                    r0 = 1
                    r10.b = r0
                    java.io.File r0 = r10.a()
                    return r0
                L28:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L4a
                    int r2 = r10.d
                    defpackage.fs1.d(r0)
                    int r0 = r0.length
                    if (r2 >= r0) goto L35
                    goto L4a
                L35:
                    w31$b r0 = r10.e
                    w31 r0 = defpackage.w31.this
                    tc1 r0 = defpackage.w31.f(r0)
                    if (r0 == 0) goto L49
                    java.io.File r2 = r10.a()
                    java.lang.Object r0 = r0.invoke(r2)
                    te4 r0 = (defpackage.te4) r0
                L49:
                    return r1
                L4a:
                    java.io.File[] r0 = r10.c
                    if (r0 != 0) goto L9c
                    java.io.File r0 = r10.a()
                    java.io.File[] r0 = r0.listFiles()
                    r10.c = r0
                    if (r0 != 0) goto L7d
                    w31$b r0 = r10.e
                    w31 r0 = defpackage.w31.this
                    hd1 r0 = defpackage.w31.e(r0)
                    if (r0 == 0) goto L7d
                    java.io.File r2 = r10.a()
                    kotlin.io.AccessDeniedException r9 = new kotlin.io.AccessDeniedException
                    java.io.File r4 = r10.a()
                    r5 = 0
                    r7 = 2
                    r8 = 0
                    java.lang.String r6 = "Cannot list files in a directory"
                    r3 = r9
                    r3.<init>(r4, r5, r6, r7, r8)
                    java.lang.Object r0 = r0.invoke(r2, r9)
                    te4 r0 = (defpackage.te4) r0
                L7d:
                    java.io.File[] r0 = r10.c
                    if (r0 == 0) goto L87
                    defpackage.fs1.d(r0)
                    int r0 = r0.length
                    if (r0 != 0) goto L9c
                L87:
                    w31$b r0 = r10.e
                    w31 r0 = defpackage.w31.this
                    tc1 r0 = defpackage.w31.f(r0)
                    if (r0 == 0) goto L9b
                    java.io.File r2 = r10.a()
                    java.lang.Object r0 = r0.invoke(r2)
                    te4 r0 = (defpackage.te4) r0
                L9b:
                    return r1
                L9c:
                    java.io.File[] r0 = r10.c
                    defpackage.fs1.d(r0)
                    int r1 = r10.d
                    int r2 = r1 + 1
                    r10.d = r2
                    r0 = r0[r1]
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: defpackage.w31.b.c.b():java.io.File");
            }
        }

        public b() {
            ArrayDeque<c> arrayDeque = new ArrayDeque<>();
            this.g0 = arrayDeque;
            if (w31.this.a.isDirectory()) {
                arrayDeque.push(f(w31.this.a));
            } else if (w31.this.a.isFile()) {
                arrayDeque.push(new C0288b(this, w31.this.a));
            } else {
                b();
            }
        }

        @Override // defpackage.b5
        public void a() {
            File g = g();
            if (g != null) {
                c(g);
            } else {
                b();
            }
        }

        public final a f(File file) {
            int i = x31.a[w31.this.b.ordinal()];
            if (i != 1) {
                if (i == 2) {
                    return new a(this, file);
                }
                throw new NoWhenBranchMatchedException();
            }
            return new c(this, file);
        }

        public final File g() {
            File b;
            while (true) {
                c peek = this.g0.peek();
                if (peek == null) {
                    return null;
                }
                b = peek.b();
                if (b == null) {
                    this.g0.pop();
                } else if (fs1.b(b, peek.a()) || !b.isDirectory() || this.g0.size() >= w31.this.f) {
                    break;
                } else {
                    this.g0.push(f(b));
                }
            }
            return b;
        }
    }

    /* compiled from: FileTreeWalk.kt */
    /* renamed from: w31$c */
    /* loaded from: classes2.dex */
    public static abstract class c {
        public final File a;

        public c(File file) {
            fs1.f(file, "root");
            this.a = file;
        }

        public final File a() {
            return this.a;
        }

        public abstract File b();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public w31(File file, FileWalkDirection fileWalkDirection, tc1<? super File, Boolean> tc1Var, tc1<? super File, te4> tc1Var2, hd1<? super File, ? super IOException, te4> hd1Var, int i) {
        this.a = file;
        this.b = fileWalkDirection;
        this.c = tc1Var;
        this.d = tc1Var2;
        this.e = hd1Var;
        this.f = i;
    }

    public final w31 h(hd1<? super File, ? super IOException, te4> hd1Var) {
        fs1.f(hd1Var, "function");
        return new w31(this.a, this.b, this.c, this.d, hd1Var, this.f);
    }

    @Override // defpackage.ol3
    public Iterator<File> iterator() {
        return new b();
    }

    public /* synthetic */ w31(File file, FileWalkDirection fileWalkDirection, tc1 tc1Var, tc1 tc1Var2, hd1 hd1Var, int i, int i2, qi0 qi0Var) {
        this(file, (i2 & 2) != 0 ? FileWalkDirection.TOP_DOWN : fileWalkDirection, tc1Var, tc1Var2, hd1Var, (i2 & 32) != 0 ? Integer.MAX_VALUE : i);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public w31(File file, FileWalkDirection fileWalkDirection) {
        this(file, fileWalkDirection, null, null, null, 0, 32, null);
        fs1.f(file, "start");
        fs1.f(fileWalkDirection, "direction");
    }
}
