package defpackage;

import java.util.Comparator;
import java.util.Objects;

/* compiled from: Comparisons.kt */
/* renamed from: l30  reason: default package */
/* loaded from: classes2.dex */
public class l30 {
    public static final <T extends Comparable<?>> int a(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    public static final <T extends Comparable<? super T>> Comparator<T> b() {
        vd2 vd2Var = vd2.a;
        Objects.requireNonNull(vd2Var, "null cannot be cast to non-null type kotlin.Comparator<T> /* = java.util.Comparator<T> */");
        return vd2Var;
    }
}
