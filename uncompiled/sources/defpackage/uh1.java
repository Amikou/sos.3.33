package defpackage;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.charts.LineChart;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: GraphDialogBinding.java */
/* renamed from: uh1  reason: default package */
/* loaded from: classes2.dex */
public final class uh1 {
    public final ImageView a;
    public final AppCompatButton b;
    public final AppCompatButton c;
    public final AppCompatButton d;
    public final AppCompatButton e;
    public final AppCompatButton f;
    public final AppCompatButton g;
    public final CandleStickChart h;
    public final MaterialCheckBox i;
    public final MaterialCheckBox j;
    public final MaterialButton k;
    public final ImageView l;
    public final LineChart m;
    public final TextView n;
    public final TextView o;
    public final TextView p;
    public final TextView q;
    public final TextView r;
    public final TextView s;
    public final WebView t;

    public uh1(ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, AppCompatButton appCompatButton, AppCompatButton appCompatButton2, AppCompatButton appCompatButton3, AppCompatButton appCompatButton4, AppCompatButton appCompatButton5, AppCompatButton appCompatButton6, CandleStickChart candleStickChart, MaterialCheckBox materialCheckBox, MaterialCheckBox materialCheckBox2, MaterialButton materialButton, LinearLayout linearLayout, ImageView imageView3, LinearLayout linearLayout2, RelativeLayout relativeLayout, LineChart lineChart, LinearLayout linearLayout3, LinearLayout linearLayout4, LinearLayout linearLayout5, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, WebView webView) {
        this.a = imageView;
        this.b = appCompatButton;
        this.c = appCompatButton2;
        this.d = appCompatButton3;
        this.e = appCompatButton4;
        this.f = appCompatButton5;
        this.g = appCompatButton6;
        this.h = candleStickChart;
        this.i = materialCheckBox;
        this.j = materialCheckBox2;
        this.k = materialButton;
        this.l = imageView3;
        this.m = lineChart;
        this.n = textView;
        this.o = textView2;
        this.p = textView3;
        this.q = textView5;
        this.r = textView6;
        this.s = textView8;
        this.t = webView;
    }

    public static uh1 a(View view) {
        int i = R.id.IvPercentDialog;
        ImageView imageView = (ImageView) ai4.a(view, R.id.IvPercentDialog);
        if (imageView != null) {
            i = R.id.IvPercentDialog_1;
            ImageView imageView2 = (ImageView) ai4.a(view, R.id.IvPercentDialog_1);
            if (imageView2 != null) {
                i = R.id.btn12Hours;
                AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btn12Hours);
                if (appCompatButton != null) {
                    i = R.id.btn1Day;
                    AppCompatButton appCompatButton2 = (AppCompatButton) ai4.a(view, R.id.btn1Day);
                    if (appCompatButton2 != null) {
                        i = R.id.btn1Month;
                        AppCompatButton appCompatButton3 = (AppCompatButton) ai4.a(view, R.id.btn1Month);
                        if (appCompatButton3 != null) {
                            i = R.id.btn1Week;
                            AppCompatButton appCompatButton4 = (AppCompatButton) ai4.a(view, R.id.btn1Week);
                            if (appCompatButton4 != null) {
                                i = R.id.btn3Months;
                                AppCompatButton appCompatButton5 = (AppCompatButton) ai4.a(view, R.id.btn3Months);
                                if (appCompatButton5 != null) {
                                    i = R.id.btn6Months;
                                    AppCompatButton appCompatButton6 = (AppCompatButton) ai4.a(view, R.id.btn6Months);
                                    if (appCompatButton6 != null) {
                                        i = R.id.candle_stick_chart;
                                        CandleStickChart candleStickChart = (CandleStickChart) ai4.a(view, R.id.candle_stick_chart);
                                        if (candleStickChart != null) {
                                            i = R.id.chk_candle_graph_view;
                                            MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chk_candle_graph_view);
                                            if (materialCheckBox != null) {
                                                i = R.id.chk_full_screen;
                                                MaterialCheckBox materialCheckBox2 = (MaterialCheckBox) ai4.a(view, R.id.chk_full_screen);
                                                if (materialCheckBox2 != null) {
                                                    i = R.id.dialog_cross;
                                                    MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                                                    if (materialButton != null) {
                                                        i = R.id.header;
                                                        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.header);
                                                        if (linearLayout != null) {
                                                            i = R.id.ivDialog;
                                                            ImageView imageView3 = (ImageView) ai4.a(view, R.id.ivDialog);
                                                            if (imageView3 != null) {
                                                                i = R.id.l1;
                                                                LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.l1);
                                                                if (linearLayout2 != null) {
                                                                    i = R.id.lSafemoon;
                                                                    RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.lSafemoon);
                                                                    if (relativeLayout != null) {
                                                                        i = R.id.line_stick_chart;
                                                                        LineChart lineChart = (LineChart) ai4.a(view, R.id.line_stick_chart);
                                                                        if (lineChart != null) {
                                                                            i = R.id.llCaption;
                                                                            LinearLayout linearLayout3 = (LinearLayout) ai4.a(view, R.id.llCaption);
                                                                            if (linearLayout3 != null) {
                                                                                i = R.id.llStatus;
                                                                                LinearLayout linearLayout4 = (LinearLayout) ai4.a(view, R.id.llStatus);
                                                                                if (linearLayout4 != null) {
                                                                                    i = R.id.llStatus_1;
                                                                                    LinearLayout linearLayout5 = (LinearLayout) ai4.a(view, R.id.llStatus_1);
                                                                                    if (linearLayout5 != null) {
                                                                                        i = R.id.providerLabel;
                                                                                        TextView textView = (TextView) ai4.a(view, R.id.providerLabel);
                                                                                        if (textView != null) {
                                                                                            i = R.id.providerLink;
                                                                                            TextView textView2 = (TextView) ai4.a(view, R.id.providerLink);
                                                                                            if (textView2 != null) {
                                                                                                i = R.id.tvBalanceDialog;
                                                                                                TextView textView3 = (TextView) ai4.a(view, R.id.tvBalanceDialog);
                                                                                                if (textView3 != null) {
                                                                                                    i = R.id.tvBalanceDialog_1;
                                                                                                    TextView textView4 = (TextView) ai4.a(view, R.id.tvBalanceDialog_1);
                                                                                                    if (textView4 != null) {
                                                                                                        i = R.id.tvNameDialog;
                                                                                                        TextView textView5 = (TextView) ai4.a(view, R.id.tvNameDialog);
                                                                                                        if (textView5 != null) {
                                                                                                            i = R.id.tvPercentDialog;
                                                                                                            TextView textView6 = (TextView) ai4.a(view, R.id.tvPercentDialog);
                                                                                                            if (textView6 != null) {
                                                                                                                i = R.id.tvPercentDialog_1;
                                                                                                                TextView textView7 = (TextView) ai4.a(view, R.id.tvPercentDialog_1);
                                                                                                                if (textView7 != null) {
                                                                                                                    i = R.id.tvPriceDialog;
                                                                                                                    TextView textView8 = (TextView) ai4.a(view, R.id.tvPriceDialog);
                                                                                                                    if (textView8 != null) {
                                                                                                                        i = R.id.web_view_chart;
                                                                                                                        WebView webView = (WebView) ai4.a(view, R.id.web_view_chart);
                                                                                                                        if (webView != null) {
                                                                                                                            return new uh1((ConstraintLayout) view, imageView, imageView2, appCompatButton, appCompatButton2, appCompatButton3, appCompatButton4, appCompatButton5, appCompatButton6, candleStickChart, materialCheckBox, materialCheckBox2, materialButton, linearLayout, imageView3, linearLayout2, relativeLayout, lineChart, linearLayout3, linearLayout4, linearLayout5, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, webView);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
