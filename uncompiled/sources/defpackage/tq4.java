package defpackage;

import androidx.work.BackoffPolicy;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/* compiled from: WorkSpec.java */
/* renamed from: tq4  reason: default package */
/* loaded from: classes.dex */
public final class tq4 {
    public String a;
    public WorkInfo.State b;
    public String c;
    public String d;
    public androidx.work.b e;
    public androidx.work.b f;
    public long g;
    public long h;
    public long i;
    public h60 j;
    public int k;
    public BackoffPolicy l;
    public long m;
    public long n;
    public long o;
    public long p;
    public boolean q;
    public OutOfQuotaPolicy r;

    /* compiled from: WorkSpec.java */
    /* renamed from: tq4$a */
    /* loaded from: classes.dex */
    public class a implements ud1<List<c>, List<WorkInfo>> {
        @Override // defpackage.ud1
        /* renamed from: a */
        public List<WorkInfo> apply(List<c> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (c cVar : list) {
                arrayList.add(cVar.a());
            }
            return arrayList;
        }
    }

    /* compiled from: WorkSpec.java */
    /* renamed from: tq4$b */
    /* loaded from: classes.dex */
    public static class b {
        public String a;
        public WorkInfo.State b;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof b) {
                b bVar = (b) obj;
                if (this.b != bVar.b) {
                    return false;
                }
                return this.a.equals(bVar.a);
            }
            return false;
        }

        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }
    }

    /* compiled from: WorkSpec.java */
    /* renamed from: tq4$c */
    /* loaded from: classes.dex */
    public static class c {
        public String a;
        public WorkInfo.State b;
        public androidx.work.b c;
        public int d;
        public List<String> e;
        public List<androidx.work.b> f;

        public WorkInfo a() {
            androidx.work.b bVar;
            List<androidx.work.b> list = this.f;
            if (list != null && !list.isEmpty()) {
                bVar = this.f.get(0);
            } else {
                bVar = androidx.work.b.c;
            }
            return new WorkInfo(UUID.fromString(this.a), this.b, this.c, this.e, bVar, this.d);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                if (this.d != cVar.d) {
                    return false;
                }
                String str = this.a;
                if (str == null ? cVar.a == null : str.equals(cVar.a)) {
                    if (this.b != cVar.b) {
                        return false;
                    }
                    androidx.work.b bVar = this.c;
                    if (bVar == null ? cVar.c == null : bVar.equals(cVar.c)) {
                        List<String> list = this.e;
                        if (list == null ? cVar.e == null : list.equals(cVar.e)) {
                            List<androidx.work.b> list2 = this.f;
                            List<androidx.work.b> list3 = cVar.f;
                            return list2 != null ? list2.equals(list3) : list3 == null;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            String str = this.a;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            WorkInfo.State state = this.b;
            int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
            androidx.work.b bVar = this.c;
            int hashCode3 = (((hashCode2 + (bVar != null ? bVar.hashCode() : 0)) * 31) + this.d) * 31;
            List<String> list = this.e;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            List<androidx.work.b> list2 = this.f;
            return hashCode4 + (list2 != null ? list2.hashCode() : 0);
        }
    }

    static {
        v12.f("WorkSpec");
        new a();
    }

    public tq4(String str, String str2) {
        this.b = WorkInfo.State.ENQUEUED;
        androidx.work.b bVar = androidx.work.b.c;
        this.e = bVar;
        this.f = bVar;
        this.j = h60.i;
        this.l = BackoffPolicy.EXPONENTIAL;
        this.m = 30000L;
        this.p = -1L;
        this.r = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        this.a = str;
        this.c = str2;
    }

    public long a() {
        long scalb;
        if (c()) {
            if (this.l == BackoffPolicy.LINEAR) {
                scalb = this.m * this.k;
            } else {
                scalb = Math.scalb((float) this.m, this.k - 1);
            }
            return this.n + Math.min(18000000L, scalb);
        }
        if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.n;
            long j2 = j == 0 ? currentTimeMillis + this.g : j;
            long j3 = this.i;
            long j4 = this.h;
            if (j3 != j4) {
                return j2 + j4 + (j == 0 ? j3 * (-1) : 0L);
            }
            return j2 + (j != 0 ? j4 : 0L);
        }
        long j5 = this.n;
        if (j5 == 0) {
            j5 = System.currentTimeMillis();
        }
        return j5 + this.g;
    }

    public boolean b() {
        return !h60.i.equals(this.j);
    }

    public boolean c() {
        return this.b == WorkInfo.State.ENQUEUED && this.k > 0;
    }

    public boolean d() {
        return this.h != 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || tq4.class != obj.getClass()) {
            return false;
        }
        tq4 tq4Var = (tq4) obj;
        if (this.g == tq4Var.g && this.h == tq4Var.h && this.i == tq4Var.i && this.k == tq4Var.k && this.m == tq4Var.m && this.n == tq4Var.n && this.o == tq4Var.o && this.p == tq4Var.p && this.q == tq4Var.q && this.a.equals(tq4Var.a) && this.b == tq4Var.b && this.c.equals(tq4Var.c)) {
            String str = this.d;
            if (str == null ? tq4Var.d == null : str.equals(tq4Var.d)) {
                return this.e.equals(tq4Var.e) && this.f.equals(tq4Var.f) && this.j.equals(tq4Var.j) && this.l == tq4Var.l && this.r == tq4Var.r;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = ((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31;
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j = this.g;
        long j2 = this.h;
        long j3 = this.i;
        long j4 = this.m;
        long j5 = this.n;
        long j6 = this.o;
        long j7 = this.p;
        return ((((((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + this.j.hashCode()) * 31) + this.k) * 31) + this.l.hashCode()) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + (this.q ? 1 : 0)) * 31) + this.r.hashCode();
    }

    public String toString() {
        return "{WorkSpec: " + this.a + "}";
    }

    public tq4(tq4 tq4Var) {
        this.b = WorkInfo.State.ENQUEUED;
        androidx.work.b bVar = androidx.work.b.c;
        this.e = bVar;
        this.f = bVar;
        this.j = h60.i;
        this.l = BackoffPolicy.EXPONENTIAL;
        this.m = 30000L;
        this.p = -1L;
        this.r = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        this.a = tq4Var.a;
        this.c = tq4Var.c;
        this.b = tq4Var.b;
        this.d = tq4Var.d;
        this.e = new androidx.work.b(tq4Var.e);
        this.f = new androidx.work.b(tq4Var.f);
        this.g = tq4Var.g;
        this.h = tq4Var.h;
        this.i = tq4Var.i;
        this.j = new h60(tq4Var.j);
        this.k = tq4Var.k;
        this.l = tq4Var.l;
        this.m = tq4Var.m;
        this.n = tq4Var.n;
        this.o = tq4Var.o;
        this.p = tq4Var.p;
        this.q = tq4Var.q;
        this.r = tq4Var.r;
    }
}
