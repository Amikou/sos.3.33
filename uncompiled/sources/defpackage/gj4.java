package defpackage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ViewModelStore.java */
/* renamed from: gj4  reason: default package */
/* loaded from: classes.dex */
public class gj4 {
    public final HashMap<String, dj4> a = new HashMap<>();

    public final void a() {
        for (dj4 dj4Var : this.a.values()) {
            dj4Var.clear();
        }
        this.a.clear();
    }

    public final dj4 b(String str) {
        return this.a.get(str);
    }

    public Set<String> c() {
        return new HashSet(this.a.keySet());
    }

    public final void d(String str, dj4 dj4Var) {
        dj4 put = this.a.put(str, dj4Var);
        if (put != null) {
            put.onCleared();
        }
    }
}
