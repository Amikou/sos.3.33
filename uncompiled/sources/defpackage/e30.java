package defpackage;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Insets;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.view.WindowMetrics;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.media3.common.PlaybackException;
import com.bumptech.glide.a;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import defpackage.u21;
import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.Pair;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.wallets.Wallet;
import net.safemoon.androidwallet.ui.wallet.Convert;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: Common.kt */
/* renamed from: e30  reason: default package */
/* loaded from: classes2.dex */
public final class e30 {
    public static Toast a;
    public static Toast b;

    public static final int A(Activity activity) {
        fs1.f(activity, "<this>");
        if (Build.VERSION.SDK_INT >= 30) {
            WindowMetrics currentWindowMetrics = activity.getWindowManager().getCurrentWindowMetrics();
            fs1.e(currentWindowMetrics, "windowManager.currentWindowMetrics");
            Insets insetsIgnoringVisibility = currentWindowMetrics.getWindowInsets().getInsetsIgnoringVisibility(WindowInsets.Type.systemBars());
            fs1.e(insetsIgnoringVisibility, "windowMetrics.windowInse…Insets.Type.systemBars())");
            return (currentWindowMetrics.getBounds().width() - insetsIgnoringVisibility.left) - insetsIgnoringVisibility.right;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static final double B(Pair<? extends Swap, ? extends Swap> pair) {
        Double d;
        Double d2;
        fs1.f(pair, "<this>");
        Swap first = pair.getFirst();
        double doubleValue = (first == null || (d = first.sellSlippage) == null) ? 0.0d : d.doubleValue();
        Swap second = pair.getSecond();
        double doubleValue2 = (second == null || (d2 = second.buySlippage) == null) ? 0.0d : d2.doubleValue();
        return doubleValue + doubleValue2 + ((doubleValue <= Utils.DOUBLE_EPSILON || doubleValue2 <= Utils.DOUBLE_EPSILON) ? 0 : 1);
    }

    public static final String C(String str, int i, String str2) {
        fs1.f(str, "<this>");
        return (i == 1 && fs1.b(str, "ETH")) ? "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2" : (i == 56 && fs1.b(str, "BNB")) ? "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c" : (i == 97 && fs1.b(str, "BNB")) ? "0xae13d989dac2f0debff460ac112a837c89baa7cd" : str2;
    }

    public static final String D(Swap swap) {
        fs1.f(swap, "<this>");
        Integer num = swap.chainId;
        if (num != null && num.intValue() == 1 && fs1.b(swap.symbol, "ETH")) {
            return "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
        }
        Integer num2 = swap.chainId;
        if (num2 != null && num2.intValue() == 56 && fs1.b(swap.symbol, "BNB")) {
            return "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c";
        }
        Integer num3 = swap.chainId;
        if (num3 != null && num3.intValue() == 97 && fs1.b(swap.symbol, "BNB")) {
            return "0xae13d989dac2f0debff460ac112a837c89baa7cd";
        }
        String str = swap.address;
        fs1.e(str, "this.address");
        return str;
    }

    public static final void E(Context context) {
        fs1.f(context, "<this>");
        Object systemService = context.getSystemService("vibrator");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.os.Vibrator");
        Vibrator vibrator = (Vibrator) systemService;
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(50L, 10));
        } else {
            vibrator.vibrate(50L);
        }
    }

    public static final byte[] F(String str) {
        fs1.f(str, "<this>");
        byte[] bArr = new byte[str.length() / 2];
        qr1 j = u33.j(u33.k(0, str.length()), 2);
        int m = j.m();
        int n = j.n();
        int o = j.o();
        if ((o > 0 && m <= n) || (o < 0 && n <= m)) {
            while (true) {
                int i = m + o;
                bArr[m >> 1] = (byte) ((StringsKt__StringsKt.Y("0123456789ABCDEF", Character.toUpperCase(str.charAt(m)), 0, false, 6, null) << 4) | StringsKt__StringsKt.Y("0123456789ABCDEF", Character.toUpperCase(str.charAt(m + 1)), 0, false, 6, null));
                if (m == n) {
                    break;
                }
                m = i;
            }
        }
        return bArr;
    }

    public static final void G(Activity activity) {
        fs1.f(activity, "<this>");
        Toast toast = b;
        if (toast == null) {
            return;
        }
        toast.cancel();
    }

    public static final boolean H(String str) {
        Object obj;
        fs1.f(str, "<this>");
        String lowerCase = str.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        switch (lowerCase.hashCode()) {
            case -2095018025:
                if (lowerCase.equals("aquagoat")) {
                    return false;
                }
                break;
            case 97686:
                if (lowerCase.equals("bnb")) {
                    return false;
                }
                break;
            case 113786:
                if (lowerCase.equals("sfm")) {
                    return false;
                }
                break;
            case 3006252:
                if (lowerCase.equals("avax")) {
                    return false;
                }
                break;
            case 1765043214:
                if (lowerCase.equals("safemoon")) {
                    return false;
                }
                break;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList<Coin> arrayList2 = MyApplicationClass.c().j0;
        fs1.e(arrayList2, "get().cmcCoins");
        arrayList.addAll(arrayList2);
        Iterator it = arrayList.iterator();
        while (true) {
            if (it.hasNext()) {
                obj = it.next();
                String symbol = ((Coin) obj).getSymbol();
                fs1.e(symbol, "it.symbol");
                Locale locale = Locale.ROOT;
                String lowerCase2 = symbol.toLowerCase(locale);
                fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String lowerCase3 = str.toLowerCase(locale);
                fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (fs1.b(lowerCase2, lowerCase3)) {
                }
            } else {
                obj = null;
            }
        }
        return ((Coin) obj) != null;
    }

    public static final boolean I(String str) {
        fs1.f(str, "<this>");
        return dv3.H(str, "0x", false, 2, null) && str.length() == 42;
    }

    public static final BigInteger J(ow0 ow0Var) {
        BigInteger amountUsed;
        if (ow0Var == null || (amountUsed = ow0Var.getAmountUsed()) == null) {
            return null;
        }
        BigDecimal multiply = new BigDecimal(amountUsed).multiply(new BigDecimal(1.1d));
        fs1.e(multiply, "this.multiply(other)");
        if (multiply == null) {
            return null;
        }
        return multiply.toBigInteger();
    }

    public static final double K(String str) {
        fs1.f(str, "<this>");
        try {
            BigDecimal L = L(str);
            return L == null ? Utils.DOUBLE_EPSILON : L.doubleValue();
        } catch (Exception unused) {
            return Utils.DOUBLE_EPSILON;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x007e A[Catch: Exception -> 0x00b4, TryCatch #0 {Exception -> 0x00b4, blocks: (B:3:0x0008, B:9:0x001e, B:12:0x0057, B:17:0x0068, B:19:0x0070, B:21:0x007e, B:22:0x009a), top: B:26:0x0008 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final java.math.BigDecimal L(java.lang.String r17) {
        /*
            java.lang.String r0 = "<this>"
            r1 = r17
            defpackage.fs1.f(r1, r0)
            r0 = 0
            java.lang.CharSequence r2 = kotlin.text.StringsKt__StringsKt.K0(r17)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r2 = r2.toString()     // Catch: java.lang.Exception -> Lb4
            int r2 = r2.length()     // Catch: java.lang.Exception -> Lb4
            r3 = 0
            r4 = 1
            if (r2 != 0) goto L1a
            r2 = r4
            goto L1b
        L1a:
            r2 = r3
        L1b:
            if (r2 == 0) goto L1e
            return r0
        L1e:
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch: java.lang.Exception -> Lb4
            java.text.NumberFormat r2 = java.text.NumberFormat.getInstance(r2)     // Catch: java.lang.Exception -> Lb4
            java.lang.CharSequence r1 = kotlin.text.StringsKt__StringsKt.K0(r17)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r5 = r1.toString()     // Catch: java.lang.Exception -> Lb4
            java.lang.String r6 = " "
            java.lang.String r7 = ""
            r8 = 0
            r9 = 4
            r10 = 0
            java.lang.String r11 = defpackage.dv3.D(r5, r6, r7, r8, r9, r10)     // Catch: java.lang.Exception -> Lb4
            char[] r12 = new char[r4]     // Catch: java.lang.Exception -> Lb4
            java.text.DecimalFormatSymbols r1 = k()     // Catch: java.lang.Exception -> Lb4
            char r1 = r1.getDecimalSeparator()     // Catch: java.lang.Exception -> Lb4
            r12[r3] = r1     // Catch: java.lang.Exception -> Lb4
            r13 = 0
            r14 = 0
            r15 = 6
            r16 = 0
            java.util.List r1 = kotlin.text.StringsKt__StringsKt.v0(r11, r12, r13, r14, r15, r16)     // Catch: java.lang.Exception -> Lb4
            boolean r5 = r1.isEmpty()     // Catch: java.lang.Exception -> Lb4
            r5 = r5 ^ r4
            java.lang.String r6 = "0"
            if (r5 == 0) goto L6f
            java.lang.Object r5 = r1.get(r3)     // Catch: java.lang.Exception -> Lb4
            java.lang.CharSequence r5 = (java.lang.CharSequence) r5     // Catch: java.lang.Exception -> Lb4
            int r5 = r5.length()     // Catch: java.lang.Exception -> Lb4
            if (r5 <= 0) goto L65
            r5 = r4
            goto L66
        L65:
            r5 = r3
        L66:
            if (r5 == 0) goto L6f
            java.lang.Object r3 = r1.get(r3)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r3 = (java.lang.String) r3     // Catch: java.lang.Exception -> Lb4
            goto L70
        L6f:
            r3 = r6
        L70:
            java.lang.Number r2 = r2.parse(r3)     // Catch: java.lang.Exception -> Lb4
            long r2 = r2.longValue()     // Catch: java.lang.Exception -> Lb4
            int r5 = r1.size()     // Catch: java.lang.Exception -> Lb4
            if (r5 <= r4) goto L9a
            java.lang.Object r1 = r1.get(r4)     // Catch: java.lang.Exception -> Lb4
            r4 = r1
            java.lang.String r4 = (java.lang.String) r4     // Catch: java.lang.Exception -> Lb4
            java.text.DecimalFormatSymbols r1 = k()     // Catch: java.lang.Exception -> Lb4
            char r1 = r1.getGroupingSeparator()     // Catch: java.lang.Exception -> Lb4
            java.lang.String r5 = java.lang.String.valueOf(r1)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r6 = ""
            r7 = 0
            r8 = 4
            r9 = 0
            java.lang.String r6 = defpackage.dv3.D(r4, r5, r6, r7, r8, r9)     // Catch: java.lang.Exception -> Lb4
        L9a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: java.lang.Exception -> Lb4
            r1.<init>()     // Catch: java.lang.Exception -> Lb4
            r1.append(r2)     // Catch: java.lang.Exception -> Lb4
            r2 = 46
            r1.append(r2)     // Catch: java.lang.Exception -> Lb4
            r1.append(r6)     // Catch: java.lang.Exception -> Lb4
            java.lang.String r1 = r1.toString()     // Catch: java.lang.Exception -> Lb4
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch: java.lang.Exception -> Lb4
            r2.<init>(r1)     // Catch: java.lang.Exception -> Lb4
            r0 = r2
        Lb4:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e30.L(java.lang.String):java.math.BigDecimal");
    }

    public static final String M(double d) {
        if (d < 1000.0d) {
            return m(d);
        }
        if (d >= 1000.0d && d < 1000000.0d) {
            return fs1.l(m(d / 1000), "K");
        }
        return fs1.l(m(d / ((double) PlaybackException.CUSTOM_ERROR_CODE_BASE)), "M");
    }

    public static final void N(TextView textView, double d, boolean z) {
        String str;
        fs1.f(textView, "<this>");
        if (!bo3.e(textView.getContext(), "SHOW_BALANCE", true)) {
            str = "****";
        } else if (z) {
            String b2 = u21.a.b();
            String str2 = b2.length() > 1 ? "%s %s" : "%s%s";
            lu3 lu3Var = lu3.a;
            str = String.format(Locale.getDefault(), str2, Arrays.copyOf(new Object[]{b2, p(v21.a(d), 2, null, false, 6, null)}, 2));
            fs1.e(str, "java.lang.String.format(locale, format, *args)");
        } else {
            lu3 lu3Var2 = lu3.a;
            str = String.format(Locale.getDefault(), "%s", Arrays.copyOf(new Object[]{p(v21.a(d), 2, null, false, 6, null)}, 1));
            fs1.e(str, "java.lang.String.format(locale, format, *args)");
        }
        textView.setText(str);
    }

    public static final void O(TextView textView, double d, boolean z) {
        String format;
        fs1.f(textView, "<this>");
        if (z) {
            u21.a aVar = u21.a;
            String str = aVar.b().length() > 1 ? "%s %s" : "%s%s";
            lu3 lu3Var = lu3.a;
            format = String.format(Locale.getDefault(), str, Arrays.copyOf(new Object[]{aVar.b(), p(v21.a(d), 2, null, false, 6, null)}, 2));
            fs1.e(format, "java.lang.String.format(locale, format, *args)");
        } else {
            lu3 lu3Var2 = lu3.a;
            format = String.format(Locale.getDefault(), "%s", Arrays.copyOf(new Object[]{p(v21.a(d), 2, null, false, 6, null)}, 1));
            fs1.e(format, "java.lang.String.format(locale, format, *args)");
        }
        textView.setText(format);
    }

    public static final void P(ImageView imageView, String str, String str2) {
        fs1.f(imageView, "<this>");
        fs1.f(str2, "symbol");
        if (str == null || cv3.l(str) == null) {
            return;
        }
        a.u(imageView).x(a4.f(Integer.parseInt(str), str2)).d0(150, 150).a(n73.v0()).I0(imageView);
    }

    public static final void Q(ImageView imageView, int i, String str, String str2) {
        Object file;
        fs1.f(imageView, "<this>");
        k73 u = a.u(imageView);
        if (i > 0) {
            file = Integer.valueOf(i);
        } else if (str != null && cv3.l(str) != null) {
            file = a4.f(Integer.parseInt(str), str2);
        } else {
            if (str != null) {
                if (str.length() > 0) {
                    boolean exists = new File(str).exists();
                    file = str;
                    if (exists) {
                        file = new File(str);
                    }
                }
            }
            file = Integer.valueOf((int) R.drawable.ic_default_empty_coin);
        }
        u.x(file).d0(150, 150).a(n73.v0()).I0(imageView);
    }

    public static final void R(TextView textView, double d) {
        fs1.f(textView, "<this>");
        textView.setText(bo3.e(textView.getContext(), "SHOW_BALANCE", true) ? p(d, 0, null, false, 7, null) : "****");
    }

    public static final void S(TextView textView, double d, int i) {
        fs1.f(textView, "<this>");
        textView.setText(bo3.e(textView.getContext(), "SHOW_BALANCE", true) ? p(d, i, null, false, 6, null) : "****");
    }

    public static final void T(TextView textView, double d, String str) {
        fs1.f(textView, "<this>");
        fs1.f(str, "tokenSymbol");
        String string = bo3.e(textView.getContext(), "SHOW_BALANCE", true) ? textView.getResources().getString(R.string.currency_usdt_balance_display_name, p(d, 0, null, false, 7, null), str) : "****";
        fs1.e(string, "if (balanceShow) resourc…nSymbol\n    ) else \"****\"");
        textView.setText(string);
    }

    public static final void U(TextView textView, double d, String str) {
        fs1.f(textView, "<this>");
        fs1.f(str, "tokenSymbol");
        String string = bo3.e(textView.getContext(), "SHOW_BALANCE", true) ? textView.getResources().getString(R.string.currency_usdt_balance_display_name, v(h0(new BigDecimal(String.valueOf(d)), 0, 1, null)), str) : "****";
        fs1.e(string, "if (balanceShow) resourc…nSymbol\n    ) else \"****\"");
        textView.setText(string);
    }

    public static final void V(TextView textView) {
        fs1.f(textView, "<this>");
        textView.getPaint().setShader(new LinearGradient((float) Utils.FLOAT_EPSILON, (float) Utils.FLOAT_EPSILON, (float) Utils.FLOAT_EPSILON, textView.getTextSize(), textView.getContext().getColor(R.color.color_edit), -1, Shader.TileMode.CLAMP));
    }

    public static final void W(TextView textView) {
        String str;
        fs1.f(textView, "<this>");
        if (bo3.e(textView.getContext(), "SHOW_BALANCE", true)) {
            u21.a aVar = u21.a;
            if (aVar.b().length() > 1) {
                str = fs1.l(aVar.b(), MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            } else {
                str = aVar.b();
            }
        } else {
            str = "";
        }
        textView.setText(str);
    }

    public static final void X(final View view, final rc1<te4> rc1Var) {
        fs1.f(view, "<this>");
        view.setOnClickListener(new View.OnClickListener() { // from class: d30
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                e30.Y(view, rc1Var, view2);
            }
        });
    }

    public static final void Y(View view, rc1 rc1Var, View view2) {
        fs1.f(view, "$this_setToggleForBalance");
        bo3.n(view.getContext(), "SHOW_BALANCE", Boolean.valueOf(!bo3.e(view.getContext(), "SHOW_BALANCE", true)));
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public static final void Z(Context context, int i) {
        fs1.f(context, "<this>");
        Toast toast = a;
        if (toast != null) {
            toast.cancel();
        }
        Toast makeText = Toast.makeText(context, context.getString(i), 1);
        makeText.setGravity(17, 0, 0);
        makeText.show();
        a = makeText;
    }

    public static final void a0(Context context, String str) {
        fs1.f(context, "<this>");
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        Toast toast = a;
        if (toast != null) {
            toast.cancel();
        }
        Toast makeText = Toast.makeText(context, str, 1);
        makeText.setGravity(17, 0, 0);
        makeText.show();
        a = makeText;
    }

    public static final void b0(Activity activity, String str) {
        fs1.f(activity, "<this>");
        fs1.f(str, ThrowableDeserializer.PROP_NAME_MESSAGE);
        View inflate = activity.getLayoutInflater().inflate(R.layout.custom_toast_view, (ViewGroup) activity.findViewById(R.id.toast_container));
        ((TextView) inflate.findViewById(R.id.toast_text)).setText(str);
        Toast toast = new Toast(activity);
        b = toast;
        toast.cancel();
        Toast toast2 = b;
        if (toast2 == null) {
            return;
        }
        toast2.setGravity(17, 0, 0);
        toast2.setDuration(1);
        toast2.setView(inflate);
        toast2.show();
    }

    public static final Wallet c(Context context) {
        fs1.f(context, "<this>");
        Wallet.Companion companion = Wallet.Companion;
        String j = bo3.j(context, "SAFEMOON_ACTIVE_WALLET", "");
        fs1.e(j, "getString(this, SharedPr…FEMOON_ACTIVE_WALLET, \"\")");
        Wallet wallet2 = companion.toWallet(j);
        if (wallet2 == null) {
            return null;
        }
        return wallet2;
    }

    public static final void c0(String str, String str2) {
        fs1.f(str, "<this>");
        fs1.f(str2, "tag");
    }

    public static final String d(String str) {
        fs1.f(str, "<this>");
        if (str.length() > 13) {
            return ((Object) str.subSequence(0, 6)) + "..." + ((Object) str.subSequence(str.length() - 6, str.length()));
        }
        return str;
    }

    public static final void d0(final View view, View view2, String str, int i, CharSequence charSequence, final tc1<? super View, te4> tc1Var) {
        fs1.f(view, "<this>");
        fs1.f(view2, "view");
        fs1.f(str, "msg");
        fs1.f(tc1Var, "action");
        Snackbar b0 = Snackbar.b0(view2, str, i);
        fs1.e(b0, "make(view, msg, length)");
        if (charSequence != null) {
            b0.e0(charSequence, new View.OnClickListener() { // from class: c30
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    e30.e0(tc1.this, view, view3);
                }
            }).Q();
        } else {
            b0.Q();
        }
    }

    public static final TokenType e(Context context) {
        fs1.f(context, "<this>");
        TokenType.a aVar = TokenType.Companion;
        String j = bo3.j(context, "DEFAULT_GATEWAY", TokenType.BEP_20.name());
        fs1.e(j, "getString(this, SharedPr…Y, TokenType.BEP_20.name)");
        return aVar.c(j);
    }

    public static final void e0(tc1 tc1Var, View view, View view2) {
        fs1.f(tc1Var, "$action");
        fs1.f(view, "$this_showSnackbar");
        tc1Var.invoke(view);
    }

    public static final TokenType f(Context context) {
        fs1.f(context, "<this>");
        TokenType.a aVar = TokenType.Companion;
        String j = bo3.j(context, "DEFAULT_GATEWAY_COLLECTIONS", e(context).name());
        fs1.e(j, "getString(this, SharedPr…OLLECTIONS, chain().name)");
        return aVar.c(j);
    }

    public static final String f0(byte[] bArr, boolean z) {
        fs1.f(bArr, "<this>");
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("0x");
        }
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            byte b2 = bArr[i];
            i++;
            lu3 lu3Var = lu3.a;
            String format = String.format("%02x", Arrays.copyOf(new Object[]{Byte.valueOf((byte) (b2 & (-1)))}, 1));
            fs1.e(format, "java.lang.String.format(format, *args)");
            sb.append(format);
        }
        String sb2 = sb.toString();
        fs1.e(sb2, "stringBuilder.toString()");
        return sb2;
    }

    public static final void g(Activity activity, String str) {
        fs1.f(activity, "<this>");
        fs1.f(str, PublicResolver.FUNC_TEXT);
        Object systemService = activity.getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        ((ClipboardManager) systemService).setPrimaryClip(ClipData.newPlainText("label", str));
        Z(activity, R.string.copied_to_clipboard);
    }

    public static final String g0(BigDecimal bigDecimal, int i) {
        fs1.f(bigDecimal, "<this>");
        int b2 = u33.b(bigDecimal.scale(), 0);
        if (!(1 <= i && i < b2)) {
            i = b2;
        }
        String[] strArr = new String[i];
        for (int i2 = 0; i2 < i; i2++) {
            strArr[i2] = "#";
        }
        String z = ai.z(strArr, "", null, null, 0, null, null, 62, null);
        DecimalFormat decimalFormat = new DecimalFormat(fs1.l("#", z.length() > 0 ? fs1.l(".", z) : ""));
        decimalFormat.setDecimalFormatSymbols(k());
        String format = decimalFormat.format(bigDecimal);
        if (b30.a.y() == ',') {
            fs1.e(format, "plainString");
            return dv3.D(format, ".", ",", false, 4, null);
        }
        fs1.e(format, "{\n        plainString\n    }");
        return format;
    }

    public static final void h(Context context, String str) {
        fs1.f(context, "<this>");
        fs1.f(str, PublicResolver.FUNC_TEXT);
        Object systemService = context.getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        ((ClipboardManager) systemService).setPrimaryClip(ClipData.newPlainText("label", str));
        Z(context, R.string.copied_to_clipboard);
    }

    public static /* synthetic */ String h0(BigDecimal bigDecimal, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = -1;
        }
        return g0(bigDecimal, i);
    }

    public static final String i(double d) {
        DecimalFormat decimalFormat = new DecimalFormat("###,##0.00############");
        decimalFormat.setDecimalFormatSymbols(k());
        return decimalFormat.format(new BigDecimal(d).setScale(14, RoundingMode.FLOOR));
    }

    public static final BigInteger i0(BigDecimal bigDecimal, int i) {
        fs1.f(bigDecimal, "<this>");
        return Convert.d(bigDecimal, Convert.Unit.fromDecmal(i)).toBigInteger();
    }

    public static final String j(double d) {
        DecimalFormat decimalFormat = new DecimalFormat("###,##0.00######");
        decimalFormat.setDecimalFormatSymbols(k());
        return decimalFormat.format(new BigDecimal(d).setScale(8, RoundingMode.FLOOR));
    }

    public static final boolean j0(String str) {
        fs1.f(str, "<this>");
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i >= str.length()) {
                break;
            }
            if (str.charAt(i) == k().getDecimalSeparator()) {
                i2++;
            }
            i++;
        }
        return i2 <= 1;
    }

    public static final DecimalFormatSymbols k() {
        DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        fs1.e(decimalFormatSymbols, "getInstance()");
        return decimalFormatSymbols;
    }

    public static final int k0(boolean z) {
        return z ? 8 : 0;
    }

    public static final <T> List<T> l(List<? extends T> list, Type type) {
        fs1.f(list, "<this>");
        fs1.f(type, "itemType");
        Object fromJson = new Gson().fromJson(new Gson().toJson(list, type), type);
        fs1.e(fromJson, "Gson().fromJson<List<T>>…his, itemType), itemType)");
        return (List) fromJson;
    }

    public static final int l0(boolean z) {
        return z ? 4 : 0;
    }

    public static final String m(double d) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setDecimalFormatSymbols(k());
        String format = decimalFormat.format(d);
        fs1.e(format, "DecimalFormat(\"0.00\")\n  …Separtor() }.format(this)");
        return format;
    }

    public static final int m0(boolean z) {
        return z ? 0 : 8;
    }

    public static final String n(double d) {
        DecimalFormat decimalFormat = new DecimalFormat("0.##");
        decimalFormat.setDecimalFormatSymbols(k());
        String format = decimalFormat.format(d);
        fs1.e(format, "DecimalFormat(\"0.##\")\n  …Separtor() }.format(this)");
        return format;
    }

    public static final String o(double d, int i, RoundingMode roundingMode, boolean z) {
        String format;
        fs1.f(roundingMode, "_roundingMode");
        if (d == Utils.DOUBLE_EPSILON) {
            StringBuilder sb = new StringBuilder();
            sb.append('0');
            sb.append(b30.a.y());
            sb.append('0');
            return sb.toString();
        } else if (d > 1.0E8d) {
            DecimalFormat decimalFormat = new DecimalFormat("###,##0.##");
            decimalFormat.setDecimalFormatSymbols(k());
            decimalFormat.setRoundingMode(roundingMode);
            String format2 = decimalFormat.format(new BigDecimal(d).setScale(2, roundingMode));
            fs1.e(format2, "{\n        DecimalFormat(…(2, _roundingMode))\n    }");
            return format2;
        } else if (d < 1.0E-8d && d > Utils.DOUBLE_EPSILON) {
            double d2 = d;
            int i2 = 0;
            while (d2 < 1.0d) {
                d2 *= 10;
                i2++;
            }
            int i3 = i2 - 1;
            if (i3 < 12) {
                BigDecimal scale = new BigDecimal(String.valueOf(d)).setScale(i3 + 2, roundingMode);
                fs1.e(scale, "this.toBigDecimal().setS…zeros + 2, _roundingMode)");
                return h0(scale, 0, 1, null);
            } else if (z) {
                return h0(new BigDecimal(String.valueOf(d)), 0, 1, null);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append('0');
                sb2.append(b30.a.y());
                sb2.append('0');
                return sb2.toString();
            }
        } else {
            String str = "###,##0.00######";
            if (i == 0) {
                DecimalFormat decimalFormat2 = new DecimalFormat("###,##0.00######");
                decimalFormat2.setDecimalFormatSymbols(k());
                format = decimalFormat2.format(new BigDecimal(d).setScale(8, RoundingMode.FLOOR));
            } else if (d > Utils.DOUBLE_EPSILON && d < 0.01d) {
                DecimalFormat decimalFormat3 = new DecimalFormat("###,##0.00######");
                decimalFormat3.setDecimalFormatSymbols(k());
                decimalFormat3.setRoundingMode(roundingMode);
                format = decimalFormat3.format(new BigDecimal(d).setScale(8, roundingMode));
            } else {
                if (i == 5) {
                    str = "###,##0.00###";
                } else if (i != 8) {
                    str = "###,##0.00";
                }
                DecimalFormat decimalFormat4 = new DecimalFormat(str);
                decimalFormat4.setDecimalFormatSymbols(k());
                decimalFormat4.setRoundingMode(roundingMode);
                format = decimalFormat4.format(new BigDecimal(d));
            }
            fs1.e(format, "{\n        if (scale == 0…        }\n        }\n    }");
            return format;
        }
    }

    public static /* synthetic */ String p(double d, int i, RoundingMode roundingMode, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        if ((i2 & 2) != 0) {
            roundingMode = RoundingMode.DOWN;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return o(d, i, roundingMode, z);
    }

    public static final BigDecimal q(String str, int i) {
        fs1.f(str, "<this>");
        return Convert.a(str, Convert.Unit.fromDecmal(i));
    }

    public static final BigDecimal r(BigInteger bigInteger, int i) {
        fs1.f(bigInteger, "<this>");
        return Convert.a(bigInteger.toString(), Convert.Unit.fromDecmal(i));
    }

    public static final wq s(TokenType tokenType) {
        fs1.f(tokenType, "<this>");
        wq j = a4.j(tokenType);
        fs1.e(j, "getEVMClient(this)");
        return j;
    }

    public static final String t(String str) {
        fs1.f(str, "symbol");
        return str.length() > 1 ? "%s %s" : "%s%s";
    }

    public static final float u(View view) {
        fs1.f(view, "<this>");
        return view.getWidth() * 0.15f;
    }

    public static final String v(String str) {
        int i;
        String valueOf;
        fs1.f(str, "<this>");
        b30 b30Var = b30.a;
        boolean z = b30Var.r() != '.';
        boolean z2 = b30Var.y() == ',';
        String str2 = Build.MANUFACTURER;
        fs1.e(str2, "MANUFACTURER");
        String lowerCase = str2.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        boolean b2 = fs1.b(lowerCase, "samsung");
        String D = dv3.D(str, String.valueOf(b30Var.r()), "", false, 4, null);
        Objects.requireNonNull(D, "null cannot be cast to non-null type kotlin.CharSequence");
        List v0 = StringsKt__StringsKt.v0(StringsKt__StringsKt.K0(D).toString(), new char[]{b30Var.y()}, false, 0, 6, null);
        if (z2 && b2 && z && StringsKt__StringsKt.L(str, '.', false, 2, null)) {
            i = 2;
            String D2 = dv3.D(str, String.valueOf(b30Var.r()), "", false, 4, null);
            Objects.requireNonNull(D2, "null cannot be cast to non-null type kotlin.CharSequence");
            v0 = StringsKt__StringsKt.v0(StringsKt__StringsKt.K0(D2).toString(), new char[]{'.'}, false, 0, 6, null);
        } else {
            i = 2;
        }
        String str3 = (String) v0.get(0);
        if (z2 && b2) {
            if (str3.length() == 0) {
                str3 = "0";
            }
        }
        if (StringsKt__StringsKt.P(str, b30Var.y(), false, i, null)) {
            valueOf = String.valueOf(b30Var.y());
        } else if (v0.size() > 1) {
            char y = b30Var.y();
            valueOf = String.valueOf(y) + ((String) v0.get(1));
        } else if (StringsKt__StringsKt.P(str, b30Var.r(), false, i, null) && z2 && b2) {
            valueOf = String.valueOf(b30Var.y());
        } else {
            valueOf = (StringsKt__StringsKt.P(str, '.', false, i, null) && z2 && b2 && z) ? String.valueOf(b30Var.y()) : "";
        }
        new String();
        lu3 lu3Var = lu3.a;
        String format = String.format("%,d", Arrays.copyOf(new Object[]{new BigInteger(str3)}, 1));
        fs1.e(format, "java.lang.String.format(format, *args)");
        return fs1.l(format, valueOf);
    }

    public static final Object w(Context context, int i, String str, String str2, String str3) {
        Object file;
        fs1.f(context, "<this>");
        if ((str3 == null ? null : cv3.l(str3)) != null && Integer.parseInt(str3) > 0) {
            if (str2 != null && H(str2)) {
                file = a4.f(Integer.parseInt(str3), str2);
                fs1.e(file, "when {\n        cmcId?.to…_default_empty_coin\n    }");
                return file;
            }
        }
        if (i > 0) {
            file = Integer.valueOf(i);
        } else if (str != null && cv3.l(str) != null) {
            file = a4.f(Integer.parseInt(str), str2);
        } else {
            if (str != null) {
                if (str.length() > 0) {
                    boolean exists = new File(str).exists();
                    file = str;
                    if (exists) {
                        file = new File(str);
                    }
                }
            }
            file = Integer.valueOf((int) R.drawable.ic_default_empty_coin);
        }
        fs1.e(file, "when {\n        cmcId?.to…_default_empty_coin\n    }");
        return file;
    }

    public static final String x(TokenType tokenType) {
        fs1.f(tokenType, "<this>");
        return tokenType.getAssignKey();
    }

    public static final String y(double d, Context context) {
        fs1.f(context, "context");
        return bo3.e(context, "SHOW_BALANCE", true) ? p(d, 0, null, false, 7, null) : "****";
    }

    public static final String z(int i) {
        return i == TokenType.ERC_20.getChainId() ? fs1.l("https://mainnet.infura.io/v3/", a4.d) : i == TokenType.ERC_20_TEST.getChainId() ? fs1.l("https://rinkeby.infura.io/v3/", a4.d) : i == TokenType.BEP_20.getChainId() ? "https://bsc-dataseed.binance.org/" : i == TokenType.BEP_20_TEST.getChainId() ? "https://data-seed-prebsc-1-s1.binance.org:8545/" : i == TokenType.POLYGON.getChainId() ? "https://polygon-rpc.com/" : i == TokenType.POLYGON_TEST.getChainId() ? "https://rpc-mumbai.matic.today/" : i == TokenType.AVALANCHE_C.getChainId() ? "https://api.avax.network/ext/bc/C/rpc" : i == TokenType.AVALANCHE_FUJI_TEST.getChainId() ? "https://api.avax-test.network/ext/bc/C/rpc" : "http://nourl/";
    }
}
