package defpackage;

/* compiled from: ImmutableQualityInfo.java */
/* renamed from: jp1  reason: default package */
/* loaded from: classes.dex */
public class jp1 implements xw2 {
    public static final xw2 d = d(Integer.MAX_VALUE, true, true);
    public int a;
    public boolean b;
    public boolean c;

    public jp1(int i, boolean z, boolean z2) {
        this.a = i;
        this.b = z;
        this.c = z2;
    }

    public static xw2 d(int i, boolean z, boolean z2) {
        return new jp1(i, z, z2);
    }

    @Override // defpackage.xw2
    public boolean a() {
        return this.c;
    }

    @Override // defpackage.xw2
    public boolean b() {
        return this.b;
    }

    @Override // defpackage.xw2
    public int c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof jp1) {
            jp1 jp1Var = (jp1) obj;
            return this.a == jp1Var.a && this.b == jp1Var.b && this.c == jp1Var.c;
        }
        return false;
    }

    public int hashCode() {
        return (this.a ^ (this.b ? 4194304 : 0)) ^ (this.c ? 8388608 : 0);
    }
}
