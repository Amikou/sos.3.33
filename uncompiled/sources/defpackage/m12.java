package defpackage;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* compiled from: LockFreeTaskQueue.kt */
/* renamed from: m12  reason: default package */
/* loaded from: classes2.dex */
public class m12<E> {
    public static final /* synthetic */ AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(m12.class, Object.class, "_cur");
    private volatile /* synthetic */ Object _cur;

    public m12(boolean z) {
        this._cur = new n12(8, z);
    }

    public final boolean a(E e) {
        while (true) {
            n12 n12Var = (n12) this._cur;
            int a2 = n12Var.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                a.compareAndSet(this, n12Var, n12Var.i());
            } else if (a2 == 2) {
                return false;
            }
        }
    }

    public final void b() {
        while (true) {
            n12 n12Var = (n12) this._cur;
            if (n12Var.d()) {
                return;
            }
            a.compareAndSet(this, n12Var, n12Var.i());
        }
    }

    public final int c() {
        return ((n12) this._cur).f();
    }

    public final E d() {
        while (true) {
            n12 n12Var = (n12) this._cur;
            E e = (E) n12Var.j();
            if (e != n12.h) {
                return e;
            }
            a.compareAndSet(this, n12Var, n12Var.i());
        }
    }
}
