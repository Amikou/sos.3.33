package defpackage;

import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: v05  reason: default package */
/* loaded from: classes.dex */
public interface v05 {
    void a();

    void c();

    void d();

    boolean e();

    void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    <A extends a.b, T extends b<? extends l83, A>> T g(T t);
}
