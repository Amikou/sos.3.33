package defpackage;

import com.google.common.base.Optional;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/* compiled from: MoreObjects.java */
/* renamed from: s92  reason: default package */
/* loaded from: classes2.dex */
public final class s92 {

    /* compiled from: MoreObjects.java */
    /* renamed from: s92$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public final String a;
        public final C0282b b;
        public C0282b c;
        public boolean d;
        public boolean e;

        /* compiled from: MoreObjects.java */
        /* renamed from: s92$b$a */
        /* loaded from: classes2.dex */
        public static final class a extends C0282b {
            public a() {
                super();
            }
        }

        /* compiled from: MoreObjects.java */
        /* renamed from: s92$b$b  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static class C0282b {
            public String a;
            public Object b;
            public C0282b c;

            public C0282b() {
            }
        }

        public static boolean i(Object obj) {
            if (obj instanceof CharSequence) {
                return ((CharSequence) obj).length() == 0;
            } else if (obj instanceof Collection) {
                return ((Collection) obj).isEmpty();
            } else {
                if (obj instanceof Map) {
                    return ((Map) obj).isEmpty();
                }
                if (obj instanceof Optional) {
                    return !((Optional) obj).isPresent();
                }
                return obj.getClass().isArray() && Array.getLength(obj) == 0;
            }
        }

        public b a(String str, int i) {
            return g(str, String.valueOf(i));
        }

        public b b(String str, Object obj) {
            return e(str, obj);
        }

        public final C0282b c() {
            C0282b c0282b = new C0282b();
            this.c.c = c0282b;
            this.c = c0282b;
            return c0282b;
        }

        public final b d(Object obj) {
            c().b = obj;
            return this;
        }

        public final b e(String str, Object obj) {
            C0282b c = c();
            c.b = obj;
            c.a = (String) au2.k(str);
            return this;
        }

        public final a f() {
            a aVar = new a();
            this.c.c = aVar;
            this.c = aVar;
            return aVar;
        }

        public final b g(String str, Object obj) {
            a f = f();
            f.b = obj;
            f.a = (String) au2.k(str);
            return this;
        }

        public b h(Object obj) {
            return d(obj);
        }

        public String toString() {
            boolean z = this.d;
            boolean z2 = this.e;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.a);
            sb.append('{');
            String str = "";
            for (C0282b c0282b = this.b.c; c0282b != null; c0282b = c0282b.c) {
                Object obj = c0282b.b;
                if (!(c0282b instanceof a)) {
                    if (obj == null) {
                        if (z) {
                        }
                    } else if (z2 && i(obj)) {
                    }
                }
                sb.append(str);
                String str2 = c0282b.a;
                if (str2 != null) {
                    sb.append(str2);
                    sb.append('=');
                }
                if (obj != null && obj.getClass().isArray()) {
                    String deepToString = Arrays.deepToString(new Object[]{obj});
                    sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
                } else {
                    sb.append(obj);
                }
                str = ", ";
            }
            sb.append('}');
            return sb.toString();
        }

        public b(String str) {
            C0282b c0282b = new C0282b();
            this.b = c0282b;
            this.c = c0282b;
            this.d = false;
            this.e = false;
            this.a = (String) au2.k(str);
        }
    }

    public static <T> T a(T t, T t2) {
        if (t != null) {
            return t;
        }
        Objects.requireNonNull(t2, "Both parameters are null");
        return t2;
    }

    public static b b(Object obj) {
        return new b(obj.getClass().getSimpleName());
    }
}
