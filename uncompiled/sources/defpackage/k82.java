package defpackage;

import android.content.Context;

/* compiled from: MetadataBackendRegistry_Factory.java */
/* renamed from: k82  reason: default package */
/* loaded from: classes.dex */
public final class k82 implements z11<j82> {
    public final ew2<Context> a;
    public final ew2<ka0> b;

    public k82(ew2<Context> ew2Var, ew2<ka0> ew2Var2) {
        this.a = ew2Var;
        this.b = ew2Var2;
    }

    public static k82 a(ew2<Context> ew2Var, ew2<ka0> ew2Var2) {
        return new k82(ew2Var, ew2Var2);
    }

    public static j82 c(Context context, Object obj) {
        return new j82(context, (ka0) obj);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public j82 get() {
        return c(this.a.get(), this.b.get());
    }
}
