package defpackage;

/* renamed from: ja3  reason: default package */
/* loaded from: classes2.dex */
public class ja3 extends re1 {
    public static final int[] n = {1116352408, 1899447441, -1245643825, -373957723, 961987163, 1508970993, -1841331548, -1424204075, -670586216, 310598401, 607225278, 1426881987, 1925078388, -2132889090, -1680079193, -1046744716, -459576895, -272742522, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, -1740746414, -1473132947, -1341970488, -1084653625, -958395405, -710438585, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, -2117940946, -1838011259, -1564481375, -1474664885, -1035236496, -949202525, -778901479, -694614492, -200395387, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, -2067236844, -1933114872, -1866530822, -1538233109, -1090935817, -965641998};
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;
    public int j;
    public int k;
    public int[] l;
    public int m;

    public ja3() {
        this.l = new int[64];
        reset();
    }

    public ja3(ja3 ja3Var) {
        super(ja3Var);
        this.l = new int[64];
        t(ja3Var);
    }

    @Override // defpackage.qo0
    public int a(byte[] bArr, int i) {
        j();
        ro2.c(this.d, bArr, i);
        ro2.c(this.e, bArr, i + 4);
        ro2.c(this.f, bArr, i + 8);
        ro2.c(this.g, bArr, i + 12);
        ro2.c(this.h, bArr, i + 16);
        ro2.c(this.i, bArr, i + 20);
        ro2.c(this.j, bArr, i + 24);
        reset();
        return 28;
    }

    @Override // defpackage.j72
    public j72 copy() {
        return new ja3(this);
    }

    @Override // defpackage.j72
    public void d(j72 j72Var) {
        t((ja3) j72Var);
    }

    @Override // defpackage.qo0
    public String g() {
        return "SHA-224";
    }

    @Override // defpackage.qo0
    public int h() {
        return 28;
    }

    @Override // defpackage.re1
    public void k() {
        for (int i = 16; i <= 63; i++) {
            int[] iArr = this.l;
            int s = s(iArr[i - 2]);
            int[] iArr2 = this.l;
            iArr[i] = s + iArr2[i - 7] + r(iArr2[i - 15]) + this.l[i - 16];
        }
        int i2 = this.d;
        int i3 = this.e;
        int i4 = this.f;
        int i5 = this.g;
        int i6 = this.h;
        int i7 = this.i;
        int i8 = this.j;
        int i9 = this.k;
        int i10 = 0;
        for (int i11 = 0; i11 < 8; i11++) {
            int q = q(i6) + n(i6, i7, i8);
            int[] iArr3 = n;
            int i12 = i9 + q + iArr3[i10] + this.l[i10];
            int i13 = i5 + i12;
            int p = i12 + p(i2) + o(i2, i3, i4);
            int i14 = i10 + 1;
            int q2 = i8 + q(i13) + n(i13, i6, i7) + iArr3[i14] + this.l[i14];
            int i15 = i4 + q2;
            int p2 = q2 + p(p) + o(p, i2, i3);
            int i16 = i14 + 1;
            int q3 = i7 + q(i15) + n(i15, i13, i6) + iArr3[i16] + this.l[i16];
            int i17 = i3 + q3;
            int p3 = q3 + p(p2) + o(p2, p, i2);
            int i18 = i16 + 1;
            int q4 = i6 + q(i17) + n(i17, i15, i13) + iArr3[i18] + this.l[i18];
            int i19 = i2 + q4;
            int p4 = q4 + p(p3) + o(p3, p2, p);
            int i20 = i18 + 1;
            int q5 = i13 + q(i19) + n(i19, i17, i15) + iArr3[i20] + this.l[i20];
            i9 = p + q5;
            i5 = q5 + p(p4) + o(p4, p3, p2);
            int i21 = i20 + 1;
            int q6 = i15 + q(i9) + n(i9, i19, i17) + iArr3[i21] + this.l[i21];
            i8 = p2 + q6;
            i4 = q6 + p(i5) + o(i5, p4, p3);
            int i22 = i21 + 1;
            int q7 = i17 + q(i8) + n(i8, i9, i19) + iArr3[i22] + this.l[i22];
            i7 = p3 + q7;
            i3 = q7 + p(i4) + o(i4, i5, p4);
            int i23 = i22 + 1;
            int q8 = i19 + q(i7) + n(i7, i8, i9) + iArr3[i23] + this.l[i23];
            i6 = p4 + q8;
            i2 = q8 + p(i3) + o(i3, i4, i5);
            i10 = i23 + 1;
        }
        this.d += i2;
        this.e += i3;
        this.f += i4;
        this.g += i5;
        this.h += i6;
        this.i += i7;
        this.j += i8;
        this.k += i9;
        this.m = 0;
        for (int i24 = 0; i24 < 16; i24++) {
            this.l[i24] = 0;
        }
    }

    @Override // defpackage.re1
    public void l(long j) {
        if (this.m > 14) {
            k();
        }
        int[] iArr = this.l;
        iArr[14] = (int) (j >>> 32);
        iArr[15] = (int) (j & (-1));
    }

    @Override // defpackage.re1
    public void m(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        int i4 = (bArr[i3 + 1] & 255) | (bArr[i] << 24) | ((bArr[i2] & 255) << 16) | ((bArr[i3] & 255) << 8);
        int[] iArr = this.l;
        int i5 = this.m;
        iArr[i5] = i4;
        int i6 = i5 + 1;
        this.m = i6;
        if (i6 == 16) {
            k();
        }
    }

    public final int n(int i, int i2, int i3) {
        return ((~i) & i3) ^ (i2 & i);
    }

    public final int o(int i, int i2, int i3) {
        return ((i & i3) ^ (i & i2)) ^ (i2 & i3);
    }

    public final int p(int i) {
        return ((i << 10) | (i >>> 22)) ^ (((i >>> 2) | (i << 30)) ^ ((i >>> 13) | (i << 19)));
    }

    public final int q(int i) {
        return ((i << 7) | (i >>> 25)) ^ (((i >>> 6) | (i << 26)) ^ ((i >>> 11) | (i << 21)));
    }

    public final int r(int i) {
        return (i >>> 3) ^ (((i >>> 7) | (i << 25)) ^ ((i >>> 18) | (i << 14)));
    }

    @Override // defpackage.re1, defpackage.qo0
    public void reset() {
        super.reset();
        this.d = -1056596264;
        this.e = 914150663;
        this.f = 812702999;
        this.g = -150054599;
        this.h = -4191439;
        this.i = 1750603025;
        this.j = 1694076839;
        this.k = -1090891868;
        this.m = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.l;
            if (i == iArr.length) {
                return;
            }
            iArr[i] = 0;
            i++;
        }
    }

    public final int s(int i) {
        return (i >>> 10) ^ (((i >>> 17) | (i << 15)) ^ ((i >>> 19) | (i << 13)));
    }

    public final void t(ja3 ja3Var) {
        super.i(ja3Var);
        this.d = ja3Var.d;
        this.e = ja3Var.e;
        this.f = ja3Var.f;
        this.g = ja3Var.g;
        this.h = ja3Var.h;
        this.i = ja3Var.i;
        this.j = ja3Var.j;
        this.k = ja3Var.k;
        int[] iArr = ja3Var.l;
        System.arraycopy(iArr, 0, this.l, 0, iArr.length);
        this.m = ja3Var.m;
    }
}
