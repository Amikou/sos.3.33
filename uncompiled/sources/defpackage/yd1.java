package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import org.web3j.abi.TypeReference;
import org.web3j.abi.a;

/* compiled from: FunctionReturnDecoder.java */
/* renamed from: yd1  reason: default package */
/* loaded from: classes2.dex */
public abstract class yd1 {
    private static yd1 DEFAULT_DECODER;
    private static final ServiceLoader<zd1> loader = ServiceLoader.load(zd1.class);

    public static List<zc4> decode(String str, List<TypeReference<zc4>> list) {
        return decoder().decodeFunctionResult(str, list);
    }

    public static <T extends zc4> zc4 decodeIndexedValue(String str, TypeReference<T> typeReference) {
        return decoder().decodeEventParameter(str, typeReference);
    }

    private static yd1 decoder() {
        Iterator<zd1> it = loader.iterator();
        return it.hasNext() ? (yd1) it.next().get() : defaultDecoder();
    }

    private static yd1 defaultDecoder() {
        if (DEFAULT_DECODER == null) {
            DEFAULT_DECODER = new a();
        }
        return DEFAULT_DECODER;
    }

    public abstract <T extends zc4> zc4 decodeEventParameter(String str, TypeReference<T> typeReference);

    public abstract List<zc4> decodeFunctionResult(String str, List<TypeReference<zc4>> list);
}
