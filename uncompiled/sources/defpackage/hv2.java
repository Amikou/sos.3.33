package defpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.memory.b;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.image.a;
import com.facebook.imagepipeline.producers.LocalExifThumbnailProducer;
import com.facebook.imagepipeline.producers.ThumbnailProducer;
import com.facebook.imagepipeline.producers.c;
import com.facebook.imagepipeline.producers.e;
import com.facebook.imagepipeline.producers.f;
import com.facebook.imagepipeline.producers.g;
import com.facebook.imagepipeline.producers.i;
import com.facebook.imagepipeline.producers.j;
import com.facebook.imagepipeline.producers.k;
import com.facebook.imagepipeline.producers.l;
import com.facebook.imagepipeline.producers.m;
import com.facebook.imagepipeline.producers.n;
import com.facebook.imagepipeline.producers.o;
import com.facebook.imagepipeline.producers.p;
import com.facebook.imagepipeline.producers.q;
import com.facebook.imagepipeline.producers.r;
import com.facebook.imagepipeline.producers.s;

/* compiled from: ProducerFactory.java */
/* renamed from: hv2  reason: default package */
/* loaded from: classes.dex */
public class hv2 {
    public ContentResolver a;
    public Resources b;
    public AssetManager c;
    public final os d;
    public final tn1 e;
    public final qv2 f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final wy0 j;
    public final b k;
    public final xr l;
    public final xr m;
    public final l72<wt, PooledByteBuffer> n;
    public final l72<wt, a> o;
    public final xt p;
    public final gr<wt> q;
    public final gr<wt> r;
    public final br2 s;
    public final int t;
    public final int u;
    public boolean v;
    public final a00 w;
    public final int x;
    public final boolean y;

    public hv2(Context context, os osVar, tn1 tn1Var, qv2 qv2Var, boolean z, boolean z2, boolean z3, wy0 wy0Var, b bVar, l72<wt, a> l72Var, l72<wt, PooledByteBuffer> l72Var2, xr xrVar, xr xrVar2, xt xtVar, br2 br2Var, int i, int i2, boolean z4, int i3, a00 a00Var, boolean z5, int i4) {
        this.a = context.getApplicationContext().getContentResolver();
        this.b = context.getApplicationContext().getResources();
        this.c = context.getApplicationContext().getAssets();
        this.d = osVar;
        this.e = tn1Var;
        this.f = qv2Var;
        this.g = z;
        this.h = z2;
        this.i = z3;
        this.j = wy0Var;
        this.k = bVar;
        this.o = l72Var;
        this.n = l72Var2;
        this.l = xrVar;
        this.m = xrVar2;
        this.p = xtVar;
        this.s = br2Var;
        this.q = new gr<>(i4);
        this.r = new gr<>(i4);
        this.t = i;
        this.u = i2;
        this.v = z4;
        this.x = i3;
        this.w = a00Var;
        this.y = z5;
    }

    public static f9 a(dv2<zu0> dv2Var) {
        return new f9(dv2Var);
    }

    public static ir h(dv2<zu0> dv2Var, dv2<zu0> dv2Var2) {
        return new ir(dv2Var, dv2Var2);
    }

    public qt2 A(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new qt2(this.o, this.p, dv2Var);
    }

    public st2 B(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new st2(dv2Var, this.s, this.j.c());
    }

    public p C() {
        return new p(this.j.e(), this.k, this.a);
    }

    public q D(dv2<zu0> dv2Var, boolean z, bp1 bp1Var) {
        return new q(this.j.c(), this.k, dv2Var, z, bp1Var);
    }

    public <T> n54<T> E(dv2<T> dv2Var) {
        return new n54<>(5, this.j.b(), dv2Var);
    }

    public r54 F(ThumbnailProducer<EncodedImage>[] thumbnailProducerArr) {
        return new r54(thumbnailProducerArr);
    }

    public s G(dv2<zu0> dv2Var) {
        return new s(this.j.c(), this.k, dv2Var);
    }

    public <T> dv2<T> b(dv2<T> dv2Var, h54 h54Var) {
        return new r(dv2Var, h54Var);
    }

    public dq c(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new dq(this.o, this.p, dv2Var);
    }

    public fq d(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new fq(this.p, dv2Var);
    }

    public gq e(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new gq(this.o, this.p, dv2Var);
    }

    public mq f(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new mq(dv2Var, this.t, this.u, this.v);
    }

    public nq g(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new nq(this.n, this.l, this.m, this.p, this.q, this.r, dv2Var);
    }

    public com.facebook.imagepipeline.producers.a i() {
        return new com.facebook.imagepipeline.producers.a(this.k);
    }

    public com.facebook.imagepipeline.producers.b j(dv2<zu0> dv2Var) {
        return new com.facebook.imagepipeline.producers.b(this.d, this.j.a(), this.e, this.f, this.g, this.h, this.i, dv2Var, this.x, this.w, null, gw3.a);
    }

    public yl0 k(dv2<com.facebook.common.references.a<a>> dv2Var) {
        return new yl0(dv2Var, this.j.g());
    }

    public c l(dv2<zu0> dv2Var) {
        return new c(this.l, this.m, this.p, dv2Var);
    }

    public dp0 m(dv2<zu0> dv2Var) {
        return new dp0(this.l, this.m, this.p, dv2Var);
    }

    public wu0 n(dv2<zu0> dv2Var) {
        return new wu0(this.p, this.y, dv2Var);
    }

    public bv0 o(dv2<zu0> dv2Var) {
        return new bv0(this.n, this.p, dv2Var);
    }

    public dv0 p(dv2<zu0> dv2Var) {
        return new dv0(this.l, this.m, this.p, this.q, this.r, dv2Var);
    }

    public e q() {
        return new e(this.j.e(), this.k, this.c);
    }

    public f r() {
        return new f(this.j.e(), this.k, this.a);
    }

    public g s() {
        return new g(this.j.e(), this.k, this.a);
    }

    public LocalExifThumbnailProducer t() {
        return new LocalExifThumbnailProducer(this.j.f(), this.k, this.a);
    }

    public i u() {
        return new i(this.j.e(), this.k);
    }

    public j v() {
        return new j(this.j.e(), this.k, this.b);
    }

    public k w() {
        return new k(this.j.c(), this.a);
    }

    public l x() {
        return new l(this.j.e(), this.a);
    }

    public dv2<zu0> y(n nVar) {
        return new m(this.k, this.d, nVar);
    }

    public o z(dv2<zu0> dv2Var) {
        return new o(this.l, this.p, this.k, this.d, dv2Var);
    }
}
