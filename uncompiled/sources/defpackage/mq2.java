package defpackage;

/* renamed from: mq2  reason: default package */
/* loaded from: classes2.dex */
public class mq2 {
    public int[] a;

    public mq2(byte[] bArr) {
        if (bArr.length <= 4) {
            throw new IllegalArgumentException("invalid encoding");
        }
        int e = p02.e(bArr, 0);
        int a = wr1.a(e - 1);
        if (bArr.length != (e * a) + 4) {
            throw new IllegalArgumentException("invalid encoding");
        }
        this.a = new int[e];
        for (int i = 0; i < e; i++) {
            this.a[i] = p02.f(bArr, (i * a) + 4, a);
        }
        if (!b(this.a)) {
            throw new IllegalArgumentException("invalid encoding");
        }
    }

    public byte[] a() {
        int length = this.a.length;
        int a = wr1.a(length - 1);
        byte[] bArr = new byte[(length * a) + 4];
        p02.a(length, bArr, 0);
        for (int i = 0; i < length; i++) {
            p02.b(this.a[i], bArr, (i * a) + 4, a);
        }
        return bArr;
    }

    public final boolean b(int[] iArr) {
        int length = iArr.length;
        boolean[] zArr = new boolean[length];
        for (int i = 0; i < length; i++) {
            if (iArr[i] < 0 || iArr[i] >= length || zArr[iArr[i]]) {
                return false;
            }
            zArr[iArr[i]] = true;
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (obj instanceof mq2) {
            return ur1.b(this.a, ((mq2) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        String str = "[" + this.a[0];
        for (int i = 1; i < this.a.length; i++) {
            str = str + ", " + this.a[i];
        }
        return str + "]";
    }
}
