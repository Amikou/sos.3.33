package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentWalletConnectBinding.java */
/* renamed from: qb1  reason: default package */
/* loaded from: classes2.dex */
public final class qb1 {
    public final MaterialButton a;
    public final RecyclerView b;
    public final rp3 c;

    public qb1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialCardView materialCardView, View view, RecyclerView recyclerView, rp3 rp3Var, AppCompatTextView appCompatTextView, MaterialTextView materialTextView, CardView cardView) {
        this.a = materialButton;
        this.b = recyclerView;
        this.c = rp3Var;
    }

    public static qb1 a(View view) {
        int i = R.id.btnCreateConnection;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnCreateConnection);
        if (materialButton != null) {
            i = R.id.ccWrapperWalletConnect;
            MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ccWrapperWalletConnect);
            if (materialCardView != null) {
                i = R.id.dividerTxtWcHeader;
                View a = ai4.a(view, R.id.dividerTxtWcHeader);
                if (a != null) {
                    i = R.id.rvConnectedDapp;
                    RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvConnectedDapp);
                    if (recyclerView != null) {
                        i = R.id.toolbar;
                        View a2 = ai4.a(view, R.id.toolbar);
                        if (a2 != null) {
                            rp3 a3 = rp3.a(a2);
                            i = R.id.txtWcDetail;
                            AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtWcDetail);
                            if (appCompatTextView != null) {
                                i = R.id.txtWcHeader;
                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtWcHeader);
                                if (materialTextView != null) {
                                    i = R.id.wrapperWalletConnect;
                                    CardView cardView = (CardView) ai4.a(view, R.id.wrapperWalletConnect);
                                    if (cardView != null) {
                                        return new qb1((ConstraintLayout) view, materialButton, materialCardView, a, recyclerView, a3, appCompatTextView, materialTextView, cardView);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
