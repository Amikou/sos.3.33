package defpackage;

import androidx.media3.common.j;
import androidx.media3.common.util.b;
import defpackage.gc4;
import java.util.Collections;
import zendesk.support.request.CellBase;

/* compiled from: H265Reader.java */
/* renamed from: fj1  reason: default package */
/* loaded from: classes.dex */
public final class fj1 implements ku0 {
    public final fj3 a;
    public String b;
    public f84 c;
    public a d;
    public boolean e;
    public long l;
    public final boolean[] f = new boolean[3];
    public final vc2 g = new vc2(32, 128);
    public final vc2 h = new vc2(33, 128);
    public final vc2 i = new vc2(34, 128);
    public final vc2 j = new vc2(39, 128);
    public final vc2 k = new vc2(40, 128);
    public long m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public final op2 n = new op2();

    /* compiled from: H265Reader.java */
    /* renamed from: fj1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final f84 a;
        public long b;
        public boolean c;
        public int d;
        public long e;
        public boolean f;
        public boolean g;
        public boolean h;
        public boolean i;
        public boolean j;
        public long k;
        public long l;
        public boolean m;

        public a(f84 f84Var) {
            this.a = f84Var;
        }

        public static boolean b(int i) {
            return (32 <= i && i <= 35) || i == 39;
        }

        public static boolean c(int i) {
            return i < 32 || i == 40;
        }

        public void a(long j, int i, boolean z) {
            if (this.j && this.g) {
                this.m = this.c;
                this.j = false;
            } else if (this.h || this.g) {
                if (z && this.i) {
                    d(i + ((int) (j - this.b)));
                }
                this.k = this.b;
                this.l = this.e;
                this.m = this.c;
                this.i = true;
            }
        }

        public final void d(int i) {
            long j = this.l;
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return;
            }
            boolean z = this.m;
            this.a.b(j, z ? 1 : 0, (int) (this.b - this.k), i, null);
        }

        public void e(byte[] bArr, int i, int i2) {
            if (this.f) {
                int i3 = this.d;
                int i4 = (i + 2) - i3;
                if (i4 < i2) {
                    this.g = (bArr[i4] & 128) != 0;
                    this.f = false;
                    return;
                }
                this.d = i3 + (i2 - i);
            }
        }

        public void f() {
            this.f = false;
            this.g = false;
            this.h = false;
            this.i = false;
            this.j = false;
        }

        public void g(long j, int i, int i2, long j2, boolean z) {
            boolean z2 = false;
            this.g = false;
            this.h = false;
            this.e = j2;
            this.d = 0;
            this.b = j;
            if (!c(i2)) {
                if (this.i && !this.j) {
                    if (z) {
                        d(i);
                    }
                    this.i = false;
                }
                if (b(i2)) {
                    this.h = !this.j;
                    this.j = true;
                }
            }
            boolean z3 = i2 >= 16 && i2 <= 21;
            this.c = z3;
            if (z3 || i2 <= 9) {
                z2 = true;
            }
            this.f = z2;
        }
    }

    public fj1(fj3 fj3Var) {
        this.a = fj3Var;
    }

    public static j i(String str, vc2 vc2Var, vc2 vc2Var2, vc2 vc2Var3) {
        int i = vc2Var.e;
        byte[] bArr = new byte[vc2Var2.e + i + vc2Var3.e];
        System.arraycopy(vc2Var.d, 0, bArr, 0, i);
        System.arraycopy(vc2Var2.d, 0, bArr, vc2Var.e, vc2Var2.e);
        System.arraycopy(vc2Var3.d, 0, bArr, vc2Var.e + vc2Var2.e, vc2Var3.e);
        pp2 pp2Var = new pp2(vc2Var2.d, 0, vc2Var2.e);
        pp2Var.l(44);
        int e = pp2Var.e(3);
        pp2Var.k();
        int e2 = pp2Var.e(2);
        boolean d = pp2Var.d();
        int e3 = pp2Var.e(5);
        int i2 = 0;
        for (int i3 = 0; i3 < 32; i3++) {
            if (pp2Var.d()) {
                i2 |= 1 << i3;
            }
        }
        int[] iArr = new int[6];
        for (int i4 = 0; i4 < 6; i4++) {
            iArr[i4] = pp2Var.e(8);
        }
        int e4 = pp2Var.e(8);
        int i5 = 0;
        for (int i6 = 0; i6 < e; i6++) {
            if (pp2Var.d()) {
                i5 += 89;
            }
            if (pp2Var.d()) {
                i5 += 8;
            }
        }
        pp2Var.l(i5);
        if (e > 0) {
            pp2Var.l((8 - e) * 2);
        }
        pp2Var.h();
        int h = pp2Var.h();
        if (h == 3) {
            pp2Var.k();
        }
        int h2 = pp2Var.h();
        int h3 = pp2Var.h();
        if (pp2Var.d()) {
            int h4 = pp2Var.h();
            int h5 = pp2Var.h();
            int h6 = pp2Var.h();
            int h7 = pp2Var.h();
            h2 -= ((h == 1 || h == 2) ? 2 : 1) * (h4 + h5);
            h3 -= (h == 1 ? 2 : 1) * (h6 + h7);
        }
        pp2Var.h();
        pp2Var.h();
        int h8 = pp2Var.h();
        for (int i7 = pp2Var.d() ? 0 : e; i7 <= e; i7++) {
            pp2Var.h();
            pp2Var.h();
            pp2Var.h();
        }
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        pp2Var.h();
        if (pp2Var.d() && pp2Var.d()) {
            j(pp2Var);
        }
        pp2Var.l(2);
        if (pp2Var.d()) {
            pp2Var.l(8);
            pp2Var.h();
            pp2Var.h();
            pp2Var.k();
        }
        k(pp2Var);
        if (pp2Var.d()) {
            for (int i8 = 0; i8 < pp2Var.h(); i8++) {
                pp2Var.l(h8 + 4 + 1);
            }
        }
        pp2Var.l(2);
        float f = 1.0f;
        if (pp2Var.d()) {
            if (pp2Var.d()) {
                int e5 = pp2Var.e(8);
                if (e5 == 255) {
                    int e6 = pp2Var.e(16);
                    int e7 = pp2Var.e(16);
                    if (e6 != 0 && e7 != 0) {
                        f = e6 / e7;
                    }
                } else {
                    float[] fArr = wc2.b;
                    if (e5 < fArr.length) {
                        f = fArr[e5];
                    } else {
                        p12.i("H265Reader", "Unexpected aspect_ratio_idc value: " + e5);
                    }
                }
            }
            if (pp2Var.d()) {
                pp2Var.k();
            }
            if (pp2Var.d()) {
                pp2Var.l(4);
                if (pp2Var.d()) {
                    pp2Var.l(24);
                }
            }
            if (pp2Var.d()) {
                pp2Var.h();
                pp2Var.h();
            }
            pp2Var.k();
            if (pp2Var.d()) {
                h3 *= 2;
            }
        }
        return new j.b().S(str).e0("video/hevc").I(h00.c(e2, d, e3, i2, iArr, e4)).j0(h2).Q(h3).a0(f).T(Collections.singletonList(bArr)).E();
    }

    public static void j(pp2 pp2Var) {
        for (int i = 0; i < 4; i++) {
            int i2 = 0;
            while (i2 < 6) {
                int i3 = 1;
                if (!pp2Var.d()) {
                    pp2Var.h();
                } else {
                    int min = Math.min(64, 1 << ((i << 1) + 4));
                    if (i > 1) {
                        pp2Var.g();
                    }
                    for (int i4 = 0; i4 < min; i4++) {
                        pp2Var.g();
                    }
                }
                if (i == 3) {
                    i3 = 3;
                }
                i2 += i3;
            }
        }
    }

    public static void k(pp2 pp2Var) {
        int h = pp2Var.h();
        boolean z = false;
        int i = 0;
        for (int i2 = 0; i2 < h; i2++) {
            if (i2 != 0) {
                z = pp2Var.d();
            }
            if (z) {
                pp2Var.k();
                pp2Var.h();
                for (int i3 = 0; i3 <= i; i3++) {
                    if (pp2Var.d()) {
                        pp2Var.k();
                    }
                }
            } else {
                int h2 = pp2Var.h();
                int h3 = pp2Var.h();
                int i4 = h2 + h3;
                for (int i5 = 0; i5 < h2; i5++) {
                    pp2Var.h();
                    pp2Var.k();
                }
                for (int i6 = 0; i6 < h3; i6++) {
                    pp2Var.h();
                    pp2Var.k();
                }
                i = i4;
            }
        }
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        b();
        while (op2Var.a() > 0) {
            int e = op2Var.e();
            int f = op2Var.f();
            byte[] d = op2Var.d();
            this.l += op2Var.a();
            this.c.a(op2Var, op2Var.a());
            while (e < f) {
                int c = wc2.c(d, e, f, this.f);
                if (c == f) {
                    h(d, e, f);
                    return;
                }
                int e2 = wc2.e(d, c);
                int i = c - e;
                if (i > 0) {
                    h(d, e, c);
                }
                int i2 = f - c;
                long j = this.l - i2;
                g(j, i2, i < 0 ? -i : 0, this.m);
                l(j, i2, e2, this.m);
                e = c + 3;
            }
        }
    }

    public final void b() {
        ii.i(this.c);
        b.j(this.d);
    }

    @Override // defpackage.ku0
    public void c() {
        this.l = 0L;
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        wc2.a(this.f);
        this.g.d();
        this.h.d();
        this.i.d();
        this.j.d();
        this.k.d();
        a aVar = this.d;
        if (aVar != null) {
            aVar.f();
        }
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.m = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.b = dVar.b();
        f84 f = r11Var.f(dVar.c(), 2);
        this.c = f;
        this.d = new a(f);
        this.a.b(r11Var, dVar);
    }

    public final void g(long j, int i, int i2, long j2) {
        this.d.a(j, i, this.e);
        if (!this.e) {
            this.g.b(i2);
            this.h.b(i2);
            this.i.b(i2);
            if (this.g.c() && this.h.c() && this.i.c()) {
                this.c.f(i(this.b, this.g, this.h, this.i));
                this.e = true;
            }
        }
        if (this.j.b(i2)) {
            vc2 vc2Var = this.j;
            this.n.N(this.j.d, wc2.q(vc2Var.d, vc2Var.e));
            this.n.Q(5);
            this.a.a(j2, this.n);
        }
        if (this.k.b(i2)) {
            vc2 vc2Var2 = this.k;
            this.n.N(this.k.d, wc2.q(vc2Var2.d, vc2Var2.e));
            this.n.Q(5);
            this.a.a(j2, this.n);
        }
    }

    public final void h(byte[] bArr, int i, int i2) {
        this.d.e(bArr, i, i2);
        if (!this.e) {
            this.g.a(bArr, i, i2);
            this.h.a(bArr, i, i2);
            this.i.a(bArr, i, i2);
        }
        this.j.a(bArr, i, i2);
        this.k.a(bArr, i, i2);
    }

    public final void l(long j, int i, int i2, long j2) {
        this.d.g(j, i, i2, j2, this.e);
        if (!this.e) {
            this.g.e(i2);
            this.h.e(i2);
            this.i.e(i2);
        }
        this.j.e(i2);
        this.k.e(i2);
    }
}
