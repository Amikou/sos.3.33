package defpackage;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.api.internal.b;
import com.google.android.gms.common.api.internal.e;
import com.google.android.gms.common.api.internal.zabj;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: d05  reason: default package */
/* loaded from: classes.dex */
public final class d05 extends GoogleApiClient implements u05 {
    public final Lock b;
    public final v15 c;
    public final int e;
    public final Context f;
    public final Looper g;
    public volatile boolean i;
    public long j;
    public long k;
    public final h05 l;
    public final dh1 m;
    public zabj n;
    public final Map<a.c<?>, a.f> o;
    public Set<Scope> p;
    public final kz q;
    public final Map<a<?>, Boolean> r;
    public final a.AbstractC0106a<? extends p15, uo3> s;
    public final e t;
    public final ArrayList<r25> u;
    public Integer v;
    public Set<j15> w;
    public final k15 x;
    public final c25 y;
    public v05 d = null;
    public final Queue<b<?, ?>> h = new LinkedList();

    public d05(Context context, Lock lock, Looper looper, kz kzVar, dh1 dh1Var, a.AbstractC0106a<? extends p15, uo3> abstractC0106a, Map<a<?>, Boolean> map, List<GoogleApiClient.b> list, List<GoogleApiClient.c> list2, Map<a.c<?>, a.f> map2, int i, int i2, ArrayList<r25> arrayList) {
        this.j = jz.a() ? 10000L : 120000L;
        this.k = 5000L;
        this.p = new HashSet();
        this.t = new e();
        this.v = null;
        this.w = null;
        f05 f05Var = new f05(this);
        this.y = f05Var;
        this.f = context;
        this.b = lock;
        this.c = new v15(looper, f05Var);
        this.g = looper;
        this.l = new h05(this, looper);
        this.m = dh1Var;
        this.e = i;
        if (i >= 0) {
            this.v = Integer.valueOf(i2);
        }
        this.r = map;
        this.o = map2;
        this.u = arrayList;
        this.x = new k15();
        for (GoogleApiClient.b bVar : list) {
            this.c.e(bVar);
        }
        for (GoogleApiClient.c cVar : list2) {
            this.c.f(cVar);
        }
        this.q = kzVar;
        this.s = abstractC0106a;
    }

    public static int m(Iterable<a.f> iterable, boolean z) {
        boolean z2 = false;
        boolean z3 = false;
        for (a.f fVar : iterable) {
            if (fVar.u()) {
                z2 = true;
            }
            if (fVar.e()) {
                z3 = true;
            }
        }
        if (z2) {
            return (z3 && z) ? 2 : 1;
        }
        return 3;
    }

    public static String p(int i) {
        return i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    @Override // defpackage.u05
    public final void a(ConnectionResult connectionResult) {
        if (!this.m.k(this.f, connectionResult.I1())) {
            r();
        }
        if (this.i) {
            return;
        }
        this.c.d(connectionResult);
        this.c.a();
    }

    @Override // defpackage.u05
    public final void b(int i, boolean z) {
        if (i == 1 && !z && !this.i) {
            this.i = true;
            if (this.n == null && !jz.a()) {
                try {
                    this.n = this.m.v(this.f.getApplicationContext(), new g05(this));
                } catch (SecurityException unused) {
                }
            }
            h05 h05Var = this.l;
            h05Var.sendMessageDelayed(h05Var.obtainMessage(1), this.j);
            h05 h05Var2 = this.l;
            h05Var2.sendMessageDelayed(h05Var2.obtainMessage(2), this.k);
        }
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.x.a.toArray(new BasePendingResult[0])) {
            basePendingResult.e(k15.c);
        }
        this.c.b(i);
        this.c.a();
        if (i == 2) {
            t();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void c(int i) {
        this.b.lock();
        boolean z = true;
        if (i != 3 && i != 1 && i != 2) {
            z = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i);
            zt2.b(z, sb.toString());
            n(i);
            t();
        } finally {
            this.b.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void connect() {
        this.b.lock();
        try {
            if (this.e >= 0) {
                zt2.n(this.v != null, "Sign-in mode should have been set explicitly by auto-manage.");
            } else {
                Integer num = this.v;
                if (num == null) {
                    this.v = Integer.valueOf(m(this.o.values(), false));
                } else if (num.intValue() == 2) {
                    throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
                }
            }
            c(((Integer) zt2.j(this.v)).intValue());
        } finally {
            this.b.unlock();
        }
    }

    @Override // defpackage.u05
    public final void d(Bundle bundle) {
        while (!this.h.isEmpty()) {
            f(this.h.remove());
        }
        this.c.c(bundle);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void disconnect() {
        this.b.lock();
        try {
            this.x.a();
            v05 v05Var = this.d;
            if (v05Var != null) {
                v05Var.c();
            }
            this.t.a();
            for (b<?, ?> bVar : this.h) {
                bVar.k(null);
                bVar.b();
            }
            this.h.clear();
            if (this.d == null) {
                return;
            }
            r();
            this.c.a();
        } finally {
            this.b.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void e(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("mContext=").println(this.f);
        printWriter.append((CharSequence) str).append("mResuming=").print(this.i);
        printWriter.append(" mWorkQueue.size()=").print(this.h.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.x.a.size());
        v05 v05Var = this.d;
        if (v05Var != null) {
            v05Var.f(str, fileDescriptor, printWriter, strArr);
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final <A extends a.b, T extends b<? extends l83, A>> T f(T t) {
        a<?> r = t.r();
        boolean containsKey = this.o.containsKey(t.s());
        String d = r != null ? r.d() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(d).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(d);
        sb.append(" required for this call.");
        zt2.b(containsKey, sb.toString());
        this.b.lock();
        try {
            v05 v05Var = this.d;
            if (v05Var != null) {
                if (this.i) {
                    this.h.add(t);
                    while (!this.h.isEmpty()) {
                        b<?, ?> remove = this.h.remove();
                        this.x.b(remove);
                        remove.w(Status.k0);
                    }
                    return t;
                }
                return (T) v05Var.g(t);
            }
            throw new IllegalStateException("GoogleApiClient is not connected yet.");
        } finally {
            this.b.unlock();
        }
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final Looper g() {
        return this.g;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final boolean h() {
        v05 v05Var = this.d;
        return v05Var != null && v05Var.e();
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void i(GoogleApiClient.c cVar) {
        this.c.f(cVar);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void j(GoogleApiClient.c cVar) {
        this.c.h(cVar);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void l(j15 j15Var) {
        v05 v05Var;
        this.b.lock();
        try {
            Set<j15> set = this.w;
            if (set == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!set.remove(j15Var)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!w() && (v05Var = this.d) != null) {
                v05Var.d();
            }
        } finally {
            this.b.unlock();
        }
    }

    public final void n(int i) {
        Integer num = this.v;
        if (num == null) {
            this.v = Integer.valueOf(i);
        } else if (num.intValue() != i) {
            String p = p(i);
            String p2 = p(this.v.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(p).length() + 51 + String.valueOf(p2).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(p);
            sb.append(". Mode was already set to ");
            sb.append(p2);
            throw new IllegalStateException(sb.toString());
        }
        if (this.d != null) {
            return;
        }
        boolean z = false;
        boolean z2 = false;
        for (a.f fVar : this.o.values()) {
            if (fVar.u()) {
                z = true;
            }
            if (fVar.e()) {
                z2 = true;
            }
        }
        int intValue = this.v.intValue();
        if (intValue != 1) {
            if (intValue == 2 && z) {
                this.d = t25.i(this.f, this, this.b, this.g, this.m, this.o, this.q, this.r, this.s, this.u);
                return;
            }
        } else if (!z) {
            throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
        } else {
            if (z2) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
        }
        this.d = new i05(this.f, this, this.b, this.g, this.m, this.o, this.q, this.r, this.s, this.u, this);
    }

    public final boolean r() {
        if (this.i) {
            this.i = false;
            this.l.removeMessages(2);
            this.l.removeMessages(1);
            zabj zabjVar = this.n;
            if (zabjVar != null) {
                zabjVar.a();
                this.n = null;
            }
            return true;
        }
        return false;
    }

    public final String s() {
        StringWriter stringWriter = new StringWriter();
        e("", null, new PrintWriter(stringWriter), null);
        return stringWriter.toString();
    }

    public final void t() {
        this.c.g();
        ((v05) zt2.j(this.d)).a();
    }

    public final void u() {
        this.b.lock();
        try {
            if (this.i) {
                t();
            }
        } finally {
            this.b.unlock();
        }
    }

    public final void v() {
        this.b.lock();
        try {
            if (r()) {
                t();
            }
        } finally {
            this.b.unlock();
        }
    }

    public final boolean w() {
        this.b.lock();
        try {
            Set<j15> set = this.w;
            if (set == null) {
                this.b.unlock();
                return false;
            }
            return !set.isEmpty();
        } finally {
            this.b.unlock();
        }
    }
}
