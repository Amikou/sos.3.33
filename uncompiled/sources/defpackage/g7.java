package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityImportPrivateKeyBinding.java */
/* renamed from: g7  reason: default package */
/* loaded from: classes2.dex */
public final class g7 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final TextInputEditText d;
    public final zh4 e;

    public g7(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, TextInputEditText textInputEditText, MaterialCardView materialCardView, ImageView imageView, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, LinearLayoutCompat linearLayoutCompat, ScrollView scrollView, TextView textView, zh4 zh4Var) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = textInputEditText;
        this.e = zh4Var;
    }

    public static g7 a(View view) {
        int i = R.id.btnBack;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBack);
        if (materialButton != null) {
            i = R.id.btnConfirm;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnConfirm);
            if (materialButton2 != null) {
                i = R.id.editPrivateKey;
                TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.editPrivateKey);
                if (textInputEditText != null) {
                    i = R.id.editPrivateKeyParent;
                    MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.editPrivateKeyParent);
                    if (materialCardView != null) {
                        i = R.id.imageView4;
                        ImageView imageView = (ImageView) ai4.a(view, R.id.imageView4);
                        if (imageView != null) {
                            i = R.id.img_logo;
                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                            if (appCompatImageView != null) {
                                i = R.id.img_safemoon;
                                AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.img_safemoon);
                                if (appCompatImageView2 != null) {
                                    i = R.id.img_wallet;
                                    AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.img_wallet);
                                    if (appCompatImageView3 != null) {
                                        i = R.id.img_wallet_parent;
                                        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.img_wallet_parent);
                                        if (linearLayoutCompat != null) {
                                            i = R.id.scrollView4;
                                            ScrollView scrollView = (ScrollView) ai4.a(view, R.id.scrollView4);
                                            if (scrollView != null) {
                                                i = R.id.textView;
                                                TextView textView = (TextView) ai4.a(view, R.id.textView);
                                                if (textView != null) {
                                                    i = R.id.view_barcode_component_layout;
                                                    View a = ai4.a(view, R.id.view_barcode_component_layout);
                                                    if (a != null) {
                                                        return new g7((ConstraintLayout) view, materialButton, materialButton2, textInputEditText, materialCardView, imageView, appCompatImageView, appCompatImageView2, appCompatImageView3, linearLayoutCompat, scrollView, textView, zh4.a(a));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static g7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static g7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_import_private_key, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
