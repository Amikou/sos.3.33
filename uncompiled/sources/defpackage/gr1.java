package defpackage;

/* compiled from: InstanceFactory.java */
/* renamed from: gr1  reason: default package */
/* loaded from: classes.dex */
public final class gr1<T> implements z11<T> {
    public final T a;

    static {
        new gr1(null);
    }

    public gr1(T t) {
        this.a = t;
    }

    public static <T> z11<T> a(T t) {
        return new gr1(yt2.c(t, "instance cannot be null"));
    }

    @Override // defpackage.ew2
    public T get() {
        return this.a;
    }
}
