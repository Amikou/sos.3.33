package defpackage;

import android.os.Bundle;
import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzkq;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: rn5  reason: default package */
/* loaded from: classes.dex */
public final class rn5 implements Runnable {
    public final /* synthetic */ Bundle a;
    public final /* synthetic */ dp5 f0;

    public rn5(dp5 dp5Var, Bundle bundle) {
        this.f0 = dp5Var;
        this.a = bundle;
    }

    @Override // java.lang.Runnable
    public final void run() {
        zzkq zzkqVar;
        dp5 dp5Var = this.f0;
        Bundle bundle = this.a;
        dp5Var.e();
        dp5Var.g();
        zt2.j(bundle);
        zt2.f(bundle.getString(PublicResolver.FUNC_NAME));
        if (dp5Var.a.h()) {
            if (dp5Var.a.z().v(null, qf5.x0)) {
                zzkqVar = new zzkq(bundle.getString(PublicResolver.FUNC_NAME), 0L, null, "");
            } else {
                zzkqVar = new zzkq(bundle.getString(PublicResolver.FUNC_NAME), 0L, null, null);
            }
            try {
                dp5Var.a.R().M(new zzaa(bundle.getString("app_id"), bundle.getString("origin"), zzkqVar, bundle.getLong("creation_timestamp"), bundle.getBoolean("active"), bundle.getString("trigger_event_name"), null, bundle.getLong("trigger_timeout"), null, bundle.getLong("time_to_live"), dp5Var.a.G().J(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), bundle.getLong("creation_timestamp"), true, false)));
                return;
            } catch (IllegalArgumentException unused) {
                return;
            }
        }
        dp5Var.a.w().v().a("Conditional property not cleared since app measurement is disabled");
    }
}
