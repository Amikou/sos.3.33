package defpackage;

import java.util.Arrays;
import java.util.List;

/* compiled from: Collection.java */
/* renamed from: i10  reason: default package */
/* loaded from: classes3.dex */
public class i10 {
    public static String[] EMPTY_STRING_ARRAY = new String[0];

    /* compiled from: Collection.java */
    /* renamed from: i10$a */
    /* loaded from: classes3.dex */
    public interface a<R, S> {
        S apply(R r);
    }

    private i10() {
    }

    public static <T> T[] create(T... tArr) {
        return tArr;
    }

    public static <T> String join(List<T> list, String str, a<T, String> aVar) {
        String str2 = "";
        int i = 0;
        while (i < list.size()) {
            str2 = str2 + aVar.apply(list.get(i)).trim();
            i++;
            if (i < list.size()) {
                str2 = str2 + str;
            }
        }
        return str2;
    }

    public static String[] tail(String[] strArr) {
        if (strArr.length == 0) {
            return EMPTY_STRING_ARRAY;
        }
        return (String[]) Arrays.copyOfRange(strArr, 1, strArr.length);
    }

    public static String join(List<String> list, String str) {
        String str2 = "";
        int i = 0;
        while (i < list.size()) {
            str2 = str2 + list.get(i).trim();
            i++;
            if (i < list.size()) {
                str2 = str2 + str;
            }
        }
        return str2;
    }
}
