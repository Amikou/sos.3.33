package defpackage;

import com.google.android.gms.internal.measurement.b1;
import com.google.android.gms.internal.measurement.d1;
import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.x1;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: tj5  reason: default package */
/* loaded from: classes.dex */
public final class tj5 extends w1<b1, tj5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public tj5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.b1 r0 = com.google.android.gms.internal.measurement.b1.I()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.tj5.<init>():void");
    }

    public final tj5 A(int i, d1 d1Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.J((b1) this.f0, i, d1Var);
        return this;
    }

    public final tj5 B(int i, yj5 yj5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.J((b1) this.f0, i, yj5Var.o());
        return this;
    }

    public final tj5 C(d1 d1Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.K((b1) this.f0, d1Var);
        return this;
    }

    public final tj5 D(yj5 yj5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.K((b1) this.f0, yj5Var.o());
        return this;
    }

    public final tj5 E(Iterable<? extends d1> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.L((b1) this.f0, iterable);
        return this;
    }

    public final tj5 G() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        ((b1) this.f0).zze = x1.o();
        return this;
    }

    public final tj5 H(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.N((b1) this.f0, i);
        return this;
    }

    public final String I() {
        return ((b1) this.f0).A();
    }

    public final tj5 J(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.O((b1) this.f0, str);
        return this;
    }

    public final boolean K() {
        return ((b1) this.f0).B();
    }

    public final long L() {
        return ((b1) this.f0).C();
    }

    public final tj5 M(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.P((b1) this.f0, j);
        return this;
    }

    public final long N() {
        return ((b1) this.f0).E();
    }

    public final tj5 O(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        b1.Q((b1) this.f0, j);
        return this;
    }

    public final List<d1> v() {
        return Collections.unmodifiableList(((b1) this.f0).x());
    }

    public final int x() {
        return ((b1) this.f0).y();
    }

    public final d1 y(int i) {
        return ((b1) this.f0).z(i);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ tj5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.b1 r1 = com.google.android.gms.internal.measurement.b1.I()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.tj5.<init>(wi5):void");
    }
}
