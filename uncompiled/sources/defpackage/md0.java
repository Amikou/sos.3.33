package defpackage;

import java.math.BigInteger;

/* renamed from: md0  reason: default package */
/* loaded from: classes2.dex */
public class md0 implements ty {
    public BigInteger a;
    public BigInteger f0;
    public BigInteger g0;

    public md0(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, int i, int i2, BigInteger bigInteger4, nd0 nd0Var) {
        if (i2 != 0) {
            if (i2 > bigInteger.bitLength()) {
                throw new IllegalArgumentException("when l value specified, it must satisfy 2^(l-1) <= p");
            }
            if (i2 < i) {
                throw new IllegalArgumentException("when l value specified, it may not be less than m value");
            }
        }
        if (i > bigInteger.bitLength()) {
            throw new IllegalArgumentException("unsafe p value so small specific l required");
        }
        this.a = bigInteger2;
        this.f0 = bigInteger;
        this.g0 = bigInteger3;
    }

    public BigInteger a() {
        return this.a;
    }

    public BigInteger b() {
        return this.f0;
    }

    public BigInteger c() {
        return this.g0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof md0) {
            md0 md0Var = (md0) obj;
            if (c() != null) {
                if (!c().equals(md0Var.c())) {
                    return false;
                }
            } else if (md0Var.c() != null) {
                return false;
            }
            return md0Var.b().equals(this.f0) && md0Var.a().equals(this.a);
        }
        return false;
    }

    public int hashCode() {
        return (b().hashCode() ^ a().hashCode()) ^ (c() != null ? c().hashCode() : 0);
    }
}
