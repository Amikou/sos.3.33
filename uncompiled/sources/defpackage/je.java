package defpackage;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.Choreographer;
import java.util.ArrayList;

/* compiled from: AnimationHandler.java */
/* renamed from: je  reason: default package */
/* loaded from: classes.dex */
public class je {
    public static final ThreadLocal<je> g = new ThreadLocal<>();
    public c d;
    public final vo3<b, Long> a = new vo3<>();
    public final ArrayList<b> b = new ArrayList<>();
    public final a c = new a();
    public long e = 0;
    public boolean f = false;

    /* compiled from: AnimationHandler.java */
    /* renamed from: je$a */
    /* loaded from: classes.dex */
    public class a {
        public a() {
        }

        public void a() {
            je.this.e = SystemClock.uptimeMillis();
            je jeVar = je.this;
            jeVar.c(jeVar.e);
            if (je.this.b.size() > 0) {
                je.this.e().a();
            }
        }
    }

    /* compiled from: AnimationHandler.java */
    /* renamed from: je$b */
    /* loaded from: classes.dex */
    public interface b {
        boolean a(long j);
    }

    /* compiled from: AnimationHandler.java */
    /* renamed from: je$c */
    /* loaded from: classes.dex */
    public static abstract class c {
        public final a a;

        public c(a aVar) {
            this.a = aVar;
        }

        public abstract void a();
    }

    /* compiled from: AnimationHandler.java */
    /* renamed from: je$d */
    /* loaded from: classes.dex */
    public static class d extends c {
        public final Runnable b;
        public final Handler c;
        public long d;

        /* compiled from: AnimationHandler.java */
        /* renamed from: je$d$a */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public a() {
            }

            @Override // java.lang.Runnable
            public void run() {
                d.this.d = SystemClock.uptimeMillis();
                d.this.a.a();
            }
        }

        public d(a aVar) {
            super(aVar);
            this.d = -1L;
            this.b = new a();
            this.c = new Handler(Looper.myLooper());
        }

        @Override // defpackage.je.c
        public void a() {
            this.c.postDelayed(this.b, Math.max(10 - (SystemClock.uptimeMillis() - this.d), 0L));
        }
    }

    /* compiled from: AnimationHandler.java */
    /* renamed from: je$e */
    /* loaded from: classes.dex */
    public static class e extends c {
        public final Choreographer b;
        public final Choreographer.FrameCallback c;

        /* compiled from: AnimationHandler.java */
        /* renamed from: je$e$a */
        /* loaded from: classes.dex */
        public class a implements Choreographer.FrameCallback {
            public a() {
            }

            @Override // android.view.Choreographer.FrameCallback
            public void doFrame(long j) {
                e.this.a.a();
            }
        }

        public e(a aVar) {
            super(aVar);
            this.b = Choreographer.getInstance();
            this.c = new a();
        }

        @Override // defpackage.je.c
        public void a() {
            this.b.postFrameCallback(this.c);
        }
    }

    public static je d() {
        ThreadLocal<je> threadLocal = g;
        if (threadLocal.get() == null) {
            threadLocal.set(new je());
        }
        return threadLocal.get();
    }

    public void a(b bVar, long j) {
        if (this.b.size() == 0) {
            e().a();
        }
        if (!this.b.contains(bVar)) {
            this.b.add(bVar);
        }
        if (j > 0) {
            this.a.put(bVar, Long.valueOf(SystemClock.uptimeMillis() + j));
        }
    }

    public final void b() {
        if (this.f) {
            for (int size = this.b.size() - 1; size >= 0; size--) {
                if (this.b.get(size) == null) {
                    this.b.remove(size);
                }
            }
            this.f = false;
        }
    }

    public void c(long j) {
        long uptimeMillis = SystemClock.uptimeMillis();
        for (int i = 0; i < this.b.size(); i++) {
            b bVar = this.b.get(i);
            if (bVar != null && f(bVar, uptimeMillis)) {
                bVar.a(j);
            }
        }
        b();
    }

    public c e() {
        if (this.d == null) {
            if (Build.VERSION.SDK_INT >= 16) {
                this.d = new e(this.c);
            } else {
                this.d = new d(this.c);
            }
        }
        return this.d;
    }

    public final boolean f(b bVar, long j) {
        Long l = this.a.get(bVar);
        if (l == null) {
            return true;
        }
        if (l.longValue() < j) {
            this.a.remove(bVar);
            return true;
        }
        return false;
    }

    public void g(b bVar) {
        this.a.remove(bVar);
        int indexOf = this.b.indexOf(bVar);
        if (indexOf >= 0) {
            this.b.set(indexOf, null);
            this.f = true;
        }
    }
}
