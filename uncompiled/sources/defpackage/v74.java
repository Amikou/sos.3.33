package defpackage;

import android.os.Trace;
import androidx.media3.common.util.b;

/* compiled from: TraceUtil.java */
/* renamed from: v74  reason: default package */
/* loaded from: classes.dex */
public final class v74 {
    public static void a(String str) {
        if (b.a >= 18) {
            b(str);
        }
    }

    public static void b(String str) {
        Trace.beginSection(str);
    }

    public static void c() {
        if (b.a >= 18) {
            d();
        }
    }

    public static void d() {
        Trace.endSection();
    }
}
