package defpackage;

import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Continuation.kt */
/* renamed from: s70  reason: default package */
/* loaded from: classes2.dex */
public final class s70 {
    public static final <T> void a(tc1<? super q70<? super T>, ? extends Object> tc1Var, q70<? super T> q70Var) {
        fs1.f(tc1Var, "$this$startCoroutine");
        fs1.f(q70Var, "completion");
        q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(IntrinsicsKt__IntrinsicsJvmKt.a(tc1Var, q70Var));
        te4 te4Var = te4.a;
        Result.a aVar = Result.Companion;
        c.resumeWith(Result.m52constructorimpl(te4Var));
    }

    public static final <R, T> void b(hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var, R r, q70<? super T> q70Var) {
        fs1.f(hd1Var, "$this$startCoroutine");
        fs1.f(q70Var, "completion");
        q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(IntrinsicsKt__IntrinsicsJvmKt.b(hd1Var, r, q70Var));
        te4 te4Var = te4.a;
        Result.a aVar = Result.Companion;
        c.resumeWith(Result.m52constructorimpl(te4Var));
    }
}
