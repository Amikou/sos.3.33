package defpackage;

import android.graphics.RectF;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* compiled from: AdjustedCornerSize.java */
/* renamed from: v9  reason: default package */
/* loaded from: classes2.dex */
public final class v9 implements v80 {
    public final v80 a;
    public final float b;

    public v9(float f, v80 v80Var) {
        while (v80Var instanceof v9) {
            v80Var = ((v9) v80Var).a;
            f += ((v9) v80Var).b;
        }
        this.a = v80Var;
        this.b = f;
    }

    @Override // defpackage.v80
    public float a(RectF rectF) {
        return Math.max((float) Utils.FLOAT_EPSILON, this.a.a(rectF) + this.b);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof v9) {
            v9 v9Var = (v9) obj;
            return this.a.equals(v9Var.a) && this.b == v9Var.b;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, Float.valueOf(this.b)});
    }
}
