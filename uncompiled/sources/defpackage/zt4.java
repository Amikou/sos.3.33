package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.play.core.internal.ar;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: zt4  reason: default package */
/* loaded from: classes2.dex */
public final class zt4<T extends IInterface> {
    public static final Map<String, Handler> l = new HashMap();
    public final Context a;
    public final it4 b;
    public final String c;
    public boolean e;
    public final Intent f;
    public final rt4<T> g;
    public ServiceConnection j;
    public T k;
    public final List<jt4> d = new ArrayList();
    public final IBinder.DeathRecipient i = new IBinder.DeathRecipient(this) { // from class: lt4
        public final zt4 a;

        {
            this.a = this;
        }

        @Override // android.os.IBinder.DeathRecipient
        public final void binderDied() {
            this.a.n();
        }
    };
    public final WeakReference<qt4> h = new WeakReference<>(null);

    public zt4(Context context, it4 it4Var, String str, Intent intent, rt4<T> rt4Var) {
        this.a = context;
        this.b = it4Var;
        this.c = str;
        this.f = intent;
        this.g = rt4Var;
    }

    public static /* synthetic */ void d(zt4 zt4Var, jt4 jt4Var) {
        if (zt4Var.k != null || zt4Var.e) {
            if (!zt4Var.e) {
                jt4Var.run();
                return;
            }
            zt4Var.b.d("Waiting to bind to the service.", new Object[0]);
            zt4Var.d.add(jt4Var);
            return;
        }
        zt4Var.b.d("Initiate binding to the service.", new Object[0]);
        zt4Var.d.add(jt4Var);
        xt4 xt4Var = new xt4(zt4Var);
        zt4Var.j = xt4Var;
        zt4Var.e = true;
        if (zt4Var.a.bindService(zt4Var.f, xt4Var, 1)) {
            return;
        }
        zt4Var.b.d("Failed to bind to the service.", new Object[0]);
        zt4Var.e = false;
        for (jt4 jt4Var2 : zt4Var.d) {
            tx4<?> b = jt4Var2.b();
            if (b != null) {
                b.d(new ar());
            }
        }
        zt4Var.d.clear();
    }

    public static /* synthetic */ void j(zt4 zt4Var) {
        zt4Var.b.d("linkToDeath", new Object[0]);
        try {
            zt4Var.k.asBinder().linkToDeath(zt4Var.i, 0);
        } catch (RemoteException e) {
            zt4Var.b.c(e, "linkToDeath failed", new Object[0]);
        }
    }

    public static /* synthetic */ void m(zt4 zt4Var) {
        zt4Var.b.d("unlinkToDeath", new Object[0]);
        zt4Var.k.asBinder().unlinkToDeath(zt4Var.i, 0);
    }

    public final void a(jt4 jt4Var) {
        r(new nt4(this, jt4Var.b(), jt4Var));
    }

    public final void b() {
        r(new pt4(this));
    }

    public final T c() {
        return this.k;
    }

    public final /* bridge */ /* synthetic */ void n() {
        this.b.d("reportBinderDeath", new Object[0]);
        qt4 qt4Var = this.h.get();
        if (qt4Var != null) {
            this.b.d("calling onBinderDied", new Object[0]);
            qt4Var.a();
            return;
        }
        this.b.d("%s : Binder has died.", this.c);
        for (jt4 jt4Var : this.d) {
            tx4<?> b = jt4Var.b();
            if (b != null) {
                b.d(Build.VERSION.SDK_INT < 15 ? new RemoteException() : new RemoteException(String.valueOf(this.c).concat(" : Binder has died.")));
            }
        }
        this.d.clear();
    }

    public final void r(jt4 jt4Var) {
        Handler handler;
        Map<String, Handler> map = l;
        synchronized (map) {
            if (!map.containsKey(this.c)) {
                HandlerThread handlerThread = new HandlerThread(this.c, 10);
                handlerThread.start();
                map.put(this.c, new Handler(handlerThread.getLooper()));
            }
            handler = map.get(this.c);
        }
        handler.post(jt4Var);
    }
}
