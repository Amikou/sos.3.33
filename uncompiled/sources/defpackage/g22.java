package defpackage;

/* renamed from: g22  reason: default package */
/* loaded from: classes2.dex */
public abstract class g22 implements g11, j72 {
    public static final long[] o = {4794697086780616226L, 8158064640168781261L, -5349999486874862801L, -1606136188198331460L, 4131703408338449720L, 6480981068601479193L, -7908458776815382629L, -6116909921290321640L, -2880145864133508542L, 1334009975649890238L, 2608012711638119052L, 6128411473006802146L, 8268148722764581231L, -9160688886553864527L, -7215885187991268811L, -4495734319001033068L, -1973867731355612462L, -1171420211273849373L, 1135362057144423861L, 2597628984639134821L, 3308224258029322869L, 5365058923640841347L, 6679025012923562964L, 8573033837759648693L, -7476448914759557205L, -6327057829258317296L, -5763719355590565569L, -4658551843659510044L, -4116276920077217854L, -3051310485924567259L, 489312712824947311L, 1452737877330783856L, 2861767655752347644L, 3322285676063803686L, 5560940570517711597L, 5996557281743188959L, 7280758554555802590L, 8532644243296465576L, -9096487096722542874L, -7894198246740708037L, -6719396339535248540L, -6333637450476146687L, -4446306890439682159L, -4076793802049405392L, -3345356375505022440L, -2983346525034927856L, -860691631967231958L, 1182934255886127544L, 1847814050463011016L, 2177327727835720531L, 2830643537854262169L, 3796741975233480872L, 4115178125766777443L, 5681478168544905931L, 6601373596472566643L, 7507060721942968483L, 8399075790359081724L, 8693463985226723168L, -8878714635349349518L, -8302665154208450068L, -8016688836872298968L, -6606660893046293015L, -4685533653050689259L, -4147400797238176981L, -3880063495543823972L, -3348786107499101689L, -1523767162380948706L, -757361751448694408L, 500013540394364858L, 748580250866718886L, 1242879168328830382L, 1977374033974150939L, 2944078676154940804L, 3659926193048069267L, 4368137639120453308L, 4836135668995329356L, 5532061633213252278L, 6448918945643986474L, 6902733635092675308L, 7801388544844847127L};
    public byte[] a;
    public int b;
    public long c;
    public long d;
    public long e;
    public long f;
    public long g;
    public long h;
    public long i;
    public long j;
    public long k;
    public long l;
    public long[] m;
    public int n;

    public g22() {
        this.a = new byte[8];
        this.m = new long[80];
        this.b = 0;
        reset();
    }

    public g22(g22 g22Var) {
        this.a = new byte[8];
        this.m = new long[80];
        p(g22Var);
    }

    @Override // defpackage.qo0
    public void b(byte[] bArr, int i, int i2) {
        while (this.b != 0 && i2 > 0) {
            c(bArr[i]);
            i++;
            i2--;
        }
        while (i2 > this.a.length) {
            t(bArr, i);
            byte[] bArr2 = this.a;
            i += bArr2.length;
            i2 -= bArr2.length;
            this.c += bArr2.length;
        }
        while (i2 > 0) {
            c(bArr[i]);
            i++;
            i2--;
        }
    }

    @Override // defpackage.qo0
    public void c(byte b) {
        byte[] bArr = this.a;
        int i = this.b;
        int i2 = i + 1;
        this.b = i2;
        bArr[i] = b;
        if (i2 == bArr.length) {
            t(bArr, 0);
            this.b = 0;
        }
        this.c++;
    }

    @Override // defpackage.g11
    public int f() {
        return 128;
    }

    public final long i(long j, long j2, long j3) {
        return ((~j) & j3) ^ (j2 & j);
    }

    public final long j(long j, long j2, long j3) {
        return ((j & j3) ^ (j & j2)) ^ (j2 & j3);
    }

    public final long k(long j) {
        return (j >>> 7) ^ (((j << 63) | (j >>> 1)) ^ ((j << 56) | (j >>> 8)));
    }

    public final long l(long j) {
        return (j >>> 6) ^ (((j << 45) | (j >>> 19)) ^ ((j << 3) | (j >>> 61)));
    }

    public final long m(long j) {
        return ((j >>> 39) | (j << 25)) ^ (((j << 36) | (j >>> 28)) ^ ((j << 30) | (j >>> 34)));
    }

    public final long n(long j) {
        return ((j >>> 41) | (j << 23)) ^ (((j << 50) | (j >>> 14)) ^ ((j << 46) | (j >>> 18)));
    }

    public final void o() {
        long j = this.c;
        if (j > 2305843009213693951L) {
            this.d += j >>> 61;
            this.c = j & 2305843009213693951L;
        }
    }

    public void p(g22 g22Var) {
        byte[] bArr = g22Var.a;
        System.arraycopy(bArr, 0, this.a, 0, bArr.length);
        this.b = g22Var.b;
        this.c = g22Var.c;
        this.d = g22Var.d;
        this.e = g22Var.e;
        this.f = g22Var.f;
        this.g = g22Var.g;
        this.h = g22Var.h;
        this.i = g22Var.i;
        this.j = g22Var.j;
        this.k = g22Var.k;
        this.l = g22Var.l;
        long[] jArr = g22Var.m;
        System.arraycopy(jArr, 0, this.m, 0, jArr.length);
        this.n = g22Var.n;
    }

    public void q() {
        o();
        long j = this.c << 3;
        long j2 = this.d;
        byte b = Byte.MIN_VALUE;
        while (true) {
            c(b);
            if (this.b == 0) {
                s(j, j2);
                r();
                return;
            }
            b = 0;
        }
    }

    public void r() {
        o();
        for (int i = 16; i <= 79; i++) {
            long[] jArr = this.m;
            long l = l(jArr[i - 2]);
            long[] jArr2 = this.m;
            jArr[i] = l + jArr2[i - 7] + k(jArr2[i - 15]) + this.m[i - 16];
        }
        long j = this.e;
        long j2 = this.f;
        long j3 = this.g;
        long j4 = this.h;
        long j5 = this.i;
        long j6 = this.j;
        long j7 = this.k;
        long j8 = j6;
        long j9 = j4;
        int i2 = 0;
        long j10 = j2;
        long j11 = j3;
        long j12 = j5;
        int i3 = 0;
        long j13 = this.l;
        long j14 = j;
        long j15 = j7;
        while (i3 < 10) {
            long j16 = j12;
            long[] jArr3 = o;
            int i4 = i2 + 1;
            long n = j13 + n(j12) + i(j12, j8, j15) + jArr3[i2] + this.m[i2];
            long j17 = j9 + n;
            long m = n + m(j14) + j(j14, j10, j11);
            int i5 = i4 + 1;
            long n2 = j15 + n(j17) + i(j17, j16, j8) + jArr3[i4] + this.m[i4];
            long j18 = j11 + n2;
            long m2 = n2 + m(m) + j(m, j14, j10);
            int i6 = i5 + 1;
            long n3 = j8 + n(j18) + i(j18, j17, j16) + jArr3[i5] + this.m[i5];
            long j19 = j10 + n3;
            long m3 = n3 + m(m2) + j(m2, m, j14);
            int i7 = i6 + 1;
            long n4 = j16 + n(j19) + i(j19, j18, j17) + jArr3[i6] + this.m[i6];
            long j20 = j14 + n4;
            long m4 = n4 + m(m3) + j(m3, m2, m);
            int i8 = i7 + 1;
            long n5 = j17 + n(j20) + i(j20, j19, j18) + jArr3[i7] + this.m[i7];
            long j21 = m + n5;
            long m5 = n5 + m(m4) + j(m4, m3, m2);
            int i9 = i8 + 1;
            long n6 = j18 + n(j21) + i(j21, j20, j19) + jArr3[i8] + this.m[i8];
            long j22 = m2 + n6;
            long m6 = n6 + m(m5) + j(m5, m4, m3);
            j15 = j22;
            int i10 = i9 + 1;
            long n7 = j19 + n(j22) + i(j22, j21, j20) + jArr3[i9] + this.m[i9];
            long j23 = m3 + n7;
            j8 = j23;
            j10 = n7 + m(m6) + j(m6, m5, m4);
            long n8 = j20 + n(j23) + i(j23, j15, j21) + jArr3[i10] + this.m[i10];
            long m7 = n8 + m(j10) + j(j10, m6, m5);
            i3++;
            j12 = m4 + n8;
            j11 = m6;
            j13 = j21;
            j9 = m5;
            i2 = i10 + 1;
            j14 = m7;
        }
        this.e += j14;
        this.f += j10;
        this.g += j11;
        this.h += j9;
        this.i += j12;
        this.j += j8;
        this.k += j15;
        this.l += j13;
        this.n = 0;
        for (int i11 = 0; i11 < 16; i11++) {
            this.m[i11] = 0;
        }
    }

    @Override // defpackage.qo0
    public void reset() {
        this.c = 0L;
        this.d = 0L;
        int i = 0;
        this.b = 0;
        int i2 = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i2 >= bArr.length) {
                break;
            }
            bArr[i2] = 0;
            i2++;
        }
        this.n = 0;
        while (true) {
            long[] jArr = this.m;
            if (i == jArr.length) {
                return;
            }
            jArr[i] = 0;
            i++;
        }
    }

    public void s(long j, long j2) {
        if (this.n > 14) {
            r();
        }
        long[] jArr = this.m;
        jArr[14] = j2;
        jArr[15] = j;
    }

    public void t(byte[] bArr, int i) {
        this.m[this.n] = ro2.b(bArr, i);
        int i2 = this.n + 1;
        this.n = i2;
        if (i2 == 16) {
            r();
        }
    }
}
