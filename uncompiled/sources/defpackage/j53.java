package defpackage;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import androidx.recyclerview.widget.o;
import com.github.mikephil.charting.utils.Utils;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsDataAndToken;

/* compiled from: ReflectionTokenDataAdapter.kt */
/* renamed from: j53  reason: default package */
/* loaded from: classes2.dex */
public final class j53 extends o<RoomReflectionsDataAndToken, b> {

    /* compiled from: ReflectionTokenDataAdapter.kt */
    /* renamed from: j53$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<RoomReflectionsDataAndToken> {
        public static final a a = new a();

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(RoomReflectionsDataAndToken roomReflectionsDataAndToken, RoomReflectionsDataAndToken roomReflectionsDataAndToken2) {
            fs1.f(roomReflectionsDataAndToken, "oldItem");
            fs1.f(roomReflectionsDataAndToken2, "newItem");
            return roomReflectionsDataAndToken.getData().getTimeStamp() == roomReflectionsDataAndToken2.getData().getTimeStamp();
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(RoomReflectionsDataAndToken roomReflectionsDataAndToken, RoomReflectionsDataAndToken roomReflectionsDataAndToken2) {
            fs1.f(roomReflectionsDataAndToken, "oldItem");
            fs1.f(roomReflectionsDataAndToken2, "newItem");
            return fs1.b(roomReflectionsDataAndToken.getData().getId(), roomReflectionsDataAndToken2.getData().getId());
        }
    }

    /* compiled from: ReflectionTokenDataAdapter.kt */
    /* renamed from: j53$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final xk1 a;
        public final /* synthetic */ j53 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(j53 j53Var, xk1 xk1Var) {
            super(xk1Var.b());
            fs1.f(j53Var, "this$0");
            fs1.f(xk1Var, "binding");
            this.b = j53Var;
            this.a = xk1Var;
        }

        public final void a(int i) {
            RoomReflectionsDataAndToken a = j53.a(this.b, i);
            if (a == null) {
                return;
            }
            xk1 b = b();
            b.b.setText(a.getDisplayDate());
            Double priceUsd = a.getToken().getPriceUsd();
            double doubleValue = priceUsd == null ? 0.0d : priceUsd.doubleValue();
            Float diffBalance = a.getDiffBalance();
            Double valueOf = diffBalance == null ? null : Double.valueOf(diffBalance.floatValue());
            Double valueOf2 = valueOf != null ? Double.valueOf(valueOf.doubleValue() * doubleValue) : null;
            if (valueOf != null) {
                TextView textView = b.d;
                fs1.e(textView, "tvTokenNativeBalance");
                e30.R(textView, valueOf.doubleValue());
            }
            if (doubleValue > Utils.DOUBLE_EPSILON && valueOf2 != null) {
                TextView textView2 = b.c;
                fs1.e(textView2, "tvTokenBalance");
                e30.N(textView2, valueOf2.doubleValue(), true);
                return;
            }
            b.c.setText("");
        }

        public final xk1 b() {
            return this.a;
        }
    }

    public j53() {
        super(a.a);
    }

    public static final /* synthetic */ RoomReflectionsDataAndToken a(j53 j53Var, int i) {
        return j53Var.getItem(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.a(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        xk1 a2 = xk1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_reflection, viewGroup, false));
        fs1.e(a2, "bind(\n                La…ent, false)\n            )");
        return new b(this, a2);
    }
}
