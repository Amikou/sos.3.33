package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSwapSlippageBinding.java */
/* renamed from: bo0  reason: default package */
/* loaded from: classes2.dex */
public final class bo0 {
    public final MaterialButton a;
    public final RadioButton b;
    public final RadioButton c;
    public final RadioButton d;
    public final MaterialButton e;
    public final TextInputEditText f;
    public final MaterialButton g;
    public final SwitchMaterial h;

    public bo0(RelativeLayout relativeLayout, MaterialButton materialButton, RadioButton radioButton, RadioButton radioButton2, RadioButton radioButton3, MaterialButton materialButton2, MaterialTextView materialTextView, RadioGroup radioGroup, TextInputEditText textInputEditText, TextInputLayout textInputLayout, MaterialButton materialButton3, SwitchMaterial switchMaterial, LinearLayout linearLayout, MaterialTextView materialTextView2) {
        this.a = materialButton;
        this.b = radioButton;
        this.c = radioButton2;
        this.d = radioButton3;
        this.e = materialButton2;
        this.f = textInputEditText;
        this.g = materialButton3;
        this.h = switchMaterial;
    }

    public static bo0 a(View view) {
        int i = R.id.btnConfirm;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConfirm);
        if (materialButton != null) {
            i = R.id.chipSlip01;
            RadioButton radioButton = (RadioButton) ai4.a(view, R.id.chipSlip01);
            if (radioButton != null) {
                i = R.id.chipSlip05;
                RadioButton radioButton2 = (RadioButton) ai4.a(view, R.id.chipSlip05);
                if (radioButton2 != null) {
                    i = R.id.chipSlip1;
                    RadioButton radioButton3 = (RadioButton) ai4.a(view, R.id.chipSlip1);
                    if (radioButton3 != null) {
                        i = R.id.dialog_cross;
                        MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                        if (materialButton2 != null) {
                            i = R.id.dialog_title;
                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_title);
                            if (materialTextView != null) {
                                i = R.id.easySlippageGroup;
                                RadioGroup radioGroup = (RadioGroup) ai4.a(view, R.id.easySlippageGroup);
                                if (radioGroup != null) {
                                    i = R.id.edtSlippageManually;
                                    TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.edtSlippageManually);
                                    if (textInputEditText != null) {
                                        i = R.id.edtSlippageManuallyParent;
                                        TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.edtSlippageManuallyParent);
                                        if (textInputLayout != null) {
                                            i = R.id.helpPrompt;
                                            MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.helpPrompt);
                                            if (materialButton3 != null) {
                                                i = R.id.switchPopup;
                                                SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchPopup);
                                                if (switchMaterial != null) {
                                                    i = R.id.switchPopupLayout;
                                                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.switchPopupLayout);
                                                    if (linearLayout != null) {
                                                        i = R.id.txtSetManually;
                                                        MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtSetManually);
                                                        if (materialTextView2 != null) {
                                                            return new bo0((RelativeLayout) view, materialButton, radioButton, radioButton2, radioButton3, materialButton2, materialTextView, radioGroup, textInputEditText, textInputLayout, materialButton3, switchMaterial, linearLayout, materialTextView2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
