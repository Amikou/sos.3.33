package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.clearcut.zzr;
import com.google.android.gms.phenotype.ExperimentTokens;

/* renamed from: ii5  reason: default package */
/* loaded from: classes.dex */
public final class ii5 implements Parcelable.Creator<zze> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zze createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        zzr zzrVar = null;
        byte[] bArr = null;
        int[] iArr = null;
        String[] strArr = null;
        int[] iArr2 = null;
        byte[][] bArr2 = null;
        ExperimentTokens[] experimentTokensArr = null;
        boolean z = true;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    zzrVar = (zzr) SafeParcelReader.o(parcel, C, zzr.CREATOR);
                    break;
                case 3:
                    bArr = SafeParcelReader.g(parcel, C);
                    break;
                case 4:
                    iArr = SafeParcelReader.k(parcel, C);
                    break;
                case 5:
                    strArr = SafeParcelReader.q(parcel, C);
                    break;
                case 6:
                    iArr2 = SafeParcelReader.k(parcel, C);
                    break;
                case 7:
                    bArr2 = SafeParcelReader.h(parcel, C);
                    break;
                case 8:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 9:
                    experimentTokensArr = (ExperimentTokens[]) SafeParcelReader.s(parcel, C, ExperimentTokens.CREATOR);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zze(zzrVar, bArr, iArr, strArr, iArr2, bArr2, z, experimentTokensArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zze[] newArray(int i) {
        return new zze[i];
    }
}
