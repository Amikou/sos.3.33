package defpackage;

/* renamed from: e33  reason: default package */
/* loaded from: classes2.dex */
public class e33 extends re1 {
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int[] i;
    public int j;

    public e33() {
        this.i = new int[16];
        reset();
    }

    public e33(e33 e33Var) {
        super(e33Var);
        this.i = new int[16];
        o(e33Var);
    }

    @Override // defpackage.qo0
    public int a(byte[] bArr, int i) {
        j();
        u(this.d, bArr, i);
        u(this.e, bArr, i + 4);
        u(this.f, bArr, i + 8);
        u(this.g, bArr, i + 12);
        u(this.h, bArr, i + 16);
        reset();
        return 20;
    }

    @Override // defpackage.j72
    public j72 copy() {
        return new e33(this);
    }

    @Override // defpackage.j72
    public void d(j72 j72Var) {
        o((e33) j72Var);
    }

    @Override // defpackage.qo0
    public String g() {
        return "RIPEMD160";
    }

    @Override // defpackage.qo0
    public int h() {
        return 20;
    }

    @Override // defpackage.re1
    public void k() {
        int i = this.d;
        int i2 = this.e;
        int i3 = this.f;
        int i4 = this.g;
        int i5 = this.h;
        int n = n(p(i2, i3, i4) + i + this.i[0], 11) + i5;
        int n2 = n(i3, 10);
        int n3 = n(p(n, i2, n2) + i5 + this.i[1], 14) + i4;
        int n4 = n(i2, 10);
        int n5 = n(p(n3, n, n4) + i4 + this.i[2], 15) + n2;
        int n6 = n(n, 10);
        int n7 = n(n2 + p(n5, n3, n6) + this.i[3], 12) + n4;
        int n8 = n(n3, 10);
        int n9 = n(n4 + p(n7, n5, n8) + this.i[4], 5) + n6;
        int n10 = n(n5, 10);
        int n11 = n(n6 + p(n9, n7, n10) + this.i[5], 8) + n8;
        int n12 = n(n7, 10);
        int n13 = n(n8 + p(n11, n9, n12) + this.i[6], 7) + n10;
        int n14 = n(n9, 10);
        int n15 = n(n10 + p(n13, n11, n14) + this.i[7], 9) + n12;
        int n16 = n(n11, 10);
        int n17 = n(n12 + p(n15, n13, n16) + this.i[8], 11) + n14;
        int n18 = n(n13, 10);
        int n19 = n(n14 + p(n17, n15, n18) + this.i[9], 13) + n16;
        int n20 = n(n15, 10);
        int n21 = n(n16 + p(n19, n17, n20) + this.i[10], 14) + n18;
        int n22 = n(n17, 10);
        int n23 = n(n18 + p(n21, n19, n22) + this.i[11], 15) + n20;
        int n24 = n(n19, 10);
        int n25 = n(n20 + p(n23, n21, n24) + this.i[12], 6) + n22;
        int n26 = n(n21, 10);
        int n27 = n(n22 + p(n25, n23, n26) + this.i[13], 7) + n24;
        int n28 = n(n23, 10);
        int n29 = n(n24 + p(n27, n25, n28) + this.i[14], 9) + n26;
        int n30 = n(n25, 10);
        int n31 = n(n26 + p(n29, n27, n30) + this.i[15], 8) + n28;
        int n32 = n(n27, 10);
        int n33 = n(i + t(i2, i3, i4) + this.i[5] + 1352829926, 8) + i5;
        int n34 = n(i3, 10);
        int n35 = n(i5 + t(n33, i2, n34) + this.i[14] + 1352829926, 9) + i4;
        int n36 = n(i2, 10);
        int n37 = n(i4 + t(n35, n33, n36) + this.i[7] + 1352829926, 9) + n34;
        int n38 = n(n33, 10);
        int n39 = n(n34 + t(n37, n35, n38) + this.i[0] + 1352829926, 11) + n36;
        int n40 = n(n35, 10);
        int n41 = n(n36 + t(n39, n37, n40) + this.i[9] + 1352829926, 13) + n38;
        int n42 = n(n37, 10);
        int n43 = n(n38 + t(n41, n39, n42) + this.i[2] + 1352829926, 15) + n40;
        int n44 = n(n39, 10);
        int n45 = n(n40 + t(n43, n41, n44) + this.i[11] + 1352829926, 15) + n42;
        int n46 = n(n41, 10);
        int n47 = n(n42 + t(n45, n43, n46) + this.i[4] + 1352829926, 5) + n44;
        int n48 = n(n43, 10);
        int n49 = n(n44 + t(n47, n45, n48) + this.i[13] + 1352829926, 7) + n46;
        int n50 = n(n45, 10);
        int n51 = n(n46 + t(n49, n47, n50) + this.i[6] + 1352829926, 7) + n48;
        int n52 = n(n47, 10);
        int n53 = n(n48 + t(n51, n49, n52) + this.i[15] + 1352829926, 8) + n50;
        int n54 = n(n49, 10);
        int n55 = n(n50 + t(n53, n51, n54) + this.i[8] + 1352829926, 11) + n52;
        int n56 = n(n51, 10);
        int n57 = n(n52 + t(n55, n53, n56) + this.i[1] + 1352829926, 14) + n54;
        int n58 = n(n53, 10);
        int n59 = n(n54 + t(n57, n55, n58) + this.i[10] + 1352829926, 14) + n56;
        int n60 = n(n55, 10);
        int n61 = n(n56 + t(n59, n57, n60) + this.i[3] + 1352829926, 12) + n58;
        int n62 = n(n57, 10);
        int n63 = n(n58 + t(n61, n59, n62) + this.i[12] + 1352829926, 6) + n60;
        int n64 = n(n59, 10);
        int n65 = n(n28 + q(n31, n29, n32) + this.i[7] + 1518500249, 7) + n30;
        int n66 = n(n29, 10);
        int n67 = n(n30 + q(n65, n31, n66) + this.i[4] + 1518500249, 6) + n32;
        int n68 = n(n31, 10);
        int n69 = n(n32 + q(n67, n65, n68) + this.i[13] + 1518500249, 8) + n66;
        int n70 = n(n65, 10);
        int n71 = n(n66 + q(n69, n67, n70) + this.i[1] + 1518500249, 13) + n68;
        int n72 = n(n67, 10);
        int n73 = n(n68 + q(n71, n69, n72) + this.i[10] + 1518500249, 11) + n70;
        int n74 = n(n69, 10);
        int n75 = n(n70 + q(n73, n71, n74) + this.i[6] + 1518500249, 9) + n72;
        int n76 = n(n71, 10);
        int n77 = n(n72 + q(n75, n73, n76) + this.i[15] + 1518500249, 7) + n74;
        int n78 = n(n73, 10);
        int n79 = n(n74 + q(n77, n75, n78) + this.i[3] + 1518500249, 15) + n76;
        int n80 = n(n75, 10);
        int n81 = n(n76 + q(n79, n77, n80) + this.i[12] + 1518500249, 7) + n78;
        int n82 = n(n77, 10);
        int n83 = n(n78 + q(n81, n79, n82) + this.i[0] + 1518500249, 12) + n80;
        int n84 = n(n79, 10);
        int n85 = n(n80 + q(n83, n81, n84) + this.i[9] + 1518500249, 15) + n82;
        int n86 = n(n81, 10);
        int n87 = n(n82 + q(n85, n83, n86) + this.i[5] + 1518500249, 9) + n84;
        int n88 = n(n83, 10);
        int n89 = n(n84 + q(n87, n85, n88) + this.i[2] + 1518500249, 11) + n86;
        int n90 = n(n85, 10);
        int n91 = n(n86 + q(n89, n87, n90) + this.i[14] + 1518500249, 7) + n88;
        int n92 = n(n87, 10);
        int n93 = n(n88 + q(n91, n89, n92) + this.i[11] + 1518500249, 13) + n90;
        int n94 = n(n89, 10);
        int n95 = n(n90 + q(n93, n91, n94) + this.i[8] + 1518500249, 12) + n92;
        int n96 = n(n91, 10);
        int n97 = n(n60 + s(n63, n61, n64) + this.i[6] + 1548603684, 9) + n62;
        int n98 = n(n61, 10);
        int n99 = n(n62 + s(n97, n63, n98) + this.i[11] + 1548603684, 13) + n64;
        int n100 = n(n63, 10);
        int n101 = n(n64 + s(n99, n97, n100) + this.i[3] + 1548603684, 15) + n98;
        int n102 = n(n97, 10);
        int n103 = n(n98 + s(n101, n99, n102) + this.i[7] + 1548603684, 7) + n100;
        int n104 = n(n99, 10);
        int n105 = n(n100 + s(n103, n101, n104) + this.i[0] + 1548603684, 12) + n102;
        int n106 = n(n101, 10);
        int n107 = n(n102 + s(n105, n103, n106) + this.i[13] + 1548603684, 8) + n104;
        int n108 = n(n103, 10);
        int n109 = n(n104 + s(n107, n105, n108) + this.i[5] + 1548603684, 9) + n106;
        int n110 = n(n105, 10);
        int n111 = n(n106 + s(n109, n107, n110) + this.i[10] + 1548603684, 11) + n108;
        int n112 = n(n107, 10);
        int n113 = n(n108 + s(n111, n109, n112) + this.i[14] + 1548603684, 7) + n110;
        int n114 = n(n109, 10);
        int n115 = n(n110 + s(n113, n111, n114) + this.i[15] + 1548603684, 7) + n112;
        int n116 = n(n111, 10);
        int n117 = n(n112 + s(n115, n113, n116) + this.i[8] + 1548603684, 12) + n114;
        int n118 = n(n113, 10);
        int n119 = n(n114 + s(n117, n115, n118) + this.i[12] + 1548603684, 7) + n116;
        int n120 = n(n115, 10);
        int n121 = n(n116 + s(n119, n117, n120) + this.i[4] + 1548603684, 6) + n118;
        int n122 = n(n117, 10);
        int n123 = n(n118 + s(n121, n119, n122) + this.i[9] + 1548603684, 15) + n120;
        int n124 = n(n119, 10);
        int n125 = n(n120 + s(n123, n121, n124) + this.i[1] + 1548603684, 13) + n122;
        int n126 = n(n121, 10);
        int n127 = n(n122 + s(n125, n123, n126) + this.i[2] + 1548603684, 11) + n124;
        int n128 = n(n123, 10);
        int n129 = n(n92 + r(n95, n93, n96) + this.i[3] + 1859775393, 11) + n94;
        int n130 = n(n93, 10);
        int n131 = n(n94 + r(n129, n95, n130) + this.i[10] + 1859775393, 13) + n96;
        int n132 = n(n95, 10);
        int n133 = n(n96 + r(n131, n129, n132) + this.i[14] + 1859775393, 6) + n130;
        int n134 = n(n129, 10);
        int n135 = n(n130 + r(n133, n131, n134) + this.i[4] + 1859775393, 7) + n132;
        int n136 = n(n131, 10);
        int n137 = n(n132 + r(n135, n133, n136) + this.i[9] + 1859775393, 14) + n134;
        int n138 = n(n133, 10);
        int n139 = n(n134 + r(n137, n135, n138) + this.i[15] + 1859775393, 9) + n136;
        int n140 = n(n135, 10);
        int n141 = n(n136 + r(n139, n137, n140) + this.i[8] + 1859775393, 13) + n138;
        int n142 = n(n137, 10);
        int n143 = n(n138 + r(n141, n139, n142) + this.i[1] + 1859775393, 15) + n140;
        int n144 = n(n139, 10);
        int n145 = n(n140 + r(n143, n141, n144) + this.i[2] + 1859775393, 14) + n142;
        int n146 = n(n141, 10);
        int n147 = n(n142 + r(n145, n143, n146) + this.i[7] + 1859775393, 8) + n144;
        int n148 = n(n143, 10);
        int n149 = n(n144 + r(n147, n145, n148) + this.i[0] + 1859775393, 13) + n146;
        int n150 = n(n145, 10);
        int n151 = n(n146 + r(n149, n147, n150) + this.i[6] + 1859775393, 6) + n148;
        int n152 = n(n147, 10);
        int n153 = n(n148 + r(n151, n149, n152) + this.i[13] + 1859775393, 5) + n150;
        int n154 = n(n149, 10);
        int n155 = n(n150 + r(n153, n151, n154) + this.i[11] + 1859775393, 12) + n152;
        int n156 = n(n151, 10);
        int n157 = n(n152 + r(n155, n153, n156) + this.i[5] + 1859775393, 7) + n154;
        int n158 = n(n153, 10);
        int n159 = n(n154 + r(n157, n155, n158) + this.i[12] + 1859775393, 5) + n156;
        int n160 = n(n155, 10);
        int n161 = n(n124 + r(n127, n125, n128) + this.i[15] + 1836072691, 9) + n126;
        int n162 = n(n125, 10);
        int n163 = n(n126 + r(n161, n127, n162) + this.i[5] + 1836072691, 7) + n128;
        int n164 = n(n127, 10);
        int n165 = n(n128 + r(n163, n161, n164) + this.i[1] + 1836072691, 15) + n162;
        int n166 = n(n161, 10);
        int n167 = n(n162 + r(n165, n163, n166) + this.i[3] + 1836072691, 11) + n164;
        int n168 = n(n163, 10);
        int n169 = n(n164 + r(n167, n165, n168) + this.i[7] + 1836072691, 8) + n166;
        int n170 = n(n165, 10);
        int n171 = n(n166 + r(n169, n167, n170) + this.i[14] + 1836072691, 6) + n168;
        int n172 = n(n167, 10);
        int n173 = n(n168 + r(n171, n169, n172) + this.i[6] + 1836072691, 6) + n170;
        int n174 = n(n169, 10);
        int n175 = n(n170 + r(n173, n171, n174) + this.i[9] + 1836072691, 14) + n172;
        int n176 = n(n171, 10);
        int n177 = n(n172 + r(n175, n173, n176) + this.i[11] + 1836072691, 12) + n174;
        int n178 = n(n173, 10);
        int n179 = n(n174 + r(n177, n175, n178) + this.i[8] + 1836072691, 13) + n176;
        int n180 = n(n175, 10);
        int n181 = n(n176 + r(n179, n177, n180) + this.i[12] + 1836072691, 5) + n178;
        int n182 = n(n177, 10);
        int n183 = n(n178 + r(n181, n179, n182) + this.i[2] + 1836072691, 14) + n180;
        int n184 = n(n179, 10);
        int n185 = n(n180 + r(n183, n181, n184) + this.i[10] + 1836072691, 13) + n182;
        int n186 = n(n181, 10);
        int n187 = n(n182 + r(n185, n183, n186) + this.i[0] + 1836072691, 13) + n184;
        int n188 = n(n183, 10);
        int n189 = n(n184 + r(n187, n185, n188) + this.i[4] + 1836072691, 7) + n186;
        int n190 = n(n185, 10);
        int n191 = n(n186 + r(n189, n187, n190) + this.i[13] + 1836072691, 5) + n188;
        int n192 = n(n187, 10);
        int n193 = n(((n156 + s(n159, n157, n160)) + this.i[1]) - 1894007588, 11) + n158;
        int n194 = n(n157, 10);
        int n195 = n(((n158 + s(n193, n159, n194)) + this.i[9]) - 1894007588, 12) + n160;
        int n196 = n(n159, 10);
        int n197 = n(((n160 + s(n195, n193, n196)) + this.i[11]) - 1894007588, 14) + n194;
        int n198 = n(n193, 10);
        int n199 = n(((n194 + s(n197, n195, n198)) + this.i[10]) - 1894007588, 15) + n196;
        int n200 = n(n195, 10);
        int n201 = n(((n196 + s(n199, n197, n200)) + this.i[0]) - 1894007588, 14) + n198;
        int n202 = n(n197, 10);
        int n203 = n(((n198 + s(n201, n199, n202)) + this.i[8]) - 1894007588, 15) + n200;
        int n204 = n(n199, 10);
        int n205 = n(((n200 + s(n203, n201, n204)) + this.i[12]) - 1894007588, 9) + n202;
        int n206 = n(n201, 10);
        int n207 = n(((n202 + s(n205, n203, n206)) + this.i[4]) - 1894007588, 8) + n204;
        int n208 = n(n203, 10);
        int n209 = n(((n204 + s(n207, n205, n208)) + this.i[13]) - 1894007588, 9) + n206;
        int n210 = n(n205, 10);
        int n211 = n(((n206 + s(n209, n207, n210)) + this.i[3]) - 1894007588, 14) + n208;
        int n212 = n(n207, 10);
        int n213 = n(((n208 + s(n211, n209, n212)) + this.i[7]) - 1894007588, 5) + n210;
        int n214 = n(n209, 10);
        int n215 = n(((n210 + s(n213, n211, n214)) + this.i[15]) - 1894007588, 6) + n212;
        int n216 = n(n211, 10);
        int n217 = n(((n212 + s(n215, n213, n216)) + this.i[14]) - 1894007588, 8) + n214;
        int n218 = n(n213, 10);
        int n219 = n(((n214 + s(n217, n215, n218)) + this.i[5]) - 1894007588, 6) + n216;
        int n220 = n(n215, 10);
        int n221 = n(((n216 + s(n219, n217, n220)) + this.i[6]) - 1894007588, 5) + n218;
        int n222 = n(n217, 10);
        int n223 = n(((n218 + s(n221, n219, n222)) + this.i[2]) - 1894007588, 12) + n220;
        int n224 = n(n219, 10);
        int n225 = n(n188 + q(n191, n189, n192) + this.i[8] + 2053994217, 15) + n190;
        int n226 = n(n189, 10);
        int n227 = n(n190 + q(n225, n191, n226) + this.i[6] + 2053994217, 5) + n192;
        int n228 = n(n191, 10);
        int n229 = n(n192 + q(n227, n225, n228) + this.i[4] + 2053994217, 8) + n226;
        int n230 = n(n225, 10);
        int n231 = n(n226 + q(n229, n227, n230) + this.i[1] + 2053994217, 11) + n228;
        int n232 = n(n227, 10);
        int n233 = n(n228 + q(n231, n229, n232) + this.i[3] + 2053994217, 14) + n230;
        int n234 = n(n229, 10);
        int n235 = n(n230 + q(n233, n231, n234) + this.i[11] + 2053994217, 14) + n232;
        int n236 = n(n231, 10);
        int n237 = n(n232 + q(n235, n233, n236) + this.i[15] + 2053994217, 6) + n234;
        int n238 = n(n233, 10);
        int n239 = n(n234 + q(n237, n235, n238) + this.i[0] + 2053994217, 14) + n236;
        int n240 = n(n235, 10);
        int n241 = n(n236 + q(n239, n237, n240) + this.i[5] + 2053994217, 6) + n238;
        int n242 = n(n237, 10);
        int n243 = n(n238 + q(n241, n239, n242) + this.i[12] + 2053994217, 9) + n240;
        int n244 = n(n239, 10);
        int n245 = n(n240 + q(n243, n241, n244) + this.i[2] + 2053994217, 12) + n242;
        int n246 = n(n241, 10);
        int n247 = n(n242 + q(n245, n243, n246) + this.i[13] + 2053994217, 9) + n244;
        int n248 = n(n243, 10);
        int n249 = n(n244 + q(n247, n245, n248) + this.i[9] + 2053994217, 12) + n246;
        int n250 = n(n245, 10);
        int n251 = n(n246 + q(n249, n247, n250) + this.i[7] + 2053994217, 5) + n248;
        int n252 = n(n247, 10);
        int n253 = n(n248 + q(n251, n249, n252) + this.i[10] + 2053994217, 15) + n250;
        int n254 = n(n249, 10);
        int n255 = n(n250 + q(n253, n251, n254) + this.i[14] + 2053994217, 8) + n252;
        int n256 = n(n251, 10);
        int n257 = n(((n220 + t(n223, n221, n224)) + this.i[4]) - 1454113458, 9) + n222;
        int n258 = n(n221, 10);
        int n259 = n(((n222 + t(n257, n223, n258)) + this.i[0]) - 1454113458, 15) + n224;
        int n260 = n(n223, 10);
        int n261 = n(((n224 + t(n259, n257, n260)) + this.i[5]) - 1454113458, 5) + n258;
        int n262 = n(n257, 10);
        int n263 = n(((n258 + t(n261, n259, n262)) + this.i[9]) - 1454113458, 11) + n260;
        int n264 = n(n259, 10);
        int n265 = n(((n260 + t(n263, n261, n264)) + this.i[7]) - 1454113458, 6) + n262;
        int n266 = n(n261, 10);
        int n267 = n(((n262 + t(n265, n263, n266)) + this.i[12]) - 1454113458, 8) + n264;
        int n268 = n(n263, 10);
        int n269 = n(((n264 + t(n267, n265, n268)) + this.i[2]) - 1454113458, 13) + n266;
        int n270 = n(n265, 10);
        int n271 = n(((n266 + t(n269, n267, n270)) + this.i[10]) - 1454113458, 12) + n268;
        int n272 = n(n267, 10);
        int n273 = n(((n268 + t(n271, n269, n272)) + this.i[14]) - 1454113458, 5) + n270;
        int n274 = n(n269, 10);
        int n275 = n(((n270 + t(n273, n271, n274)) + this.i[1]) - 1454113458, 12) + n272;
        int n276 = n(n271, 10);
        int n277 = n(((n272 + t(n275, n273, n276)) + this.i[3]) - 1454113458, 13) + n274;
        int n278 = n(n273, 10);
        int n279 = n(((n274 + t(n277, n275, n278)) + this.i[8]) - 1454113458, 14) + n276;
        int n280 = n(n275, 10);
        int n281 = n(((n276 + t(n279, n277, n280)) + this.i[11]) - 1454113458, 11) + n278;
        int n282 = n(n277, 10);
        int n283 = n(((n278 + t(n281, n279, n282)) + this.i[6]) - 1454113458, 8) + n280;
        int n284 = n(n279, 10);
        int n285 = n(((n280 + t(n283, n281, n284)) + this.i[15]) - 1454113458, 5) + n282;
        int n286 = n(n281, 10);
        int n287 = n(n283, 10);
        int n288 = n(n252 + p(n255, n253, n256) + this.i[12], 8) + n254;
        int n289 = n(n253, 10);
        int n290 = n(n254 + p(n288, n255, n289) + this.i[15], 5) + n256;
        int n291 = n(n255, 10);
        int n292 = n(n256 + p(n290, n288, n291) + this.i[10], 12) + n289;
        int n293 = n(n288, 10);
        int n294 = n(n289 + p(n292, n290, n293) + this.i[4], 9) + n291;
        int n295 = n(n290, 10);
        int n296 = n(n291 + p(n294, n292, n295) + this.i[1], 12) + n293;
        int n297 = n(n292, 10);
        int n298 = n(n293 + p(n296, n294, n297) + this.i[5], 5) + n295;
        int n299 = n(n294, 10);
        int n300 = n(n295 + p(n298, n296, n299) + this.i[8], 14) + n297;
        int n301 = n(n296, 10);
        int n302 = n(n297 + p(n300, n298, n301) + this.i[7], 6) + n299;
        int n303 = n(n298, 10);
        int n304 = n(n299 + p(n302, n300, n303) + this.i[6], 8) + n301;
        int n305 = n(n300, 10);
        int n306 = n(n301 + p(n304, n302, n305) + this.i[2], 13) + n303;
        int n307 = n(n302, 10);
        int n308 = n(n303 + p(n306, n304, n307) + this.i[13], 6) + n305;
        int n309 = n(n304, 10);
        int n310 = n(n305 + p(n308, n306, n309) + this.i[14], 5) + n307;
        int n311 = n(n306, 10);
        int n312 = n(n307 + p(n310, n308, n311) + this.i[0], 15) + n309;
        int n313 = n(n308, 10);
        int n314 = n(n309 + p(n312, n310, n313) + this.i[3], 13) + n311;
        int n315 = n(n310, 10);
        int n316 = n(n311 + p(n314, n312, n315) + this.i[9], 11) + n313;
        int n317 = n(n312, 10);
        int n318 = n(n313 + p(n316, n314, n317) + this.i[11], 11) + n315;
        this.e = this.f + n287 + n317;
        this.f = this.g + n286 + n315;
        this.g = this.h + n284 + n318;
        this.h = this.d + n(((n282 + t(n285, n283, n286)) + this.i[13]) - 1454113458, 6) + n284 + n316;
        this.d = n(n314, 10) + n285 + this.e;
        this.j = 0;
        int i6 = 0;
        while (true) {
            int[] iArr = this.i;
            if (i6 == iArr.length) {
                return;
            }
            iArr[i6] = 0;
            i6++;
        }
    }

    @Override // defpackage.re1
    public void l(long j) {
        if (this.j > 14) {
            k();
        }
        int[] iArr = this.i;
        iArr[14] = (int) ((-1) & j);
        iArr[15] = (int) (j >>> 32);
    }

    @Override // defpackage.re1
    public void m(byte[] bArr, int i) {
        int[] iArr = this.i;
        int i2 = this.j;
        int i3 = i2 + 1;
        this.j = i3;
        iArr[i2] = ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        if (i3 == 16) {
            k();
        }
    }

    public final int n(int i, int i2) {
        return (i >>> (32 - i2)) | (i << i2);
    }

    public final void o(e33 e33Var) {
        super.i(e33Var);
        this.d = e33Var.d;
        this.e = e33Var.e;
        this.f = e33Var.f;
        this.g = e33Var.g;
        this.h = e33Var.h;
        int[] iArr = e33Var.i;
        System.arraycopy(iArr, 0, this.i, 0, iArr.length);
        this.j = e33Var.j;
    }

    public final int p(int i, int i2, int i3) {
        return (i ^ i2) ^ i3;
    }

    public final int q(int i, int i2, int i3) {
        return ((~i) & i3) | (i2 & i);
    }

    public final int r(int i, int i2, int i3) {
        return (i | (~i2)) ^ i3;
    }

    @Override // defpackage.re1, defpackage.qo0
    public void reset() {
        super.reset();
        this.d = 1732584193;
        this.e = -271733879;
        this.f = -1732584194;
        this.g = 271733878;
        this.h = -1009589776;
        this.j = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.i;
            if (i == iArr.length) {
                return;
            }
            iArr[i] = 0;
            i++;
        }
    }

    public final int s(int i, int i2, int i3) {
        return (i & i3) | (i2 & (~i3));
    }

    public final int t(int i, int i2, int i3) {
        return i ^ (i2 | (~i3));
    }

    public final void u(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) i;
        bArr[i2 + 1] = (byte) (i >>> 8);
        bArr[i2 + 2] = (byte) (i >>> 16);
        bArr[i2 + 3] = (byte) (i >>> 24);
    }
}
