package defpackage;

import android.util.Pair;
import android.util.SparseArray;
import androidx.media3.common.DrmInitData;
import androidx.media3.common.ParserException;
import androidx.media3.common.f;
import com.github.mikephil.charting.utils.Utils;
import defpackage.f84;
import defpackage.wi3;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* compiled from: MatroskaExtractor.java */
/* renamed from: d52  reason: default package */
/* loaded from: classes.dex */
public class d52 implements p11 {
    public static final byte[] c0;
    public static final byte[] d0;
    public static final byte[] e0;
    public static final byte[] f0;
    public static final UUID g0;
    public static final Map<String, Integer> h0;
    public long A;
    public long B;
    public e22 C;
    public e22 D;
    public boolean E;
    public boolean F;
    public int G;
    public long H;
    public long I;
    public int J;
    public int K;
    public int[] L;
    public int M;
    public int N;
    public int O;
    public int P;
    public boolean Q;
    public long R;
    public int S;
    public int T;
    public int U;
    public boolean V;
    public boolean W;
    public boolean X;
    public int Y;
    public byte Z;
    public final au0 a;
    public boolean a0;
    public final bh4 b;
    public r11 b0;
    public final SparseArray<c> c;
    public final boolean d;
    public final op2 e;
    public final op2 f;
    public final op2 g;
    public final op2 h;
    public final op2 i;
    public final op2 j;
    public final op2 k;
    public final op2 l;
    public final op2 m;
    public final op2 n;
    public ByteBuffer o;
    public long p;
    public long q;
    public long r;
    public long s;
    public long t;
    public c u;
    public boolean v;
    public int w;
    public long x;
    public boolean y;
    public long z;

    /* compiled from: MatroskaExtractor.java */
    /* renamed from: d52$b */
    /* loaded from: classes.dex */
    public final class b implements zt0 {
        public b() {
        }

        @Override // defpackage.zt0
        public void a(int i) throws ParserException {
            d52.this.p(i);
        }

        @Override // defpackage.zt0
        public int b(int i) {
            return d52.this.v(i);
        }

        @Override // defpackage.zt0
        public boolean c(int i) {
            return d52.this.A(i);
        }

        @Override // defpackage.zt0
        public void d(int i, String str) throws ParserException {
            d52.this.I(i, str);
        }

        @Override // defpackage.zt0
        public void e(int i, double d) throws ParserException {
            d52.this.s(i, d);
        }

        @Override // defpackage.zt0
        public void f(int i, int i2, q11 q11Var) throws IOException {
            d52.this.m(i, i2, q11Var);
        }

        @Override // defpackage.zt0
        public void g(int i, long j, long j2) throws ParserException {
            d52.this.H(i, j, j2);
        }

        @Override // defpackage.zt0
        public void h(int i, long j) throws ParserException {
            d52.this.y(i, j);
        }
    }

    /* compiled from: MatroskaExtractor.java */
    /* renamed from: d52$c */
    /* loaded from: classes.dex */
    public static final class c {
        public byte[] N;
        public ac4 T;
        public boolean U;
        public f84 X;
        public int Y;
        public String a;
        public String b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public boolean h;
        public byte[] i;
        public f84.a j;
        public byte[] k;
        public DrmInitData l;
        public int m = -1;
        public int n = -1;
        public int o = -1;
        public int p = -1;
        public int q = 0;
        public int r = -1;
        public float s = Utils.FLOAT_EPSILON;
        public float t = Utils.FLOAT_EPSILON;
        public float u = Utils.FLOAT_EPSILON;
        public byte[] v = null;
        public int w = -1;
        public boolean x = false;
        public int y = -1;
        public int z = -1;
        public int A = -1;
        public int B = 1000;
        public int C = 200;
        public float D = -1.0f;
        public float E = -1.0f;
        public float F = -1.0f;
        public float G = -1.0f;
        public float H = -1.0f;
        public float I = -1.0f;
        public float J = -1.0f;
        public float K = -1.0f;
        public float L = -1.0f;
        public float M = -1.0f;
        public int O = 1;
        public int P = -1;
        public int Q = 8000;
        public long R = 0;
        public long S = 0;
        public boolean V = true;
        public String W = "eng";

        public static Pair<String, List<byte[]>> k(op2 op2Var) throws ParserException {
            try {
                op2Var.Q(16);
                long t = op2Var.t();
                if (t == 1482049860) {
                    return new Pair<>("video/divx", null);
                }
                if (t == 859189832) {
                    return new Pair<>("video/3gpp", null);
                }
                if (t == 826496599) {
                    byte[] d = op2Var.d();
                    for (int e = op2Var.e() + 20; e < d.length - 4; e++) {
                        if (d[e] == 0 && d[e + 1] == 0 && d[e + 2] == 1 && d[e + 3] == 15) {
                            return new Pair<>("video/wvc1", Collections.singletonList(Arrays.copyOfRange(d, e, d.length)));
                        }
                    }
                    throw ParserException.createForMalformedContainer("Failed to find FourCC VC1 initialization data", null);
                }
                p12.i("MatroskaExtractor", "Unknown FourCC. Setting mimeType to video/x-unknown");
                return new Pair<>("video/x-unknown", null);
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw ParserException.createForMalformedContainer("Error parsing FourCC private data", null);
            }
        }

        public static boolean l(op2 op2Var) throws ParserException {
            try {
                int v = op2Var.v();
                if (v == 1) {
                    return true;
                }
                if (v == 65534) {
                    op2Var.P(24);
                    if (op2Var.w() == d52.g0.getMostSignificantBits()) {
                        if (op2Var.w() == d52.g0.getLeastSignificantBits()) {
                            return true;
                        }
                    }
                    return false;
                }
                return false;
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw ParserException.createForMalformedContainer("Error parsing MS/ACM codec private", null);
            }
        }

        public static List<byte[]> m(byte[] bArr) throws ParserException {
            try {
                if (bArr[0] == 2) {
                    int i = 0;
                    int i2 = 1;
                    while ((bArr[i2] & 255) == 255) {
                        i += 255;
                        i2++;
                    }
                    int i3 = i2 + 1;
                    int i4 = i + (bArr[i2] & 255);
                    int i5 = 0;
                    while ((bArr[i3] & 255) == 255) {
                        i5 += 255;
                        i3++;
                    }
                    int i6 = i3 + 1;
                    int i7 = i5 + (bArr[i3] & 255);
                    if (bArr[i6] == 1) {
                        byte[] bArr2 = new byte[i4];
                        System.arraycopy(bArr, i6, bArr2, 0, i4);
                        int i8 = i6 + i4;
                        if (bArr[i8] == 3) {
                            int i9 = i8 + i7;
                            if (bArr[i9] == 5) {
                                byte[] bArr3 = new byte[bArr.length - i9];
                                System.arraycopy(bArr, i9, bArr3, 0, bArr.length - i9);
                                ArrayList arrayList = new ArrayList(2);
                                arrayList.add(bArr2);
                                arrayList.add(bArr3);
                                return arrayList;
                            }
                            throw ParserException.createForMalformedContainer("Error parsing vorbis codec private", null);
                        }
                        throw ParserException.createForMalformedContainer("Error parsing vorbis codec private", null);
                    }
                    throw ParserException.createForMalformedContainer("Error parsing vorbis codec private", null);
                }
                throw ParserException.createForMalformedContainer("Error parsing vorbis codec private", null);
            } catch (ArrayIndexOutOfBoundsException unused) {
                throw ParserException.createForMalformedContainer("Error parsing vorbis codec private", null);
            }
        }

        public final void f() {
            ii.e(this.X);
        }

        public final byte[] g(String str) throws ParserException {
            byte[] bArr = this.k;
            if (bArr != null) {
                return bArr;
            }
            throw ParserException.createForMalformedContainer("Missing CodecPrivate for codec " + str, null);
        }

        public final byte[] h() {
            if (this.D == -1.0f || this.E == -1.0f || this.F == -1.0f || this.G == -1.0f || this.H == -1.0f || this.I == -1.0f || this.J == -1.0f || this.K == -1.0f || this.L == -1.0f || this.M == -1.0f) {
                return null;
            }
            byte[] bArr = new byte[25];
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            order.put((byte) 0);
            order.putShort((short) ((this.D * 50000.0f) + 0.5f));
            order.putShort((short) ((this.E * 50000.0f) + 0.5f));
            order.putShort((short) ((this.F * 50000.0f) + 0.5f));
            order.putShort((short) ((this.G * 50000.0f) + 0.5f));
            order.putShort((short) ((this.H * 50000.0f) + 0.5f));
            order.putShort((short) ((this.I * 50000.0f) + 0.5f));
            order.putShort((short) ((this.J * 50000.0f) + 0.5f));
            order.putShort((short) ((this.K * 50000.0f) + 0.5f));
            order.putShort((short) (this.L + 0.5f));
            order.putShort((short) (this.M + 0.5f));
            order.putShort((short) this.B);
            order.putShort((short) this.C);
            return bArr;
        }

        /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARN: Removed duplicated region for block: B:202:0x0417  */
        /* JADX WARN: Removed duplicated region for block: B:207:0x0430  */
        /* JADX WARN: Removed duplicated region for block: B:208:0x0432  */
        /* JADX WARN: Removed duplicated region for block: B:211:0x043f  */
        /* JADX WARN: Removed duplicated region for block: B:212:0x0451  */
        /* JADX WARN: Removed duplicated region for block: B:277:0x055a  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void i(defpackage.r11 r20, int r21) throws androidx.media3.common.ParserException {
            /*
                Method dump skipped, instructions count: 1648
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.d52.c.i(r11, int):void");
        }

        public void j() {
            ac4 ac4Var = this.T;
            if (ac4Var != null) {
                ac4Var.a(this.X, this.j);
            }
        }

        public void n() {
            ac4 ac4Var = this.T;
            if (ac4Var != null) {
                ac4Var.b();
            }
        }

        public final boolean o(boolean z) {
            return "A_OPUS".equals(this.b) ? z : this.f > 0;
        }
    }

    static {
        c52 c52Var = c52.b;
        c0 = new byte[]{49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10};
        d0 = androidx.media3.common.util.b.j0("Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
        e0 = new byte[]{68, 105, 97, 108, 111, 103, 117, 101, 58, 32, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44};
        f0 = new byte[]{87, 69, 66, 86, 84, 84, 10, 10, 48, 48, 58, 48, 48, 58, 48, 48, 46, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 46, 48, 48, 48, 10};
        g0 = new UUID(72057594037932032L, -9223371306706625679L);
        HashMap hashMap = new HashMap();
        hashMap.put("htc_video_rotA-000", 0);
        hashMap.put("htc_video_rotA-090", 90);
        hashMap.put("htc_video_rotA-180", 180);
        hashMap.put("htc_video_rotA-270", 270);
        h0 = Collections.unmodifiableMap(hashMap);
    }

    public d52() {
        this(0);
    }

    public static /* synthetic */ p11[] B() {
        return new p11[]{new d52()};
    }

    public static void G(String str, long j, byte[] bArr) {
        byte[] t;
        int i;
        str.hashCode();
        char c2 = 65535;
        switch (str.hashCode()) {
            case 738597099:
                if (str.equals("S_TEXT/ASS")) {
                    c2 = 0;
                    break;
                }
                break;
            case 1045209816:
                if (str.equals("S_TEXT/WEBVTT")) {
                    c2 = 1;
                    break;
                }
                break;
            case 1422270023:
                if (str.equals("S_TEXT/UTF8")) {
                    c2 = 2;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                t = t(j, "%01d:%02d:%02d:%02d", 10000L);
                i = 21;
                break;
            case 1:
                t = t(j, "%02d:%02d:%02d.%03d", 1000L);
                i = 25;
                break;
            case 2:
                t = t(j, "%02d:%02d:%02d,%03d", 1000L);
                i = 19;
                break;
            default:
                throw new IllegalArgumentException();
        }
        System.arraycopy(t, 0, bArr, i, t.length);
    }

    public static int[] q(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        return iArr.length >= i ? iArr : new int[Math.max(iArr.length * 2, i)];
    }

    public static byte[] t(long j, String str, long j2) {
        ii.a(j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        int i = (int) (j / 3600000000L);
        long j3 = j - ((i * 3600) * 1000000);
        int i2 = (int) (j3 / 60000000);
        long j4 = j3 - ((i2 * 60) * 1000000);
        int i3 = (int) (j4 / 1000000);
        return androidx.media3.common.util.b.j0(String.format(Locale.US, str, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf((int) ((j4 - (i3 * 1000000)) / j2))));
    }

    public static boolean z(String str) {
        str.hashCode();
        char c2 = 65535;
        switch (str.hashCode()) {
            case -2095576542:
                if (str.equals("V_MPEG4/ISO/AP")) {
                    c2 = 0;
                    break;
                }
                break;
            case -2095575984:
                if (str.equals("V_MPEG4/ISO/SP")) {
                    c2 = 1;
                    break;
                }
                break;
            case -1985379776:
                if (str.equals("A_MS/ACM")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1784763192:
                if (str.equals("A_TRUEHD")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1730367663:
                if (str.equals("A_VORBIS")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1482641358:
                if (str.equals("A_MPEG/L2")) {
                    c2 = 5;
                    break;
                }
                break;
            case -1482641357:
                if (str.equals("A_MPEG/L3")) {
                    c2 = 6;
                    break;
                }
                break;
            case -1373388978:
                if (str.equals("V_MS/VFW/FOURCC")) {
                    c2 = 7;
                    break;
                }
                break;
            case -933872740:
                if (str.equals("S_DVBSUB")) {
                    c2 = '\b';
                    break;
                }
                break;
            case -538363189:
                if (str.equals("V_MPEG4/ISO/ASP")) {
                    c2 = '\t';
                    break;
                }
                break;
            case -538363109:
                if (str.equals("V_MPEG4/ISO/AVC")) {
                    c2 = '\n';
                    break;
                }
                break;
            case -425012669:
                if (str.equals("S_VOBSUB")) {
                    c2 = 11;
                    break;
                }
                break;
            case -356037306:
                if (str.equals("A_DTS/LOSSLESS")) {
                    c2 = '\f';
                    break;
                }
                break;
            case 62923557:
                if (str.equals("A_AAC")) {
                    c2 = '\r';
                    break;
                }
                break;
            case 62923603:
                if (str.equals("A_AC3")) {
                    c2 = 14;
                    break;
                }
                break;
            case 62927045:
                if (str.equals("A_DTS")) {
                    c2 = 15;
                    break;
                }
                break;
            case 82318131:
                if (str.equals("V_AV1")) {
                    c2 = 16;
                    break;
                }
                break;
            case 82338133:
                if (str.equals("V_VP8")) {
                    c2 = 17;
                    break;
                }
                break;
            case 82338134:
                if (str.equals("V_VP9")) {
                    c2 = 18;
                    break;
                }
                break;
            case 99146302:
                if (str.equals("S_HDMV/PGS")) {
                    c2 = 19;
                    break;
                }
                break;
            case 444813526:
                if (str.equals("V_THEORA")) {
                    c2 = 20;
                    break;
                }
                break;
            case 542569478:
                if (str.equals("A_DTS/EXPRESS")) {
                    c2 = 21;
                    break;
                }
                break;
            case 635596514:
                if (str.equals("A_PCM/FLOAT/IEEE")) {
                    c2 = 22;
                    break;
                }
                break;
            case 725948237:
                if (str.equals("A_PCM/INT/BIG")) {
                    c2 = 23;
                    break;
                }
                break;
            case 725957860:
                if (str.equals("A_PCM/INT/LIT")) {
                    c2 = 24;
                    break;
                }
                break;
            case 738597099:
                if (str.equals("S_TEXT/ASS")) {
                    c2 = 25;
                    break;
                }
                break;
            case 855502857:
                if (str.equals("V_MPEGH/ISO/HEVC")) {
                    c2 = 26;
                    break;
                }
                break;
            case 1045209816:
                if (str.equals("S_TEXT/WEBVTT")) {
                    c2 = 27;
                    break;
                }
                break;
            case 1422270023:
                if (str.equals("S_TEXT/UTF8")) {
                    c2 = 28;
                    break;
                }
                break;
            case 1809237540:
                if (str.equals("V_MPEG2")) {
                    c2 = 29;
                    break;
                }
                break;
            case 1950749482:
                if (str.equals("A_EAC3")) {
                    c2 = 30;
                    break;
                }
                break;
            case 1950789798:
                if (str.equals("A_FLAC")) {
                    c2 = 31;
                    break;
                }
                break;
            case 1951062397:
                if (str.equals("A_OPUS")) {
                    c2 = ' ';
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case '\b':
            case '\t':
            case '\n':
            case 11:
            case '\f':
            case '\r':
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case ' ':
                return true;
            default:
                return false;
        }
    }

    public boolean A(int i) {
        return i == 357149030 || i == 524531317 || i == 475249515 || i == 374648427;
    }

    public final boolean C(ot2 ot2Var, long j) {
        if (this.y) {
            this.A = j;
            ot2Var.a = this.z;
            this.y = false;
            return true;
        }
        if (this.v) {
            long j2 = this.A;
            if (j2 != -1) {
                ot2Var.a = j2;
                this.A = -1L;
                return true;
            }
        }
        return false;
    }

    public final void D(q11 q11Var, int i) throws IOException {
        if (this.g.f() >= i) {
            return;
        }
        if (this.g.b() < i) {
            op2 op2Var = this.g;
            op2Var.c(Math.max(op2Var.b() * 2, i));
        }
        q11Var.readFully(this.g.d(), this.g.f(), i - this.g.f());
        this.g.O(i);
    }

    public final void E() {
        this.S = 0;
        this.T = 0;
        this.U = 0;
        this.V = false;
        this.W = false;
        this.X = false;
        this.Y = 0;
        this.Z = (byte) 0;
        this.a0 = false;
        this.j.L(0);
    }

    public final long F(long j) throws ParserException {
        long j2 = this.r;
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return androidx.media3.common.util.b.J0(j, j2, 1000L);
        }
        throw ParserException.createForMalformedContainer("Can't scale timecode prior to timecodeScale being set.", null);
    }

    public void H(int i, long j, long j2) throws ParserException {
        l();
        if (i == 160) {
            this.Q = false;
            this.R = 0L;
        } else if (i == 174) {
            this.u = new c();
        } else if (i == 187) {
            this.E = false;
        } else if (i == 19899) {
            this.w = -1;
            this.x = -1L;
        } else if (i == 20533) {
            u(i).h = true;
        } else if (i == 21968) {
            u(i).x = true;
        } else if (i == 408125543) {
            long j3 = this.q;
            if (j3 != -1 && j3 != j) {
                throw ParserException.createForMalformedContainer("Multiple Segment elements not supported", null);
            }
            this.q = j;
            this.p = j2;
        } else if (i != 475249515) {
            if (i == 524531317 && !this.v) {
                if (this.d && this.z != -1) {
                    this.y = true;
                    return;
                }
                this.b0.p(new wi3.b(this.t));
                this.v = true;
            }
        } else {
            this.C = new e22();
            this.D = new e22();
        }
    }

    public void I(int i, String str) throws ParserException {
        if (i == 134) {
            u(i).b = str;
        } else if (i != 17026) {
            if (i == 21358) {
                u(i).a = str;
            } else if (i != 2274716) {
            } else {
                u(i).W = str;
            }
        } else if ("webm".equals(str) || "matroska".equals(str)) {
        } else {
            throw ParserException.createForMalformedContainer("DocType " + str + " not supported", null);
        }
    }

    public final int J(q11 q11Var, c cVar, int i, boolean z) throws IOException {
        int i2;
        if ("S_TEXT/UTF8".equals(cVar.b)) {
            K(q11Var, c0, i);
            return r();
        } else if ("S_TEXT/ASS".equals(cVar.b)) {
            K(q11Var, e0, i);
            return r();
        } else if ("S_TEXT/WEBVTT".equals(cVar.b)) {
            K(q11Var, f0, i);
            return r();
        } else {
            f84 f84Var = cVar.X;
            if (!this.V) {
                if (cVar.h) {
                    this.O &= -1073741825;
                    if (!this.W) {
                        q11Var.readFully(this.g.d(), 0, 1);
                        this.S++;
                        if ((this.g.d()[0] & 128) != 128) {
                            this.Z = this.g.d()[0];
                            this.W = true;
                        } else {
                            throw ParserException.createForMalformedContainer("Extension bit is set in signal byte", null);
                        }
                    }
                    byte b2 = this.Z;
                    if ((b2 & 1) == 1) {
                        boolean z2 = (b2 & 2) == 2;
                        this.O |= 1073741824;
                        if (!this.a0) {
                            q11Var.readFully(this.l.d(), 0, 8);
                            this.S += 8;
                            this.a0 = true;
                            this.g.d()[0] = (byte) ((z2 ? 128 : 0) | 8);
                            this.g.P(0);
                            f84Var.c(this.g, 1, 1);
                            this.T++;
                            this.l.P(0);
                            f84Var.c(this.l, 8, 1);
                            this.T += 8;
                        }
                        if (z2) {
                            if (!this.X) {
                                q11Var.readFully(this.g.d(), 0, 1);
                                this.S++;
                                this.g.P(0);
                                this.Y = this.g.D();
                                this.X = true;
                            }
                            int i3 = this.Y * 4;
                            this.g.L(i3);
                            q11Var.readFully(this.g.d(), 0, i3);
                            this.S += i3;
                            short s = (short) ((this.Y / 2) + 1);
                            int i4 = (s * 6) + 2;
                            ByteBuffer byteBuffer = this.o;
                            if (byteBuffer == null || byteBuffer.capacity() < i4) {
                                this.o = ByteBuffer.allocate(i4);
                            }
                            this.o.position(0);
                            this.o.putShort(s);
                            int i5 = 0;
                            int i6 = 0;
                            while (true) {
                                i2 = this.Y;
                                if (i5 >= i2) {
                                    break;
                                }
                                int H = this.g.H();
                                if (i5 % 2 == 0) {
                                    this.o.putShort((short) (H - i6));
                                } else {
                                    this.o.putInt(H - i6);
                                }
                                i5++;
                                i6 = H;
                            }
                            int i7 = (i - this.S) - i6;
                            if (i2 % 2 == 1) {
                                this.o.putInt(i7);
                            } else {
                                this.o.putShort((short) i7);
                                this.o.putInt(0);
                            }
                            this.m.N(this.o.array(), i4);
                            f84Var.c(this.m, i4, 1);
                            this.T += i4;
                        }
                    }
                } else {
                    byte[] bArr = cVar.i;
                    if (bArr != null) {
                        this.j.N(bArr, bArr.length);
                    }
                }
                if (cVar.o(z)) {
                    this.O |= 268435456;
                    this.n.L(0);
                    int f = (this.j.f() + i) - this.S;
                    this.g.L(4);
                    this.g.d()[0] = (byte) ((f >> 24) & 255);
                    this.g.d()[1] = (byte) ((f >> 16) & 255);
                    this.g.d()[2] = (byte) ((f >> 8) & 255);
                    this.g.d()[3] = (byte) (f & 255);
                    f84Var.c(this.g, 4, 2);
                    this.T += 4;
                }
                this.V = true;
            }
            int f2 = i + this.j.f();
            if (!"V_MPEG4/ISO/AVC".equals(cVar.b) && !"V_MPEGH/ISO/HEVC".equals(cVar.b)) {
                if (cVar.T != null) {
                    ii.g(this.j.f() == 0);
                    cVar.T.d(q11Var);
                }
                while (true) {
                    int i8 = this.S;
                    if (i8 >= f2) {
                        break;
                    }
                    int L = L(q11Var, f84Var, f2 - i8);
                    this.S += L;
                    this.T += L;
                }
            } else {
                byte[] d = this.f.d();
                d[0] = 0;
                d[1] = 0;
                d[2] = 0;
                int i9 = cVar.Y;
                int i10 = 4 - i9;
                while (this.S < f2) {
                    int i11 = this.U;
                    if (i11 == 0) {
                        M(q11Var, d, i10, i9);
                        this.S += i9;
                        this.f.P(0);
                        this.U = this.f.H();
                        this.e.P(0);
                        f84Var.a(this.e, 4);
                        this.T += 4;
                    } else {
                        int L2 = L(q11Var, f84Var, i11);
                        this.S += L2;
                        this.T += L2;
                        this.U -= L2;
                    }
                }
            }
            if ("A_VORBIS".equals(cVar.b)) {
                this.h.P(0);
                f84Var.a(this.h, 4);
                this.T += 4;
            }
            return r();
        }
    }

    public final void K(q11 q11Var, byte[] bArr, int i) throws IOException {
        int length = bArr.length + i;
        if (this.k.b() < length) {
            this.k.M(Arrays.copyOf(bArr, length + i));
        } else {
            System.arraycopy(bArr, 0, this.k.d(), 0, bArr.length);
        }
        q11Var.readFully(this.k.d(), bArr.length, i);
        this.k.P(0);
        this.k.O(length);
    }

    public final int L(q11 q11Var, f84 f84Var, int i) throws IOException {
        int a2 = this.j.a();
        if (a2 > 0) {
            int min = Math.min(i, a2);
            f84Var.a(this.j, min);
            return min;
        }
        return f84Var.d(q11Var, i, false);
    }

    public final void M(q11 q11Var, byte[] bArr, int i, int i2) throws IOException {
        int min = Math.min(i2, this.j.a());
        q11Var.readFully(bArr, i + min, i2 - min);
        if (min > 0) {
            this.j.j(bArr, i, min);
        }
    }

    @Override // defpackage.p11
    public final void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.B = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.G = 0;
        this.a.reset();
        this.b.e();
        E();
        for (int i = 0; i < this.c.size(); i++) {
            this.c.valueAt(i).n();
        }
    }

    @Override // defpackage.p11
    public final int f(q11 q11Var, ot2 ot2Var) throws IOException {
        this.F = false;
        boolean z = true;
        while (z && !this.F) {
            z = this.a.b(q11Var);
            if (z && C(ot2Var, q11Var.getPosition())) {
                return 1;
            }
        }
        if (z) {
            return 0;
        }
        for (int i = 0; i < this.c.size(); i++) {
            c valueAt = this.c.valueAt(i);
            valueAt.f();
            valueAt.j();
        }
        return -1;
    }

    @Override // defpackage.p11
    public final boolean g(q11 q11Var) throws IOException {
        return new yq3().b(q11Var);
    }

    public final void i(int i) throws ParserException {
        if (this.C == null || this.D == null) {
            throw ParserException.createForMalformedContainer("Element " + i + " must be in a Cues", null);
        }
    }

    @Override // defpackage.p11
    public final void j(r11 r11Var) {
        this.b0 = r11Var;
    }

    public final void k(int i) throws ParserException {
        if (this.u != null) {
            return;
        }
        throw ParserException.createForMalformedContainer("Element " + i + " must be in a TrackEntry", null);
    }

    public final void l() {
        ii.i(this.b0);
    }

    /* JADX WARN: Code restructure failed: missing block: B:86:0x0241, code lost:
        throw androidx.media3.common.ParserException.createForMalformedContainer("EBML lacing sample size out of range.", null);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void m(int r22, int r23, defpackage.q11 r24) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 766
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.d52.m(int, int, q11):void");
    }

    public final wi3 n(e22 e22Var, e22 e22Var2) {
        int i;
        if (this.q != -1 && this.t != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && e22Var != null && e22Var.c() != 0 && e22Var2 != null && e22Var2.c() == e22Var.c()) {
            int c2 = e22Var.c();
            int[] iArr = new int[c2];
            long[] jArr = new long[c2];
            long[] jArr2 = new long[c2];
            long[] jArr3 = new long[c2];
            int i2 = 0;
            for (int i3 = 0; i3 < c2; i3++) {
                jArr3[i3] = e22Var.b(i3);
                jArr[i3] = this.q + e22Var2.b(i3);
            }
            while (true) {
                i = c2 - 1;
                if (i2 >= i) {
                    break;
                }
                int i4 = i2 + 1;
                iArr[i2] = (int) (jArr[i4] - jArr[i2]);
                jArr2[i2] = jArr3[i4] - jArr3[i2];
                i2 = i4;
            }
            iArr[i] = (int) ((this.q + this.p) - jArr[i]);
            jArr2[i] = this.t - jArr3[i];
            long j = jArr2[i];
            if (j <= 0) {
                p12.i("MatroskaExtractor", "Discarding last cue point with unexpected duration: " + j);
                iArr = Arrays.copyOf(iArr, i);
                jArr = Arrays.copyOf(jArr, i);
                jArr2 = Arrays.copyOf(jArr2, i);
                jArr3 = Arrays.copyOf(jArr3, i);
            }
            return new py(iArr, jArr, jArr2, jArr3);
        }
        return new wi3.b(this.t);
    }

    public final void o(c cVar, long j, int i, int i2, int i3) {
        ac4 ac4Var = cVar.T;
        if (ac4Var != null) {
            ac4Var.c(cVar.X, j, i, i2, i3, cVar.j);
        } else {
            if ("S_TEXT/UTF8".equals(cVar.b) || "S_TEXT/ASS".equals(cVar.b) || "S_TEXT/WEBVTT".equals(cVar.b)) {
                if (this.K > 1) {
                    p12.i("MatroskaExtractor", "Skipping subtitle sample in laced block.");
                } else {
                    long j2 = this.I;
                    if (j2 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                        p12.i("MatroskaExtractor", "Skipping subtitle sample with no duration.");
                    } else {
                        G(cVar.b, j2, this.k.d());
                        int e = this.k.e();
                        while (true) {
                            if (e >= this.k.f()) {
                                break;
                            } else if (this.k.d()[e] == 0) {
                                this.k.O(e);
                                break;
                            } else {
                                e++;
                            }
                        }
                        f84 f84Var = cVar.X;
                        op2 op2Var = this.k;
                        f84Var.a(op2Var, op2Var.f());
                        i2 += this.k.f();
                    }
                }
            }
            if ((268435456 & i) != 0) {
                if (this.K > 1) {
                    this.n.L(0);
                } else {
                    int f = this.n.f();
                    cVar.X.c(this.n, f, 2);
                    i2 += f;
                }
            }
            cVar.X.b(j, i, i2, i3, cVar.j);
        }
        this.F = true;
    }

    public void p(int i) throws ParserException {
        l();
        if (i == 160) {
            if (this.G != 2) {
                return;
            }
            c cVar = this.c.get(this.M);
            cVar.f();
            if (this.R > 0 && "A_OPUS".equals(cVar.b)) {
                this.n.M(ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(this.R).array());
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.K; i3++) {
                i2 += this.L[i3];
            }
            int i4 = 0;
            while (i4 < this.K) {
                long j = this.H + ((cVar.e * i4) / 1000);
                int i5 = this.O;
                if (i4 == 0 && !this.Q) {
                    i5 |= 1;
                }
                int i6 = this.L[i4];
                int i7 = i2 - i6;
                o(cVar, j, i5, i6, i7);
                i4++;
                i2 = i7;
            }
            this.G = 0;
        } else if (i == 174) {
            c cVar2 = (c) ii.i(this.u);
            String str = cVar2.b;
            if (str != null) {
                if (z(str)) {
                    cVar2.i(this.b0, cVar2.c);
                    this.c.put(cVar2.c, cVar2);
                }
                this.u = null;
                return;
            }
            throw ParserException.createForMalformedContainer("CodecId is missing in TrackEntry element", null);
        } else if (i == 19899) {
            int i8 = this.w;
            if (i8 != -1) {
                long j2 = this.x;
                if (j2 != -1) {
                    if (i8 == 475249515) {
                        this.z = j2;
                        return;
                    }
                    return;
                }
            }
            throw ParserException.createForMalformedContainer("Mandatory element SeekID or SeekPosition not found", null);
        } else if (i == 25152) {
            k(i);
            c cVar3 = this.u;
            if (cVar3.h) {
                if (cVar3.j != null) {
                    cVar3.l = new DrmInitData(new DrmInitData.SchemeData(ft.a, "video/webm", this.u.j.b));
                    return;
                }
                throw ParserException.createForMalformedContainer("Encrypted Track found but ContentEncKeyID was not found", null);
            }
        } else if (i == 28032) {
            k(i);
            c cVar4 = this.u;
            if (cVar4.h && cVar4.i != null) {
                throw ParserException.createForMalformedContainer("Combining encryption and compression is not supported", null);
            }
        } else if (i == 357149030) {
            if (this.r == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                this.r = 1000000L;
            }
            long j3 = this.s;
            if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                this.t = F(j3);
            }
        } else if (i == 374648427) {
            if (this.c.size() != 0) {
                this.b0.m();
                return;
            }
            throw ParserException.createForMalformedContainer("No valid tracks were found", null);
        } else if (i == 475249515) {
            if (!this.v) {
                this.b0.p(n(this.C, this.D));
                this.v = true;
            }
            this.C = null;
            this.D = null;
        }
    }

    public final int r() {
        int i = this.T;
        E();
        return i;
    }

    public void s(int i, double d) throws ParserException {
        if (i == 181) {
            u(i).Q = (int) d;
        } else if (i != 17545) {
            switch (i) {
                case 21969:
                    u(i).D = (float) d;
                    return;
                case 21970:
                    u(i).E = (float) d;
                    return;
                case 21971:
                    u(i).F = (float) d;
                    return;
                case 21972:
                    u(i).G = (float) d;
                    return;
                case 21973:
                    u(i).H = (float) d;
                    return;
                case 21974:
                    u(i).I = (float) d;
                    return;
                case 21975:
                    u(i).J = (float) d;
                    return;
                case 21976:
                    u(i).K = (float) d;
                    return;
                case 21977:
                    u(i).L = (float) d;
                    return;
                case 21978:
                    u(i).M = (float) d;
                    return;
                default:
                    switch (i) {
                        case 30323:
                            u(i).s = (float) d;
                            return;
                        case 30324:
                            u(i).t = (float) d;
                            return;
                        case 30325:
                            u(i).u = (float) d;
                            return;
                        default:
                            return;
                    }
            }
        } else {
            this.s = (long) d;
        }
    }

    public c u(int i) throws ParserException {
        k(i);
        return this.u;
    }

    public int v(int i) {
        switch (i) {
            case 131:
            case 136:
            case 155:
            case 159:
            case 176:
            case 179:
            case 186:
            case 215:
            case 231:
            case 238:
            case 241:
            case 251:
            case 16871:
            case 16980:
            case 17029:
            case 17143:
            case 18401:
            case 18408:
            case 20529:
            case 20530:
            case 21420:
            case 21432:
            case 21680:
            case 21682:
            case 21690:
            case 21930:
            case 21945:
            case 21946:
            case 21947:
            case 21948:
            case 21949:
            case 21998:
            case 22186:
            case 22203:
            case 25188:
            case 30114:
            case 30321:
            case 2352003:
            case 2807729:
                return 2;
            case 134:
            case 17026:
            case 21358:
            case 2274716:
                return 3;
            case 160:
            case 166:
            case 174:
            case 183:
            case 187:
            case 224:
            case 225:
            case 16868:
            case 18407:
            case 19899:
            case 20532:
            case 20533:
            case 21936:
            case 21968:
            case 25152:
            case 28032:
            case 30113:
            case 30320:
            case 290298740:
            case 357149030:
            case 374648427:
            case 408125543:
            case 440786851:
            case 475249515:
            case 524531317:
                return 1;
            case 161:
            case 163:
            case 165:
            case 16877:
            case 16981:
            case 18402:
            case 21419:
            case 25506:
            case 30322:
                return 4;
            case 181:
            case 17545:
            case 21969:
            case 21970:
            case 21971:
            case 21972:
            case 21973:
            case 21974:
            case 21975:
            case 21976:
            case 21977:
            case 21978:
            case 30323:
            case 30324:
            case 30325:
                return 5;
            default:
                return 0;
        }
    }

    public void w(c cVar, q11 q11Var, int i) throws IOException {
        if (cVar.g != 1685485123 && cVar.g != 1685480259) {
            q11Var.k(i);
            return;
        }
        byte[] bArr = new byte[i];
        cVar.N = bArr;
        q11Var.readFully(bArr, 0, i);
    }

    public void x(c cVar, int i, q11 q11Var, int i2) throws IOException {
        if (i == 4 && "V_VP9".equals(cVar.b)) {
            this.n.L(i2);
            q11Var.readFully(this.n.d(), 0, i2);
            return;
        }
        q11Var.k(i2);
    }

    public void y(int i, long j) throws ParserException {
        if (i == 20529) {
            if (j == 0) {
                return;
            }
            throw ParserException.createForMalformedContainer("ContentEncodingOrder " + j + " not supported", null);
        } else if (i == 20530) {
            if (j == 1) {
                return;
            }
            throw ParserException.createForMalformedContainer("ContentEncodingScope " + j + " not supported", null);
        } else {
            switch (i) {
                case 131:
                    u(i).d = (int) j;
                    return;
                case 136:
                    u(i).V = j == 1;
                    return;
                case 155:
                    this.I = F(j);
                    return;
                case 159:
                    u(i).O = (int) j;
                    return;
                case 176:
                    u(i).m = (int) j;
                    return;
                case 179:
                    i(i);
                    this.C.a(F(j));
                    return;
                case 186:
                    u(i).n = (int) j;
                    return;
                case 215:
                    u(i).c = (int) j;
                    return;
                case 231:
                    this.B = F(j);
                    return;
                case 238:
                    this.P = (int) j;
                    return;
                case 241:
                    if (this.E) {
                        return;
                    }
                    i(i);
                    this.D.a(j);
                    this.E = true;
                    return;
                case 251:
                    this.Q = true;
                    return;
                case 16871:
                    u(i).g = (int) j;
                    return;
                case 16980:
                    if (j == 3) {
                        return;
                    }
                    throw ParserException.createForMalformedContainer("ContentCompAlgo " + j + " not supported", null);
                case 17029:
                    if (j < 1 || j > 2) {
                        throw ParserException.createForMalformedContainer("DocTypeReadVersion " + j + " not supported", null);
                    }
                    return;
                case 17143:
                    if (j == 1) {
                        return;
                    }
                    throw ParserException.createForMalformedContainer("EBMLReadVersion " + j + " not supported", null);
                case 18401:
                    if (j == 5) {
                        return;
                    }
                    throw ParserException.createForMalformedContainer("ContentEncAlgo " + j + " not supported", null);
                case 18408:
                    if (j == 1) {
                        return;
                    }
                    throw ParserException.createForMalformedContainer("AESSettingsCipherMode " + j + " not supported", null);
                case 21420:
                    this.x = j + this.q;
                    return;
                case 21432:
                    int i2 = (int) j;
                    k(i);
                    if (i2 == 0) {
                        this.u.w = 0;
                        return;
                    } else if (i2 == 1) {
                        this.u.w = 2;
                        return;
                    } else if (i2 == 3) {
                        this.u.w = 1;
                        return;
                    } else if (i2 != 15) {
                        return;
                    } else {
                        this.u.w = 3;
                        return;
                    }
                case 21680:
                    u(i).o = (int) j;
                    return;
                case 21682:
                    u(i).q = (int) j;
                    return;
                case 21690:
                    u(i).p = (int) j;
                    return;
                case 21930:
                    u(i).U = j == 1;
                    return;
                case 21998:
                    u(i).f = (int) j;
                    return;
                case 22186:
                    u(i).R = j;
                    return;
                case 22203:
                    u(i).S = j;
                    return;
                case 25188:
                    u(i).P = (int) j;
                    return;
                case 30114:
                    this.R = j;
                    return;
                case 30321:
                    k(i);
                    int i3 = (int) j;
                    if (i3 == 0) {
                        this.u.r = 0;
                        return;
                    } else if (i3 == 1) {
                        this.u.r = 1;
                        return;
                    } else if (i3 == 2) {
                        this.u.r = 2;
                        return;
                    } else if (i3 != 3) {
                        return;
                    } else {
                        this.u.r = 3;
                        return;
                    }
                case 2352003:
                    u(i).e = (int) j;
                    return;
                case 2807729:
                    this.r = j;
                    return;
                default:
                    switch (i) {
                        case 21945:
                            k(i);
                            int i4 = (int) j;
                            if (i4 == 1) {
                                this.u.A = 2;
                                return;
                            } else if (i4 != 2) {
                                return;
                            } else {
                                this.u.A = 1;
                                return;
                            }
                        case 21946:
                            k(i);
                            int c2 = f.c((int) j);
                            if (c2 != -1) {
                                this.u.z = c2;
                                return;
                            }
                            return;
                        case 21947:
                            k(i);
                            this.u.x = true;
                            int b2 = f.b((int) j);
                            if (b2 != -1) {
                                this.u.y = b2;
                                return;
                            }
                            return;
                        case 21948:
                            u(i).B = (int) j;
                            return;
                        case 21949:
                            u(i).C = (int) j;
                            return;
                        default:
                            return;
                    }
            }
        }
    }

    public d52(int i) {
        this(new cj0(), i);
    }

    public d52(au0 au0Var, int i) {
        this.q = -1L;
        this.r = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.s = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.t = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.z = -1L;
        this.A = -1L;
        this.B = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.a = au0Var;
        au0Var.a(new b());
        this.d = (i & 1) == 0;
        this.b = new bh4();
        this.c = new SparseArray<>();
        this.g = new op2(4);
        this.h = new op2(ByteBuffer.allocate(4).putInt(-1).array());
        this.i = new op2(4);
        this.e = new op2(wc2.a);
        this.f = new op2(4);
        this.j = new op2();
        this.k = new op2();
        this.l = new op2(8);
        this.m = new op2();
        this.n = new op2();
        this.L = new int[1];
    }
}
