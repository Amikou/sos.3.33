package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: BranchOnSeparateImagesProducer.java */
/* renamed from: ir  reason: default package */
/* loaded from: classes.dex */
public class ir implements dv2<zu0> {
    public final dv2<zu0> a;
    public final dv2<zu0> b;

    /* compiled from: BranchOnSeparateImagesProducer.java */
    /* renamed from: ir$b */
    /* loaded from: classes.dex */
    public class b extends bm0<zu0, zu0> {
        public ev2 c;

        @Override // defpackage.bm0, defpackage.qm
        public void h(Throwable th) {
            ir.this.b.a(p(), this.c);
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(zu0 zu0Var, int i) {
            ImageRequest c = this.c.c();
            boolean e = qm.e(i);
            boolean c2 = u54.c(zu0Var, c.q());
            if (zu0Var != null && (c2 || c.i())) {
                if (e && c2) {
                    p().d(zu0Var, i);
                } else {
                    p().d(zu0Var, qm.o(i, 1));
                }
            }
            if (!e || c2 || c.h()) {
                return;
            }
            zu0.c(zu0Var);
            ir.this.b.a(p(), this.c);
        }

        public b(l60<zu0> l60Var, ev2 ev2Var) {
            super(l60Var);
            this.c = ev2Var;
        }
    }

    public ir(dv2<zu0> dv2Var, dv2<zu0> dv2Var2) {
        this.a = dv2Var;
        this.b = dv2Var2;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        this.a.a(new b(l60Var, ev2Var), ev2Var);
    }
}
