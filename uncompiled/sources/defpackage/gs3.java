package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: ChannelFlow.kt */
/* renamed from: gs3  reason: default package */
/* loaded from: classes2.dex */
public final class gs3<T> implements q70<T>, e90 {
    public final q70<T> a;
    public final CoroutineContext f0;

    /* JADX WARN: Multi-variable type inference failed */
    public gs3(q70<? super T> q70Var, CoroutineContext coroutineContext) {
        this.a = q70Var;
        this.f0 = coroutineContext;
    }

    @Override // defpackage.e90
    public e90 getCallerFrame() {
        q70<T> q70Var = this.a;
        if (q70Var instanceof e90) {
            return (e90) q70Var;
        }
        return null;
    }

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        return this.f0;
    }

    @Override // defpackage.e90
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
        this.a.resumeWith(obj);
    }
}
