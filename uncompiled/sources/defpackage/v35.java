package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.common.api.internal.zzb;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: v35  reason: default package */
/* loaded from: classes.dex */
public final class v35 implements Runnable {
    public final /* synthetic */ LifecycleCallback a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ zzb g0;

    public v35(zzb zzbVar, LifecycleCallback lifecycleCallback, String str) {
        this.g0 = zzbVar;
        this.a = lifecycleCallback;
        this.f0 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        Bundle bundle;
        Bundle bundle2;
        Bundle bundle3;
        i = this.g0.f0;
        if (i > 0) {
            LifecycleCallback lifecycleCallback = this.a;
            bundle = this.g0.g0;
            if (bundle != null) {
                bundle3 = this.g0.g0;
                bundle2 = bundle3.getBundle(this.f0);
            } else {
                bundle2 = null;
            }
            lifecycleCallback.e(bundle2);
        }
        i2 = this.g0.f0;
        if (i2 >= 2) {
            this.a.i();
        }
        i3 = this.g0.f0;
        if (i3 >= 3) {
            this.a.g();
        }
        i4 = this.g0.f0;
        if (i4 >= 4) {
            this.a.j();
        }
        i5 = this.g0.f0;
        if (i5 >= 5) {
            this.a.f();
        }
    }
}
