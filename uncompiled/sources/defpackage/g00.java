package defpackage;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.o;

/* compiled from: CMCListCheckable.kt */
/* renamed from: g00  reason: default package */
/* loaded from: classes2.dex */
public final class g00 extends o<d11, f11> {
    public final rl1 a;

    public g00(rl1 rl1Var) {
        super(vl1.a);
        this.a = rl1Var;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: a */
    public void onBindViewHolder(f11 f11Var, int i) {
        fs1.f(f11Var, "holder");
        d11 item = getItem(i);
        fs1.e(item, "getItem(position)");
        f11Var.b(item);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public f11 onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        vs1 c = vs1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        fs1.e(c, "inflate(LayoutInflater.f…t.context),parent, false)");
        return new f11(c, this.a);
    }
}
