package defpackage;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WorkNameDao_Impl.java */
/* renamed from: lq4  reason: default package */
/* loaded from: classes.dex */
public final class lq4 implements kq4 {
    public final RoomDatabase a;
    public final zv0<jq4> b;

    /* compiled from: WorkNameDao_Impl.java */
    /* renamed from: lq4$a */
    /* loaded from: classes.dex */
    public class a extends zv0<jq4> {
        public a(lq4 lq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR IGNORE INTO `WorkName` (`name`,`work_spec_id`) VALUES (?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, jq4 jq4Var) {
            String str = jq4Var.a;
            if (str == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, str);
            }
            String str2 = jq4Var.b;
            if (str2 == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.L(2, str2);
            }
        }
    }

    public lq4(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @Override // defpackage.kq4
    public void a(jq4 jq4Var) {
        this.a.d();
        this.a.e();
        try {
            this.b.h(jq4Var);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.kq4
    public List<String> b(String str) {
        k93 c = k93.c("SELECT name FROM workname WHERE work_spec_id=?", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        this.a.d();
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            ArrayList arrayList = new ArrayList(c2.getCount());
            while (c2.moveToNext()) {
                arrayList.add(c2.getString(0));
            }
            return arrayList;
        } finally {
            c2.close();
            c.f();
        }
    }
}
