package defpackage;

/* compiled from: GlideSuppliers.java */
/* renamed from: lg1  reason: default package */
/* loaded from: classes.dex */
public final class lg1 {

    /* compiled from: GlideSuppliers.java */
    /* renamed from: lg1$a */
    /* loaded from: classes.dex */
    public class a implements b<T> {
        public volatile T a;
        public final /* synthetic */ b b;

        public a(b bVar) {
            this.b = bVar;
        }

        /* JADX WARN: Type inference failed for: r0v6, types: [T, java.lang.Object] */
        @Override // defpackage.lg1.b
        public T get() {
            if (this.a == 0) {
                synchronized (this) {
                    if (this.a == 0) {
                        this.a = wt2.d(this.b.get());
                    }
                }
            }
            return this.a;
        }
    }

    /* compiled from: GlideSuppliers.java */
    /* renamed from: lg1$b */
    /* loaded from: classes.dex */
    public interface b<T> {
        T get();
    }

    public static <T> b<T> a(b<T> bVar) {
        return new a(bVar);
    }
}
