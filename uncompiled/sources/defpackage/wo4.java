package defpackage;

import android.text.TextUtils;
import androidx.media3.common.ParserException;
import androidx.media3.extractor.text.SubtitleDecoderException;
import androidx.media3.extractor.text.a;
import java.util.ArrayList;

/* compiled from: WebvttDecoder.java */
/* renamed from: wo4  reason: default package */
/* loaded from: classes.dex */
public final class wo4 extends a {
    public final op2 n;
    public final ro4 o;

    public wo4() {
        super("WebvttDecoder");
        this.n = new op2();
        this.o = new ro4();
    }

    public static int C(op2 op2Var) {
        int i = 0;
        int i2 = -1;
        while (i2 == -1) {
            i = op2Var.e();
            String p = op2Var.p();
            if (p == null) {
                i2 = 0;
            } else if ("STYLE".equals(p)) {
                i2 = 2;
            } else {
                i2 = p.startsWith("NOTE") ? 1 : 3;
            }
        }
        op2Var.P(i);
        return i2;
    }

    public static void D(op2 op2Var) {
        do {
        } while (!TextUtils.isEmpty(op2Var.p()));
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        to4 m;
        this.n.N(bArr, i);
        ArrayList arrayList = new ArrayList();
        try {
            xo4.d(this.n);
            do {
            } while (!TextUtils.isEmpty(this.n.p()));
            ArrayList arrayList2 = new ArrayList();
            while (true) {
                int C = C(this.n);
                if (C == 0) {
                    return new zo4(arrayList2);
                }
                if (C == 1) {
                    D(this.n);
                } else if (C == 2) {
                    if (arrayList2.isEmpty()) {
                        this.n.p();
                        arrayList.addAll(this.o.d(this.n));
                    } else {
                        throw new SubtitleDecoderException("A style block was found after the first cue.");
                    }
                } else if (C == 3 && (m = uo4.m(this.n, arrayList)) != null) {
                    arrayList2.add(m);
                }
            }
        } catch (ParserException e) {
            throw new SubtitleDecoderException(e);
        }
    }
}
