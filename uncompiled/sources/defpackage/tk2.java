package defpackage;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalStateSynchronizer;
import com.onesignal.b1;
import com.onesignal.n0;
import org.json.JSONObject;

/* compiled from: OSSMSSubscriptionState.java */
/* renamed from: tk2  reason: default package */
/* loaded from: classes2.dex */
public class tk2 implements Cloneable {
    public n0<Object, tk2> a = new n0<>("changed", false);
    public String f0;
    public String g0;

    public tk2(boolean z) {
        if (z) {
            String str = b1.a;
            this.f0 = b1.f(str, "PREFS_OS_SMS_ID_LAST", null);
            this.g0 = b1.f(str, "PREFS_OS_SMS_NUMBER_LAST", null);
            return;
        }
        this.f0 = OneSignal.m0();
        this.g0 = OneSignalStateSynchronizer.f().E();
    }

    public n0<Object, tk2> a() {
        return this.a;
    }

    public boolean b() {
        return (this.f0 == null || this.g0 == null) ? false : true;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (Throwable unused) {
            return null;
        }
    }

    public void d() {
        String str = b1.a;
        b1.m(str, "PREFS_OS_SMS_ID_LAST", this.f0);
        b1.m(str, "PREFS_OS_SMS_NUMBER_LAST", this.g0);
    }

    public void e(String str) {
        boolean z = true;
        if (str != null ? str.equals(this.f0) : this.f0 == null) {
            z = false;
        }
        this.f0 = str;
        if (z) {
            this.a.c(this);
        }
    }

    public JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        try {
            String str = this.f0;
            if (str != null) {
                jSONObject.put("smsUserId", str);
            } else {
                jSONObject.put("smsUserId", JSONObject.NULL);
            }
            String str2 = this.g0;
            if (str2 != null) {
                jSONObject.put("smsNumber", str2);
            } else {
                jSONObject.put("smsNumber", JSONObject.NULL);
            }
            jSONObject.put("isSubscribed", b());
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return f().toString();
    }
}
