package defpackage;

import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidFrameException;
import org.java_websocket.framing.Framedata;

/* compiled from: DefaultExtension.java */
/* renamed from: gj0  reason: default package */
/* loaded from: classes2.dex */
public class gj0 implements xl1 {
    @Override // defpackage.xl1
    public xl1 a() {
        return new gj0();
    }

    @Override // defpackage.xl1
    public boolean b(String str) {
        return true;
    }

    @Override // defpackage.xl1
    public void c(Framedata framedata) throws InvalidDataException {
    }

    @Override // defpackage.xl1
    public boolean d(String str) {
        return true;
    }

    @Override // defpackage.xl1
    public void e(Framedata framedata) {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && gj0.class == obj.getClass();
    }

    @Override // defpackage.xl1
    public void f(Framedata framedata) throws InvalidDataException {
        if (framedata.a() || framedata.b() || framedata.d()) {
            throw new InvalidFrameException("bad rsv RSV1: " + framedata.a() + " RSV2: " + framedata.b() + " RSV3: " + framedata.d());
        }
    }

    @Override // defpackage.xl1
    public String g() {
        return "";
    }

    @Override // defpackage.xl1
    public String h() {
        return "";
    }

    public int hashCode() {
        return gj0.class.hashCode();
    }

    @Override // defpackage.xl1
    public void reset() {
    }

    @Override // defpackage.xl1
    public String toString() {
        return gj0.class.getSimpleName();
    }
}
