package defpackage;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FieldDescriptor.java */
/* renamed from: h31  reason: default package */
/* loaded from: classes2.dex */
public final class h31 {
    public final String a;
    public final Map<Class<?>, Object> b;

    /* compiled from: FieldDescriptor.java */
    /* renamed from: h31$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public final String a;
        public Map<Class<?>, Object> b = null;

        public b(String str) {
            this.a = str;
        }

        public h31 a() {
            Map unmodifiableMap;
            String str = this.a;
            if (this.b == null) {
                unmodifiableMap = Collections.emptyMap();
            } else {
                unmodifiableMap = Collections.unmodifiableMap(new HashMap(this.b));
            }
            return new h31(str, unmodifiableMap);
        }

        public <T extends Annotation> b b(T t) {
            if (this.b == null) {
                this.b = new HashMap();
            }
            this.b.put(t.annotationType(), t);
            return this;
        }
    }

    public static b a(String str) {
        return new b(str);
    }

    public static h31 d(String str) {
        return new h31(str, Collections.emptyMap());
    }

    public String b() {
        return this.a;
    }

    public <T extends Annotation> T c(Class<T> cls) {
        return (T) this.b.get(cls);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof h31) {
            h31 h31Var = (h31) obj;
            return this.a.equals(h31Var.a) && this.b.equals(h31Var.b);
        }
        return false;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    public String toString() {
        return "FieldDescriptor{name=" + this.a + ", properties=" + this.b.values() + "}";
    }

    public h31(String str, Map<Class<?>, Object> map) {
        this.a = str;
        this.b = map;
    }
}
