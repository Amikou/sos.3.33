package defpackage;

import defpackage.ey0;

/* compiled from: AutoValue_EventStoreConfig.java */
/* renamed from: kl  reason: default package */
/* loaded from: classes.dex */
public final class kl extends ey0 {
    public final long b;
    public final int c;
    public final int d;
    public final long e;
    public final int f;

    /* compiled from: AutoValue_EventStoreConfig.java */
    /* renamed from: kl$b */
    /* loaded from: classes.dex */
    public static final class b extends ey0.a {
        public Long a;
        public Integer b;
        public Integer c;
        public Long d;
        public Integer e;

        @Override // defpackage.ey0.a
        public ey0 a() {
            String str = "";
            if (this.a == null) {
                str = " maxStorageSizeInBytes";
            }
            if (this.b == null) {
                str = str + " loadBatchSize";
            }
            if (this.c == null) {
                str = str + " criticalSectionEnterTimeoutMs";
            }
            if (this.d == null) {
                str = str + " eventCleanUpAge";
            }
            if (this.e == null) {
                str = str + " maxBlobByteSizePerRow";
            }
            if (str.isEmpty()) {
                return new kl(this.a.longValue(), this.b.intValue(), this.c.intValue(), this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.ey0.a
        public ey0.a b(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.ey0.a
        public ey0.a c(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.ey0.a
        public ey0.a d(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.ey0.a
        public ey0.a e(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.ey0.a
        public ey0.a f(long j) {
            this.a = Long.valueOf(j);
            return this;
        }
    }

    @Override // defpackage.ey0
    public int b() {
        return this.d;
    }

    @Override // defpackage.ey0
    public long c() {
        return this.e;
    }

    @Override // defpackage.ey0
    public int d() {
        return this.c;
    }

    @Override // defpackage.ey0
    public int e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ey0) {
            ey0 ey0Var = (ey0) obj;
            return this.b == ey0Var.f() && this.c == ey0Var.d() && this.d == ey0Var.b() && this.e == ey0Var.c() && this.f == ey0Var.e();
        }
        return false;
    }

    @Override // defpackage.ey0
    public long f() {
        return this.b;
    }

    public int hashCode() {
        long j = this.b;
        long j2 = this.e;
        return ((((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.c) * 1000003) ^ this.d) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.f;
    }

    public String toString() {
        return "EventStoreConfig{maxStorageSizeInBytes=" + this.b + ", loadBatchSize=" + this.c + ", criticalSectionEnterTimeoutMs=" + this.d + ", eventCleanUpAge=" + this.e + ", maxBlobByteSizePerRow=" + this.f + "}";
    }

    public kl(long j, int i, int i2, long j2, int i3) {
        this.b = j;
        this.c = i;
        this.d = i2;
        this.e = j2;
        this.f = i3;
    }
}
