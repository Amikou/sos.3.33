package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: h46  reason: default package */
/* loaded from: classes.dex */
public final class h46 implements g46 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.upload.file_lock_state_check", true);

    @Override // defpackage.g46
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
