package defpackage;

import com.google.android.gms.internal.clearcut.j;
import com.google.android.gms.internal.clearcut.k;
import com.google.protobuf.t;

/* renamed from: x95  reason: default package */
/* loaded from: classes.dex */
public final class x95 {
    public static final j<?> a = new k();
    public static final j<?> b = a();

    public static j<?> a() {
        try {
            int i = t.b;
            return (j) t.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    public static j<?> b() {
        return a;
    }

    public static j<?> c() {
        j<?> jVar = b;
        if (jVar != null) {
            return jVar;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
