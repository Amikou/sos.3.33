package defpackage;

import com.onesignal.influence.domain.OSInfluenceType;

/* renamed from: ek2  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class ek2 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[OSInfluenceType.values().length];
        a = iArr;
        OSInfluenceType oSInfluenceType = OSInfluenceType.DIRECT;
        iArr[oSInfluenceType.ordinal()] = 1;
        OSInfluenceType oSInfluenceType2 = OSInfluenceType.INDIRECT;
        iArr[oSInfluenceType2.ordinal()] = 2;
        int[] iArr2 = new int[OSInfluenceType.values().length];
        b = iArr2;
        iArr2[oSInfluenceType.ordinal()] = 1;
        iArr2[oSInfluenceType2.ordinal()] = 2;
    }
}
