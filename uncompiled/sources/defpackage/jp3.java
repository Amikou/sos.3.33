package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: SimpleListViewBinding.java */
/* renamed from: jp3  reason: default package */
/* loaded from: classes2.dex */
public final class jp3 {
    public final rp3 a;
    public final RecyclerView b;
    public final zd3 c;

    public jp3(ConstraintLayout constraintLayout, rp3 rp3Var, RecyclerView recyclerView, zd3 zd3Var) {
        this.a = rp3Var;
        this.b = recyclerView;
        this.c = zd3Var;
    }

    public static jp3 a(View view) {
        int i = R.id.includeTopBar;
        View a = ai4.a(view, R.id.includeTopBar);
        if (a != null) {
            rp3 a2 = rp3.a(a);
            int i2 = R.id.listView;
            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.listView);
            if (recyclerView != null) {
                i2 = R.id.searchBar;
                View a3 = ai4.a(view, R.id.searchBar);
                if (a3 != null) {
                    return new jp3((ConstraintLayout) view, a2, recyclerView, zd3.a(a3));
                }
            }
            i = i2;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
