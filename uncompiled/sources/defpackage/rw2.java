package defpackage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;

/* compiled from: PurchaseMethod.kt */
/* renamed from: rw2  reason: default package */
/* loaded from: classes2.dex */
public final class rw2 {
    public static final rw2 a = new rw2();

    public static final void e(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void f(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public final Dialog c(Context context) {
        Dialog dialog = new Dialog(context, 2132017235);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.getAttributes().gravity = 17;
            window.getAttributes().width = -1;
        }
        return dialog;
    }

    public final Dialog d(WeakReference<Activity> weakReference, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog c = a.c(activity);
        wn0 a2 = wn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_purchase_method, (ViewGroup) null));
        fs1.e(a2, "bind(it.layoutInflater.i…g_purchase_method, null))");
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: qw2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                rw2.e(tc1.this, c, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: pw2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                rw2.f(tc1.this, c, view);
            }
        });
        a2.d.setText(activity.getString(R.string.purchase_method_moonpay_fee, new Object[]{Character.valueOf(b30.a.y())}));
        c.setContentView(a2.b());
        c.show();
        return c;
    }
}
