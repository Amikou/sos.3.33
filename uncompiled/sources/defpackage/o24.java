package defpackage;

/* compiled from: SystemClock.java */
/* renamed from: o24  reason: default package */
/* loaded from: classes.dex */
public class o24 implements pz {
    public static final o24 a = new o24();

    public static o24 a() {
        return a;
    }

    @Override // defpackage.pz
    public long now() {
        return System.currentTimeMillis();
    }
}
