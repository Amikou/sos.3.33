package defpackage;

/* compiled from: Ranges.kt */
/* renamed from: t33  reason: default package */
/* loaded from: classes2.dex */
public class t33 {
    public static final void a(boolean z, Number number) {
        fs1.f(number, "step");
        if (z) {
            return;
        }
        throw new IllegalArgumentException("Step must be positive, was: " + number + '.');
    }
}
