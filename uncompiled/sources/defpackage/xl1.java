package defpackage;

import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.framing.Framedata;

/* compiled from: IExtension.java */
/* renamed from: xl1  reason: default package */
/* loaded from: classes2.dex */
public interface xl1 {
    xl1 a();

    boolean b(String str);

    void c(Framedata framedata) throws InvalidDataException;

    boolean d(String str);

    void e(Framedata framedata);

    void f(Framedata framedata) throws InvalidDataException;

    String g();

    String h();

    void reset();

    String toString();
}
