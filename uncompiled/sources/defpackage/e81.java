package defpackage;

/* compiled from: FlowableScalarXMap.java */
/* renamed from: e81  reason: default package */
/* loaded from: classes2.dex */
public final class e81 {

    /* compiled from: FlowableScalarXMap.java */
    /* renamed from: e81$a */
    /* loaded from: classes2.dex */
    public static final class a<T, R> extends q71<R> {
        public final T b;

        public a(T t, ld1<? super T, ? extends nw2<? extends R>> ld1Var) {
            this.b = t;
        }
    }

    public static <T, U> q71<U> a(T t, ld1<? super T, ? extends nw2<? extends U>> ld1Var) {
        return da3.h(new a(t, ld1Var));
    }
}
