package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: mu4  reason: default package */
/* loaded from: classes2.dex */
public abstract class mu4<StateT> {
    public final it4 a;
    public final IntentFilter b;
    public final Context c;
    public final Set<dt3<StateT>> d = new HashSet();
    public ys4 e = null;
    public volatile boolean f = false;

    public mu4(it4 it4Var, IntentFilter intentFilter, Context context) {
        this.a = it4Var;
        this.b = intentFilter;
        this.c = ny4.c(context);
    }

    public abstract void a(Context context, Intent intent);

    public final void b() {
        ys4 ys4Var;
        if ((this.f || !this.d.isEmpty()) && this.e == null) {
            ys4 ys4Var2 = new ys4(this);
            this.e = ys4Var2;
            this.c.registerReceiver(ys4Var2, this.b);
        }
        if (this.f || !this.d.isEmpty() || (ys4Var = this.e) == null) {
            return;
        }
        this.c.unregisterReceiver(ys4Var);
        this.e = null;
    }

    public final synchronized void c(boolean z) {
        this.f = z;
        b();
    }

    public final synchronized void d(StateT statet) {
        Iterator it = new HashSet(this.d).iterator();
        while (it.hasNext()) {
            ((dt3) it.next()).a(statet);
        }
    }

    public final synchronized boolean e() {
        return this.e != null;
    }
}
