package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.SetPasswordActivity;

/* renamed from: pm3  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class pm3 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ pm3 a = new pm3();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        SetPasswordActivity.z(dialogInterface);
    }
}
