package defpackage;

import androidx.paging.LoadType;
import java.util.List;

/* compiled from: DataSource.kt */
/* renamed from: fe0  reason: default package */
/* loaded from: classes.dex */
public abstract class fe0<Key, Value> {

    /* compiled from: DataSource.kt */
    /* renamed from: fe0$a */
    /* loaded from: classes.dex */
    public static final class a<Value> {
        public final List<Value> a;
        public final Object b;
        public final Object c;
        public final int d;
        public final int e;

        /* compiled from: DataSource.kt */
        /* renamed from: fe0$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0171a {
            public C0171a() {
            }

            public /* synthetic */ C0171a(qi0 qi0Var) {
                this();
            }
        }

        static {
            new C0171a(null);
        }

        public final int a() {
            return this.e;
        }

        public final int b() {
            return this.d;
        }

        public final Object c() {
            return this.c;
        }

        public final Object d() {
            return this.b;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return fs1.b(this.a, aVar.a) && fs1.b(this.b, aVar.b) && fs1.b(this.c, aVar.c) && this.d == aVar.d && this.e == aVar.e;
            }
            return false;
        }
    }

    /* compiled from: DataSource.kt */
    /* renamed from: fe0$b */
    /* loaded from: classes.dex */
    public static final class b<K> {
        public final LoadType a;
        public final K b;

        public b(LoadType loadType, K k, int i, boolean z, int i2) {
            fs1.f(loadType, "type");
            this.a = loadType;
            this.b = k;
            if (loadType != LoadType.REFRESH && k == null) {
                throw new IllegalArgumentException("Key must be non-null for prepend/append");
            }
        }
    }
}
