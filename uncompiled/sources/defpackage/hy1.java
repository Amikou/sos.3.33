package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.hy1;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LanguageItem;

/* compiled from: LanguageListAdapter.kt */
/* renamed from: hy1  reason: default package */
/* loaded from: classes2.dex */
public abstract class hy1 extends RecyclerView.Adapter<b> {
    public final Context a;
    public final a b;
    public final List<LanguageItem> c;

    /* compiled from: LanguageListAdapter.kt */
    /* renamed from: hy1$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(LanguageItem languageItem);
    }

    /* compiled from: LanguageListAdapter.kt */
    /* renamed from: hy1$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final ts1 a;
        public final /* synthetic */ hy1 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(hy1 hy1Var, ts1 ts1Var) {
            super(ts1Var.b());
            fs1.f(hy1Var, "this$0");
            fs1.f(ts1Var, "binding");
            this.b = hy1Var;
            this.a = ts1Var;
        }

        public static final void c(hy1 hy1Var, LanguageItem languageItem, View view) {
            fs1.f(hy1Var, "this$0");
            fs1.f(languageItem, "$item");
            hy1Var.b.a(languageItem);
            hy1Var.c();
        }

        public final void b(final LanguageItem languageItem) {
            fs1.f(languageItem, "item");
            ts1 ts1Var = this.a;
            final hy1 hy1Var = this.b;
            ts1Var.c.setText(languageItem.getTitleResId());
            ts1Var.d.setText(languageItem.getRegionResId());
            ts1Var.b.setChecked(fs1.b(hy1Var.f(), languageItem.getLanguageCode()));
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: gy1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    hy1.b.c(hy1.this, languageItem, view);
                }
            });
            if (fs1.b(hy1Var.f(), languageItem.getLanguageCode())) {
                ts1Var.c.setTextColor(m70.d(hy1Var.b(), R.color.akt_register_guide_title));
                ts1Var.d.setTextColor(m70.d(hy1Var.b(), R.color.akt_register_guide_title));
                return;
            }
            ts1Var.c.setTextColor(m70.d(hy1Var.b(), R.color.white));
            ts1Var.d.setTextColor(m70.d(hy1Var.b(), R.color.white));
        }
    }

    public hy1(Context context, a aVar) {
        fs1.f(context, "context");
        fs1.f(aVar, "onDefaultLanguageSelectedListener");
        this.a = context;
        this.b = aVar;
        this.c = new ArrayList();
    }

    public final Context b() {
        return this.a;
    }

    public final void c() {
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.b(this.c.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        ts1 a2 = ts1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_akt_language, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        return new b(this, a2);
    }

    public abstract String f();

    public final void g(List<LanguageItem> list) {
        fs1.f(list, "items");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.c.size();
    }
}
