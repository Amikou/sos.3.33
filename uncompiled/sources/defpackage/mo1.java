package defpackage;

import android.graphics.Rect;
import com.facebook.common.references.a;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: ImagePerfMonitor.java */
/* renamed from: mo1  reason: default package */
/* loaded from: classes.dex */
public class mo1 implements no1 {
    public final wq2 a;
    public final o92 b;
    public final po1 c = new po1();
    public final fw3<Boolean> d;
    public go1 e;
    public fo1 f;
    public oo1 g;
    public io1 h;
    public k91 i;
    public List<ko1> j;
    public boolean k;

    public mo1(o92 o92Var, wq2 wq2Var, fw3<Boolean> fw3Var) {
        this.b = o92Var;
        this.a = wq2Var;
        this.d = fw3Var;
    }

    @Override // defpackage.no1
    public void a(po1 po1Var, int i) {
        List<ko1> list;
        if (!this.k || (list = this.j) == null || list.isEmpty()) {
            return;
        }
        jo1 B = po1Var.B();
        for (ko1 ko1Var : this.j) {
            ko1Var.a(B, i);
        }
    }

    @Override // defpackage.no1
    public void b(po1 po1Var, int i) {
        List<ko1> list;
        po1Var.o(i);
        if (!this.k || (list = this.j) == null || list.isEmpty()) {
            return;
        }
        if (i == 3) {
            d();
        }
        jo1 B = po1Var.B();
        for (ko1 ko1Var : this.j) {
            ko1Var.b(B, i);
        }
    }

    public void c(ko1 ko1Var) {
        if (ko1Var == null) {
            return;
        }
        if (this.j == null) {
            this.j = new CopyOnWriteArrayList();
        }
        this.j.add(ko1Var);
    }

    public void d() {
        jr0 d = this.a.d();
        if (d == null || d.f() == null) {
            return;
        }
        Rect bounds = d.f().getBounds();
        this.c.v(bounds.width());
        this.c.u(bounds.height());
    }

    public void e() {
        List<ko1> list = this.j;
        if (list != null) {
            list.clear();
        }
    }

    public void f() {
        e();
        g(false);
        this.c.b();
    }

    public void g(boolean z) {
        this.k = z;
        if (z) {
            h();
            fo1 fo1Var = this.f;
            if (fo1Var != null) {
                this.a.i0(fo1Var);
            }
            io1 io1Var = this.h;
            if (io1Var != null) {
                this.a.m(io1Var);
            }
            k91 k91Var = this.i;
            if (k91Var != null) {
                this.a.j0(k91Var);
                return;
            }
            return;
        }
        fo1 fo1Var2 = this.f;
        if (fo1Var2 != null) {
            this.a.y0(fo1Var2);
        }
        io1 io1Var2 = this.h;
        if (io1Var2 != null) {
            this.a.S(io1Var2);
        }
        k91 k91Var2 = this.i;
        if (k91Var2 != null) {
            this.a.z0(k91Var2);
        }
    }

    public final void h() {
        if (this.h == null) {
            this.h = new io1(this.b, this.c, this, this.d, gw3.a);
        }
        if (this.g == null) {
            this.g = new oo1(this.b, this.c);
        }
        if (this.f == null) {
            this.f = new lo1(this.c, this);
        }
        go1 go1Var = this.e;
        if (go1Var == null) {
            this.e = new go1(this.a.x(), this.f);
        } else {
            go1Var.l(this.a.x());
        }
        if (this.i == null) {
            this.i = new k91(this.g, this.e);
        }
    }

    public void i(AbstractDraweeControllerBuilder<xq2, ImageRequest, a<com.facebook.imagepipeline.image.a>, ao1> abstractDraweeControllerBuilder) {
        this.c.i(abstractDraweeControllerBuilder.n(), abstractDraweeControllerBuilder.o(), abstractDraweeControllerBuilder.m());
    }
}
