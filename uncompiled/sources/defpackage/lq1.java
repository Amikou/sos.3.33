package defpackage;

/* compiled from: IndexedValue.kt */
/* renamed from: lq1  reason: default package */
/* loaded from: classes2.dex */
public final class lq1<T> {
    public final int a;
    public final T b;

    public lq1(int i, T t) {
        this.a = i;
        this.b = t;
    }

    public final int a() {
        return this.a;
    }

    public final T b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof lq1) {
                lq1 lq1Var = (lq1) obj;
                return this.a == lq1Var.a && fs1.b(this.b, lq1Var.b);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = this.a * 31;
        T t = this.b;
        return i + (t != null ? t.hashCode() : 0);
    }

    public String toString() {
        return "IndexedValue(index=" + this.a + ", value=" + this.b + ")";
    }
}
