package defpackage;

import android.util.Pair;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

/* compiled from: ThrottlingProducer.java */
/* renamed from: n54  reason: default package */
/* loaded from: classes.dex */
public class n54<T> implements dv2<T> {
    public final dv2<T> a;
    public final int b;
    public final Executor e;
    public final ConcurrentLinkedQueue<Pair<l60<T>, ev2>> d = new ConcurrentLinkedQueue<>();
    public int c = 0;

    /* compiled from: ThrottlingProducer.java */
    /* renamed from: n54$b */
    /* loaded from: classes.dex */
    public class b extends bm0<T, T> {

        /* compiled from: ThrottlingProducer.java */
        /* renamed from: n54$b$a */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public final /* synthetic */ Pair a;

            public a(Pair pair) {
                this.a = pair;
            }

            @Override // java.lang.Runnable
            public void run() {
                n54 n54Var = n54.this;
                Pair pair = this.a;
                n54Var.e((l60) pair.first, (ev2) pair.second);
            }
        }

        @Override // defpackage.bm0, defpackage.qm
        public void g() {
            p().b();
            q();
        }

        @Override // defpackage.bm0, defpackage.qm
        public void h(Throwable th) {
            p().a(th);
            q();
        }

        @Override // defpackage.qm
        public void i(T t, int i) {
            p().d(t, i);
            if (qm.e(i)) {
                q();
            }
        }

        public final void q() {
            Pair pair;
            synchronized (n54.this) {
                pair = (Pair) n54.this.d.poll();
                if (pair == null) {
                    n54.c(n54.this);
                }
            }
            if (pair != null) {
                n54.this.e.execute(new a(pair));
            }
        }

        public b(l60<T> l60Var) {
            super(l60Var);
        }
    }

    public n54(int i, Executor executor, dv2<T> dv2Var) {
        this.b = i;
        this.e = (Executor) xt2.g(executor);
        this.a = (dv2) xt2.g(dv2Var);
    }

    public static /* synthetic */ int c(n54 n54Var) {
        int i = n54Var.c;
        n54Var.c = i - 1;
        return i;
    }

    @Override // defpackage.dv2
    public void a(l60<T> l60Var, ev2 ev2Var) {
        boolean z;
        ev2Var.l().k(ev2Var, "ThrottlingProducer");
        synchronized (this) {
            int i = this.c;
            z = true;
            if (i >= this.b) {
                this.d.add(Pair.create(l60Var, ev2Var));
            } else {
                this.c = i + 1;
                z = false;
            }
        }
        if (z) {
            return;
        }
        e(l60Var, ev2Var);
    }

    public void e(l60<T> l60Var, ev2 ev2Var) {
        ev2Var.l().a(ev2Var, "ThrottlingProducer", null);
        this.a.a(new b(l60Var), ev2Var);
    }
}
