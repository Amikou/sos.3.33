package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: nl5  reason: default package */
/* loaded from: classes.dex */
public final class nl5 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ long h0;
    public final /* synthetic */ pl5 i0;

    public nl5(pl5 pl5Var, String str, String str2, String str3, long j) {
        this.i0 = pl5Var;
        this.a = str;
        this.f0 = str2;
        this.g0 = str3;
        this.h0 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        fw5 fw5Var2;
        String str = this.a;
        if (str == null) {
            fw5Var2 = this.i0.a;
            fw5Var2.r().Q().y(this.f0, null);
            return;
        }
        bq5 bq5Var = new bq5(this.g0, str, this.h0);
        fw5Var = this.i0.a;
        fw5Var.r().Q().y(this.f0, bq5Var);
    }
}
