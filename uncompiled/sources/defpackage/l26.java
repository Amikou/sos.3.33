package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: l26  reason: default package */
/* loaded from: classes.dex */
public final class l26 implements yp5<m26> {
    public static final l26 f0 = new l26();
    public final yp5<m26> a = gq5.a(gq5.b(new n26()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    public static boolean c() {
        return f0.zza().zzc();
    }

    @Override // defpackage.yp5
    /* renamed from: d */
    public final m26 zza() {
        return this.a.zza();
    }
}
