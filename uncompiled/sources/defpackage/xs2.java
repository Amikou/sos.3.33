package defpackage;

import android.os.Build;
import com.facebook.common.memory.b;
import com.facebook.imagepipeline.memory.AshmemMemoryChunkPool;
import com.facebook.imagepipeline.memory.BufferMemoryChunkPool;
import com.facebook.imagepipeline.memory.NativeMemoryChunkPool;
import com.facebook.imagepipeline.memory.a;
import com.facebook.imagepipeline.memory.c;
import com.facebook.imagepipeline.memory.e;
import java.lang.reflect.InvocationTargetException;

/* compiled from: PoolFactory.java */
/* renamed from: xs2  reason: default package */
/* loaded from: classes.dex */
public class xs2 {
    public final ws2 a;
    public c b;
    public iq c;
    public c d;
    public y61 e;
    public c f;
    public b g;
    public com.facebook.common.memory.c h;
    public os i;

    public xs2(ws2 ws2Var) {
        this.a = (ws2) xt2.g(ws2Var);
    }

    public final c a() {
        if (this.b == null) {
            try {
                this.b = (c) AshmemMemoryChunkPool.class.getConstructor(r72.class, ys2.class, zs2.class).newInstance(this.a.i(), this.a.g(), this.a.h());
            } catch (ClassNotFoundException unused) {
                this.b = null;
            } catch (IllegalAccessException unused2) {
                this.b = null;
            } catch (InstantiationException unused3) {
                this.b = null;
            } catch (NoSuchMethodException unused4) {
                this.b = null;
            } catch (InvocationTargetException unused5) {
                this.b = null;
            }
        }
        return this.b;
    }

    public iq b() {
        if (this.c == null) {
            String e = this.a.e();
            char c = 65535;
            switch (e.hashCode()) {
                case -1868884870:
                    if (e.equals("legacy_default_params")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1106578487:
                    if (e.equals("legacy")) {
                        c = 4;
                        break;
                    }
                    break;
                case -404562712:
                    if (e.equals("experimental")) {
                        c = 2;
                        break;
                    }
                    break;
                case -402149703:
                    if (e.equals("dummy_with_tracking")) {
                        c = 1;
                        break;
                    }
                    break;
                case 95945896:
                    if (e.equals("dummy")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                this.c = new is0();
            } else if (c == 1) {
                this.c = new ls0();
            } else if (c == 2) {
                this.c = new p22(this.a.b(), this.a.a(), rg2.h(), this.a.m() ? this.a.i() : null);
            } else if (c != 3) {
                if (Build.VERSION.SDK_INT >= 21) {
                    this.c = new qr(this.a.i(), this.a.c(), this.a.d(), this.a.l());
                } else {
                    this.c = new is0();
                }
            } else {
                this.c = new qr(this.a.i(), ei0.a(), this.a.d(), this.a.l());
            }
        }
        return this.c;
    }

    public c c() {
        if (this.d == null) {
            try {
                this.d = (c) BufferMemoryChunkPool.class.getConstructor(r72.class, ys2.class, zs2.class).newInstance(this.a.i(), this.a.g(), this.a.h());
            } catch (ClassNotFoundException unused) {
                this.d = null;
            } catch (IllegalAccessException unused2) {
                this.d = null;
            } catch (InstantiationException unused3) {
                this.d = null;
            } catch (NoSuchMethodException unused4) {
                this.d = null;
            } catch (InvocationTargetException unused5) {
                this.d = null;
            }
        }
        return this.d;
    }

    public y61 d() {
        if (this.e == null) {
            this.e = new y61(this.a.i(), this.a.f());
        }
        return this.e;
    }

    public int e() {
        return this.a.f().e;
    }

    public final c f(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    return a();
                }
                throw new IllegalArgumentException("Invalid MemoryChunkType");
            }
            return c();
        }
        return g();
    }

    public c g() {
        if (this.f == null) {
            try {
                this.f = (c) NativeMemoryChunkPool.class.getConstructor(r72.class, ys2.class, zs2.class).newInstance(this.a.i(), this.a.g(), this.a.h());
            } catch (ClassNotFoundException e) {
                v11.i("PoolFactory", "", e);
                this.f = null;
            } catch (IllegalAccessException e2) {
                v11.i("PoolFactory", "", e2);
                this.f = null;
            } catch (InstantiationException e3) {
                v11.i("PoolFactory", "", e3);
                this.f = null;
            } catch (NoSuchMethodException e4) {
                v11.i("PoolFactory", "", e4);
                this.f = null;
            } catch (InvocationTargetException e5) {
                v11.i("PoolFactory", "", e5);
                this.f = null;
            }
        }
        return this.f;
    }

    public b h() {
        return i(!ld2.a() ? 1 : 0);
    }

    public b i(int i) {
        if (this.g == null) {
            c f = f(i);
            xt2.h(f, "failed to get pool for chunk type: " + i);
            this.g = new e(f, j());
        }
        return this.g;
    }

    public com.facebook.common.memory.c j() {
        if (this.h == null) {
            this.h = new com.facebook.common.memory.c(k());
        }
        return this.h;
    }

    public os k() {
        if (this.i == null) {
            this.i = new a(this.a.i(), this.a.j(), this.a.k());
        }
        return this.i;
    }
}
