package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Delay.kt */
/* renamed from: xl0  reason: default package */
/* loaded from: classes2.dex */
public final class xl0 {
    public static final Object a(long j, q70<? super te4> q70Var) {
        if (j <= 0) {
            return te4.a;
        }
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.A();
        if (j < Long.MAX_VALUE) {
            b(pvVar.getContext()).f(j, pvVar);
        }
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x == gs1.d() ? x : te4.a;
    }

    public static final wl0 b(CoroutineContext coroutineContext) {
        CoroutineContext.a aVar = coroutineContext.get(r70.d);
        wl0 wl0Var = aVar instanceof wl0 ? (wl0) aVar : null;
        return wl0Var == null ? ej0.a() : wl0Var;
    }
}
