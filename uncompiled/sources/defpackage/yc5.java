package defpackage;

import android.content.SharedPreferences;
import com.google.android.gms.flags.impl.b;

/* renamed from: yc5  reason: default package */
/* loaded from: classes.dex */
public final class yc5 extends g35<Integer> {
    public static Integer a(SharedPreferences sharedPreferences, String str, Integer num) {
        try {
            return (Integer) of5.a(new b(sharedPreferences, str, num));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() != 0) {
                "Flag value not available, returning default: ".concat(valueOf);
            }
            return num;
        }
    }
}
