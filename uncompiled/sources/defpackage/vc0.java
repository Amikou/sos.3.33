package defpackage;

import android.app.PendingIntent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

/* compiled from: CustomTabsSessionToken.java */
/* renamed from: vc0  reason: default package */
/* loaded from: classes.dex */
public class vc0 {
    public final tl1 a;
    public final PendingIntent b;

    /* compiled from: CustomTabsSessionToken.java */
    /* renamed from: vc0$a */
    /* loaded from: classes.dex */
    public class a extends qc0 {
        public a() {
        }

        @Override // defpackage.qc0
        public void a(String str, Bundle bundle) {
            try {
                vc0.this.a.I0(str, bundle);
            } catch (RemoteException unused) {
            }
        }

        @Override // defpackage.qc0
        public Bundle b(String str, Bundle bundle) {
            try {
                return vc0.this.a.L(str, bundle);
            } catch (RemoteException unused) {
                return null;
            }
        }

        @Override // defpackage.qc0
        public void c(Bundle bundle) {
            try {
                vc0.this.a.n1(bundle);
            } catch (RemoteException unused) {
            }
        }

        @Override // defpackage.qc0
        public void d(int i, Bundle bundle) {
            try {
                vc0.this.a.W0(i, bundle);
            } catch (RemoteException unused) {
            }
        }

        @Override // defpackage.qc0
        public void e(String str, Bundle bundle) {
            try {
                vc0.this.a.j1(str, bundle);
            } catch (RemoteException unused) {
            }
        }

        @Override // defpackage.qc0
        public void f(int i, Uri uri, boolean z, Bundle bundle) {
            try {
                vc0.this.a.r1(i, uri, z, bundle);
            } catch (RemoteException unused) {
            }
        }
    }

    public vc0(tl1 tl1Var, PendingIntent pendingIntent) {
        if (tl1Var == null && pendingIntent == null) {
            throw new IllegalStateException("CustomTabsSessionToken must have either a session id or a callback (or both).");
        }
        this.a = tl1Var;
        this.b = pendingIntent;
        if (tl1Var == null) {
            return;
        }
        new a();
    }

    public IBinder a() {
        tl1 tl1Var = this.a;
        if (tl1Var == null) {
            return null;
        }
        return tl1Var.asBinder();
    }

    public final IBinder b() {
        tl1 tl1Var = this.a;
        if (tl1Var != null) {
            return tl1Var.asBinder();
        }
        throw new IllegalStateException("CustomTabSessionToken must have valid binder or pending session");
    }

    public PendingIntent c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj instanceof vc0) {
            vc0 vc0Var = (vc0) obj;
            PendingIntent c = vc0Var.c();
            PendingIntent pendingIntent = this.b;
            if ((pendingIntent == null) != (c == null)) {
                return false;
            }
            if (pendingIntent != null) {
                return pendingIntent.equals(c);
            }
            return b().equals(vc0Var.b());
        }
        return false;
    }

    public int hashCode() {
        PendingIntent pendingIntent = this.b;
        return pendingIntent != null ? pendingIntent.hashCode() : b().hashCode();
    }
}
