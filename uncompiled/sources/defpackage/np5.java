package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import defpackage.rg1;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: np5  reason: default package */
/* loaded from: classes.dex */
public final class np5 implements ServiceConnection, tu5 {
    public final Map<ServiceConnection, ServiceConnection> a = new HashMap();
    public int f0 = 2;
    public boolean g0;
    public IBinder h0;
    public final rg1.a i0;
    public ComponentName j0;
    public final /* synthetic */ rk5 k0;

    public np5(rk5 rk5Var, rg1.a aVar) {
        this.k0 = rk5Var;
        this.i0 = aVar;
    }

    public final void a(ServiceConnection serviceConnection, ServiceConnection serviceConnection2, String str) {
        this.a.put(serviceConnection, serviceConnection2);
    }

    public final void b(ServiceConnection serviceConnection, String str) {
        this.a.remove(serviceConnection);
    }

    public final void c(String str) {
        v50 v50Var;
        Context context;
        Context context2;
        v50 v50Var2;
        Context context3;
        Handler handler;
        Handler handler2;
        long j;
        this.f0 = 3;
        v50Var = this.k0.g;
        context = this.k0.e;
        rg1.a aVar = this.i0;
        context2 = this.k0.e;
        boolean d = v50Var.d(context, str, aVar.a(context2), this, this.i0.e());
        this.g0 = d;
        if (d) {
            handler = this.k0.f;
            Message obtainMessage = handler.obtainMessage(1, this.i0);
            handler2 = this.k0.f;
            j = this.k0.i;
            handler2.sendMessageDelayed(obtainMessage, j);
            return;
        }
        this.f0 = 2;
        try {
            v50Var2 = this.k0.g;
            context3 = this.k0.e;
            v50Var2.c(context3, this);
        } catch (IllegalArgumentException unused) {
        }
    }

    public final boolean d() {
        return this.g0;
    }

    public final boolean e(ServiceConnection serviceConnection) {
        return this.a.containsKey(serviceConnection);
    }

    public final int f() {
        return this.f0;
    }

    public final void g(String str) {
        Handler handler;
        v50 v50Var;
        Context context;
        handler = this.k0.f;
        handler.removeMessages(1, this.i0);
        v50Var = this.k0.g;
        context = this.k0.e;
        v50Var.c(context, this);
        this.g0 = false;
        this.f0 = 2;
    }

    public final boolean h() {
        return this.a.isEmpty();
    }

    public final IBinder i() {
        return this.h0;
    }

    public final ComponentName j() {
        return this.j0;
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        HashMap hashMap;
        Handler handler;
        hashMap = this.k0.d;
        synchronized (hashMap) {
            handler = this.k0.f;
            handler.removeMessages(1, this.i0);
            this.h0 = iBinder;
            this.j0 = componentName;
            for (ServiceConnection serviceConnection : this.a.values()) {
                serviceConnection.onServiceConnected(componentName, iBinder);
            }
            this.f0 = 1;
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        HashMap hashMap;
        Handler handler;
        hashMap = this.k0.d;
        synchronized (hashMap) {
            handler = this.k0.f;
            handler.removeMessages(1, this.i0);
            this.h0 = null;
            this.j0 = componentName;
            for (ServiceConnection serviceConnection : this.a.values()) {
                serviceConnection.onServiceDisconnected(componentName);
            }
            this.f0 = 2;
        }
    }
}
