package defpackage;

import java.util.Random;

/* compiled from: TOTPData.java */
/* renamed from: e34  reason: default package */
/* loaded from: classes2.dex */
public final class e34 {
    public static final Random d;
    public final String a;
    public final String b;
    public final byte[] c;

    static {
        "0123456789ABCDEF".toCharArray();
        d = new Random();
    }

    public e34(String str, String str2, byte[] bArr) {
        this.a = str;
        this.b = str2;
        this.c = bArr;
    }

    public static byte[] a() {
        byte[] bArr = new byte[20];
        d.nextBytes(bArr);
        return bArr;
    }

    public String b() {
        return im.b(this.c);
    }

    public String c() {
        String b = b();
        String str = this.a;
        return String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s", str, this.b, b, str);
    }
}
