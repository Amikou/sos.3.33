package defpackage;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.i;

/* renamed from: kl4  reason: default package */
/* loaded from: classes2.dex */
public final class kl4 {
    public final byte[][] a;

    public kl4(jl4 jl4Var, byte[][] bArr) {
        Objects.requireNonNull(jl4Var, "params == null");
        Objects.requireNonNull(bArr, "publicKey == null");
        if (i.k(bArr)) {
            throw new NullPointerException("publicKey byte array == null");
        }
        if (bArr.length != jl4Var.c()) {
            throw new IllegalArgumentException("wrong publicKey size");
        }
        for (byte[] bArr2 : bArr) {
            if (bArr2.length != jl4Var.b()) {
                throw new IllegalArgumentException("wrong publicKey format");
            }
        }
        this.a = i.d(bArr);
    }

    public byte[][] a() {
        return i.d(this.a);
    }
}
