package defpackage;

/* renamed from: ts2  reason: default package */
/* loaded from: classes2.dex */
public class ts2 {
    public je1 a;
    public rs2 b;
    public rs2[] c;
    public rs2[] d;

    public ts2(je1 je1Var, rs2 rs2Var) {
        this.a = je1Var;
        this.b = rs2Var;
        b();
        a();
    }

    public static void d(rs2[] rs2VarArr, int i, int i2) {
        rs2 rs2Var = rs2VarArr[i];
        rs2VarArr[i] = rs2VarArr[i2];
        rs2VarArr[i2] = rs2Var;
    }

    public final void a() {
        int f;
        int g = this.b.g();
        rs2[] rs2VarArr = new rs2[g];
        int i = g - 1;
        for (int i2 = i; i2 >= 0; i2--) {
            rs2VarArr[i2] = new rs2(this.c[i2]);
        }
        this.d = new rs2[g];
        while (i >= 0) {
            this.d[i] = new rs2(this.a, i);
            i--;
        }
        for (int i3 = 0; i3 < g; i3++) {
            if (rs2VarArr[i3].f(i3) == 0) {
                int i4 = i3 + 1;
                boolean z = false;
                while (i4 < g) {
                    if (rs2VarArr[i4].f(i3) != 0) {
                        d(rs2VarArr, i3, i4);
                        d(this.d, i3, i4);
                        i4 = g;
                        z = true;
                    }
                    i4++;
                }
                if (!z) {
                    throw new ArithmeticException("Squaring matrix is not invertible.");
                }
            }
            int f2 = this.a.f(rs2VarArr[i3].f(i3));
            rs2VarArr[i3].m(f2);
            this.d[i3].m(f2);
            for (int i5 = 0; i5 < g; i5++) {
                if (i5 != i3 && (f = rs2VarArr[i5].f(i3)) != 0) {
                    rs2 n = rs2VarArr[i3].n(f);
                    rs2 n2 = this.d[i3].n(f);
                    rs2VarArr[i5].b(n);
                    this.d[i5].b(n2);
                }
            }
        }
    }

    public final void b() {
        int i;
        int g = this.b.g();
        this.c = new rs2[g];
        int i2 = 0;
        while (true) {
            i = g >> 1;
            if (i2 >= i) {
                break;
            }
            int i3 = i2 << 1;
            int[] iArr = new int[i3 + 1];
            iArr[i3] = 1;
            this.c[i2] = new rs2(this.a, iArr);
            i2++;
        }
        while (i < g) {
            int i4 = i << 1;
            int[] iArr2 = new int[i4 + 1];
            iArr2[i4] = 1;
            this.c[i] = new rs2(this.a, iArr2).k(this.b);
            i++;
        }
    }

    public rs2[] c() {
        return this.d;
    }
}
