package defpackage;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/* compiled from: LruArrayPool.java */
/* renamed from: n22  reason: default package */
/* loaded from: classes.dex */
public final class n22 implements sh {
    public final ui1<a, Object> a = new ui1<>();
    public final b b = new b();
    public final Map<Class<?>, NavigableMap<Integer, Integer>> c = new HashMap();
    public final Map<Class<?>, jh<?>> d = new HashMap();
    public final int e;
    public int f;

    /* compiled from: LruArrayPool.java */
    /* renamed from: n22$a */
    /* loaded from: classes.dex */
    public static final class a implements at2 {
        public final b a;
        public int b;
        public Class<?> c;

        public a(b bVar) {
            this.a = bVar;
        }

        @Override // defpackage.at2
        public void a() {
            this.a.c(this);
        }

        public void b(int i, Class<?> cls) {
            this.b = i;
            this.c = cls;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.b == aVar.b && this.c == aVar.c;
            }
            return false;
        }

        public int hashCode() {
            int i = this.b * 31;
            Class<?> cls = this.c;
            return i + (cls != null ? cls.hashCode() : 0);
        }

        public String toString() {
            return "Key{size=" + this.b + "array=" + this.c + '}';
        }
    }

    /* compiled from: LruArrayPool.java */
    /* renamed from: n22$b */
    /* loaded from: classes.dex */
    public static final class b extends kn<a> {
        @Override // defpackage.kn
        /* renamed from: d */
        public a a() {
            return new a(this);
        }

        public a e(int i, Class<?> cls) {
            a b = b();
            b.b(i, cls);
            return b;
        }
    }

    public n22(int i) {
        this.e = i;
    }

    @Override // defpackage.sh
    public synchronized void a(int i) {
        try {
            if (i >= 40) {
                b();
            } else if (i >= 20 || i == 15) {
                h(this.e / 2);
            }
        } catch (Throwable th) {
            throw th;
        }
    }

    @Override // defpackage.sh
    public synchronized void b() {
        h(0);
    }

    @Override // defpackage.sh
    public synchronized <T> void c(T t) {
        Class<?> cls = t.getClass();
        jh<T> j = j(cls);
        int c = j.c(t);
        int b2 = j.b() * c;
        if (o(b2)) {
            a e = this.b.e(c, cls);
            this.a.d(e, t);
            NavigableMap<Integer, Integer> m = m(cls);
            Integer num = (Integer) m.get(Integer.valueOf(e.b));
            Integer valueOf = Integer.valueOf(e.b);
            int i = 1;
            if (num != null) {
                i = 1 + num.intValue();
            }
            m.put(valueOf, Integer.valueOf(i));
            this.f += b2;
            g();
        }
    }

    @Override // defpackage.sh
    public synchronized <T> T d(int i, Class<T> cls) {
        return (T) l(this.b.e(i, cls), cls);
    }

    @Override // defpackage.sh
    public synchronized <T> T e(int i, Class<T> cls) {
        a e;
        Integer ceilingKey = m(cls).ceilingKey(Integer.valueOf(i));
        if (p(i, ceilingKey)) {
            e = this.b.e(ceilingKey.intValue(), cls);
        } else {
            e = this.b.e(i, cls);
        }
        return (T) l(e, cls);
    }

    public final void f(int i, Class<?> cls) {
        NavigableMap<Integer, Integer> m = m(cls);
        Integer num = (Integer) m.get(Integer.valueOf(i));
        if (num != null) {
            if (num.intValue() == 1) {
                m.remove(Integer.valueOf(i));
                return;
            } else {
                m.put(Integer.valueOf(i), Integer.valueOf(num.intValue() - 1));
                return;
            }
        }
        throw new NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
    }

    public final void g() {
        h(this.e);
    }

    public final void h(int i) {
        while (this.f > i) {
            Object f = this.a.f();
            wt2.d(f);
            jh i2 = i(f);
            this.f -= i2.c(f) * i2.b();
            f(i2.c(f), f.getClass());
            if (Log.isLoggable(i2.a(), 2)) {
                i2.a();
                StringBuilder sb = new StringBuilder();
                sb.append("evicted: ");
                sb.append(i2.c(f));
            }
        }
    }

    public final <T> jh<T> i(T t) {
        return j(t.getClass());
    }

    public final <T> jh<T> j(Class<T> cls) {
        vr1 vr1Var = (jh<T>) this.d.get(cls);
        if (vr1Var == null) {
            if (cls.equals(int[].class)) {
                vr1Var = new vr1();
            } else if (cls.equals(byte[].class)) {
                vr1Var = new ls();
            } else {
                throw new IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.d.put(cls, vr1Var);
        }
        return vr1Var;
    }

    public final <T> T k(a aVar) {
        return (T) this.a.a(aVar);
    }

    public final <T> T l(a aVar, Class<T> cls) {
        jh<T> j = j(cls);
        T t = (T) k(aVar);
        if (t != null) {
            this.f -= j.c(t) * j.b();
            f(j.c(t), cls);
        }
        if (t == null) {
            if (Log.isLoggable(j.a(), 2)) {
                j.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Allocated ");
                sb.append(aVar.b);
                sb.append(" bytes");
            }
            return j.newArray(aVar.b);
        }
        return t;
    }

    public final NavigableMap<Integer, Integer> m(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(cls);
        if (navigableMap == null) {
            TreeMap treeMap = new TreeMap();
            this.c.put(cls, treeMap);
            return treeMap;
        }
        return navigableMap;
    }

    public final boolean n() {
        int i = this.f;
        return i == 0 || this.e / i >= 2;
    }

    public final boolean o(int i) {
        return i <= this.e / 2;
    }

    public final boolean p(int i, Integer num) {
        return num != null && (n() || num.intValue() <= i * 8);
    }
}
