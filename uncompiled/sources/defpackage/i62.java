package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.DeniedByServerException;
import android.media.MediaCodec;
import android.media.MediaDrm;
import android.media.MediaDrmResetException;
import android.media.NotProvisionedException;
import android.media.metrics.LogSessionId;
import android.media.metrics.MediaMetricsManager;
import android.media.metrics.NetworkEvent;
import android.media.metrics.PlaybackErrorEvent;
import android.media.metrics.PlaybackMetrics;
import android.media.metrics.PlaybackSession;
import android.media.metrics.PlaybackStateEvent;
import android.media.metrics.TrackChangeEvent;
import android.os.SystemClock;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.util.Pair;
import androidx.media3.common.DrmInitData;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.j;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.datasource.FileDataSource;
import androidx.media3.datasource.HttpDataSource$HttpDataSourceException;
import androidx.media3.datasource.HttpDataSource$InvalidContentTypeException;
import androidx.media3.datasource.HttpDataSource$InvalidResponseCodeException;
import androidx.media3.datasource.UdpDataSource;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.audio.AudioSink;
import androidx.media3.exoplayer.drm.DefaultDrmSessionManager;
import androidx.media3.exoplayer.drm.DrmSession;
import androidx.media3.exoplayer.drm.UnsupportedDrmException;
import androidx.media3.exoplayer.mediacodec.MediaCodecDecoderException;
import androidx.media3.exoplayer.mediacodec.MediaCodecRenderer;
import androidx.media3.exoplayer.source.j;
import com.google.common.collect.ImmutableList;
import defpackage.mr2;
import defpackage.tb;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* compiled from: MediaMetricsListener.java */
/* renamed from: i62  reason: default package */
/* loaded from: classes.dex */
public final class i62 implements tb, mr2.a {
    public boolean A;
    public final Context a;
    public final mr2 b;
    public final PlaybackSession c;
    public String i;
    public PlaybackMetrics.Builder j;
    public int k;
    public PlaybackException n;
    public b o;
    public b p;
    public b q;
    public j r;
    public j s;
    public j t;
    public boolean u;
    public int v;
    public boolean w;
    public int x;
    public int y;
    public int z;
    public final u.c e = new u.c();
    public final u.b f = new u.b();
    public final HashMap<String, Long> h = new HashMap<>();
    public final HashMap<String, Long> g = new HashMap<>();
    public final long d = SystemClock.elapsedRealtime();
    public int l = 0;
    public int m = 0;

    /* compiled from: MediaMetricsListener.java */
    /* renamed from: i62$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final int b;

        public a(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    /* compiled from: MediaMetricsListener.java */
    /* renamed from: i62$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final j a;
        public final int b;
        public final String c;

        public b(j jVar, int i, String str) {
            this.a = jVar;
            this.b = i;
            this.c = str;
        }
    }

    public i62(Context context, PlaybackSession playbackSession) {
        this.a = context.getApplicationContext();
        this.c = playbackSession;
        jk0 jk0Var = new jk0();
        this.b = jk0Var;
        jk0Var.g(this);
    }

    public static i62 A0(Context context) {
        MediaMetricsManager mediaMetricsManager = (MediaMetricsManager) context.getSystemService("media_metrics");
        if (mediaMetricsManager == null) {
            return null;
        }
        return new i62(context, mediaMetricsManager.createPlaybackSession());
    }

    @SuppressLint({"SwitchIntDef"})
    public static int C0(int i) {
        switch (androidx.media3.common.util.b.R(i)) {
            case PlaybackException.ERROR_CODE_DRM_PROVISIONING_FAILED /* 6002 */:
                return 24;
            case PlaybackException.ERROR_CODE_DRM_CONTENT_ERROR /* 6003 */:
                return 28;
            case PlaybackException.ERROR_CODE_DRM_LICENSE_ACQUISITION_FAILED /* 6004 */:
                return 25;
            case PlaybackException.ERROR_CODE_DRM_DISALLOWED_OPERATION /* 6005 */:
                return 26;
            default:
                return 27;
        }
    }

    public static DrmInitData D0(ImmutableList<y.a> immutableList) {
        DrmInitData drmInitData;
        af4<y.a> it = immutableList.iterator();
        while (it.hasNext()) {
            y.a next = it.next();
            for (int i = 0; i < next.a; i++) {
                if (next.g(i) && (drmInitData = next.c(i).s0) != null) {
                    return drmInitData;
                }
            }
        }
        return null;
    }

    public static int E0(DrmInitData drmInitData) {
        for (int i = 0; i < drmInitData.h0; i++) {
            UUID uuid = drmInitData.e(i).f0;
            if (uuid.equals(ft.d)) {
                return 3;
            }
            if (uuid.equals(ft.e)) {
                return 2;
            }
            if (uuid.equals(ft.c)) {
                return 6;
            }
        }
        return 1;
    }

    public static a F0(PlaybackException playbackException, Context context, boolean z) {
        int i;
        boolean z2;
        if (playbackException.errorCode == 1001) {
            return new a(20, 0);
        }
        if (playbackException instanceof ExoPlaybackException) {
            ExoPlaybackException exoPlaybackException = (ExoPlaybackException) playbackException;
            z2 = exoPlaybackException.type == 1;
            i = exoPlaybackException.rendererFormatSupport;
        } else {
            i = 0;
            z2 = false;
        }
        Throwable th = (Throwable) ii.e(playbackException.getCause());
        if (!(th instanceof IOException)) {
            if (z2 && (i == 0 || i == 1)) {
                return new a(35, 0);
            }
            if (z2 && i == 3) {
                return new a(15, 0);
            }
            if (z2 && i == 2) {
                return new a(23, 0);
            }
            if (th instanceof MediaCodecRenderer.DecoderInitializationException) {
                return new a(13, androidx.media3.common.util.b.S(((MediaCodecRenderer.DecoderInitializationException) th).diagnosticInfo));
            }
            if (th instanceof MediaCodecDecoderException) {
                return new a(14, androidx.media3.common.util.b.S(((MediaCodecDecoderException) th).diagnosticInfo));
            }
            if (th instanceof OutOfMemoryError) {
                return new a(14, 0);
            }
            if (th instanceof AudioSink.InitializationException) {
                return new a(17, ((AudioSink.InitializationException) th).audioTrackState);
            }
            if (th instanceof AudioSink.WriteException) {
                return new a(18, ((AudioSink.WriteException) th).errorCode);
            }
            if (androidx.media3.common.util.b.a >= 16 && (th instanceof MediaCodec.CryptoException)) {
                int errorCode = ((MediaCodec.CryptoException) th).getErrorCode();
                return new a(C0(errorCode), errorCode);
            }
            return new a(22, 0);
        } else if (th instanceof HttpDataSource$InvalidResponseCodeException) {
            return new a(5, ((HttpDataSource$InvalidResponseCodeException) th).responseCode);
        } else {
            if (!(th instanceof HttpDataSource$InvalidContentTypeException) && !(th instanceof ParserException)) {
                if (!(th instanceof HttpDataSource$HttpDataSourceException) && !(th instanceof UdpDataSource.UdpDataSourceException)) {
                    if (playbackException.errorCode == 1002) {
                        return new a(21, 0);
                    }
                    if (th instanceof DrmSession.DrmSessionException) {
                        Throwable th2 = (Throwable) ii.e(th.getCause());
                        int i2 = androidx.media3.common.util.b.a;
                        if (i2 >= 21 && (th2 instanceof MediaDrm.MediaDrmStateException)) {
                            int S = androidx.media3.common.util.b.S(((MediaDrm.MediaDrmStateException) th2).getDiagnosticInfo());
                            return new a(C0(S), S);
                        } else if (i2 >= 23 && (th2 instanceof MediaDrmResetException)) {
                            return new a(27, 0);
                        } else {
                            if (i2 >= 18 && (th2 instanceof NotProvisionedException)) {
                                return new a(24, 0);
                            }
                            if (i2 >= 18 && (th2 instanceof DeniedByServerException)) {
                                return new a(29, 0);
                            }
                            if (th2 instanceof UnsupportedDrmException) {
                                return new a(23, 0);
                            }
                            if (th2 instanceof DefaultDrmSessionManager.MissingSchemeDataException) {
                                return new a(28, 0);
                            }
                            return new a(30, 0);
                        }
                    } else if ((th instanceof FileDataSource.FileDataSourceException) && (th.getCause() instanceof FileNotFoundException)) {
                        Throwable cause = ((Throwable) ii.e(th.getCause())).getCause();
                        if (androidx.media3.common.util.b.a >= 21 && (cause instanceof ErrnoException) && ((ErrnoException) cause).errno == OsConstants.EACCES) {
                            return new a(32, 0);
                        }
                        return new a(31, 0);
                    } else {
                        return new a(9, 0);
                    }
                } else if (ff2.d(context).f() == 1) {
                    return new a(3, 0);
                } else {
                    Throwable cause2 = th.getCause();
                    if (cause2 instanceof UnknownHostException) {
                        return new a(6, 0);
                    }
                    if (cause2 instanceof SocketTimeoutException) {
                        return new a(7, 0);
                    }
                    if ((th instanceof HttpDataSource$HttpDataSourceException) && ((HttpDataSource$HttpDataSourceException) th).type == 1) {
                        return new a(4, 0);
                    }
                    return new a(8, 0);
                }
            }
            return new a(z ? 10 : 11, 0);
        }
    }

    public static Pair<String, String> G0(String str) {
        String[] L0 = androidx.media3.common.util.b.L0(str, "-");
        return Pair.create(L0[0], L0.length >= 2 ? L0[1] : null);
    }

    public static int I0(Context context) {
        switch (ff2.d(context).f()) {
            case 0:
                return 0;
            case 1:
                return 9;
            case 2:
                return 2;
            case 3:
                return 4;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
            case 8:
            default:
                return 1;
            case 7:
                return 3;
            case 9:
                return 8;
            case 10:
                return 7;
        }
    }

    public static int J0(m mVar) {
        m.h hVar = mVar.f0;
        if (hVar == null) {
            return 0;
        }
        int m0 = androidx.media3.common.util.b.m0(hVar.a, hVar.b);
        if (m0 != 0) {
            if (m0 != 1) {
                return m0 != 2 ? 1 : 4;
            }
            return 5;
        }
        return 3;
    }

    public static int K0(int i) {
        if (i != 1) {
            if (i != 2) {
                return i != 3 ? 1 : 4;
            }
            return 3;
        }
        return 2;
    }

    @Override // defpackage.tb
    public /* synthetic */ void A(tb.a aVar, g62 g62Var) {
        sb.d0(this, aVar, g62Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void B(tb.a aVar, int i, int i2) {
        sb.Z(this, aVar, i, i2);
    }

    public final void B0() {
        PlaybackMetrics.Builder builder = this.j;
        if (builder != null && this.A) {
            builder.setAudioUnderrunCount(this.z);
            this.j.setVideoFramesDropped(this.x);
            this.j.setVideoFramesPlayed(this.y);
            Long l = this.g.get(this.i);
            this.j.setNetworkTransferDurationMillis(l == null ? 0L : l.longValue());
            Long l2 = this.h.get(this.i);
            this.j.setNetworkBytesRead(l2 == null ? 0L : l2.longValue());
            this.j.setStreamSource((l2 == null || l2.longValue() <= 0) ? 0 : 1);
            this.c.reportPlaybackMetrics(this.j.build());
        }
        this.j = null;
        this.i = null;
        this.z = 0;
        this.x = 0;
        this.y = 0;
        this.r = null;
        this.s = null;
        this.t = null;
        this.A = false;
    }

    @Override // defpackage.tb
    public /* synthetic */ void C(tb.a aVar, String str, long j, long j2) {
        sb.c(this, aVar, str, j, j2);
    }

    @Override // defpackage.tb
    public /* synthetic */ void D(tb.a aVar, int i) {
        sb.U(this, aVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void E(tb.a aVar, m mVar, int i) {
        sb.I(this, aVar, mVar, i);
    }

    @Override // defpackage.mr2.a
    public void F(tb.a aVar, String str) {
        j.b bVar = aVar.d;
        if (bVar == null || !bVar.b()) {
            B0();
            this.i = str;
            this.j = new PlaybackMetrics.Builder().setPlayerName("AndroidXMedia3").setPlayerVersion("1.0.0-beta02");
            T0(aVar.b, aVar.d);
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void G(tb.a aVar, x xVar) {
        sb.b0(this, aVar, xVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void H(tb.a aVar, y yVar) {
        sb.c0(this, aVar, yVar);
    }

    public LogSessionId H0() {
        return this.c.getSessionId();
    }

    @Override // defpackage.tb
    public /* synthetic */ void I(tb.a aVar, Exception exc) {
        sb.e0(this, aVar, exc);
    }

    @Override // defpackage.tb
    public /* synthetic */ void J(tb.a aVar, n nVar) {
        sb.J(this, aVar, nVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void K(tb.a aVar, int i) {
        sb.a0(this, aVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void L(tb.a aVar, String str, long j, long j2) {
        sb.g0(this, aVar, str, j, j2);
    }

    public final void L0(tb.b bVar) {
        for (int i = 0; i < bVar.d(); i++) {
            int b2 = bVar.b(i);
            tb.a c = bVar.c(b2);
            if (b2 == 0) {
                this.b.e(c);
            } else if (b2 == 11) {
                this.b.c(c, this.k);
            } else {
                this.b.f(c);
            }
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void M(tb.a aVar, int i, int i2, int i3, float f) {
        sb.m0(this, aVar, i, i2, i3, f);
    }

    public final void M0(long j) {
        int I0 = I0(this.a);
        if (I0 != this.m) {
            this.m = I0;
            this.c.reportNetworkEvent(new NetworkEvent.Builder().setNetworkType(I0).setTimeSinceCreatedMillis(j - this.d).build());
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void N(tb.a aVar, hf0 hf0Var) {
        sb.f(this, aVar, hf0Var);
    }

    public final void N0(long j) {
        PlaybackException playbackException = this.n;
        if (playbackException == null) {
            return;
        }
        a F0 = F0(playbackException, this.a, this.v == 4);
        this.c.reportPlaybackErrorEvent(new PlaybackErrorEvent.Builder().setTimeSinceCreatedMillis(j - this.d).setErrorCode(F0.a).setSubErrorCode(F0.b).setException(playbackException).build());
        this.A = true;
        this.n = null;
    }

    @Override // defpackage.tb
    public /* synthetic */ void O(tb.a aVar, boolean z, int i) {
        sb.R(this, aVar, z, i);
    }

    public final void O0(q qVar, tb.b bVar, long j) {
        if (qVar.B() != 2) {
            this.u = false;
        }
        if (qVar.v() == null) {
            this.w = false;
        } else if (bVar.a(10)) {
            this.w = true;
        }
        int W0 = W0(qVar);
        if (this.l != W0) {
            this.l = W0;
            this.A = true;
            this.c.reportPlaybackStateEvent(new PlaybackStateEvent.Builder().setState(this.l).setTimeSinceCreatedMillis(j - this.d).build());
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void P(tb.a aVar) {
        sb.V(this, aVar);
    }

    public final void P0(q qVar, tb.b bVar, long j) {
        if (bVar.a(2)) {
            y C = qVar.C();
            boolean c = C.c(2);
            boolean c2 = C.c(1);
            boolean c3 = C.c(3);
            if (c || c2 || c3) {
                if (!c) {
                    U0(j, null, 0);
                }
                if (!c2) {
                    Q0(j, null, 0);
                }
                if (!c3) {
                    S0(j, null, 0);
                }
            }
        }
        if (z0(this.o)) {
            b bVar2 = this.o;
            androidx.media3.common.j jVar = bVar2.a;
            if (jVar.v0 != -1) {
                U0(j, jVar, bVar2.b);
                this.o = null;
            }
        }
        if (z0(this.p)) {
            b bVar3 = this.p;
            Q0(j, bVar3.a, bVar3.b);
            this.p = null;
        }
        if (z0(this.q)) {
            b bVar4 = this.q;
            S0(j, bVar4.a, bVar4.b);
            this.q = null;
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void Q(tb.a aVar, int i, String str, long j) {
        sb.q(this, aVar, i, str, j);
    }

    public final void Q0(long j, androidx.media3.common.j jVar, int i) {
        if (androidx.media3.common.util.b.c(this.s, jVar)) {
            return;
        }
        if (this.s == null && i == 0) {
            i = 1;
        }
        this.s = jVar;
        V0(0, j, jVar, i);
    }

    @Override // defpackage.tb
    public void R(q qVar, tb.b bVar) {
        if (bVar.d() == 0) {
            return;
        }
        L0(bVar);
        long elapsedRealtime = SystemClock.elapsedRealtime();
        R0(qVar, bVar);
        N0(elapsedRealtime);
        P0(qVar, bVar, elapsedRealtime);
        M0(elapsedRealtime);
        O0(qVar, bVar, elapsedRealtime);
        if (bVar.a(1028)) {
            this.b.d(bVar.c(1028));
        }
    }

    public final void R0(q qVar, tb.b bVar) {
        DrmInitData D0;
        if (bVar.a(0)) {
            tb.a c = bVar.c(0);
            if (this.j != null) {
                T0(c.b, c.d);
            }
        }
        if (bVar.a(2) && this.j != null && (D0 = D0(qVar.C().a())) != null) {
            ((PlaybackMetrics.Builder) androidx.media3.common.util.b.j(this.j)).setDrmType(E0(D0));
        }
        if (bVar.a(1011)) {
            this.z++;
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void S(tb.a aVar, Object obj, long j) {
        sb.T(this, aVar, obj, j);
    }

    public final void S0(long j, androidx.media3.common.j jVar, int i) {
        if (androidx.media3.common.util.b.c(this.t, jVar)) {
            return;
        }
        if (this.t == null && i == 0) {
            i = 1;
        }
        this.t = jVar;
        V0(2, j, jVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void T(tb.a aVar, String str) {
        sb.h0(this, aVar, str);
    }

    public final void T0(u uVar, j.b bVar) {
        int b2;
        PlaybackMetrics.Builder builder = this.j;
        if (bVar == null || (b2 = uVar.b(bVar.a)) == -1) {
            return;
        }
        uVar.f(b2, this.f);
        uVar.n(this.f.g0, this.e);
        builder.setStreamType(J0(this.e.g0));
        u.c cVar = this.e;
        if (cVar.r0 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && !cVar.p0 && !cVar.m0 && !cVar.i()) {
            builder.setMediaDurationMillis(this.e.g());
        }
        builder.setPlaybackType(this.e.i() ? 2 : 1);
        this.A = true;
    }

    @Override // defpackage.tb
    public /* synthetic */ void U(tb.a aVar, String str, long j) {
        sb.b(this, aVar, str, j);
    }

    public final void U0(long j, androidx.media3.common.j jVar, int i) {
        if (androidx.media3.common.util.b.c(this.r, jVar)) {
            return;
        }
        if (this.r == null && i == 0) {
            i = 1;
        }
        this.r = jVar;
        V0(1, j, jVar, i);
    }

    @Override // defpackage.mr2.a
    public void V(tb.a aVar, String str, String str2) {
    }

    public final void V0(int i, long j, androidx.media3.common.j jVar, int i2) {
        TrackChangeEvent.Builder timeSinceCreatedMillis = new TrackChangeEvent.Builder(i).setTimeSinceCreatedMillis(j - this.d);
        if (jVar != null) {
            timeSinceCreatedMillis.setTrackState(1);
            timeSinceCreatedMillis.setTrackChangeReason(K0(i2));
            String str = jVar.o0;
            if (str != null) {
                timeSinceCreatedMillis.setContainerMimeType(str);
            }
            String str2 = jVar.p0;
            if (str2 != null) {
                timeSinceCreatedMillis.setSampleMimeType(str2);
            }
            String str3 = jVar.m0;
            if (str3 != null) {
                timeSinceCreatedMillis.setCodecName(str3);
            }
            int i3 = jVar.l0;
            if (i3 != -1) {
                timeSinceCreatedMillis.setBitrate(i3);
            }
            int i4 = jVar.u0;
            if (i4 != -1) {
                timeSinceCreatedMillis.setWidth(i4);
            }
            int i5 = jVar.v0;
            if (i5 != -1) {
                timeSinceCreatedMillis.setHeight(i5);
            }
            int i6 = jVar.C0;
            if (i6 != -1) {
                timeSinceCreatedMillis.setChannelCount(i6);
            }
            int i7 = jVar.D0;
            if (i7 != -1) {
                timeSinceCreatedMillis.setAudioSampleRate(i7);
            }
            String str4 = jVar.g0;
            if (str4 != null) {
                Pair<String, String> G0 = G0(str4);
                timeSinceCreatedMillis.setLanguage((String) G0.first);
                Object obj = G0.second;
                if (obj != null) {
                    timeSinceCreatedMillis.setLanguageRegion((String) obj);
                }
            }
            float f = jVar.w0;
            if (f != -1.0f) {
                timeSinceCreatedMillis.setVideoFrameRate(f);
            }
        } else {
            timeSinceCreatedMillis.setTrackState(0);
        }
        this.A = true;
        this.c.reportTrackChangeEvent(timeSinceCreatedMillis.build());
    }

    @Override // defpackage.tb
    public /* synthetic */ void W(tb.a aVar, hf0 hf0Var) {
        sb.i0(this, aVar, hf0Var);
    }

    public final int W0(q qVar) {
        int B = qVar.B();
        if (this.u) {
            return 5;
        }
        if (this.w) {
            return 13;
        }
        if (B == 4) {
            return 11;
        }
        if (B == 2) {
            int i = this.l;
            if (i == 0 || i == 2) {
                return 2;
            }
            if (qVar.k()) {
                return qVar.P() != 0 ? 10 : 6;
            }
            return 7;
        } else if (B == 3) {
            if (qVar.k()) {
                return qVar.P() != 0 ? 9 : 3;
            }
            return 4;
        } else if (B != 1 || this.l == 0) {
            return this.l;
        } else {
            return 12;
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void X(tb.a aVar, int i) {
        sb.y(this, aVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void Y(tb.a aVar, androidx.media3.common.j jVar) {
        sb.k0(this, aVar, jVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void Z(tb.a aVar, u02 u02Var, g62 g62Var) {
        sb.F(this, aVar, u02Var, g62Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void a(tb.a aVar, Metadata metadata) {
        sb.K(this, aVar, metadata);
    }

    @Override // defpackage.tb
    public /* synthetic */ void a0(tb.a aVar, int i, androidx.media3.common.j jVar) {
        sb.r(this, aVar, i, jVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void b(tb.a aVar, boolean z) {
        sb.X(this, aVar, z);
    }

    @Override // defpackage.tb
    public void b0(tb.a aVar, PlaybackException playbackException) {
        this.n = playbackException;
    }

    @Override // defpackage.tb
    public /* synthetic */ void c(tb.a aVar) {
        sb.u(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void c0(tb.a aVar, int i, long j, long j2) {
        sb.k(this, aVar, i, j, j2);
    }

    @Override // defpackage.tb
    public /* synthetic */ void d(tb.a aVar, boolean z) {
        sb.D(this, aVar, z);
    }

    @Override // defpackage.tb
    public void d0(tb.a aVar, int i, long j, long j2) {
        j.b bVar = aVar.d;
        if (bVar != null) {
            String b2 = this.b.b(aVar.b, (j.b) ii.e(bVar));
            Long l = this.h.get(b2);
            Long l2 = this.g.get(b2);
            this.h.put(b2, Long.valueOf((l == null ? 0L : l.longValue()) + j));
            this.g.put(b2, Long.valueOf((l2 != null ? l2.longValue() : 0L) + i));
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void e(tb.a aVar, Exception exc) {
        sb.a(this, aVar, exc);
    }

    @Override // defpackage.tb
    public void e0(tb.a aVar, hf0 hf0Var) {
        this.x += hf0Var.g;
        this.y += hf0Var.e;
    }

    @Override // defpackage.tb
    public /* synthetic */ void f(tb.a aVar, q.b bVar) {
        sb.l(this, aVar, bVar);
    }

    @Override // defpackage.tb
    public void f0(tb.a aVar, g62 g62Var) {
        if (aVar.d == null) {
            return;
        }
        b bVar = new b((androidx.media3.common.j) ii.e(g62Var.c), g62Var.d, this.b.b(aVar.b, (j.b) ii.e(aVar.d)));
        int i = g62Var.b;
        if (i != 0) {
            if (i == 1) {
                this.p = bVar;
                return;
            } else if (i != 2) {
                if (i != 3) {
                    return;
                }
                this.q = bVar;
                return;
            }
        }
        this.o = bVar;
    }

    @Override // defpackage.mr2.a
    public void g(tb.a aVar, String str) {
    }

    @Override // defpackage.tb
    public /* synthetic */ void g0(tb.a aVar, int i, boolean z) {
        sb.t(this, aVar, i, z);
    }

    @Override // defpackage.tb
    public /* synthetic */ void h(tb.a aVar, Exception exc) {
        sb.j(this, aVar, exc);
    }

    @Override // defpackage.tb
    public /* synthetic */ void h0(tb.a aVar, PlaybackException playbackException) {
        sb.P(this, aVar, playbackException);
    }

    @Override // defpackage.tb
    public /* synthetic */ void i(tb.a aVar, int i, hf0 hf0Var) {
        sb.o(this, aVar, i, hf0Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void i0(tb.a aVar, int i, hf0 hf0Var) {
        sb.p(this, aVar, i, hf0Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void j(tb.a aVar, List list) {
        sb.n(this, aVar, list);
    }

    @Override // defpackage.tb
    public /* synthetic */ void j0(tb.a aVar) {
        sb.A(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void k(tb.a aVar) {
        sb.v(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void k0(tb.a aVar, nb0 nb0Var) {
        sb.m(this, aVar, nb0Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void l(tb.a aVar, String str) {
        sb.d(this, aVar, str);
    }

    @Override // defpackage.tb
    public /* synthetic */ void l0(tb.a aVar, int i, long j) {
        sb.B(this, aVar, i, j);
    }

    @Override // defpackage.tb
    public /* synthetic */ void m(tb.a aVar, boolean z) {
        sb.Y(this, aVar, z);
    }

    @Override // defpackage.tb
    public /* synthetic */ void m0(tb.a aVar, u02 u02Var, g62 g62Var) {
        sb.E(this, aVar, u02Var, g62Var);
    }

    @Override // defpackage.mr2.a
    public void n(tb.a aVar, String str, boolean z) {
        j.b bVar = aVar.d;
        if ((bVar == null || !bVar.b()) && str.equals(this.i)) {
            B0();
        }
        this.g.remove(str);
        this.h.remove(str);
    }

    @Override // defpackage.tb
    public /* synthetic */ void n0(tb.a aVar, boolean z) {
        sb.C(this, aVar, z);
    }

    @Override // defpackage.tb
    public void o(tb.a aVar, u02 u02Var, g62 g62Var, IOException iOException, boolean z) {
        this.v = g62Var.a;
    }

    @Override // defpackage.tb
    public /* synthetic */ void o0(tb.a aVar, boolean z, int i) {
        sb.L(this, aVar, z, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void p(tb.a aVar, long j, int i) {
        sb.j0(this, aVar, j, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void p0(tb.a aVar, androidx.media3.common.j jVar) {
        sb.g(this, aVar, jVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void q(tb.a aVar, int i) {
        sb.N(this, aVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void q0(tb.a aVar, hf0 hf0Var) {
        sb.e(this, aVar, hf0Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void r(tb.a aVar, p pVar) {
        sb.M(this, aVar, pVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void r0(tb.a aVar, int i) {
        sb.S(this, aVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void s(tb.a aVar, androidx.media3.common.j jVar, jf0 jf0Var) {
        sb.h(this, aVar, jVar, jf0Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void s0(tb.a aVar, Exception exc) {
        sb.z(this, aVar, exc);
    }

    @Override // defpackage.tb
    public /* synthetic */ void t(tb.a aVar) {
        sb.x(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void t0(tb.a aVar, boolean z) {
        sb.H(this, aVar, z);
    }

    @Override // defpackage.tb
    public /* synthetic */ void u(tb.a aVar, long j) {
        sb.i(this, aVar, j);
    }

    @Override // defpackage.tb
    public void u0(tb.a aVar, q.e eVar, q.e eVar2, int i) {
        if (i == 1) {
            this.u = true;
        }
        this.k = i;
    }

    @Override // defpackage.tb
    public /* synthetic */ void v(tb.a aVar) {
        sb.W(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void v0(tb.a aVar, String str, long j) {
        sb.f0(this, aVar, str, j);
    }

    @Override // defpackage.tb
    public /* synthetic */ void w(tb.a aVar, u02 u02Var, g62 g62Var) {
        sb.G(this, aVar, u02Var, g62Var);
    }

    @Override // defpackage.tb
    public /* synthetic */ void w0(tb.a aVar, int i) {
        sb.O(this, aVar, i);
    }

    @Override // defpackage.tb
    public /* synthetic */ void x(tb.a aVar) {
        sb.Q(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void x0(tb.a aVar) {
        sb.w(this, aVar);
    }

    @Override // defpackage.tb
    public /* synthetic */ void y(tb.a aVar, androidx.media3.common.j jVar, jf0 jf0Var) {
        sb.l0(this, aVar, jVar, jf0Var);
    }

    @Override // defpackage.tb
    public void y0(tb.a aVar, z zVar) {
        b bVar = this.o;
        if (bVar != null) {
            androidx.media3.common.j jVar = bVar.a;
            if (jVar.v0 == -1) {
                this.o = new b(jVar.b().j0(zVar.a).Q(zVar.f0).E(), bVar.b, bVar.c);
            }
        }
    }

    @Override // defpackage.tb
    public /* synthetic */ void z(tb.a aVar, h hVar) {
        sb.s(this, aVar, hVar);
    }

    public final boolean z0(b bVar) {
        return bVar != null && bVar.c.equals(this.b.a());
    }
}
