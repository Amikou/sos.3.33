package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.z21;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.HeaderItemHistory;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.fiat.ResultItemFiat;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: FiatListAdapter.kt */
/* renamed from: z21  reason: default package */
/* loaded from: classes2.dex */
public abstract class z21 extends RecyclerView.Adapter<RecyclerView.a0> {
    public final Context a;
    public final a b;
    public final List<HistoryListItem> c;

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: z21$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(Fiat fiat);
    }

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: z21$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final rs1 a;
        public final /* synthetic */ z21 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(z21 z21Var, rs1 rs1Var) {
            super(rs1Var.b());
            fs1.f(z21Var, "this$0");
            fs1.f(rs1Var, "binding");
            this.b = z21Var;
            this.a = rs1Var;
        }

        public static final void c(z21 z21Var, RoomFiat roomFiat, View view) {
            fs1.f(z21Var, "this$0");
            z21Var.b.a(new Fiat(roomFiat.getSymbol(), roomFiat.getName(), roomFiat.getRate()));
        }

        public final void b(HistoryListItem historyListItem) {
            fs1.f(historyListItem, "item");
            final RoomFiat result = ((ResultItemFiat) historyListItem).getResult();
            rs1 rs1Var = this.a;
            final z21 z21Var = this.b;
            if (result != null) {
                TextView textView = rs1Var.c;
                textView.setText(result.getSymbol() + " - " + ((Object) result.getName()));
                rs1Var.b.setChecked(fs1.b(z21Var.c().getSymbol(), result.getSymbol()));
                if (fs1.b(z21Var.c().getSymbol(), result.getSymbol())) {
                    rs1Var.c.setTextColor(m70.d(z21Var.b(), R.color.akt_register_guide_title));
                } else {
                    rs1Var.c.setTextColor(m70.d(z21Var.b(), R.color.white));
                }
            }
            if (result != null) {
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: y21
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        z21.b.c(z21.this, result, view);
                    }
                });
            }
        }
    }

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: z21$c */
    /* loaded from: classes2.dex */
    public static final class c extends RecyclerView.a0 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(View view) {
            super(view);
            fs1.f(view, "itemView");
        }
    }

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: z21$d */
    /* loaded from: classes2.dex */
    public static final class d extends RecyclerView.a0 {
        public final ss1 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(ss1 ss1Var) {
            super(ss1Var.b());
            fs1.f(ss1Var, "binding");
            this.a = ss1Var;
        }

        public final void a(HistoryListItem historyListItem) {
            fs1.f(historyListItem, "item");
            this.a.b.setText(((HeaderItemHistory) historyListItem).getTitle());
        }
    }

    public z21(Context context, a aVar) {
        fs1.f(context, "context");
        fs1.f(aVar, "onDefaultCurrencySelectedListener");
        this.a = context;
        this.b = aVar;
        this.c = new ArrayList();
    }

    public final Context b() {
        return this.a;
    }

    public abstract Fiat c();

    public final void d(List<HistoryListItem> list) {
        fs1.f(list, "items");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.c.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.c.get(i).getType();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        fs1.f(a0Var, "holder");
        HistoryListItem historyListItem = this.c.get(i);
        if (getItemViewType(i) == 0) {
            if (a0Var instanceof d) {
                ((d) a0Var).a(historyListItem);
            }
        } else if (getItemViewType(i) != 1 && (a0Var instanceof b)) {
            ((b) a0Var).b(historyListItem);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        if (i == 0) {
            ss1 a2 = ss1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_akt_fiat_list_header, viewGroup, false));
            fs1.e(a2, "bind(\n                  … false)\n                )");
            return new d(a2);
        } else if (i != 1) {
            rs1 a3 = rs1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_akt_fiat, viewGroup, false));
            fs1.e(a3, "bind(\n                  … false)\n                )");
            return new b(this, a3);
        } else {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_akt_fiat_list_footer, viewGroup, false);
            fs1.e(inflate, "from(parent.context)\n   …st_footer, parent, false)");
            return new c(inflate);
        }
    }
}
