package defpackage;

import com.google.gson.Gson;
import net.safemoon.androidwallet.model.Error;
import org.json.JSONObject;
import retrofit2.n;

/* compiled from: NetUtils.java */
/* renamed from: xe2  reason: default package */
/* loaded from: classes2.dex */
public class xe2 {
    public static Error a(n<?> nVar) {
        try {
            return (Error) new Gson().fromJson(new JSONObject(q44.c(nVar.d().charStream())).toString(), (Class<Object>) Error.class);
        } catch (Exception e) {
            Error error = new Error();
            error.code = "400";
            error.message = e.getMessage();
            return error;
        }
    }
}
