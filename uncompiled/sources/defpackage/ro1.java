package defpackage;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.producers.d;
import com.facebook.imagepipeline.producers.n;
import defpackage.i90;
import defpackage.l72;
import defpackage.oo4;
import defpackage.to1;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ImagePipelineConfig.java */
/* renamed from: ro1  reason: default package */
/* loaded from: classes.dex */
public class ro1 implements so1 {
    public static c I = new c(null);
    public final to1 A;
    public final boolean B;
    public final av C;
    public final b00 D;
    public final l72<wt, com.facebook.imagepipeline.image.a> E;
    public final l72<wt, PooledByteBuffer> F;
    public final xl3 G;
    public final cq H;
    public final fw3<m72> a;
    public final l72.a b;
    public final i90.b<wt> c;
    public final xt d;
    public final Context e;
    public final boolean f;
    public final l31 g;
    public final fw3<m72> h;
    public final wy0 i;
    public final qn1 j;
    public final tn1 k;
    public final bp1 l;
    public final Integer m;
    public final fw3<Boolean> n;
    public final ap0 o;
    public final r72 p;
    public final int q;
    public final n r;
    public final int s;
    public final xs2 t;
    public final qv2 u;
    public final Set<h73> v;
    public final Set<i73> w;
    public final boolean x;
    public final ap0 y;
    public final un1 z;

    /* compiled from: ImagePipelineConfig.java */
    /* renamed from: ro1$a */
    /* loaded from: classes.dex */
    public class a implements fw3<Boolean> {
        public a(ro1 ro1Var) {
        }

        @Override // defpackage.fw3
        /* renamed from: a */
        public Boolean get() {
            return Boolean.TRUE;
        }
    }

    /* compiled from: ImagePipelineConfig.java */
    /* renamed from: ro1$b */
    /* loaded from: classes.dex */
    public static class b {
        public un1 A;
        public int B;
        public final to1.b C;
        public boolean D;
        public av E;
        public b00 F;
        public l72<wt, com.facebook.imagepipeline.image.a> G;
        public l72<wt, PooledByteBuffer> H;
        public xl3 I;
        public cq J;
        public Bitmap.Config a;
        public fw3<m72> b;
        public i90.b<wt> c;
        public l72.a d;
        public xt e;
        public final Context f;
        public boolean g;
        public fw3<m72> h;
        public wy0 i;
        public qn1 j;
        public tn1 k;
        public bp1 l;
        public Integer m;
        public fw3<Boolean> n;
        public ap0 o;
        public r72 p;
        public Integer q;
        public n r;
        public br2 s;
        public xs2 t;
        public qv2 u;
        public Set<h73> v;
        public Set<i73> w;
        public boolean x;
        public ap0 y;
        public l31 z;

        public /* synthetic */ b(Context context, a aVar) {
            this(context);
        }

        public ro1 K() {
            return new ro1(this, null);
        }

        public b(Context context) {
            this.g = false;
            this.m = null;
            this.q = null;
            this.x = true;
            this.B = -1;
            this.C = new to1.b(this);
            this.D = true;
            this.F = new mg2();
            this.f = (Context) xt2.g(context);
        }
    }

    /* compiled from: ImagePipelineConfig.java */
    /* renamed from: ro1$c */
    /* loaded from: classes.dex */
    public static class c {
        public boolean a;

        public /* synthetic */ c(a aVar) {
            this();
        }

        public boolean a() {
            return this.a;
        }

        public c() {
            this.a = false;
        }
    }

    public /* synthetic */ ro1(b bVar, a aVar) {
        this(bVar);
    }

    public static c F() {
        return I;
    }

    public static ap0 G(Context context) {
        try {
            if (nc1.d()) {
                nc1.a("DiskCacheConfig.getDefaultMainDiskCacheConfig");
            }
            return ap0.m(context).n();
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public static bp1 H(b bVar) {
        if (bVar.l == null || bVar.m == null) {
            if (bVar.l != null) {
                return bVar.l;
            }
            return null;
        }
        throw new IllegalStateException("You can't define a custom ImageTranscoderFactory and provide an ImageTranscoderType");
    }

    public static int I(b bVar, to1 to1Var) {
        if (bVar.q != null) {
            return bVar.q.intValue();
        }
        if (to1Var.g() != 2 || Build.VERSION.SDK_INT < 27) {
            if (to1Var.g() == 1) {
                return 1;
            }
            to1Var.g();
            return 0;
        }
        return 2;
    }

    public static b J(Context context) {
        return new b(context, null);
    }

    public static void K(oo4 oo4Var, to1 to1Var, tp tpVar) {
        po4.c = oo4Var;
        oo4.a n = to1Var.n();
        if (n != null) {
            oo4Var.c(n);
        }
        if (tpVar != null) {
            oo4Var.b(tpVar);
        }
    }

    @Override // defpackage.so1
    public fw3<m72> A() {
        return this.a;
    }

    @Override // defpackage.so1
    public tn1 B() {
        return this.k;
    }

    @Override // defpackage.so1
    public to1 C() {
        return this.A;
    }

    @Override // defpackage.so1
    public fw3<m72> D() {
        return this.h;
    }

    @Override // defpackage.so1
    public wy0 E() {
        return this.i;
    }

    @Override // defpackage.so1
    public xs2 a() {
        return this.t;
    }

    @Override // defpackage.so1
    public Set<i73> b() {
        return Collections.unmodifiableSet(this.w);
    }

    @Override // defpackage.so1
    public int c() {
        return this.q;
    }

    @Override // defpackage.so1
    public fw3<Boolean> d() {
        return this.n;
    }

    @Override // defpackage.so1
    public l31 e() {
        return this.g;
    }

    @Override // defpackage.so1
    public b00 f() {
        return this.D;
    }

    @Override // defpackage.so1
    public cq g() {
        return this.H;
    }

    @Override // defpackage.so1
    public Context getContext() {
        return this.e;
    }

    @Override // defpackage.so1
    public n h() {
        return this.r;
    }

    @Override // defpackage.so1
    public l72<wt, PooledByteBuffer> i() {
        return this.F;
    }

    @Override // defpackage.so1
    public ap0 j() {
        return this.o;
    }

    @Override // defpackage.so1
    public Set<h73> k() {
        return Collections.unmodifiableSet(this.v);
    }

    @Override // defpackage.so1
    public xt l() {
        return this.d;
    }

    @Override // defpackage.so1
    public boolean m() {
        return this.x;
    }

    @Override // defpackage.so1
    public l72.a n() {
        return this.b;
    }

    @Override // defpackage.so1
    public qv2 o() {
        return this.u;
    }

    @Override // defpackage.so1
    public ap0 p() {
        return this.y;
    }

    @Override // defpackage.so1
    public qn1 q() {
        return this.j;
    }

    @Override // defpackage.so1
    public i90.b<wt> r() {
        return this.c;
    }

    @Override // defpackage.so1
    public boolean s() {
        return this.f;
    }

    @Override // defpackage.so1
    public xl3 t() {
        return this.G;
    }

    @Override // defpackage.so1
    public Integer u() {
        return this.m;
    }

    @Override // defpackage.so1
    public bp1 v() {
        return this.l;
    }

    @Override // defpackage.so1
    public r72 w() {
        return this.p;
    }

    @Override // defpackage.so1
    public un1 x() {
        return this.z;
    }

    @Override // defpackage.so1
    public boolean y() {
        return this.B;
    }

    @Override // defpackage.so1
    public av z() {
        return this.C;
    }

    public ro1(b bVar) {
        fw3<m72> fw3Var;
        xt xtVar;
        qn1 qn1Var;
        r72 r72Var;
        xs2 xs2Var;
        oo4 i;
        if (nc1.d()) {
            nc1.a("ImagePipelineConfig()");
        }
        to1 t = bVar.C.t();
        this.A = t;
        if (bVar.b != null) {
            fw3Var = bVar.b;
        } else {
            fw3Var = new di0((ActivityManager) xt2.g(bVar.f.getSystemService("activity")));
        }
        this.a = fw3Var;
        this.b = bVar.d == null ? new hq() : bVar.d;
        this.c = bVar.c;
        if (bVar.a == null) {
            Bitmap.Config config = Bitmap.Config.ARGB_8888;
        } else {
            Bitmap.Config unused = bVar.a;
        }
        if (bVar.e != null) {
            xtVar = bVar.e;
        } else {
            xtVar = ji0.f();
        }
        this.d = xtVar;
        this.e = (Context) xt2.g(bVar.f);
        this.g = bVar.z == null ? new ip0(new ts0()) : bVar.z;
        this.f = bVar.g;
        this.h = bVar.h == null ? new dj0() : bVar.h;
        if (bVar.j != null) {
            qn1Var = bVar.j;
        } else {
            qn1Var = pg2.o();
        }
        this.j = qn1Var;
        this.k = bVar.k;
        this.l = H(bVar);
        this.m = bVar.m;
        this.n = bVar.n == null ? new a(this) : bVar.n;
        ap0 G = bVar.o == null ? G(bVar.f) : bVar.o;
        this.o = G;
        if (bVar.p != null) {
            r72Var = bVar.p;
        } else {
            r72Var = qg2.b();
        }
        this.p = r72Var;
        this.q = I(bVar, t);
        int i2 = bVar.B < 0 ? 30000 : bVar.B;
        this.s = i2;
        if (nc1.d()) {
            nc1.a("ImagePipelineConfig->mNetworkFetcher");
        }
        this.r = bVar.r == null ? new d(i2) : bVar.r;
        if (nc1.d()) {
            nc1.b();
        }
        br2 unused2 = bVar.s;
        if (bVar.t != null) {
            xs2Var = bVar.t;
        } else {
            xs2Var = new xs2(ws2.n().m());
        }
        this.t = xs2Var;
        this.u = bVar.u == null ? new np3() : bVar.u;
        this.v = bVar.v == null ? new HashSet<>() : bVar.v;
        this.w = bVar.w == null ? new HashSet<>() : bVar.w;
        this.x = bVar.x;
        this.y = bVar.y != null ? bVar.y : G;
        un1 unused3 = bVar.A;
        this.i = bVar.i == null ? new fj0(xs2Var.e()) : bVar.i;
        this.B = bVar.D;
        this.C = bVar.E;
        this.D = bVar.F;
        this.E = bVar.G;
        this.H = bVar.J == null ? new g90() : bVar.J;
        this.F = bVar.H;
        this.G = bVar.I;
        oo4 m = t.m();
        if (m != null) {
            K(m, t, new bl1(a()));
        } else if (t.z() && po4.a && (i = po4.i()) != null) {
            K(i, t, new bl1(a()));
        }
        if (nc1.d()) {
            nc1.b();
        }
    }
}
