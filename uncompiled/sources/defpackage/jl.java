package defpackage;

import defpackage.wx0;
import java.util.Map;
import java.util.Objects;

/* compiled from: AutoValue_EventInternal.java */
/* renamed from: jl  reason: default package */
/* loaded from: classes.dex */
public final class jl extends wx0 {
    public final String a;
    public final Integer b;
    public final cv0 c;
    public final long d;
    public final long e;
    public final Map<String, String> f;

    /* compiled from: AutoValue_EventInternal.java */
    /* renamed from: jl$b */
    /* loaded from: classes.dex */
    public static final class b extends wx0.a {
        public String a;
        public Integer b;
        public cv0 c;
        public Long d;
        public Long e;
        public Map<String, String> f;

        @Override // defpackage.wx0.a
        public wx0 d() {
            String str = "";
            if (this.a == null) {
                str = " transportName";
            }
            if (this.c == null) {
                str = str + " encodedPayload";
            }
            if (this.d == null) {
                str = str + " eventMillis";
            }
            if (this.e == null) {
                str = str + " uptimeMillis";
            }
            if (this.f == null) {
                str = str + " autoMetadata";
            }
            if (str.isEmpty()) {
                return new jl(this.a, this.b, this.c, this.d.longValue(), this.e.longValue(), this.f);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.wx0.a
        public Map<String, String> e() {
            Map<String, String> map = this.f;
            if (map != null) {
                return map;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }

        @Override // defpackage.wx0.a
        public wx0.a f(Map<String, String> map) {
            Objects.requireNonNull(map, "Null autoMetadata");
            this.f = map;
            return this;
        }

        @Override // defpackage.wx0.a
        public wx0.a g(Integer num) {
            this.b = num;
            return this;
        }

        @Override // defpackage.wx0.a
        public wx0.a h(cv0 cv0Var) {
            Objects.requireNonNull(cv0Var, "Null encodedPayload");
            this.c = cv0Var;
            return this;
        }

        @Override // defpackage.wx0.a
        public wx0.a i(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.wx0.a
        public wx0.a j(String str) {
            Objects.requireNonNull(str, "Null transportName");
            this.a = str;
            return this;
        }

        @Override // defpackage.wx0.a
        public wx0.a k(long j) {
            this.e = Long.valueOf(j);
            return this;
        }
    }

    @Override // defpackage.wx0
    public Map<String, String> c() {
        return this.f;
    }

    @Override // defpackage.wx0
    public Integer d() {
        return this.b;
    }

    @Override // defpackage.wx0
    public cv0 e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        Integer num;
        if (obj == this) {
            return true;
        }
        if (obj instanceof wx0) {
            wx0 wx0Var = (wx0) obj;
            return this.a.equals(wx0Var.j()) && ((num = this.b) != null ? num.equals(wx0Var.d()) : wx0Var.d() == null) && this.c.equals(wx0Var.e()) && this.d == wx0Var.f() && this.e == wx0Var.k() && this.f.equals(wx0Var.c());
        }
        return false;
    }

    @Override // defpackage.wx0
    public long f() {
        return this.d;
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        Integer num = this.b;
        int hashCode2 = num == null ? 0 : num.hashCode();
        long j = this.d;
        long j2 = this.e;
        return ((((((((hashCode ^ hashCode2) * 1000003) ^ this.c.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.f.hashCode();
    }

    @Override // defpackage.wx0
    public String j() {
        return this.a;
    }

    @Override // defpackage.wx0
    public long k() {
        return this.e;
    }

    public String toString() {
        return "EventInternal{transportName=" + this.a + ", code=" + this.b + ", encodedPayload=" + this.c + ", eventMillis=" + this.d + ", uptimeMillis=" + this.e + ", autoMetadata=" + this.f + "}";
    }

    public jl(String str, Integer num, cv0 cv0Var, long j, long j2, Map<String, String> map) {
        this.a = str;
        this.b = num;
        this.c = cv0Var;
        this.d = j;
        this.e = j2;
        this.f = map;
    }
}
