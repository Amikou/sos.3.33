package defpackage;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.a;
import androidx.navigation.d;
import androidx.navigation.h;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.lang.ref.WeakReference;
import java.util.Set;

/* compiled from: NavigationUI.java */
/* renamed from: oe2  reason: default package */
/* loaded from: classes.dex */
public final class oe2 {

    /* compiled from: NavigationUI.java */
    /* renamed from: oe2$a */
    /* loaded from: classes.dex */
    public class a implements BottomNavigationView.b {
        public final /* synthetic */ NavController a;

        public a(NavController navController) {
            this.a = navController;
        }

        @Override // com.google.android.material.navigation.NavigationBarView.d
        public boolean a(MenuItem menuItem) {
            return oe2.d(menuItem, this.a);
        }
    }

    /* compiled from: NavigationUI.java */
    /* renamed from: oe2$b */
    /* loaded from: classes.dex */
    public class b implements NavController.b {
        public final /* synthetic */ WeakReference a;
        public final /* synthetic */ NavController b;

        public b(WeakReference weakReference, NavController navController) {
            this.a = weakReference;
            this.b = navController;
        }

        @Override // androidx.navigation.NavController.b
        public void a(NavController navController, d dVar, Bundle bundle) {
            BottomNavigationView bottomNavigationView = (BottomNavigationView) this.a.get();
            if (bottomNavigationView == null) {
                this.b.B(this);
                return;
            }
            Menu menu = bottomNavigationView.getMenu();
            int size = menu.size();
            for (int i = 0; i < size; i++) {
                MenuItem item = menu.getItem(i);
                if (oe2.b(dVar, item.getItemId())) {
                    item.setChecked(true);
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
        r1 = r1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static androidx.navigation.d a(androidx.navigation.e r1) {
        /*
        L0:
            boolean r0 = r1 instanceof androidx.navigation.e
            if (r0 == 0) goto Lf
            androidx.navigation.e r1 = (androidx.navigation.e) r1
            int r0 = r1.N()
            androidx.navigation.d r1 = r1.J(r0)
            goto L0
        Lf:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.oe2.a(androidx.navigation.e):androidx.navigation.d");
    }

    public static boolean b(d dVar, int i) {
        while (dVar.s() != i && dVar.y() != null) {
            dVar = dVar.y();
        }
        return dVar.s() == i;
    }

    public static boolean c(d dVar, Set<Integer> set) {
        while (!set.contains(Integer.valueOf(dVar.s()))) {
            dVar = dVar.y();
            if (dVar == null) {
                return false;
            }
        }
        return true;
    }

    public static boolean d(MenuItem menuItem, NavController navController) {
        h.a d = new h.a().d(true);
        if (navController.i().y().J(menuItem.getItemId()) instanceof a.C0049a) {
            d.b(tx2.nav_default_enter_anim).c(tx2.nav_default_exit_anim).e(tx2.nav_default_pop_enter_anim).f(tx2.nav_default_pop_exit_anim);
        } else {
            d.b(ux2.nav_default_enter_anim).c(ux2.nav_default_exit_anim).e(ux2.nav_default_pop_enter_anim).f(ux2.nav_default_pop_exit_anim);
        }
        if ((menuItem.getOrder() & 196608) == 0) {
            d.g(a(navController.k()).s(), false);
        }
        try {
            navController.r(menuItem.getItemId(), null, d.a());
            return true;
        } catch (IllegalArgumentException unused) {
            return false;
        }
    }

    public static void e(AppCompatActivity appCompatActivity, NavController navController, af afVar) {
        navController.a(new h6(appCompatActivity, afVar));
    }

    public static void f(BottomNavigationView bottomNavigationView, NavController navController) {
        bottomNavigationView.setOnNavigationItemSelectedListener(new a(navController));
        navController.a(new b(new WeakReference(bottomNavigationView), navController));
    }
}
