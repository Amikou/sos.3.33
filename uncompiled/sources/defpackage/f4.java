package defpackage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.p;

/* renamed from: f4  reason: default package */
/* loaded from: classes2.dex */
public abstract class f4 extends k implements g4 {
    public byte[] a;

    public f4(byte[] bArr) {
        Objects.requireNonNull(bArr, "string cannot be null");
        this.a = bArr;
    }

    public static f4 B(Object obj) {
        if (obj == null || (obj instanceof f4)) {
            return (f4) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return B(k.s((byte[]) obj));
            } catch (IOException e) {
                throw new IllegalArgumentException("failed to construct OCTET STRING from byte[]: " + e.getMessage());
            }
        }
        if (obj instanceof c4) {
            k i = ((c4) obj).i();
            if (i instanceof f4) {
                return (f4) i;
            }
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static f4 z(k4 k4Var, boolean z) {
        k B = k4Var.B();
        return (z || (B instanceof f4)) ? B(B) : p.F(h4.z(B));
    }

    public byte[] D() {
        return this.a;
    }

    @Override // defpackage.g4
    public InputStream e() {
        return new ByteArrayInputStream(this.a);
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return wh.r(D());
    }

    @Override // org.bouncycastle.asn1.d1
    public k k() {
        return i();
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof f4) {
            return wh.a(this.a, ((f4) kVar).a);
        }
        return false;
    }

    public String toString() {
        return "#" + su3.b(pk1.b(this.a));
    }

    @Override // org.bouncycastle.asn1.k
    public k w() {
        return new j0(this.a);
    }

    @Override // org.bouncycastle.asn1.k
    public k y() {
        return new j0(this.a);
    }
}
