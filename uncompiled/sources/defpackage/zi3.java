package defpackage;

import defpackage.wi3;
import zendesk.support.request.CellBase;

/* compiled from: Seeker.java */
/* renamed from: zi3  reason: default package */
/* loaded from: classes.dex */
public interface zi3 extends wi3 {

    /* compiled from: Seeker.java */
    /* renamed from: zi3$a */
    /* loaded from: classes.dex */
    public static class a extends wi3.b implements zi3 {
        public a() {
            super(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }

        @Override // defpackage.zi3
        public long b(long j) {
            return 0L;
        }

        @Override // defpackage.zi3
        public long d() {
            return -1L;
        }
    }

    long b(long j);

    long d();
}
