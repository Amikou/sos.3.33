package defpackage;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.util.InternCache;
import java.util.Arrays;
import java.util.BitSet;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: CharsToNameCanonicalizer.java */
/* renamed from: zx  reason: default package */
/* loaded from: classes.dex */
public final class zx {
    public final zx a;
    public final AtomicReference<b> b;
    public final int c;
    public final int d;
    public boolean e;
    public String[] f;
    public a[] g;
    public int h;
    public int i;
    public int j;
    public int k;
    public boolean l;
    public BitSet m;

    /* compiled from: CharsToNameCanonicalizer.java */
    /* renamed from: zx$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final String a;
        public final a b;
        public final int c;

        public a(String str, a aVar) {
            this.a = str;
            this.b = aVar;
            this.c = aVar != null ? 1 + aVar.c : 1;
        }

        public String a(char[] cArr, int i, int i2) {
            if (this.a.length() != i2) {
                return null;
            }
            int i3 = 0;
            while (this.a.charAt(i3) == cArr[i + i3]) {
                i3++;
                if (i3 >= i2) {
                    return this.a;
                }
            }
            return null;
        }
    }

    public zx(int i) {
        this.a = null;
        this.c = i;
        this.e = true;
        this.d = -1;
        this.l = false;
        this.k = 0;
        this.b = new AtomicReference<>(b.a(64));
    }

    public static int e(int i) {
        return i - (i >> 2);
    }

    public static zx m() {
        long currentTimeMillis = System.currentTimeMillis();
        return n((((int) currentTimeMillis) + ((int) (currentTimeMillis >>> 32))) | 1);
    }

    public static zx n(int i) {
        return new zx(i);
    }

    public final String a(char[] cArr, int i, int i2, int i3, int i4) {
        if (this.l) {
            l();
            this.l = false;
        } else if (this.h >= this.i) {
            t();
            i4 = d(k(cArr, i, i2));
        }
        String str = new String(cArr, i, i2);
        if (JsonFactory.Feature.INTERN_FIELD_NAMES.enabledIn(this.d)) {
            str = InternCache.instance.intern(str);
        }
        this.h++;
        String[] strArr = this.f;
        if (strArr[i4] == null) {
            strArr[i4] = str;
        } else {
            int i5 = i4 >> 1;
            a aVar = new a(str, this.g[i5]);
            int i6 = aVar.c;
            if (i6 > 100) {
                c(i5, aVar);
            } else {
                this.g[i5] = aVar;
                this.k = Math.max(i6, this.k);
            }
        }
        return str;
    }

    public final String b(char[] cArr, int i, int i2, a aVar) {
        while (aVar != null) {
            String a2 = aVar.a(cArr, i, i2);
            if (a2 != null) {
                return a2;
            }
            aVar = aVar.b;
        }
        return null;
    }

    public final void c(int i, a aVar) {
        BitSet bitSet = this.m;
        if (bitSet == null) {
            BitSet bitSet2 = new BitSet();
            this.m = bitSet2;
            bitSet2.set(i);
        } else if (bitSet.get(i)) {
            if (JsonFactory.Feature.FAIL_ON_SYMBOL_HASH_OVERFLOW.enabledIn(this.d)) {
                v(100);
            }
            this.e = false;
        } else {
            this.m.set(i);
        }
        this.f[i + i] = aVar.a;
        this.g[i] = null;
        this.h -= aVar.c;
        this.k = -1;
    }

    public int d(int i) {
        int i2 = i + (i >>> 15);
        int i3 = i2 ^ (i2 << 7);
        return (i3 + (i3 >>> 3)) & this.j;
    }

    public int j(String str) {
        int length = str.length();
        int i = this.c;
        for (int i2 = 0; i2 < length; i2++) {
            i = (i * 33) + str.charAt(i2);
        }
        if (i == 0) {
            return 1;
        }
        return i;
    }

    public int k(char[] cArr, int i, int i2) {
        int i3 = this.c;
        int i4 = i2 + i;
        while (i < i4) {
            i3 = (i3 * 33) + cArr[i];
            i++;
        }
        if (i3 == 0) {
            return 1;
        }
        return i3;
    }

    public final void l() {
        String[] strArr = this.f;
        this.f = (String[]) Arrays.copyOf(strArr, strArr.length);
        a[] aVarArr = this.g;
        this.g = (a[]) Arrays.copyOf(aVarArr, aVarArr.length);
    }

    public String o(char[] cArr, int i, int i2, int i3) {
        if (i2 < 1) {
            return "";
        }
        if (!this.e) {
            return new String(cArr, i, i2);
        }
        int d = d(i3);
        String str = this.f[d];
        if (str != null) {
            if (str.length() == i2) {
                int i4 = 0;
                while (str.charAt(i4) == cArr[i + i4]) {
                    i4++;
                    if (i4 == i2) {
                        return str;
                    }
                }
            }
            a aVar = this.g[d >> 1];
            if (aVar != null) {
                String a2 = aVar.a(cArr, i, i2);
                if (a2 != null) {
                    return a2;
                }
                String b2 = b(cArr, i, i2, aVar.b);
                if (b2 != null) {
                    return b2;
                }
            }
        }
        return a(cArr, i, i2, i3, d);
    }

    public int p() {
        return this.c;
    }

    public zx q(int i) {
        return new zx(this, i, this.c, this.b.get());
    }

    public boolean r() {
        return !this.l;
    }

    public final void s(b bVar) {
        int i = bVar.a;
        b bVar2 = this.b.get();
        if (i == bVar2.a) {
            return;
        }
        if (i > 12000) {
            bVar = b.a(64);
        }
        this.b.compareAndSet(bVar2, bVar);
    }

    public final void t() {
        String[] strArr = this.f;
        int length = strArr.length;
        int i = length + length;
        if (i > 65536) {
            this.h = 0;
            this.e = false;
            this.f = new String[64];
            this.g = new a[32];
            this.j = 63;
            this.l = false;
            return;
        }
        a[] aVarArr = this.g;
        this.f = new String[i];
        this.g = new a[i >> 1];
        this.j = i - 1;
        this.i = e(i);
        int i2 = 0;
        int i3 = 0;
        for (String str : strArr) {
            if (str != null) {
                i2++;
                int d = d(j(str));
                String[] strArr2 = this.f;
                if (strArr2[d] == null) {
                    strArr2[d] = str;
                } else {
                    int i4 = d >> 1;
                    a aVar = new a(str, this.g[i4]);
                    this.g[i4] = aVar;
                    i3 = Math.max(i3, aVar.c);
                }
            }
        }
        int i5 = length >> 1;
        for (int i6 = 0; i6 < i5; i6++) {
            for (a aVar2 = aVarArr[i6]; aVar2 != null; aVar2 = aVar2.b) {
                i2++;
                String str2 = aVar2.a;
                int d2 = d(j(str2));
                String[] strArr3 = this.f;
                if (strArr3[d2] == null) {
                    strArr3[d2] = str2;
                } else {
                    int i7 = d2 >> 1;
                    a aVar3 = new a(str2, this.g[i7]);
                    this.g[i7] = aVar3;
                    i3 = Math.max(i3, aVar3.c);
                }
            }
        }
        this.k = i3;
        this.m = null;
        if (i2 != this.h) {
            throw new IllegalStateException(String.format("Internal error on SymbolTable.rehash(): had %d entries; now have %d", Integer.valueOf(this.h), Integer.valueOf(i2)));
        }
    }

    public void u() {
        zx zxVar;
        if (r() && (zxVar = this.a) != null && this.e) {
            zxVar.s(new b(this));
            this.l = true;
        }
    }

    public void v(int i) {
        throw new IllegalStateException("Longest collision chain in symbol table (of size " + this.h + ") now exceeds maximum, " + i + " -- suspect a DoS attack based on hash collisions");
    }

    /* compiled from: CharsToNameCanonicalizer.java */
    /* renamed from: zx$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final int b;
        public final String[] c;
        public final a[] d;

        public b(int i, int i2, String[] strArr, a[] aVarArr) {
            this.a = i;
            this.b = i2;
            this.c = strArr;
            this.d = aVarArr;
        }

        public static b a(int i) {
            return new b(0, 0, new String[i], new a[i >> 1]);
        }

        public b(zx zxVar) {
            this.a = zxVar.h;
            this.b = zxVar.k;
            this.c = zxVar.f;
            this.d = zxVar.g;
        }
    }

    public zx(zx zxVar, int i, int i2, b bVar) {
        this.a = zxVar;
        this.c = i2;
        this.b = null;
        this.d = i;
        this.e = JsonFactory.Feature.CANONICALIZE_FIELD_NAMES.enabledIn(i);
        String[] strArr = bVar.c;
        this.f = strArr;
        this.g = bVar.d;
        this.h = bVar.a;
        this.k = bVar.b;
        int length = strArr.length;
        this.i = e(length);
        this.j = length - 1;
        this.l = true;
    }
}
