package defpackage;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.common.internal.c;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.zar;
import com.google.android.gms.signin.internal.a;
import com.google.android.gms.signin.internal.e;
import com.google.android.gms.signin.internal.zak;
import com.google.android.gms.signin.internal.zam;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: so3  reason: default package */
/* loaded from: classes.dex */
public class so3 extends c<com.google.android.gms.signin.internal.c> implements p15 {
    public final boolean E;
    public final kz F;
    public final Bundle G;
    public final Integer H;

    public so3(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull boolean z, @RecentlyNonNull kz kzVar, @RecentlyNonNull Bundle bundle, @RecentlyNonNull GoogleApiClient.b bVar, @RecentlyNonNull GoogleApiClient.c cVar) {
        super(context, looper, 44, kzVar, bVar, cVar);
        this.E = z;
        this.F = kzVar;
        this.G = bundle;
        this.H = kzVar.j();
    }

    @RecentlyNonNull
    public static Bundle s0(@RecentlyNonNull kz kzVar) {
        uo3 i = kzVar.i();
        Integer j = kzVar.j();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", kzVar.a());
        if (j != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", j.intValue());
        }
        if (i != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", false);
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", false);
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", null);
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", false);
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", null);
            bundle.putString("com.google.android.gms.signin.internal.logSessionId", null);
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", false);
        }
        return bundle;
    }

    @Override // com.google.android.gms.common.internal.b
    @RecentlyNonNull
    public Bundle D() {
        if (!C().getPackageName().equals(this.F.d())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.d());
        }
        return this.G;
    }

    @Override // com.google.android.gms.common.internal.b
    @RecentlyNonNull
    public String H() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @Override // com.google.android.gms.common.internal.b
    @RecentlyNonNull
    public String I() {
        return "com.google.android.gms.signin.service.START";
    }

    @Override // defpackage.p15
    public final void a() {
        try {
            ((com.google.android.gms.signin.internal.c) G()).e(((Integer) zt2.j(this.H)).intValue());
        } catch (RemoteException unused) {
        }
    }

    @Override // defpackage.p15
    public final void d(@RecentlyNonNull d dVar, @RecentlyNonNull boolean z) {
        try {
            ((com.google.android.gms.signin.internal.c) G()).S(dVar, ((Integer) zt2.j(this.H)).intValue(), z);
        } catch (RemoteException unused) {
        }
    }

    @Override // defpackage.p15
    public final void f() {
        m(new b.d());
    }

    @Override // defpackage.p15
    public final void j(a aVar) {
        zt2.k(aVar, "Expecting a valid ISignInCallbacks");
        try {
            Account b = this.F.b();
            ((com.google.android.gms.signin.internal.c) G()).M0(new zak(new zar(b, ((Integer) zt2.j(this.H)).intValue(), "<<default account>>".equals(b.name) ? wt3.a(C()).b() : null)), aVar);
        } catch (RemoteException e) {
            try {
                aVar.d1(new zam(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @Override // com.google.android.gms.common.internal.b, com.google.android.gms.common.api.a.f
    @RecentlyNonNull
    public int q() {
        return qh1.a;
    }

    @Override // com.google.android.gms.common.internal.b, com.google.android.gms.common.api.a.f
    @RecentlyNonNull
    public boolean u() {
        return this.E;
    }

    @Override // com.google.android.gms.common.internal.b
    @RecentlyNonNull
    public /* synthetic */ IInterface y(@RecentlyNonNull IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof com.google.android.gms.signin.internal.c) {
            return (com.google.android.gms.signin.internal.c) queryLocalInterface;
        }
        return new e(iBinder);
    }

    public so3(@RecentlyNonNull Context context, @RecentlyNonNull Looper looper, @RecentlyNonNull boolean z, @RecentlyNonNull kz kzVar, @RecentlyNonNull uo3 uo3Var, @RecentlyNonNull GoogleApiClient.b bVar, @RecentlyNonNull GoogleApiClient.c cVar) {
        this(context, looper, true, kzVar, s0(kzVar), bVar, cVar);
    }
}
