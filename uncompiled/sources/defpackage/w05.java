package defpackage;

import com.google.android.gms.common.api.b;
import com.google.android.gms.common.api.internal.o;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: w05  reason: default package */
/* loaded from: classes.dex */
public final class w05 {
    public final o a;
    public final int b;
    public final b<?> c;

    public w05(o oVar, int i, b<?> bVar) {
        this.a = oVar;
        this.b = i;
        this.c = bVar;
    }
}
