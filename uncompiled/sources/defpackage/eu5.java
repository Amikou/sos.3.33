package defpackage;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: eu5  reason: default package */
/* loaded from: classes.dex */
public abstract class eu5 {
    public static final eu5 a = new hu5();
    public static final eu5 b = new ru5();

    public eu5() {
    }

    public static eu5 a() {
        return a;
    }

    public static eu5 c() {
        return b;
    }

    public abstract <L> void b(Object obj, Object obj2, long j);

    public abstract void d(Object obj, long j);
}
