package defpackage;

import com.google.android.gms.internal.vision.z0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ru5  reason: default package */
/* loaded from: classes.dex */
public final class ru5 extends eu5 {
    public ru5() {
        super();
    }

    public static <E> gt5<E> e(Object obj, long j) {
        return (gt5) z0.F(obj, j);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
    @Override // defpackage.eu5
    public final <E> void b(Object obj, Object obj2, long j) {
        gt5<E> e = e(obj, j);
        gt5<E> e2 = e(obj2, j);
        int size = e.size();
        int size2 = e2.size();
        gt5<E> gt5Var = e;
        gt5Var = e;
        if (size > 0 && size2 > 0) {
            boolean zza = e.zza();
            gt5<E> gt5Var2 = e;
            if (!zza) {
                gt5Var2 = e.d(size2 + size);
            }
            gt5Var2.addAll(e2);
            gt5Var = gt5Var2;
        }
        if (size > 0) {
            e2 = gt5Var;
        }
        z0.j(obj, j, e2);
    }

    @Override // defpackage.eu5
    public final void d(Object obj, long j) {
        e(obj, j).zzb();
    }
}
