package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapPrepareProducer.java */
/* renamed from: mq  reason: default package */
/* loaded from: classes.dex */
public class mq implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> a;
    public final int b;
    public final int c;
    public final boolean d;

    /* compiled from: BitmapPrepareProducer.java */
    /* renamed from: mq$a */
    /* loaded from: classes.dex */
    public static class a extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final int c;
        public final int d;

        public a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, int i, int i2) {
            super(l60Var);
            this.c = i;
            this.d = i2;
        }

        public final void q(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            com.facebook.imagepipeline.image.a j;
            Bitmap f;
            int rowBytes;
            if (aVar == null || !aVar.r() || (j = aVar.j()) == null || j.isClosed() || !(j instanceof c00) || (f = ((c00) j).f()) == null || (rowBytes = f.getRowBytes() * f.getHeight()) < this.c || rowBytes > this.d) {
                return;
            }
            f.prepareToDraw();
        }

        @Override // defpackage.qm
        /* renamed from: r */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            q(aVar);
            p().d(aVar, i);
        }
    }

    public mq(dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dv2Var, int i, int i2, boolean z) {
        xt2.b(Boolean.valueOf(i <= i2));
        this.a = (dv2) xt2.g(dv2Var);
        this.b = i;
        this.c = i2;
        this.d = z;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        if (ev2Var.h() && !this.d) {
            this.a.a(l60Var, ev2Var);
        } else {
            this.a.a(new a(l60Var, this.b, this.c), ev2Var);
        }
    }
}
