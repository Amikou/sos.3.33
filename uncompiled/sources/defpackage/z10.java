package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: CollectionsFragmentDirections.java */
/* renamed from: z10  reason: default package */
/* loaded from: classes2.dex */
public class z10 {

    /* compiled from: CollectionsFragmentDirections.java */
    /* renamed from: z10$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("collection")) {
                RoomCollection roomCollection = (RoomCollection) this.a.get("collection");
                if (!Parcelable.class.isAssignableFrom(RoomCollection.class) && roomCollection != null) {
                    if (Serializable.class.isAssignableFrom(RoomCollection.class)) {
                        bundle.putSerializable("collection", (Serializable) Serializable.class.cast(roomCollection));
                    } else {
                        throw new UnsupportedOperationException(RoomCollection.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("collection", (Parcelable) Parcelable.class.cast(roomCollection));
                }
            }
            if (this.a.containsKey("nftData")) {
                RoomNFT roomNFT = (RoomNFT) this.a.get("nftData");
                if (!Parcelable.class.isAssignableFrom(RoomNFT.class) && roomNFT != null) {
                    if (Serializable.class.isAssignableFrom(RoomNFT.class)) {
                        bundle.putSerializable("nftData", (Serializable) Serializable.class.cast(roomNFT));
                    } else {
                        throw new UnsupportedOperationException(RoomNFT.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("nftData", (Parcelable) Parcelable.class.cast(roomNFT));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_navigation_collectibles_to_nftDetailFragment;
        }

        public RoomCollection c() {
            return (RoomCollection) this.a.get("collection");
        }

        public RoomNFT d() {
            return (RoomNFT) this.a.get("nftData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("collection") != bVar.a.containsKey("collection")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                if (this.a.containsKey("nftData") != bVar.a.containsKey("nftData")) {
                    return false;
                }
                if (d() == null ? bVar.d() == null : d().equals(bVar.d())) {
                    return b() == bVar.b();
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return (((((c() != null ? c().hashCode() : 0) + 31) * 31) + (d() != null ? d().hashCode() : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionNavigationCollectiblesToNftDetailFragment(actionId=" + b() + "){collection=" + c() + ", nftData=" + d() + "}";
        }

        public b(RoomCollection roomCollection, RoomNFT roomNFT) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (roomCollection != null) {
                hashMap.put("collection", roomCollection);
                if (roomNFT != null) {
                    hashMap.put("nftData", roomNFT);
                    return;
                }
                throw new IllegalArgumentException("Argument \"nftData\" is marked as non-null but was passed a null value.");
            }
            throw new IllegalArgumentException("Argument \"collection\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(RoomCollection roomCollection, RoomNFT roomNFT) {
        return new b(roomCollection, roomNFT);
    }
}
