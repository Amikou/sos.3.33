package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ArrayIterator.kt */
/* renamed from: oh  reason: default package */
/* loaded from: classes2.dex */
public final class oh<T> implements Iterator<T>, tw1 {
    public int a;
    public final T[] f0;

    public oh(T[] tArr) {
        fs1.f(tArr, "array");
        this.f0 = tArr;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.a < this.f0.length;
    }

    @Override // java.util.Iterator
    public T next() {
        try {
            T[] tArr = this.f0;
            int i = this.a;
            this.a = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.a--;
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
