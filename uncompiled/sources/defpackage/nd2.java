package defpackage;

import com.facebook.imagepipeline.nativecode.NativeJpegTranscoderFactory;
import java.lang.reflect.InvocationTargetException;

/* compiled from: NativeImageTranscoderFactory.java */
/* renamed from: nd2  reason: default package */
/* loaded from: classes.dex */
public final class nd2 {
    public static bp1 a(int i, boolean z, boolean z2) {
        try {
            Class cls = Boolean.TYPE;
            return (bp1) NativeJpegTranscoderFactory.class.getConstructor(Integer.TYPE, cls, cls).newInstance(Integer.valueOf(i), Boolean.valueOf(z), Boolean.valueOf(z2));
        } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            throw new RuntimeException("Dependency ':native-imagetranscoder' is needed to use the default native image transcoder.", e);
        }
    }
}
