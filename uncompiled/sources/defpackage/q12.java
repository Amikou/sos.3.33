package defpackage;

import java.math.BigInteger;
import java.util.List;

/* compiled from: Log.java */
/* renamed from: q12  reason: default package */
/* loaded from: classes3.dex */
public class q12 {
    private String address;
    private String blockHash;
    private String blockNumber;
    private String data;
    private String logIndex;
    private boolean removed;
    private List<String> topics;
    private String transactionHash;
    private String transactionIndex;
    private String type;

    public q12() {
    }

    private BigInteger convert(String str) {
        if (str != null) {
            return ej2.decodeQuantity(str);
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof q12) {
            q12 q12Var = (q12) obj;
            if (isRemoved() != q12Var.isRemoved()) {
                return false;
            }
            if (getLogIndexRaw() == null ? q12Var.getLogIndexRaw() == null : getLogIndexRaw().equals(q12Var.getLogIndexRaw())) {
                if (getTransactionIndexRaw() == null ? q12Var.getTransactionIndexRaw() == null : getTransactionIndexRaw().equals(q12Var.getTransactionIndexRaw())) {
                    if (getTransactionHash() == null ? q12Var.getTransactionHash() == null : getTransactionHash().equals(q12Var.getTransactionHash())) {
                        if (getBlockHash() == null ? q12Var.getBlockHash() == null : getBlockHash().equals(q12Var.getBlockHash())) {
                            if (getBlockNumberRaw() == null ? q12Var.getBlockNumberRaw() == null : getBlockNumberRaw().equals(q12Var.getBlockNumberRaw())) {
                                if (getAddress() == null ? q12Var.getAddress() == null : getAddress().equals(q12Var.getAddress())) {
                                    if (getData() == null ? q12Var.getData() == null : getData().equals(q12Var.getData())) {
                                        if (getType() == null ? q12Var.getType() == null : getType().equals(q12Var.getType())) {
                                            return getTopics() != null ? getTopics().equals(q12Var.getTopics()) : q12Var.getTopics() == null;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public String getAddress() {
        return this.address;
    }

    public String getBlockHash() {
        return this.blockHash;
    }

    public BigInteger getBlockNumber() {
        return convert(this.blockNumber);
    }

    public String getBlockNumberRaw() {
        return this.blockNumber;
    }

    public String getData() {
        return this.data;
    }

    public BigInteger getLogIndex() {
        return convert(this.logIndex);
    }

    public String getLogIndexRaw() {
        return this.logIndex;
    }

    public List<String> getTopics() {
        return this.topics;
    }

    public String getTransactionHash() {
        return this.transactionHash;
    }

    public BigInteger getTransactionIndex() {
        return convert(this.transactionIndex);
    }

    public String getTransactionIndexRaw() {
        return this.transactionIndex;
    }

    public String getType() {
        return this.type;
    }

    public int hashCode() {
        return ((((((((((((((((((isRemoved() ? 1 : 0) * 31) + (getLogIndexRaw() != null ? getLogIndexRaw().hashCode() : 0)) * 31) + (getTransactionIndexRaw() != null ? getTransactionIndexRaw().hashCode() : 0)) * 31) + (getTransactionHash() != null ? getTransactionHash().hashCode() : 0)) * 31) + (getBlockHash() != null ? getBlockHash().hashCode() : 0)) * 31) + (getBlockNumberRaw() != null ? getBlockNumberRaw().hashCode() : 0)) * 31) + (getAddress() != null ? getAddress().hashCode() : 0)) * 31) + (getData() != null ? getData().hashCode() : 0)) * 31) + (getType() != null ? getType().hashCode() : 0)) * 31) + (getTopics() != null ? getTopics().hashCode() : 0);
    }

    public boolean isRemoved() {
        return this.removed;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public void setBlockHash(String str) {
        this.blockHash = str;
    }

    public void setBlockNumber(String str) {
        this.blockNumber = str;
    }

    public void setData(String str) {
        this.data = str;
    }

    public void setLogIndex(String str) {
        this.logIndex = str;
    }

    public void setRemoved(boolean z) {
        this.removed = z;
    }

    public void setTopics(List<String> list) {
        this.topics = list;
    }

    public void setTransactionHash(String str) {
        this.transactionHash = str;
    }

    public void setTransactionIndex(String str) {
        this.transactionIndex = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public String toString() {
        return "Log{removed=" + this.removed + ", logIndex='" + this.logIndex + "', transactionIndex='" + this.transactionIndex + "', transactionHash='" + this.transactionHash + "', blockHash='" + this.blockHash + "', blockNumber='" + this.blockNumber + "', address='" + this.address + "', data='" + this.data + "', type='" + this.type + "', topics=" + this.topics + '}';
    }

    public q12(boolean z, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, List<String> list) {
        this.removed = z;
        this.logIndex = str;
        this.transactionIndex = str2;
        this.transactionHash = str3;
        this.blockHash = str4;
        this.blockNumber = str5;
        this.address = str6;
        this.data = str7;
        this.type = str8;
        this.topics = list;
    }
}
