package defpackage;

import java.lang.reflect.Method;
import kotlin.random.Random;

/* compiled from: PlatformImplementations.kt */
/* renamed from: fr2  reason: default package */
/* loaded from: classes2.dex */
public class fr2 {

    /* compiled from: PlatformImplementations.kt */
    /* renamed from: fr2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static final Method a;

        /* JADX WARN: Removed duplicated region for block: B:13:0x0043 A[LOOP:0: B:3:0x0013->B:13:0x0043, LOOP_END] */
        /* JADX WARN: Removed duplicated region for block: B:22:0x0047 A[EDGE_INSN: B:22:0x0047->B:15:0x0047 ?: BREAK  , SYNTHETIC] */
        static {
            /*
                fr2$a r0 = new fr2$a
                r0.<init>()
                java.lang.Class<java.lang.Throwable> r0 = java.lang.Throwable.class
                java.lang.reflect.Method[] r1 = r0.getMethods()
                java.lang.String r2 = "throwableMethods"
                defpackage.fs1.e(r1, r2)
                int r2 = r1.length
                r3 = 0
                r4 = r3
            L13:
                java.lang.String r5 = "it"
                if (r4 >= r2) goto L46
                r6 = r1[r4]
                defpackage.fs1.e(r6, r5)
                java.lang.String r7 = r6.getName()
                java.lang.String r8 = "addSuppressed"
                boolean r7 = defpackage.fs1.b(r7, r8)
                if (r7 == 0) goto L3f
                java.lang.Class[] r7 = r6.getParameterTypes()
                java.lang.String r8 = "it.parameterTypes"
                defpackage.fs1.e(r7, r8)
                java.lang.Object r7 = defpackage.ai.E(r7)
                java.lang.Class r7 = (java.lang.Class) r7
                boolean r7 = defpackage.fs1.b(r7, r0)
                if (r7 == 0) goto L3f
                r7 = 1
                goto L40
            L3f:
                r7 = r3
            L40:
                if (r7 == 0) goto L43
                goto L47
            L43:
                int r4 = r4 + 1
                goto L13
            L46:
                r6 = 0
            L47:
                defpackage.fr2.a.a = r6
                int r0 = r1.length
            L4a:
                if (r3 >= r0) goto L61
                r2 = r1[r3]
                defpackage.fs1.e(r2, r5)
                java.lang.String r2 = r2.getName()
                java.lang.String r4 = "getSuppressed"
                boolean r2 = defpackage.fs1.b(r2, r4)
                if (r2 == 0) goto L5e
                goto L61
            L5e:
                int r3 = r3 + 1
                goto L4a
            L61:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.fr2.a.<clinit>():void");
        }
    }

    public void a(Throwable th, Throwable th2) {
        fs1.f(th, "cause");
        fs1.f(th2, "exception");
        Method method = a.a;
        if (method != null) {
            method.invoke(th, th2);
        }
    }

    public Random b() {
        return new d21();
    }
}
