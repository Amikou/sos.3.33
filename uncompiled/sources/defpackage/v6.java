package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktResetPasswordBinding.java */
/* renamed from: v6  reason: default package */
/* loaded from: classes2.dex */
public final class v6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final LinearLayoutCompat c;
    public final TextInputLayout d;
    public final TextInputLayout e;
    public final TextInputLayout f;
    public final ImageView g;
    public final ImageView h;
    public final ImageView i;
    public final TextView j;
    public final sp3 k;
    public final TextView l;
    public final TextView m;
    public final TextView n;
    public final TextView o;

    public v6(ConstraintLayout constraintLayout, MaterialButton materialButton, LinearLayoutCompat linearLayoutCompat, ConstraintLayout constraintLayout2, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, ImageView imageView, ImageView imageView2, ImageView imageView3, TextView textView, TextView textView2, sp3 sp3Var, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, TextView textView9) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = linearLayoutCompat;
        this.d = textInputLayout;
        this.e = textInputLayout2;
        this.f = textInputLayout3;
        this.g = imageView;
        this.h = imageView2;
        this.i = imageView3;
        this.j = textView;
        this.k = sp3Var;
        this.l = textView3;
        this.m = textView4;
        this.n = textView5;
        this.o = textView9;
    }

    public static v6 a(View view) {
        int i = R.id.btnSave;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnSave);
        if (materialButton != null) {
            i = R.id.content_layout;
            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.content_layout);
            if (linearLayoutCompat != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                i = R.id.et_confirm_password;
                TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.et_confirm_password);
                if (textInputLayout != null) {
                    i = R.id.et_password;
                    TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.et_password);
                    if (textInputLayout2 != null) {
                        i = R.id.et_username;
                        TextInputLayout textInputLayout3 = (TextInputLayout) ai4.a(view, R.id.et_username);
                        if (textInputLayout3 != null) {
                            i = R.id.iv_1;
                            ImageView imageView = (ImageView) ai4.a(view, R.id.iv_1);
                            if (imageView != null) {
                                i = R.id.iv_2;
                                ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv_2);
                                if (imageView2 != null) {
                                    i = R.id.iv_4;
                                    ImageView imageView3 = (ImageView) ai4.a(view, R.id.iv_4);
                                    if (imageView3 != null) {
                                        i = R.id.min_char;
                                        TextView textView = (TextView) ai4.a(view, R.id.min_char);
                                        if (textView != null) {
                                            i = R.id.textView21;
                                            TextView textView2 = (TextView) ai4.a(view, R.id.textView21);
                                            if (textView2 != null) {
                                                i = R.id.toolbar;
                                                View a = ai4.a(view, R.id.toolbar);
                                                if (a != null) {
                                                    sp3 a2 = sp3.a(a);
                                                    i = R.id.tv_not;
                                                    TextView textView3 = (TextView) ai4.a(view, R.id.tv_not);
                                                    if (textView3 != null) {
                                                        i = R.id.tv_number;
                                                        TextView textView4 = (TextView) ai4.a(view, R.id.tv_number);
                                                        if (textView4 != null) {
                                                            i = R.id.tv_special;
                                                            TextView textView5 = (TextView) ai4.a(view, R.id.tv_special);
                                                            if (textView5 != null) {
                                                                i = R.id.txt_confirm_password_header;
                                                                TextView textView6 = (TextView) ai4.a(view, R.id.txt_confirm_password_header);
                                                                if (textView6 != null) {
                                                                    i = R.id.txt_password_header;
                                                                    TextView textView7 = (TextView) ai4.a(view, R.id.txt_password_header);
                                                                    if (textView7 != null) {
                                                                        i = R.id.txt_username_header;
                                                                        TextView textView8 = (TextView) ai4.a(view, R.id.txt_username_header);
                                                                        if (textView8 != null) {
                                                                            i = R.id.txt_username_invalid;
                                                                            TextView textView9 = (TextView) ai4.a(view, R.id.txt_username_invalid);
                                                                            if (textView9 != null) {
                                                                                return new v6(constraintLayout, materialButton, linearLayoutCompat, constraintLayout, textInputLayout, textInputLayout2, textInputLayout3, imageView, imageView2, imageView3, textView, textView2, a2, textView3, textView4, textView5, textView6, textView7, textView8, textView9);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static v6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static v6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_reset_password, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
