package defpackage;

import java.util.List;
import org.web3j.abi.TypeReference;
import org.web3j.abi.d;

/* compiled from: Function.java */
/* renamed from: id1  reason: default package */
/* loaded from: classes3.dex */
public class id1 {
    private List<zc4> inputParameters;
    private String name;
    private List<TypeReference<zc4>> outputParameters;

    public id1(String str, List<zc4> list, List<TypeReference<?>> list2) {
        this.name = str;
        this.inputParameters = list;
        this.outputParameters = d.convert(list2);
    }

    public List<zc4> getInputParameters() {
        return this.inputParameters;
    }

    public String getName() {
        return this.name;
    }

    public List<TypeReference<zc4>> getOutputParameters() {
        return this.outputParameters;
    }
}
