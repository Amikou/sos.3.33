package defpackage;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.e;

/* compiled from: GsonResponseBodyConverter.java */
/* renamed from: aj1  reason: default package */
/* loaded from: classes3.dex */
public final class aj1<T> implements e<ResponseBody, T> {
    public final Gson a;
    public final TypeAdapter<T> b;

    public aj1(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @Override // retrofit2.e
    /* renamed from: b */
    public T a(ResponseBody responseBody) throws IOException {
        JsonReader newJsonReader = this.a.newJsonReader(responseBody.charStream());
        try {
            T read = this.b.read(newJsonReader);
            if (newJsonReader.peek() == JsonToken.END_DOCUMENT) {
                return read;
            }
            throw new JsonIOException("JSON document was not fully consumed.");
        } finally {
            responseBody.close();
        }
    }
}
