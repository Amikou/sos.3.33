package defpackage;

/* compiled from: TtmlRegion.java */
/* renamed from: kc4  reason: default package */
/* loaded from: classes.dex */
public final class kc4 {
    public final String a;
    public final float b;
    public final float c;
    public final int d;
    public final int e;
    public final float f;
    public final float g;
    public final int h;
    public final float i;
    public final int j;

    public kc4(String str) {
        this(str, -3.4028235E38f, -3.4028235E38f, Integer.MIN_VALUE, Integer.MIN_VALUE, -3.4028235E38f, -3.4028235E38f, Integer.MIN_VALUE, -3.4028235E38f, Integer.MIN_VALUE);
    }

    public kc4(String str, float f, float f2, int i, int i2, float f3, float f4, int i3, float f5, int i4) {
        this.a = str;
        this.b = f;
        this.c = f2;
        this.d = i;
        this.e = i2;
        this.f = f3;
        this.g = f4;
        this.h = i3;
        this.i = f5;
        this.j = i4;
    }
}
