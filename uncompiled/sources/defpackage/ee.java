package defpackage;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import defpackage.de;

/* compiled from: AnimationBackendDelegate.java */
/* renamed from: ee  reason: default package */
/* loaded from: classes.dex */
public class ee<T extends de> implements de {
    public T a;

    public ee(T t) {
        this.a = t;
    }

    @Override // defpackage.ke
    public int a() {
        T t = this.a;
        if (t == null) {
            return 0;
        }
        return t.a();
    }

    @Override // defpackage.ke
    public int b() {
        T t = this.a;
        if (t == null) {
            return 0;
        }
        return t.b();
    }

    @Override // defpackage.de
    public int c() {
        T t = this.a;
        if (t == null) {
            return -1;
        }
        return t.c();
    }

    @Override // defpackage.de
    public void clear() {
        T t = this.a;
        if (t != null) {
            t.clear();
        }
    }

    @Override // defpackage.de
    public void d(Rect rect) {
        T t = this.a;
        if (t != null) {
            t.d(rect);
        }
    }

    @Override // defpackage.de
    public int e() {
        T t = this.a;
        if (t == null) {
            return -1;
        }
        return t.e();
    }

    @Override // defpackage.de
    public void g(ColorFilter colorFilter) {
        T t = this.a;
        if (t != null) {
            t.g(colorFilter);
        }
    }

    @Override // defpackage.ke
    public int h(int i) {
        T t = this.a;
        if (t == null) {
            return 0;
        }
        return t.h(i);
    }

    @Override // defpackage.de
    public void i(int i) {
        T t = this.a;
        if (t != null) {
            t.i(i);
        }
    }

    @Override // defpackage.de
    public boolean j(Drawable drawable, Canvas canvas, int i) {
        T t = this.a;
        return t != null && t.j(drawable, canvas, i);
    }
}
