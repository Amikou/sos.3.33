package defpackage;

import defpackage.yo0;
import java.io.File;

/* compiled from: DataCacheWriter.java */
/* renamed from: ae0  reason: default package */
/* loaded from: classes.dex */
public class ae0<DataType> implements yo0.b {
    public final ev0<DataType> a;
    public final DataType b;
    public final vn2 c;

    public ae0(ev0<DataType> ev0Var, DataType datatype, vn2 vn2Var) {
        this.a = ev0Var;
        this.b = datatype;
        this.c = vn2Var;
    }

    @Override // defpackage.yo0.b
    public boolean a(File file) {
        return this.a.a(this.b, file, this.c);
    }
}
