package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ev5  reason: default package */
/* loaded from: classes.dex */
public final class ev5 extends n55 {
    public final /* synthetic */ gv5 e;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ev5(gv5 gv5Var, tl5 tl5Var) {
        super(tl5Var);
        this.e = gv5Var;
    }

    @Override // defpackage.n55
    public final void a() {
        this.e.k();
        this.e.a.w().v().a("Starting upload from DelayedRunnable");
        this.e.b.d();
    }
}
