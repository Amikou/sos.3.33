package defpackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

/* compiled from: _Arrays.kt */
/* renamed from: ai */
/* loaded from: classes2.dex */
public class ai extends zh {
    public static final Integer A(int[] iArr) {
        fs1.f(iArr, "$this$maxOrNull");
        int i = 1;
        if (iArr.length == 0) {
            return null;
        }
        int i2 = iArr[0];
        int u = u(iArr);
        if (1 <= u) {
            while (true) {
                int i3 = iArr[i];
                if (i2 < i3) {
                    i2 = i3;
                }
                if (i == u) {
                    break;
                }
                i++;
            }
        }
        return Integer.valueOf(i2);
    }

    public static final Integer B(int[] iArr) {
        fs1.f(iArr, "$this$minOrNull");
        int i = 1;
        if (iArr.length == 0) {
            return null;
        }
        int i2 = iArr[0];
        int u = u(iArr);
        if (1 <= u) {
            while (true) {
                int i3 = iArr[i];
                if (i2 > i3) {
                    i2 = i3;
                }
                if (i == u) {
                    break;
                }
                i++;
            }
        }
        return Integer.valueOf(i2);
    }

    public static final <T> T[] C(T[] tArr) {
        fs1.f(tArr, "$this$reversedArray");
        int i = 0;
        if (tArr.length == 0) {
            return tArr;
        }
        T[] tArr2 = (T[]) xh.a(tArr, tArr.length);
        int v = v(tArr);
        if (v >= 0) {
            while (true) {
                tArr2[v - i] = tArr[i];
                if (i == v) {
                    break;
                }
                i++;
            }
        }
        return tArr2;
    }

    public static final char D(char[] cArr) {
        fs1.f(cArr, "$this$single");
        int length = cArr.length;
        if (length != 0) {
            if (length == 1) {
                return cArr[0];
            }
            throw new IllegalArgumentException("Array has more than one element.");
        }
        throw new NoSuchElementException("Array is empty.");
    }

    public static final <T> T E(T[] tArr) {
        fs1.f(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    public static final <T> T[] F(T[] tArr, Comparator<? super T> comparator) {
        fs1.f(tArr, "$this$sortedArrayWith");
        fs1.f(comparator, "comparator");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] tArr2 = (T[]) Arrays.copyOf(tArr, tArr.length);
        fs1.e(tArr2, "java.util.Arrays.copyOf(this, size)");
        zh.o(tArr2, comparator);
        return tArr2;
    }

    public static final <T> List<T> G(T[] tArr, Comparator<? super T> comparator) {
        fs1.f(tArr, "$this$sortedWith");
        fs1.f(comparator, "comparator");
        return zh.c(F(tArr, comparator));
    }

    public static final <T, C extends Collection<? super T>> C H(T[] tArr, C c) {
        fs1.f(tArr, "$this$toCollection");
        fs1.f(c, "destination");
        for (T t : tArr) {
            c.add(t);
        }
        return c;
    }

    public static final int[] I(Integer[] numArr) {
        fs1.f(numArr, "$this$toIntArray");
        int length = numArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = numArr[i].intValue();
        }
        return iArr;
    }

    public static final List<Integer> J(int[] iArr) {
        fs1.f(iArr, "$this$toList");
        int length = iArr.length;
        if (length != 0) {
            if (length != 1) {
                return L(iArr);
            }
            return a20.b(Integer.valueOf(iArr[0]));
        }
        return b20.g();
    }

    public static final <T> List<T> K(T[] tArr) {
        fs1.f(tArr, "$this$toList");
        int length = tArr.length;
        if (length != 0) {
            if (length != 1) {
                return M(tArr);
            }
            return a20.b(tArr[0]);
        }
        return b20.g();
    }

    public static final List<Integer> L(int[] iArr) {
        fs1.f(iArr, "$this$toMutableList");
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int i : iArr) {
            arrayList.add(Integer.valueOf(i));
        }
        return arrayList;
    }

    public static final <T> List<T> M(T[] tArr) {
        fs1.f(tArr, "$this$toMutableList");
        return new ArrayList(b20.d(tArr));
    }

    public static final <T> Set<T> N(T[] tArr) {
        fs1.f(tArr, "$this$toMutableSet");
        return (Set) H(tArr, new LinkedHashSet(y32.a(tArr.length)));
    }

    public static final <T> boolean p(T[] tArr, T t) {
        fs1.f(tArr, "$this$contains");
        return w(tArr, t) >= 0;
    }

    public static final <T> List<T> q(T[] tArr) {
        fs1.f(tArr, "$this$distinct");
        return j20.k0(N(tArr));
    }

    public static final <T> List<T> r(T[] tArr) {
        fs1.f(tArr, "$this$filterNotNull");
        return (List) s(tArr, new ArrayList());
    }

    public static final <C extends Collection<? super T>, T> C s(T[] tArr, C c) {
        fs1.f(tArr, "$this$filterNotNullTo");
        fs1.f(c, "destination");
        for (T t : tArr) {
            if (t != null) {
                c.add(t);
            }
        }
        return c;
    }

    public static final <T> sr1 t(T[] tArr) {
        fs1.f(tArr, "$this$indices");
        return new sr1(0, v(tArr));
    }

    public static final int u(int[] iArr) {
        fs1.f(iArr, "$this$lastIndex");
        return iArr.length - 1;
    }

    public static final <T> int v(T[] tArr) {
        fs1.f(tArr, "$this$lastIndex");
        return tArr.length - 1;
    }

    public static final <T> int w(T[] tArr, T t) {
        fs1.f(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (fs1.b(t, tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static final <T, A extends Appendable> A x(T[] tArr, A a, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(tArr, "$this$joinTo");
        fs1.f(a, "buffer");
        fs1.f(charSequence, "separator");
        fs1.f(charSequence2, "prefix");
        fs1.f(charSequence3, "postfix");
        fs1.f(charSequence4, "truncated");
        a.append(charSequence2);
        int i2 = 0;
        for (T t : tArr) {
            i2++;
            if (i2 > 1) {
                a.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            wu3.a(a, t, tc1Var);
        }
        if (i >= 0 && i2 > i) {
            a.append(charSequence4);
        }
        a.append(charSequence3);
        return a;
    }

    public static final <T> String y(T[] tArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(tArr, "$this$joinToString");
        fs1.f(charSequence, "separator");
        fs1.f(charSequence2, "prefix");
        fs1.f(charSequence3, "postfix");
        fs1.f(charSequence4, "truncated");
        String sb = ((StringBuilder) x(tArr, new StringBuilder(), charSequence, charSequence2, charSequence3, i, charSequence4, tc1Var)).toString();
        fs1.e(sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }

    public static /* synthetic */ String z(Object[] objArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1 tc1Var, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        String str = (i2 & 2) != 0 ? "" : charSequence2;
        String str2 = (i2 & 4) == 0 ? charSequence3 : "";
        if ((i2 & 8) != 0) {
            i = -1;
        }
        int i3 = i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence5 = charSequence4;
        if ((i2 & 32) != 0) {
            tc1Var = null;
        }
        return y(objArr, charSequence, str, str2, i3, charSequence5, tc1Var);
    }
}
