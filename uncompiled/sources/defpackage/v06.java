package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: v06  reason: default package */
/* loaded from: classes.dex */
public final class v06 implements u06 {
    public static final wo5<Boolean> a;
    public static final wo5<Boolean> b;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.euid.client.dev", false);
        b = ro5Var.b("measurement.euid.service", false);
    }

    @Override // defpackage.u06
    public final boolean zza() {
        return a.e().booleanValue();
    }

    @Override // defpackage.u06
    public final boolean zzb() {
        return b.e().booleanValue();
    }
}
