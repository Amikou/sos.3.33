package defpackage;

import com.google.android.gms.internal.clearcut.s;
import com.google.protobuf.r0;

/* renamed from: ve5  reason: default package */
/* loaded from: classes.dex */
public final class ve5 {
    public static final se5 a = c();
    public static final se5 b = new s();

    public static se5 a() {
        return a;
    }

    public static se5 b() {
        return b;
    }

    public static se5 c() {
        try {
            return (se5) r0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
