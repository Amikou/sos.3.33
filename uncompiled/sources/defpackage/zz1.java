package defpackage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.util.Property;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.LinearProgressIndicatorSpec;
import java.util.Arrays;

/* compiled from: LinearIndeterminateContiguousAnimatorDelegate.java */
/* renamed from: zz1  reason: default package */
/* loaded from: classes2.dex */
public final class zz1 extends hq1<ObjectAnimator> {
    public static final Property<zz1, Float> j = new b(Float.class, "animationFraction");
    public ObjectAnimator d;
    public f21 e;
    public final wn f;
    public int g;
    public boolean h;
    public float i;

    /* compiled from: LinearIndeterminateContiguousAnimatorDelegate.java */
    /* renamed from: zz1$a */
    /* loaded from: classes2.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
            super.onAnimationRepeat(animator);
            zz1 zz1Var = zz1.this;
            zz1Var.g = (zz1Var.g + 1) % zz1.this.f.c.length;
            zz1.this.h = true;
        }
    }

    /* compiled from: LinearIndeterminateContiguousAnimatorDelegate.java */
    /* renamed from: zz1$b */
    /* loaded from: classes2.dex */
    public static class b extends Property<zz1, Float> {
        public b(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(zz1 zz1Var) {
            return Float.valueOf(zz1Var.n());
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(zz1 zz1Var, Float f) {
            zz1Var.r(f.floatValue());
        }
    }

    public zz1(LinearProgressIndicatorSpec linearProgressIndicatorSpec) {
        super(3);
        this.g = 1;
        this.f = linearProgressIndicatorSpec;
        this.e = new f21();
    }

    @Override // defpackage.hq1
    public void a() {
        ObjectAnimator objectAnimator = this.d;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
    }

    @Override // defpackage.hq1
    public void c() {
        q();
    }

    @Override // defpackage.hq1
    public void d(hd hdVar) {
    }

    @Override // defpackage.hq1
    public void f() {
    }

    @Override // defpackage.hq1
    public void g() {
        o();
        q();
        this.d.start();
    }

    @Override // defpackage.hq1
    public void h() {
    }

    public final float n() {
        return this.i;
    }

    public final void o() {
        if (this.d == null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, j, Utils.FLOAT_EPSILON, 1.0f);
            this.d = ofFloat;
            ofFloat.setDuration(333L);
            this.d.setInterpolator(null);
            this.d.setRepeatCount(-1);
            this.d.addListener(new a());
        }
    }

    public final void p() {
        if (!this.h || this.b[3] >= 1.0f) {
            return;
        }
        int[] iArr = this.c;
        iArr[2] = iArr[1];
        iArr[1] = iArr[0];
        iArr[0] = l42.a(this.f.c[this.g], this.a.getAlpha());
        this.h = false;
    }

    public void q() {
        this.h = true;
        this.g = 1;
        Arrays.fill(this.c, l42.a(this.f.c[0], this.a.getAlpha()));
    }

    public void r(float f) {
        this.i = f;
        s((int) (f * 333.0f));
        p();
        this.a.invalidateSelf();
    }

    public final void s(int i) {
        this.b[0] = 0.0f;
        float b2 = b(i, 0, 667);
        float[] fArr = this.b;
        float interpolation = this.e.getInterpolation(b2);
        fArr[2] = interpolation;
        fArr[1] = interpolation;
        float[] fArr2 = this.b;
        float interpolation2 = this.e.getInterpolation(b2 + 0.49925038f);
        fArr2[4] = interpolation2;
        fArr2[3] = interpolation2;
        this.b[5] = 1.0f;
    }
}
