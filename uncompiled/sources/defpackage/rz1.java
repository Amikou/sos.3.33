package defpackage;

import androidx.lifecycle.Lifecycle;

/* compiled from: LifecycleOwner.java */
/* renamed from: rz1  reason: default package */
/* loaded from: classes.dex */
public interface rz1 {
    Lifecycle getLifecycle();
}
