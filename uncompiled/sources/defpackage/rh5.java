package defpackage;

import android.content.SharedPreferences;
import android.util.Pair;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: rh5  reason: default package */
/* loaded from: classes.dex */
public final class rh5 {
    public final String a;
    public final String b;
    public final String c;
    public final long d;
    public final /* synthetic */ mi5 e;

    public /* synthetic */ rh5(mi5 mi5Var, String str, long j, lh5 lh5Var) {
        this.e = mi5Var;
        zt2.f("health_monitor");
        zt2.a(j > 0);
        this.a = "health_monitor:start";
        this.b = "health_monitor:count";
        this.c = "health_monitor:value";
        this.d = j;
    }

    public final void a(String str, long j) {
        this.e.e();
        if (d() == 0) {
            c();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.e.n().getLong(this.b, 0L);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = this.e.n().edit();
            edit.putString(this.c, str);
            edit.putLong(this.b, 1L);
            edit.apply();
            return;
        }
        long nextLong = this.e.a.G().i0().nextLong();
        long j3 = j2 + 1;
        SharedPreferences.Editor edit2 = this.e.n().edit();
        if ((Long.MAX_VALUE & nextLong) < Long.MAX_VALUE / j3) {
            edit2.putString(this.c, str);
        }
        edit2.putLong(this.b, j3);
        edit2.apply();
    }

    public final Pair<String, Long> b() {
        long abs;
        this.e.e();
        this.e.e();
        long d = d();
        if (d == 0) {
            c();
            abs = 0;
        } else {
            abs = Math.abs(d - this.e.a.a().a());
        }
        long j = this.d;
        if (abs < j) {
            return null;
        }
        if (abs > j + j) {
            c();
            return null;
        }
        String string = this.e.n().getString(this.c, null);
        long j2 = this.e.n().getLong(this.b, 0L);
        c();
        if (string != null && j2 > 0) {
            return new Pair<>(string, Long.valueOf(j2));
        }
        return mi5.x;
    }

    public final void c() {
        this.e.e();
        long a = this.e.a.a().a();
        SharedPreferences.Editor edit = this.e.n().edit();
        edit.remove(this.b);
        edit.remove(this.c);
        edit.putLong(this.a, a);
        edit.apply();
    }

    public final long d() {
        return this.e.n().getLong(this.a, 0L);
    }
}
