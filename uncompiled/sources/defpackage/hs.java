package defpackage;

import android.os.Bundle;
import android.os.IBinder;
import androidx.media3.common.util.b;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: BundleUtil.java */
/* renamed from: hs  reason: default package */
/* loaded from: classes.dex */
public final class hs {
    public static Method a;

    public static void a(Bundle bundle, String str, IBinder iBinder) {
        if (b.a >= 18) {
            bundle.putBinder(str, iBinder);
        } else {
            b(bundle, str, iBinder);
        }
    }

    public static void b(Bundle bundle, String str, IBinder iBinder) {
        Method method = a;
        if (method == null) {
            try {
                Method method2 = Bundle.class.getMethod("putIBinder", String.class, IBinder.class);
                a = method2;
                method2.setAccessible(true);
                method = a;
            } catch (NoSuchMethodException e) {
                p12.g("BundleUtil", "Failed to retrieve putIBinder method", e);
                return;
            }
        }
        try {
            method.invoke(bundle, str, iBinder);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
            p12.g("BundleUtil", "Failed to invoke putIBinder via reflection", e2);
        }
    }
}
