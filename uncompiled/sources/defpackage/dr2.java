package defpackage;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import com.facebook.common.references.a;

/* compiled from: PlatformDecoder.java */
/* renamed from: dr2  reason: default package */
/* loaded from: classes.dex */
public interface dr2 {
    a<Bitmap> a(zu0 zu0Var, Bitmap.Config config, Rect rect, int i);

    a<Bitmap> b(zu0 zu0Var, Bitmap.Config config, Rect rect, ColorSpace colorSpace);

    a<Bitmap> c(zu0 zu0Var, Bitmap.Config config, Rect rect, int i, ColorSpace colorSpace);
}
