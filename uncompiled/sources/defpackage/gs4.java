package defpackage;

import java.math.BigInteger;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;
import org.bouncycastle.asn1.s0;

/* renamed from: gs4  reason: default package */
/* loaded from: classes2.dex */
public class gs4 extends h {
    public final int a;
    public final byte[] f0;
    public final byte[] g0;
    public final byte[] h0;
    public final byte[] i0;
    public final byte[] j0;

    public gs4(int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5) {
        this.a = i;
        this.f0 = wh.e(bArr);
        this.g0 = wh.e(bArr2);
        this.h0 = wh.e(bArr3);
        this.i0 = wh.e(bArr4);
        this.j0 = wh.e(bArr5);
    }

    public gs4(h4 h4Var) {
        if (!g.z(h4Var.D(0)).B().equals(BigInteger.valueOf(0L))) {
            throw new IllegalArgumentException("unknown version of sequence");
        }
        if (h4Var.size() != 2 && h4Var.size() != 3) {
            throw new IllegalArgumentException("key sequence wrong size");
        }
        h4 z = h4.z(h4Var.D(1));
        this.a = g.z(z.D(0)).B().intValue();
        this.f0 = wh.e(f4.B(z.D(1)).D());
        this.g0 = wh.e(f4.B(z.D(2)).D());
        this.h0 = wh.e(f4.B(z.D(3)).D());
        this.i0 = wh.e(f4.B(z.D(4)).D());
        if (h4Var.size() == 3) {
            this.j0 = wh.e(f4.z(k4.z(h4Var.D(2)), true).D());
        } else {
            this.j0 = null;
        }
    }

    public static gs4 q(Object obj) {
        if (obj instanceof gs4) {
            return (gs4) obj;
        }
        if (obj != null) {
            return new gs4(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(0L));
        d4 d4Var2 = new d4();
        d4Var2.a(new g(this.a));
        d4Var2.a(new j0(this.f0));
        d4Var2.a(new j0(this.g0));
        d4Var2.a(new j0(this.h0));
        d4Var2.a(new j0(this.i0));
        d4Var.a(new n0(d4Var2));
        d4Var.a(new s0(true, 0, new j0(this.j0)));
        return new n0(d4Var);
    }

    public byte[] o() {
        return wh.e(this.j0);
    }

    public int p() {
        return this.a;
    }

    public byte[] s() {
        return wh.e(this.h0);
    }

    public byte[] t() {
        return wh.e(this.i0);
    }

    public byte[] w() {
        return wh.e(this.g0);
    }

    public byte[] y() {
        return wh.e(this.f0);
    }
}
