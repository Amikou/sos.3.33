package defpackage;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.facebook.common.internal.ImmutableList;
import com.facebook.common.references.a;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.imagepipeline.request.ImageRequest;
import defpackage.qc3;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: PipelineDraweeController.java */
/* renamed from: wq2  reason: default package */
/* loaded from: classes.dex */
public class wq2 extends w4<a<com.facebook.imagepipeline.image.a>, ao1> {
    public static final Class<?> M = wq2.class;
    public final l72<wt, com.facebook.imagepipeline.image.a> A;
    public wt B;
    public fw3<ge0<a<com.facebook.imagepipeline.image.a>>> C;
    public boolean D;
    public ImmutableList<uq0> E;
    public mo1 F;
    public Set<h73> G;
    public fo1 H;
    public df0 I;
    public ImageRequest J;
    public ImageRequest[] K;
    public ImageRequest L;
    public final uq0 y;
    public final ImmutableList<uq0> z;

    public wq2(Resources resources, tl0 tl0Var, uq0 uq0Var, Executor executor, l72<wt, com.facebook.imagepipeline.image.a> l72Var, ImmutableList<uq0> immutableList) {
        super(tl0Var, executor, null, null);
        this.y = new ti0(resources, uq0Var);
        this.z = immutableList;
        this.A = l72Var;
    }

    public void A0(ImmutableList<uq0> immutableList) {
        this.E = immutableList;
    }

    @Override // defpackage.w4
    public Uri B() {
        return ra2.a(this.J, this.L, this.K, ImageRequest.y);
    }

    public void B0(boolean z) {
        this.D = z;
    }

    public void C0(com.facebook.imagepipeline.image.a aVar, ye0 ye0Var) {
        oc3 a;
        ye0Var.i(x());
        jr0 d = d();
        qc3.b bVar = null;
        if (d != null && (a = qc3.a(d.f())) != null) {
            bVar = a.s();
        }
        ye0Var.m(bVar);
        int b = this.I.b();
        ye0Var.l(ho1.b(b), cf0.a(b));
        if (aVar != null) {
            ye0Var.j(aVar.getWidth(), aVar.getHeight());
            ye0Var.k(aVar.b());
            return;
        }
        ye0Var.h();
    }

    @Override // defpackage.w4
    public void P(Drawable drawable) {
        if (drawable instanceof fr0) {
            ((fr0) drawable).a();
        }
    }

    @Override // defpackage.w4, defpackage.ir0
    public void e(jr0 jr0Var) {
        super.e(jr0Var);
        u0(null);
    }

    public synchronized void i0(fo1 fo1Var) {
        fo1 fo1Var2 = this.H;
        if (fo1Var2 instanceof f91) {
            ((f91) fo1Var2).b(fo1Var);
        } else if (fo1Var2 != null) {
            this.H = new f91(fo1Var2, fo1Var);
        } else {
            this.H = fo1Var;
        }
    }

    public synchronized void j0(h73 h73Var) {
        if (this.G == null) {
            this.G = new HashSet();
        }
        this.G.add(h73Var);
    }

    public void k0() {
        synchronized (this) {
            this.H = null;
        }
    }

    @Override // defpackage.w4
    /* renamed from: l0 */
    public Drawable n(a<com.facebook.imagepipeline.image.a> aVar) {
        try {
            if (nc1.d()) {
                nc1.a("PipelineDraweeController#createDrawable");
            }
            xt2.i(a.u(aVar));
            com.facebook.imagepipeline.image.a j = aVar.j();
            u0(j);
            Drawable t0 = t0(this.E, j);
            if (t0 != null) {
                return t0;
            }
            Drawable t02 = t0(this.z, j);
            if (t02 != null) {
                if (nc1.d()) {
                    nc1.b();
                }
                return t02;
            }
            Drawable b = this.y.b(j);
            if (b != null) {
                if (nc1.d()) {
                    nc1.b();
                }
                return b;
            }
            throw new UnsupportedOperationException("Unrecognized image class: " + j);
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    @Override // defpackage.w4
    /* renamed from: m0 */
    public a<com.facebook.imagepipeline.image.a> p() {
        wt wtVar;
        if (nc1.d()) {
            nc1.a("PipelineDraweeController#getCachedImage");
        }
        try {
            l72<wt, com.facebook.imagepipeline.image.a> l72Var = this.A;
            if (l72Var != null && (wtVar = this.B) != null) {
                a<com.facebook.imagepipeline.image.a> aVar = l72Var.get(wtVar);
                if (aVar != null && !aVar.j().a().a()) {
                    aVar.close();
                    return null;
                }
                if (nc1.d()) {
                    nc1.b();
                }
                return aVar;
            }
            if (nc1.d()) {
                nc1.b();
            }
            return null;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    @Override // defpackage.w4
    /* renamed from: n0 */
    public int z(a<com.facebook.imagepipeline.image.a> aVar) {
        if (aVar != null) {
            return aVar.l();
        }
        return 0;
    }

    @Override // defpackage.w4
    /* renamed from: o0 */
    public ao1 A(a<com.facebook.imagepipeline.image.a> aVar) {
        xt2.i(a.u(aVar));
        return aVar.j();
    }

    public synchronized h73 p0() {
        go1 go1Var = this.H != null ? new go1(x(), this.H) : null;
        Set<h73> set = this.G;
        if (set != null) {
            k91 k91Var = new k91(set);
            if (go1Var != null) {
                k91Var.l(go1Var);
            }
            return k91Var;
        }
        return go1Var;
    }

    public final void q0(fw3<ge0<a<com.facebook.imagepipeline.image.a>>> fw3Var) {
        this.C = fw3Var;
        u0(null);
    }

    public void r0(fw3<ge0<a<com.facebook.imagepipeline.image.a>>> fw3Var, String str, wt wtVar, Object obj, ImmutableList<uq0> immutableList, fo1 fo1Var) {
        if (nc1.d()) {
            nc1.a("PipelineDraweeController#initialize");
        }
        super.E(str, obj);
        q0(fw3Var);
        this.B = wtVar;
        A0(immutableList);
        k0();
        u0(null);
        i0(fo1Var);
        if (nc1.d()) {
            nc1.b();
        }
    }

    public synchronized void s0(ko1 ko1Var, AbstractDraweeControllerBuilder<xq2, ImageRequest, a<com.facebook.imagepipeline.image.a>, ao1> abstractDraweeControllerBuilder, fw3<Boolean> fw3Var) {
        mo1 mo1Var = this.F;
        if (mo1Var != null) {
            mo1Var.f();
        }
        if (ko1Var != null) {
            if (this.F == null) {
                this.F = new mo1(AwakeTimeSinceBootClock.get(), this, fw3Var);
            }
            this.F.c(ko1Var);
            this.F.g(true);
            this.F.i(abstractDraweeControllerBuilder);
        }
        this.J = abstractDraweeControllerBuilder.n();
        this.K = abstractDraweeControllerBuilder.m();
        this.L = abstractDraweeControllerBuilder.o();
    }

    public final Drawable t0(ImmutableList<uq0> immutableList, com.facebook.imagepipeline.image.a aVar) {
        Drawable b;
        if (immutableList == null) {
            return null;
        }
        Iterator<uq0> it = immutableList.iterator();
        while (it.hasNext()) {
            uq0 next = it.next();
            if (next.a(aVar) && (b = next.b(aVar)) != null) {
                return b;
            }
        }
        return null;
    }

    @Override // defpackage.w4
    public String toString() {
        return ol2.c(this).b("super", super.toString()).b("dataSourceSupplier", this.C).toString();
    }

    @Override // defpackage.w4
    public ge0<a<com.facebook.imagepipeline.image.a>> u() {
        if (nc1.d()) {
            nc1.a("PipelineDraweeController#getDataSource");
        }
        if (v11.m(2)) {
            v11.o(M, "controller %x: getDataSource", Integer.valueOf(System.identityHashCode(this)));
        }
        ge0<a<com.facebook.imagepipeline.image.a>> ge0Var = this.C.get();
        if (nc1.d()) {
            nc1.b();
        }
        return ge0Var;
    }

    public final void u0(com.facebook.imagepipeline.image.a aVar) {
        if (this.D) {
            if (t() == null) {
                ye0 ye0Var = new ye0();
                bo1 bo1Var = new bo1(ye0Var);
                this.I = new df0();
                l(bo1Var);
                a0(ye0Var);
            }
            if (this.H == null) {
                i0(this.I);
            }
            if (t() instanceof ye0) {
                C0(aVar, (ye0) t());
            }
        }
    }

    @Override // defpackage.w4
    /* renamed from: v0 */
    public Map<String, Object> K(ao1 ao1Var) {
        if (ao1Var == null) {
            return null;
        }
        return ao1Var.getExtras();
    }

    @Override // defpackage.w4
    /* renamed from: w0 */
    public void M(String str, a<com.facebook.imagepipeline.image.a> aVar) {
        super.M(str, aVar);
        synchronized (this) {
            fo1 fo1Var = this.H;
            if (fo1Var != null) {
                fo1Var.a(str, 6, true, "PipelineDraweeController");
            }
        }
    }

    @Override // defpackage.w4
    /* renamed from: x0 */
    public void R(a<com.facebook.imagepipeline.image.a> aVar) {
        a.g(aVar);
    }

    public synchronized void y0(fo1 fo1Var) {
        fo1 fo1Var2 = this.H;
        if (fo1Var2 instanceof f91) {
            ((f91) fo1Var2).c(fo1Var);
            return;
        }
        if (fo1Var2 == fo1Var) {
            this.H = null;
        }
    }

    public synchronized void z0(h73 h73Var) {
        Set<h73> set = this.G;
        if (set == null) {
            return;
        }
        set.remove(h73Var);
    }
}
