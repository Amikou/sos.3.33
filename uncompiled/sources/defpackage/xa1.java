package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSelectCurrencyBinding.java */
/* renamed from: xa1  reason: default package */
/* loaded from: classes2.dex */
public final class xa1 {
    public final AppCompatEditText a;
    public final ProgressBar b;
    public final RecyclerView c;
    public final SwipeRefreshLayout d;
    public final g74 e;

    public xa1(ConstraintLayout constraintLayout, AppCompatEditText appCompatEditText, ProgressBar progressBar, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, g74 g74Var, LinearLayout linearLayout) {
        this.a = appCompatEditText;
        this.b = progressBar;
        this.c = recyclerView;
        this.d = swipeRefreshLayout;
        this.e = g74Var;
    }

    public static xa1 a(View view) {
        int i = R.id.etSearch;
        AppCompatEditText appCompatEditText = (AppCompatEditText) ai4.a(view, R.id.etSearch);
        if (appCompatEditText != null) {
            i = R.id.pbTokenList;
            ProgressBar progressBar = (ProgressBar) ai4.a(view, R.id.pbTokenList);
            if (progressBar != null) {
                i = R.id.rvToken;
                RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvToken);
                if (recyclerView != null) {
                    i = R.id.swipeRefreshLayout;
                    SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swipeRefreshLayout);
                    if (swipeRefreshLayout != null) {
                        i = R.id.toolbar;
                        View a = ai4.a(view, R.id.toolbar);
                        if (a != null) {
                            g74 a2 = g74.a(a);
                            i = R.id.vSearchContainer;
                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vSearchContainer);
                            if (linearLayout != null) {
                                return new xa1((ConstraintLayout) view, appCompatEditText, progressBar, recyclerView, swipeRefreshLayout, a2, linearLayout);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
