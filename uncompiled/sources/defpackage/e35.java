package defpackage;

import com.google.android.gms.common.api.GoogleApiClient;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: e35  reason: default package */
/* loaded from: classes.dex */
public class e35 extends GoogleApiClient {
    public final String b;

    public e35(String str) {
        this.b = str;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void connect() {
        throw new UnsupportedOperationException(this.b);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void disconnect() {
        throw new UnsupportedOperationException(this.b);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void e(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public boolean h() {
        throw new UnsupportedOperationException(this.b);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void i(GoogleApiClient.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public void j(GoogleApiClient.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }
}
