package defpackage;

import android.app.ActivityManager;
import android.os.Build;
import java.util.concurrent.TimeUnit;

/* compiled from: DefaultBitmapMemoryCacheParamsSupplier.java */
/* renamed from: di0  reason: default package */
/* loaded from: classes.dex */
public class di0 implements fw3<m72> {
    public static final long b = TimeUnit.MINUTES.toMillis(5);
    public final ActivityManager a;

    public di0(ActivityManager activityManager) {
        this.a = activityManager;
    }

    @Override // defpackage.fw3
    /* renamed from: a */
    public m72 get() {
        return new m72(b(), 256, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, b);
    }

    public final int b() {
        int min = Math.min(this.a.getMemoryClass() * 1048576, Integer.MAX_VALUE);
        if (min < 33554432) {
            return 4194304;
        }
        if (min < 67108864) {
            return 6291456;
        }
        if (Build.VERSION.SDK_INT < 11) {
            return 8388608;
        }
        return min / 4;
    }
}
