package defpackage;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import defpackage.hb4;

/* compiled from: ImageViewTarget.java */
/* renamed from: ep1  reason: default package */
/* loaded from: classes.dex */
public abstract class ep1<Z> extends ek4<ImageView, Z> implements hb4.a {
    public Animatable k0;

    public ep1(ImageView imageView) {
        super(imageView);
    }

    @Override // defpackage.yn, defpackage.pz1
    public void b() {
        Animatable animatable = this.k0;
        if (animatable != null) {
            animatable.start();
        }
    }

    @Override // defpackage.yn, defpackage.i34
    public void g(Drawable drawable) {
        super.g(drawable);
        s(null);
        q(drawable);
    }

    @Override // defpackage.yn, defpackage.pz1
    public void h() {
        Animatable animatable = this.k0;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @Override // defpackage.i34
    public void j(Z z, hb4<? super Z> hb4Var) {
        if (hb4Var != null && hb4Var.a(z, this)) {
            p(z);
        } else {
            s(z);
        }
    }

    @Override // defpackage.ek4, defpackage.yn, defpackage.i34
    public void k(Drawable drawable) {
        super.k(drawable);
        s(null);
        q(drawable);
    }

    @Override // defpackage.ek4, defpackage.yn, defpackage.i34
    public void m(Drawable drawable) {
        super.m(drawable);
        Animatable animatable = this.k0;
        if (animatable != null) {
            animatable.stop();
        }
        s(null);
        q(drawable);
    }

    public final void p(Z z) {
        if (z instanceof Animatable) {
            Animatable animatable = (Animatable) z;
            this.k0 = animatable;
            animatable.start();
            return;
        }
        this.k0 = null;
    }

    public void q(Drawable drawable) {
        ((ImageView) this.a).setImageDrawable(drawable);
    }

    public abstract void r(Z z);

    public final void s(Z z) {
        r(z);
        p(z);
    }
}
