package defpackage;

import android.content.Context;
import android.net.Uri;
import com.onesignal.k0;
import org.json.JSONObject;

/* compiled from: OSNotificationOpenBehaviorFromPushPayload.kt */
/* renamed from: bk2  reason: default package */
/* loaded from: classes2.dex */
public final class bk2 {
    public final Context a;
    public final JSONObject b;

    public bk2(Context context, JSONObject jSONObject) {
        fs1.f(context, "context");
        fs1.f(jSONObject, "fcmPayload");
        this.a = context;
        this.b = jSONObject;
    }

    public final boolean a() {
        return k0.a.a(this.a) && b() == null;
    }

    public final Uri b() {
        k0 k0Var = k0.a;
        if (k0Var.a(this.a) && !k0Var.b(this.a)) {
            JSONObject jSONObject = new JSONObject(this.b.optString("custom"));
            if (jSONObject.has("u")) {
                String optString = jSONObject.optString("u");
                if (!fs1.b(optString, "")) {
                    fs1.e(optString, "url");
                    int length = optString.length() - 1;
                    int i = 0;
                    boolean z = false;
                    while (i <= length) {
                        boolean z2 = fs1.h(optString.charAt(!z ? i : length), 32) <= 0;
                        if (z) {
                            if (!z2) {
                                break;
                            }
                            length--;
                        } else if (z2) {
                            i++;
                        } else {
                            z = true;
                        }
                    }
                    return Uri.parse(optString.subSequence(i, length + 1).toString());
                }
            }
            return null;
        }
        return null;
    }
}
