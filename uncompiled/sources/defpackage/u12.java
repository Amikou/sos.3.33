package defpackage;

import java.io.Writer;

/* compiled from: LogWriter.java */
/* renamed from: u12  reason: default package */
/* loaded from: classes.dex */
public final class u12 extends Writer {
    public final String a;
    public StringBuilder f0 = new StringBuilder(128);

    public u12(String str) {
        this.a = str;
    }

    public final void a() {
        if (this.f0.length() > 0) {
            this.f0.toString();
            StringBuilder sb = this.f0;
            sb.delete(0, sb.length());
        }
    }

    @Override // java.io.Writer, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        a();
    }

    @Override // java.io.Writer, java.io.Flushable
    public void flush() {
        a();
    }

    @Override // java.io.Writer
    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == '\n') {
                a();
            } else {
                this.f0.append(c);
            }
        }
    }
}
