package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: k36  reason: default package */
/* loaded from: classes.dex */
public final class k36 extends c55 {
    public k36(u46 u46Var, String str) {
        super("unmonitored");
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        return this;
    }
}
