package defpackage;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSwapTransactionSpeedBinding.java */
/* renamed from: io0  reason: default package */
/* loaded from: classes2.dex */
public final class io0 {
    public final MaterialButton a;
    public final RadioButton b;
    public final RadioButton c;
    public final RadioButton d;
    public final MaterialButton e;

    public io0(ConstraintLayout constraintLayout, MaterialButton materialButton, RadioButton radioButton, RadioButton radioButton2, RadioButton radioButton3, MaterialButton materialButton2, MaterialTextView materialTextView, RadioGroup radioGroup, MaterialTextView materialTextView2) {
        this.a = materialButton;
        this.b = radioButton;
        this.c = radioButton2;
        this.d = radioButton3;
        this.e = materialButton2;
    }

    public static io0 a(View view) {
        int i = R.id.btnConfirm;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConfirm);
        if (materialButton != null) {
            i = R.id.chipTsFast;
            RadioButton radioButton = (RadioButton) ai4.a(view, R.id.chipTsFast);
            if (radioButton != null) {
                i = R.id.chipTsLight;
                RadioButton radioButton2 = (RadioButton) ai4.a(view, R.id.chipTsLight);
                if (radioButton2 != null) {
                    i = R.id.chipTsStandard;
                    RadioButton radioButton3 = (RadioButton) ai4.a(view, R.id.chipTsStandard);
                    if (radioButton3 != null) {
                        i = R.id.dialog_cross;
                        MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                        if (materialButton2 != null) {
                            i = R.id.dialog_title;
                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_title);
                            if (materialTextView != null) {
                                i = R.id.easyTspageGroup;
                                RadioGroup radioGroup = (RadioGroup) ai4.a(view, R.id.easyTspageGroup);
                                if (radioGroup != null) {
                                    i = R.id.txtInfo;
                                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtInfo);
                                    if (materialTextView2 != null) {
                                        return new io0((ConstraintLayout) view, materialButton, radioButton, radioButton2, radioButton3, materialButton2, materialTextView, radioGroup, materialTextView2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
