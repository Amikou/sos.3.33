package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: p36  reason: default package */
/* loaded from: classes.dex */
public final class p36 implements o36 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.client.reject_blank_user_id", true);

    @Override // defpackage.o36
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
