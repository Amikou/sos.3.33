package defpackage;

import io.reactivex.BackpressureStrategy;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;

/* compiled from: Flowable.java */
/* renamed from: q71  reason: default package */
/* loaded from: classes2.dex */
public abstract class q71<T> implements nw2<T> {
    public static final int a = Math.max(1, Integer.getInteger("rx2.buffer-size", 128).intValue());

    public static int a() {
        return a;
    }

    public static <T> q71<T> b(nw2<? extends T> nw2Var, nw2<? extends T> nw2Var2) {
        il2.a(nw2Var, "source1 is null");
        il2.a(nw2Var2, "source2 is null");
        return c(nw2Var, nw2Var2);
    }

    public static <T> q71<T> c(Publisher<? extends T>... publisherArr) {
        if (publisherArr.length == 0) {
            return f();
        }
        if (publisherArr.length == 1) {
            return o(publisherArr[0]);
        }
        return da3.h(new r71(publisherArr, false));
    }

    public static <T> q71<T> d(d81<T> d81Var, BackpressureStrategy backpressureStrategy) {
        il2.a(d81Var, "source is null");
        il2.a(backpressureStrategy, "mode is null");
        return da3.h(new s71(d81Var, backpressureStrategy));
    }

    public static <T> q71<T> e(Callable<? extends nw2<? extends T>> callable) {
        il2.a(callable, "supplier is null");
        return da3.h(new t71(callable));
    }

    public static <T> q71<T> f() {
        return da3.h(v71.b);
    }

    public static <T> q71<T> g(Throwable th) {
        il2.a(th, "throwable is null");
        return h(ae1.a(th));
    }

    public static <T> q71<T> h(Callable<? extends Throwable> callable) {
        il2.a(callable, "errorSupplier is null");
        return da3.h(new w71(callable));
    }

    public static <T> q71<T> n(Callable<? extends T> callable) {
        il2.a(callable, "supplier is null");
        return da3.h(new a81(callable));
    }

    public static <T> q71<T> o(nw2<? extends T> nw2Var) {
        if (nw2Var instanceof q71) {
            return da3.h((q71) nw2Var);
        }
        il2.a(nw2Var, "publisher is null");
        return da3.h(new b81(nw2Var));
    }

    public final q71<T> i(eu2<? super T> eu2Var) {
        il2.a(eu2Var, "predicate is null");
        return da3.h(new x71(this, eu2Var));
    }

    public final <R> q71<R> j(ld1<? super T, ? extends nw2<? extends R>> ld1Var) {
        return k(ld1Var, false, a(), a());
    }

    public final <R> q71<R> k(ld1<? super T, ? extends nw2<? extends R>> ld1Var, boolean z, int i, int i2) {
        il2.a(ld1Var, "mapper is null");
        il2.b(i, "maxConcurrency");
        il2.b(i2, "bufferSize");
        if (this instanceof gc3) {
            Object call = ((gc3) this).call();
            if (call == null) {
                return f();
            }
            return e81.a(call, ld1Var);
        }
        return da3.h(new y71(this, ld1Var, z, i, i2));
    }

    public final <U> q71<U> l(ld1<? super T, ? extends Iterable<? extends U>> ld1Var) {
        return m(ld1Var, a());
    }

    public final <U> q71<U> m(ld1<? super T, ? extends Iterable<? extends U>> ld1Var, int i) {
        il2.a(ld1Var, "mapper is null");
        il2.b(i, "bufferSize");
        return da3.h(new z71(this, ld1Var, i));
    }

    public final <R> q71<R> p(ld1<? super T, ? extends R> ld1Var) {
        il2.a(ld1Var, "mapper is null");
        return da3.h(new c81(this, ld1Var));
    }

    public final q71<T> q(bd3 bd3Var) {
        il2.a(bd3Var, "scheduler is null");
        return r(bd3Var, !(this instanceof s71));
    }

    public final q71<T> r(bd3 bd3Var, boolean z) {
        il2.a(bd3Var, "scheduler is null");
        return da3.h(new f81(this, bd3Var, z));
    }
}
