package defpackage;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: qz4  reason: default package */
/* loaded from: classes.dex */
public final class qz4 implements j05 {
    public final i05 a;
    public boolean b = false;

    public qz4(i05 i05Var) {
        this.a = i05Var;
    }

    @Override // defpackage.j05
    public final void a() {
    }

    @Override // defpackage.j05
    public final void b(ConnectionResult connectionResult, a<?> aVar, boolean z) {
    }

    @Override // defpackage.j05
    public final void c() {
        if (this.b) {
            this.b = false;
            this.a.k(new sz4(this, this));
        }
    }

    @Override // defpackage.j05
    public final void d(Bundle bundle) {
    }

    @Override // defpackage.j05
    public final void e(int i) {
        this.a.i(null);
        this.a.n.b(i, this.b);
    }

    @Override // defpackage.j05
    public final boolean f() {
        if (this.b) {
            return false;
        }
        Set<j15> set = this.a.m.w;
        if (set != null && !set.isEmpty()) {
            this.b = true;
            for (j15 j15Var : set) {
                j15Var.c();
            }
            return false;
        }
        this.a.i(null);
        return true;
    }

    @Override // defpackage.j05
    public final <A extends a.b, T extends b<? extends l83, A>> T g(T t) {
        try {
            this.a.m.x.b(t);
            d05 d05Var = this.a.m;
            a.f fVar = d05Var.o.get(t.s());
            zt2.k(fVar, "Appropriate Api was not requested.");
            if (!fVar.c() && this.a.g.containsKey(t.s())) {
                t.w(new Status(17));
            } else {
                t.u(fVar);
            }
        } catch (DeadObjectException unused) {
            this.a.k(new pz4(this, this));
        }
        return t;
    }

    public final void i() {
        if (this.b) {
            this.b = false;
            this.a.m.x.a();
            f();
        }
    }
}
