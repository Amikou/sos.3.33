package defpackage;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;

/* compiled from: UnitModelLoader.java */
/* renamed from: ve4  reason: default package */
/* loaded from: classes.dex */
public class ve4<Model> implements j92<Model, Model> {
    public static final ve4<?> a = new ve4<>();

    /* compiled from: UnitModelLoader.java */
    /* renamed from: ve4$a */
    /* loaded from: classes.dex */
    public static class a<Model> implements k92<Model, Model> {
        public static final a<?> a = new a<>();

        public static <T> a<T> b() {
            return (a<T>) a;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Model, Model> c(qa2 qa2Var) {
            return ve4.c();
        }
    }

    /* compiled from: UnitModelLoader.java */
    /* renamed from: ve4$b */
    /* loaded from: classes.dex */
    public static class b<Model> implements d<Model> {
        public final Model a;

        public b(Model model) {
            this.a = model;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<Model> a() {
            return (Class<Model>) this.a.getClass();
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super Model> aVar) {
            aVar.f((Model) this.a);
        }
    }

    public static <T> ve4<T> c() {
        return (ve4<T>) a;
    }

    @Override // defpackage.j92
    public boolean a(Model model) {
        return true;
    }

    @Override // defpackage.j92
    public j92.a<Model> b(Model model, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(model), new b(model));
    }
}
