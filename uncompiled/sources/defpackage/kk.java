package defpackage;

import com.google.android.datatransport.cct.internal.h;
import com.google.android.datatransport.cct.internal.j;
import java.util.List;
import java.util.Objects;

/* compiled from: AutoValue_BatchedLogRequest.java */
/* renamed from: kk  reason: default package */
/* loaded from: classes.dex */
public final class kk extends h {
    public final List<j> a;

    public kk(List<j> list) {
        Objects.requireNonNull(list, "Null logRequests");
        this.a = list;
    }

    @Override // com.google.android.datatransport.cct.internal.h
    public List<j> c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof h) {
            return this.a.equals(((h) obj).c());
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    public String toString() {
        return "BatchedLogRequest{logRequests=" + this.a + "}";
    }
}
