package defpackage;

/* compiled from: JsonStreamContext.java */
/* renamed from: cw1  reason: default package */
/* loaded from: classes.dex */
public abstract class cw1 {
    public int a;
    public int b;

    public final int a() {
        int i = this.b;
        if (i < 0) {
            return 0;
        }
        return i;
    }

    public abstract String b();

    public final int c() {
        return this.b + 1;
    }

    public abstract cw1 d();

    public final boolean e() {
        return this.a == 1;
    }

    public final boolean f() {
        return this.a == 2;
    }

    public final boolean g() {
        return this.a == 0;
    }

    public abstract void h(Object obj);

    public String i() {
        int i = this.a;
        return i != 0 ? i != 1 ? i != 2 ? "?" : "Object" : "Array" : "root";
    }
}
