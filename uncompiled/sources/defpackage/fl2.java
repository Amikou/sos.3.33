package defpackage;

/* compiled from: ObjectArrays.java */
/* renamed from: fl2  reason: default package */
/* loaded from: classes2.dex */
public final class fl2 {
    public static Object a(Object obj, int i) {
        if (obj != null) {
            return obj;
        }
        StringBuilder sb = new StringBuilder(20);
        sb.append("at index ");
        sb.append(i);
        throw new NullPointerException(sb.toString());
    }

    public static Object[] b(Object... objArr) {
        c(objArr, objArr.length);
        return objArr;
    }

    public static Object[] c(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            a(objArr[i2], i2);
        }
        return objArr;
    }

    public static <T> T[] d(T[] tArr, int i) {
        return (T[]) ar2.b(tArr, i);
    }

    public static <T> T[] e(Object[] objArr, int i, int i2, T[] tArr) {
        au2.o(i, i + i2, objArr.length);
        if (tArr.length < i2) {
            tArr = (T[]) d(tArr, i2);
        } else if (tArr.length > i2) {
            tArr[i2] = null;
        }
        System.arraycopy(objArr, i, tArr, 0, i2);
        return tArr;
    }
}
