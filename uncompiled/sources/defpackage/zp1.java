package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: IncludeMoneyBagMenuItemBinding.java */
/* renamed from: zp1  reason: default package */
/* loaded from: classes2.dex */
public final class zp1 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final MaterialButton c;

    public zp1(LinearLayoutCompat linearLayoutCompat, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, AppCompatTextView appCompatTextView) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = materialButton3;
    }

    public static zp1 a(View view) {
        int i = R.id.btnBag;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBag);
        if (materialButton != null) {
            i = R.id.btnPercentageOfBalance;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPercentageOfBalance);
            if (materialButton2 != null) {
                i = R.id.btnPercentageOfTraditional;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnPercentageOfTraditional);
                if (materialButton3 != null) {
                    i = R.id.txtTitle;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtTitle);
                    if (appCompatTextView != null) {
                        return new zp1((LinearLayoutCompat) view, materialButton, materialButton2, materialButton3, appCompatTextView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
