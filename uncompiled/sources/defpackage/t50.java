package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;

/* compiled from: ConnectedInfoDao.kt */
/* renamed from: t50  reason: default package */
/* loaded from: classes2.dex */
public interface t50 {
    Object a(RoomConnectedInfo roomConnectedInfo, q70<? super te4> q70Var);

    Object b(String str, q70<? super te4> q70Var);

    LiveData<List<RoomConnectedInfoAndWallet>> c();
}
