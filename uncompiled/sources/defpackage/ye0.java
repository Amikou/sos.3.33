package defpackage;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import defpackage.qc3;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: DebugControllerOverlayDrawable.java */
/* renamed from: ye0  reason: default package */
/* loaded from: classes.dex */
public class ye0 extends Drawable implements co1 {
    public String a;
    public String f0;
    public int g0;
    public int h0;
    public int i0;
    public String j0;
    public qc3.b k0;
    public int m0;
    public int n0;
    public int t0;
    public int u0;
    public int v0;
    public int w0;
    public int x0;
    public long y0;
    public String z0;
    public HashMap<String, String> l0 = new HashMap<>();
    public int o0 = 80;
    public final Paint p0 = new Paint(1);
    public final Matrix q0 = new Matrix();
    public final Rect r0 = new Rect();
    public final RectF s0 = new RectF();
    public int A0 = -1;
    public int B0 = 0;

    public ye0() {
        h();
    }

    public static String f(String str, Object... objArr) {
        return objArr == null ? str : String.format(Locale.US, str, objArr);
    }

    @Override // defpackage.co1
    public void a(long j) {
        this.y0 = j;
        invalidateSelf();
    }

    public final void b(Canvas canvas, String str, Object obj) {
        d(canvas, str, String.valueOf(obj), -1);
    }

    public final void c(Canvas canvas, String str, String str2) {
        d(canvas, str, str2, -1);
    }

    public final void d(Canvas canvas, String str, String str2, int i) {
        String str3 = str + ": ";
        float measureText = this.p0.measureText(str3);
        float measureText2 = this.p0.measureText(str2);
        this.p0.setColor(1711276032);
        int i2 = this.w0;
        int i3 = this.x0;
        canvas.drawRect(i2 - 4, i3 + 8, i2 + measureText + measureText2 + 4.0f, i3 + this.v0 + 8, this.p0);
        this.p0.setColor(-1);
        canvas.drawText(str3, this.w0, this.x0, this.p0);
        this.p0.setColor(i);
        canvas.drawText(str2, this.w0 + measureText, this.x0, this.p0);
        this.x0 += this.v0;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        this.p0.setStyle(Paint.Style.STROKE);
        this.p0.setStrokeWidth(2.0f);
        this.p0.setColor(-26624);
        canvas.drawRect(bounds.left, bounds.top, bounds.right, bounds.bottom, this.p0);
        this.p0.setStyle(Paint.Style.FILL);
        this.p0.setColor(this.B0);
        canvas.drawRect(bounds.left, bounds.top, bounds.right, bounds.bottom, this.p0);
        this.p0.setStyle(Paint.Style.FILL);
        this.p0.setStrokeWidth(Utils.FLOAT_EPSILON);
        this.p0.setColor(-1);
        this.w0 = this.t0;
        this.x0 = this.u0;
        String str = this.f0;
        if (str != null) {
            c(canvas, "IDs", f("%s, %s", this.a, str));
        } else {
            c(canvas, "ID", this.a);
        }
        c(canvas, "D", f("%dx%d", Integer.valueOf(bounds.width()), Integer.valueOf(bounds.height())));
        d(canvas, "I", f("%dx%d", Integer.valueOf(this.g0), Integer.valueOf(this.h0)), e(this.g0, this.h0, this.k0));
        c(canvas, "I", f("%d KiB", Integer.valueOf(this.i0 / RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE)));
        String str2 = this.j0;
        if (str2 != null) {
            c(canvas, "i format", str2);
        }
        int i = this.m0;
        if (i > 0) {
            c(canvas, "anim", f("f %d, l %d", Integer.valueOf(i), Integer.valueOf(this.n0)));
        }
        qc3.b bVar = this.k0;
        if (bVar != null) {
            b(canvas, "scale", bVar);
        }
        long j = this.y0;
        if (j >= 0) {
            c(canvas, "t", f("%d ms", Long.valueOf(j)));
        }
        String str3 = this.z0;
        if (str3 != null) {
            d(canvas, "origin", str3, this.A0);
        }
        for (Map.Entry<String, String> entry : this.l0.entrySet()) {
            c(canvas, entry.getKey(), entry.getValue());
        }
    }

    public int e(int i, int i2, qc3.b bVar) {
        int width = getBounds().width();
        int height = getBounds().height();
        if (width > 0 && height > 0 && i > 0 && i2 > 0) {
            if (bVar != null) {
                Rect rect = this.r0;
                rect.top = 0;
                rect.left = 0;
                rect.right = width;
                rect.bottom = height;
                this.q0.reset();
                bVar.a(this.q0, this.r0, i, i2, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
                RectF rectF = this.s0;
                rectF.top = Utils.FLOAT_EPSILON;
                rectF.left = Utils.FLOAT_EPSILON;
                rectF.right = i;
                rectF.bottom = i2;
                this.q0.mapRect(rectF);
                width = Math.min(width, (int) this.s0.width());
                height = Math.min(height, (int) this.s0.height());
            }
            float f = width;
            float f2 = f * 0.1f;
            float f3 = f * 0.5f;
            float f4 = height;
            float f5 = 0.1f * f4;
            float f6 = f4 * 0.5f;
            int abs = Math.abs(i - width);
            int abs2 = Math.abs(i2 - height);
            float f7 = abs;
            if (f7 < f2 && abs2 < f5) {
                return -16711936;
            }
            if (f7 < f3 && abs2 < f6) {
                return -256;
            }
        }
        return -65536;
    }

    public final void g(Rect rect, int i, int i2) {
        int min = Math.min(40, Math.max(10, Math.min(rect.width() / i2, rect.height() / i)));
        this.p0.setTextSize(min);
        int i3 = min + 8;
        this.v0 = i3;
        int i4 = this.o0;
        if (i4 == 80) {
            this.v0 = i3 * (-1);
        }
        this.t0 = rect.left + 10;
        this.u0 = i4 == 80 ? rect.bottom - 10 : rect.top + 10 + 10;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public void h() {
        this.g0 = -1;
        this.h0 = -1;
        this.i0 = -1;
        this.l0 = new HashMap<>();
        this.m0 = -1;
        this.n0 = -1;
        this.j0 = null;
        i(null);
        this.y0 = -1L;
        this.z0 = null;
        this.A0 = -1;
        invalidateSelf();
    }

    public void i(String str) {
        if (str == null) {
            str = "none";
        }
        this.a = str;
        invalidateSelf();
    }

    public void j(int i, int i2) {
        this.g0 = i;
        this.h0 = i2;
        invalidateSelf();
    }

    public void k(int i) {
        this.i0 = i;
    }

    public void l(String str, int i) {
        this.z0 = str;
        this.A0 = i;
        invalidateSelf();
    }

    public void m(qc3.b bVar) {
        this.k0 = bVar;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        g(rect, 9, 8);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }
}
