package defpackage;

import java.security.GeneralSecurityException;

/* compiled from: DeterministicAeadConfig.java */
/* renamed from: tm0  reason: default package */
/* loaded from: classes2.dex */
public final class tm0 {
    static {
        new pa().c();
        i63.D();
        i63.D();
        try {
            a();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    @Deprecated
    public static void a() throws GeneralSecurityException {
        b();
    }

    public static void b() throws GeneralSecurityException {
        pa.n(true);
        um0.e();
    }
}
