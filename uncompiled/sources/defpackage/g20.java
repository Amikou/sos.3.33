package defpackage;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.RandomAccess;

/* compiled from: MutableCollections.kt */
/* renamed from: g20  reason: default package */
/* loaded from: classes2.dex */
public class g20 extends f20 {
    public static final <T> T A(List<T> list) {
        fs1.f(list, "$this$removeLast");
        if (list.isEmpty()) {
            throw new NoSuchElementException("List is empty.");
        }
        return list.remove(b20.i(list));
    }

    public static final <T> boolean B(Iterable<? extends T> iterable, tc1<? super T, Boolean> tc1Var) {
        fs1.f(iterable, "$this$retainAll");
        fs1.f(tc1Var, "predicate");
        return x(iterable, tc1Var, false);
    }

    public static final <T> boolean v(Collection<? super T> collection, Iterable<? extends T> iterable) {
        fs1.f(collection, "$this$addAll");
        fs1.f(iterable, "elements");
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        Iterator<? extends T> it = iterable.iterator();
        while (it.hasNext()) {
            if (collection.add((T) it.next())) {
                z = true;
            }
        }
        return z;
    }

    public static final <T> boolean w(Collection<? super T> collection, T[] tArr) {
        fs1.f(collection, "$this$addAll");
        fs1.f(tArr, "elements");
        return collection.addAll(zh.c(tArr));
    }

    public static final <T> boolean x(Iterable<? extends T> iterable, tc1<? super T, Boolean> tc1Var, boolean z) {
        Iterator<? extends T> it = iterable.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (tc1Var.invoke((T) it.next()).booleanValue() == z) {
                it.remove();
                z2 = true;
            }
        }
        return z2;
    }

    public static final <T> boolean y(List<T> list, tc1<? super T, Boolean> tc1Var, boolean z) {
        int i;
        if (!(list instanceof RandomAccess)) {
            Objects.requireNonNull(list, "null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
            return x(qd4.a(list), tc1Var, z);
        }
        int i2 = b20.i(list);
        if (i2 >= 0) {
            int i3 = 0;
            i = 0;
            while (true) {
                T t = list.get(i3);
                if (tc1Var.invoke(t).booleanValue() != z) {
                    if (i != i3) {
                        list.set(i, t);
                    }
                    i++;
                }
                if (i3 == i2) {
                    break;
                }
                i3++;
            }
        } else {
            i = 0;
        }
        if (i >= list.size()) {
            return false;
        }
        int i4 = b20.i(list);
        if (i4 < i) {
            return true;
        }
        while (true) {
            list.remove(i4);
            if (i4 == i) {
                return true;
            }
            i4--;
        }
    }

    public static final <T> boolean z(List<T> list, tc1<? super T, Boolean> tc1Var) {
        fs1.f(list, "$this$removeAll");
        fs1.f(tc1Var, "predicate");
        return y(list, tc1Var, true);
    }
}
