package defpackage;

import com.facebook.common.references.SharedReference;

/* compiled from: CloseableReferenceLeakTracker.java */
/* renamed from: b00  reason: default package */
/* loaded from: classes.dex */
public interface b00 {
    boolean a();

    void b(SharedReference<Object> sharedReference, Throwable th);
}
