package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: hb0  reason: default package */
/* loaded from: classes2.dex */
public interface hb0 {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;

    static {
        i iVar = new i("1.2.643.2.2");
        a = iVar;
        iVar.z("9");
        iVar.z("10");
        iVar.z("13.0");
        iVar.z("13.1");
        iVar.z("21");
        iVar.z("31.0");
        iVar.z("31.1");
        iVar.z("31.2");
        iVar.z("31.3");
        iVar.z("31.4");
        iVar.z("20");
        iVar.z("19");
        iVar.z("4");
        iVar.z("3");
        iVar.z("30.1");
        iVar.z("32.2");
        iVar.z("32.3");
        iVar.z("32.4");
        iVar.z("32.5");
        iVar.z("33.1");
        iVar.z("33.2");
        iVar.z("33.3");
        b = iVar.z("35.1");
        c = iVar.z("35.2");
        d = iVar.z("35.3");
        e = iVar.z("36.0");
        f = iVar.z("36.1");
        iVar.z("36.0");
        iVar.z("36.1");
        iVar.z("96");
        iVar.z("98");
    }
}
