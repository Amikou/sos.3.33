package defpackage;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: oe4  reason: default package */
/* loaded from: classes.dex */
public final class oe4 {
    @RecentlyNonNull
    public static boolean a(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        if (b(context, i, "com.google.android.gms")) {
            try {
                return rh1.a(context).b(context.getPackageManager().getPackageInfo("com.google.android.gms", 64));
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        }
        return false;
    }

    @RecentlyNonNull
    @TargetApi(19)
    public static boolean b(@RecentlyNonNull Context context, @RecentlyNonNull int i, @RecentlyNonNull String str) {
        return kr4.a(context).g(i, str);
    }
}
