package defpackage;

import com.google.android.gms.internal.clearcut.y;

/* renamed from: hd5  reason: default package */
/* loaded from: classes.dex */
public final class hd5 extends lc5 {
    public hd5() {
        super();
    }

    public static <E> rb5<E> e(Object obj, long j) {
        return (rb5) y.M(obj, j);
    }

    @Override // defpackage.lc5
    public final void a(Object obj, long j) {
        e(obj, j).v();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.util.List] */
    @Override // defpackage.lc5
    public final <E> void b(Object obj, Object obj2, long j) {
        rb5<E> e = e(obj, j);
        rb5<E> e2 = e(obj2, j);
        int size = e.size();
        int size2 = e2.size();
        rb5<E> rb5Var = e;
        rb5Var = e;
        if (size > 0 && size2 > 0) {
            boolean u = e.u();
            rb5<E> rb5Var2 = e;
            if (!u) {
                rb5Var2 = e.T0(size2 + size);
            }
            rb5Var2.addAll(e2);
            rb5Var = rb5Var2;
        }
        if (size > 0) {
            e2 = rb5Var;
        }
        y.i(obj, j, e2);
    }
}
