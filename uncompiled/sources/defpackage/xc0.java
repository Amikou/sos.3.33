package defpackage;

import java.util.List;
import net.safemoon.androidwallet.model.token.room.RoomCustomToken;

/* compiled from: CustomTokenDao.kt */
/* renamed from: xc0  reason: default package */
/* loaded from: classes2.dex */
public interface xc0 {
    Object g(int i, q70<? super List<RoomCustomToken>> q70Var);

    Object h(String str, String str2, q70<? super te4> q70Var);

    Object i(RoomCustomToken roomCustomToken, q70<? super te4> q70Var);

    Object j(String str, q70<? super RoomCustomToken> q70Var);

    Object k(q70<? super List<RoomCustomToken>> q70Var);

    Object l(String str, q70<? super te4> q70Var);
}
