package defpackage;

import com.google.android.gms.internal.clearcut.m;
import com.google.android.gms.internal.clearcut.q;
import com.google.android.gms.internal.clearcut.r;
import com.google.android.gms.internal.clearcut.t;
import com.google.android.gms.internal.clearcut.u;
import com.google.android.gms.internal.clearcut.v;
import java.util.Set;

/* renamed from: ld5  reason: default package */
/* loaded from: classes.dex */
public final class ld5 implements bg5 {
    public static final ee5 b = new nd5();
    public final ee5 a;

    public ld5() {
        this(new pd5(bb5.c(), c()));
    }

    public ld5(ee5 ee5Var) {
        this.a = (ee5) gb5.e(ee5Var, "messageInfoFactory");
    }

    public static boolean b(ce5 ce5Var) {
        return ce5Var.a() == m.e.i;
    }

    public static ee5 c() {
        try {
            Set<String> set = com.google.protobuf.m.a;
            return (ee5) com.google.protobuf.m.class.getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    @Override // defpackage.bg5
    public final <T> t<T> a(Class<T> cls) {
        u.I(cls);
        ce5 b2 = this.a.b(cls);
        if (b2.b()) {
            return m.class.isAssignableFrom(cls) ? r.j(u.B(), x95.b(), b2.c()) : r.j(u.z(), x95.c(), b2.c());
        } else if (!m.class.isAssignableFrom(cls)) {
            boolean b3 = b(b2);
            se5 a = ve5.a();
            lc5 c = lc5.c();
            return b3 ? q.s(cls, b2, a, c, u.z(), x95.c(), ae5.a()) : q.s(cls, b2, a, c, u.A(), null, ae5.a());
        } else {
            boolean b4 = b(b2);
            se5 b5 = ve5.b();
            lc5 d = lc5.d();
            v<?, ?> B = u.B();
            return b4 ? q.s(cls, b2, b5, d, B, x95.b(), ae5.b()) : q.s(cls, b2, b5, d, B, null, ae5.b());
        }
    }
}
