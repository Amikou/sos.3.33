package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.viewpager.widget.ViewPager;
import com.alexvasilkov.gestures.GestureController;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: GestureControllerForPager.java */
/* renamed from: df1  reason: default package */
/* loaded from: classes.dex */
public class df1 extends GestureController {
    public static final Matrix a1 = new Matrix();
    public static final RectF b1 = new RectF();
    public final int R0;
    public ViewPager S0;
    public boolean T0;
    public boolean U0;
    public boolean V0;
    public int W0;
    public float X0;
    public boolean Y0;
    public float Z0;

    /* compiled from: GestureControllerForPager.java */
    /* renamed from: df1$a */
    /* loaded from: classes.dex */
    public class a implements View.OnTouchListener {
        public boolean a;

        @Override // android.view.View.OnTouchListener
        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (this.a || motionEvent.getActionMasked() != 0) {
                df1.f0((ViewPager) view, motionEvent);
                return true;
            }
            this.a = true;
            view.dispatchTouchEvent(motionEvent);
            this.a = false;
            return true;
        }
    }

    static {
        new a();
    }

    public df1(View view) {
        super(view);
        this.R0 = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
    }

    public static MotionEvent b0(MotionEvent motionEvent) {
        return MotionEvent.obtain(motionEvent.getDownTime(), motionEvent.getEventTime(), motionEvent.getAction(), motionEvent.getX(), motionEvent.getY(), motionEvent.getMetaState());
    }

    public static void f0(ViewPager viewPager, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == 1 || motionEvent.getActionMasked() == 3) {
            try {
                viewPager.e();
                if (viewPager.z()) {
                    viewPager.q();
                }
            } catch (Exception unused) {
            }
        }
    }

    public static void i0(Matrix matrix, View view, ViewPager viewPager) {
        if (view.getParent() instanceof View) {
            View view2 = (View) view.getParent();
            if (view2 != viewPager) {
                i0(matrix, view2, viewPager);
            }
            matrix.preTranslate(-view2.getScrollX(), -view2.getScrollY());
        }
        matrix.preTranslate(view.getLeft(), view.getTop());
        matrix.preConcat(view.getMatrix());
    }

    public static void j0(MotionEvent motionEvent, View view, ViewPager viewPager) {
        Matrix matrix = a1;
        matrix.reset();
        i0(matrix, view, viewPager);
        motionEvent.transform(matrix);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean F(o93 o93Var) {
        return !a0() && super.F(o93Var);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean I(ScaleGestureDetector scaleGestureDetector) {
        return !a0() && super.I(scaleGestureDetector);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean K(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.S0 == null) {
            return super.K(motionEvent, motionEvent2, f, f2);
        }
        if (!this.U0) {
            this.U0 = true;
            return true;
        }
        float f3 = -e0(motionEvent2, -f);
        if (a0()) {
            f2 = Utils.FLOAT_EPSILON;
        }
        return super.K(motionEvent, motionEvent2, f3, f2);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean O(View view, MotionEvent motionEvent) {
        if (this.S0 == null) {
            return super.O(view, motionEvent);
        }
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        j0(obtain, view, this.S0);
        Z(obtain);
        boolean O = super.O(view, obtain);
        obtain.recycle();
        return O;
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public void P(MotionEvent motionEvent) {
        c0(motionEvent);
        super.P(motionEvent);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean R(MotionEvent motionEvent) {
        return this.S0 != null || super.R(motionEvent);
    }

    public final int X(MotionEvent motionEvent) {
        int scrollX = this.S0.getScrollX();
        int width = this.S0.getWidth() + this.S0.getPageMargin();
        while (scrollX < 0) {
            scrollX += width;
        }
        return (width * ((int) ((scrollX + motionEvent.getX()) / width))) - scrollX;
    }

    public void Y(boolean z) {
        this.T0 = z;
    }

    public final void Z(MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == 5 && motionEvent.getPointerCount() == 2) {
            this.V0 = !a0();
        }
    }

    public final boolean a0() {
        int i = this.W0;
        return i < -1 || i > 1;
    }

    public final void c0(MotionEvent motionEvent) {
        if (this.S0 == null) {
            return;
        }
        MotionEvent b0 = b0(motionEvent);
        b0.setLocation(this.Z0, Utils.FLOAT_EPSILON);
        if (this.Y0) {
            this.S0.onTouchEvent(b0);
        } else {
            this.Y0 = this.S0.onInterceptTouchEvent(b0);
        }
        if (!this.Y0 && a0()) {
            f0(this.S0, motionEvent);
        }
        try {
            ViewPager viewPager = this.S0;
            if (viewPager != null && viewPager.z()) {
                this.S0.q();
            }
        } catch (Exception unused) {
        }
        b0.recycle();
    }

    public final int d0(MotionEvent motionEvent, float f) {
        int scrollX = this.S0.getScrollX();
        this.Z0 += f;
        c0(motionEvent);
        return scrollX - this.S0.getScrollX();
    }

    public final float e0(MotionEvent motionEvent, float f) {
        int d0;
        if (this.V0 || this.T0) {
            return f;
        }
        us3 o = o();
        vs3 p = p();
        RectF rectF = b1;
        p.g(o, rectF);
        float g0 = g0(h0(f, o, rectF), o, rectF);
        float f2 = f - g0;
        boolean z = this.Y0 && this.W0 == 0;
        this.W0 += d0(motionEvent, g0);
        return z ? f2 + (Math.round(g0) - d0) : f2;
    }

    public final float g0(float f, us3 us3Var, RectF rectF) {
        float g;
        float e;
        float r = n().r() * 4.0f;
        float g2 = us3Var.g();
        float f2 = rectF.top;
        if (g2 < f2) {
            g = (f2 - us3Var.g()) / r;
        } else {
            g = us3Var.g() > rectF.bottom ? (us3Var.g() - rectF.bottom) / r : 0.0f;
        }
        float sqrt = ((float) Math.sqrt(Math.max((float) Utils.FLOAT_EPSILON, Math.min(Math.max(g, p().e(us3Var) == Utils.FLOAT_EPSILON ? 0.0f : (us3Var.h() / e) - 1.0f), 1.0f)))) * this.R0 * 15.0f;
        if (this.X0 * f < Utils.FLOAT_EPSILON && this.W0 == 0) {
            this.X0 = Utils.FLOAT_EPSILON;
        }
        if (a0()) {
            this.X0 = Math.signum(this.W0) * sqrt;
        }
        if (Math.abs(this.X0) < sqrt) {
            float f3 = this.X0;
            if (f * f3 >= Utils.FLOAT_EPSILON) {
                float f4 = f3 + f;
                this.X0 = f4;
                float max = Math.max((float) Utils.FLOAT_EPSILON, Math.abs(f4) - sqrt) * Math.signum(f);
                this.X0 -= max;
                return max;
            }
        }
        return f;
    }

    public final float h0(float f, us3 us3Var, RectF rectF) {
        if (n().E()) {
            float signum = Math.signum(f);
            float abs = Math.abs(f);
            float f2 = us3Var.f();
            float f3 = Utils.FLOAT_EPSILON;
            float f4 = signum < Utils.FLOAT_EPSILON ? f2 - rectF.left : rectF.right - f2;
            int i = this.W0;
            float abs2 = ((float) i) * signum < Utils.FLOAT_EPSILON ? Math.abs(i) : 0.0f;
            if (f4 >= Utils.FLOAT_EPSILON) {
                f3 = f4;
            }
            if (abs2 < abs) {
                abs = f3 + abs2 >= abs ? abs2 : abs - f3;
            }
            return abs * signum;
        }
        return f;
    }

    @Override // com.alexvasilkov.gestures.GestureController, android.view.View.OnTouchListener
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return this.S0 != null || super.onTouch(view, motionEvent);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean x(MotionEvent motionEvent) {
        return !a0() && super.x(motionEvent);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean y(MotionEvent motionEvent) {
        if (this.S0 == null) {
            return super.y(motionEvent);
        }
        this.V0 = false;
        this.Y0 = false;
        this.U0 = false;
        this.W0 = X(motionEvent);
        this.Z0 = motionEvent.getX();
        this.X0 = Utils.FLOAT_EPSILON;
        c0(motionEvent);
        return super.y(motionEvent);
    }

    @Override // com.alexvasilkov.gestures.GestureController
    public boolean z(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return !a0() && super.z(motionEvent, motionEvent2, f, f2);
    }
}
