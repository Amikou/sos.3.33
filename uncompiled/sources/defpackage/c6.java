package defpackage;

import android.os.Build;
import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AccessibilityNodeProviderCompat.java */
/* renamed from: c6  reason: default package */
/* loaded from: classes.dex */
public class c6 {
    public final Object a;

    /* compiled from: AccessibilityNodeProviderCompat.java */
    /* renamed from: c6$a */
    /* loaded from: classes.dex */
    public static class a extends AccessibilityNodeProvider {
        public final c6 a;

        public a(c6 c6Var) {
            this.a = c6Var;
        }

        @Override // android.view.accessibility.AccessibilityNodeProvider
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            b6 b = this.a.b(i);
            if (b == null) {
                return null;
            }
            return b.H0();
        }

        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            List<b6> c = this.a.c(str, i);
            if (c == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int size = c.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(c.get(i2).H0());
            }
            return arrayList;
        }

        @Override // android.view.accessibility.AccessibilityNodeProvider
        public boolean performAction(int i, int i2, Bundle bundle) {
            return this.a.f(i, i2, bundle);
        }
    }

    /* compiled from: AccessibilityNodeProviderCompat.java */
    /* renamed from: c6$b */
    /* loaded from: classes.dex */
    public static class b extends a {
        public b(c6 c6Var) {
            super(c6Var);
        }

        @Override // android.view.accessibility.AccessibilityNodeProvider
        public AccessibilityNodeInfo findFocus(int i) {
            b6 d = this.a.d(i);
            if (d == null) {
                return null;
            }
            return d.H0();
        }
    }

    /* compiled from: AccessibilityNodeProviderCompat.java */
    /* renamed from: c6$c */
    /* loaded from: classes.dex */
    public static class c extends b {
        public c(c6 c6Var) {
            super(c6Var);
        }

        @Override // android.view.accessibility.AccessibilityNodeProvider
        public void addExtraDataToAccessibilityNodeInfo(int i, AccessibilityNodeInfo accessibilityNodeInfo, String str, Bundle bundle) {
            this.a.a(i, b6.I0(accessibilityNodeInfo), str, bundle);
        }
    }

    public c6() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.a = new c(this);
        } else if (i >= 19) {
            this.a = new b(this);
        } else if (i >= 16) {
            this.a = new a(this);
        } else {
            this.a = null;
        }
    }

    public void a(int i, b6 b6Var, String str, Bundle bundle) {
    }

    public b6 b(int i) {
        return null;
    }

    public List<b6> c(String str, int i) {
        return null;
    }

    public b6 d(int i) {
        return null;
    }

    public Object e() {
        return this.a;
    }

    public boolean f(int i, int i2, Bundle bundle) {
        return false;
    }

    public c6(Object obj) {
        this.a = obj;
    }
}
