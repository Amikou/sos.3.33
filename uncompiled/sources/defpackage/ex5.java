package defpackage;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: ex5  reason: default package */
/* loaded from: classes.dex */
public final class ex5 {
    public final String a;
    public final String b;
    public final int c;
    public final boolean d;

    public ex5(String str, String str2, boolean z, int i, boolean z2) {
        this.b = str;
        this.a = str2;
        this.c = i;
        this.d = z2;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }
}
