package defpackage;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.Choreographer;
import java.util.Objects;
import kotlin.Result;
import kotlinx.coroutines.android.HandlerContext;

/* compiled from: HandlerDispatcher.kt */
/* renamed from: mj1  reason: default package */
/* loaded from: classes2.dex */
public final class mj1 {
    private static volatile Choreographer choreographer;

    static {
        Object m52constructorimpl;
        try {
            Result.a aVar = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(new HandlerContext(a(Looper.getMainLooper(), true), null, 2, null));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(o83.a(th));
        }
        lj1 lj1Var = Result.m57isFailureimpl(m52constructorimpl) ? null : m52constructorimpl;
    }

    public static final Handler a(Looper looper, boolean z) {
        int i;
        if (!z || (i = Build.VERSION.SDK_INT) < 16) {
            return new Handler(looper);
        }
        if (i >= 28) {
            Object invoke = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, looper);
            Objects.requireNonNull(invoke, "null cannot be cast to non-null type android.os.Handler");
            return (Handler) invoke;
        }
        try {
            return (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
        } catch (NoSuchMethodException unused) {
            return new Handler(looper);
        }
    }
}
