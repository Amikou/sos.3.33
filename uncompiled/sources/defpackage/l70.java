package defpackage;

import android.content.Context;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/* compiled from: ContextAwareHelper.java */
/* renamed from: l70  reason: default package */
/* loaded from: classes.dex */
public final class l70 {
    public final Set<km2> a = new CopyOnWriteArraySet();
    public volatile Context b;

    public void a(km2 km2Var) {
        if (this.b != null) {
            km2Var.a(this.b);
        }
        this.a.add(km2Var);
    }

    public void b() {
        this.b = null;
    }

    public void c(Context context) {
        this.b = context;
        for (km2 km2Var : this.a) {
            km2Var.a(context);
        }
    }

    public Context d() {
        return this.b;
    }

    public void e(km2 km2Var) {
        this.a.remove(km2Var);
    }
}
