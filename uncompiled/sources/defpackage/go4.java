package defpackage;

import androidx.media3.common.util.b;

/* compiled from: WavUtil.java */
/* renamed from: go4  reason: default package */
/* loaded from: classes.dex */
public final class go4 {
    public static int a(int i, int i2) {
        if (i != 1) {
            if (i == 3) {
                return i2 == 32 ? 4 : 0;
            } else if (i != 65534) {
                return 0;
            }
        }
        return b.Y(i2);
    }
}
