package defpackage;

import android.widget.ListView;

/* compiled from: ListViewAutoScrollHelper.java */
/* renamed from: j02  reason: default package */
/* loaded from: classes.dex */
public class j02 extends ik {
    public final ListView w0;

    public j02(ListView listView) {
        super(listView);
        this.w0 = listView;
    }

    @Override // defpackage.ik
    public boolean a(int i) {
        return false;
    }

    @Override // defpackage.ik
    public boolean b(int i) {
        ListView listView = this.w0;
        int count = listView.getCount();
        if (count == 0) {
            return false;
        }
        int childCount = listView.getChildCount();
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int i2 = firstVisiblePosition + childCount;
        if (i > 0) {
            if (i2 >= count && listView.getChildAt(childCount - 1).getBottom() <= listView.getHeight()) {
                return false;
            }
        } else if (i >= 0) {
            return false;
        } else {
            if (firstVisiblePosition <= 0 && listView.getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }

    @Override // defpackage.ik
    public void j(int i, int i2) {
        k02.b(this.w0, i2);
    }
}
