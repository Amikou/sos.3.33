package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: nm5  reason: default package */
/* loaded from: classes.dex */
public final class nm5 implements Runnable {
    public final /* synthetic */ boolean a;
    public final /* synthetic */ dp5 f0;

    public nm5(dp5 dp5Var, boolean z) {
        this.f0 = dp5Var;
        this.a = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        boolean h = this.f0.a.h();
        boolean g = this.f0.a.g();
        this.f0.a.f(this.a);
        if (g == this.a) {
            this.f0.a.w().v().b("Default data collection state already set to", Boolean.valueOf(this.a));
        }
        if (this.f0.a.h() == h || this.f0.a.h() != this.f0.a.g()) {
            this.f0.a.w().s().c("Default data collection is different than actual status", Boolean.valueOf(this.a), Boolean.valueOf(h));
        }
        this.f0.M();
    }
}
