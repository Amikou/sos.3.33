package defpackage;

import androidx.lifecycle.l;
import defpackage.dj4;

/* compiled from: ViewModelProvider.kt */
/* renamed from: fj4  reason: default package */
/* loaded from: classes.dex */
public final class fj4<VM extends dj4> implements sy1<VM> {
    public VM a;
    public final qw1<VM> f0;
    public final rc1<gj4> g0;
    public final rc1<l.b> h0;

    /* JADX WARN: Multi-variable type inference failed */
    public fj4(qw1<VM> qw1Var, rc1<? extends gj4> rc1Var, rc1<? extends l.b> rc1Var2) {
        fs1.f(qw1Var, "viewModelClass");
        fs1.f(rc1Var, "storeProducer");
        fs1.f(rc1Var2, "factoryProducer");
        this.f0 = qw1Var;
        this.g0 = rc1Var;
        this.h0 = rc1Var2;
    }

    @Override // defpackage.sy1
    /* renamed from: a */
    public VM getValue() {
        VM vm = this.a;
        if (vm == null) {
            VM vm2 = (VM) new l(this.g0.invoke(), this.h0.invoke()).a(nw1.a(this.f0));
            this.a = vm2;
            fs1.e(vm2, "ViewModelProvider(store,…ed = it\n                }");
            return vm2;
        }
        return vm;
    }
}
