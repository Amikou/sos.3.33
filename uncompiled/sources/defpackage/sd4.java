package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeBindings;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.lang.reflect.Type;

/* compiled from: TypeModifier.java */
/* renamed from: sd4  reason: default package */
/* loaded from: classes.dex */
public abstract class sd4 {
    public abstract JavaType a(JavaType javaType, Type type, TypeBindings typeBindings, TypeFactory typeFactory);
}
