package defpackage;

import java.math.BigInteger;

/* compiled from: INftWeb.kt */
/* renamed from: im1  reason: default package */
/* loaded from: classes2.dex */
public interface im1 {

    /* compiled from: INftWeb.kt */
    /* renamed from: im1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ Object a(im1 im1Var, String str, BigInteger bigInteger, BigInteger bigInteger2, boolean z, q70 q70Var, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    bigInteger2 = BigInteger.ONE;
                    fs1.e(bigInteger2, "ONE");
                }
                return im1Var.c(str, bigInteger, bigInteger2, z, q70Var);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: estimateGas");
        }

        public static /* synthetic */ Object b(im1 im1Var, String str, BigInteger bigInteger, BigInteger bigInteger2, q70 q70Var, int i, Object obj) {
            if (obj == null) {
                if ((i & 4) != 0) {
                    bigInteger2 = BigInteger.ONE;
                    fs1.e(bigInteger2, "ONE");
                }
                return im1Var.b(str, bigInteger, bigInteger2, q70Var);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: safeTransferFrom");
        }
    }

    int a();

    Object b(String str, BigInteger bigInteger, BigInteger bigInteger2, q70<? super hx0> q70Var);

    Object c(String str, BigInteger bigInteger, BigInteger bigInteger2, boolean z, q70<? super ow0> q70Var);

    BigInteger getGasPrice();
}
