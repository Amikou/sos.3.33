package defpackage;

import android.content.ComponentCallbacks;
import android.content.res.Configuration;

/* compiled from: LingverApplicationCallbacks.kt */
/* renamed from: d02  reason: default package */
/* loaded from: classes2.dex */
public final class d02 implements ComponentCallbacks {
    public final tc1<Configuration, te4> a;

    /* JADX WARN: Multi-variable type inference failed */
    public d02(tc1<? super Configuration, te4> tc1Var) {
        fs1.g(tc1Var, "callback");
        this.a = tc1Var;
    }

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        fs1.g(configuration, "newConfig");
        this.a.invoke(configuration);
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
    }
}
