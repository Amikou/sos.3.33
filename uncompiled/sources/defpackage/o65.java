package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: o65  reason: default package */
/* loaded from: classes.dex */
public abstract class o65 {
    public final List<zzbl> a = new ArrayList();

    public abstract z55 a(String str, wk5 wk5Var, List<z55> list);

    public final z55 b(String str) {
        if (this.a.contains(vm5.e(str))) {
            String valueOf = String.valueOf(str);
            throw new UnsupportedOperationException(valueOf.length() != 0 ? "Command not implemented: ".concat(valueOf) : new String("Command not implemented: "));
        }
        throw new IllegalArgumentException("Command not supported");
    }
}
