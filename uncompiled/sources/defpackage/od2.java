package defpackage;

import android.os.Build;

/* compiled from: NativeJpegTranscoderSoLoader.java */
/* renamed from: od2  reason: default package */
/* loaded from: classes.dex */
public class od2 {
    public static boolean a;

    public static synchronized void a() {
        synchronized (od2.class) {
            if (!a) {
                if (Build.VERSION.SDK_INT <= 16) {
                    try {
                        pd2.e("fb_jpegturbo", 1);
                    } catch (UnsatisfiedLinkError unused) {
                    }
                }
                pd2.d("native-imagetranscoder");
                a = true;
            }
        }
    }
}
