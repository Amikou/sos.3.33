package defpackage;

import android.graphics.Bitmap;

/* compiled from: DummyBitmapPool.java */
/* renamed from: is0  reason: default package */
/* loaded from: classes.dex */
public class is0 implements iq {
    @Override // defpackage.us2
    /* renamed from: f */
    public Bitmap get(int i) {
        return Bitmap.createBitmap(1, (int) Math.ceil(i / 2.0d), Bitmap.Config.RGB_565);
    }

    @Override // defpackage.us2, defpackage.d83
    /* renamed from: g */
    public void a(Bitmap bitmap) {
        xt2.g(bitmap);
        bitmap.recycle();
    }
}
