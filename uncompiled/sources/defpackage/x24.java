package defpackage;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.BackoffPolicy;
import androidx.work.NetworkType;
import androidx.work.impl.background.systemjob.SystemJobService;
import defpackage.j70;

/* compiled from: SystemJobInfoConverter.java */
/* renamed from: x24  reason: default package */
/* loaded from: classes.dex */
public class x24 {
    public static final String b = v12.f("SystemJobInfoConverter");
    public final ComponentName a;

    /* compiled from: SystemJobInfoConverter.java */
    /* renamed from: x24$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[NetworkType.values().length];
            a = iArr;
            try {
                iArr[NetworkType.NOT_REQUIRED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[NetworkType.CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[NetworkType.UNMETERED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[NetworkType.NOT_ROAMING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[NetworkType.METERED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public x24(Context context) {
        this.a = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    public static JobInfo.TriggerContentUri b(j70.a aVar) {
        return new JobInfo.TriggerContentUri(aVar.a(), aVar.b() ? 1 : 0);
    }

    public static int c(NetworkType networkType) {
        int i = a.a[networkType.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        if (i == 5 && Build.VERSION.SDK_INT >= 26) {
                            return 4;
                        }
                    } else if (Build.VERSION.SDK_INT >= 24) {
                        return 3;
                    }
                    v12.c().a(b, String.format("API version too low. Cannot convert network type value %s", networkType), new Throwable[0]);
                    return 1;
                }
                return 2;
            }
            return 1;
        }
        return 0;
    }

    public static void d(JobInfo.Builder builder, NetworkType networkType) {
        if (Build.VERSION.SDK_INT >= 30 && networkType == NetworkType.TEMPORARILY_UNMETERED) {
            builder.setRequiredNetwork(new NetworkRequest.Builder().addCapability(25).build());
        } else {
            builder.setRequiredNetworkType(c(networkType));
        }
    }

    public JobInfo a(tq4 tq4Var, int i) {
        h60 h60Var = tq4Var.j;
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", tq4Var.a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", tq4Var.d());
        JobInfo.Builder extras = new JobInfo.Builder(i, this.a).setRequiresCharging(h60Var.g()).setRequiresDeviceIdle(h60Var.h()).setExtras(persistableBundle);
        d(extras, h60Var.b());
        if (!h60Var.h()) {
            extras.setBackoffCriteria(tq4Var.m, tq4Var.l == BackoffPolicy.LINEAR ? 0 : 1);
        }
        long max = Math.max(tq4Var.a() - System.currentTimeMillis(), 0L);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else if (!tq4Var.q) {
            extras.setImportantWhileForeground(true);
        }
        if (i2 >= 24 && h60Var.e()) {
            for (j70.a aVar : h60Var.a().b()) {
                extras.addTriggerContentUri(b(aVar));
            }
            extras.setTriggerContentUpdateDelay(h60Var.c());
            extras.setTriggerContentMaxDelay(h60Var.d());
        }
        extras.setPersisted(false);
        if (Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(h60Var.f());
            extras.setRequiresStorageNotLow(h60Var.i());
        }
        boolean z = tq4Var.k > 0;
        boolean z2 = max > 0;
        if (yr.c() && tq4Var.q && !z && !z2) {
            extras.setExpedited(true);
        }
        return extras.build();
    }
}
