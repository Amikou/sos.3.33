package defpackage;

/* compiled from: WavFormat.java */
/* renamed from: do4  reason: default package */
/* loaded from: classes.dex */
public final class do4 {
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final byte[] f;

    public do4(int i, int i2, int i3, int i4, int i5, int i6, byte[] bArr) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i5;
        this.e = i6;
        this.f = bArr;
    }
}
