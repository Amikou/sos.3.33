package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: EncoderRegistry.java */
/* renamed from: gv0  reason: default package */
/* loaded from: classes.dex */
public class gv0 {
    public final List<a<?>> a = new ArrayList();

    /* compiled from: EncoderRegistry.java */
    /* renamed from: gv0$a */
    /* loaded from: classes.dex */
    public static final class a<T> {
        public final Class<T> a;
        public final ev0<T> b;

        public a(Class<T> cls, ev0<T> ev0Var) {
            this.a = cls;
            this.b = ev0Var;
        }

        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    public synchronized <T> void a(Class<T> cls, ev0<T> ev0Var) {
        this.a.add(new a<>(cls, ev0Var));
    }

    public synchronized <T> ev0<T> b(Class<T> cls) {
        for (a<?> aVar : this.a) {
            if (aVar.a(cls)) {
                return (ev0<T>) aVar.b;
            }
        }
        return null;
    }
}
