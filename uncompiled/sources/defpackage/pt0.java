package defpackage;

import defpackage.ct0;
import defpackage.xs0;
import java.math.BigInteger;
import java.util.Hashtable;

/* renamed from: pt0  reason: default package */
/* loaded from: classes2.dex */
public abstract class pt0 {
    public static ct0[] g = new ct0[0];
    public xs0 a;
    public ct0 b;
    public ct0 c;
    public ct0[] d;
    public boolean e;
    public Hashtable f;

    /* renamed from: pt0$a */
    /* loaded from: classes2.dex */
    public class a implements tt2 {
        public final /* synthetic */ boolean a;

        public a(boolean z) {
            this.a = z;
        }

        @Override // defpackage.tt2
        public ut2 a(ut2 ut2Var) {
            vg4 vg4Var = ut2Var instanceof vg4 ? (vg4) ut2Var : null;
            if (vg4Var == null) {
                vg4Var = new vg4();
            }
            if (vg4Var.b()) {
                return vg4Var;
            }
            if (!vg4Var.a()) {
                if (!pt0.this.C()) {
                    vg4Var.e();
                    return vg4Var;
                }
                vg4Var.d();
            }
            if (this.a && !vg4Var.c()) {
                if (!pt0.this.D()) {
                    vg4Var.e();
                    return vg4Var;
                }
                vg4Var.f();
            }
            return vg4Var;
        }
    }

    /* renamed from: pt0$b */
    /* loaded from: classes2.dex */
    public static abstract class b extends pt0 {
        public b(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
            super(xs0Var, ct0Var, ct0Var2);
        }

        public b(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr) {
            super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        }

        @Override // defpackage.pt0
        public boolean C() {
            ct0 l;
            ct0 p;
            xs0 i = i();
            ct0 ct0Var = this.b;
            ct0 o = i.o();
            ct0 p2 = i.p();
            int r = i.r();
            if (r != 6) {
                ct0 ct0Var2 = this.c;
                ct0 j = ct0Var2.a(ct0Var).j(ct0Var2);
                if (r != 0) {
                    if (r != 1) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                    ct0 ct0Var3 = this.d[0];
                    if (!ct0Var3.h()) {
                        ct0 j2 = ct0Var3.j(ct0Var3.o());
                        j = j.j(ct0Var3);
                        o = o.j(ct0Var3);
                        p2 = p2.j(j2);
                    }
                }
                return j.equals(ct0Var.a(o).j(ct0Var.o()).a(p2));
            }
            ct0 ct0Var4 = this.d[0];
            boolean h = ct0Var4.h();
            if (ct0Var.i()) {
                ct0 o2 = this.c.o();
                if (!h) {
                    p2 = p2.j(ct0Var4.o());
                }
                return o2.equals(p2);
            }
            ct0 ct0Var5 = this.c;
            ct0 o3 = ct0Var.o();
            if (h) {
                l = ct0Var5.o().a(ct0Var5).a(o);
                p = o3.o().a(p2);
            } else {
                ct0 o4 = ct0Var4.o();
                ct0 o5 = o4.o();
                l = ct0Var5.a(ct0Var4).l(ct0Var5, o, o4);
                p = o3.p(p2, o5);
            }
            return l.j(o3).equals(p);
        }

        @Override // defpackage.pt0
        public boolean D() {
            BigInteger q = this.a.q();
            if (ws0.c.equals(q)) {
                return ((ct0.a) A().f().a(this.a.o())).u() == 0;
            } else if (ws0.e.equals(q)) {
                pt0 A = A();
                ct0 f = A.f();
                xs0 xs0Var = this.a;
                ct0 I = ((xs0.b) xs0Var).I(f.a(xs0Var.o()));
                if (I == null) {
                    return false;
                }
                ct0 a = f.j(I).a(A.g()).a(this.a.o());
                return ((ct0.a) a).u() == 0 || ((ct0.a) a.a(f)).u() == 0;
            } else {
                return super.D();
            }
        }

        @Override // defpackage.pt0
        public pt0 E(ct0 ct0Var) {
            if (u()) {
                return this;
            }
            int j = j();
            if (j == 5) {
                ct0 n = n();
                return i().j(n, o().a(n).d(ct0Var).a(n.j(ct0Var)), p(), this.e);
            } else if (j != 6) {
                return super.E(ct0Var);
            } else {
                ct0 n2 = n();
                ct0 o = o();
                ct0 ct0Var2 = p()[0];
                ct0 j2 = n2.j(ct0Var.o());
                return i().j(j2, o.a(n2).a(j2), new ct0[]{ct0Var2.j(ct0Var)}, this.e);
            }
        }

        @Override // defpackage.pt0
        public pt0 F(ct0 ct0Var) {
            if (u()) {
                return this;
            }
            int j = j();
            if (j == 5 || j == 6) {
                ct0 n = n();
                return i().j(n, o().a(n).j(ct0Var).a(n), p(), this.e);
            }
            return super.F(ct0Var);
        }

        @Override // defpackage.pt0
        public pt0 G(pt0 pt0Var) {
            return pt0Var.u() ? this : a(pt0Var.z());
        }

        public b L(int i) {
            pt0 i2;
            if (u()) {
                return this;
            }
            xs0 i3 = i();
            int r = i3.r();
            ct0 ct0Var = this.b;
            if (r != 0) {
                if (r != 1) {
                    if (r != 5) {
                        if (r != 6) {
                            throw new IllegalStateException("unsupported coordinate system");
                        }
                    }
                }
                i2 = i3.j(ct0Var.q(i), this.c.q(i), new ct0[]{this.d[0].q(i)}, this.e);
                return (b) i2;
            }
            i2 = i3.i(ct0Var.q(i), this.c.q(i), this.e);
            return (b) i2;
        }
    }

    /* renamed from: pt0$c */
    /* loaded from: classes2.dex */
    public static abstract class c extends pt0 {
        public c(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
            super(xs0Var, ct0Var, ct0Var2);
        }

        public c(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr) {
            super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        }

        @Override // defpackage.pt0
        public boolean C() {
            ct0 ct0Var = this.b;
            ct0 ct0Var2 = this.c;
            ct0 o = this.a.o();
            ct0 p = this.a.p();
            ct0 o2 = ct0Var2.o();
            int j = j();
            if (j != 0) {
                if (j == 1) {
                    ct0 ct0Var3 = this.d[0];
                    if (!ct0Var3.h()) {
                        ct0 o3 = ct0Var3.o();
                        ct0 j2 = ct0Var3.j(o3);
                        o2 = o2.j(ct0Var3);
                        o = o.j(o3);
                        p = p.j(j2);
                    }
                } else if (j != 2 && j != 3 && j != 4) {
                    throw new IllegalStateException("unsupported coordinate system");
                } else {
                    ct0 ct0Var4 = this.d[0];
                    if (!ct0Var4.h()) {
                        ct0 o4 = ct0Var4.o();
                        ct0 o5 = o4.o();
                        ct0 j3 = o4.j(o5);
                        o = o.j(o5);
                        p = p.j(j3);
                    }
                }
            }
            return o2.equals(ct0Var.o().a(o).j(ct0Var).a(p));
        }

        @Override // defpackage.pt0
        public pt0 G(pt0 pt0Var) {
            return pt0Var.u() ? this : a(pt0Var.z());
        }

        @Override // defpackage.pt0
        public boolean h() {
            return g().s();
        }
    }

    /* renamed from: pt0$d */
    /* loaded from: classes2.dex */
    public static class d extends b {
        public d(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
            super(xs0Var, ct0Var, ct0Var2);
            if ((ct0Var == null) != (ct0Var2 == null)) {
                throw new IllegalArgumentException("Exactly one of the field elements is null");
            }
            if (ct0Var != null) {
                ct0.c.v(this.b, this.c);
                if (xs0Var != null) {
                    ct0.c.v(this.b, this.a.o());
                }
            }
            this.e = z;
        }

        public d(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
            super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
            this.e = z;
        }

        @Override // defpackage.pt0
        public pt0 J() {
            ct0 a;
            if (u()) {
                return this;
            }
            xs0 i = i();
            ct0 ct0Var = this.b;
            if (ct0Var.i()) {
                return i.v();
            }
            int r = i.r();
            if (r == 0) {
                ct0 a2 = this.c.d(ct0Var).a(ct0Var);
                ct0 a3 = a2.o().a(a2).a(i.o());
                return new d(i, a3, ct0Var.p(a3, a2.b()), this.e);
            } else if (r == 1) {
                ct0 ct0Var2 = this.c;
                ct0 ct0Var3 = this.d[0];
                boolean h = ct0Var3.h();
                ct0 j = h ? ct0Var : ct0Var.j(ct0Var3);
                if (!h) {
                    ct0Var2 = ct0Var2.j(ct0Var3);
                }
                ct0 o = ct0Var.o();
                ct0 a4 = o.a(ct0Var2);
                ct0 o2 = j.o();
                ct0 a5 = a4.a(j);
                ct0 l = a5.l(a4, o2, i.o());
                return new d(i, j.j(l), o.o().l(j, l, a5), new ct0[]{j.j(o2)}, this.e);
            } else if (r == 6) {
                ct0 ct0Var4 = this.c;
                ct0 ct0Var5 = this.d[0];
                boolean h2 = ct0Var5.h();
                ct0 j2 = h2 ? ct0Var4 : ct0Var4.j(ct0Var5);
                ct0 o3 = h2 ? ct0Var5 : ct0Var5.o();
                ct0 o4 = i.o();
                ct0 j3 = h2 ? o4 : o4.j(o3);
                ct0 a6 = ct0Var4.o().a(j2).a(j3);
                if (a6.i()) {
                    return new d(i, a6, i.p().n(), this.e);
                }
                ct0 o5 = a6.o();
                ct0 j4 = h2 ? a6 : a6.j(o3);
                ct0 p = i.p();
                if (p.c() < (i.u() >> 1)) {
                    ct0 o6 = ct0Var4.a(ct0Var).o();
                    a = o6.a(a6).a(o3).j(o6).a(p.h() ? j3.a(o3).o() : j3.p(p, o3.o())).a(o5);
                    if (!o4.i()) {
                        if (!o4.h()) {
                            a = a.a(o4.b().j(j4));
                        }
                        return new d(i, o5, a, new ct0[]{j4}, this.e);
                    }
                } else {
                    if (!h2) {
                        ct0Var = ct0Var.j(ct0Var5);
                    }
                    a = ct0Var.p(a6, j2).a(o5);
                }
                a = a.a(j4);
                return new d(i, o5, a, new ct0[]{j4}, this.e);
            } else {
                throw new IllegalStateException("unsupported coordinate system");
            }
        }

        @Override // defpackage.pt0
        public pt0 K(pt0 pt0Var) {
            if (u()) {
                return pt0Var;
            }
            if (pt0Var.u()) {
                return J();
            }
            xs0 i = i();
            ct0 ct0Var = this.b;
            if (ct0Var.i()) {
                return pt0Var;
            }
            if (i.r() != 6) {
                return J().a(pt0Var);
            }
            ct0 ct0Var2 = pt0Var.b;
            ct0 ct0Var3 = pt0Var.d[0];
            if (ct0Var2.i() || !ct0Var3.h()) {
                return J().a(pt0Var);
            }
            ct0 ct0Var4 = this.c;
            ct0 ct0Var5 = this.d[0];
            ct0 ct0Var6 = pt0Var.c;
            ct0 o = ct0Var.o();
            ct0 o2 = ct0Var4.o();
            ct0 o3 = ct0Var5.o();
            ct0 a = i.o().j(o3).a(o2).a(ct0Var4.j(ct0Var5));
            ct0 b = ct0Var6.b();
            ct0 l = i.o().a(b).j(o3).a(o2).l(a, o, o3);
            ct0 j = ct0Var2.j(o3);
            ct0 o4 = j.a(a).o();
            if (o4.i()) {
                return l.i() ? pt0Var.J() : i.v();
            } else if (l.i()) {
                return new d(i, l, i.p().n(), this.e);
            } else {
                ct0 j2 = l.o().j(j);
                ct0 j3 = l.j(o4).j(o3);
                return new d(i, j2, l.a(o4).o().l(a, b, j3), new ct0[]{j3}, this.e);
            }
        }

        @Override // defpackage.pt0
        public pt0 a(pt0 pt0Var) {
            ct0 ct0Var;
            ct0 ct0Var2;
            ct0 ct0Var3;
            ct0 ct0Var4;
            ct0 ct0Var5;
            ct0 ct0Var6;
            if (u()) {
                return pt0Var;
            }
            if (pt0Var.u()) {
                return this;
            }
            xs0 i = i();
            int r = i.r();
            ct0 ct0Var7 = this.b;
            ct0 ct0Var8 = pt0Var.b;
            if (r == 0) {
                ct0 ct0Var9 = this.c;
                ct0 ct0Var10 = pt0Var.c;
                ct0 a = ct0Var7.a(ct0Var8);
                ct0 a2 = ct0Var9.a(ct0Var10);
                if (a.i()) {
                    return a2.i() ? J() : i.v();
                }
                ct0 d = a2.d(a);
                ct0 a3 = d.o().a(d).a(a).a(i.o());
                return new d(i, a3, d.j(ct0Var7.a(a3)).a(a3).a(ct0Var9), this.e);
            } else if (r == 1) {
                ct0 ct0Var11 = this.c;
                ct0 ct0Var12 = this.d[0];
                ct0 ct0Var13 = pt0Var.c;
                ct0 ct0Var14 = pt0Var.d[0];
                boolean h = ct0Var14.h();
                ct0 a4 = ct0Var12.j(ct0Var13).a(h ? ct0Var11 : ct0Var11.j(ct0Var14));
                ct0 a5 = ct0Var12.j(ct0Var8).a(h ? ct0Var7 : ct0Var7.j(ct0Var14));
                if (a5.i()) {
                    return a4.i() ? J() : i.v();
                }
                ct0 o = a5.o();
                ct0 j = o.j(a5);
                if (!h) {
                    ct0Var12 = ct0Var12.j(ct0Var14);
                }
                ct0 a6 = a4.a(a5);
                ct0 a7 = a6.l(a4, o, i.o()).j(ct0Var12).a(j);
                ct0 j2 = a5.j(a7);
                if (!h) {
                    o = o.j(ct0Var14);
                }
                return new d(i, j2, a4.l(ct0Var7, a5, ct0Var11).l(o, a6, a7), new ct0[]{j.j(ct0Var12)}, this.e);
            } else if (r == 6) {
                if (ct0Var7.i()) {
                    return ct0Var8.i() ? i.v() : pt0Var.a(this);
                }
                ct0 ct0Var15 = this.c;
                ct0 ct0Var16 = this.d[0];
                ct0 ct0Var17 = pt0Var.c;
                ct0 ct0Var18 = pt0Var.d[0];
                boolean h2 = ct0Var16.h();
                if (h2) {
                    ct0Var = ct0Var8;
                    ct0Var2 = ct0Var17;
                } else {
                    ct0Var = ct0Var8.j(ct0Var16);
                    ct0Var2 = ct0Var17.j(ct0Var16);
                }
                boolean h3 = ct0Var18.h();
                if (h3) {
                    ct0Var3 = ct0Var15;
                } else {
                    ct0Var7 = ct0Var7.j(ct0Var18);
                    ct0Var3 = ct0Var15.j(ct0Var18);
                }
                ct0 a8 = ct0Var3.a(ct0Var2);
                ct0 a9 = ct0Var7.a(ct0Var);
                if (a9.i()) {
                    return a8.i() ? J() : i.v();
                }
                if (ct0Var8.i()) {
                    pt0 A = A();
                    ct0 q = A.q();
                    ct0 r2 = A.r();
                    ct0 d2 = r2.a(ct0Var17).d(q);
                    ct0Var4 = d2.o().a(d2).a(q).a(i.o());
                    if (ct0Var4.i()) {
                        return new d(i, ct0Var4, i.p().n(), this.e);
                    }
                    ct0Var6 = d2.j(q.a(ct0Var4)).a(ct0Var4).a(r2).d(ct0Var4).a(ct0Var4);
                    ct0Var5 = i.n(ws0.b);
                } else {
                    ct0 o2 = a9.o();
                    ct0 j3 = a8.j(ct0Var7);
                    ct0 j4 = a8.j(ct0Var);
                    ct0 j5 = j3.j(j4);
                    if (j5.i()) {
                        return new d(i, j5, i.p().n(), this.e);
                    }
                    ct0 j6 = a8.j(o2);
                    ct0 j7 = !h3 ? j6.j(ct0Var18) : j6;
                    ct0 p = j4.a(o2).p(j7, ct0Var15.a(ct0Var16));
                    if (!h2) {
                        j7 = j7.j(ct0Var16);
                    }
                    ct0Var4 = j5;
                    ct0Var5 = j7;
                    ct0Var6 = p;
                }
                return new d(i, ct0Var4, ct0Var6, new ct0[]{ct0Var5}, this.e);
            } else {
                throw new IllegalStateException("unsupported coordinate system");
            }
        }

        @Override // defpackage.pt0
        public pt0 d() {
            return new d(null, f(), g(), false);
        }

        @Override // defpackage.pt0
        public boolean h() {
            ct0 n = n();
            if (n.i()) {
                return false;
            }
            ct0 o = o();
            int j = j();
            return (j == 5 || j == 6) ? o.s() != n.s() : o.d(n).s();
        }

        @Override // defpackage.pt0
        public ct0 r() {
            int j = j();
            if (j == 5 || j == 6) {
                ct0 ct0Var = this.b;
                ct0 ct0Var2 = this.c;
                if (u() || ct0Var.i()) {
                    return ct0Var2;
                }
                ct0 j2 = ct0Var2.a(ct0Var).j(ct0Var);
                if (6 == j) {
                    ct0 ct0Var3 = this.d[0];
                    return !ct0Var3.h() ? j2.d(ct0Var3) : j2;
                }
                return j2;
            }
            return this.c;
        }

        @Override // defpackage.pt0
        public pt0 z() {
            if (u()) {
                return this;
            }
            ct0 ct0Var = this.b;
            if (ct0Var.i()) {
                return this;
            }
            int j = j();
            if (j != 0) {
                if (j == 1) {
                    return new d(this.a, ct0Var, this.c.a(ct0Var), new ct0[]{this.d[0]}, this.e);
                } else if (j != 5) {
                    if (j == 6) {
                        ct0 ct0Var2 = this.c;
                        ct0 ct0Var3 = this.d[0];
                        return new d(this.a, ct0Var, ct0Var2.a(ct0Var3), new ct0[]{ct0Var3}, this.e);
                    }
                    throw new IllegalStateException("unsupported coordinate system");
                } else {
                    return new d(this.a, ct0Var, this.c.b(), this.e);
                }
            }
            return new d(this.a, ct0Var, this.c.a(ct0Var), this.e);
        }
    }

    /* renamed from: pt0$e */
    /* loaded from: classes2.dex */
    public static class e extends c {
        public e(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
            super(xs0Var, ct0Var, ct0Var2);
            if ((ct0Var == null) != (ct0Var2 == null)) {
                throw new IllegalArgumentException("Exactly one of the field elements is null");
            }
            this.e = z;
        }

        public e(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
            super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
            this.e = z;
        }

        @Override // defpackage.pt0
        public pt0 H() {
            if (u()) {
                return this;
            }
            ct0 ct0Var = this.c;
            if (ct0Var.i()) {
                return this;
            }
            xs0 i = i();
            int r = i.r();
            if (r != 0) {
                return r != 4 ? J().a(this) : Q(false).a(this);
            }
            ct0 ct0Var2 = this.b;
            ct0 R = R(ct0Var);
            ct0 o = R.o();
            ct0 a = P(ct0Var2.o()).a(i().o());
            ct0 r2 = P(ct0Var2).j(o).r(a.o());
            if (r2.i()) {
                return i().v();
            }
            ct0 g = r2.j(R).g();
            ct0 j = r2.j(g).j(a);
            ct0 r3 = o.o().j(g).r(j);
            ct0 a2 = r3.r(j).j(j.a(r3)).a(ct0Var2);
            return new e(i, a2, ct0Var2.r(a2).j(r3).r(ct0Var), this.e);
        }

        @Override // defpackage.pt0
        public pt0 I(int i) {
            ct0 o;
            if (i >= 0) {
                if (i == 0 || u()) {
                    return this;
                }
                if (i == 1) {
                    return J();
                }
                xs0 i2 = i();
                ct0 ct0Var = this.c;
                if (ct0Var.i()) {
                    return i2.v();
                }
                int r = i2.r();
                ct0 o2 = i2.o();
                ct0 ct0Var2 = this.b;
                ct0[] ct0VarArr = this.d;
                ct0 n = ct0VarArr.length < 1 ? i2.n(ws0.b) : ct0VarArr[0];
                if (!n.h() && r != 0) {
                    if (r == 1) {
                        o = n.o();
                        ct0Var2 = ct0Var2.j(n);
                        ct0Var = ct0Var.j(o);
                    } else if (r == 2) {
                        o = null;
                    } else if (r != 4) {
                        throw new IllegalStateException("unsupported coordinate system");
                    } else {
                        o2 = O();
                    }
                    o2 = L(n, o);
                }
                int i3 = 0;
                ct0 ct0Var3 = o2;
                ct0 ct0Var4 = ct0Var;
                ct0 ct0Var5 = ct0Var2;
                ct0 ct0Var6 = ct0Var3;
                while (i3 < i) {
                    if (ct0Var4.i()) {
                        return i2.v();
                    }
                    ct0 P = P(ct0Var5.o());
                    ct0 R = R(ct0Var4);
                    ct0 j = R.j(ct0Var4);
                    ct0 R2 = R(ct0Var5.j(j));
                    ct0 R3 = R(j.o());
                    if (!ct0Var6.i()) {
                        P = P.a(ct0Var6);
                        ct0Var6 = R(R3.j(ct0Var6));
                    }
                    ct0 r2 = P.o().r(R(R2));
                    ct0Var4 = P.j(R2.r(r2)).r(R3);
                    n = n.h() ? R : R.j(n);
                    i3++;
                    ct0Var5 = r2;
                }
                if (r == 0) {
                    ct0 g = n.g();
                    ct0 o3 = g.o();
                    return new e(i2, ct0Var5.j(o3), ct0Var4.j(o3.j(g)), this.e);
                } else if (r != 1) {
                    if (r != 2) {
                        if (r == 4) {
                            return new e(i2, ct0Var5, ct0Var4, new ct0[]{n, ct0Var6}, this.e);
                        }
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                    return new e(i2, ct0Var5, ct0Var4, new ct0[]{n}, this.e);
                } else {
                    return new e(i2, ct0Var5.j(n), ct0Var4, new ct0[]{n.j(n.o())}, this.e);
                }
            }
            throw new IllegalArgumentException("'e' cannot be negative");
        }

        @Override // defpackage.pt0
        public pt0 J() {
            ct0 ct0Var;
            ct0 j;
            if (u()) {
                return this;
            }
            xs0 i = i();
            ct0 ct0Var2 = this.c;
            if (ct0Var2.i()) {
                return i.v();
            }
            int r = i.r();
            ct0 ct0Var3 = this.b;
            if (r == 0) {
                ct0 d = P(ct0Var3.o()).a(i().o()).d(R(ct0Var2));
                ct0 r2 = d.o().r(R(ct0Var3));
                return new e(i, r2, d.j(ct0Var3.r(r2)).r(ct0Var2), this.e);
            } else if (r == 1) {
                ct0 ct0Var4 = this.d[0];
                boolean h = ct0Var4.h();
                ct0 o = i.o();
                if (!o.i() && !h) {
                    o = o.j(ct0Var4.o());
                }
                ct0 a = o.a(P(ct0Var3.o()));
                ct0 j2 = h ? ct0Var2 : ct0Var2.j(ct0Var4);
                ct0 o2 = h ? ct0Var2.o() : j2.j(ct0Var2);
                ct0 N = N(ct0Var3.j(o2));
                ct0 r3 = a.o().r(R(N));
                ct0 R = R(j2);
                ct0 j3 = r3.j(R);
                ct0 R2 = R(o2);
                return new e(i, j3, N.r(r3).j(a).r(R(R2.o())), new ct0[]{R(h ? R(R2) : R.o()).j(j2)}, this.e);
            } else if (r != 2) {
                if (r == 4) {
                    return Q(true);
                }
                throw new IllegalStateException("unsupported coordinate system");
            } else {
                ct0 ct0Var5 = this.d[0];
                boolean h2 = ct0Var5.h();
                ct0 o3 = ct0Var2.o();
                ct0 o4 = o3.o();
                ct0 o5 = i.o();
                ct0 m = o5.m();
                if (m.t().equals(BigInteger.valueOf(3L))) {
                    ct0 o6 = h2 ? ct0Var5 : ct0Var5.o();
                    ct0Var = P(ct0Var3.a(o6).j(ct0Var3.r(o6)));
                    j = o3.j(ct0Var3);
                } else {
                    ct0 P = P(ct0Var3.o());
                    if (!h2) {
                        if (o5.i()) {
                            ct0Var = P;
                        } else {
                            ct0 o7 = ct0Var5.o().o();
                            if (m.c() < o5.c()) {
                                ct0Var = P.r(o7.j(m));
                            } else {
                                o5 = o7.j(o5);
                            }
                        }
                        j = ct0Var3.j(o3);
                    }
                    ct0Var = P.a(o5);
                    j = ct0Var3.j(o3);
                }
                ct0 N2 = N(j);
                ct0 r4 = ct0Var.o().r(R(N2));
                ct0 r5 = N2.r(r4).j(ct0Var).r(M(o4));
                ct0 R3 = R(ct0Var2);
                if (!h2) {
                    R3 = R3.j(ct0Var5);
                }
                return new e(i, r4, r5, new ct0[]{R3}, this.e);
            }
        }

        @Override // defpackage.pt0
        public pt0 K(pt0 pt0Var) {
            if (this == pt0Var) {
                return H();
            }
            if (u()) {
                return pt0Var;
            }
            if (pt0Var.u()) {
                return J();
            }
            ct0 ct0Var = this.c;
            if (ct0Var.i()) {
                return pt0Var;
            }
            xs0 i = i();
            int r = i.r();
            if (r != 0) {
                return r != 4 ? J().a(pt0Var) : Q(false).a(pt0Var);
            }
            ct0 ct0Var2 = this.b;
            ct0 ct0Var3 = pt0Var.b;
            ct0 ct0Var4 = pt0Var.c;
            ct0 r2 = ct0Var3.r(ct0Var2);
            ct0 r3 = ct0Var4.r(ct0Var);
            if (r2.i()) {
                return r3.i() ? H() : this;
            }
            ct0 o = r2.o();
            ct0 r4 = o.j(R(ct0Var2).a(ct0Var3)).r(r3.o());
            if (r4.i()) {
                return i.v();
            }
            ct0 g = r4.j(r2).g();
            ct0 j = r4.j(g).j(r3);
            ct0 r5 = R(ct0Var).j(o).j(r2).j(g).r(j);
            ct0 a = r5.r(j).j(j.a(r5)).a(ct0Var3);
            return new e(i, a, ct0Var2.r(a).j(r5).r(ct0Var), this.e);
        }

        public ct0 L(ct0 ct0Var, ct0 ct0Var2) {
            ct0 o = i().o();
            if (o.i() || ct0Var.h()) {
                return o;
            }
            if (ct0Var2 == null) {
                ct0Var2 = ct0Var.o();
            }
            ct0 o2 = ct0Var2.o();
            ct0 m = o.m();
            return m.c() < o.c() ? o2.j(m).m() : o2.j(o);
        }

        public ct0 M(ct0 ct0Var) {
            return N(R(ct0Var));
        }

        public ct0 N(ct0 ct0Var) {
            return R(R(ct0Var));
        }

        public ct0 O() {
            ct0[] ct0VarArr = this.d;
            ct0 ct0Var = ct0VarArr[1];
            if (ct0Var == null) {
                ct0 L = L(ct0VarArr[0], null);
                ct0VarArr[1] = L;
                return L;
            }
            return ct0Var;
        }

        public ct0 P(ct0 ct0Var) {
            return R(ct0Var).a(ct0Var);
        }

        public e Q(boolean z) {
            ct0 ct0Var = this.b;
            ct0 ct0Var2 = this.c;
            ct0 ct0Var3 = this.d[0];
            ct0 O = O();
            ct0 a = P(ct0Var.o()).a(O);
            ct0 R = R(ct0Var2);
            ct0 j = R.j(ct0Var2);
            ct0 R2 = R(ct0Var.j(j));
            ct0 r = a.o().r(R(R2));
            ct0 R3 = R(j.o());
            ct0 r2 = a.j(R2.r(r)).r(R3);
            ct0 R4 = z ? R(R3.j(O)) : null;
            if (!ct0Var3.h()) {
                R = R.j(ct0Var3);
            }
            return new e(i(), r, r2, new ct0[]{R, R4}, this.e);
        }

        public ct0 R(ct0 ct0Var) {
            return ct0Var.a(ct0Var);
        }

        /* JADX WARN: Code restructure failed: missing block: B:58:0x0123, code lost:
            if (r1 == r6) goto L34;
         */
        @Override // defpackage.pt0
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public defpackage.pt0 a(defpackage.pt0 r17) {
            /*
                Method dump skipped, instructions count: 543
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.pt0.e.a(pt0):pt0");
        }

        @Override // defpackage.pt0
        public pt0 d() {
            return new e(null, f(), g(), false);
        }

        @Override // defpackage.pt0
        public ct0 s(int i) {
            return (i == 1 && 4 == j()) ? O() : super.s(i);
        }

        @Override // defpackage.pt0
        public pt0 z() {
            if (u()) {
                return this;
            }
            xs0 i = i();
            return i.r() != 0 ? new e(i, this.b, this.c.m(), this.d, this.e) : new e(i, this.b, this.c.m(), this.e);
        }
    }

    public pt0(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, m(xs0Var));
    }

    public pt0(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr) {
        this.f = null;
        this.a = xs0Var;
        this.b = ct0Var;
        this.c = ct0Var2;
        this.d = ct0VarArr;
    }

    public static ct0[] m(xs0 xs0Var) {
        int r = xs0Var == null ? 0 : xs0Var.r();
        if (r == 0 || r == 5) {
            return g;
        }
        ct0 n = xs0Var.n(ws0.b);
        if (r != 1 && r != 2) {
            if (r == 3) {
                return new ct0[]{n, n, n};
            }
            if (r == 4) {
                return new ct0[]{n, xs0Var.o()};
            }
            if (r != 6) {
                throw new IllegalArgumentException("unknown coordinate system");
            }
        }
        return new ct0[]{n};
    }

    public pt0 A() {
        int j;
        if (u() || (j = j()) == 0 || j == 5) {
            return this;
        }
        ct0 s = s(0);
        return s.h() ? this : B(s.g());
    }

    public pt0 B(ct0 ct0Var) {
        int j = j();
        if (j != 1) {
            if (j == 2 || j == 3 || j == 4) {
                ct0 o = ct0Var.o();
                return c(o, o.j(ct0Var));
            } else if (j != 6) {
                throw new IllegalStateException("not a projective coordinate system");
            }
        }
        return c(ct0Var, ct0Var);
    }

    public abstract boolean C();

    public boolean D() {
        BigInteger x;
        return ws0.b.equals(this.a.q()) || (x = this.a.x()) == null || vs0.o(this, x).u();
    }

    public pt0 E(ct0 ct0Var) {
        return u() ? this : i().j(n().j(ct0Var), o(), p(), this.e);
    }

    public pt0 F(ct0 ct0Var) {
        return u() ? this : i().j(n(), o().j(ct0Var), p(), this.e);
    }

    public abstract pt0 G(pt0 pt0Var);

    public pt0 H() {
        return K(this);
    }

    public pt0 I(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("'e' cannot be negative");
        }
        pt0 pt0Var = this;
        while (true) {
            i--;
            if (i < 0) {
                return pt0Var;
            }
            pt0Var = pt0Var.J();
        }
    }

    public abstract pt0 J();

    public pt0 K(pt0 pt0Var) {
        return J().a(pt0Var);
    }

    public abstract pt0 a(pt0 pt0Var);

    public void b() {
        if (!v()) {
            throw new IllegalStateException("point not in normal form");
        }
    }

    public pt0 c(ct0 ct0Var, ct0 ct0Var2) {
        return i().i(n().j(ct0Var), o().j(ct0Var2), this.e);
    }

    public abstract pt0 d();

    public boolean e(pt0 pt0Var) {
        pt0 pt0Var2;
        if (pt0Var == null) {
            return false;
        }
        xs0 i = i();
        xs0 i2 = pt0Var.i();
        boolean z = i == null;
        boolean z2 = i2 == null;
        boolean u = u();
        boolean u2 = pt0Var.u();
        if (u || u2) {
            if (u && u2) {
                return z || z2 || i.m(i2);
            }
            return false;
        }
        if (!z || !z2) {
            if (!z) {
                if (z2) {
                    pt0Var2 = A();
                } else if (!i.m(i2)) {
                    return false;
                } else {
                    pt0[] pt0VarArr = {this, i.z(pt0Var)};
                    i.A(pt0VarArr);
                    pt0Var2 = pt0VarArr[0];
                    pt0Var = pt0VarArr[1];
                }
                return pt0Var2.q().equals(pt0Var.q()) && pt0Var2.r().equals(pt0Var.r());
            }
            pt0Var = pt0Var.A();
        }
        pt0Var2 = this;
        if (pt0Var2.q().equals(pt0Var.q())) {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof pt0) {
            return e((pt0) obj);
        }
        return false;
    }

    public ct0 f() {
        b();
        return q();
    }

    public ct0 g() {
        b();
        return r();
    }

    public abstract boolean h();

    public int hashCode() {
        xs0 i = i();
        int i2 = i == null ? 0 : ~i.hashCode();
        if (u()) {
            return i2;
        }
        pt0 A = A();
        return (i2 ^ (A.q().hashCode() * 17)) ^ (A.r().hashCode() * 257);
    }

    public xs0 i() {
        return this.a;
    }

    public int j() {
        xs0 xs0Var = this.a;
        if (xs0Var == null) {
            return 0;
        }
        return xs0Var.r();
    }

    public final pt0 k() {
        return A().d();
    }

    public byte[] l(boolean z) {
        if (u()) {
            return new byte[1];
        }
        pt0 A = A();
        byte[] e2 = A.q().e();
        if (z) {
            byte[] bArr = new byte[e2.length + 1];
            bArr[0] = (byte) (A.h() ? 3 : 2);
            System.arraycopy(e2, 0, bArr, 1, e2.length);
            return bArr;
        }
        byte[] e3 = A.r().e();
        byte[] bArr2 = new byte[e2.length + e3.length + 1];
        bArr2[0] = 4;
        System.arraycopy(e2, 0, bArr2, 1, e2.length);
        System.arraycopy(e3, 0, bArr2, e2.length + 1, e3.length);
        return bArr2;
    }

    public final ct0 n() {
        return this.b;
    }

    public final ct0 o() {
        return this.c;
    }

    public final ct0[] p() {
        return this.d;
    }

    public ct0 q() {
        return this.b;
    }

    public ct0 r() {
        return this.c;
    }

    public ct0 s(int i) {
        if (i >= 0) {
            ct0[] ct0VarArr = this.d;
            if (i < ct0VarArr.length) {
                return ct0VarArr[i];
            }
        }
        return null;
    }

    public boolean t(boolean z) {
        if (u()) {
            return true;
        }
        return !((vg4) i().C(this, "bc_validity", new a(z))).b();
    }

    public String toString() {
        if (u()) {
            return "INF";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        stringBuffer.append(n());
        stringBuffer.append(',');
        stringBuffer.append(o());
        for (int i = 0; i < this.d.length; i++) {
            stringBuffer.append(',');
            stringBuffer.append(this.d[i]);
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }

    public boolean u() {
        if (this.b != null && this.c != null) {
            ct0[] ct0VarArr = this.d;
            if (ct0VarArr.length <= 0 || !ct0VarArr[0].i()) {
                return false;
            }
        }
        return true;
    }

    public boolean v() {
        int j = j();
        return j == 0 || j == 5 || u() || this.d[0].h();
    }

    public boolean w() {
        return t(true);
    }

    public boolean x() {
        return t(false);
    }

    public pt0 y(BigInteger bigInteger) {
        return i().w().a(this, bigInteger);
    }

    public abstract pt0 z();
}
