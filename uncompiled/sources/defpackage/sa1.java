package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.creageek.segmentedbutton.SegmentedButton;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.radiobutton.MaterialRadioButton;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.TouchControlLineChart;

/* compiled from: FragmentReflectionsAdvanceBinding.java */
/* renamed from: sa1  reason: default package */
/* loaded from: classes2.dex */
public final class sa1 {
    public final AppBarLayout a;
    public final yk1 b;
    public final TouchControlLineChart c;
    public final RecyclerView d;
    public final SegmentedButton e;
    public final SegmentedButton f;
    public final g74 g;
    public final xk1 h;
    public final MaterialRadioButton i;
    public final MaterialRadioButton j;
    public final MaterialRadioButton k;
    public final AppCompatTextView l;

    public sa1(ConstraintLayout constraintLayout, AppBarLayout appBarLayout, CollapsingToolbarLayout collapsingToolbarLayout, CoordinatorLayout coordinatorLayout, yk1 yk1Var, TouchControlLineChart touchControlLineChart, RecyclerView recyclerView, SegmentedButton segmentedButton, SegmentedButton segmentedButton2, MaterialRadioButton materialRadioButton, MaterialRadioButton materialRadioButton2, MaterialRadioButton materialRadioButton3, MaterialRadioButton materialRadioButton4, g74 g74Var, xk1 xk1Var, MaterialRadioButton materialRadioButton5, MaterialRadioButton materialRadioButton6, MaterialRadioButton materialRadioButton7, AppCompatTextView appCompatTextView) {
        this.a = appBarLayout;
        this.b = yk1Var;
        this.c = touchControlLineChart;
        this.d = recyclerView;
        this.e = segmentedButton;
        this.f = segmentedButton2;
        this.g = g74Var;
        this.h = xk1Var;
        this.i = materialRadioButton5;
        this.j = materialRadioButton6;
        this.k = materialRadioButton7;
        this.l = appCompatTextView;
    }

    public static sa1 a(View view) {
        int i = R.id.appBar;
        AppBarLayout appBarLayout = (AppBarLayout) ai4.a(view, R.id.appBar);
        if (appBarLayout != null) {
            i = R.id.ccToolBar;
            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) ai4.a(view, R.id.ccToolBar);
            if (collapsingToolbarLayout != null) {
                i = R.id.coordinator;
                CoordinatorLayout coordinatorLayout = (CoordinatorLayout) ai4.a(view, R.id.coordinator);
                if (coordinatorLayout != null) {
                    i = R.id.lSelectToken;
                    View a = ai4.a(view, R.id.lSelectToken);
                    if (a != null) {
                        yk1 a2 = yk1.a(a);
                        i = R.id.lineStickChart;
                        TouchControlLineChart touchControlLineChart = (TouchControlLineChart) ai4.a(view, R.id.lineStickChart);
                        if (touchControlLineChart != null) {
                            i = R.id.rvReflectionList;
                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvReflectionList);
                            if (recyclerView != null) {
                                i = R.id.sbTokenMode;
                                SegmentedButton segmentedButton = (SegmentedButton) ai4.a(view, R.id.sbTokenMode);
                                if (segmentedButton != null) {
                                    i = R.id.sbTokenSpan;
                                    SegmentedButton segmentedButton2 = (SegmentedButton) ai4.a(view, R.id.sbTokenSpan);
                                    if (segmentedButton2 != null) {
                                        i = R.id.tm_daily;
                                        MaterialRadioButton materialRadioButton = (MaterialRadioButton) ai4.a(view, R.id.tm_daily);
                                        if (materialRadioButton != null) {
                                            i = R.id.tm_monthly;
                                            MaterialRadioButton materialRadioButton2 = (MaterialRadioButton) ai4.a(view, R.id.tm_monthly);
                                            if (materialRadioButton2 != null) {
                                                i = R.id.tm_weekly;
                                                MaterialRadioButton materialRadioButton3 = (MaterialRadioButton) ai4.a(view, R.id.tm_weekly);
                                                if (materialRadioButton3 != null) {
                                                    i = R.id.tm_yearly;
                                                    MaterialRadioButton materialRadioButton4 = (MaterialRadioButton) ai4.a(view, R.id.tm_yearly);
                                                    if (materialRadioButton4 != null) {
                                                        i = R.id.toolbar;
                                                        View a3 = ai4.a(view, R.id.toolbar);
                                                        if (a3 != null) {
                                                            g74 a4 = g74.a(a3);
                                                            i = R.id.totalField;
                                                            View a5 = ai4.a(view, R.id.totalField);
                                                            if (a5 != null) {
                                                                xk1 a6 = xk1.a(a5);
                                                                i = R.id.ts_max;
                                                                MaterialRadioButton materialRadioButton5 = (MaterialRadioButton) ai4.a(view, R.id.ts_max);
                                                                if (materialRadioButton5 != null) {
                                                                    i = R.id.ts_med;
                                                                    MaterialRadioButton materialRadioButton6 = (MaterialRadioButton) ai4.a(view, R.id.ts_med);
                                                                    if (materialRadioButton6 != null) {
                                                                        i = R.id.ts_min;
                                                                        MaterialRadioButton materialRadioButton7 = (MaterialRadioButton) ai4.a(view, R.id.ts_min);
                                                                        if (materialRadioButton7 != null) {
                                                                            i = R.id.txtLoading;
                                                                            AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtLoading);
                                                                            if (appCompatTextView != null) {
                                                                                return new sa1((ConstraintLayout) view, appBarLayout, collapsingToolbarLayout, coordinatorLayout, a2, touchControlLineChart, recyclerView, segmentedButton, segmentedButton2, materialRadioButton, materialRadioButton2, materialRadioButton3, materialRadioButton4, a4, a6, materialRadioButton5, materialRadioButton6, materialRadioButton7, appCompatTextView);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
