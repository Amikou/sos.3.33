package defpackage;

/* compiled from: BiFunction.java */
/* renamed from: fp  reason: default package */
/* loaded from: classes2.dex */
public interface fp<T, U, R> {
    R apply(T t, U u);
}
