package defpackage;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.bouncycastle.asn1.i;

/* renamed from: ko2  reason: default package */
/* loaded from: classes2.dex */
public class ko2 {
    public Hashtable a;
    public Vector b;

    public ko2() {
        this(new Hashtable(), new Vector());
    }

    public ko2(Hashtable hashtable, Vector vector) {
        this.a = hashtable;
        this.b = vector;
    }

    public c4 a(i iVar) {
        return (c4) this.a.get(iVar);
    }

    public Enumeration b() {
        return this.b.elements();
    }

    public void c(i iVar, c4 c4Var) {
        if (this.a.containsKey(iVar)) {
            this.a.put(iVar, c4Var);
            return;
        }
        this.a.put(iVar, c4Var);
        this.b.addElement(iVar);
    }
}
