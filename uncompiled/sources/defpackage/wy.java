package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.CircularProgressIndicatorSpec;

/* compiled from: CircularDrawingDelegate.java */
/* renamed from: wy  reason: default package */
/* loaded from: classes2.dex */
public final class wy extends nr0<CircularProgressIndicatorSpec> {
    public int c;
    public float d;
    public float e;
    public float f;

    public wy(CircularProgressIndicatorSpec circularProgressIndicatorSpec) {
        super(circularProgressIndicatorSpec);
        this.c = 1;
    }

    @Override // defpackage.nr0
    public void a(Canvas canvas, float f) {
        S s;
        S s2 = this.a;
        float f2 = (((CircularProgressIndicatorSpec) s2).g / 2.0f) + ((CircularProgressIndicatorSpec) s2).h;
        canvas.translate(f2, f2);
        canvas.rotate(-90.0f);
        float f3 = -f2;
        canvas.clipRect(f3, f3, f2, f2);
        this.c = ((CircularProgressIndicatorSpec) this.a).i == 0 ? 1 : -1;
        this.d = ((CircularProgressIndicatorSpec) s).a * f;
        this.e = ((CircularProgressIndicatorSpec) s).b * f;
        this.f = (((CircularProgressIndicatorSpec) s).g - ((CircularProgressIndicatorSpec) s).a) / 2.0f;
        if ((this.b.j() && ((CircularProgressIndicatorSpec) this.a).e == 2) || (this.b.i() && ((CircularProgressIndicatorSpec) this.a).f == 1)) {
            this.f += ((1.0f - f) * ((CircularProgressIndicatorSpec) this.a).a) / 2.0f;
        } else if ((this.b.j() && ((CircularProgressIndicatorSpec) this.a).e == 1) || (this.b.i() && ((CircularProgressIndicatorSpec) this.a).f == 2)) {
            this.f -= ((1.0f - f) * ((CircularProgressIndicatorSpec) this.a).a) / 2.0f;
        }
    }

    @Override // defpackage.nr0
    public void b(Canvas canvas, Paint paint, float f, float f2, int i) {
        if (f == f2) {
            return;
        }
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.BUTT);
        paint.setAntiAlias(true);
        paint.setColor(i);
        paint.setStrokeWidth(this.d);
        int i2 = this.c;
        float f3 = f * 360.0f * i2;
        float f4 = (f2 >= f ? f2 - f : (f2 + 1.0f) - f) * 360.0f * i2;
        float f5 = this.f;
        canvas.drawArc(new RectF(-f5, -f5, f5, f5), f3, f4, false, paint);
        if (this.e <= Utils.FLOAT_EPSILON || Math.abs(f4) >= 360.0f) {
            return;
        }
        paint.setStyle(Paint.Style.FILL);
        float f6 = this.e;
        RectF rectF = new RectF(-f6, -f6, f6, f6);
        h(canvas, paint, this.d, this.e, f3, true, rectF);
        h(canvas, paint, this.d, this.e, f3 + f4, false, rectF);
    }

    @Override // defpackage.nr0
    public void c(Canvas canvas, Paint paint) {
        int a = l42.a(((CircularProgressIndicatorSpec) this.a).d, this.b.getAlpha());
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.BUTT);
        paint.setAntiAlias(true);
        paint.setColor(a);
        paint.setStrokeWidth(this.d);
        float f = this.f;
        canvas.drawArc(new RectF(-f, -f, f, f), Utils.FLOAT_EPSILON, 360.0f, false, paint);
    }

    @Override // defpackage.nr0
    public int d() {
        return i();
    }

    @Override // defpackage.nr0
    public int e() {
        return i();
    }

    public final void h(Canvas canvas, Paint paint, float f, float f2, float f3, boolean z, RectF rectF) {
        float f4 = z ? -1.0f : 1.0f;
        canvas.save();
        canvas.rotate(f3);
        float f5 = f / 2.0f;
        float f6 = f4 * f2;
        canvas.drawRect((this.f - f5) + f2, Math.min((float) Utils.FLOAT_EPSILON, this.c * f6), (this.f + f5) - f2, Math.max((float) Utils.FLOAT_EPSILON, f6 * this.c), paint);
        canvas.translate((this.f - f5) + f2, Utils.FLOAT_EPSILON);
        canvas.drawArc(rectF, 180.0f, (-f4) * 90.0f * this.c, true, paint);
        canvas.translate(f - (f2 * 2.0f), Utils.FLOAT_EPSILON);
        canvas.drawArc(rectF, Utils.FLOAT_EPSILON, f4 * 90.0f * this.c, true, paint);
        canvas.restore();
    }

    public final int i() {
        S s = this.a;
        return ((CircularProgressIndicatorSpec) s).g + (((CircularProgressIndicatorSpec) s).h * 2);
    }
}
