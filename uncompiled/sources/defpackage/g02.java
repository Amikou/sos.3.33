package defpackage;

import com.google.common.collect.ImmutableList;

/* compiled from: ListChunk.java */
/* renamed from: g02  reason: default package */
/* loaded from: classes.dex */
public final class g02 implements sl {
    public final ImmutableList<sl> a;
    public final int b;

    public g02(int i, ImmutableList<sl> immutableList) {
        this.b = i;
        this.a = immutableList;
    }

    public static sl a(int i, int i2, op2 op2Var) {
        switch (i) {
            case 1718776947:
                return cu3.d(i2, op2Var);
            case 1751742049:
                return ul.b(op2Var);
            case 1752331379:
                return vl.c(op2Var);
            case 1852994675:
                return eu3.a(op2Var);
            default:
                return null;
        }
    }

    public static g02 c(int i, op2 op2Var) {
        sl a;
        ImmutableList.a aVar = new ImmutableList.a();
        int f = op2Var.f();
        int i2 = -2;
        while (op2Var.a() > 8) {
            int q = op2Var.q();
            int e = op2Var.e() + op2Var.q();
            op2Var.O(e);
            if (q == 1414744396) {
                a = c(op2Var.q(), op2Var);
            } else {
                a = a(q, i2, op2Var);
            }
            if (a != null) {
                if (a.getType() == 1752331379) {
                    i2 = ((vl) a).b();
                }
                aVar.a(a);
            }
            op2Var.P(e);
            op2Var.O(f);
        }
        return new g02(i, aVar.l());
    }

    public <T extends sl> T b(Class<T> cls) {
        af4<sl> it = this.a.iterator();
        while (it.hasNext()) {
            T t = (T) it.next();
            if (t.getClass() == cls) {
                return t;
            }
        }
        return null;
    }

    @Override // defpackage.sl
    public int getType() {
        return this.b;
    }
}
