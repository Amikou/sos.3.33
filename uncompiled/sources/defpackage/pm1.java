package defpackage;

/* compiled from: IRegisterDeviceTokenUseCase.kt */
/* renamed from: pm1  reason: default package */
/* loaded from: classes2.dex */
public interface pm1 {

    /* compiled from: IRegisterDeviceTokenUseCase.kt */
    /* renamed from: pm1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ void a(pm1 pm1Var, String str, String[] strArr, tc1 tc1Var, int i, Object obj) {
            if (obj != null) {
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: register");
            }
            if ((i & 4) != 0) {
                tc1Var = null;
            }
            pm1Var.a(str, strArr, tc1Var);
        }
    }

    void a(String str, String[] strArr, tc1<? super Boolean, te4> tc1Var);
}
