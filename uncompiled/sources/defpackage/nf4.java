package defpackage;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: Uploader_Factory.java */
/* renamed from: nf4  reason: default package */
/* loaded from: classes.dex */
public final class nf4 implements z11<mf4> {
    public final ew2<Context> a;
    public final ew2<bm> b;
    public final ew2<dy0> c;
    public final ew2<rq4> d;
    public final ew2<Executor> e;
    public final ew2<l24> f;
    public final ew2<qz> g;

    public nf4(ew2<Context> ew2Var, ew2<bm> ew2Var2, ew2<dy0> ew2Var3, ew2<rq4> ew2Var4, ew2<Executor> ew2Var5, ew2<l24> ew2Var6, ew2<qz> ew2Var7) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
        this.d = ew2Var4;
        this.e = ew2Var5;
        this.f = ew2Var6;
        this.g = ew2Var7;
    }

    public static nf4 a(ew2<Context> ew2Var, ew2<bm> ew2Var2, ew2<dy0> ew2Var3, ew2<rq4> ew2Var4, ew2<Executor> ew2Var5, ew2<l24> ew2Var6, ew2<qz> ew2Var7) {
        return new nf4(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5, ew2Var6, ew2Var7);
    }

    public static mf4 c(Context context, bm bmVar, dy0 dy0Var, rq4 rq4Var, Executor executor, l24 l24Var, qz qzVar) {
        return new mf4(context, bmVar, dy0Var, rq4Var, executor, l24Var, qzVar);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public mf4 get() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get());
    }
}
