package defpackage;

import android.content.Context;
import com.yariksoffice.lingver.Lingver;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LanguageItem;

/* compiled from: Languages.kt */
/* renamed from: ly1  reason: default package */
/* loaded from: classes2.dex */
public final class ly1 {
    public static final ly1 a = new ly1();

    /* JADX WARN: Removed duplicated region for block: B:19:0x0058 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final net.safemoon.androidwallet.model.common.LanguageItem a() {
        /*
            r8 = this;
            android.content.res.Resources r0 = android.content.res.Resources.getSystem()
            android.content.res.Configuration r0 = r0.getConfiguration()
            c12 r0 = defpackage.y40.a(r0)
            r1 = 0
            java.util.Locale r0 = r0.c(r1)
            java.lang.String r0 = r0.getLanguage()
            java.util.List r2 = r8.b()
            net.safemoon.androidwallet.model.common.LanguageItem r3 = new net.safemoon.androidwallet.model.common.LanguageItem
            java.lang.String r4 = "en"
            r5 = 2131952193(0x7f130241, float:1.9540822E38)
            r6 = 2131952194(0x7f130242, float:1.9540824E38)
            r3.<init>(r4, r5, r6)
            java.util.Iterator r2 = r2.iterator()
        L2a:
            boolean r4 = r2.hasNext()
            r5 = 0
            if (r4 == 0) goto L59
            java.lang.Object r4 = r2.next()
            r6 = r4
            net.safemoon.androidwallet.model.common.LanguageItem r6 = (net.safemoon.androidwallet.model.common.LanguageItem) r6
            java.lang.String r7 = r6.getLanguageCode()
            boolean r7 = defpackage.fs1.b(r7, r0)
            if (r7 != 0) goto L55
            java.lang.String r6 = r6.getLanguageCode()
            java.lang.String r7 = "language"
            defpackage.fs1.e(r0, r7)
            r7 = 2
            boolean r5 = kotlin.text.StringsKt__StringsKt.M(r6, r0, r1, r7, r5)
            if (r5 == 0) goto L53
            goto L55
        L53:
            r5 = r1
            goto L56
        L55:
            r5 = 1
        L56:
            if (r5 == 0) goto L2a
            r5 = r4
        L59:
            net.safemoon.androidwallet.model.common.LanguageItem r5 = (net.safemoon.androidwallet.model.common.LanguageItem) r5
            if (r5 != 0) goto L5e
            goto L5f
        L5e:
            r3 = r5
        L5f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ly1.a():net.safemoon.androidwallet.model.common.LanguageItem");
    }

    public final List<LanguageItem> b() {
        return b20.l(new LanguageItem("zh", R.string.language_chinese_simple, R.string.language_chinese_simple_location), new LanguageItem("zh-rHK", R.string.language_chinese_hk, R.string.language_chinese_hk_location), new LanguageItem("nl", R.string.language_dutch, R.string.language_dutch_location), new LanguageItem("en", R.string.language_english, R.string.language_english_location), new LanguageItem("fr", R.string.language_french, R.string.language_french_location), new LanguageItem("de", R.string.language_german, R.string.language_german_location), new LanguageItem("hi", R.string.language_hindi, R.string.language_hindi_location), new LanguageItem("in", R.string.language_indonesian, R.string.language_indonesian_location), new LanguageItem("it", R.string.language_italian, R.string.language_italian_location), new LanguageItem("fa", R.string.language_persian, R.string.language_persian_location), new LanguageItem("pl", R.string.language_polish, R.string.language_polish_location), new LanguageItem("pt-rBR", R.string.language_portuguese_brazil, R.string.language_portuguese_brazil_location), new LanguageItem("pt-rPT", R.string.language_portuguese_portugal, R.string.language_portuguese_portugal_location), new LanguageItem("es", R.string.language_spanish, R.string.language_spanish_location), new LanguageItem("es-rMX", R.string.language_spanish_mexico, R.string.language_spanish_mexico_location), new LanguageItem("tr", R.string.language_turkish, R.string.language_turkish_location), new LanguageItem("vi", R.string.language_vietnamese, R.string.language_vietnamese_location));
    }

    public final void c(Context context, LanguageItem languageItem) {
        fs1.f(context, "context");
        fs1.f(languageItem, "item");
        do3.a.e(context, languageItem.getLanguageCode());
        if (StringsKt__StringsKt.M(languageItem.getLanguageCode(), "-r", false, 2, null)) {
            List w0 = StringsKt__StringsKt.w0(languageItem.getLanguageCode(), new String[]{"-r"}, false, 0, 6, null);
            Lingver b = Lingver.f.b();
            Context applicationContext = context.getApplicationContext();
            fs1.e(applicationContext, "context.applicationContext");
            Lingver.m(b, applicationContext, (String) w0.get(0), (String) w0.get(1), null, 8, null);
            return;
        }
        Lingver b2 = Lingver.f.b();
        Context applicationContext2 = context.getApplicationContext();
        fs1.e(applicationContext2, "context.applicationContext");
        Lingver.m(b2, applicationContext2, languageItem.getLanguageCode(), null, null, 12, null);
    }
}
