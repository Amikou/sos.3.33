package defpackage;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/* compiled from: DrawableBytesTranscoder.java */
/* renamed from: rq0  reason: default package */
/* loaded from: classes.dex */
public final class rq0 implements e83<Drawable, byte[]> {
    public final jq a;
    public final e83<Bitmap, byte[]> b;
    public final e83<uf1, byte[]> c;

    public rq0(jq jqVar, e83<Bitmap, byte[]> e83Var, e83<uf1, byte[]> e83Var2) {
        this.a = jqVar;
        this.b = e83Var;
        this.c = e83Var2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static s73<uf1> b(s73<Drawable> s73Var) {
        return s73Var;
    }

    @Override // defpackage.e83
    public s73<byte[]> a(s73<Drawable> s73Var, vn2 vn2Var) {
        Drawable drawable = s73Var.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(oq.f(((BitmapDrawable) drawable).getBitmap(), this.a), vn2Var);
        }
        if (drawable instanceof uf1) {
            return this.c.a(b(s73Var), vn2Var);
        }
        return null;
    }
}
