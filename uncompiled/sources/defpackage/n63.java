package defpackage;

import android.app.RemoteInput;

/* compiled from: RemoteInput.java */
/* renamed from: n63  reason: default package */
/* loaded from: classes.dex */
public final class n63 {
    public static RemoteInput a(n63 n63Var) {
        throw null;
    }

    public static RemoteInput[] b(n63[] n63VarArr) {
        if (n63VarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[n63VarArr.length];
        for (int i = 0; i < n63VarArr.length; i++) {
            remoteInputArr[i] = a(n63VarArr[i]);
        }
        return remoteInputArr;
    }
}
