package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: n93  reason: default package */
/* loaded from: classes2.dex */
public interface n93 {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;
    public static final i g;
    public static final i h;
    public static final i i;

    static {
        i iVar = new i("1.2.643.7");
        a = iVar;
        i z = iVar.z("1");
        b = z;
        z.z("1.2.2");
        z.z("1.2.3");
        z.z("1.4.1");
        z.z("1.4.2");
        z.z("1.1.1");
        z.z("1.1.2");
        z.z("1.3.2");
        z.z("1.3.3");
        i z2 = z.z("1.6");
        c = z2;
        z2.z("1");
        z2.z("2");
        i z3 = z.z("2.1.1");
        d = z3;
        e = z3.z("1");
        i z4 = z.z("2.1.2");
        f = z4;
        g = z4.z("1");
        h = z4.z("2");
        i = z4.z("3");
        z.z("2.5.1.1");
    }
}
