package defpackage;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/* compiled from: MessageDigest.java */
/* renamed from: y72  reason: default package */
/* loaded from: classes.dex */
public abstract class y72 {
    public byte[] a;
    public boolean b;

    public final void a(PrintStream printStream, byte b) {
        char c = (char) ((b >> 4) & 15);
        printStream.write((char) (c > '\t' ? (c - '\n') + 65 : c + '0'));
        char c2 = (char) (b & 15);
        printStream.write((char) (c2 > '\t' ? (c2 - '\n') + 65 : c2 + '0'));
    }

    public abstract void b(byte b);

    public synchronized void c(byte[] bArr) {
        d(bArr, 0, bArr.length);
    }

    public synchronized void d(byte[] bArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            b(bArr[i3 + i]);
        }
    }

    public String toString() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        if (this.b) {
            int i = 0;
            while (true) {
                byte[] bArr = this.a;
                if (i >= bArr.length) {
                    break;
                }
                a(printStream, bArr[i]);
                i++;
            }
        } else {
            printStream.print("<incomplete>");
        }
        return byteArrayOutputStream.toString();
    }
}
