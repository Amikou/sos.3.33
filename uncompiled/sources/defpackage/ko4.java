package defpackage;

import java.math.BigInteger;
import java.util.List;
import org.web3j.protocol.core.a;
import org.web3j.protocol.core.c;

/* compiled from: Web3j.java */
/* renamed from: ko4  reason: default package */
/* loaded from: classes3.dex */
public interface ko4 {
    /* synthetic */ c<?, w9> adminNodeInfo();

    /* synthetic */ c<?, x9> adminPeers();

    /* synthetic */ q71<gw0> blockFlowable(boolean z);

    /* synthetic */ c<?, te0> dbGetHex(String str, String str2);

    /* synthetic */ c<?, ue0> dbGetString(String str, String str2);

    /* synthetic */ c<?, ve0> dbPutHex(String str, String str2, String str3);

    /* synthetic */ c<?, we0> dbPutString(String str, String str2, String str3);

    /* synthetic */ c<?, fw0> ethAccounts();

    /* synthetic */ q71<String> ethBlockHashFlowable();

    /* synthetic */ c<?, hw0> ethBlockNumber();

    /* synthetic */ c<?, iw0> ethCall(p84 p84Var, gi0 gi0Var);

    /* synthetic */ c<?, jw0> ethChainId();

    /* synthetic */ c<?, kw0> ethCoinbase();

    /* synthetic */ c<?, lw0> ethCompileLLL(String str);

    /* synthetic */ c<?, mw0> ethCompileSerpent(String str);

    /* synthetic */ c<?, nw0> ethCompileSolidity(String str);

    /* synthetic */ c<?, ow0> ethEstimateGas(p84 p84Var);

    /* synthetic */ c<?, rw0> ethGasPrice();

    /* synthetic */ c<?, sw0> ethGetBalance(String str, gi0 gi0Var);

    /* synthetic */ c<?, gw0> ethGetBlockByHash(String str, boolean z);

    /* synthetic */ c<?, gw0> ethGetBlockByNumber(gi0 gi0Var, boolean z);

    /* synthetic */ c<?, tw0> ethGetBlockTransactionCountByHash(String str);

    /* synthetic */ c<?, uw0> ethGetBlockTransactionCountByNumber(gi0 gi0Var);

    /* synthetic */ c<?, vw0> ethGetCode(String str, gi0 gi0Var);

    /* synthetic */ c<?, ww0> ethGetCompilers();

    /* synthetic */ c<?, ex0> ethGetFilterChanges(BigInteger bigInteger);

    /* synthetic */ c<?, ex0> ethGetFilterLogs(BigInteger bigInteger);

    /* synthetic */ c<?, ex0> ethGetLogs(qw0 qw0Var);

    /* synthetic */ c<?, xw0> ethGetStorageAt(String str, BigInteger bigInteger, gi0 gi0Var);

    /* synthetic */ c<?, nx0> ethGetTransactionByBlockHashAndIndex(String str, BigInteger bigInteger);

    /* synthetic */ c<?, nx0> ethGetTransactionByBlockNumberAndIndex(gi0 gi0Var, BigInteger bigInteger);

    /* synthetic */ c<?, nx0> ethGetTransactionByHash(String str);

    /* synthetic */ c<?, yw0> ethGetTransactionCount(String str, gi0 gi0Var);

    /* synthetic */ c<?, zw0> ethGetTransactionReceipt(String str);

    /* synthetic */ c<?, gw0> ethGetUncleByBlockHashAndIndex(String str, BigInteger bigInteger);

    /* synthetic */ c<?, gw0> ethGetUncleByBlockNumberAndIndex(gi0 gi0Var, BigInteger bigInteger);

    /* synthetic */ c<?, ax0> ethGetUncleCountByBlockHash(String str);

    /* synthetic */ c<?, bx0> ethGetUncleCountByBlockNumber(gi0 gi0Var);

    /* synthetic */ c<?, cx0> ethGetWork();

    /* synthetic */ c<?, dx0> ethHashrate();

    /* synthetic */ q71<q12> ethLogFlowable(qw0 qw0Var);

    /* synthetic */ c<?, fx0> ethMining();

    /* synthetic */ c<?, pw0> ethNewBlockFilter();

    /* synthetic */ c<?, pw0> ethNewFilter(qw0 qw0Var);

    /* synthetic */ c<?, pw0> ethNewPendingTransactionFilter();

    /* synthetic */ q71<String> ethPendingTransactionHashFlowable();

    /* synthetic */ c<?, gx0> ethProtocolVersion();

    /* synthetic */ c<?, hx0> ethSendRawTransaction(String str);

    /* synthetic */ c<?, hx0> ethSendTransaction(p84 p84Var);

    /* synthetic */ c<?, ix0> ethSign(String str, String str2);

    /* synthetic */ c<?, jx0> ethSubmitHashrate(String str, String str2);

    /* synthetic */ c<?, kx0> ethSubmitWork(String str, String str2, String str3);

    /* synthetic */ c<?, mx0> ethSyncing();

    /* synthetic */ c<?, ox0> ethUninstallFilter(BigInteger bigInteger);

    /* synthetic */ q71<s12> logsNotifications(List<String> list, List<String> list2);

    /* synthetic */ c<?, ve2> netListening();

    /* synthetic */ c<?, we2> netPeerCount();

    /* synthetic */ c<?, ye2> netVersion();

    /* synthetic */ a newBatch();

    /* synthetic */ q71<hf2> newHeadsNotifications();

    /* synthetic */ q71<o84> pendingTransactionFlowable();

    /* synthetic */ q71<gw0> replayPastAndFutureBlocksFlowable(gi0 gi0Var, boolean z);

    /* synthetic */ q71<o84> replayPastAndFutureTransactionsFlowable(gi0 gi0Var);

    /* synthetic */ q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, gi0 gi0Var2, boolean z);

    /* synthetic */ q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, gi0 gi0Var2, boolean z, boolean z2);

    /* synthetic */ q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, boolean z);

    /* synthetic */ q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, boolean z, q71<gw0> q71Var);

    /* synthetic */ q71<o84> replayPastTransactionsFlowable(gi0 gi0Var);

    /* synthetic */ q71<o84> replayPastTransactionsFlowable(gi0 gi0Var, gi0 gi0Var2);

    /* synthetic */ c<?, eo3> shhAddToGroup(String str);

    /* synthetic */ c<?, ho3> shhGetFilterChanges(BigInteger bigInteger);

    /* synthetic */ c<?, ho3> shhGetMessages(BigInteger bigInteger);

    /* synthetic */ c<?, go3> shhHasIdentity(String str);

    /* synthetic */ c<?, io3> shhNewFilter(fo3 fo3Var);

    /* synthetic */ c<?, jo3> shhNewGroup();

    /* synthetic */ c<?, ko3> shhNewIdentity();

    /* synthetic */ c<?, lo3> shhPost(mo3 mo3Var);

    /* synthetic */ c<?, no3> shhUninstallFilter(BigInteger bigInteger);

    /* synthetic */ c<?, oo3> shhVersion();

    void shutdown();

    /* synthetic */ q71<o84> transactionFlowable();

    /* synthetic */ c<?, ho4> web3ClientVersion();

    /* synthetic */ c<?, io4> web3Sha3(String str);
}
