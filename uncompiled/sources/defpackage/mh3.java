package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: mh3  reason: default package */
/* loaded from: classes2.dex */
public class mh3 extends xs0.b {
    public nh3 j;

    /* renamed from: mh3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] b = hd2.b();
            long[] b2 = hd2.b();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                long j = ((i3 ^ i) - 1) >> 31;
                for (int i4 = 0; i4 < 7; i4++) {
                    long j2 = b[i4];
                    long[] jArr = this.b;
                    b[i4] = j2 ^ (jArr[i2 + i4] & j);
                    b2[i4] = b2[i4] ^ (jArr[(i2 + 7) + i4] & j);
                }
                i2 += 14;
            }
            return mh3.this.i(new lh3(b), new lh3(b2), false);
        }
    }

    public mh3() {
        super(409, 87, 0, 0);
        this.j = new nh3(this, null, null);
        this.b = n(BigInteger.valueOf(0L));
        this.c = n(BigInteger.valueOf(1L));
        this.d = new BigInteger(1, pk1.a("7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F83B2D4EA20400EC4557D5ED3E3E7CA5B4B5C83B8E01E5FCF"));
        this.e = BigInteger.valueOf(4L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return true;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new mh3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 7 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            hd2.a(((lh3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 7;
            hd2.a(((lh3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 7;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public it0 f() {
        return new ll4();
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new nh3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new nh3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new lh3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 409;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
