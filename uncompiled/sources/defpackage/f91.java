package defpackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ForwardingImageOriginListener.java */
/* renamed from: f91  reason: default package */
/* loaded from: classes.dex */
public class f91 implements fo1 {
    public final List<fo1> a;

    public f91(fo1... fo1VarArr) {
        ArrayList arrayList = new ArrayList(fo1VarArr.length);
        this.a = arrayList;
        Collections.addAll(arrayList, fo1VarArr);
    }

    @Override // defpackage.fo1
    public synchronized void a(String str, int i, boolean z, String str2) {
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            fo1 fo1Var = this.a.get(i2);
            if (fo1Var != null) {
                try {
                    fo1Var.a(str, i, z, str2);
                } catch (Exception e) {
                    v11.i("ForwardingImageOriginListener", "InternalListener exception in onImageLoaded", e);
                }
            }
        }
    }

    public synchronized void b(fo1 fo1Var) {
        this.a.add(fo1Var);
    }

    public synchronized void c(fo1 fo1Var) {
        this.a.remove(fo1Var);
    }
}
