package defpackage;

import android.animation.IntEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;

/* compiled from: SwapAnimation.java */
/* renamed from: zw3  reason: default package */
/* loaded from: classes2.dex */
public class zw3 extends nm<ValueAnimator> {
    public int d;
    public int e;
    public ax3 f;

    /* compiled from: SwapAnimation.java */
    /* renamed from: zw3$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            zw3.this.j(valueAnimator);
        }
    }

    public zw3(xg4.a aVar) {
        super(aVar);
        this.d = -1;
        this.e = -1;
        this.f = new ax3();
    }

    @Override // defpackage.nm
    /* renamed from: g */
    public ValueAnimator a() {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(350L);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new a());
        return valueAnimator;
    }

    public final PropertyValuesHolder h(String str, int i, int i2) {
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt(str, i, i2);
        ofInt.setEvaluator(new IntEvaluator());
        return ofInt;
    }

    public final boolean i(int i, int i2) {
        return (this.d == i && this.e == i2) ? false : true;
    }

    public final void j(ValueAnimator valueAnimator) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COORDINATE")).intValue();
        int intValue2 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COORDINATE_REVERSE")).intValue();
        this.f.c(intValue);
        this.f.d(intValue2);
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.f);
        }
    }

    @Override // defpackage.nm
    /* renamed from: k */
    public zw3 d(float f) {
        T t = this.c;
        if (t != 0) {
            long j = f * ((float) this.a);
            if (((ValueAnimator) t).getValues() != null && ((ValueAnimator) this.c).getValues().length > 0) {
                ((ValueAnimator) this.c).setCurrentPlayTime(j);
            }
        }
        return this;
    }

    public zw3 l(int i, int i2) {
        if (this.c != 0 && i(i, i2)) {
            this.d = i;
            this.e = i2;
            ((ValueAnimator) this.c).setValues(h("ANIMATION_COORDINATE", i, i2), h("ANIMATION_COORDINATE_REVERSE", i2, i));
        }
        return this;
    }
}
