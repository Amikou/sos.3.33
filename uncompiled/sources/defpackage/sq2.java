package defpackage;

import android.net.Uri;
import com.google.android.gms.common.api.a;

/* renamed from: sq2  reason: default package */
/* loaded from: classes.dex */
public final class sq2 {
    public static final a.g<ze5> a;
    public static final a.AbstractC0106a<ze5, Object> b;

    static {
        a.g<ze5> gVar = new a.g<>();
        a = gVar;
        hx5 hx5Var = new hx5();
        b = hx5Var;
        new a("Phenotype.API", hx5Var, gVar);
        new cd5();
    }

    public static Uri a(String str) {
        String valueOf = String.valueOf(Uri.encode(str));
        return Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
    }
}
