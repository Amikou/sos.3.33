package defpackage;

import android.util.SparseArray;
import androidx.media3.common.ParserException;
import defpackage.gc4;
import defpackage.wi3;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: PsExtractor.java */
/* renamed from: lw2  reason: default package */
/* loaded from: classes.dex */
public final class lw2 implements p11 {
    public final h64 a;
    public final SparseArray<a> b;
    public final op2 c;
    public final jw2 d;
    public boolean e;
    public boolean f;
    public boolean g;
    public long h;
    public iw2 i;
    public r11 j;
    public boolean k;

    /* compiled from: PsExtractor.java */
    /* renamed from: lw2$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final ku0 a;
        public final h64 b;
        public final np2 c = new np2(new byte[64]);
        public boolean d;
        public boolean e;
        public boolean f;
        public int g;
        public long h;

        public a(ku0 ku0Var, h64 h64Var) {
            this.a = ku0Var;
            this.b = h64Var;
        }

        public void a(op2 op2Var) throws ParserException {
            op2Var.j(this.c.a, 0, 3);
            this.c.p(0);
            b();
            op2Var.j(this.c.a, 0, this.g);
            this.c.p(0);
            c();
            this.a.e(this.h, 4);
            this.a.a(op2Var);
            this.a.d();
        }

        public final void b() {
            this.c.r(8);
            this.d = this.c.g();
            this.e = this.c.g();
            this.c.r(6);
            this.g = this.c.h(8);
        }

        public final void c() {
            this.h = 0L;
            if (this.d) {
                this.c.r(4);
                this.c.r(1);
                this.c.r(1);
                long h = (this.c.h(3) << 30) | (this.c.h(15) << 15) | this.c.h(15);
                this.c.r(1);
                if (!this.f && this.e) {
                    this.c.r(4);
                    this.c.r(1);
                    this.c.r(1);
                    this.c.r(1);
                    this.b.b((this.c.h(3) << 30) | (this.c.h(15) << 15) | this.c.h(15));
                    this.f = true;
                }
                this.h = this.b.b(h);
            }
        }

        public void d() {
            this.f = false;
            this.a.c();
        }
    }

    static {
        kw2 kw2Var = kw2.b;
    }

    public lw2() {
        this(new h64(0L));
    }

    public static /* synthetic */ p11[] d() {
        return new p11[]{new lw2()};
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        boolean z = true;
        boolean z2 = this.a.e() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        if (!z2) {
            long c = this.a.c();
            z2 = (c == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || c == 0 || c == j2) ? false : false;
        }
        if (z2) {
            this.a.g(j2);
        }
        iw2 iw2Var = this.i;
        if (iw2Var != null) {
            iw2Var.h(j2);
        }
        for (int i = 0; i < this.b.size(); i++) {
            this.b.valueAt(i).d();
        }
    }

    public final void e(long j) {
        if (this.k) {
            return;
        }
        this.k = true;
        if (this.d.c() != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            iw2 iw2Var = new iw2(this.d.d(), this.d.c(), j);
            this.i = iw2Var;
            this.j.p(iw2Var.b());
            return;
        }
        this.j.p(new wi3.b(this.d.c()));
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        ii.i(this.j);
        long length = q11Var.getLength();
        int i = (length > (-1L) ? 1 : (length == (-1L) ? 0 : -1));
        if ((i != 0) && !this.d.e()) {
            return this.d.g(q11Var, ot2Var);
        }
        e(length);
        iw2 iw2Var = this.i;
        if (iw2Var != null && iw2Var.d()) {
            return this.i.c(q11Var, ot2Var);
        }
        q11Var.j();
        long e = i != 0 ? length - q11Var.e() : -1L;
        if ((e == -1 || e >= 4) && q11Var.d(this.c.d(), 0, 4, true)) {
            this.c.P(0);
            int n = this.c.n();
            if (n == 441) {
                return -1;
            }
            if (n == 442) {
                q11Var.n(this.c.d(), 0, 10);
                this.c.P(9);
                q11Var.k((this.c.D() & 7) + 14);
                return 0;
            } else if (n == 443) {
                q11Var.n(this.c.d(), 0, 2);
                this.c.P(0);
                q11Var.k(this.c.J() + 6);
                return 0;
            } else if (((n & (-256)) >> 8) != 1) {
                q11Var.k(1);
                return 0;
            } else {
                int i2 = n & 255;
                a aVar = this.b.get(i2);
                if (!this.e) {
                    if (aVar == null) {
                        ku0 ku0Var = null;
                        if (i2 == 189) {
                            ku0Var = new s5();
                            this.f = true;
                            this.h = q11Var.getPosition();
                        } else if ((i2 & 224) == 192) {
                            ku0Var = new ka2();
                            this.f = true;
                            this.h = q11Var.getPosition();
                        } else if ((i2 & 240) == 224) {
                            ku0Var = new cj1();
                            this.g = true;
                            this.h = q11Var.getPosition();
                        }
                        if (ku0Var != null) {
                            ku0Var.f(this.j, new gc4.d(i2, 256));
                            aVar = new a(ku0Var, this.a);
                            this.b.put(i2, aVar);
                        }
                    }
                    if (q11Var.getPosition() > ((this.f && this.g) ? this.h + 8192 : 1048576L)) {
                        this.e = true;
                        this.j.m();
                    }
                }
                q11Var.n(this.c.d(), 0, 2);
                this.c.P(0);
                int J = this.c.J() + 6;
                if (aVar == null) {
                    q11Var.k(J);
                } else {
                    this.c.L(J);
                    q11Var.readFully(this.c.d(), 0, J);
                    this.c.P(6);
                    aVar.a(this.c);
                    op2 op2Var = this.c;
                    op2Var.O(op2Var.b());
                }
                return 0;
            }
        }
        return -1;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        byte[] bArr = new byte[14];
        q11Var.n(bArr, 0, 14);
        if (442 == (((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255)) && (bArr[4] & 196) == 68 && (bArr[6] & 4) == 4 && (bArr[8] & 4) == 4 && (bArr[9] & 1) == 1 && (bArr[12] & 3) == 3) {
            q11Var.f(bArr[13] & 7);
            q11Var.n(bArr, 0, 3);
            return 1 == ((((bArr[0] & 255) << 16) | ((bArr[1] & 255) << 8)) | (bArr[2] & 255));
        }
        return false;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.j = r11Var;
    }

    public lw2(h64 h64Var) {
        this.a = h64Var;
        this.c = new op2(4096);
        this.b = new SparseArray<>();
        this.d = new jw2();
    }
}
