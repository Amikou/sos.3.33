package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: p74  reason: default package */
/* loaded from: classes2.dex */
public final class p74 {
    public static WeakReference<p74> d;
    public final SharedPreferences a;
    public ao3 b;
    public final Executor c;

    public p74(SharedPreferences sharedPreferences, Executor executor) {
        this.c = executor;
        this.a = sharedPreferences;
    }

    public static synchronized p74 a(Context context, Executor executor) {
        synchronized (p74.class) {
            WeakReference<p74> weakReference = d;
            p74 p74Var = weakReference != null ? weakReference.get() : null;
            if (p74Var == null) {
                p74 p74Var2 = new p74(context.getSharedPreferences("com.google.android.gms.appid", 0), executor);
                p74Var2.c();
                d = new WeakReference<>(p74Var2);
                return p74Var2;
            }
            return p74Var;
        }
    }

    public synchronized o74 b() {
        return o74.a(this.b.e());
    }

    public final synchronized void c() {
        this.b = ao3.c(this.a, "topic_operation_queue", ",", this.c);
    }

    public synchronized boolean d(o74 o74Var) {
        return this.b.f(o74Var.e());
    }
}
