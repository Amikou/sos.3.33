package defpackage;

import androidx.media3.common.j;
import androidx.media3.common.util.b;
import defpackage.gc4;
import defpackage.t5;
import zendesk.support.request.CellBase;

/* compiled from: Ac3Reader.java */
/* renamed from: s5  reason: default package */
/* loaded from: classes.dex */
public final class s5 implements ku0 {
    public final np2 a;
    public final op2 b;
    public final String c;
    public String d;
    public f84 e;
    public int f;
    public int g;
    public boolean h;
    public long i;
    public j j;
    public int k;
    public long l;

    public s5() {
        this(null);
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        ii.i(this.e);
        while (op2Var.a() > 0) {
            int i = this.f;
            if (i != 0) {
                if (i != 1) {
                    if (i == 2) {
                        int min = Math.min(op2Var.a(), this.k - this.g);
                        this.e.a(op2Var, min);
                        int i2 = this.g + min;
                        this.g = i2;
                        int i3 = this.k;
                        if (i2 == i3) {
                            long j = this.l;
                            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                this.e.b(j, 1, i3, 0, null);
                                this.l += this.i;
                            }
                            this.f = 0;
                        }
                    }
                } else if (b(op2Var, this.b.d(), 128)) {
                    g();
                    this.b.P(0);
                    this.e.a(this.b, 128);
                    this.f = 2;
                }
            } else if (h(op2Var)) {
                this.f = 1;
                this.b.d()[0] = 11;
                this.b.d()[1] = 119;
                this.g = 2;
            }
        }
    }

    public final boolean b(op2 op2Var, byte[] bArr, int i) {
        int min = Math.min(op2Var.a(), i - this.g);
        op2Var.j(bArr, this.g, min);
        int i2 = this.g + min;
        this.g = i2;
        return i2 == i;
    }

    @Override // defpackage.ku0
    public void c() {
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.l = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.d = dVar.b();
        this.e = r11Var.f(dVar.c(), 1);
    }

    public final void g() {
        this.a.p(0);
        t5.b e = t5.e(this.a);
        j jVar = this.j;
        if (jVar == null || e.c != jVar.C0 || e.b != jVar.D0 || !b.c(e.a, jVar.p0)) {
            j E = new j.b().S(this.d).e0(e.a).H(e.c).f0(e.b).V(this.c).E();
            this.j = E;
            this.e.f(E);
        }
        this.k = e.d;
        this.i = (e.e * 1000000) / this.j.D0;
    }

    public final boolean h(op2 op2Var) {
        while (true) {
            if (op2Var.a() <= 0) {
                return false;
            }
            if (!this.h) {
                this.h = op2Var.D() == 11;
            } else {
                int D = op2Var.D();
                if (D == 119) {
                    this.h = false;
                    return true;
                }
                this.h = D == 11;
            }
        }
    }

    public s5(String str) {
        np2 np2Var = new np2(new byte[128]);
        this.a = np2Var;
        this.b = new op2(np2Var.a);
        this.f = 0;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.c = str;
    }
}
