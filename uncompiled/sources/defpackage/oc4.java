package defpackage;

/* compiled from: Tuple2.java */
/* renamed from: oc4  reason: default package */
/* loaded from: classes3.dex */
public final class oc4<T1, T2> {
    private static final int SIZE = 2;
    private final T1 value1;
    private final T2 value2;

    public oc4(T1 t1, T2 t2) {
        this.value1 = t1;
        this.value2 = t2;
    }

    public T1 component1() {
        return this.value1;
    }

    public T2 component2() {
        return this.value2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || oc4.class != obj.getClass()) {
            return false;
        }
        oc4 oc4Var = (oc4) obj;
        T1 t1 = this.value1;
        if (t1 == null ? oc4Var.value1 == null : t1.equals(oc4Var.value1)) {
            T2 t2 = this.value2;
            T2 t22 = oc4Var.value2;
            return t2 != null ? t2.equals(t22) : t22 == null;
        }
        return false;
    }

    public int getSize() {
        return 2;
    }

    @Deprecated
    public T1 getValue1() {
        return this.value1;
    }

    @Deprecated
    public T2 getValue2() {
        return this.value2;
    }

    public int hashCode() {
        int hashCode = this.value1.hashCode() * 31;
        T2 t2 = this.value2;
        return hashCode + (t2 != null ? t2.hashCode() : 0);
    }

    public String toString() {
        return "Tuple2{value1=" + this.value1 + ", value2=" + this.value2 + "}";
    }
}
