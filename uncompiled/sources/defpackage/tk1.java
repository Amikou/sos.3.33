package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: HolderChooseNetworkBinding.java */
/* renamed from: tk1  reason: default package */
/* loaded from: classes2.dex */
public final class tk1 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final ImageView c;
    public final TextView d;

    public tk1(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, ConstraintLayout constraintLayout2, ImageView imageView, TextView textView) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = imageView;
        this.d = textView;
    }

    public static tk1 a(View view) {
        int i = R.id.cbSelectFiat;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbSelectFiat);
        if (materialCheckBox != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.ivChainIcon;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivChainIcon);
            if (imageView != null) {
                i = R.id.tvChainName;
                TextView textView = (TextView) ai4.a(view, R.id.tvChainName);
                if (textView != null) {
                    return new tk1(constraintLayout, materialCheckBox, constraintLayout, imageView, textView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
