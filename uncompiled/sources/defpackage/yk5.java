package defpackage;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: yk5  reason: default package */
/* loaded from: classes.dex */
public final class yk5 {
    public long A;
    public long B;
    public String C;
    public boolean D;
    public long E;
    public long F;
    public final ck5 a;
    public final String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public long g;
    public long h;
    public long i;
    public String j;
    public long k;
    public String l;
    public long m;
    public long n;
    public boolean o;
    public long p;
    public boolean q;
    public String r;
    public Boolean s;
    public long t;
    public List<String> u;
    public String v;
    public long w;
    public long x;
    public long y;
    public long z;

    public yk5(ck5 ck5Var, String str) {
        zt2.j(ck5Var);
        zt2.f(str);
        this.a = ck5Var;
        this.b = str;
        ck5Var.q().e();
    }

    public final boolean A() {
        this.a.q().e();
        return this.D;
    }

    public final String B() {
        this.a.q().e();
        return this.C;
    }

    public final String C() {
        this.a.q().e();
        String str = this.C;
        D(null);
        return str;
    }

    public final void D(String str) {
        this.a.q().e();
        this.D |= !sw5.G(this.C, str);
        this.C = str;
    }

    public final long E() {
        this.a.q().e();
        return this.p;
    }

    public final void F(long j) {
        this.a.q().e();
        this.D |= this.p != j;
        this.p = j;
    }

    public final boolean G() {
        this.a.q().e();
        return this.q;
    }

    public final void H(boolean z) {
        this.a.q().e();
        this.D |= this.q != z;
        this.q = z;
    }

    public final Boolean I() {
        this.a.q().e();
        return this.s;
    }

    public final void J(Boolean bool) {
        boolean equals;
        this.a.q().e();
        boolean z = this.D;
        Boolean bool2 = this.s;
        int i = sw5.i;
        if (bool2 == null && bool == null) {
            equals = true;
        } else {
            equals = bool2 == null ? false : bool2.equals(bool);
        }
        this.D = z | (!equals);
        this.s = bool;
    }

    public final List<String> K() {
        this.a.q().e();
        return this.u;
    }

    public final void L(List<String> list) {
        this.a.q().e();
        List<String> list2 = this.u;
        int i = sw5.i;
        if (list2 == null && list == null) {
            return;
        }
        if (list2 != null && list2.equals(list)) {
            return;
        }
        this.D = true;
        this.u = list != null ? new ArrayList(list) : null;
    }

    public final void M() {
        this.a.q().e();
        this.D = false;
    }

    public final String N() {
        this.a.q().e();
        return this.b;
    }

    public final String O() {
        this.a.q().e();
        return this.c;
    }

    public final void P(String str) {
        this.a.q().e();
        this.D |= !sw5.G(this.c, str);
        this.c = str;
    }

    public final String Q() {
        this.a.q().e();
        return this.d;
    }

    public final void R(String str) {
        this.a.q().e();
        if (true == TextUtils.isEmpty(str)) {
            str = null;
        }
        this.D |= true ^ sw5.G(this.d, str);
        this.d = str;
    }

    public final String S() {
        this.a.q().e();
        return this.r;
    }

    public final void T(String str) {
        this.a.q().e();
        if (true == TextUtils.isEmpty(str)) {
            str = null;
        }
        this.D |= true ^ sw5.G(this.r, str);
        this.r = str;
    }

    public final String U() {
        this.a.q().e();
        return this.v;
    }

    public final void V(String str) {
        this.a.q().e();
        if (true == TextUtils.isEmpty(str)) {
            str = null;
        }
        this.D |= true ^ sw5.G(this.v, str);
        this.v = str;
    }

    public final String W() {
        this.a.q().e();
        return this.e;
    }

    public final void X(String str) {
        this.a.q().e();
        this.D |= !sw5.G(this.e, str);
        this.e = str;
    }

    public final String Y() {
        this.a.q().e();
        return this.f;
    }

    public final void Z(String str) {
        this.a.q().e();
        this.D |= !sw5.G(this.f, str);
        this.f = str;
    }

    public final void a(long j) {
        this.a.q().e();
        this.D |= this.m != j;
        this.m = j;
    }

    public final long a0() {
        this.a.q().e();
        return this.h;
    }

    public final long b() {
        this.a.q().e();
        return this.n;
    }

    public final void b0(long j) {
        this.a.q().e();
        this.D |= this.h != j;
        this.h = j;
    }

    public final void c(long j) {
        this.a.q().e();
        this.D |= this.n != j;
        this.n = j;
    }

    public final long c0() {
        this.a.q().e();
        return this.i;
    }

    public final long d() {
        this.a.q().e();
        return this.t;
    }

    public final void d0(long j) {
        this.a.q().e();
        this.D |= this.i != j;
        this.i = j;
    }

    public final void e(long j) {
        this.a.q().e();
        this.D |= this.t != j;
        this.t = j;
    }

    public final String e0() {
        this.a.q().e();
        return this.j;
    }

    public final boolean f() {
        this.a.q().e();
        return this.o;
    }

    public final void f0(String str) {
        this.a.q().e();
        this.D |= !sw5.G(this.j, str);
        this.j = str;
    }

    public final void g(boolean z) {
        this.a.q().e();
        this.D |= this.o != z;
        this.o = z;
    }

    public final long g0() {
        this.a.q().e();
        return this.k;
    }

    public final void h(long j) {
        zt2.a(j >= 0);
        this.a.q().e();
        this.D |= this.g != j;
        this.g = j;
    }

    public final void h0(long j) {
        this.a.q().e();
        this.D |= this.k != j;
        this.k = j;
    }

    public final long i() {
        this.a.q().e();
        return this.g;
    }

    public final String i0() {
        this.a.q().e();
        return this.l;
    }

    public final long j() {
        this.a.q().e();
        return this.E;
    }

    public final void j0(String str) {
        this.a.q().e();
        this.D |= !sw5.G(this.l, str);
        this.l = str;
    }

    public final void k(long j) {
        this.a.q().e();
        this.D |= this.E != j;
        this.E = j;
    }

    public final long k0() {
        this.a.q().e();
        return this.m;
    }

    public final long l() {
        this.a.q().e();
        return this.F;
    }

    public final void m(long j) {
        this.a.q().e();
        this.D |= this.F != j;
        this.F = j;
    }

    public final void n() {
        this.a.q().e();
        long j = this.g + 1;
        if (j > 2147483647L) {
            this.a.w().p().b("Bundle index overflow. appId", og5.x(this.b));
            j = 0;
        }
        this.D = true;
        this.g = j;
    }

    public final long o() {
        this.a.q().e();
        return this.w;
    }

    public final void p(long j) {
        this.a.q().e();
        this.D |= this.w != j;
        this.w = j;
    }

    public final long q() {
        this.a.q().e();
        return this.x;
    }

    public final void r(long j) {
        this.a.q().e();
        this.D |= this.x != j;
        this.x = j;
    }

    public final long s() {
        this.a.q().e();
        return this.y;
    }

    public final void t(long j) {
        this.a.q().e();
        this.D |= this.y != j;
        this.y = j;
    }

    public final long u() {
        this.a.q().e();
        return this.z;
    }

    public final void v(long j) {
        this.a.q().e();
        this.D |= this.z != j;
        this.z = j;
    }

    public final long w() {
        this.a.q().e();
        return this.B;
    }

    public final void x(long j) {
        this.a.q().e();
        this.D |= this.B != j;
        this.B = j;
    }

    public final long y() {
        this.a.q().e();
        return this.A;
    }

    public final void z(long j) {
        this.a.q().e();
        this.D |= this.A != j;
        this.A = j;
    }
}
