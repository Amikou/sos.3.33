package defpackage;

import java.math.BigInteger;

/* renamed from: ws0  reason: default package */
/* loaded from: classes2.dex */
public interface ws0 {
    public static final BigInteger a = BigInteger.valueOf(0);
    public static final BigInteger b = BigInteger.valueOf(1);
    public static final BigInteger c = BigInteger.valueOf(2);
    public static final BigInteger d = BigInteger.valueOf(3);
    public static final BigInteger e = BigInteger.valueOf(4);

    static {
        BigInteger.valueOf(8L);
    }
}
