package defpackage;

import defpackage.r90;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Device.java */
/* renamed from: dl  reason: default package */
/* loaded from: classes2.dex */
public final class dl extends r90.e.d.c {
    public final Double a;
    public final int b;
    public final boolean c;
    public final int d;
    public final long e;
    public final long f;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Device.java */
    /* renamed from: dl$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.c.a {
        public Double a;
        public Integer b;
        public Boolean c;
        public Integer d;
        public Long e;
        public Long f;

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c a() {
            String str = "";
            if (this.b == null) {
                str = " batteryVelocity";
            }
            if (this.c == null) {
                str = str + " proximityOn";
            }
            if (this.d == null) {
                str = str + " orientation";
            }
            if (this.e == null) {
                str = str + " ramUsed";
            }
            if (this.f == null) {
                str = str + " diskUsed";
            }
            if (str.isEmpty()) {
                return new dl(this.a, this.b.intValue(), this.c.booleanValue(), this.d.intValue(), this.e.longValue(), this.f.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c.a b(Double d) {
            this.a = d;
            return this;
        }

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c.a c(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c.a d(long j) {
            this.f = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c.a e(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c.a f(boolean z) {
            this.c = Boolean.valueOf(z);
            return this;
        }

        @Override // defpackage.r90.e.d.c.a
        public r90.e.d.c.a g(long j) {
            this.e = Long.valueOf(j);
            return this;
        }
    }

    @Override // defpackage.r90.e.d.c
    public Double b() {
        return this.a;
    }

    @Override // defpackage.r90.e.d.c
    public int c() {
        return this.b;
    }

    @Override // defpackage.r90.e.d.c
    public long d() {
        return this.f;
    }

    @Override // defpackage.r90.e.d.c
    public int e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.c) {
            r90.e.d.c cVar = (r90.e.d.c) obj;
            Double d = this.a;
            if (d != null ? d.equals(cVar.b()) : cVar.b() == null) {
                if (this.b == cVar.c() && this.c == cVar.g() && this.d == cVar.e() && this.e == cVar.f() && this.f == cVar.d()) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override // defpackage.r90.e.d.c
    public long f() {
        return this.e;
    }

    @Override // defpackage.r90.e.d.c
    public boolean g() {
        return this.c;
    }

    public int hashCode() {
        Double d = this.a;
        int hashCode = ((((d == null ? 0 : d.hashCode()) ^ 1000003) * 1000003) ^ this.b) * 1000003;
        int i = this.c ? 1231 : 1237;
        long j = this.e;
        long j2 = this.f;
        return ((((((hashCode ^ i) * 1000003) ^ this.d) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        return "Device{batteryLevel=" + this.a + ", batteryVelocity=" + this.b + ", proximityOn=" + this.c + ", orientation=" + this.d + ", ramUsed=" + this.e + ", diskUsed=" + this.f + "}";
    }

    public dl(Double d, int i, boolean z, int i2, long j, long j2) {
        this.a = d;
        this.b = i;
        this.c = z;
        this.d = i2;
        this.e = j;
        this.f = j2;
    }
}
