package defpackage;

import java.util.concurrent.Executor;

/* compiled from: SafeLoggingExecutor.java */
/* renamed from: xb3  reason: default package */
/* loaded from: classes.dex */
public class xb3 implements Executor {
    public final Executor a;

    /* compiled from: SafeLoggingExecutor.java */
    /* renamed from: xb3$a */
    /* loaded from: classes.dex */
    public static class a implements Runnable {
        public final Runnable a;

        public a(Runnable runnable) {
            this.a = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                this.a.run();
            } catch (Exception e) {
                z12.c("Executor", "Background execution failure.", e);
            }
        }
    }

    public xb3(Executor executor) {
        this.a = executor;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.a.execute(new a(runnable));
    }
}
