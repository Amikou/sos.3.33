package defpackage;

/* renamed from: aq3  reason: default package */
/* loaded from: classes.dex */
public final class aq3 {
    public static aq3 b;
    public final v61 a = new v61();

    static {
        aq3 aq3Var = new aq3();
        synchronized (aq3.class) {
            b = aq3Var;
        }
    }

    public aq3() {
        new r75();
    }

    public static v61 a() {
        return b().a;
    }

    public static aq3 b() {
        aq3 aq3Var;
        synchronized (aq3.class) {
            aq3Var = b;
        }
        return aq3Var;
    }
}
