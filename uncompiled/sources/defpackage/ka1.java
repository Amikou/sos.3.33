package defpackage;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

/* compiled from: Fragment.kt */
/* renamed from: ka1  reason: default package */
/* loaded from: classes.dex */
public final class ka1 {
    public static final NavController a(Fragment fragment) {
        fs1.g(fragment, "$this$findNavController");
        NavController f = NavHostFragment.f(fragment);
        fs1.c(f, "NavHostFragment.findNavController(this)");
        return f;
    }
}
