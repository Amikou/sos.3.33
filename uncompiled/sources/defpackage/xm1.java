package defpackage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;

/* compiled from: IUnusedAppRestrictionsBackportCallback.java */
/* renamed from: xm1  reason: default package */
/* loaded from: classes.dex */
public interface xm1 extends IInterface {

    /* compiled from: IUnusedAppRestrictionsBackportCallback.java */
    /* renamed from: xm1$a */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements xm1 {

        /* compiled from: IUnusedAppRestrictionsBackportCallback.java */
        /* renamed from: xm1$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0290a implements xm1 {
            public IBinder a;

            public C0290a(IBinder iBinder) {
                this.a = iBinder;
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.a;
            }
        }

        public static xm1 b(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportCallback");
            if (queryLocalInterface != null && (queryLocalInterface instanceof xm1)) {
                return (xm1) queryLocalInterface;
            }
            return new C0290a(iBinder);
        }
    }
}
