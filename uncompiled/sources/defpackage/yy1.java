package defpackage;

import kotlin.LazyThreadSafetyMode;

/* renamed from: yy1  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class yy1 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[LazyThreadSafetyMode.values().length];
        a = iArr;
        iArr[LazyThreadSafetyMode.SYNCHRONIZED.ordinal()] = 1;
        iArr[LazyThreadSafetyMode.PUBLICATION.ordinal()] = 2;
        iArr[LazyThreadSafetyMode.NONE.ordinal()] = 3;
    }
}
