package defpackage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.internal.measurement.b1;
import com.google.android.gms.internal.measurement.f1;
import com.google.android.gms.measurement.internal.r;
import com.google.android.gms.measurement.internal.zzaa;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Marker;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: e55  reason: default package */
/* loaded from: classes.dex */
public final class e55 extends jv5 {
    public static final String[] f = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    public static final String[] g = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    public static final String[] h = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;", "ga_app_id", "ALTER TABLE apps ADD COLUMN ga_app_id TEXT;", "config_last_modified_time", "ALTER TABLE apps ADD COLUMN config_last_modified_time TEXT;"};
    public static final String[] i = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    public static final String[] j = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    public static final String[] k = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};
    public static final String[] l = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};
    public static final String[] m = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    public final a55 d;
    public final bv5 e;

    public e55(fw5 fw5Var) {
        super(fw5Var);
        this.e = new bv5(this.a.a());
        this.a.z();
        this.d = new a55(this, this.a.m(), "google_app_measurement.db");
    }

    public static final void J(ContentValues contentValues, String str, Object obj) {
        zt2.f("value");
        zt2.j(obj);
        if (obj instanceof String) {
            contentValues.put("value", (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put("value", (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put("value", (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Not initialized variable reg: 4, insn: 0x0233: MOVE  (r3 I:??[OBJECT, ARRAY]) = (r4 I:??[OBJECT, ARRAY]), block:B:98:0x0233 */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v12 */
    /* JADX WARN: Type inference failed for: r4v15 */
    /* JADX WARN: Type inference failed for: r4v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r4v3, types: [boolean] */
    public final void I(String str, long j2, long j3, dw5 dw5Var) {
        ?? r4;
        Cursor cursor;
        String str2;
        Cursor rawQuery;
        String string;
        int i2;
        String str3;
        String[] strArr;
        zt2.j(dw5Var);
        e();
        g();
        Cursor cursor2 = null;
        r3 = null;
        r3 = null;
        String str4 = null;
        try {
            try {
                SQLiteDatabase P = P();
                r4 = TextUtils.isEmpty(null);
                try {
                    if (r4 != 0) {
                        int i3 = (j3 > (-1L) ? 1 : (j3 == (-1L) ? 0 : -1));
                        String[] strArr2 = i3 != 0 ? new String[]{String.valueOf(j3), String.valueOf(j2)} : new String[]{String.valueOf(j2)};
                        str2 = i3 != 0 ? "rowid <= ? and " : "";
                        StringBuilder sb = new StringBuilder(str2.length() + 148);
                        sb.append("select app_id, metadata_fingerprint from raw_events where ");
                        sb.append(str2);
                        sb.append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;");
                        rawQuery = P.rawQuery(sb.toString(), strArr2);
                        if (!rawQuery.moveToFirst()) {
                            rawQuery.close();
                            return;
                        }
                        str4 = rawQuery.getString(0);
                        string = rawQuery.getString(1);
                        rawQuery.close();
                    } else {
                        int i4 = (j3 > (-1L) ? 1 : (j3 == (-1L) ? 0 : -1));
                        String[] strArr3 = i4 != 0 ? new String[]{null, String.valueOf(j3)} : new String[]{null};
                        str2 = i4 != 0 ? " and rowid <= ?" : "";
                        StringBuilder sb2 = new StringBuilder(str2.length() + 84);
                        sb2.append("select metadata_fingerprint from raw_events where app_id = ?");
                        sb2.append(str2);
                        sb2.append(" order by rowid limit 1;");
                        rawQuery = P.rawQuery(sb2.toString(), strArr3);
                        if (!rawQuery.moveToFirst()) {
                            rawQuery.close();
                            return;
                        } else {
                            string = rawQuery.getString(0);
                            rawQuery.close();
                        }
                    }
                    Cursor cursor3 = rawQuery;
                    String str5 = string;
                    try {
                        Cursor query = P.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str4, str5}, null, null, "rowid", "2");
                        try {
                            if (!query.moveToFirst()) {
                                this.a.w().l().b("Raw event metadata record is missing. appId", og5.x(str4));
                                query.close();
                                return;
                            }
                            try {
                                try {
                                    f1 o = ((dk5) r.J(f1.K0(), query.getBlob(0))).o();
                                    if (query.moveToNext()) {
                                        this.a.w().p().b("Get multiple raw event metadata records, expected one. appId", og5.x(str4));
                                    }
                                    query.close();
                                    zt2.j(o);
                                    dw5Var.a = o;
                                    if (j3 != -1) {
                                        i2 = 1;
                                        str3 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                                        strArr = new String[]{str4, str5, String.valueOf(j3)};
                                    } else {
                                        i2 = 1;
                                        str3 = "app_id = ? and metadata_fingerprint = ?";
                                        strArr = new String[]{str4, str5};
                                    }
                                    Cursor query2 = P.query("raw_events", new String[]{"rowid", PublicResolver.FUNC_NAME, "timestamp", "data"}, str3, strArr, null, null, "rowid", null);
                                    if (query2.moveToFirst()) {
                                        do {
                                            long j4 = query2.getLong(0);
                                            try {
                                                tj5 tj5Var = (tj5) r.J(b1.H(), query2.getBlob(3));
                                                tj5Var.J(query2.getString(i2));
                                                tj5Var.M(query2.getLong(2));
                                                if (!dw5Var.a(j4, tj5Var.o())) {
                                                    query2.close();
                                                    return;
                                                }
                                            } catch (IOException e) {
                                                this.a.w().l().c("Data loss. Failed to merge raw event. appId", og5.x(str4), e);
                                            }
                                        } while (query2.moveToNext());
                                        query2.close();
                                        return;
                                    }
                                    this.a.w().p().b("Raw event data disappeared while in transaction. appId", og5.x(str4));
                                    query2.close();
                                } catch (IOException e2) {
                                    this.a.w().l().c("Data loss. Failed to merge raw event metadata. appId", og5.x(str4), e2);
                                    query.close();
                                }
                            } catch (SQLiteException e3) {
                                e = e3;
                                r4 = str5;
                                this.a.w().l().c("Data loss. Error selecting raw event. appId", og5.x(str4), e);
                                if (r4 != 0) {
                                    r4.close();
                                }
                            } catch (Throwable th) {
                                th = th;
                                cursor2 = str5;
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                throw th;
                            }
                        } catch (SQLiteException e4) {
                            e = e4;
                            str5 = query;
                        } catch (Throwable th2) {
                            th = th2;
                            str5 = query;
                        }
                    } catch (SQLiteException e5) {
                        e = e5;
                        r4 = cursor3;
                    } catch (Throwable th3) {
                        th = th3;
                        cursor2 = cursor3;
                    }
                } catch (SQLiteException e6) {
                    e = e6;
                }
            } catch (SQLiteException e7) {
                e = e7;
                r4 = 0;
            } catch (Throwable th4) {
                th = th4;
            }
        } catch (Throwable th5) {
            th = th5;
            cursor2 = cursor;
        }
    }

    public final long K(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            try {
                Cursor rawQuery = P().rawQuery(str, strArr);
                if (rawQuery.moveToFirst()) {
                    long j2 = rawQuery.getLong(0);
                    rawQuery.close();
                    return j2;
                }
                throw new SQLiteException("Database returned empty set");
            } catch (SQLiteException e) {
                this.a.w().l().c("Database error", str, e);
                throw e;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    public final long L(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            try {
                cursor = P().rawQuery(str, strArr);
                if (!cursor.moveToFirst()) {
                    cursor.close();
                    return j2;
                }
                long j3 = cursor.getLong(0);
                cursor.close();
                return j3;
            } catch (SQLiteException e) {
                this.a.w().l().c("Database error", str, e);
                throw e;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final void M() {
        g();
        P().beginTransaction();
    }

    public final void N() {
        g();
        P().setTransactionSuccessful();
    }

    public final void O() {
        g();
        P().endTransaction();
    }

    public final SQLiteDatabase P() {
        e();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e) {
            this.a.w().p().b("Error opening database", e);
            throw e;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:63:0x0150  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.v55 Q(java.lang.String r28, java.lang.String r29) {
        /*
            Method dump skipped, instructions count: 340
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.Q(java.lang.String, java.lang.String):v55");
    }

    public final void R(v55 v55Var) {
        zt2.j(v55Var);
        e();
        g();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", v55Var.a);
        contentValues.put(PublicResolver.FUNC_NAME, v55Var.b);
        contentValues.put("lifetime_count", Long.valueOf(v55Var.c));
        contentValues.put("current_bundle_count", Long.valueOf(v55Var.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(v55Var.f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(v55Var.g));
        contentValues.put("last_bundled_day", v55Var.h);
        contentValues.put("last_sampled_complex_event_id", v55Var.i);
        contentValues.put("last_sampling_rate", v55Var.j);
        contentValues.put("current_session_count", Long.valueOf(v55Var.e));
        Boolean bool = v55Var.k;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (P().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                this.a.w().l().b("Failed to insert/update event aggregates (got -1). appId", og5.x(v55Var.a));
            }
        } catch (SQLiteException e) {
            this.a.w().l().c("Error storing event aggregates. appId", og5.x(v55Var.a), e);
        }
    }

    public final void S(String str, String str2) {
        zt2.f(str);
        zt2.f(str2);
        e();
        g();
        try {
            P().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e) {
            this.a.w().l().d("Error deleting user property. appId", og5.x(str), this.a.H().p(str2), e);
        }
    }

    public final boolean T(mw5 mw5Var) {
        zt2.j(mw5Var);
        e();
        g();
        if (U(mw5Var.a, mw5Var.c) == null) {
            if (sw5.j0(mw5Var.c)) {
                if (K("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{mw5Var.a}) >= this.a.z().t(mw5Var.a, qf5.G, 25, 100)) {
                    return false;
                }
            } else if (!"_npa".equals(mw5Var.c)) {
                long K = K("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{mw5Var.a, mw5Var.b});
                this.a.z();
                if (K >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", mw5Var.a);
        contentValues.put("origin", mw5Var.b);
        contentValues.put(PublicResolver.FUNC_NAME, mw5Var.c);
        contentValues.put("set_timestamp", Long.valueOf(mw5Var.d));
        J(contentValues, "value", mw5Var.e);
        try {
            if (P().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                this.a.w().l().b("Failed to insert/update user property (got -1). appId", og5.x(mw5Var.a));
            }
        } catch (SQLiteException e) {
            this.a.w().l().c("Error storing user property. appId", og5.x(mw5Var.a), e);
        }
        return true;
    }

    /* JADX WARN: Not initialized variable reg: 11, insn: 0x00a3: MOVE  (r10 I:??[OBJECT, ARRAY]) = (r11 I:??[OBJECT, ARRAY]), block:B:28:0x00a3 */
    /* JADX WARN: Removed duplicated region for block: B:30:0x00a6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.mw5 U(java.lang.String r20, java.lang.String r21) {
        /*
            r19 = this;
            r1 = r19
            r9 = r21
            defpackage.zt2.f(r20)
            defpackage.zt2.f(r21)
            r19.e()
            r19.g()
            r10 = 0
            android.database.sqlite.SQLiteDatabase r11 = r19.P()     // Catch: java.lang.Throwable -> L7b android.database.sqlite.SQLiteException -> L7d
            java.lang.String r0 = "set_timestamp"
            java.lang.String r2 = "value"
            java.lang.String r3 = "origin"
            java.lang.String[] r13 = new java.lang.String[]{r0, r2, r3}     // Catch: java.lang.Throwable -> L7b android.database.sqlite.SQLiteException -> L7d
            r0 = 2
            java.lang.String[] r15 = new java.lang.String[r0]     // Catch: java.lang.Throwable -> L7b android.database.sqlite.SQLiteException -> L7d
            r2 = 0
            r15[r2] = r20     // Catch: java.lang.Throwable -> L7b android.database.sqlite.SQLiteException -> L7d
            r3 = 1
            r15[r3] = r9     // Catch: java.lang.Throwable -> L7b android.database.sqlite.SQLiteException -> L7d
            java.lang.String r12 = "user_attributes"
            java.lang.String r14 = "app_id=? and name=?"
            r16 = 0
            r17 = 0
            r18 = 0
            android.database.Cursor r11 = r11.query(r12, r13, r14, r15, r16, r17, r18)     // Catch: java.lang.Throwable -> L7b android.database.sqlite.SQLiteException -> L7d
            boolean r4 = r11.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            if (r4 != 0) goto L40
            r11.close()
            return r10
        L40:
            long r6 = r11.getLong(r2)     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            java.lang.Object r8 = r1.l(r11, r3)     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            if (r8 != 0) goto L4e
            r11.close()
            return r10
        L4e:
            java.lang.String r4 = r11.getString(r0)     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            mw5 r0 = new mw5     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            r2 = r0
            r3 = r20
            r5 = r21
            r2.<init>(r3, r4, r5, r6, r8)     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            boolean r2 = r11.moveToNext()     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            if (r2 == 0) goto L75
            ck5 r2 = r1.a     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            og5 r2 = r2.w()     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            jg5 r2 = r2.l()     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            java.lang.String r3 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r4 = defpackage.og5.x(r20)     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
            r2.b(r3, r4)     // Catch: android.database.sqlite.SQLiteException -> L79 java.lang.Throwable -> La2
        L75:
            r11.close()
            return r0
        L79:
            r0 = move-exception
            goto L7f
        L7b:
            r0 = move-exception
            goto La4
        L7d:
            r0 = move-exception
            r11 = r10
        L7f:
            ck5 r2 = r1.a     // Catch: java.lang.Throwable -> La2
            og5 r2 = r2.w()     // Catch: java.lang.Throwable -> La2
            jg5 r2 = r2.l()     // Catch: java.lang.Throwable -> La2
            java.lang.String r3 = "Error querying user property. appId"
            java.lang.Object r4 = defpackage.og5.x(r20)     // Catch: java.lang.Throwable -> La2
            ck5 r5 = r1.a     // Catch: java.lang.Throwable -> La2
            cg5 r5 = r5.H()     // Catch: java.lang.Throwable -> La2
            java.lang.String r5 = r5.p(r9)     // Catch: java.lang.Throwable -> La2
            r2.d(r3, r4, r5, r0)     // Catch: java.lang.Throwable -> La2
            if (r11 == 0) goto La1
            r11.close()
        La1:
            return r10
        La2:
            r0 = move-exception
            r10 = r11
        La4:
            if (r10 == 0) goto La9
            r10.close()
        La9:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.U(java.lang.String, java.lang.String):mw5");
    }

    public final List<mw5> V(String str) {
        zt2.f(str);
        e();
        g();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            try {
                this.a.z();
                cursor = P().query("user_attributes", new String[]{PublicResolver.FUNC_NAME, "origin", "set_timestamp", "value"}, "app_id=?", new String[]{str}, null, null, "rowid", "1000");
                if (!cursor.moveToFirst()) {
                    cursor.close();
                    return arrayList;
                }
                do {
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    if (string2 == null) {
                        string2 = "";
                    }
                    String str2 = string2;
                    long j2 = cursor.getLong(2);
                    Object l2 = l(cursor, 3);
                    if (l2 == null) {
                        this.a.w().l().b("Read invalid user property value, ignoring it. appId", og5.x(str));
                    } else {
                        arrayList.add(new mw5(str, str2, string, j2, l2));
                    }
                } while (cursor.moveToNext());
                cursor.close();
                return arrayList;
            } catch (SQLiteException e) {
                this.a.w().l().c("Error querying user properties. appId", og5.x(str), e);
                List<mw5> emptyList = Collections.emptyList();
                if (cursor != null) {
                    cursor.close();
                }
                return emptyList;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:19:0x009c, code lost:
        r2 = r16.a.w().l();
        r16.a.z();
        r2.b("Read more than the max allowed user properties, ignoring excess", 1000);
     */
    /* JADX WARN: Removed duplicated region for block: B:40:0x011d A[DONT_GENERATE] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.List<defpackage.mw5> W(java.lang.String r17, java.lang.String r18, java.lang.String r19) {
        /*
            Method dump skipped, instructions count: 295
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.W(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    public final boolean X(zzaa zzaaVar) {
        zt2.j(zzaaVar);
        e();
        g();
        String str = zzaaVar.a;
        zt2.j(str);
        if (U(str, zzaaVar.g0.f0) == null) {
            long K = K("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{str});
            this.a.z();
            if (K >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("origin", zzaaVar.f0);
        contentValues.put(PublicResolver.FUNC_NAME, zzaaVar.g0.f0);
        J(contentValues, "value", zt2.j(zzaaVar.g0.I1()));
        contentValues.put("active", Boolean.valueOf(zzaaVar.i0));
        contentValues.put("trigger_event_name", zzaaVar.j0);
        contentValues.put("trigger_timeout", Long.valueOf(zzaaVar.l0));
        contentValues.put("timed_out_event", this.a.G().L(zzaaVar.k0));
        contentValues.put("creation_timestamp", Long.valueOf(zzaaVar.h0));
        contentValues.put("triggered_event", this.a.G().L(zzaaVar.m0));
        contentValues.put("triggered_timestamp", Long.valueOf(zzaaVar.g0.g0));
        contentValues.put("time_to_live", Long.valueOf(zzaaVar.n0));
        contentValues.put("expired_event", this.a.G().L(zzaaVar.o0));
        try {
            if (P().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                this.a.w().l().b("Failed to insert/update conditional user property (got -1)", og5.x(str));
            }
        } catch (SQLiteException e) {
            this.a.w().l().c("Error storing conditional user property", og5.x(str), e);
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:30:0x0120  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.google.android.gms.measurement.internal.zzaa Y(java.lang.String r31, java.lang.String r32) {
        /*
            Method dump skipped, instructions count: 292
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.Y(java.lang.String, java.lang.String):com.google.android.gms.measurement.internal.zzaa");
    }

    public final int Z(String str, String str2) {
        zt2.f(str);
        zt2.f(str2);
        e();
        g();
        try {
            return P().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e) {
            this.a.w().l().d("Error deleting conditional property", og5.x(str), this.a.H().p(str2), e);
            return 0;
        }
    }

    public final List<zzaa> a0(String str, String str2, String str3) {
        zt2.f(str);
        e();
        g();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat(Marker.ANY_MARKER));
            sb.append(" and name glob ?");
        }
        return b0(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x0058, code lost:
        r2 = r27.a.w().l();
        r27.a.z();
        r2.b("Read more than the max allowed conditional properties, ignoring extra", 1000);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.List<com.google.android.gms.measurement.internal.zzaa> b0(java.lang.String r28, java.lang.String[] r29) {
        /*
            Method dump skipped, instructions count: 299
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.b0(java.lang.String, java.lang.String[]):java.util.List");
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0117  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x011b A[Catch: SQLiteException -> 0x01e8, all -> 0x0207, TryCatch #0 {SQLiteException -> 0x01e8, blocks: (B:4:0x005f, B:8:0x0069, B:10:0x00cc, B:15:0x00d6, B:19:0x0120, B:21:0x0157, B:25:0x0165, B:24:0x0161, B:26:0x0168, B:28:0x0170, B:32:0x0178, B:36:0x0191, B:38:0x019c, B:39:0x01ae, B:41:0x01bf, B:42:0x01c8, B:44:0x01d1, B:35:0x018d, B:18:0x011b), top: B:62:0x005f }] */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0157 A[Catch: SQLiteException -> 0x01e8, all -> 0x0207, TryCatch #0 {SQLiteException -> 0x01e8, blocks: (B:4:0x005f, B:8:0x0069, B:10:0x00cc, B:15:0x00d6, B:19:0x0120, B:21:0x0157, B:25:0x0165, B:24:0x0161, B:26:0x0168, B:28:0x0170, B:32:0x0178, B:36:0x0191, B:38:0x019c, B:39:0x01ae, B:41:0x01bf, B:42:0x01c8, B:44:0x01d1, B:35:0x018d, B:18:0x011b), top: B:62:0x005f }] */
    /* JADX WARN: Removed duplicated region for block: B:34:0x018c  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x018d A[Catch: SQLiteException -> 0x01e8, all -> 0x0207, TryCatch #0 {SQLiteException -> 0x01e8, blocks: (B:4:0x005f, B:8:0x0069, B:10:0x00cc, B:15:0x00d6, B:19:0x0120, B:21:0x0157, B:25:0x0165, B:24:0x0161, B:26:0x0168, B:28:0x0170, B:32:0x0178, B:36:0x0191, B:38:0x019c, B:39:0x01ae, B:41:0x01bf, B:42:0x01c8, B:44:0x01d1, B:35:0x018d, B:18:0x011b), top: B:62:0x005f }] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x019c A[Catch: SQLiteException -> 0x01e8, all -> 0x0207, TryCatch #0 {SQLiteException -> 0x01e8, blocks: (B:4:0x005f, B:8:0x0069, B:10:0x00cc, B:15:0x00d6, B:19:0x0120, B:21:0x0157, B:25:0x0165, B:24:0x0161, B:26:0x0168, B:28:0x0170, B:32:0x0178, B:36:0x0191, B:38:0x019c, B:39:0x01ae, B:41:0x01bf, B:42:0x01c8, B:44:0x01d1, B:35:0x018d, B:18:0x011b), top: B:62:0x005f }] */
    /* JADX WARN: Removed duplicated region for block: B:41:0x01bf A[Catch: SQLiteException -> 0x01e8, all -> 0x0207, TryCatch #0 {SQLiteException -> 0x01e8, blocks: (B:4:0x005f, B:8:0x0069, B:10:0x00cc, B:15:0x00d6, B:19:0x0120, B:21:0x0157, B:25:0x0165, B:24:0x0161, B:26:0x0168, B:28:0x0170, B:32:0x0178, B:36:0x0191, B:38:0x019c, B:39:0x01ae, B:41:0x01bf, B:42:0x01c8, B:44:0x01d1, B:35:0x018d, B:18:0x011b), top: B:62:0x005f }] */
    /* JADX WARN: Removed duplicated region for block: B:44:0x01d1 A[Catch: SQLiteException -> 0x01e8, all -> 0x0207, TRY_LEAVE, TryCatch #0 {SQLiteException -> 0x01e8, blocks: (B:4:0x005f, B:8:0x0069, B:10:0x00cc, B:15:0x00d6, B:19:0x0120, B:21:0x0157, B:25:0x0165, B:24:0x0161, B:26:0x0168, B:28:0x0170, B:32:0x0178, B:36:0x0191, B:38:0x019c, B:39:0x01ae, B:41:0x01bf, B:42:0x01c8, B:44:0x01d1, B:35:0x018d, B:18:0x011b), top: B:62:0x005f }] */
    /* JADX WARN: Removed duplicated region for block: B:60:0x020b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.yk5 c0(java.lang.String r34) {
        /*
            Method dump skipped, instructions count: 527
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.c0(java.lang.String):yk5");
    }

    public final void d0(yk5 yk5Var) {
        zt2.j(yk5Var);
        e();
        g();
        String N = yk5Var.N();
        zt2.j(N);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", N);
        contentValues.put("app_instance_id", yk5Var.O());
        contentValues.put("gmp_app_id", yk5Var.Q());
        contentValues.put("resettable_device_id_hash", yk5Var.W());
        contentValues.put("last_bundle_index", Long.valueOf(yk5Var.i()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(yk5Var.a0()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(yk5Var.c0()));
        contentValues.put("app_version", yk5Var.e0());
        contentValues.put("app_store", yk5Var.i0());
        contentValues.put("gmp_version", Long.valueOf(yk5Var.k0()));
        contentValues.put("dev_cert_hash", Long.valueOf(yk5Var.b()));
        contentValues.put("measurement_enabled", Boolean.valueOf(yk5Var.f()));
        contentValues.put("day", Long.valueOf(yk5Var.o()));
        contentValues.put("daily_public_events_count", Long.valueOf(yk5Var.q()));
        contentValues.put("daily_events_count", Long.valueOf(yk5Var.s()));
        contentValues.put("daily_conversions_count", Long.valueOf(yk5Var.u()));
        contentValues.put("config_fetched_time", Long.valueOf(yk5Var.j()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(yk5Var.l()));
        contentValues.put("app_version_int", Long.valueOf(yk5Var.g0()));
        contentValues.put("firebase_instance_id", yk5Var.Y());
        contentValues.put("daily_error_events_count", Long.valueOf(yk5Var.y()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(yk5Var.w()));
        contentValues.put("health_monitor_sample", yk5Var.B());
        contentValues.put("android_id", Long.valueOf(yk5Var.E()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(yk5Var.G()));
        contentValues.put("admob_app_id", yk5Var.S());
        contentValues.put("dynamite_version", Long.valueOf(yk5Var.d()));
        List<String> K = yk5Var.K();
        if (K != null) {
            if (K.size() == 0) {
                this.a.w().p().b("Safelisted events should not be an empty list. appId", N);
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", K));
            }
        }
        z16.a();
        if (this.a.z().v(N, qf5.h0)) {
            contentValues.put("ga_app_id", yk5Var.U());
        }
        try {
            SQLiteDatabase P = P();
            if (P.update("apps", contentValues, "app_id = ?", new String[]{N}) == 0 && P.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                this.a.w().l().b("Failed to insert/update app (got -1). appId", og5.x(N));
            }
        } catch (SQLiteException e) {
            this.a.w().l().c("Error storing app. appId", og5.x(N), e);
        }
    }

    public final x45 e0(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        return f0(j2, str, 1L, false, false, z3, false, z5);
    }

    public final x45 f0(long j2, String str, long j3, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        zt2.f(str);
        e();
        g();
        String[] strArr = {str};
        x45 x45Var = new x45();
        Cursor cursor = null;
        try {
            try {
                SQLiteDatabase P = P();
                Cursor query = P.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
                if (!query.moveToFirst()) {
                    this.a.w().p().b("Not updating daily counts, app is not known. appId", og5.x(str));
                    query.close();
                    return x45Var;
                }
                if (query.getLong(0) == j2) {
                    x45Var.b = query.getLong(1);
                    x45Var.a = query.getLong(2);
                    x45Var.c = query.getLong(3);
                    x45Var.d = query.getLong(4);
                    x45Var.e = query.getLong(5);
                }
                if (z) {
                    x45Var.b += j3;
                }
                if (z2) {
                    x45Var.a += j3;
                }
                if (z3) {
                    x45Var.c += j3;
                }
                if (z4) {
                    x45Var.d += j3;
                }
                if (z5) {
                    x45Var.e += j3;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("day", Long.valueOf(j2));
                contentValues.put("daily_public_events_count", Long.valueOf(x45Var.a));
                contentValues.put("daily_events_count", Long.valueOf(x45Var.b));
                contentValues.put("daily_conversions_count", Long.valueOf(x45Var.c));
                contentValues.put("daily_error_events_count", Long.valueOf(x45Var.d));
                contentValues.put("daily_realtime_events_count", Long.valueOf(x45Var.e));
                P.update("apps", contentValues, "app_id=?", strArr);
                query.close();
                return x45Var;
            } catch (SQLiteException e) {
                this.a.w().l().c("Error updating daily counts. appId", og5.x(str), e);
                if (0 != 0) {
                    cursor.close();
                }
                return x45Var;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    public final void g0(String str, byte[] bArr, String str2) {
        zt2.f(str);
        e();
        g();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        contentValues.put("config_last_modified_time", str2);
        try {
            if (P().update("apps", contentValues, "app_id = ?", new String[]{str}) == 0) {
                this.a.w().l().b("Failed to update remote config (got 0). appId", og5.x(str));
            }
        } catch (SQLiteException e) {
            this.a.w().l().c("Error storing remote config. appId", og5.x(str), e);
        }
    }

    @Override // defpackage.jv5
    public final boolean h() {
        return false;
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0047, code lost:
        if (r2 > (defpackage.q45.f() + r0)) goto L26;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean h0(com.google.android.gms.internal.measurement.f1 r7, boolean r8) {
        /*
            Method dump skipped, instructions count: 296
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.h0(com.google.android.gms.internal.measurement.f1, boolean):boolean");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0040  */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.String i0() {
        /*
            r6 = this;
            android.database.sqlite.SQLiteDatabase r0 = r6.P()
            r1 = 0
            java.lang.String r2 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch: java.lang.Throwable -> L20 android.database.sqlite.SQLiteException -> L22
            boolean r2 = r0.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> L1e java.lang.Throwable -> L3a
            if (r2 == 0) goto L1a
            r2 = 0
            java.lang.String r1 = r0.getString(r2)     // Catch: android.database.sqlite.SQLiteException -> L1e java.lang.Throwable -> L3a
            r0.close()
            return r1
        L1a:
            r0.close()
            return r1
        L1e:
            r2 = move-exception
            goto L25
        L20:
            r0 = move-exception
            goto L3e
        L22:
            r0 = move-exception
            r2 = r0
            r0 = r1
        L25:
            ck5 r3 = r6.a     // Catch: java.lang.Throwable -> L3a
            og5 r3 = r3.w()     // Catch: java.lang.Throwable -> L3a
            jg5 r3 = r3.l()     // Catch: java.lang.Throwable -> L3a
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.b(r4, r2)     // Catch: java.lang.Throwable -> L3a
            if (r0 == 0) goto L39
            r0.close()
        L39:
            return r1
        L3a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L3e:
            if (r1 == 0) goto L43
            r1.close()
        L43:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.i0():java.lang.String");
    }

    public final void j() {
        e();
        g();
        if (y()) {
            long a = this.b.a0().g.a();
            long b = this.a.a().b();
            long abs = Math.abs(b - a);
            this.a.z();
            if (abs > qf5.y.b(null).longValue()) {
                this.b.a0().g.b(b);
                e();
                g();
                if (y()) {
                    SQLiteDatabase P = P();
                    this.a.z();
                    int delete = P.delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(this.a.a().a()), String.valueOf(q45.f())});
                    if (delete > 0) {
                        this.a.w().v().b("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                    }
                }
            }
        }
    }

    public final boolean j0() {
        return K("select count(1) > 0 from queue where has_realtime = 1", null) != 0;
    }

    public final void k(List<Long> list) {
        e();
        g();
        zt2.j(list);
        zt2.l(list.size());
        if (y()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (K(sb3.toString(), null) > 0) {
                this.a.w().p().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase P = P();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < ");
                sb4.append(Integer.MAX_VALUE);
                sb4.append(")");
                P.execSQL(sb4.toString());
            } catch (SQLiteException e) {
                this.a.w().l().b("Error incrementing retry count. error", e);
            }
        }
    }

    public final Object l(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            this.a.w().l().a("Loaded invalid null value from database");
            return null;
        } else if (type != 1) {
            if (type != 2) {
                if (type != 3) {
                    if (type != 4) {
                        this.a.w().l().b("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                        return null;
                    }
                    this.a.w().l().a("Loaded invalid blob type value, ignoring it");
                    return null;
                }
                return cursor.getString(i2);
            }
            return Double.valueOf(cursor.getDouble(i2));
        } else {
            return Long.valueOf(cursor.getLong(i2));
        }
    }

    public final long n() {
        return L("select max(bundle_end_timestamp) from queue", null, 0L);
    }

    public final long o(String str, String str2) {
        long j2;
        SQLiteException e;
        ContentValues contentValues;
        zt2.f(str);
        zt2.f("first_open_count");
        e();
        g();
        SQLiteDatabase P = P();
        P.beginTransaction();
        try {
            try {
                StringBuilder sb = new StringBuilder(48);
                sb.append("select ");
                sb.append("first_open_count");
                sb.append(" from app2 where app_id=?");
                j2 = L(sb.toString(), new String[]{str}, -1L);
                if (j2 == -1) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("app_id", str);
                    contentValues2.put("first_open_count", (Integer) 0);
                    contentValues2.put("previous_install_count", (Integer) 0);
                    if (P.insertWithOnConflict("app2", null, contentValues2, 5) == -1) {
                        this.a.w().l().c("Failed to insert column (got -1). appId", og5.x(str), "first_open_count");
                        return -1L;
                    }
                    j2 = 0;
                }
                try {
                    contentValues = new ContentValues();
                    contentValues.put("app_id", str);
                    contentValues.put("first_open_count", Long.valueOf(1 + j2));
                } catch (SQLiteException e2) {
                    e = e2;
                    this.a.w().l().d("Error inserting column. appId", og5.x(str), "first_open_count", e);
                    return j2;
                }
            } finally {
                P.endTransaction();
            }
        } catch (SQLiteException e3) {
            j2 = 0;
            e = e3;
        }
        if (P.update("app2", contentValues, "app_id = ?", new String[]{str}) == 0) {
            this.a.w().l().c("Failed to update column (got 0). appId", og5.x(str), "first_open_count");
            return -1L;
        }
        P.setTransactionSuccessful();
        return j2;
    }

    public final long p() {
        return L("select max(timestamp) from raw_events", null, 0L);
    }

    public final boolean r() {
        return K("select count(1) > 0 from raw_events", null) != 0;
    }

    public final boolean s() {
        return K("select count(1) > 0 from raw_events where realtime = 1", null) != 0;
    }

    public final long t(String str) {
        zt2.f(str);
        return L("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0L);
    }

    public final boolean u(String str, Long l2, long j2, b1 b1Var) {
        e();
        g();
        zt2.j(b1Var);
        zt2.f(str);
        zt2.j(l2);
        byte[] c = b1Var.c();
        this.a.w().v().c("Saving complex main event, appId, data size", this.a.H().n(str), Integer.valueOf(c.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("event_id", l2);
        contentValues.put("children_to_process", Long.valueOf(j2));
        contentValues.put("main_event", c);
        try {
            if (P().insertWithOnConflict("main_event_params", null, contentValues, 5) == -1) {
                this.a.w().l().b("Failed to insert complex main event (got -1). appId", og5.x(str));
                return false;
            }
            return true;
        } catch (SQLiteException e) {
            this.a.w().l().c("Error storing complex main event. appId", og5.x(str), e);
            return false;
        }
    }

    /* JADX WARN: Not initialized variable reg: 1, insn: 0x00d6: MOVE  (r0 I:??[OBJECT, ARRAY]) = (r1 I:??[OBJECT, ARRAY]), block:B:44:0x00d6 */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00d9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.os.Bundle v(java.lang.String r8) {
        /*
            r7 = this;
            r7.e()
            r7.g()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r7.P()     // Catch: java.lang.Throwable -> Lbc android.database.sqlite.SQLiteException -> Lbe
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch: java.lang.Throwable -> Lbc android.database.sqlite.SQLiteException -> Lbe
            r3 = 0
            r2[r3] = r8     // Catch: java.lang.Throwable -> Lbc android.database.sqlite.SQLiteException -> Lbe
            java.lang.String r4 = "select parameters from default_event_params where app_id=?"
            android.database.Cursor r1 = r1.rawQuery(r4, r2)     // Catch: java.lang.Throwable -> Lbc android.database.sqlite.SQLiteException -> Lbe
            boolean r2 = r1.moveToFirst()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            if (r2 != 0) goto L30
            ck5 r8 = r7.a     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            og5 r8 = r8.w()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            jg5 r8 = r8.v()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            java.lang.String r2 = "Default event parameters not found"
            r8.a(r2)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r1.close()
            return r0
        L30:
            byte[] r2 = r1.getBlob(r3)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            tj5 r3 = com.google.android.gms.internal.measurement.b1.H()     // Catch: java.io.IOException -> La2 android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            com.google.android.gms.internal.measurement.y1 r2 = com.google.android.gms.measurement.internal.r.J(r3, r2)     // Catch: java.io.IOException -> La2 android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            tj5 r2 = (defpackage.tj5) r2     // Catch: java.io.IOException -> La2 android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            com.google.android.gms.internal.measurement.x1 r2 = r2.o()     // Catch: java.io.IOException -> La2 android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            com.google.android.gms.internal.measurement.b1 r2 = (com.google.android.gms.internal.measurement.b1) r2     // Catch: java.io.IOException -> La2 android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            fw5 r8 = r7.b     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r8.Z()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            java.util.List r8 = r2.x()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            android.os.Bundle r2 = new android.os.Bundle     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r2.<init>()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            java.util.Iterator r8 = r8.iterator()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
        L56:
            boolean r3 = r8.hasNext()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            if (r3 == 0) goto L9e
            java.lang.Object r3 = r8.next()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            com.google.android.gms.internal.measurement.d1 r3 = (com.google.android.gms.internal.measurement.d1) r3     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            java.lang.String r4 = r3.y()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            boolean r5 = r3.F()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            if (r5 == 0) goto L74
            double r5 = r3.G()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r2.putDouble(r4, r5)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            goto L56
        L74:
            boolean r5 = r3.D()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            if (r5 == 0) goto L82
            float r3 = r3.E()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r2.putFloat(r4, r3)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            goto L56
        L82:
            boolean r5 = r3.z()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            if (r5 == 0) goto L90
            java.lang.String r3 = r3.A()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r2.putString(r4, r3)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            goto L56
        L90:
            boolean r5 = r3.B()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            if (r5 == 0) goto L56
            long r5 = r3.C()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r2.putLong(r4, r5)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            goto L56
        L9e:
            r1.close()
            return r2
        La2:
            r2 = move-exception
            ck5 r3 = r7.a     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            og5 r3 = r3.w()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            jg5 r3 = r3.l()     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            java.lang.String r4 = "Failed to retrieve default event parameters. appId"
            java.lang.Object r8 = defpackage.og5.x(r8)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r3.c(r4, r8, r2)     // Catch: android.database.sqlite.SQLiteException -> Lba java.lang.Throwable -> Ld5
            r1.close()
            return r0
        Lba:
            r8 = move-exception
            goto Lc0
        Lbc:
            r8 = move-exception
            goto Ld7
        Lbe:
            r8 = move-exception
            r1 = r0
        Lc0:
            ck5 r2 = r7.a     // Catch: java.lang.Throwable -> Ld5
            og5 r2 = r2.w()     // Catch: java.lang.Throwable -> Ld5
            jg5 r2 = r2.l()     // Catch: java.lang.Throwable -> Ld5
            java.lang.String r3 = "Error selecting default event parameters"
            r2.b(r3, r8)     // Catch: java.lang.Throwable -> Ld5
            if (r1 == 0) goto Ld4
            r1.close()
        Ld4:
            return r0
        Ld5:
            r8 = move-exception
            r0 = r1
        Ld7:
            if (r0 == 0) goto Ldc
            r0.close()
        Ldc:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.v(java.lang.String):android.os.Bundle");
    }

    /* JADX WARN: Code restructure failed: missing block: B:100:0x0343, code lost:
        r0 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:101:0x0344, code lost:
        r11.put("session_scoped", r0);
        r11.put("data", r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:103:0x0358, code lost:
        if (P().insertWithOnConflict("property_filters", null, r11, 5) != (-1)) goto L127;
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x035a, code lost:
        r23.a.w().l().b("Failed to insert property filter (got -1). appId", defpackage.og5.x(r24));
     */
    /* JADX WARN: Code restructure failed: missing block: B:106:0x036e, code lost:
        r0 = r22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:107:0x0372, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:108:0x0373, code lost:
        r23.a.w().l().c("Error storing property filter. appId", defpackage.og5.x(r24), r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:109:0x0386, code lost:
        g();
        e();
        defpackage.zt2.f(r24);
        r0 = P();
        r3 = r17;
        r0.delete("property_filters", r3, new java.lang.String[]{r24, java.lang.String.valueOf(r10)});
        r0.delete("event_filters", r3, new java.lang.String[]{r24, java.lang.String.valueOf(r10)});
        r17 = r3;
        r4 = r21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:110:0x03bd, code lost:
        r4 = r21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x018b, code lost:
        r11 = r0.z().iterator();
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x0197, code lost:
        if (r11.hasNext() == false) goto L73;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x01a3, code lost:
        if (r11.next().x() != false) goto L66;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x01a5, code lost:
        r23.a.w().p().c("Property filter with no ID. Audience definition ignored. appId, audienceId", defpackage.og5.x(r24), java.lang.Integer.valueOf(r10));
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x01be, code lost:
        r11 = r0.C().iterator();
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x01d4, code lost:
        if (r11.hasNext() == false) goto L111;
     */
    /* JADX WARN: Code restructure failed: missing block: B:61:0x01d6, code lost:
        r12 = r11.next();
        g();
        e();
        defpackage.zt2.f(r24);
        defpackage.zt2.j(r12);
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x01f0, code lost:
        if (android.text.TextUtils.isEmpty(r12.z()) == false) goto L80;
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x01f2, code lost:
        r0 = r23.a.w().p();
        r8 = defpackage.og5.x(r24);
        r11 = java.lang.Integer.valueOf(r10);
     */
    /* JADX WARN: Code restructure failed: missing block: B:64:0x020a, code lost:
        if (r12.x() == false) goto L110;
     */
    /* JADX WARN: Code restructure failed: missing block: B:65:0x020c, code lost:
        r20 = java.lang.Integer.valueOf(r12.y());
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0217, code lost:
        r20 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x0219, code lost:
        r0.d("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", r8, r11, java.lang.String.valueOf(r20));
        r21 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x0224, code lost:
        r3 = r12.c();
        r21 = r4;
        r4 = new android.content.ContentValues();
        r4.put("app_id", r24);
        r4.put("audience_id", java.lang.Integer.valueOf(r10));
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x023d, code lost:
        if (r12.x() == false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x023f, code lost:
        r8 = java.lang.Integer.valueOf(r12.y());
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0248, code lost:
        r8 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x0249, code lost:
        r4.put("filter_id", r8);
        r4.put("event_name", r12.z());
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x0259, code lost:
        if (r12.H() == false) goto L103;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x025b, code lost:
        r8 = java.lang.Boolean.valueOf(r12.I());
     */
    /* JADX WARN: Code restructure failed: missing block: B:75:0x0264, code lost:
        r8 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x0265, code lost:
        r4.put("session_scoped", r8);
        r4.put("data", r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x0279, code lost:
        if (P().insertWithOnConflict("event_filters", null, r4, 5) != (-1)) goto L93;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x027b, code lost:
        r23.a.w().l().b("Failed to insert event filter (got -1). appId", defpackage.og5.x(r24));
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x028e, code lost:
        r4 = r21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x0294, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x0295, code lost:
        r23.a.w().l().c("Error storing event filter. appId", defpackage.og5.x(r24), r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x02aa, code lost:
        r21 = r4;
        r0 = r0.z().iterator();
     */
    /* JADX WARN: Code restructure failed: missing block: B:85:0x02b8, code lost:
        if (r0.hasNext() == false) goto L141;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x02ba, code lost:
        r3 = r0.next();
        g();
        e();
        defpackage.zt2.f(r24);
        defpackage.zt2.j(r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x02d4, code lost:
        if (android.text.TextUtils.isEmpty(r3.z()) == false) goto L117;
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x02d6, code lost:
        r0 = r23.a.w().p();
        r7 = defpackage.og5.x(r24);
        r8 = java.lang.Integer.valueOf(r10);
     */
    /* JADX WARN: Code restructure failed: missing block: B:89:0x02ee, code lost:
        if (r3.x() == false) goto L140;
     */
    /* JADX WARN: Code restructure failed: missing block: B:90:0x02f0, code lost:
        r3 = java.lang.Integer.valueOf(r3.y());
     */
    /* JADX WARN: Code restructure failed: missing block: B:91:0x02f9, code lost:
        r3 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:92:0x02fa, code lost:
        r0.d("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", r7, r8, java.lang.String.valueOf(r3));
     */
    /* JADX WARN: Code restructure failed: missing block: B:93:0x0303, code lost:
        r4 = r3.c();
        r11 = new android.content.ContentValues();
        r11.put("app_id", r24);
        r11.put("audience_id", java.lang.Integer.valueOf(r10));
     */
    /* JADX WARN: Code restructure failed: missing block: B:94:0x031a, code lost:
        if (r3.x() == false) goto L134;
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:0x031c, code lost:
        r12 = java.lang.Integer.valueOf(r3.y());
     */
    /* JADX WARN: Code restructure failed: missing block: B:96:0x0325, code lost:
        r12 = null;
     */
    /* JADX WARN: Code restructure failed: missing block: B:97:0x0326, code lost:
        r11.put("filter_id", r12);
        r22 = r0;
        r11.put("property_name", r3.z());
     */
    /* JADX WARN: Code restructure failed: missing block: B:98:0x0338, code lost:
        if (r3.D() == false) goto L133;
     */
    /* JADX WARN: Code restructure failed: missing block: B:99:0x033a, code lost:
        r0 = java.lang.Boolean.valueOf(r3.E());
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void x(java.lang.String r24, java.util.List<com.google.android.gms.internal.measurement.q0> r25) {
        /*
            Method dump skipped, instructions count: 1221
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.e55.x(java.lang.String, java.util.List):void");
    }

    public final boolean y() {
        Context m2 = this.a.m();
        this.a.z();
        return m2.getDatabasePath("google_app_measurement.db").exists();
    }
}
