package defpackage;

import java.math.BigInteger;

/* compiled from: EthEstimateGas.java */
/* renamed from: ow0  reason: default package */
/* loaded from: classes3.dex */
public class ow0 extends i83<String> {
    public BigInteger getAmountUsed() {
        return ej2.decodeQuantity(getResult());
    }
}
