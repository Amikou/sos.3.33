package defpackage;

import kotlin.coroutines.intrinsics.CoroutineSingletons;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Intrinsics.kt */
/* renamed from: gs1  reason: default package */
/* loaded from: classes2.dex */
public class gs1 extends IntrinsicsKt__IntrinsicsJvmKt {
    public static final Object d() {
        return CoroutineSingletons.COROUTINE_SUSPENDED;
    }
}
