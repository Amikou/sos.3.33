package defpackage;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Property;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: BaseMotionStrategy.java */
/* renamed from: pn  reason: default package */
/* loaded from: classes2.dex */
public abstract class pn implements b {
    public final Context a;
    public final ExtendedFloatingActionButton b;
    public final ArrayList<Animator.AnimatorListener> c = new ArrayList<>();
    public final se d;
    public z92 e;
    public z92 f;

    /* compiled from: BaseMotionStrategy.java */
    /* renamed from: pn$a */
    /* loaded from: classes2.dex */
    public class a extends Property<ExtendedFloatingActionButton, Float> {
        public a(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(ExtendedFloatingActionButton extendedFloatingActionButton) {
            return Float.valueOf(ne.a(Utils.FLOAT_EPSILON, 1.0f, (Color.alpha(extendedFloatingActionButton.getCurrentTextColor()) / 255.0f) / Color.alpha(extendedFloatingActionButton.J0.getColorForState(extendedFloatingActionButton.getDrawableState(), pn.this.b.J0.getDefaultColor()))));
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(ExtendedFloatingActionButton extendedFloatingActionButton, Float f) {
            int colorForState = extendedFloatingActionButton.J0.getColorForState(extendedFloatingActionButton.getDrawableState(), pn.this.b.J0.getDefaultColor());
            ColorStateList valueOf = ColorStateList.valueOf(Color.argb((int) (ne.a(Utils.FLOAT_EPSILON, Color.alpha(colorForState) / 255.0f, f.floatValue()) * 255.0f), Color.red(colorForState), Color.green(colorForState), Color.blue(colorForState)));
            if (f.floatValue() == 1.0f) {
                extendedFloatingActionButton.D(extendedFloatingActionButton.J0);
            } else {
                extendedFloatingActionButton.D(valueOf);
            }
        }
    }

    public pn(ExtendedFloatingActionButton extendedFloatingActionButton, se seVar) {
        this.b = extendedFloatingActionButton;
        this.a = extendedFloatingActionButton.getContext();
        this.d = seVar;
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public void a() {
        this.d.b();
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public z92 d() {
        return this.f;
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public void f() {
        this.d.b();
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public AnimatorSet g() {
        return l(m());
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public final List<Animator.AnimatorListener> h() {
        return this.c;
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public final void j(z92 z92Var) {
        this.f = z92Var;
    }

    public AnimatorSet l(z92 z92Var) {
        ArrayList arrayList = new ArrayList();
        if (z92Var.j("opacity")) {
            arrayList.add(z92Var.f("opacity", this.b, View.ALPHA));
        }
        if (z92Var.j("scale")) {
            arrayList.add(z92Var.f("scale", this.b, View.SCALE_Y));
            arrayList.add(z92Var.f("scale", this.b, View.SCALE_X));
        }
        if (z92Var.j("width")) {
            arrayList.add(z92Var.f("width", this.b, ExtendedFloatingActionButton.L0));
        }
        if (z92Var.j("height")) {
            arrayList.add(z92Var.f("height", this.b, ExtendedFloatingActionButton.M0));
        }
        if (z92Var.j("paddingStart")) {
            arrayList.add(z92Var.f("paddingStart", this.b, ExtendedFloatingActionButton.N0));
        }
        if (z92Var.j("paddingEnd")) {
            arrayList.add(z92Var.f("paddingEnd", this.b, ExtendedFloatingActionButton.O0));
        }
        if (z92Var.j("labelOpacity")) {
            arrayList.add(z92Var.f("labelOpacity", this.b, new a(Float.class, "LABEL_OPACITY_PROPERTY")));
        }
        AnimatorSet animatorSet = new AnimatorSet();
        re.a(animatorSet, arrayList);
        return animatorSet;
    }

    public final z92 m() {
        z92 z92Var = this.f;
        if (z92Var != null) {
            return z92Var;
        }
        if (this.e == null) {
            this.e = z92.d(this.a, b());
        }
        return (z92) du2.e(this.e);
    }

    @Override // com.google.android.material.floatingactionbutton.b
    public void onAnimationStart(Animator animator) {
        this.d.c(animator);
    }
}
