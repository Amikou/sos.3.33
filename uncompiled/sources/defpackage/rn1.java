package defpackage;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import defpackage.ol2;

/* compiled from: ImageDecodeOptions.java */
/* renamed from: rn1  reason: default package */
/* loaded from: classes.dex */
public class rn1 {
    public static final rn1 m = b().a();
    public final int a;
    public final int b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final Bitmap.Config g;
    public final Bitmap.Config h;
    public final tn1 i;
    public final pq j;
    public final ColorSpace k;
    public final boolean l;

    public rn1(sn1 sn1Var) {
        this.a = sn1Var.l();
        this.b = sn1Var.k();
        this.c = sn1Var.h();
        this.d = sn1Var.m();
        this.e = sn1Var.g();
        this.f = sn1Var.j();
        this.g = sn1Var.c();
        this.h = sn1Var.b();
        this.i = sn1Var.f();
        this.j = sn1Var.d();
        this.k = sn1Var.e();
        this.l = sn1Var.i();
    }

    public static rn1 a() {
        return m;
    }

    public static sn1 b() {
        return new sn1();
    }

    public ol2.b c() {
        return ol2.c(this).a("minDecodeIntervalMs", this.a).a("maxDimensionPx", this.b).c("decodePreviewFrame", this.c).c("useLastFrameForPreview", this.d).c("decodeAllFrames", this.e).c("forceStaticImage", this.f).b("bitmapConfigName", this.g.name()).b("animatedBitmapConfigName", this.h.name()).b("customImageDecoder", this.i).b("bitmapTransformation", this.j).b("colorSpace", this.k);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || rn1.class != obj.getClass()) {
            return false;
        }
        rn1 rn1Var = (rn1) obj;
        if (this.a == rn1Var.a && this.b == rn1Var.b && this.c == rn1Var.c && this.d == rn1Var.d && this.e == rn1Var.e && this.f == rn1Var.f) {
            boolean z = this.l;
            if (z || this.g == rn1Var.g) {
                return (z || this.h == rn1Var.h) && this.i == rn1Var.i && this.j == rn1Var.j && this.k == rn1Var.k;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        int i = (((((((((this.a * 31) + this.b) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0);
        if (!this.l) {
            i = (i * 31) + this.g.ordinal();
        }
        if (!this.l) {
            int i2 = i * 31;
            Bitmap.Config config = this.h;
            i = i2 + (config != null ? config.ordinal() : 0);
        }
        int i3 = i * 31;
        tn1 tn1Var = this.i;
        int hashCode = (i3 + (tn1Var != null ? tn1Var.hashCode() : 0)) * 31;
        pq pqVar = this.j;
        int hashCode2 = (hashCode + (pqVar != null ? pqVar.hashCode() : 0)) * 31;
        ColorSpace colorSpace = this.k;
        return hashCode2 + (colorSpace != null ? colorSpace.hashCode() : 0);
    }

    public String toString() {
        return "ImageDecodeOptions{" + c().toString() + "}";
    }
}
