package defpackage;

import com.rd.animation.type.DropAnimation;

/* compiled from: ValueController.java */
/* renamed from: xg4  reason: default package */
/* loaded from: classes2.dex */
public class xg4 {
    public t20 a;
    public hc3 b;
    public dr4 c;
    public gq3 d;
    public e41 e;
    public d54 f;
    public DropAnimation g;
    public zw3 h;
    public jc3 i;
    public a j;

    /* compiled from: ValueController.java */
    /* renamed from: xg4$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(wg4 wg4Var);
    }

    public xg4(a aVar) {
        this.j = aVar;
    }

    public t20 a() {
        if (this.a == null) {
            this.a = new t20(this.j);
        }
        return this.a;
    }

    public DropAnimation b() {
        if (this.g == null) {
            this.g = new DropAnimation(this.j);
        }
        return this.g;
    }

    public e41 c() {
        if (this.e == null) {
            this.e = new e41(this.j);
        }
        return this.e;
    }

    public hc3 d() {
        if (this.b == null) {
            this.b = new hc3(this.j);
        }
        return this.b;
    }

    public jc3 e() {
        if (this.i == null) {
            this.i = new jc3(this.j);
        }
        return this.i;
    }

    public gq3 f() {
        if (this.d == null) {
            this.d = new gq3(this.j);
        }
        return this.d;
    }

    public zw3 g() {
        if (this.h == null) {
            this.h = new zw3(this.j);
        }
        return this.h;
    }

    public d54 h() {
        if (this.f == null) {
            this.f = new d54(this.j);
        }
        return this.f;
    }

    public dr4 i() {
        if (this.c == null) {
            this.c = new dr4(this.j);
        }
        return this.c;
    }
}
