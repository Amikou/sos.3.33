package defpackage;

import com.bumptech.glide.load.data.j;
import defpackage.j92;
import java.io.InputStream;

/* compiled from: HttpGlideUrlLoader.java */
/* renamed from: il1  reason: default package */
/* loaded from: classes.dex */
public class il1 implements j92<ng1, InputStream> {
    public static final mn2<Integer> b = mn2.f("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);
    public final i92<ng1, ng1> a;

    /* compiled from: HttpGlideUrlLoader.java */
    /* renamed from: il1$a */
    /* loaded from: classes.dex */
    public static class a implements k92<ng1, InputStream> {
        public final i92<ng1, ng1> a = new i92<>(500);

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<ng1, InputStream> c(qa2 qa2Var) {
            return new il1(this.a);
        }
    }

    public il1(i92<ng1, ng1> i92Var) {
        this.a = i92Var;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<InputStream> b(ng1 ng1Var, int i, int i2, vn2 vn2Var) {
        i92<ng1, ng1> i92Var = this.a;
        if (i92Var != null) {
            ng1 a2 = i92Var.a(ng1Var, 0, 0);
            if (a2 == null) {
                this.a.b(ng1Var, 0, 0, ng1Var);
            } else {
                ng1Var = a2;
            }
        }
        return new j92.a<>(ng1Var, new j(ng1Var, ((Integer) vn2Var.c(b)).intValue()));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(ng1 ng1Var) {
        return true;
    }
}
