package defpackage;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: QueueFile.java */
/* renamed from: nx2  reason: default package */
/* loaded from: classes2.dex */
public class nx2 implements Closeable {
    public static final Logger k0 = Logger.getLogger(nx2.class.getName());
    public final RandomAccessFile a;
    public int f0;
    public int g0;
    public b h0;
    public b i0;
    public final byte[] j0 = new byte[16];

    /* compiled from: QueueFile.java */
    /* renamed from: nx2$a */
    /* loaded from: classes2.dex */
    public class a implements d {
        public boolean a = true;
        public final /* synthetic */ StringBuilder b;

        public a(nx2 nx2Var, StringBuilder sb) {
            this.b = sb;
        }

        @Override // defpackage.nx2.d
        public void a(InputStream inputStream, int i) throws IOException {
            if (this.a) {
                this.a = false;
            } else {
                this.b.append(", ");
            }
            this.b.append(i);
        }
    }

    /* compiled from: QueueFile.java */
    /* renamed from: nx2$b */
    /* loaded from: classes2.dex */
    public static class b {
        public static final b c = new b(0, 0);
        public final int a;
        public final int b;

        public b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public String toString() {
            return b.class.getSimpleName() + "[position = " + this.a + ", length = " + this.b + "]";
        }
    }

    /* compiled from: QueueFile.java */
    /* renamed from: nx2$c */
    /* loaded from: classes2.dex */
    public final class c extends InputStream {
        public int a;
        public int f0;

        public /* synthetic */ c(nx2 nx2Var, b bVar, a aVar) {
            this(bVar);
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            nx2.m(bArr, "buffer");
            if ((i | i2) >= 0 && i2 <= bArr.length - i) {
                int i3 = this.f0;
                if (i3 > 0) {
                    if (i2 > i3) {
                        i2 = i3;
                    }
                    nx2.this.x(this.a, bArr, i, i2);
                    this.a = nx2.this.F(this.a + i2);
                    this.f0 -= i2;
                    return i2;
                }
                return -1;
            }
            throw new ArrayIndexOutOfBoundsException();
        }

        public c(b bVar) {
            this.a = nx2.this.F(bVar.a + 4);
            this.f0 = bVar.b;
        }

        @Override // java.io.InputStream
        public int read() throws IOException {
            if (this.f0 == 0) {
                return -1;
            }
            nx2.this.a.seek(this.a);
            int read = nx2.this.a.read();
            this.a = nx2.this.F(this.a + 1);
            this.f0--;
            return read;
        }
    }

    /* compiled from: QueueFile.java */
    /* renamed from: nx2$d */
    /* loaded from: classes2.dex */
    public interface d {
        void a(InputStream inputStream, int i) throws IOException;
    }

    public nx2(File file) throws IOException {
        if (!file.exists()) {
            j(file);
        }
        this.a = n(file);
        r();
    }

    public static void N(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    public static void Q(byte[] bArr, int... iArr) {
        int i = 0;
        for (int i2 : iArr) {
            N(bArr, i, i2);
            i += 4;
        }
    }

    public static void j(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile n = n(file2);
        try {
            n.setLength(4096L);
            n.seek(0L);
            byte[] bArr = new byte[16];
            Q(bArr, 4096, 0, 0, 0);
            n.write(bArr);
            n.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            n.close();
            throw th;
        }
    }

    public static <T> T m(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static RandomAccessFile n(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    public static int u(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + (bArr[i + 3] & 255);
    }

    public final void A(int i) throws IOException {
        this.a.setLength(i);
        this.a.getChannel().force(true);
    }

    public int C() {
        if (this.g0 == 0) {
            return 16;
        }
        b bVar = this.i0;
        int i = bVar.a;
        int i2 = this.h0.a;
        if (i >= i2) {
            return (i - i2) + 4 + bVar.b + 16;
        }
        return (((i + 4) + bVar.b) + this.f0) - i2;
    }

    public final int F(int i) {
        int i2 = this.f0;
        return i < i2 ? i : (i + 16) - i2;
    }

    public final void M(int i, int i2, int i3, int i4) throws IOException {
        Q(this.j0, i, i2, i3, i4);
        this.a.seek(0L);
        this.a.write(this.j0);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        this.a.close();
    }

    public void e(byte[] bArr) throws IOException {
        f(bArr, 0, bArr.length);
    }

    public synchronized void f(byte[] bArr, int i, int i2) throws IOException {
        int F;
        m(bArr, "buffer");
        if ((i | i2) >= 0 && i2 <= bArr.length - i) {
            h(i2);
            boolean l = l();
            if (l) {
                F = 16;
            } else {
                b bVar = this.i0;
                F = F(bVar.a + 4 + bVar.b);
            }
            b bVar2 = new b(F, i2);
            N(this.j0, 0, i2);
            z(bVar2.a, this.j0, 0, 4);
            z(bVar2.a + 4, bArr, i, i2);
            M(this.f0, this.g0 + 1, l ? bVar2.a : this.h0.a, bVar2.a);
            this.i0 = bVar2;
            this.g0++;
            if (l) {
                this.h0 = bVar2;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public synchronized void g() throws IOException {
        M(4096, 0, 0, 0);
        this.g0 = 0;
        b bVar = b.c;
        this.h0 = bVar;
        this.i0 = bVar;
        if (this.f0 > 4096) {
            A(4096);
        }
        this.f0 = 4096;
    }

    public final void h(int i) throws IOException {
        int i2 = i + 4;
        int v = v();
        if (v >= i2) {
            return;
        }
        int i3 = this.f0;
        do {
            v += i3;
            i3 <<= 1;
        } while (v < i2);
        A(i3);
        b bVar = this.i0;
        int F = F(bVar.a + 4 + bVar.b);
        if (F < this.h0.a) {
            FileChannel channel = this.a.getChannel();
            channel.position(this.f0);
            long j = F - 4;
            if (channel.transferTo(16L, j, channel) != j) {
                throw new AssertionError("Copied insufficient number of bytes!");
            }
        }
        int i4 = this.i0.a;
        int i5 = this.h0.a;
        if (i4 < i5) {
            int i6 = (this.f0 + i4) - 16;
            M(i3, this.g0, i5, i6);
            this.i0 = new b(i6, this.i0.b);
        } else {
            M(i3, this.g0, i5, i4);
        }
        this.f0 = i3;
    }

    public synchronized void i(d dVar) throws IOException {
        int i = this.h0.a;
        for (int i2 = 0; i2 < this.g0; i2++) {
            b q = q(i);
            dVar.a(new c(this, q, null), q.b);
            i = F(q.a + 4 + q.b);
        }
    }

    public synchronized boolean l() {
        return this.g0 == 0;
    }

    public final b q(int i) throws IOException {
        if (i == 0) {
            return b.c;
        }
        this.a.seek(i);
        return new b(i, this.a.readInt());
    }

    public final void r() throws IOException {
        this.a.seek(0L);
        this.a.readFully(this.j0);
        int u = u(this.j0, 0);
        this.f0 = u;
        if (u <= this.a.length()) {
            this.g0 = u(this.j0, 4);
            int u2 = u(this.j0, 8);
            int u3 = u(this.j0, 12);
            this.h0 = q(u2);
            this.i0 = q(u3);
            return;
        }
        throw new IOException("File is truncated. Expected length: " + this.f0 + ", Actual length: " + this.a.length());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(nx2.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.f0);
        sb.append(", size=");
        sb.append(this.g0);
        sb.append(", first=");
        sb.append(this.h0);
        sb.append(", last=");
        sb.append(this.i0);
        sb.append(", element lengths=[");
        try {
            i(new a(this, sb));
        } catch (IOException e) {
            k0.log(Level.WARNING, "read error", (Throwable) e);
        }
        sb.append("]]");
        return sb.toString();
    }

    public final int v() {
        return this.f0 - C();
    }

    public synchronized void w() throws IOException {
        if (!l()) {
            if (this.g0 == 1) {
                g();
            } else {
                b bVar = this.h0;
                int F = F(bVar.a + 4 + bVar.b);
                x(F, this.j0, 0, 4);
                int u = u(this.j0, 0);
                M(this.f0, this.g0 - 1, F, this.i0.a);
                this.g0--;
                this.h0 = new b(F, u);
            }
        } else {
            throw new NoSuchElementException();
        }
    }

    public final void x(int i, byte[] bArr, int i2, int i3) throws IOException {
        int F = F(i);
        int i4 = F + i3;
        int i5 = this.f0;
        if (i4 <= i5) {
            this.a.seek(F);
            this.a.readFully(bArr, i2, i3);
            return;
        }
        int i6 = i5 - F;
        this.a.seek(F);
        this.a.readFully(bArr, i2, i6);
        this.a.seek(16L);
        this.a.readFully(bArr, i2 + i6, i3 - i6);
    }

    public final void z(int i, byte[] bArr, int i2, int i3) throws IOException {
        int F = F(i);
        int i4 = F + i3;
        int i5 = this.f0;
        if (i4 <= i5) {
            this.a.seek(F);
            this.a.write(bArr, i2, i3);
            return;
        }
        int i6 = i5 - F;
        this.a.seek(F);
        this.a.write(bArr, i2, i6);
        this.a.seek(16L);
        this.a.write(bArr, i2 + i6, i3 - i6);
    }
}
