package defpackage;

import android.content.Context;

/* compiled from: CreationContextFactory_Factory.java */
/* renamed from: la0  reason: default package */
/* loaded from: classes.dex */
public final class la0 implements z11<ka0> {
    public final ew2<Context> a;
    public final ew2<qz> b;
    public final ew2<qz> c;

    public la0(ew2<Context> ew2Var, ew2<qz> ew2Var2, ew2<qz> ew2Var3) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
    }

    public static la0 a(ew2<Context> ew2Var, ew2<qz> ew2Var2, ew2<qz> ew2Var3) {
        return new la0(ew2Var, ew2Var2, ew2Var3);
    }

    public static ka0 c(Context context, qz qzVar, qz qzVar2) {
        return new ka0(context, qzVar, qzVar2);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public ka0 get() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }
}
