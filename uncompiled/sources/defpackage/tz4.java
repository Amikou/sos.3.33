package defpackage;

import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.b;
import java.lang.ref.WeakReference;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: tz4  reason: default package */
/* loaded from: classes.dex */
public final class tz4 implements b.c {
    public final WeakReference<rz4> a;
    public final a<?> b;
    public final boolean c;

    public tz4(rz4 rz4Var, a<?> aVar, boolean z) {
        this.a = new WeakReference<>(rz4Var);
        this.b = aVar;
        this.c = z;
    }

    @Override // com.google.android.gms.common.internal.b.c
    public final void b(ConnectionResult connectionResult) {
        i05 i05Var;
        Lock lock;
        Lock lock2;
        boolean s;
        boolean x;
        rz4 rz4Var = this.a.get();
        if (rz4Var == null) {
            return;
        }
        Looper myLooper = Looper.myLooper();
        i05Var = rz4Var.a;
        zt2.n(myLooper == i05Var.m.g(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
        lock = rz4Var.b;
        lock.lock();
        try {
            s = rz4Var.s(0);
            if (s) {
                if (!connectionResult.M1()) {
                    rz4Var.r(connectionResult, this.b, this.c);
                }
                x = rz4Var.x();
                if (x) {
                    rz4Var.y();
                }
            }
        } finally {
            lock2 = rz4Var.b;
            lock2.unlock();
        }
    }
}
