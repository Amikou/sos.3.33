package defpackage;

import com.facebook.imagepipeline.memory.b;
import java.io.Closeable;
import java.nio.ByteBuffer;

/* compiled from: BufferMemoryChunk.java */
/* renamed from: vr  reason: default package */
/* loaded from: classes.dex */
public class vr implements b, Closeable {
    public ByteBuffer a;
    public final int f0;
    public final long g0 = System.identityHashCode(this);

    public vr(int i) {
        this.a = ByteBuffer.allocateDirect(i);
        this.f0 = i;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public int a() {
        return this.f0;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public void b(int i, b bVar, int i2, int i3) {
        xt2.g(bVar);
        if (bVar.getUniqueId() == getUniqueId()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Copying from BufferMemoryChunk ");
            sb.append(Long.toHexString(getUniqueId()));
            sb.append(" to BufferMemoryChunk ");
            sb.append(Long.toHexString(bVar.getUniqueId()));
            sb.append(" which are the same ");
            xt2.b(Boolean.FALSE);
        }
        if (bVar.getUniqueId() < getUniqueId()) {
            synchronized (bVar) {
                synchronized (this) {
                    d(i, bVar, i2, i3);
                }
            }
            return;
        }
        synchronized (this) {
            synchronized (bVar) {
                d(i, bVar, i2, i3);
            }
        }
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized int c(int i, byte[] bArr, int i2, int i3) {
        int a;
        xt2.g(bArr);
        xt2.i(!isClosed());
        xt2.g(this.a);
        a = o72.a(i, i3, this.f0);
        o72.b(i, bArr.length, i2, a, this.f0);
        this.a.position(i);
        this.a.put(bArr, i2, a);
        return a;
    }

    @Override // com.facebook.imagepipeline.memory.b, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        this.a = null;
    }

    public final void d(int i, b bVar, int i2, int i3) {
        if (bVar instanceof vr) {
            xt2.i(!isClosed());
            xt2.i(!bVar.isClosed());
            xt2.g(this.a);
            o72.b(i, bVar.a(), i2, i3, this.f0);
            this.a.position(i);
            ByteBuffer byteBuffer = (ByteBuffer) xt2.g(bVar.t());
            byteBuffer.position(i2);
            byte[] bArr = new byte[i3];
            this.a.get(bArr, 0, i3);
            byteBuffer.put(bArr, 0, i3);
            return;
        }
        throw new IllegalArgumentException("Cannot copy two incompatible MemoryChunks");
    }

    @Override // com.facebook.imagepipeline.memory.b
    public long getUniqueId() {
        return this.g0;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized boolean isClosed() {
        return this.a == null;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized byte p(int i) {
        boolean z = true;
        xt2.i(!isClosed());
        xt2.b(Boolean.valueOf(i >= 0));
        if (i >= this.f0) {
            z = false;
        }
        xt2.b(Boolean.valueOf(z));
        xt2.g(this.a);
        return this.a.get(i);
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized int s(int i, byte[] bArr, int i2, int i3) {
        int a;
        xt2.g(bArr);
        xt2.i(!isClosed());
        xt2.g(this.a);
        a = o72.a(i, i3, this.f0);
        o72.b(i, bArr.length, i2, a, this.f0);
        this.a.position(i);
        this.a.get(bArr, i2, a);
        return a;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized ByteBuffer t() {
        return this.a;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public long y() {
        throw new UnsupportedOperationException("Cannot get the pointer of a BufferMemoryChunk");
    }
}
