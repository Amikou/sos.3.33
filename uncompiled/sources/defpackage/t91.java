package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentChangePasswordBinding.java */
/* renamed from: t91  reason: default package */
/* loaded from: classes2.dex */
public final class t91 {
    public final ConstraintLayout a;
    public final AppCompatButton b;
    public final AppCompatEditText c;
    public final AppCompatEditText d;
    public final AppCompatEditText e;
    public final qy1 f;
    public final rp3 g;

    public t91(ConstraintLayout constraintLayout, AppCompatButton appCompatButton, ConstraintLayout constraintLayout2, MaterialCardView materialCardView, MaterialCardView materialCardView2, AppCompatEditText appCompatEditText, AppCompatEditText appCompatEditText2, AppCompatEditText appCompatEditText3, qy1 qy1Var, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, rp3 rp3Var, TextView textView, TextView textView2, TextView textView3) {
        this.a = constraintLayout;
        this.b = appCompatButton;
        this.c = appCompatEditText;
        this.d = appCompatEditText2;
        this.e = appCompatEditText3;
        this.f = qy1Var;
        this.g = rp3Var;
    }

    public static t91 a(View view) {
        int i = R.id.btnSave;
        AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btnSave);
        if (appCompatButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.cvCurrentPassword;
            MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.cvCurrentPassword);
            if (materialCardView != null) {
                i = R.id.cvNewPassword;
                MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.cvNewPassword);
                if (materialCardView2 != null) {
                    i = R.id.etConfirmNewPassword;
                    AppCompatEditText appCompatEditText = (AppCompatEditText) ai4.a(view, R.id.etConfirmNewPassword);
                    if (appCompatEditText != null) {
                        i = R.id.etCurrentPassword;
                        AppCompatEditText appCompatEditText2 = (AppCompatEditText) ai4.a(view, R.id.etCurrentPassword);
                        if (appCompatEditText2 != null) {
                            i = R.id.etNewPassword;
                            AppCompatEditText appCompatEditText3 = (AppCompatEditText) ai4.a(view, R.id.etNewPassword);
                            if (appCompatEditText3 != null) {
                                i = R.id.lPasswordRequirements;
                                View a = ai4.a(view, R.id.lPasswordRequirements);
                                if (a != null) {
                                    qy1 a2 = qy1.a(a);
                                    i = R.id.tilConfirmNewPassword;
                                    TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilConfirmNewPassword);
                                    if (textInputLayout != null) {
                                        i = R.id.tilCurrentPassword;
                                        TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.tilCurrentPassword);
                                        if (textInputLayout2 != null) {
                                            i = R.id.tilNewPassword;
                                            TextInputLayout textInputLayout3 = (TextInputLayout) ai4.a(view, R.id.tilNewPassword);
                                            if (textInputLayout3 != null) {
                                                i = R.id.toolbar;
                                                View a3 = ai4.a(view, R.id.toolbar);
                                                if (a3 != null) {
                                                    rp3 a4 = rp3.a(a3);
                                                    i = R.id.tvConfirmNewPassTitle;
                                                    TextView textView = (TextView) ai4.a(view, R.id.tvConfirmNewPassTitle);
                                                    if (textView != null) {
                                                        i = R.id.tvCurrentPassTitle;
                                                        TextView textView2 = (TextView) ai4.a(view, R.id.tvCurrentPassTitle);
                                                        if (textView2 != null) {
                                                            i = R.id.tvNewPassTitle;
                                                            TextView textView3 = (TextView) ai4.a(view, R.id.tvNewPassTitle);
                                                            if (textView3 != null) {
                                                                return new t91(constraintLayout, appCompatButton, constraintLayout, materialCardView, materialCardView2, appCompatEditText, appCompatEditText2, appCompatEditText3, a2, textInputLayout, textInputLayout2, textInputLayout3, a4, textView, textView2, textView3);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
