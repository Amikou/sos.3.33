package defpackage;

import defpackage.pt0;

/* renamed from: je3  reason: default package */
/* loaded from: classes2.dex */
public class je3 extends pt0.c {
    public je3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public je3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public je3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        ie3 ie3Var = (ie3) this.c;
        if (ie3Var.i()) {
            return i.v();
        }
        ie3 ie3Var2 = (ie3) this.b;
        ie3 ie3Var3 = (ie3) this.d[0];
        int[] d = bd2.d();
        int[] d2 = bd2.d();
        int[] d3 = bd2.d();
        he3.i(ie3Var.f, d3);
        int[] d4 = bd2.d();
        he3.i(d3, d4);
        boolean h = ie3Var3.h();
        int[] iArr = ie3Var3.f;
        if (!h) {
            he3.i(iArr, d2);
            iArr = d2;
        }
        he3.k(ie3Var2.f, iArr, d);
        he3.a(ie3Var2.f, iArr, d2);
        he3.d(d2, d, d2);
        he3.h(bd2.b(d2, d2, d2), d2);
        he3.d(d3, ie3Var2.f, d3);
        he3.h(kd2.G(5, d3, 2, 0), d3);
        he3.h(kd2.H(5, d4, 3, 0, d), d);
        ie3 ie3Var4 = new ie3(d4);
        he3.i(d2, ie3Var4.f);
        int[] iArr2 = ie3Var4.f;
        he3.k(iArr2, d3, iArr2);
        int[] iArr3 = ie3Var4.f;
        he3.k(iArr3, d3, iArr3);
        ie3 ie3Var5 = new ie3(d3);
        he3.k(d3, ie3Var4.f, ie3Var5.f);
        int[] iArr4 = ie3Var5.f;
        he3.d(iArr4, d2, iArr4);
        int[] iArr5 = ie3Var5.f;
        he3.k(iArr5, d, iArr5);
        ie3 ie3Var6 = new ie3(d2);
        he3.l(ie3Var.f, ie3Var6.f);
        if (!h) {
            int[] iArr6 = ie3Var6.f;
            he3.d(iArr6, ie3Var3.f, iArr6);
        }
        return new je3(i, ie3Var4, ie3Var5, new ct0[]{ie3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        ie3 ie3Var = (ie3) this.b;
        ie3 ie3Var2 = (ie3) this.c;
        ie3 ie3Var3 = (ie3) pt0Var.q();
        ie3 ie3Var4 = (ie3) pt0Var.r();
        ie3 ie3Var5 = (ie3) this.d[0];
        ie3 ie3Var6 = (ie3) pt0Var.s(0);
        int[] e = bd2.e();
        int[] d = bd2.d();
        int[] d2 = bd2.d();
        int[] d3 = bd2.d();
        boolean h = ie3Var5.h();
        if (h) {
            iArr = ie3Var3.f;
            iArr2 = ie3Var4.f;
        } else {
            he3.i(ie3Var5.f, d2);
            he3.d(d2, ie3Var3.f, d);
            he3.d(d2, ie3Var5.f, d2);
            he3.d(d2, ie3Var4.f, d2);
            iArr = d;
            iArr2 = d2;
        }
        boolean h2 = ie3Var6.h();
        if (h2) {
            iArr3 = ie3Var.f;
            iArr4 = ie3Var2.f;
        } else {
            he3.i(ie3Var6.f, d3);
            he3.d(d3, ie3Var.f, e);
            he3.d(d3, ie3Var6.f, d3);
            he3.d(d3, ie3Var2.f, d3);
            iArr3 = e;
            iArr4 = d3;
        }
        int[] d4 = bd2.d();
        he3.k(iArr3, iArr, d4);
        he3.k(iArr4, iArr2, d);
        if (bd2.k(d4)) {
            return bd2.k(d) ? J() : i.v();
        }
        he3.i(d4, d2);
        int[] d5 = bd2.d();
        he3.d(d2, d4, d5);
        he3.d(d2, iArr3, d2);
        he3.f(d5, d5);
        bd2.l(iArr4, d5, e);
        he3.h(bd2.b(d2, d2, d5), d5);
        ie3 ie3Var7 = new ie3(d3);
        he3.i(d, ie3Var7.f);
        int[] iArr5 = ie3Var7.f;
        he3.k(iArr5, d5, iArr5);
        ie3 ie3Var8 = new ie3(d5);
        he3.k(d2, ie3Var7.f, ie3Var8.f);
        he3.e(ie3Var8.f, d, e);
        he3.g(e, ie3Var8.f);
        ie3 ie3Var9 = new ie3(d4);
        if (!h) {
            int[] iArr6 = ie3Var9.f;
            he3.d(iArr6, ie3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = ie3Var9.f;
            he3.d(iArr7, ie3Var6.f, iArr7);
        }
        return new je3(i, ie3Var7, ie3Var8, new ct0[]{ie3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new je3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new je3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
