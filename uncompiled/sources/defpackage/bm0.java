package defpackage;

/* compiled from: DelegatingConsumer.java */
/* renamed from: bm0  reason: default package */
/* loaded from: classes.dex */
public abstract class bm0<I, O> extends qm<I> {
    public final l60<O> b;

    public bm0(l60<O> l60Var) {
        this.b = l60Var;
    }

    @Override // defpackage.qm
    public void g() {
        this.b.b();
    }

    @Override // defpackage.qm
    public void h(Throwable th) {
        this.b.a(th);
    }

    @Override // defpackage.qm
    public void j(float f) {
        this.b.c(f);
    }

    public l60<O> p() {
        return this.b;
    }
}
