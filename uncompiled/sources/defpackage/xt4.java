package defpackage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

/* renamed from: xt4  reason: default package */
/* loaded from: classes2.dex */
public final class xt4 implements ServiceConnection {
    public final /* synthetic */ zt4 a;

    public /* synthetic */ xt4(zt4 zt4Var) {
        this.a = zt4Var;
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        it4 it4Var;
        it4Var = this.a.b;
        it4Var.d("ServiceConnectionImpl.onServiceConnected(%s)", componentName);
        this.a.r(new tt4(this, iBinder));
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        it4 it4Var;
        it4Var = this.a.b;
        it4Var.d("ServiceConnectionImpl.onServiceDisconnected(%s)", componentName);
        this.a.r(new vt4(this));
    }
}
