package defpackage;

import androidx.media3.common.util.b;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: PsDurationReader.java */
/* renamed from: jw2  reason: default package */
/* loaded from: classes.dex */
public final class jw2 {
    public boolean c;
    public boolean d;
    public boolean e;
    public final h64 a = new h64(0);
    public long f = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public long g = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public long h = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public final op2 b = new op2();

    public static boolean a(byte[] bArr) {
        return (bArr[0] & 196) == 68 && (bArr[2] & 4) == 4 && (bArr[4] & 4) == 4 && (bArr[5] & 1) == 1 && (bArr[8] & 3) == 3;
    }

    public static long l(op2 op2Var) {
        int e = op2Var.e();
        if (op2Var.a() < 9) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        byte[] bArr = new byte[9];
        op2Var.j(bArr, 0, 9);
        op2Var.P(e);
        return !a(bArr) ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : m(bArr);
    }

    public static long m(byte[] bArr) {
        return (((bArr[0] & 56) >> 3) << 30) | ((bArr[0] & 3) << 28) | ((bArr[1] & 255) << 20) | (((bArr[2] & 248) >> 3) << 15) | ((bArr[2] & 3) << 13) | ((bArr[3] & 255) << 5) | ((bArr[4] & 248) >> 3);
    }

    public final int b(q11 q11Var) {
        this.b.M(b.f);
        this.c = true;
        q11Var.j();
        return 0;
    }

    public long c() {
        return this.h;
    }

    public h64 d() {
        return this.a;
    }

    public boolean e() {
        return this.c;
    }

    public final int f(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    public int g(q11 q11Var, ot2 ot2Var) throws IOException {
        if (!this.e) {
            return j(q11Var, ot2Var);
        }
        if (this.g == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return b(q11Var);
        }
        if (!this.d) {
            return h(q11Var, ot2Var);
        }
        long j = this.f;
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return b(q11Var);
        }
        long b = this.a.b(this.g) - this.a.b(j);
        this.h = b;
        if (b < 0) {
            p12.i("PsDurationReader", "Invalid duration: " + this.h + ". Using TIME_UNSET instead.");
            this.h = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return b(q11Var);
    }

    public final int h(q11 q11Var, ot2 ot2Var) throws IOException {
        int min = (int) Math.min(20000L, q11Var.getLength());
        long j = 0;
        if (q11Var.getPosition() != j) {
            ot2Var.a = j;
            return 1;
        }
        this.b.L(min);
        q11Var.j();
        q11Var.n(this.b.d(), 0, min);
        this.f = i(this.b);
        this.d = true;
        return 0;
    }

    public final long i(op2 op2Var) {
        int f = op2Var.f();
        for (int e = op2Var.e(); e < f - 3; e++) {
            if (f(op2Var.d(), e) == 442) {
                op2Var.P(e + 4);
                long l = l(op2Var);
                if (l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return l;
                }
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final int j(q11 q11Var, ot2 ot2Var) throws IOException {
        long length = q11Var.getLength();
        int min = (int) Math.min(20000L, length);
        long j = length - min;
        if (q11Var.getPosition() != j) {
            ot2Var.a = j;
            return 1;
        }
        this.b.L(min);
        q11Var.j();
        q11Var.n(this.b.d(), 0, min);
        this.g = k(this.b);
        this.e = true;
        return 0;
    }

    public final long k(op2 op2Var) {
        int e = op2Var.e();
        for (int f = op2Var.f() - 4; f >= e; f--) {
            if (f(op2Var.d(), f) == 442) {
                op2Var.P(f + 4);
                long l = l(op2Var);
                if (l != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return l;
                }
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
