package defpackage;

import android.os.SystemClock;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

/* compiled from: FloatScroller.java */
/* renamed from: e71  reason: default package */
/* loaded from: classes.dex */
public class e71 {
    public float c;
    public float d;
    public float e;
    public long f;
    public boolean b = true;
    public long g = 250;
    public final Interpolator a = new AccelerateDecelerateInterpolator();

    public static float d(float f, float f2, float f3) {
        return f + ((f2 - f) * f3);
    }

    public boolean a() {
        if (this.b) {
            return false;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.f;
        long j = this.g;
        if (elapsedRealtime >= j) {
            this.b = true;
            this.e = this.d;
            return false;
        }
        this.e = d(this.c, this.d, this.a.getInterpolation(((float) elapsedRealtime) / ((float) j)));
        return true;
    }

    public void b() {
        this.b = true;
    }

    public float c() {
        return this.e;
    }

    public boolean e() {
        return this.b;
    }

    public void f(long j) {
        this.g = j;
    }

    public void g(float f, float f2) {
        this.b = false;
        this.f = SystemClock.elapsedRealtime();
        this.c = f;
        this.d = f2;
        this.e = f;
    }
}
