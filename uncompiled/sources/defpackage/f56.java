package defpackage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: f56  reason: default package */
/* loaded from: classes.dex */
public final class f56 extends c55 {
    public final ds5 g0;
    public final Map<String, c55> h0;

    public f56(ds5 ds5Var) {
        super("require");
        this.h0 = new HashMap();
        this.g0 = ds5Var;
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        c55 c55Var;
        vm5.a("require", 1, list);
        String zzc = wk5Var.a(list.get(0)).zzc();
        if (this.h0.containsKey(zzc)) {
            return this.h0.get(zzc);
        }
        ds5 ds5Var = this.g0;
        if (ds5Var.a.containsKey(zzc)) {
            try {
                c55Var = ds5Var.a.get(zzc).call();
            } catch (Exception unused) {
                String valueOf = String.valueOf(zzc);
                throw new IllegalStateException(valueOf.length() != 0 ? "Failed to create API implementation: ".concat(valueOf) : new String("Failed to create API implementation: "));
            }
        } else {
            c55Var = z55.X;
        }
        if (c55Var instanceof c55) {
            this.h0.put(zzc, (c55) c55Var);
        }
        return c55Var;
    }
}
