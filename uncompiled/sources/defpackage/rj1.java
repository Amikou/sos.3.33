package defpackage;

import org.slf4j.Marker;

/* compiled from: HandshakeImpl1Client.java */
/* renamed from: rj1  reason: default package */
/* loaded from: classes2.dex */
public class rj1 extends uj1 implements iz {
    public String c = Marker.ANY_MARKER;

    @Override // defpackage.hz
    public String a() {
        return this.c;
    }

    @Override // defpackage.iz
    public void f(String str) throws IllegalArgumentException {
        if (str != null) {
            this.c = str;
            return;
        }
        throw new IllegalArgumentException("http resource descriptor must not be null");
    }
}
