package defpackage;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import net.safemoon.androidwallet.dialogs.GraphFragment;
import net.safemoon.androidwallet.dialogs.GraphTradingViewFragment;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.model.graph.TradingViewSymbol;
import net.safemoon.androidwallet.utils.ChartParameter;

/* compiled from: ChartProvider.kt */
/* renamed from: dy */
/* loaded from: classes2.dex */
public final class dy {
    public static final a f = new a(null);
    public static boolean g;
    public final Coin a;
    public final FragmentManager b;
    public final String c;
    public final String d;
    public final String e;

    /* compiled from: ChartProvider.kt */
    /* renamed from: dy$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final void a(boolean z) {
            dy.g = z;
        }
    }

    /* compiled from: ChartProvider.kt */
    /* renamed from: dy$b */
    /* loaded from: classes2.dex */
    public static final class b implements wu<List<? extends TradingViewSymbol>> {
        public final /* synthetic */ rc1<te4> a;
        public final /* synthetic */ dy b;
        public final /* synthetic */ ChartParameter c;

        public b(rc1<te4> rc1Var, dy dyVar, ChartParameter chartParameter) {
            this.a = rc1Var;
            this.b = dyVar;
            this.c = chartParameter;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<List<? extends TradingViewSymbol>> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            dy.f.a(false);
            rc1<te4> rc1Var = this.a;
            if (rc1Var != null) {
                rc1Var.invoke();
            }
            this.b.i(null, this.c);
        }

        /* JADX WARN: Removed duplicated region for block: B:80:0x00b4 A[EDGE_INSN: B:80:0x00b4->B:71:0x00b4 ?: BREAK  , SYNTHETIC] */
        @Override // defpackage.wu
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void b(retrofit2.b<java.util.List<? extends net.safemoon.androidwallet.model.graph.TradingViewSymbol>> r10, retrofit2.n<java.util.List<? extends net.safemoon.androidwallet.model.graph.TradingViewSymbol>> r11) {
            /*
                r9 = this;
                java.lang.String r0 = "call"
                defpackage.fs1.f(r10, r0)
                java.lang.String r10 = "response"
                defpackage.fs1.f(r11, r10)
                dy$a r10 = defpackage.dy.f
                r0 = 0
                r10.a(r0)
                rc1<te4> r10 = r9.a
                if (r10 == 0) goto L17
                r10.invoke()
            L17:
                dy r10 = r9.b
                net.safemoon.androidwallet.model.Coin r10 = r10.f()
                java.lang.String r10 = r10.getSymbol()
                dy r1 = r9.b
                java.lang.String r1 = defpackage.dy.b(r1)
                boolean r10 = r10.equals(r1)
                if (r10 == 0) goto L42
                net.safemoon.androidwallet.model.graph.TradingViewSymbol r10 = new net.safemoon.androidwallet.model.graph.TradingViewSymbol
                r10.<init>()
                dy r11 = r9.b
                java.lang.String r11 = defpackage.dy.c(r11)
                r10.symbol = r11
                dy r11 = r9.b
                net.safemoon.androidwallet.utils.ChartParameter r0 = r9.c
                defpackage.dy.e(r11, r10, r0)
                return
            L42:
                dy r10 = r9.b
                net.safemoon.androidwallet.model.Coin r10 = r10.f()
                java.lang.String r10 = r10.getSymbol()
                java.lang.String r1 = "coin.symbol"
                defpackage.fs1.e(r10, r1)
                java.util.Locale r1 = java.util.Locale.ROOT
                java.lang.String r10 = r10.toLowerCase(r1)
                java.lang.String r1 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
                defpackage.fs1.e(r10, r1)
                java.lang.String r1 = "safemoon"
                boolean r10 = defpackage.fs1.b(r10, r1)
                r1 = 0
                if (r10 == 0) goto L6d
                dy r10 = r9.b
                net.safemoon.androidwallet.utils.ChartParameter r11 = r9.c
                defpackage.dy.e(r10, r1, r11)
                return
            L6d:
                dy r10 = r9.b
                java.lang.Object r11 = r11.a()
                java.util.List r11 = (java.util.List) r11
                if (r11 != 0) goto L78
                goto Lbf
            L78:
                dy r2 = r9.b
                java.util.Iterator r11 = r11.iterator()
            L7e:
                boolean r3 = r11.hasNext()
                if (r3 == 0) goto Lb3
                java.lang.Object r3 = r11.next()
                r4 = r3
                net.safemoon.androidwallet.model.graph.TradingViewSymbol r4 = (net.safemoon.androidwallet.model.graph.TradingViewSymbol) r4
                java.lang.String r5 = r4.symbol
                java.lang.String r6 = "it.symbol"
                defpackage.fs1.e(r5, r6)
                java.lang.String r7 = defpackage.dy.b(r2)
                r8 = 2
                boolean r5 = defpackage.dv3.s(r5, r7, r0, r8, r1)
                if (r5 != 0) goto Laf
                java.lang.String r4 = r4.symbol
                defpackage.fs1.e(r4, r6)
                java.lang.String r5 = defpackage.dy.a(r2)
                boolean r4 = defpackage.dv3.s(r4, r5, r0, r8, r1)
                if (r4 == 0) goto Lad
                goto Laf
            Lad:
                r4 = r0
                goto Lb0
            Laf:
                r4 = 1
            Lb0:
                if (r4 == 0) goto L7e
                goto Lb4
            Lb3:
                r3 = r1
            Lb4:
                net.safemoon.androidwallet.model.graph.TradingViewSymbol r3 = (net.safemoon.androidwallet.model.graph.TradingViewSymbol) r3
                if (r3 != 0) goto Lb9
                goto Lbf
            Lb9:
                java.lang.String r11 = r3.symbol
                if (r11 != 0) goto Lbe
                goto Lbf
            Lbe:
                r1 = r3
            Lbf:
                net.safemoon.androidwallet.utils.ChartParameter r11 = r9.c
                defpackage.dy.e(r10, r1, r11)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.dy.b.b(retrofit2.b, retrofit2.n):void");
        }
    }

    public dy(Coin coin, FragmentManager fragmentManager) {
        fs1.f(coin, "coin");
        fs1.f(fragmentManager, "fragmentManager");
        this.a = coin;
        this.b = fragmentManager;
        this.c = "USDT";
        this.d = "BUSD";
        this.e = "USDTUSD";
    }

    public static /* synthetic */ void h(dy dyVar, ChartParameter chartParameter, rc1 rc1Var, int i, Object obj) {
        if ((i & 1) != 0) {
            chartParameter = null;
        }
        dyVar.g(chartParameter, rc1Var);
    }

    public final Coin f() {
        return this.a;
    }

    public final void g(ChartParameter chartParameter, rc1<te4> rc1Var) {
        fs1.f(rc1Var, "onDone");
        if (g) {
            return;
        }
        g = true;
        String symbol = this.a.getSymbol();
        if (symbol == null) {
            return;
        }
        a4.o().a(symbol).n(new b(rc1Var, this, chartParameter));
    }

    public final void i(TradingViewSymbol tradingViewSymbol, ChartParameter chartParameter) {
        GraphFragment c;
        if (this.b.M0()) {
            return;
        }
        if (tradingViewSymbol != null) {
            GraphTradingViewFragment.x0.a(this.a, tradingViewSymbol).M(this.b);
            return;
        }
        GraphFragment.b bVar = GraphFragment.L0;
        String name = this.a.getName();
        fs1.e(name, "coin.name");
        String symbol = this.a.getSymbol();
        fs1.e(symbol, "coin.symbol");
        String valueOf = String.valueOf(this.a.getId());
        String slug = this.a.getSlug();
        fs1.e(slug, "coin.slug");
        c = bVar.c(name, symbol, valueOf, slug, (r25 & 16) != 0 ? null : null, (r25 & 32) != 0 ? 0 : chartParameter == null ? 0 : chartParameter.getDefaultInterval(), (r25 & 64) != 0 ? null : null, (r25 & 128) != 0 ? 0 : 0, (r25 & 256) != 0 ? false : false, (r25 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? null : null);
        c.C0(this.b);
    }
}
