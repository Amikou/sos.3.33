package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.view.menu.i;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.s;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.internal.ParcelableSparseArray;
import defpackage.b6;
import java.util.ArrayList;

/* compiled from: NavigationMenuPresenter.java */
/* renamed from: le2  reason: default package */
/* loaded from: classes2.dex */
public class le2 implements androidx.appcompat.view.menu.i {
    public NavigationMenuView a;
    public LinearLayout f0;
    public i.a g0;
    public androidx.appcompat.view.menu.e h0;
    public int i0;
    public c j0;
    public LayoutInflater k0;
    public int l0;
    public boolean m0;
    public ColorStateList n0;
    public ColorStateList o0;
    public Drawable p0;
    public int q0;
    public int r0;
    public int s0;
    public boolean t0;
    public int v0;
    public int w0;
    public int x0;
    public boolean u0 = true;
    public int y0 = -1;
    public final View.OnClickListener z0 = new a();

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$a */
    /* loaded from: classes2.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            boolean z = true;
            le2.this.J(true);
            androidx.appcompat.view.menu.g itemData = ((NavigationMenuItemView) view).getItemData();
            le2 le2Var = le2.this;
            boolean O = le2Var.h0.O(itemData, le2Var, 0);
            if (itemData != null && itemData.isCheckable() && O) {
                le2.this.j0.j(itemData);
            } else {
                z = false;
            }
            le2.this.J(false);
            if (z) {
                le2.this.d(false);
            }
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$b */
    /* loaded from: classes2.dex */
    public static class b extends l {
        public b(View view) {
            super(view);
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$c */
    /* loaded from: classes2.dex */
    public class c extends RecyclerView.Adapter<l> {
        public final ArrayList<e> a = new ArrayList<>();
        public androidx.appcompat.view.menu.g b;
        public boolean c;

        public c() {
            h();
        }

        public final void a(int i, int i2) {
            while (i < i2) {
                ((g) this.a.get(i)).b = true;
                i++;
            }
        }

        public Bundle b() {
            Bundle bundle = new Bundle();
            androidx.appcompat.view.menu.g gVar = this.b;
            if (gVar != null) {
                bundle.putInt("android:menu:checked", gVar.getItemId());
            }
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                e eVar = this.a.get(i);
                if (eVar instanceof g) {
                    androidx.appcompat.view.menu.g a = ((g) eVar).a();
                    View actionView = a != null ? a.getActionView() : null;
                    if (actionView != null) {
                        ParcelableSparseArray parcelableSparseArray = new ParcelableSparseArray();
                        actionView.saveHierarchyState(parcelableSparseArray);
                        sparseArray.put(a.getItemId(), parcelableSparseArray);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        public androidx.appcompat.view.menu.g c() {
            return this.b;
        }

        public int d() {
            int i = le2.this.f0.getChildCount() == 0 ? 0 : 1;
            for (int i2 = 0; i2 < le2.this.j0.getItemCount(); i2++) {
                if (le2.this.j0.getItemViewType(i2) == 0) {
                    i++;
                }
            }
            return i;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: e */
        public void onBindViewHolder(l lVar, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType != 0) {
                if (itemViewType == 1) {
                    ((TextView) lVar.itemView).setText(((g) this.a.get(i)).a().getTitle());
                    return;
                } else if (itemViewType != 2) {
                    return;
                } else {
                    f fVar = (f) this.a.get(i);
                    lVar.itemView.setPadding(0, fVar.b(), 0, fVar.a());
                    return;
                }
            }
            NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) lVar.itemView;
            navigationMenuItemView.setIconTintList(le2.this.o0);
            le2 le2Var = le2.this;
            if (le2Var.m0) {
                navigationMenuItemView.setTextAppearance(le2Var.l0);
            }
            ColorStateList colorStateList = le2.this.n0;
            if (colorStateList != null) {
                navigationMenuItemView.setTextColor(colorStateList);
            }
            Drawable drawable = le2.this.p0;
            ei4.w0(navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
            g gVar = (g) this.a.get(i);
            navigationMenuItemView.setNeedsEmptyIcon(gVar.b);
            navigationMenuItemView.setHorizontalPadding(le2.this.q0);
            navigationMenuItemView.setIconPadding(le2.this.r0);
            le2 le2Var2 = le2.this;
            if (le2Var2.t0) {
                navigationMenuItemView.setIconSize(le2Var2.s0);
            }
            navigationMenuItemView.setMaxLines(le2.this.v0);
            navigationMenuItemView.d(gVar.a(), 0);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: f */
        public l onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 0) {
                le2 le2Var = le2.this;
                return new i(le2Var.k0, viewGroup, le2Var.z0);
            } else if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return null;
                    }
                    return new b(le2.this.f0);
                }
                return new j(le2.this.k0, viewGroup);
            } else {
                return new k(le2.this.k0, viewGroup);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        /* renamed from: g */
        public void onViewRecycled(l lVar) {
            if (lVar instanceof i) {
                ((NavigationMenuItemView) lVar.itemView).g();
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.a.size();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public long getItemId(int i) {
            return i;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemViewType(int i) {
            e eVar = this.a.get(i);
            if (eVar instanceof f) {
                return 2;
            }
            if (eVar instanceof d) {
                return 3;
            }
            if (eVar instanceof g) {
                return ((g) eVar).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        public final void h() {
            if (this.c) {
                return;
            }
            boolean z = true;
            this.c = true;
            this.a.clear();
            this.a.add(new d());
            int i = -1;
            int size = le2.this.h0.G().size();
            int i2 = 0;
            boolean z2 = false;
            int i3 = 0;
            while (i2 < size) {
                androidx.appcompat.view.menu.g gVar = le2.this.h0.G().get(i2);
                if (gVar.isChecked()) {
                    j(gVar);
                }
                if (gVar.isCheckable()) {
                    gVar.t(false);
                }
                if (gVar.hasSubMenu()) {
                    SubMenu subMenu = gVar.getSubMenu();
                    if (subMenu.hasVisibleItems()) {
                        if (i2 != 0) {
                            this.a.add(new f(le2.this.x0, 0));
                        }
                        this.a.add(new g(gVar));
                        int size2 = this.a.size();
                        int size3 = subMenu.size();
                        int i4 = 0;
                        boolean z3 = false;
                        while (i4 < size3) {
                            androidx.appcompat.view.menu.g gVar2 = (androidx.appcompat.view.menu.g) subMenu.getItem(i4);
                            if (gVar2.isVisible()) {
                                if (!z3 && gVar2.getIcon() != null) {
                                    z3 = z;
                                }
                                if (gVar2.isCheckable()) {
                                    gVar2.t(false);
                                }
                                if (gVar.isChecked()) {
                                    j(gVar);
                                }
                                this.a.add(new g(gVar2));
                            }
                            i4++;
                            z = true;
                        }
                        if (z3) {
                            a(size2, this.a.size());
                        }
                    }
                } else {
                    int groupId = gVar.getGroupId();
                    if (groupId != i) {
                        i3 = this.a.size();
                        z2 = gVar.getIcon() != null;
                        if (i2 != 0) {
                            i3++;
                            ArrayList<e> arrayList = this.a;
                            int i5 = le2.this.x0;
                            arrayList.add(new f(i5, i5));
                        }
                    } else if (!z2 && gVar.getIcon() != null) {
                        a(i3, this.a.size());
                        z2 = true;
                    }
                    g gVar3 = new g(gVar);
                    gVar3.b = z2;
                    this.a.add(gVar3);
                    i = groupId;
                }
                i2++;
                z = true;
            }
            this.c = false;
        }

        public void i(Bundle bundle) {
            androidx.appcompat.view.menu.g a;
            View actionView;
            ParcelableSparseArray parcelableSparseArray;
            androidx.appcompat.view.menu.g a2;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    e eVar = this.a.get(i2);
                    if ((eVar instanceof g) && (a2 = ((g) eVar).a()) != null && a2.getItemId() == i) {
                        j(a2);
                        break;
                    }
                    i2++;
                }
                this.c = false;
                h();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    e eVar2 = this.a.get(i3);
                    if ((eVar2 instanceof g) && (a = ((g) eVar2).a()) != null && (actionView = a.getActionView()) != null && (parcelableSparseArray = (ParcelableSparseArray) sparseParcelableArray.get(a.getItemId())) != null) {
                        actionView.restoreHierarchyState(parcelableSparseArray);
                    }
                }
            }
        }

        public void j(androidx.appcompat.view.menu.g gVar) {
            if (this.b == gVar || !gVar.isCheckable()) {
                return;
            }
            androidx.appcompat.view.menu.g gVar2 = this.b;
            if (gVar2 != null) {
                gVar2.setChecked(false);
            }
            this.b = gVar;
            gVar.setChecked(true);
        }

        public void k(boolean z) {
            this.c = z;
        }

        public void l() {
            h();
            notifyDataSetChanged();
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$d */
    /* loaded from: classes2.dex */
    public static class d implements e {
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$e */
    /* loaded from: classes2.dex */
    public interface e {
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$f */
    /* loaded from: classes2.dex */
    public static class f implements e {
        public final int a;
        public final int b;

        public f(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public int a() {
            return this.b;
        }

        public int b() {
            return this.a;
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$g */
    /* loaded from: classes2.dex */
    public static class g implements e {
        public final androidx.appcompat.view.menu.g a;
        public boolean b;

        public g(androidx.appcompat.view.menu.g gVar) {
            this.a = gVar;
        }

        public androidx.appcompat.view.menu.g a() {
            return this.a;
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$h */
    /* loaded from: classes2.dex */
    public class h extends s {
        public h(RecyclerView recyclerView) {
            super(recyclerView);
        }

        @Override // androidx.recyclerview.widget.s, defpackage.z5
        public void g(View view, b6 b6Var) {
            super.g(view, b6Var);
            b6Var.e0(b6.b.a(le2.this.j0.d(), 0, false));
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$i */
    /* loaded from: classes2.dex */
    public static class i extends l {
        public i(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(x03.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$j */
    /* loaded from: classes2.dex */
    public static class j extends l {
        public j(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(x03.design_navigation_item_separator, viewGroup, false));
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$k */
    /* loaded from: classes2.dex */
    public static class k extends l {
        public k(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(x03.design_navigation_item_subheader, viewGroup, false));
        }
    }

    /* compiled from: NavigationMenuPresenter.java */
    /* renamed from: le2$l */
    /* loaded from: classes2.dex */
    public static abstract class l extends RecyclerView.a0 {
        public l(View view) {
            super(view);
        }
    }

    public void A(Drawable drawable) {
        this.p0 = drawable;
        d(false);
    }

    public void B(int i2) {
        this.q0 = i2;
        d(false);
    }

    public void C(int i2) {
        this.r0 = i2;
        d(false);
    }

    public void D(int i2) {
        if (this.s0 != i2) {
            this.s0 = i2;
            this.t0 = true;
            d(false);
        }
    }

    public void E(ColorStateList colorStateList) {
        this.o0 = colorStateList;
        d(false);
    }

    public void F(int i2) {
        this.v0 = i2;
        d(false);
    }

    public void G(int i2) {
        this.l0 = i2;
        this.m0 = true;
        d(false);
    }

    public void H(ColorStateList colorStateList) {
        this.n0 = colorStateList;
        d(false);
    }

    public void I(int i2) {
        this.y0 = i2;
        NavigationMenuView navigationMenuView = this.a;
        if (navigationMenuView != null) {
            navigationMenuView.setOverScrollMode(i2);
        }
    }

    public void J(boolean z) {
        c cVar = this.j0;
        if (cVar != null) {
            cVar.k(z);
        }
    }

    public final void K() {
        int i2 = (this.f0.getChildCount() == 0 && this.u0) ? this.w0 : 0;
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, i2, 0, navigationMenuView.getPaddingBottom());
    }

    public void b(View view) {
        this.f0.addView(view);
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    @Override // androidx.appcompat.view.menu.i
    public void c(androidx.appcompat.view.menu.e eVar, boolean z) {
        i.a aVar = this.g0;
        if (aVar != null) {
            aVar.c(eVar, z);
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public void d(boolean z) {
        c cVar = this.j0;
        if (cVar != null) {
            cVar.l();
        }
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean e() {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean f(androidx.appcompat.view.menu.e eVar, androidx.appcompat.view.menu.g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean g(androidx.appcompat.view.menu.e eVar, androidx.appcompat.view.menu.g gVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public int getId() {
        return this.i0;
    }

    @Override // androidx.appcompat.view.menu.i
    public void i(Context context, androidx.appcompat.view.menu.e eVar) {
        this.k0 = LayoutInflater.from(context);
        this.h0 = eVar;
        this.x0 = context.getResources().getDimensionPixelOffset(jz2.design_navigation_separator_vertical_padding);
    }

    @Override // androidx.appcompat.view.menu.i
    public void j(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.a.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.j0.i(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.f0.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    public void k(jp4 jp4Var) {
        int m = jp4Var.m();
        if (this.w0 != m) {
            this.w0 = m;
            K();
        }
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, navigationMenuView.getPaddingTop(), 0, jp4Var.j());
        ei4.i(this.f0, jp4Var);
    }

    @Override // androidx.appcompat.view.menu.i
    public boolean l(androidx.appcompat.view.menu.l lVar) {
        return false;
    }

    @Override // androidx.appcompat.view.menu.i
    public Parcelable m() {
        Bundle bundle = new Bundle();
        if (this.a != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.a.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        c cVar = this.j0;
        if (cVar != null) {
            bundle.putBundle("android:menu:adapter", cVar.b());
        }
        if (this.f0 != null) {
            SparseArray<? extends Parcelable> sparseArray2 = new SparseArray<>();
            this.f0.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    public androidx.appcompat.view.menu.g n() {
        return this.j0.c();
    }

    public int o() {
        return this.f0.getChildCount();
    }

    public Drawable p() {
        return this.p0;
    }

    public int q() {
        return this.q0;
    }

    public int r() {
        return this.r0;
    }

    public int s() {
        return this.v0;
    }

    public ColorStateList t() {
        return this.n0;
    }

    public ColorStateList u() {
        return this.o0;
    }

    public androidx.appcompat.view.menu.j v(ViewGroup viewGroup) {
        if (this.a == null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) this.k0.inflate(x03.design_navigation_menu, viewGroup, false);
            this.a = navigationMenuView;
            navigationMenuView.setAccessibilityDelegateCompat(new h(this.a));
            if (this.j0 == null) {
                this.j0 = new c();
            }
            int i2 = this.y0;
            if (i2 != -1) {
                this.a.setOverScrollMode(i2);
            }
            this.f0 = (LinearLayout) this.k0.inflate(x03.design_navigation_item_header, (ViewGroup) this.a, false);
            this.a.setAdapter(this.j0);
        }
        return this.a;
    }

    public View w(int i2) {
        View inflate = this.k0.inflate(i2, (ViewGroup) this.f0, false);
        b(inflate);
        return inflate;
    }

    public void x(boolean z) {
        if (this.u0 != z) {
            this.u0 = z;
            K();
        }
    }

    public void y(androidx.appcompat.view.menu.g gVar) {
        this.j0.j(gVar);
    }

    public void z(int i2) {
        this.i0 = i2;
    }
}
