package defpackage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.j;

/* compiled from: DialogFragment.java */
/* renamed from: sn0  reason: default package */
/* loaded from: classes.dex */
public class sn0 extends Fragment implements DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {
    public Handler a;
    public Runnable f0;
    public DialogInterface.OnCancelListener g0;
    public DialogInterface.OnDismissListener h0;
    public int i0;
    public int j0;
    public boolean k0;
    public boolean l0;
    public int m0;
    public boolean n0;
    public tl2<rz1> o0;
    public Dialog p0;
    public boolean q0;
    public boolean r0;
    public boolean s0;
    public boolean t0;

    /* compiled from: DialogFragment.java */
    /* renamed from: sn0$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            sn0.this.h0.onDismiss(sn0.this.p0);
        }
    }

    /* compiled from: DialogFragment.java */
    /* renamed from: sn0$b */
    /* loaded from: classes.dex */
    public class b implements DialogInterface.OnCancelListener {
        public b() {
        }

        @Override // android.content.DialogInterface.OnCancelListener
        @SuppressLint({"SyntheticAccessor"})
        public void onCancel(DialogInterface dialogInterface) {
            if (sn0.this.p0 != null) {
                sn0 sn0Var = sn0.this;
                sn0Var.onCancel(sn0Var.p0);
            }
        }
    }

    /* compiled from: DialogFragment.java */
    /* renamed from: sn0$c */
    /* loaded from: classes.dex */
    public class c implements DialogInterface.OnDismissListener {
        public c() {
        }

        @Override // android.content.DialogInterface.OnDismissListener
        @SuppressLint({"SyntheticAccessor"})
        public void onDismiss(DialogInterface dialogInterface) {
            if (sn0.this.p0 != null) {
                sn0 sn0Var = sn0.this;
                sn0Var.onDismiss(sn0Var.p0);
            }
        }
    }

    /* compiled from: DialogFragment.java */
    /* renamed from: sn0$d */
    /* loaded from: classes.dex */
    public class d implements tl2<rz1> {
        public d() {
        }

        @Override // defpackage.tl2
        @SuppressLint({"SyntheticAccessor"})
        /* renamed from: a */
        public void onChanged(rz1 rz1Var) {
            if (rz1Var == null || !sn0.this.l0) {
                return;
            }
            View requireView = sn0.this.requireView();
            if (requireView.getParent() == null) {
                if (sn0.this.p0 != null) {
                    if (FragmentManager.H0(3)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("DialogFragment ");
                        sb.append(this);
                        sb.append(" setting the content view on ");
                        sb.append(sn0.this.p0);
                    }
                    sn0.this.p0.setContentView(requireView);
                    return;
                }
                return;
            }
            throw new IllegalStateException("DialogFragment can not be attached to a container view");
        }
    }

    /* compiled from: DialogFragment.java */
    /* renamed from: sn0$e */
    /* loaded from: classes.dex */
    public class e extends x91 {
        public final /* synthetic */ x91 a;

        public e(x91 x91Var) {
            this.a = x91Var;
        }

        @Override // defpackage.x91
        public View c(int i) {
            if (this.a.d()) {
                return this.a.c(i);
            }
            return sn0.this.n(i);
        }

        @Override // defpackage.x91
        public boolean d() {
            return this.a.d() || sn0.this.o();
        }
    }

    public sn0() {
        this.f0 = new a();
        this.g0 = new b();
        this.h0 = new c();
        this.i0 = 0;
        this.j0 = 0;
        this.k0 = true;
        this.l0 = true;
        this.m0 = -1;
        this.o0 = new d();
        this.t0 = false;
    }

    @Override // androidx.fragment.app.Fragment
    public x91 createFragmentContainer() {
        return new e(super.createFragmentContainer());
    }

    public void h() {
        j(false, false);
    }

    public void i() {
        j(true, false);
    }

    public final void j(boolean z, boolean z2) {
        if (this.r0) {
            return;
        }
        this.r0 = true;
        this.s0 = false;
        Dialog dialog = this.p0;
        if (dialog != null) {
            dialog.setOnDismissListener(null);
            this.p0.dismiss();
            if (!z2) {
                if (Looper.myLooper() == this.a.getLooper()) {
                    onDismiss(this.p0);
                } else {
                    this.a.post(this.f0);
                }
            }
        }
        this.q0 = true;
        if (this.m0 >= 0) {
            getParentFragmentManager().Z0(this.m0, 1);
            this.m0 = -1;
            return;
        }
        j n = getParentFragmentManager().n();
        n.r(this);
        if (z) {
            n.k();
        } else {
            n.j();
        }
    }

    public Dialog k() {
        return this.p0;
    }

    public int l() {
        return this.j0;
    }

    public Dialog m(Bundle bundle) {
        if (FragmentManager.H0(3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("onCreateDialog called for DialogFragment ");
            sb.append(this);
        }
        return new Dialog(requireContext(), l());
    }

    public View n(int i) {
        Dialog dialog = this.p0;
        if (dialog != null) {
            return dialog.findViewById(i);
        }
        return null;
    }

    public boolean o() {
        return this.t0;
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        getViewLifecycleOwnerLiveData().observeForever(this.o0);
        if (this.s0) {
            return;
        }
        this.r0 = false;
    }

    public void onCancel(DialogInterface dialogInterface) {
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = new Handler();
        this.l0 = this.mContainerId == 0;
        if (bundle != null) {
            this.i0 = bundle.getInt("android:style", 0);
            this.j0 = bundle.getInt("android:theme", 0);
            this.k0 = bundle.getBoolean("android:cancelable", true);
            this.l0 = bundle.getBoolean("android:showsDialog", this.l0);
            this.m0 = bundle.getInt("android:backStackId", -1);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        Dialog dialog = this.p0;
        if (dialog != null) {
            this.q0 = true;
            dialog.setOnDismissListener(null);
            this.p0.dismiss();
            if (!this.r0) {
                onDismiss(this.p0);
            }
            this.p0 = null;
            this.t0 = false;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        if (!this.s0 && !this.r0) {
            this.r0 = true;
        }
        getViewLifecycleOwnerLiveData().removeObserver(this.o0);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (this.q0) {
            return;
        }
        if (FragmentManager.H0(3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("onDismiss called for DialogFragment ");
            sb.append(this);
        }
        j(true, true);
    }

    @Override // androidx.fragment.app.Fragment
    public LayoutInflater onGetLayoutInflater(Bundle bundle) {
        LayoutInflater onGetLayoutInflater = super.onGetLayoutInflater(bundle);
        if (this.l0 && !this.n0) {
            p(bundle);
            if (FragmentManager.H0(2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("get layout inflater for DialogFragment ");
                sb.append(this);
                sb.append(" from dialog context");
            }
            Dialog dialog = this.p0;
            return dialog != null ? onGetLayoutInflater.cloneInContext(dialog.getContext()) : onGetLayoutInflater;
        }
        if (FragmentManager.H0(2)) {
            String str = "getting layout inflater for DialogFragment " + this;
            if (!this.l0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("mShowsDialog = false: ");
                sb2.append(str);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("mCreatingDialog = true: ");
                sb3.append(str);
            }
        }
        return onGetLayoutInflater;
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Dialog dialog = this.p0;
        if (dialog != null) {
            Bundle onSaveInstanceState = dialog.onSaveInstanceState();
            onSaveInstanceState.putBoolean("android:dialogShowing", false);
            bundle.putBundle("android:savedDialogState", onSaveInstanceState);
        }
        int i = this.i0;
        if (i != 0) {
            bundle.putInt("android:style", i);
        }
        int i2 = this.j0;
        if (i2 != 0) {
            bundle.putInt("android:theme", i2);
        }
        boolean z = this.k0;
        if (!z) {
            bundle.putBoolean("android:cancelable", z);
        }
        boolean z2 = this.l0;
        if (!z2) {
            bundle.putBoolean("android:showsDialog", z2);
        }
        int i3 = this.m0;
        if (i3 != -1) {
            bundle.putInt("android:backStackId", i3);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog dialog = this.p0;
        if (dialog != null) {
            this.q0 = false;
            dialog.show();
            View decorView = this.p0.getWindow().getDecorView();
            jk4.a(decorView, this);
            lk4.a(decorView, this);
            kk4.a(decorView, this);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        Dialog dialog = this.p0;
        if (dialog != null) {
            dialog.hide();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        Bundle bundle2;
        super.onViewStateRestored(bundle);
        if (this.p0 == null || bundle == null || (bundle2 = bundle.getBundle("android:savedDialogState")) == null) {
            return;
        }
        this.p0.onRestoreInstanceState(bundle2);
    }

    public final void p(Bundle bundle) {
        if (this.l0 && !this.t0) {
            try {
                this.n0 = true;
                Dialog m = m(bundle);
                this.p0 = m;
                if (this.l0) {
                    t(m, this.i0);
                    Context context = getContext();
                    if (context instanceof Activity) {
                        this.p0.setOwnerActivity((Activity) context);
                    }
                    this.p0.setCancelable(this.k0);
                    this.p0.setOnCancelListener(this.g0);
                    this.p0.setOnDismissListener(this.h0);
                    this.t0 = true;
                } else {
                    this.p0 = null;
                }
            } finally {
                this.n0 = false;
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void performCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Bundle bundle2;
        super.performCreateView(layoutInflater, viewGroup, bundle);
        if (this.mView != null || this.p0 == null || bundle == null || (bundle2 = bundle.getBundle("android:savedDialogState")) == null) {
            return;
        }
        this.p0.onRestoreInstanceState(bundle2);
    }

    public final Dialog q() {
        Dialog k = k();
        if (k != null) {
            return k;
        }
        throw new IllegalStateException("DialogFragment " + this + " does not have a Dialog.");
    }

    public void r(boolean z) {
        this.l0 = z;
    }

    public void s(int i, int i2) {
        if (FragmentManager.H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Setting style and theme for DialogFragment ");
            sb.append(this);
            sb.append(" to ");
            sb.append(i);
            sb.append(", ");
            sb.append(i2);
        }
        this.i0 = i;
        if (i == 2 || i == 3) {
            this.j0 = 16973913;
        }
        if (i2 != 0) {
            this.j0 = i2;
        }
    }

    public void t(Dialog dialog, int i) {
        if (i != 1 && i != 2) {
            if (i != 3) {
                return;
            }
            Window window = dialog.getWindow();
            if (window != null) {
                window.addFlags(24);
            }
        }
        dialog.requestWindowFeature(1);
    }

    public void u(FragmentManager fragmentManager, String str) {
        this.r0 = false;
        this.s0 = true;
        j n = fragmentManager.n();
        n.e(this, str);
        n.j();
    }

    public sn0(int i) {
        super(i);
        this.f0 = new a();
        this.g0 = new b();
        this.h0 = new c();
        this.i0 = 0;
        this.j0 = 0;
        this.k0 = true;
        this.l0 = true;
        this.m0 = -1;
        this.o0 = new d();
        this.t0 = false;
    }
}
