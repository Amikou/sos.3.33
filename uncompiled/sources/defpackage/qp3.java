package defpackage;

import android.view.View;
import com.google.android.material.button.MaterialButton;
import java.util.Objects;

/* compiled from: SimpleTextButtonNoInsetBinding.java */
/* renamed from: qp3  reason: default package */
/* loaded from: classes2.dex */
public final class qp3 {
    public final MaterialButton a;
    public final MaterialButton b;

    public qp3(MaterialButton materialButton, MaterialButton materialButton2) {
        this.a = materialButton;
        this.b = materialButton2;
    }

    public static qp3 a(View view) {
        Objects.requireNonNull(view, "rootView");
        MaterialButton materialButton = (MaterialButton) view;
        return new qp3(materialButton, materialButton);
    }

    public MaterialButton b() {
        return this.a;
    }
}
