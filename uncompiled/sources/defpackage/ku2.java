package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Locale;
import org.json.JSONObject;

/* compiled from: PreferenceLocaleStore.kt */
/* renamed from: ku2  reason: default package */
/* loaded from: classes2.dex */
public final class ku2 implements g12 {
    public final SharedPreferences a;
    public final Locale b;

    /* compiled from: PreferenceLocaleStore.kt */
    /* renamed from: ku2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public ku2(Context context, Locale locale, String str) {
        fs1.g(context, "context");
        fs1.g(locale, "defaultLocale");
        fs1.g(str, "preferenceName");
        this.b = locale;
        this.a = context.getSharedPreferences(str, 0);
    }

    @Override // defpackage.g12
    public boolean a() {
        return this.a.getBoolean("follow_system_locale_key", false);
    }

    @Override // defpackage.g12
    public void b(boolean z) {
        this.a.edit().putBoolean("follow_system_locale_key", z).apply();
    }

    @Override // defpackage.g12
    public void c(Locale locale) {
        fs1.g(locale, "locale");
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("language", locale.getLanguage());
        jSONObject.put("country", locale.getCountry());
        jSONObject.put("variant", locale.getVariant());
        this.a.edit().putString("language_key", jSONObject.toString()).apply();
    }

    @Override // defpackage.g12
    public Locale d() {
        String string = this.a.getString("language_key", null);
        if (!(string == null || dv3.w(string))) {
            String string2 = this.a.getString("language_key", null);
            if (string2 == null) {
                fs1.n();
            }
            JSONObject jSONObject = new JSONObject(string2);
            return new Locale(jSONObject.getString("language"), jSONObject.getString("country"), jSONObject.getString("variant"));
        }
        return this.b;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ ku2(android.content.Context r1, java.util.Locale r2, java.lang.String r3, int r4, defpackage.qi0 r5) {
        /*
            r0 = this;
            r5 = r4 & 2
            if (r5 == 0) goto Ld
            java.util.Locale r2 = java.util.Locale.getDefault()
            java.lang.String r5 = "Locale.getDefault()"
            defpackage.fs1.c(r2, r5)
        Ld:
            r4 = r4 & 4
            if (r4 == 0) goto L13
            java.lang.String r3 = "lingver_preference"
        L13:
            r0.<init>(r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ku2.<init>(android.content.Context, java.util.Locale, java.lang.String, int, qi0):void");
    }
}
