package defpackage;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: ApplicationVersionSignature.java */
/* renamed from: dh  reason: default package */
/* loaded from: classes.dex */
public final class dh {
    public static final ConcurrentMap<String, fx1> a = new ConcurrentHashMap();

    public static PackageInfo a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot resolve info for");
            sb.append(context.getPackageName());
            return null;
        }
    }

    public static String b(PackageInfo packageInfo) {
        if (packageInfo != null) {
            return String.valueOf(packageInfo.versionCode);
        }
        return UUID.randomUUID().toString();
    }

    public static fx1 c(Context context) {
        String packageName = context.getPackageName();
        ConcurrentMap<String, fx1> concurrentMap = a;
        fx1 fx1Var = concurrentMap.get(packageName);
        if (fx1Var == null) {
            fx1 d = d(context);
            fx1 putIfAbsent = concurrentMap.putIfAbsent(packageName, d);
            return putIfAbsent == null ? d : putIfAbsent;
        }
        return fx1Var;
    }

    public static fx1 d(Context context) {
        return new ll2(b(a(context)));
    }
}
