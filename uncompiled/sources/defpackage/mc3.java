package defpackage;

import android.os.Build;
import android.view.ScaleGestureDetector;

/* compiled from: ScaleGestureDetectorCompat.java */
/* renamed from: mc3  reason: default package */
/* loaded from: classes.dex */
public final class mc3 {
    public static void a(ScaleGestureDetector scaleGestureDetector, boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            scaleGestureDetector.setQuickScaleEnabled(z);
        }
    }
}
