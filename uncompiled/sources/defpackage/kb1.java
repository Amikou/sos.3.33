package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentTransferNftBinding.java */
/* renamed from: kb1  reason: default package */
/* loaded from: classes2.dex */
public final class kb1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final NestedScrollView c;
    public final MaterialTextView d;
    public final AppCompatImageView e;
    public final ProgressBar f;
    public final rp3 g;
    public final MaterialTextView h;
    public final MaterialTextView i;
    public final MaterialTextView j;
    public final MaterialTextView k;
    public final MaterialTextView l;

    public kb1(ConstraintLayout constraintLayout, MaterialButton materialButton, FrameLayout frameLayout, NestedScrollView nestedScrollView, MaterialTextView materialTextView, AppCompatImageView appCompatImageView, ProgressBar progressBar, rp3 rp3Var, CardView cardView, CardView cardView2, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5, MaterialTextView materialTextView6) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = nestedScrollView;
        this.d = materialTextView;
        this.e = appCompatImageView;
        this.f = progressBar;
        this.g = rp3Var;
        this.h = materialTextView2;
        this.i = materialTextView3;
        this.j = materialTextView4;
        this.k = materialTextView5;
        this.l = materialTextView6;
    }

    public static kb1 a(View view) {
        int i = R.id.btnSendNew;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnSendNew);
        if (materialButton != null) {
            i = R.id.btnSendParent;
            FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.btnSendParent);
            if (frameLayout != null) {
                i = R.id.nestedScrollView;
                NestedScrollView nestedScrollView = (NestedScrollView) ai4.a(view, R.id.nestedScrollView);
                if (nestedScrollView != null) {
                    i = R.id.networkFeeInfo;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.networkFeeInfo);
                    if (materialTextView != null) {
                        i = R.id.nftDetailImage;
                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.nftDetailImage);
                        if (appCompatImageView != null) {
                            i = R.id.progressBar;
                            ProgressBar progressBar = (ProgressBar) ai4.a(view, R.id.progressBar);
                            if (progressBar != null) {
                                i = R.id.toolbarWrapper;
                                View a = ai4.a(view, R.id.toolbarWrapper);
                                if (a != null) {
                                    rp3 a2 = rp3.a(a);
                                    i = R.id.transferAddress;
                                    CardView cardView = (CardView) ai4.a(view, R.id.transferAddress);
                                    if (cardView != null) {
                                        i = R.id.transferDetails;
                                        CardView cardView2 = (CardView) ai4.a(view, R.id.transferDetails);
                                        if (cardView2 != null) {
                                            i = R.id.txtCollectionName;
                                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtCollectionName);
                                            if (materialTextView2 != null) {
                                                i = R.id.txtMaxTotal;
                                                MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtMaxTotal);
                                                if (materialTextView3 != null) {
                                                    i = R.id.txtNetWorkFee;
                                                    MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtNetWorkFee);
                                                    if (materialTextView4 != null) {
                                                        i = R.id.txtWalletFrom;
                                                        MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txtWalletFrom);
                                                        if (materialTextView5 != null) {
                                                            i = R.id.txtWalletTo;
                                                            MaterialTextView materialTextView6 = (MaterialTextView) ai4.a(view, R.id.txtWalletTo);
                                                            if (materialTextView6 != null) {
                                                                return new kb1((ConstraintLayout) view, materialButton, frameLayout, nestedScrollView, materialTextView, appCompatImageView, progressBar, a2, cardView, cardView2, materialTextView2, materialTextView3, materialTextView4, materialTextView5, materialTextView6);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static kb1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.fragment_transfer_nft, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
