package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkerParameters;
import androidx.work.a;
import androidx.work.c;
import androidx.work.d;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import defpackage.v12;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/* compiled from: WorkManagerImpl.java */
/* renamed from: hq4  reason: default package */
/* loaded from: classes.dex */
public class hq4 extends gq4 {
    public static hq4 j;
    public static hq4 k;
    public static final Object l;
    public Context a;
    public a b;
    public WorkDatabase c;
    public q34 d;
    public List<cd3> e;
    public bv2 f;
    public lu2 g;
    public boolean h;
    public BroadcastReceiver.PendingResult i;

    static {
        v12.f("WorkManagerImpl");
        j = null;
        k = null;
        l = new Object();
    }

    public hq4(Context context, a aVar, q34 q34Var) {
        this(context, aVar, q34Var, context.getResources().getBoolean(ky2.workmanager_test_configuration));
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0016, code lost:
        r4 = r4.getApplicationContext();
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (defpackage.hq4.k != null) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x001e, code lost:
        defpackage.hq4.k = new defpackage.hq4(r4, r5, new defpackage.iq4(r5.l()));
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x002e, code lost:
        defpackage.hq4.j = defpackage.hq4.k;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void g(android.content.Context r4, androidx.work.a r5) {
        /*
            java.lang.Object r0 = defpackage.hq4.l
            monitor-enter(r0)
            hq4 r1 = defpackage.hq4.j     // Catch: java.lang.Throwable -> L34
            if (r1 == 0) goto L14
            hq4 r2 = defpackage.hq4.k     // Catch: java.lang.Throwable -> L34
            if (r2 != 0) goto Lc
            goto L14
        Lc:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException     // Catch: java.lang.Throwable -> L34
            java.lang.String r5 = "WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information."
            r4.<init>(r5)     // Catch: java.lang.Throwable -> L34
            throw r4     // Catch: java.lang.Throwable -> L34
        L14:
            if (r1 != 0) goto L32
            android.content.Context r4 = r4.getApplicationContext()     // Catch: java.lang.Throwable -> L34
            hq4 r1 = defpackage.hq4.k     // Catch: java.lang.Throwable -> L34
            if (r1 != 0) goto L2e
            hq4 r1 = new hq4     // Catch: java.lang.Throwable -> L34
            iq4 r2 = new iq4     // Catch: java.lang.Throwable -> L34
            java.util.concurrent.Executor r3 = r5.l()     // Catch: java.lang.Throwable -> L34
            r2.<init>(r3)     // Catch: java.lang.Throwable -> L34
            r1.<init>(r4, r5, r2)     // Catch: java.lang.Throwable -> L34
            defpackage.hq4.k = r1     // Catch: java.lang.Throwable -> L34
        L2e:
            hq4 r4 = defpackage.hq4.k     // Catch: java.lang.Throwable -> L34
            defpackage.hq4.j = r4     // Catch: java.lang.Throwable -> L34
        L32:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L34
            return
        L34:
            r4 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L34
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.hq4.g(android.content.Context, androidx.work.a):void");
    }

    @Deprecated
    public static hq4 l() {
        synchronized (l) {
            hq4 hq4Var = j;
            if (hq4Var != null) {
                return hq4Var;
            }
            return k;
        }
    }

    public static hq4 m(Context context) {
        hq4 l2;
        synchronized (l) {
            l2 = l();
            if (l2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof a.c) {
                    g(applicationContext, ((a.c) applicationContext).a());
                    l2 = m(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return l2;
    }

    @Override // defpackage.gq4
    public kn2 a(String str) {
        kv d = kv.d(str, this);
        this.d.b(d);
        return d.e();
    }

    @Override // defpackage.gq4
    public kn2 c(List<? extends d> list) {
        if (!list.isEmpty()) {
            return new yp4(this, list).a();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }

    @Override // defpackage.gq4
    public kn2 e(String str, ExistingWorkPolicy existingWorkPolicy, List<c> list) {
        return new yp4(this, str, existingWorkPolicy, list).a();
    }

    public kn2 h(UUID uuid) {
        kv b = kv.b(uuid, this);
        this.d.b(b);
        return b.e();
    }

    public List<cd3> i(Context context, a aVar, q34 q34Var) {
        return Arrays.asList(gd3.a(context, this), new ti1(context, aVar, q34Var, this));
    }

    public Context j() {
        return this.a;
    }

    public a k() {
        return this.b;
    }

    public lu2 n() {
        return this.g;
    }

    public bv2 o() {
        return this.f;
    }

    public List<cd3> p() {
        return this.e;
    }

    public WorkDatabase q() {
        return this.c;
    }

    public q34 r() {
        return this.d;
    }

    public final void s(Context context, a aVar, q34 q34Var, WorkDatabase workDatabase, List<cd3> list, bv2 bv2Var) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = aVar;
        this.d = q34Var;
        this.c = workDatabase;
        this.e = list;
        this.f = bv2Var;
        this.g = new lu2(workDatabase);
        this.h = false;
        if (Build.VERSION.SDK_INT >= 24 && applicationContext.isDeviceProtectedStorage()) {
            throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
        }
        this.d.b(new ForceStopRunnable(applicationContext, this));
    }

    public void t() {
        synchronized (l) {
            this.h = true;
            BroadcastReceiver.PendingResult pendingResult = this.i;
            if (pendingResult != null) {
                pendingResult.finish();
                this.i = null;
            }
        }
    }

    public void u() {
        if (Build.VERSION.SDK_INT >= 23) {
            y24.b(j());
        }
        q().P().t();
        gd3.b(k(), q(), p());
    }

    public void v(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                pendingResult.finish();
                this.i = null;
            }
        }
    }

    public void w(String str) {
        x(str, null);
    }

    public void x(String str, WorkerParameters.a aVar) {
        this.d.b(new ts3(this, str, aVar));
    }

    public void y(String str) {
        this.d.b(new ut3(this, str, true));
    }

    public void z(String str) {
        this.d.b(new ut3(this, str, false));
    }

    public hq4(Context context, a aVar, q34 q34Var, boolean z) {
        this(context, aVar, q34Var, WorkDatabase.G(context.getApplicationContext(), q34Var.c(), z));
    }

    public hq4(Context context, a aVar, q34 q34Var, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        v12.e(new v12.a(aVar.j()));
        List<cd3> i = i(applicationContext, aVar, q34Var);
        s(context, aVar, q34Var, workDatabase, i, new bv2(context, aVar, q34Var, workDatabase, i));
    }
}
