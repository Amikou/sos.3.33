package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.f;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ReadOnlyClassToSerializerMap.java */
/* renamed from: a43  reason: default package */
/* loaded from: classes.dex */
public final class a43 {
    public final a[] a;
    public final int b;

    /* compiled from: ReadOnlyClassToSerializerMap.java */
    /* renamed from: a43$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final f<Object> a;
        public final a b;
        public final Class<?> c;
        public final JavaType d;
        public final boolean e;

        public a(a aVar, rd4 rd4Var, f<Object> fVar) {
            this.b = aVar;
            this.a = fVar;
            this.e = rd4Var.c();
            this.c = rd4Var.a();
            this.d = rd4Var.b();
        }

        public boolean a(JavaType javaType) {
            return this.e && javaType.equals(this.d);
        }

        public boolean b(Class<?> cls) {
            return this.c == cls && this.e;
        }

        public boolean c(JavaType javaType) {
            return !this.e && javaType.equals(this.d);
        }

        public boolean d(Class<?> cls) {
            return this.c == cls && !this.e;
        }
    }

    public a43(Map<rd4, f<Object>> map) {
        int a2 = a(map.size());
        this.b = a2 - 1;
        a[] aVarArr = new a[a2];
        for (Map.Entry<rd4, f<Object>> entry : map.entrySet()) {
            rd4 key = entry.getKey();
            int hashCode = key.hashCode() & this.b;
            aVarArr[hashCode] = new a(aVarArr[hashCode], key, entry.getValue());
        }
        this.a = aVarArr;
    }

    public static final int a(int i) {
        int i2 = 8;
        while (i2 < (i <= 64 ? i + i : i + (i >> 2))) {
            i2 += i2;
        }
        return i2;
    }

    public static a43 b(HashMap<rd4, f<Object>> hashMap) {
        return new a43(hashMap);
    }

    public f<Object> c(JavaType javaType) {
        a aVar = this.a[rd4.d(javaType) & this.b];
        if (aVar == null) {
            return null;
        }
        if (aVar.a(javaType)) {
            return aVar.a;
        }
        do {
            aVar = aVar.b;
            if (aVar == null) {
                return null;
            }
        } while (!aVar.a(javaType));
        return aVar.a;
    }

    public f<Object> d(Class<?> cls) {
        a aVar = this.a[rd4.e(cls) & this.b];
        if (aVar == null) {
            return null;
        }
        if (aVar.b(cls)) {
            return aVar.a;
        }
        do {
            aVar = aVar.b;
            if (aVar == null) {
                return null;
            }
        } while (!aVar.b(cls));
        return aVar.a;
    }

    public f<Object> e(JavaType javaType) {
        a aVar = this.a[rd4.f(javaType) & this.b];
        if (aVar == null) {
            return null;
        }
        if (aVar.c(javaType)) {
            return aVar.a;
        }
        do {
            aVar = aVar.b;
            if (aVar == null) {
                return null;
            }
        } while (!aVar.c(javaType));
        return aVar.a;
    }

    public f<Object> f(Class<?> cls) {
        a aVar = this.a[rd4.g(cls) & this.b];
        if (aVar == null) {
            return null;
        }
        if (aVar.d(cls)) {
            return aVar.a;
        }
        do {
            aVar = aVar.b;
            if (aVar == null) {
                return null;
            }
        } while (!aVar.d(cls));
        return aVar.a;
    }
}
