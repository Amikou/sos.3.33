package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: LongSummaryStatistics.java */
/* renamed from: j22  reason: default package */
/* loaded from: classes2.dex */
public class j22 implements f22, mr1 {
    public long a;
    public long b;
    public long c = Long.MAX_VALUE;
    public long d = Long.MIN_VALUE;

    public final double a() {
        return b() > 0 ? e() / b() : Utils.DOUBLE_EPSILON;
    }

    public final long b() {
        return this.a;
    }

    public final long c() {
        return this.d;
    }

    public final long d() {
        return this.c;
    }

    public final long e() {
        return this.b;
    }

    public String toString() {
        return String.format("%s{count=%d, sum=%d, min=%d, average=%f, max=%d}", j22.class.getSimpleName(), Long.valueOf(b()), Long.valueOf(e()), Long.valueOf(d()), Double.valueOf(a()), Long.valueOf(c()));
    }
}
