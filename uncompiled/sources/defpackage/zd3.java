package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;

/* compiled from: SearchBarBinding.java */
/* renamed from: zd3  reason: default package */
/* loaded from: classes2.dex */
public final class zd3 {
    public final LinearLayout a;
    public final TextInputEditText b;

    public zd3(LinearLayout linearLayout, TextInputEditText textInputEditText, LinearLayout linearLayout2) {
        this.a = linearLayout;
        this.b = textInputEditText;
    }

    public static zd3 a(View view) {
        TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.etSearch);
        if (textInputEditText != null) {
            LinearLayout linearLayout = (LinearLayout) view;
            return new zd3(linearLayout, textInputEditText, linearLayout);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.etSearch)));
    }

    public LinearLayout b() {
        return this.a;
    }
}
