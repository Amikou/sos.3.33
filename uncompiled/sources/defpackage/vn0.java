package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogProgressLoadingBinding.java */
/* renamed from: vn0  reason: default package */
/* loaded from: classes2.dex */
public final class vn0 {
    public final MaterialButton a;
    public final MaterialTextView b;
    public final MaterialTextView c;
    public final ShapeableImageView d;

    public vn0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialTextView materialTextView, MaterialTextView materialTextView2, ShapeableImageView shapeableImageView) {
        this.a = materialButton;
        this.b = materialTextView;
        this.c = materialTextView2;
        this.d = shapeableImageView;
    }

    public static vn0 a(View view) {
        int i = R.id.dialog_cross;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialog_cross);
        if (materialButton != null) {
            i = R.id.dialog_msg;
            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_msg);
            if (materialTextView != null) {
                i = R.id.dialog_title;
                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.dialog_title);
                if (materialTextView2 != null) {
                    i = R.id.ivProgressLoading;
                    ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.ivProgressLoading);
                    if (shapeableImageView != null) {
                        return new vn0((ConstraintLayout) view, materialButton, materialTextView, materialTextView2, shapeableImageView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
