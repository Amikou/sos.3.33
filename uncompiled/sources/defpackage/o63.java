package defpackage;

import androidx.paging.RemoteMediator;

/* compiled from: RemoteMediatorAccessor.kt */
/* renamed from: o63  reason: default package */
/* loaded from: classes.dex */
public interface o63<Key, Value> extends p63<Key, Value> {
    Object b(q70<? super RemoteMediator.InitializeAction> q70Var);

    ws3<x02> getState();
}
