package defpackage;

import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbi;
import com.google.android.gms.internal.clearcut.zzbn;

/* renamed from: m85  reason: default package */
/* loaded from: classes.dex */
public final class m85 {
    public final zzbn a;
    public final byte[] b;

    public m85(int i) {
        byte[] bArr = new byte[i];
        this.b = bArr;
        this.a = zzbn.S(bArr);
    }

    public /* synthetic */ m85(int i, b85 b85Var) {
        this(i);
    }

    public final zzbb a() {
        if (this.a.u() == 0) {
            return new zzbi(this.b);
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }

    public final zzbn b() {
        return this.a;
    }
}
