package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: g06  reason: default package */
/* loaded from: classes.dex */
public final class g06 implements f06 {
    public static final wo5<Boolean> a;
    public static final wo5<Boolean> b;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.frontend.directly_maybe_log_error_events", false);
        b = ro5Var.b("measurement.upload.directly_maybe_log_error_events", true);
        ro5Var.a("measurement.id.frontend.directly_maybe_log_error_events", 0L);
    }

    @Override // defpackage.f06
    public final boolean zza() {
        return a.e().booleanValue();
    }

    @Override // defpackage.f06
    public final boolean zzb() {
        return b.e().booleanValue();
    }
}
