package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.ImportPrivateKeyActivity;

/* renamed from: kp1  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class kp1 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ kp1 a = new kp1();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        ImportPrivateKeyActivity.m0(dialogInterface);
    }
}
