package defpackage;

import java.lang.annotation.Annotation;
import java.util.List;

/* compiled from: KAnnotatedElement.kt */
/* renamed from: ow1  reason: default package */
/* loaded from: classes2.dex */
public interface ow1 {
    List<Annotation> getAnnotations();
}
