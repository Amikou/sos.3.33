package defpackage;

import defpackage.pt0;

/* renamed from: zg3  reason: default package */
/* loaded from: classes2.dex */
public class zg3 extends pt0.b {
    public zg3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public zg3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public zg3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        ct0 ct0Var = this.b;
        if (ct0Var.i()) {
            return i.v();
        }
        ct0 ct0Var2 = this.c;
        ct0 ct0Var3 = this.d[0];
        boolean h = ct0Var3.h();
        ct0 j = h ? ct0Var2 : ct0Var2.j(ct0Var3);
        ct0 o = h ? ct0Var3 : ct0Var3.o();
        ct0 a = ct0Var2.o().a(j).a(o);
        if (a.i()) {
            return new zg3(i, a, i.p().n(), this.e);
        }
        ct0 o2 = a.o();
        ct0 j2 = h ? a : a.j(o);
        if (!h) {
            ct0Var = ct0Var.j(ct0Var3);
        }
        return new zg3(i, o2, ct0Var.p(a, j).a(o2).a(j2), new ct0[]{j2}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return J();
        }
        xs0 i = i();
        ct0 ct0Var = this.b;
        if (ct0Var.i()) {
            return pt0Var;
        }
        ct0 n = pt0Var.n();
        ct0 s = pt0Var.s(0);
        if (n.i() || !s.h()) {
            return J().a(pt0Var);
        }
        ct0 ct0Var2 = this.c;
        ct0 ct0Var3 = this.d[0];
        ct0 o = pt0Var.o();
        ct0 o2 = ct0Var.o();
        ct0 o3 = ct0Var2.o();
        ct0 o4 = ct0Var3.o();
        ct0 a = o4.a(o3).a(ct0Var2.j(ct0Var3));
        ct0 l = o.j(o4).a(o3).l(a, o2, o4);
        ct0 j = n.j(o4);
        ct0 o5 = j.a(a).o();
        if (o5.i()) {
            return l.i() ? pt0Var.J() : i.v();
        } else if (l.i()) {
            return new zg3(i, l, i.p().n(), this.e);
        } else {
            ct0 j2 = l.o().j(j);
            ct0 j3 = l.j(o5).j(o4);
            return new zg3(i, j2, l.a(o5).o().l(a, o.b(), j3), new ct0[]{j3}, this.e);
        }
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        ct0 ct0Var;
        ct0 ct0Var2;
        ct0 ct0Var3;
        ct0 ct0Var4;
        ct0 ct0Var5;
        ct0 ct0Var6;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        xs0 i = i();
        ct0 ct0Var7 = this.b;
        ct0 n = pt0Var.n();
        if (ct0Var7.i()) {
            return n.i() ? i.v() : pt0Var.a(this);
        }
        ct0 ct0Var8 = this.c;
        ct0 ct0Var9 = this.d[0];
        ct0 o = pt0Var.o();
        ct0 s = pt0Var.s(0);
        boolean h = ct0Var9.h();
        if (h) {
            ct0Var = n;
            ct0Var2 = o;
        } else {
            ct0Var = n.j(ct0Var9);
            ct0Var2 = o.j(ct0Var9);
        }
        boolean h2 = s.h();
        if (h2) {
            ct0Var3 = ct0Var8;
        } else {
            ct0Var7 = ct0Var7.j(s);
            ct0Var3 = ct0Var8.j(s);
        }
        ct0 a = ct0Var3.a(ct0Var2);
        ct0 a2 = ct0Var7.a(ct0Var);
        if (a2.i()) {
            return a.i() ? J() : i.v();
        }
        if (n.i()) {
            pt0 A = A();
            ct0 q = A.q();
            ct0 r = A.r();
            ct0 d = r.a(o).d(q);
            ct0Var4 = d.o().a(d).a(q).b();
            if (ct0Var4.i()) {
                return new zg3(i, ct0Var4, i.p().n(), this.e);
            }
            ct0 a3 = d.j(q.a(ct0Var4)).a(ct0Var4).a(r).d(ct0Var4).a(ct0Var4);
            ct0Var6 = i.n(ws0.b);
            ct0Var5 = a3;
        } else {
            ct0 o2 = a2.o();
            ct0 j = a.j(ct0Var7);
            ct0 j2 = a.j(ct0Var);
            ct0 j3 = j.j(j2);
            if (j3.i()) {
                return new zg3(i, j3, i.p().n(), this.e);
            }
            ct0 j4 = a.j(o2);
            ct0 j5 = !h2 ? j4.j(s) : j4;
            ct0 p = j2.a(o2).p(j5, ct0Var8.a(ct0Var9));
            if (!h) {
                j5 = j5.j(ct0Var9);
            }
            ct0Var4 = j3;
            ct0Var5 = p;
            ct0Var6 = j5;
        }
        return new zg3(i, ct0Var4, ct0Var5, new ct0[]{ct0Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new zg3(null, f(), g());
    }

    @Override // defpackage.pt0
    public boolean h() {
        ct0 n = n();
        return (n.i() || o().s() == n.s()) ? false : true;
    }

    @Override // defpackage.pt0
    public ct0 r() {
        ct0 ct0Var = this.b;
        ct0 ct0Var2 = this.c;
        if (u() || ct0Var.i()) {
            return ct0Var2;
        }
        ct0 j = ct0Var2.a(ct0Var).j(ct0Var);
        ct0 ct0Var3 = this.d[0];
        return !ct0Var3.h() ? j.d(ct0Var3) : j;
    }

    @Override // defpackage.pt0
    public pt0 z() {
        if (u()) {
            return this;
        }
        ct0 ct0Var = this.b;
        if (ct0Var.i()) {
            return this;
        }
        ct0 ct0Var2 = this.c;
        ct0 ct0Var3 = this.d[0];
        return new zg3(this.a, ct0Var, ct0Var2.a(ct0Var3), new ct0[]{ct0Var3}, this.e);
    }
}
