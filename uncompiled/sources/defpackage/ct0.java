package defpackage;

import java.math.BigInteger;
import java.util.Random;

/* renamed from: ct0  reason: default package */
/* loaded from: classes2.dex */
public abstract class ct0 implements ws0 {

    /* renamed from: ct0$a */
    /* loaded from: classes2.dex */
    public static abstract class a extends ct0 {
        public int u() {
            int f = f();
            ct0 ct0Var = this;
            ct0 ct0Var2 = ct0Var;
            for (int i = 1; i < f; i++) {
                ct0Var2 = ct0Var2.o();
                ct0Var = ct0Var.a(ct0Var2);
            }
            if (ct0Var.i()) {
                return 0;
            }
            if (ct0Var.h()) {
                return 1;
            }
            throw new IllegalStateException("Internal error in trace calculation");
        }
    }

    /* renamed from: ct0$b */
    /* loaded from: classes2.dex */
    public static abstract class b extends ct0 {
    }

    /* renamed from: ct0$c */
    /* loaded from: classes2.dex */
    public static class c extends a {
        public int f;
        public int g;
        public int[] h;
        public d22 i;

        public c(int i, int i2, int i3, int i4, BigInteger bigInteger) {
            if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > i) {
                throw new IllegalArgumentException("x value invalid in F2m field element");
            }
            if (i3 == 0 && i4 == 0) {
                this.f = 2;
                this.h = new int[]{i2};
            } else if (i3 >= i4) {
                throw new IllegalArgumentException("k2 must be smaller than k3");
            } else {
                if (i3 <= 0) {
                    throw new IllegalArgumentException("k2 must be larger than 0");
                }
                this.f = 3;
                this.h = new int[]{i2, i3, i4};
            }
            this.g = i;
            this.i = new d22(bigInteger);
        }

        public c(int i, int[] iArr, d22 d22Var) {
            this.g = i;
            this.f = iArr.length == 1 ? 2 : 3;
            this.h = iArr;
            this.i = d22Var;
        }

        public static void v(ct0 ct0Var, ct0 ct0Var2) {
            if (!(ct0Var instanceof c) || !(ct0Var2 instanceof c)) {
                throw new IllegalArgumentException("Field elements are not both instances of ECFieldElement.F2m");
            }
            c cVar = (c) ct0Var;
            c cVar2 = (c) ct0Var2;
            if (cVar.f != cVar2.f) {
                throw new IllegalArgumentException("One of the F2m field elements has incorrect representation");
            }
            if (cVar.g != cVar2.g || !wh.c(cVar.h, cVar2.h)) {
                throw new IllegalArgumentException("Field elements are not elements of the same field F2m");
            }
        }

        @Override // defpackage.ct0
        public ct0 a(ct0 ct0Var) {
            d22 d22Var = (d22) this.i.clone();
            d22Var.g(((c) ct0Var).i, 0);
            return new c(this.g, this.h, d22Var);
        }

        @Override // defpackage.ct0
        public ct0 b() {
            return new c(this.g, this.h, this.i.e());
        }

        @Override // defpackage.ct0
        public int c() {
            return this.i.o();
        }

        @Override // defpackage.ct0
        public ct0 d(ct0 ct0Var) {
            return j(ct0Var.g());
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                return this.g == cVar.g && this.f == cVar.f && wh.c(this.h, cVar.h) && this.i.equals(cVar.i);
            }
            return false;
        }

        @Override // defpackage.ct0
        public int f() {
            return this.g;
        }

        @Override // defpackage.ct0
        public ct0 g() {
            int i = this.g;
            int[] iArr = this.h;
            return new c(i, iArr, this.i.B(i, iArr));
        }

        @Override // defpackage.ct0
        public boolean h() {
            return this.i.y();
        }

        public int hashCode() {
            return (this.i.hashCode() ^ this.g) ^ wh.t(this.h);
        }

        @Override // defpackage.ct0
        public boolean i() {
            return this.i.A();
        }

        @Override // defpackage.ct0
        public ct0 j(ct0 ct0Var) {
            int i = this.g;
            int[] iArr = this.h;
            return new c(i, iArr, this.i.C(((c) ct0Var).i, i, iArr));
        }

        @Override // defpackage.ct0
        public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
            return l(ct0Var, ct0Var2, ct0Var3);
        }

        @Override // defpackage.ct0
        public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
            d22 d22Var = this.i;
            d22 d22Var2 = ((c) ct0Var).i;
            d22 d22Var3 = ((c) ct0Var2).i;
            d22 d22Var4 = ((c) ct0Var3).i;
            d22 G = d22Var.G(d22Var2, this.g, this.h);
            d22 G2 = d22Var3.G(d22Var4, this.g, this.h);
            if (G == d22Var || G == d22Var2) {
                G = (d22) G.clone();
            }
            G.g(G2, 0);
            G.I(this.g, this.h);
            return new c(this.g, this.h, G);
        }

        @Override // defpackage.ct0
        public ct0 m() {
            return this;
        }

        @Override // defpackage.ct0
        public ct0 n() {
            return (this.i.A() || this.i.y()) ? this : q(this.g - 1);
        }

        @Override // defpackage.ct0
        public ct0 o() {
            int i = this.g;
            int[] iArr = this.h;
            return new c(i, iArr, this.i.D(i, iArr));
        }

        @Override // defpackage.ct0
        public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
            d22 d22Var = this.i;
            d22 d22Var2 = ((c) ct0Var).i;
            d22 d22Var3 = ((c) ct0Var2).i;
            d22 S = d22Var.S(this.g, this.h);
            d22 G = d22Var2.G(d22Var3, this.g, this.h);
            if (S == d22Var) {
                S = (d22) S.clone();
            }
            S.g(G, 0);
            S.I(this.g, this.h);
            return new c(this.g, this.h, S);
        }

        @Override // defpackage.ct0
        public ct0 q(int i) {
            if (i < 1) {
                return this;
            }
            int i2 = this.g;
            int[] iArr = this.h;
            return new c(i2, iArr, this.i.E(i, i2, iArr));
        }

        @Override // defpackage.ct0
        public ct0 r(ct0 ct0Var) {
            return a(ct0Var);
        }

        @Override // defpackage.ct0
        public boolean s() {
            return this.i.V();
        }

        @Override // defpackage.ct0
        public BigInteger t() {
            return this.i.W();
        }
    }

    /* renamed from: ct0$d */
    /* loaded from: classes2.dex */
    public static class d extends b {
        public BigInteger f;
        public BigInteger g;
        public BigInteger h;

        public d(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
            if (bigInteger3 == null || bigInteger3.signum() < 0 || bigInteger3.compareTo(bigInteger) >= 0) {
                throw new IllegalArgumentException("x value invalid in Fp field element");
            }
            this.f = bigInteger;
            this.g = bigInteger2;
            this.h = bigInteger3;
        }

        public static BigInteger u(BigInteger bigInteger) {
            int bitLength = bigInteger.bitLength();
            if (bitLength < 96 || bigInteger.shiftRight(bitLength - 64).longValue() != -1) {
                return null;
            }
            return ws0.b.shiftLeft(bitLength).subtract(bigInteger);
        }

        public BigInteger A(BigInteger bigInteger) {
            int f = f();
            int i = (f + 31) >> 5;
            int[] o = kd2.o(f, this.f);
            int[] o2 = kd2.o(f, bigInteger);
            int[] j = kd2.j(i);
            g92.d(o, o2, j);
            return kd2.P(i, j);
        }

        public BigInteger B(BigInteger bigInteger, BigInteger bigInteger2) {
            return C(bigInteger.multiply(bigInteger2));
        }

        public BigInteger C(BigInteger bigInteger) {
            if (this.g != null) {
                boolean z = bigInteger.signum() < 0;
                if (z) {
                    bigInteger = bigInteger.abs();
                }
                int bitLength = this.f.bitLength();
                boolean equals = this.g.equals(ws0.b);
                while (bigInteger.bitLength() > bitLength + 1) {
                    BigInteger shiftRight = bigInteger.shiftRight(bitLength);
                    BigInteger subtract = bigInteger.subtract(shiftRight.shiftLeft(bitLength));
                    if (!equals) {
                        shiftRight = shiftRight.multiply(this.g);
                    }
                    bigInteger = shiftRight.add(subtract);
                }
                while (bigInteger.compareTo(this.f) >= 0) {
                    bigInteger = bigInteger.subtract(this.f);
                }
                return (!z || bigInteger.signum() == 0) ? bigInteger : this.f.subtract(bigInteger);
            }
            return bigInteger.mod(this.f);
        }

        public BigInteger D(BigInteger bigInteger, BigInteger bigInteger2) {
            BigInteger subtract = bigInteger.subtract(bigInteger2);
            return subtract.signum() < 0 ? subtract.add(this.f) : subtract;
        }

        @Override // defpackage.ct0
        public ct0 a(ct0 ct0Var) {
            return new d(this.f, this.g, x(this.h, ct0Var.t()));
        }

        @Override // defpackage.ct0
        public ct0 b() {
            BigInteger add = this.h.add(ws0.b);
            if (add.compareTo(this.f) == 0) {
                add = ws0.a;
            }
            return new d(this.f, this.g, add);
        }

        @Override // defpackage.ct0
        public ct0 d(ct0 ct0Var) {
            return new d(this.f, this.g, B(this.h, A(ct0Var.t())));
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof d) {
                d dVar = (d) obj;
                return this.f.equals(dVar.f) && this.h.equals(dVar.h);
            }
            return false;
        }

        @Override // defpackage.ct0
        public int f() {
            return this.f.bitLength();
        }

        @Override // defpackage.ct0
        public ct0 g() {
            return new d(this.f, this.g, A(this.h));
        }

        public int hashCode() {
            return this.f.hashCode() ^ this.h.hashCode();
        }

        @Override // defpackage.ct0
        public ct0 j(ct0 ct0Var) {
            return new d(this.f, this.g, B(this.h, ct0Var.t()));
        }

        @Override // defpackage.ct0
        public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
            BigInteger bigInteger = this.h;
            BigInteger t = ct0Var.t();
            BigInteger t2 = ct0Var2.t();
            BigInteger t3 = ct0Var3.t();
            return new d(this.f, this.g, C(bigInteger.multiply(t).subtract(t2.multiply(t3))));
        }

        @Override // defpackage.ct0
        public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
            BigInteger bigInteger = this.h;
            BigInteger t = ct0Var.t();
            BigInteger t2 = ct0Var2.t();
            BigInteger t3 = ct0Var3.t();
            return new d(this.f, this.g, C(bigInteger.multiply(t).add(t2.multiply(t3))));
        }

        @Override // defpackage.ct0
        public ct0 m() {
            if (this.h.signum() == 0) {
                return this;
            }
            BigInteger bigInteger = this.f;
            return new d(bigInteger, this.g, bigInteger.subtract(this.h));
        }

        @Override // defpackage.ct0
        public ct0 n() {
            if (i() || h()) {
                return this;
            }
            if (!this.f.testBit(0)) {
                throw new RuntimeException("not done yet");
            }
            if (this.f.testBit(1)) {
                BigInteger add = this.f.shiftRight(2).add(ws0.b);
                BigInteger bigInteger = this.f;
                return v(new d(bigInteger, this.g, this.h.modPow(add, bigInteger)));
            } else if (this.f.testBit(2)) {
                BigInteger modPow = this.h.modPow(this.f.shiftRight(3), this.f);
                BigInteger B = B(modPow, this.h);
                if (B(B, modPow).equals(ws0.b)) {
                    return v(new d(this.f, this.g, B));
                }
                return v(new d(this.f, this.g, B(B, ws0.c.modPow(this.f.shiftRight(2), this.f))));
            } else {
                BigInteger shiftRight = this.f.shiftRight(1);
                BigInteger modPow2 = this.h.modPow(shiftRight, this.f);
                BigInteger bigInteger2 = ws0.b;
                if (!modPow2.equals(bigInteger2)) {
                    return null;
                }
                BigInteger bigInteger3 = this.h;
                BigInteger y = y(y(bigInteger3));
                BigInteger add2 = shiftRight.add(bigInteger2);
                BigInteger subtract = this.f.subtract(bigInteger2);
                Random random = new Random();
                while (true) {
                    BigInteger bigInteger4 = new BigInteger(this.f.bitLength(), random);
                    if (bigInteger4.compareTo(this.f) < 0 && C(bigInteger4.multiply(bigInteger4).subtract(y)).modPow(shiftRight, this.f).equals(subtract)) {
                        BigInteger[] w = w(bigInteger4, bigInteger3, add2);
                        BigInteger bigInteger5 = w[0];
                        BigInteger bigInteger6 = w[1];
                        if (B(bigInteger6, bigInteger6).equals(y)) {
                            return new d(this.f, this.g, z(bigInteger6));
                        }
                        if (!bigInteger5.equals(ws0.b) && !bigInteger5.equals(subtract)) {
                            return null;
                        }
                    }
                }
            }
        }

        @Override // defpackage.ct0
        public ct0 o() {
            BigInteger bigInteger = this.f;
            BigInteger bigInteger2 = this.g;
            BigInteger bigInteger3 = this.h;
            return new d(bigInteger, bigInteger2, B(bigInteger3, bigInteger3));
        }

        @Override // defpackage.ct0
        public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
            BigInteger bigInteger = this.h;
            BigInteger t = ct0Var.t();
            BigInteger t2 = ct0Var2.t();
            return new d(this.f, this.g, C(bigInteger.multiply(bigInteger).add(t.multiply(t2))));
        }

        @Override // defpackage.ct0
        public ct0 r(ct0 ct0Var) {
            return new d(this.f, this.g, D(this.h, ct0Var.t()));
        }

        @Override // defpackage.ct0
        public BigInteger t() {
            return this.h;
        }

        public final ct0 v(ct0 ct0Var) {
            if (ct0Var.o().equals(this)) {
                return ct0Var;
            }
            return null;
        }

        public final BigInteger[] w(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
            int bitLength = bigInteger3.bitLength();
            int lowestSetBit = bigInteger3.getLowestSetBit();
            BigInteger bigInteger4 = ws0.b;
            BigInteger bigInteger5 = bigInteger;
            BigInteger bigInteger6 = bigInteger4;
            BigInteger bigInteger7 = ws0.c;
            BigInteger bigInteger8 = bigInteger6;
            for (int i = bitLength - 1; i >= lowestSetBit + 1; i--) {
                bigInteger4 = B(bigInteger4, bigInteger8);
                if (bigInteger3.testBit(i)) {
                    bigInteger8 = B(bigInteger4, bigInteger2);
                    bigInteger6 = B(bigInteger6, bigInteger5);
                    bigInteger7 = C(bigInteger5.multiply(bigInteger7).subtract(bigInteger.multiply(bigInteger4)));
                    bigInteger5 = C(bigInteger5.multiply(bigInteger5).subtract(bigInteger8.shiftLeft(1)));
                } else {
                    BigInteger C = C(bigInteger6.multiply(bigInteger7).subtract(bigInteger4));
                    BigInteger C2 = C(bigInteger5.multiply(bigInteger7).subtract(bigInteger.multiply(bigInteger4)));
                    bigInteger7 = C(bigInteger7.multiply(bigInteger7).subtract(bigInteger4.shiftLeft(1)));
                    bigInteger5 = C2;
                    bigInteger6 = C;
                    bigInteger8 = bigInteger4;
                }
            }
            BigInteger B = B(bigInteger4, bigInteger8);
            BigInteger B2 = B(B, bigInteger2);
            BigInteger C3 = C(bigInteger6.multiply(bigInteger7).subtract(B));
            BigInteger C4 = C(bigInteger5.multiply(bigInteger7).subtract(bigInteger.multiply(B)));
            BigInteger B3 = B(B, B2);
            for (int i2 = 1; i2 <= lowestSetBit; i2++) {
                C3 = B(C3, C4);
                C4 = C(C4.multiply(C4).subtract(B3.shiftLeft(1)));
                B3 = B(B3, B3);
            }
            return new BigInteger[]{C3, C4};
        }

        public BigInteger x(BigInteger bigInteger, BigInteger bigInteger2) {
            BigInteger add = bigInteger.add(bigInteger2);
            return add.compareTo(this.f) >= 0 ? add.subtract(this.f) : add;
        }

        public BigInteger y(BigInteger bigInteger) {
            BigInteger shiftLeft = bigInteger.shiftLeft(1);
            return shiftLeft.compareTo(this.f) >= 0 ? shiftLeft.subtract(this.f) : shiftLeft;
        }

        public BigInteger z(BigInteger bigInteger) {
            if (bigInteger.testBit(0)) {
                bigInteger = this.f.subtract(bigInteger);
            }
            return bigInteger.shiftRight(1);
        }
    }

    public abstract ct0 a(ct0 ct0Var);

    public abstract ct0 b();

    public int c() {
        return t().bitLength();
    }

    public abstract ct0 d(ct0 ct0Var);

    public byte[] e() {
        return ip.a((f() + 7) / 8, t());
    }

    public abstract int f();

    public abstract ct0 g();

    public boolean h() {
        return c() == 1;
    }

    public boolean i() {
        return t().signum() == 0;
    }

    public abstract ct0 j(ct0 ct0Var);

    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return j(ct0Var).r(ct0Var2.j(ct0Var3));
    }

    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return j(ct0Var).a(ct0Var2.j(ct0Var3));
    }

    public abstract ct0 m();

    public abstract ct0 n();

    public abstract ct0 o();

    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        return o().a(ct0Var.j(ct0Var2));
    }

    public ct0 q(int i) {
        ct0 ct0Var = this;
        for (int i2 = 0; i2 < i; i2++) {
            ct0Var = ct0Var.o();
        }
        return ct0Var;
    }

    public abstract ct0 r(ct0 ct0Var);

    public boolean s() {
        return t().testBit(0);
    }

    public abstract BigInteger t();

    public String toString() {
        return t().toString(16);
    }
}
