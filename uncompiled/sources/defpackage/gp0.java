package defpackage;

import defpackage.yo0;
import java.io.File;

/* compiled from: DiskLruCacheFactory.java */
/* renamed from: gp0  reason: default package */
/* loaded from: classes.dex */
public class gp0 implements yo0.a {
    public final long a;
    public final a b;

    /* compiled from: DiskLruCacheFactory.java */
    /* renamed from: gp0$a */
    /* loaded from: classes.dex */
    public interface a {
        File a();
    }

    public gp0(a aVar, long j) {
        this.a = j;
        this.b = aVar;
    }

    @Override // defpackage.yo0.a
    public yo0 build() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.isDirectory() || a2.mkdirs()) {
            return hp0.c(a2, this.a);
        }
        return null;
    }
}
