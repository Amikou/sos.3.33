package defpackage;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: HttpDataSource.java */
/* renamed from: gl1  reason: default package */
/* loaded from: classes.dex */
public final class gl1 {
    public final Map<String, String> a = new HashMap();
    public Map<String, String> b;

    public synchronized Map<String, String> a() {
        if (this.b == null) {
            this.b = Collections.unmodifiableMap(new HashMap(this.a));
        }
        return this.b;
    }
}
