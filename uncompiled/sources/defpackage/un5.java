package defpackage;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: un5  reason: default package */
/* loaded from: classes.dex */
public final class un5 implements Runnable {
    public final /* synthetic */ AtomicReference a;
    public final /* synthetic */ dp5 f0;

    public un5(dp5 dp5Var, AtomicReference atomicReference) {
        this.f0 = dp5Var;
        this.a = atomicReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        synchronized (this.a) {
            this.a.set(Boolean.valueOf(this.f0.a.z().v(this.f0.a.c().n(), qf5.K)));
            this.a.notify();
        }
    }
}
