package defpackage;

/* compiled from: Pool.java */
/* renamed from: us2  reason: default package */
/* loaded from: classes.dex */
public interface us2<V> extends d83<V>, q72 {
    @Override // defpackage.d83
    void a(V v);

    V get(int i);
}
