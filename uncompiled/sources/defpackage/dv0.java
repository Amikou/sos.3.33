package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: EncodedProbeProducer.java */
/* renamed from: dv0  reason: default package */
/* loaded from: classes.dex */
public class dv0 implements dv2<zu0> {
    public final xr a;
    public final xr b;
    public final xt c;
    public final dv2<zu0> d;
    public final gr<wt> e;
    public final gr<wt> f;

    /* compiled from: EncodedProbeProducer.java */
    /* renamed from: dv0$a */
    /* loaded from: classes.dex */
    public static class a extends bm0<zu0, zu0> {
        public final ev2 c;
        public final xr d;
        public final xr e;
        public final xt f;
        public final gr<wt> g;
        public final gr<wt> h;

        public a(l60<zu0> l60Var, ev2 ev2Var, xr xrVar, xr xrVar2, xt xtVar, gr<wt> grVar, gr<wt> grVar2) {
            super(l60Var);
            this.c = ev2Var;
            this.d = xrVar;
            this.e = xrVar2;
            this.f = xtVar;
            this.g = grVar;
            this.h = grVar2;
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(zu0 zu0Var, int i) {
            boolean d;
            try {
                if (nc1.d()) {
                    nc1.a("EncodedProbeProducer#onNewResultImpl");
                }
                if (!qm.f(i) && zu0Var != null && !qm.m(i, 10) && zu0Var.l() != wn1.b) {
                    ImageRequest c = this.c.c();
                    wt d2 = this.f.d(c, this.c.a());
                    this.g.a(d2);
                    if ("memory_encoded".equals(this.c.i("origin"))) {
                        if (!this.h.b(d2)) {
                            (c.d() == ImageRequest.CacheChoice.SMALL ? this.e : this.d).h(d2);
                            this.h.a(d2);
                        }
                    } else if ("disk".equals(this.c.i("origin"))) {
                        this.h.a(d2);
                    }
                    p().d(zu0Var, i);
                    if (d) {
                        return;
                    }
                    return;
                }
                p().d(zu0Var, i);
                if (nc1.d()) {
                    nc1.b();
                }
            } finally {
                if (nc1.d()) {
                    nc1.b();
                }
            }
        }
    }

    public dv0(xr xrVar, xr xrVar2, xt xtVar, gr grVar, gr grVar2, dv2<zu0> dv2Var) {
        this.a = xrVar;
        this.b = xrVar2;
        this.c = xtVar;
        this.e = grVar;
        this.f = grVar2;
        this.d = dv2Var;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        try {
            if (nc1.d()) {
                nc1.a("EncodedProbeProducer#produceResults");
            }
            iv2 l = ev2Var.l();
            l.k(ev2Var, b());
            a aVar = new a(l60Var, ev2Var, this.a, this.b, this.c, this.e, this.f);
            l.a(ev2Var, "EncodedProbeProducer", null);
            if (nc1.d()) {
                nc1.a("mInputProducer.produceResult");
            }
            this.d.a(aVar, ev2Var);
            if (nc1.d()) {
                nc1.b();
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public String b() {
        return "EncodedProbeProducer";
    }
}
