package defpackage;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializer;
import com.fasterxml.jackson.databind.ser.a;
import java.util.List;

/* compiled from: BeanSerializerBuilder.java */
/* renamed from: wo  reason: default package */
/* loaded from: classes.dex */
public class wo {
    public static final BeanPropertyWriter[] i = new BeanPropertyWriter[0];
    public final so a;
    public SerializationConfig b;
    public List<BeanPropertyWriter> c;
    public BeanPropertyWriter[] d;
    public a e;
    public Object f;
    public AnnotatedMember g;
    public kl2 h;

    public wo(so soVar) {
        this.a = soVar;
    }

    public f<?> a() {
        BeanPropertyWriter[] beanPropertyWriterArr;
        List<BeanPropertyWriter> list = this.c;
        if (list != null && !list.isEmpty()) {
            List<BeanPropertyWriter> list2 = this.c;
            beanPropertyWriterArr = (BeanPropertyWriter[]) list2.toArray(new BeanPropertyWriter[list2.size()]);
            if (this.b.isEnabled(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
                for (BeanPropertyWriter beanPropertyWriter : beanPropertyWriterArr) {
                    beanPropertyWriter.fixAccess(this.b);
                }
            }
        } else if (this.e == null && this.h == null) {
            return null;
        } else {
            beanPropertyWriterArr = i;
        }
        a aVar = this.e;
        if (aVar != null) {
            aVar.a(this.b);
        }
        if (this.g != null && this.b.isEnabled(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            this.g.fixAccess(this.b.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
        return new BeanSerializer(this.a.y(), this, beanPropertyWriterArr, this.d);
    }

    public BeanSerializer b() {
        return BeanSerializer.createDummy(this.a.y());
    }

    public a c() {
        return this.e;
    }

    public so d() {
        return this.a;
    }

    public Object e() {
        return this.f;
    }

    public kl2 f() {
        return this.h;
    }

    public List<BeanPropertyWriter> g() {
        return this.c;
    }

    public AnnotatedMember h() {
        return this.g;
    }

    public void i(a aVar) {
        this.e = aVar;
    }

    public void j(SerializationConfig serializationConfig) {
        this.b = serializationConfig;
    }

    public void k(Object obj) {
        this.f = obj;
    }

    public void l(BeanPropertyWriter[] beanPropertyWriterArr) {
        this.d = beanPropertyWriterArr;
    }

    public void m(kl2 kl2Var) {
        this.h = kl2Var;
    }

    public void n(List<BeanPropertyWriter> list) {
        this.c = list;
    }

    public void o(AnnotatedMember annotatedMember) {
        if (this.g == null) {
            this.g = annotatedMember;
            return;
        }
        throw new IllegalArgumentException("Multiple type ids specified with " + this.g + " and " + annotatedMember);
    }
}
