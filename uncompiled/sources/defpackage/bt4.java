package defpackage;

import android.app.PendingIntent;
import android.content.Intent;
import java.util.List;

/* renamed from: bt4  reason: default package */
/* loaded from: classes2.dex */
public final class bt4 extends tr3 {
    public final int a;
    public final int b;
    public final int c;
    public final long d;
    public final long e;
    public final List<String> f;
    public final List<String> g;
    public final PendingIntent h;
    public final List<Intent> i;

    public bt4(int i, int i2, int i3, long j, long j2, List<String> list, List<String> list2, PendingIntent pendingIntent, List<Intent> list3) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = j;
        this.e = j2;
        this.f = list;
        this.g = list2;
        this.h = pendingIntent;
        this.i = list3;
    }

    @Override // defpackage.tr3
    public final List<String> a() {
        return this.f;
    }

    @Override // defpackage.tr3
    public final List<String> b() {
        return this.g;
    }

    @Override // defpackage.tr3
    public final long c() {
        return this.d;
    }

    @Override // defpackage.tr3
    public final List<Intent> d() {
        return this.i;
    }

    public final boolean equals(Object obj) {
        List<String> list;
        List<String> list2;
        PendingIntent pendingIntent;
        if (obj == this) {
            return true;
        }
        if (obj instanceof tr3) {
            tr3 tr3Var = (tr3) obj;
            if (this.a == tr3Var.h() && this.b == tr3Var.i() && this.c == tr3Var.f() && this.d == tr3Var.c() && this.e == tr3Var.j() && ((list = this.f) != null ? list.equals(tr3Var.a()) : tr3Var.a() == null) && ((list2 = this.g) != null ? list2.equals(tr3Var.b()) : tr3Var.b() == null) && ((pendingIntent = this.h) != null ? pendingIntent.equals(tr3Var.g()) : tr3Var.g() == null)) {
                List<Intent> list3 = this.i;
                List<Intent> d = tr3Var.d();
                if (list3 != null ? list3.equals(d) : d == null) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // defpackage.tr3
    public final int f() {
        return this.c;
    }

    @Override // defpackage.tr3
    @Deprecated
    public final PendingIntent g() {
        return this.h;
    }

    @Override // defpackage.tr3
    public final int h() {
        return this.a;
    }

    public final int hashCode() {
        int i = this.a;
        int i2 = this.b;
        int i3 = this.c;
        long j = this.d;
        long j2 = this.e;
        int i4 = (((((((((i ^ 1000003) * 1000003) ^ i2) * 1000003) ^ i3) * 1000003) ^ ((int) ((j >>> 32) ^ j))) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003;
        List<String> list = this.f;
        int hashCode = (i4 ^ (list == null ? 0 : list.hashCode())) * 1000003;
        List<String> list2 = this.g;
        int hashCode2 = (hashCode ^ (list2 == null ? 0 : list2.hashCode())) * 1000003;
        PendingIntent pendingIntent = this.h;
        int hashCode3 = (hashCode2 ^ (pendingIntent == null ? 0 : pendingIntent.hashCode())) * 1000003;
        List<Intent> list3 = this.i;
        return hashCode3 ^ (list3 != null ? list3.hashCode() : 0);
    }

    @Override // defpackage.tr3
    public final int i() {
        return this.b;
    }

    @Override // defpackage.tr3
    public final long j() {
        return this.e;
    }

    public final String toString() {
        int i = this.a;
        int i2 = this.b;
        int i3 = this.c;
        long j = this.d;
        long j2 = this.e;
        String valueOf = String.valueOf(this.f);
        String valueOf2 = String.valueOf(this.g);
        String valueOf3 = String.valueOf(this.h);
        String valueOf4 = String.valueOf(this.i);
        int length = valueOf.length();
        int length2 = valueOf2.length();
        StringBuilder sb = new StringBuilder(length + 251 + length2 + valueOf3.length() + valueOf4.length());
        sb.append("SplitInstallSessionState{sessionId=");
        sb.append(i);
        sb.append(", status=");
        sb.append(i2);
        sb.append(", errorCode=");
        sb.append(i3);
        sb.append(", bytesDownloaded=");
        sb.append(j);
        sb.append(", totalBytesToDownload=");
        sb.append(j2);
        sb.append(", moduleNamesNullable=");
        sb.append(valueOf);
        sb.append(", languagesNullable=");
        sb.append(valueOf2);
        sb.append(", resolutionIntent=");
        sb.append(valueOf3);
        sb.append(", splitFileIntents=");
        sb.append(valueOf4);
        sb.append("}");
        return sb.toString();
    }
}
