package defpackage;

import android.graphics.Bitmap;
import java.util.Set;

/* compiled from: DummyTrackingInUseBitmapPool.java */
/* renamed from: ls0  reason: default package */
/* loaded from: classes.dex */
public class ls0 implements iq {
    public final Set<Bitmap> a = rm3.b();

    @Override // defpackage.us2
    /* renamed from: f */
    public Bitmap get(int i) {
        Bitmap createBitmap = Bitmap.createBitmap(1, (int) Math.ceil(i / 2.0d), Bitmap.Config.RGB_565);
        this.a.add(createBitmap);
        return createBitmap;
    }

    @Override // defpackage.us2, defpackage.d83
    /* renamed from: g */
    public void a(Bitmap bitmap) {
        xt2.g(bitmap);
        this.a.remove(bitmap);
        bitmap.recycle();
    }
}
