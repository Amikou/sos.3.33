package defpackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: StringUtils.java */
/* renamed from: ru3  reason: default package */
/* loaded from: classes2.dex */
public class ru3 {
    public static Map<Character, String> a;
    public static final String b;

    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put('\'', "\\'");
        a.put('\"', "\\\"");
        a.put('\\', "\\\\");
        a.put('/', "\\/");
        a.put('\b', "\\b");
        a.put('\n', "\\n");
        a.put('\t', "\\t");
        a.put('\f', "\\f");
        a.put('\r', "\\r");
        b = System.getProperty("line.separator");
    }

    public static List<String> a(String str) {
        if (b(str)) {
            String[] split = str.split(",");
            ArrayList arrayList = new ArrayList();
            for (String str2 : split) {
                if (b(str2)) {
                    arrayList.add(str2);
                }
            }
            return l10.l(arrayList);
        }
        return l10.l(new ArrayList(0));
    }

    public static boolean b(String str) {
        return str != null && str.trim().length() > 0;
    }

    public static boolean c(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return false;
        }
        for (String str : strArr) {
            if (d(str)) {
                return false;
            }
        }
        return true;
    }

    public static boolean d(String str) {
        return !b(str);
    }

    public static boolean e(String str) {
        if (d(str)) {
            return false;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String f(List<String> list) {
        if (list != null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (b(list.get(i))) {
                    sb.append(list.get(i));
                    if (i < list.size() - 1) {
                        sb.append(",");
                    }
                }
            }
            return sb.toString();
        }
        return null;
    }

    public static String g(String... strArr) {
        return f(strArr == null ? null : Arrays.asList(strArr));
    }

    public static String h(List<? extends Number> list) {
        ArrayList arrayList;
        if (list != null) {
            arrayList = new ArrayList();
            for (Number number : list) {
                if (number != null) {
                    arrayList.add(number.toString());
                }
            }
        } else {
            arrayList = null;
        }
        return f(arrayList);
    }
}
