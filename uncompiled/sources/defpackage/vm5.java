package defpackage;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.zzbl;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: vm5  reason: default package */
/* loaded from: classes.dex */
public final class vm5 {
    public static void a(String str, int i, List<z55> list) {
        if (list.size() != i) {
            throw new IllegalArgumentException(String.format("%s operation requires %s parameters found %s", str, Integer.valueOf(i), Integer.valueOf(list.size())));
        }
    }

    public static void b(String str, int i, List<z55> list) {
        if (list.size() < i) {
            throw new IllegalArgumentException(String.format("%s operation requires at least %s parameters found %s", str, Integer.valueOf(i), Integer.valueOf(list.size())));
        }
    }

    public static void c(String str, int i, List<z55> list) {
        if (list.size() > i) {
            throw new IllegalArgumentException(String.format("%s operation requires at most %s parameters found %s", str, Integer.valueOf(i), Integer.valueOf(list.size())));
        }
    }

    public static boolean d(z55 z55Var) {
        if (z55Var == null) {
            return false;
        }
        Double b = z55Var.b();
        return !b.isNaN() && b.doubleValue() >= Utils.DOUBLE_EPSILON && b.equals(Double.valueOf(Math.floor(b.doubleValue())));
    }

    public static zzbl e(String str) {
        zzbl zzblVar = null;
        if (str != null && !str.isEmpty()) {
            zzblVar = zzbl.zza(Integer.parseInt(str));
        }
        if (zzblVar != null) {
            return zzblVar;
        }
        throw new IllegalArgumentException(String.format("Unsupported commandId %s", str));
    }

    public static boolean f(z55 z55Var, z55 z55Var2) {
        if (z55Var.getClass().equals(z55Var2.getClass())) {
            if ((z55Var instanceof i65) || (z55Var instanceof t55)) {
                return true;
            }
            if (z55Var instanceof z45) {
                if (Double.isNaN(z55Var.b().doubleValue()) || Double.isNaN(z55Var2.b().doubleValue())) {
                    return false;
                }
                return z55Var.b().equals(z55Var2.b());
            } else if (z55Var instanceof f65) {
                return z55Var.zzc().equals(z55Var2.zzc());
            } else {
                if (z55Var instanceof s45) {
                    return z55Var.c().equals(z55Var2.c());
                }
                return z55Var == z55Var2;
            }
        }
        return false;
    }

    public static int g(double d) {
        int i;
        if (Double.isNaN(d) || Double.isInfinite(d) || d == Utils.DOUBLE_EPSILON) {
            return 0;
        }
        return (int) (((i > 0 ? 1 : -1) * Math.floor(Math.abs(d))) % 4.294967296E9d);
    }

    public static long h(double d) {
        return g(d) & 4294967295L;
    }

    public static double i(double d) {
        int i;
        if (Double.isNaN(d)) {
            return Utils.DOUBLE_EPSILON;
        }
        if (Double.isInfinite(d) || d == Utils.DOUBLE_EPSILON || i == 0) {
            return d;
        }
        return (i > 0 ? 1 : -1) * Math.floor(Math.abs(d));
    }

    public static Object j(z55 z55Var) {
        if (z55.Y.equals(z55Var)) {
            return null;
        }
        if (z55.X.equals(z55Var)) {
            return "";
        }
        if (!z55Var.b().isNaN()) {
            return z55Var.b();
        }
        return z55Var.zzc();
    }

    public static int k(wk5 wk5Var) {
        int g = g(wk5Var.h("runtime.counter").b().doubleValue() + 1.0d);
        if (g <= 1000000) {
            wk5Var.e("runtime.counter", new z45(Double.valueOf(g)));
            return g;
        }
        throw new IllegalStateException("Instructions allowed exceeded");
    }
}
