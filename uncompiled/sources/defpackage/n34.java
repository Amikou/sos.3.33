package defpackage;

import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.e;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: n34  reason: default package */
/* loaded from: classes.dex */
public class n34<TResult> {
    public final e<TResult> a = new e<>();

    public c<TResult> a() {
        return this.a;
    }

    public void b(Exception exc) {
        this.a.s(exc);
    }

    public void c(TResult tresult) {
        this.a.t(tresult);
    }

    public boolean d(Exception exc) {
        return this.a.w(exc);
    }

    public boolean e(TResult tresult) {
        return this.a.x(tresult);
    }
}
