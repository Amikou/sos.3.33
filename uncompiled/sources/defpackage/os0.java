package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.SparseArray;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: DvbParser.java */
/* renamed from: os0  reason: default package */
/* loaded from: classes.dex */
public final class os0 {
    public static final byte[] h = {0, 7, 8, 15};
    public static final byte[] i = {0, 119, -120, -1};
    public static final byte[] j = {0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1};
    public final Paint a;
    public final Paint b;
    public final Canvas c;
    public final b d;
    public final a e;
    public final h f;
    public Bitmap g;

    /* compiled from: DvbParser.java */
    /* renamed from: os0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final int[] b;
        public final int[] c;
        public final int[] d;

        public a(int i, int[] iArr, int[] iArr2, int[] iArr3) {
            this.a = i;
            this.b = iArr;
            this.c = iArr2;
            this.d = iArr3;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;

        public b(int i, int i2, int i3, int i4, int i5, int i6) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.e = i5;
            this.f = i6;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final int a;
        public final boolean b;
        public final byte[] c;
        public final byte[] d;

        public c(int i, boolean z, byte[] bArr, byte[] bArr2) {
            this.a = i;
            this.b = z;
            this.c = bArr;
            this.d = bArr2;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$d */
    /* loaded from: classes.dex */
    public static final class d {
        public final int a;
        public final int b;
        public final SparseArray<e> c;

        public d(int i, int i2, int i3, SparseArray<e> sparseArray) {
            this.a = i2;
            this.b = i3;
            this.c = sparseArray;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$e */
    /* loaded from: classes.dex */
    public static final class e {
        public final int a;
        public final int b;

        public e(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$f */
    /* loaded from: classes.dex */
    public static final class f {
        public final int a;
        public final boolean b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final int i;
        public final SparseArray<g> j;

        public f(int i, boolean z, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, SparseArray<g> sparseArray) {
            this.a = i;
            this.b = z;
            this.c = i2;
            this.d = i3;
            this.e = i5;
            this.f = i6;
            this.g = i7;
            this.h = i8;
            this.i = i9;
            this.j = sparseArray;
        }

        public void a(f fVar) {
            SparseArray<g> sparseArray = fVar.j;
            for (int i = 0; i < sparseArray.size(); i++) {
                this.j.put(sparseArray.keyAt(i), sparseArray.valueAt(i));
            }
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$g */
    /* loaded from: classes.dex */
    public static final class g {
        public final int a;
        public final int b;

        public g(int i, int i2, int i3, int i4, int i5, int i6) {
            this.a = i3;
            this.b = i4;
        }
    }

    /* compiled from: DvbParser.java */
    /* renamed from: os0$h */
    /* loaded from: classes.dex */
    public static final class h {
        public final int a;
        public final int b;
        public final SparseArray<f> c = new SparseArray<>();
        public final SparseArray<a> d = new SparseArray<>();
        public final SparseArray<c> e = new SparseArray<>();
        public final SparseArray<a> f = new SparseArray<>();
        public final SparseArray<c> g = new SparseArray<>();
        public b h;
        public d i;

        public h(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public void a() {
            this.c.clear();
            this.d.clear();
            this.e.clear();
            this.f.clear();
            this.g.clear();
            this.h = null;
            this.i = null;
        }
    }

    public os0(int i2, int i3) {
        Paint paint = new Paint();
        this.a = paint;
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        paint.setPathEffect(null);
        Paint paint2 = new Paint();
        this.b = paint2;
        paint2.setStyle(Paint.Style.FILL);
        paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        paint2.setPathEffect(null);
        this.c = new Canvas();
        this.d = new b(719, 575, 0, 719, 0, 575);
        this.e = new a(0, c(), d(), e());
        this.f = new h(i2, i3);
    }

    public static byte[] a(int i2, int i3, np2 np2Var) {
        byte[] bArr = new byte[i2];
        for (int i4 = 0; i4 < i2; i4++) {
            bArr[i4] = (byte) np2Var.h(i3);
        }
        return bArr;
    }

    public static int[] c() {
        return new int[]{0, -1, -16777216, -8421505};
    }

    public static int[] d() {
        int[] iArr = new int[16];
        iArr[0] = 0;
        for (int i2 = 1; i2 < 16; i2++) {
            if (i2 < 8) {
                iArr[i2] = f(255, (i2 & 1) != 0 ? 255 : 0, (i2 & 2) != 0 ? 255 : 0, (i2 & 4) != 0 ? 255 : 0);
            } else {
                iArr[i2] = f(255, (i2 & 1) != 0 ? 127 : 0, (i2 & 2) != 0 ? 127 : 0, (i2 & 4) == 0 ? 0 : 127);
            }
        }
        return iArr;
    }

    public static int[] e() {
        int[] iArr = new int[256];
        iArr[0] = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            if (i2 < 8) {
                iArr[i2] = f(63, (i2 & 1) != 0 ? 255 : 0, (i2 & 2) != 0 ? 255 : 0, (i2 & 4) == 0 ? 0 : 255);
            } else {
                int i3 = i2 & 136;
                if (i3 == 0) {
                    iArr[i2] = f(255, ((i2 & 1) != 0 ? 85 : 0) + ((i2 & 16) != 0 ? 170 : 0), ((i2 & 2) != 0 ? 85 : 0) + ((i2 & 32) != 0 ? 170 : 0), ((i2 & 4) == 0 ? 0 : 85) + ((i2 & 64) == 0 ? 0 : 170));
                } else if (i3 == 8) {
                    iArr[i2] = f(127, ((i2 & 1) != 0 ? 85 : 0) + ((i2 & 16) != 0 ? 170 : 0), ((i2 & 2) != 0 ? 85 : 0) + ((i2 & 32) != 0 ? 170 : 0), ((i2 & 4) == 0 ? 0 : 85) + ((i2 & 64) == 0 ? 0 : 170));
                } else if (i3 == 128) {
                    iArr[i2] = f(255, ((i2 & 1) != 0 ? 43 : 0) + 127 + ((i2 & 16) != 0 ? 85 : 0), ((i2 & 2) != 0 ? 43 : 0) + 127 + ((i2 & 32) != 0 ? 85 : 0), ((i2 & 4) == 0 ? 0 : 43) + 127 + ((i2 & 64) == 0 ? 0 : 85));
                } else if (i3 == 136) {
                    iArr[i2] = f(255, ((i2 & 1) != 0 ? 43 : 0) + ((i2 & 16) != 0 ? 85 : 0), ((i2 & 2) != 0 ? 43 : 0) + ((i2 & 32) != 0 ? 85 : 0), ((i2 & 4) == 0 ? 0 : 43) + ((i2 & 64) == 0 ? 0 : 85));
                }
            }
        }
        return iArr;
    }

    public static int f(int i2, int i3, int i4, int i5) {
        return (i2 << 24) | (i3 << 16) | (i4 << 8) | i5;
    }

    public static int g(np2 np2Var, int[] iArr, byte[] bArr, int i2, int i3, Paint paint, Canvas canvas) {
        boolean z;
        int i4;
        int h2;
        int h3;
        int i5 = i2;
        boolean z2 = false;
        while (true) {
            byte h4 = np2Var.h(2);
            if (h4 != 0) {
                z = z2;
                i4 = 1;
            } else {
                if (np2Var.g()) {
                    h2 = np2Var.h(3) + 3;
                    h3 = np2Var.h(2);
                } else {
                    if (np2Var.g()) {
                        z = z2;
                        i4 = 1;
                    } else {
                        int h5 = np2Var.h(2);
                        if (h5 == 0) {
                            z = true;
                        } else if (h5 == 1) {
                            z = z2;
                            i4 = 2;
                        } else if (h5 == 2) {
                            h2 = np2Var.h(4) + 12;
                            h3 = np2Var.h(2);
                        } else if (h5 != 3) {
                            z = z2;
                        } else {
                            h2 = np2Var.h(8) + 29;
                            h3 = np2Var.h(2);
                        }
                        h4 = 0;
                        i4 = 0;
                    }
                    h4 = 0;
                }
                z = z2;
                i4 = h2;
                h4 = h3;
            }
            if (i4 != 0 && paint != null) {
                if (bArr != null) {
                    h4 = bArr[h4];
                }
                paint.setColor(iArr[h4]);
                canvas.drawRect(i5, i3, i5 + i4, i3 + 1, paint);
            }
            i5 += i4;
            if (z) {
                return i5;
            }
            z2 = z;
        }
    }

    public static int h(np2 np2Var, int[] iArr, byte[] bArr, int i2, int i3, Paint paint, Canvas canvas) {
        boolean z;
        int i4;
        int h2;
        int i5 = i2;
        boolean z2 = false;
        while (true) {
            byte h3 = np2Var.h(4);
            int i6 = 2;
            if (h3 != 0) {
                z = z2;
                i4 = 1;
            } else if (!np2Var.g()) {
                int h4 = np2Var.h(3);
                if (h4 != 0) {
                    i6 = h4 + 2;
                    z = z2;
                    i4 = i6;
                    h3 = 0;
                } else {
                    z = true;
                    h3 = 0;
                    i4 = 0;
                }
            } else {
                if (!np2Var.g()) {
                    h2 = np2Var.h(2) + 4;
                    h3 = np2Var.h(4);
                } else {
                    int h5 = np2Var.h(2);
                    if (h5 != 0) {
                        if (h5 != 1) {
                            if (h5 == 2) {
                                h2 = np2Var.h(4) + 9;
                                h3 = np2Var.h(4);
                            } else if (h5 != 3) {
                                z = z2;
                                h3 = 0;
                                i4 = 0;
                            } else {
                                h2 = np2Var.h(8) + 25;
                                h3 = np2Var.h(4);
                            }
                        }
                        z = z2;
                        i4 = i6;
                        h3 = 0;
                    } else {
                        z = z2;
                        i4 = 1;
                        h3 = 0;
                    }
                }
                z = z2;
                i4 = h2;
            }
            if (i4 != 0 && paint != null) {
                if (bArr != null) {
                    h3 = bArr[h3];
                }
                paint.setColor(iArr[h3]);
                canvas.drawRect(i5, i3, i5 + i4, i3 + 1, paint);
            }
            i5 += i4;
            if (z) {
                return i5;
            }
            z2 = z;
        }
    }

    public static int i(np2 np2Var, int[] iArr, byte[] bArr, int i2, int i3, Paint paint, Canvas canvas) {
        boolean z;
        int h2;
        int i4 = i2;
        boolean z2 = false;
        while (true) {
            byte h3 = np2Var.h(8);
            if (h3 != 0) {
                z = z2;
                h2 = 1;
            } else if (!np2Var.g()) {
                int h4 = np2Var.h(7);
                if (h4 != 0) {
                    z = z2;
                    h2 = h4;
                    h3 = 0;
                } else {
                    z = true;
                    h3 = 0;
                    h2 = 0;
                }
            } else {
                z = z2;
                h2 = np2Var.h(7);
                h3 = np2Var.h(8);
            }
            if (h2 != 0 && paint != null) {
                if (bArr != null) {
                    h3 = bArr[h3];
                }
                paint.setColor(iArr[h3]);
                canvas.drawRect(i4, i3, i4 + h2, i3 + 1, paint);
            }
            i4 += h2;
            if (z) {
                return i4;
            }
            z2 = z;
        }
    }

    public static void j(byte[] bArr, int[] iArr, int i2, int i3, int i4, Paint paint, Canvas canvas) {
        byte[] bArr2;
        byte[] bArr3;
        byte[] bArr4;
        np2 np2Var = new np2(bArr);
        int i5 = i3;
        int i6 = i4;
        byte[] bArr5 = null;
        byte[] bArr6 = null;
        byte[] bArr7 = null;
        while (np2Var.b() != 0) {
            int h2 = np2Var.h(8);
            if (h2 != 240) {
                switch (h2) {
                    case 16:
                        if (i2 == 3) {
                            bArr3 = bArr5 == null ? i : bArr5;
                        } else if (i2 == 2) {
                            bArr3 = bArr7 == null ? h : bArr7;
                        } else {
                            bArr2 = null;
                            i5 = g(np2Var, iArr, bArr2, i5, i6, paint, canvas);
                            np2Var.c();
                            continue;
                        }
                        bArr2 = bArr3;
                        i5 = g(np2Var, iArr, bArr2, i5, i6, paint, canvas);
                        np2Var.c();
                        continue;
                    case 17:
                        if (i2 == 3) {
                            bArr4 = bArr6 == null ? j : bArr6;
                        } else {
                            bArr4 = null;
                        }
                        i5 = h(np2Var, iArr, bArr4, i5, i6, paint, canvas);
                        np2Var.c();
                        continue;
                    case 18:
                        i5 = i(np2Var, iArr, null, i5, i6, paint, canvas);
                        continue;
                    default:
                        switch (h2) {
                            case 32:
                                bArr7 = a(4, 4, np2Var);
                                continue;
                            case 33:
                                bArr5 = a(4, 8, np2Var);
                                continue;
                            case 34:
                                bArr6 = a(16, 8, np2Var);
                                continue;
                            default:
                                continue;
                        }
                }
            } else {
                i6 += 2;
                i5 = i3;
            }
        }
    }

    public static void k(c cVar, a aVar, int i2, int i3, int i4, Paint paint, Canvas canvas) {
        int[] iArr;
        if (i2 == 3) {
            iArr = aVar.d;
        } else if (i2 == 2) {
            iArr = aVar.c;
        } else {
            iArr = aVar.b;
        }
        int[] iArr2 = iArr;
        j(cVar.c, iArr2, i2, i3, i4, paint, canvas);
        j(cVar.d, iArr2, i2, i3, i4 + 1, paint, canvas);
    }

    public static a l(np2 np2Var, int i2) {
        int h2;
        int i3;
        int h3;
        int h4;
        int i4;
        int i5 = 8;
        int h5 = np2Var.h(8);
        np2Var.r(8);
        int i6 = 2;
        int i7 = i2 - 2;
        int[] c2 = c();
        int[] d2 = d();
        int[] e2 = e();
        while (i7 > 0) {
            int h6 = np2Var.h(i5);
            int h7 = np2Var.h(i5);
            int i8 = i7 - 2;
            int[] iArr = (h7 & 128) != 0 ? c2 : (h7 & 64) != 0 ? d2 : e2;
            if ((h7 & 1) != 0) {
                h4 = np2Var.h(i5);
                i4 = np2Var.h(i5);
                h2 = np2Var.h(i5);
                h3 = np2Var.h(i5);
                i3 = i8 - 4;
            } else {
                int h8 = np2Var.h(4) << 4;
                h2 = np2Var.h(4) << 4;
                i3 = i8 - 2;
                h3 = np2Var.h(i6) << 6;
                h4 = np2Var.h(6) << i6;
                i4 = h8;
            }
            if (h4 == 0) {
                h3 = 255;
                i4 = 0;
                h2 = 0;
            }
            double d3 = h4;
            double d4 = i4 - 128;
            double d5 = h2 - 128;
            iArr[h6] = f((byte) (255 - (h3 & 255)), androidx.media3.common.util.b.q((int) (d3 + (1.402d * d4)), 0, 255), androidx.media3.common.util.b.q((int) ((d3 - (0.34414d * d5)) - (d4 * 0.71414d)), 0, 255), androidx.media3.common.util.b.q((int) (d3 + (d5 * 1.772d)), 0, 255));
            i7 = i3;
            h5 = h5;
            i5 = 8;
            i6 = 2;
        }
        return new a(h5, c2, d2, e2);
    }

    public static b m(np2 np2Var) {
        int i2;
        int i3;
        int i4;
        int i5;
        np2Var.r(4);
        boolean g2 = np2Var.g();
        np2Var.r(3);
        int h2 = np2Var.h(16);
        int h3 = np2Var.h(16);
        if (g2) {
            int h4 = np2Var.h(16);
            int h5 = np2Var.h(16);
            int h6 = np2Var.h(16);
            i5 = np2Var.h(16);
            i4 = h5;
            i3 = h6;
            i2 = h4;
        } else {
            i2 = 0;
            i3 = 0;
            i4 = h2;
            i5 = h3;
        }
        return new b(h2, h3, i2, i4, i3, i5);
    }

    public static c n(np2 np2Var) {
        byte[] bArr;
        int h2 = np2Var.h(16);
        np2Var.r(4);
        int h3 = np2Var.h(2);
        boolean g2 = np2Var.g();
        np2Var.r(1);
        byte[] bArr2 = androidx.media3.common.util.b.f;
        if (h3 == 1) {
            np2Var.r(np2Var.h(8) * 16);
        } else if (h3 == 0) {
            int h4 = np2Var.h(16);
            int h5 = np2Var.h(16);
            if (h4 > 0) {
                bArr2 = new byte[h4];
                np2Var.k(bArr2, 0, h4);
            }
            if (h5 > 0) {
                bArr = new byte[h5];
                np2Var.k(bArr, 0, h5);
                return new c(h2, g2, bArr2, bArr);
            }
        }
        bArr = bArr2;
        return new c(h2, g2, bArr2, bArr);
    }

    public static d o(np2 np2Var, int i2) {
        int h2 = np2Var.h(8);
        int h3 = np2Var.h(4);
        int h4 = np2Var.h(2);
        np2Var.r(2);
        int i3 = i2 - 2;
        SparseArray sparseArray = new SparseArray();
        while (i3 > 0) {
            int h5 = np2Var.h(8);
            np2Var.r(8);
            i3 -= 6;
            sparseArray.put(h5, new e(np2Var.h(16), np2Var.h(16)));
        }
        return new d(h2, h3, h4, sparseArray);
    }

    public static f p(np2 np2Var, int i2) {
        int h2;
        int h3;
        int h4 = np2Var.h(8);
        np2Var.r(4);
        boolean g2 = np2Var.g();
        np2Var.r(3);
        int i3 = 16;
        int h5 = np2Var.h(16);
        int h6 = np2Var.h(16);
        int h7 = np2Var.h(3);
        int h8 = np2Var.h(3);
        int i4 = 2;
        np2Var.r(2);
        int h9 = np2Var.h(8);
        int h10 = np2Var.h(8);
        int h11 = np2Var.h(4);
        int h12 = np2Var.h(2);
        np2Var.r(2);
        int i5 = i2 - 10;
        SparseArray sparseArray = new SparseArray();
        while (i5 > 0) {
            int h13 = np2Var.h(i3);
            int h14 = np2Var.h(i4);
            int h15 = np2Var.h(i4);
            int h16 = np2Var.h(12);
            int i6 = h12;
            np2Var.r(4);
            int h17 = np2Var.h(12);
            i5 -= 6;
            if (h14 == 1 || h14 == 2) {
                i5 -= 2;
                h2 = np2Var.h(8);
                h3 = np2Var.h(8);
            } else {
                h2 = 0;
                h3 = 0;
            }
            sparseArray.put(h13, new g(h14, h15, h16, h17, h2, h3));
            h12 = i6;
            i4 = 2;
            i3 = 16;
        }
        return new f(h4, g2, h5, h6, h7, h8, h9, h10, h11, h12, sparseArray);
    }

    public static void q(np2 np2Var, h hVar) {
        f fVar;
        int h2 = np2Var.h(8);
        int h3 = np2Var.h(16);
        int h4 = np2Var.h(16);
        int d2 = np2Var.d() + h4;
        if (h4 * 8 > np2Var.b()) {
            p12.i("DvbParser", "Data field length exceeds limit");
            np2Var.r(np2Var.b());
            return;
        }
        switch (h2) {
            case 16:
                if (h3 == hVar.a) {
                    d dVar = hVar.i;
                    d o = o(np2Var, h4);
                    if (o.b != 0) {
                        hVar.i = o;
                        hVar.c.clear();
                        hVar.d.clear();
                        hVar.e.clear();
                        break;
                    } else if (dVar != null && dVar.a != o.a) {
                        hVar.i = o;
                        break;
                    }
                }
                break;
            case 17:
                d dVar2 = hVar.i;
                if (h3 == hVar.a && dVar2 != null) {
                    f p = p(np2Var, h4);
                    if (dVar2.b == 0 && (fVar = hVar.c.get(p.a)) != null) {
                        p.a(fVar);
                    }
                    hVar.c.put(p.a, p);
                    break;
                }
                break;
            case 18:
                if (h3 == hVar.a) {
                    a l = l(np2Var, h4);
                    hVar.d.put(l.a, l);
                    break;
                } else if (h3 == hVar.b) {
                    a l2 = l(np2Var, h4);
                    hVar.f.put(l2.a, l2);
                    break;
                }
                break;
            case 19:
                if (h3 == hVar.a) {
                    c n = n(np2Var);
                    hVar.e.put(n.a, n);
                    break;
                } else if (h3 == hVar.b) {
                    c n2 = n(np2Var);
                    hVar.g.put(n2.a, n2);
                    break;
                }
                break;
            case 20:
                if (h3 == hVar.a) {
                    hVar.h = m(np2Var);
                    break;
                }
                break;
        }
        np2Var.s(d2 - np2Var.d());
    }

    public List<kb0> b(byte[] bArr, int i2) {
        int i3;
        int i4;
        SparseArray<g> sparseArray;
        np2 np2Var = new np2(bArr, i2);
        while (np2Var.b() >= 48 && np2Var.h(8) == 15) {
            q(np2Var, this.f);
        }
        h hVar = this.f;
        d dVar = hVar.i;
        if (dVar == null) {
            return Collections.emptyList();
        }
        b bVar = hVar.h;
        if (bVar == null) {
            bVar = this.d;
        }
        Bitmap bitmap = this.g;
        if (bitmap == null || bVar.a + 1 != bitmap.getWidth() || bVar.b + 1 != this.g.getHeight()) {
            Bitmap createBitmap = Bitmap.createBitmap(bVar.a + 1, bVar.b + 1, Bitmap.Config.ARGB_8888);
            this.g = createBitmap;
            this.c.setBitmap(createBitmap);
        }
        ArrayList arrayList = new ArrayList();
        SparseArray<e> sparseArray2 = dVar.c;
        for (int i5 = 0; i5 < sparseArray2.size(); i5++) {
            this.c.save();
            e valueAt = sparseArray2.valueAt(i5);
            f fVar = this.f.c.get(sparseArray2.keyAt(i5));
            int i6 = valueAt.a + bVar.c;
            int i7 = valueAt.b + bVar.e;
            this.c.clipRect(i6, i7, Math.min(fVar.c + i6, bVar.d), Math.min(fVar.d + i7, bVar.f));
            a aVar = this.f.d.get(fVar.f);
            if (aVar == null && (aVar = this.f.f.get(fVar.f)) == null) {
                aVar = this.e;
            }
            SparseArray<g> sparseArray3 = fVar.j;
            int i8 = 0;
            while (i8 < sparseArray3.size()) {
                int keyAt = sparseArray3.keyAt(i8);
                g valueAt2 = sparseArray3.valueAt(i8);
                c cVar = this.f.e.get(keyAt);
                c cVar2 = cVar == null ? this.f.g.get(keyAt) : cVar;
                if (cVar2 != null) {
                    i4 = i8;
                    sparseArray = sparseArray3;
                    k(cVar2, aVar, fVar.e, valueAt2.a + i6, i7 + valueAt2.b, cVar2.b ? null : this.a, this.c);
                } else {
                    i4 = i8;
                    sparseArray = sparseArray3;
                }
                i8 = i4 + 1;
                sparseArray3 = sparseArray;
            }
            if (fVar.b) {
                int i9 = fVar.e;
                if (i9 == 3) {
                    i3 = aVar.d[fVar.g];
                } else if (i9 == 2) {
                    i3 = aVar.c[fVar.h];
                } else {
                    i3 = aVar.b[fVar.i];
                }
                this.b.setColor(i3);
                this.c.drawRect(i6, i7, fVar.c + i6, fVar.d + i7, this.b);
            }
            arrayList.add(new kb0.b().f(Bitmap.createBitmap(this.g, i6, i7, fVar.c, fVar.d)).k(i6 / bVar.a).l(0).h(i7 / bVar.b, 0).i(0).n(fVar.c / bVar.a).g(fVar.d / bVar.b).a());
            this.c.drawColor(0, PorterDuff.Mode.CLEAR);
            this.c.restore();
        }
        return Collections.unmodifiableList(arrayList);
    }

    public void r() {
        this.f.a();
    }
}
