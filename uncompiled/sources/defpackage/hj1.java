package defpackage;

import java.util.Hashtable;
import org.bouncycastle.crypto.a;

/* renamed from: hj1  reason: default package */
/* loaded from: classes2.dex */
public class hj1 implements a {
    public static Hashtable h;
    public qo0 a;
    public int b;
    public int c;
    public j72 d;
    public j72 e;
    public byte[] f;
    public byte[] g;

    static {
        Hashtable hashtable = new Hashtable();
        h = hashtable;
        hashtable.put("GOST3411", yr1.b(32));
        h.put("MD2", yr1.b(16));
        h.put("MD4", yr1.b(64));
        h.put("MD5", yr1.b(64));
        h.put("RIPEMD128", yr1.b(64));
        h.put("RIPEMD160", yr1.b(64));
        h.put("SHA-1", yr1.b(64));
        h.put("SHA-224", yr1.b(64));
        h.put("SHA-256", yr1.b(64));
        h.put("SHA-384", yr1.b(128));
        h.put("SHA-512", yr1.b(128));
        h.put("Tiger", yr1.b(64));
        h.put("Whirlpool", yr1.b(64));
    }

    public hj1(qo0 qo0Var) {
        this(qo0Var, e(qo0Var));
    }

    public hj1(qo0 qo0Var, int i) {
        this.a = qo0Var;
        int h2 = qo0Var.h();
        this.b = h2;
        this.c = i;
        this.f = new byte[i];
        this.g = new byte[i + h2];
    }

    public static int e(qo0 qo0Var) {
        if (qo0Var instanceof g11) {
            return ((g11) qo0Var).f();
        }
        Integer num = (Integer) h.get(qo0Var.g());
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalArgumentException("unknown digest passed: " + qo0Var.g());
    }

    public static void g(byte[] bArr, int i, byte b) {
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = (byte) (bArr[i2] ^ b);
        }
    }

    @Override // org.bouncycastle.crypto.a
    public int a(byte[] bArr, int i) {
        this.a.a(this.g, this.c);
        j72 j72Var = this.e;
        if (j72Var != null) {
            ((j72) this.a).d(j72Var);
            qo0 qo0Var = this.a;
            qo0Var.b(this.g, this.c, qo0Var.h());
        } else {
            qo0 qo0Var2 = this.a;
            byte[] bArr2 = this.g;
            qo0Var2.b(bArr2, 0, bArr2.length);
        }
        int a = this.a.a(bArr, i);
        int i2 = this.c;
        while (true) {
            byte[] bArr3 = this.g;
            if (i2 >= bArr3.length) {
                break;
            }
            bArr3[i2] = 0;
            i2++;
        }
        j72 j72Var2 = this.d;
        if (j72Var2 != null) {
            ((j72) this.a).d(j72Var2);
        } else {
            qo0 qo0Var3 = this.a;
            byte[] bArr4 = this.f;
            qo0Var3.b(bArr4, 0, bArr4.length);
        }
        return a;
    }

    @Override // org.bouncycastle.crypto.a
    public void b(byte[] bArr, int i, int i2) {
        this.a.b(bArr, i, i2);
    }

    @Override // org.bouncycastle.crypto.a
    public int c() {
        return this.b;
    }

    @Override // org.bouncycastle.crypto.a
    public void d(ty tyVar) {
        byte[] bArr;
        this.a.reset();
        byte[] a = ((kx1) tyVar).a();
        int length = a.length;
        if (length > this.c) {
            this.a.b(a, 0, length);
            this.a.a(this.f, 0);
            length = this.b;
        } else {
            System.arraycopy(a, 0, this.f, 0, length);
        }
        while (true) {
            bArr = this.f;
            if (length >= bArr.length) {
                break;
            }
            bArr[length] = 0;
            length++;
        }
        System.arraycopy(bArr, 0, this.g, 0, this.c);
        g(this.f, this.c, (byte) 54);
        g(this.g, this.c, (byte) 92);
        qo0 qo0Var = this.a;
        if (qo0Var instanceof j72) {
            j72 copy = ((j72) qo0Var).copy();
            this.e = copy;
            ((qo0) copy).b(this.g, 0, this.c);
        }
        qo0 qo0Var2 = this.a;
        byte[] bArr2 = this.f;
        qo0Var2.b(bArr2, 0, bArr2.length);
        qo0 qo0Var3 = this.a;
        if (qo0Var3 instanceof j72) {
            this.d = ((j72) qo0Var3).copy();
        }
    }

    public void f(byte b) {
        this.a.c(b);
    }
}
