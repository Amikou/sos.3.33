package defpackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: CopyOnWriteMultiset.java */
/* renamed from: t80  reason: default package */
/* loaded from: classes.dex */
public final class t80<E> implements Iterable<E> {
    public final Object a = new Object();
    public final Map<E, Integer> f0 = new HashMap();
    public Set<E> g0 = Collections.emptySet();
    public List<E> h0 = Collections.emptyList();

    public int count(E e) {
        int intValue;
        synchronized (this.a) {
            intValue = this.f0.containsKey(e) ? this.f0.get(e).intValue() : 0;
        }
        return intValue;
    }

    public void e(E e) {
        synchronized (this.a) {
            ArrayList arrayList = new ArrayList(this.h0);
            arrayList.add(e);
            this.h0 = Collections.unmodifiableList(arrayList);
            Integer num = this.f0.get(e);
            if (num == null) {
                HashSet hashSet = new HashSet(this.g0);
                hashSet.add(e);
                this.g0 = Collections.unmodifiableSet(hashSet);
            }
            this.f0.put(e, Integer.valueOf(num != null ? 1 + num.intValue() : 1));
        }
    }

    public Set<E> elementSet() {
        Set<E> set;
        synchronized (this.a) {
            set = this.g0;
        }
        return set;
    }

    public void i(E e) {
        synchronized (this.a) {
            Integer num = this.f0.get(e);
            if (num == null) {
                return;
            }
            ArrayList arrayList = new ArrayList(this.h0);
            arrayList.remove(e);
            this.h0 = Collections.unmodifiableList(arrayList);
            if (num.intValue() == 1) {
                this.f0.remove(e);
                HashSet hashSet = new HashSet(this.g0);
                hashSet.remove(e);
                this.g0 = Collections.unmodifiableSet(hashSet);
            } else {
                this.f0.put(e, Integer.valueOf(num.intValue() - 1));
            }
        }
    }

    @Override // java.lang.Iterable
    public Iterator<E> iterator() {
        Iterator<E> it;
        synchronized (this.a) {
            it = this.h0.iterator();
        }
        return it;
    }
}
