package defpackage;

import android.net.Uri;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: CacheKeyFactory.java */
/* renamed from: xt  reason: default package */
/* loaded from: classes.dex */
public interface xt {
    wt a(ImageRequest imageRequest, Object obj);

    wt b(ImageRequest imageRequest, Uri uri, Object obj);

    wt c(ImageRequest imageRequest, Object obj);

    wt d(ImageRequest imageRequest, Object obj);
}
