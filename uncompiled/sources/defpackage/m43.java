package defpackage;

import android.app.Activity;

/* compiled from: RecentAppsThumbnailHidingActivity.kt */
/* renamed from: m43  reason: default package */
/* loaded from: classes.dex */
public final class m43 {
    public static final void a(Activity activity, boolean z) {
        fs1.f(activity, "<this>");
        if (z) {
            activity.getWindow().setFlags(8192, 8192);
        } else {
            activity.getWindow().clearFlags(8192);
        }
    }

    public static final boolean b(Activity activity) {
        fs1.f(activity, "<this>");
        return (activity.getWindow().getAttributes().flags & 8192) != 0;
    }
}
