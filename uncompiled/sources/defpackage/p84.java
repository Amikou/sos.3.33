package defpackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigInteger;

/* compiled from: Transaction.java */
@JsonInclude(JsonInclude.Include.NON_NULL)
/* renamed from: p84  reason: default package */
/* loaded from: classes3.dex */
public class p84 {
    public static final BigInteger DEFAULT_GAS = BigInteger.valueOf(9000);
    private String data;
    private BigInteger feeCap;
    private String from;
    private BigInteger gas;
    private BigInteger gasPremium;
    private BigInteger gasPrice;
    private BigInteger nonce;
    private String to;
    private BigInteger value;

    public p84(String str, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str2, BigInteger bigInteger4, String str3) {
        this(str, bigInteger, bigInteger2, bigInteger3, str2, bigInteger4, str3, null, null);
    }

    private static String convert(BigInteger bigInteger) {
        if (bigInteger != null) {
            return ej2.encodeQuantity(bigInteger);
        }
        return null;
    }

    public static p84 createContractTransaction(String str, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, String str2) {
        return new p84(str, bigInteger, bigInteger2, bigInteger3, null, bigInteger4, str2);
    }

    public static p84 createEthCallTransaction(String str, String str2, String str3) {
        return new p84(str, null, null, null, str2, null, str3);
    }

    public static p84 createEtherTransaction(String str, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str2, BigInteger bigInteger4) {
        return new p84(str, bigInteger, bigInteger2, bigInteger3, str2, bigInteger4, null);
    }

    public static p84 createFunctionCallTransaction(String str, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str2, BigInteger bigInteger4, String str3) {
        return new p84(str, bigInteger, bigInteger2, bigInteger3, str2, bigInteger4, str3);
    }

    public String getData() {
        return this.data;
    }

    public String getFrom() {
        return this.from;
    }

    public String getGas() {
        return convert(this.gas);
    }

    public String getGasPrice() {
        return convert(this.gasPrice);
    }

    public String getNonce() {
        return convert(this.nonce);
    }

    public String getTo() {
        return this.to;
    }

    public String getValue() {
        return convert(this.value);
    }

    public p84(String str, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str2, BigInteger bigInteger4, String str3, BigInteger bigInteger5, BigInteger bigInteger6) {
        this.from = str;
        this.to = str2;
        this.gas = bigInteger3;
        this.gasPrice = bigInteger2;
        this.value = bigInteger4;
        if (str3 != null) {
            this.data = ej2.prependHexPrefix(str3);
        }
        this.nonce = bigInteger;
        this.gasPremium = bigInteger5;
        this.feeCap = bigInteger6;
    }

    public static p84 createContractTransaction(String str, BigInteger bigInteger, BigInteger bigInteger2, String str2) {
        return createContractTransaction(str, bigInteger, bigInteger2, null, null, str2);
    }

    public static p84 createFunctionCallTransaction(String str, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str2, String str3) {
        return new p84(str, bigInteger, bigInteger2, bigInteger3, str2, null, str3);
    }
}
