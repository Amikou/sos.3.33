package defpackage;

import java.security.MessageDigest;
import java.util.Map;

/* compiled from: EngineKey.java */
/* renamed from: nv0  reason: default package */
/* loaded from: classes.dex */
public class nv0 implements fx1 {
    public final Object b;
    public final int c;
    public final int d;
    public final Class<?> e;
    public final Class<?> f;
    public final fx1 g;
    public final Map<Class<?>, za4<?>> h;
    public final vn2 i;
    public int j;

    public nv0(Object obj, fx1 fx1Var, int i, int i2, Map<Class<?>, za4<?>> map, Class<?> cls, Class<?> cls2, vn2 vn2Var) {
        this.b = wt2.d(obj);
        this.g = (fx1) wt2.e(fx1Var, "Signature must not be null");
        this.c = i;
        this.d = i2;
        this.h = (Map) wt2.d(map);
        this.e = (Class) wt2.e(cls, "Resource class must not be null");
        this.f = (Class) wt2.e(cls2, "Transcode class must not be null");
        this.i = (vn2) wt2.d(vn2Var);
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof nv0) {
            nv0 nv0Var = (nv0) obj;
            return this.b.equals(nv0Var.b) && this.g.equals(nv0Var.g) && this.d == nv0Var.d && this.c == nv0Var.c && this.h.equals(nv0Var.h) && this.e.equals(nv0Var.e) && this.f.equals(nv0Var.f) && this.i.equals(nv0Var.i);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        if (this.j == 0) {
            int hashCode = this.b.hashCode();
            this.j = hashCode;
            int hashCode2 = (hashCode * 31) + this.g.hashCode();
            this.j = hashCode2;
            int i = (hashCode2 * 31) + this.c;
            this.j = i;
            int i2 = (i * 31) + this.d;
            this.j = i2;
            int hashCode3 = (i2 * 31) + this.h.hashCode();
            this.j = hashCode3;
            int hashCode4 = (hashCode3 * 31) + this.e.hashCode();
            this.j = hashCode4;
            int hashCode5 = (hashCode4 * 31) + this.f.hashCode();
            this.j = hashCode5;
            this.j = (hashCode5 * 31) + this.i.hashCode();
        }
        return this.j;
    }

    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
