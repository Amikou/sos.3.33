package defpackage;

import defpackage.l12;
import defpackage.uj3;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlinx.coroutines.internal.OnUndeliveredElementKt;
import kotlinx.coroutines.internal.UndeliveredElementException;

/* compiled from: AbstractChannel.kt */
/* renamed from: h5  reason: default package */
/* loaded from: classes2.dex */
public abstract class h5<E> implements uj3<E> {
    public static final /* synthetic */ AtomicReferenceFieldUpdater g0 = AtomicReferenceFieldUpdater.newUpdater(h5.class, Object.class, "onCloseHandler");
    public final tc1<E, te4> a;
    public final j12 f0 = new j12();
    private volatile /* synthetic */ Object onCloseHandler = null;

    /* compiled from: AbstractChannel.kt */
    /* renamed from: h5$a */
    /* loaded from: classes2.dex */
    public static final class a<E> extends tj3 {
        public final E h0;

        public a(E e) {
            this.h0 = e;
        }

        @Override // defpackage.tj3
        public void A(d00<?> d00Var) {
            if (ze0.a()) {
                throw new AssertionError();
            }
        }

        @Override // defpackage.tj3
        public k24 B(l12.b bVar) {
            return qv.a;
        }

        @Override // defpackage.l12
        public String toString() {
            return "SendBuffered@" + ff0.b(this) + '(' + this.h0 + ')';
        }

        @Override // defpackage.tj3
        public void y() {
        }

        @Override // defpackage.tj3
        public Object z() {
            return this.h0;
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* renamed from: h5$b */
    /* loaded from: classes2.dex */
    public static final class b extends l12.a {
        public final /* synthetic */ h5 d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(l12 l12Var, h5 h5Var) {
            super(l12Var);
            this.d = h5Var;
        }

        @Override // defpackage.cj
        /* renamed from: i */
        public Object g(l12 l12Var) {
            if (this.d.t()) {
                return null;
            }
            return k12.a();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public h5(tc1<? super E, te4> tc1Var) {
        this.a = tc1Var;
    }

    public final tj3 A() {
        l12 l12Var;
        l12 v;
        j12 j12Var = this.f0;
        while (true) {
            l12Var = (l12) j12Var.n();
            if (l12Var != j12Var && (l12Var instanceof tj3)) {
                if (((((tj3) l12Var) instanceof d00) && !l12Var.s()) || (v = l12Var.v()) == null) {
                    break;
                }
                v.r();
            }
        }
        l12Var = null;
        return (tj3) l12Var;
    }

    public final int d() {
        j12 j12Var = this.f0;
        int i = 0;
        for (l12 l12Var = (l12) j12Var.n(); !fs1.b(l12Var, j12Var); l12Var = l12Var.o()) {
            if (l12Var instanceof l12) {
                i++;
            }
        }
        return i;
    }

    public Object f(tj3 tj3Var) {
        boolean z;
        l12 p;
        if (s()) {
            l12 l12Var = this.f0;
            do {
                p = l12Var.p();
                if (p instanceof l43) {
                    return p;
                }
            } while (!p.i(tj3Var, l12Var));
            return null;
        }
        l12 l12Var2 = this.f0;
        b bVar = new b(tj3Var, this);
        while (true) {
            l12 p2 = l12Var2.p();
            if (!(p2 instanceof l43)) {
                int x = p2.x(tj3Var, l12Var2, bVar);
                z = true;
                if (x != 1) {
                    if (x == 2) {
                        z = false;
                        break;
                    }
                } else {
                    break;
                }
            } else {
                return p2;
            }
        }
        if (z) {
            return null;
        }
        return q4.e;
    }

    public String g() {
        return "";
    }

    @Override // defpackage.uj3
    public final Object h(E e, q70<? super te4> q70Var) {
        Object y;
        return (v(e) != q4.b && (y = y(e, q70Var)) == gs1.d()) ? y : te4.a;
    }

    public final d00<?> i() {
        l12 o = this.f0.o();
        d00<?> d00Var = o instanceof d00 ? (d00) o : null;
        if (d00Var == null) {
            return null;
        }
        n(d00Var);
        return d00Var;
    }

    public final d00<?> j() {
        l12 p = this.f0.p();
        d00<?> d00Var = p instanceof d00 ? (d00) p : null;
        if (d00Var == null) {
            return null;
        }
        n(d00Var);
        return d00Var;
    }

    public final j12 k() {
        return this.f0;
    }

    @Override // defpackage.uj3
    public boolean l(Throwable th) {
        boolean z;
        d00<?> d00Var = new d00<>(th);
        l12 l12Var = this.f0;
        while (true) {
            l12 p = l12Var.p();
            z = true;
            if (!(!(p instanceof d00))) {
                z = false;
                break;
            } else if (p.i(d00Var, l12Var)) {
                break;
            }
        }
        if (!z) {
            d00Var = (d00) this.f0.p();
        }
        n(d00Var);
        if (z) {
            r(th);
        }
        return z;
    }

    public final String m() {
        String l;
        l12 o = this.f0.o();
        if (o == this.f0) {
            return "EmptyQueue";
        }
        if (o instanceof d00) {
            l = o.toString();
        } else if (o instanceof e43) {
            l = "ReceiveQueued";
        } else {
            l = o instanceof tj3 ? "SendQueued" : fs1.l("UNEXPECTED:", o);
        }
        l12 p = this.f0.p();
        if (p != o) {
            String str = l + ",queueSize=" + d();
            if (p instanceof d00) {
                return str + ",closedForSend=" + p;
            }
            return str;
        }
        return l;
    }

    public final void n(d00<?> d00Var) {
        Object b2 = tq1.b(null, 1, null);
        while (true) {
            l12 p = d00Var.p();
            e43 e43Var = p instanceof e43 ? (e43) p : null;
            if (e43Var == null) {
                break;
            } else if (!e43Var.t()) {
                e43Var.q();
            } else {
                b2 = tq1.c(b2, e43Var);
            }
        }
        if (b2 != null) {
            if (!(b2 instanceof ArrayList)) {
                ((e43) b2).A(d00Var);
            } else {
                ArrayList arrayList = (ArrayList) b2;
                int size = arrayList.size() - 1;
                if (size >= 0) {
                    while (true) {
                        int i = size - 1;
                        ((e43) arrayList.get(size)).A(d00Var);
                        if (i < 0) {
                            break;
                        }
                        size = i;
                    }
                }
            }
        }
        w(d00Var);
    }

    public final Throwable o(d00<?> d00Var) {
        n(d00Var);
        return d00Var.G();
    }

    @Override // defpackage.uj3
    public boolean offer(E e) {
        UndeliveredElementException d;
        try {
            return uj3.a.b(this, e);
        } catch (Throwable th) {
            tc1<E, te4> tc1Var = this.a;
            if (tc1Var != null && (d = OnUndeliveredElementKt.d(tc1Var, e, null, 2, null)) != null) {
                py0.a(d, th);
                throw d;
            }
            throw th;
        }
    }

    @Override // defpackage.uj3
    public final Object p(E e) {
        Object v = v(e);
        if (v == q4.b) {
            return tx.b.c(te4.a);
        }
        if (v == q4.c) {
            d00<?> j = j();
            return j == null ? tx.b.b() : tx.b.a(o(j));
        } else if (v instanceof d00) {
            return tx.b.a(o((d00) v));
        } else {
            throw new IllegalStateException(fs1.l("trySend returned ", v).toString());
        }
    }

    public final void q(q70<?> q70Var, E e, d00<?> d00Var) {
        UndeliveredElementException d;
        n(d00Var);
        Throwable G = d00Var.G();
        tc1<E, te4> tc1Var = this.a;
        if (tc1Var == null || (d = OnUndeliveredElementKt.d(tc1Var, e, null, 2, null)) == null) {
            Result.a aVar = Result.Companion;
            q70Var.resumeWith(Result.m52constructorimpl(o83.a(G)));
            return;
        }
        py0.a(d, G);
        Result.a aVar2 = Result.Companion;
        q70Var.resumeWith(Result.m52constructorimpl(o83.a(d)));
    }

    public final void r(Throwable th) {
        k24 k24Var;
        Object obj = this.onCloseHandler;
        if (obj == null || obj == (k24Var = q4.f) || !g0.compareAndSet(this, obj, k24Var)) {
            return;
        }
        ((tc1) qd4.c(obj, 1)).invoke(th);
    }

    public abstract boolean s();

    public abstract boolean t();

    public String toString() {
        return ff0.a(this) + '@' + ff0.b(this) + '{' + m() + '}' + g();
    }

    public final boolean u() {
        return !(this.f0.o() instanceof l43) && t();
    }

    public Object v(E e) {
        l43<E> z;
        k24 g;
        do {
            z = z();
            if (z == null) {
                return q4.c;
            }
            g = z.g(e, null);
        } while (g == null);
        if (ze0.a()) {
            if (!(g == qv.a)) {
                throw new AssertionError();
            }
        }
        z.f(e);
        return z.c();
    }

    public void w(l12 l12Var) {
    }

    public final l43<?> x(E e) {
        l12 p;
        l12 l12Var = this.f0;
        a aVar = new a(e);
        do {
            p = l12Var.p();
            if (p instanceof l43) {
                return (l43) p;
            }
        } while (!p.i(aVar, l12Var));
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:32:0x006f, code lost:
        r4 = r0.x();
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0077, code lost:
        if (r4 != defpackage.gs1.d()) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x0079, code lost:
        defpackage.ef0.c(r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x0080, code lost:
        if (r4 != defpackage.gs1.d()) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x0082, code lost:
        return r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x0085, code lost:
        return defpackage.te4.a;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.Object y(E r4, defpackage.q70<? super defpackage.te4> r5) {
        /*
            r3 = this;
            q70 r0 = kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt.c(r5)
            pv r0 = defpackage.rv.b(r0)
        L8:
            boolean r1 = c(r3)
            if (r1 == 0) goto L4d
            tc1<E, te4> r1 = r3.a
            if (r1 != 0) goto L18
            vj3 r1 = new vj3
            r1.<init>(r4, r0)
            goto L1f
        L18:
            wj3 r1 = new wj3
            tc1<E, te4> r2 = r3.a
            r1.<init>(r4, r0, r2)
        L1f:
            java.lang.Object r2 = r3.f(r1)
            if (r2 != 0) goto L29
            defpackage.rv.c(r0, r1)
            goto L6f
        L29:
            boolean r1 = r2 instanceof defpackage.d00
            if (r1 == 0) goto L33
            d00 r2 = (defpackage.d00) r2
            b(r3, r0, r4, r2)
            goto L6f
        L33:
            k24 r1 = defpackage.q4.e
            if (r2 != r1) goto L38
            goto L4d
        L38:
            boolean r1 = r2 instanceof defpackage.e43
            if (r1 == 0) goto L3d
            goto L4d
        L3d:
            java.lang.String r4 = "enqueueSend returned "
            java.lang.String r4 = defpackage.fs1.l(r4, r2)
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r4 = r4.toString()
            r5.<init>(r4)
            throw r5
        L4d:
            java.lang.Object r1 = r3.v(r4)
            k24 r2 = defpackage.q4.b
            if (r1 != r2) goto L61
            te4 r4 = defpackage.te4.a
            kotlin.Result$a r1 = kotlin.Result.Companion
            java.lang.Object r4 = kotlin.Result.m52constructorimpl(r4)
            r0.resumeWith(r4)
            goto L6f
        L61:
            k24 r2 = defpackage.q4.c
            if (r1 != r2) goto L66
            goto L8
        L66:
            boolean r2 = r1 instanceof defpackage.d00
            if (r2 == 0) goto L86
            d00 r1 = (defpackage.d00) r1
            b(r3, r0, r4, r1)
        L6f:
            java.lang.Object r4 = r0.x()
            java.lang.Object r0 = defpackage.gs1.d()
            if (r4 != r0) goto L7c
            defpackage.ef0.c(r5)
        L7c:
            java.lang.Object r5 = defpackage.gs1.d()
            if (r4 != r5) goto L83
            return r4
        L83:
            te4 r4 = defpackage.te4.a
            return r4
        L86:
            java.lang.String r4 = "offerInternal returned "
            java.lang.String r4 = defpackage.fs1.l(r4, r1)
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r4 = r4.toString()
            r5.<init>(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.h5.y(java.lang.Object, q70):java.lang.Object");
    }

    public l43<E> z() {
        l12 l12Var;
        l12 v;
        j12 j12Var = this.f0;
        while (true) {
            l12Var = (l12) j12Var.n();
            if (l12Var != j12Var && (l12Var instanceof l43)) {
                if (((((l43) l12Var) instanceof d00) && !l12Var.s()) || (v = l12Var.v()) == null) {
                    break;
                }
                v.r();
            }
        }
        l12Var = null;
        return (l43) l12Var;
    }
}
