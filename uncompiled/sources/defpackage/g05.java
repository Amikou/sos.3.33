package defpackage;

import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: g05  reason: default package */
/* loaded from: classes.dex */
public final class g05 extends t05 {
    public WeakReference<d05> a;

    public g05(d05 d05Var) {
        this.a = new WeakReference<>(d05Var);
    }

    @Override // defpackage.t05
    public final void a() {
        d05 d05Var = this.a.get();
        if (d05Var == null) {
            return;
        }
        d05Var.u();
    }
}
