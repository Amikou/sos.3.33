package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: g16  reason: default package */
/* loaded from: classes.dex */
public final class g16 implements f16 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.client.firebase_feature_rollout.v1.enable", true);

    @Override // defpackage.f16
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.f16
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
