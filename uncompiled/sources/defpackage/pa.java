package defpackage;

import com.google.crypto.tink.KeyTemplate;
import com.google.crypto.tink.d;
import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.p;
import com.google.crypto.tink.proto.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

/* compiled from: AesSivKeyManager.java */
/* renamed from: pa  reason: default package */
/* loaded from: classes2.dex */
public final class pa extends g<p> {

    /* compiled from: AesSivKeyManager.java */
    /* renamed from: pa$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<d, p> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public d a(p pVar) throws GeneralSecurityException {
            return new oa(pVar.G().toByteArray());
        }
    }

    /* compiled from: AesSivKeyManager.java */
    /* renamed from: pa$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<q, p> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public p a(q qVar) throws GeneralSecurityException {
            return p.I().s(ByteString.copyFrom(p33.c(qVar.E()))).t(pa.this.l()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public q c(ByteString byteString) throws InvalidProtocolBufferException {
            return q.H(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(q qVar) throws GeneralSecurityException {
            if (qVar.E() == 64) {
                return;
            }
            throw new InvalidAlgorithmParameterException("invalid key size: " + qVar.E() + ". Valid keys must have 64 bytes.");
        }
    }

    public pa() {
        super(p.class, new a(d.class));
    }

    public static final KeyTemplate j() {
        return k(64, KeyTemplate.OutputPrefixType.TINK);
    }

    public static KeyTemplate k(int i, KeyTemplate.OutputPrefixType outputPrefixType) {
        return KeyTemplate.a(new pa().c(), q.G().s(i).build().toByteArray(), outputPrefixType);
    }

    public static void n(boolean z) throws GeneralSecurityException {
        com.google.crypto.tink.q.q(new pa(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesSivKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, p> e() {
        return new b(q.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int l() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public p g(ByteString byteString) throws InvalidProtocolBufferException {
        return p.J(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: o */
    public void i(p pVar) throws GeneralSecurityException {
        ug4.c(pVar.H(), l());
        if (pVar.G().size() == 64) {
            return;
        }
        throw new InvalidKeyException("invalid key size: " + pVar.G().size() + ". Valid keys must have 64 bytes.");
    }
}
