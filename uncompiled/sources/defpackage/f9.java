package defpackage;

/* compiled from: AddImageTransformMetaDataProducer.java */
/* renamed from: f9  reason: default package */
/* loaded from: classes.dex */
public class f9 implements dv2<zu0> {
    public final dv2<zu0> a;

    /* compiled from: AddImageTransformMetaDataProducer.java */
    /* renamed from: f9$b */
    /* loaded from: classes.dex */
    public static class b extends bm0<zu0, zu0> {
        @Override // defpackage.qm
        /* renamed from: q */
        public void i(zu0 zu0Var, int i) {
            if (zu0Var == null) {
                p().d(null, i);
                return;
            }
            if (!zu0.A(zu0Var)) {
                zu0Var.M();
            }
            p().d(zu0Var, i);
        }

        public b(l60<zu0> l60Var) {
            super(l60Var);
        }
    }

    public f9(dv2<zu0> dv2Var) {
        this.a = dv2Var;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        this.a.a(new b(l60Var), ev2Var);
    }
}
