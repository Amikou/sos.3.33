package defpackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Set;

/* compiled from: _Collections.kt */
/* renamed from: j20 */
/* loaded from: classes2.dex */
public class j20 extends i20 {

    /* compiled from: Sequences.kt */
    /* renamed from: j20$a */
    /* loaded from: classes2.dex */
    public static final class a implements ol3<T> {
        public final /* synthetic */ Iterable a;

        public a(Iterable iterable) {
            this.a = iterable;
        }

        @Override // defpackage.ol3
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    public static final <T> ol3<T> D(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$asSequence");
        return new a(iterable);
    }

    public static final <T> boolean E(Iterable<? extends T> iterable, T t) {
        fs1.f(iterable, "$this$contains");
        if (iterable instanceof Collection) {
            return ((Collection) iterable).contains(t);
        }
        return O(iterable, t) >= 0;
    }

    public static final <T> List<T> F(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$distinct");
        return k0(n0(iterable));
    }

    public static final <T> List<T> G(Iterable<? extends T> iterable, int i) {
        ArrayList arrayList;
        fs1.f(iterable, "$this$drop");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return k0(iterable);
        } else {
            if (iterable instanceof Collection) {
                Collection collection = (Collection) iterable;
                int size = collection.size() - i;
                if (size <= 0) {
                    return b20.g();
                }
                if (size == 1) {
                    return a20.b(T(iterable));
                }
                arrayList = new ArrayList(size);
                if (iterable instanceof List) {
                    if (iterable instanceof RandomAccess) {
                        int size2 = collection.size();
                        while (i < size2) {
                            arrayList.add(((List) iterable).get(i));
                            i++;
                        }
                    } else {
                        ListIterator listIterator = ((List) iterable).listIterator(i);
                        while (listIterator.hasNext()) {
                            arrayList.add(listIterator.next());
                        }
                    }
                    return arrayList;
                }
            } else {
                arrayList = new ArrayList();
            }
            for (T t : iterable) {
                if (i2 >= i) {
                    arrayList.add(t);
                } else {
                    i2++;
                }
            }
            return b20.m(arrayList);
        }
    }

    public static final <T> List<T> H(List<? extends T> list, int i) {
        fs1.f(list, "$this$dropLast");
        if (i >= 0) {
            return f0(list, u33.b(list.size() - i, 0));
        }
        throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
    }

    public static final <T> List<T> I(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$filterNotNull");
        return (List) J(iterable, new ArrayList());
    }

    public static final <C extends Collection<? super T>, T> C J(Iterable<? extends T> iterable, C c) {
        fs1.f(iterable, "$this$filterNotNullTo");
        fs1.f(c, "destination");
        for (T t : iterable) {
            if (t != null) {
                c.add(t);
            }
        }
        return c;
    }

    public static final <T> T K(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$first");
        if (iterable instanceof List) {
            return (T) L((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            return it.next();
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    public static final <T> T L(List<? extends T> list) {
        fs1.f(list, "$this$first");
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> T M(List<? extends T> list) {
        fs1.f(list, "$this$firstOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public static final <T> T N(List<? extends T> list, int i) {
        fs1.f(list, "$this$getOrNull");
        if (i < 0 || i > b20.i(list)) {
            return null;
        }
        return list.get(i);
    }

    public static final <T> int O(Iterable<? extends T> iterable, T t) {
        fs1.f(iterable, "$this$indexOf");
        if (iterable instanceof List) {
            return ((List) iterable).indexOf(t);
        }
        int i = 0;
        for (T t2 : iterable) {
            if (i < 0) {
                b20.p();
            }
            if (fs1.b(t, t2)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static final <T, A extends Appendable> A P(Iterable<? extends T> iterable, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(iterable, "$this$joinTo");
        fs1.f(a2, "buffer");
        fs1.f(charSequence, "separator");
        fs1.f(charSequence2, "prefix");
        fs1.f(charSequence3, "postfix");
        fs1.f(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (T t : iterable) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            wu3.a(a2, t, tc1Var);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    public static /* synthetic */ Appendable Q(Iterable iterable, Appendable appendable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1 tc1Var, int i2, Object obj) {
        return P(iterable, appendable, (i2 & 2) != 0 ? ", " : charSequence, (i2 & 4) != 0 ? "" : charSequence2, (i2 & 8) == 0 ? charSequence3 : "", (i2 & 16) != 0 ? -1 : i, (i2 & 32) != 0 ? "..." : charSequence4, (i2 & 64) != 0 ? null : tc1Var);
    }

    public static final <T> String R(Iterable<? extends T> iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(iterable, "$this$joinToString");
        fs1.f(charSequence, "separator");
        fs1.f(charSequence2, "prefix");
        fs1.f(charSequence3, "postfix");
        fs1.f(charSequence4, "truncated");
        String sb = ((StringBuilder) P(iterable, new StringBuilder(), charSequence, charSequence2, charSequence3, i, charSequence4, tc1Var)).toString();
        fs1.e(sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }

    public static /* synthetic */ String S(Iterable iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1 tc1Var, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        String str = (i2 & 2) != 0 ? "" : charSequence2;
        String str2 = (i2 & 4) == 0 ? charSequence3 : "";
        if ((i2 & 8) != 0) {
            i = -1;
        }
        int i3 = i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence5 = charSequence4;
        if ((i2 & 32) != 0) {
            tc1Var = null;
        }
        return R(iterable, charSequence, str, str2, i3, charSequence5, tc1Var);
    }

    public static final <T> T T(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$last");
        if (iterable instanceof List) {
            return (T) U((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T next = it.next();
            while (it.hasNext()) {
                next = it.next();
            }
            return next;
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    public static final <T> T U(List<? extends T> list) {
        fs1.f(list, "$this$last");
        if (!list.isEmpty()) {
            return list.get(b20.i(list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> T V(List<? extends T> list) {
        fs1.f(list, "$this$lastOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    public static final <T extends Comparable<? super T>> T W(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$minOrNull");
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T next = it.next();
            while (it.hasNext()) {
                T next2 = it.next();
                if (next.compareTo(next2) > 0) {
                    next = next2;
                }
            }
            return next;
        }
        return null;
    }

    public static final <T> List<T> X(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        fs1.f(iterable, "$this$minus");
        fs1.f(iterable2, "elements");
        Collection r = c20.r(iterable2, iterable);
        if (r.isEmpty()) {
            return k0(iterable);
        }
        ArrayList arrayList = new ArrayList();
        for (T t : iterable) {
            if (!r.contains(t)) {
                arrayList.add(t);
            }
        }
        return arrayList;
    }

    public static final <T> List<T> Y(Iterable<? extends T> iterable, T t) {
        fs1.f(iterable, "$this$minus");
        ArrayList arrayList = new ArrayList(c20.q(iterable, 10));
        boolean z = false;
        for (T t2 : iterable) {
            boolean z2 = true;
            if (!z && fs1.b(t2, t)) {
                z = true;
                z2 = false;
            }
            if (z2) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    public static final <T> List<T> Z(Collection<? extends T> collection, Iterable<? extends T> iterable) {
        fs1.f(collection, "$this$plus");
        fs1.f(iterable, "elements");
        if (iterable instanceof Collection) {
            Collection collection2 = (Collection) iterable;
            ArrayList arrayList = new ArrayList(collection.size() + collection2.size());
            arrayList.addAll(collection);
            arrayList.addAll(collection2);
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList(collection);
        g20.v(arrayList2, iterable);
        return arrayList2;
    }

    public static final <T> List<T> a0(Collection<? extends T> collection, T t) {
        fs1.f(collection, "$this$plus");
        ArrayList arrayList = new ArrayList(collection.size() + 1);
        arrayList.addAll(collection);
        arrayList.add(t);
        return arrayList;
    }

    public static final <T> List<T> b0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$reversed");
        if (!(iterable instanceof Collection) || ((Collection) iterable).size() > 1) {
            List<T> l0 = l0(iterable);
            i20.C(l0);
            return l0;
        }
        return k0(iterable);
    }

    public static final <T> T c0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$single");
        if (iterable instanceof List) {
            return (T) d0((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T next = it.next();
            if (it.hasNext()) {
                throw new IllegalArgumentException("Collection has more than one element.");
            }
            return next;
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    public static final <T> T d0(List<? extends T> list) {
        fs1.f(list, "$this$single");
        int size = list.size();
        if (size != 0) {
            if (size == 1) {
                return list.get(0);
            }
            throw new IllegalArgumentException("List has more than one element.");
        }
        throw new NoSuchElementException("List is empty.");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> e0(Iterable<? extends T> iterable, Comparator<? super T> comparator) {
        fs1.f(iterable, "$this$sortedWith");
        fs1.f(comparator, "comparator");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return k0(iterable);
            }
            Object[] array = collection.toArray(new Object[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            zh.o(array, comparator);
            return zh.c(array);
        }
        List<T> l0 = l0(iterable);
        f20.u(l0, comparator);
        return l0;
    }

    public static final <T> List<T> f0(Iterable<? extends T> iterable, int i) {
        fs1.f(iterable, "$this$take");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return b20.g();
        } else {
            if (iterable instanceof Collection) {
                if (i >= ((Collection) iterable).size()) {
                    return k0(iterable);
                }
                if (i == 1) {
                    return a20.b(K(iterable));
                }
            }
            ArrayList arrayList = new ArrayList(i);
            for (T t : iterable) {
                arrayList.add(t);
                i2++;
                if (i2 == i) {
                    break;
                }
            }
            return b20.m(arrayList);
        }
    }

    public static final byte[] g0(Collection<Byte> collection) {
        fs1.f(collection, "$this$toByteArray");
        byte[] bArr = new byte[collection.size()];
        int i = 0;
        for (Byte b : collection) {
            bArr[i] = b.byteValue();
            i++;
        }
        return bArr;
    }

    public static final <T, C extends Collection<? super T>> C h0(Iterable<? extends T> iterable, C c) {
        fs1.f(iterable, "$this$toCollection");
        fs1.f(c, "destination");
        for (T t : iterable) {
            c.add(t);
        }
        return c;
    }

    public static final <T> HashSet<T> i0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$toHashSet");
        return (HashSet) h0(iterable, new HashSet(y32.a(c20.q(iterable, 12))));
    }

    public static final int[] j0(Collection<Integer> collection) {
        fs1.f(collection, "$this$toIntArray");
        int[] iArr = new int[collection.size()];
        int i = 0;
        for (Integer num : collection) {
            iArr[i] = num.intValue();
            i++;
        }
        return iArr;
    }

    public static final <T> List<T> k0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$toList");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size != 0) {
                if (size != 1) {
                    return m0(collection);
                }
                return a20.b(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
            }
            return b20.g();
        }
        return b20.m(l0(iterable));
    }

    public static final <T> List<T> l0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$toMutableList");
        if (iterable instanceof Collection) {
            return m0((Collection) iterable);
        }
        return (List) h0(iterable, new ArrayList());
    }

    public static final <T> List<T> m0(Collection<? extends T> collection) {
        fs1.f(collection, "$this$toMutableList");
        return new ArrayList(collection);
    }

    public static final <T> Set<T> n0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$toMutableSet");
        return iterable instanceof Collection ? new LinkedHashSet((Collection) iterable) : (Set) h0(iterable, new LinkedHashSet());
    }

    public static final <T> Set<T> o0(Iterable<? extends T> iterable) {
        fs1.f(iterable, "$this$toSet");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size != 0) {
                if (size != 1) {
                    return (Set) h0(iterable, new LinkedHashSet(y32.a(collection.size())));
                }
                return sm3.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
            }
            return tm3.b();
        }
        return tm3.d((Set) h0(iterable, new LinkedHashSet()));
    }
}
