package defpackage;

import java.io.IOException;
import java8.util.o;
import org.web3j.protocol.exceptions.TransactionException;

/* compiled from: TransactionReceiptProcessor.java */
/* renamed from: x84  reason: default package */
/* loaded from: classes3.dex */
public abstract class x84 {
    private final ko4 web3j;

    public x84(ko4 ko4Var) {
        this.web3j = ko4Var;
    }

    public o<? extends w84> sendTransactionReceiptRequest(String str) throws IOException, TransactionException {
        zw0 send = this.web3j.ethGetTransactionReceipt(str).send();
        if (!send.hasError()) {
            return send.getTransactionReceipt();
        }
        throw new TransactionException("Error processing request: " + send.getError().getMessage());
    }

    public abstract w84 waitForTransactionReceipt(String str) throws IOException, TransactionException;
}
