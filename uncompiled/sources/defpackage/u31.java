package defpackage;

import java.io.File;

/* compiled from: FileTree.java */
/* renamed from: u31  reason: default package */
/* loaded from: classes.dex */
public class u31 {
    public static boolean a(File file) {
        File[] listFiles = file.listFiles();
        boolean z = true;
        if (listFiles != null) {
            for (File file2 : listFiles) {
                z &= b(file2);
            }
        }
        return z;
    }

    public static boolean b(File file) {
        if (file.isDirectory()) {
            a(file);
        }
        return file.delete();
    }

    public static void c(File file, v31 v31Var) {
        v31Var.b(file);
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    c(file2, v31Var);
                } else {
                    v31Var.a(file2);
                }
            }
        }
        v31Var.c(file);
    }
}
