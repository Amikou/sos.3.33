package defpackage;

import com.google.crypto.tink.shaded.protobuf.n;

/* compiled from: ExtensionRegistryFactory.java */
/* renamed from: j11  reason: default package */
/* loaded from: classes2.dex */
public final class j11 {
    public static final Class<?> a = c();

    public static n a() {
        n b = b("getEmptyRegistry");
        return b != null ? b : n.d;
    }

    public static final n b(String str) {
        Class<?> cls = a;
        if (cls == null) {
            return null;
        }
        try {
            return (n) cls.getDeclaredMethod(str, new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    public static Class<?> c() {
        try {
            return Class.forName("com.google.crypto.tink.shaded.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }
}
