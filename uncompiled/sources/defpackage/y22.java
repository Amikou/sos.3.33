package defpackage;

import com.google.crypto.tink.n;
import com.google.crypto.tink.o;
import com.google.crypto.tink.p;
import com.google.crypto.tink.proto.OutputPrefixType;
import com.google.crypto.tink.q;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.logging.Logger;

/* compiled from: MacWrapper.java */
/* renamed from: y22  reason: default package */
/* loaded from: classes2.dex */
public class y22 implements p<n, n> {
    public static final Logger a = Logger.getLogger(y22.class.getName());

    /* compiled from: MacWrapper.java */
    /* renamed from: y22$b */
    /* loaded from: classes2.dex */
    public static class b implements n {
        public final o<n> a;
        public final byte[] b;

        @Override // com.google.crypto.tink.n
        public void a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            if (bArr.length > 5) {
                byte[] copyOf = Arrays.copyOf(bArr, 5);
                byte[] copyOfRange = Arrays.copyOfRange(bArr, 5, bArr.length);
                for (o.b<n> bVar : this.a.c(copyOf)) {
                    try {
                        if (bVar.b().equals(OutputPrefixType.LEGACY)) {
                            bVar.c().a(copyOfRange, at.a(bArr2, this.b));
                            return;
                        } else {
                            bVar.c().a(copyOfRange, bArr2);
                            return;
                        }
                    } catch (GeneralSecurityException e) {
                        Logger logger = y22.a;
                        logger.info("tag prefix matches a key, but cannot verify: " + e);
                    }
                }
                for (o.b<n> bVar2 : this.a.e()) {
                    try {
                        bVar2.c().a(bArr, bArr2);
                        return;
                    } catch (GeneralSecurityException unused) {
                    }
                }
                throw new GeneralSecurityException("invalid MAC");
            }
            throw new GeneralSecurityException("tag too short");
        }

        @Override // com.google.crypto.tink.n
        public byte[] b(byte[] bArr) throws GeneralSecurityException {
            if (this.a.b().b().equals(OutputPrefixType.LEGACY)) {
                return at.a(this.a.b().a(), this.a.b().c().b(at.a(bArr, this.b)));
            }
            return at.a(this.a.b().a(), this.a.b().c().b(bArr));
        }

        public b(o<n> oVar) {
            this.b = new byte[]{0};
            this.a = oVar;
        }
    }

    public static void e() throws GeneralSecurityException {
        q.r(new y22());
    }

    @Override // com.google.crypto.tink.p
    public Class<n> a() {
        return n.class;
    }

    @Override // com.google.crypto.tink.p
    public Class<n> b() {
        return n.class;
    }

    @Override // com.google.crypto.tink.p
    /* renamed from: f */
    public n c(o<n> oVar) throws GeneralSecurityException {
        return new b(oVar);
    }
}
