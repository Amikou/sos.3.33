package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStatsDataQuoteList;
import net.safemoon.androidwallet.views.marker.BaseGraphMarkerView;

/* compiled from: CandleGraphMarkerView.kt */
@SuppressLint({"ViewConstructor"})
/* renamed from: CandleGraphMarkerView  reason: default package */
/* loaded from: classes3.dex */
public final class CandleGraphMarkerView extends BaseGraphMarkerView {
    public final List<CoinPriceStatsDataQuoteList> f0;
    public final String g0;
    public final TextView h0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CandleGraphMarkerView(Context context, int i, List<CoinPriceStatsDataQuoteList> list, String str) {
        super(context, i);
        fs1.f(list, "data");
        fs1.f(str, "currencySymbol");
        this.f0 = list;
        this.g0 = str;
        View findViewById = findViewById(R.id.tvMarkerContent);
        fs1.e(findViewById, "findViewById(R.id.tvMarkerContent)");
        this.h0 = (TextView) findViewById;
    }

    @Override // net.safemoon.androidwallet.views.marker.BaseGraphMarkerView
    public int getMarkerColor() {
        return R.color.p5;
    }

    @Override // com.github.mikephil.charting.components.MarkerView, com.github.mikephil.charting.components.IMarker
    public void refreshContent(Entry entry, Highlight highlight) {
        if (entry instanceof CandleEntry) {
            TextView textView = this.h0;
            Context context = getContext();
            re0 re0Var = re0.a;
            CandleEntry candleEntry = (CandleEntry) entry;
            textView.setText(context.getString(R.string.chart_text_new_line, re0Var.a(re0Var.b(this.f0.get((int) candleEntry.getX()).getOpenTimestamp())), this.g0, e30.p(candleEntry.getClose(), 0, null, false, 7, null)));
        }
        super.refreshContent(entry, highlight);
    }
}
