package defpackage;

import com.google.android.gms.measurement.internal.g;
import java.net.URL;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: bh5  reason: default package */
/* loaded from: classes.dex */
public final class bh5 implements Runnable {
    public final URL a;
    public final byte[] f0;
    public final wg5 g0;
    public final String h0;
    public final Map<String, String> i0;
    public final /* synthetic */ g j0;

    public bh5(g gVar, String str, URL url, byte[] bArr, Map<String, String> map, wg5 wg5Var) {
        this.j0 = gVar;
        zt2.f(str);
        zt2.j(url);
        zt2.j(wg5Var);
        this.a = url;
        this.f0 = bArr;
        this.g0 = wg5Var;
        this.h0 = str;
        this.i0 = map;
    }

    /* JADX WARN: Removed duplicated region for block: B:61:0x010a  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x014a  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x012e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:77:0x00ee A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // java.lang.Runnable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void run() {
        /*
            Method dump skipped, instructions count: 355
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bh5.run():void");
    }
}
