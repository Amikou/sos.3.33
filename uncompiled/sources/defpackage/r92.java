package defpackage;

import net.safemoon.androidwallet.model.collectible.MoralisNFTs;
import retrofit2.b;

/* compiled from: MoralisAPIInterface.kt */
/* renamed from: r92  reason: default package */
/* loaded from: classes2.dex */
public interface r92 {

    /* compiled from: MoralisAPIInterface.kt */
    /* renamed from: r92$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ b a(r92 r92Var, String str, String str2, String str3, int i, int i2, Object obj) {
            if (obj == null) {
                if ((i2 & 4) != 0) {
                    str3 = "decimal";
                }
                return r92Var.a(str, str2, str3, i);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getCollections");
        }
    }

    @ge1("api/v2/{address}/nft")
    b<MoralisNFTs> a(@vp2("address") String str, @yw2("chain") String str2, @yw2("format") String str3, @yw2("limit") int i);
}
