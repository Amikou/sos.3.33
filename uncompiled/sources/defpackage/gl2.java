package defpackage;

import java.lang.reflect.Array;
import java.util.List;

/* compiled from: ObjectBuffer.java */
/* renamed from: gl2  reason: default package */
/* loaded from: classes.dex */
public final class gl2 {
    public f02<Object[]> a;
    public f02<Object[]> b;
    public int c;
    public Object[] d;

    public final void a(Object obj, int i, Object[] objArr, int i2) {
        int i3 = 0;
        for (f02<Object[]> f02Var = this.a; f02Var != null; f02Var = f02Var.c()) {
            Object[] d = f02Var.d();
            int length = d.length;
            System.arraycopy(d, 0, obj, i3, length);
            i3 += length;
        }
        System.arraycopy(objArr, 0, obj, i3, i2);
        int i4 = i3 + i2;
        if (i4 == i) {
            return;
        }
        throw new IllegalStateException("Should have gotten " + i + " entries, got " + i4);
    }

    public void b() {
        f02<Object[]> f02Var = this.b;
        if (f02Var != null) {
            this.d = f02Var.d();
        }
        this.b = null;
        this.a = null;
        this.c = 0;
    }

    public Object[] c(Object[] objArr) {
        f02<Object[]> f02Var = new f02<>(objArr, null);
        if (this.a == null) {
            this.b = f02Var;
            this.a = f02Var;
        } else {
            this.b.b(f02Var);
            this.b = f02Var;
        }
        int length = objArr.length;
        this.c += length;
        if (length < 16384) {
            length += length;
        } else if (length < 262144) {
            length += length >> 2;
        }
        return new Object[length];
    }

    public int d() {
        return this.c;
    }

    public void e(Object[] objArr, int i, List<Object> list) {
        int i2;
        f02<Object[]> f02Var = this.a;
        while (true) {
            i2 = 0;
            if (f02Var == null) {
                break;
            }
            Object[] d = f02Var.d();
            int length = d.length;
            while (i2 < length) {
                list.add(d[i2]);
                i2++;
            }
            f02Var = f02Var.c();
        }
        while (i2 < i) {
            list.add(objArr[i2]);
            i2++;
        }
    }

    public Object[] f(Object[] objArr, int i) {
        int i2 = this.c + i;
        Object[] objArr2 = new Object[i2];
        a(objArr2, i2, objArr, i);
        return objArr2;
    }

    public <T> T[] g(Object[] objArr, int i, Class<T> cls) {
        int i2 = this.c + i;
        T[] tArr = (T[]) ((Object[]) Array.newInstance((Class<?>) cls, i2));
        a(tArr, i2, objArr, i);
        b();
        return tArr;
    }

    public int h() {
        Object[] objArr = this.d;
        if (objArr == null) {
            return 0;
        }
        return objArr.length;
    }

    public Object[] i() {
        b();
        Object[] objArr = this.d;
        return objArr == null ? new Object[12] : objArr;
    }
}
