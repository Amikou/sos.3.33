package defpackage;

import androidx.media3.common.j;
import androidx.media3.datasource.b;

/* compiled from: BaseMediaChunk.java */
/* renamed from: ln  reason: default package */
/* loaded from: classes.dex */
public abstract class ln extends s52 {
    public final long k;
    public final long l;
    public nn m;
    public int[] n;

    public ln(b bVar, je0 je0Var, j jVar, int i, Object obj, long j, long j2, long j3, long j4, long j5) {
        super(bVar, je0Var, jVar, i, obj, j, j2, j5);
        this.k = j3;
        this.l = j4;
    }

    public final int h(int i) {
        return ((int[]) ii.i(this.n))[i];
    }

    public final nn i() {
        return (nn) ii.i(this.m);
    }

    public void j(nn nnVar) {
        this.m = nnVar;
        this.n = nnVar.a();
    }
}
