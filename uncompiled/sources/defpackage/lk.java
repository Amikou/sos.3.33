package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport.java */
/* renamed from: lk  reason: default package */
/* loaded from: classes2.dex */
public final class lk extends r90 {
    public final String b;
    public final String c;
    public final int d;
    public final String e;
    public final String f;
    public final String g;
    public final r90.e h;
    public final r90.d i;

    /* compiled from: AutoValue_CrashlyticsReport.java */
    /* renamed from: lk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.b {
        public String a;
        public String b;
        public Integer c;
        public String d;
        public String e;
        public String f;
        public r90.e g;
        public r90.d h;

        @Override // defpackage.r90.b
        public r90 a() {
            String str = "";
            if (this.a == null) {
                str = " sdkVersion";
            }
            if (this.b == null) {
                str = str + " gmpAppId";
            }
            if (this.c == null) {
                str = str + " platform";
            }
            if (this.d == null) {
                str = str + " installationUuid";
            }
            if (this.e == null) {
                str = str + " buildVersion";
            }
            if (this.f == null) {
                str = str + " displayVersion";
            }
            if (str.isEmpty()) {
                return new lk(this.a, this.b, this.c.intValue(), this.d, this.e, this.f, this.g, this.h);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.b
        public r90.b b(String str) {
            Objects.requireNonNull(str, "Null buildVersion");
            this.e = str;
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b c(String str) {
            Objects.requireNonNull(str, "Null displayVersion");
            this.f = str;
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b d(String str) {
            Objects.requireNonNull(str, "Null gmpAppId");
            this.b = str;
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b e(String str) {
            Objects.requireNonNull(str, "Null installationUuid");
            this.d = str;
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b f(r90.d dVar) {
            this.h = dVar;
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b g(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b h(String str) {
            Objects.requireNonNull(str, "Null sdkVersion");
            this.a = str;
            return this;
        }

        @Override // defpackage.r90.b
        public r90.b i(r90.e eVar) {
            this.g = eVar;
            return this;
        }

        public b() {
        }

        public b(r90 r90Var) {
            this.a = r90Var.i();
            this.b = r90Var.e();
            this.c = Integer.valueOf(r90Var.h());
            this.d = r90Var.f();
            this.e = r90Var.c();
            this.f = r90Var.d();
            this.g = r90Var.j();
            this.h = r90Var.g();
        }
    }

    @Override // defpackage.r90
    public String c() {
        return this.f;
    }

    @Override // defpackage.r90
    public String d() {
        return this.g;
    }

    @Override // defpackage.r90
    public String e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        r90.e eVar;
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90) {
            r90 r90Var = (r90) obj;
            if (this.b.equals(r90Var.i()) && this.c.equals(r90Var.e()) && this.d == r90Var.h() && this.e.equals(r90Var.f()) && this.f.equals(r90Var.c()) && this.g.equals(r90Var.d()) && ((eVar = this.h) != null ? eVar.equals(r90Var.j()) : r90Var.j() == null)) {
                r90.d dVar = this.i;
                if (dVar == null) {
                    if (r90Var.g() == null) {
                        return true;
                    }
                } else if (dVar.equals(r90Var.g())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override // defpackage.r90
    public String f() {
        return this.e;
    }

    @Override // defpackage.r90
    public r90.d g() {
        return this.i;
    }

    @Override // defpackage.r90
    public int h() {
        return this.d;
    }

    public int hashCode() {
        int hashCode = (((((((((((this.b.hashCode() ^ 1000003) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d) * 1000003) ^ this.e.hashCode()) * 1000003) ^ this.f.hashCode()) * 1000003) ^ this.g.hashCode()) * 1000003;
        r90.e eVar = this.h;
        int hashCode2 = (hashCode ^ (eVar == null ? 0 : eVar.hashCode())) * 1000003;
        r90.d dVar = this.i;
        return hashCode2 ^ (dVar != null ? dVar.hashCode() : 0);
    }

    @Override // defpackage.r90
    public String i() {
        return this.b;
    }

    @Override // defpackage.r90
    public r90.e j() {
        return this.h;
    }

    @Override // defpackage.r90
    public r90.b k() {
        return new b(this);
    }

    public String toString() {
        return "CrashlyticsReport{sdkVersion=" + this.b + ", gmpAppId=" + this.c + ", platform=" + this.d + ", installationUuid=" + this.e + ", buildVersion=" + this.f + ", displayVersion=" + this.g + ", session=" + this.h + ", ndkPayload=" + this.i + "}";
    }

    public lk(String str, String str2, int i, String str3, String str4, String str5, r90.e eVar, r90.d dVar) {
        this.b = str;
        this.c = str2;
        this.d = i;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = eVar;
        this.i = dVar;
    }
}
