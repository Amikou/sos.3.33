package defpackage;

import androidx.media3.common.util.b;
import androidx.media3.exoplayer.m;
import androidx.media3.exoplayer.trackselection.c;
import zendesk.support.request.CellBase;

/* compiled from: DefaultLoadControl.java */
/* renamed from: yj0  reason: default package */
/* loaded from: classes.dex */
public class yj0 implements s02 {
    public final mf0 a;
    public final long b;
    public final long c;
    public final long d;
    public final long e;
    public final int f;
    public final boolean g;
    public final long h;
    public final boolean i;
    public int j;
    public boolean k;

    public yj0() {
        this(new mf0(true, 65536), 50000, 50000, 2500, 5000, -1, false, 0, false);
    }

    public static void j(int i, int i2, String str, String str2) {
        boolean z = i >= i2;
        ii.b(z, str + " cannot be less than " + str2);
    }

    public static int l(int i) {
        switch (i) {
            case -2:
                return 0;
            case -1:
            default:
                throw new IllegalArgumentException();
            case 0:
                return 144310272;
            case 1:
                return 13107200;
            case 2:
                return 131072000;
            case 3:
            case 4:
            case 5:
            case 6:
                return 131072;
        }
    }

    @Override // defpackage.s02
    public boolean a() {
        return this.i;
    }

    @Override // defpackage.s02
    public long b() {
        return this.h;
    }

    @Override // defpackage.s02
    public void c() {
        m(false);
    }

    @Override // defpackage.s02
    public void d(m[] mVarArr, c84 c84Var, c[] cVarArr) {
        int i = this.f;
        if (i == -1) {
            i = k(mVarArr, cVarArr);
        }
        this.j = i;
        this.a.h(i);
    }

    @Override // defpackage.s02
    public void e() {
        m(true);
    }

    @Override // defpackage.s02
    public boolean f(long j, float f, boolean z, long j2) {
        long b0 = b.b0(j, f);
        long j3 = z ? this.e : this.d;
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j3 = Math.min(j2 / 2, j3);
        }
        return j3 <= 0 || b0 >= j3 || (!this.g && this.a.f() >= this.j);
    }

    @Override // defpackage.s02
    public boolean g(long j, long j2, float f) {
        boolean z = true;
        boolean z2 = this.a.f() >= this.j;
        long j3 = this.b;
        if (f > 1.0f) {
            j3 = Math.min(b.W(j3, f), this.c);
        }
        if (j2 < Math.max(j3, 500000L)) {
            if (!this.g && z2) {
                z = false;
            }
            this.k = z;
            if (!z && j2 < 500000) {
                p12.i("DefaultLoadControl", "Target buffer size reached with less than 500ms of buffered media data.");
            }
        } else if (j2 >= this.c || z2) {
            this.k = false;
        }
        return this.k;
    }

    @Override // defpackage.s02
    public gb h() {
        return this.a;
    }

    @Override // defpackage.s02
    public void i() {
        m(true);
    }

    public int k(m[] mVarArr, c[] cVarArr) {
        int i = 0;
        for (int i2 = 0; i2 < mVarArr.length; i2++) {
            if (cVarArr[i2] != null) {
                i += l(mVarArr[i2].h());
            }
        }
        return Math.max(13107200, i);
    }

    public final void m(boolean z) {
        int i = this.f;
        if (i == -1) {
            i = 13107200;
        }
        this.j = i;
        this.k = false;
        if (z) {
            this.a.g();
        }
    }

    public yj0(mf0 mf0Var, int i, int i2, int i3, int i4, int i5, boolean z, int i6, boolean z2) {
        j(i3, 0, "bufferForPlaybackMs", "0");
        j(i4, 0, "bufferForPlaybackAfterRebufferMs", "0");
        j(i, i3, "minBufferMs", "bufferForPlaybackMs");
        j(i, i4, "minBufferMs", "bufferForPlaybackAfterRebufferMs");
        j(i2, i, "maxBufferMs", "minBufferMs");
        j(i6, 0, "backBufferDurationMs", "0");
        this.a = mf0Var;
        this.b = b.y0(i);
        this.c = b.y0(i2);
        this.d = b.y0(i3);
        this.e = b.y0(i4);
        this.f = i5;
        this.j = i5 == -1 ? 13107200 : i5;
        this.g = z;
        this.h = b.y0(i6);
        this.i = z2;
    }
}
