package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import java.util.List;

/* compiled from: Player.java */
/* renamed from: nr2  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class nr2 {
    public static void A(q.d dVar, int i, int i2) {
    }

    public static void B(q.d dVar, u uVar, int i) {
    }

    public static void C(q.d dVar, x xVar) {
    }

    public static void D(q.d dVar, y yVar) {
    }

    public static void E(q.d dVar, z zVar) {
    }

    public static void a(q.d dVar, q.b bVar) {
    }

    public static void b(q.d dVar, nb0 nb0Var) {
    }

    @Deprecated
    public static void c(q.d dVar, List list) {
    }

    public static void d(q.d dVar, h hVar) {
    }

    public static void e(q.d dVar, int i, boolean z) {
    }

    public static void f(q.d dVar, q qVar, q.c cVar) {
    }

    public static void g(q.d dVar, boolean z) {
    }

    public static void h(q.d dVar, boolean z) {
    }

    @Deprecated
    public static void i(q.d dVar, boolean z) {
    }

    public static void j(q.d dVar, m mVar, int i) {
    }

    public static void k(q.d dVar, n nVar) {
    }

    public static void l(q.d dVar, Metadata metadata) {
    }

    public static void m(q.d dVar, boolean z, int i) {
    }

    public static void n(q.d dVar, p pVar) {
    }

    public static void o(q.d dVar, int i) {
    }

    public static void p(q.d dVar, int i) {
    }

    public static void q(q.d dVar, PlaybackException playbackException) {
    }

    public static void r(q.d dVar, PlaybackException playbackException) {
    }

    @Deprecated
    public static void s(q.d dVar, boolean z, int i) {
    }

    @Deprecated
    public static void t(q.d dVar, int i) {
    }

    public static void u(q.d dVar, q.e eVar, q.e eVar2, int i) {
    }

    public static void v(q.d dVar) {
    }

    public static void w(q.d dVar, int i) {
    }

    @Deprecated
    public static void x(q.d dVar) {
    }

    public static void y(q.d dVar, boolean z) {
    }

    public static void z(q.d dVar, boolean z) {
    }
}
