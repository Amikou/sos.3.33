package defpackage;

import androidx.constraintlayout.core.c;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.b;
import androidx.constraintlayout.core.widgets.d;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: WidgetGroup.java */
/* renamed from: dp4  reason: default package */
/* loaded from: classes.dex */
public class dp4 {
    public static int f;
    public int b;
    public int c;
    public ArrayList<ConstraintWidget> a = new ArrayList<>();
    public ArrayList<a> d = null;
    public int e = -1;

    /* compiled from: WidgetGroup.java */
    /* renamed from: dp4$a */
    /* loaded from: classes.dex */
    public class a {
        public a(dp4 dp4Var, ConstraintWidget constraintWidget, c cVar, int i) {
            new WeakReference(constraintWidget);
            cVar.x(constraintWidget.M);
            cVar.x(constraintWidget.N);
            cVar.x(constraintWidget.O);
            cVar.x(constraintWidget.P);
            cVar.x(constraintWidget.Q);
        }
    }

    public dp4(int i) {
        this.b = -1;
        this.c = 0;
        int i2 = f;
        f = i2 + 1;
        this.b = i2;
        this.c = i;
    }

    public boolean a(ConstraintWidget constraintWidget) {
        if (this.a.contains(constraintWidget)) {
            return false;
        }
        this.a.add(constraintWidget);
        return true;
    }

    public void b(ArrayList<dp4> arrayList) {
        int size = this.a.size();
        if (this.e != -1 && size > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                dp4 dp4Var = arrayList.get(i);
                if (this.e == dp4Var.b) {
                    g(this.c, dp4Var);
                }
            }
        }
        if (size == 0) {
            arrayList.remove(this);
        }
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.c;
    }

    public final String e() {
        int i = this.c;
        return i == 0 ? "Horizontal" : i == 1 ? "Vertical" : i == 2 ? "Both" : "Unknown";
    }

    public int f(c cVar, int i) {
        if (this.a.size() == 0) {
            return 0;
        }
        return j(cVar, this.a, i);
    }

    public void g(int i, dp4 dp4Var) {
        Iterator<ConstraintWidget> it = this.a.iterator();
        while (it.hasNext()) {
            ConstraintWidget next = it.next();
            dp4Var.a(next);
            if (i == 0) {
                next.M0 = dp4Var.c();
            } else {
                next.N0 = dp4Var.c();
            }
        }
        this.e = dp4Var.b;
    }

    public void h(boolean z) {
    }

    public void i(int i) {
        this.c = i;
    }

    public final int j(c cVar, ArrayList<ConstraintWidget> arrayList, int i) {
        int x;
        int x2;
        d dVar = (d) arrayList.get(0).M();
        cVar.D();
        dVar.g(cVar, false);
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            arrayList.get(i2).g(cVar, false);
        }
        if (i == 0 && dVar.Y0 > 0) {
            b.b(dVar, cVar, arrayList, 0);
        }
        if (i == 1 && dVar.Z0 > 0) {
            b.b(dVar, cVar, arrayList, 1);
        }
        try {
            cVar.z();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.d = new ArrayList<>();
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            this.d.add(new a(this, arrayList.get(i3), cVar, i));
        }
        if (i == 0) {
            x = cVar.x(dVar.M);
            x2 = cVar.x(dVar.O);
            cVar.D();
        } else {
            x = cVar.x(dVar.N);
            x2 = cVar.x(dVar.P);
            cVar.D();
        }
        return x2 - x;
    }

    public String toString() {
        Iterator<ConstraintWidget> it;
        String str = e() + " [" + this.b + "] <";
        while (this.a.iterator().hasNext()) {
            str = str + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + it.next().v();
        }
        return str + " >";
    }
}
