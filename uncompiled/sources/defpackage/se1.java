package defpackage;

import android.content.Context;
import android.content.Intent;

/* compiled from: GenerateNotificationOpenIntent.kt */
/* renamed from: se1  reason: default package */
/* loaded from: classes2.dex */
public final class se1 {
    public final Context a;
    public final Intent b;
    public final boolean c;

    public se1(Context context, Intent intent, boolean z) {
        fs1.f(context, "context");
        this.a = context;
        this.b = intent;
        this.c = z;
    }

    public final Intent a() {
        Intent launchIntentForPackage;
        if (this.c && (launchIntentForPackage = this.a.getPackageManager().getLaunchIntentForPackage(this.a.getPackageName())) != null) {
            fs1.e(launchIntentForPackage, "context.packageManager.g…           ?: return null");
            launchIntentForPackage.setPackage(null);
            launchIntentForPackage.setFlags(270532608);
            return launchIntentForPackage;
        }
        return null;
    }

    public final Intent b() {
        Intent intent = this.b;
        return intent != null ? intent : a();
    }
}
