package defpackage;

import androidx.paging.LoadType;

/* compiled from: RemoteMediatorAccessor.kt */
/* renamed from: p63  reason: default package */
/* loaded from: classes.dex */
public interface p63<Key, Value> {
    void a(LoadType loadType, ip2<Key, Value> ip2Var);

    void c(ip2<Key, Value> ip2Var);
}
