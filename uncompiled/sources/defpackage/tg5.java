package defpackage;

import com.google.android.gms.internal.measurement.zzep;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: tg5  reason: default package */
/* loaded from: classes.dex */
public final class tg5 implements sv5 {
    public static final sv5 a = new tg5();

    @Override // defpackage.sv5
    public final boolean d(int i) {
        return zzep.zza(i) != null;
    }
}
