package defpackage;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.RecentlyNonNull;
import androidx.fragment.app.FragmentManager;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: iw3  reason: default package */
/* loaded from: classes.dex */
public class iw3 extends sn0 {
    public Dialog u0;
    public DialogInterface.OnCancelListener v0;

    public static iw3 v(@RecentlyNonNull Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        iw3 iw3Var = new iw3();
        Dialog dialog2 = (Dialog) zt2.k(dialog, "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        iw3Var.u0 = dialog2;
        if (onCancelListener != null) {
            iw3Var.v0 = onCancelListener;
        }
        return iw3Var;
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        if (this.u0 == null) {
            r(false);
        }
        return this.u0;
    }

    @Override // defpackage.sn0, android.content.DialogInterface.OnCancelListener
    public void onCancel(@RecentlyNonNull DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.v0;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @Override // defpackage.sn0
    public void u(@RecentlyNonNull FragmentManager fragmentManager, String str) {
        super.u(fragmentManager, str);
    }
}
