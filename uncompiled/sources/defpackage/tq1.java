package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: InlineList.kt */
/* renamed from: tq1  reason: default package */
/* loaded from: classes2.dex */
public final class tq1<E> {
    public static <E> Object a(Object obj) {
        return obj;
    }

    public static /* synthetic */ Object b(Object obj, int i, qi0 qi0Var) {
        if ((i & 1) != 0) {
            obj = null;
        }
        return a(obj);
    }

    public static final Object c(Object obj, E e) {
        if (!ze0.a() || (!(e instanceof List))) {
            if (obj == null) {
                return a(e);
            }
            if (obj instanceof ArrayList) {
                ((ArrayList) obj).add(e);
                return a(obj);
            }
            ArrayList arrayList = new ArrayList(4);
            arrayList.add(obj);
            arrayList.add(e);
            return a(arrayList);
        }
        throw new AssertionError();
    }
}
