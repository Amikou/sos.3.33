package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;

/* compiled from: WorkForegroundRunnable.java */
/* renamed from: aq4  reason: default package */
/* loaded from: classes.dex */
public class aq4 implements Runnable {
    public static final String k0 = v12.f("WorkForegroundRunnable");
    public final wm3<Void> a = wm3.t();
    public final Context f0;
    public final tq4 g0;
    public final ListenableWorker h0;
    public final u81 i0;
    public final q34 j0;

    /* compiled from: WorkForegroundRunnable.java */
    /* renamed from: aq4$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ wm3 a;

        public a(wm3 wm3Var) {
            this.a = wm3Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.r(aq4.this.h0.d());
        }
    }

    /* compiled from: WorkForegroundRunnable.java */
    /* renamed from: aq4$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ wm3 a;

        public b(wm3 wm3Var) {
            this.a = wm3Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                s81 s81Var = (s81) this.a.get();
                if (s81Var != null) {
                    v12.c().a(aq4.k0, String.format("Updating notification for %s", aq4.this.g0.c), new Throwable[0]);
                    aq4.this.h0.n(true);
                    aq4 aq4Var = aq4.this;
                    aq4Var.a.r(aq4Var.i0.a(aq4Var.f0, aq4Var.h0.e(), s81Var));
                    return;
                }
                throw new IllegalStateException(String.format("Worker was marked important (%s) but did not provide ForegroundInfo", aq4.this.g0.c));
            } catch (Throwable th) {
                aq4.this.a.q(th);
            }
        }
    }

    @SuppressLint({"LambdaLast"})
    public aq4(Context context, tq4 tq4Var, ListenableWorker listenableWorker, u81 u81Var, q34 q34Var) {
        this.f0 = context;
        this.g0 = tq4Var;
        this.h0 = listenableWorker;
        this.i0 = u81Var;
        this.j0 = q34Var;
    }

    public l02<Void> a() {
        return this.a;
    }

    @Override // java.lang.Runnable
    @SuppressLint({"UnsafeExperimentalUsageError"})
    public void run() {
        if (this.g0.q && !yr.c()) {
            wm3 t = wm3.t();
            this.j0.a().execute(new a(t));
            t.d(new b(t), this.j0.a());
            return;
        }
        this.a.p(null);
    }
}
