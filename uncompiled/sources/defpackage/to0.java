package defpackage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/* compiled from: DigestUtils.java */
/* renamed from: to0  reason: default package */
/* loaded from: classes2.dex */
public class to0 {
    public static byte[] a(String str, String str2) {
        if (ru3.b(str) && ru3.b(str2)) {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance(str);
                messageDigest.update(str2.getBytes());
                return messageDigest.digest();
            } catch (NoSuchAlgorithmException unused) {
                return new byte[0];
            }
        }
        return new byte[0];
    }

    public static String b(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            sb.append(String.format(Locale.US, "%02x", Integer.valueOf(b & 255)));
        }
        return sb.toString();
    }

    public static String c(String str) {
        return ru3.b(str) ? b(a("SHA-1", str)) : "";
    }
}
