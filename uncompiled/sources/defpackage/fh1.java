package defpackage;

import com.google.android.gms.common.api.GoogleApiClient;

/* compiled from: GoogleApiClientCompatProxy.java */
/* renamed from: fh1  reason: default package */
/* loaded from: classes2.dex */
public class fh1 {
    public final GoogleApiClient a;
    public final Class b;

    public fh1(GoogleApiClient googleApiClient) {
        this.a = googleApiClient;
        this.b = googleApiClient.getClass();
    }

    public void a() {
        try {
            this.b.getMethod("connect", new Class[0]).invoke(this.a, new Object[0]);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void b() {
        try {
            this.b.getMethod("disconnect", new Class[0]).invoke(this.a, new Object[0]);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public GoogleApiClient c() {
        return this.a;
    }
}
