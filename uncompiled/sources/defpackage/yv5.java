package defpackage;

import android.os.Bundle;
import com.google.android.gms.measurement.internal.zzas;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: yv5  reason: default package */
/* loaded from: classes.dex */
public final class yv5 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ String f0 = "_err";
    public final /* synthetic */ Bundle g0;
    public final /* synthetic */ bw5 h0;

    public yv5(bw5 bw5Var, String str, String str2, Bundle bundle) {
        this.h0 = bw5Var;
        this.a = str;
        this.g0 = bundle;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.h0.a.h0((zzas) zt2.j(this.h0.a.c0().J(this.a, this.f0, this.g0, "auto", this.h0.a.a().a(), false, false)), this.a);
    }
}
