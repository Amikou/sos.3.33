package defpackage;

import androidx.annotation.RecentlyNonNull;
import defpackage.l83;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: m83  reason: default package */
/* loaded from: classes.dex */
public interface m83<R extends l83> {
    void a(@RecentlyNonNull R r);
}
