package defpackage;

import java.lang.reflect.Method;
import kotlin.coroutines.jvm.internal.BaseContinuationImpl;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: DebugMetadata.kt */
/* renamed from: n92  reason: default package */
/* loaded from: classes2.dex */
public final class n92 {
    public static a b;
    public static final n92 c = new n92();
    public static final a a = new a(null, null, null);

    /* compiled from: DebugMetadata.kt */
    /* renamed from: n92$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public final Method a;
        public final Method b;
        public final Method c;

        public a(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }
    }

    public final a a(BaseContinuationImpl baseContinuationImpl) {
        try {
            a aVar = new a(Class.class.getDeclaredMethod("getModule", new Class[0]), baseContinuationImpl.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), baseContinuationImpl.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod(PublicResolver.FUNC_NAME, new Class[0]));
            b = aVar;
            return aVar;
        } catch (Exception unused) {
            a aVar2 = a;
            b = aVar2;
            return aVar2;
        }
    }

    public final String b(BaseContinuationImpl baseContinuationImpl) {
        Method method;
        Object invoke;
        Method method2;
        Object invoke2;
        fs1.f(baseContinuationImpl, "continuation");
        a aVar = b;
        if (aVar == null) {
            aVar = a(baseContinuationImpl);
        }
        if (aVar == a || (method = aVar.a) == null || (invoke = method.invoke(baseContinuationImpl.getClass(), new Object[0])) == null || (method2 = aVar.b) == null || (invoke2 = method2.invoke(invoke, new Object[0])) == null) {
            return null;
        }
        Method method3 = aVar.c;
        Object invoke3 = method3 != null ? method3.invoke(invoke2, new Object[0]) : null;
        return invoke3 instanceof String ? invoke3 : null;
    }
}
