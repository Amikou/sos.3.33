package defpackage;

import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: gg5  reason: default package */
/* loaded from: classes.dex */
public final class gg5 implements Runnable {
    public final /* synthetic */ int a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ Object g0;
    public final /* synthetic */ Object h0;
    public final /* synthetic */ Object i0;
    public final /* synthetic */ og5 j0;

    public gg5(og5 og5Var, int i, String str, Object obj, Object obj2, Object obj3) {
        this.j0 = og5Var;
        this.a = i;
        this.f0 = str;
        this.g0 = obj;
        this.h0 = obj2;
        this.i0 = obj3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        char c;
        long j;
        char c2;
        long j2;
        mi5 A = this.j0.a.A();
        if (A.h()) {
            c = this.j0.c;
            if (c == 0) {
                if (this.j0.a.z().o()) {
                    og5 og5Var = this.j0;
                    og5Var.a.b();
                    og5Var.c = 'C';
                } else {
                    og5 og5Var2 = this.j0;
                    og5Var2.a.b();
                    og5Var2.c = 'c';
                }
            }
            j = this.j0.d;
            if (j < 0) {
                og5 og5Var3 = this.j0;
                og5Var3.a.z().n();
                og5Var3.d = 42004L;
            }
            char charAt = "01VDIWEA?".charAt(this.a);
            c2 = this.j0.c;
            j2 = this.j0.d;
            String A2 = og5.A(true, this.f0, this.g0, this.h0, this.i0);
            StringBuilder sb = new StringBuilder(String.valueOf(A2).length() + 24);
            sb.append("2");
            sb.append(charAt);
            sb.append(c2);
            sb.append(j2);
            sb.append(":");
            sb.append(A2);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.f0.substring(0, RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
            }
            rh5 rh5Var = A.d;
            if (rh5Var != null) {
                rh5Var.a(sb2, 1L);
                return;
            }
            return;
        }
        Log.println(6, this.j0.z(), "Persisted config not initialized. Not logging error/warn");
    }
}
