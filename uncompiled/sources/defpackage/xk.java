package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution.java */
/* renamed from: xk  reason: default package */
/* loaded from: classes2.dex */
public final class xk extends r90.e.d.a.b {
    public final ip1<r90.e.d.a.b.AbstractC0272e> a;
    public final r90.e.d.a.b.c b;
    public final r90.a c;
    public final r90.e.d.a.b.AbstractC0270d d;
    public final ip1<r90.e.d.a.b.AbstractC0266a> e;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution.java */
    /* renamed from: xk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.b.AbstractC0268b {
        public ip1<r90.e.d.a.b.AbstractC0272e> a;
        public r90.e.d.a.b.c b;
        public r90.a c;
        public r90.e.d.a.b.AbstractC0270d d;
        public ip1<r90.e.d.a.b.AbstractC0266a> e;

        @Override // defpackage.r90.e.d.a.b.AbstractC0268b
        public r90.e.d.a.b a() {
            String str = "";
            if (this.d == null) {
                str = " signal";
            }
            if (this.e == null) {
                str = str + " binaries";
            }
            if (str.isEmpty()) {
                return new xk(this.a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0268b
        public r90.e.d.a.b.AbstractC0268b b(r90.a aVar) {
            this.c = aVar;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0268b
        public r90.e.d.a.b.AbstractC0268b c(ip1<r90.e.d.a.b.AbstractC0266a> ip1Var) {
            Objects.requireNonNull(ip1Var, "Null binaries");
            this.e = ip1Var;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0268b
        public r90.e.d.a.b.AbstractC0268b d(r90.e.d.a.b.c cVar) {
            this.b = cVar;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0268b
        public r90.e.d.a.b.AbstractC0268b e(r90.e.d.a.b.AbstractC0270d abstractC0270d) {
            Objects.requireNonNull(abstractC0270d, "Null signal");
            this.d = abstractC0270d;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0268b
        public r90.e.d.a.b.AbstractC0268b f(ip1<r90.e.d.a.b.AbstractC0272e> ip1Var) {
            this.a = ip1Var;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.a.b
    public r90.a b() {
        return this.c;
    }

    @Override // defpackage.r90.e.d.a.b
    public ip1<r90.e.d.a.b.AbstractC0266a> c() {
        return this.e;
    }

    @Override // defpackage.r90.e.d.a.b
    public r90.e.d.a.b.c d() {
        return this.b;
    }

    @Override // defpackage.r90.e.d.a.b
    public r90.e.d.a.b.AbstractC0270d e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a.b) {
            r90.e.d.a.b bVar = (r90.e.d.a.b) obj;
            ip1<r90.e.d.a.b.AbstractC0272e> ip1Var = this.a;
            if (ip1Var != null ? ip1Var.equals(bVar.f()) : bVar.f() == null) {
                r90.e.d.a.b.c cVar = this.b;
                if (cVar != null ? cVar.equals(bVar.d()) : bVar.d() == null) {
                    r90.a aVar = this.c;
                    if (aVar != null ? aVar.equals(bVar.b()) : bVar.b() == null) {
                        if (this.d.equals(bVar.e()) && this.e.equals(bVar.c())) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }

    @Override // defpackage.r90.e.d.a.b
    public ip1<r90.e.d.a.b.AbstractC0272e> f() {
        return this.a;
    }

    public int hashCode() {
        ip1<r90.e.d.a.b.AbstractC0272e> ip1Var = this.a;
        int hashCode = ((ip1Var == null ? 0 : ip1Var.hashCode()) ^ 1000003) * 1000003;
        r90.e.d.a.b.c cVar = this.b;
        int hashCode2 = (hashCode ^ (cVar == null ? 0 : cVar.hashCode())) * 1000003;
        r90.a aVar = this.c;
        return ((((hashCode2 ^ (aVar != null ? aVar.hashCode() : 0)) * 1000003) ^ this.d.hashCode()) * 1000003) ^ this.e.hashCode();
    }

    public String toString() {
        return "Execution{threads=" + this.a + ", exception=" + this.b + ", appExitInfo=" + this.c + ", signal=" + this.d + ", binaries=" + this.e + "}";
    }

    public xk(ip1<r90.e.d.a.b.AbstractC0272e> ip1Var, r90.e.d.a.b.c cVar, r90.a aVar, r90.e.d.a.b.AbstractC0270d abstractC0270d, ip1<r90.e.d.a.b.AbstractC0266a> ip1Var2) {
        this.a = ip1Var;
        this.b = cVar;
        this.c = aVar;
        this.d = abstractC0270d;
        this.e = ip1Var2;
    }
}
