package defpackage;

import android.graphics.Bitmap;

/* compiled from: AnimatedImageFrame.java */
/* renamed from: xd  reason: default package */
/* loaded from: classes.dex */
public interface xd {
    void a();

    void b(int i, int i2, Bitmap bitmap);

    int c();

    int d();

    int getHeight();

    int getWidth();
}
