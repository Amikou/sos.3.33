package defpackage;

import com.google.auto.value.AutoValue;
import defpackage.kl;

/* compiled from: EventStoreConfig.java */
@AutoValue
/* renamed from: ey0  reason: default package */
/* loaded from: classes.dex */
public abstract class ey0 {
    public static final ey0 a = a().f(10485760).d(200).b(10000).c(604800000).e(81920).a();

    /* compiled from: EventStoreConfig.java */
    @AutoValue.Builder
    /* renamed from: ey0$a */
    /* loaded from: classes.dex */
    public static abstract class a {
        public abstract ey0 a();

        public abstract a b(int i);

        public abstract a c(long j);

        public abstract a d(int i);

        public abstract a e(int i);

        public abstract a f(long j);
    }

    public static a a() {
        return new kl.b();
    }

    public abstract int b();

    public abstract long c();

    public abstract int d();

    public abstract int e();

    public abstract long f();
}
