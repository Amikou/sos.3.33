package defpackage;

import java.util.List;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: IFilterTokenListByPhraseUseCase.kt */
/* renamed from: zl1  reason: default package */
/* loaded from: classes2.dex */
public interface zl1 {
    List<IToken> a(List<? extends IToken> list, String str);
}
