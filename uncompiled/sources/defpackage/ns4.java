package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Yield.kt */
/* renamed from: ns4  reason: default package */
/* loaded from: classes2.dex */
public final class ns4 {
    public static final Object a(q70<? super te4> q70Var) {
        Object d;
        CoroutineContext context = q70Var.getContext();
        xt1.j(context);
        q70 c = IntrinsicsKt__IntrinsicsJvmKt.c(q70Var);
        np0 np0Var = c instanceof np0 ? (np0) c : null;
        if (np0Var == null) {
            d = te4.a;
        } else {
            if (np0Var.h0.j(context)) {
                np0Var.n(context, te4.a);
            } else {
                ms4 ms4Var = new ms4();
                CoroutineContext plus = context.plus(ms4Var);
                te4 te4Var = te4.a;
                np0Var.n(plus, te4Var);
                if (ms4Var.a) {
                    d = op0.d(np0Var) ? gs1.d() : te4Var;
                }
            }
            d = gs1.d();
        }
        if (d == gs1.d()) {
            ef0.c(q70Var);
        }
        return d == gs1.d() ? d : te4.a;
    }
}
