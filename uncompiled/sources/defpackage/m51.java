package defpackage;

import com.google.android.gms.tasks.c;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-iid-interop@@17.1.0 */
/* renamed from: m51  reason: default package */
/* loaded from: classes2.dex */
public interface m51 {

    /* compiled from: com.google.firebase:firebase-iid-interop@@17.1.0 */
    /* renamed from: m51$a */
    /* loaded from: classes2.dex */
    public interface a {
    }

    void a(String str, String str2) throws IOException;

    c<String> b();

    void c(a aVar);

    String d();
}
