package defpackage;

import android.net.Uri;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: vn5  reason: default package */
/* loaded from: classes.dex */
public final class vn5 {
    public final Map<String, Map<String, String>> a;

    public vn5(Map<String, Map<String, String>> map) {
        this.a = map;
    }

    public final String a(Uri uri, String str, String str2, String str3) {
        if (uri != null) {
            Map<String, String> map = this.a.get(uri.toString());
            if (map == null) {
                return null;
            }
            String valueOf = String.valueOf(str3);
            return map.get(valueOf.length() != 0 ? "".concat(valueOf) : new String(""));
        }
        return null;
    }
}
