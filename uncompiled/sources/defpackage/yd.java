package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;
import java.util.List;

/* compiled from: AnimatedImageResult.java */
/* renamed from: yd  reason: default package */
/* loaded from: classes.dex */
public class yd {
    public final td a;
    public a<Bitmap> b;
    public List<a<Bitmap>> c;
    public pq d;

    public yd(zd zdVar) {
        this.a = (td) xt2.g(zdVar.e());
        zdVar.d();
        this.b = zdVar.f();
        this.c = zdVar.c();
        this.d = zdVar.b();
    }

    public static yd b(td tdVar) {
        return new yd(tdVar);
    }

    public static zd e(td tdVar) {
        return new zd(tdVar);
    }

    public synchronized void a() {
        a.g(this.b);
        this.b = null;
        a.h(this.c);
        this.c = null;
    }

    public pq c() {
        return this.d;
    }

    public td d() {
        return this.a;
    }

    public yd(td tdVar) {
        this.a = (td) xt2.g(tdVar);
    }
}
