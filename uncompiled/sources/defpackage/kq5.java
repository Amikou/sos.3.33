package defpackage;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: kq5  reason: default package */
/* loaded from: classes.dex */
public final class kq5 extends WeakReference<Throwable> {
    public final int a;

    public kq5(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        this.a = System.identityHashCode(th);
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == kq5.class) {
            if (this == obj) {
                return true;
            }
            kq5 kq5Var = (kq5) obj;
            if (this.a == kq5Var.a && get() == kq5Var.get()) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return this.a;
    }
}
