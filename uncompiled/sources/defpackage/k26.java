package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: k26  reason: default package */
/* loaded from: classes.dex */
public final class k26 implements j26 {
    public static final wo5<Boolean> a;
    public static final wo5<Boolean> b;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        ro5Var.a("measurement.id.lifecycle.app_in_background_parameter", 0L);
        a = ro5Var.b("measurement.lifecycle.app_backgrounded_engagement", false);
        ro5Var.b("measurement.lifecycle.app_backgrounded_tracking", true);
        b = ro5Var.b("measurement.lifecycle.app_in_background_parameter", false);
        ro5Var.a("measurement.id.lifecycle.app_backgrounded_tracking", 0L);
    }

    @Override // defpackage.j26
    public final boolean zza() {
        return a.e().booleanValue();
    }

    @Override // defpackage.j26
    public final boolean zzb() {
        return b.e().booleanValue();
    }
}
