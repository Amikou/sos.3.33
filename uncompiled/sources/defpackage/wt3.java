package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: wt3  reason: default package */
/* loaded from: classes.dex */
public class wt3 {
    public static final Lock c = new ReentrantLock();
    public static wt3 d;
    public final Lock a = new ReentrantLock();
    public final SharedPreferences b;

    public wt3(Context context) {
        this.b = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    @RecentlyNonNull
    public static wt3 a(@RecentlyNonNull Context context) {
        zt2.j(context);
        Lock lock = c;
        lock.lock();
        try {
            if (d == null) {
                d = new wt3(context.getApplicationContext());
            }
            wt3 wt3Var = d;
            lock.unlock();
            return wt3Var;
        } catch (Throwable th) {
            c.unlock();
            throw th;
        }
    }

    public static String d(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        return sb.toString();
    }

    @RecentlyNullable
    public GoogleSignInAccount b() {
        return c(e("defaultGoogleSignInAccount"));
    }

    public final GoogleSignInAccount c(String str) {
        String e;
        if (!TextUtils.isEmpty(str) && (e = e(d("googleSignInAccount", str))) != null) {
            try {
                return GoogleSignInAccount.R1(e);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public final String e(String str) {
        this.a.lock();
        try {
            return this.b.getString(str, null);
        } finally {
            this.a.unlock();
        }
    }
}
