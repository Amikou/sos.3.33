package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.Group;
import androidx.core.widget.NestedScrollView;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityWalletManageBinding.java */
/* renamed from: d8  reason: default package */
/* loaded from: classes2.dex */
public final class d8 {
    public final NestedScrollView a;
    public final CardView b;
    public final CardView c;
    public final Group d;
    public final Group e;
    public final Group f;
    public final AppCompatImageView g;
    public final TextInputLayout h;
    public final i74 i;
    public final TextView j;
    public final TextView k;
    public final TextView l;
    public final TextView m;
    public final TextView n;
    public final TextView o;
    public final TextView p;
    public final TextView q;
    public final TextView r;
    public final TextView s;
    public final TextView t;
    public final TextView u;
    public final TextView v;
    public final TextView w;
    public final TextView x;

    public d8(NestedScrollView nestedScrollView, CardView cardView, CardView cardView2, CardView cardView3, CardView cardView4, CardView cardView5, CardView cardView6, Group group, Group group2, Group group3, AppCompatImageView appCompatImageView, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextInputLayout textInputLayout, i74 i74Var, TextView textView7, TextView textView8, TextView textView9, TextView textView10, TextView textView11, TextView textView12, TextView textView13, TextView textView14, TextView textView15, TextView textView16, TextView textView17, TextView textView18, TextView textView19, TextView textView20, TextView textView21) {
        this.a = nestedScrollView;
        this.b = cardView;
        this.c = cardView3;
        this.d = group;
        this.e = group2;
        this.f = group3;
        this.g = appCompatImageView;
        this.h = textInputLayout;
        this.i = i74Var;
        this.j = textView7;
        this.k = textView8;
        this.l = textView9;
        this.m = textView10;
        this.n = textView11;
        this.o = textView12;
        this.p = textView13;
        this.q = textView14;
        this.r = textView15;
        this.s = textView16;
        this.t = textView17;
        this.u = textView18;
        this.v = textView19;
        this.w = textView20;
        this.x = textView21;
    }

    public static d8 a(View view) {
        int i = R.id.cvPassPhrase;
        CardView cardView = (CardView) ai4.a(view, R.id.cvPassPhrase);
        if (cardView != null) {
            i = R.id.cvPrivateKey;
            CardView cardView2 = (CardView) ai4.a(view, R.id.cvPrivateKey);
            if (cardView2 != null) {
                i = R.id.cvRemoveWallet;
                CardView cardView3 = (CardView) ai4.a(view, R.id.cvRemoveWallet);
                if (cardView3 != null) {
                    i = R.id.cvWalletAddress;
                    CardView cardView4 = (CardView) ai4.a(view, R.id.cvWalletAddress);
                    if (cardView4 != null) {
                        i = R.id.cvWalletLink;
                        CardView cardView5 = (CardView) ai4.a(view, R.id.cvWalletLink);
                        if (cardView5 != null) {
                            i = R.id.cvWalletName;
                            CardView cardView6 = (CardView) ai4.a(view, R.id.cvWalletName);
                            if (cardView6 != null) {
                                i = R.id.gp_pass;
                                Group group = (Group) ai4.a(view, R.id.gp_pass);
                                if (group != null) {
                                    i = R.id.group;
                                    Group group2 = (Group) ai4.a(view, R.id.group);
                                    if (group2 != null) {
                                        i = R.id.group2;
                                        Group group3 = (Group) ai4.a(view, R.id.group2);
                                        if (group3 != null) {
                                            i = R.id.imWalletLink;
                                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imWalletLink);
                                            if (appCompatImageView != null) {
                                                i = R.id.textView22;
                                                TextView textView = (TextView) ai4.a(view, R.id.textView22);
                                                if (textView != null) {
                                                    i = R.id.textView25;
                                                    TextView textView2 = (TextView) ai4.a(view, R.id.textView25);
                                                    if (textView2 != null) {
                                                        i = R.id.textView35;
                                                        TextView textView3 = (TextView) ai4.a(view, R.id.textView35);
                                                        if (textView3 != null) {
                                                            i = R.id.textView8;
                                                            TextView textView4 = (TextView) ai4.a(view, R.id.textView8);
                                                            if (textView4 != null) {
                                                                i = R.id.textViewWalletAddressTitle;
                                                                TextView textView5 = (TextView) ai4.a(view, R.id.textViewWalletAddressTitle);
                                                                if (textView5 != null) {
                                                                    i = R.id.textViewWalletLinkTitle;
                                                                    TextView textView6 = (TextView) ai4.a(view, R.id.textViewWalletLinkTitle);
                                                                    if (textView6 != null) {
                                                                        i = R.id.tilWalletName;
                                                                        TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilWalletName);
                                                                        if (textInputLayout != null) {
                                                                            i = R.id.toolbarParent;
                                                                            View a = ai4.a(view, R.id.toolbarParent);
                                                                            if (a != null) {
                                                                                i74 a2 = i74.a(a);
                                                                                i = R.id.tv_copy;
                                                                                TextView textView7 = (TextView) ai4.a(view, R.id.tv_copy);
                                                                                if (textView7 != null) {
                                                                                    i = R.id.tv_passpharese_reveal;
                                                                                    TextView textView8 = (TextView) ai4.a(view, R.id.tv_passpharese_reveal);
                                                                                    if (textView8 != null) {
                                                                                        i = R.id.tv_passphrase;
                                                                                        TextView textView9 = (TextView) ai4.a(view, R.id.tv_passphrase);
                                                                                        if (textView9 != null) {
                                                                                            i = R.id.tvPassphraseSecurityDescription;
                                                                                            TextView textView10 = (TextView) ai4.a(view, R.id.tvPassphraseSecurityDescription);
                                                                                            if (textView10 != null) {
                                                                                                i = R.id.tv_private;
                                                                                                TextView textView11 = (TextView) ai4.a(view, R.id.tv_private);
                                                                                                if (textView11 != null) {
                                                                                                    i = R.id.tv_private_copy;
                                                                                                    TextView textView12 = (TextView) ai4.a(view, R.id.tv_private_copy);
                                                                                                    if (textView12 != null) {
                                                                                                        i = R.id.tv_private_key_reveal;
                                                                                                        TextView textView13 = (TextView) ai4.a(view, R.id.tv_private_key_reveal);
                                                                                                        if (textView13 != null) {
                                                                                                            i = R.id.tvPrivateKeySecurityDescription;
                                                                                                            TextView textView14 = (TextView) ai4.a(view, R.id.tvPrivateKeySecurityDescription);
                                                                                                            if (textView14 != null) {
                                                                                                                i = R.id.tv_remove_wallet;
                                                                                                                TextView textView15 = (TextView) ai4.a(view, R.id.tv_remove_wallet);
                                                                                                                if (textView15 != null) {
                                                                                                                    i = R.id.tvWalletAddressCopy;
                                                                                                                    TextView textView16 = (TextView) ai4.a(view, R.id.tvWalletAddressCopy);
                                                                                                                    if (textView16 != null) {
                                                                                                                        i = R.id.tvWalletAddressDescription;
                                                                                                                        TextView textView17 = (TextView) ai4.a(view, R.id.tvWalletAddressDescription);
                                                                                                                        if (textView17 != null) {
                                                                                                                            i = R.id.walletLinkAvaxC;
                                                                                                                            TextView textView18 = (TextView) ai4.a(view, R.id.walletLinkAvaxC);
                                                                                                                            if (textView18 != null) {
                                                                                                                                i = R.id.walletLinkBscscan;
                                                                                                                                TextView textView19 = (TextView) ai4.a(view, R.id.walletLinkBscscan);
                                                                                                                                if (textView19 != null) {
                                                                                                                                    i = R.id.walletLinkEtherscan;
                                                                                                                                    TextView textView20 = (TextView) ai4.a(view, R.id.walletLinkEtherscan);
                                                                                                                                    if (textView20 != null) {
                                                                                                                                        i = R.id.walletLinkMatic;
                                                                                                                                        TextView textView21 = (TextView) ai4.a(view, R.id.walletLinkMatic);
                                                                                                                                        if (textView21 != null) {
                                                                                                                                            return new d8((NestedScrollView) view, cardView, cardView2, cardView3, cardView4, cardView5, cardView6, group, group2, group3, appCompatImageView, textView, textView2, textView3, textView4, textView5, textView6, textInputLayout, a2, textView7, textView8, textView9, textView10, textView11, textView12, textView13, textView14, textView15, textView16, textView17, textView18, textView19, textView20, textView21);
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static d8 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static d8 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_wallet_manage, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public NestedScrollView b() {
        return this.a;
    }
}
