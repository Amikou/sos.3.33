package defpackage;

import java.math.BigInteger;
import java.util.ArrayList;
import net.safemoon.androidwallet.model.swap.PairsData;

/* compiled from: TradeStructure.kt */
/* renamed from: l84  reason: default package */
/* loaded from: classes2.dex */
public final class l84 {
    public final ArrayList<PairsData> a;
    public final String b;
    public final String c;
    public final BigInteger d;
    public final BigInteger e;
    public double f;

    public l84(ArrayList<PairsData> arrayList, String str, String str2, BigInteger bigInteger, BigInteger bigInteger2, double d) {
        fs1.f(arrayList, "route");
        fs1.f(str, "tokenIn");
        fs1.f(str2, "tokenOut");
        fs1.f(bigInteger, "amountIn");
        fs1.f(bigInteger2, "amountOut");
        this.a = arrayList;
        this.b = str;
        this.c = str2;
        this.d = bigInteger;
        this.e = bigInteger2;
        this.f = d;
    }

    public final BigInteger a() {
        return this.d;
    }

    public final BigInteger b() {
        return this.e;
    }

    public final double c() {
        return this.f;
    }

    public final ArrayList<PairsData> d() {
        return this.a;
    }

    public final void e(double d) {
        this.f = d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof l84) {
            l84 l84Var = (l84) obj;
            return fs1.b(this.a, l84Var.a) && fs1.b(this.b, l84Var.b) && fs1.b(this.c, l84Var.c) && fs1.b(this.d, l84Var.d) && fs1.b(this.e, l84Var.e) && fs1.b(Double.valueOf(this.f), Double.valueOf(l84Var.f));
        }
        return false;
    }

    public int hashCode() {
        return (((((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + Double.doubleToLongBits(this.f);
    }

    public String toString() {
        return "TradeStructure(route=" + this.a + ", tokenIn=" + this.b + ", tokenOut=" + this.c + ", amountIn=" + this.d + ", amountOut=" + this.e + ", priceImpact=" + this.f + ')';
    }

    public /* synthetic */ l84(ArrayList arrayList, String str, String str2, BigInteger bigInteger, BigInteger bigInteger2, double d, int i, qi0 qi0Var) {
        this(arrayList, str, str2, bigInteger, bigInteger2, (i & 32) != 0 ? 0.0d : d);
    }
}
