package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.internal.ImmutableMap;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: PostprocessorProducer.java */
/* renamed from: st2  reason: default package */
/* loaded from: classes.dex */
public class st2 implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> a;
    public final br2 b;
    public final Executor c;

    /* compiled from: PostprocessorProducer.java */
    /* renamed from: st2$b */
    /* loaded from: classes.dex */
    public class b extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final iv2 c;
        public final ev2 d;
        public final rt2 e;
        public boolean f;
        public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> g;
        public int h;
        public boolean i;
        public boolean j;

        /* compiled from: PostprocessorProducer.java */
        /* renamed from: st2$b$a */
        /* loaded from: classes.dex */
        public class a extends vn {
            public a(st2 st2Var) {
            }

            @Override // defpackage.fv2
            public void a() {
                b.this.C();
            }
        }

        /* compiled from: PostprocessorProducer.java */
        /* renamed from: st2$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class RunnableC0283b implements Runnable {
            public RunnableC0283b() {
            }

            @Override // java.lang.Runnable
            public void run() {
                com.facebook.common.references.a aVar;
                int i;
                synchronized (b.this) {
                    aVar = b.this.g;
                    i = b.this.h;
                    b.this.g = null;
                    b.this.i = false;
                }
                if (com.facebook.common.references.a.u(aVar)) {
                    try {
                        b.this.z(aVar, i);
                    } finally {
                        com.facebook.common.references.a.g(aVar);
                    }
                }
                b.this.x();
            }
        }

        public b(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, iv2 iv2Var, rt2 rt2Var, ev2 ev2Var) {
            super(l60Var);
            this.g = null;
            this.h = 0;
            this.i = false;
            this.j = false;
            this.c = iv2Var;
            this.e = rt2Var;
            this.d = ev2Var;
            ev2Var.o(new a(st2.this));
        }

        public final Map<String, String> A(iv2 iv2Var, ev2 ev2Var, rt2 rt2Var) {
            if (iv2Var.j(ev2Var, "PostprocessorProducer")) {
                return ImmutableMap.of("Postprocessor", rt2Var.getName());
            }
            return null;
        }

        public final synchronized boolean B() {
            return this.f;
        }

        public final void C() {
            if (y()) {
                p().b();
            }
        }

        public final void D(Throwable th) {
            if (y()) {
                p().a(th);
            }
        }

        public final void E(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            boolean e = qm.e(i);
            if ((e || B()) && !(e && y())) {
                return;
            }
            p().d(aVar, i);
        }

        @Override // defpackage.qm
        /* renamed from: F */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            if (!com.facebook.common.references.a.u(aVar)) {
                if (qm.e(i)) {
                    E(null, i);
                    return;
                }
                return;
            }
            K(aVar, i);
        }

        public final com.facebook.common.references.a<com.facebook.imagepipeline.image.a> G(com.facebook.imagepipeline.image.a aVar) {
            c00 c00Var = (c00) aVar;
            com.facebook.common.references.a<Bitmap> b = this.e.b(c00Var.f(), st2.this.b);
            try {
                c00 c00Var2 = new c00(b, aVar.a(), c00Var.m(), c00Var.l());
                c00Var2.e(c00Var.getExtras());
                return com.facebook.common.references.a.v(c00Var2);
            } finally {
                com.facebook.common.references.a.g(b);
            }
        }

        public final synchronized boolean H() {
            if (this.f || !this.i || this.j || !com.facebook.common.references.a.u(this.g)) {
                return false;
            }
            this.j = true;
            return true;
        }

        public final boolean I(com.facebook.imagepipeline.image.a aVar) {
            return aVar instanceof c00;
        }

        public final void J() {
            st2.this.c.execute(new RunnableC0283b());
        }

        public final void K(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            synchronized (this) {
                if (this.f) {
                    return;
                }
                com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar2 = this.g;
                this.g = com.facebook.common.references.a.e(aVar);
                this.h = i;
                this.i = true;
                boolean H = H();
                com.facebook.common.references.a.g(aVar2);
                if (H) {
                    J();
                }
            }
        }

        @Override // defpackage.bm0, defpackage.qm
        public void g() {
            C();
        }

        @Override // defpackage.bm0, defpackage.qm
        public void h(Throwable th) {
            D(th);
        }

        public final void x() {
            boolean H;
            synchronized (this) {
                this.j = false;
                H = H();
            }
            if (H) {
                J();
            }
        }

        public final boolean y() {
            synchronized (this) {
                if (this.f) {
                    return false;
                }
                com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar = this.g;
                this.g = null;
                this.f = true;
                com.facebook.common.references.a.g(aVar);
                return true;
            }
        }

        public final void z(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            xt2.b(Boolean.valueOf(com.facebook.common.references.a.u(aVar)));
            if (!I(aVar.j())) {
                E(aVar, i);
                return;
            }
            this.c.k(this.d, "PostprocessorProducer");
            try {
                try {
                    com.facebook.common.references.a<com.facebook.imagepipeline.image.a> G = G(aVar.j());
                    iv2 iv2Var = this.c;
                    ev2 ev2Var = this.d;
                    iv2Var.a(ev2Var, "PostprocessorProducer", A(iv2Var, ev2Var, this.e));
                    E(G, i);
                    com.facebook.common.references.a.g(G);
                } catch (Exception e) {
                    iv2 iv2Var2 = this.c;
                    ev2 ev2Var2 = this.d;
                    iv2Var2.c(ev2Var2, "PostprocessorProducer", e, A(iv2Var2, ev2Var2, this.e));
                    D(e);
                    com.facebook.common.references.a.g(null);
                }
            } catch (Throwable th) {
                com.facebook.common.references.a.g(null);
                throw th;
            }
        }
    }

    /* compiled from: PostprocessorProducer.java */
    /* renamed from: st2$c */
    /* loaded from: classes.dex */
    public class c extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> implements a73 {
        public boolean c;
        public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> d;

        /* compiled from: PostprocessorProducer.java */
        /* renamed from: st2$c$a */
        /* loaded from: classes.dex */
        public class a extends vn {
            public a(st2 st2Var) {
            }

            @Override // defpackage.fv2
            public void a() {
                if (c.this.r()) {
                    c.this.p().b();
                }
            }
        }

        @Override // defpackage.bm0, defpackage.qm
        public void g() {
            if (r()) {
                p().b();
            }
        }

        @Override // defpackage.bm0, defpackage.qm
        public void h(Throwable th) {
            if (r()) {
                p().a(th);
            }
        }

        public final boolean r() {
            synchronized (this) {
                if (this.c) {
                    return false;
                }
                com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar = this.d;
                this.d = null;
                this.c = true;
                com.facebook.common.references.a.g(aVar);
                return true;
            }
        }

        @Override // defpackage.qm
        /* renamed from: s */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            if (qm.f(i)) {
                return;
            }
            t(aVar);
            u();
        }

        public final void t(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
            synchronized (this) {
                if (this.c) {
                    return;
                }
                com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar2 = this.d;
                this.d = com.facebook.common.references.a.e(aVar);
                com.facebook.common.references.a.g(aVar2);
            }
        }

        public final void u() {
            synchronized (this) {
                if (this.c) {
                    return;
                }
                com.facebook.common.references.a<com.facebook.imagepipeline.image.a> e = com.facebook.common.references.a.e(this.d);
                try {
                    p().d(e, 0);
                } finally {
                    com.facebook.common.references.a.g(e);
                }
            }
        }

        public c(st2 st2Var, b bVar, z63 z63Var, ev2 ev2Var) {
            super(bVar);
            this.c = false;
            this.d = null;
            z63Var.a(this);
            ev2Var.o(new a(st2Var));
        }
    }

    /* compiled from: PostprocessorProducer.java */
    /* renamed from: st2$d */
    /* loaded from: classes.dex */
    public class d extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        @Override // defpackage.qm
        /* renamed from: q */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            if (qm.f(i)) {
                return;
            }
            p().d(aVar, i);
        }

        public d(st2 st2Var, b bVar) {
            super(bVar);
        }
    }

    public st2(dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dv2Var, br2 br2Var, Executor executor) {
        this.a = (dv2) xt2.g(dv2Var);
        this.b = br2Var;
        this.c = (Executor) xt2.g(executor);
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dVar;
        iv2 l = ev2Var.l();
        rt2 k = ev2Var.c().k();
        xt2.g(k);
        b bVar = new b(l60Var, l, k, ev2Var);
        if (k instanceof z63) {
            dVar = new c(bVar, (z63) k, ev2Var);
        } else {
            dVar = new d(bVar);
        }
        this.a.a(dVar, ev2Var);
    }
}
