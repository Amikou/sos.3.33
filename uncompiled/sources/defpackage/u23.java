package defpackage;

import net.safemoon.androidwallet.R;

/* renamed from: u23  reason: default package */
/* loaded from: classes2.dex */
public final class u23 {
    public static final int[] CircleProgressBar = {R.attr.background_color, R.attr.background_width, R.attr.progress_color, R.attr.progress_value, R.attr.progress_width};
    public static final int CircleProgressBar_background_color = 0;
    public static final int CircleProgressBar_background_width = 1;
    public static final int CircleProgressBar_progress_color = 2;
    public static final int CircleProgressBar_progress_value = 3;
    public static final int CircleProgressBar_progress_width = 4;
}
