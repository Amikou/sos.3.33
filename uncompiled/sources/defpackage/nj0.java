package defpackage;

import java.math.BigInteger;
import java.util.List;
import org.web3j.abi.b;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.Uint;

/* compiled from: DefaultFunctionEncoder.java */
/* renamed from: nj0  reason: default package */
/* loaded from: classes2.dex */
public class nj0 extends b {
    private static int getLength(List<zc4> list) {
        int i = 0;
        for (zc4 zc4Var : list) {
            i = zc4Var instanceof StaticArray ? i + ((StaticArray) zc4Var).getValue().size() : i + 1;
        }
        return i;
    }

    @Override // org.web3j.abi.b
    public String encodeFunction(id1 id1Var) {
        List<zc4> inputParameters = id1Var.getInputParameters();
        String buildMethodId = b.buildMethodId(b.buildMethodSignature(id1Var.getName(), inputParameters));
        StringBuilder sb = new StringBuilder();
        sb.append(buildMethodId);
        return encodeParameters(inputParameters, sb);
    }

    @Override // org.web3j.abi.b
    public String encodeParameters(List<zc4> list) {
        return encodeParameters(list, new StringBuilder());
    }

    private static String encodeParameters(List<zc4> list, StringBuilder sb) {
        int length = getLength(list) * 32;
        StringBuilder sb2 = new StringBuilder();
        for (zc4 zc4Var : list) {
            String encode = od4.encode(zc4Var);
            if (od4.isDynamic(zc4Var)) {
                sb.append(od4.encodeNumeric(new Uint(BigInteger.valueOf(length))));
                sb2.append(encode);
                length += encode.length() >> 1;
            } else {
                sb.append(encode);
            }
        }
        sb.append((CharSequence) sb2);
        return sb.toString();
    }
}
