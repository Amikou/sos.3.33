package defpackage;

/* compiled from: SynchronizationGuard.java */
/* renamed from: l24  reason: default package */
/* loaded from: classes.dex */
public interface l24 {

    /* compiled from: SynchronizationGuard.java */
    /* renamed from: l24$a */
    /* loaded from: classes.dex */
    public interface a<T> {
        T execute();
    }

    <T> T a(a<T> aVar);
}
