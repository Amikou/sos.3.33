package defpackage;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;

/* compiled from: AccessibilityClickableSpanCompat.java */
/* renamed from: y5  reason: default package */
/* loaded from: classes.dex */
public final class y5 extends ClickableSpan {
    public final int a;
    public final b6 f0;
    public final int g0;

    public y5(int i, b6 b6Var, int i2) {
        this.a = i;
        this.f0 = b6Var;
        this.g0 = i2;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.a);
        this.f0.R(this.g0, bundle);
    }
}
