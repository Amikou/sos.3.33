package defpackage;

import java.util.Arrays;
import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: w75  reason: default package */
/* loaded from: classes.dex */
public final class w75 implements Comparator<z55> {
    public final /* synthetic */ c55 a;
    public final /* synthetic */ wk5 f0;

    public w75(c55 c55Var, wk5 wk5Var) {
        this.a = c55Var;
        this.f0 = wk5Var;
    }

    @Override // java.util.Comparator
    public final /* bridge */ /* synthetic */ int compare(z55 z55Var, z55 z55Var2) {
        z55 z55Var3 = z55Var;
        z55 z55Var4 = z55Var2;
        c55 c55Var = this.a;
        wk5 wk5Var = this.f0;
        if (z55Var3 instanceof i65) {
            return !(z55Var4 instanceof i65) ? 1 : 0;
        } else if (z55Var4 instanceof i65) {
            return -1;
        } else {
            if (c55Var == null) {
                return z55Var3.zzc().compareTo(z55Var4.zzc());
            }
            return (int) vm5.i(c55Var.a(wk5Var, Arrays.asList(z55Var3, z55Var4)).b().doubleValue());
        }
    }
}
