package defpackage;

/* compiled from: Bip39Wallet.java */
/* renamed from: op  reason: default package */
/* loaded from: classes3.dex */
public class op {
    private final String filename;
    private final String mnemonic;

    public op(String str, String str2) {
        this.filename = str;
        this.mnemonic = str2;
    }

    public String getFilename() {
        return this.filename;
    }

    public String getMnemonic() {
        return this.mnemonic;
    }

    public String toString() {
        return "Bip39Wallet{filename='" + this.filename + "', mnemonic='" + this.mnemonic + "'}";
    }
}
