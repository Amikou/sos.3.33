package defpackage;

import androidx.recyclerview.widget.g;

/* compiled from: CMCListCheckable.kt */
/* renamed from: vl1  reason: default package */
/* loaded from: classes2.dex */
public final class vl1 extends g.f<d11> {
    public static final vl1 a = new vl1();

    @Override // androidx.recyclerview.widget.g.f
    /* renamed from: a */
    public boolean areContentsTheSame(d11 d11Var, d11 d11Var2) {
        fs1.f(d11Var, "oldItem");
        fs1.f(d11Var2, "newItem");
        return fs1.b(d11Var.c(), d11Var2.c()) && d11Var.d() == d11Var2.d();
    }

    @Override // androidx.recyclerview.widget.g.f
    /* renamed from: b */
    public boolean areItemsTheSame(d11 d11Var, d11 d11Var2) {
        fs1.f(d11Var, "oldItem");
        fs1.f(d11Var2, "newItem");
        return fs1.b(d11Var, d11Var2);
    }
}
