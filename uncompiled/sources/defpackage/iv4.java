package defpackage;

import android.os.Bundle;

/* renamed from: iv4  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class iv4 implements xv4 {
    public final zv4 a;
    public final Bundle b;
    public final /* synthetic */ int c = 0;

    public iv4(zv4 zv4Var, Bundle bundle) {
        this.a = zv4Var;
        this.b = bundle;
    }

    public iv4(zv4 zv4Var, Bundle bundle, byte[] bArr) {
        this.a = zv4Var;
        this.b = bundle;
    }

    @Override // defpackage.xv4
    public final Object a() {
        return this.c != 0 ? this.a.k(this.b) : this.a.j(this.b);
    }
}
