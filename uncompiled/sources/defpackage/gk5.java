package defpackage;

import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: gk5  reason: default package */
/* loaded from: classes.dex */
public final class gk5 implements Runnable {
    public final /* synthetic */ zzaa a;
    public final /* synthetic */ zzp f0;
    public final /* synthetic */ pl5 g0;

    public gk5(pl5 pl5Var, zzaa zzaaVar, zzp zzpVar) {
        this.g0 = pl5Var;
        this.a = zzaaVar;
        this.f0 = zzpVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        fw5 fw5Var2;
        fw5 fw5Var3;
        fw5Var = this.g0.a;
        fw5Var.i();
        if (this.a.g0.I1() == null) {
            fw5Var3 = this.g0.a;
            fw5Var3.x(this.a, this.f0);
            return;
        }
        fw5Var2 = this.g0.a;
        fw5Var2.u(this.a, this.f0);
    }
}
