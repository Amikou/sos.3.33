package defpackage;

import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: gx5  reason: default package */
/* loaded from: classes.dex */
public final class gx5 implements Runnable {
    public final /* synthetic */ m a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ AppMeasurementDynamiteService h0;

    public gx5(AppMeasurementDynamiteService appMeasurementDynamiteService, m mVar, String str, String str2) {
        this.h0 = appMeasurementDynamiteService;
        this.a = mVar;
        this.f0 = str;
        this.g0 = str2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.h0.a.R().O(this.a, this.f0, this.g0);
    }
}
