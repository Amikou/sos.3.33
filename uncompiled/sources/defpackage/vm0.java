package defpackage;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: vm0  reason: default package */
/* loaded from: classes.dex */
public final class vm0 {
    public static Boolean a;
    public static Boolean b;
    public static Boolean c;
    public static Boolean d;

    @RecentlyNonNull
    public static boolean a(@RecentlyNonNull Context context) {
        return b(context.getPackageManager());
    }

    @RecentlyNonNull
    public static boolean b(@RecentlyNonNull PackageManager packageManager) {
        if (d == null) {
            d = Boolean.valueOf(jr2.h() && packageManager.hasSystemFeature("android.hardware.type.automotive"));
        }
        return d.booleanValue();
    }

    @RecentlyNonNull
    public static boolean c() {
        return "user".equals(Build.TYPE);
    }

    @RecentlyNonNull
    @TargetApi(20)
    public static boolean d(@RecentlyNonNull Context context) {
        return e(context.getPackageManager());
    }

    @RecentlyNonNull
    @TargetApi(20)
    public static boolean e(@RecentlyNonNull PackageManager packageManager) {
        if (a == null) {
            a = Boolean.valueOf(jr2.e() && packageManager.hasSystemFeature("android.hardware.type.watch"));
        }
        return a.booleanValue();
    }

    @RecentlyNonNull
    @TargetApi(26)
    public static boolean f(@RecentlyNonNull Context context) {
        if (d(context)) {
            if (jr2.g()) {
                return h(context) && !jr2.h();
            }
            return true;
        }
        return false;
    }

    @RecentlyNonNull
    public static boolean g(@RecentlyNonNull Context context) {
        if (c == null) {
            c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return c.booleanValue();
    }

    @TargetApi(21)
    public static boolean h(Context context) {
        if (b == null) {
            b = Boolean.valueOf(jr2.f() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }
}
