package defpackage;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.media3.common.util.b;
import java.util.ArrayDeque;

/* compiled from: AsynchronousMediaCodecCallback.java */
/* renamed from: yi  reason: default package */
/* loaded from: classes.dex */
public final class yi extends MediaCodec.Callback {
    public final HandlerThread b;
    public Handler c;
    public MediaFormat h;
    public MediaFormat i;
    public MediaCodec.CodecException j;
    public long k;
    public boolean l;
    public IllegalStateException m;
    public final Object a = new Object();
    public final lr1 d = new lr1();
    public final lr1 e = new lr1();
    public final ArrayDeque<MediaCodec.BufferInfo> f = new ArrayDeque<>();
    public final ArrayDeque<MediaFormat> g = new ArrayDeque<>();

    public yi(HandlerThread handlerThread) {
        this.b = handlerThread;
    }

    public final void b(MediaFormat mediaFormat) {
        this.e.a(-2);
        this.g.add(mediaFormat);
    }

    public int c() {
        synchronized (this.a) {
            int i = -1;
            if (i()) {
                return -1;
            }
            j();
            if (!this.d.d()) {
                i = this.d.e();
            }
            return i;
        }
    }

    public int d(MediaCodec.BufferInfo bufferInfo) {
        synchronized (this.a) {
            if (i()) {
                return -1;
            }
            j();
            if (this.e.d()) {
                return -1;
            }
            int e = this.e.e();
            if (e >= 0) {
                ii.i(this.h);
                MediaCodec.BufferInfo remove = this.f.remove();
                bufferInfo.set(remove.offset, remove.size, remove.presentationTimeUs, remove.flags);
            } else if (e == -2) {
                this.h = this.g.remove();
            }
            return e;
        }
    }

    public void e() {
        synchronized (this.a) {
            this.k++;
            ((Handler) b.j(this.c)).post(new Runnable() { // from class: xi
                @Override // java.lang.Runnable
                public final void run() {
                    yi.this.m();
                }
            });
        }
    }

    public final void f() {
        if (!this.g.isEmpty()) {
            this.i = this.g.getLast();
        }
        this.d.b();
        this.e.b();
        this.f.clear();
        this.g.clear();
        this.j = null;
    }

    public MediaFormat g() {
        MediaFormat mediaFormat;
        synchronized (this.a) {
            mediaFormat = this.h;
            if (mediaFormat == null) {
                throw new IllegalStateException();
            }
        }
        return mediaFormat;
    }

    public void h(MediaCodec mediaCodec) {
        ii.g(this.c == null);
        this.b.start();
        Handler handler = new Handler(this.b.getLooper());
        mediaCodec.setCallback(this, handler);
        this.c = handler;
    }

    public final boolean i() {
        return this.k > 0 || this.l;
    }

    public final void j() {
        k();
        l();
    }

    public final void k() {
        IllegalStateException illegalStateException = this.m;
        if (illegalStateException == null) {
            return;
        }
        this.m = null;
        throw illegalStateException;
    }

    public final void l() {
        MediaCodec.CodecException codecException = this.j;
        if (codecException == null) {
            return;
        }
        this.j = null;
        throw codecException;
    }

    public final void m() {
        synchronized (this.a) {
            if (this.l) {
                return;
            }
            long j = this.k - 1;
            this.k = j;
            if (j > 0) {
                return;
            }
            if (j < 0) {
                n(new IllegalStateException());
            } else {
                f();
            }
        }
    }

    public final void n(IllegalStateException illegalStateException) {
        synchronized (this.a) {
            this.m = illegalStateException;
        }
    }

    public void o() {
        synchronized (this.a) {
            this.l = true;
            this.b.quit();
            f();
        }
    }

    @Override // android.media.MediaCodec.Callback
    public void onError(MediaCodec mediaCodec, MediaCodec.CodecException codecException) {
        synchronized (this.a) {
            this.j = codecException;
        }
    }

    @Override // android.media.MediaCodec.Callback
    public void onInputBufferAvailable(MediaCodec mediaCodec, int i) {
        synchronized (this.a) {
            this.d.a(i);
        }
    }

    @Override // android.media.MediaCodec.Callback
    public void onOutputBufferAvailable(MediaCodec mediaCodec, int i, MediaCodec.BufferInfo bufferInfo) {
        synchronized (this.a) {
            MediaFormat mediaFormat = this.i;
            if (mediaFormat != null) {
                b(mediaFormat);
                this.i = null;
            }
            this.e.a(i);
            this.f.add(bufferInfo);
        }
    }

    @Override // android.media.MediaCodec.Callback
    public void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        synchronized (this.a) {
            b(mediaFormat);
            this.i = null;
        }
    }
}
