package defpackage;

import java.util.Collections;
import java.util.List;

/* compiled from: CeaSubtitle.java */
/* renamed from: tw  reason: default package */
/* loaded from: classes.dex */
public final class tw implements qv3 {
    public final List<kb0> a;

    public tw(List<kb0> list) {
        this.a = list;
    }

    @Override // defpackage.qv3
    public int a(long j) {
        return j < 0 ? 0 : -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        ii.a(i == 0);
        return 0L;
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return j >= 0 ? this.a : Collections.emptyList();
    }

    @Override // defpackage.qv3
    public int f() {
        return 1;
    }
}
