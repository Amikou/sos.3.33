package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.facebook.drawee.generic.RoundingParams;
import defpackage.qc3;

/* compiled from: GenericDraweeHierarchyInflater.java */
/* renamed from: af1  reason: default package */
/* loaded from: classes.dex */
public class af1 {
    public static Drawable a(Context context, TypedArray typedArray, int i) {
        int resourceId = typedArray.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        return context.getResources().getDrawable(resourceId);
    }

    public static RoundingParams b(ze1 ze1Var) {
        if (ze1Var.s() == null) {
            ze1Var.I(new RoundingParams());
        }
        return ze1Var.s();
    }

    public static qc3.b c(TypedArray typedArray, int i) {
        switch (typedArray.getInt(i, -2)) {
            case -1:
                return null;
            case 0:
                return qc3.b.a;
            case 1:
                return qc3.b.b;
            case 2:
                return qc3.b.c;
            case 3:
                return qc3.b.d;
            case 4:
                return qc3.b.e;
            case 5:
                return qc3.b.f;
            case 6:
                return qc3.b.g;
            case 7:
                return qc3.b.h;
            case 8:
                return qc3.b.i;
            default:
                throw new RuntimeException("XML attribute not specified!");
        }
    }

    public static ze1 d(Context context, AttributeSet attributeSet) {
        if (nc1.d()) {
            nc1.a("GenericDraweeHierarchyBuilder#inflateBuilder");
        }
        ze1 e = e(new ze1(context.getResources()), context, attributeSet);
        if (nc1.d()) {
            nc1.b();
        }
        return e;
    }

    /* JADX WARN: Code restructure failed: missing block: B:117:0x01cb, code lost:
        if (r14 != false) goto L124;
     */
    /* JADX WARN: Code restructure failed: missing block: B:132:0x01e5, code lost:
        if (r5 != false) goto L124;
     */
    /* JADX WARN: Code restructure failed: missing block: B:133:0x01e7, code lost:
        r4 = true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.ze1 e(defpackage.ze1 r18, android.content.Context r19, android.util.AttributeSet r20) {
        /*
            Method dump skipped, instructions count: 585
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.af1.e(ze1, android.content.Context, android.util.AttributeSet):ze1");
    }
}
