package defpackage;

import java.math.BigInteger;

/* compiled from: RawTransaction.java */
/* renamed from: y33  reason: default package */
/* loaded from: classes3.dex */
public class y33 {
    private String data;
    private BigInteger feeCap;
    private BigInteger gasLimit;
    private BigInteger gasPremium;
    private BigInteger gasPrice;
    private BigInteger nonce;
    private String to;
    private BigInteger value;

    public y33(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, BigInteger bigInteger4, String str2) {
        this(bigInteger, bigInteger2, bigInteger3, str, bigInteger4, str2, null, null);
    }

    public static y33 createContractTransaction(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, String str) {
        return new y33(bigInteger, bigInteger2, bigInteger3, "", bigInteger4, str);
    }

    public static y33 createEtherTransaction(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, BigInteger bigInteger4) {
        return new y33(bigInteger, bigInteger2, bigInteger3, str, bigInteger4, "");
    }

    public static y33 createTransaction(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2) {
        return createTransaction(bigInteger, bigInteger2, bigInteger3, str, BigInteger.ZERO, str2);
    }

    public String getData() {
        return this.data;
    }

    public BigInteger getFeeCap() {
        return this.feeCap;
    }

    public BigInteger getGasLimit() {
        return this.gasLimit;
    }

    public BigInteger getGasPremium() {
        return this.gasPremium;
    }

    public BigInteger getGasPrice() {
        return this.gasPrice;
    }

    public BigInteger getNonce() {
        return this.nonce;
    }

    public String getTo() {
        return this.to;
    }

    public BigInteger getValue() {
        return this.value;
    }

    public boolean isEIP1559Transaction() {
        return (this.gasPrice != null || this.gasPremium == null || this.feeCap == null) ? false : true;
    }

    public boolean isLegacyTransaction() {
        return this.gasPrice != null && this.gasPremium == null && this.feeCap == null;
    }

    public y33(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, BigInteger bigInteger4, String str2, BigInteger bigInteger5, BigInteger bigInteger6) {
        this.nonce = bigInteger;
        this.gasPrice = bigInteger2;
        this.gasLimit = bigInteger3;
        this.to = str;
        this.value = bigInteger4;
        this.data = str2 != null ? ej2.cleanHexPrefix(str2) : null;
        this.gasPremium = bigInteger5;
        this.feeCap = bigInteger6;
    }

    public static y33 createEtherTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, BigInteger bigInteger3, BigInteger bigInteger4, BigInteger bigInteger5) {
        return new y33(bigInteger, null, bigInteger2, str, bigInteger3, "", bigInteger4, bigInteger5);
    }

    public static y33 createTransaction(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, BigInteger bigInteger4, String str2) {
        return new y33(bigInteger, bigInteger2, bigInteger3, str, bigInteger4, str2);
    }

    public static y33 createTransaction(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, BigInteger bigInteger4, String str2, BigInteger bigInteger5, BigInteger bigInteger6) {
        return new y33(bigInteger, bigInteger2, bigInteger3, str, bigInteger4, str2, bigInteger5, bigInteger6);
    }
}
