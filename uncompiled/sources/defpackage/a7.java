package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityConfirmPassphraseBinding.java */
/* renamed from: a7  reason: default package */
/* loaded from: classes2.dex */
public final class a7 {
    public final View A;
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final AppCompatButton c;
    public final ImageView d;
    public final ImageView e;
    public final ImageView f;
    public final ImageView g;
    public final ImageView h;
    public final ImageView i;
    public final ImageView j;
    public final ImageView k;
    public final ImageView l;
    public final ImageView m;
    public final ImageView n;
    public final ImageView o;
    public final View p;
    public final View q;
    public final View r;
    public final View s;
    public final View t;
    public final View u;
    public final View v;
    public final View w;
    public final View x;
    public final View y;
    public final View z;

    public a7(ConstraintLayout constraintLayout, MaterialButton materialButton, AppCompatButton appCompatButton, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, TextView textView9, TextView textView10, TextView textView11, TextView textView12, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, ImageView imageView5, ImageView imageView6, ImageView imageView7, ImageView imageView8, ImageView imageView9, ImageView imageView10, ImageView imageView11, ImageView imageView12, ScrollView scrollView, TextView textView13, TextView textView14, TextView textView15, TextView textView16, TextView textView17, TextView textView18, TextView textView19, TextView textView20, TextView textView21, TextView textView22, TextView textView23, TextView textView24, View view, View view2, View view3, View view4, View view5, View view6, View view7, View view8, View view9, View view10, View view11, View view12, TextView textView25, TextView textView26, TextView textView27, TextView textView28, TextView textView29, TextView textView30, TextView textView31, TextView textView32, TextView textView33, TextView textView34, TextView textView35, TextView textView36) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = appCompatButton;
        this.d = imageView;
        this.e = imageView2;
        this.f = imageView3;
        this.g = imageView4;
        this.h = imageView5;
        this.i = imageView6;
        this.j = imageView7;
        this.k = imageView8;
        this.l = imageView9;
        this.m = imageView10;
        this.n = imageView11;
        this.o = imageView12;
        this.p = view;
        this.q = view2;
        this.r = view3;
        this.s = view4;
        this.t = view5;
        this.u = view6;
        this.v = view7;
        this.w = view8;
        this.x = view9;
        this.y = view10;
        this.z = view11;
        this.A = view12;
    }

    public static a7 a(View view) {
        int i = R.id.btnBack;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBack);
        if (materialButton != null) {
            i = R.id.btnConfirm;
            AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btnConfirm);
            if (appCompatButton != null) {
                i = R.id.indexWord1;
                TextView textView = (TextView) ai4.a(view, R.id.indexWord1);
                if (textView != null) {
                    i = R.id.indexWord10;
                    TextView textView2 = (TextView) ai4.a(view, R.id.indexWord10);
                    if (textView2 != null) {
                        i = R.id.indexWord11;
                        TextView textView3 = (TextView) ai4.a(view, R.id.indexWord11);
                        if (textView3 != null) {
                            i = R.id.indexWord12;
                            TextView textView4 = (TextView) ai4.a(view, R.id.indexWord12);
                            if (textView4 != null) {
                                i = R.id.indexWord2;
                                TextView textView5 = (TextView) ai4.a(view, R.id.indexWord2);
                                if (textView5 != null) {
                                    i = R.id.indexWord3;
                                    TextView textView6 = (TextView) ai4.a(view, R.id.indexWord3);
                                    if (textView6 != null) {
                                        i = R.id.indexWord4;
                                        TextView textView7 = (TextView) ai4.a(view, R.id.indexWord4);
                                        if (textView7 != null) {
                                            i = R.id.indexWord5;
                                            TextView textView8 = (TextView) ai4.a(view, R.id.indexWord5);
                                            if (textView8 != null) {
                                                i = R.id.indexWord6;
                                                TextView textView9 = (TextView) ai4.a(view, R.id.indexWord6);
                                                if (textView9 != null) {
                                                    i = R.id.indexWord7;
                                                    TextView textView10 = (TextView) ai4.a(view, R.id.indexWord7);
                                                    if (textView10 != null) {
                                                        i = R.id.indexWord8;
                                                        TextView textView11 = (TextView) ai4.a(view, R.id.indexWord8);
                                                        if (textView11 != null) {
                                                            i = R.id.indexWord9;
                                                            TextView textView12 = (TextView) ai4.a(view, R.id.indexWord9);
                                                            if (textView12 != null) {
                                                                i = R.id.iv1;
                                                                ImageView imageView = (ImageView) ai4.a(view, R.id.iv1);
                                                                if (imageView != null) {
                                                                    i = R.id.iv10;
                                                                    ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv10);
                                                                    if (imageView2 != null) {
                                                                        i = R.id.iv11;
                                                                        ImageView imageView3 = (ImageView) ai4.a(view, R.id.iv11);
                                                                        if (imageView3 != null) {
                                                                            i = R.id.iv12;
                                                                            ImageView imageView4 = (ImageView) ai4.a(view, R.id.iv12);
                                                                            if (imageView4 != null) {
                                                                                i = R.id.iv2;
                                                                                ImageView imageView5 = (ImageView) ai4.a(view, R.id.iv2);
                                                                                if (imageView5 != null) {
                                                                                    i = R.id.iv3;
                                                                                    ImageView imageView6 = (ImageView) ai4.a(view, R.id.iv3);
                                                                                    if (imageView6 != null) {
                                                                                        i = R.id.iv4;
                                                                                        ImageView imageView7 = (ImageView) ai4.a(view, R.id.iv4);
                                                                                        if (imageView7 != null) {
                                                                                            i = R.id.iv5;
                                                                                            ImageView imageView8 = (ImageView) ai4.a(view, R.id.iv5);
                                                                                            if (imageView8 != null) {
                                                                                                i = R.id.iv6;
                                                                                                ImageView imageView9 = (ImageView) ai4.a(view, R.id.iv6);
                                                                                                if (imageView9 != null) {
                                                                                                    i = R.id.iv7;
                                                                                                    ImageView imageView10 = (ImageView) ai4.a(view, R.id.iv7);
                                                                                                    if (imageView10 != null) {
                                                                                                        i = R.id.iv8;
                                                                                                        ImageView imageView11 = (ImageView) ai4.a(view, R.id.iv8);
                                                                                                        if (imageView11 != null) {
                                                                                                            i = R.id.iv9;
                                                                                                            ImageView imageView12 = (ImageView) ai4.a(view, R.id.iv9);
                                                                                                            if (imageView12 != null) {
                                                                                                                i = R.id.scrollView3;
                                                                                                                ScrollView scrollView = (ScrollView) ai4.a(view, R.id.scrollView3);
                                                                                                                if (scrollView != null) {
                                                                                                                    i = R.id.sepratedword1;
                                                                                                                    TextView textView13 = (TextView) ai4.a(view, R.id.sepratedword1);
                                                                                                                    if (textView13 != null) {
                                                                                                                        i = R.id.sepratedword10;
                                                                                                                        TextView textView14 = (TextView) ai4.a(view, R.id.sepratedword10);
                                                                                                                        if (textView14 != null) {
                                                                                                                            i = R.id.sepratedword11;
                                                                                                                            TextView textView15 = (TextView) ai4.a(view, R.id.sepratedword11);
                                                                                                                            if (textView15 != null) {
                                                                                                                                i = R.id.sepratedword12;
                                                                                                                                TextView textView16 = (TextView) ai4.a(view, R.id.sepratedword12);
                                                                                                                                if (textView16 != null) {
                                                                                                                                    i = R.id.sepratedword2;
                                                                                                                                    TextView textView17 = (TextView) ai4.a(view, R.id.sepratedword2);
                                                                                                                                    if (textView17 != null) {
                                                                                                                                        i = R.id.sepratedword3;
                                                                                                                                        TextView textView18 = (TextView) ai4.a(view, R.id.sepratedword3);
                                                                                                                                        if (textView18 != null) {
                                                                                                                                            i = R.id.sepratedword4;
                                                                                                                                            TextView textView19 = (TextView) ai4.a(view, R.id.sepratedword4);
                                                                                                                                            if (textView19 != null) {
                                                                                                                                                i = R.id.sepratedword5;
                                                                                                                                                TextView textView20 = (TextView) ai4.a(view, R.id.sepratedword5);
                                                                                                                                                if (textView20 != null) {
                                                                                                                                                    i = R.id.sepratedword6;
                                                                                                                                                    TextView textView21 = (TextView) ai4.a(view, R.id.sepratedword6);
                                                                                                                                                    if (textView21 != null) {
                                                                                                                                                        i = R.id.sepratedword7;
                                                                                                                                                        TextView textView22 = (TextView) ai4.a(view, R.id.sepratedword7);
                                                                                                                                                        if (textView22 != null) {
                                                                                                                                                            i = R.id.sepratedword8;
                                                                                                                                                            TextView textView23 = (TextView) ai4.a(view, R.id.sepratedword8);
                                                                                                                                                            if (textView23 != null) {
                                                                                                                                                                i = R.id.sepratedword9;
                                                                                                                                                                TextView textView24 = (TextView) ai4.a(view, R.id.sepratedword9);
                                                                                                                                                                if (textView24 != null) {
                                                                                                                                                                    i = R.id.view1;
                                                                                                                                                                    View a = ai4.a(view, R.id.view1);
                                                                                                                                                                    if (a != null) {
                                                                                                                                                                        i = R.id.view10;
                                                                                                                                                                        View a2 = ai4.a(view, R.id.view10);
                                                                                                                                                                        if (a2 != null) {
                                                                                                                                                                            i = R.id.view11;
                                                                                                                                                                            View a3 = ai4.a(view, R.id.view11);
                                                                                                                                                                            if (a3 != null) {
                                                                                                                                                                                i = R.id.view12;
                                                                                                                                                                                View a4 = ai4.a(view, R.id.view12);
                                                                                                                                                                                if (a4 != null) {
                                                                                                                                                                                    i = R.id.view2;
                                                                                                                                                                                    View a5 = ai4.a(view, R.id.view2);
                                                                                                                                                                                    if (a5 != null) {
                                                                                                                                                                                        i = R.id.view3;
                                                                                                                                                                                        View a6 = ai4.a(view, R.id.view3);
                                                                                                                                                                                        if (a6 != null) {
                                                                                                                                                                                            i = R.id.view4;
                                                                                                                                                                                            View a7 = ai4.a(view, R.id.view4);
                                                                                                                                                                                            if (a7 != null) {
                                                                                                                                                                                                i = R.id.view5;
                                                                                                                                                                                                View a8 = ai4.a(view, R.id.view5);
                                                                                                                                                                                                if (a8 != null) {
                                                                                                                                                                                                    i = R.id.view6;
                                                                                                                                                                                                    View a9 = ai4.a(view, R.id.view6);
                                                                                                                                                                                                    if (a9 != null) {
                                                                                                                                                                                                        i = R.id.view7;
                                                                                                                                                                                                        View a10 = ai4.a(view, R.id.view7);
                                                                                                                                                                                                        if (a10 != null) {
                                                                                                                                                                                                            i = R.id.view8;
                                                                                                                                                                                                            View a11 = ai4.a(view, R.id.view8);
                                                                                                                                                                                                            if (a11 != null) {
                                                                                                                                                                                                                i = R.id.view9;
                                                                                                                                                                                                                View a12 = ai4.a(view, R.id.view9);
                                                                                                                                                                                                                if (a12 != null) {
                                                                                                                                                                                                                    i = R.id.word1;
                                                                                                                                                                                                                    TextView textView25 = (TextView) ai4.a(view, R.id.word1);
                                                                                                                                                                                                                    if (textView25 != null) {
                                                                                                                                                                                                                        i = R.id.word10;
                                                                                                                                                                                                                        TextView textView26 = (TextView) ai4.a(view, R.id.word10);
                                                                                                                                                                                                                        if (textView26 != null) {
                                                                                                                                                                                                                            i = R.id.word11;
                                                                                                                                                                                                                            TextView textView27 = (TextView) ai4.a(view, R.id.word11);
                                                                                                                                                                                                                            if (textView27 != null) {
                                                                                                                                                                                                                                i = R.id.word12;
                                                                                                                                                                                                                                TextView textView28 = (TextView) ai4.a(view, R.id.word12);
                                                                                                                                                                                                                                if (textView28 != null) {
                                                                                                                                                                                                                                    i = R.id.word2;
                                                                                                                                                                                                                                    TextView textView29 = (TextView) ai4.a(view, R.id.word2);
                                                                                                                                                                                                                                    if (textView29 != null) {
                                                                                                                                                                                                                                        i = R.id.word3;
                                                                                                                                                                                                                                        TextView textView30 = (TextView) ai4.a(view, R.id.word3);
                                                                                                                                                                                                                                        if (textView30 != null) {
                                                                                                                                                                                                                                            i = R.id.word4;
                                                                                                                                                                                                                                            TextView textView31 = (TextView) ai4.a(view, R.id.word4);
                                                                                                                                                                                                                                            if (textView31 != null) {
                                                                                                                                                                                                                                                i = R.id.word5;
                                                                                                                                                                                                                                                TextView textView32 = (TextView) ai4.a(view, R.id.word5);
                                                                                                                                                                                                                                                if (textView32 != null) {
                                                                                                                                                                                                                                                    i = R.id.word6;
                                                                                                                                                                                                                                                    TextView textView33 = (TextView) ai4.a(view, R.id.word6);
                                                                                                                                                                                                                                                    if (textView33 != null) {
                                                                                                                                                                                                                                                        i = R.id.word7;
                                                                                                                                                                                                                                                        TextView textView34 = (TextView) ai4.a(view, R.id.word7);
                                                                                                                                                                                                                                                        if (textView34 != null) {
                                                                                                                                                                                                                                                            i = R.id.word8;
                                                                                                                                                                                                                                                            TextView textView35 = (TextView) ai4.a(view, R.id.word8);
                                                                                                                                                                                                                                                            if (textView35 != null) {
                                                                                                                                                                                                                                                                i = R.id.word9;
                                                                                                                                                                                                                                                                TextView textView36 = (TextView) ai4.a(view, R.id.word9);
                                                                                                                                                                                                                                                                if (textView36 != null) {
                                                                                                                                                                                                                                                                    return new a7((ConstraintLayout) view, materialButton, appCompatButton, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9, textView10, textView11, textView12, imageView, imageView2, imageView3, imageView4, imageView5, imageView6, imageView7, imageView8, imageView9, imageView10, imageView11, imageView12, scrollView, textView13, textView14, textView15, textView16, textView17, textView18, textView19, textView20, textView21, textView22, textView23, textView24, a, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, textView25, textView26, textView27, textView28, textView29, textView30, textView31, textView32, textView33, textView34, textView35, textView36);
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static a7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static a7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_confirm_passphrase, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
