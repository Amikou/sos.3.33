package defpackage;

import android.net.Uri;
import androidx.media3.common.m;
import androidx.media3.common.u;
import zendesk.support.request.CellBase;

/* compiled from: SinglePeriodTimeline.java */
/* renamed from: vp3  reason: default package */
/* loaded from: classes.dex */
public final class vp3 extends u {
    public static final Object s0 = new Object();
    public final long f0;
    public final long g0;
    public final long h0;
    public final long i0;
    public final long j0;
    public final long k0;
    public final long l0;
    public final boolean m0;
    public final boolean n0;
    public final boolean o0;
    public final Object p0;
    public final m q0;
    public final m.g r0;

    static {
        new m.c().d("SinglePeriodTimeline").g(Uri.EMPTY).a();
    }

    public vp3(long j, boolean z, boolean z2, boolean z3, Object obj, m mVar) {
        this(j, j, 0L, 0L, z, z2, z3, obj, mVar);
    }

    @Override // androidx.media3.common.u
    public int b(Object obj) {
        return s0.equals(obj) ? 0 : -1;
    }

    @Override // androidx.media3.common.u
    public u.b g(int i, u.b bVar, boolean z) {
        ii.c(i, 0, 1);
        return bVar.u(null, z ? s0 : null, 0, this.i0, -this.k0);
    }

    @Override // androidx.media3.common.u
    public int i() {
        return 1;
    }

    @Override // androidx.media3.common.u
    public Object m(int i) {
        ii.c(i, 0, 1);
        return s0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x002b, code lost:
        if (r1 > r5) goto L9;
     */
    @Override // androidx.media3.common.u
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public androidx.media3.common.u.c o(int r25, androidx.media3.common.u.c r26, long r27) {
        /*
            r24 = this;
            r0 = r24
            r1 = 0
            r2 = 1
            r3 = r25
            defpackage.ii.c(r3, r1, r2)
            long r1 = r0.l0
            boolean r14 = r0.n0
            r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r14 == 0) goto L2e
            boolean r5 = r0.o0
            if (r5 != 0) goto L2e
            r5 = 0
            int r5 = (r27 > r5 ? 1 : (r27 == r5 ? 0 : -1))
            if (r5 == 0) goto L2e
            long r5 = r0.j0
            int r7 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r7 != 0) goto L27
        L24:
            r16 = r3
            goto L30
        L27:
            long r1 = r1 + r27
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 <= 0) goto L2e
            goto L24
        L2e:
            r16 = r1
        L30:
            java.lang.Object r4 = androidx.media3.common.u.c.v0
            androidx.media3.common.m r5 = r0.q0
            java.lang.Object r6 = r0.p0
            long r7 = r0.f0
            long r9 = r0.g0
            long r11 = r0.h0
            boolean r13 = r0.m0
            androidx.media3.common.m$g r15 = r0.r0
            long r1 = r0.j0
            r18 = r1
            r20 = 0
            r21 = 0
            long r1 = r0.k0
            r22 = r1
            r3 = r26
            androidx.media3.common.u$c r1 = r3.k(r4, r5, r6, r7, r9, r11, r13, r14, r15, r16, r18, r20, r21, r22)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.vp3.o(int, androidx.media3.common.u$c, long):androidx.media3.common.u$c");
    }

    @Override // androidx.media3.common.u
    public int p() {
        return 1;
    }

    public vp3(long j, long j2, long j3, long j4, boolean z, boolean z2, boolean z3, Object obj, m mVar) {
        this(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, j, j2, j3, j4, z, z2, false, obj, mVar, z3 ? mVar.g0 : null);
    }

    public vp3(long j, long j2, long j3, long j4, long j5, long j6, long j7, boolean z, boolean z2, boolean z3, Object obj, m mVar, m.g gVar) {
        this.f0 = j;
        this.g0 = j2;
        this.h0 = j3;
        this.i0 = j4;
        this.j0 = j5;
        this.k0 = j6;
        this.l0 = j7;
        this.m0 = z;
        this.n0 = z2;
        this.o0 = z3;
        this.p0 = obj;
        this.q0 = (m) ii.e(mVar);
        this.r0 = gVar;
    }
}
