package defpackage;

import android.database.sqlite.SQLiteStatement;

/* compiled from: FrameworkSQLiteStatement.java */
/* renamed from: jc1  reason: default package */
/* loaded from: classes.dex */
public class jc1 extends ic1 implements ww3 {
    public final SQLiteStatement f0;

    public jc1(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.f0 = sQLiteStatement;
    }

    @Override // defpackage.ww3
    public long C1() {
        return this.f0.executeInsert();
    }

    @Override // defpackage.ww3
    public int T() {
        return this.f0.executeUpdateDelete();
    }
}
