package defpackage;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import com.bumptech.glide.load.engine.GlideException;
import defpackage.j92;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: MultiModelLoader.java */
/* renamed from: pa2  reason: default package */
/* loaded from: classes.dex */
public class pa2<Model, Data> implements j92<Model, Data> {
    public final List<j92<Model, Data>> a;
    public final et2<List<Throwable>> b;

    /* compiled from: MultiModelLoader.java */
    /* renamed from: pa2$a */
    /* loaded from: classes.dex */
    public static class a<Data> implements d<Data>, d.a<Data> {
        public final List<d<Data>> a;
        public final et2<List<Throwable>> f0;
        public int g0;
        public Priority h0;
        public d.a<? super Data> i0;
        public List<Throwable> j0;
        public boolean k0;

        public a(List<d<Data>> list, et2<List<Throwable>> et2Var) {
            this.f0 = et2Var;
            wt2.c(list);
            this.a = list;
            this.g0 = 0;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<Data> a() {
            return this.a.get(0).a();
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
            List<Throwable> list = this.j0;
            if (list != null) {
                this.f0.a(list);
            }
            this.j0 = null;
            for (d<Data> dVar : this.a) {
                dVar.b();
            }
        }

        @Override // com.bumptech.glide.load.data.d.a
        public void c(Exception exc) {
            ((List) wt2.d(this.j0)).add(exc);
            g();
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
            this.k0 = true;
            for (d<Data> dVar : this.a) {
                dVar.cancel();
            }
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return this.a.get(0).d();
        }

        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super Data> aVar) {
            this.h0 = priority;
            this.i0 = aVar;
            this.j0 = this.f0.b();
            this.a.get(this.g0).e(priority, this);
            if (this.k0) {
                cancel();
            }
        }

        @Override // com.bumptech.glide.load.data.d.a
        public void f(Data data) {
            if (data != null) {
                this.i0.f(data);
            } else {
                g();
            }
        }

        public final void g() {
            if (this.k0) {
                return;
            }
            if (this.g0 < this.a.size() - 1) {
                this.g0++;
                e(this.h0, this.i0);
                return;
            }
            wt2.d(this.j0);
            this.i0.c(new GlideException("Fetch failed", new ArrayList(this.j0)));
        }
    }

    public pa2(List<j92<Model, Data>> list, et2<List<Throwable>> et2Var) {
        this.a = list;
        this.b = et2Var;
    }

    @Override // defpackage.j92
    public boolean a(Model model) {
        for (j92<Model, Data> j92Var : this.a) {
            if (j92Var.a(model)) {
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.j92
    public j92.a<Data> b(Model model, int i, int i2, vn2 vn2Var) {
        j92.a<Data> b;
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        fx1 fx1Var = null;
        for (int i3 = 0; i3 < size; i3++) {
            j92<Model, Data> j92Var = this.a.get(i3);
            if (j92Var.a(model) && (b = j92Var.b(model, i, i2, vn2Var)) != null) {
                fx1Var = b.a;
                arrayList.add(b.c);
            }
        }
        if (arrayList.isEmpty() || fx1Var == null) {
            return null;
        }
        return new j92.a<>(fx1Var, new a(arrayList, this.b));
    }

    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray()) + '}';
    }
}
