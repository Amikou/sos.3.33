package defpackage;

import android.graphics.Bitmap;
import android.util.Log;
import defpackage.tf1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

/* compiled from: StandardGifDecoder.java */
/* renamed from: ns3  reason: default package */
/* loaded from: classes.dex */
public class ns3 implements tf1 {
    public static final String u = "ns3";
    public int[] a;
    public final int[] b;
    public final tf1.a c;
    public ByteBuffer d;
    public byte[] e;
    public short[] f;
    public byte[] g;
    public byte[] h;
    public byte[] i;
    public int[] j;
    public int k;
    public cg1 l;
    public Bitmap m;
    public boolean n;
    public int o;
    public int p;
    public int q;
    public int r;
    public Boolean s;
    public Bitmap.Config t;

    public ns3(tf1.a aVar, cg1 cg1Var, ByteBuffer byteBuffer, int i) {
        this(aVar);
        q(cg1Var, byteBuffer, i);
    }

    @Override // defpackage.tf1
    public int a() {
        return this.l.c;
    }

    @Override // defpackage.tf1
    public synchronized Bitmap b() {
        if (this.l.c <= 0 || this.k < 0) {
            if (Log.isLoggable(u, 3)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to decode frame, frameCount=");
                sb.append(this.l.c);
                sb.append(", framePointer=");
                sb.append(this.k);
            }
            this.o = 1;
        }
        int i = this.o;
        if (i != 1 && i != 2) {
            this.o = 0;
            if (this.e == null) {
                this.e = this.c.e(255);
            }
            zf1 zf1Var = this.l.e.get(this.k);
            int i2 = this.k - 1;
            zf1 zf1Var2 = i2 >= 0 ? this.l.e.get(i2) : null;
            int[] iArr = zf1Var.k;
            if (iArr == null) {
                iArr = this.l.a;
            }
            this.a = iArr;
            if (iArr == null) {
                if (Log.isLoggable(u, 3)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("No valid color table found for frame #");
                    sb2.append(this.k);
                }
                this.o = 1;
                return null;
            }
            if (zf1Var.f) {
                System.arraycopy(iArr, 0, this.b, 0, iArr.length);
                int[] iArr2 = this.b;
                this.a = iArr2;
                iArr2[zf1Var.h] = 0;
                if (zf1Var.g == 2 && this.k == 0) {
                    this.s = Boolean.TRUE;
                }
            }
            return r(zf1Var, zf1Var2);
        }
        if (Log.isLoggable(u, 3)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to decode frame, status=");
            sb3.append(this.o);
        }
        return null;
    }

    @Override // defpackage.tf1
    public void c() {
        this.k = (this.k + 1) % this.l.c;
    }

    @Override // defpackage.tf1
    public void clear() {
        this.l = null;
        byte[] bArr = this.i;
        if (bArr != null) {
            this.c.d(bArr);
        }
        int[] iArr = this.j;
        if (iArr != null) {
            this.c.f(iArr);
        }
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.c.c(bitmap);
        }
        this.m = null;
        this.d = null;
        this.s = null;
        byte[] bArr2 = this.e;
        if (bArr2 != null) {
            this.c.d(bArr2);
        }
    }

    @Override // defpackage.tf1
    public int d() {
        int i;
        if (this.l.c <= 0 || (i = this.k) < 0) {
            return 0;
        }
        return m(i);
    }

    @Override // defpackage.tf1
    public void e(Bitmap.Config config) {
        if (config != Bitmap.Config.ARGB_8888 && config != Bitmap.Config.RGB_565) {
            throw new IllegalArgumentException("Unsupported format: " + config + ", must be one of " + Bitmap.Config.ARGB_8888 + " or " + Bitmap.Config.RGB_565);
        }
        this.t = config;
    }

    @Override // defpackage.tf1
    public void f() {
        this.k = -1;
    }

    @Override // defpackage.tf1
    public int g() {
        return this.k;
    }

    @Override // defpackage.tf1
    public ByteBuffer getData() {
        return this.d;
    }

    @Override // defpackage.tf1
    public int h() {
        return this.d.limit() + this.i.length + (this.j.length * 4);
    }

    public final int i(int i, int i2, int i3) {
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        for (int i9 = i; i9 < this.p + i; i9++) {
            byte[] bArr = this.i;
            if (i9 >= bArr.length || i9 >= i2) {
                break;
            }
            int i10 = this.a[bArr[i9] & 255];
            if (i10 != 0) {
                i4 += (i10 >> 24) & 255;
                i5 += (i10 >> 16) & 255;
                i6 += (i10 >> 8) & 255;
                i7 += i10 & 255;
                i8++;
            }
        }
        int i11 = i + i3;
        for (int i12 = i11; i12 < this.p + i11; i12++) {
            byte[] bArr2 = this.i;
            if (i12 >= bArr2.length || i12 >= i2) {
                break;
            }
            int i13 = this.a[bArr2[i12] & 255];
            if (i13 != 0) {
                i4 += (i13 >> 24) & 255;
                i5 += (i13 >> 16) & 255;
                i6 += (i13 >> 8) & 255;
                i7 += i13 & 255;
                i8++;
            }
        }
        if (i8 == 0) {
            return 0;
        }
        return ((i4 / i8) << 24) | ((i5 / i8) << 16) | ((i6 / i8) << 8) | (i7 / i8);
    }

    public final void j(zf1 zf1Var) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int[] iArr = this.j;
        int i6 = zf1Var.d;
        int i7 = this.p;
        int i8 = i6 / i7;
        int i9 = zf1Var.b / i7;
        int i10 = zf1Var.c / i7;
        int i11 = zf1Var.a / i7;
        boolean z = this.k == 0;
        int i12 = this.r;
        int i13 = this.q;
        byte[] bArr = this.i;
        int[] iArr2 = this.a;
        Boolean bool = this.s;
        int i14 = 8;
        int i15 = 0;
        int i16 = 0;
        int i17 = 1;
        while (i15 < i8) {
            Boolean bool2 = bool;
            if (zf1Var.e) {
                if (i16 >= i8) {
                    i = i8;
                    int i18 = i17 + 1;
                    if (i18 == 2) {
                        i17 = i18;
                        i16 = 4;
                    } else if (i18 == 3) {
                        i17 = i18;
                        i14 = 4;
                        i16 = 2;
                    } else if (i18 != 4) {
                        i17 = i18;
                    } else {
                        i17 = i18;
                        i16 = 1;
                        i14 = 2;
                    }
                } else {
                    i = i8;
                }
                i2 = i16 + i14;
            } else {
                i = i8;
                i2 = i16;
                i16 = i15;
            }
            int i19 = i16 + i9;
            boolean z2 = i7 == 1;
            if (i19 < i13) {
                int i20 = i19 * i12;
                int i21 = i20 + i11;
                int i22 = i21 + i10;
                int i23 = i20 + i12;
                if (i23 < i22) {
                    i22 = i23;
                }
                i3 = i2;
                int i24 = i15 * i7 * zf1Var.c;
                if (z2) {
                    int i25 = i21;
                    while (i25 < i22) {
                        int i26 = i9;
                        int i27 = iArr2[bArr[i24] & 255];
                        if (i27 != 0) {
                            iArr[i25] = i27;
                        } else if (z && bool2 == null) {
                            bool2 = Boolean.TRUE;
                        }
                        i24 += i7;
                        i25++;
                        i9 = i26;
                    }
                } else {
                    i5 = i9;
                    int i28 = ((i22 - i21) * i7) + i24;
                    int i29 = i21;
                    while (true) {
                        i4 = i10;
                        if (i29 < i22) {
                            int i30 = i(i24, i28, zf1Var.c);
                            if (i30 != 0) {
                                iArr[i29] = i30;
                            } else if (z && bool2 == null) {
                                bool2 = Boolean.TRUE;
                            }
                            i24 += i7;
                            i29++;
                            i10 = i4;
                        }
                    }
                    bool = bool2;
                    i15++;
                    i9 = i5;
                    i10 = i4;
                    i8 = i;
                    i16 = i3;
                }
            } else {
                i3 = i2;
            }
            i5 = i9;
            i4 = i10;
            bool = bool2;
            i15++;
            i9 = i5;
            i10 = i4;
            i8 = i;
            i16 = i3;
        }
        Boolean bool3 = bool;
        if (this.s == null) {
            this.s = Boolean.valueOf(bool3 == null ? false : bool3.booleanValue());
        }
    }

    public final void k(zf1 zf1Var) {
        zf1 zf1Var2 = zf1Var;
        int[] iArr = this.j;
        int i = zf1Var2.d;
        int i2 = zf1Var2.b;
        int i3 = zf1Var2.c;
        int i4 = zf1Var2.a;
        boolean z = this.k == 0;
        int i5 = this.r;
        byte[] bArr = this.i;
        int[] iArr2 = this.a;
        int i6 = 0;
        byte b = -1;
        while (i6 < i) {
            int i7 = (i6 + i2) * i5;
            int i8 = i7 + i4;
            int i9 = i8 + i3;
            int i10 = i7 + i5;
            if (i10 < i9) {
                i9 = i10;
            }
            int i11 = zf1Var2.c * i6;
            int i12 = i8;
            while (i12 < i9) {
                byte b2 = bArr[i11];
                int i13 = i;
                int i14 = b2 & 255;
                if (i14 != b) {
                    int i15 = iArr2[i14];
                    if (i15 != 0) {
                        iArr[i12] = i15;
                    } else {
                        b = b2;
                    }
                }
                i11++;
                i12++;
                i = i13;
            }
            i6++;
            zf1Var2 = zf1Var;
        }
        Boolean bool = this.s;
        this.s = Boolean.valueOf((bool != null && bool.booleanValue()) || (this.s == null && z && b != -1));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void l(zf1 zf1Var) {
        int i;
        int i2;
        short s;
        ns3 ns3Var = this;
        if (zf1Var != null) {
            ns3Var.d.position(zf1Var.j);
        }
        if (zf1Var == null) {
            cg1 cg1Var = ns3Var.l;
            i = cg1Var.f;
            i2 = cg1Var.g;
        } else {
            i = zf1Var.c;
            i2 = zf1Var.d;
        }
        int i3 = i * i2;
        byte[] bArr = ns3Var.i;
        if (bArr == null || bArr.length < i3) {
            ns3Var.i = ns3Var.c.e(i3);
        }
        byte[] bArr2 = ns3Var.i;
        if (ns3Var.f == null) {
            ns3Var.f = new short[4096];
        }
        short[] sArr = ns3Var.f;
        if (ns3Var.g == null) {
            ns3Var.g = new byte[4096];
        }
        byte[] bArr3 = ns3Var.g;
        if (ns3Var.h == null) {
            ns3Var.h = new byte[4097];
        }
        byte[] bArr4 = ns3Var.h;
        int p = p();
        int i4 = 1 << p;
        int i5 = i4 + 1;
        int i6 = i4 + 2;
        int i7 = p + 1;
        int i8 = (1 << i7) - 1;
        int i9 = 0;
        for (int i10 = 0; i10 < i4; i10++) {
            sArr[i10] = 0;
            bArr3[i10] = (byte) i10;
        }
        byte[] bArr5 = ns3Var.e;
        int i11 = i7;
        int i12 = i6;
        int i13 = i8;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        int i21 = -1;
        while (true) {
            if (i9 >= i3) {
                break;
            }
            if (i14 == 0) {
                i14 = o();
                if (i14 <= 0) {
                    ns3Var.o = 3;
                    break;
                }
                i15 = 0;
            }
            i17 += (bArr5[i15] & 255) << i16;
            i15++;
            i14--;
            int i22 = i16 + 8;
            int i23 = i12;
            int i24 = i11;
            int i25 = i21;
            int i26 = i7;
            int i27 = i19;
            while (true) {
                if (i22 < i24) {
                    i21 = i25;
                    i12 = i23;
                    i16 = i22;
                    ns3Var = this;
                    i19 = i27;
                    i7 = i26;
                    i11 = i24;
                    break;
                }
                int i28 = i6;
                int i29 = i17 & i13;
                i17 >>= i24;
                i22 -= i24;
                if (i29 == i4) {
                    i13 = i8;
                    i24 = i26;
                    i23 = i28;
                    i6 = i23;
                    i25 = -1;
                } else if (i29 == i5) {
                    i16 = i22;
                    i19 = i27;
                    i12 = i23;
                    i7 = i26;
                    i6 = i28;
                    i21 = i25;
                    i11 = i24;
                    ns3Var = this;
                    break;
                } else if (i25 == -1) {
                    bArr2[i18] = bArr3[i29];
                    i18++;
                    i9++;
                    i25 = i29;
                    i27 = i25;
                    i6 = i28;
                    i22 = i22;
                } else {
                    if (i29 >= i23) {
                        bArr4[i20] = (byte) i27;
                        i20++;
                        s = i25;
                    } else {
                        s = i29;
                    }
                    while (s >= i4) {
                        bArr4[i20] = bArr3[s];
                        i20++;
                        s = sArr[s];
                    }
                    i27 = bArr3[s] & 255;
                    byte b = (byte) i27;
                    bArr2[i18] = b;
                    while (true) {
                        i18++;
                        i9++;
                        if (i20 <= 0) {
                            break;
                        }
                        i20--;
                        bArr2[i18] = bArr4[i20];
                    }
                    byte[] bArr6 = bArr4;
                    if (i23 < 4096) {
                        sArr[i23] = (short) i25;
                        bArr3[i23] = b;
                        i23++;
                        if ((i23 & i13) == 0 && i23 < 4096) {
                            i24++;
                            i13 += i23;
                        }
                    }
                    i25 = i29;
                    i6 = i28;
                    i22 = i22;
                    bArr4 = bArr6;
                }
            }
        }
        Arrays.fill(bArr2, i18, i3, (byte) 0);
    }

    public int m(int i) {
        if (i >= 0) {
            cg1 cg1Var = this.l;
            if (i < cg1Var.c) {
                return cg1Var.e.get(i).i;
            }
        }
        return -1;
    }

    public final Bitmap n() {
        Boolean bool = this.s;
        Bitmap a = this.c.a(this.r, this.q, (bool == null || bool.booleanValue()) ? Bitmap.Config.ARGB_8888 : this.t);
        a.setHasAlpha(true);
        return a;
    }

    public final int o() {
        int p = p();
        if (p <= 0) {
            return p;
        }
        ByteBuffer byteBuffer = this.d;
        byteBuffer.get(this.e, 0, Math.min(p, byteBuffer.remaining()));
        return p;
    }

    public final int p() {
        return this.d.get() & 255;
    }

    public synchronized void q(cg1 cg1Var, ByteBuffer byteBuffer, int i) {
        if (i > 0) {
            int highestOneBit = Integer.highestOneBit(i);
            this.o = 0;
            this.l = cg1Var;
            this.k = -1;
            ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
            this.d = asReadOnlyBuffer;
            asReadOnlyBuffer.position(0);
            this.d.order(ByteOrder.LITTLE_ENDIAN);
            this.n = false;
            Iterator<zf1> it = cg1Var.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                } else if (it.next().g == 3) {
                    this.n = true;
                    break;
                }
            }
            this.p = highestOneBit;
            int i2 = cg1Var.f;
            this.r = i2 / highestOneBit;
            int i3 = cg1Var.g;
            this.q = i3 / highestOneBit;
            this.i = this.c.e(i2 * i3);
            this.j = this.c.b(this.r * this.q);
        } else {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i);
        }
    }

    public final Bitmap r(zf1 zf1Var, zf1 zf1Var2) {
        int i;
        int i2;
        Bitmap bitmap;
        int[] iArr = this.j;
        int i3 = 0;
        if (zf1Var2 == null) {
            Bitmap bitmap2 = this.m;
            if (bitmap2 != null) {
                this.c.c(bitmap2);
            }
            this.m = null;
            Arrays.fill(iArr, 0);
        }
        if (zf1Var2 != null && zf1Var2.g == 3 && this.m == null) {
            Arrays.fill(iArr, 0);
        }
        if (zf1Var2 != null && (i2 = zf1Var2.g) > 0) {
            if (i2 == 2) {
                if (!zf1Var.f) {
                    cg1 cg1Var = this.l;
                    int i4 = cg1Var.l;
                    if (zf1Var.k == null || cg1Var.j != zf1Var.h) {
                        i3 = i4;
                    }
                }
                int i5 = zf1Var2.d;
                int i6 = this.p;
                int i7 = i5 / i6;
                int i8 = zf1Var2.b / i6;
                int i9 = zf1Var2.c / i6;
                int i10 = zf1Var2.a / i6;
                int i11 = this.r;
                int i12 = (i8 * i11) + i10;
                int i13 = (i7 * i11) + i12;
                while (i12 < i13) {
                    int i14 = i12 + i9;
                    for (int i15 = i12; i15 < i14; i15++) {
                        iArr[i15] = i3;
                    }
                    i12 += this.r;
                }
            } else if (i2 == 3 && (bitmap = this.m) != null) {
                int i16 = this.r;
                bitmap.getPixels(iArr, 0, i16, 0, 0, i16, this.q);
            }
        }
        l(zf1Var);
        if (!zf1Var.e && this.p == 1) {
            k(zf1Var);
        } else {
            j(zf1Var);
        }
        if (this.n && ((i = zf1Var.g) == 0 || i == 1)) {
            if (this.m == null) {
                this.m = n();
            }
            Bitmap bitmap3 = this.m;
            int i17 = this.r;
            bitmap3.setPixels(iArr, 0, i17, 0, 0, i17, this.q);
        }
        Bitmap n = n();
        int i18 = this.r;
        n.setPixels(iArr, 0, i18, 0, 0, i18, this.q);
        return n;
    }

    public ns3(tf1.a aVar) {
        this.b = new int[256];
        this.t = Bitmap.Config.ARGB_8888;
        this.c = aVar;
        this.l = new cg1();
    }
}
