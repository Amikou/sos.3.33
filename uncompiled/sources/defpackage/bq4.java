package defpackage;

import android.content.Context;
import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import java.util.UUID;

/* compiled from: WorkForegroundUpdater.java */
/* renamed from: bq4  reason: default package */
/* loaded from: classes.dex */
public class bq4 implements u81 {
    public final q34 a;
    public final t81 b;
    public final uq4 c;

    /* compiled from: WorkForegroundUpdater.java */
    /* renamed from: bq4$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ wm3 a;
        public final /* synthetic */ UUID f0;
        public final /* synthetic */ s81 g0;
        public final /* synthetic */ Context h0;

        public a(wm3 wm3Var, UUID uuid, s81 s81Var, Context context) {
            this.a = wm3Var;
            this.f0 = uuid;
            this.g0 = s81Var;
            this.h0 = context;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                if (!this.a.isCancelled()) {
                    String uuid = this.f0.toString();
                    WorkInfo.State k = bq4.this.c.k(uuid);
                    if (k != null && !k.isFinished()) {
                        bq4.this.b.a(uuid, this.g0);
                        this.h0.startService(androidx.work.impl.foreground.a.a(this.h0, uuid, this.g0));
                    } else {
                        throw new IllegalStateException("Calls to setForegroundAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                    }
                }
                this.a.p(null);
            } catch (Throwable th) {
                this.a.q(th);
            }
        }
    }

    static {
        v12.f("WMFgUpdater");
    }

    public bq4(WorkDatabase workDatabase, t81 t81Var, q34 q34Var) {
        this.b = t81Var;
        this.a = q34Var;
        this.c = workDatabase.P();
    }

    @Override // defpackage.u81
    public l02<Void> a(Context context, UUID uuid, s81 s81Var) {
        wm3 t = wm3.t();
        this.a.b(new a(t, uuid, s81Var, context));
        return t;
    }
}
