package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.PrintStream;
import java.util.Date;
import java.util.Random;

/* compiled from: LIB.java */
/* renamed from: ay1  reason: default package */
/* loaded from: classes.dex */
public class ay1 {
    public static byte[] a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        int length = str.length();
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) str.charAt(i);
        }
        return bArr;
    }

    public static String b(int i) {
        if (i < 1) {
            return new String();
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        byte[] c = c(i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append((char) c[i2]);
        }
        return stringBuffer.toString();
    }

    public static byte[] c(int i) {
        int i2;
        byte[] bArr = new byte[i];
        Random e = e();
        int i3 = 0;
        while (i3 < i) {
            int nextLong = (int) e.nextLong();
            if (nextLong < 0) {
                nextLong *= -1;
            }
            if (i3 == 0) {
                i2 = (nextLong % 94) + 32;
            } else {
                i2 = nextLong % 255;
            }
            if (i2 > 0) {
                bArr[i3] = (byte) i2;
                i3++;
            }
        }
        return bArr;
    }

    public static String d(byte[] bArr) {
        String str = new String();
        char[] cArr = new char[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = bArr[i];
            if (i2 < 0) {
                i2 += 256;
            }
            cArr[i] = (char) i2;
            StringBuilder sb = new StringBuilder(String.valueOf(str));
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb.append(cArr[i] < 16 ? "0" : "");
            sb.append(Integer.toHexString(cArr[i]).toUpperCase());
            str = sb.toString();
        }
        return str;
    }

    public static Random e() {
        return new Random(new Date().getTime());
    }

    public static String f(byte[] bArr) {
        if (bArr == null) {
            return new String();
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            stringBuffer.append((char) b);
        }
        return stringBuffer.toString();
    }

    public static String g(String str) {
        if (str == null) {
            return new String();
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char charAt = (char) ((str.charAt(i) >>> 4) & 15);
            char charAt2 = (char) (str.charAt(i) & 15);
            char c = (char) (charAt > '\t' ? (charAt - '\n') + 97 : charAt + '0');
            int i2 = charAt2 > '\t' ? (charAt2 - '\n') + 97 : charAt2 + '0';
            stringBuffer.append(c);
            stringBuffer.append((char) i2);
        }
        return stringBuffer.toString();
    }

    public static byte[] h(byte[] bArr, String str) {
        if (bArr != null && str != null && str.compareTo("") != 0) {
            byte[] bArr2 = new byte[40];
            new ez4();
            byte[] c = ez4.c(bArr);
            byte[] a = ez4.a(str);
            System.arraycopy(c, 0, bArr2, 0, 20);
            System.arraycopy(a, 0, bArr2, 20, 20);
            new cz4();
            byte[] bArr3 = new byte[20];
            String d = d(bArr2);
            PrintStream printStream = System.out;
            printStream.println("LIB 496 seed=" + d);
            if (cz4.h(bArr2) && cz4.g(bArr3)) {
                String d2 = d(bArr3);
                PrintStream printStream2 = System.out;
                printStream2.println("LIB 503 seed=" + d2);
                return bArr3;
            }
        }
        return null;
    }

    public static int i(short s, short s2, short s3, short s4) {
        return (s & 255) | ((s2 & 255) << 8) | ((s3 & 255) << 16) | ((s4 & 255) << 24);
    }

    public static String j(char c) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append((char) ((byte) (c & 255)));
        stringBuffer.append((char) ((byte) ((c >> '\b') & 255)));
        return stringBuffer.toString();
    }

    public static void k(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 & 255);
        bArr[i + 1] = (byte) ((i2 >> 8) & 255);
        bArr[i + 2] = (byte) ((i2 >> 16) & 255);
        bArr[i + 3] = (byte) ((i2 >> 24) & 255);
    }

    public static int l(int i, int i2) {
        return (i2 >> (i * 8)) & 255;
    }

    public static int m(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) | (bArr[i + 3] & 255) | ((bArr[i + 2] & 255) << 8) | ((bArr[i + 1] & 255) << 16);
    }

    public static void n(int i, byte[] bArr, int i2) {
        int i3 = i >> 8;
        int i4 = i3 >> 8;
        bArr[i2] = (byte) ((i4 >> 8) & 255);
        bArr[i2 + 1] = (byte) (i4 & 255);
        bArr[i2 + 2] = (byte) (i3 & 255);
        bArr[i2 + 3] = (byte) (i & 255);
    }

    public static int o(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public static int p(char[] cArr, int i) {
        return ((cArr[i + 3] & 255) << 24) | (cArr[i] & 255) | ((cArr[i + 1] & 255) << 8) | ((cArr[i + 2] & 255) << 16);
    }

    public static int q(int i, int i2) {
        int i3;
        int i4;
        int i5;
        if (i == 1) {
            i5 = (i2 >> 24) & 255;
        } else {
            if (i == 2) {
                i3 = i2 >> 16;
                i4 = 65535;
            } else {
                i3 = i2 >> 8;
                i4 = 16777215;
            }
            i5 = i3 & i4;
        }
        return (i2 << (i * 8)) | i5;
    }
}
