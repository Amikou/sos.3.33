package defpackage;

import java.util.Collections;
import java.util.Iterator;
import java.util.TreeMap;

/* compiled from: HandshakedataImpl1.java */
/* renamed from: uj1  reason: default package */
/* loaded from: classes2.dex */
public class uj1 implements qj1 {
    public byte[] a;
    public TreeMap<String, String> b = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    @Override // defpackage.tj1
    public Iterator<String> b() {
        return Collections.unmodifiableSet(this.b.keySet()).iterator();
    }

    @Override // defpackage.tj1
    public boolean d(String str) {
        return this.b.containsKey(str);
    }

    @Override // defpackage.tj1
    public byte[] getContent() {
        return this.a;
    }

    @Override // defpackage.tj1
    public String h(String str) {
        String str2 = this.b.get(str);
        return str2 == null ? "" : str2;
    }

    @Override // defpackage.qj1
    public void put(String str, String str2) {
        this.b.put(str, str2);
    }
}
