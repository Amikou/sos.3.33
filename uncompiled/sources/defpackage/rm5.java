package defpackage;

import android.content.ComponentName;
import android.os.Handler;
import android.os.Message;
import defpackage.rg1;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: rm5  reason: default package */
/* loaded from: classes.dex */
public final class rm5 implements Handler.Callback {
    public final /* synthetic */ rk5 a;

    public rm5(rk5 rk5Var) {
        this.a = rk5Var;
    }

    @Override // android.os.Handler.Callback
    public final boolean handleMessage(Message message) {
        HashMap hashMap;
        HashMap hashMap2;
        HashMap hashMap3;
        HashMap hashMap4;
        HashMap hashMap5;
        int i = message.what;
        if (i == 0) {
            hashMap = this.a.d;
            synchronized (hashMap) {
                rg1.a aVar = (rg1.a) message.obj;
                hashMap2 = this.a.d;
                np5 np5Var = (np5) hashMap2.get(aVar);
                if (np5Var != null && np5Var.h()) {
                    if (np5Var.d()) {
                        np5Var.g("GmsClientSupervisor");
                    }
                    hashMap3 = this.a.d;
                    hashMap3.remove(aVar);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            hashMap4 = this.a.d;
            synchronized (hashMap4) {
                rg1.a aVar2 = (rg1.a) message.obj;
                hashMap5 = this.a.d;
                np5 np5Var2 = (np5) hashMap5.get(aVar2);
                if (np5Var2 != null && np5Var2.f() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    new Exception();
                    ComponentName j = np5Var2.j();
                    if (j == null) {
                        j = aVar2.c();
                    }
                    if (j == null) {
                        j = new ComponentName((String) zt2.j(aVar2.b()), "unknown");
                    }
                    np5Var2.onServiceDisconnected(j);
                }
            }
            return true;
        }
    }
}
