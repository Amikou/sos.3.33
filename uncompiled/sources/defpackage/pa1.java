package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentNotificationHistoryBinding.java */
/* renamed from: pa1  reason: default package */
/* loaded from: classes2.dex */
public final class pa1 {
    public final ConstraintLayout a;
    public final TextView b;
    public final TextView c;
    public final TextView d;
    public final TextView e;
    public final RecyclerView f;
    public final SwipeRefreshLayout g;
    public final RelativeLayout h;
    public final rp3 i;
    public final TextView j;

    public pa1(ConstraintLayout constraintLayout, FrameLayout frameLayout, TextView textView, TextView textView2, TextView textView3, TextView textView4, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, RelativeLayout relativeLayout, rp3 rp3Var, TextView textView5) {
        this.a = constraintLayout;
        this.b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = textView4;
        this.f = recyclerView;
        this.g = swipeRefreshLayout;
        this.h = relativeLayout;
        this.i = rp3Var;
        this.j = textView5;
    }

    public static pa1 a(View view) {
        int i = R.id.cv_2;
        FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.cv_2);
        if (frameLayout != null) {
            i = R.id.delete;
            TextView textView = (TextView) ai4.a(view, R.id.delete);
            if (textView != null) {
                i = R.id.deleteAll;
                TextView textView2 = (TextView) ai4.a(view, R.id.deleteAll);
                if (textView2 != null) {
                    i = R.id.markAllAsRead;
                    TextView textView3 = (TextView) ai4.a(view, R.id.markAllAsRead);
                    if (textView3 != null) {
                        i = R.id.markAsRead;
                        TextView textView4 = (TextView) ai4.a(view, R.id.markAsRead);
                        if (textView4 != null) {
                            i = R.id.rvNotifications;
                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvNotifications);
                            if (recyclerView != null) {
                                i = R.id.rvTransGroup;
                                SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.rvTransGroup);
                                if (swipeRefreshLayout != null) {
                                    i = R.id.selectOptions;
                                    RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.selectOptions);
                                    if (relativeLayout != null) {
                                        i = R.id.toolbar;
                                        View a = ai4.a(view, R.id.toolbar);
                                        if (a != null) {
                                            rp3 a2 = rp3.a(a);
                                            i = R.id.tvNoNotifications;
                                            TextView textView5 = (TextView) ai4.a(view, R.id.tvNoNotifications);
                                            if (textView5 != null) {
                                                return new pa1((ConstraintLayout) view, frameLayout, textView, textView2, textView3, textView4, recyclerView, swipeRefreshLayout, relativeLayout, a2, textView5);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
