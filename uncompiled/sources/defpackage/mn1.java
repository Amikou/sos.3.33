package defpackage;

import android.os.Build;
import android.view.View;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: IdUtils.java */
/* renamed from: mn1  reason: default package */
/* loaded from: classes2.dex */
public class mn1 {
    public static final AtomicInteger a = new AtomicInteger(1);

    public static int a() {
        AtomicInteger atomicInteger;
        int i;
        int i2;
        do {
            atomicInteger = a;
            i = atomicInteger.get();
            i2 = i + 1;
            if (i2 > 16777215) {
                i2 = 1;
            }
        } while (!atomicInteger.compareAndSet(i, i2));
        return i;
    }

    public static int b() {
        if (Build.VERSION.SDK_INT < 17) {
            return a();
        }
        return View.generateViewId();
    }
}
