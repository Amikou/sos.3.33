package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: rh3  reason: default package */
/* loaded from: classes2.dex */
public class rh3 extends ct0.a {
    public long[] f;

    public rh3() {
        this.f = jd2.b();
    }

    public rh3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 571) {
            throw new IllegalArgumentException("x value invalid for SecT571FieldElement");
        }
        this.f = qh3.g(bigInteger);
    }

    public rh3(long[] jArr) {
        this.f = jArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        long[] b = jd2.b();
        qh3.b(this.f, ((rh3) ct0Var).f, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        long[] b = jd2.b();
        qh3.f(this.f, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        return j(ct0Var.g());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof rh3) {
            return jd2.d(this.f, ((rh3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return 571;
    }

    @Override // defpackage.ct0
    public ct0 g() {
        long[] b = jd2.b();
        qh3.k(this.f, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return jd2.f(this.f);
    }

    public int hashCode() {
        return wh.v(this.f, 0, 9) ^ 5711052;
    }

    @Override // defpackage.ct0
    public boolean i() {
        return jd2.g(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        long[] b = jd2.b();
        qh3.l(this.f, ((rh3) ct0Var).f, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return l(ct0Var, ct0Var2, ct0Var3);
    }

    @Override // defpackage.ct0
    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        long[] jArr = this.f;
        long[] jArr2 = ((rh3) ct0Var).f;
        long[] jArr3 = ((rh3) ct0Var2).f;
        long[] jArr4 = ((rh3) ct0Var3).f;
        long[] c = jd2.c();
        qh3.m(jArr, jArr2, c);
        qh3.m(jArr3, jArr4, c);
        long[] b = jd2.b();
        qh3.q(c, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        return this;
    }

    @Override // defpackage.ct0
    public ct0 n() {
        long[] b = jd2.b();
        qh3.s(this.f, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 o() {
        long[] b = jd2.b();
        qh3.t(this.f, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        long[] jArr = this.f;
        long[] jArr2 = ((rh3) ct0Var).f;
        long[] jArr3 = ((rh3) ct0Var2).f;
        long[] c = jd2.c();
        qh3.u(jArr, c);
        qh3.m(jArr2, jArr3, c);
        long[] b = jd2.b();
        qh3.q(c, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 q(int i) {
        if (i < 1) {
            return this;
        }
        long[] b = jd2.b();
        qh3.v(this.f, i, b);
        return new rh3(b);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        return a(ct0Var);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return (this.f[0] & 1) != 0;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return jd2.h(this.f);
    }

    @Override // defpackage.ct0.a
    public int u() {
        return qh3.w(this.f);
    }
}
