package defpackage;

import android.content.Context;
import androidx.work.impl.background.systemalarm.a;

/* compiled from: SystemAlarmScheduler.java */
/* renamed from: n24  reason: default package */
/* loaded from: classes.dex */
public class n24 implements cd3 {
    public static final String f0 = v12.f("SystemAlarmScheduler");
    public final Context a;

    public n24(Context context) {
        this.a = context.getApplicationContext();
    }

    @Override // defpackage.cd3
    public boolean a() {
        return true;
    }

    public final void b(tq4 tq4Var) {
        v12.c().a(f0, String.format("Scheduling work with workSpecId %s", tq4Var.a), new Throwable[0]);
        this.a.startService(a.f(this.a, tq4Var.a));
    }

    @Override // defpackage.cd3
    public void d(String str) {
        this.a.startService(a.g(this.a, str));
    }

    @Override // defpackage.cd3
    public void e(tq4... tq4VarArr) {
        for (tq4 tq4Var : tq4VarArr) {
            b(tq4Var);
        }
    }
}
