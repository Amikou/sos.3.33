package defpackage;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import com.github.mikephil.charting.utils.Utils;
import defpackage.j81;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ExploreByTouchHelper.java */
/* renamed from: c11  reason: default package */
/* loaded from: classes.dex */
public abstract class c11 extends z5 {
    public static final Rect n = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    public static final j81.a<b6> o = new a();
    public static final j81.b<lr3<b6>, b6> p = new b();
    public final AccessibilityManager h;
    public final View i;
    public c j;
    public final Rect d = new Rect();
    public final Rect e = new Rect();
    public final Rect f = new Rect();
    public final int[] g = new int[2];
    public int k = Integer.MIN_VALUE;
    public int l = Integer.MIN_VALUE;
    public int m = Integer.MIN_VALUE;

    /* compiled from: ExploreByTouchHelper.java */
    /* renamed from: c11$a */
    /* loaded from: classes.dex */
    public class a implements j81.a<b6> {
        @Override // defpackage.j81.a
        /* renamed from: b */
        public void a(b6 b6Var, Rect rect) {
            b6Var.m(rect);
        }
    }

    /* compiled from: ExploreByTouchHelper.java */
    /* renamed from: c11$b */
    /* loaded from: classes.dex */
    public class b implements j81.b<lr3<b6>, b6> {
        @Override // defpackage.j81.b
        /* renamed from: c */
        public b6 a(lr3<b6> lr3Var, int i) {
            return lr3Var.p(i);
        }

        @Override // defpackage.j81.b
        /* renamed from: d */
        public int b(lr3<b6> lr3Var) {
            return lr3Var.o();
        }
    }

    /* compiled from: ExploreByTouchHelper.java */
    /* renamed from: c11$c */
    /* loaded from: classes.dex */
    public class c extends c6 {
        public c() {
        }

        @Override // defpackage.c6
        public b6 b(int i) {
            return b6.P(c11.this.J(i));
        }

        @Override // defpackage.c6
        public b6 d(int i) {
            int i2 = i == 2 ? c11.this.k : c11.this.l;
            if (i2 == Integer.MIN_VALUE) {
                return null;
            }
            return b(i2);
        }

        @Override // defpackage.c6
        public boolean f(int i, int i2, Bundle bundle) {
            return c11.this.R(i, i2, bundle);
        }
    }

    public c11(View view) {
        if (view != null) {
            this.i = view;
            this.h = (AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (ei4.C(view) == 0) {
                ei4.D0(view, 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }

    public static Rect D(View view, int i, Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        if (i == 17) {
            rect.set(width, 0, width, height);
        } else if (i == 33) {
            rect.set(0, height, width, height);
        } else if (i == 66) {
            rect.set(-1, 0, -1, height);
        } else if (i == 130) {
            rect.set(0, -1, width, -1);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    public static int H(int i) {
        if (i != 19) {
            if (i != 21) {
                return i != 22 ? 130 : 66;
            }
            return 17;
        }
        return 33;
    }

    public final int A() {
        return this.l;
    }

    public abstract int B(float f, float f2);

    public abstract void C(List<Integer> list);

    public final void E(int i) {
        F(i, 0);
    }

    public final void F(int i, int i2) {
        ViewParent parent;
        if (i == Integer.MIN_VALUE || !this.h.isEnabled() || (parent = this.i.getParent()) == null) {
            return;
        }
        AccessibilityEvent q = q(i, 2048);
        a6.b(q, i2);
        parent.requestSendAccessibilityEvent(this.i, q);
    }

    public final boolean G(Rect rect) {
        if (rect == null || rect.isEmpty() || this.i.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.i.getParent();
        while (parent instanceof View) {
            View view = (View) parent;
            if (view.getAlpha() <= Utils.FLOAT_EPSILON || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        return parent != null;
    }

    public final boolean I(int i, Rect rect) {
        b6 b6Var;
        lr3<b6> y = y();
        int i2 = this.l;
        b6 f = i2 == Integer.MIN_VALUE ? null : y.f(i2);
        if (i == 1 || i == 2) {
            b6Var = (b6) j81.d(y, p, o, f, i, ei4.E(this.i) == 1, false);
        } else if (i != 17 && i != 33 && i != 66 && i != 130) {
            throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        } else {
            Rect rect2 = new Rect();
            int i3 = this.l;
            if (i3 != Integer.MIN_VALUE) {
                z(i3, rect2);
            } else if (rect != null) {
                rect2.set(rect);
            } else {
                D(this.i, i, rect2);
            }
            b6Var = (b6) j81.c(y, p, o, f, rect2, i);
        }
        return V(b6Var != null ? y.j(y.h(b6Var)) : Integer.MIN_VALUE);
    }

    public b6 J(int i) {
        if (i == -1) {
            return u();
        }
        return t(i);
    }

    public final void K(boolean z, int i, Rect rect) {
        int i2 = this.l;
        if (i2 != Integer.MIN_VALUE) {
            o(i2);
        }
        if (z) {
            I(i, rect);
        }
    }

    public abstract boolean L(int i, int i2, Bundle bundle);

    public void M(AccessibilityEvent accessibilityEvent) {
    }

    public void N(int i, AccessibilityEvent accessibilityEvent) {
    }

    public void O(b6 b6Var) {
    }

    public abstract void P(int i, b6 b6Var);

    public void Q(int i, boolean z) {
    }

    public boolean R(int i, int i2, Bundle bundle) {
        if (i != -1) {
            return S(i, i2, bundle);
        }
        return T(i2, bundle);
    }

    public final boolean S(int i, int i2, Bundle bundle) {
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 64) {
                    if (i2 != 128) {
                        return L(i, i2, bundle);
                    }
                    return n(i);
                }
                return U(i);
            }
            return o(i);
        }
        return V(i);
    }

    public final boolean T(int i, Bundle bundle) {
        return ei4.h0(this.i, i, bundle);
    }

    public final boolean U(int i) {
        int i2;
        if (this.h.isEnabled() && this.h.isTouchExplorationEnabled() && (i2 = this.k) != i) {
            if (i2 != Integer.MIN_VALUE) {
                n(i2);
            }
            this.k = i;
            this.i.invalidate();
            W(i, 32768);
            return true;
        }
        return false;
    }

    public final boolean V(int i) {
        int i2;
        if ((this.i.isFocused() || this.i.requestFocus()) && (i2 = this.l) != i) {
            if (i2 != Integer.MIN_VALUE) {
                o(i2);
            }
            if (i == Integer.MIN_VALUE) {
                return false;
            }
            this.l = i;
            Q(i, true);
            W(i, 8);
            return true;
        }
        return false;
    }

    public final boolean W(int i, int i2) {
        ViewParent parent;
        if (i == Integer.MIN_VALUE || !this.h.isEnabled() || (parent = this.i.getParent()) == null) {
            return false;
        }
        return parent.requestSendAccessibilityEvent(this.i, q(i, i2));
    }

    public final void X(int i) {
        int i2 = this.m;
        if (i2 == i) {
            return;
        }
        this.m = i;
        W(i, 128);
        W(i2, 256);
    }

    @Override // defpackage.z5
    public c6 b(View view) {
        if (this.j == null) {
            this.j = new c();
        }
        return this.j;
    }

    @Override // defpackage.z5
    public void f(View view, AccessibilityEvent accessibilityEvent) {
        super.f(view, accessibilityEvent);
        M(accessibilityEvent);
    }

    @Override // defpackage.z5
    public void g(View view, b6 b6Var) {
        super.g(view, b6Var);
        O(b6Var);
    }

    public final boolean n(int i) {
        if (this.k == i) {
            this.k = Integer.MIN_VALUE;
            this.i.invalidate();
            W(i, 65536);
            return true;
        }
        return false;
    }

    public final boolean o(int i) {
        if (this.l != i) {
            return false;
        }
        this.l = Integer.MIN_VALUE;
        Q(i, false);
        W(i, 8);
        return true;
    }

    public final boolean p() {
        int i = this.l;
        return i != Integer.MIN_VALUE && L(i, 16, null);
    }

    public final AccessibilityEvent q(int i, int i2) {
        if (i != -1) {
            return r(i, i2);
        }
        return s(i2);
    }

    public final AccessibilityEvent r(int i, int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        b6 J = J(i);
        obtain.getText().add(J.x());
        obtain.setContentDescription(J.r());
        obtain.setScrollable(J.K());
        obtain.setPassword(J.J());
        obtain.setEnabled(J.F());
        obtain.setChecked(J.D());
        N(i, obtain);
        if (obtain.getText().isEmpty() && obtain.getContentDescription() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
        }
        obtain.setClassName(J.p());
        d6.c(obtain, this.i, i);
        obtain.setPackageName(this.i.getContext().getPackageName());
        return obtain;
    }

    public final AccessibilityEvent s(int i) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
        this.i.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }

    public final b6 t(int i) {
        b6 O = b6.O();
        O.i0(true);
        O.k0(true);
        O.c0("android.view.View");
        Rect rect = n;
        O.X(rect);
        O.Y(rect);
        O.t0(this.i);
        P(i, O);
        if (O.x() == null && O.r() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        O.m(this.e);
        if (!this.e.equals(rect)) {
            int k = O.k();
            if ((k & 64) == 0) {
                if ((k & 128) == 0) {
                    O.r0(this.i.getContext().getPackageName());
                    O.C0(this.i, i);
                    if (this.k == i) {
                        O.V(true);
                        O.a(128);
                    } else {
                        O.V(false);
                        O.a(64);
                    }
                    boolean z = this.l == i;
                    if (z) {
                        O.a(2);
                    } else if (O.G()) {
                        O.a(1);
                    }
                    O.l0(z);
                    this.i.getLocationOnScreen(this.g);
                    O.n(this.d);
                    if (this.d.equals(rect)) {
                        O.m(this.d);
                        if (O.b != -1) {
                            b6 O2 = b6.O();
                            for (int i2 = O.b; i2 != -1; i2 = O2.b) {
                                O2.u0(this.i, -1);
                                O2.X(n);
                                P(i2, O2);
                                O2.m(this.e);
                                Rect rect2 = this.d;
                                Rect rect3 = this.e;
                                rect2.offset(rect3.left, rect3.top);
                            }
                            O2.S();
                        }
                        this.d.offset(this.g[0] - this.i.getScrollX(), this.g[1] - this.i.getScrollY());
                    }
                    if (this.i.getLocalVisibleRect(this.f)) {
                        this.f.offset(this.g[0] - this.i.getScrollX(), this.g[1] - this.i.getScrollY());
                        if (this.d.intersect(this.f)) {
                            O.Y(this.d);
                            if (G(this.d)) {
                                O.G0(true);
                            }
                        }
                    }
                    return O;
                }
                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
            throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        }
        throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
    }

    public final b6 u() {
        b6 Q = b6.Q(this.i);
        ei4.f0(this.i, Q);
        ArrayList arrayList = new ArrayList();
        C(arrayList);
        if (Q.o() > 0 && arrayList.size() > 0) {
            throw new RuntimeException("Views cannot have both real and virtual children");
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Q.d(this.i, ((Integer) arrayList.get(i)).intValue());
        }
        return Q;
    }

    public final boolean v(MotionEvent motionEvent) {
        if (this.h.isEnabled() && this.h.isTouchExplorationEnabled()) {
            int action = motionEvent.getAction();
            if (action != 7 && action != 9) {
                if (action == 10 && this.m != Integer.MIN_VALUE) {
                    X(Integer.MIN_VALUE);
                    return true;
                }
                return false;
            }
            int B = B(motionEvent.getX(), motionEvent.getY());
            X(B);
            return B != Integer.MIN_VALUE;
        }
        return false;
    }

    public final boolean w(KeyEvent keyEvent) {
        int i = 0;
        if (keyEvent.getAction() != 1) {
            int keyCode = keyEvent.getKeyCode();
            if (keyCode != 61) {
                if (keyCode != 66) {
                    switch (keyCode) {
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                            if (keyEvent.hasNoModifiers()) {
                                int H = H(keyCode);
                                int repeatCount = keyEvent.getRepeatCount() + 1;
                                boolean z = false;
                                while (i < repeatCount && I(H, null)) {
                                    i++;
                                    z = true;
                                }
                                return z;
                            }
                            return false;
                        case 23:
                            break;
                        default:
                            return false;
                    }
                }
                if (keyEvent.hasNoModifiers() && keyEvent.getRepeatCount() == 0) {
                    p();
                    return true;
                }
                return false;
            } else if (keyEvent.hasNoModifiers()) {
                return I(2, null);
            } else {
                if (keyEvent.hasModifiers(1)) {
                    return I(1, null);
                }
                return false;
            }
        }
        return false;
    }

    public final int x() {
        return this.k;
    }

    public final lr3<b6> y() {
        ArrayList arrayList = new ArrayList();
        C(arrayList);
        lr3<b6> lr3Var = new lr3<>();
        for (int i = 0; i < arrayList.size(); i++) {
            lr3Var.k(arrayList.get(i).intValue(), t(arrayList.get(i).intValue()));
        }
        return lr3Var;
    }

    public final void z(int i, Rect rect) {
        J(i).m(rect);
    }
}
