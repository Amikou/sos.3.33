package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: jm2  reason: default package */
/* loaded from: classes.dex */
public interface jm2 {
    void onConnectionFailed(@RecentlyNonNull ConnectionResult connectionResult);
}
