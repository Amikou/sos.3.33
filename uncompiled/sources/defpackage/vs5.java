package defpackage;

import com.google.android.gms.internal.vision.b0;
import com.google.android.gms.internal.vision.b1;
import com.google.android.gms.internal.vision.e0;
import com.google.android.gms.internal.vision.n0;
import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: vs5  reason: default package */
/* loaded from: classes.dex */
public final class vs5 {
    public static final Charset a = Charset.forName("UTF-8");
    public static final byte[] b;

    static {
        Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING);
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        e0.b(bArr, 0, bArr.length, false);
    }

    public static int a(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    public static int b(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static int c(boolean z) {
        return z ? 1231 : 1237;
    }

    public static <T> T d(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static Object e(Object obj, Object obj2) {
        return ((n0) obj).d().F0((n0) obj2).c();
    }

    public static <T> T f(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static boolean g(n0 n0Var) {
        if (n0Var instanceof b0) {
            b0 b0Var = (b0) n0Var;
            return false;
        }
        return false;
    }

    public static boolean h(byte[] bArr) {
        return b1.f(bArr);
    }

    public static String i(byte[] bArr) {
        return new String(bArr, a);
    }

    public static int j(byte[] bArr) {
        int length = bArr.length;
        int a2 = a(length, bArr, 0, length);
        if (a2 == 0) {
            return 1;
        }
        return a2;
    }
}
