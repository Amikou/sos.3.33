package defpackage;

import com.onesignal.OneSignal;
import com.onesignal.c1;
import com.onesignal.influence.domain.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

/* compiled from: OSTrackerFactory.kt */
/* renamed from: cl2  reason: default package */
/* loaded from: classes2.dex */
public final class cl2 {
    public final ConcurrentHashMap<String, mj2> a;
    public final xj2 b;

    public cl2(vk2 vk2Var, yj2 yj2Var, zk2 zk2Var) {
        fs1.f(vk2Var, "preferences");
        fs1.f(yj2Var, "logger");
        fs1.f(zk2Var, "timeProvider");
        ConcurrentHashMap<String, mj2> concurrentHashMap = new ConcurrentHashMap<>();
        this.a = concurrentHashMap;
        xj2 xj2Var = new xj2(vk2Var);
        this.b = xj2Var;
        wj2 wj2Var = wj2.c;
        concurrentHashMap.put(wj2Var.a(), new vj2(xj2Var, yj2Var, zk2Var));
        concurrentHashMap.put(wj2Var.b(), new ck2(xj2Var, yj2Var, zk2Var));
    }

    public final void a(JSONObject jSONObject, List<a> list) {
        fs1.f(jSONObject, "jsonObject");
        fs1.f(list, "influences");
        for (a aVar : list) {
            if (bl2.a[aVar.c().ordinal()] == 1) {
                g().a(jSONObject, aVar);
            }
        }
    }

    public final mj2 b(OneSignal.AppEntryAction appEntryAction) {
        fs1.f(appEntryAction, "entryAction");
        if (appEntryAction.isNotificationClick()) {
            return g();
        }
        return null;
    }

    public final List<mj2> c() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(g());
        arrayList.add(e());
        return arrayList;
    }

    public final List<mj2> d(OneSignal.AppEntryAction appEntryAction) {
        fs1.f(appEntryAction, "entryAction");
        ArrayList arrayList = new ArrayList();
        if (appEntryAction.isAppClose()) {
            return arrayList;
        }
        mj2 g = appEntryAction.isAppOpen() ? g() : null;
        if (g != null) {
            arrayList.add(g);
        }
        arrayList.add(e());
        return arrayList;
    }

    public final mj2 e() {
        mj2 mj2Var = this.a.get(wj2.c.a());
        fs1.d(mj2Var);
        return mj2Var;
    }

    public final List<a> f() {
        Collection<mj2> values = this.a.values();
        fs1.e(values, "trackers.values");
        ArrayList arrayList = new ArrayList(c20.q(values, 10));
        for (mj2 mj2Var : values) {
            arrayList.add(mj2Var.e());
        }
        return arrayList;
    }

    public final mj2 g() {
        mj2 mj2Var = this.a.get(wj2.c.b());
        fs1.d(mj2Var);
        return mj2Var;
    }

    public final List<a> h() {
        Collection<mj2> values = this.a.values();
        fs1.e(values, "trackers.values");
        ArrayList<mj2> arrayList = new ArrayList();
        for (Object obj : values) {
            if (!fs1.b(((mj2) obj).h(), wj2.c.a())) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(c20.q(arrayList, 10));
        for (mj2 mj2Var : arrayList) {
            arrayList2.add(mj2Var.e());
        }
        return arrayList2;
    }

    public final void i() {
        Collection<mj2> values = this.a.values();
        fs1.e(values, "trackers.values");
        for (mj2 mj2Var : values) {
            mj2Var.p();
        }
    }

    public final void j(c1.e eVar) {
        fs1.f(eVar, "influenceParams");
        this.b.q(eVar);
    }
}
