package defpackage;

import java.util.Objects;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;

/* compiled from: ContinuationInterceptor.kt */
/* renamed from: r70  reason: default package */
/* loaded from: classes2.dex */
public interface r70 extends CoroutineContext.a {
    public static final b d = b.a;

    /* compiled from: ContinuationInterceptor.kt */
    /* renamed from: r70$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static <E extends CoroutineContext.a> E a(r70 r70Var, CoroutineContext.b<E> bVar) {
            fs1.f(bVar, "key");
            if (bVar instanceof v4) {
                v4 v4Var = (v4) bVar;
                if (v4Var.a(r70Var.getKey())) {
                    E e = (E) v4Var.b(r70Var);
                    if (e instanceof CoroutineContext.a) {
                        return e;
                    }
                    return null;
                }
                return null;
            } else if (r70.d == bVar) {
                Objects.requireNonNull(r70Var, "null cannot be cast to non-null type E");
                return r70Var;
            } else {
                return null;
            }
        }

        public static CoroutineContext b(r70 r70Var, CoroutineContext.b<?> bVar) {
            fs1.f(bVar, "key");
            if (!(bVar instanceof v4)) {
                return r70.d == bVar ? EmptyCoroutineContext.INSTANCE : r70Var;
            }
            v4 v4Var = (v4) bVar;
            return (!v4Var.a(r70Var.getKey()) || v4Var.b(r70Var) == null) ? r70Var : EmptyCoroutineContext.INSTANCE;
        }
    }

    /* compiled from: ContinuationInterceptor.kt */
    /* renamed from: r70$b */
    /* loaded from: classes2.dex */
    public static final class b implements CoroutineContext.b<r70> {
        public static final /* synthetic */ b a = new b();
    }

    void A(q70<?> q70Var);

    <T> q70<T> v(q70<? super T> q70Var);
}
