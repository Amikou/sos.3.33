package defpackage;

import com.google.android.gms.internal.measurement.a1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: rj5  reason: default package */
/* loaded from: classes.dex */
public final class rj5 extends w1<a1, rj5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public rj5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.a1 r0 = com.google.android.gms.internal.measurement.a1.C()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.rj5.<init>():void");
    }

    public final rj5 v(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        a1.D((a1) this.f0, i);
        return this;
    }

    public final rj5 x(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        a1.E((a1) this.f0, j);
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ rj5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.a1 r1 = com.google.android.gms.internal.measurement.a1.C()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.rj5.<init>(wi5):void");
    }
}
