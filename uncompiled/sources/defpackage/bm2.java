package defpackage;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.HttpException;
import com.bumptech.glide.load.data.d;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/* compiled from: OkHttpStreamFetcher.java */
/* renamed from: bm2  reason: default package */
/* loaded from: classes.dex */
public class bm2 implements d<InputStream>, Callback {
    public final Call.Factory a;
    public final ng1 f0;
    public InputStream g0;
    public ResponseBody h0;
    public d.a<? super InputStream> i0;
    public volatile Call j0;

    public bm2(Call.Factory factory, ng1 ng1Var) {
        this.a = factory;
        this.f0 = ng1Var;
    }

    @Override // com.bumptech.glide.load.data.d
    public Class<InputStream> a() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.d
    public void b() {
        try {
            InputStream inputStream = this.g0;
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException unused) {
        }
        ResponseBody responseBody = this.h0;
        if (responseBody != null) {
            responseBody.close();
        }
        this.i0 = null;
    }

    @Override // com.bumptech.glide.load.data.d
    public void cancel() {
        Call call = this.j0;
        if (call != null) {
            call.cancel();
        }
    }

    @Override // com.bumptech.glide.load.data.d
    public DataSource d() {
        return DataSource.REMOTE;
    }

    @Override // com.bumptech.glide.load.data.d
    public void e(Priority priority, d.a<? super InputStream> aVar) {
        Request.Builder url = new Request.Builder().url(this.f0.h());
        for (Map.Entry<String, String> entry : this.f0.e().entrySet()) {
            url.addHeader(entry.getKey(), entry.getValue());
        }
        Request build = url.build();
        this.i0 = aVar;
        this.j0 = this.a.newCall(build);
        this.j0.enqueue(this);
    }

    @Override // okhttp3.Callback
    public void onFailure(Call call, IOException iOException) {
        this.i0.c(iOException);
    }

    @Override // okhttp3.Callback
    public void onResponse(Call call, Response response) {
        this.h0 = response.body();
        if (response.isSuccessful()) {
            InputStream b = g70.b(this.h0.byteStream(), ((ResponseBody) wt2.d(this.h0)).contentLength());
            this.g0 = b;
            this.i0.f(b);
            return;
        }
        this.i0.c(new HttpException(response.message(), response.code()));
    }
}
