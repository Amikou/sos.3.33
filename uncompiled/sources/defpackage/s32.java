package defpackage;

import android.os.Build;
import java.util.Locale;

/* compiled from: ManufacturerUtils.java */
/* renamed from: s32  reason: default package */
/* loaded from: classes2.dex */
public class s32 {
    public static boolean a() {
        return b() || d();
    }

    public static boolean b() {
        return Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).equals("lge");
    }

    public static boolean c() {
        return Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).equals("meizu");
    }

    public static boolean d() {
        return Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).equals("samsung");
    }
}
