package defpackage;

/* compiled from: PrimitiveArrayBuilder.java */
/* renamed from: tu2  reason: default package */
/* loaded from: classes.dex */
public abstract class tu2<T> {
    public T a;
    public a<T> b;
    public a<T> c;
    public int d;

    /* compiled from: PrimitiveArrayBuilder.java */
    /* renamed from: tu2$a */
    /* loaded from: classes.dex */
    public static final class a<T> {
        public final T a;
        public final int b;
        public a<T> c;

        public a(T t, int i) {
            this.a = t;
            this.b = i;
        }

        public int a(T t, int i) {
            System.arraycopy(this.a, 0, t, i, this.b);
            return i + this.b;
        }

        public T b() {
            return this.a;
        }

        public void c(a<T> aVar) {
            if (this.c == null) {
                this.c = aVar;
                return;
            }
            throw new IllegalStateException();
        }

        public a<T> d() {
            return this.c;
        }
    }

    public abstract T a(int i);

    public void b() {
        a<T> aVar = this.c;
        if (aVar != null) {
            this.a = aVar.b();
        }
        this.c = null;
        this.b = null;
        this.d = 0;
    }

    public final T c(T t, int i) {
        a<T> aVar = new a<>(t, i);
        if (this.b == null) {
            this.c = aVar;
            this.b = aVar;
        } else {
            this.c.c(aVar);
            this.c = aVar;
        }
        this.d += i;
        return a(i < 16384 ? i + i : i + (i >> 2));
    }

    public int d() {
        return this.d;
    }

    public T e(T t, int i) {
        int i2 = this.d + i;
        T a2 = a(i2);
        int i3 = 0;
        for (a<T> aVar = this.b; aVar != null; aVar = aVar.d()) {
            i3 = aVar.a(a2, i3);
        }
        System.arraycopy(t, 0, a2, i3, i);
        int i4 = i3 + i;
        if (i4 == i2) {
            return a2;
        }
        throw new IllegalStateException("Should have gotten " + i2 + " entries, got " + i4);
    }

    public T f() {
        b();
        T t = this.a;
        return t == null ? a(12) : t;
    }
}
