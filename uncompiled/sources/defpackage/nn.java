package defpackage;

import androidx.media3.exoplayer.source.q;
import defpackage.ny;

/* compiled from: BaseMediaChunkOutput.java */
/* renamed from: nn  reason: default package */
/* loaded from: classes.dex */
public final class nn implements ny.b {
    public final int[] a;
    public final q[] b;

    public nn(int[] iArr, q[] qVarArr) {
        this.a = iArr;
        this.b = qVarArr;
    }

    public int[] a() {
        int[] iArr = new int[this.b.length];
        int i = 0;
        while (true) {
            q[] qVarArr = this.b;
            if (i >= qVarArr.length) {
                return iArr;
            }
            iArr[i] = qVarArr[i].G();
            i++;
        }
    }

    public void b(long j) {
        for (q qVar : this.b) {
            qVar.Z(j);
        }
    }

    @Override // defpackage.ny.b
    public f84 f(int i, int i2) {
        int i3 = 0;
        while (true) {
            int[] iArr = this.a;
            if (i3 < iArr.length) {
                if (i2 == iArr[i3]) {
                    return this.b[i3];
                }
                i3++;
            } else {
                p12.c("BaseMediaChunkOutput", "Unmatched track of type: " + i2);
                return new ks0();
            }
        }
    }
}
