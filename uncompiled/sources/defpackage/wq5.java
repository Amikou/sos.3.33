package defpackage;

import java.io.PrintStream;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: wq5  reason: default package */
/* loaded from: classes.dex */
public final class wq5 {
    public static final iq5 a;

    static {
        iq5 rq5Var;
        Integer num = null;
        try {
            try {
                num = (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
            } catch (Exception e) {
                System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
                e.printStackTrace(System.err);
            }
            if (num == null || num.intValue() < 19) {
                rq5Var = !Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic") ? new oq5() : new rq5();
            } else {
                rq5Var = new tq5();
            }
        } catch (Throwable th) {
            PrintStream printStream = System.err;
            String name = rq5.class.getName();
            StringBuilder sb = new StringBuilder(name.length() + 133);
            sb.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
            sb.append(name);
            sb.append("will be used. The error is: ");
            printStream.println(sb.toString());
            th.printStackTrace(System.err);
            rq5Var = new rq5();
        }
        a = rq5Var;
        if (num == null) {
            return;
        }
        num.intValue();
    }

    public static void a(Throwable th, Throwable th2) {
        a.a(th, th2);
    }
}
