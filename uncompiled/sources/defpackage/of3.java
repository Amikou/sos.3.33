package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: of3  reason: default package */
/* loaded from: classes2.dex */
public class of3 extends ct0.b {
    public static final BigInteger g = mf3.j;
    public int[] f;

    public of3() {
        this.f = kd2.j(12);
    }

    public of3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP384R1FieldElement");
        }
        this.f = nf3.e(bigInteger);
    }

    public of3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] j = kd2.j(12);
        nf3.a(this.f, ((of3) ct0Var).f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] j = kd2.j(12);
        nf3.c(this.f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] j = kd2.j(12);
        g92.d(nf3.a, ((of3) ct0Var).f, j);
        nf3.f(j, this.f, j);
        return new of3(j);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof of3) {
            return kd2.n(12, this.f, ((of3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] j = kd2.j(12);
        g92.d(nf3.a, this.f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return kd2.v(12, this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 12);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return kd2.w(12, this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] j = kd2.j(12);
        nf3.f(this.f, ((of3) ct0Var).f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] j = kd2.j(12);
        nf3.g(this.f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (kd2.w(12, iArr) || kd2.v(12, iArr)) {
            return this;
        }
        int[] j = kd2.j(12);
        int[] j2 = kd2.j(12);
        int[] j3 = kd2.j(12);
        int[] j4 = kd2.j(12);
        nf3.j(iArr, j);
        nf3.f(j, iArr, j);
        nf3.k(j, 2, j2);
        nf3.f(j2, j, j2);
        nf3.j(j2, j2);
        nf3.f(j2, iArr, j2);
        nf3.k(j2, 5, j3);
        nf3.f(j3, j2, j3);
        nf3.k(j3, 5, j4);
        nf3.f(j4, j2, j4);
        nf3.k(j4, 15, j2);
        nf3.f(j2, j4, j2);
        nf3.k(j2, 2, j3);
        nf3.f(j, j3, j);
        nf3.k(j3, 28, j3);
        nf3.f(j2, j3, j2);
        nf3.k(j2, 60, j3);
        nf3.f(j3, j2, j3);
        nf3.k(j3, 120, j2);
        nf3.f(j2, j3, j2);
        nf3.k(j2, 15, j2);
        nf3.f(j2, j4, j2);
        nf3.k(j2, 33, j2);
        nf3.f(j2, j, j2);
        nf3.k(j2, 64, j2);
        nf3.f(j2, iArr, j2);
        nf3.k(j2, 30, j);
        nf3.j(j, j2);
        if (kd2.n(12, iArr, j2)) {
            return new of3(j);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] j = kd2.j(12);
        nf3.j(this.f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] j = kd2.j(12);
        nf3.m(this.f, ((of3) ct0Var).f, j);
        return new of3(j);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return kd2.p(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return kd2.P(12, this.f);
    }
}
