package defpackage;

import com.google.protobuf.q;

/* renamed from: k95  reason: default package */
/* loaded from: classes.dex */
public final class k95 {
    public static final Class<?> a = a();

    public static Class<?> a() {
        try {
            q qVar = q.g;
            return q.class;
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    public static n95 b() {
        Class<?> cls = a;
        if (cls != null) {
            try {
                return (n95) cls.getDeclaredMethod("getEmptyRegistry", new Class[0]).invoke(null, new Object[0]);
            } catch (Exception unused) {
            }
        }
        return n95.a;
    }
}
