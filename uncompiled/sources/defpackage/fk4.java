package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ViewTilEndLayoutBinding.java */
/* renamed from: fk4  reason: default package */
/* loaded from: classes2.dex */
public final class fk4 {
    public final MaterialButton a;
    public final MaterialButton b;

    public fk4(LinearLayoutCompat linearLayoutCompat, MaterialButton materialButton, MaterialButton materialButton2, AppCompatImageView appCompatImageView) {
        this.a = materialButton;
        this.b = materialButton2;
    }

    public static fk4 a(View view) {
        int i = R.id.btnClear;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnClear);
        if (materialButton != null) {
            i = R.id.btnPaste;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPaste);
            if (materialButton2 != null) {
                i = R.id.btnQr;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnQr);
                if (appCompatImageView != null) {
                    return new fk4((LinearLayoutCompat) view, materialButton, materialButton2, appCompatImageView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
