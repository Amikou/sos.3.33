package defpackage;

import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ViewNftContactEndLayoutBinding.java */
/* renamed from: ij4  reason: default package */
/* loaded from: classes2.dex */
public final class ij4 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final AppCompatImageView c;
    public final ImageView d;

    public ij4(LinearLayoutCompat linearLayoutCompat, MaterialButton materialButton, MaterialButton materialButton2, AppCompatImageView appCompatImageView, ImageView imageView) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = appCompatImageView;
        this.d = imageView;
    }

    public static ij4 a(View view) {
        int i = R.id.btnClear;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnClear);
        if (materialButton != null) {
            i = R.id.btnPaste;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPaste);
            if (materialButton2 != null) {
                i = R.id.btnQr;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnQr);
                if (appCompatImageView != null) {
                    i = R.id.btnSelectContact;
                    ImageView imageView = (ImageView) ai4.a(view, R.id.btnSelectContact);
                    if (imageView != null) {
                        return new ij4((LinearLayoutCompat) view, materialButton, materialButton2, appCompatImageView, imageView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
