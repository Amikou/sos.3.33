package defpackage;

import androidx.media3.common.PlaybackException;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: TOTP.java */
/* renamed from: d34  reason: default package */
/* loaded from: classes2.dex */
public final class d34 {
    public static String a(long j, String str) {
        String upperCase = Long.toHexString(j).toUpperCase();
        while (upperCase.length() < 16) {
            upperCase = "0" + upperCase;
        }
        byte[] e = e(d(str), d(upperCase));
        int i = e[e.length - 1] & 15;
        String num = Integer.toString(((e[i + 3] & 255) | ((((e[i] & Byte.MAX_VALUE) << 24) | ((e[i + 1] & 255) << 16)) | ((e[i + 2] & 255) << 8))) % PlaybackException.CUSTOM_ERROR_CODE_BASE);
        while (num.length() < 6) {
            num = "0" + num;
        }
        return num;
    }

    public static String b(String str) {
        return a(c(), e30.f0(im.a(str), false));
    }

    public static long c() {
        return System.currentTimeMillis() / 30000;
    }

    public static byte[] d(String str) {
        byte[] byteArray = new BigInteger("10" + str, 16).toByteArray();
        int length = byteArray.length - 1;
        byte[] bArr = new byte[length];
        System.arraycopy(byteArray, 1, bArr, 0, length);
        return bArr;
    }

    public static byte[] e(byte[] bArr, byte[] bArr2) {
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(new SecretKeySpec(bArr, "RAW"));
            return mac.doFinal(bArr2);
        } catch (GeneralSecurityException e) {
            throw new UndeclaredThrowableException(e);
        }
    }
}
