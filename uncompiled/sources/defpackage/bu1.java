package defpackage;

import defpackage.l12;
import defpackage.st1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.CompletionHandlerException;
import kotlinx.coroutines.JobCancellationException;
import kotlinx.coroutines.TimeoutCancellationException;

/* compiled from: JobSupport.kt */
/* renamed from: bu1  reason: default package */
/* loaded from: classes2.dex */
public class bu1 implements st1, hy, mp2 {
    public static final /* synthetic */ AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(bu1.class, Object.class, "_state");
    private volatile /* synthetic */ Object _parentHandle;
    private volatile /* synthetic */ Object _state;

    /* compiled from: JobSupport.kt */
    /* renamed from: bu1$a */
    /* loaded from: classes2.dex */
    public static final class a<T> extends pv<T> {
        public final bu1 m0;

        public a(q70<? super T> q70Var, bu1 bu1Var) {
            super(q70Var, 1);
            this.m0 = bu1Var;
        }

        @Override // defpackage.pv
        public String G() {
            return "AwaitContinuation";
        }

        @Override // defpackage.pv
        public Throwable w(st1 st1Var) {
            Throwable f;
            Object a0 = this.m0.a0();
            return (!(a0 instanceof c) || (f = ((c) a0).f()) == null) ? a0 instanceof t30 ? ((t30) a0).a : st1Var.g() : f;
        }
    }

    /* compiled from: JobSupport.kt */
    /* renamed from: bu1$b */
    /* loaded from: classes2.dex */
    public static final class b extends au1 {
        public final bu1 i0;
        public final c j0;
        public final gy k0;
        public final Object l0;

        public b(bu1 bu1Var, c cVar, gy gyVar, Object obj) {
            this.i0 = bu1Var;
            this.j0 = cVar;
            this.k0 = gyVar;
            this.l0 = obj;
        }

        @Override // defpackage.tc1
        public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
            y(th);
            return te4.a;
        }

        @Override // defpackage.v30
        public void y(Throwable th) {
            this.i0.Q(this.j0, this.k0, this.l0);
        }
    }

    /* compiled from: JobSupport.kt */
    /* renamed from: bu1$c */
    /* loaded from: classes2.dex */
    public static final class c implements dq1 {
        private volatile /* synthetic */ Object _exceptionsHolder = null;
        private volatile /* synthetic */ int _isCompleting;
        private volatile /* synthetic */ Object _rootCause;
        public final tg2 a;

        public c(tg2 tg2Var, boolean z, Throwable th) {
            this.a = tg2Var;
            this._isCompleting = z ? 1 : 0;
            this._rootCause = th;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public final void a(Throwable th) {
            Throwable f = f();
            if (f == null) {
                m(th);
            } else if (th == f) {
            } else {
                Object d = d();
                if (d == null) {
                    l(th);
                } else if (!(d instanceof Throwable)) {
                    if (!(d instanceof ArrayList)) {
                        throw new IllegalStateException(fs1.l("State is ", d).toString());
                    }
                    ((ArrayList) d).add(th);
                } else if (th == d) {
                } else {
                    ArrayList<Throwable> c = c();
                    c.add(d);
                    c.add(th);
                    te4 te4Var = te4.a;
                    l(c);
                }
            }
        }

        @Override // defpackage.dq1
        public boolean b() {
            return f() == null;
        }

        public final ArrayList<Throwable> c() {
            return new ArrayList<>(4);
        }

        public final Object d() {
            return this._exceptionsHolder;
        }

        @Override // defpackage.dq1
        public tg2 e() {
            return this.a;
        }

        public final Throwable f() {
            return (Throwable) this._rootCause;
        }

        public final boolean g() {
            return f() != null;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
        public final boolean h() {
            return this._isCompleting;
        }

        public final boolean i() {
            k24 k24Var;
            Object d = d();
            k24Var = cu1.e;
            return d == k24Var;
        }

        public final List<Throwable> j(Throwable th) {
            ArrayList<Throwable> arrayList;
            k24 k24Var;
            Object d = d();
            if (d == null) {
                arrayList = c();
            } else if (d instanceof Throwable) {
                ArrayList<Throwable> c = c();
                c.add(d);
                arrayList = c;
            } else if (!(d instanceof ArrayList)) {
                throw new IllegalStateException(fs1.l("State is ", d).toString());
            } else {
                arrayList = (ArrayList) d;
            }
            Throwable f = f();
            if (f != null) {
                arrayList.add(0, f);
            }
            if (th != null && !fs1.b(th, f)) {
                arrayList.add(th);
            }
            k24Var = cu1.e;
            l(k24Var);
            return arrayList;
        }

        public final void k(boolean z) {
            this._isCompleting = z ? 1 : 0;
        }

        public final void l(Object obj) {
            this._exceptionsHolder = obj;
        }

        public final void m(Throwable th) {
            this._rootCause = th;
        }

        public String toString() {
            return "Finishing[cancelling=" + g() + ", completing=" + h() + ", rootCause=" + f() + ", exceptions=" + d() + ", list=" + e() + ']';
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* renamed from: bu1$d */
    /* loaded from: classes2.dex */
    public static final class d extends l12.a {
        public final /* synthetic */ bu1 d;
        public final /* synthetic */ Object e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(l12 l12Var, bu1 bu1Var, Object obj) {
            super(l12Var);
            this.d = bu1Var;
            this.e = obj;
        }

        @Override // defpackage.cj
        /* renamed from: i */
        public Object g(l12 l12Var) {
            if (this.d.a0() == this.e) {
                return null;
            }
            return k12.a();
        }
    }

    public bu1(boolean z) {
        this._state = z ? cu1.g : cu1.f;
        this._parentHandle = null;
    }

    public static /* synthetic */ CancellationException A0(bu1 bu1Var, Throwable th, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                str = null;
            }
            return bu1Var.z0(th, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toCancellationException");
    }

    public final boolean B(Object obj, tg2 tg2Var, au1 au1Var) {
        int x;
        d dVar = new d(au1Var, this, obj);
        do {
            x = tg2Var.p().x(au1Var, tg2Var, dVar);
            if (x == 1) {
                return true;
            }
        } while (x != 2);
        return false;
    }

    public final String B0() {
        return m0() + '{' + y0(a0()) + '}';
    }

    public final boolean C0(dq1 dq1Var, Object obj) {
        if (ze0.a()) {
            if (!((dq1Var instanceof ou0) || (dq1Var instanceof au1))) {
                throw new AssertionError();
            }
        }
        if (!ze0.a() || (!(obj instanceof t30))) {
            if (a.compareAndSet(this, dq1Var, cu1.g(obj))) {
                q0(null);
                r0(obj);
                P(dq1Var, obj);
                return true;
            }
            return false;
        }
        throw new AssertionError();
    }

    public final void D(Throwable th, List<? extends Throwable> list) {
        if (list.size() <= 1) {
            return;
        }
        Set newSetFromMap = Collections.newSetFromMap(new IdentityHashMap(list.size()));
        Throwable m = !ze0.d() ? th : hs3.m(th);
        for (Throwable th2 : list) {
            if (ze0.d()) {
                th2 = hs3.m(th2);
            }
            if (th2 != th && th2 != m && !(th2 instanceof CancellationException) && newSetFromMap.add(th2)) {
                py0.a(th, th2);
            }
        }
    }

    public final boolean D0(dq1 dq1Var, Throwable th) {
        if (!ze0.a() || (!(dq1Var instanceof c))) {
            if (!ze0.a() || dq1Var.b()) {
                tg2 Y = Y(dq1Var);
                if (Y == null) {
                    return false;
                }
                if (a.compareAndSet(this, dq1Var, new c(Y, false, th))) {
                    o0(Y, th);
                    return true;
                }
                return false;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public void E(Object obj) {
    }

    public final Object E0(Object obj, Object obj2) {
        k24 k24Var;
        k24 k24Var2;
        if (!(obj instanceof dq1)) {
            k24Var2 = cu1.a;
            return k24Var2;
        } else if (((obj instanceof ou0) || (obj instanceof au1)) && !(obj instanceof gy) && !(obj2 instanceof t30)) {
            if (C0((dq1) obj, obj2)) {
                return obj2;
            }
            k24Var = cu1.c;
            return k24Var;
        } else {
            return F0((dq1) obj, obj2);
        }
    }

    @Override // defpackage.st1
    public final Object F(q70<? super te4> q70Var) {
        if (!g0()) {
            xt1.j(q70Var.getContext());
            return te4.a;
        }
        Object h0 = h0(q70Var);
        return h0 == gs1.d() ? h0 : te4.a;
    }

    public final Object F0(dq1 dq1Var, Object obj) {
        k24 k24Var;
        k24 k24Var2;
        k24 k24Var3;
        tg2 Y = Y(dq1Var);
        if (Y == null) {
            k24Var3 = cu1.c;
            return k24Var3;
        }
        c cVar = dq1Var instanceof c ? (c) dq1Var : null;
        if (cVar == null) {
            cVar = new c(Y, false, null);
        }
        synchronized (cVar) {
            if (cVar.h()) {
                k24Var2 = cu1.a;
                return k24Var2;
            }
            cVar.k(true);
            if (cVar != dq1Var && !a.compareAndSet(this, dq1Var, cVar)) {
                k24Var = cu1.c;
                return k24Var;
            }
            if (ze0.a() && !(!cVar.i())) {
                throw new AssertionError();
            }
            boolean g = cVar.g();
            t30 t30Var = obj instanceof t30 ? (t30) obj : null;
            if (t30Var != null) {
                cVar.a(t30Var.a);
            }
            Throwable f = true ^ g ? cVar.f() : null;
            te4 te4Var = te4.a;
            if (f != null) {
                o0(Y, f);
            }
            gy T = T(dq1Var);
            if (T != null && G0(cVar, T, obj)) {
                return cu1.b;
            }
            return S(cVar, obj);
        }
    }

    public final Object G(q70<Object> q70Var) {
        Object a0;
        Throwable j;
        do {
            a0 = a0();
            if (!(a0 instanceof dq1)) {
                if (a0 instanceof t30) {
                    Throwable th = ((t30) a0).a;
                    if (ze0.d()) {
                        if (q70Var instanceof e90) {
                            j = hs3.j(th, (e90) q70Var);
                            throw j;
                        }
                        throw th;
                    }
                    throw th;
                }
                return cu1.h(a0);
            }
        } while (x0(a0) < 0);
        return H(q70Var);
    }

    public final boolean G0(c cVar, gy gyVar, Object obj) {
        while (st1.a.d(gyVar.i0, false, false, new b(this, cVar, gyVar, obj), 1, null) == vg2.a) {
            gyVar = n0(gyVar);
            if (gyVar == null) {
                return false;
            }
        }
        return true;
    }

    public final Object H(q70<Object> q70Var) {
        a aVar = new a(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), this);
        aVar.A();
        rv.a(aVar, z(new r83(aVar)));
        Object x = aVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x;
    }

    public final boolean I(Throwable th) {
        return J(th);
    }

    public final boolean J(Object obj) {
        Object obj2;
        k24 k24Var;
        k24 k24Var2;
        k24 k24Var3;
        obj2 = cu1.a;
        if (X() && (obj2 = L(obj)) == cu1.b) {
            return true;
        }
        k24Var = cu1.a;
        if (obj2 == k24Var) {
            obj2 = i0(obj);
        }
        k24Var2 = cu1.a;
        if (obj2 == k24Var2 || obj2 == cu1.b) {
            return true;
        }
        k24Var3 = cu1.d;
        if (obj2 == k24Var3) {
            return false;
        }
        E(obj2);
        return true;
    }

    public void K(Throwable th) {
        J(th);
    }

    public final Object L(Object obj) {
        k24 k24Var;
        Object E0;
        k24 k24Var2;
        do {
            Object a0 = a0();
            if (!(a0 instanceof dq1) || ((a0 instanceof c) && ((c) a0).h())) {
                k24Var = cu1.a;
                return k24Var;
            }
            E0 = E0(a0, new t30(R(obj), false, 2, null));
            k24Var2 = cu1.c;
        } while (E0 == k24Var2);
        return E0;
    }

    public final boolean M(Throwable th) {
        if (f0()) {
            return true;
        }
        boolean z = th instanceof CancellationException;
        fy Z = Z();
        return (Z == null || Z == vg2.a) ? z : Z.d(th) || z;
    }

    public String N() {
        return "Job was cancelled";
    }

    public boolean O(Throwable th) {
        if (th instanceof CancellationException) {
            return true;
        }
        return J(th) && W();
    }

    public final void P(dq1 dq1Var, Object obj) {
        fy Z = Z();
        if (Z != null) {
            Z.a();
            w0(vg2.a);
        }
        t30 t30Var = obj instanceof t30 ? (t30) obj : null;
        Throwable th = t30Var != null ? t30Var.a : null;
        if (dq1Var instanceof au1) {
            try {
                ((au1) dq1Var).y(th);
                return;
            } catch (Throwable th2) {
                c0(new CompletionHandlerException("Exception in completion handler " + dq1Var + " for " + this, th2));
                return;
            }
        }
        tg2 e = dq1Var.e();
        if (e == null) {
            return;
        }
        p0(e, th);
    }

    public final void Q(c cVar, gy gyVar, Object obj) {
        if (ze0.a()) {
            if (!(a0() == cVar)) {
                throw new AssertionError();
            }
        }
        gy n0 = n0(gyVar);
        if (n0 == null || !G0(cVar, n0, obj)) {
            E(S(cVar, obj));
        }
    }

    public final Throwable R(Object obj) {
        if (obj == null ? true : obj instanceof Throwable) {
            Throwable th = (Throwable) obj;
            return th == null ? new JobCancellationException(N(), null, this) : th;
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
        return ((mp2) obj).w();
    }

    public final Object S(c cVar, Object obj) {
        boolean g;
        Throwable V;
        boolean z = true;
        if (ze0.a()) {
            if (!(a0() == cVar)) {
                throw new AssertionError();
            }
        }
        if (!ze0.a() || (!cVar.i())) {
            if (!ze0.a() || cVar.h()) {
                t30 t30Var = obj instanceof t30 ? (t30) obj : null;
                Throwable th = t30Var == null ? null : t30Var.a;
                synchronized (cVar) {
                    g = cVar.g();
                    List<Throwable> j = cVar.j(th);
                    V = V(cVar, j);
                    if (V != null) {
                        D(V, j);
                    }
                }
                if (V != null && V != th) {
                    obj = new t30(V, false, 2, null);
                }
                if (V != null) {
                    if (!M(V) && !b0(V)) {
                        z = false;
                    }
                    if (z) {
                        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                        ((t30) obj).b();
                    }
                }
                if (!g) {
                    q0(V);
                }
                r0(obj);
                boolean compareAndSet = a.compareAndSet(this, cVar, cu1.g(obj));
                if (!ze0.a() || compareAndSet) {
                    P(cVar, obj);
                    return obj;
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public final gy T(dq1 dq1Var) {
        gy gyVar = dq1Var instanceof gy ? (gy) dq1Var : null;
        if (gyVar == null) {
            tg2 e = dq1Var.e();
            if (e == null) {
                return null;
            }
            return n0(e);
        }
        return gyVar;
    }

    public final Throwable U(Object obj) {
        t30 t30Var = obj instanceof t30 ? (t30) obj : null;
        if (t30Var == null) {
            return null;
        }
        return t30Var.a;
    }

    public final Throwable V(c cVar, List<? extends Throwable> list) {
        Object obj;
        boolean z;
        Object obj2 = null;
        if (list.isEmpty()) {
            if (cVar.g()) {
                return new JobCancellationException(N(), null, this);
            }
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (!(((Throwable) obj) instanceof CancellationException)) {
                break;
            }
        }
        Throwable th = (Throwable) obj;
        if (th != null) {
            return th;
        }
        Throwable th2 = list.get(0);
        if (th2 instanceof TimeoutCancellationException) {
            Iterator<T> it2 = list.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                Throwable th3 = (Throwable) next;
                if (th3 == th2 || !(th3 instanceof TimeoutCancellationException)) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    obj2 = next;
                    break;
                }
            }
            Throwable th4 = (Throwable) obj2;
            if (th4 != null) {
                return th4;
            }
        }
        return th2;
    }

    public boolean W() {
        return true;
    }

    public boolean X() {
        return false;
    }

    public final tg2 Y(dq1 dq1Var) {
        tg2 e = dq1Var.e();
        if (e == null) {
            if (dq1Var instanceof ou0) {
                return new tg2();
            }
            if (dq1Var instanceof au1) {
                u0((au1) dq1Var);
                return null;
            }
            throw new IllegalStateException(fs1.l("State should have list: ", dq1Var).toString());
        }
        return e;
    }

    public final fy Z() {
        return (fy) this._parentHandle;
    }

    @Override // defpackage.st1
    public void a(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new JobCancellationException(N(), null, this);
        }
        K(cancellationException);
    }

    public final Object a0() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof fn2)) {
                return obj;
            }
            ((fn2) obj).c(this);
        }
    }

    @Override // defpackage.st1
    public boolean b() {
        Object a0 = a0();
        return (a0 instanceof dq1) && ((dq1) a0).b();
    }

    public boolean b0(Throwable th) {
        return false;
    }

    public void c0(Throwable th) {
        throw th;
    }

    @Override // defpackage.st1
    public final yp0 d(boolean z, boolean z2, tc1<? super Throwable, te4> tc1Var) {
        au1 l0 = l0(tc1Var, z);
        while (true) {
            Object a0 = a0();
            if (a0 instanceof ou0) {
                ou0 ou0Var = (ou0) a0;
                if (ou0Var.b()) {
                    if (a.compareAndSet(this, a0, l0)) {
                        return l0;
                    }
                } else {
                    t0(ou0Var);
                }
            } else {
                if (a0 instanceof dq1) {
                    tg2 e = ((dq1) a0).e();
                    if (e == null) {
                        Objects.requireNonNull(a0, "null cannot be cast to non-null type kotlinx.coroutines.JobNode");
                        u0((au1) a0);
                    } else {
                        yp0 yp0Var = vg2.a;
                        if (z && (a0 instanceof c)) {
                            synchronized (a0) {
                                r3 = ((c) a0).f();
                                if (r3 == null || ((tc1Var instanceof gy) && !((c) a0).h())) {
                                    if (B(a0, e, l0)) {
                                        if (r3 == null) {
                                            return l0;
                                        }
                                        yp0Var = l0;
                                    }
                                }
                                te4 te4Var = te4.a;
                            }
                        }
                        if (r3 != null) {
                            if (z2) {
                                tc1Var.invoke(r3);
                            }
                            return yp0Var;
                        } else if (B(a0, e, l0)) {
                            return l0;
                        }
                    }
                } else {
                    if (z2) {
                        t30 t30Var = a0 instanceof t30 ? (t30) a0 : null;
                        tc1Var.invoke(t30Var != null ? t30Var.a : null);
                    }
                    return vg2.a;
                }
            }
        }
    }

    public final void d0(st1 st1Var) {
        if (ze0.a()) {
            if (!(Z() == null)) {
                throw new AssertionError();
            }
        }
        if (st1Var == null) {
            w0(vg2.a);
            return;
        }
        st1Var.start();
        fy x = st1Var.x(this);
        w0(x);
        if (e0()) {
            x.a();
            w0(vg2.a);
        }
    }

    public final boolean e0() {
        return !(a0() instanceof dq1);
    }

    public boolean f0() {
        return false;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public <R> R fold(R r, hd1<? super R, ? super CoroutineContext.a, ? extends R> hd1Var) {
        return (R) st1.a.b(this, r, hd1Var);
    }

    @Override // defpackage.st1
    public final CancellationException g() {
        Object a0 = a0();
        if (!(a0 instanceof c)) {
            if (a0 instanceof dq1) {
                throw new IllegalStateException(fs1.l("Job is still new or active: ", this).toString());
            }
            return a0 instanceof t30 ? A0(this, ((t30) a0).a, null, 1, null) : new JobCancellationException(fs1.l(ff0.a(this), " has completed normally"), null, this);
        }
        Throwable f = ((c) a0).f();
        if (f != null) {
            return z0(f, fs1.l(ff0.a(this), " is cancelling"));
        }
        throw new IllegalStateException(fs1.l("Job is still new or active: ", this).toString());
    }

    public final boolean g0() {
        Object a0;
        do {
            a0 = a0();
            if (!(a0 instanceof dq1)) {
                return false;
            }
        } while (x0(a0) < 0);
        return true;
    }

    @Override // kotlin.coroutines.CoroutineContext.a, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        return (E) st1.a.c(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext.a
    public final CoroutineContext.b<?> getKey() {
        return st1.f;
    }

    public final Object h0(q70<? super te4> q70Var) {
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.A();
        rv.a(pvVar, z(new s83(pvVar)));
        Object x = pvVar.x();
        if (x == gs1.d()) {
            ef0.c(q70Var);
        }
        return x == gs1.d() ? x : te4.a;
    }

    public final Object i0(Object obj) {
        k24 k24Var;
        k24 k24Var2;
        k24 k24Var3;
        k24 k24Var4;
        k24 k24Var5;
        k24 k24Var6;
        Throwable th = null;
        while (true) {
            Object a0 = a0();
            if (a0 instanceof c) {
                synchronized (a0) {
                    if (((c) a0).i()) {
                        k24Var2 = cu1.d;
                        return k24Var2;
                    }
                    boolean g = ((c) a0).g();
                    if (obj != null || !g) {
                        if (th == null) {
                            th = R(obj);
                        }
                        ((c) a0).a(th);
                    }
                    Throwable f = g ^ true ? ((c) a0).f() : null;
                    if (f != null) {
                        o0(((c) a0).e(), f);
                    }
                    k24Var = cu1.a;
                    return k24Var;
                }
            } else if (!(a0 instanceof dq1)) {
                k24Var3 = cu1.d;
                return k24Var3;
            } else {
                if (th == null) {
                    th = R(obj);
                }
                dq1 dq1Var = (dq1) a0;
                if (dq1Var.b()) {
                    if (D0(dq1Var, th)) {
                        k24Var4 = cu1.a;
                        return k24Var4;
                    }
                } else {
                    Object E0 = E0(a0, new t30(th, false, 2, null));
                    k24Var5 = cu1.a;
                    if (E0 != k24Var5) {
                        k24Var6 = cu1.c;
                        if (E0 != k24Var6) {
                            return E0;
                        }
                    } else {
                        throw new IllegalStateException(fs1.l("Cannot happen in ", a0).toString());
                    }
                }
            }
        }
    }

    @Override // defpackage.st1
    public final boolean isCancelled() {
        Object a0 = a0();
        return (a0 instanceof t30) || ((a0 instanceof c) && ((c) a0).g());
    }

    public final boolean j0(Object obj) {
        Object E0;
        k24 k24Var;
        k24 k24Var2;
        do {
            E0 = E0(a0(), obj);
            k24Var = cu1.a;
            if (E0 == k24Var) {
                return false;
            }
            if (E0 == cu1.b) {
                return true;
            }
            k24Var2 = cu1.c;
        } while (E0 == k24Var2);
        E(E0);
        return true;
    }

    public final Object k0(Object obj) {
        Object E0;
        k24 k24Var;
        k24 k24Var2;
        do {
            E0 = E0(a0(), obj);
            k24Var = cu1.a;
            if (E0 != k24Var) {
                k24Var2 = cu1.c;
            } else {
                throw new IllegalStateException("Job " + this + " is already complete or completing, but is being completed with " + obj, U(obj));
            }
        } while (E0 == k24Var2);
        return E0;
    }

    public final au1 l0(tc1<? super Throwable, te4> tc1Var, boolean z) {
        if (z) {
            r0 = tc1Var instanceof tt1 ? (tt1) tc1Var : null;
            if (r0 == null) {
                r0 = new ns1(tc1Var);
            }
        } else {
            au1 au1Var = tc1Var instanceof au1 ? (au1) tc1Var : null;
            if (au1Var != null) {
                if (ze0.a() && !(!(au1Var instanceof tt1))) {
                    throw new AssertionError();
                }
                r0 = au1Var;
            }
            if (r0 == null) {
                r0 = new os1(tc1Var);
            }
        }
        r0.A(this);
        return r0;
    }

    public String m0() {
        return ff0.a(this);
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        return st1.a.e(this, bVar);
    }

    @Override // defpackage.hy
    public final void n(mp2 mp2Var) {
        J(mp2Var);
    }

    public final gy n0(l12 l12Var) {
        while (l12Var.s()) {
            l12Var = l12Var.p();
        }
        while (true) {
            l12Var = l12Var.o();
            if (!l12Var.s()) {
                if (l12Var instanceof gy) {
                    return (gy) l12Var;
                }
                if (l12Var instanceof tg2) {
                    return null;
                }
            }
        }
    }

    public final void o0(tg2 tg2Var, Throwable th) {
        CompletionHandlerException completionHandlerException;
        q0(th);
        CompletionHandlerException completionHandlerException2 = null;
        for (l12 l12Var = (l12) tg2Var.n(); !fs1.b(l12Var, tg2Var); l12Var = l12Var.o()) {
            if (l12Var instanceof tt1) {
                au1 au1Var = (au1) l12Var;
                try {
                    au1Var.y(th);
                } catch (Throwable th2) {
                    if (completionHandlerException2 == null) {
                        completionHandlerException = null;
                    } else {
                        py0.a(completionHandlerException2, th2);
                        completionHandlerException = completionHandlerException2;
                    }
                    if (completionHandlerException == null) {
                        completionHandlerException2 = new CompletionHandlerException("Exception in completion handler " + au1Var + " for " + this, th2);
                    }
                }
            }
        }
        if (completionHandlerException2 != null) {
            c0(completionHandlerException2);
        }
        M(th);
    }

    public final void p0(tg2 tg2Var, Throwable th) {
        CompletionHandlerException completionHandlerException;
        CompletionHandlerException completionHandlerException2 = null;
        for (l12 l12Var = (l12) tg2Var.n(); !fs1.b(l12Var, tg2Var); l12Var = l12Var.o()) {
            if (l12Var instanceof au1) {
                au1 au1Var = (au1) l12Var;
                try {
                    au1Var.y(th);
                } catch (Throwable th2) {
                    if (completionHandlerException2 == null) {
                        completionHandlerException = null;
                    } else {
                        py0.a(completionHandlerException2, th2);
                        completionHandlerException = completionHandlerException2;
                    }
                    if (completionHandlerException == null) {
                        completionHandlerException2 = new CompletionHandlerException("Exception in completion handler " + au1Var + " for " + this, th2);
                    }
                }
            }
        }
        if (completionHandlerException2 == null) {
            return;
        }
        c0(completionHandlerException2);
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        return st1.a.f(this, coroutineContext);
    }

    public void q0(Throwable th) {
    }

    public void r0(Object obj) {
    }

    public void s0() {
    }

    @Override // defpackage.st1
    public final boolean start() {
        int x0;
        do {
            x0 = x0(a0());
            if (x0 == 0) {
                return false;
            }
        } while (x0 != 1);
        return true;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v2, types: [wp1] */
    public final void t0(ou0 ou0Var) {
        tg2 tg2Var = new tg2();
        if (!ou0Var.b()) {
            tg2Var = new wp1(tg2Var);
        }
        a.compareAndSet(this, ou0Var, tg2Var);
    }

    public String toString() {
        return B0() + '@' + ff0.b(this);
    }

    public final void u0(au1 au1Var) {
        au1Var.j(new tg2());
        a.compareAndSet(this, au1Var, au1Var.o());
    }

    public final void v0(au1 au1Var) {
        Object a0;
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater;
        ou0 ou0Var;
        do {
            a0 = a0();
            if (!(a0 instanceof au1)) {
                if (!(a0 instanceof dq1) || ((dq1) a0).e() == null) {
                    return;
                }
                au1Var.t();
                return;
            } else if (a0 != au1Var) {
                return;
            } else {
                atomicReferenceFieldUpdater = a;
                ou0Var = cu1.g;
            }
        } while (!atomicReferenceFieldUpdater.compareAndSet(this, a0, ou0Var));
    }

    @Override // defpackage.mp2
    public CancellationException w() {
        Throwable th;
        Object a0 = a0();
        if (a0 instanceof c) {
            th = ((c) a0).f();
        } else if (a0 instanceof t30) {
            th = ((t30) a0).a;
        } else if (a0 instanceof dq1) {
            throw new IllegalStateException(fs1.l("Cannot be cancelling child in this state: ", a0).toString());
        } else {
            th = null;
        }
        CancellationException cancellationException = th instanceof CancellationException ? th : null;
        return cancellationException == null ? new JobCancellationException(fs1.l("Parent job is ", y0(a0)), th, this) : cancellationException;
    }

    public final void w0(fy fyVar) {
        this._parentHandle = fyVar;
    }

    @Override // defpackage.st1
    public final fy x(hy hyVar) {
        return (fy) st1.a.d(this, true, false, new gy(hyVar), 2, null);
    }

    public final int x0(Object obj) {
        ou0 ou0Var;
        if (obj instanceof ou0) {
            if (((ou0) obj).b()) {
                return 0;
            }
            AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
            ou0Var = cu1.g;
            if (atomicReferenceFieldUpdater.compareAndSet(this, obj, ou0Var)) {
                s0();
                return 1;
            }
            return -1;
        } else if (obj instanceof wp1) {
            if (a.compareAndSet(this, obj, ((wp1) obj).e())) {
                s0();
                return 1;
            }
            return -1;
        } else {
            return 0;
        }
    }

    public final String y0(Object obj) {
        if (!(obj instanceof c)) {
            return obj instanceof dq1 ? ((dq1) obj).b() ? "Active" : "New" : obj instanceof t30 ? "Cancelled" : "Completed";
        }
        c cVar = (c) obj;
        return cVar.g() ? "Cancelling" : cVar.h() ? "Completing" : "Active";
    }

    @Override // defpackage.st1
    public final yp0 z(tc1<? super Throwable, te4> tc1Var) {
        return d(false, true, tc1Var);
    }

    public final CancellationException z0(Throwable th, String str) {
        CancellationException cancellationException = th instanceof CancellationException ? (CancellationException) th : null;
        if (cancellationException == null) {
            if (str == null) {
                str = N();
            }
            cancellationException = new JobCancellationException(str, th, this);
        }
        return cancellationException;
    }
}
