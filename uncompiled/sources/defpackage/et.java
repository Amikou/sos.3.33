package defpackage;

import androidx.media3.common.j;
import androidx.media3.decoder.DecoderInputBuffer;
import java.nio.ByteBuffer;

/* compiled from: C2Mp3TimestampTracker.java */
/* renamed from: et  reason: default package */
/* loaded from: classes.dex */
public final class et {
    public long a;
    public long b;
    public boolean c;

    public final long a(long j) {
        return this.a + Math.max(0L, ((this.b - 529) * 1000000) / j);
    }

    public long b(j jVar) {
        return a(jVar.D0);
    }

    public void c() {
        this.a = 0L;
        this.b = 0L;
        this.c = false;
    }

    public long d(j jVar, DecoderInputBuffer decoderInputBuffer) {
        if (this.b == 0) {
            this.a = decoderInputBuffer.i0;
        }
        if (this.c) {
            return decoderInputBuffer.i0;
        }
        ByteBuffer byteBuffer = (ByteBuffer) ii.e(decoderInputBuffer.g0);
        int i = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            i = (i << 8) | (byteBuffer.get(i2) & 255);
        }
        int m = la2.m(i);
        if (m == -1) {
            this.c = true;
            this.b = 0L;
            this.a = decoderInputBuffer.i0;
            p12.i("C2Mp3TimestampTracker", "MPEG audio header is invalid.");
            return decoderInputBuffer.i0;
        }
        long a = a(jVar.D0);
        this.b += m;
        return a;
    }
}
