package defpackage;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.o;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.collectible.TYPE_DELETE_NFT;
import net.safemoon.androidwallet.views.CustomVideoPlayer;

/* compiled from: CollectionsAdapter.kt */
/* renamed from: r10  reason: default package */
/* loaded from: classes2.dex */
public final class r10 extends o<RoomCollectionAndNft, a> {
    public final tc1<RoomCollectionAndNft, te4> a;
    public final hd1<RoomCollectionAndNft, TYPE_DELETE_NFT, te4> b;

    /* compiled from: CollectionsAdapter.kt */
    /* renamed from: r10$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final ws1 a;
        public final /* synthetic */ r10 b;

        /* compiled from: CollectionsAdapter.kt */
        /* renamed from: r10$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C0261a extends en<ao1> {
            public final /* synthetic */ ws1 b;
            public final /* synthetic */ String c;

            /* compiled from: CollectionsAdapter.kt */
            /* renamed from: r10$a$a$a  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C0262a implements j73<Drawable> {
                public final /* synthetic */ ws1 a;
                public final /* synthetic */ String f0;

                public C0262a(ws1 ws1Var, String str) {
                    this.a = ws1Var;
                    this.f0 = str;
                }

                @Override // defpackage.j73
                /* renamed from: a */
                public boolean n(Drawable drawable, Object obj, i34<Drawable> i34Var, DataSource dataSource, boolean z) {
                    return false;
                }

                @Override // defpackage.j73
                public boolean i(GlideException glideException, Object obj, i34<Drawable> i34Var, boolean z) {
                    CustomVideoPlayer customVideoPlayer = this.a.g;
                    fs1.e(customVideoPlayer, "videoPlayer");
                    customVideoPlayer.setVisibility(0);
                    this.a.g.setPlayURL(this.f0);
                    return false;
                }
            }

            public C0261a(ws1 ws1Var, String str) {
                this.b = ws1Var;
                this.c = str;
            }

            @Override // defpackage.en, defpackage.m80
            public void c(String str, Throwable th) {
                super.c(str, th);
                ImageView imageView = this.b.d;
                fs1.e(imageView, "nftIcon");
                imageView.setVisibility(0);
                SimpleDraweeView simpleDraweeView = this.b.f;
                fs1.e(simpleDraweeView, "sdvImage");
                simpleDraweeView.setVisibility(4);
                k73 u = com.bumptech.glide.a.u(this.b.d);
                String str2 = this.c;
                Objects.requireNonNull(str2, "null cannot be cast to non-null type kotlin.CharSequence");
                u.y(StringsKt__StringsKt.K0(str2).toString()).l(R.drawable.safemoon).d().u0(new C0262a(this.b, this.c)).I0(this.b.d);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(r10 r10Var, ws1 ws1Var) {
            super(ws1Var.b());
            fs1.f(r10Var, "this$0");
            fs1.f(ws1Var, "itemCollectible");
            this.b = r10Var;
            this.a = ws1Var;
        }

        public static final void f(r10 r10Var, RoomCollectionAndNft roomCollectionAndNft, View view) {
            fs1.f(r10Var, "this$0");
            fs1.f(roomCollectionAndNft, "$item");
            r10Var.b().invoke(roomCollectionAndNft, TYPE_DELETE_NFT.VISIBLE);
        }

        public static final void g(r10 r10Var, RoomCollectionAndNft roomCollectionAndNft, View view) {
            fs1.f(r10Var, "this$0");
            fs1.f(roomCollectionAndNft, "$item");
            r10Var.b().invoke(roomCollectionAndNft, TYPE_DELETE_NFT.DELETE_FOREVER);
        }

        public static final void h(r10 r10Var, RoomCollectionAndNft roomCollectionAndNft, View view) {
            fs1.f(r10Var, "this$0");
            fs1.f(roomCollectionAndNft, "$item");
            r10Var.b().invoke(roomCollectionAndNft, TYPE_DELETE_NFT.HIDE);
        }

        public static final void i(r10 r10Var, RoomCollectionAndNft roomCollectionAndNft, View view) {
            fs1.f(r10Var, "this$0");
            fs1.f(roomCollectionAndNft, "$item");
            r10Var.a().invoke(roomCollectionAndNft);
        }

        /* JADX WARN: Removed duplicated region for block: B:29:0x005d  */
        /* JADX WARN: Removed duplicated region for block: B:30:0x00a6  */
        /* JADX WARN: Removed duplicated region for block: B:36:0x0105  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x0132  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final void e(final net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft r12) {
            /*
                Method dump skipped, instructions count: 365
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.r10.a.e(net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft):void");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public r10(defpackage.tc1<? super net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft, defpackage.te4> r2, defpackage.hd1<? super net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft, ? super net.safemoon.androidwallet.model.collectible.TYPE_DELETE_NFT, defpackage.te4> r3) {
        /*
            r1 = this;
            java.lang.String r0 = "callBack"
            defpackage.fs1.f(r2, r0)
            java.lang.String r0 = "callBackCollection"
            defpackage.fs1.f(r3, r0)
            s10$a r0 = defpackage.s10.a()
            r1.<init>(r0)
            r1.a = r2
            r1.b = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.r10.<init>(tc1, hd1):void");
    }

    public final tc1<RoomCollectionAndNft, te4> a() {
        return this.a;
    }

    public final hd1<RoomCollectionAndNft, TYPE_DELETE_NFT, te4> b() {
        return this.b;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        RoomCollectionAndNft item = getItem(i);
        fs1.e(item, "getItem(position)");
        aVar.e(item);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        ws1 c = ws1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        fs1.e(c, "inflate(LayoutInflater.f….context), parent, false)");
        return new a(this, c);
    }

    public final void e(int i, int i2) {
        List<RoomCollectionAndNft> currentList = getCurrentList();
        fs1.e(currentList, "currentList");
        List m0 = j20.m0(currentList);
        Collections.swap(m0, i, i2);
        submitList(m0);
    }
}
