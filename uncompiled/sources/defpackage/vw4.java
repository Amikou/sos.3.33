package defpackage;

import android.content.Context;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.j;
import java.io.File;
import java.util.concurrent.Executor;

/* renamed from: vw4  reason: default package */
/* loaded from: classes2.dex */
public final class vw4 implements jw4<uw4> {
    public final jw4 a;
    public final jw4 b;
    public final jw4 c;
    public final jw4 d;
    public final jw4 e;
    public final jw4 f;
    public final /* synthetic */ int g = 0;

    public vw4(jw4<c> jw4Var, jw4<zy4> jw4Var2, jw4<zv4> jw4Var3, jw4<Executor> jw4Var4, jw4<fv4> jw4Var5, jw4<ws4> jw4Var6) {
        this.a = jw4Var;
        this.b = jw4Var2;
        this.c = jw4Var3;
        this.d = jw4Var4;
        this.e = jw4Var5;
        this.f = jw4Var6;
    }

    public vw4(jw4<String> jw4Var, jw4<au4> jw4Var2, jw4<fv4> jw4Var3, jw4<Context> jw4Var4, jw4<ww4> jw4Var5, jw4<Executor> jw4Var6, byte[] bArr) {
        this.f = jw4Var;
        this.b = jw4Var2;
        this.e = jw4Var3;
        this.d = jw4Var4;
        this.c = jw4Var5;
        this.a = jw4Var6;
    }

    /* JADX WARN: Type inference failed for: r11v0, types: [uw4, com.google.android.play.core.assetpacks.j] */
    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ uw4 a() {
        if (this.g == 0) {
            Object a = this.a.a();
            cw4 c = gw4.c(this.b);
            Object a2 = this.c.a();
            return new uw4((c) a, c, (zv4) a2, gw4.c(this.d), (fv4) this.e.a(), (ws4) this.f.a());
        }
        String str = (String) this.f.a();
        Object a3 = this.b.a();
        Object a4 = this.e.a();
        Context a5 = ((my4) this.d).a();
        Object a6 = this.c.a();
        return new j(str != null ? new File(a5.getExternalFilesDir(null), str) : a5.getExternalFilesDir(null), (au4) a3, (fv4) a4, a5, (ww4) a6, gw4.c(this.a));
    }
}
