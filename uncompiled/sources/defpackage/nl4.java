package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: nl4  reason: default package */
/* loaded from: classes.dex */
public class nl4 {
    public static ScheduledExecutorService l;
    public final Object a;
    public final PowerManager.WakeLock b;
    public WorkSource c;
    public final int d;
    public final String e;
    public final String f;
    public final Context g;
    public boolean h;
    public final Map<String, Integer[]> i;
    public int j;
    public AtomicInteger k;

    static {
        new o35();
    }

    public nl4(Context context, int i, String str) {
        this(context, i, str, null, context == null ? null : context.getPackageName());
    }

    /* JADX WARN: Code restructure failed: missing block: B:17:0x0054, code lost:
        if (r2 == false) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x005c, code lost:
        if (r13.j == 0) goto L14;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x005e, code lost:
        defpackage.ql4.a().c(r13.g, defpackage.mt3.a(r13.b, r6), 7, r13.e, r6, null, r13.d, e(), r14);
        r13.j++;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void a(long r14) {
        /*
            r13 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r13.k
            r0.incrementAndGet()
            r0 = 0
            java.lang.String r6 = r13.d(r0)
            java.lang.Object r0 = r13.a
            monitor-enter(r0)
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch: java.lang.Throwable -> L96
            boolean r1 = r1.isEmpty()     // Catch: java.lang.Throwable -> L96
            r2 = 0
            if (r1 == 0) goto L1a
            int r1 = r13.j     // Catch: java.lang.Throwable -> L96
            if (r1 <= 0) goto L29
        L1a:
            android.os.PowerManager$WakeLock r1 = r13.b     // Catch: java.lang.Throwable -> L96
            boolean r1 = r1.isHeld()     // Catch: java.lang.Throwable -> L96
            if (r1 != 0) goto L29
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch: java.lang.Throwable -> L96
            r1.clear()     // Catch: java.lang.Throwable -> L96
            r13.j = r2     // Catch: java.lang.Throwable -> L96
        L29:
            boolean r1 = r13.h     // Catch: java.lang.Throwable -> L96
            r12 = 1
            if (r1 == 0) goto L56
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch: java.lang.Throwable -> L96
            java.lang.Object r1 = r1.get(r6)     // Catch: java.lang.Throwable -> L96
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1     // Catch: java.lang.Throwable -> L96
            if (r1 != 0) goto L47
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.i     // Catch: java.lang.Throwable -> L96
            java.lang.Integer[] r3 = new java.lang.Integer[r12]     // Catch: java.lang.Throwable -> L96
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)     // Catch: java.lang.Throwable -> L96
            r3[r2] = r4     // Catch: java.lang.Throwable -> L96
            r1.put(r6, r3)     // Catch: java.lang.Throwable -> L96
            r2 = r12
            goto L54
        L47:
            r3 = r1[r2]     // Catch: java.lang.Throwable -> L96
            int r3 = r3.intValue()     // Catch: java.lang.Throwable -> L96
            int r3 = r3 + r12
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch: java.lang.Throwable -> L96
            r1[r2] = r3     // Catch: java.lang.Throwable -> L96
        L54:
            if (r2 != 0) goto L5e
        L56:
            boolean r1 = r13.h     // Catch: java.lang.Throwable -> L96
            if (r1 != 0) goto L7d
            int r1 = r13.j     // Catch: java.lang.Throwable -> L96
            if (r1 != 0) goto L7d
        L5e:
            ql4 r1 = defpackage.ql4.a()     // Catch: java.lang.Throwable -> L96
            android.content.Context r2 = r13.g     // Catch: java.lang.Throwable -> L96
            android.os.PowerManager$WakeLock r3 = r13.b     // Catch: java.lang.Throwable -> L96
            java.lang.String r3 = defpackage.mt3.a(r3, r6)     // Catch: java.lang.Throwable -> L96
            r4 = 7
            java.lang.String r5 = r13.e     // Catch: java.lang.Throwable -> L96
            r7 = 0
            int r8 = r13.d     // Catch: java.lang.Throwable -> L96
            java.util.List r9 = r13.e()     // Catch: java.lang.Throwable -> L96
            r10 = r14
            r1.c(r2, r3, r4, r5, r6, r7, r8, r9, r10)     // Catch: java.lang.Throwable -> L96
            int r1 = r13.j     // Catch: java.lang.Throwable -> L96
            int r1 = r1 + r12
            r13.j = r1     // Catch: java.lang.Throwable -> L96
        L7d:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L96
            android.os.PowerManager$WakeLock r0 = r13.b
            r0.acquire()
            r0 = 0
            int r0 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1))
            if (r0 <= 0) goto L95
            java.util.concurrent.ScheduledExecutorService r0 = defpackage.nl4.l
            g75 r1 = new g75
            r1.<init>(r13)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.schedule(r1, r14, r2)
        L95:
            return
        L96:
            r14 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L96
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.nl4.a(long):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r1 != false) goto L13;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0052, code lost:
        if (r12.j == 1) goto L13;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0054, code lost:
        defpackage.ql4.a().b(r12.g, defpackage.mt3.a(r12.b, r6), 8, r12.e, r6, null, r12.d, e());
        r12.j--;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void b() {
        /*
            r12 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r12.k
            int r0 = r0.decrementAndGet()
            if (r0 >= 0) goto L13
            java.lang.String r0 = r12.e
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r1 = " release without a matched acquire!"
            r0.concat(r1)
        L13:
            r0 = 0
            java.lang.String r6 = r12.d(r0)
            java.lang.Object r0 = r12.a
            monitor-enter(r0)
            boolean r1 = r12.h     // Catch: java.lang.Throwable -> L78
            r10 = 1
            r11 = 0
            if (r1 == 0) goto L4c
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r12.i     // Catch: java.lang.Throwable -> L78
            java.lang.Object r1 = r1.get(r6)     // Catch: java.lang.Throwable -> L78
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1     // Catch: java.lang.Throwable -> L78
            if (r1 != 0) goto L2d
        L2b:
            r1 = r11
            goto L4a
        L2d:
            r2 = r1[r11]     // Catch: java.lang.Throwable -> L78
            int r2 = r2.intValue()     // Catch: java.lang.Throwable -> L78
            if (r2 != r10) goto L3c
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r12.i     // Catch: java.lang.Throwable -> L78
            r1.remove(r6)     // Catch: java.lang.Throwable -> L78
            r1 = r10
            goto L4a
        L3c:
            r2 = r1[r11]     // Catch: java.lang.Throwable -> L78
            int r2 = r2.intValue()     // Catch: java.lang.Throwable -> L78
            int r2 = r2 - r10
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch: java.lang.Throwable -> L78
            r1[r11] = r2     // Catch: java.lang.Throwable -> L78
            goto L2b
        L4a:
            if (r1 != 0) goto L54
        L4c:
            boolean r1 = r12.h     // Catch: java.lang.Throwable -> L78
            if (r1 != 0) goto L73
            int r1 = r12.j     // Catch: java.lang.Throwable -> L78
            if (r1 != r10) goto L73
        L54:
            ql4 r1 = defpackage.ql4.a()     // Catch: java.lang.Throwable -> L78
            android.content.Context r2 = r12.g     // Catch: java.lang.Throwable -> L78
            android.os.PowerManager$WakeLock r3 = r12.b     // Catch: java.lang.Throwable -> L78
            java.lang.String r3 = defpackage.mt3.a(r3, r6)     // Catch: java.lang.Throwable -> L78
            r4 = 8
            java.lang.String r5 = r12.e     // Catch: java.lang.Throwable -> L78
            r7 = 0
            int r8 = r12.d     // Catch: java.lang.Throwable -> L78
            java.util.List r9 = r12.e()     // Catch: java.lang.Throwable -> L78
            r1.b(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch: java.lang.Throwable -> L78
            int r1 = r12.j     // Catch: java.lang.Throwable -> L78
            int r1 = r1 - r10
            r12.j = r1     // Catch: java.lang.Throwable -> L78
        L73:
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L78
            r12.f(r11)
            return
        L78:
            r1 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L78
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.nl4.b():void");
    }

    public void c(boolean z) {
        this.b.setReferenceCounted(z);
        this.h = z;
    }

    public final String d(String str) {
        return (!this.h || TextUtils.isEmpty(str)) ? this.f : str;
    }

    public final List<String> e() {
        return sq4.b(this.c);
    }

    public final void f(int i) {
        if (this.b.isHeld()) {
            try {
                this.b.release();
            } catch (RuntimeException e) {
                if (e.getClass().equals(RuntimeException.class)) {
                    String.valueOf(this.e).concat(" was already released!");
                } else {
                    throw e;
                }
            }
            this.b.isHeld();
        }
    }

    public nl4(Context context, int i, String str, String str2, String str3) {
        this(context, i, str, null, str3, null);
    }

    @SuppressLint({"UnwrappedWakeLock"})
    public nl4(Context context, int i, String str, String str2, String str3, String str4) {
        this.a = this;
        this.h = true;
        this.i = new HashMap();
        Collections.synchronizedSet(new HashSet());
        this.k = new AtomicInteger(0);
        zt2.k(context, "WakeLock: context must not be null");
        zt2.g(str, "WakeLock: wakeLockName must not be empty");
        this.d = i;
        this.f = null;
        Context applicationContext = context.getApplicationContext();
        this.g = applicationContext;
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(str);
            this.e = valueOf.length() != 0 ? "*gcore*:".concat(valueOf) : new String("*gcore*:");
        } else {
            this.e = str;
        }
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(i, str);
        this.b = newWakeLock;
        if (sq4.c(context)) {
            WorkSource a = sq4.a(context, vu3.a(str3) ? context.getPackageName() : str3);
            this.c = a;
            if (a != null && sq4.c(applicationContext)) {
                WorkSource workSource = this.c;
                if (workSource != null) {
                    workSource.add(a);
                } else {
                    this.c = a;
                }
                try {
                    newWakeLock.setWorkSource(this.c);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
                    Log.wtf("WakeLock", e.toString());
                }
            }
        }
        if (l == null) {
            l = dt2.a().a();
        }
    }
}
