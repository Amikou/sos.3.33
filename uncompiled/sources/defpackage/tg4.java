package defpackage;

import android.text.TextUtils;
import com.google.firebase.installations.local.b;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/* compiled from: Utils.java */
/* renamed from: tg4  reason: default package */
/* loaded from: classes2.dex */
public final class tg4 {
    public static final long b = TimeUnit.HOURS.toSeconds(1);
    public static final Pattern c = Pattern.compile("\\AA[\\w-]{38}\\z");
    public static tg4 d;
    public final sz a;

    public tg4(sz szVar) {
        this.a = szVar;
    }

    public static tg4 c() {
        return d(p24.b());
    }

    public static tg4 d(sz szVar) {
        if (d == null) {
            d = new tg4(szVar);
        }
        return d;
    }

    public static boolean g(String str) {
        return c.matcher(str).matches();
    }

    public static boolean h(String str) {
        return str.contains(":");
    }

    public long a() {
        return this.a.a();
    }

    public long b() {
        return TimeUnit.MILLISECONDS.toSeconds(a());
    }

    public long e() {
        return (long) (Math.random() * 1000.0d);
    }

    public boolean f(b bVar) {
        return TextUtils.isEmpty(bVar.b()) || bVar.h() + bVar.c() < b() + b;
    }
}
