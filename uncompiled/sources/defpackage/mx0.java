package defpackage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;

/* compiled from: EthSyncing.java */
/* renamed from: mx0  reason: default package */
/* loaded from: classes3.dex */
public class mx0 extends i83<b> {

    /* compiled from: EthSyncing.java */
    /* renamed from: mx0$a */
    /* loaded from: classes3.dex */
    public static class a extends com.fasterxml.jackson.databind.c<b> {
        private ObjectReader objectReader = ml2.getObjectReader();

        @Override // com.fasterxml.jackson.databind.c
        public b deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            if (jsonParser.u() == JsonToken.VALUE_FALSE) {
                b bVar = new b();
                bVar.setSyncing(jsonParser.l());
                return bVar;
            }
            return (b) this.objectReader.readValue(jsonParser, c.class);
        }
    }

    /* compiled from: EthSyncing.java */
    /* renamed from: mx0$b */
    /* loaded from: classes3.dex */
    public static class b {
        private boolean isSyncing = true;

        public boolean isSyncing() {
            return this.isSyncing;
        }

        public void setSyncing(boolean z) {
            this.isSyncing = z;
        }
    }

    /* compiled from: EthSyncing.java */
    @JsonIgnoreProperties({"knownStates", "pulledStates"})
    /* renamed from: mx0$c */
    /* loaded from: classes3.dex */
    public static class c extends b {
        private String currentBlock;
        private String highestBlock;
        private String knownStates;
        private String pulledStates;
        private String startingBlock;

        public c() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                if (isSyncing() != cVar.isSyncing()) {
                    return false;
                }
                if (getStartingBlock() == null ? cVar.getStartingBlock() == null : getStartingBlock().equals(cVar.getStartingBlock())) {
                    if (getCurrentBlock() == null ? cVar.getCurrentBlock() == null : getCurrentBlock().equals(cVar.getCurrentBlock())) {
                        if (getHighestBlock() == null ? cVar.getHighestBlock() == null : getHighestBlock().equals(cVar.getHighestBlock())) {
                            String str = this.knownStates;
                            if (str == null ? cVar.knownStates == null : str.equals(cVar.knownStates)) {
                                String str2 = this.pulledStates;
                                if (str2 != null) {
                                    return str2.equals(cVar.pulledStates);
                                }
                                return cVar.pulledStates == null;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public String getCurrentBlock() {
            return this.currentBlock;
        }

        public String getHighestBlock() {
            return this.highestBlock;
        }

        public String getStartingBlock() {
            return this.startingBlock;
        }

        public int hashCode() {
            int hashCode = (((((((getStartingBlock() != null ? getStartingBlock().hashCode() : 0) * 31) + m30.hashCode(isSyncing())) * 31) + (getCurrentBlock() != null ? getCurrentBlock().hashCode() : 0)) * 31) + (getHighestBlock() != null ? getHighestBlock().hashCode() : 0)) * 31;
            String str = this.knownStates;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.pulledStates;
            return hashCode2 + (str2 != null ? str2.hashCode() : 0);
        }

        public void setCurrentBlock(String str) {
            this.currentBlock = str;
        }

        public void setHighestBlock(String str) {
            this.highestBlock = str;
        }

        public void setStartingBlock(String str) {
            this.startingBlock = str;
        }

        public c(String str, String str2, String str3, String str4, String str5) {
            this.startingBlock = str;
            this.currentBlock = str2;
            this.highestBlock = str3;
            this.knownStates = str4;
            this.pulledStates = str5;
        }
    }

    public boolean isSyncing() {
        return getResult().isSyncing();
    }

    @Override // defpackage.i83
    @com.fasterxml.jackson.databind.annotation.b(using = a.class)
    public void setResult(b bVar) {
        super.setResult((mx0) bVar);
    }
}
