package defpackage;

import com.facebook.datasource.AbstractDataSource;
import java.util.Map;

/* compiled from: AbstractProducerToDataSourceAdapter.java */
/* renamed from: g5  reason: default package */
/* loaded from: classes.dex */
public abstract class g5<T> extends AbstractDataSource<T> {
    public final xm3 i;
    public final i73 j;

    /* compiled from: AbstractProducerToDataSourceAdapter.java */
    /* renamed from: g5$a */
    /* loaded from: classes.dex */
    public class a extends qm<T> {
        public a() {
        }

        @Override // defpackage.qm
        public void g() {
            g5.this.D();
        }

        @Override // defpackage.qm
        public void h(Throwable th) {
            g5.this.E(th);
        }

        @Override // defpackage.qm
        public void i(T t, int i) {
            g5 g5Var = g5.this;
            g5Var.F(t, i, g5Var.i);
        }

        @Override // defpackage.qm
        public void j(float f) {
            g5.this.s(f);
        }
    }

    public g5(dv2<T> dv2Var, xm3 xm3Var, i73 i73Var) {
        if (nc1.d()) {
            nc1.a("AbstractProducerToDataSourceAdapter()");
        }
        this.i = xm3Var;
        this.j = i73Var;
        G();
        if (nc1.d()) {
            nc1.a("AbstractProducerToDataSourceAdapter()->onRequestStart");
        }
        i73Var.b(xm3Var);
        if (nc1.d()) {
            nc1.b();
        }
        if (nc1.d()) {
            nc1.a("AbstractProducerToDataSourceAdapter()->produceResult");
        }
        dv2Var.a(B(), xm3Var);
        if (nc1.d()) {
            nc1.b();
        }
        if (nc1.d()) {
            nc1.b();
        }
    }

    public final l60<T> B() {
        return new a();
    }

    public Map<String, Object> C(ev2 ev2Var) {
        return ev2Var.getExtras();
    }

    public final synchronized void D() {
        xt2.i(k());
    }

    public final void E(Throwable th) {
        if (super.q(th, C(this.i))) {
            this.j.f(this.i, th);
        }
    }

    public void F(T t, int i, ev2 ev2Var) {
        boolean e = qm.e(i);
        if (super.u(t, e, C(ev2Var)) && e) {
            this.j.g(this.i);
        }
    }

    public final void G() {
        o(this.i.getExtras());
    }

    @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
    public boolean close() {
        if (super.close()) {
            if (super.b()) {
                return true;
            }
            this.j.h(this.i);
            this.i.t();
            return true;
        }
        return false;
    }
}
