package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: y26  reason: default package */
/* loaded from: classes.dex */
public interface y26 {
    long b();

    String c();

    boolean zza();

    double zzb();

    long zzc();
}
