package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import java.util.List;

/* compiled from: BeanSerializerModifier.java */
/* renamed from: xo  reason: default package */
/* loaded from: classes.dex */
public abstract class xo {
    public List<BeanPropertyWriter> a(SerializationConfig serializationConfig, so soVar, List<BeanPropertyWriter> list) {
        return list;
    }

    public f<?> b(SerializationConfig serializationConfig, ArrayType arrayType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> c(SerializationConfig serializationConfig, CollectionLikeType collectionLikeType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> d(SerializationConfig serializationConfig, CollectionType collectionType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> e(SerializationConfig serializationConfig, JavaType javaType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> f(SerializationConfig serializationConfig, JavaType javaType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> g(SerializationConfig serializationConfig, MapLikeType mapLikeType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> h(SerializationConfig serializationConfig, MapType mapType, so soVar, f<?> fVar) {
        return fVar;
    }

    public f<?> i(SerializationConfig serializationConfig, so soVar, f<?> fVar) {
        return fVar;
    }

    public List<BeanPropertyWriter> j(SerializationConfig serializationConfig, so soVar, List<BeanPropertyWriter> list) {
        return list;
    }

    public wo k(SerializationConfig serializationConfig, so soVar, wo woVar) {
        return woVar;
    }
}
