package defpackage;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: lz4  reason: default package */
/* loaded from: classes.dex */
public final class lz4 {
    public static final a.g<so3> a;
    public static final a.g<so3> b;
    public static final a.AbstractC0106a<so3, uo3> c;
    public static final a.AbstractC0106a<so3, s15> d;
    public static final a<uo3> e;

    static {
        a.g<so3> gVar = new a.g<>();
        a = gVar;
        a.g<so3> gVar2 = new a.g<>();
        b = gVar2;
        b15 b15Var = new b15();
        c = b15Var;
        o05 o05Var = new o05();
        d = o05Var;
        new Scope("profile");
        new Scope("email");
        e = new a<>("SignIn.API", b15Var, gVar);
        new a("SignIn.INTERNAL_API", o05Var, gVar2);
    }
}
