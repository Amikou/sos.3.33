package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: y65  reason: default package */
/* loaded from: classes.dex */
public final class y65 extends n65<Boolean> implements rb5<Boolean> {
    public boolean[] f0;
    public int g0;

    static {
        new y65().v();
    }

    public y65() {
        this(new boolean[10], 0);
    }

    public y65(boolean[] zArr, int i) {
        this.f0 = zArr;
        this.g0 = i;
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5<Boolean> T0(int i) {
        if (i >= this.g0) {
            return new y65(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        k(i, ((Boolean) obj).booleanValue());
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Boolean> collection) {
        e();
        gb5.a(collection);
        if (collection instanceof y65) {
            y65 y65Var = (y65) collection;
            int i = y65Var.g0;
            if (i == 0) {
                return false;
            }
            int i2 = this.g0;
            if (Integer.MAX_VALUE - i2 >= i) {
                int i3 = i2 + i;
                boolean[] zArr = this.f0;
                if (i3 > zArr.length) {
                    this.f0 = Arrays.copyOf(zArr, i3);
                }
                System.arraycopy(y65Var.f0, 0, this.f0, this.g0, y65Var.g0);
                this.g0 = i3;
                ((AbstractList) this).modCount++;
                return true;
            }
            throw new OutOfMemoryError();
        }
        return super.addAll(collection);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof y65) {
            y65 y65Var = (y65) obj;
            if (this.g0 != y65Var.g0) {
                return false;
            }
            boolean[] zArr = y65Var.f0;
            for (int i = 0; i < this.g0; i++) {
                if (this.f0[i] != zArr[i]) {
                    return false;
                }
            }
            return true;
        }
        return super.equals(obj);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        m(i);
        return Boolean.valueOf(this.f0[i]);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + gb5.f(this.f0[i2]);
        }
        return i;
    }

    public final void i(boolean z) {
        k(this.g0, z);
    }

    public final void k(int i, boolean z) {
        int i2;
        e();
        if (i < 0 || i > (i2 = this.g0)) {
            throw new IndexOutOfBoundsException(n(i));
        }
        boolean[] zArr = this.f0;
        if (i2 < zArr.length) {
            System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
        } else {
            boolean[] zArr2 = new boolean[((i2 * 3) / 2) + 1];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.f0, i, zArr2, i + 1, this.g0 - i);
            this.f0 = zArr2;
        }
        this.f0[i] = z;
        this.g0++;
        ((AbstractList) this).modCount++;
    }

    public final void m(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(n(i));
        }
    }

    public final String n(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        m(i);
        boolean[] zArr = this.f0;
        boolean z = zArr[i];
        int i2 = this.g0;
        if (i < i2 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, i2 - i);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Boolean.valueOf(z);
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Boolean.valueOf(this.f0[i]))) {
                boolean[] zArr = this.f0;
                System.arraycopy(zArr, i + 1, zArr, i, this.g0 - i);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        boolean[] zArr = this.f0;
        System.arraycopy(zArr, i2, zArr, i, this.g0 - i2);
        this.g0 -= i2 - i;
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        e();
        m(i);
        boolean[] zArr = this.f0;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }
}
