package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.c;

/* compiled from: CoroutineContext.kt */
/* renamed from: x80  reason: default package */
/* loaded from: classes2.dex */
public final class x80 {
    public static final boolean a;

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0027, code lost:
        if (r0.equals("on") != false) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0030, code lost:
        if (r0.equals("") != false) goto L21;
     */
    static {
        /*
            java.lang.String r0 = "kotlinx.coroutines.scheduler"
            java.lang.String r0 = defpackage.a34.d(r0)
            if (r0 == 0) goto L53
            int r1 = r0.hashCode()
            if (r1 == 0) goto L2a
            r2 = 3551(0xddf, float:4.976E-42)
            if (r1 == r2) goto L21
            r2 = 109935(0x1ad6f, float:1.54052E-40)
            if (r1 != r2) goto L33
            java.lang.String r1 = "off"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L33
            r0 = 0
            goto L54
        L21:
            java.lang.String r1 = "on"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L33
            goto L53
        L2a:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L33
            goto L53
        L33:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "System property 'kotlinx.coroutines.scheduler' has unrecognized value '"
            r1.append(r2)
            r1.append(r0)
            r0 = 39
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L53:
            r0 = 1
        L54:
            defpackage.x80.a = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.x80.<clinit>():void");
    }

    public static final CoroutineDispatcher a() {
        return a ? pk0.k0 : c.f0;
    }

    public static final String b(CoroutineContext coroutineContext) {
        a90 a90Var;
        String h;
        if (ze0.c() && (a90Var = (a90) coroutineContext.get(a90.f0)) != null) {
            b90 b90Var = (b90) coroutineContext.get(b90.f0);
            String str = "coroutine";
            if (b90Var != null && (h = b90Var.h()) != null) {
                str = h;
            }
            return str + '#' + a90Var.h();
        }
        return null;
    }

    public static final CoroutineContext c(c90 c90Var, CoroutineContext coroutineContext) {
        CoroutineContext plus = c90Var.m().plus(coroutineContext);
        CoroutineContext plus2 = ze0.c() ? plus.plus(new a90(ze0.b().incrementAndGet())) : plus;
        tp0 tp0Var = tp0.a;
        return (plus == tp0.a() || plus.get(r70.d) != null) ? plus2 : plus2.plus(tp0.a());
    }

    public static final qe4<?> d(e90 e90Var) {
        while (!(e90Var instanceof pp0) && (e90Var = e90Var.getCallerFrame()) != null) {
            if (e90Var instanceof qe4) {
                return (qe4) e90Var;
            }
        }
        return null;
    }

    public static final qe4<?> e(q70<?> q70Var, CoroutineContext coroutineContext, Object obj) {
        if (q70Var instanceof e90) {
            if (coroutineContext.get(se4.a) != null) {
                qe4<?> d = d((e90) q70Var);
                if (d != null) {
                    d.N0(coroutineContext, obj);
                }
                return d;
            }
            return null;
        }
        return null;
    }
}
