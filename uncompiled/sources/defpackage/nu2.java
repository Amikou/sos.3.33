package defpackage;

import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: PrefillUserDefaultTokensUseCase.kt */
/* renamed from: nu2  reason: default package */
/* loaded from: classes2.dex */
public final class nu2 implements nm1 {
    public final pl1 a;
    public final bn1 b;

    public nu2(pl1 pl1Var, bn1 bn1Var) {
        fs1.f(pl1Var, "allTokensListRepository");
        fs1.f(bn1Var, "userTokenListRepository");
        this.a = pl1Var;
        this.b = bn1Var;
    }

    @Override // defpackage.nm1
    public Object a(q70<? super te4> q70Var) {
        b();
        return te4.a;
    }

    public final void b() {
        Iterator<T> it = ApplicationRoomDatabase.n.d().iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            List<IToken> a = this.a.a((TokenType) pair.getFirst());
            for (String str : (List) pair.getSecond()) {
                for (IToken iToken : a) {
                    if (fs1.b(iToken.getSymbolWithType(), str) && !this.b.g(iToken, TokenType.Companion.b(iToken.getChainId()))) {
                        this.b.i(iToken);
                    }
                }
            }
        }
    }
}
