package defpackage;

import com.google.android.gms.internal.vision.zzht;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: gu5  reason: default package */
/* loaded from: classes.dex */
public interface gu5 extends List {
    List<?> b();

    gu5 c();

    void f1(zzht zzhtVar);

    Object h(int i);
}
