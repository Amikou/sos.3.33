package defpackage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/* compiled from: OkCacheControl.java */
/* renamed from: am2  reason: default package */
/* loaded from: classes2.dex */
public class am2 {
    public d a;
    public long b;
    public TimeUnit c;
    public OkHttpClient.Builder d;
    public c e;

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$a */
    /* loaded from: classes2.dex */
    public class a implements Interceptor {
        public final /* synthetic */ f a;
        public final /* synthetic */ g b;

        public a(f fVar, g gVar) {
            this.a = fVar;
            this.b = gVar;
        }

        @Override // okhttp3.Interceptor
        public Response intercept(Interceptor.Chain chain) throws IOException {
            return this.b.a(chain.proceed(this.a.a(chain.request())));
        }
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$b */
    /* loaded from: classes2.dex */
    public static class b extends g {
        public c a;

        public /* synthetic */ b(c cVar, a aVar) {
            this(cVar);
        }

        @Override // defpackage.am2.g
        public Response a(Response response) {
            Response.Builder removeHeader = response.newBuilder().removeHeader("Pragma").removeHeader("Cache-Control");
            return removeHeader.header("Cache-Control", "max-age=" + this.a.a()).build();
        }

        public b(c cVar) {
            super(null);
            this.a = cVar;
        }
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$c */
    /* loaded from: classes2.dex */
    public interface c {
        long a();
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$d */
    /* loaded from: classes2.dex */
    public interface d {
        boolean a();
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$e */
    /* loaded from: classes2.dex */
    public static class e extends f {
        public d a;

        public /* synthetic */ e(d dVar, a aVar) {
            this(dVar);
        }

        @Override // defpackage.am2.f
        public Request a(Request request) {
            Request.Builder newBuilder = request.newBuilder();
            if (!this.a.a()) {
                newBuilder.cacheControl(CacheControl.FORCE_CACHE);
            }
            return newBuilder.build();
        }

        public e(d dVar) {
            super(null);
            this.a = dVar;
        }
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$f */
    /* loaded from: classes2.dex */
    public static class f {
        public f() {
        }

        public Request a(Request request) {
            return request;
        }

        public /* synthetic */ f(a aVar) {
            this();
        }
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$g */
    /* loaded from: classes2.dex */
    public static class g {
        public g() {
        }

        public Response a(Response response) {
            return response;
        }

        public /* synthetic */ g(a aVar) {
            this();
        }
    }

    /* compiled from: OkCacheControl.java */
    /* renamed from: am2$h */
    /* loaded from: classes2.dex */
    public static class h implements c {
        public TimeUnit a;
        public long b;

        public /* synthetic */ h(long j, TimeUnit timeUnit, a aVar) {
            this(j, timeUnit);
        }

        @Override // defpackage.am2.c
        public long a() {
            return this.a.toSeconds(this.b);
        }

        public h(long j, TimeUnit timeUnit) {
            this.a = timeUnit;
            this.b = j;
        }
    }

    public am2(OkHttpClient.Builder builder) {
        this.d = builder;
    }

    public static Interceptor b(f fVar, g gVar) {
        return new a(fVar, gVar);
    }

    public static am2 c(OkHttpClient.Builder builder) {
        return new am2(builder);
    }

    public OkHttpClient.Builder a() {
        g gVar;
        f fVar;
        if (this.a == null && this.c == null && this.e == null) {
            return this.d;
        }
        TimeUnit timeUnit = this.c;
        if (timeUnit != null) {
            this.e = new h(this.b, timeUnit, null);
        }
        c cVar = this.e;
        if (cVar != null) {
            gVar = new b(cVar, null);
        } else {
            gVar = new g(null);
        }
        d dVar = this.a;
        if (dVar != null) {
            fVar = new e(dVar, null);
        } else {
            fVar = new f(null);
        }
        Interceptor b2 = b(fVar, gVar);
        this.d.addNetworkInterceptor(b2);
        if (this.a != null) {
            this.d.addInterceptor(b2);
        }
        return this.d;
    }

    public am2 d(long j, TimeUnit timeUnit) {
        this.e = null;
        this.b = j;
        this.c = timeUnit;
        return this;
    }
}
