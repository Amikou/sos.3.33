package defpackage;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.a;
import okio.k;
import okio.l;

/* compiled from: JvmOkio.kt */
/* renamed from: br3  reason: default package */
/* loaded from: classes2.dex */
public final class br3 extends a {
    public final Socket a;

    public br3(Socket socket) {
        fs1.f(socket, "socket");
        this.a = socket;
    }

    @Override // okio.a
    public IOException newTimeoutException(IOException iOException) {
        SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
        if (iOException != null) {
            socketTimeoutException.initCause(iOException);
        }
        return socketTimeoutException;
    }

    @Override // okio.a
    public void timedOut() {
        Logger logger;
        Logger logger2;
        try {
            this.a.close();
        } catch (AssertionError e) {
            if (k.e(e)) {
                logger2 = l.a;
                Level level = Level.WARNING;
                logger2.log(level, "Failed to close timed out socket " + this.a, (Throwable) e);
                return;
            }
            throw e;
        } catch (Exception e2) {
            logger = l.a;
            Level level2 = Level.WARNING;
            logger.log(level2, "Failed to close timed out socket " + this.a, (Throwable) e2);
        }
    }
}
