package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;

/* compiled from: Converter.java */
/* renamed from: o80  reason: default package */
/* loaded from: classes.dex */
public interface o80<IN, OUT> {

    /* compiled from: Converter.java */
    /* renamed from: o80$a */
    /* loaded from: classes.dex */
    public static abstract class a implements o80<Object, Object> {
    }

    OUT a(IN in);

    JavaType b(TypeFactory typeFactory);

    JavaType c(TypeFactory typeFactory);
}
