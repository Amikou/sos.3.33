package defpackage;

import android.os.Bundle;
import androidx.media3.common.e;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.List;

/* compiled from: CueGroup.java */
/* renamed from: nb0  reason: default package */
/* loaded from: classes.dex */
public final class nb0 implements e {
    public static final nb0 f0 = new nb0(ImmutableList.of());
    public final ImmutableList<kb0> a;

    public nb0(List<kb0> list) {
        this.a = ImmutableList.copyOf((Collection) list);
    }

    public static ImmutableList<kb0> a(List<kb0> list) {
        ImmutableList.a builder = ImmutableList.builder();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).h0 == null) {
                builder.a(list.get(i));
            }
        }
        return builder.l();
    }

    public static String b(int i) {
        return Integer.toString(i, 36);
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(b(0), is.c(a(this.a)));
        return bundle;
    }
}
