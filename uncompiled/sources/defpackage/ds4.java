package defpackage;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.f;
import org.bouncycastle.pqc.crypto.xmss.i;

/* renamed from: ds4  reason: default package */
/* loaded from: classes2.dex */
public final class ds4 extends pi {
    public final f a;
    public final byte[] f0;
    public final byte[] g0;

    /* renamed from: ds4$b */
    /* loaded from: classes2.dex */
    public static class b {
        public final f a;
        public byte[] b = null;
        public byte[] c = null;
        public byte[] d = null;

        public b(f fVar) {
            this.a = fVar;
        }

        public ds4 e() {
            return new ds4(this);
        }

        public b f(byte[] bArr) {
            this.c = i.c(bArr);
            return this;
        }

        public b g(byte[] bArr) {
            this.b = i.c(bArr);
            return this;
        }
    }

    public ds4(b bVar) {
        super(false);
        f fVar = bVar.a;
        this.a = fVar;
        Objects.requireNonNull(fVar, "params == null");
        int b2 = fVar.b();
        byte[] bArr = bVar.d;
        if (bArr != null) {
            if (bArr.length != b2 + b2) {
                throw new IllegalArgumentException("public key has wrong size");
            }
            this.f0 = i.g(bArr, 0, b2);
            this.g0 = i.g(bArr, b2 + 0, b2);
            return;
        }
        byte[] bArr2 = bVar.b;
        if (bArr2 == null) {
            this.f0 = new byte[b2];
        } else if (bArr2.length != b2) {
            throw new IllegalArgumentException("length of root must be equal to length of digest");
        } else {
            this.f0 = bArr2;
        }
        byte[] bArr3 = bVar.c;
        if (bArr3 == null) {
            this.g0 = new byte[b2];
        } else if (bArr3.length != b2) {
            throw new IllegalArgumentException("length of publicSeed must be equal to length of digest");
        } else {
            this.g0 = bArr3;
        }
    }

    public f a() {
        return this.a;
    }

    public byte[] b() {
        return i.c(this.g0);
    }

    public byte[] c() {
        return i.c(this.f0);
    }

    public byte[] d() {
        int b2 = this.a.b();
        byte[] bArr = new byte[b2 + b2];
        i.e(bArr, this.f0, 0);
        i.e(bArr, this.g0, b2 + 0);
        return bArr;
    }
}
