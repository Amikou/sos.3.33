package defpackage;

import com.bumptech.glide.load.engine.e;
import com.bumptech.glide.load.engine.i;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: LoadPathCache.java */
/* renamed from: v02  reason: default package */
/* loaded from: classes.dex */
public class v02 {
    public static final i<?, ?, ?> c = new i<>(Object.class, Object.class, Object.class, Collections.singletonList(new e(Object.class, Object.class, Object.class, Collections.emptyList(), new we4(), null)), null);
    public final rh<na2, i<?, ?, ?>> a = new rh<>();
    public final AtomicReference<na2> b = new AtomicReference<>();

    public <Data, TResource, Transcode> i<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        i<Data, TResource, Transcode> iVar;
        na2 b = b(cls, cls2, cls3);
        synchronized (this.a) {
            iVar = (i<Data, TResource, Transcode>) this.a.get(b);
        }
        this.b.set(b);
        return iVar;
    }

    public final na2 b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        na2 andSet = this.b.getAndSet(null);
        if (andSet == null) {
            andSet = new na2();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    public boolean c(i<?, ?, ?> iVar) {
        return c.equals(iVar);
    }

    public void d(Class<?> cls, Class<?> cls2, Class<?> cls3, i<?, ?, ?> iVar) {
        synchronized (this.a) {
            rh<na2, i<?, ?, ?>> rhVar = this.a;
            na2 na2Var = new na2(cls, cls2, cls3);
            if (iVar == null) {
                iVar = c;
            }
            rhVar.put(na2Var, iVar);
        }
    }
}
