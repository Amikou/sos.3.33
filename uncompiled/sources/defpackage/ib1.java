package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentTransactionNotificationDetailsBinding.java */
/* renamed from: ib1  reason: default package */
/* loaded from: classes2.dex */
public final class ib1 {
    public final ConstraintLayout a;
    public final ConstraintLayout b;
    public final CardView c;
    public final ImageView d;
    public final ProgressBar e;
    public final rp3 f;
    public final TextView g;
    public final TextView h;
    public final TextView i;
    public final TextView j;
    public final TextView k;
    public final TextView l;
    public final MaterialTextView m;

    public ib1(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CardView cardView, ImageView imageView, ProgressBar progressBar, TextView textView, rp3 rp3Var, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, MaterialTextView materialTextView) {
        this.a = constraintLayout;
        this.b = constraintLayout2;
        this.c = cardView;
        this.d = imageView;
        this.e = progressBar;
        this.f = rp3Var;
        this.g = textView2;
        this.h = textView3;
        this.i = textView5;
        this.j = textView6;
        this.k = textView7;
        this.l = textView8;
        this.m = materialTextView;
    }

    public static ib1 a(View view) {
        int i = R.id.cl_l;
        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.cl_l);
        if (constraintLayout != null) {
            i = R.id.cv_2;
            CardView cardView = (CardView) ai4.a(view, R.id.cv_2);
            if (cardView != null) {
                i = R.id.imageView2;
                ImageView imageView = (ImageView) ai4.a(view, R.id.imageView2);
                if (imageView != null) {
                    i = R.id.pbDetails;
                    ProgressBar progressBar = (ProgressBar) ai4.a(view, R.id.pbDetails);
                    if (progressBar != null) {
                        i = R.id.textView26;
                        TextView textView = (TextView) ai4.a(view, R.id.textView26);
                        if (textView != null) {
                            i = R.id.toolbar;
                            View a = ai4.a(view, R.id.toolbar);
                            if (a != null) {
                                rp3 a2 = rp3.a(a);
                                i = R.id.tv_check;
                                TextView textView2 = (TextView) ai4.a(view, R.id.tv_check);
                                if (textView2 != null) {
                                    i = R.id.tv_date;
                                    TextView textView3 = (TextView) ai4.a(view, R.id.tv_date);
                                    if (textView3 != null) {
                                        i = R.id.tv_fee_amount;
                                        TextView textView4 = (TextView) ai4.a(view, R.id.tv_fee_amount);
                                        if (textView4 != null) {
                                            i = R.id.tv_nw_fee;
                                            TextView textView5 = (TextView) ai4.a(view, R.id.tv_nw_fee);
                                            if (textView5 != null) {
                                                i = R.id.tv_status;
                                                TextView textView6 = (TextView) ai4.a(view, R.id.tv_status);
                                                if (textView6 != null) {
                                                    i = R.id.tv_to;
                                                    TextView textView7 = (TextView) ai4.a(view, R.id.tv_to);
                                                    if (textView7 != null) {
                                                        i = R.id.tv_total;
                                                        TextView textView8 = (TextView) ai4.a(view, R.id.tv_total);
                                                        if (textView8 != null) {
                                                            i = R.id.tv_trans_status;
                                                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tv_trans_status);
                                                            if (materialTextView != null) {
                                                                return new ib1((ConstraintLayout) view, constraintLayout, cardView, imageView, progressBar, textView, a2, textView2, textView3, textView4, textView5, textView6, textView7, textView8, materialTextView);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
