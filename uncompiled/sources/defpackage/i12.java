package defpackage;

import android.app.Activity;
import com.onesignal.LocationController;
import com.onesignal.OneSignal;
import com.onesignal.PermissionsActivity;
import defpackage.ua;

/* compiled from: LocationPermissionController.kt */
/* renamed from: i12  reason: default package */
/* loaded from: classes2.dex */
public final class i12 implements PermissionsActivity.c {
    public static final i12 a;

    /* compiled from: LocationPermissionController.kt */
    /* renamed from: i12$a */
    /* loaded from: classes2.dex */
    public static final class a implements ua.a {
        public final /* synthetic */ Activity a;

        public a(Activity activity) {
            this.a = activity;
        }

        @Override // defpackage.ua.a
        public void a() {
            fe2.a.a(this.a);
            LocationController.n(true, OneSignal.PromptActionResult.PERMISSION_DENIED);
        }

        @Override // defpackage.ua.a
        public void b() {
            LocationController.n(true, OneSignal.PromptActionResult.PERMISSION_DENIED);
        }
    }

    static {
        i12 i12Var = new i12();
        a = i12Var;
        PermissionsActivity.e("LOCATION", i12Var);
    }

    @Override // com.onesignal.PermissionsActivity.c
    public void a() {
        c(OneSignal.PromptActionResult.PERMISSION_GRANTED);
        LocationController.p();
    }

    @Override // com.onesignal.PermissionsActivity.c
    public void b(boolean z) {
        c(OneSignal.PromptActionResult.PERMISSION_DENIED);
        if (z) {
            e();
        }
        LocationController.e();
    }

    public final void c(OneSignal.PromptActionResult promptActionResult) {
        LocationController.n(true, promptActionResult);
    }

    public final void d(boolean z, String str) {
        fs1.f(str, "androidPermissionString");
        PermissionsActivity.i(z, "LOCATION", str, i12.class);
    }

    public final void e() {
        Activity Q = OneSignal.Q();
        if (Q != null) {
            fs1.e(Q, "OneSignal.getCurrentActivity() ?: return");
            ua uaVar = ua.a;
            String string = Q.getString(s13.location_permission_name_for_title);
            fs1.e(string, "activity.getString(R.str…ermission_name_for_title)");
            String string2 = Q.getString(s13.location_permission_settings_message);
            fs1.e(string2, "activity.getString(R.str…mission_settings_message)");
            uaVar.a(Q, string, string2, new a(Q));
        }
    }
}
