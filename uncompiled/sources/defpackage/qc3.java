package defpackage;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ScalingUtils.java */
/* renamed from: qc3  reason: default package */
/* loaded from: classes.dex */
public class qc3 {

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$a */
    /* loaded from: classes.dex */
    public static abstract class a implements b {
        @Override // defpackage.qc3.b
        public Matrix a(Matrix matrix, Rect rect, int i, int i2, float f, float f2) {
            b(matrix, rect, i, i2, f, f2, rect.width() / i, rect.height() / i2);
            return matrix;
        }

        public abstract void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4);
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$b */
    /* loaded from: classes.dex */
    public interface b {
        public static final b a = k.j;
        public static final b b;
        public static final b c;
        public static final b d;
        public static final b e;
        public static final b f;
        public static final b g;
        public static final b h;
        public static final b i;

        static {
            b bVar = j.j;
            b bVar2 = l.j;
            b = i.j;
            c = g.j;
            d = h.j;
            e = c.j;
            f = e.j;
            g = d.j;
            h = m.j;
            i = f.j;
        }

        Matrix a(Matrix matrix, Rect rect, int i2, int i3, float f2, float f3);
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$c */
    /* loaded from: classes.dex */
    public static class c extends a {
        public static final b j = new c();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            matrix.setTranslate((int) (rect.left + ((rect.width() - i) * 0.5f) + 0.5f), (int) (rect.top + ((rect.height() - i2) * 0.5f) + 0.5f));
        }

        public String toString() {
            return "center";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$d */
    /* loaded from: classes.dex */
    public static class d extends a {
        public static final b j = new d();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float height;
            float f5;
            if (f4 > f3) {
                f5 = rect.left + ((rect.width() - (i * f4)) * 0.5f);
                height = rect.top;
                f3 = f4;
            } else {
                height = ((rect.height() - (i2 * f3)) * 0.5f) + rect.top;
                f5 = rect.left;
            }
            matrix.setScale(f3, f3);
            matrix.postTranslate((int) (f5 + 0.5f), (int) (height + 0.5f));
        }

        public String toString() {
            return "center_crop";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$e */
    /* loaded from: classes.dex */
    public static class e extends a {
        public static final b j = new e();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float min = Math.min(Math.min(f3, f4), 1.0f);
            float width = rect.left + ((rect.width() - (i * min)) * 0.5f);
            float height = rect.top + ((rect.height() - (i2 * min)) * 0.5f);
            matrix.setScale(min, min);
            matrix.postTranslate((int) (width + 0.5f), (int) (height + 0.5f));
        }

        public String toString() {
            return "center_inside";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$f */
    /* loaded from: classes.dex */
    public static class f extends a {
        public static final b j = new f();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float min = Math.min(f3, f4);
            matrix.setScale(min, min);
            matrix.postTranslate((int) (rect.left + 0.5f), (int) (rect.top + (rect.height() - (i2 * min)) + 0.5f));
        }

        public String toString() {
            return "fit_bottom_start";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$g */
    /* loaded from: classes.dex */
    public static class g extends a {
        public static final b j = new g();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float min = Math.min(f3, f4);
            float width = rect.left + ((rect.width() - (i * min)) * 0.5f);
            float height = rect.top + ((rect.height() - (i2 * min)) * 0.5f);
            matrix.setScale(min, min);
            matrix.postTranslate((int) (width + 0.5f), (int) (height + 0.5f));
        }

        public String toString() {
            return "fit_center";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$h */
    /* loaded from: classes.dex */
    public static class h extends a {
        public static final b j = new h();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float min = Math.min(f3, f4);
            matrix.setScale(min, min);
            matrix.postTranslate((int) (rect.left + (rect.width() - (i * min)) + 0.5f), (int) (rect.top + (rect.height() - (i2 * min)) + 0.5f));
        }

        public String toString() {
            return "fit_end";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$i */
    /* loaded from: classes.dex */
    public static class i extends a {
        public static final b j = new i();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float min = Math.min(f3, f4);
            matrix.setScale(min, min);
            matrix.postTranslate((int) (rect.left + 0.5f), (int) (rect.top + 0.5f));
        }

        public String toString() {
            return "fit_start";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$j */
    /* loaded from: classes.dex */
    public static class j extends a {
        public static final b j = new j();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float height = rect.top + ((rect.height() - (i2 * f3)) * 0.5f);
            matrix.setScale(f3, f3);
            matrix.postTranslate((int) (rect.left + 0.5f), (int) (height + 0.5f));
        }

        public String toString() {
            return "fit_x";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$k */
    /* loaded from: classes.dex */
    public static class k extends a {
        public static final b j = new k();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            matrix.setScale(f3, f4);
            matrix.postTranslate((int) (rect.left + 0.5f), (int) (rect.top + 0.5f));
        }

        public String toString() {
            return "fit_xy";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$l */
    /* loaded from: classes.dex */
    public static class l extends a {
        public static final b j = new l();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            matrix.setScale(f4, f4);
            matrix.postTranslate((int) (rect.left + ((rect.width() - (i * f4)) * 0.5f) + 0.5f), (int) (rect.top + 0.5f));
        }

        public String toString() {
            return "fit_y";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$m */
    /* loaded from: classes.dex */
    public static class m extends a {
        public static final b j = new m();

        @Override // defpackage.qc3.a
        public void b(Matrix matrix, Rect rect, int i, int i2, float f, float f2, float f3, float f4) {
            float f5;
            float max;
            if (f4 > f3) {
                float f6 = i * f4;
                f5 = rect.left + Math.max(Math.min((rect.width() * 0.5f) - (f * f6), (float) Utils.FLOAT_EPSILON), rect.width() - f6);
                max = rect.top;
                f3 = f4;
            } else {
                f5 = rect.left;
                float f7 = i2 * f3;
                max = Math.max(Math.min((rect.height() * 0.5f) - (f2 * f7), (float) Utils.FLOAT_EPSILON), rect.height() - f7) + rect.top;
            }
            matrix.setScale(f3, f3);
            matrix.postTranslate((int) (f5 + 0.5f), (int) (max + 0.5f));
        }

        public String toString() {
            return "focus_crop";
        }
    }

    /* compiled from: ScalingUtils.java */
    /* renamed from: qc3$n */
    /* loaded from: classes.dex */
    public interface n {
        Object getState();
    }

    public static oc3 a(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof oc3) {
            return (oc3) drawable;
        }
        if (drawable instanceof wq0) {
            return a(((wq0) drawable).i());
        }
        if (drawable instanceof nh) {
            nh nhVar = (nh) drawable;
            int e2 = nhVar.e();
            for (int i2 = 0; i2 < e2; i2++) {
                oc3 a2 = a(nhVar.b(i2));
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }
}
