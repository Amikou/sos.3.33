package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: r85  reason: default package */
/* loaded from: classes.dex */
public final class r85 extends o65 {
    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        if (str != null && !str.isEmpty() && wk5Var.d(str)) {
            z55 h = wk5Var.h(str);
            if (h instanceof c55) {
                return ((c55) h).a(wk5Var, list);
            }
            throw new IllegalArgumentException(String.format("Function %s is not defined", str));
        }
        throw new IllegalArgumentException(String.format("Command not found: %s", str));
    }
}
