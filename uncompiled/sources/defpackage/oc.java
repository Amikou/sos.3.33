package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.List;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;

/* compiled from: AnchorSelectSecurityQuestions.kt */
/* renamed from: oc  reason: default package */
/* loaded from: classes2.dex */
public final class oc {
    public final List<String> a;
    public PopupWindow b;

    public oc(List<String> list) {
        fs1.f(list, "list");
        this.a = list;
    }

    public static /* synthetic */ PopupWindow d(oc ocVar, View view, View view2, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 4;
        }
        return ocVar.c(view, view2, i);
    }

    public static final void h(tc1 tc1Var, String str, oc ocVar, View view) {
        fs1.f(tc1Var, "$onItemClickListener");
        fs1.f(str, "$question");
        fs1.f(ocVar, "this$0");
        tc1Var.invoke(str);
        ocVar.e();
    }

    public static final void i(tc1 tc1Var, String str, oc ocVar, View view) {
        fs1.f(tc1Var, "$onItemClickListener");
        fs1.f(str, "$question");
        fs1.f(ocVar, "this$0");
        tc1Var.invoke(str);
        ocVar.e();
    }

    public final PopupWindow c(View view, View view2, int i) {
        PopupWindow popupWindow = new PopupWindow(view, view2 == null ? -1 : view2.getWidth(), view.getContext().getResources().getDimensionPixelSize(R.dimen._150sdp));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void e() {
        PopupWindow popupWindow;
        if (!f() || (popupWindow = this.b) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final boolean f() {
        PopupWindow popupWindow = this.b;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final oc g(Context context, View view, View view2, final tc1<? super String, te4> tc1Var) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        fs1.f(tc1Var, "onItemClickListener");
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        LayoutInflater layoutInflater = (LayoutInflater) systemService;
        View inflate = layoutInflater.inflate(R.layout.dialog_anchor_select_security_questions, (ViewGroup) null);
        hn0 a = hn0.a(inflate);
        fs1.e(a, "bind(view)");
        a.a.removeAllViews();
        try {
            for (final String str : this.a) {
                Object[] array = StringsKt__StringsKt.w0(str, new String[]{"="}, false, 0, 6, null).toArray(new String[0]);
                if (array != null) {
                    String[] strArr = (String[]) array;
                    if (strArr.length > 1) {
                        ft1 c = ft1.c(layoutInflater, a.a, false);
                        c.b.setText(strArr[1]);
                        a.a.addView(c.b());
                        c.b().setOnClickListener(new View.OnClickListener() { // from class: mc
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view3) {
                                oc.h(tc1.this, str, this, view3);
                            }
                        });
                    } else if (strArr.length == 1) {
                        ft1 c2 = ft1.c(layoutInflater, a.a, false);
                        c2.b.setText(strArr[0]);
                        a.a.addView(c2.b());
                        c2.b().setOnClickListener(new View.OnClickListener() { // from class: nc
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view3) {
                                oc.i(tc1.this, str, this, view3);
                            }
                        });
                    }
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        } catch (Exception unused) {
        }
        fs1.e(inflate, "view");
        PopupWindow d = d(this, inflate, view2, 0, 4, null);
        this.b = d;
        if (d != null) {
            d.showAsDropDown(view);
        }
        return this;
    }
}
