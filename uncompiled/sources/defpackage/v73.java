package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: ResourceCursorAdapter.java */
/* renamed from: v73  reason: default package */
/* loaded from: classes.dex */
public abstract class v73 extends ub0 {
    public int m0;
    public int n0;
    public LayoutInflater o0;

    @Deprecated
    public v73(Context context, int i, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.n0 = i;
        this.m0 = i;
        this.o0 = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    @Override // defpackage.ub0
    public View g(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.o0.inflate(this.n0, viewGroup, false);
    }

    @Override // defpackage.ub0
    public View h(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.o0.inflate(this.m0, viewGroup, false);
    }
}
