package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: bg3  reason: default package */
/* loaded from: classes2.dex */
public class bg3 extends ct0.a {
    public long[] f;

    public bg3() {
        this.f = cd2.h();
    }

    public bg3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 131) {
            throw new IllegalArgumentException("x value invalid for SecT131FieldElement");
        }
        this.f = ag3.d(bigInteger);
    }

    public bg3(long[] jArr) {
        this.f = jArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        long[] h = cd2.h();
        ag3.a(this.f, ((bg3) ct0Var).f, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        long[] h = cd2.h();
        ag3.c(this.f, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        return j(ct0Var.g());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof bg3) {
            return cd2.m(this.f, ((bg3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return 131;
    }

    @Override // defpackage.ct0
    public ct0 g() {
        long[] h = cd2.h();
        ag3.i(this.f, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return cd2.t(this.f);
    }

    public int hashCode() {
        return wh.v(this.f, 0, 3) ^ 131832;
    }

    @Override // defpackage.ct0
    public boolean i() {
        return cd2.v(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        long[] h = cd2.h();
        ag3.j(this.f, ((bg3) ct0Var).f, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return l(ct0Var, ct0Var2, ct0Var3);
    }

    @Override // defpackage.ct0
    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        long[] jArr = this.f;
        long[] jArr2 = ((bg3) ct0Var).f;
        long[] jArr3 = ((bg3) ct0Var2).f;
        long[] jArr4 = ((bg3) ct0Var3).f;
        long[] k = kd2.k(5);
        ag3.k(jArr, jArr2, k);
        ag3.k(jArr3, jArr4, k);
        long[] h = cd2.h();
        ag3.l(k, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        return this;
    }

    @Override // defpackage.ct0
    public ct0 n() {
        long[] h = cd2.h();
        ag3.n(this.f, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 o() {
        long[] h = cd2.h();
        ag3.o(this.f, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        long[] jArr = this.f;
        long[] jArr2 = ((bg3) ct0Var).f;
        long[] jArr3 = ((bg3) ct0Var2).f;
        long[] k = kd2.k(5);
        ag3.p(jArr, k);
        ag3.k(jArr2, jArr3, k);
        long[] h = cd2.h();
        ag3.l(k, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 q(int i) {
        if (i < 1) {
            return this;
        }
        long[] h = cd2.h();
        ag3.q(this.f, i, h);
        return new bg3(h);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        return a(ct0Var);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return (this.f[0] & 1) != 0;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return cd2.I(this.f);
    }

    @Override // defpackage.ct0.a
    public int u() {
        return ag3.r(this.f);
    }
}
