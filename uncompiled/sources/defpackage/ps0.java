package defpackage;

import java.util.List;

/* compiled from: DvbSubtitle.java */
/* renamed from: ps0  reason: default package */
/* loaded from: classes.dex */
public final class ps0 implements qv3 {
    public final List<kb0> a;

    public ps0(List<kb0> list) {
        this.a = list;
    }

    @Override // defpackage.qv3
    public int a(long j) {
        return -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        return 0L;
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return this.a;
    }

    @Override // defpackage.qv3
    public int f() {
        return 1;
    }
}
