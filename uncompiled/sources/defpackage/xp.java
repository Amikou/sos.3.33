package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapFrameCache.java */
/* renamed from: xp  reason: default package */
/* loaded from: classes.dex */
public interface xp {

    /* compiled from: BitmapFrameCache.java */
    /* renamed from: xp$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(xp xpVar, int i);

        void b(xp xpVar, int i);
    }

    com.facebook.common.references.a<Bitmap> a(int i, int i2, int i3);

    void b(int i, com.facebook.common.references.a<Bitmap> aVar, int i2);

    boolean c(int i);

    void clear();

    com.facebook.common.references.a<Bitmap> d(int i);

    void e(int i, com.facebook.common.references.a<Bitmap> aVar, int i2);

    com.facebook.common.references.a<Bitmap> f(int i);
}
