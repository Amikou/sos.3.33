package defpackage;

import java.util.Arrays;

/* compiled from: BiometricErrorData.java */
/* renamed from: np  reason: default package */
/* loaded from: classes.dex */
public class np {
    public final int a;
    public final CharSequence b;

    public np(int i, CharSequence charSequence) {
        this.a = i;
        this.b = charSequence;
    }

    public static String a(CharSequence charSequence) {
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    public int b() {
        return this.a;
    }

    public CharSequence c() {
        return this.b;
    }

    public final boolean d(CharSequence charSequence) {
        String a = a(this.b);
        String a2 = a(charSequence);
        return (a == null && a2 == null) || (a != null && a.equals(a2));
    }

    public boolean equals(Object obj) {
        if (obj instanceof np) {
            np npVar = (np) obj;
            return this.a == npVar.a && d(npVar.b);
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.a), a(this.b)});
    }
}
