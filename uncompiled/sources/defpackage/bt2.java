package defpackage;

import com.facebook.common.memory.PooledByteBuffer;
import java.io.InputStream;

/* compiled from: PooledByteBufferInputStream.java */
/* renamed from: bt2  reason: default package */
/* loaded from: classes.dex */
public class bt2 extends InputStream {
    public final PooledByteBuffer a;
    public int f0;
    public int g0;

    public bt2(PooledByteBuffer pooledByteBuffer) {
        xt2.b(Boolean.valueOf(!pooledByteBuffer.isClosed()));
        this.a = (PooledByteBuffer) xt2.g(pooledByteBuffer);
        this.f0 = 0;
        this.g0 = 0;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.a.size() - this.f0;
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        this.g0 = this.f0;
    }

    @Override // java.io.InputStream
    public boolean markSupported() {
        return true;
    }

    @Override // java.io.InputStream
    public int read() {
        if (available() <= 0) {
            return -1;
        }
        PooledByteBuffer pooledByteBuffer = this.a;
        int i = this.f0;
        this.f0 = i + 1;
        return pooledByteBuffer.p(i) & 255;
    }

    @Override // java.io.InputStream
    public void reset() {
        this.f0 = this.g0;
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        xt2.b(Boolean.valueOf(j >= 0));
        int min = Math.min((int) j, available());
        this.f0 += min;
        return min;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        if (i >= 0 && i2 >= 0 && i + i2 <= bArr.length) {
            int available = available();
            if (available <= 0) {
                return -1;
            }
            if (i2 <= 0) {
                return 0;
            }
            int min = Math.min(available, i2);
            this.a.s(this.f0, bArr, i, min);
            this.f0 += min;
            return min;
        }
        throw new ArrayIndexOutOfBoundsException("length=" + bArr.length + "; regionStart=" + i + "; regionLength=" + i2);
    }
}
