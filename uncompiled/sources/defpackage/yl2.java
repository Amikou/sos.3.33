package defpackage;

import androidx.media3.common.ParserException;
import java.io.IOException;

/* compiled from: OggPageHeader.java */
/* renamed from: yl2  reason: default package */
/* loaded from: classes.dex */
public final class yl2 {
    public int a;
    public int b;
    public long c;
    public int d;
    public int e;
    public int f;
    public final int[] g = new int[255];
    public final op2 h = new op2(255);

    public boolean a(q11 q11Var, boolean z) throws IOException {
        b();
        this.h.L(27);
        if (s11.b(q11Var, this.h.d(), 0, 27, z) && this.h.F() == 1332176723) {
            int D = this.h.D();
            this.a = D;
            if (D != 0) {
                if (z) {
                    return false;
                }
                throw ParserException.createForUnsupportedContainerFeature("unsupported bit stream revision");
            }
            this.b = this.h.D();
            this.c = this.h.r();
            this.h.t();
            this.h.t();
            this.h.t();
            int D2 = this.h.D();
            this.d = D2;
            this.e = D2 + 27;
            this.h.L(D2);
            if (s11.b(q11Var, this.h.d(), 0, this.d, z)) {
                for (int i = 0; i < this.d; i++) {
                    this.g[i] = this.h.D();
                    this.f += this.g[i];
                }
                return true;
            }
            return false;
        }
        return false;
    }

    public void b() {
        this.a = 0;
        this.b = 0;
        this.c = 0L;
        this.d = 0;
        this.e = 0;
        this.f = 0;
    }

    public boolean c(q11 q11Var) throws IOException {
        return d(q11Var, -1L);
    }

    public boolean d(q11 q11Var, long j) throws IOException {
        int i;
        ii.a(q11Var.getPosition() == q11Var.e());
        this.h.L(4);
        while (true) {
            i = (j > (-1L) ? 1 : (j == (-1L) ? 0 : -1));
            if ((i == 0 || q11Var.getPosition() + 4 < j) && s11.b(q11Var, this.h.d(), 0, 4, true)) {
                this.h.P(0);
                if (this.h.F() == 1332176723) {
                    q11Var.j();
                    return true;
                }
                q11Var.k(1);
            }
        }
        do {
            if (i != 0 && q11Var.getPosition() >= j) {
                break;
            }
        } while (q11Var.g(1) != -1);
        return false;
    }
}
