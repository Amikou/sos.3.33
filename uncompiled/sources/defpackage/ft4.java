package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.play.core.assetpacks.a;
import com.google.android.play.core.internal.p;

/* renamed from: ft4  reason: default package */
/* loaded from: classes2.dex */
public final class ft4 extends jt4 {
    public final /* synthetic */ int f0;
    public final /* synthetic */ tx4 g0;
    public final /* synthetic */ st4 h0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ft4(st4 st4Var, tx4 tx4Var, int i, tx4 tx4Var2) {
        super(tx4Var);
        this.h0 = st4Var;
        this.f0 = i;
        this.g0 = tx4Var2;
    }

    @Override // defpackage.jt4
    public final void a() {
        it4 it4Var;
        zt4 zt4Var;
        String str;
        Bundle i;
        Bundle j;
        try {
            zt4Var = this.h0.c;
            str = this.h0.a;
            i = st4.i(this.f0);
            j = st4.j();
            ((p) zt4Var.c()).A(str, i, j, new a(this.h0, this.g0, (int[]) null));
        } catch (RemoteException e) {
            it4Var = st4.f;
            it4Var.c(e, "notifySessionFailed", new Object[0]);
        }
    }
}
