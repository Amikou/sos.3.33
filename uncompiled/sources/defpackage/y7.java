package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import net.safemoon.androidwallet.R;

/* compiled from: ActivitySetPasswordBinding.java */
/* renamed from: y7  reason: default package */
/* loaded from: classes2.dex */
public final class y7 {
    public final ConstraintLayout a;
    public final AppCompatButton b;
    public final EditText c;
    public final EditText d;
    public final Group e;
    public final ImageView f;
    public final ImageView g;
    public final ImageView h;
    public final ImageView i;
    public final TextView j;
    public final TextView k;
    public final TextView l;
    public final TextView m;
    public final TextView n;

    public y7(ConstraintLayout constraintLayout, AppCompatButton appCompatButton, EditText editText, EditText editText2, Group group, ImageView imageView, AppCompatImageView appCompatImageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, ImageView imageView5, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, TextView textView9) {
        this.a = constraintLayout;
        this.b = appCompatButton;
        this.c = editText;
        this.d = editText2;
        this.e = group;
        this.f = imageView2;
        this.g = imageView3;
        this.h = imageView4;
        this.i = imageView5;
        this.j = textView;
        this.k = textView6;
        this.l = textView7;
        this.m = textView8;
        this.n = textView9;
    }

    public static y7 a(View view) {
        int i = R.id.btnConfirm;
        AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btnConfirm);
        if (appCompatButton != null) {
            i = R.id.et_confirm;
            EditText editText = (EditText) ai4.a(view, R.id.et_confirm);
            if (editText != null) {
                i = R.id.et_enter;
                EditText editText2 = (EditText) ai4.a(view, R.id.et_enter);
                if (editText2 != null) {
                    i = R.id.gp_require;
                    Group group = (Group) ai4.a(view, R.id.gp_require);
                    if (group != null) {
                        i = R.id.imageView15;
                        ImageView imageView = (ImageView) ai4.a(view, R.id.imageView15);
                        if (imageView != null) {
                            i = R.id.img_logo;
                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                            if (appCompatImageView != null) {
                                i = R.id.iv_1;
                                ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv_1);
                                if (imageView2 != null) {
                                    i = R.id.iv_2;
                                    ImageView imageView3 = (ImageView) ai4.a(view, R.id.iv_2);
                                    if (imageView3 != null) {
                                        i = R.id.iv_3;
                                        ImageView imageView4 = (ImageView) ai4.a(view, R.id.iv_3);
                                        if (imageView4 != null) {
                                            i = R.id.iv_4;
                                            ImageView imageView5 = (ImageView) ai4.a(view, R.id.iv_4);
                                            if (imageView5 != null) {
                                                i = R.id.min_char;
                                                TextView textView = (TextView) ai4.a(view, R.id.min_char);
                                                if (textView != null) {
                                                    i = R.id.textView;
                                                    TextView textView2 = (TextView) ai4.a(view, R.id.textView);
                                                    if (textView2 != null) {
                                                        i = R.id.textView1;
                                                        TextView textView3 = (TextView) ai4.a(view, R.id.textView1);
                                                        if (textView3 != null) {
                                                            i = R.id.textView2;
                                                            TextView textView4 = (TextView) ai4.a(view, R.id.textView2);
                                                            if (textView4 != null) {
                                                                i = R.id.textView21;
                                                                TextView textView5 = (TextView) ai4.a(view, R.id.textView21);
                                                                if (textView5 != null) {
                                                                    i = R.id.tv_cap;
                                                                    TextView textView6 = (TextView) ai4.a(view, R.id.tv_cap);
                                                                    if (textView6 != null) {
                                                                        i = R.id.tv_not;
                                                                        TextView textView7 = (TextView) ai4.a(view, R.id.tv_not);
                                                                        if (textView7 != null) {
                                                                            i = R.id.tv_number;
                                                                            TextView textView8 = (TextView) ai4.a(view, R.id.tv_number);
                                                                            if (textView8 != null) {
                                                                                i = R.id.tv_special;
                                                                                TextView textView9 = (TextView) ai4.a(view, R.id.tv_special);
                                                                                if (textView9 != null) {
                                                                                    return new y7((ConstraintLayout) view, appCompatButton, editText, editText2, group, imageView, appCompatImageView, imageView2, imageView3, imageView4, imageView5, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static y7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static y7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_set_password, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
