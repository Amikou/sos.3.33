package defpackage;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: mz5  reason: default package */
/* loaded from: classes.dex */
public final class mz5 implements Iterator<String> {
    public final Iterator<String> a;
    public final /* synthetic */ nz5 f0;

    public mz5(nz5 nz5Var) {
        nw5 nw5Var;
        this.f0 = nz5Var;
        nw5Var = nz5Var.a;
        this.a = nw5Var.iterator();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.Iterator
    public final /* bridge */ /* synthetic */ String next() {
        return this.a.next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
