package defpackage;

import android.util.SparseArray;
import androidx.media3.common.ParserException;
import java.util.Collections;
import java.util.List;

/* compiled from: TsPayloadReader.java */
/* renamed from: gc4  reason: default package */
/* loaded from: classes.dex */
public interface gc4 {

    /* compiled from: TsPayloadReader.java */
    /* renamed from: gc4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final String a;
        public final byte[] b;

        public a(String str, int i, byte[] bArr) {
            this.a = str;
            this.b = bArr;
        }
    }

    /* compiled from: TsPayloadReader.java */
    /* renamed from: gc4$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final String b;
        public final List<a> c;
        public final byte[] d;

        public b(int i, String str, List<a> list, byte[] bArr) {
            List<a> unmodifiableList;
            this.a = i;
            this.b = str;
            if (list == null) {
                unmodifiableList = Collections.emptyList();
            } else {
                unmodifiableList = Collections.unmodifiableList(list);
            }
            this.c = unmodifiableList;
            this.d = bArr;
        }
    }

    /* compiled from: TsPayloadReader.java */
    /* renamed from: gc4$c */
    /* loaded from: classes.dex */
    public interface c {
        SparseArray<gc4> a();

        gc4 b(int i, b bVar);
    }

    /* compiled from: TsPayloadReader.java */
    /* renamed from: gc4$d */
    /* loaded from: classes.dex */
    public static final class d {
        public final String a;
        public final int b;
        public final int c;
        public int d;
        public String e;

        public d(int i, int i2) {
            this(Integer.MIN_VALUE, i, i2);
        }

        public void a() {
            int i = this.d;
            this.d = i == Integer.MIN_VALUE ? this.b : i + this.c;
            this.e = this.a + this.d;
        }

        public String b() {
            d();
            return this.e;
        }

        public int c() {
            d();
            return this.d;
        }

        public final void d() {
            if (this.d == Integer.MIN_VALUE) {
                throw new IllegalStateException("generateNewId() must be called before retrieving ids.");
            }
        }

        public d(int i, int i2, int i3) {
            String str;
            if (i != Integer.MIN_VALUE) {
                str = i + "/";
            } else {
                str = "";
            }
            this.a = str;
            this.b = i2;
            this.c = i3;
            this.d = Integer.MIN_VALUE;
            this.e = "";
        }
    }

    void a(op2 op2Var, int i) throws ParserException;

    void b(h64 h64Var, r11 r11Var, d dVar);

    void c();
}
