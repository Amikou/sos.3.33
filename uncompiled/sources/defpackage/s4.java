package defpackage;

import android.util.Pair;
import androidx.media3.common.u;

/* compiled from: AbstractConcatenatedTimeline.java */
/* renamed from: s4  reason: default package */
/* loaded from: classes.dex */
public abstract class s4 extends u {
    public final int f0;
    public final qo3 g0;
    public final boolean h0;

    public s4(boolean z, qo3 qo3Var) {
        this.h0 = z;
        this.g0 = qo3Var;
        this.f0 = qo3Var.getLength();
    }

    public static Object A(Object obj, Object obj2) {
        return Pair.create(obj, obj2);
    }

    public static Object x(Object obj) {
        return ((Pair) obj).second;
    }

    public static Object y(Object obj) {
        return ((Pair) obj).first;
    }

    public abstract int B(int i);

    public abstract int C(int i);

    public final int D(int i, boolean z) {
        if (z) {
            return this.g0.d(i);
        }
        if (i < this.f0 - 1) {
            return i + 1;
        }
        return -1;
    }

    public final int E(int i, boolean z) {
        if (z) {
            return this.g0.c(i);
        }
        if (i > 0) {
            return i - 1;
        }
        return -1;
    }

    public abstract u F(int i);

    @Override // androidx.media3.common.u
    public int a(boolean z) {
        if (this.f0 == 0) {
            return -1;
        }
        if (this.h0) {
            z = false;
        }
        int b = z ? this.g0.b() : 0;
        while (F(b).q()) {
            b = D(b, z);
            if (b == -1) {
                return -1;
            }
        }
        return C(b) + F(b).a(z);
    }

    @Override // androidx.media3.common.u
    public final int b(Object obj) {
        int b;
        if (obj instanceof Pair) {
            Object y = y(obj);
            Object x = x(obj);
            int u = u(y);
            if (u == -1 || (b = F(u).b(x)) == -1) {
                return -1;
            }
            return B(u) + b;
        }
        return -1;
    }

    @Override // androidx.media3.common.u
    public int c(boolean z) {
        int i = this.f0;
        if (i == 0) {
            return -1;
        }
        if (this.h0) {
            z = false;
        }
        int f = z ? this.g0.f() : i - 1;
        while (F(f).q()) {
            f = E(f, z);
            if (f == -1) {
                return -1;
            }
        }
        return C(f) + F(f).c(z);
    }

    @Override // androidx.media3.common.u
    public int e(int i, int i2, boolean z) {
        if (this.h0) {
            if (i2 == 1) {
                i2 = 2;
            }
            z = false;
        }
        int w = w(i);
        int C = C(w);
        int e = F(w).e(i - C, i2 != 2 ? i2 : 0, z);
        if (e != -1) {
            return C + e;
        }
        int D = D(w, z);
        while (D != -1 && F(D).q()) {
            D = D(D, z);
        }
        if (D != -1) {
            return C(D) + F(D).a(z);
        }
        if (i2 == 2) {
            return a(z);
        }
        return -1;
    }

    @Override // androidx.media3.common.u
    public final u.b g(int i, u.b bVar, boolean z) {
        int v = v(i);
        int C = C(v);
        F(v).g(i - B(v), bVar, z);
        bVar.g0 += C;
        if (z) {
            bVar.f0 = A(z(v), ii.e(bVar.f0));
        }
        return bVar;
    }

    @Override // androidx.media3.common.u
    public final u.b h(Object obj, u.b bVar) {
        Object y = y(obj);
        Object x = x(obj);
        int u = u(y);
        int C = C(u);
        F(u).h(x, bVar);
        bVar.g0 += C;
        bVar.f0 = obj;
        return bVar;
    }

    @Override // androidx.media3.common.u
    public int l(int i, int i2, boolean z) {
        if (this.h0) {
            if (i2 == 1) {
                i2 = 2;
            }
            z = false;
        }
        int w = w(i);
        int C = C(w);
        int l = F(w).l(i - C, i2 != 2 ? i2 : 0, z);
        if (l != -1) {
            return C + l;
        }
        int E = E(w, z);
        while (E != -1 && F(E).q()) {
            E = E(E, z);
        }
        if (E != -1) {
            return C(E) + F(E).c(z);
        }
        if (i2 == 2) {
            return c(z);
        }
        return -1;
    }

    @Override // androidx.media3.common.u
    public final Object m(int i) {
        int v = v(i);
        return A(z(v), F(v).m(i - B(v)));
    }

    @Override // androidx.media3.common.u
    public final u.c o(int i, u.c cVar, long j) {
        int w = w(i);
        int C = C(w);
        int B = B(w);
        F(w).o(i - C, cVar, j);
        Object z = z(w);
        if (!u.c.v0.equals(cVar.a)) {
            z = A(z, cVar.a);
        }
        cVar.a = z;
        cVar.s0 += B;
        cVar.t0 += B;
        return cVar;
    }

    public abstract int u(Object obj);

    public abstract int v(int i);

    public abstract int w(int i);

    public abstract Object z(int i);
}
