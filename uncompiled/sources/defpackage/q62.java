package defpackage;

import android.text.TextUtils;

/* compiled from: MediaSessionManagerImplBase.java */
/* renamed from: q62  reason: default package */
/* loaded from: classes.dex */
public class q62 implements o62 {
    public String a;
    public int b;
    public int c;

    public q62(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof q62) {
            q62 q62Var = (q62) obj;
            return (this.b < 0 || q62Var.b < 0) ? TextUtils.equals(this.a, q62Var.a) && this.c == q62Var.c : TextUtils.equals(this.a, q62Var.a) && this.b == q62Var.b && this.c == q62Var.c;
        }
        return false;
    }

    public int hashCode() {
        return sl2.b(this.a, Integer.valueOf(this.c));
    }
}
