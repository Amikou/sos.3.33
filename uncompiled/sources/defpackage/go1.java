package defpackage;

/* compiled from: ImageOriginRequestListener.java */
/* renamed from: go1  reason: default package */
/* loaded from: classes.dex */
public class go1 extends xn {
    public String a;
    public final fo1 b;

    public go1(String str, fo1 fo1Var) {
        this.b = fo1Var;
        l(str);
    }

    @Override // defpackage.xn, defpackage.jv2
    public void e(String str, String str2, boolean z) {
        fo1 fo1Var = this.b;
        if (fo1Var != null) {
            fo1Var.a(this.a, ho1.a(str2), z, str2);
        }
    }

    public void l(String str) {
        this.a = str;
    }
}
