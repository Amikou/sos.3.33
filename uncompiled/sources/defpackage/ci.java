package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import com.facebook.common.references.a;

/* compiled from: ArtBitmapFactory.java */
@TargetApi(21)
/* renamed from: ci  reason: default package */
/* loaded from: classes.dex */
public class ci extends br2 {
    public final iq a;
    public final a00 b;

    public ci(iq iqVar, a00 a00Var) {
        this.a = iqVar;
        this.b = a00Var;
    }

    @Override // defpackage.br2
    public a<Bitmap> c(int i, int i2, Bitmap.Config config) {
        Bitmap bitmap = this.a.get(rq.d(i, i2, config));
        xt2.b(Boolean.valueOf(bitmap.getAllocationByteCount() >= (i * i2) * rq.c(config)));
        bitmap.reconfigure(i, i2, config);
        return this.b.c(bitmap, this.a);
    }
}
