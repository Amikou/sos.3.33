package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import java.util.Arrays;

/* compiled from: AlertDialogPrepromptForAndroidSettings.kt */
/* renamed from: ua  reason: default package */
/* loaded from: classes2.dex */
public final class ua {
    public static final ua a = new ua();

    /* compiled from: AlertDialogPrepromptForAndroidSettings.kt */
    /* renamed from: ua$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a();

        void b();
    }

    /* compiled from: AlertDialogPrepromptForAndroidSettings.kt */
    /* renamed from: ua$b */
    /* loaded from: classes2.dex */
    public static final class b implements DialogInterface.OnClickListener {
        public final /* synthetic */ a a;

        public b(a aVar) {
            this.a = aVar;
        }

        @Override // android.content.DialogInterface.OnClickListener
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.a.a();
        }
    }

    /* compiled from: AlertDialogPrepromptForAndroidSettings.kt */
    /* renamed from: ua$c */
    /* loaded from: classes2.dex */
    public static final class c implements DialogInterface.OnClickListener {
        public final /* synthetic */ a a;

        public c(a aVar) {
            this.a = aVar;
        }

        @Override // android.content.DialogInterface.OnClickListener
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.a.b();
        }
    }

    public final void a(Activity activity, String str, String str2, a aVar) {
        fs1.f(activity, "activity");
        fs1.f(str, "titlePrefix");
        fs1.f(str2, "previouslyDeniedPostfix");
        fs1.f(aVar, "callback");
        String string = activity.getString(s13.permission_not_available_title);
        fs1.e(string, "activity.getString(R.str…sion_not_available_title)");
        String format = String.format(string, Arrays.copyOf(new Object[]{str}, 1));
        fs1.e(format, "java.lang.String.format(this, *args)");
        String string2 = activity.getString(s13.permission_not_available_message);
        fs1.e(string2, "activity.getString(R.str…on_not_available_message)");
        String format2 = String.format(string2, Arrays.copyOf(new Object[]{str2}, 1));
        fs1.e(format2, "java.lang.String.format(this, *args)");
        new AlertDialog.Builder(activity).setTitle(format).setMessage(format2).setPositiveButton(s13.permission_not_available_open_settings_option, new b(aVar)).setNegativeButton(17039369, new c(aVar)).show();
    }
}
