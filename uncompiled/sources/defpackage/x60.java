package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ContactListAddressAdapter.kt */
/* renamed from: x60  reason: default package */
/* loaded from: classes2.dex */
public final class x60 extends RecyclerView.Adapter<a> {
    public final List<IContact> a;
    public final sl1 b;
    public final List<IContact> c;
    public xs1 d;

    /* compiled from: ContactListAddressAdapter.kt */
    /* renamed from: x60$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final ImageView a;
        public final TextView b;
        public final TextView c;
        public final View d;
        public final LinearLayout e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(x60 x60Var, xs1 xs1Var) {
            super(xs1Var.b());
            fs1.f(x60Var, "this$0");
            fs1.f(xs1Var, "binding");
            ImageView imageView = xs1Var.c;
            fs1.e(imageView, "binding.ivContactIcon");
            this.a = imageView;
            TextView textView = xs1Var.e;
            fs1.e(textView, "binding.tvContactName");
            this.b = textView;
            TextView textView2 = xs1Var.d;
            fs1.e(textView2, "binding.tvContactAddress");
            this.c = textView2;
            View view = xs1Var.f;
            fs1.e(view, "binding.vDivider");
            this.d = view;
            LinearLayout linearLayout = xs1Var.b;
            fs1.e(linearLayout, "binding.container");
            this.e = linearLayout;
        }

        public final TextView a() {
            return this.c;
        }

        public final LinearLayout b() {
            return this.e;
        }

        public final View c() {
            return this.d;
        }

        public final ImageView d() {
            return this.a;
        }

        public final TextView e() {
            return this.b;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public x60(List<? extends IContact> list, sl1 sl1Var) {
        fs1.f(list, "mainItem");
        fs1.f(sl1Var, "clickListener");
        this.a = list;
        this.b = sl1Var;
        ArrayList arrayList = new ArrayList();
        this.c = arrayList;
        arrayList.addAll(list);
    }

    public static final void c(x60 x60Var, IContact iContact, View view) {
        fs1.f(x60Var, "this$0");
        fs1.f(iContact, "$model");
        x60Var.b.a(iContact);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        final IContact iContact = this.c.get(i);
        aVar.e().setText(iContact.getName());
        aVar.a().setText(iContact.getAddress());
        aVar.c().setVisibility(i != this.c.size() - 1 ? 0 : 8);
        com.bumptech.glide.a.u(aVar.d()).y(iContact.getProfilePath()).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).a(n73.v0()).d0(250, 250).I0(aVar.d());
        aVar.b().setOnClickListener(new View.OnClickListener() { // from class: w60
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                x60.c(x60.this, iContact, view);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        xs1 a2 = xs1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_drop_down_contact_address, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        this.d = a2;
        xs1 xs1Var = this.d;
        if (xs1Var == null) {
            fs1.r("binding");
            xs1Var = null;
        }
        return new a(this, xs1Var);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.c.size();
    }
}
