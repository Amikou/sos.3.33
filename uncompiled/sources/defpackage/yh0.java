package defpackage;

import android.os.Looper;
import android.util.SparseArray;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.i;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.util.b;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.source.j;
import androidx.recyclerview.widget.RecyclerView;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import defpackage.o02;
import defpackage.tb;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import okhttp3.internal.ws.WebSocketProtocol;

/* compiled from: DefaultAnalyticsCollector.java */
/* renamed from: yh0  reason: default package */
/* loaded from: classes.dex */
public class yh0 implements jb {
    public final tz a;
    public final u.b f0;
    public final u.c g0;
    public final a h0;
    public final SparseArray<tb.a> i0;
    public o02<tb> j0;
    public q k0;
    public pj1 l0;
    public boolean m0;

    /* compiled from: DefaultAnalyticsCollector.java */
    /* renamed from: yh0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final u.b a;
        public ImmutableList<j.b> b = ImmutableList.of();
        public ImmutableMap<j.b, u> c = ImmutableMap.of();
        public j.b d;
        public j.b e;
        public j.b f;

        public a(u.b bVar) {
            this.a = bVar;
        }

        public static j.b c(q qVar, ImmutableList<j.b> immutableList, j.b bVar, u.b bVar2) {
            u S = qVar.S();
            int n = qVar.n();
            Object m = S.q() ? null : S.m(n);
            int f = (qVar.g() || S.q()) ? -1 : S.f(n, bVar2).f(b.y0(qVar.c0()) - bVar2.p());
            for (int i = 0; i < immutableList.size(); i++) {
                j.b bVar3 = immutableList.get(i);
                if (i(bVar3, m, qVar.g(), qVar.H(), qVar.s(), f)) {
                    return bVar3;
                }
            }
            if (immutableList.isEmpty() && bVar != null) {
                if (i(bVar, m, qVar.g(), qVar.H(), qVar.s(), f)) {
                    return bVar;
                }
            }
            return null;
        }

        public static boolean i(j.b bVar, Object obj, boolean z, int i, int i2, int i3) {
            if (bVar.a.equals(obj)) {
                return (z && bVar.b == i && bVar.c == i2) || (!z && bVar.b == -1 && bVar.e == i3);
            }
            return false;
        }

        public final void b(ImmutableMap.b<j.b, u> bVar, j.b bVar2, u uVar) {
            if (bVar2 == null) {
                return;
            }
            if (uVar.b(bVar2.a) != -1) {
                bVar.d(bVar2, uVar);
                return;
            }
            u uVar2 = this.c.get(bVar2);
            if (uVar2 != null) {
                bVar.d(bVar2, uVar2);
            }
        }

        public j.b d() {
            return this.d;
        }

        public j.b e() {
            if (this.b.isEmpty()) {
                return null;
            }
            return (j.b) lt1.d(this.b);
        }

        public u f(j.b bVar) {
            return this.c.get(bVar);
        }

        public j.b g() {
            return this.e;
        }

        public j.b h() {
            return this.f;
        }

        public void j(q qVar) {
            this.d = c(qVar, this.b, this.e, this.a);
        }

        public void k(List<j.b> list, j.b bVar, q qVar) {
            this.b = ImmutableList.copyOf((Collection) list);
            if (!list.isEmpty()) {
                this.e = list.get(0);
                this.f = (j.b) ii.e(bVar);
            }
            if (this.d == null) {
                this.d = c(qVar, this.b, this.e, this.a);
            }
            m(qVar.S());
        }

        public void l(q qVar) {
            this.d = c(qVar, this.b, this.e, this.a);
            m(qVar.S());
        }

        public final void m(u uVar) {
            ImmutableMap.b<j.b, u> builder = ImmutableMap.builder();
            if (this.b.isEmpty()) {
                b(builder, this.e, uVar);
                if (!ql2.a(this.f, this.e)) {
                    b(builder, this.f, uVar);
                }
                if (!ql2.a(this.d, this.e) && !ql2.a(this.d, this.f)) {
                    b(builder, this.d, uVar);
                }
            } else {
                for (int i = 0; i < this.b.size(); i++) {
                    b(builder, this.b.get(i), uVar);
                }
                if (!this.b.contains(this.d)) {
                    b(builder, this.d, uVar);
                }
            }
            this.c = builder.b();
        }
    }

    public yh0(tz tzVar) {
        this.a = (tz) ii.e(tzVar);
        this.j0 = new o02<>(b.M(), tzVar, sh0.a);
        u.b bVar = new u.b();
        this.f0 = bVar;
        this.g0 = new u.c();
        this.h0 = new a(bVar);
        this.i0 = new SparseArray<>();
    }

    public static /* synthetic */ void J1(tb tbVar, i iVar) {
    }

    public static /* synthetic */ void K2(tb.a aVar, String str, long j, long j2, tb tbVar) {
        tbVar.v0(aVar, str, j);
        tbVar.L(aVar, str, j2, j);
        tbVar.Q(aVar, 2, str, j);
    }

    public static /* synthetic */ void M1(tb.a aVar, String str, long j, long j2, tb tbVar) {
        tbVar.U(aVar, str, j);
        tbVar.C(aVar, str, j2, j);
        tbVar.Q(aVar, 1, str, j);
    }

    public static /* synthetic */ void M2(tb.a aVar, hf0 hf0Var, tb tbVar) {
        tbVar.e0(aVar, hf0Var);
        tbVar.i(aVar, 2, hf0Var);
    }

    public static /* synthetic */ void N2(tb.a aVar, hf0 hf0Var, tb tbVar) {
        tbVar.W(aVar, hf0Var);
        tbVar.i0(aVar, 2, hf0Var);
    }

    public static /* synthetic */ void O1(tb.a aVar, hf0 hf0Var, tb tbVar) {
        tbVar.q0(aVar, hf0Var);
        tbVar.i(aVar, 1, hf0Var);
    }

    public static /* synthetic */ void P1(tb.a aVar, hf0 hf0Var, tb tbVar) {
        tbVar.N(aVar, hf0Var);
        tbVar.i0(aVar, 1, hf0Var);
    }

    public static /* synthetic */ void P2(tb.a aVar, androidx.media3.common.j jVar, jf0 jf0Var, tb tbVar) {
        tbVar.Y(aVar, jVar);
        tbVar.y(aVar, jVar, jf0Var);
        tbVar.a0(aVar, 2, jVar);
    }

    public static /* synthetic */ void Q1(tb.a aVar, androidx.media3.common.j jVar, jf0 jf0Var, tb tbVar) {
        tbVar.p0(aVar, jVar);
        tbVar.s(aVar, jVar, jf0Var);
        tbVar.a0(aVar, 1, jVar);
    }

    public static /* synthetic */ void Q2(tb.a aVar, z zVar, tb tbVar) {
        tbVar.y0(aVar, zVar);
        tbVar.M(aVar, zVar.a, zVar.f0, zVar.g0, zVar.h0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void S2(q qVar, tb tbVar, i iVar) {
        tbVar.R(qVar, new tb.b(iVar, this.i0));
    }

    public static /* synthetic */ void e2(tb.a aVar, int i, tb tbVar) {
        tbVar.t(aVar);
        tbVar.X(aVar, i);
    }

    public static /* synthetic */ void i2(tb.a aVar, boolean z, tb tbVar) {
        tbVar.t0(aVar, z);
        tbVar.n0(aVar, z);
    }

    public static /* synthetic */ void y2(tb.a aVar, int i, q.e eVar, q.e eVar2, tb tbVar) {
        tbVar.r0(aVar, i);
        tbVar.u0(aVar, eVar, eVar2, i);
    }

    @Override // defpackage.gm.a
    public final void A(final int i, final long j, final long j2) {
        final tb.a E1 = E1();
        U2(E1, 1006, new o02.a() { // from class: sf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).d0(tb.a.this, i, j, j2);
            }
        });
    }

    @Override // defpackage.jb
    public final void B(final hf0 hf0Var) {
        final tb.a H1 = H1();
        U2(H1, 1007, new o02.a() { // from class: cg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.P1(tb.a.this, hf0Var, (tb) obj);
            }
        });
    }

    public final tb.a B1() {
        return D1(this.h0.d());
    }

    @Override // defpackage.jb
    public final void C(final long j, final int i) {
        final tb.a G1 = G1();
        U2(G1, 1021, new o02.a() { // from class: xf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).p(tb.a.this, j, i);
            }
        });
    }

    public final tb.a C1(u uVar, int i, j.b bVar) {
        long y;
        j.b bVar2 = uVar.q() ? null : bVar;
        long b = this.a.b();
        boolean z = true;
        boolean z2 = uVar.equals(this.k0.S()) && i == this.k0.I();
        long j = 0;
        if (bVar2 != null && bVar2.b()) {
            if (!z2 || this.k0.H() != bVar2.b || this.k0.s() != bVar2.c) {
                z = false;
            }
            if (z) {
                j = this.k0.c0();
            }
        } else if (z2) {
            y = this.k0.y();
            return new tb.a(b, uVar, i, bVar2, y, this.k0.S(), this.k0.I(), this.h0.d(), this.k0.c0(), this.k0.h());
        } else if (!uVar.q()) {
            j = uVar.n(i, this.g0).e();
        }
        y = j;
        return new tb.a(b, uVar, i, bVar2, y, this.k0.S(), this.k0.I(), this.h0.d(), this.k0.c0(), this.k0.h());
    }

    @Override // androidx.media3.common.q.d
    public final void D(final int i) {
        final tb.a B1 = B1();
        U2(B1, 6, new o02.a() { // from class: xh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).w0(tb.a.this, i);
            }
        });
    }

    public final tb.a D1(j.b bVar) {
        ii.e(this.k0);
        u f = bVar == null ? null : this.h0.f(bVar);
        if (bVar != null && f != null) {
            return C1(f, f.h(bVar.a, this.f0).g0, bVar);
        }
        int I = this.k0.I();
        u S = this.k0.S();
        if (!(I < S.p())) {
            S = u.a;
        }
        return C1(S, I, null);
    }

    @Override // androidx.media3.common.q.d
    public void E(boolean z) {
    }

    public final tb.a E1() {
        return D1(this.h0.e());
    }

    @Override // androidx.media3.common.q.d
    public void F(int i) {
    }

    public final tb.a F1(int i, j.b bVar) {
        ii.e(this.k0);
        if (bVar != null) {
            if (this.h0.f(bVar) != null) {
                return D1(bVar);
            }
            return C1(u.a, i, bVar);
        }
        u S = this.k0.S();
        if (!(i < S.p())) {
            S = u.a;
        }
        return C1(S, i, null);
    }

    @Override // defpackage.jb
    public final void G(List<j.b> list, j.b bVar) {
        this.h0.k(list, bVar, (q) ii.e(this.k0));
    }

    public final tb.a G1() {
        return D1(this.h0.g());
    }

    @Override // androidx.media3.common.q.d
    public final void H(final boolean z) {
        final tb.a B1 = B1();
        U2(B1, 3, new o02.a() { // from class: kh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.i2(tb.a.this, z, (tb) obj);
            }
        });
    }

    public final tb.a H1() {
        return D1(this.h0.h());
    }

    @Override // androidx.media3.common.q.d
    public final void I() {
        final tb.a B1 = B1();
        U2(B1, -1, new o02.a() { // from class: yf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).P(tb.a.this);
            }
        });
    }

    public final tb.a I1(PlaybackException playbackException) {
        j62 j62Var;
        if ((playbackException instanceof ExoPlaybackException) && (j62Var = ((ExoPlaybackException) playbackException).mediaPeriodId) != null) {
            return D1(new j.b(j62Var));
        }
        return B1();
    }

    @Override // androidx.media3.common.q.d
    public void J(q qVar, q.c cVar) {
    }

    @Override // androidx.media3.exoplayer.drm.b
    public final void K(int i, j.b bVar) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1025, new o02.a() { // from class: qh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).x0(tb.a.this);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void L(u uVar, final int i) {
        this.h0.l((q) ii.e(this.k0));
        final tb.a B1 = B1();
        U2(B1, 0, new o02.a() { // from class: vh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).K(tb.a.this, i);
            }
        });
    }

    @Override // defpackage.jb
    public final void M() {
        if (this.m0) {
            return;
        }
        final tb.a B1 = B1();
        this.m0 = true;
        U2(B1, -1, new o02.a() { // from class: uh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).v(tb.a.this);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void N(final boolean z) {
        final tb.a B1 = B1();
        U2(B1, 9, new o02.a() { // from class: mh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).b(tb.a.this, z);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.k
    public final void O(int i, j.b bVar, final u02 u02Var, final g62 g62Var) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, PlaybackException.ERROR_CODE_BEHIND_LIVE_WINDOW, new o02.a() { // from class: fg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).m0(tb.a.this, u02Var, g62Var);
            }
        });
    }

    @Override // defpackage.jb
    public void P(tb tbVar) {
        ii.e(tbVar);
        this.j0.c(tbVar);
    }

    @Override // androidx.media3.common.q.d
    public void Q(final int i, final boolean z) {
        final tb.a B1 = B1();
        U2(B1, 30, new o02.a() { // from class: vf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).g0(tb.a.this, i, z);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void R(final boolean z, final int i) {
        final tb.a B1 = B1();
        U2(B1, -1, new o02.a() { // from class: oh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).O(tb.a.this, z, i);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.k
    public final void S(int i, j.b bVar, final g62 g62Var) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, PlaybackException.ERROR_CODE_FAILED_RUNTIME_CHECK, new o02.a() { // from class: kg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).f0(tb.a.this, g62Var);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void T(final n nVar) {
        final tb.a B1 = B1();
        U2(B1, 14, new o02.a() { // from class: pg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).J(tb.a.this, nVar);
            }
        });
    }

    public final void T2() {
        final tb.a B1 = B1();
        U2(B1, 1028, new o02.a() { // from class: ug0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).x(tb.a.this);
            }
        });
        this.j0.j();
    }

    @Override // androidx.media3.exoplayer.drm.b
    public final void U(int i, j.b bVar) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1023, new o02.a() { // from class: jg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).c(tb.a.this);
            }
        });
    }

    public final void U2(tb.a aVar, int i, o02.a<tb> aVar2) {
        this.i0.put(i, aVar);
        this.j0.l(i, aVar2);
    }

    @Override // androidx.media3.common.q.d
    public void V(final x xVar) {
        final tb.a B1 = B1();
        U2(B1, 19, new o02.a() { // from class: wg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).G(tb.a.this, xVar);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.k
    public final void W(int i, j.b bVar, final g62 g62Var) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, WebSocketProtocol.CLOSE_NO_STATUS_CODE, new o02.a() { // from class: ig0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).A(tb.a.this, g62Var);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void X() {
    }

    @Override // androidx.media3.common.q.d
    public void Y(final y yVar) {
        final tb.a B1 = B1();
        U2(B1, 2, new o02.a() { // from class: xg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).H(tb.a.this, yVar);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void Z(final h hVar) {
        final tb.a B1 = B1();
        U2(B1, 29, new o02.a() { // from class: lg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).z(tb.a.this, hVar);
            }
        });
    }

    @Override // defpackage.jb
    public void a() {
        ((pj1) ii.i(this.l0)).c(new Runnable() { // from class: th0
            @Override // java.lang.Runnable
            public final void run() {
                yh0.this.T2();
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void a0(final m mVar, final int i) {
        final tb.a B1 = B1();
        U2(B1, 1, new o02.a() { // from class: og0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).E(tb.a.this, mVar, i);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void b(final boolean z) {
        final tb.a H1 = H1();
        U2(H1, 23, new o02.a() { // from class: lh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).m(tb.a.this, z);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.k
    public final void b0(int i, j.b bVar, final u02 u02Var, final g62 g62Var, final IOException iOException, final boolean z) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, PlaybackException.ERROR_CODE_TIMEOUT, new o02.a() { // from class: hg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).o(tb.a.this, u02Var, g62Var, iOException, z);
            }
        });
    }

    @Override // defpackage.jb
    public final void c(final Exception exc) {
        final tb.a H1 = H1();
        U2(H1, 1014, new o02.a() { // from class: bh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).h(tb.a.this, exc);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void c0(final PlaybackException playbackException) {
        final tb.a I1 = I1(playbackException);
        U2(I1, 10, new o02.a() { // from class: rg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).h0(tb.a.this, playbackException);
            }
        });
    }

    @Override // defpackage.jb
    public final void d(final androidx.media3.common.j jVar, final jf0 jf0Var) {
        final tb.a H1 = H1();
        U2(H1, 1017, new o02.a() { // from class: mg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.P2(tb.a.this, jVar, jf0Var, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.exoplayer.drm.b
    public final void d0(int i, j.b bVar) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1027, new o02.a() { // from class: fh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).j0(tb.a.this);
            }
        });
    }

    @Override // defpackage.jb
    public final void e(final String str) {
        final tb.a H1 = H1();
        U2(H1, 1019, new o02.a() { // from class: eh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).T(tb.a.this, str);
            }
        });
    }

    @Override // defpackage.jb
    public void e0(final q qVar, Looper looper) {
        ii.g(this.k0 == null || this.h0.b.isEmpty());
        this.k0 = (q) ii.e(qVar);
        this.l0 = this.a.c(looper, null);
        this.j0 = this.j0.e(looper, new o02.b() { // from class: rh0
            @Override // defpackage.o02.b
            public final void a(Object obj, i iVar) {
                yh0.this.S2(qVar, (tb) obj, iVar);
            }
        });
    }

    @Override // defpackage.jb
    public final void f(final String str, final long j, final long j2) {
        final tb.a H1 = H1();
        U2(H1, 1016, new o02.a() { // from class: hh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.K2(tb.a.this, str, j2, j, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.k
    public final void f0(int i, j.b bVar, final u02 u02Var, final g62 g62Var) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1000, new o02.a() { // from class: gg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).w(tb.a.this, u02Var, g62Var);
            }
        });
    }

    @Override // defpackage.jb
    public final void g(final hf0 hf0Var) {
        final tb.a G1 = G1();
        U2(G1, 1020, new o02.a() { // from class: ag0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.M2(tb.a.this, hf0Var, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.exoplayer.drm.b
    public final void g0(int i, j.b bVar, final int i2) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1022, new o02.a() { // from class: wh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.e2(tb.a.this, i2, (tb) obj);
            }
        });
    }

    @Override // defpackage.jb
    public final void h(final hf0 hf0Var) {
        final tb.a H1 = H1();
        U2(H1, 1015, new o02.a() { // from class: bg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.N2(tb.a.this, hf0Var, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void h0(final PlaybackException playbackException) {
        final tb.a I1 = I1(playbackException);
        U2(I1, 10, new o02.a() { // from class: sg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).b0(tb.a.this, playbackException);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void i(final int i) {
        final tb.a B1 = B1();
        U2(B1, 4, new o02.a() { // from class: pf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).q(tb.a.this, i);
            }
        });
    }

    @Override // androidx.media3.exoplayer.source.k
    public final void i0(int i, j.b bVar, final u02 u02Var, final g62 g62Var) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1001, new o02.a() { // from class: eg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).Z(tb.a.this, u02Var, g62Var);
            }
        });
    }

    @Override // defpackage.jb
    public final void j(final hf0 hf0Var) {
        final tb.a G1 = G1();
        U2(G1, 1013, new o02.a() { // from class: dg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.O1(tb.a.this, hf0Var, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.exoplayer.drm.b
    public /* synthetic */ void j0(int i, j.b bVar) {
        pr0.a(this, i, bVar);
    }

    @Override // androidx.media3.common.q.d
    public final void k(final z zVar) {
        final tb.a H1 = H1();
        U2(H1, 25, new o02.a() { // from class: yg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.Q2(tb.a.this, zVar, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void k0(final int i, final int i2) {
        final tb.a H1 = H1();
        U2(H1, 24, new o02.a() { // from class: qf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).B(tb.a.this, i, i2);
            }
        });
    }

    @Override // defpackage.jb
    public final void l(final String str) {
        final tb.a H1 = H1();
        U2(H1, 1012, new o02.a() { // from class: gh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).l(tb.a.this, str);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void l0(final q.b bVar) {
        final tb.a B1 = B1();
        U2(B1, 13, new o02.a() { // from class: vg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).f(tb.a.this, bVar);
            }
        });
    }

    @Override // defpackage.jb
    public final void m(final String str, final long j, final long j2) {
        final tb.a H1 = H1();
        U2(H1, 1008, new o02.a() { // from class: ih0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.M1(tb.a.this, str, j2, j, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void m0(final q.e eVar, final q.e eVar2, final int i) {
        if (i == 1) {
            this.m0 = false;
        }
        this.h0.j((q) ii.e(this.k0));
        final tb.a B1 = B1();
        U2(B1, 11, new o02.a() { // from class: uf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.y2(tb.a.this, i, eVar, eVar2, (tb) obj);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void n(final p pVar) {
        final tb.a B1 = B1();
        U2(B1, 12, new o02.a() { // from class: tg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).r(tb.a.this, pVar);
            }
        });
    }

    @Override // androidx.media3.exoplayer.drm.b
    public final void n0(int i, j.b bVar) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, 1026, new o02.a() { // from class: nf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).k(tb.a.this);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void o(final nb0 nb0Var) {
        final tb.a B1 = B1();
        U2(B1, 27, new o02.a() { // from class: zf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).k0(tb.a.this, nb0Var);
            }
        });
    }

    @Override // androidx.media3.exoplayer.drm.b
    public final void o0(int i, j.b bVar, final Exception exc) {
        final tb.a F1 = F1(i, bVar);
        U2(F1, RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE, new o02.a() { // from class: ah0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).s0(tb.a.this, exc);
            }
        });
    }

    @Override // defpackage.jb
    public final void p(final int i, final long j) {
        final tb.a G1 = G1();
        U2(G1, 1018, new o02.a() { // from class: rf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).l0(tb.a.this, i, j);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void p0(final boolean z) {
        final tb.a B1 = B1();
        U2(B1, 7, new o02.a() { // from class: nh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).d(tb.a.this, z);
            }
        });
    }

    @Override // defpackage.jb
    public final void q(final androidx.media3.common.j jVar, final jf0 jf0Var) {
        final tb.a H1 = H1();
        U2(H1, 1009, new o02.a() { // from class: ng0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                yh0.Q1(tb.a.this, jVar, jf0Var, (tb) obj);
            }
        });
    }

    @Override // defpackage.jb
    public final void r(final Object obj, final long j) {
        final tb.a H1 = H1();
        U2(H1, 26, new o02.a() { // from class: dh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj2) {
                ((tb) obj2).S(tb.a.this, obj, j);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void s(final int i) {
        final tb.a B1 = B1();
        U2(B1, 8, new o02.a() { // from class: of0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).D(tb.a.this, i);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void t(final Metadata metadata) {
        final tb.a B1 = B1();
        U2(B1, 28, new o02.a() { // from class: qg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).a(tb.a.this, metadata);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public void u(final List<kb0> list) {
        final tb.a B1 = B1();
        U2(B1, 27, new o02.a() { // from class: jh0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).j(tb.a.this, list);
            }
        });
    }

    @Override // defpackage.jb
    public final void v(final long j) {
        final tb.a H1 = H1();
        U2(H1, 1010, new o02.a() { // from class: wf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).u(tb.a.this, j);
            }
        });
    }

    @Override // defpackage.jb
    public final void w(final Exception exc) {
        final tb.a H1 = H1();
        U2(H1, 1029, new o02.a() { // from class: zg0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).e(tb.a.this, exc);
            }
        });
    }

    @Override // defpackage.jb
    public final void x(final Exception exc) {
        final tb.a H1 = H1();
        U2(H1, 1030, new o02.a() { // from class: ch0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).I(tb.a.this, exc);
            }
        });
    }

    @Override // androidx.media3.common.q.d
    public final void y(final boolean z, final int i) {
        final tb.a B1 = B1();
        U2(B1, 5, new o02.a() { // from class: ph0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).o0(tb.a.this, z, i);
            }
        });
    }

    @Override // defpackage.jb
    public final void z(final int i, final long j, final long j2) {
        final tb.a H1 = H1();
        U2(H1, 1011, new o02.a() { // from class: tf0
            @Override // defpackage.o02.a
            public final void invoke(Object obj) {
                ((tb) obj).c0(tb.a.this, i, j, j2);
            }
        });
    }
}
