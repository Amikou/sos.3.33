package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import net.safemoon.androidwallet.R;

/* compiled from: SimpleToolbarWithTitleBinding.java */
/* renamed from: rp3  reason: default package */
/* loaded from: classes2.dex */
public final class rp3 {
    public final ImageView a;
    public final TextView b;
    public final AppCompatTextView c;

    public rp3(Toolbar toolbar, AppCompatImageView appCompatImageView, ImageView imageView, TextView textView, AppCompatTextView appCompatTextView) {
        this.a = imageView;
        this.b = textView;
        this.c = appCompatTextView;
    }

    public static rp3 a(View view) {
        int i = R.id.imgInfo;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgInfo);
        if (appCompatImageView != null) {
            i = R.id.ivToolbarBack;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivToolbarBack);
            if (imageView != null) {
                i = R.id.rightMenu;
                TextView textView = (TextView) ai4.a(view, R.id.rightMenu);
                if (textView != null) {
                    i = R.id.tvToolbarTitle;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.tvToolbarTitle);
                    if (appCompatTextView != null) {
                        return new rp3((Toolbar) view, appCompatImageView, imageView, textView, appCompatTextView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
