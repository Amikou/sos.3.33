package defpackage;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Executor;

/* compiled from: ThreadHandoffProducerQueueImpl.java */
/* renamed from: i54  reason: default package */
/* loaded from: classes.dex */
public class i54 implements h54 {
    public boolean a = false;
    public final Deque<Runnable> b = new ArrayDeque();
    public final Executor c;

    public i54(Executor executor) {
        this.c = (Executor) xt2.g(executor);
    }

    @Override // defpackage.h54
    public synchronized void a(Runnable runnable) {
        this.b.remove(runnable);
    }

    @Override // defpackage.h54
    public synchronized void b(Runnable runnable) {
        if (this.a) {
            this.b.add(runnable);
        } else {
            this.c.execute(runnable);
        }
    }
}
