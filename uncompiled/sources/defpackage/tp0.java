package defpackage;

import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.f;

/* compiled from: Dispatchers.kt */
/* renamed from: tp0  reason: default package */
/* loaded from: classes2.dex */
public final class tp0 {
    public static final tp0 a = new tp0();
    public static final CoroutineDispatcher b = x80.a();
    public static final CoroutineDispatcher c;

    static {
        f fVar = f.f0;
        c = pk0.k0.N();
    }

    public static final CoroutineDispatcher a() {
        return b;
    }

    public static final CoroutineDispatcher b() {
        return c;
    }

    public static final e32 c() {
        return f32.c;
    }
}
