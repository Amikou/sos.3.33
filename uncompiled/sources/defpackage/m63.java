package defpackage;

import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.protocol.core.b;

/* compiled from: RemoteFunctionCall.java */
/* renamed from: m63  reason: default package */
/* loaded from: classes3.dex */
public class m63<T> extends b<T> {
    private final id1 function;

    public m63(id1 id1Var, Callable<T> callable) {
        super(callable);
        this.function = id1Var;
    }

    public List<zc4> decodeFunctionResponse(String str) {
        return yd1.decode(str, this.function.getOutputParameters());
    }

    public String encodeFunctionCall() {
        return org.web3j.abi.b.encode(this.function);
    }
}
