package defpackage;

import com.google.android.gms.signin.internal.zam;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: f15  reason: default package */
/* loaded from: classes.dex */
public final class f15 implements Runnable {
    public final /* synthetic */ zam a;
    public final /* synthetic */ e15 f0;

    public f15(e15 e15Var, zam zamVar) {
        this.f0 = e15Var;
        this.a = zamVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.K1(this.a);
    }
}
