package defpackage;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: qy5  reason: default package */
/* loaded from: classes.dex */
public final class qy5 extends hz5 {
    public qy5(int i) {
        super(i, null);
    }

    @Override // defpackage.hz5
    public final void a() {
        if (!b()) {
            for (int i = 0; i < c(); i++) {
                Map.Entry d = d(i);
                if (((cu5) d.getKey()).zzc()) {
                    d.setValue(Collections.unmodifiableList((List) d.getValue()));
                }
            }
            for (Map.Entry entry : e()) {
                if (((cu5) entry.getKey()).zzc()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.a();
    }
}
