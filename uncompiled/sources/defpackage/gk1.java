package defpackage;

import defpackage.xy1;
import java.util.Collections;
import java.util.Map;

/* compiled from: Headers.java */
/* renamed from: gk1  reason: default package */
/* loaded from: classes.dex */
public interface gk1 {
    public static final gk1 a;

    /* compiled from: Headers.java */
    /* renamed from: gk1$a */
    /* loaded from: classes.dex */
    public class a implements gk1 {
        @Override // defpackage.gk1
        public Map<String, String> a() {
            return Collections.emptyMap();
        }
    }

    static {
        new a();
        a = new xy1.a().a();
    }

    Map<String, String> a();
}
