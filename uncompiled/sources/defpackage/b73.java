package defpackage;

import android.net.Uri;
import androidx.media3.common.j;
import com.google.common.collect.ImmutableList;
import defpackage.cj3;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* compiled from: Representation.java */
/* renamed from: b73  reason: default package */
/* loaded from: classes.dex */
public abstract class b73 {
    public final j a;
    public final ImmutableList<bo> b;
    public final long c;
    public final List<nm0> d;
    public final List<nm0> e;
    public final List<nm0> f;
    public final s33 g;

    /* compiled from: Representation.java */
    /* renamed from: b73$b */
    /* loaded from: classes.dex */
    public static class b extends b73 implements wd0 {
        public final cj3.a h;

        public b(long j, j jVar, List<bo> list, cj3.a aVar, List<nm0> list2, List<nm0> list3, List<nm0> list4) {
            super(j, jVar, list, aVar, list2, list3, list4);
            this.h = aVar;
        }

        @Override // defpackage.b73
        public String a() {
            return null;
        }

        @Override // defpackage.wd0
        public long b(long j) {
            return this.h.j(j);
        }

        @Override // defpackage.wd0
        public long c(long j, long j2) {
            return this.h.h(j, j2);
        }

        @Override // defpackage.wd0
        public long d(long j, long j2) {
            return this.h.d(j, j2);
        }

        @Override // defpackage.wd0
        public long e(long j, long j2) {
            return this.h.f(j, j2);
        }

        @Override // defpackage.wd0
        public s33 f(long j) {
            return this.h.k(this, j);
        }

        @Override // defpackage.wd0
        public long g(long j, long j2) {
            return this.h.i(j, j2);
        }

        @Override // defpackage.wd0
        public boolean h() {
            return this.h.l();
        }

        @Override // defpackage.wd0
        public long i() {
            return this.h.e();
        }

        @Override // defpackage.wd0
        public long j(long j) {
            return this.h.g(j);
        }

        @Override // defpackage.wd0
        public long k(long j, long j2) {
            return this.h.c(j, j2);
        }

        @Override // defpackage.b73
        public wd0 l() {
            return this;
        }

        @Override // defpackage.b73
        public s33 m() {
            return null;
        }
    }

    /* compiled from: Representation.java */
    /* renamed from: b73$c */
    /* loaded from: classes.dex */
    public static class c extends b73 {
        public final String h;
        public final s33 i;
        public final yp3 j;

        public c(long j, j jVar, List<bo> list, cj3.e eVar, List<nm0> list2, List<nm0> list3, List<nm0> list4, String str, long j2) {
            super(j, jVar, list, eVar, list2, list3, list4);
            Uri.parse(list.get(0).a);
            s33 c = eVar.c();
            this.i = c;
            this.h = str;
            this.j = c != null ? null : new yp3(new s33(null, 0L, j2));
        }

        @Override // defpackage.b73
        public String a() {
            return this.h;
        }

        @Override // defpackage.b73
        public wd0 l() {
            return this.j;
        }

        @Override // defpackage.b73
        public s33 m() {
            return this.i;
        }
    }

    public static b73 o(long j, j jVar, List<bo> list, cj3 cj3Var, List<nm0> list2, List<nm0> list3, List<nm0> list4, String str) {
        if (cj3Var instanceof cj3.e) {
            return new c(j, jVar, list, (cj3.e) cj3Var, list2, list3, list4, str, -1L);
        }
        if (cj3Var instanceof cj3.a) {
            return new b(j, jVar, list, (cj3.a) cj3Var, list2, list3, list4);
        }
        throw new IllegalArgumentException("segmentBase must be of type SingleSegmentBase or MultiSegmentBase");
    }

    public abstract String a();

    public abstract wd0 l();

    public abstract s33 m();

    public s33 n() {
        return this.g;
    }

    public b73(long j, j jVar, List<bo> list, cj3 cj3Var, List<nm0> list2, List<nm0> list3, List<nm0> list4) {
        List<nm0> unmodifiableList;
        ii.a(!list.isEmpty());
        this.a = jVar;
        this.b = ImmutableList.copyOf((Collection) list);
        if (list2 == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(list2);
        }
        this.d = unmodifiableList;
        this.e = list3;
        this.f = list4;
        this.g = cj3Var.a(this);
        this.c = cj3Var.b();
    }
}
