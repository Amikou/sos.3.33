package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: w26  reason: default package */
/* loaded from: classes.dex */
public final class w26 implements v26 {
    public static final wo5<Boolean> a;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.config.persist_last_modified", true);
        ro5Var.a("measurement.id.config.persist_last_modified", 0L);
    }

    @Override // defpackage.v26
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.v26
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
