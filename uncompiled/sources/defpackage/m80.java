package defpackage;

import android.graphics.drawable.Animatable;

/* compiled from: ControllerListener.java */
/* renamed from: m80  reason: default package */
/* loaded from: classes.dex */
public interface m80<INFO> {
    void a(String str, INFO info2);

    void b(String str, INFO info2, Animatable animatable);

    void c(String str, Throwable th);

    void d(String str);

    void e(String str, Object obj);

    void f(String str, Throwable th);
}
