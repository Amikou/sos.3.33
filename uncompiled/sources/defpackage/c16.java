package defpackage;

import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.b;
import com.google.android.gms.tasks.c;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: c16  reason: default package */
/* loaded from: classes.dex */
public final class c16 implements Runnable {
    public final /* synthetic */ c a;
    public final /* synthetic */ f26 f0;

    public c16(f26 f26Var, c cVar) {
        this.f0 = f26Var;
        this.a = cVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        b bVar;
        try {
            bVar = this.f0.b;
            c a = bVar.a(this.a.l());
            if (a == null) {
                this.f0.b(new NullPointerException("Continuation returned null"));
                return;
            }
            Executor executor = s34.b;
            a.g(executor, this.f0);
            a.e(executor, this.f0);
            a.a(executor, this.f0);
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                this.f0.b((Exception) e.getCause());
            } else {
                this.f0.b(e);
            }
        } catch (CancellationException unused) {
            this.f0.d();
        } catch (Exception e2) {
            this.f0.b(e2);
        }
    }
}
