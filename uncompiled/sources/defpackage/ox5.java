package defpackage;

import com.google.android.gms.internal.measurement.zzlc;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: ox5  reason: default package */
/* loaded from: classes.dex */
public final class ox5 {
    public static final int a(int i, Object obj, Object obj2) {
        zzlc zzlcVar = (zzlc) obj;
        nx5 nx5Var = (nx5) obj2;
        if (zzlcVar.isEmpty()) {
            return 0;
        }
        Iterator it = zzlcVar.entrySet().iterator();
        if (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            entry.getKey();
            entry.getValue();
            throw null;
        }
        return 0;
    }

    public static final Object b(Object obj, Object obj2) {
        zzlc zzlcVar = (zzlc) obj;
        zzlc zzlcVar2 = (zzlc) obj2;
        if (!zzlcVar2.isEmpty()) {
            if (!zzlcVar.zze()) {
                zzlcVar = zzlcVar.zzc();
            }
            zzlcVar.zzb(zzlcVar2);
        }
        return zzlcVar;
    }
}
