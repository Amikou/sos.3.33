package defpackage;

import java.io.Closeable;

/* compiled from: SupportSQLiteProgram.java */
/* renamed from: uw3  reason: default package */
/* loaded from: classes.dex */
public interface uw3 extends Closeable {
    void A0(int i, byte[] bArr);

    void L(int i, String str);

    void Y(int i, double d);

    void Y0(int i);

    void q0(int i, long j);
}
