package defpackage;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: v15  reason: default package */
/* loaded from: classes.dex */
public final class v15 implements Handler.Callback {
    public final c25 a;
    public final Handler l0;
    public final ArrayList<GoogleApiClient.b> f0 = new ArrayList<>();
    public final ArrayList<GoogleApiClient.b> g0 = new ArrayList<>();
    public final ArrayList<GoogleApiClient.c> h0 = new ArrayList<>();
    public volatile boolean i0 = false;
    public final AtomicInteger j0 = new AtomicInteger(0);
    public boolean k0 = false;
    public final Object m0 = new Object();

    public v15(Looper looper, c25 c25Var) {
        this.a = c25Var;
        this.l0 = new q25(looper, this);
    }

    public final void a() {
        this.i0 = false;
        this.j0.incrementAndGet();
    }

    public final void b(int i) {
        zt2.d(this.l0, "onUnintentionalDisconnection must only be called on the Handler thread");
        this.l0.removeMessages(1);
        synchronized (this.m0) {
            this.k0 = true;
            ArrayList arrayList = new ArrayList(this.f0);
            int i2 = this.j0.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                GoogleApiClient.b bVar = (GoogleApiClient.b) obj;
                if (!this.i0 || this.j0.get() != i2) {
                    break;
                } else if (this.f0.contains(bVar)) {
                    bVar.onConnectionSuspended(i);
                }
            }
            this.g0.clear();
            this.k0 = false;
        }
    }

    public final void c(Bundle bundle) {
        zt2.d(this.l0, "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.m0) {
            boolean z = true;
            zt2.m(!this.k0);
            this.l0.removeMessages(1);
            this.k0 = true;
            if (this.g0.size() != 0) {
                z = false;
            }
            zt2.m(z);
            ArrayList arrayList = new ArrayList(this.f0);
            int i = this.j0.get();
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                GoogleApiClient.b bVar = (GoogleApiClient.b) obj;
                if (!this.i0 || !this.a.c() || this.j0.get() != i) {
                    break;
                } else if (!this.g0.contains(bVar)) {
                    bVar.onConnected(bundle);
                }
            }
            this.g0.clear();
            this.k0 = false;
        }
    }

    public final void d(ConnectionResult connectionResult) {
        zt2.d(this.l0, "onConnectionFailure must only be called on the Handler thread");
        this.l0.removeMessages(1);
        synchronized (this.m0) {
            ArrayList arrayList = new ArrayList(this.h0);
            int i = this.j0.get();
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                GoogleApiClient.c cVar = (GoogleApiClient.c) obj;
                if (this.i0 && this.j0.get() == i) {
                    if (this.h0.contains(cVar)) {
                        cVar.onConnectionFailed(connectionResult);
                    }
                }
                return;
            }
        }
    }

    public final void e(GoogleApiClient.b bVar) {
        zt2.j(bVar);
        synchronized (this.m0) {
            if (this.f0.contains(bVar)) {
                String valueOf = String.valueOf(bVar);
                StringBuilder sb = new StringBuilder(valueOf.length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
            } else {
                this.f0.add(bVar);
            }
        }
        if (this.a.c()) {
            Handler handler = this.l0;
            handler.sendMessage(handler.obtainMessage(1, bVar));
        }
    }

    public final void f(GoogleApiClient.c cVar) {
        zt2.j(cVar);
        synchronized (this.m0) {
            if (this.h0.contains(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(valueOf.length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
            } else {
                this.h0.add(cVar);
            }
        }
    }

    public final void g() {
        this.i0 = true;
    }

    public final void h(GoogleApiClient.c cVar) {
        zt2.j(cVar);
        synchronized (this.m0) {
            if (!this.h0.remove(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(valueOf.length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
            }
        }
    }

    @Override // android.os.Handler.Callback
    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            GoogleApiClient.b bVar = (GoogleApiClient.b) message.obj;
            synchronized (this.m0) {
                if (this.i0 && this.a.c() && this.f0.contains(bVar)) {
                    bVar.onConnected(this.a.v());
                }
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }
}
