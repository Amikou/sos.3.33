package defpackage;

import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;
import java.io.IOException;

/* compiled from: GifDrawableEncoder.java */
/* renamed from: wf1  reason: default package */
/* loaded from: classes.dex */
public class wf1 implements y73<uf1> {
    @Override // defpackage.y73
    public EncodeStrategy b(vn2 vn2Var) {
        return EncodeStrategy.SOURCE;
    }

    @Override // defpackage.ev0
    /* renamed from: c */
    public boolean a(s73<uf1> s73Var, File file, vn2 vn2Var) {
        try {
            ts.f(s73Var.get().c(), file);
            return true;
        } catch (IOException unused) {
            return false;
        }
    }
}
