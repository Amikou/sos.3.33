package defpackage;

import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;

/* compiled from: ViewGroup.kt */
/* renamed from: li4  reason: default package */
/* loaded from: classes.dex */
public final class li4 {

    /* compiled from: ViewGroup.kt */
    /* renamed from: li4$a */
    /* loaded from: classes.dex */
    public static final class a implements ol3<View> {
        public final /* synthetic */ ViewGroup a;

        public a(ViewGroup viewGroup) {
            this.a = viewGroup;
        }

        @Override // defpackage.ol3
        public Iterator<View> iterator() {
            return li4.b(this.a);
        }
    }

    /* compiled from: ViewGroup.kt */
    /* renamed from: li4$b */
    /* loaded from: classes.dex */
    public static final class b implements Iterator<View>, tw1 {
        public int a;
        public final /* synthetic */ ViewGroup f0;

        public b(ViewGroup viewGroup) {
            this.f0 = viewGroup;
        }

        @Override // java.util.Iterator
        /* renamed from: a */
        public View next() {
            ViewGroup viewGroup = this.f0;
            int i = this.a;
            this.a = i + 1;
            View childAt = viewGroup.getChildAt(i);
            if (childAt != null) {
                return childAt;
            }
            throw new IndexOutOfBoundsException();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a < this.f0.getChildCount();
        }

        @Override // java.util.Iterator
        public void remove() {
            ViewGroup viewGroup = this.f0;
            int i = this.a - 1;
            this.a = i;
            viewGroup.removeViewAt(i);
        }
    }

    public static final ol3<View> a(ViewGroup viewGroup) {
        fs1.f(viewGroup, "<this>");
        return new a(viewGroup);
    }

    public static final Iterator<View> b(ViewGroup viewGroup) {
        fs1.f(viewGroup, "<this>");
        return new b(viewGroup);
    }
}
