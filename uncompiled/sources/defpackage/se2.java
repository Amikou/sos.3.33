package defpackage;

import android.view.View;

/* compiled from: NestedScrollingParent3.java */
/* renamed from: se2  reason: default package */
/* loaded from: classes.dex */
public interface se2 extends re2 {
    void onNestedScroll(View view, int i, int i2, int i3, int i4, int i5, int[] iArr);
}
