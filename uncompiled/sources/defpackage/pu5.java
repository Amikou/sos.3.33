package defpackage;

import com.google.android.gms.internal.measurement.x1;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: pu5  reason: default package */
/* loaded from: classes.dex */
public final class pu5 implements tx5 {
    public static final pu5 a = new pu5();

    public static pu5 c() {
        return a;
    }

    @Override // defpackage.tx5
    public final rx5 a(Class<?> cls) {
        if (!x1.class.isAssignableFrom(cls)) {
            String name = cls.getName();
            throw new IllegalArgumentException(name.length() != 0 ? "Unsupported message type: ".concat(name) : new String("Unsupported message type: "));
        }
        try {
            return (rx5) x1.t(cls.asSubclass(x1.class)).w(3, null, null);
        } catch (Exception e) {
            String name2 = cls.getName();
            throw new RuntimeException(name2.length() != 0 ? "Unable to get message info for ".concat(name2) : new String("Unable to get message info for "), e);
        }
    }

    @Override // defpackage.tx5
    public final boolean b(Class<?> cls) {
        return x1.class.isAssignableFrom(cls);
    }
}
