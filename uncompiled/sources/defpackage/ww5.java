package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: ww5  reason: default package */
/* loaded from: classes.dex */
public abstract class ww5 {
    public static final ww5 a = new tw5(null);
    public static final ww5 b = new uw5(null);

    public /* synthetic */ ww5(qw5 qw5Var) {
    }

    public static ww5 c() {
        return a;
    }

    public static ww5 d() {
        return b;
    }

    public abstract void a(Object obj, long j);

    public abstract <L> void b(Object obj, Object obj2, long j);
}
