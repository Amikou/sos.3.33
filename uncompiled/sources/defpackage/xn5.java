package defpackage;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: xn5  reason: default package */
/* loaded from: classes.dex */
public final class xn5 implements Runnable {
    public final /* synthetic */ AtomicReference a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ dp5 h0;

    public xn5(dp5 dp5Var, AtomicReference atomicReference, String str, String str2, String str3) {
        this.h0 = dp5Var;
        this.a = atomicReference;
        this.f0 = str2;
        this.g0 = str3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.h0.a.R().N(this.a, null, this.f0, this.g0);
    }
}
