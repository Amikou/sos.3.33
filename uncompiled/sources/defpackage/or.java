package defpackage;

import java.util.LinkedList;
import java.util.Queue;

/* compiled from: Bucket.java */
/* renamed from: or  reason: default package */
/* loaded from: classes.dex */
public class or<V> {
    public final int a;
    public final int b;
    public final Queue c;
    public final boolean d;
    public int e;

    public or(int i, int i2, int i3, boolean z) {
        xt2.i(i > 0);
        xt2.i(i2 >= 0);
        xt2.i(i3 >= 0);
        this.a = i;
        this.b = i2;
        this.c = new LinkedList();
        this.e = i3;
        this.d = z;
    }

    public void a(V v) {
        this.c.add(v);
    }

    public void b() {
        xt2.i(this.e > 0);
        this.e--;
    }

    @Deprecated
    public V c() {
        V g = g();
        if (g != null) {
            this.e++;
        }
        return g;
    }

    public int d() {
        return this.c.size();
    }

    public void e() {
        this.e++;
    }

    public boolean f() {
        return this.e + d() > this.b;
    }

    public V g() {
        return (V) this.c.poll();
    }

    public void h(V v) {
        xt2.g(v);
        if (this.d) {
            xt2.i(this.e > 0);
            this.e--;
            a(v);
            return;
        }
        int i = this.e;
        if (i > 0) {
            this.e = i - 1;
            a(v);
            return;
        }
        v11.j("BUCKET", "Tried to release value %s from an empty bucket!", v);
    }
}
