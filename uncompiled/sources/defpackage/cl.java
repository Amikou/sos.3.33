package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread_Frame.java */
/* renamed from: cl  reason: default package */
/* loaded from: classes2.dex */
public final class cl extends r90.e.d.a.b.AbstractC0272e.AbstractC0274b {
    public final long a;
    public final String b;
    public final String c;
    public final long d;
    public final int e;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread_Frame.java */
    /* renamed from: cl$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a {
        public Long a;
        public String b;
        public String c;
        public Long d;
        public Integer e;

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0274b a() {
            String str = "";
            if (this.a == null) {
                str = " pc";
            }
            if (this.b == null) {
                str = str + " symbol";
            }
            if (this.d == null) {
                str = str + " offset";
            }
            if (this.e == null) {
                str = str + " importance";
            }
            if (str.isEmpty()) {
                return new cl(this.a.longValue(), this.b, this.c, this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a b(String str) {
            this.c = str;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a c(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a d(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a e(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a f(String str) {
            Objects.requireNonNull(str, "Null symbol");
            this.b = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b
    public String b() {
        return this.c;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b
    public int c() {
        return this.e;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b
    public long d() {
        return this.d;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b
    public long e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        String str;
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a.b.AbstractC0272e.AbstractC0274b) {
            r90.e.d.a.b.AbstractC0272e.AbstractC0274b abstractC0274b = (r90.e.d.a.b.AbstractC0272e.AbstractC0274b) obj;
            return this.a == abstractC0274b.e() && this.b.equals(abstractC0274b.f()) && ((str = this.c) != null ? str.equals(abstractC0274b.b()) : abstractC0274b.b() == null) && this.d == abstractC0274b.d() && this.e == abstractC0274b.c();
        }
        return false;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0274b
    public String f() {
        return this.b;
    }

    public int hashCode() {
        long j = this.a;
        int hashCode = (((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003;
        String str = this.c;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j2 = this.d;
        return ((((hashCode ^ hashCode2) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.e;
    }

    public String toString() {
        return "Frame{pc=" + this.a + ", symbol=" + this.b + ", file=" + this.c + ", offset=" + this.d + ", importance=" + this.e + "}";
    }

    public cl(long j, String str, String str2, long j2, int i) {
        this.a = j;
        this.b = str;
        this.c = str2;
        this.d = j2;
        this.e = i;
    }
}
