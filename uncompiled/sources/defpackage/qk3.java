package defpackage;

/* compiled from: SendingCollector.kt */
/* renamed from: qk3  reason: default package */
/* loaded from: classes2.dex */
public final class qk3<T> implements k71<T> {
    public final uj3<T> a;

    /* JADX WARN: Multi-variable type inference failed */
    public qk3(uj3<? super T> uj3Var) {
        this.a = uj3Var;
    }

    @Override // defpackage.k71
    public Object emit(T t, q70<? super te4> q70Var) {
        Object h = this.a.h(t, q70Var);
        return h == gs1.d() ? h : te4.a;
    }
}
