package defpackage;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: i05  reason: default package */
/* loaded from: classes.dex */
public final class i05 implements v05, u25 {
    public final Lock a;
    public final Condition b;
    public final Context c;
    public final eh1 d;
    public final k05 e;
    public final Map<a.c<?>, a.f> f;
    public final Map<a.c<?>, ConnectionResult> g = new HashMap();
    public final kz h;
    public final Map<a<?>, Boolean> i;
    public final a.AbstractC0106a<? extends p15, uo3> j;
    public volatile j05 k;
    public int l;
    public final d05 m;
    public final u05 n;

    public i05(Context context, d05 d05Var, Lock lock, Looper looper, eh1 eh1Var, Map<a.c<?>, a.f> map, kz kzVar, Map<a<?>, Boolean> map2, a.AbstractC0106a<? extends p15, uo3> abstractC0106a, ArrayList<r25> arrayList, u05 u05Var) {
        this.c = context;
        this.a = lock;
        this.d = eh1Var;
        this.f = map;
        this.h = kzVar;
        this.i = map2;
        this.j = abstractC0106a;
        this.m = d05Var;
        this.n = u05Var;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            r25 r25Var = arrayList.get(i);
            i++;
            r25Var.c(this);
        }
        this.e = new k05(this, looper);
        this.b = lock.newCondition();
        this.k = new e05(this);
    }

    @Override // defpackage.v05
    public final void a() {
        this.k.c();
    }

    @Override // defpackage.u25
    public final void b(ConnectionResult connectionResult, a<?> aVar, boolean z) {
        this.a.lock();
        try {
            this.k.b(connectionResult, aVar, z);
        } finally {
            this.a.unlock();
        }
    }

    @Override // defpackage.v05
    public final void c() {
        if (this.k.f()) {
            this.g.clear();
        }
    }

    @Override // defpackage.v05
    public final void d() {
        if (e()) {
            ((qz4) this.k).i();
        }
    }

    @Override // defpackage.v05
    public final boolean e() {
        return this.k instanceof qz4;
    }

    @Override // defpackage.v05
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append("mState=").println(this.k);
        for (a<?> aVar : this.i.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) aVar.d()).println(":");
            ((a.f) zt2.j(this.f.get(aVar.c()))).o(concat, fileDescriptor, printWriter, strArr);
        }
    }

    @Override // defpackage.v05
    public final <A extends a.b, T extends b<? extends l83, A>> T g(T t) {
        t.n();
        return (T) this.k.g(t);
    }

    public final void i(ConnectionResult connectionResult) {
        this.a.lock();
        try {
            this.k = new e05(this);
            this.k.a();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    public final void j(RuntimeException runtimeException) {
        this.e.sendMessage(this.e.obtainMessage(2, runtimeException));
    }

    public final void k(q05 q05Var) {
        this.e.sendMessage(this.e.obtainMessage(1, q05Var));
    }

    public final void m() {
        this.a.lock();
        try {
            this.k = new rz4(this, this.h, this.i, this.d, this.j, this.a, this.c);
            this.k.a();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    public final void n() {
        this.a.lock();
        try {
            this.m.r();
            this.k = new qz4(this);
            this.k.a();
            this.b.signalAll();
        } finally {
            this.a.unlock();
        }
    }

    @Override // defpackage.u50
    public final void onConnected(Bundle bundle) {
        this.a.lock();
        try {
            this.k.d(bundle);
        } finally {
            this.a.unlock();
        }
    }

    @Override // defpackage.u50
    public final void onConnectionSuspended(int i) {
        this.a.lock();
        try {
            this.k.e(i);
        } finally {
            this.a.unlock();
        }
    }
}
