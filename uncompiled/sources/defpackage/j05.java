package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: j05  reason: default package */
/* loaded from: classes.dex */
public interface j05 {
    void a();

    void b(ConnectionResult connectionResult, a<?> aVar, boolean z);

    void c();

    void d(Bundle bundle);

    void e(int i);

    boolean f();

    <A extends a.b, T extends b<? extends l83, A>> T g(T t);
}
