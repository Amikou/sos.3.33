package defpackage;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: LazySet.java */
/* renamed from: az1  reason: default package */
/* loaded from: classes2.dex */
public class az1<T> implements fw2<Set<T>> {
    public volatile Set<T> b = null;
    public volatile Set<fw2<T>> a = Collections.newSetFromMap(new ConcurrentHashMap());

    public az1(Collection<fw2<T>> collection) {
        this.a.addAll(collection);
    }

    public static az1<?> b(Collection<fw2<?>> collection) {
        return new az1<>((Set) collection);
    }

    public synchronized void a(fw2<T> fw2Var) {
        if (this.b == null) {
            this.a.add(fw2Var);
        } else {
            this.b.add(fw2Var.get());
        }
    }

    @Override // defpackage.fw2
    /* renamed from: c */
    public Set<T> get() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = Collections.newSetFromMap(new ConcurrentHashMap());
                    d();
                }
            }
        }
        return Collections.unmodifiableSet(this.b);
    }

    public final synchronized void d() {
        for (fw2<T> fw2Var : this.a) {
            this.b.add(fw2Var.get());
        }
        this.a = null;
    }
}
