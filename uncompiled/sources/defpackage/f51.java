package defpackage;

import android.content.Context;
import com.google.firebase.FirebaseCommonRegistrar;
import defpackage.jz1;

/* renamed from: f51  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class f51 implements jz1.a {
    public static final /* synthetic */ f51 a = new f51();

    @Override // defpackage.jz1.a
    public final String extract(Object obj) {
        String f;
        f = FirebaseCommonRegistrar.f((Context) obj);
        return f;
    }
}
