package defpackage;

import com.google.android.gms.internal.measurement.zzjd;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: nz5  reason: default package */
/* loaded from: classes.dex */
public final class nz5 extends AbstractList<String> implements RandomAccess, nw5 {
    public final nw5 a;

    public nz5(nw5 nw5Var) {
        this.a = nw5Var;
    }

    @Override // defpackage.nw5
    public final void M(zzjd zzjdVar) {
        throw new UnsupportedOperationException();
    }

    @Override // defpackage.nw5
    public final List<?> f() {
        return this.a.f();
    }

    @Override // defpackage.nw5
    public final nw5 g() {
        return this;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object get(int i) {
        return ((kw5) this.a).get(i);
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
    public final Iterator<String> iterator() {
        return new mz5(this);
    }

    @Override // java.util.AbstractList, java.util.List
    public final ListIterator<String> listIterator(int i) {
        return new lz5(this, i);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.a.size();
    }

    @Override // defpackage.nw5
    public final Object v1(int i) {
        return this.a.v1(i);
    }
}
