package defpackage;

import android.content.Context;
import android.os.Bundle;
import defpackage.kb;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: lb  reason: default package */
/* loaded from: classes2.dex */
public class lb implements kb {
    public static volatile kb c;
    public final tf a;
    public final Map<String, Object> b;

    /* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
    /* renamed from: lb$a */
    /* loaded from: classes2.dex */
    public class a implements kb.a {
        public a(lb lbVar, String str) {
        }
    }

    public lb(tf tfVar) {
        zt2.j(tfVar);
        this.a = tfVar;
        this.b = new ConcurrentHashMap();
    }

    public static kb d(c51 c51Var, Context context, mv3 mv3Var) {
        zt2.j(c51Var);
        zt2.j(context);
        zt2.j(mv3Var);
        zt2.j(context.getApplicationContext());
        if (c == null) {
            synchronized (lb.class) {
                if (c == null) {
                    Bundle bundle = new Bundle(1);
                    if (c51Var.r()) {
                        mv3Var.a(de0.class, s35.a, k75.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", c51Var.q());
                    }
                    c = new lb(wf5.r(context, null, null, null, bundle).s());
                }
            }
        }
        return c;
    }

    public static final /* synthetic */ void e(qx0 qx0Var) {
        boolean z = ((de0) qx0Var.a()).a;
        synchronized (lb.class) {
            ((lb) zt2.j(c)).a.d(z);
        }
    }

    @Override // defpackage.kb
    public kb.a a(String str, kb.b bVar) {
        Object pk5Var;
        zt2.j(bVar);
        if (ha5.a(str) && !f(str)) {
            tf tfVar = this.a;
            if ("fiam".equals(str)) {
                pk5Var = new ff5(tfVar, bVar);
            } else {
                pk5Var = ("crash".equals(str) || "clx".equals(str)) ? new pk5(tfVar, bVar) : null;
            }
            if (pk5Var != null) {
                this.b.put(str, pk5Var);
                return new a(this, str);
            }
            return null;
        }
        return null;
    }

    @Override // defpackage.kb
    public void b(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (ha5.a(str) && ha5.b(str2, bundle) && ha5.e(str, str2, bundle)) {
            ha5.g(str, str2, bundle);
            this.a.a(str, str2, bundle);
        }
    }

    @Override // defpackage.kb
    public void c(String str, String str2, Object obj) {
        if (ha5.a(str) && ha5.d(str, str2)) {
            this.a.c(str, str2, obj);
        }
    }

    public final boolean f(String str) {
        return (str.isEmpty() || !this.b.containsKey(str) || this.b.get(str) == null) ? false : true;
    }
}
