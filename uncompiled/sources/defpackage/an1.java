package defpackage;

import java.util.List;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: IUserTokenDisplayModelMapper.kt */
/* renamed from: an1  reason: default package */
/* loaded from: classes2.dex */
public interface an1 {
    List<UserTokenItemDisplayModel> a(List<? extends IToken> list);
}
