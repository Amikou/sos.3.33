package defpackage;

import com.google.android.gms.internal.measurement.zzmx;
import defpackage.cu5;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: cu5  reason: default package */
/* loaded from: classes.dex */
public interface cu5<T extends cu5<T>> extends Comparable<T> {
    int zza();

    zzmx zzb();

    boolean zzc();
}
