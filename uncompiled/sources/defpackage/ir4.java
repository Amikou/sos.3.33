package defpackage;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import java.lang.reflect.Method;

/* compiled from: WrappedDrawableApi21.java */
/* renamed from: ir4  reason: default package */
/* loaded from: classes.dex */
public class ir4 extends hr4 {
    public static Method l0;

    public ir4(Drawable drawable) {
        super(drawable);
        g();
    }

    @Override // defpackage.hr4
    public boolean c() {
        if (Build.VERSION.SDK_INT == 21) {
            Drawable drawable = this.j0;
            return (drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer) || (drawable instanceof InsetDrawable) || (drawable instanceof RippleDrawable);
        }
        return false;
    }

    public final void g() {
        if (l0 == null) {
            try {
                l0 = Drawable.class.getDeclaredMethod("isProjected", new Class[0]);
            } catch (Exception unused) {
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public Rect getDirtyBounds() {
        return this.j0.getDirtyBounds();
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        this.j0.getOutline(outline);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isProjected() {
        Method method;
        Drawable drawable = this.j0;
        if (drawable != null && (method = l0) != null) {
            try {
                return ((Boolean) method.invoke(drawable, new Object[0])).booleanValue();
            } catch (Exception unused) {
            }
        }
        return false;
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspot(float f, float f2) {
        this.j0.setHotspot(f, f2);
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.j0.setHotspotBounds(i, i2, i3, i4);
    }

    @Override // defpackage.hr4, android.graphics.drawable.Drawable
    public boolean setState(int[] iArr) {
        if (super.setState(iArr)) {
            invalidateSelf();
            return true;
        }
        return false;
    }

    @Override // defpackage.hr4, android.graphics.drawable.Drawable, defpackage.i64
    public void setTint(int i) {
        if (c()) {
            super.setTint(i);
        } else {
            this.j0.setTint(i);
        }
    }

    @Override // defpackage.hr4, android.graphics.drawable.Drawable, defpackage.i64
    public void setTintList(ColorStateList colorStateList) {
        if (c()) {
            super.setTintList(colorStateList);
        } else {
            this.j0.setTintList(colorStateList);
        }
    }

    @Override // defpackage.hr4, android.graphics.drawable.Drawable, defpackage.i64
    public void setTintMode(PorterDuff.Mode mode) {
        if (c()) {
            super.setTintMode(mode);
        } else {
            this.j0.setTintMode(mode);
        }
    }

    public ir4(jr4 jr4Var, Resources resources) {
        super(jr4Var, resources);
        g();
    }
}
