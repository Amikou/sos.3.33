package defpackage;

import android.net.Uri;
import androidx.media3.extractor.Extractor;
import java.util.List;
import java.util.Map;

/* compiled from: ExtractorsFactory.java */
/* renamed from: u11  reason: default package */
/* loaded from: classes.dex */
public interface u11 {
    public static final u11 a = null;

    p11[] a();

    Extractor[] b(Uri uri, Map<String, List<String>> map);
}
