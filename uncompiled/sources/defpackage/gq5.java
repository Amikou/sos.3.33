package defpackage;

import com.google.android.gms.internal.measurement.zzic;
import com.google.android.gms.internal.measurement.zzie;
import java.io.Serializable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: gq5  reason: default package */
/* loaded from: classes.dex */
public final class gq5 {
    public static <T> yp5<T> a(yp5<T> yp5Var) {
        if ((yp5Var instanceof cq5) || (yp5Var instanceof zzic)) {
            return yp5Var;
        }
        if (yp5Var instanceof Serializable) {
            return new zzic(yp5Var);
        }
        return new cq5(yp5Var);
    }

    public static <T> yp5<T> b(T t) {
        return new zzie(t);
    }
}
