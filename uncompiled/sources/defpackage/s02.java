package defpackage;

import androidx.media3.exoplayer.m;
import androidx.media3.exoplayer.trackselection.c;

/* compiled from: LoadControl.java */
/* renamed from: s02  reason: default package */
/* loaded from: classes.dex */
public interface s02 {
    boolean a();

    long b();

    void c();

    void d(m[] mVarArr, c84 c84Var, c[] cVarArr);

    void e();

    boolean f(long j, float f, boolean z, long j2);

    boolean g(long j, long j2, float f);

    gb h();

    void i();
}
