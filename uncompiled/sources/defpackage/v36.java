package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: v36  reason: default package */
/* loaded from: classes.dex */
public final class v36 implements u36 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.collection.synthetic_data_mitigation", false);

    @Override // defpackage.u36
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
