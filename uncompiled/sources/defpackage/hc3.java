package defpackage;

import android.animation.IntEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;

/* compiled from: ScaleAnimation.java */
/* renamed from: hc3  reason: default package */
/* loaded from: classes2.dex */
public class hc3 extends t20 {
    public int g;
    public float h;
    public ic3 i;

    /* compiled from: ScaleAnimation.java */
    /* renamed from: hc3$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            hc3.this.j(valueAnimator);
        }
    }

    public hc3(xg4.a aVar) {
        super(aVar);
        this.i = new ic3();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void j(ValueAnimator valueAnimator) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COLOR")).intValue();
        int intValue2 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COLOR_REVERSE")).intValue();
        int intValue3 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_SCALE")).intValue();
        int intValue4 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_SCALE_REVERSE")).intValue();
        this.i.c(intValue);
        this.i.d(intValue2);
        this.i.g(intValue3);
        this.i.h(intValue4);
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.i);
        }
    }

    @Override // defpackage.t20, defpackage.nm
    /* renamed from: g */
    public ValueAnimator a() {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(350L);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new a());
        return valueAnimator;
    }

    public PropertyValuesHolder n(boolean z) {
        int i;
        int i2;
        String str;
        if (z) {
            i2 = this.g;
            i = (int) (i2 * this.h);
            str = "ANIMATION_SCALE_REVERSE";
        } else {
            i = this.g;
            i2 = (int) (i * this.h);
            str = "ANIMATION_SCALE";
        }
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt(str, i2, i);
        ofInt.setEvaluator(new IntEvaluator());
        return ofInt;
    }

    public final boolean o(int i, int i2, int i3, float f) {
        return (this.e == i && this.f == i2 && this.g == i3 && this.h == f) ? false : true;
    }

    public hc3 p(int i, int i2, int i3, float f) {
        if (this.c != 0 && o(i, i2, i3, f)) {
            this.e = i;
            this.f = i2;
            this.g = i3;
            this.h = f;
            ((ValueAnimator) this.c).setValues(h(false), h(true), n(false), n(true));
        }
        return this;
    }
}
