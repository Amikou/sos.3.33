package defpackage;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: ArraySet.java */
/* renamed from: uh  reason: default package */
/* loaded from: classes.dex */
public final class uh<E> implements Collection<E>, Set<E> {
    public static final int[] i0 = new int[0];
    public static final Object[] j0 = new Object[0];
    public static Object[] k0;
    public static int l0;
    public static Object[] m0;
    public static int n0;
    public int[] a;
    public Object[] f0;
    public int g0;
    public t32<E, E> h0;

    /* compiled from: ArraySet.java */
    /* renamed from: uh$a */
    /* loaded from: classes.dex */
    public class a extends t32<E, E> {
        public a() {
        }

        @Override // defpackage.t32
        public void a() {
            uh.this.clear();
        }

        @Override // defpackage.t32
        public Object b(int i, int i2) {
            return uh.this.f0[i];
        }

        @Override // defpackage.t32
        public Map<E, E> c() {
            throw new UnsupportedOperationException("not a map");
        }

        @Override // defpackage.t32
        public int d() {
            return uh.this.g0;
        }

        @Override // defpackage.t32
        public int e(Object obj) {
            return uh.this.indexOf(obj);
        }

        @Override // defpackage.t32
        public int f(Object obj) {
            return uh.this.indexOf(obj);
        }

        @Override // defpackage.t32
        public void g(E e, E e2) {
            uh.this.add(e);
        }

        @Override // defpackage.t32
        public void h(int i) {
            uh.this.p(i);
        }

        @Override // defpackage.t32
        public E i(int i, E e) {
            throw new UnsupportedOperationException("not a map");
        }
    }

    public uh() {
        this(0);
    }

    public static void k(int[] iArr, Object[] objArr, int i) {
        if (iArr.length == 8) {
            synchronized (uh.class) {
                if (n0 < 10) {
                    objArr[0] = m0;
                    objArr[1] = iArr;
                    for (int i2 = i - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    m0 = objArr;
                    n0++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (uh.class) {
                if (l0 < 10) {
                    objArr[0] = k0;
                    objArr[1] = iArr;
                    for (int i3 = i - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    k0 = objArr;
                    l0++;
                }
            }
        }
    }

    @Override // java.util.Collection, java.util.Set
    public boolean add(E e) {
        int i;
        int n;
        if (e == null) {
            n = o();
            i = 0;
        } else {
            int hashCode = e.hashCode();
            i = hashCode;
            n = n(e, hashCode);
        }
        if (n >= 0) {
            return false;
        }
        int i2 = ~n;
        int i3 = this.g0;
        int[] iArr = this.a;
        if (i3 >= iArr.length) {
            int i4 = 4;
            if (i3 >= 8) {
                i4 = (i3 >> 1) + i3;
            } else if (i3 >= 4) {
                i4 = 8;
            }
            Object[] objArr = this.f0;
            e(i4);
            int[] iArr2 = this.a;
            if (iArr2.length > 0) {
                System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                System.arraycopy(objArr, 0, this.f0, 0, objArr.length);
            }
            k(iArr, objArr, this.g0);
        }
        int i5 = this.g0;
        if (i2 < i5) {
            int[] iArr3 = this.a;
            int i6 = i2 + 1;
            System.arraycopy(iArr3, i2, iArr3, i6, i5 - i2);
            Object[] objArr2 = this.f0;
            System.arraycopy(objArr2, i2, objArr2, i6, this.g0 - i2);
        }
        this.a[i2] = i;
        this.f0[i2] = e;
        this.g0++;
        return true;
    }

    @Override // java.util.Collection, java.util.Set
    public boolean addAll(Collection<? extends E> collection) {
        i(this.g0 + collection.size());
        boolean z = false;
        for (E e : collection) {
            z |= add(e);
        }
        return z;
    }

    @Override // java.util.Collection, java.util.Set
    public void clear() {
        int i = this.g0;
        if (i != 0) {
            k(this.a, this.f0, i);
            this.a = i0;
            this.f0 = j0;
            this.g0 = 0;
        }
    }

    @Override // java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @Override // java.util.Collection, java.util.Set
    public boolean containsAll(Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    public final void e(int i) {
        if (i == 8) {
            synchronized (uh.class) {
                Object[] objArr = m0;
                if (objArr != null) {
                    this.f0 = objArr;
                    m0 = (Object[]) objArr[0];
                    this.a = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    n0--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (uh.class) {
                Object[] objArr2 = k0;
                if (objArr2 != null) {
                    this.f0 = objArr2;
                    k0 = (Object[]) objArr2[0];
                    this.a = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    l0--;
                    return;
                }
            }
        }
        this.a = new int[i];
        this.f0 = new Object[i];
    }

    @Override // java.util.Collection, java.util.Set
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            if (size() != set.size()) {
                return false;
            }
            for (int i = 0; i < this.g0; i++) {
                try {
                    if (!set.contains(q(i))) {
                        return false;
                    }
                } catch (ClassCastException | NullPointerException unused) {
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.util.Collection, java.util.Set
    public int hashCode() {
        int[] iArr = this.a;
        int i = this.g0;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            i2 += iArr[i3];
        }
        return i2;
    }

    public void i(int i) {
        int[] iArr = this.a;
        if (iArr.length < i) {
            Object[] objArr = this.f0;
            e(i);
            int i2 = this.g0;
            if (i2 > 0) {
                System.arraycopy(iArr, 0, this.a, 0, i2);
                System.arraycopy(objArr, 0, this.f0, 0, this.g0);
            }
            k(iArr, objArr, this.g0);
        }
    }

    public int indexOf(Object obj) {
        return obj == null ? o() : n(obj, obj.hashCode());
    }

    @Override // java.util.Collection, java.util.Set
    public boolean isEmpty() {
        return this.g0 <= 0;
    }

    @Override // java.util.Collection, java.lang.Iterable, java.util.Set
    public Iterator<E> iterator() {
        return m().m().iterator();
    }

    public final t32<E, E> m() {
        if (this.h0 == null) {
            this.h0 = new a();
        }
        return this.h0;
    }

    public final int n(Object obj, int i) {
        int i2 = this.g0;
        if (i2 == 0) {
            return -1;
        }
        int a2 = c70.a(this.a, i2, i);
        if (a2 >= 0 && !obj.equals(this.f0[a2])) {
            int i3 = a2 + 1;
            while (i3 < i2 && this.a[i3] == i) {
                if (obj.equals(this.f0[i3])) {
                    return i3;
                }
                i3++;
            }
            for (int i4 = a2 - 1; i4 >= 0 && this.a[i4] == i; i4--) {
                if (obj.equals(this.f0[i4])) {
                    return i4;
                }
            }
            return ~i3;
        }
        return a2;
    }

    public final int o() {
        int i = this.g0;
        if (i == 0) {
            return -1;
        }
        int a2 = c70.a(this.a, i, 0);
        if (a2 >= 0 && this.f0[a2] != null) {
            int i2 = a2 + 1;
            while (i2 < i && this.a[i2] == 0) {
                if (this.f0[i2] == null) {
                    return i2;
                }
                i2++;
            }
            for (int i3 = a2 - 1; i3 >= 0 && this.a[i3] == 0; i3--) {
                if (this.f0[i3] == null) {
                    return i3;
                }
            }
            return ~i2;
        }
        return a2;
    }

    public E p(int i) {
        Object[] objArr = this.f0;
        E e = (E) objArr[i];
        int i2 = this.g0;
        if (i2 <= 1) {
            k(this.a, objArr, i2);
            this.a = i0;
            this.f0 = j0;
            this.g0 = 0;
        } else {
            int[] iArr = this.a;
            if (iArr.length > 8 && i2 < iArr.length / 3) {
                e(i2 > 8 ? i2 + (i2 >> 1) : 8);
                this.g0--;
                if (i > 0) {
                    System.arraycopy(iArr, 0, this.a, 0, i);
                    System.arraycopy(objArr, 0, this.f0, 0, i);
                }
                int i3 = this.g0;
                if (i < i3) {
                    int i4 = i + 1;
                    System.arraycopy(iArr, i4, this.a, i, i3 - i);
                    System.arraycopy(objArr, i4, this.f0, i, this.g0 - i);
                }
            } else {
                int i5 = i2 - 1;
                this.g0 = i5;
                if (i < i5) {
                    int i6 = i + 1;
                    System.arraycopy(iArr, i6, iArr, i, i5 - i);
                    Object[] objArr2 = this.f0;
                    System.arraycopy(objArr2, i6, objArr2, i, this.g0 - i);
                }
                this.f0[this.g0] = null;
            }
        }
        return e;
    }

    public E q(int i) {
        return (E) this.f0[i];
    }

    @Override // java.util.Collection, java.util.Set
    public boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf >= 0) {
            p(indexOf);
            return true;
        }
        return false;
    }

    @Override // java.util.Collection, java.util.Set
    public boolean removeAll(Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        boolean z = false;
        while (it.hasNext()) {
            z |= remove(it.next());
        }
        return z;
    }

    @Override // java.util.Collection, java.util.Set
    public boolean retainAll(Collection<?> collection) {
        boolean z = false;
        for (int i = this.g0 - 1; i >= 0; i--) {
            if (!collection.contains(this.f0[i])) {
                p(i);
                z = true;
            }
        }
        return z;
    }

    @Override // java.util.Collection, java.util.Set
    public int size() {
        return this.g0;
    }

    @Override // java.util.Collection, java.util.Set
    public Object[] toArray() {
        int i = this.g0;
        Object[] objArr = new Object[i];
        System.arraycopy(this.f0, 0, objArr, 0, i);
        return objArr;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.g0 * 14);
        sb.append('{');
        for (int i = 0; i < this.g0; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            E q = q(i);
            if (q != this) {
                sb.append(q);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public uh(int i) {
        if (i == 0) {
            this.a = i0;
            this.f0 = j0;
        } else {
            e(i);
        }
        this.g0 = 0;
    }

    @Override // java.util.Collection, java.util.Set
    public <T> T[] toArray(T[] tArr) {
        if (tArr.length < this.g0) {
            tArr = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.g0));
        }
        System.arraycopy(this.f0, 0, tArr, 0, this.g0);
        int length = tArr.length;
        int i = this.g0;
        if (length > i) {
            tArr[i] = null;
        }
        return tArr;
    }
}
