package defpackage;

import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: gt5  reason: default package */
/* loaded from: classes.dex */
public interface gt5<E> extends List<E>, RandomAccess {
    gt5<E> d(int i);

    boolean zza();

    void zzb();
}
