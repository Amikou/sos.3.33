package defpackage;

import android.content.Context;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.producers.n;
import defpackage.i90;
import defpackage.l72;
import java.util.Set;

/* compiled from: ImagePipelineConfigInterface.java */
/* renamed from: so1  reason: default package */
/* loaded from: classes.dex */
public interface so1 {
    fw3<m72> A();

    tn1 B();

    to1 C();

    fw3<m72> D();

    wy0 E();

    xs2 a();

    Set<i73> b();

    int c();

    fw3<Boolean> d();

    l31 e();

    b00 f();

    cq g();

    Context getContext();

    n h();

    l72<wt, PooledByteBuffer> i();

    ap0 j();

    Set<h73> k();

    xt l();

    boolean m();

    l72.a n();

    qv2 o();

    ap0 p();

    qn1 q();

    i90.b<wt> r();

    boolean s();

    xl3 t();

    Integer u();

    bp1 v();

    r72 w();

    un1 x();

    boolean y();

    av z();
}
