package defpackage;

import java.util.Queue;

/* compiled from: ModelCache.java */
/* renamed from: i92  reason: default package */
/* loaded from: classes.dex */
public class i92<A, B> {
    public final s22<b<A>, B> a;

    /* compiled from: ModelCache.java */
    /* renamed from: i92$a */
    /* loaded from: classes.dex */
    public class a extends s22<b<A>, B> {
        public a(i92 i92Var, long j) {
            super(j);
        }

        @Override // defpackage.s22
        /* renamed from: n */
        public void j(b<A> bVar, B b) {
            bVar.c();
        }
    }

    /* compiled from: ModelCache.java */
    /* renamed from: i92$b */
    /* loaded from: classes.dex */
    public static final class b<A> {
        public static final Queue<b<?>> d = mg4.f(0);
        public int a;
        public int b;
        public A c;

        public static <A> b<A> a(A a, int i, int i2) {
            b<A> bVar;
            Queue<b<?>> queue = d;
            synchronized (queue) {
                bVar = (b<A>) queue.poll();
            }
            if (bVar == null) {
                bVar = new b<>();
            }
            bVar.b(a, i, i2);
            return bVar;
        }

        public final void b(A a, int i, int i2) {
            this.c = a;
            this.b = i;
            this.a = i2;
        }

        public void c() {
            Queue<b<?>> queue = d;
            synchronized (queue) {
                queue.offer(this);
            }
        }

        public boolean equals(Object obj) {
            if (obj instanceof b) {
                b bVar = (b) obj;
                return this.b == bVar.b && this.a == bVar.a && this.c.equals(bVar.c);
            }
            return false;
        }

        public int hashCode() {
            return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
        }
    }

    public i92(long j) {
        this.a = new a(this, j);
    }

    public B a(A a2, int i, int i2) {
        b<A> a3 = b.a(a2, i, i2);
        B g = this.a.g(a3);
        a3.c();
        return g;
    }

    public void b(A a2, int i, int i2, B b2) {
        this.a.k(b.a(a2, i, i2), b2);
    }
}
