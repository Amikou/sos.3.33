package defpackage;

import androidx.paging.multicast.ChannelManager;
import defpackage.rr;
import java.util.ArrayDeque;

/* compiled from: ChannelManager.kt */
/* renamed from: tr  reason: default package */
/* loaded from: classes.dex */
public final class tr<T> implements rr<T> {
    public final ArrayDeque<ChannelManager.b.AbstractC0053b.c<T>> a;
    public final int b;

    public tr(int i) {
        this.b = i;
        this.a = new ArrayDeque<>(u33.d(i, 10));
    }

    @Override // defpackage.rr
    public void a(ChannelManager.b.AbstractC0053b.c<T> cVar) {
        fs1.f(cVar, "item");
        while (b().size() >= this.b) {
            b().pollFirst();
        }
        b().offerLast(cVar);
    }

    @Override // defpackage.rr
    /* renamed from: c */
    public ArrayDeque<ChannelManager.b.AbstractC0053b.c<T>> b() {
        return this.a;
    }

    @Override // defpackage.rr
    public boolean isEmpty() {
        return rr.a.a(this);
    }
}
