package defpackage;

import java.util.regex.Pattern;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: ChainValidation.kt */
/* renamed from: hx  reason: default package */
/* loaded from: classes2.dex */
public final class hx {
    public static final boolean a(String str, TokenType tokenType) {
        fs1.f(str, "<this>");
        fs1.f(tokenType, "tokenType");
        return Pattern.compile("^0x[0-9a-fA-F]{40}$").matcher(str).matches();
    }
}
