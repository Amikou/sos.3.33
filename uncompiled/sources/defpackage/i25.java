package defpackage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: i25  reason: default package */
/* loaded from: classes.dex */
public interface i25 {
    ExecutorService a(int i, ThreadFactory threadFactory, int i2);

    ExecutorService b(ThreadFactory threadFactory, int i);
}
