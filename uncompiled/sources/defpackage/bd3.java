package defpackage;

import java.util.concurrent.TimeUnit;

/* compiled from: Scheduler.java */
/* renamed from: bd3  reason: default package */
/* loaded from: classes2.dex */
public abstract class bd3 {

    /* compiled from: Scheduler.java */
    /* renamed from: bd3$a */
    /* loaded from: classes2.dex */
    public static abstract class a implements xp0 {
    }

    static {
        TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15L).longValue());
    }
}
