package defpackage;

import android.app.Activity;
import android.content.Context;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import defpackage.qm1;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.R;

/* compiled from: FingerprintBiometricAuthUseCase.kt */
/* renamed from: w41  reason: default package */
/* loaded from: classes2.dex */
public final class w41 implements qm1 {
    public BiometricPrompt a;
    public qm1.a b;
    public WeakReference<Activity> c;
    public final BiometricPrompt.a d = new a();

    /* compiled from: FingerprintBiometricAuthUseCase.kt */
    /* renamed from: w41$a */
    /* loaded from: classes2.dex */
    public static final class a extends BiometricPrompt.a {
        public a() {
        }

        @Override // androidx.biometric.BiometricPrompt.a
        public void a(int i, CharSequence charSequence) {
            fs1.f(charSequence, "errString");
            super.a(i, charSequence);
            BiometricPrompt biometricPrompt = w41.this.a;
            if (biometricPrompt != null) {
                biometricPrompt.d();
            }
            WeakReference weakReference = w41.this.c;
            if (weakReference != null) {
                pg4.b((Activity) weakReference.get(), Boolean.FALSE);
            }
            if (i == 10 || i == 11) {
                qm1.a aVar = w41.this.b;
                if (aVar == null) {
                    return;
                }
                aVar.b(i);
                return;
            }
            String str = "AuthenticationCallback: error(" + i + ") " + ((Object) charSequence);
            String simpleName = w41.this.getClass().getSimpleName();
            fs1.e(simpleName, "this@FingerprintBiometri…se::class.java.simpleName");
            e30.c0(str, simpleName);
        }

        @Override // androidx.biometric.BiometricPrompt.a
        public void c(BiometricPrompt.b bVar) {
            fs1.f(bVar, "result");
            super.c(bVar);
            qm1.a aVar = w41.this.b;
            if (aVar != null) {
                aVar.a();
            }
            WeakReference weakReference = w41.this.c;
            if (weakReference == null) {
                return;
            }
            pg4.b((Activity) weakReference.get(), Boolean.FALSE);
        }
    }

    @Override // defpackage.qm1
    public BiometricPrompt a(Fragment fragment, qm1.a aVar) {
        fs1.f(fragment, "fragment");
        fs1.f(aVar, "callback");
        this.b = aVar;
        WeakReference<Activity> weakReference = new WeakReference<>(fragment.requireActivity());
        this.c = weakReference;
        pg4.b(weakReference.get(), Boolean.TRUE);
        BiometricPrompt biometricPrompt = new BiometricPrompt(fragment, this.d);
        this.a = biometricPrompt;
        fs1.d(biometricPrompt);
        Context applicationContext = fragment.requireActivity().getApplicationContext();
        fs1.e(applicationContext, "fragment.requireActivity().applicationContext");
        biometricPrompt.b(f(applicationContext));
        BiometricPrompt biometricPrompt2 = this.a;
        fs1.d(biometricPrompt2);
        return biometricPrompt2;
    }

    @Override // defpackage.qm1
    public BiometricPrompt b(FragmentActivity fragmentActivity, qm1.a aVar) {
        fs1.f(fragmentActivity, "activity");
        fs1.f(aVar, "callback");
        this.b = aVar;
        WeakReference<Activity> weakReference = new WeakReference<>(fragmentActivity);
        this.c = weakReference;
        pg4.b(weakReference.get(), Boolean.TRUE);
        BiometricPrompt biometricPrompt = new BiometricPrompt(fragmentActivity, this.d);
        this.a = biometricPrompt;
        fs1.d(biometricPrompt);
        Context applicationContext = fragmentActivity.getApplicationContext();
        fs1.e(applicationContext, "activity.applicationContext");
        biometricPrompt.b(f(applicationContext));
        BiometricPrompt biometricPrompt2 = this.a;
        fs1.d(biometricPrompt2);
        return biometricPrompt2;
    }

    public final BiometricPrompt.d f(Context context) {
        BiometricPrompt.d a2 = new BiometricPrompt.d.a().e(context.getText(R.string.biometric_dialog_title)).d(context.getText(R.string.biometric_dialog_content)).b(33023).c(false).a();
        fs1.e(a2, "Builder()\n            .s…lse)\n            .build()");
        return a2;
    }
}
