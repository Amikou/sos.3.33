package defpackage;

import com.facebook.common.internal.ImmutableMap;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: BitmapMemoryCacheProducer.java */
/* renamed from: gq  reason: default package */
/* loaded from: classes.dex */
public class gq implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final l72<wt, com.facebook.imagepipeline.image.a> a;
    public final xt b;
    public final dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> c;

    /* compiled from: BitmapMemoryCacheProducer.java */
    /* renamed from: gq$a */
    /* loaded from: classes.dex */
    public class a extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final /* synthetic */ wt c;
        public final /* synthetic */ boolean d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l60 l60Var, wt wtVar, boolean z) {
            super(l60Var);
            this.c = wtVar;
            this.d = z;
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar2;
            boolean d;
            try {
                if (nc1.d()) {
                    nc1.a("BitmapMemoryCacheProducer#onNewResultImpl");
                }
                boolean e = qm.e(i);
                if (aVar == null) {
                    if (e) {
                        p().d(null, i);
                    }
                    if (d) {
                        return;
                    }
                    return;
                }
                if (!aVar.j().c() && !qm.n(i, 8)) {
                    if (!e && (aVar2 = gq.this.a.get(this.c)) != null) {
                        xw2 a = aVar.j().a();
                        xw2 a2 = aVar2.j().a();
                        if (!a2.a() && a2.c() < a.c()) {
                            com.facebook.common.references.a.g(aVar2);
                        } else {
                            p().d(aVar2, i);
                            com.facebook.common.references.a.g(aVar2);
                            if (nc1.d()) {
                                nc1.b();
                                return;
                            }
                            return;
                        }
                    }
                    com.facebook.common.references.a<com.facebook.imagepipeline.image.a> c = this.d ? gq.this.a.c(this.c, aVar) : null;
                    if (e) {
                        p().c(1.0f);
                    }
                    l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> p = p();
                    if (c != null) {
                        aVar = c;
                    }
                    p.d(aVar, i);
                    com.facebook.common.references.a.g(c);
                    if (nc1.d()) {
                        nc1.b();
                        return;
                    }
                    return;
                }
                p().d(aVar, i);
                if (nc1.d()) {
                    nc1.b();
                }
            } finally {
                if (nc1.d()) {
                    nc1.b();
                }
            }
        }
    }

    public gq(l72<wt, com.facebook.imagepipeline.image.a> l72Var, xt xtVar, dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dv2Var) {
        this.a = l72Var;
        this.b = xtVar;
        this.c = dv2Var;
    }

    public static void e(zj1 zj1Var, ev2 ev2Var) {
        ev2Var.g(zj1Var.getExtras());
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        boolean d;
        try {
            if (nc1.d()) {
                nc1.a("BitmapMemoryCacheProducer#produceResults");
            }
            iv2 l = ev2Var.l();
            l.k(ev2Var, d());
            wt a2 = this.b.a(ev2Var.c(), ev2Var.a());
            com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar = ev2Var.c().x(1) ? this.a.get(a2) : null;
            if (aVar != null) {
                e(aVar.j(), ev2Var);
                boolean a3 = aVar.j().a().a();
                if (a3) {
                    l.a(ev2Var, d(), l.j(ev2Var, d()) ? ImmutableMap.of("cached_value_found", "true") : null);
                    l.e(ev2Var, d(), true);
                    ev2Var.f("memory_bitmap", c());
                    l60Var.c(1.0f);
                }
                l60Var.d(aVar, qm.l(a3));
                aVar.close();
                if (a3) {
                    if (d) {
                        return;
                    }
                    return;
                }
            }
            if (ev2Var.n().getValue() >= ImageRequest.RequestLevel.BITMAP_MEMORY_CACHE.getValue()) {
                l.a(ev2Var, d(), l.j(ev2Var, d()) ? ImmutableMap.of("cached_value_found", "false") : null);
                l.e(ev2Var, d(), false);
                ev2Var.f("memory_bitmap", c());
                l60Var.d(null, 1);
                if (nc1.d()) {
                    nc1.b();
                    return;
                }
                return;
            }
            l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> f = f(l60Var, a2, ev2Var.c().x(2));
            l.a(ev2Var, d(), l.j(ev2Var, d()) ? ImmutableMap.of("cached_value_found", "false") : null);
            if (nc1.d()) {
                nc1.a("mInputProducer.produceResult");
            }
            this.c.a(f, ev2Var);
            if (nc1.d()) {
                nc1.b();
            }
            if (nc1.d()) {
                nc1.b();
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public String c() {
        return "pipe_bg";
    }

    public String d() {
        return "BitmapMemoryCacheProducer";
    }

    public l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> f(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, wt wtVar, boolean z) {
        return new a(l60Var, wtVar, z);
    }
}
