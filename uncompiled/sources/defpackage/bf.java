package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/* compiled from: AppCompatBackgroundHelper.java */
/* renamed from: bf  reason: default package */
/* loaded from: classes.dex */
public class bf {
    public final View a;
    public k64 d;
    public k64 e;
    public k64 f;
    public int c = -1;
    public final gf b = gf.b();

    public bf(View view) {
        this.a = view;
    }

    public final boolean a(Drawable drawable) {
        if (this.f == null) {
            this.f = new k64();
        }
        k64 k64Var = this.f;
        k64Var.a();
        ColorStateList u = ei4.u(this.a);
        if (u != null) {
            k64Var.d = true;
            k64Var.a = u;
        }
        PorterDuff.Mode v = ei4.v(this.a);
        if (v != null) {
            k64Var.c = true;
            k64Var.b = v;
        }
        if (k64Var.d || k64Var.c) {
            gf.i(drawable, k64Var, this.a.getDrawableState());
            return true;
        }
        return false;
    }

    public void b() {
        Drawable background = this.a.getBackground();
        if (background != null) {
            if (k() && a(background)) {
                return;
            }
            k64 k64Var = this.e;
            if (k64Var != null) {
                gf.i(background, k64Var, this.a.getDrawableState());
                return;
            }
            k64 k64Var2 = this.d;
            if (k64Var2 != null) {
                gf.i(background, k64Var2, this.a.getDrawableState());
            }
        }
    }

    public ColorStateList c() {
        k64 k64Var = this.e;
        if (k64Var != null) {
            return k64Var.a;
        }
        return null;
    }

    public PorterDuff.Mode d() {
        k64 k64Var = this.e;
        if (k64Var != null) {
            return k64Var.b;
        }
        return null;
    }

    public void e(AttributeSet attributeSet, int i) {
        Context context = this.a.getContext();
        int[] iArr = d33.ViewBackgroundHelper;
        l64 v = l64.v(context, attributeSet, iArr, i, 0);
        View view = this.a;
        ei4.r0(view, view.getContext(), iArr, attributeSet, v.r(), i, 0);
        try {
            int i2 = d33.ViewBackgroundHelper_android_background;
            if (v.s(i2)) {
                this.c = v.n(i2, -1);
                ColorStateList f = this.b.f(this.a.getContext(), this.c);
                if (f != null) {
                    h(f);
                }
            }
            int i3 = d33.ViewBackgroundHelper_backgroundTint;
            if (v.s(i3)) {
                ei4.x0(this.a, v.c(i3));
            }
            int i4 = d33.ViewBackgroundHelper_backgroundTintMode;
            if (v.s(i4)) {
                ei4.y0(this.a, dr0.e(v.k(i4, -1), null));
            }
        } finally {
            v.w();
        }
    }

    public void f(Drawable drawable) {
        this.c = -1;
        h(null);
        b();
    }

    public void g(int i) {
        this.c = i;
        gf gfVar = this.b;
        h(gfVar != null ? gfVar.f(this.a.getContext(), i) : null);
        b();
    }

    public void h(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.d == null) {
                this.d = new k64();
            }
            k64 k64Var = this.d;
            k64Var.a = colorStateList;
            k64Var.d = true;
        } else {
            this.d = null;
        }
        b();
    }

    public void i(ColorStateList colorStateList) {
        if (this.e == null) {
            this.e = new k64();
        }
        k64 k64Var = this.e;
        k64Var.a = colorStateList;
        k64Var.d = true;
        b();
    }

    public void j(PorterDuff.Mode mode) {
        if (this.e == null) {
            this.e = new k64();
        }
        k64 k64Var = this.e;
        k64Var.b = mode;
        k64Var.c = true;
        b();
    }

    public final boolean k() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.d != null : i == 21;
    }
}
