package defpackage;

import java.util.concurrent.Future;

/* compiled from: Future.kt */
/* renamed from: hv  reason: default package */
/* loaded from: classes2.dex */
public final class hv extends iv {
    public final Future<?> a;

    public hv(Future<?> future) {
        this.a = future;
    }

    @Override // defpackage.jv
    public void a(Throwable th) {
        this.a.cancel(false);
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        a(th);
        return te4.a;
    }

    public String toString() {
        return "CancelFutureOnCancel[" + this.a + ']';
    }
}
