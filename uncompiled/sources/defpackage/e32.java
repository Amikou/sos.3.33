package defpackage;

import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: MainCoroutineDispatcher.kt */
/* renamed from: e32  reason: default package */
/* loaded from: classes2.dex */
public abstract class e32 extends CoroutineDispatcher {
    public abstract e32 l();

    public final String m() {
        e32 e32Var;
        tp0 tp0Var = tp0.a;
        e32 c = tp0.c();
        if (this == c) {
            return "Dispatchers.Main";
        }
        try {
            e32Var = c.l();
        } catch (UnsupportedOperationException unused) {
            e32Var = null;
        }
        if (this == e32Var) {
            return "Dispatchers.Main.immediate";
        }
        return null;
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        String m = m();
        if (m == null) {
            return ff0.a(this) + '@' + ff0.b(this);
        }
        return m;
    }
}
