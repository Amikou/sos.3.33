package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: dq5  reason: default package */
/* loaded from: classes.dex */
public final class dq5 implements Runnable {
    public final /* synthetic */ Bundle a;
    public final /* synthetic */ bq5 f0;
    public final /* synthetic */ bq5 g0;
    public final /* synthetic */ long h0;
    public final /* synthetic */ qq5 i0;

    public dq5(qq5 qq5Var, Bundle bundle, bq5 bq5Var, bq5 bq5Var2, long j) {
        this.i0 = qq5Var;
        this.a = bundle;
        this.f0 = bq5Var;
        this.g0 = bq5Var2;
        this.h0 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        qq5.E(this.i0, this.a, this.f0, this.g0, this.h0);
    }
}
