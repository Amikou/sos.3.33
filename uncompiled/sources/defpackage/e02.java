package defpackage;

import defpackage.h5;
import java.util.ArrayList;
import kotlinx.coroutines.channels.AbstractChannel;
import kotlinx.coroutines.internal.OnUndeliveredElementKt;
import kotlinx.coroutines.internal.UndeliveredElementException;

/* compiled from: LinkedListChannel.kt */
/* renamed from: e02  reason: default package */
/* loaded from: classes2.dex */
public class e02<E> extends AbstractChannel<E> {
    public e02(tc1<? super E, te4> tc1Var) {
        super(tc1Var);
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean G() {
        return true;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean H() {
        return true;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public void K(Object obj, d00<?> d00Var) {
        UndeliveredElementException undeliveredElementException = null;
        if (obj != null) {
            if (!(obj instanceof ArrayList)) {
                tj3 tj3Var = (tj3) obj;
                if (tj3Var instanceof h5.a) {
                    tc1<E, te4> tc1Var = this.a;
                    if (tc1Var != null) {
                        undeliveredElementException = OnUndeliveredElementKt.c(tc1Var, ((h5.a) tj3Var).h0, null);
                    }
                } else {
                    tj3Var.A(d00Var);
                }
            } else {
                ArrayList arrayList = (ArrayList) obj;
                int size = arrayList.size() - 1;
                if (size >= 0) {
                    UndeliveredElementException undeliveredElementException2 = null;
                    while (true) {
                        int i = size - 1;
                        tj3 tj3Var2 = (tj3) arrayList.get(size);
                        if (tj3Var2 instanceof h5.a) {
                            tc1<E, te4> tc1Var2 = this.a;
                            undeliveredElementException2 = tc1Var2 == null ? null : OnUndeliveredElementKt.c(tc1Var2, ((h5.a) tj3Var2).h0, undeliveredElementException2);
                        } else {
                            tj3Var2.A(d00Var);
                        }
                        if (i < 0) {
                            break;
                        }
                        size = i;
                    }
                    undeliveredElementException = undeliveredElementException2;
                }
            }
        }
        if (undeliveredElementException != null) {
            throw undeliveredElementException;
        }
    }

    @Override // defpackage.h5
    public final boolean s() {
        return false;
    }

    @Override // defpackage.h5
    public final boolean t() {
        return false;
    }

    @Override // defpackage.h5
    public Object v(E e) {
        l43<?> x;
        do {
            Object v = super.v(e);
            k24 k24Var = q4.b;
            if (v == k24Var) {
                return k24Var;
            }
            if (v == q4.c) {
                x = x(e);
                if (x == null) {
                    return k24Var;
                }
            } else if (v instanceof d00) {
                return v;
            } else {
                throw new IllegalStateException(fs1.l("Invalid offerInternal result ", v).toString());
            }
        } while (!(x instanceof d00));
        return x;
    }
}
