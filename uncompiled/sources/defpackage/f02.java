package defpackage;

/* compiled from: LinkedNode.java */
/* renamed from: f02  reason: default package */
/* loaded from: classes.dex */
public final class f02<T> {
    public final T a;
    public f02<T> b;

    public f02(T t, f02<T> f02Var) {
        this.a = t;
        this.b = f02Var;
    }

    public static <ST> boolean a(f02<ST> f02Var, ST st) {
        while (f02Var != null) {
            if (f02Var.d() == st) {
                return true;
            }
            f02Var = f02Var.c();
        }
        return false;
    }

    public void b(f02<T> f02Var) {
        if (this.b == null) {
            this.b = f02Var;
            return;
        }
        throw new IllegalStateException();
    }

    public f02<T> c() {
        return this.b;
    }

    public T d() {
        return this.a;
    }
}
