package defpackage;

import androidx.paging.multicast.ChannelManager;
import defpackage.rr;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/* compiled from: ChannelManager.kt */
/* renamed from: hg2  reason: default package */
/* loaded from: classes.dex */
public final class hg2<T> implements rr<T> {
    @Override // defpackage.rr
    public void a(ChannelManager.b.AbstractC0053b.c<T> cVar) {
        fs1.f(cVar, "item");
    }

    @Override // defpackage.rr
    public Collection<ChannelManager.b.AbstractC0053b.c<T>> b() {
        List emptyList = Collections.emptyList();
        fs1.e(emptyList, "Collections.emptyList()");
        return emptyList;
    }

    @Override // defpackage.rr
    public boolean isEmpty() {
        return rr.a.a(this);
    }
}
