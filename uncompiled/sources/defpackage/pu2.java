package defpackage;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: PrfAesCmac.java */
/* renamed from: pu2  reason: default package */
/* loaded from: classes2.dex */
public final class pu2 implements ou2 {
    public final SecretKey a;
    public byte[] b;
    public byte[] c;

    public pu2(byte[] bArr) throws GeneralSecurityException {
        ug4.a(bArr.length);
        this.a = new SecretKeySpec(bArr, "AES");
        b();
    }

    public static Cipher c() throws GeneralSecurityException {
        return lv0.f.a("AES/ECB/NoPadding");
    }

    @Override // defpackage.ou2
    public byte[] a(byte[] bArr, int i) throws GeneralSecurityException {
        byte[] e;
        if (i <= 16) {
            Cipher c = c();
            c.init(1, this.a);
            int max = Math.max(1, (int) Math.ceil(bArr.length / 16.0d));
            if (max * 16 == bArr.length) {
                e = at.d(bArr, (max - 1) * 16, this.b, 0, 16);
            } else {
                e = at.e(qa.a(Arrays.copyOfRange(bArr, (max - 1) * 16, bArr.length)), this.c);
            }
            byte[] bArr2 = new byte[16];
            for (int i2 = 0; i2 < max - 1; i2++) {
                bArr2 = c.doFinal(at.d(bArr2, 0, bArr, i2 * 16, 16));
            }
            return Arrays.copyOf(c.doFinal(at.e(e, bArr2)), i);
        }
        throw new InvalidAlgorithmParameterException("outputLength too large, max is 16 bytes");
    }

    public final void b() throws GeneralSecurityException {
        Cipher c = c();
        c.init(1, this.a);
        byte[] b = qa.b(c.doFinal(new byte[16]));
        this.b = b;
        this.c = qa.b(b);
    }
}
