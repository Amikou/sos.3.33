package defpackage;

import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: LoaderManager.java */
/* renamed from: a12  reason: default package */
/* loaded from: classes.dex */
public abstract class a12 {
    public static <T extends rz1 & hj4> a12 b(T t) {
        return new b12(t, t.getViewModelStore());
    }

    @Deprecated
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract void c();
}
