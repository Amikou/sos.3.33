package defpackage;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import android.util.Xml;
import androidx.media3.common.DrmInitData;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.common.util.c;
import androidx.media3.exoplayer.upstream.d;
import androidx.media3.extractor.metadata.emsg.EventMessage;
import androidx.recyclerview.widget.RecyclerView;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import defpackage.cj3;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.internal.http2.Http2;
import org.web3j.ens.contracts.generated.PublicResolver;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;
import zendesk.support.request.CellBase;

/* compiled from: DashManifestParser.java */
/* renamed from: td0  reason: default package */
/* loaded from: classes.dex */
public class td0 extends DefaultHandler implements d.a<sd0> {
    public static final Pattern b = Pattern.compile("(\\d+)(?:/(\\d+))?");
    public static final Pattern c = Pattern.compile("CC([1-4])=.*");
    public static final Pattern d = Pattern.compile("([1-9]|[1-5][0-9]|6[0-3])=.*");
    public static final int[] e = {-1, 1, 2, 3, 4, 5, 6, 8, 2, 3, 4, 7, 8, 24, 8, 12, 10, 12, 14, 12, 14};
    public final XmlPullParserFactory a;

    /* compiled from: DashManifestParser.java */
    /* renamed from: td0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final j a;
        public final ImmutableList<bo> b;
        public final cj3 c;
        public final String d;
        public final ArrayList<DrmInitData.SchemeData> e;
        public final ArrayList<nm0> f;
        public final long g;
        public final List<nm0> h;
        public final List<nm0> i;

        public a(j jVar, List<bo> list, cj3 cj3Var, String str, ArrayList<DrmInitData.SchemeData> arrayList, ArrayList<nm0> arrayList2, List<nm0> list2, List<nm0> list3, long j) {
            this.a = jVar;
            this.b = ImmutableList.copyOf((Collection) list);
            this.c = cj3Var;
            this.d = str;
            this.e = arrayList;
            this.f = arrayList2;
            this.h = list2;
            this.i = list3;
            this.g = j;
        }
    }

    public td0() {
        try {
            this.a = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e2) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e2);
        }
    }

    public static int D(List<nm0> list) {
        String str;
        for (int i = 0; i < list.size(); i++) {
            nm0 nm0Var = list.get(i);
            if ("urn:scte:dash:cc:cea-608:2015".equals(nm0Var.a) && (str = nm0Var.b) != null) {
                Matcher matcher = c.matcher(str);
                if (matcher.matches()) {
                    return Integer.parseInt(matcher.group(1));
                }
                p12.i("MpdParser", "Unable to parse CEA-608 channel number from: " + nm0Var.b);
            }
        }
        return -1;
    }

    public static int E(List<nm0> list) {
        String str;
        for (int i = 0; i < list.size(); i++) {
            nm0 nm0Var = list.get(i);
            if ("urn:scte:dash:cc:cea-708:2015".equals(nm0Var.a) && (str = nm0Var.b) != null) {
                Matcher matcher = d.matcher(str);
                if (matcher.matches()) {
                    return Integer.parseInt(matcher.group(1));
                }
                p12.i("MpdParser", "Unable to parse CEA-708 service block number from: " + nm0Var.b);
            }
        }
        return -1;
    }

    public static long H(XmlPullParser xmlPullParser, String str, long j) throws ParserException {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? j : b.E0(attributeValue);
    }

    public static nm0 I(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        String r0 = r0(xmlPullParser, "schemeIdUri", "");
        String r02 = r0(xmlPullParser, "value", null);
        String r03 = r0(xmlPullParser, "id", null);
        do {
            xmlPullParser.next();
        } while (!c.d(xmlPullParser, str));
        return new nm0(r0, r02, r03);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static int J(XmlPullParser xmlPullParser) {
        char c2;
        String attributeValue = xmlPullParser.getAttributeValue(null, "value");
        if (attributeValue == null) {
            return -1;
        }
        String e2 = ei.e(attributeValue);
        e2.hashCode();
        switch (e2.hashCode()) {
            case 1596796:
                if (e2.equals("4000")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case 2937391:
                if (e2.equals("a000")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 3094035:
                if (e2.equals("f801")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case 3133436:
                if (e2.equals("fa01")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 6;
            case 3:
                return 8;
            default:
                return -1;
        }
    }

    public static int K(XmlPullParser xmlPullParser) {
        int U = U(xmlPullParser, "value", -1);
        if (U <= 0 || U >= 33) {
            return -1;
        }
        return U;
    }

    public static int L(XmlPullParser xmlPullParser) {
        int bitCount;
        String attributeValue = xmlPullParser.getAttributeValue(null, "value");
        if (attributeValue == null || (bitCount = Integer.bitCount(Integer.parseInt(attributeValue, 16))) == 0) {
            return -1;
        }
        return bitCount;
    }

    public static long M(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? j : b.F0(attributeValue);
    }

    public static String N(List<nm0> list) {
        for (int i = 0; i < list.size(); i++) {
            nm0 nm0Var = list.get(i);
            String str = nm0Var.a;
            if ("tag:dolby.com,2018:dash:EC3_ExtensionType:2018".equals(str) && "JOC".equals(nm0Var.b)) {
                return "audio/eac3-joc";
            }
            if ("tag:dolby.com,2014:dash:DolbyDigitalPlusExtensionType:2014".equals(str) && "ec+3".equals(nm0Var.b)) {
                return "audio/eac3-joc";
            }
        }
        return "audio/eac3";
    }

    public static float R(XmlPullParser xmlPullParser, String str, float f) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? f : Float.parseFloat(attributeValue);
    }

    public static float S(XmlPullParser xmlPullParser, float f) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "frameRate");
        if (attributeValue != null) {
            Matcher matcher = b.matcher(attributeValue);
            if (matcher.matches()) {
                int parseInt = Integer.parseInt(matcher.group(1));
                String group = matcher.group(2);
                return !TextUtils.isEmpty(group) ? parseInt / Integer.parseInt(group) : parseInt;
            }
            return f;
        }
        return f;
    }

    public static int U(XmlPullParser xmlPullParser, String str, int i) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? i : Integer.parseInt(attributeValue);
    }

    public static long W(List<nm0> list) {
        for (int i = 0; i < list.size(); i++) {
            nm0 nm0Var = list.get(i);
            if (ei.a("http://dashif.org/guidelines/last-segment-number", nm0Var.a)) {
                return Long.parseLong(nm0Var.b);
            }
        }
        return -1L;
    }

    public static long X(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? j : Long.parseLong(attributeValue);
    }

    public static int Z(XmlPullParser xmlPullParser) {
        int U = U(xmlPullParser, "value", -1);
        if (U >= 0) {
            int[] iArr = e;
            if (U < iArr.length) {
                return iArr[U];
            }
            return -1;
        }
        return -1;
    }

    public static int p(int i, int i2) {
        if (i == -1) {
            return i2;
        }
        if (i2 == -1) {
            return i;
        }
        ii.g(i == i2);
        return i;
    }

    public static String q(String str, String str2) {
        if (str == null) {
            return str2;
        }
        if (str2 == null) {
            return str;
        }
        ii.g(str.equals(str2));
        return str;
    }

    public static void r(ArrayList<DrmInitData.SchemeData> arrayList) {
        String str;
        int i = 0;
        while (true) {
            if (i >= arrayList.size()) {
                str = null;
                break;
            }
            DrmInitData.SchemeData schemeData = arrayList.get(i);
            if (ft.c.equals(schemeData.f0) && (str = schemeData.g0) != null) {
                arrayList.remove(i);
                break;
            }
            i++;
        }
        if (str == null) {
            return;
        }
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            DrmInitData.SchemeData schemeData2 = arrayList.get(i2);
            if (ft.b.equals(schemeData2.f0) && schemeData2.g0 == null) {
                arrayList.set(i2, new DrmInitData.SchemeData(ft.c, str, schemeData2.h0, schemeData2.i0));
            }
        }
    }

    public static String r0(XmlPullParser xmlPullParser, String str, String str2) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? str2 : attributeValue;
    }

    public static void s(ArrayList<DrmInitData.SchemeData> arrayList) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            DrmInitData.SchemeData schemeData = arrayList.get(size);
            if (!schemeData.c()) {
                int i = 0;
                while (true) {
                    if (i >= arrayList.size()) {
                        break;
                    } else if (arrayList.get(i).a(schemeData)) {
                        arrayList.remove(size);
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
    }

    public static String s0(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        String str2 = "";
        do {
            xmlPullParser.next();
            if (xmlPullParser.getEventType() == 4) {
                str2 = xmlPullParser.getText();
            } else {
                w(xmlPullParser);
            }
        } while (!c.d(xmlPullParser, str));
        return str2;
    }

    public static long t(long j, long j2) {
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j = j2;
        }
        return j == Long.MAX_VALUE ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : j;
    }

    public static String u(String str, String str2) {
        if (y82.m(str)) {
            return y82.b(str2);
        }
        if (y82.q(str)) {
            return y82.l(str2);
        }
        if (y82.p(str) || y82.n(str)) {
            return str;
        }
        if ("application/mp4".equals(str)) {
            String e2 = y82.e(str2);
            return "text/vtt".equals(e2) ? "application/x-mp4-vtt" : e2;
        }
        return null;
    }

    public static void w(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        if (c.e(xmlPullParser)) {
            int i = 1;
            while (i != 0) {
                xmlPullParser.next();
                if (c.e(xmlPullParser)) {
                    i++;
                } else if (c.c(xmlPullParser)) {
                    i--;
                }
            }
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public int A(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        char c2;
        String r0 = r0(xmlPullParser, "schemeIdUri", null);
        r0.hashCode();
        int i = -1;
        switch (r0.hashCode()) {
            case -2128649360:
                if (r0.equals("urn:dts:dash:audio_channel_configuration:2012")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case -1352850286:
                if (r0.equals("urn:mpeg:dash:23003:3:audio_channel_configuration:2011")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case -1138141449:
                if (r0.equals("tag:dolby.com,2014:dash:audio_channel_configuration:2011")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case -986633423:
                if (r0.equals("urn:mpeg:mpegB:cicp:ChannelConfiguration")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -79006963:
                if (r0.equals("tag:dts.com,2014:dash:audio_channel_configuration:2012")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 312179081:
                if (r0.equals("tag:dts.com,2018:uhd:audio_channel_configuration")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case 2036691300:
                if (r0.equals("urn:dolby:dash:audio_channel_configuration:2011")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
            case 4:
                i = K(xmlPullParser);
                break;
            case 1:
                i = U(xmlPullParser, "value", -1);
                break;
            case 2:
            case 6:
                i = J(xmlPullParser);
                break;
            case 3:
                i = Z(xmlPullParser);
                break;
            case 5:
                i = L(xmlPullParser);
                break;
        }
        do {
            xmlPullParser.next();
        } while (!c.d(xmlPullParser, "AudioChannelConfiguration"));
        return i;
    }

    public long B(XmlPullParser xmlPullParser, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "availabilityTimeOffset");
        if (attributeValue == null) {
            return j;
        }
        if ("INF".equals(attributeValue)) {
            return Long.MAX_VALUE;
        }
        return Float.parseFloat(attributeValue) * 1000000.0f;
    }

    public List<bo> C(XmlPullParser xmlPullParser, List<bo> list, boolean z) throws XmlPullParserException, IOException {
        int i;
        String attributeValue = xmlPullParser.getAttributeValue(null, "dvb:priority");
        if (attributeValue != null) {
            i = Integer.parseInt(attributeValue);
        } else {
            i = z ? 1 : Integer.MIN_VALUE;
        }
        String attributeValue2 = xmlPullParser.getAttributeValue(null, "dvb:weight");
        int parseInt = attributeValue2 != null ? Integer.parseInt(attributeValue2) : 1;
        String attributeValue3 = xmlPullParser.getAttributeValue(null, "serviceLocation");
        String s0 = s0(xmlPullParser, "BaseURL");
        if (rf4.b(s0)) {
            if (attributeValue3 == null) {
                attributeValue3 = s0;
            }
            return Lists.k(new bo(s0, attributeValue3, i, parseInt));
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            bo boVar = list.get(i2);
            String d2 = rf4.d(boVar.a, s0);
            String str = attributeValue3 == null ? d2 : attributeValue3;
            if (z) {
                i = boVar.c;
                parseInt = boVar.d;
                str = boVar.b;
            }
            arrayList.add(new bo(d2, str, i, parseInt));
        }
        return arrayList;
    }

    /* JADX WARN: Removed duplicated region for block: B:49:0x00b4  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00bb  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x0119  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public android.util.Pair<java.lang.String, androidx.media3.common.DrmInitData.SchemeData> F(org.xmlpull.v1.XmlPullParser r11) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 324
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.td0.F(org.xmlpull.v1.XmlPullParser):android.util.Pair");
    }

    public int G(XmlPullParser xmlPullParser) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "contentType");
        if (TextUtils.isEmpty(attributeValue)) {
            return -1;
        }
        if ("audio".equals(attributeValue)) {
            return 1;
        }
        if ("video".equals(attributeValue)) {
            return 2;
        }
        return PublicResolver.FUNC_TEXT.equals(attributeValue) ? 3 : -1;
    }

    public Pair<Long, EventMessage> O(XmlPullParser xmlPullParser, String str, String str2, long j, ByteArrayOutputStream byteArrayOutputStream) throws IOException, XmlPullParserException {
        long X = X(xmlPullParser, "id", 0L);
        long X2 = X(xmlPullParser, "duration", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        long X3 = X(xmlPullParser, "presentationTime", 0L);
        long J0 = b.J0(X2, 1000L, j);
        long J02 = b.J0(X3, 1000000L, j);
        String r0 = r0(xmlPullParser, "messageData", null);
        byte[] P = P(xmlPullParser, byteArrayOutputStream);
        Long valueOf = Long.valueOf(J02);
        if (r0 != null) {
            P = b.j0(r0);
        }
        return Pair.create(valueOf, d(str, str2, X, J0, P));
    }

    public byte[] P(XmlPullParser xmlPullParser, ByteArrayOutputStream byteArrayOutputStream) throws XmlPullParserException, IOException {
        byteArrayOutputStream.reset();
        XmlSerializer newSerializer = Xml.newSerializer();
        newSerializer.setOutput(byteArrayOutputStream, cy.c.name());
        xmlPullParser.nextToken();
        while (!c.d(xmlPullParser, "Event")) {
            switch (xmlPullParser.getEventType()) {
                case 0:
                    newSerializer.startDocument(null, Boolean.FALSE);
                    break;
                case 1:
                    newSerializer.endDocument();
                    break;
                case 2:
                    newSerializer.startTag(xmlPullParser.getNamespace(), xmlPullParser.getName());
                    for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
                        newSerializer.attribute(xmlPullParser.getAttributeNamespace(i), xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
                    }
                    break;
                case 3:
                    newSerializer.endTag(xmlPullParser.getNamespace(), xmlPullParser.getName());
                    break;
                case 4:
                    newSerializer.text(xmlPullParser.getText());
                    break;
                case 5:
                    newSerializer.cdsect(xmlPullParser.getText());
                    break;
                case 6:
                    newSerializer.entityRef(xmlPullParser.getText());
                    break;
                case 7:
                    newSerializer.ignorableWhitespace(xmlPullParser.getText());
                    break;
                case 8:
                    newSerializer.processingInstruction(xmlPullParser.getText());
                    break;
                case 9:
                    newSerializer.comment(xmlPullParser.getText());
                    break;
                case 10:
                    newSerializer.docdecl(xmlPullParser.getText());
                    break;
            }
            xmlPullParser.nextToken();
        }
        newSerializer.flush();
        return byteArrayOutputStream.toByteArray();
    }

    public jy0 Q(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String r0 = r0(xmlPullParser, "schemeIdUri", "");
        String r02 = r0(xmlPullParser, "value", "");
        long X = X(xmlPullParser, "timescale", 1L);
        ArrayList arrayList = new ArrayList();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN);
        do {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "Event")) {
                arrayList.add(O(xmlPullParser, r0, r02, X, byteArrayOutputStream));
            } else {
                w(xmlPullParser);
            }
        } while (!c.d(xmlPullParser, "EventStream"));
        long[] jArr = new long[arrayList.size()];
        EventMessage[] eventMessageArr = new EventMessage[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            Pair pair = (Pair) arrayList.get(i);
            jArr[i] = ((Long) pair.first).longValue();
            eventMessageArr[i] = (EventMessage) pair.second;
        }
        return e(r0, r02, X, jArr, eventMessageArr);
    }

    public s33 T(XmlPullParser xmlPullParser) {
        return d0(xmlPullParser, "sourceURL", "range");
    }

    public String V(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        return s0(xmlPullParser, "Label");
    }

    /* JADX WARN: Removed duplicated region for block: B:78:0x01c0  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x01e0  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x01e7 A[LOOP:0: B:25:0x00a4->B:82:0x01e7, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x01a3 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.sd0 Y(org.xmlpull.v1.XmlPullParser r47, android.net.Uri r48) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 501
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.td0.Y(org.xmlpull.v1.XmlPullParser, android.net.Uri):sd0");
    }

    public Pair<jq2, Long> a0(XmlPullParser xmlPullParser, List<bo> list, long j, long j2, long j3, long j4, boolean z) throws XmlPullParserException, IOException {
        long j5;
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        Object obj;
        long j6;
        cj3 l0;
        td0 td0Var = this;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        Object obj2 = null;
        String attributeValue = xmlPullParser2.getAttributeValue(null, "id");
        long M = M(xmlPullParser2, "start", j);
        long j7 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        long j8 = j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? j3 + M : -9223372036854775807L;
        long M2 = M(xmlPullParser2, "duration", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        long j9 = j2;
        boolean z2 = false;
        long j10 = -9223372036854775807L;
        cj3.e eVar = null;
        nm0 nm0Var = null;
        while (true) {
            xmlPullParser.next();
            if (c.f(xmlPullParser2, "BaseURL")) {
                if (!z2) {
                    j9 = td0Var.B(xmlPullParser2, j9);
                    z2 = true;
                }
                arrayList6.addAll(td0Var.C(xmlPullParser2, list, z));
                arrayList3 = arrayList5;
                arrayList = arrayList6;
                j6 = j7;
                obj = obj2;
                arrayList2 = arrayList4;
            } else {
                if (c.f(xmlPullParser2, "AdaptationSet")) {
                    j5 = j9;
                    arrayList = arrayList6;
                    arrayList2 = arrayList4;
                    arrayList2.add(y(xmlPullParser, !arrayList6.isEmpty() ? arrayList6 : list, eVar, M2, j9, j10, j8, j4, z));
                    xmlPullParser2 = xmlPullParser;
                    arrayList3 = arrayList5;
                } else {
                    j5 = j9;
                    ArrayList arrayList7 = arrayList5;
                    arrayList = arrayList6;
                    arrayList2 = arrayList4;
                    xmlPullParser2 = xmlPullParser;
                    if (c.f(xmlPullParser2, "EventStream")) {
                        arrayList7.add(Q(xmlPullParser));
                        arrayList3 = arrayList7;
                    } else if (c.f(xmlPullParser2, "SegmentBase")) {
                        arrayList3 = arrayList7;
                        eVar = j0(xmlPullParser2, null);
                        obj = null;
                        j9 = j5;
                        j6 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                    } else {
                        arrayList3 = arrayList7;
                        if (c.f(xmlPullParser2, "SegmentList")) {
                            long B = B(xmlPullParser2, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                            obj = null;
                            l0 = k0(xmlPullParser, null, j8, M2, j5, B, j4);
                            j10 = B;
                            j9 = j5;
                            j6 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                        } else {
                            obj = null;
                            if (c.f(xmlPullParser2, "SegmentTemplate")) {
                                long B2 = B(xmlPullParser2, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                                j6 = -9223372036854775807L;
                                l0 = l0(xmlPullParser, null, ImmutableList.of(), j8, M2, j5, B2, j4);
                                j10 = B2;
                                j9 = j5;
                            } else {
                                j6 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                                if (c.f(xmlPullParser2, "AssetIdentifier")) {
                                    nm0Var = I(xmlPullParser2, "AssetIdentifier");
                                } else {
                                    w(xmlPullParser);
                                }
                                j9 = j5;
                            }
                        }
                        eVar = l0;
                    }
                }
                obj = null;
                j6 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                j9 = j5;
            }
            if (c.d(xmlPullParser2, "Period")) {
                return Pair.create(h(attributeValue, M, arrayList2, arrayList3, nm0Var), Long.valueOf(M2));
            }
            arrayList4 = arrayList2;
            arrayList6 = arrayList;
            obj2 = obj;
            arrayList5 = arrayList3;
            j7 = j6;
            td0Var = this;
        }
    }

    public final long b(List<cj3.d> list, long j, long j2, int i, long j3) {
        int m = i >= 0 ? i + 1 : (int) b.m(j3 - j, j2);
        for (int i2 = 0; i2 < m; i2++) {
            list.add(m(j, j2));
            j += j2;
        }
        return j;
    }

    public String[] b0(XmlPullParser xmlPullParser, String str, String[] strArr) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? strArr : attributeValue.split(",");
    }

    public i8 c(int i, int i2, List<b73> list, List<nm0> list2, List<nm0> list3, List<nm0> list4) {
        return new i8(i, i2, list, list2, list3, list4);
    }

    public mv2 c0(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        String str = null;
        String r0 = r0(xmlPullParser, "moreInformationURL", null);
        String r02 = r0(xmlPullParser, "lang", null);
        String str2 = null;
        String str3 = null;
        while (true) {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "Title")) {
                str = xmlPullParser.nextText();
            } else if (c.f(xmlPullParser, "Source")) {
                str2 = xmlPullParser.nextText();
            } else if (c.f(xmlPullParser, "Copyright")) {
                str3 = xmlPullParser.nextText();
            } else {
                w(xmlPullParser);
            }
            String str4 = str3;
            if (c.d(xmlPullParser, "ProgramInformation")) {
                return new mv2(str, str2, str4, r0, r02);
            }
            str3 = str4;
        }
    }

    public EventMessage d(String str, String str2, long j, long j2, byte[] bArr) {
        return new EventMessage(str, str2, j2, j, bArr);
    }

    public s33 d0(XmlPullParser xmlPullParser, String str, String str2) {
        long j;
        long j2;
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        String attributeValue2 = xmlPullParser.getAttributeValue(null, str2);
        if (attributeValue2 != null) {
            String[] split = attributeValue2.split("-");
            j = Long.parseLong(split[0]);
            if (split.length == 2) {
                j2 = (Long.parseLong(split[1]) - j) + 1;
                return i(attributeValue, j, j2);
            }
        } else {
            j = 0;
        }
        j2 = -1;
        return i(attributeValue, j, j2);
    }

    public jy0 e(String str, String str2, long j, long[] jArr, EventMessage[] eventMessageArr) {
        return new jy0(str, str2, j, jArr, eventMessageArr);
    }

    /* JADX WARN: Removed duplicated region for block: B:57:0x01ee A[LOOP:0: B:3:0x006a->B:57:0x01ee, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0198 A[EDGE_INSN: B:58:0x0198->B:47:0x0198 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.td0.a e0(org.xmlpull.v1.XmlPullParser r36, java.util.List<defpackage.bo> r37, java.lang.String r38, java.lang.String r39, int r40, int r41, float r42, int r43, int r44, java.lang.String r45, java.util.List<defpackage.nm0> r46, java.util.List<defpackage.nm0> r47, java.util.List<defpackage.nm0> r48, java.util.List<defpackage.nm0> r49, defpackage.cj3 r50, long r51, long r53, long r55, long r57, long r59, boolean r61) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 509
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.td0.e0(org.xmlpull.v1.XmlPullParser, java.util.List, java.lang.String, java.lang.String, int, int, float, int, int, java.lang.String, java.util.List, java.util.List, java.util.List, java.util.List, cj3, long, long, long, long, long, boolean):td0$a");
    }

    public j f(String str, String str2, int i, int i2, float f, int i3, int i4, int i5, String str3, List<nm0> list, List<nm0> list2, String str4, List<nm0> list3, List<nm0> list4) {
        String str5 = str4;
        String u = u(str2, str5);
        if ("audio/eac3".equals(u)) {
            u = N(list4);
            if ("audio/eac3-joc".equals(u)) {
                str5 = "ec+3";
            }
        }
        int p0 = p0(list);
        j.b V = new j.b().S(str).K(str2).e0(u).I(str5).Z(i5).g0(p0).c0(i0(list) | f0(list2) | h0(list3) | h0(list4)).V(str3);
        if (y82.q(u)) {
            V.j0(i).Q(i2).P(f);
        } else if (y82.m(u)) {
            V.H(i3).f0(i4);
        } else if (y82.p(u)) {
            int i6 = -1;
            if ("application/cea-608".equals(u)) {
                i6 = D(list2);
            } else if ("application/cea-708".equals(u)) {
                i6 = E(list2);
            }
            V.F(i6);
        } else if (y82.n(u)) {
            V.j0(i).Q(i2);
        }
        return V.E();
    }

    public int f0(List<nm0> list) {
        int t0;
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            nm0 nm0Var = list.get(i2);
            if (ei.a("urn:mpeg:dash:role:2011", nm0Var.a)) {
                t0 = g0(nm0Var.b);
            } else if (ei.a("urn:tva:metadata:cs:AudioPurposeCS:2007", nm0Var.a)) {
                t0 = t0(nm0Var.b);
            }
            i |= t0;
        }
        return i;
    }

    public sd0 g(long j, long j2, long j3, boolean z, long j4, long j5, long j6, long j7, mv2 mv2Var, ig4 ig4Var, gm3 gm3Var, Uri uri, List<jq2> list) {
        return new sd0(j, j2, j3, z, j4, j5, j6, j7, mv2Var, ig4Var, gm3Var, uri, list);
    }

    public int g0(String str) {
        if (str == null) {
            return 0;
        }
        char c2 = 65535;
        switch (str.hashCode()) {
            case -2060497896:
                if (str.equals("subtitle")) {
                    c2 = 0;
                    break;
                }
                break;
            case -1724546052:
                if (str.equals("description")) {
                    c2 = 1;
                    break;
                }
                break;
            case -1580883024:
                if (str.equals("enhanced-audio-intelligibility")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1574842690:
                if (str.equals("forced_subtitle")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1408024454:
                if (str.equals("alternate")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1396432756:
                if (str.equals("forced-subtitle")) {
                    c2 = 5;
                    break;
                }
                break;
            case 99825:
                if (str.equals("dub")) {
                    c2 = 6;
                    break;
                }
                break;
            case 3343801:
                if (str.equals("main")) {
                    c2 = 7;
                    break;
                }
                break;
            case 3530173:
                if (str.equals("sign")) {
                    c2 = '\b';
                    break;
                }
                break;
            case 552573414:
                if (str.equals("caption")) {
                    c2 = '\t';
                    break;
                }
                break;
            case 899152809:
                if (str.equals("commentary")) {
                    c2 = '\n';
                    break;
                }
                break;
            case 1629013393:
                if (str.equals("emergency")) {
                    c2 = 11;
                    break;
                }
                break;
            case 1855372047:
                if (str.equals("supplementary")) {
                    c2 = '\f';
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
            case 3:
            case 5:
                return 128;
            case 1:
                return RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
            case 2:
                return 2048;
            case 4:
                return 2;
            case 6:
                return 16;
            case 7:
                return 1;
            case '\b':
                return 256;
            case '\t':
                return 64;
            case '\n':
                return 8;
            case 11:
                return 32;
            case '\f':
                return 4;
            default:
                return 0;
        }
    }

    public jq2 h(String str, long j, List<i8> list, List<jy0> list2, nm0 nm0Var) {
        return new jq2(str, j, list, list2, nm0Var);
    }

    public int h0(List<nm0> list) {
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            if (ei.a("http://dashif.org/guidelines/trickmode", list.get(i2).a)) {
                i |= Http2.INITIAL_MAX_FRAME_SIZE;
            }
        }
        return i;
    }

    public s33 i(String str, long j, long j2) {
        return new s33(str, j, j2);
    }

    public int i0(List<nm0> list) {
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            nm0 nm0Var = list.get(i2);
            if (ei.a("urn:mpeg:dash:role:2011", nm0Var.a)) {
                i |= g0(nm0Var.b);
            }
        }
        return i;
    }

    public b73 j(a aVar, String str, String str2, ArrayList<DrmInitData.SchemeData> arrayList, ArrayList<nm0> arrayList2) {
        j.b b2 = aVar.a.b();
        if (str != null) {
            b2.U(str);
        }
        String str3 = aVar.d;
        if (str3 != null) {
            str2 = str3;
        }
        ArrayList<DrmInitData.SchemeData> arrayList3 = aVar.e;
        arrayList3.addAll(arrayList);
        if (!arrayList3.isEmpty()) {
            r(arrayList3);
            s(arrayList3);
            b2.M(new DrmInitData(str2, arrayList3));
        }
        ArrayList<nm0> arrayList4 = aVar.f;
        arrayList4.addAll(arrayList2);
        return b73.o(aVar.g, b2.E(), aVar.b, aVar.c, arrayList4, aVar.h, aVar.i, null);
    }

    public cj3.e j0(XmlPullParser xmlPullParser, cj3.e eVar) throws XmlPullParserException, IOException {
        long j;
        long j2;
        long X = X(xmlPullParser, "timescale", eVar != null ? eVar.b : 1L);
        long X2 = X(xmlPullParser, "presentationTimeOffset", eVar != null ? eVar.c : 0L);
        long j3 = eVar != null ? eVar.d : 0L;
        long j4 = eVar != null ? eVar.e : 0L;
        String attributeValue = xmlPullParser.getAttributeValue(null, "indexRange");
        if (attributeValue != null) {
            String[] split = attributeValue.split("-");
            long parseLong = Long.parseLong(split[0]);
            j = (Long.parseLong(split[1]) - parseLong) + 1;
            j2 = parseLong;
        } else {
            j = j4;
            j2 = j3;
        }
        s33 s33Var = eVar != null ? eVar.a : null;
        do {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "Initialization")) {
                s33Var = T(xmlPullParser);
            } else {
                w(xmlPullParser);
            }
        } while (!c.d(xmlPullParser, "SegmentBase"));
        return n(s33Var, X, X2, j2, j);
    }

    public cj3.b k(s33 s33Var, long j, long j2, long j3, long j4, List<cj3.d> list, long j5, List<s33> list2, long j6, long j7) {
        return new cj3.b(s33Var, j, j2, j3, j4, list, j5, list2, b.y0(j6), b.y0(j7));
    }

    public cj3.b k0(XmlPullParser xmlPullParser, cj3.b bVar, long j, long j2, long j3, long j4, long j5) throws XmlPullParserException, IOException {
        long X = X(xmlPullParser, "timescale", bVar != null ? bVar.b : 1L);
        long X2 = X(xmlPullParser, "presentationTimeOffset", bVar != null ? bVar.c : 0L);
        long X3 = X(xmlPullParser, "duration", bVar != null ? bVar.e : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        long X4 = X(xmlPullParser, "startNumber", bVar != null ? bVar.d : 1L);
        long t = t(j3, j4);
        List<cj3.d> list = null;
        List<s33> list2 = null;
        s33 s33Var = null;
        do {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "Initialization")) {
                s33Var = T(xmlPullParser);
            } else if (c.f(xmlPullParser, "SegmentTimeline")) {
                list = m0(xmlPullParser, X, j2);
            } else if (c.f(xmlPullParser, "SegmentURL")) {
                if (list2 == null) {
                    list2 = new ArrayList<>();
                }
                list2.add(n0(xmlPullParser));
            } else {
                w(xmlPullParser);
            }
        } while (!c.d(xmlPullParser, "SegmentList"));
        if (bVar != null) {
            if (s33Var == null) {
                s33Var = bVar.a;
            }
            if (list == null) {
                list = bVar.f;
            }
            if (list2 == null) {
                list2 = bVar.j;
            }
        }
        return k(s33Var, X, X2, X4, X3, list, t, list2, j5, j);
    }

    public cj3.c l(s33 s33Var, long j, long j2, long j3, long j4, long j5, List<cj3.d> list, long j6, uf4 uf4Var, uf4 uf4Var2, long j7, long j8) {
        return new cj3.c(s33Var, j, j2, j3, j4, j5, list, j6, uf4Var, uf4Var2, b.y0(j7), b.y0(j8));
    }

    public cj3.c l0(XmlPullParser xmlPullParser, cj3.c cVar, List<nm0> list, long j, long j2, long j3, long j4, long j5) throws XmlPullParserException, IOException {
        long X = X(xmlPullParser, "timescale", cVar != null ? cVar.b : 1L);
        long X2 = X(xmlPullParser, "presentationTimeOffset", cVar != null ? cVar.c : 0L);
        long X3 = X(xmlPullParser, "duration", cVar != null ? cVar.e : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        long X4 = X(xmlPullParser, "startNumber", cVar != null ? cVar.d : 1L);
        long W = W(list);
        long t = t(j3, j4);
        List<cj3.d> list2 = null;
        uf4 u0 = u0(xmlPullParser, "media", cVar != null ? cVar.k : null);
        uf4 u02 = u0(xmlPullParser, "initialization", cVar != null ? cVar.j : null);
        s33 s33Var = null;
        while (true) {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "Initialization")) {
                s33Var = T(xmlPullParser);
            } else if (c.f(xmlPullParser, "SegmentTimeline")) {
                list2 = m0(xmlPullParser, X, j2);
            } else {
                w(xmlPullParser);
            }
            if (c.d(xmlPullParser, "SegmentTemplate")) {
                break;
            }
        }
        if (cVar != null) {
            if (s33Var == null) {
                s33Var = cVar.a;
            }
            if (list2 == null) {
                list2 = cVar.f;
            }
        }
        return l(s33Var, X, X2, X4, W, X3, list2, t, u02, u0, j5, j);
    }

    public cj3.d m(long j, long j2) {
        return new cj3.d(j, j2);
    }

    public List<cj3.d> m0(XmlPullParser xmlPullParser, long j, long j2) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        long j3 = 0;
        boolean z = false;
        int i = 0;
        long j4 = -9223372036854775807L;
        do {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "S")) {
                long X = X(xmlPullParser, "t", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                if (z) {
                    j3 = b(arrayList, j3, j4, i, X);
                }
                if (X == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    X = j3;
                }
                j4 = X(xmlPullParser, "d", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                i = U(xmlPullParser, "r", 0);
                z = true;
                j3 = X;
            } else {
                w(xmlPullParser);
            }
        } while (!c.d(xmlPullParser, "SegmentTimeline"));
        if (z) {
            b(arrayList, j3, j4, i, b.J0(j2, j, 1000L));
        }
        return arrayList;
    }

    public cj3.e n(s33 s33Var, long j, long j2, long j3, long j4) {
        return new cj3.e(s33Var, j, j2, j3, j4);
    }

    public s33 n0(XmlPullParser xmlPullParser) {
        return d0(xmlPullParser, "media", "mediaRange");
    }

    public ig4 o(String str, String str2) {
        return new ig4(str, str2);
    }

    public int o0(String str) {
        if (str == null) {
            return 0;
        }
        return (str.equals("forced_subtitle") || str.equals("forced-subtitle")) ? 2 : 0;
    }

    public int p0(List<nm0> list) {
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            nm0 nm0Var = list.get(i2);
            if (ei.a("urn:mpeg:dash:role:2011", nm0Var.a)) {
                i |= o0(nm0Var.b);
            }
        }
        return i;
    }

    public gm3 q0(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        float f = -3.4028235E38f;
        float f2 = -3.4028235E38f;
        long j = -9223372036854775807L;
        long j2 = -9223372036854775807L;
        long j3 = -9223372036854775807L;
        while (true) {
            xmlPullParser.next();
            if (c.f(xmlPullParser, "Latency")) {
                j = X(xmlPullParser, "target", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                j2 = X(xmlPullParser, "min", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
                j3 = X(xmlPullParser, "max", CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
            } else if (c.f(xmlPullParser, "PlaybackRate")) {
                f = R(xmlPullParser, "min", -3.4028235E38f);
                f2 = R(xmlPullParser, "max", -3.4028235E38f);
            }
            long j4 = j;
            long j5 = j2;
            long j6 = j3;
            float f3 = f;
            float f4 = f2;
            if (c.d(xmlPullParser, "ServiceDescription")) {
                return new gm3(j4, j5, j6, f3, f4);
            }
            j = j4;
            j2 = j5;
            j3 = j6;
            f = f3;
            f2 = f4;
        }
    }

    public int t0(String str) {
        if (str == null) {
            return 0;
        }
        char c2 = 65535;
        switch (str.hashCode()) {
            case 49:
                if (str.equals("1")) {
                    c2 = 0;
                    break;
                }
                break;
            case 50:
                if (str.equals("2")) {
                    c2 = 1;
                    break;
                }
                break;
            case 51:
                if (str.equals("3")) {
                    c2 = 2;
                    break;
                }
                break;
            case 52:
                if (str.equals("4")) {
                    c2 = 3;
                    break;
                }
                break;
            case 54:
                if (str.equals("6")) {
                    c2 = 4;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
            case 1:
                return 2048;
            case 2:
                return 4;
            case 3:
                return 8;
            case 4:
                return 1;
            default:
                return 0;
        }
    }

    public uf4 u0(XmlPullParser xmlPullParser, String str, uf4 uf4Var) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue != null ? uf4.b(attributeValue) : uf4Var;
    }

    public final boolean v(String[] strArr) {
        for (String str : strArr) {
            if (str.startsWith("urn:dvb:dash:profile:dvb-dash:")) {
                return true;
            }
        }
        return false;
    }

    public ig4 v0(XmlPullParser xmlPullParser) {
        return o(xmlPullParser.getAttributeValue(null, "schemeIdUri"), xmlPullParser.getAttributeValue(null, "value"));
    }

    @Override // androidx.media3.exoplayer.upstream.d.a
    /* renamed from: x */
    public sd0 a(Uri uri, InputStream inputStream) throws IOException {
        try {
            XmlPullParser newPullParser = this.a.newPullParser();
            newPullParser.setInput(inputStream, null);
            if (newPullParser.next() == 2 && "MPD".equals(newPullParser.getName())) {
                return Y(newPullParser, uri);
            }
            throw ParserException.createForMalformedManifest("inputStream does not contain a valid media presentation description", null);
        } catch (XmlPullParserException e2) {
            throw ParserException.createForMalformedManifest(null, e2);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:71:0x030e A[LOOP:0: B:3:0x007c->B:71:0x030e, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:72:0x02ce A[EDGE_INSN: B:72:0x02ce->B:65:0x02ce ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.i8 y(org.xmlpull.v1.XmlPullParser r55, java.util.List<defpackage.bo> r56, defpackage.cj3 r57, long r58, long r60, long r62, long r64, long r66, boolean r68) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            Method dump skipped, instructions count: 810
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.td0.y(org.xmlpull.v1.XmlPullParser, java.util.List, cj3, long, long, long, long, long, boolean):i8");
    }

    public void z(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        w(xmlPullParser);
    }
}
