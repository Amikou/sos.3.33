package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.carousel.ContactCarouselView;

/* compiled from: FragmentSendToNftBinding.java */
/* renamed from: za1  reason: default package */
/* loaded from: classes2.dex */
public final class za1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final RecyclerView d;
    public final ContactCarouselView e;
    public final AppCompatEditText f;
    public final AppCompatImageView g;
    public final AppCompatImageView h;
    public final ScrollView i;
    public final LinearLayout j;
    public final AppCompatEditText k;
    public final rp3 l;
    public final ij4 m;

    public za1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, RecyclerView recyclerView, ContactCarouselView contactCarouselView, AppCompatEditText appCompatEditText, AppCompatImageView appCompatImageView, LinearLayoutCompat linearLayoutCompat, AppCompatImageView appCompatImageView2, ScrollView scrollView, LinearLayout linearLayout, AppCompatEditText appCompatEditText2, rp3 rp3Var, ij4 ij4Var) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = recyclerView;
        this.e = contactCarouselView;
        this.f = appCompatEditText;
        this.g = appCompatImageView;
        this.h = appCompatImageView2;
        this.i = scrollView;
        this.j = linearLayout;
        this.k = appCompatEditText2;
        this.l = rp3Var;
        this.m = ij4Var;
    }

    public static za1 a(View view) {
        int i = R.id.btnAddContact;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAddContact);
        if (materialButton != null) {
            i = R.id.btnSend;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnSend);
            if (materialButton2 != null) {
                i = R.id.contacts;
                RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.contacts);
                if (recyclerView != null) {
                    i = R.id.cvRecentContact;
                    ContactCarouselView contactCarouselView = (ContactCarouselView) ai4.a(view, R.id.cvRecentContact);
                    if (contactCarouselView != null) {
                        i = R.id.etContactAddress;
                        AppCompatEditText appCompatEditText = (AppCompatEditText) ai4.a(view, R.id.etContactAddress);
                        if (appCompatEditText != null) {
                            i = R.id.imgWallets;
                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgWallets);
                            if (appCompatImageView != null) {
                                i = R.id.lContactAddressContainer;
                                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.lContactAddressContainer);
                                if (linearLayoutCompat != null) {
                                    i = R.id.nftDetailImage;
                                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.nftDetailImage);
                                    if (appCompatImageView2 != null) {
                                        i = R.id.scroll_layout;
                                        ScrollView scrollView = (ScrollView) ai4.a(view, R.id.scroll_layout);
                                        if (scrollView != null) {
                                            i = R.id.searchBox;
                                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.searchBox);
                                            if (linearLayout != null) {
                                                i = R.id.searchField;
                                                AppCompatEditText appCompatEditText2 = (AppCompatEditText) ai4.a(view, R.id.searchField);
                                                if (appCompatEditText2 != null) {
                                                    i = R.id.toolbarWrapper;
                                                    View a = ai4.a(view, R.id.toolbarWrapper);
                                                    if (a != null) {
                                                        rp3 a2 = rp3.a(a);
                                                        i = R.id.view_til_end_layout;
                                                        View a3 = ai4.a(view, R.id.view_til_end_layout);
                                                        if (a3 != null) {
                                                            return new za1((ConstraintLayout) view, materialButton, materialButton2, recyclerView, contactCarouselView, appCompatEditText, appCompatImageView, linearLayoutCompat, appCompatImageView2, scrollView, linearLayout, appCompatEditText2, a2, ij4.a(a3));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static za1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.fragment_send_to_nft, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
