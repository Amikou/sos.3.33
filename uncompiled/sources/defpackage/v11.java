package defpackage;

/* compiled from: FLog.java */
/* renamed from: v11  reason: default package */
/* loaded from: classes.dex */
public class v11 {
    public static a22 a = w11.k();

    public static void A(String str, String str2, Object... objArr) {
        if (a.j(6)) {
            a.h(str, k(str2, objArr));
        }
    }

    public static void a(Class<?> cls, String str, Object obj) {
        if (a.j(3)) {
            a.c(l(cls), k(str, obj));
        }
    }

    public static void b(String str, String str2) {
        if (a.j(3)) {
            a.c(str, str2);
        }
    }

    public static void c(String str, String str2, Throwable th) {
        if (a.j(3)) {
            a.i(str, str2, th);
        }
    }

    public static void d(Class<?> cls, String str) {
        if (a.j(6)) {
            a.b(l(cls), str);
        }
    }

    public static void e(Class<?> cls, String str, Throwable th) {
        if (a.j(6)) {
            a.d(l(cls), str, th);
        }
    }

    public static void f(Class<?> cls, String str, Object... objArr) {
        if (a.j(6)) {
            a.b(l(cls), k(str, objArr));
        }
    }

    public static void g(Class<?> cls, Throwable th, String str, Object... objArr) {
        if (a.j(6)) {
            a.d(l(cls), k(str, objArr), th);
        }
    }

    public static void h(String str, String str2) {
        if (a.j(6)) {
            a.b(str, str2);
        }
    }

    public static void i(String str, String str2, Throwable th) {
        if (a.j(6)) {
            a.d(str, str2, th);
        }
    }

    public static void j(String str, String str2, Object... objArr) {
        if (a.j(6)) {
            a.b(str, k(str2, objArr));
        }
    }

    public static String k(String str, Object... objArr) {
        return String.format(null, str, objArr);
    }

    public static String l(Class<?> cls) {
        return cls.getSimpleName();
    }

    public static boolean m(int i) {
        return a.j(i);
    }

    public static void n(Class<?> cls, String str) {
        if (a.j(2)) {
            a.g(l(cls), str);
        }
    }

    public static void o(Class<?> cls, String str, Object obj) {
        if (a.j(2)) {
            a.g(l(cls), k(str, obj));
        }
    }

    public static void p(Class<?> cls, String str, Object obj, Object obj2) {
        if (a.j(2)) {
            a.g(l(cls), k(str, obj, obj2));
        }
    }

    public static void q(Class<?> cls, String str, Object obj, Object obj2, Object obj3) {
        if (m(2)) {
            n(cls, k(str, obj, obj2, obj3));
        }
    }

    public static void r(Class<?> cls, String str, Object obj, Object obj2, Object obj3, Object obj4) {
        if (a.j(2)) {
            a.g(l(cls), k(str, obj, obj2, obj3, obj4));
        }
    }

    public static void s(Class<?> cls, String str, Object... objArr) {
        if (a.j(2)) {
            a.g(l(cls), k(str, objArr));
        }
    }

    public static void t(Class<?> cls, String str) {
        if (a.j(5)) {
            a.a(l(cls), str);
        }
    }

    public static void u(Class<?> cls, String str, Throwable th) {
        if (a.j(5)) {
            a.f(l(cls), str, th);
        }
    }

    public static void v(Class<?> cls, String str, Object... objArr) {
        if (a.j(5)) {
            a.a(l(cls), k(str, objArr));
        }
    }

    public static void w(Class<?> cls, Throwable th, String str, Object... objArr) {
        if (m(5)) {
            u(cls, k(str, objArr), th);
        }
    }

    public static void x(String str, String str2, Object... objArr) {
        if (a.j(5)) {
            a.a(str, k(str2, objArr));
        }
    }

    public static void y(Class<?> cls, String str, Throwable th) {
        if (a.j(6)) {
            a.e(l(cls), str, th);
        }
    }

    public static void z(String str, String str2) {
        if (a.j(6)) {
            a.h(str, str2);
        }
    }
}
