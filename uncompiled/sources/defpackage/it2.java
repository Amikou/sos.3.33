package defpackage;

/* compiled from: Pools.java */
/* renamed from: it2  reason: default package */
/* loaded from: classes.dex */
public class it2<T> extends gt2<T> {
    public final Object c;

    public it2(int i) {
        super(i);
        this.c = new Object();
    }

    @Override // defpackage.gt2, defpackage.et2
    public boolean a(T t) {
        boolean a;
        synchronized (this.c) {
            a = super.a(t);
        }
        return a;
    }

    @Override // defpackage.gt2, defpackage.et2
    public T b() {
        T t;
        synchronized (this.c) {
            t = (T) super.b();
        }
        return t;
    }
}
