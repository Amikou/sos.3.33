package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.internal.zas;
import com.google.android.gms.signin.internal.zam;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: f25  reason: default package */
/* loaded from: classes.dex */
public final class f25 implements Parcelable.Creator<zam> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zam createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        ConnectionResult connectionResult = null;
        int i = 0;
        zas zasVar = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 1) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 2) {
                connectionResult = (ConnectionResult) SafeParcelReader.o(parcel, C, ConnectionResult.CREATOR);
            } else if (v != 3) {
                SafeParcelReader.I(parcel, C);
            } else {
                zasVar = (zas) SafeParcelReader.o(parcel, C, zas.CREATOR);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zam(i, connectionResult, zasVar);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zam[] newArray(int i) {
        return new zam[i];
    }
}
