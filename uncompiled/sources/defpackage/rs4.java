package defpackage;

/* compiled from: ZendeskCallback.java */
/* renamed from: rs4  reason: default package */
/* loaded from: classes2.dex */
public abstract class rs4<T> {
    public abstract void onError(cw0 cw0Var);

    public abstract void onSuccess(T t);
}
