package defpackage;

import java.math.BigInteger;

/* compiled from: EthGetUncleCountByBlockHash.java */
/* renamed from: ax0  reason: default package */
/* loaded from: classes3.dex */
public class ax0 extends i83<String> {
    public BigInteger getUncleCount() {
        return ej2.decodeQuantity(getResult());
    }
}
