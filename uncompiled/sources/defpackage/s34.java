package defpackage;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: s34  reason: default package */
/* loaded from: classes.dex */
public final class s34 {
    public static final Executor a = new a();
    public static final Executor b = new w46();

    /* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
    /* renamed from: s34$a */
    /* loaded from: classes.dex */
    public static final class a implements Executor {
        public final Handler a = new e75(Looper.getMainLooper());

        @Override // java.util.concurrent.Executor
        public final void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }
}
