package defpackage;

import android.os.ParcelFileDescriptor;
import java.util.List;
import java.util.Map;

/* renamed from: zy4  reason: default package */
/* loaded from: classes2.dex */
public interface zy4 {
    void a();

    void b(int i);

    void c(int i, String str, String str2, int i2);

    void d(int i, String str);

    l34<ParcelFileDescriptor> e(int i, String str, String str2, int i2);

    l34<List<String>> f(Map<String, Long> map);

    void g(List<String> list);
}
