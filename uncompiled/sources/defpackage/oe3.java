package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: oe3  reason: default package */
/* loaded from: classes2.dex */
public class oe3 extends xs0.c {
    public static final BigInteger j = new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFEE37"));
    public re3 i;

    /* renamed from: oe3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ int[] b;

        public a(int i, int[] iArr) {
            this.a = i;
            this.b = iArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            int[] g = cd2.g();
            int[] g2 = cd2.g();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                int i4 = ((i3 ^ i) - 1) >> 31;
                for (int i5 = 0; i5 < 6; i5++) {
                    int i6 = g[i5];
                    int[] iArr = this.b;
                    g[i5] = i6 ^ (iArr[i2 + i5] & i4);
                    g2[i5] = g2[i5] ^ (iArr[(i2 + 6) + i5] & i4);
                }
                i2 += 12;
            }
            return oe3.this.i(new qe3(g), new qe3(g2), false);
        }
    }

    public oe3() {
        super(j);
        this.i = new re3(this, null, null);
        this.b = n(ws0.a);
        this.c = n(BigInteger.valueOf(3L));
        this.d = new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFE26F2FC170F69466A74DEFD8D"));
        this.e = BigInteger.valueOf(1L);
        this.f = 2;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 2;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new oe3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        int[] iArr = new int[i2 * 6 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            cd2.e(((qe3) pt0Var.n()).f, 0, iArr, i3);
            int i5 = i3 + 6;
            cd2.e(((qe3) pt0Var.o()).f, 0, iArr, i5);
            i3 = i5 + 6;
        }
        return new a(i2, iArr);
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new re3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new re3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new qe3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return j.bitLength();
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.i;
    }
}
