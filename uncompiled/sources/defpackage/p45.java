package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: p45  reason: default package */
/* loaded from: classes.dex */
public final class p45 implements Iterable<z55>, z55, m55 {
    public final SortedMap<Integer, z55> a;
    public final Map<String, z55> f0;

    public p45() {
        this.a = new TreeMap();
        this.f0 = new TreeMap();
    }

    public final void B() {
        this.a.clear();
    }

    public final void D(int i, z55 z55Var) {
        if (i >= 0) {
            if (i >= s()) {
                y(i, z55Var);
                return;
            }
            for (int intValue = this.a.lastKey().intValue(); intValue >= i; intValue--) {
                SortedMap<Integer, z55> sortedMap = this.a;
                Integer valueOf = Integer.valueOf(intValue);
                z55 z55Var2 = sortedMap.get(valueOf);
                if (z55Var2 != null) {
                    y(intValue + 1, z55Var2);
                    this.a.remove(valueOf);
                }
            }
            y(i, z55Var);
            return;
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append("Invalid value index: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public final void E(int i) {
        int intValue = this.a.lastKey().intValue();
        if (i > intValue || i < 0) {
            return;
        }
        this.a.remove(Integer.valueOf(i));
        if (i == intValue) {
            SortedMap<Integer, z55> sortedMap = this.a;
            int i2 = i - 1;
            Integer valueOf = Integer.valueOf(i2);
            if (sortedMap.containsKey(valueOf) || i2 < 0) {
                return;
            }
            this.a.put(valueOf, z55.X);
            return;
        }
        while (true) {
            i++;
            if (i > this.a.lastKey().intValue()) {
                return;
            }
            SortedMap<Integer, z55> sortedMap2 = this.a;
            Integer valueOf2 = Integer.valueOf(i);
            z55 z55Var = sortedMap2.get(valueOf2);
            if (z55Var != null) {
                this.a.put(Integer.valueOf(i - 1), z55Var);
                this.a.remove(valueOf2);
            }
        }
    }

    public final String F(String str) {
        if (str == null) {
            str = "";
        }
        StringBuilder sb = new StringBuilder();
        if (!this.a.isEmpty()) {
            for (int i = 0; i < s(); i++) {
                z55 w = w(i);
                sb.append(str);
                if (!(w instanceof i65) && !(w instanceof t55)) {
                    sb.append(w.zzc());
                }
            }
            sb.delete(0, str.length());
        }
        return sb.toString();
    }

    @Override // defpackage.z55
    public final Double b() {
        if (this.a.size() == 1) {
            return w(0).b();
        }
        if (this.a.size() <= 0) {
            return Double.valueOf((double) Utils.DOUBLE_EPSILON);
        }
        return Double.valueOf(Double.NaN);
    }

    @Override // defpackage.z55
    public final Boolean c() {
        return Boolean.TRUE;
    }

    @Override // defpackage.m55
    public final z55 e(String str) {
        z55 z55Var;
        if ("length".equals(str)) {
            return new z45(Double.valueOf(s()));
        }
        return (!o(str) || (z55Var = this.f0.get(str)) == null) ? z55.X : z55Var;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof p45) {
            p45 p45Var = (p45) obj;
            if (s() != p45Var.s()) {
                return false;
            }
            if (this.a.isEmpty()) {
                return p45Var.a.isEmpty();
            }
            for (int intValue = this.a.firstKey().intValue(); intValue <= this.a.lastKey().intValue(); intValue++) {
                if (!w(intValue).equals(p45Var.w(intValue))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return this.a.hashCode() * 31;
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return new i45(this, this.a.keySet().iterator(), this.f0.keySet().iterator());
    }

    @Override // java.lang.Iterable
    public final Iterator<z55> iterator() {
        return new m45(this);
    }

    @Override // defpackage.m55
    public final void k(String str, z55 z55Var) {
        if (z55Var == null) {
            this.f0.remove(str);
        } else {
            this.f0.put(str, z55Var);
        }
    }

    @Override // defpackage.z55
    public final z55 m() {
        p45 p45Var = new p45();
        for (Map.Entry<Integer, z55> entry : this.a.entrySet()) {
            if (entry.getValue() instanceof m55) {
                p45Var.a.put(entry.getKey(), entry.getValue());
            } else {
                p45Var.a.put(entry.getKey(), entry.getValue().m());
            }
        }
        return p45Var;
    }

    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        if (!"concat".equals(str) && !"every".equals(str) && !"filter".equals(str) && !"forEach".equals(str) && !"indexOf".equals(str) && !"join".equals(str) && !"lastIndexOf".equals(str) && !"map".equals(str) && !"pop".equals(str) && !"push".equals(str) && !"reduce".equals(str) && !"reduceRight".equals(str) && !"reverse".equals(str) && !"shift".equals(str) && !"slice".equals(str) && !"some".equals(str) && !"sort".equals(str) && !"splice".equals(str) && !"toString".equals(str) && !"unshift".equals(str)) {
            return g55.a(this, new f65(str), wk5Var, list);
        }
        return y75.a(str, this, wk5Var, list);
    }

    @Override // defpackage.m55
    public final boolean o(String str) {
        return "length".equals(str) || this.f0.containsKey(str);
    }

    public final List<z55> p() {
        ArrayList arrayList = new ArrayList(s());
        for (int i = 0; i < s(); i++) {
            arrayList.add(w(i));
        }
        return arrayList;
    }

    public final Iterator<Integer> q() {
        return this.a.keySet().iterator();
    }

    public final int s() {
        if (this.a.isEmpty()) {
            return 0;
        }
        return this.a.lastKey().intValue() + 1;
    }

    public final int t() {
        return this.a.size();
    }

    public final String toString() {
        return F(",");
    }

    public final z55 w(int i) {
        z55 z55Var;
        if (i < s()) {
            return (!z(i) || (z55Var = this.a.get(Integer.valueOf(i))) == null) ? z55.X : z55Var;
        }
        throw new IndexOutOfBoundsException("Attempting to get element outside of current array");
    }

    public final void y(int i, z55 z55Var) {
        if (i > 32468) {
            throw new IllegalStateException("Array too large");
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Out of bounds index: ");
            sb.append(i);
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (z55Var == null) {
            this.a.remove(Integer.valueOf(i));
        } else {
            this.a.put(Integer.valueOf(i), z55Var);
        }
    }

    public final boolean z(int i) {
        if (i >= 0 && i <= this.a.lastKey().intValue()) {
            return this.a.containsKey(Integer.valueOf(i));
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append("Out of bounds index: ");
        sb.append(i);
        throw new IndexOutOfBoundsException(sb.toString());
    }

    @Override // defpackage.z55
    public final String zzc() {
        return F(",");
    }

    public p45(List<z55> list) {
        this();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                y(i, list.get(i));
            }
        }
    }
}
