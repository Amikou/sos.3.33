package defpackage;

/* compiled from: ViewModel.kt */
/* renamed from: ej4  reason: default package */
/* loaded from: classes.dex */
public final class ej4 {
    public static final c90 a(dj4 dj4Var) {
        fs1.f(dj4Var, "$this$viewModelScope");
        c90 c90Var = (c90) dj4Var.getTag("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (c90Var != null) {
            return c90Var;
        }
        Object tagIfAbsent = dj4Var.setTagIfAbsent("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new xz(cw3.b(null, 1, null).plus(tp0.c().l())));
        fs1.e(tagIfAbsent, "setTagIfAbsent(\n        …Main.immediate)\n        )");
        return (c90) tagIfAbsent;
    }
}
