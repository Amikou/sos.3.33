package defpackage;

import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;

/* compiled from: StopWorkRunnable.java */
/* renamed from: ut3  reason: default package */
/* loaded from: classes.dex */
public class ut3 implements Runnable {
    public static final String h0 = v12.f("StopWorkRunnable");
    public final hq4 a;
    public final String f0;
    public final boolean g0;

    public ut3(hq4 hq4Var, String str, boolean z) {
        this.a = hq4Var;
        this.f0 = str;
        this.g0 = z;
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean o;
        WorkDatabase q = this.a.q();
        bv2 o2 = this.a.o();
        uq4 P = q.P();
        q.e();
        try {
            boolean h = o2.h(this.f0);
            if (this.g0) {
                o = this.a.o().n(this.f0);
            } else {
                if (!h && P.k(this.f0) == WorkInfo.State.RUNNING) {
                    P.a(WorkInfo.State.ENQUEUED, this.f0);
                }
                o = this.a.o().o(this.f0);
            }
            v12.c().a(h0, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", this.f0, Boolean.valueOf(o)), new Throwable[0]);
            q.E();
        } finally {
            q.j();
        }
    }
}
