package defpackage;

/* compiled from: MemoryCache.java */
/* renamed from: l72  reason: default package */
/* loaded from: classes.dex */
public interface l72<K, V> extends q72 {

    /* compiled from: MemoryCache.java */
    /* renamed from: l72$a */
    /* loaded from: classes.dex */
    public interface a {
    }

    void b(K k);

    com.facebook.common.references.a<V> c(K k, com.facebook.common.references.a<V> aVar);

    boolean contains(K k);

    com.facebook.common.references.a<V> get(K k);
}
