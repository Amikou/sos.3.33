package defpackage;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: LruBitmapPool.java */
/* renamed from: o22  reason: default package */
/* loaded from: classes.dex */
public class o22 implements jq {
    public static final Bitmap.Config j = Bitmap.Config.ARGB_8888;
    public final v22 a;
    public final Set<Bitmap.Config> b;
    public final a c;
    public long d;
    public long e;
    public int f;
    public int g;
    public int h;
    public int i;

    /* compiled from: LruBitmapPool.java */
    /* renamed from: o22$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(Bitmap bitmap);

        void b(Bitmap bitmap);
    }

    /* compiled from: LruBitmapPool.java */
    /* renamed from: o22$b */
    /* loaded from: classes.dex */
    public static final class b implements a {
        @Override // defpackage.o22.a
        public void a(Bitmap bitmap) {
        }

        @Override // defpackage.o22.a
        public void b(Bitmap bitmap) {
        }
    }

    public o22(long j2, v22 v22Var, Set<Bitmap.Config> set) {
        this.d = j2;
        this.a = v22Var;
        this.b = set;
        this.c = new b();
    }

    @TargetApi(26)
    public static void f(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE) {
            throw new IllegalArgumentException("Cannot create a mutable Bitmap with config: " + config + ". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
        }
    }

    public static Bitmap g(int i, int i2, Bitmap.Config config) {
        if (config == null) {
            config = j;
        }
        return Bitmap.createBitmap(i, i2, config);
    }

    @TargetApi(26)
    public static Set<Bitmap.Config> k() {
        HashSet hashSet = new HashSet(Arrays.asList(Bitmap.Config.values()));
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            hashSet.add(null);
        }
        if (i >= 26) {
            hashSet.remove(Bitmap.Config.HARDWARE);
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public static v22 l() {
        if (Build.VERSION.SDK_INT >= 19) {
            return new cq3();
        }
        return new ej();
    }

    @TargetApi(19)
    public static void o(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    public static void p(Bitmap bitmap) {
        bitmap.setHasAlpha(true);
        o(bitmap);
    }

    @Override // defpackage.jq
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("trimMemory, level=");
            sb.append(i);
        }
        if (i >= 40 || (Build.VERSION.SDK_INT >= 23 && i >= 20)) {
            b();
        } else if (i >= 20 || i == 15) {
            q(n() / 2);
        }
    }

    @Override // defpackage.jq
    public void b() {
        q(0L);
    }

    @Override // defpackage.jq
    public synchronized void c(Bitmap bitmap) {
        try {
            if (bitmap != null) {
                if (!bitmap.isRecycled()) {
                    if (bitmap.isMutable() && this.a.b(bitmap) <= this.d && this.b.contains(bitmap.getConfig())) {
                        int b2 = this.a.b(bitmap);
                        this.a.c(bitmap);
                        this.c.b(bitmap);
                        this.h++;
                        this.e += b2;
                        if (Log.isLoggable("LruBitmapPool", 2)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Put bitmap in pool=");
                            sb.append(this.a.e(bitmap));
                        }
                        h();
                        j();
                        return;
                    }
                    if (Log.isLoggable("LruBitmapPool", 2)) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Reject bitmap from pool, bitmap: ");
                        sb2.append(this.a.e(bitmap));
                        sb2.append(", is mutable: ");
                        sb2.append(bitmap.isMutable());
                        sb2.append(", is allowed config: ");
                        sb2.append(this.b.contains(bitmap.getConfig()));
                    }
                    bitmap.recycle();
                    return;
                }
                throw new IllegalStateException("Cannot pool recycled bitmap");
            }
            throw new NullPointerException("Bitmap must not be null");
        } catch (Throwable th) {
            throw th;
        }
    }

    @Override // defpackage.jq
    public Bitmap d(int i, int i2, Bitmap.Config config) {
        Bitmap m = m(i, i2, config);
        if (m != null) {
            m.eraseColor(0);
            return m;
        }
        return g(i, i2, config);
    }

    @Override // defpackage.jq
    public Bitmap e(int i, int i2, Bitmap.Config config) {
        Bitmap m = m(i, i2, config);
        return m == null ? g(i, i2, config) : m;
    }

    public final void h() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            i();
        }
    }

    public final void i() {
        StringBuilder sb = new StringBuilder();
        sb.append("Hits=");
        sb.append(this.f);
        sb.append(", misses=");
        sb.append(this.g);
        sb.append(", puts=");
        sb.append(this.h);
        sb.append(", evictions=");
        sb.append(this.i);
        sb.append(", currentSize=");
        sb.append(this.e);
        sb.append(", maxSize=");
        sb.append(this.d);
        sb.append("\nStrategy=");
        sb.append(this.a);
    }

    public final void j() {
        q(this.d);
    }

    public final synchronized Bitmap m(int i, int i2, Bitmap.Config config) {
        Bitmap d;
        f(config);
        d = this.a.d(i, i2, config != null ? config : j);
        if (d == null) {
            if (Log.isLoggable("LruBitmapPool", 3)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Missing bitmap=");
                sb.append(this.a.a(i, i2, config));
            }
            this.g++;
        } else {
            this.f++;
            this.e -= this.a.b(d);
            this.c.a(d);
            p(d);
        }
        if (Log.isLoggable("LruBitmapPool", 2)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Get bitmap=");
            sb2.append(this.a.a(i, i2, config));
        }
        h();
        return d;
    }

    public long n() {
        return this.d;
    }

    public final synchronized void q(long j2) {
        while (this.e > j2) {
            Bitmap removeLast = this.a.removeLast();
            if (removeLast == null) {
                if (Log.isLoggable("LruBitmapPool", 5)) {
                    i();
                }
                this.e = 0L;
                return;
            }
            this.c.a(removeLast);
            this.e -= this.a.b(removeLast);
            this.i++;
            if (Log.isLoggable("LruBitmapPool", 3)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Evicting bitmap=");
                sb.append(this.a.e(removeLast));
            }
            h();
            removeLast.recycle();
        }
    }

    public o22(long j2) {
        this(j2, l(), k());
    }
}
