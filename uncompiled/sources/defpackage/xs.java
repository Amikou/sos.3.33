package defpackage;

import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: IOStreams.kt */
/* renamed from: xs  reason: default package */
/* loaded from: classes2.dex */
public final class xs {
    public static final long a(InputStream inputStream, OutputStream outputStream, int i) {
        fs1.f(inputStream, "$this$copyTo");
        fs1.f(outputStream, "out");
        byte[] bArr = new byte[i];
        int read = inputStream.read(bArr);
        long j = 0;
        while (read >= 0) {
            outputStream.write(bArr, 0, read);
            j += read;
            read = inputStream.read(bArr);
        }
        return j;
    }
}
