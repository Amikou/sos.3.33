package defpackage;

import android.os.IInterface;
import android.os.RemoteException;

/* renamed from: jf5  reason: default package */
/* loaded from: classes.dex */
public interface jf5 extends IInterface {
    boolean O(boolean z) throws RemoteException;

    String getId() throws RemoteException;
}
