package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.gc4;
import defpackage.l4;
import java.util.Arrays;
import java.util.Collections;
import zendesk.support.request.CellBase;

/* compiled from: AdtsReader.java */
/* renamed from: aa  reason: default package */
/* loaded from: classes.dex */
public final class aa implements ku0 {
    public static final byte[] v = {73, 68, 51};
    public final boolean a;
    public final np2 b;
    public final op2 c;
    public final String d;
    public String e;
    public f84 f;
    public f84 g;
    public int h;
    public int i;
    public int j;
    public boolean k;
    public boolean l;
    public int m;
    public int n;
    public int o;
    public boolean p;
    public long q;
    public int r;
    public long s;
    public f84 t;
    public long u;

    public aa(boolean z) {
        this(z, null);
    }

    public static boolean m(int i) {
        return (i & 65526) == 65520;
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) throws ParserException {
        b();
        while (op2Var.a() > 0) {
            int i = this.h;
            if (i == 0) {
                j(op2Var);
            } else if (i == 1) {
                g(op2Var);
            } else if (i != 2) {
                if (i == 3) {
                    if (i(op2Var, this.b.a, this.k ? 7 : 5)) {
                        n();
                    }
                } else if (i == 4) {
                    p(op2Var);
                } else {
                    throw new IllegalStateException();
                }
            } else if (i(op2Var, this.c.d(), 10)) {
                o();
            }
        }
    }

    public final void b() {
        ii.e(this.f);
        b.j(this.t);
        b.j(this.g);
    }

    @Override // defpackage.ku0
    public void c() {
        this.s = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        q();
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.s = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.e = dVar.b();
        f84 f = r11Var.f(dVar.c(), 1);
        this.f = f;
        this.t = f;
        if (this.a) {
            dVar.a();
            f84 f2 = r11Var.f(dVar.c(), 5);
            this.g = f2;
            f2.f(new j.b().S(dVar.b()).e0("application/id3").E());
            return;
        }
        this.g = new ks0();
    }

    public final void g(op2 op2Var) {
        if (op2Var.a() == 0) {
            return;
        }
        this.b.a[0] = op2Var.d()[op2Var.e()];
        this.b.p(2);
        int h = this.b.h(4);
        int i = this.n;
        if (i != -1 && h != i) {
            q();
            return;
        }
        if (!this.l) {
            this.l = true;
            this.m = this.o;
            this.n = h;
        }
        t();
    }

    public final boolean h(op2 op2Var, int i) {
        op2Var.P(i + 1);
        if (w(op2Var, this.b.a, 1)) {
            this.b.p(4);
            int h = this.b.h(1);
            int i2 = this.m;
            if (i2 == -1 || h == i2) {
                if (this.n != -1) {
                    if (!w(op2Var, this.b.a, 1)) {
                        return true;
                    }
                    this.b.p(2);
                    if (this.b.h(4) != this.n) {
                        return false;
                    }
                    op2Var.P(i + 2);
                }
                if (w(op2Var, this.b.a, 4)) {
                    this.b.p(14);
                    int h2 = this.b.h(13);
                    if (h2 < 7) {
                        return false;
                    }
                    byte[] d = op2Var.d();
                    int f = op2Var.f();
                    int i3 = i + h2;
                    if (i3 >= f) {
                        return true;
                    }
                    if (d[i3] == -1) {
                        int i4 = i3 + 1;
                        if (i4 == f) {
                            return true;
                        }
                        return l((byte) -1, d[i4]) && ((d[i4] & 8) >> 3) == h;
                    } else if (d[i3] != 73) {
                        return false;
                    } else {
                        int i5 = i3 + 1;
                        if (i5 == f) {
                            return true;
                        }
                        if (d[i5] != 68) {
                            return false;
                        }
                        int i6 = i3 + 2;
                        return i6 == f || d[i6] == 51;
                    }
                }
                return true;
            }
            return false;
        }
        return false;
    }

    public final boolean i(op2 op2Var, byte[] bArr, int i) {
        int min = Math.min(op2Var.a(), i - this.i);
        op2Var.j(bArr, this.i, min);
        int i2 = this.i + min;
        this.i = i2;
        return i2 == i;
    }

    public final void j(op2 op2Var) {
        byte[] d = op2Var.d();
        int e = op2Var.e();
        int f = op2Var.f();
        while (e < f) {
            int i = e + 1;
            int i2 = d[e] & 255;
            if (this.j == 512 && l((byte) -1, (byte) i2) && (this.l || h(op2Var, i - 2))) {
                this.o = (i2 & 8) >> 3;
                this.k = (i2 & 1) == 0;
                if (!this.l) {
                    r();
                } else {
                    t();
                }
                op2Var.P(i);
                return;
            }
            int i3 = this.j;
            int i4 = i2 | i3;
            if (i4 == 329) {
                this.j = 768;
            } else if (i4 == 511) {
                this.j = RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN;
            } else if (i4 == 836) {
                this.j = RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
            } else if (i4 == 1075) {
                u();
                op2Var.P(i);
                return;
            } else if (i3 != 256) {
                this.j = 256;
                i--;
            }
            e = i;
        }
        op2Var.P(e);
    }

    public long k() {
        return this.q;
    }

    public final boolean l(byte b, byte b2) {
        return m(((b & 255) << 8) | (b2 & 255));
    }

    public final void n() throws ParserException {
        this.b.p(0);
        if (!this.p) {
            int h = this.b.h(2) + 1;
            if (h != 2) {
                p12.i("AdtsReader", "Detected audio object type: " + h + ", but assuming AAC LC.");
                h = 2;
            }
            this.b.r(5);
            byte[] a = l4.a(h, this.n, this.b.h(3));
            l4.b e = l4.e(a);
            j E = new j.b().S(this.e).e0("audio/mp4a-latm").I(e.c).H(e.b).f0(e.a).T(Collections.singletonList(a)).V(this.d).E();
            this.q = 1024000000 / E.D0;
            this.f.f(E);
            this.p = true;
        } else {
            this.b.r(10);
        }
        this.b.r(4);
        int h2 = (this.b.h(13) - 2) - 5;
        if (this.k) {
            h2 -= 2;
        }
        v(this.f, this.q, 0, h2);
    }

    public final void o() {
        this.g.a(this.c, 10);
        this.c.P(6);
        v(this.g, 0L, 10, this.c.C() + 10);
    }

    public final void p(op2 op2Var) {
        int min = Math.min(op2Var.a(), this.r - this.i);
        this.t.a(op2Var, min);
        int i = this.i + min;
        this.i = i;
        int i2 = this.r;
        if (i == i2) {
            long j = this.s;
            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                this.t.b(j, 1, i2, 0, null);
                this.s += this.u;
            }
            s();
        }
    }

    public final void q() {
        this.l = false;
        s();
    }

    public final void r() {
        this.h = 1;
        this.i = 0;
    }

    public final void s() {
        this.h = 0;
        this.i = 0;
        this.j = 256;
    }

    public final void t() {
        this.h = 3;
        this.i = 0;
    }

    public final void u() {
        this.h = 2;
        this.i = v.length;
        this.r = 0;
        this.c.P(0);
    }

    public final void v(f84 f84Var, long j, int i, int i2) {
        this.h = 4;
        this.i = i;
        this.t = f84Var;
        this.u = j;
        this.r = i2;
    }

    public final boolean w(op2 op2Var, byte[] bArr, int i) {
        if (op2Var.a() < i) {
            return false;
        }
        op2Var.j(bArr, 0, i);
        return true;
    }

    public aa(boolean z, String str) {
        this.b = new np2(new byte[7]);
        this.c = new op2(Arrays.copyOf(v, 10));
        s();
        this.m = -1;
        this.n = -1;
        this.q = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.s = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.a = z;
        this.d = str;
    }
}
