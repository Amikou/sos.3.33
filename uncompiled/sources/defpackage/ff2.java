package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyCallback;
import android.telephony.TelephonyDisplayInfo;
import android.telephony.TelephonyManager;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: NetworkTypeObserver.java */
/* renamed from: ff2  reason: default package */
/* loaded from: classes.dex */
public final class ff2 {
    public static ff2 e;
    public final Handler a = new Handler(Looper.getMainLooper());
    public final CopyOnWriteArrayList<WeakReference<c>> b = new CopyOnWriteArrayList<>();
    public final Object c = new Object();
    public int d = 0;

    /* compiled from: NetworkTypeObserver.java */
    /* renamed from: ff2$b */
    /* loaded from: classes.dex */
    public static final class b {

        /* compiled from: NetworkTypeObserver.java */
        /* renamed from: ff2$b$a */
        /* loaded from: classes.dex */
        public static final class a extends TelephonyCallback implements TelephonyCallback.DisplayInfoListener {
            public final ff2 a;

            public a(ff2 ff2Var) {
                this.a = ff2Var;
            }

            @Override // android.telephony.TelephonyCallback.DisplayInfoListener
            public void onDisplayInfoChanged(TelephonyDisplayInfo telephonyDisplayInfo) {
                int overrideNetworkType = telephonyDisplayInfo.getOverrideNetworkType();
                this.a.k(overrideNetworkType == 3 || overrideNetworkType == 4 || overrideNetworkType == 5 ? 10 : 5);
            }
        }

        public static void a(Context context, ff2 ff2Var) {
            try {
                TelephonyManager telephonyManager = (TelephonyManager) ii.e((TelephonyManager) context.getSystemService("phone"));
                a aVar = new a(ff2Var);
                telephonyManager.registerTelephonyCallback(context.getMainExecutor(), aVar);
                telephonyManager.unregisterTelephonyCallback(aVar);
            } catch (RuntimeException unused) {
                ff2Var.k(5);
            }
        }
    }

    /* compiled from: NetworkTypeObserver.java */
    /* renamed from: ff2$c */
    /* loaded from: classes.dex */
    public interface c {
        void a(int i);
    }

    /* compiled from: NetworkTypeObserver.java */
    /* renamed from: ff2$d */
    /* loaded from: classes.dex */
    public final class d extends BroadcastReceiver {
        public d() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            int g = ff2.g(context);
            if (androidx.media3.common.util.b.a < 31 || g != 5) {
                ff2.this.k(g);
            } else {
                b.a(context, ff2.this);
            }
        }
    }

    public ff2(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        context.registerReceiver(new d(), intentFilter);
    }

    public static synchronized ff2 d(Context context) {
        ff2 ff2Var;
        synchronized (ff2.class) {
            if (e == null) {
                e = new ff2(context);
            }
            ff2Var = e;
        }
        return ff2Var;
    }

    public static int e(NetworkInfo networkInfo) {
        switch (networkInfo.getSubtype()) {
            case 1:
            case 2:
                return 3;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 14:
            case 15:
            case 17:
                return 4;
            case 13:
                return 5;
            case 16:
            case 19:
            default:
                return 6;
            case 18:
                return 2;
            case 20:
                return androidx.media3.common.util.b.a >= 29 ? 9 : 0;
        }
    }

    public static int g(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        int i = 0;
        if (connectivityManager == null) {
            return 0;
        }
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            i = 1;
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                int type = activeNetworkInfo.getType();
                if (type != 0) {
                    if (type == 1) {
                        return 2;
                    }
                    if (type != 4 && type != 5) {
                        if (type != 6) {
                            return type != 9 ? 8 : 7;
                        }
                        return 5;
                    }
                }
                return e(activeNetworkInfo);
            }
        } catch (SecurityException unused) {
        }
        return i;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void h(c cVar) {
        cVar.a(f());
    }

    public int f() {
        int i;
        synchronized (this.c) {
            i = this.d;
        }
        return i;
    }

    public void i(final c cVar) {
        j();
        this.b.add(new WeakReference<>(cVar));
        this.a.post(new Runnable() { // from class: ef2
            @Override // java.lang.Runnable
            public final void run() {
                ff2.this.h(cVar);
            }
        });
    }

    public final void j() {
        Iterator<WeakReference<c>> it = this.b.iterator();
        while (it.hasNext()) {
            WeakReference<c> next = it.next();
            if (next.get() == null) {
                this.b.remove(next);
            }
        }
    }

    public final void k(int i) {
        synchronized (this.c) {
            if (this.d == i) {
                return;
            }
            this.d = i;
            Iterator<WeakReference<c>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference<c> next = it.next();
                c cVar = next.get();
                if (cVar != null) {
                    cVar.a(i);
                } else {
                    this.b.remove(next);
                }
            }
        }
    }
}
