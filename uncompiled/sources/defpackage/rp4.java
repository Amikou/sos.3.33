package defpackage;

import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: rp4  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class rp4 implements Executor {
    public static final Executor a = new rp4();

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        runnable.run();
    }
}
