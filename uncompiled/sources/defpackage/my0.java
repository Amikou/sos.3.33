package defpackage;

/* compiled from: Exceptions.java */
/* renamed from: my0  reason: default package */
/* loaded from: classes2.dex */
public final class my0 {
    public static void a(Throwable th) {
        if (!(th instanceof VirtualMachineError)) {
            if (!(th instanceof ThreadDeath)) {
                if (th instanceof LinkageError) {
                    throw ((LinkageError) th);
                }
                return;
            }
            throw ((ThreadDeath) th);
        }
        throw ((VirtualMachineError) th);
    }
}
