package defpackage;

import android.graphics.ColorSpace;
import android.util.Pair;

/* compiled from: ImageMetaData.java */
/* renamed from: eo1  reason: default package */
/* loaded from: classes.dex */
public class eo1 {
    public final Pair<Integer, Integer> a;
    public final ColorSpace b;

    public eo1(int i, int i2, ColorSpace colorSpace) {
        this.a = (i == -1 || i2 == -1) ? null : new Pair<>(Integer.valueOf(i), Integer.valueOf(i2));
        this.b = colorSpace;
    }

    public ColorSpace a() {
        return this.b;
    }

    public Pair<Integer, Integer> b() {
        return this.a;
    }
}
