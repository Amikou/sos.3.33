package defpackage;

import defpackage.gp2;
import java.util.List;

/* compiled from: PagingState.kt */
/* renamed from: ip2  reason: default package */
/* loaded from: classes.dex */
public final class ip2<Key, Value> {
    public final List<gp2.b.C0179b<Key, Value>> a;
    public final Integer b;
    public final ep2 c;
    public final int d;

    public ip2(List<gp2.b.C0179b<Key, Value>> list, Integer num, ep2 ep2Var, int i) {
        fs1.f(list, "pages");
        fs1.f(ep2Var, "config");
        this.a = list;
        this.b = num;
        this.c = ep2Var;
        this.d = i;
    }

    public final Integer a() {
        return this.b;
    }

    public final List<gp2.b.C0179b<Key, Value>> b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ip2) {
            ip2 ip2Var = (ip2) obj;
            if (fs1.b(this.a, ip2Var.a) && fs1.b(this.b, ip2Var.b) && fs1.b(this.c, ip2Var.c) && this.d == ip2Var.d) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.a.hashCode();
        Integer num = this.b;
        return hashCode + (num != null ? num.hashCode() : 0) + this.c.hashCode() + this.d;
    }

    public String toString() {
        return "PagingState(pages=" + this.a + ", anchorPosition=" + this.b + ", config=" + this.c + ", leadingPlaceholderCount=" + this.d + ')';
    }
}
