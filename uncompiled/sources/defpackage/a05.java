package defpackage;

import com.google.android.gms.signin.internal.b;
import com.google.android.gms.signin.internal.zam;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: a05  reason: default package */
/* loaded from: classes.dex */
public final class a05 extends b {
    public final WeakReference<rz4> a;

    public a05(rz4 rz4Var) {
        this.a = new WeakReference<>(rz4Var);
    }

    @Override // com.google.android.gms.signin.internal.a
    public final void d1(zam zamVar) {
        i05 i05Var;
        rz4 rz4Var = this.a.get();
        if (rz4Var == null) {
            return;
        }
        i05Var = rz4Var.a;
        i05Var.k(new zz4(this, rz4Var, rz4Var, zamVar));
    }
}
