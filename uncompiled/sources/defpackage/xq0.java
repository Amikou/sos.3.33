package defpackage;

import android.annotation.SuppressLint;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* compiled from: DrawableProperties.java */
/* renamed from: xq0  reason: default package */
/* loaded from: classes.dex */
public class xq0 {
    public int a = -1;
    public boolean b = false;
    public ColorFilter c = null;
    public int d = -1;
    public int e = -1;

    @SuppressLint({"Range"})
    public void a(Drawable drawable) {
        if (drawable == null) {
            return;
        }
        int i = this.a;
        if (i != -1) {
            drawable.setAlpha(i);
        }
        if (this.b) {
            drawable.setColorFilter(this.c);
        }
        int i2 = this.d;
        if (i2 != -1) {
            drawable.setDither(i2 != 0);
        }
        int i3 = this.e;
        if (i3 != -1) {
            drawable.setFilterBitmap(i3 != 0);
        }
    }

    public void b(int i) {
        this.a = i;
    }

    public void c(ColorFilter colorFilter) {
        this.c = colorFilter;
        this.b = colorFilter != null;
    }

    public void d(boolean z) {
        this.d = z ? 1 : 0;
    }

    public void e(boolean z) {
        this.e = z ? 1 : 0;
    }
}
