package defpackage;

/* compiled from: uisAES.java */
/* renamed from: bz4  reason: default package */
/* loaded from: classes.dex */
public class bz4 {
    public long a;
    public boolean b;
    public d c;
    public boolean d;

    public bz4(boolean z) {
        this.c = new d();
        this.b = z;
        this.a = 0L;
        j();
    }

    public byte[] a(byte[] bArr) {
        int m;
        int length = bArr.length;
        if (!this.d) {
            this.a = st.e;
            return null;
        }
        byte[] bArr2 = new byte[16];
        byte[] bArr3 = new byte[16];
        int i = length / 16;
        byte[] bArr4 = new byte[length];
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            int i5 = 0;
            while (i5 < 16) {
                bArr2[i5] = bArr[i2];
                i5++;
                i2++;
            }
            this.c.e(bArr2, bArr3, 8);
            int i6 = 0;
            while (i6 < 16) {
                bArr4[i3] = bArr3[i6];
                i6++;
                i3++;
            }
        }
        ay1.d(bArr4);
        int i7 = length - 1;
        if (bArr4[i7] == 1) {
            byte[] bArr5 = new byte[20];
            int i8 = i7 - 20;
            System.arraycopy(bArr4, i8, bArr5, 0, 20);
            m = ay1.m(bArr4, i8 - 4);
            if (m > length) {
                this.a = st.f;
                return null;
            }
            ha3 ha3Var = new ha3();
            ha3Var.n();
            ha3Var.d(bArr4, 0, m);
            ha3Var.m();
            if (!ha3Var.f) {
                this.a = st.f;
                return null;
            }
            for (int i9 = 0; i9 < 20; i9++) {
                if (ha3Var.e[i9] != bArr5[i9]) {
                    this.a = st.f;
                    return null;
                }
            }
        } else {
            m = ay1.m(bArr4, i7 - 4);
            if (m > length) {
                this.a = st.f;
                return null;
            }
        }
        byte[] bArr6 = new byte[m];
        System.arraycopy(bArr4, 0, bArr6, 0, m);
        this.a = 0L;
        return bArr6;
    }

    public String b(String str) {
        if (!this.d) {
            this.a = st.e;
            return new String();
        } else if (str != null && str.length() != 0) {
            new String();
            byte[] c = lm.c(str);
            StringBuffer stringBuffer = new StringBuffer();
            byte[] a = a(c);
            if (a == null) {
                return new String();
            }
            for (byte b : a) {
                stringBuffer.append((char) b);
            }
            this.a = 0L;
            return stringBuffer.toString();
        } else {
            this.a = st.b;
            return new String();
        }
    }

    public byte[] c(byte[] bArr) {
        if (!this.d) {
            this.a = st.e;
            return null;
        } else if (bArr == null) {
            return null;
        } else {
            byte[] bArr2 = new byte[16];
            byte[] bArr3 = new byte[16];
            int length = bArr.length;
            int i = length + 4 + (this.b ? 20 : 0) + 1;
            int i2 = i % 16;
            int i3 = i2 == 0 ? 0 : 16 - i2;
            int i4 = i + i3;
            byte[] bArr4 = new byte[i4];
            int i5 = 0;
            int i6 = 0;
            while (i5 < length) {
                bArr4[i6] = bArr[i5];
                i5++;
                i6++;
            }
            int i7 = 0;
            while (i7 < i3) {
                bArr4[i6] = 32;
                i7++;
                i6++;
            }
            ay1.n(length, bArr4, i6);
            int i8 = i6 + 4;
            if (this.b) {
                int length2 = bArr.length;
                byte[] bArr5 = new byte[length2];
                for (int i9 = 0; i9 < length2; i9++) {
                    bArr5[i9] = bArr[i9];
                }
                ha3 ha3Var = new ha3();
                ha3Var.n();
                ha3Var.c(bArr5);
                ha3Var.m();
                int i10 = 0;
                while (i10 < 20) {
                    bArr4[i8] = ha3Var.e[i10];
                    i10++;
                    i8++;
                }
            }
            bArr4[i8] = this.b ? (byte) 1 : (byte) 0;
            int i11 = i4 / 16;
            byte[] bArr6 = new byte[i4];
            int i12 = 0;
            int i13 = 0;
            for (int i14 = 0; i14 < i11; i14++) {
                int i15 = 0;
                while (i15 < 16) {
                    bArr2[i15] = bArr4[i12];
                    i15++;
                    i12++;
                }
                this.c.f(bArr2, bArr3, 8);
                int i16 = 0;
                while (i16 < 16) {
                    bArr6[i13] = bArr3[i16];
                    i16++;
                    i13++;
                }
            }
            this.a = 0L;
            return bArr6;
        }
    }

    public String d(String str) {
        if (!this.d) {
            this.a = st.e;
            return new String();
        } else if (str != null && str.length() != 0) {
            byte[] bArr = new byte[str.length()];
            for (int i = 0; i < str.length(); i++) {
                bArr[i] = (byte) str.charAt(i);
            }
            return e(bArr);
        } else {
            this.a = st.b;
            return new String();
        }
    }

    public String e(byte[] bArr) {
        if (!this.d) {
            this.a = st.e;
            return new String();
        } else if (bArr == null) {
            this.a = st.b;
            return new String();
        } else {
            return lm.f(c(bArr));
        }
    }

    public long f() {
        return this.a;
    }

    public boolean g(byte[] bArr) {
        int length = bArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = (char) bArr[i];
        }
        this.d = true;
        d dVar = new d();
        this.c = dVar;
        return dVar.c(cArr);
    }

    public boolean h(byte[] bArr, int i) {
        d dVar = new d();
        this.c = dVar;
        boolean b = dVar.b(bArr);
        this.d = true;
        return b;
    }

    public void i(boolean z) {
        this.b = z;
    }

    public void j() {
        this.d = false;
        this.c.d();
    }

    public bz4() {
        this.c = new d();
        this.b = false;
        this.a = 0L;
        j();
    }
}
