package defpackage;

import java.util.concurrent.Executor;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: Executors.kt */
/* renamed from: zy0  reason: default package */
/* loaded from: classes2.dex */
public final class zy0 {
    public static final CoroutineDispatcher a(Executor executor) {
        sp0 sp0Var = executor instanceof sp0 ? (sp0) executor : null;
        CoroutineDispatcher coroutineDispatcher = sp0Var != null ? sp0Var.a : null;
        return coroutineDispatcher == null ? new uy0(executor) : coroutineDispatcher;
    }
}
