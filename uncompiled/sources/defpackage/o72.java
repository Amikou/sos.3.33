package defpackage;

/* compiled from: MemoryChunkUtil.java */
/* renamed from: o72  reason: default package */
/* loaded from: classes.dex */
public class o72 {
    public static int a(int i, int i2, int i3) {
        return Math.min(Math.max(0, i3 - i), i2);
    }

    public static void b(int i, int i2, int i3, int i4, int i5) {
        xt2.b(Boolean.valueOf(i4 >= 0));
        xt2.b(Boolean.valueOf(i >= 0));
        xt2.b(Boolean.valueOf(i3 >= 0));
        xt2.b(Boolean.valueOf(i + i4 <= i5));
        xt2.b(Boolean.valueOf(i3 + i4 <= i2));
    }
}
