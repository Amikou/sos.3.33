package defpackage;

import com.google.android.gms.internal.measurement.v1;
import com.google.android.gms.internal.measurement.z1;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: wt5  reason: default package */
/* loaded from: classes.dex */
public final class wt5 extends tt5 {
    @Override // defpackage.tt5
    public final boolean a(z1 z1Var) {
        return z1Var instanceof cv5;
    }

    @Override // defpackage.tt5
    public final v1 b(Object obj) {
        v1 v1Var = ((cv5) obj).zza;
        throw null;
    }

    @Override // defpackage.tt5
    public final void c(Object obj) {
        v1 v1Var = ((cv5) obj).zza;
        throw null;
    }
}
