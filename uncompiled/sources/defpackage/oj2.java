package defpackage;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSEmailSubscriptionStateChanges.java */
/* renamed from: oj2  reason: default package */
/* loaded from: classes2.dex */
public class oj2 {
    public nj2 a;
    public nj2 b;

    public oj2(nj2 nj2Var, nj2 nj2Var2) {
        this.a = nj2Var;
        this.b = nj2Var2;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("from", this.a.f());
            jSONObject.put("to", this.b.f());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return a().toString();
    }
}
