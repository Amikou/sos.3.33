package defpackage;

import com.google.protobuf.ProtoSyntax;
import com.google.protobuf.m0;

/* compiled from: RawMessageInfo.java */
/* renamed from: w33  reason: default package */
/* loaded from: classes2.dex */
public final class w33 implements z72 {
    public final m0 a;
    public final String b;
    public final Object[] c;
    public final int d;

    @Override // defpackage.z72
    public boolean a() {
        return (this.d & 2) == 2;
    }

    @Override // defpackage.z72
    public m0 b() {
        return this.a;
    }

    @Override // defpackage.z72
    public ProtoSyntax c() {
        return (this.d & 1) == 1 ? ProtoSyntax.PROTO2 : ProtoSyntax.PROTO3;
    }

    public Object[] d() {
        return this.c;
    }

    public String e() {
        return this.b;
    }
}
