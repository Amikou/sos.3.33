package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: mw5  reason: default package */
/* loaded from: classes.dex */
public final class mw5 {
    public final String a;
    public final String b;
    public final String c;
    public final long d;
    public final Object e;

    public mw5(String str, String str2, String str3, long j, Object obj) {
        zt2.f(str);
        zt2.f(str3);
        zt2.j(obj);
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = j;
        this.e = obj;
    }
}
