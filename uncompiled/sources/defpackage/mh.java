package defpackage;

import java.util.concurrent.locks.ReentrantLock;
import kotlin.NoWhenBranchMatchedException;
import kotlinx.coroutines.channels.AbstractChannel;
import kotlinx.coroutines.channels.BufferOverflow;
import kotlinx.coroutines.internal.OnUndeliveredElementKt;
import kotlinx.coroutines.internal.UndeliveredElementException;

/* compiled from: ArrayChannel.kt */
/* renamed from: mh  reason: default package */
/* loaded from: classes2.dex */
public class mh<E> extends AbstractChannel<E> {
    public final int h0;
    public final BufferOverflow i0;
    public final ReentrantLock j0;
    public Object[] k0;
    public int l0;
    private volatile /* synthetic */ int size;

    /* compiled from: ArrayChannel.kt */
    /* renamed from: mh$a */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[BufferOverflow.valuesCustom().length];
            iArr[BufferOverflow.SUSPEND.ordinal()] = 1;
            iArr[BufferOverflow.DROP_LATEST.ordinal()] = 2;
            iArr[BufferOverflow.DROP_OLDEST.ordinal()] = 3;
            a = iArr;
        }
    }

    public mh(int i, BufferOverflow bufferOverflow, tc1<? super E, te4> tc1Var) {
        super(tc1Var);
        this.h0 = i;
        this.i0 = bufferOverflow;
        if (i >= 1) {
            this.j0 = new ReentrantLock();
            Object[] objArr = new Object[Math.min(i, 8)];
            zh.m(objArr, q4.a, 0, 0, 6, null);
            te4 te4Var = te4.a;
            this.k0 = objArr;
            this.size = 0;
            return;
        }
        throw new IllegalArgumentException(("ArrayChannel capacity must be at least 1, but " + i + " was specified").toString());
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public boolean F(e43<? super E> e43Var) {
        ReentrantLock reentrantLock = this.j0;
        reentrantLock.lock();
        try {
            return super.F(e43Var);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean G() {
        return false;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean H() {
        return this.size == 0;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public boolean I() {
        ReentrantLock reentrantLock = this.j0;
        reentrantLock.lock();
        try {
            return super.I();
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public void J(boolean z) {
        tc1<E, te4> tc1Var = this.a;
        ReentrantLock reentrantLock = this.j0;
        reentrantLock.lock();
        try {
            int i = this.size;
            UndeliveredElementException undeliveredElementException = null;
            for (int i2 = 0; i2 < i; i2++) {
                Object obj = this.k0[this.l0];
                if (tc1Var != null && obj != q4.a) {
                    undeliveredElementException = OnUndeliveredElementKt.c(tc1Var, obj, undeliveredElementException);
                }
                Object[] objArr = this.k0;
                int i3 = this.l0;
                objArr[i3] = q4.a;
                this.l0 = (i3 + 1) % objArr.length;
            }
            this.size = 0;
            te4 te4Var = te4.a;
            reentrantLock.unlock();
            super.J(z);
            if (undeliveredElementException != null) {
                throw undeliveredElementException;
            }
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public Object N() {
        ReentrantLock reentrantLock = this.j0;
        reentrantLock.lock();
        try {
            int i = this.size;
            if (i == 0) {
                Object j = j();
                if (j == null) {
                    j = q4.d;
                }
                return j;
            }
            Object[] objArr = this.k0;
            int i2 = this.l0;
            Object obj = objArr[i2];
            tj3 tj3Var = null;
            objArr[i2] = null;
            this.size = i - 1;
            Object obj2 = q4.d;
            if (i == this.h0) {
                tj3 tj3Var2 = null;
                while (true) {
                    tj3 A = A();
                    if (A == null) {
                        tj3Var = tj3Var2;
                        break;
                    }
                    k24 B = A.B(null);
                    if (B != null) {
                        if (ze0.a()) {
                            if (!(B == qv.a)) {
                                throw new AssertionError();
                            }
                        }
                        obj2 = A.z();
                        r6 = true;
                        tj3Var = A;
                    } else {
                        A.C();
                        tj3Var2 = A;
                    }
                }
            }
            if (obj2 != q4.d && !(obj2 instanceof d00)) {
                this.size = i;
                Object[] objArr2 = this.k0;
                objArr2[(this.l0 + i) % objArr2.length] = obj2;
            }
            this.l0 = (this.l0 + 1) % this.k0.length;
            te4 te4Var = te4.a;
            if (r6) {
                fs1.d(tj3Var);
                tj3Var.y();
            }
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }

    public final void Q(int i, E e) {
        if (i < this.h0) {
            R(i);
            Object[] objArr = this.k0;
            objArr[(this.l0 + i) % objArr.length] = e;
            return;
        }
        if (ze0.a()) {
            if (!(this.i0 == BufferOverflow.DROP_OLDEST)) {
                throw new AssertionError();
            }
        }
        Object[] objArr2 = this.k0;
        int i2 = this.l0;
        objArr2[i2 % objArr2.length] = null;
        objArr2[(i + i2) % objArr2.length] = e;
        this.l0 = (i2 + 1) % objArr2.length;
    }

    public final void R(int i) {
        Object[] objArr = this.k0;
        if (i >= objArr.length) {
            int min = Math.min(objArr.length * 2, this.h0);
            Object[] objArr2 = new Object[min];
            if (i > 0) {
                int i2 = 0;
                while (true) {
                    int i3 = i2 + 1;
                    Object[] objArr3 = this.k0;
                    objArr2[i2] = objArr3[(this.l0 + i2) % objArr3.length];
                    if (i3 >= i) {
                        break;
                    }
                    i2 = i3;
                }
            }
            zh.k(objArr2, q4.a, i, min);
            this.k0 = objArr2;
            this.l0 = 0;
        }
    }

    public final k24 S(int i) {
        if (i < this.h0) {
            this.size = i + 1;
            return null;
        }
        int i2 = a.a[this.i0.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 == 3) {
                    return null;
                }
                throw new NoWhenBranchMatchedException();
            }
            return q4.b;
        }
        return q4.c;
    }

    @Override // defpackage.h5
    public Object f(tj3 tj3Var) {
        ReentrantLock reentrantLock = this.j0;
        reentrantLock.lock();
        try {
            return super.f(tj3Var);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // defpackage.h5
    public String g() {
        return "(buffer:capacity=" + this.h0 + ",size=" + this.size + ')';
    }

    @Override // defpackage.h5
    public final boolean s() {
        return false;
    }

    @Override // defpackage.h5
    public final boolean t() {
        return this.size == this.h0 && this.i0 == BufferOverflow.SUSPEND;
    }

    @Override // defpackage.h5
    public Object v(E e) {
        l43<E> z;
        k24 g;
        ReentrantLock reentrantLock = this.j0;
        reentrantLock.lock();
        try {
            int i = this.size;
            d00<?> j = j();
            if (j == null) {
                k24 S = S(i);
                if (S == null) {
                    if (i == 0) {
                        do {
                            z = z();
                            if (z != null) {
                                if (z instanceof d00) {
                                    this.size = i;
                                    return z;
                                }
                                g = z.g(e, null);
                            }
                        } while (g == null);
                        if (ze0.a()) {
                            if (!(g == qv.a)) {
                                throw new AssertionError();
                            }
                        }
                        this.size = i;
                        te4 te4Var = te4.a;
                        reentrantLock.unlock();
                        z.f(e);
                        return z.c();
                    }
                    Q(i, e);
                    return q4.b;
                }
                return S;
            }
            return j;
        } finally {
            reentrantLock.unlock();
        }
    }
}
