package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentWalletConnectSmartContractCallBinding.java */
/* renamed from: ub1  reason: default package */
/* loaded from: classes2.dex */
public final class ub1 {
    public final MaterialButton a;
    public final rp3 b;
    public final MaterialTextView c;
    public final AppCompatTextView d;
    public final AppCompatTextView e;
    public final MaterialTextView f;
    public final MaterialTextView g;
    public final MaterialTextView h;
    public final MaterialTextView i;

    public ub1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialCardView materialCardView, rp3 rp3Var, MaterialTextView materialTextView, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5) {
        this.a = materialButton;
        this.b = rp3Var;
        this.c = materialTextView;
        this.d = appCompatTextView;
        this.e = appCompatTextView2;
        this.f = materialTextView2;
        this.g = materialTextView3;
        this.h = materialTextView4;
        this.i = materialTextView5;
    }

    public static ub1 a(View view) {
        int i = R.id.btnConfirm;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConfirm);
        if (materialButton != null) {
            i = R.id.ccNoteWrapper;
            MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ccNoteWrapper);
            if (materialCardView != null) {
                i = R.id.toolbarWrapper;
                View a = ai4.a(view, R.id.toolbarWrapper);
                if (a != null) {
                    rp3 a2 = rp3.a(a);
                    i = R.id.txtAssetName;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAssetName);
                    if (materialTextView != null) {
                        i = R.id.txtBalanceFiat;
                        AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtBalanceFiat);
                        if (appCompatTextView != null) {
                            i = R.id.txtBalanceNative;
                            AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txtBalanceNative);
                            if (appCompatTextView2 != null) {
                                i = R.id.txtDAppLink;
                                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtDAppLink);
                                if (materialTextView2 != null) {
                                    i = R.id.txtMaxTotal;
                                    MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtMaxTotal);
                                    if (materialTextView3 != null) {
                                        i = R.id.txtNetWorkFee;
                                        MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtNetWorkFee);
                                        if (materialTextView4 != null) {
                                            i = R.id.txtWalletName;
                                            MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txtWalletName);
                                            if (materialTextView5 != null) {
                                                return new ub1((ConstraintLayout) view, materialButton, materialCardView, a2, materialTextView, appCompatTextView, appCompatTextView2, materialTextView2, materialTextView3, materialTextView4, materialTextView5);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
