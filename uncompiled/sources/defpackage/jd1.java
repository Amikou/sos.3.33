package defpackage;

/* compiled from: Function.java */
/* renamed from: jd1  reason: default package */
/* loaded from: classes2.dex */
public interface jd1<F, T> {
    T apply(F f);

    boolean equals(Object obj);
}
