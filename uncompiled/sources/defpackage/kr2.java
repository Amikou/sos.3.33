package defpackage;

import android.os.Bundle;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.e;

/* renamed from: kr2  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class kr2 implements e.a {
    public static final /* synthetic */ kr2 a = new kr2();

    @Override // androidx.media3.common.e.a
    public final e a(Bundle bundle) {
        return new PlaybackException(bundle);
    }
}
