package defpackage;

import com.bumptech.glide.load.DataSource;
import defpackage.hb4;

/* compiled from: NoTransition.java */
/* renamed from: sg2  reason: default package */
/* loaded from: classes.dex */
public class sg2<R> implements hb4<R> {
    public static final sg2<?> a = new sg2<>();
    public static final ib4<?> b = new a();

    /* compiled from: NoTransition.java */
    /* renamed from: sg2$a */
    /* loaded from: classes.dex */
    public static class a<R> implements ib4<R> {
        @Override // defpackage.ib4
        public hb4<R> a(DataSource dataSource, boolean z) {
            return sg2.a;
        }
    }

    public static <R> ib4<R> b() {
        return (ib4<R>) b;
    }

    @Override // defpackage.hb4
    public boolean a(Object obj, hb4.a aVar) {
        return false;
    }
}
