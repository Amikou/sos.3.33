package defpackage;

import io.reactivex.internal.schedulers.RxThreadFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: SingleScheduler.java */
/* renamed from: xp3  reason: default package */
/* loaded from: classes2.dex */
public final class xp3 extends bd3 {
    public static final RxThreadFactory b;
    public static final ScheduledExecutorService c;
    public final AtomicReference<ScheduledExecutorService> a;

    static {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(0);
        c = newScheduledThreadPool;
        newScheduledThreadPool.shutdown();
        b = new RxThreadFactory("RxSingleScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.single-priority", 5).intValue())), true);
    }

    public xp3() {
        this(b);
    }

    public static ScheduledExecutorService a(ThreadFactory threadFactory) {
        return dd3.a(threadFactory);
    }

    public xp3(ThreadFactory threadFactory) {
        AtomicReference<ScheduledExecutorService> atomicReference = new AtomicReference<>();
        this.a = atomicReference;
        atomicReference.lazySet(a(threadFactory));
    }
}
