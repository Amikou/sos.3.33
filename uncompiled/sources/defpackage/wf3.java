package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: wf3  reason: default package */
/* loaded from: classes2.dex */
public class wf3 extends xs0.b {
    public xf3 j;

    /* renamed from: wf3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] f = ad2.f();
            long[] f2 = ad2.f();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                long j = ((i3 ^ i) - 1) >> 31;
                for (int i4 = 0; i4 < 2; i4++) {
                    long j2 = f[i4];
                    long[] jArr = this.b;
                    f[i4] = j2 ^ (jArr[i2 + i4] & j);
                    f2[i4] = f2[i4] ^ (jArr[(i2 + 2) + i4] & j);
                }
                i2 += 4;
            }
            return wf3.this.i(new vf3(f), new vf3(f2), false);
        }
    }

    public wf3() {
        super(113, 9, 0, 0);
        this.j = new xf3(this, null, null);
        this.b = n(new BigInteger(1, pk1.a("003088250CA6E7C7FE649CE85820F7")));
        this.c = n(new BigInteger(1, pk1.a("00E8BEE4D3E2260744188BE0E9C723")));
        this.d = new BigInteger(1, pk1.a("0100000000000000D9CCEC8A39E56F"));
        this.e = BigInteger.valueOf(2L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return false;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new wf3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 2 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            ad2.d(((vf3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 2;
            ad2.d(((vf3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 2;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new xf3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new xf3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new vf3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 113;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
