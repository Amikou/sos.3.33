package defpackage;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: pu3  reason: default package */
/* loaded from: classes.dex */
public class pu3 {
    public final Resources a;
    public final String b;

    public pu3(@RecentlyNonNull Context context) {
        zt2.j(context);
        Resources resources = context.getResources();
        this.a = resources;
        this.b = resources.getResourcePackageName(q13.common_google_play_services_unknown_issue);
    }

    @RecentlyNullable
    public String a(@RecentlyNonNull String str) {
        int identifier = this.a.getIdentifier(str, Utf8String.TYPE_NAME, this.b);
        if (identifier == 0) {
            return null;
        }
        return this.a.getString(identifier);
    }
}
