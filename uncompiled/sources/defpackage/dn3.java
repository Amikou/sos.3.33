package defpackage;

/* compiled from: SettingsData.java */
/* renamed from: dn3  reason: default package */
/* loaded from: classes2.dex */
public class dn3 implements cn3 {
    public final vf a;
    public final lm3 b;
    public final s21 c;
    public final long d;

    public dn3(long j, vf vfVar, lm3 lm3Var, s21 s21Var, int i, int i2) {
        this.d = j;
        this.a = vfVar;
        this.b = lm3Var;
        this.c = s21Var;
    }

    @Override // defpackage.cn3
    public s21 a() {
        return this.c;
    }

    @Override // defpackage.cn3
    public lm3 b() {
        return this.b;
    }

    public vf c() {
        return this.a;
    }

    public long d() {
        return this.d;
    }

    public boolean e(long j) {
        return this.d < j;
    }
}
