package defpackage;

import android.graphics.Bitmap;
import com.facebook.imagepipeline.decoder.DecodeException;
import java.io.InputStream;
import java.util.Map;

/* compiled from: DefaultImageDecoder.java */
/* renamed from: uj0  reason: default package */
/* loaded from: classes.dex */
public class uj0 implements tn1 {
    public final tn1 a;
    public final tn1 b;
    public final dr2 c;
    public final tn1 d;
    public final Map<wn1, tn1> e;

    /* compiled from: DefaultImageDecoder.java */
    /* renamed from: uj0$a */
    /* loaded from: classes.dex */
    public class a implements tn1 {
        public a() {
        }

        @Override // defpackage.tn1
        public com.facebook.imagepipeline.image.a a(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
            wn1 l = zu0Var.l();
            if (l == wj0.a) {
                return uj0.this.d(zu0Var, i, xw2Var, rn1Var);
            }
            if (l == wj0.c) {
                return uj0.this.c(zu0Var, i, xw2Var, rn1Var);
            }
            if (l == wj0.j) {
                return uj0.this.b(zu0Var, i, xw2Var, rn1Var);
            }
            if (l != wn1.b) {
                return uj0.this.e(zu0Var, rn1Var);
            }
            throw new DecodeException("unknown image format", zu0Var);
        }
    }

    public uj0(tn1 tn1Var, tn1 tn1Var2, dr2 dr2Var) {
        this(tn1Var, tn1Var2, dr2Var, null);
    }

    @Override // defpackage.tn1
    public com.facebook.imagepipeline.image.a a(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
        InputStream m;
        tn1 tn1Var;
        tn1 tn1Var2 = rn1Var.i;
        if (tn1Var2 != null) {
            return tn1Var2.a(zu0Var, i, xw2Var, rn1Var);
        }
        wn1 l = zu0Var.l();
        if ((l == null || l == wn1.b) && (m = zu0Var.m()) != null) {
            l = xn1.c(m);
            zu0Var.a0(l);
        }
        Map<wn1, tn1> map = this.e;
        if (map != null && (tn1Var = map.get(l)) != null) {
            return tn1Var.a(zu0Var, i, xw2Var, rn1Var);
        }
        return this.d.a(zu0Var, i, xw2Var, rn1Var);
    }

    public com.facebook.imagepipeline.image.a b(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
        tn1 tn1Var = this.b;
        if (tn1Var != null) {
            return tn1Var.a(zu0Var, i, xw2Var, rn1Var);
        }
        throw new DecodeException("Animated WebP support not set up!", zu0Var);
    }

    public com.facebook.imagepipeline.image.a c(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
        tn1 tn1Var;
        if (zu0Var.v() != -1 && zu0Var.j() != -1) {
            if (!rn1Var.f && (tn1Var = this.a) != null) {
                return tn1Var.a(zu0Var, i, xw2Var, rn1Var);
            }
            return e(zu0Var, rn1Var);
        }
        throw new DecodeException("image width or height is incorrect", zu0Var);
    }

    public c00 d(zu0 zu0Var, int i, xw2 xw2Var, rn1 rn1Var) {
        com.facebook.common.references.a<Bitmap> c = this.c.c(zu0Var, rn1Var.g, null, i, rn1Var.k);
        try {
            bb4.a(rn1Var.j, c);
            c00 c00Var = new c00(c, xw2Var, zu0Var.q(), zu0Var.h());
            c00Var.d("is_rounded", false);
            return c00Var;
        } finally {
            c.close();
        }
    }

    public c00 e(zu0 zu0Var, rn1 rn1Var) {
        com.facebook.common.references.a<Bitmap> b = this.c.b(zu0Var, rn1Var.g, null, rn1Var.k);
        try {
            bb4.a(rn1Var.j, b);
            c00 c00Var = new c00(b, jp1.d, zu0Var.q(), zu0Var.h());
            c00Var.d("is_rounded", false);
            return c00Var;
        } finally {
            b.close();
        }
    }

    public uj0(tn1 tn1Var, tn1 tn1Var2, dr2 dr2Var, Map<wn1, tn1> map) {
        this.d = new a();
        this.a = tn1Var;
        this.b = tn1Var2;
        this.c = dr2Var;
        this.e = map;
    }
}
