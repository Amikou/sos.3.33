package defpackage;

/* compiled from: AbstractChannel.kt */
/* renamed from: q4  reason: default package */
/* loaded from: classes2.dex */
public final class q4 {
    public static final k24 a = new k24("EMPTY");
    public static final k24 b = new k24("OFFER_SUCCESS");
    public static final k24 c = new k24("OFFER_FAILED");
    public static final k24 d = new k24("POLL_FAILED");
    public static final k24 e = new k24("ENQUEUE_FAILED");
    public static final k24 f = new k24("ON_CLOSE_HANDLER_INVOKED");
}
