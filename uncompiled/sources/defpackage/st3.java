package defpackage;

/* compiled from: StopLogic.java */
/* renamed from: st3  reason: default package */
/* loaded from: classes.dex */
public class st3 extends w92 {
    public tt3 a;
    public bs3 b;
    public rt3 c;

    public st3() {
        tt3 tt3Var = new tt3();
        this.a = tt3Var;
        this.c = tt3Var;
    }

    @Override // defpackage.w92
    public float a() {
        return this.c.b();
    }

    public void b(float f, float f2, float f3, float f4, float f5, float f6) {
        tt3 tt3Var = this.a;
        this.c = tt3Var;
        tt3Var.d(f, f2, f3, f4, f5, f6);
    }

    public boolean c() {
        return this.c.a();
    }

    public void d(float f, float f2, float f3, float f4, float f5, float f6, float f7, int i) {
        if (this.b == null) {
            this.b = new bs3();
        }
        bs3 bs3Var = this.b;
        this.c = bs3Var;
        bs3Var.d(f, f2, f3, f4, f5, f6, f7, i);
    }

    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        return this.c.getInterpolation(f);
    }
}
