package defpackage;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.c;
import java.io.IOException;
import java.math.BigDecimal;

/* compiled from: GeneratorBase.java */
/* renamed from: we1  reason: default package */
/* loaded from: classes.dex */
public abstract class we1 extends JsonGenerator {
    public static final int j0 = (JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS.getMask() | JsonGenerator.Feature.ESCAPE_NON_ASCII.getMask()) | JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION.getMask();
    public c f0;
    public int g0;
    public boolean h0;
    public mw1 i0;

    public we1(int i, c cVar) {
        this.g0 = i;
        this.f0 = cVar;
        this.i0 = mw1.o(JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION.enabledIn(i) ? ms0.e(this) : null);
        this.h0 = JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS.enabledIn(i);
    }

    public final boolean F1(JsonGenerator.Feature feature) {
        return (feature.getMask() & this.g0) != 0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void J0(Object obj) throws IOException {
        if (obj == null) {
            m0();
            return;
        }
        c cVar = this.f0;
        if (cVar != null) {
            cVar.writeValue(this, obj);
        } else {
            d(obj);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void Z0(yl3 yl3Var) throws IOException {
        z1("write raw value");
        U0(yl3Var);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void c1(String str) throws IOException {
        z1("write raw value");
        W0(str);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator i(JsonGenerator.Feature feature) {
        int mask = feature.getMask();
        this.g0 &= ~mask;
        if ((mask & j0) != 0) {
            if (feature == JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS) {
                this.h0 = false;
            } else if (feature == JsonGenerator.Feature.ESCAPE_NON_ASCII) {
                w(0);
            } else if (feature == JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION) {
                this.i0 = this.i0.s(null);
            }
        }
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public int j() {
        return this.g0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public cw1 l() {
        return this.i0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator q(int i, int i2) {
        int i3 = this.g0;
        int i4 = (i & i2) | ((~i2) & i3);
        int i5 = i3 ^ i4;
        if (i5 != 0) {
            this.g0 = i4;
            w1(i4, i5);
        }
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void u(Object obj) {
        this.i0.h(obj);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    @Deprecated
    public JsonGenerator v(int i) {
        int i2 = this.g0 ^ i;
        this.g0 = i;
        if (i2 != 0) {
            w1(i, i2);
        }
        return this;
    }

    public String v1(BigDecimal bigDecimal) throws IOException {
        if (JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN.enabledIn(this.g0)) {
            int scale = bigDecimal.scale();
            if (scale < -9999 || scale > 9999) {
                a(String.format("Attempt to write plain `java.math.BigDecimal` (see JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN) with illegal scale (%d): needs to be between [-%d, %d]", Integer.valueOf(scale), 9999, 9999));
            }
            return bigDecimal.toPlainString();
        }
        return bigDecimal.toString();
    }

    public void w1(int i, int i2) {
        if ((j0 & i2) == 0) {
            return;
        }
        this.h0 = JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS.enabledIn(i);
        JsonGenerator.Feature feature = JsonGenerator.Feature.ESCAPE_NON_ASCII;
        if (feature.enabledIn(i2)) {
            if (feature.enabledIn(i)) {
                w(127);
            } else {
                w(0);
            }
        }
        JsonGenerator.Feature feature2 = JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION;
        if (feature2.enabledIn(i2)) {
            if (feature2.enabledIn(i)) {
                if (this.i0.p() == null) {
                    this.i0 = this.i0.s(ms0.e(this));
                    return;
                }
                return;
            }
            this.i0 = this.i0.s(null);
        }
    }

    public final int y1(int i, int i2) throws IOException {
        if (i2 < 56320 || i2 > 57343) {
            a("Incomplete surrogate pair: first char 0x" + Integer.toHexString(i) + ", second 0x" + Integer.toHexString(i2));
        }
        return ((i - 55296) << 10) + 65536 + (i2 - 56320);
    }

    public abstract void z1(String str) throws IOException;
}
