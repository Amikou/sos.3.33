package defpackage;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ScaleGestureDetectorFixed.java */
/* renamed from: nc3  reason: default package */
/* loaded from: classes.dex */
public class nc3 extends ScaleGestureDetector {
    public float a;
    public float b;

    public nc3(Context context, ScaleGestureDetector.OnScaleGestureListener onScaleGestureListener) {
        super(context, onScaleGestureListener);
        b();
    }

    @TargetApi(19)
    public final boolean a() {
        return Build.VERSION.SDK_INT >= 19 && isQuickScaleEnabled() && getCurrentSpan() == getCurrentSpanY();
    }

    public final void b() {
        long currentTimeMillis = System.currentTimeMillis();
        MotionEvent obtain = MotionEvent.obtain(currentTimeMillis, currentTimeMillis, 3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0);
        onTouchEvent(obtain);
        obtain.recycle();
    }

    @Override // android.view.ScaleGestureDetector
    public float getScaleFactor() {
        float scaleFactor = super.getScaleFactor();
        if (a()) {
            float f = this.a;
            float f2 = this.b;
            if ((f <= f2 || scaleFactor <= 1.0f) && (f >= f2 || scaleFactor >= 1.0f)) {
                return 1.0f;
            }
            return Math.max(0.8f, Math.min(scaleFactor, 1.25f));
        }
        return scaleFactor;
    }

    @Override // android.view.ScaleGestureDetector
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        this.b = this.a;
        this.a = motionEvent.getY();
        if (motionEvent.getActionMasked() == 0) {
            this.b = motionEvent.getY();
        }
        return onTouchEvent;
    }
}
