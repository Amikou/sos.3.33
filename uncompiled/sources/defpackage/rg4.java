package defpackage;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Locale;

/* compiled from: Utils.java */
/* renamed from: rg4  reason: default package */
/* loaded from: classes3.dex */
public class rg4 {

    /* compiled from: Utils.java */
    /* renamed from: rg4$a */
    /* loaded from: classes3.dex */
    public static class a implements Runnable {
        public final /* synthetic */ com.google.android.material.bottomsheet.a a;

        public a(com.google.android.material.bottomsheet.a aVar) {
            this.a = aVar;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.cancel();
        }
    }

    /* compiled from: Utils.java */
    /* renamed from: rg4$b */
    /* loaded from: classes3.dex */
    public static class b implements View.OnClickListener {
        public final /* synthetic */ View.OnClickListener a;
        public final /* synthetic */ View f0;
        public final /* synthetic */ com.google.android.material.bottomsheet.a g0;

        public b(View.OnClickListener onClickListener, View view, com.google.android.material.bottomsheet.a aVar) {
            this.a = onClickListener;
            this.f0 = view;
            this.g0 = aVar;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            this.a.onClick(this.f0);
            this.g0.cancel();
        }
    }

    /* compiled from: Utils.java */
    /* renamed from: rg4$c */
    /* loaded from: classes3.dex */
    public static class c implements DialogInterface.OnCancelListener {
        public final /* synthetic */ Handler a;
        public final /* synthetic */ Runnable f0;

        public c(Handler handler, Runnable runnable) {
            this.a = handler;
            this.f0 = runnable;
        }

        @Override // android.content.DialogInterface.OnCancelListener
        public void onCancel(DialogInterface dialogInterface) {
            this.a.removeCallbacks(this.f0);
        }
    }

    /* compiled from: Utils.java */
    /* renamed from: rg4$d */
    /* loaded from: classes3.dex */
    public static class d implements DialogInterface.OnDismissListener {
        public final /* synthetic */ Handler a;
        public final /* synthetic */ Runnable f0;

        public d(Handler handler, Runnable runnable) {
            this.a = handler;
            this.f0 = runnable;
        }

        @Override // android.content.DialogInterface.OnDismissListener
        public void onDismiss(DialogInterface dialogInterface) {
            this.a.removeCallbacks(this.f0);
        }
    }

    /* compiled from: Utils.java */
    /* renamed from: rg4$e */
    /* loaded from: classes3.dex */
    public static class e implements ya4 {
        public final int a;
        public final int b;

        public e(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @Override // defpackage.ya4
        public String key() {
            return String.format(Locale.US, "rounded-%s-%s", Integer.valueOf(this.a), Integer.valueOf(this.b));
        }

        @Override // defpackage.ya4
        public Bitmap transform(Bitmap bitmap) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            paint.setShader(new BitmapShader(bitmap, tileMode, tileMode));
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            int i = this.b;
            RectF rectF = new RectF(i, i, bitmap.getWidth() - this.b, bitmap.getHeight() - this.b);
            int i2 = this.a;
            canvas.drawRoundRect(rectF, i2, i2, paint);
            if (bitmap != createBitmap) {
                bitmap.recycle();
            }
            return createBitmap;
        }
    }

    public static int a(Context context, int i) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i, typedValue, true)) {
            int i2 = typedValue.resourceId;
            return i2 == 0 ? typedValue.data : m70.d(context, i2);
        }
        return -16777216;
    }

    public static void b(ImageView imageView, int i) {
        if (imageView == null || imageView.getDrawable() == null) {
            return;
        }
        androidx.core.graphics.drawable.a.n(androidx.core.graphics.drawable.a.r(imageView.getDrawable()).mutate(), i);
        imageView.invalidate();
    }

    public static boolean c(String str, Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(str, 128).enabled;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static ya4 d(Context context, int i) {
        return new e(context.getResources().getDimensionPixelOffset(i), 0);
    }

    public static void e(View view, String str, long j, CharSequence charSequence, View.OnClickListener onClickListener) {
        com.google.android.material.bottomsheet.a aVar = new com.google.android.material.bottomsheet.a(view.getContext());
        Handler handler = new Handler();
        a aVar2 = new a(aVar);
        aVar.setContentView(c13.belvedere_bottom_sheet);
        TextView textView = (TextView) aVar.findViewById(g03.belvedere_bottom_sheet_message_text);
        if (textView != null) {
            textView.setText(str);
        }
        TextView textView2 = (TextView) aVar.findViewById(g03.belvedere_bottom_sheet_actions_text);
        if (textView2 != null) {
            textView2.setText(charSequence);
            textView2.setOnClickListener(new b(onClickListener, view, aVar));
        }
        aVar.setCancelable(true);
        aVar.setOnCancelListener(new c(handler, aVar2));
        aVar.setOnDismissListener(new d(handler, aVar2));
        aVar.show();
        handler.postDelayed(aVar2, j);
    }

    public static void f(View view, boolean z) {
        view.findViewById(g03.image_stream_toolbar).setVisibility(z ? 0 : 8);
        View findViewById = view.findViewById(g03.image_stream_toolbar_container);
        if (findViewById != null) {
            findViewById.setVisibility(z ? 0 : 8);
        }
    }
}
