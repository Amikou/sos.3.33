package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentTokenListBinding.java */
/* renamed from: gb1  reason: default package */
/* loaded from: classes2.dex */
public final class gb1 {
    public final RecyclerView a;
    public final yd3 b;
    public final rp3 c;

    public gb1(ConstraintLayout constraintLayout, RecyclerView recyclerView, yd3 yd3Var, rp3 rp3Var) {
        this.a = recyclerView;
        this.b = yd3Var;
        this.c = rp3Var;
    }

    public static gb1 a(View view) {
        int i = R.id.rvTokenList;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvTokenList);
        if (recyclerView != null) {
            i = R.id.searchBar;
            View a = ai4.a(view, R.id.searchBar);
            if (a != null) {
                yd3 a2 = yd3.a(a);
                View a3 = ai4.a(view, R.id.toolbar);
                if (a3 != null) {
                    return new gb1((ConstraintLayout) view, recyclerView, a2, rp3.a(a3));
                }
                i = R.id.toolbar;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
