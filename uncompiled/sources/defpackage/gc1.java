package defpackage;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import defpackage.tw3;
import java.io.File;

/* compiled from: FrameworkSQLiteOpenHelper.java */
/* renamed from: gc1  reason: default package */
/* loaded from: classes.dex */
public class gc1 implements tw3 {
    public final Context a;
    public final String f0;
    public final tw3.a g0;
    public final boolean h0;
    public final Object i0 = new Object();
    public a j0;
    public boolean k0;

    /* compiled from: FrameworkSQLiteOpenHelper.java */
    /* renamed from: gc1$a */
    /* loaded from: classes.dex */
    public static class a extends SQLiteOpenHelper {
        public final fc1[] a;
        public final tw3.a f0;
        public boolean g0;

        /* compiled from: FrameworkSQLiteOpenHelper.java */
        /* renamed from: gc1$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0175a implements DatabaseErrorHandler {
            public final /* synthetic */ tw3.a a;
            public final /* synthetic */ fc1[] b;

            public C0175a(tw3.a aVar, fc1[] fc1VarArr) {
                this.a = aVar;
                this.b = fc1VarArr;
            }

            @Override // android.database.DatabaseErrorHandler
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.a.c(a.b(this.b, sQLiteDatabase));
            }
        }

        public a(Context context, String str, fc1[] fc1VarArr, tw3.a aVar) {
            super(context, str, null, aVar.a, new C0175a(aVar, fc1VarArr));
            this.f0 = aVar;
            this.a = fc1VarArr;
        }

        public static fc1 b(fc1[] fc1VarArr, SQLiteDatabase sQLiteDatabase) {
            fc1 fc1Var = fc1VarArr[0];
            if (fc1Var == null || !fc1Var.a(sQLiteDatabase)) {
                fc1VarArr[0] = new fc1(sQLiteDatabase);
            }
            return fc1VarArr[0];
        }

        public fc1 a(SQLiteDatabase sQLiteDatabase) {
            return b(this.a, sQLiteDatabase);
        }

        public synchronized sw3 c() {
            this.g0 = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.g0) {
                close();
                return c();
            }
            return a(writableDatabase);
        }

        @Override // android.database.sqlite.SQLiteOpenHelper, java.lang.AutoCloseable
        public synchronized void close() {
            super.close();
            this.a[0] = null;
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.f0.b(a(sQLiteDatabase));
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.f0.d(a(sQLiteDatabase));
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.g0 = true;
            this.f0.e(a(sQLiteDatabase), i, i2);
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (this.g0) {
                return;
            }
            this.f0.f(a(sQLiteDatabase));
        }

        @Override // android.database.sqlite.SQLiteOpenHelper
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.g0 = true;
            this.f0.g(a(sQLiteDatabase), i, i2);
        }
    }

    public gc1(Context context, String str, tw3.a aVar, boolean z) {
        this.a = context;
        this.f0 = str;
        this.g0 = aVar;
        this.h0 = z;
    }

    @Override // defpackage.tw3
    public sw3 D0() {
        return a().c();
    }

    public final a a() {
        a aVar;
        synchronized (this.i0) {
            if (this.j0 == null) {
                fc1[] fc1VarArr = new fc1[1];
                int i = Build.VERSION.SDK_INT;
                if (i >= 23 && this.f0 != null && this.h0) {
                    this.j0 = new a(this.a, new File(pw3.a(this.a), this.f0).getAbsolutePath(), fc1VarArr, this.g0);
                } else {
                    this.j0 = new a(this.a, this.f0, fc1VarArr, this.g0);
                }
                if (i >= 16) {
                    nw3.f(this.j0, this.k0);
                }
            }
            aVar = this.j0;
        }
        return aVar;
    }

    @Override // defpackage.tw3, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        a().close();
    }

    @Override // defpackage.tw3
    public String getDatabaseName() {
        return this.f0;
    }

    @Override // defpackage.tw3
    public void setWriteAheadLoggingEnabled(boolean z) {
        synchronized (this.i0) {
            a aVar = this.j0;
            if (aVar != null) {
                nw3.f(aVar, z);
            }
            this.k0 = z;
        }
    }
}
