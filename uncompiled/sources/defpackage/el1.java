package defpackage;

import android.annotation.SuppressLint;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/* compiled from: HtmlCompat.java */
@SuppressLint({"InlinedApi"})
/* renamed from: el1  reason: default package */
/* loaded from: classes.dex */
public final class el1 {
    public static Spanned a(String str, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(str, i);
        }
        return Html.fromHtml(str);
    }
}
