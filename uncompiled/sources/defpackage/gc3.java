package defpackage;

import java.util.concurrent.Callable;

/* compiled from: ScalarCallable.java */
/* renamed from: gc3  reason: default package */
/* loaded from: classes2.dex */
public interface gc3<T> extends Callable<T> {
    @Override // java.util.concurrent.Callable
    T call();
}
