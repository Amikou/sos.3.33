package defpackage;

import android.graphics.Insets;
import android.graphics.Rect;

/* compiled from: Insets.java */
/* renamed from: cr1  reason: default package */
/* loaded from: classes.dex */
public final class cr1 {
    public static final cr1 e = new cr1(0, 0, 0, 0);
    public final int a;
    public final int b;
    public final int c;
    public final int d;

    public cr1(int i, int i2, int i3, int i4) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public static cr1 a(cr1 cr1Var, cr1 cr1Var2) {
        return b(Math.max(cr1Var.a, cr1Var2.a), Math.max(cr1Var.b, cr1Var2.b), Math.max(cr1Var.c, cr1Var2.c), Math.max(cr1Var.d, cr1Var2.d));
    }

    public static cr1 b(int i, int i2, int i3, int i4) {
        if (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) {
            return e;
        }
        return new cr1(i, i2, i3, i4);
    }

    public static cr1 c(Rect rect) {
        return b(rect.left, rect.top, rect.right, rect.bottom);
    }

    public static cr1 d(Insets insets) {
        return b(insets.left, insets.top, insets.right, insets.bottom);
    }

    public Insets e() {
        return Insets.of(this.a, this.b, this.c, this.d);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || cr1.class != obj.getClass()) {
            return false;
        }
        cr1 cr1Var = (cr1) obj;
        return this.d == cr1Var.d && this.a == cr1Var.a && this.c == cr1Var.c && this.b == cr1Var.b;
    }

    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    public String toString() {
        return "Insets{left=" + this.a + ", top=" + this.b + ", right=" + this.c + ", bottom=" + this.d + '}';
    }
}
