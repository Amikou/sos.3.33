package defpackage;

import com.google.android.gms.internal.measurement.q0;
import com.google.android.gms.internal.measurement.r0;
import com.google.android.gms.internal.measurement.u0;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ag5  reason: default package */
/* loaded from: classes.dex */
public final class ag5 extends w1<q0, ag5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public ag5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.q0 r0 = com.google.android.gms.internal.measurement.q0.F()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ag5.<init>():void");
    }

    public final int A() {
        return ((q0) this.f0).D();
    }

    public final r0 B(int i) {
        return ((q0) this.f0).E(i);
    }

    public final ag5 C(int i, dg5 dg5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        q0.H((q0) this.f0, i, dg5Var.o());
        return this;
    }

    public final int v() {
        return ((q0) this.f0).A();
    }

    public final u0 x(int i) {
        return ((q0) this.f0).B(i);
    }

    public final ag5 y(int i, ah5 ah5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        q0.G((q0) this.f0, i, ah5Var.o());
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ ag5(defpackage.zf5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.q0 r1 = com.google.android.gms.internal.measurement.q0.F()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ag5.<init>(zf5):void");
    }
}
