package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import net.safemoon.androidwallet.views.FixedForAppBarLayoutManager;

/* compiled from: MyTokensAdapter.kt */
/* renamed from: qb2  reason: default package */
/* loaded from: classes2.dex */
public final class qb2 extends RecyclerView.Adapter<a> {
    public et1 a;
    public final List<String> b;
    public tc1<? super UserTokenItemDisplayModel, te4> c;
    public final List<UserTokenItemDisplayModel> d;

    /* compiled from: MyTokensAdapter.kt */
    /* renamed from: qb2$a */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.a0 {
        public final et1 a;
        public final ImageView b;
        public final TextView c;
        public final TextView d;
        public final TextView e;
        public final TextView f;
        public final TextView g;
        public final View h;
        public final ImageView i;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(et1 et1Var) {
            super(et1Var.b());
            fs1.f(et1Var, "binding");
            this.a = et1Var;
            ImageView imageView = et1Var.c;
            fs1.e(imageView, "binding.ivTokenIcon");
            this.b = imageView;
            TextView textView = et1Var.e;
            fs1.e(textView, "binding.tvTokenName");
            this.c = textView;
            TextView textView2 = et1Var.h;
            fs1.e(textView2, "binding.tvTokenSymbol");
            this.d = textView2;
            TextView textView3 = et1Var.d;
            fs1.e(textView3, "binding.tvTokenBalance");
            this.e = textView3;
            TextView textView4 = et1Var.g;
            fs1.e(textView4, "binding.tvTokenPercent");
            this.f = textView4;
            TextView textView5 = et1Var.f;
            fs1.e(textView5, "binding.tvTokenNativeBalance");
            this.g = textView5;
            View view = et1Var.i;
            fs1.e(view, "binding.vDivider");
            this.h = view;
            ImageView imageView2 = et1Var.b;
            fs1.e(imageView2, "binding.ivPercentageDirection");
            this.i = imageView2;
        }

        public final ImageView a() {
            return this.i;
        }

        public final ImageView b() {
            return this.b;
        }

        public final TextView c() {
            return this.e;
        }

        public final TextView d() {
            return this.c;
        }

        public final TextView e() {
            return this.g;
        }

        public final TextView f() {
            return this.f;
        }

        public final TextView g() {
            return this.d;
        }

        public final View h() {
            return this.h;
        }
    }

    public qb2() {
        ArrayList arrayList = new ArrayList();
        for (Pair<TokenType, List<String>> pair : ApplicationRoomDatabase.n.d()) {
            arrayList.addAll(pair.getSecond());
        }
        te4 te4Var = te4.a;
        this.b = arrayList;
        this.d = new ArrayList();
    }

    public static final void f(qb2 qb2Var, UserTokenItemDisplayModel userTokenItemDisplayModel, View view) {
        fs1.f(qb2Var, "this$0");
        fs1.f(userTokenItemDisplayModel, "$model");
        tc1<? super UserTokenItemDisplayModel, te4> tc1Var = qb2Var.c;
        if (tc1Var == null) {
            return;
        }
        tc1Var.invoke(userTokenItemDisplayModel);
    }

    public final List<UserTokenItemDisplayModel> b() {
        return this.d;
    }

    public final boolean c(RecyclerView.a0 a0Var) {
        if (a0Var == null) {
            return false;
        }
        return a0Var.itemView.isSelected();
    }

    public final void d(int i, int i2) {
        Collections.swap(this.d, i, i2);
        notifyItemMoved(i, i2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        final UserTokenItemDisplayModel userTokenItemDisplayModel = this.d.get(i);
        e30.Q(aVar.b(), userTokenItemDisplayModel.getIconResId(), userTokenItemDisplayModel.getIconFile(), userTokenItemDisplayModel.getSymbol());
        if (e30.H(userTokenItemDisplayModel.getSymbol())) {
            e30.P(aVar.b(), userTokenItemDisplayModel.getCmcId(), userTokenItemDisplayModel.getSymbol());
        }
        aVar.d().setText(userTokenItemDisplayModel.getName());
        aVar.g().setText(userTokenItemDisplayModel.getSymbol());
        try {
            double percentChange1h = userTokenItemDisplayModel.getPercentChange1h();
            TextView f = aVar.f();
            lu3 lu3Var = lu3.a;
            String format = String.format("%.2f", Arrays.copyOf(new Object[]{Double.valueOf(percentChange1h)}, 1));
            fs1.e(format, "java.lang.String.format(format, *args)");
            f.setText(fs1.l(format, "%"));
        } catch (Exception unused) {
            aVar.f().setText("");
        }
        e30.R(aVar.e(), userTokenItemDisplayModel.getNativeBalance());
        e30.N(aVar.c(), userTokenItemDisplayModel.getBalanceInUSDT(), true);
        aVar.h().setVisibility(i + 1 != getItemCount() ? 0 : 8);
        aVar.itemView.setOnClickListener(new View.OnClickListener() { // from class: pb2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                qb2.f(qb2.this, userTokenItemDisplayModel, view);
            }
        });
        try {
            com.bumptech.glide.a.u(aVar.a()).w(Integer.valueOf(userTokenItemDisplayModel.getPercentChange1h() >= Utils.DOUBLE_EPSILON ? R.drawable.arrow_up : R.drawable.arrow_down)).I0(aVar.a());
        } catch (Exception e) {
            String localizedMessage = e.getLocalizedMessage();
            if (localizedMessage != null) {
                String simpleName = qb2.class.getSimpleName();
                fs1.e(simpleName, "this::class.java.simpleName");
                e30.c0(localizedMessage, simpleName);
            }
        }
        i(aVar, !this.b.contains(userTokenItemDisplayModel.getSymbolWithType()));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: g */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        et1 a2 = et1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_my_tokens_screen, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        this.a = a2;
        et1 et1Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        fs1.e(a2.b().getContext().getResources(), "binding.root.context.resources");
        et1 et1Var2 = this.a;
        if (et1Var2 == null) {
            fs1.r("binding");
        } else {
            et1Var = et1Var2;
        }
        return new a(et1Var);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.d.size();
    }

    public final void h(int i) {
        this.d.remove(i);
        notifyItemRemoved(i);
        notifyItemRangeChanged(i, getItemCount());
    }

    public final void i(RecyclerView.a0 a0Var, boolean z) {
        if (a0Var == null) {
            return;
        }
        a0Var.itemView.setSelected(z);
    }

    public final void j(FixedForAppBarLayoutManager fixedForAppBarLayoutManager) {
        fs1.f(fixedForAppBarLayoutManager, "layoutManager");
    }

    public final void k(tc1<? super UserTokenItemDisplayModel, te4> tc1Var) {
        this.c = tc1Var;
    }

    public final void l(List<UserTokenItemDisplayModel> list) {
        fs1.f(list, "newItemList");
        this.d.clear();
        this.d.addAll(list);
        notifyDataSetChanged();
    }
}
