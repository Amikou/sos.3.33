package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ToolbarWithTitleAndActionTitleBinding.java */
/* renamed from: i74  reason: default package */
/* loaded from: classes2.dex */
public final class i74 {
    public final MaterialButton a;
    public final ImageView b;
    public final TextView c;

    public i74(Toolbar toolbar, MaterialButton materialButton, ImageView imageView, TextView textView) {
        this.a = materialButton;
        this.b = imageView;
        this.c = textView;
    }

    public static i74 a(View view) {
        int i = R.id.ivToolbarAction;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.ivToolbarAction);
        if (materialButton != null) {
            i = R.id.ivToolbarBack;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivToolbarBack);
            if (imageView != null) {
                i = R.id.tvToolbarTitle;
                TextView textView = (TextView) ai4.a(view, R.id.tvToolbarTitle);
                if (textView != null) {
                    return new i74((Toolbar) view, materialButton, imageView, textView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
