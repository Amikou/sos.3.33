package defpackage;

import com.google.protobuf.q;
import com.google.protobuf.r;

/* compiled from: ExtensionRegistryFactory.java */
/* renamed from: i11  reason: default package */
/* loaded from: classes2.dex */
public final class i11 {
    public static final Class<?> a = c();

    public static r a() {
        r b = b("getEmptyRegistry");
        return b != null ? b : r.e;
    }

    public static final r b(String str) {
        Class<?> cls = a;
        if (cls == null) {
            return null;
        }
        try {
            return (r) cls.getDeclaredMethod(str, new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    public static Class<?> c() {
        try {
            q qVar = q.g;
            return q.class;
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }
}
