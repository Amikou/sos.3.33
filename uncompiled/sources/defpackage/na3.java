package defpackage;

/* renamed from: na3  reason: default package */
/* loaded from: classes2.dex */
public class na3 extends g22 {
    public na3() {
    }

    public na3(na3 na3Var) {
        super(na3Var);
    }

    @Override // defpackage.qo0
    public int a(byte[] bArr, int i) {
        q();
        ro2.k(this.e, bArr, i);
        ro2.k(this.f, bArr, i + 8);
        ro2.k(this.g, bArr, i + 16);
        ro2.k(this.h, bArr, i + 24);
        ro2.k(this.i, bArr, i + 32);
        ro2.k(this.j, bArr, i + 40);
        ro2.k(this.k, bArr, i + 48);
        ro2.k(this.l, bArr, i + 56);
        reset();
        return 64;
    }

    @Override // defpackage.j72
    public j72 copy() {
        return new na3(this);
    }

    @Override // defpackage.j72
    public void d(j72 j72Var) {
        p((na3) j72Var);
    }

    @Override // defpackage.qo0
    public String g() {
        return "SHA-512";
    }

    @Override // defpackage.qo0
    public int h() {
        return 64;
    }

    @Override // defpackage.g22, defpackage.qo0
    public void reset() {
        super.reset();
        this.e = 7640891576956012808L;
        this.f = -4942790177534073029L;
        this.g = 4354685564936845355L;
        this.h = -6534734903238641935L;
        this.i = 5840696475078001361L;
        this.j = -7276294671716946913L;
        this.k = 2270897969802886507L;
        this.l = 6620516959819538809L;
    }
}
