package defpackage;

import android.os.Bundle;
import com.google.android.play.core.assetpacks.a;
import java.util.List;

/* renamed from: kt4  reason: default package */
/* loaded from: classes2.dex */
public final class kt4 extends a<List<String>> {
    public final /* synthetic */ st4 c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public kt4(st4 st4Var, tx4<List<String>> tx4Var) {
        super(st4Var, tx4Var);
        this.c = st4Var;
    }

    @Override // com.google.android.play.core.assetpacks.a, com.google.android.play.core.internal.r
    public final void i1(List<Bundle> list) {
        super.i1(list);
        this.a.e(st4.u(this.c, list));
    }
}
