package defpackage;

import android.graphics.Typeface;
import android.os.Handler;
import defpackage.m81;
import defpackage.o81;

/* compiled from: CallbackWithHandler.java */
/* renamed from: yu  reason: default package */
/* loaded from: classes.dex */
public class yu {
    public final o81.c a;
    public final Handler b;

    /* compiled from: CallbackWithHandler.java */
    /* renamed from: yu$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ o81.c a;
        public final /* synthetic */ Typeface f0;

        public a(yu yuVar, o81.c cVar, Typeface typeface) {
            this.a = cVar;
            this.f0 = typeface;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.b(this.f0);
        }
    }

    /* compiled from: CallbackWithHandler.java */
    /* renamed from: yu$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ o81.c a;
        public final /* synthetic */ int f0;

        public b(yu yuVar, o81.c cVar, int i) {
            this.a = cVar;
            this.f0 = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.a.a(this.f0);
        }
    }

    public yu(o81.c cVar, Handler handler) {
        this.a = cVar;
        this.b = handler;
    }

    public final void a(int i) {
        this.b.post(new b(this, this.a, i));
    }

    public void b(m81.e eVar) {
        if (eVar.a()) {
            c(eVar.a);
        } else {
            a(eVar.b);
        }
    }

    public final void c(Typeface typeface) {
        this.b.post(new a(this, this.a, typeface));
    }
}
