package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ManageContactsFragmentDirections.java */
/* renamed from: n32  reason: default package */
/* loaded from: classes2.dex */
public class n32 {

    /* compiled from: ManageContactsFragmentDirections.java */
    /* renamed from: n32$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("contact")) {
                IContact iContact = (IContact) this.a.get("contact");
                if (!Parcelable.class.isAssignableFrom(IContact.class) && iContact != null) {
                    if (Serializable.class.isAssignableFrom(IContact.class)) {
                        bundle.putSerializable("contact", (Serializable) Serializable.class.cast(iContact));
                    } else {
                        throw new UnsupportedOperationException(IContact.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("contact", (Parcelable) Parcelable.class.cast(iContact));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_manageContactsFragment_to_editContactFragment;
        }

        public IContact c() {
            return (IContact) this.a.get("contact");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("contact") != bVar.a.containsKey("contact")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionManageContactsFragmentToEditContactFragment(actionId=" + b() + "){contact=" + c() + "}";
        }

        public b(IContact iContact) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (iContact != null) {
                hashMap.put("contact", iContact);
                return;
            }
            throw new IllegalArgumentException("Argument \"contact\" is marked as non-null but was passed a null value.");
        }
    }

    public static ce2 a() {
        return new l6(R.id.action_manageContactsFragment_to_addContactFragment);
    }

    public static b b(IContact iContact) {
        return new b(iContact);
    }
}
