package defpackage;

import androidx.annotation.RecentlyNonNull;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: dj2  reason: default package */
/* loaded from: classes.dex */
public class dj2 implements ThreadFactory {
    public final String a;
    public final AtomicInteger f0;
    public final ThreadFactory g0;

    public dj2(@RecentlyNonNull String str) {
        this(str, 0);
    }

    @Override // java.util.concurrent.ThreadFactory
    @RecentlyNonNull
    public Thread newThread(@RecentlyNonNull Runnable runnable) {
        Thread newThread = this.g0.newThread(new a45(runnable, 0));
        String str = this.a;
        int andIncrement = this.f0.getAndIncrement();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13);
        sb.append(str);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        newThread.setName(sb.toString());
        return newThread;
    }

    public dj2(String str, int i) {
        this.f0 = new AtomicInteger();
        this.g0 = Executors.defaultThreadFactory();
        this.a = (String) zt2.k(str, "Name must not be null");
    }
}
