package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentWalletConnectSignatureRequestBinding.java */
/* renamed from: tb1  reason: default package */
/* loaded from: classes2.dex */
public final class tb1 {
    public final MaterialButton a;
    public final rp3 b;
    public final MaterialTextView c;
    public final MaterialTextView d;
    public final MaterialTextView e;

    public tb1(ConstraintLayout constraintLayout, MaterialButton materialButton, rp3 rp3Var, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3) {
        this.a = materialButton;
        this.b = rp3Var;
        this.c = materialTextView;
        this.d = materialTextView2;
        this.e = materialTextView3;
    }

    public static tb1 a(View view) {
        int i = R.id.btnConfirm;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConfirm);
        if (materialButton != null) {
            i = R.id.toolbarWrapper;
            View a = ai4.a(view, R.id.toolbarWrapper);
            if (a != null) {
                rp3 a2 = rp3.a(a);
                i = R.id.txtDAppLink;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtDAppLink);
                if (materialTextView != null) {
                    i = R.id.txtData;
                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtData);
                    if (materialTextView2 != null) {
                        i = R.id.txtWalletName;
                        MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtWalletName);
                        if (materialTextView3 != null) {
                            return new tb1((ConstraintLayout) view, materialButton, a2, materialTextView, materialTextView2, materialTextView3);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
