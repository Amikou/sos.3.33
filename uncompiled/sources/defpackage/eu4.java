package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import com.google.android.play.core.splitcompat.a;
import com.google.android.play.core.splitcompat.b;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: eu4  reason: default package */
/* loaded from: classes2.dex */
public final class eu4 implements lx4 {
    public final Context a;
    public final b b;
    public final com.google.android.play.core.internal.b c;
    public final Executor d;

    public eu4(Context context, Executor executor, com.google.android.play.core.internal.b bVar, b bVar2, ny4 ny4Var) {
        this.a = context;
        this.b = bVar2;
        this.c = bVar;
        this.d = executor;
    }

    public static /* synthetic */ void c(eu4 eu4Var, ax4 ax4Var) {
        try {
            if (a.a(ny4.c(eu4Var.a))) {
                ax4Var.a();
            } else {
                ax4Var.c(-12);
            }
        } catch (Exception unused) {
            ax4Var.c(-12);
        }
    }

    public static /* synthetic */ void d(eu4 eu4Var, List list, ax4 ax4Var) {
        Integer e = eu4Var.e(list);
        if (e == null) {
            return;
        }
        if (e.intValue() == 0) {
            ax4Var.b();
        } else {
            ax4Var.c(e.intValue());
        }
    }

    @Override // defpackage.lx4
    public final void a(List<Intent> list, ax4 ax4Var) {
        if (!a.b()) {
            throw new IllegalStateException("Ingestion should only be called in SplitCompat mode.");
        }
        this.d.execute(new cu4(this, list, ax4Var));
    }

    public final Integer e(List<Intent> list) {
        FileLock fileLock;
        File[] listFiles;
        try {
            FileChannel channel = new RandomAccessFile(this.b.f(), "rw").getChannel();
            Integer num = null;
            try {
                fileLock = channel.tryLock();
            } catch (OverlappingFileLockException unused) {
                fileLock = null;
            }
            if (fileLock != null) {
                int i = 0;
                try {
                    for (Intent intent : list) {
                        String stringExtra = intent.getStringExtra("split_id");
                        AssetFileDescriptor openAssetFileDescriptor = this.a.getContentResolver().openAssetFileDescriptor(intent.getData(), "r");
                        File b = this.b.b(stringExtra);
                        if ((!b.exists() || b.length() == openAssetFileDescriptor.getLength()) && b.exists()) {
                        }
                        if (this.b.c(stringExtra).exists()) {
                            continue;
                        } else {
                            BufferedInputStream bufferedInputStream = new BufferedInputStream(openAssetFileDescriptor.createInputStream());
                            try {
                                FileOutputStream fileOutputStream = new FileOutputStream(b);
                                byte[] bArr = new byte[4096];
                                while (true) {
                                    int read = bufferedInputStream.read(bArr);
                                    if (read <= 0) {
                                        break;
                                    }
                                    fileOutputStream.write(bArr, 0, read);
                                }
                                fileOutputStream.close();
                                bufferedInputStream.close();
                            } catch (Throwable th) {
                                try {
                                    bufferedInputStream.close();
                                } catch (Throwable th2) {
                                    yv4.a(th, th2);
                                }
                                throw th;
                            }
                        }
                    }
                    listFiles = this.b.g().listFiles();
                } catch (IOException | Exception unused2) {
                    i = -13;
                }
                if (this.c.a(listFiles)) {
                    if (this.c.b(listFiles)) {
                        File[] listFiles2 = this.b.g().listFiles();
                        Arrays.sort(listFiles2);
                        int length = listFiles2.length;
                        while (true) {
                            length--;
                            if (length < 0) {
                                break;
                            }
                            File file = listFiles2[length];
                            file.renameTo(this.b.d(file));
                        }
                        num = Integer.valueOf(i);
                        fileLock.release();
                    }
                }
                i = -11;
                num = Integer.valueOf(i);
                fileLock.release();
            }
            if (channel != null) {
                channel.close();
            }
            return num;
        } catch (Exception unused3) {
            return -13;
        }
    }
}
