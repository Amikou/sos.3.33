package defpackage;

import com.onesignal.influence.domain.OSInfluenceChannel;
import com.onesignal.influence.domain.OSInfluenceType;
import com.onesignal.influence.domain.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSNotificationTracker.kt */
/* renamed from: ck2  reason: default package */
/* loaded from: classes2.dex */
public final class ck2 extends mj2 {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ck2(xj2 xj2Var, yj2 yj2Var, zk2 zk2Var) {
        super(xj2Var, yj2Var, zk2Var);
        fs1.f(xj2Var, "dataRepository");
        fs1.f(yj2Var, "logger");
        fs1.f(zk2Var, "timeProvider");
    }

    @Override // defpackage.mj2
    public void a(JSONObject jSONObject, a aVar) {
        fs1.f(jSONObject, "jsonObject");
        fs1.f(aVar, "influence");
        if (aVar.d().isAttributed()) {
            try {
                jSONObject.put("direct", aVar.d().isDirect());
                jSONObject.put("notification_ids", aVar.b());
            } catch (JSONException e) {
                o().error("Generating notification tracker addSessionData JSONObject ", e);
            }
        }
    }

    @Override // defpackage.mj2
    public void b() {
        xj2 f = f();
        OSInfluenceType k = k();
        if (k == null) {
            k = OSInfluenceType.UNATTRIBUTED;
        }
        f.b(k);
        f().c(g());
    }

    @Override // defpackage.mj2
    public int c() {
        return f().l();
    }

    @Override // defpackage.mj2
    public OSInfluenceChannel d() {
        return OSInfluenceChannel.NOTIFICATION;
    }

    @Override // defpackage.mj2
    public String h() {
        return "notification_id";
    }

    @Override // defpackage.mj2
    public int i() {
        return f().k();
    }

    @Override // defpackage.mj2
    public JSONArray l() throws JSONException {
        return f().i();
    }

    @Override // defpackage.mj2
    public JSONArray m(String str) {
        try {
            return l();
        } catch (JSONException e) {
            o().error("Generating Notification tracker getLastChannelObjects JSONObject ", e);
            return new JSONArray();
        }
    }

    @Override // defpackage.mj2
    public void p() {
        OSInfluenceType j = f().j();
        if (j.isIndirect()) {
            x(n());
        } else if (j.isDirect()) {
            w(f().d());
        }
        te4 te4Var = te4.a;
        y(j);
        yj2 o = o();
        o.debug("OneSignal NotificationTracker initInfluencedTypeFromCache: " + this);
    }

    @Override // defpackage.mj2
    public void u(JSONArray jSONArray) {
        fs1.f(jSONArray, "channelObjects");
        f().r(jSONArray);
    }
}
