package defpackage;

/* compiled from: CancellableContinuation.kt */
/* renamed from: r63  reason: default package */
/* loaded from: classes2.dex */
public final class r63 extends zo {
    public final l12 a;

    public r63(l12 l12Var) {
        this.a = l12Var;
    }

    @Override // defpackage.jv
    public void a(Throwable th) {
        this.a.t();
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        a(th);
        return te4.a;
    }

    public String toString() {
        return "RemoveOnCancel[" + this.a + ']';
    }
}
