package defpackage;

import android.content.Context;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: SimpleITokenAdapter.kt */
/* renamed from: gp3  reason: default package */
/* loaded from: classes2.dex */
public final class gp3 extends RecyclerView.a0 {
    public final qs1 a;
    public final Context b;
    public final String c;
    public final ConstraintLayout d;
    public final MaterialCheckBox e;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public gp3(qs1 qs1Var, Context context, String str) {
        super(qs1Var.b());
        fs1.f(qs1Var, "binding");
        fs1.f(context, "context");
        this.a = qs1Var;
        this.b = context;
        this.c = str;
        ConstraintLayout b = qs1Var.b();
        fs1.e(b, "binding.root");
        this.d = b;
        MaterialCheckBox materialCheckBox = qs1Var.c;
        fs1.e(materialCheckBox, "binding.cbAddToken");
        this.e = materialCheckBox;
        MaterialCardView materialCardView = qs1Var.e;
        fs1.e(materialCardView, "rowBG");
        materialCardView.setVisibility(8);
        MaterialCardView materialCardView2 = qs1Var.f;
        fs1.e(materialCardView2, "rowFG");
        materialCardView2.setVisibility(0);
    }

    public final void a(q9 q9Var) {
        fs1.f(q9Var, "model");
        qs1 qs1Var = this.a;
        qs1Var.g.setText(c().getResources().getString(R.string.add_new_tokens_item_name, q9Var.f(), q9Var.g()));
        ImageView imageView = qs1Var.d;
        fs1.e(imageView, "ivTokenIcon");
        e30.Q(imageView, q9Var.e(), q9Var.d(), q9Var.g());
        if (e30.H(q9Var.g())) {
            String c = kt.c(q9Var.a(), q9Var.g(), null, 2, null);
            ImageView imageView2 = qs1Var.d;
            fs1.e(imageView2, "ivTokenIcon");
            e30.P(imageView2, c, q9Var.g());
        }
        qs1Var.c.setChecked(fs1.b(this.c, q9Var.h()));
    }

    public final MaterialCheckBox b() {
        return this.e;
    }

    public final Context c() {
        return this.b;
    }

    public final ConstraintLayout d() {
        return this.d;
    }
}
