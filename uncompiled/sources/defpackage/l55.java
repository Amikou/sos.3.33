package defpackage;

import android.content.SharedPreferences;

/* renamed from: l55  reason: default package */
/* loaded from: classes.dex */
public final class l55 extends r45<String> {
    public l55(w55 w55Var, String str, String str2) {
        super(w55Var, str, str2, null);
    }

    @Override // defpackage.r45
    public final /* synthetic */ String j(String str) {
        return str;
    }

    @Override // defpackage.r45
    /* renamed from: r */
    public final String c(SharedPreferences sharedPreferences) {
        try {
            return sharedPreferences.getString(this.b, null);
        } catch (ClassCastException unused) {
            String valueOf = String.valueOf(this.b);
            if (valueOf.length() != 0) {
                "Invalid string value in SharedPreferences for ".concat(valueOf);
            }
            return null;
        }
    }
}
