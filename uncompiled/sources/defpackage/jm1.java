package defpackage;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.util.c;

/* compiled from: IOContext.java */
/* renamed from: jm1  reason: default package */
/* loaded from: classes.dex */
public class jm1 {
    public final Object a;
    public JsonEncoding b;
    public final boolean c;
    public final wr d;
    public byte[] e;
    public byte[] f;
    public byte[] g;
    public char[] h;
    public char[] i;
    public char[] j;

    public jm1(wr wrVar, Object obj, boolean z) {
        this.d = wrVar;
        this.a = obj;
        this.c = z;
    }

    public final void a(Object obj) {
        if (obj != null) {
            throw new IllegalStateException("Trying to call same allocXxx() method second time");
        }
    }

    public final void b(byte[] bArr, byte[] bArr2) {
        if (bArr != bArr2 && bArr.length < bArr2.length) {
            throw v();
        }
    }

    public final void c(char[] cArr, char[] cArr2) {
        if (cArr != cArr2 && cArr.length < cArr2.length) {
            throw v();
        }
    }

    public byte[] d() {
        a(this.g);
        byte[] a = this.d.a(3);
        this.g = a;
        return a;
    }

    public char[] e() {
        a(this.i);
        char[] c = this.d.c(1);
        this.i = c;
        return c;
    }

    public char[] f(int i) {
        a(this.j);
        char[] d = this.d.d(3, i);
        this.j = d;
        return d;
    }

    public byte[] g() {
        a(this.e);
        byte[] a = this.d.a(0);
        this.e = a;
        return a;
    }

    public char[] h() {
        a(this.h);
        char[] c = this.d.c(0);
        this.h = c;
        return c;
    }

    public char[] i(int i) {
        a(this.h);
        char[] d = this.d.d(0, i);
        this.h = d;
        return d;
    }

    public byte[] j() {
        a(this.f);
        byte[] a = this.d.a(1);
        this.f = a;
        return a;
    }

    public c k() {
        return new c(this.d);
    }

    public JsonEncoding l() {
        return this.b;
    }

    public Object m() {
        return this.a;
    }

    public boolean n() {
        return this.c;
    }

    public void o(byte[] bArr) {
        if (bArr != null) {
            b(bArr, this.g);
            this.g = null;
            this.d.i(3, bArr);
        }
    }

    public void p(char[] cArr) {
        if (cArr != null) {
            c(cArr, this.i);
            this.i = null;
            this.d.j(1, cArr);
        }
    }

    public void q(char[] cArr) {
        if (cArr != null) {
            c(cArr, this.j);
            this.j = null;
            this.d.j(3, cArr);
        }
    }

    public void r(byte[] bArr) {
        if (bArr != null) {
            b(bArr, this.e);
            this.e = null;
            this.d.i(0, bArr);
        }
    }

    public void s(char[] cArr) {
        if (cArr != null) {
            c(cArr, this.h);
            this.h = null;
            this.d.j(0, cArr);
        }
    }

    public void t(byte[] bArr) {
        if (bArr != null) {
            b(bArr, this.f);
            this.f = null;
            this.d.i(1, bArr);
        }
    }

    public void u(JsonEncoding jsonEncoding) {
        this.b = jsonEncoding;
    }

    public final IllegalArgumentException v() {
        return new IllegalArgumentException("Trying to release buffer smaller than original");
    }
}
