package defpackage;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import com.facebook.drawee.generic.RoundingParams;
import com.github.mikephil.charting.utils.Utils;
import defpackage.qc3;
import java.util.Arrays;
import java.util.List;

/* compiled from: GenericDraweeHierarchyBuilder.java */
/* renamed from: ze1  reason: default package */
/* loaded from: classes.dex */
public class ze1 {
    public static final qc3.b s = qc3.b.f;
    public static final qc3.b t = qc3.b.g;
    public Resources a;
    public int b;
    public float c;
    public Drawable d;
    public qc3.b e;
    public Drawable f;
    public qc3.b g;
    public Drawable h;
    public qc3.b i;
    public Drawable j;
    public qc3.b k;
    public qc3.b l;
    public PointF m;
    public ColorFilter n;
    public Drawable o;
    public List<Drawable> p;
    public Drawable q;
    public RoundingParams r;

    public ze1(Resources resources) {
        this.a = resources;
        t();
    }

    public ze1 A(Drawable drawable) {
        if (drawable == null) {
            this.p = null;
        } else {
            this.p = Arrays.asList(drawable);
        }
        return this;
    }

    public ze1 B(Drawable drawable) {
        this.d = drawable;
        return this;
    }

    public ze1 C(qc3.b bVar) {
        this.e = bVar;
        return this;
    }

    public ze1 D(Drawable drawable) {
        if (drawable == null) {
            this.q = null;
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{16842919}, drawable);
            this.q = stateListDrawable;
        }
        return this;
    }

    public ze1 E(Drawable drawable) {
        this.j = drawable;
        return this;
    }

    public ze1 F(qc3.b bVar) {
        this.k = bVar;
        return this;
    }

    public ze1 G(Drawable drawable) {
        this.f = drawable;
        return this;
    }

    public ze1 H(qc3.b bVar) {
        this.g = bVar;
        return this;
    }

    public ze1 I(RoundingParams roundingParams) {
        this.r = roundingParams;
        return this;
    }

    public final void J() {
        List<Drawable> list = this.p;
        if (list != null) {
            for (Drawable drawable : list) {
                xt2.g(drawable);
            }
        }
    }

    public ye1 a() {
        J();
        return new ye1(this);
    }

    public ColorFilter b() {
        return this.n;
    }

    public PointF c() {
        return this.m;
    }

    public qc3.b d() {
        return this.l;
    }

    public Drawable e() {
        return this.o;
    }

    public float f() {
        return this.c;
    }

    public int g() {
        return this.b;
    }

    public Drawable h() {
        return this.h;
    }

    public qc3.b i() {
        return this.i;
    }

    public List<Drawable> j() {
        return this.p;
    }

    public Drawable k() {
        return this.d;
    }

    public qc3.b l() {
        return this.e;
    }

    public Drawable m() {
        return this.q;
    }

    public Drawable n() {
        return this.j;
    }

    public qc3.b o() {
        return this.k;
    }

    public Resources p() {
        return this.a;
    }

    public Drawable q() {
        return this.f;
    }

    public qc3.b r() {
        return this.g;
    }

    public RoundingParams s() {
        return this.r;
    }

    public final void t() {
        this.b = 300;
        this.c = Utils.FLOAT_EPSILON;
        this.d = null;
        qc3.b bVar = s;
        this.e = bVar;
        this.f = null;
        this.g = bVar;
        this.h = null;
        this.i = bVar;
        this.j = null;
        this.k = bVar;
        this.l = t;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
    }

    public ze1 u(qc3.b bVar) {
        this.l = bVar;
        return this;
    }

    public ze1 v(Drawable drawable) {
        this.o = drawable;
        return this;
    }

    public ze1 w(float f) {
        this.c = f;
        return this;
    }

    public ze1 x(int i) {
        this.b = i;
        return this;
    }

    public ze1 y(Drawable drawable) {
        this.h = drawable;
        return this;
    }

    public ze1 z(qc3.b bVar) {
        this.i = bVar;
        return this;
    }
}
