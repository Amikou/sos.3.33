package defpackage;

import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.r;

/* compiled from: NumericNode.java */
/* renamed from: fj2  reason: default package */
/* loaded from: classes.dex */
public abstract class fj2 extends r {
    @Override // com.fasterxml.jackson.databind.d
    public final JsonNodeType y() {
        return JsonNodeType.NUMBER;
    }
}
