package defpackage;

import android.util.SparseArray;
import androidx.media3.common.j;
import com.google.common.collect.ImmutableList;
import defpackage.gc4;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DefaultTsPayloadReaderFactory.java */
/* renamed from: ml0  reason: default package */
/* loaded from: classes.dex */
public final class ml0 implements gc4.c {
    public final int a;
    public final List<j> b;

    public ml0(int i) {
        this(i, ImmutableList.of());
    }

    @Override // defpackage.gc4.c
    public SparseArray<gc4> a() {
        return new SparseArray<>();
    }

    @Override // defpackage.gc4.c
    public gc4 b(int i, gc4.b bVar) {
        if (i != 2) {
            if (i == 3 || i == 4) {
                return new pq2(new ka2(bVar.b));
            }
            if (i != 21) {
                if (i == 27) {
                    if (f(4)) {
                        return null;
                    }
                    return new pq2(new ej1(c(bVar), f(1), f(8)));
                } else if (i != 36) {
                    if (i != 89) {
                        if (i != 138) {
                            if (i != 172) {
                                if (i != 257) {
                                    if (i != 134) {
                                        if (i != 135) {
                                            switch (i) {
                                                case 15:
                                                    if (f(2)) {
                                                        return null;
                                                    }
                                                    return new pq2(new aa(false, bVar.b));
                                                case 16:
                                                    return new pq2(new dj1(d(bVar)));
                                                case 17:
                                                    if (f(2)) {
                                                        return null;
                                                    }
                                                    return new pq2(new my1(bVar.b));
                                                default:
                                                    switch (i) {
                                                        case 128:
                                                            break;
                                                        case 129:
                                                            break;
                                                        case 130:
                                                            if (!f(64)) {
                                                                return null;
                                                            }
                                                            break;
                                                        default:
                                                            return null;
                                                    }
                                            }
                                        }
                                        return new pq2(new s5(bVar.b));
                                    } else if (f(16)) {
                                        return null;
                                    } else {
                                        return new xh3(new up2("application/x-scte35"));
                                    }
                                }
                                return new xh3(new up2("application/vnd.dvb.ait"));
                            }
                            return new pq2(new w5(bVar.b));
                        }
                        return new pq2(new gs0(bVar.b));
                    }
                    return new pq2(new qs0(bVar.c));
                } else {
                    return new pq2(new fj1(c(bVar)));
                }
            }
            return new pq2(new jn1());
        }
        return new pq2(new cj1(d(bVar)));
    }

    public final fj3 c(gc4.b bVar) {
        return new fj3(e(bVar));
    }

    public final xf4 d(gc4.b bVar) {
        return new xf4(e(bVar));
    }

    public final List<j> e(gc4.b bVar) {
        String str;
        int i;
        if (f(32)) {
            return this.b;
        }
        op2 op2Var = new op2(bVar.d);
        List<j> list = this.b;
        while (op2Var.a() > 0) {
            int D = op2Var.D();
            int e = op2Var.e() + op2Var.D();
            if (D == 134) {
                list = new ArrayList<>();
                int D2 = op2Var.D() & 31;
                for (int i2 = 0; i2 < D2; i2++) {
                    String A = op2Var.A(3);
                    int D3 = op2Var.D();
                    boolean z = (D3 & 128) != 0;
                    if (z) {
                        i = D3 & 63;
                        str = "application/cea-708";
                    } else {
                        str = "application/cea-608";
                        i = 1;
                    }
                    byte D4 = (byte) op2Var.D();
                    op2Var.Q(1);
                    List<byte[]> list2 = null;
                    if (z) {
                        list2 = h00.b((D4 & 64) != 0);
                    }
                    list.add(new j.b().e0(str).V(A).F(i).T(list2).E());
                }
            }
            op2Var.P(e);
        }
        return list;
    }

    public final boolean f(int i) {
        return (i & this.a) != 0;
    }

    public ml0(int i, List<j> list) {
        this.a = i;
        this.b = list;
    }
}
