package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;

/* compiled from: NoOpCache.java */
/* renamed from: kg2  reason: default package */
/* loaded from: classes.dex */
public class kg2 implements xp {
    @Override // defpackage.xp
    public a<Bitmap> a(int i, int i2, int i3) {
        return null;
    }

    @Override // defpackage.xp
    public void b(int i, a<Bitmap> aVar, int i2) {
    }

    @Override // defpackage.xp
    public boolean c(int i) {
        return false;
    }

    @Override // defpackage.xp
    public void clear() {
    }

    @Override // defpackage.xp
    public a<Bitmap> d(int i) {
        return null;
    }

    @Override // defpackage.xp
    public void e(int i, a<Bitmap> aVar, int i2) {
    }

    @Override // defpackage.xp
    public a<Bitmap> f(int i) {
        return null;
    }
}
