package defpackage;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;
import kotlinx.coroutines.flow.FlowKt__ChannelsKt;
import kotlinx.coroutines.flow.internal.ChannelFlow;

/* compiled from: Channels.kt */
/* renamed from: lx  reason: default package */
/* loaded from: classes2.dex */
public final class lx<T> extends ChannelFlow<T> {
    public static final /* synthetic */ AtomicIntegerFieldUpdater j0 = AtomicIntegerFieldUpdater.newUpdater(lx.class, "consumed");
    private volatile /* synthetic */ int consumed;
    public final f43<T> h0;
    public final boolean i0;

    public /* synthetic */ lx(f43 f43Var, boolean z, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow, int i2, qi0 qi0Var) {
        this(f43Var, z, (i2 & 4) != 0 ? EmptyCoroutineContext.INSTANCE : coroutineContext, (i2 & 8) != 0 ? -3 : i, (i2 & 16) != 0 ? BufferOverflow.SUSPEND : bufferOverflow);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow, defpackage.j71
    public Object a(k71<? super T> k71Var, q70<? super te4> q70Var) {
        Object d;
        if (this.f0 == -3) {
            l();
            d = FlowKt__ChannelsKt.d(k71Var, this.h0, this.i0, q70Var);
            return d == gs1.d() ? d : te4.a;
        }
        Object a = super.a(k71Var, q70Var);
        return a == gs1.d() ? a : te4.a;
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public String e() {
        return fs1.l("channel=", this.h0);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public Object g(kv2<? super T> kv2Var, q70<? super te4> q70Var) {
        Object d;
        d = FlowKt__ChannelsKt.d(new qk3(kv2Var), this.h0, this.i0, q70Var);
        return d == gs1.d() ? d : te4.a;
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public ChannelFlow<T> h(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        return new lx(this.h0, this.i0, coroutineContext, i, bufferOverflow);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public f43<T> k(c90 c90Var) {
        l();
        if (this.f0 == -3) {
            return this.h0;
        }
        return super.k(c90Var);
    }

    public final void l() {
        if (this.i0) {
            if (!(j0.getAndSet(this, 1) == 0)) {
                throw new IllegalStateException("ReceiveChannel.consumeAsFlow can be collected just once".toString());
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public lx(f43<? extends T> f43Var, boolean z, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        super(coroutineContext, i, bufferOverflow);
        this.h0 = f43Var;
        this.i0 = z;
        this.consumed = 0;
    }
}
