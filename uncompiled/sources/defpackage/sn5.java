package defpackage;

import android.content.Context;
import android.database.ContentObserver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: sn5  reason: default package */
/* loaded from: classes.dex */
public final class sn5 implements ln5 {
    public static sn5 c;
    public final Context a;
    public final ContentObserver b;

    public sn5() {
        this.a = null;
        this.b = null;
    }

    public sn5(Context context) {
        this.a = context;
        qn5 qn5Var = new qn5(this, null);
        this.b = qn5Var;
        context.getContentResolver().registerContentObserver(fm5.a, true, qn5Var);
    }

    public static sn5 b(Context context) {
        sn5 sn5Var;
        sn5 sn5Var2;
        synchronized (sn5.class) {
            if (c == null) {
                if (kq2.c(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0) {
                    sn5Var2 = new sn5(context);
                } else {
                    sn5Var2 = new sn5();
                }
                c = sn5Var2;
            }
            sn5Var = c;
        }
        return sn5Var;
    }

    public static synchronized void d() {
        Context context;
        synchronized (sn5.class) {
            sn5 sn5Var = c;
            if (sn5Var != null && (context = sn5Var.a) != null && sn5Var.b != null) {
                context.getContentResolver().unregisterContentObserver(c.b);
            }
            c = null;
        }
    }

    @Override // defpackage.ln5
    /* renamed from: c */
    public final String a(final String str) {
        if (this.a == null) {
            return null;
        }
        try {
            return (String) hn5.a(new kn5(this, str) { // from class: nn5
                public final sn5 a;
                public final String b;

                {
                    this.a = this;
                    this.b = str;
                }

                @Override // defpackage.kn5
                public final Object zza() {
                    return this.a.e(this.b);
                }
            });
        } catch (IllegalStateException | SecurityException unused) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                "Unable to read GServices for: ".concat(valueOf);
            }
            return null;
        }
    }

    public final /* synthetic */ String e(String str) {
        return fm5.a(this.a.getContentResolver(), str, null);
    }
}
