package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ShapeAppearanceModel.java */
/* renamed from: pn3  reason: default package */
/* loaded from: classes2.dex */
public class pn3 {
    public static final v80 m = new j63(0.5f);
    public w80 a;
    public w80 b;
    public w80 c;
    public w80 d;
    public v80 e;
    public v80 f;
    public v80 g;
    public v80 h;
    public cu0 i;
    public cu0 j;
    public cu0 k;
    public cu0 l;

    /* compiled from: ShapeAppearanceModel.java */
    /* renamed from: pn3$c */
    /* loaded from: classes2.dex */
    public interface c {
        v80 a(v80 v80Var);
    }

    public static b a() {
        return new b();
    }

    public static b b(Context context, int i, int i2) {
        return c(context, i, i2, 0);
    }

    public static b c(Context context, int i, int i2, int i3) {
        return d(context, i, i2, new o4(i3));
    }

    public static b d(Context context, int i, int i2, v80 v80Var) {
        if (i2 != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i);
            i = i2;
            context = contextThemeWrapper;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, o23.ShapeAppearance);
        try {
            int i3 = obtainStyledAttributes.getInt(o23.ShapeAppearance_cornerFamily, 0);
            int i4 = obtainStyledAttributes.getInt(o23.ShapeAppearance_cornerFamilyTopLeft, i3);
            int i5 = obtainStyledAttributes.getInt(o23.ShapeAppearance_cornerFamilyTopRight, i3);
            int i6 = obtainStyledAttributes.getInt(o23.ShapeAppearance_cornerFamilyBottomRight, i3);
            int i7 = obtainStyledAttributes.getInt(o23.ShapeAppearance_cornerFamilyBottomLeft, i3);
            v80 m2 = m(obtainStyledAttributes, o23.ShapeAppearance_cornerSize, v80Var);
            v80 m3 = m(obtainStyledAttributes, o23.ShapeAppearance_cornerSizeTopLeft, m2);
            v80 m4 = m(obtainStyledAttributes, o23.ShapeAppearance_cornerSizeTopRight, m2);
            v80 m5 = m(obtainStyledAttributes, o23.ShapeAppearance_cornerSizeBottomRight, m2);
            return new b().C(i4, m3).G(i5, m4).x(i6, m5).t(i7, m(obtainStyledAttributes, o23.ShapeAppearance_cornerSizeBottomLeft, m2));
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public static b e(Context context, AttributeSet attributeSet, int i, int i2) {
        return f(context, attributeSet, i, i2, 0);
    }

    public static b f(Context context, AttributeSet attributeSet, int i, int i2, int i3) {
        return g(context, attributeSet, i, i2, new o4(i3));
    }

    public static b g(Context context, AttributeSet attributeSet, int i, int i2, v80 v80Var) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, o23.MaterialShape, i, i2);
        int resourceId = obtainStyledAttributes.getResourceId(o23.MaterialShape_shapeAppearance, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(o23.MaterialShape_shapeAppearanceOverlay, 0);
        obtainStyledAttributes.recycle();
        return d(context, resourceId, resourceId2, v80Var);
    }

    public static v80 m(TypedArray typedArray, int i, v80 v80Var) {
        TypedValue peekValue = typedArray.peekValue(i);
        if (peekValue == null) {
            return v80Var;
        }
        int i2 = peekValue.type;
        if (i2 == 5) {
            return new o4(TypedValue.complexToDimensionPixelSize(peekValue.data, typedArray.getResources().getDisplayMetrics()));
        }
        return i2 == 6 ? new j63(peekValue.getFraction(1.0f, 1.0f)) : v80Var;
    }

    public cu0 h() {
        return this.k;
    }

    public w80 i() {
        return this.d;
    }

    public v80 j() {
        return this.h;
    }

    public w80 k() {
        return this.c;
    }

    public v80 l() {
        return this.g;
    }

    public cu0 n() {
        return this.l;
    }

    public cu0 o() {
        return this.j;
    }

    public cu0 p() {
        return this.i;
    }

    public w80 q() {
        return this.a;
    }

    public v80 r() {
        return this.e;
    }

    public w80 s() {
        return this.b;
    }

    public v80 t() {
        return this.f;
    }

    public boolean u(RectF rectF) {
        boolean z = this.l.getClass().equals(cu0.class) && this.j.getClass().equals(cu0.class) && this.i.getClass().equals(cu0.class) && this.k.getClass().equals(cu0.class);
        float a2 = this.e.a(rectF);
        return z && ((this.f.a(rectF) > a2 ? 1 : (this.f.a(rectF) == a2 ? 0 : -1)) == 0 && (this.h.a(rectF) > a2 ? 1 : (this.h.a(rectF) == a2 ? 0 : -1)) == 0 && (this.g.a(rectF) > a2 ? 1 : (this.g.a(rectF) == a2 ? 0 : -1)) == 0) && ((this.b instanceof v93) && (this.a instanceof v93) && (this.c instanceof v93) && (this.d instanceof v93));
    }

    public b v() {
        return new b(this);
    }

    public pn3 w(float f) {
        return v().o(f).m();
    }

    public pn3 x(v80 v80Var) {
        return v().p(v80Var).m();
    }

    public pn3 y(c cVar) {
        return v().F(cVar.a(r())).J(cVar.a(t())).w(cVar.a(j())).A(cVar.a(l())).m();
    }

    public pn3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
    }

    /* compiled from: ShapeAppearanceModel.java */
    /* renamed from: pn3$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public w80 a;
        public w80 b;
        public w80 c;
        public w80 d;
        public v80 e;
        public v80 f;
        public v80 g;
        public v80 h;
        public cu0 i;
        public cu0 j;
        public cu0 k;
        public cu0 l;

        public b() {
            this.a = p42.b();
            this.b = p42.b();
            this.c = p42.b();
            this.d = p42.b();
            this.e = new o4(Utils.FLOAT_EPSILON);
            this.f = new o4(Utils.FLOAT_EPSILON);
            this.g = new o4(Utils.FLOAT_EPSILON);
            this.h = new o4(Utils.FLOAT_EPSILON);
            this.i = p42.c();
            this.j = p42.c();
            this.k = p42.c();
            this.l = p42.c();
        }

        public static float n(w80 w80Var) {
            if (w80Var instanceof v93) {
                return ((v93) w80Var).a;
            }
            if (w80Var instanceof ad0) {
                return ((ad0) w80Var).a;
            }
            return -1.0f;
        }

        public b A(v80 v80Var) {
            this.g = v80Var;
            return this;
        }

        public b B(cu0 cu0Var) {
            this.i = cu0Var;
            return this;
        }

        public b C(int i, v80 v80Var) {
            return D(p42.a(i)).F(v80Var);
        }

        public b D(w80 w80Var) {
            this.a = w80Var;
            float n = n(w80Var);
            if (n != -1.0f) {
                E(n);
            }
            return this;
        }

        public b E(float f) {
            this.e = new o4(f);
            return this;
        }

        public b F(v80 v80Var) {
            this.e = v80Var;
            return this;
        }

        public b G(int i, v80 v80Var) {
            return H(p42.a(i)).J(v80Var);
        }

        public b H(w80 w80Var) {
            this.b = w80Var;
            float n = n(w80Var);
            if (n != -1.0f) {
                I(n);
            }
            return this;
        }

        public b I(float f) {
            this.f = new o4(f);
            return this;
        }

        public b J(v80 v80Var) {
            this.f = v80Var;
            return this;
        }

        public pn3 m() {
            return new pn3(this);
        }

        public b o(float f) {
            return E(f).I(f).z(f).v(f);
        }

        public b p(v80 v80Var) {
            return F(v80Var).J(v80Var).A(v80Var).w(v80Var);
        }

        public b q(int i, float f) {
            return r(p42.a(i)).o(f);
        }

        public b r(w80 w80Var) {
            return D(w80Var).H(w80Var).y(w80Var).u(w80Var);
        }

        public b s(cu0 cu0Var) {
            this.k = cu0Var;
            return this;
        }

        public b t(int i, v80 v80Var) {
            return u(p42.a(i)).w(v80Var);
        }

        public b u(w80 w80Var) {
            this.d = w80Var;
            float n = n(w80Var);
            if (n != -1.0f) {
                v(n);
            }
            return this;
        }

        public b v(float f) {
            this.h = new o4(f);
            return this;
        }

        public b w(v80 v80Var) {
            this.h = v80Var;
            return this;
        }

        public b x(int i, v80 v80Var) {
            return y(p42.a(i)).A(v80Var);
        }

        public b y(w80 w80Var) {
            this.c = w80Var;
            float n = n(w80Var);
            if (n != -1.0f) {
                z(n);
            }
            return this;
        }

        public b z(float f) {
            this.g = new o4(f);
            return this;
        }

        public b(pn3 pn3Var) {
            this.a = p42.b();
            this.b = p42.b();
            this.c = p42.b();
            this.d = p42.b();
            this.e = new o4(Utils.FLOAT_EPSILON);
            this.f = new o4(Utils.FLOAT_EPSILON);
            this.g = new o4(Utils.FLOAT_EPSILON);
            this.h = new o4(Utils.FLOAT_EPSILON);
            this.i = p42.c();
            this.j = p42.c();
            this.k = p42.c();
            this.l = p42.c();
            this.a = pn3Var.a;
            this.b = pn3Var.b;
            this.c = pn3Var.c;
            this.d = pn3Var.d;
            this.e = pn3Var.e;
            this.f = pn3Var.f;
            this.g = pn3Var.g;
            this.h = pn3Var.h;
            this.i = pn3Var.i;
            this.j = pn3Var.j;
            this.k = pn3Var.k;
            this.l = pn3Var.l;
        }
    }

    public pn3() {
        this.a = p42.b();
        this.b = p42.b();
        this.c = p42.b();
        this.d = p42.b();
        this.e = new o4(Utils.FLOAT_EPSILON);
        this.f = new o4(Utils.FLOAT_EPSILON);
        this.g = new o4(Utils.FLOAT_EPSILON);
        this.h = new o4(Utils.FLOAT_EPSILON);
        this.i = p42.c();
        this.j = p42.c();
        this.k = p42.c();
        this.l = p42.c();
    }
}
