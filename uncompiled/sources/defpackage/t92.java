package defpackage;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintAttribute;
import androidx.constraintlayout.widget.a;
import com.github.mikephil.charting.utils.Utils;
import defpackage.ak4;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

/* compiled from: MotionConstrainedPoint.java */
/* renamed from: t92  reason: default package */
/* loaded from: classes.dex */
public class t92 implements Comparable<t92> {
    public int g0;
    public float s0;
    public float a = 1.0f;
    public int f0 = 0;
    public float h0 = Utils.FLOAT_EPSILON;
    public float i0 = Utils.FLOAT_EPSILON;
    public float j0 = Utils.FLOAT_EPSILON;
    public float k0 = Utils.FLOAT_EPSILON;
    public float l0 = 1.0f;
    public float m0 = 1.0f;
    public float n0 = Float.NaN;
    public float o0 = Float.NaN;
    public float p0 = Utils.FLOAT_EPSILON;
    public float q0 = Utils.FLOAT_EPSILON;
    public float r0 = Utils.FLOAT_EPSILON;
    public float t0 = Float.NaN;
    public float u0 = Float.NaN;
    public LinkedHashMap<String, ConstraintAttribute> v0 = new LinkedHashMap<>();

    public void a(HashMap<String, ak4> hashMap, int i) {
        for (String str : hashMap.keySet()) {
            ak4 ak4Var = hashMap.get(str);
            str.hashCode();
            char c = 65535;
            switch (str.hashCode()) {
                case -1249320806:
                    if (str.equals("rotationX")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1249320805:
                    if (str.equals("rotationY")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1225497657:
                    if (str.equals("translationX")) {
                        c = 2;
                        break;
                    }
                    break;
                case -1225497656:
                    if (str.equals("translationY")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1225497655:
                    if (str.equals("translationZ")) {
                        c = 4;
                        break;
                    }
                    break;
                case -1001078227:
                    if (str.equals("progress")) {
                        c = 5;
                        break;
                    }
                    break;
                case -908189618:
                    if (str.equals("scaleX")) {
                        c = 6;
                        break;
                    }
                    break;
                case -908189617:
                    if (str.equals("scaleY")) {
                        c = 7;
                        break;
                    }
                    break;
                case -760884510:
                    if (str.equals("transformPivotX")) {
                        c = '\b';
                        break;
                    }
                    break;
                case -760884509:
                    if (str.equals("transformPivotY")) {
                        c = '\t';
                        break;
                    }
                    break;
                case -40300674:
                    if (str.equals("rotation")) {
                        c = '\n';
                        break;
                    }
                    break;
                case -4379043:
                    if (str.equals("elevation")) {
                        c = 11;
                        break;
                    }
                    break;
                case 37232917:
                    if (str.equals("transitionPathRotate")) {
                        c = '\f';
                        break;
                    }
                    break;
                case 92909918:
                    if (str.equals("alpha")) {
                        c = '\r';
                        break;
                    }
                    break;
            }
            float f = Utils.FLOAT_EPSILON;
            switch (c) {
                case 0:
                    if (!Float.isNaN(this.j0)) {
                        f = this.j0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 1:
                    if (!Float.isNaN(this.k0)) {
                        f = this.k0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 2:
                    if (!Float.isNaN(this.p0)) {
                        f = this.p0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 3:
                    if (!Float.isNaN(this.q0)) {
                        f = this.q0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 4:
                    if (!Float.isNaN(this.r0)) {
                        f = this.r0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 5:
                    if (!Float.isNaN(this.u0)) {
                        f = this.u0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 6:
                    ak4Var.c(i, Float.isNaN(this.l0) ? 1.0f : this.l0);
                    break;
                case 7:
                    ak4Var.c(i, Float.isNaN(this.m0) ? 1.0f : this.m0);
                    break;
                case '\b':
                    if (!Float.isNaN(this.n0)) {
                        f = this.n0;
                    }
                    ak4Var.c(i, f);
                    break;
                case '\t':
                    if (!Float.isNaN(this.o0)) {
                        f = this.o0;
                    }
                    ak4Var.c(i, f);
                    break;
                case '\n':
                    if (!Float.isNaN(this.i0)) {
                        f = this.i0;
                    }
                    ak4Var.c(i, f);
                    break;
                case 11:
                    if (!Float.isNaN(this.h0)) {
                        f = this.h0;
                    }
                    ak4Var.c(i, f);
                    break;
                case '\f':
                    if (!Float.isNaN(this.t0)) {
                        f = this.t0;
                    }
                    ak4Var.c(i, f);
                    break;
                case '\r':
                    ak4Var.c(i, Float.isNaN(this.a) ? 1.0f : this.a);
                    break;
                default:
                    if (str.startsWith("CUSTOM")) {
                        String str2 = str.split(",")[1];
                        if (this.v0.containsKey(str2)) {
                            ConstraintAttribute constraintAttribute = this.v0.get(str2);
                            if (ak4Var instanceof ak4.b) {
                                ((ak4.b) ak4Var).i(i, constraintAttribute);
                                break;
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append(str);
                                sb.append(" ViewSpline not a CustomSet frame = ");
                                sb.append(i);
                                sb.append(", value");
                                sb.append(constraintAttribute.e());
                                sb.append(ak4Var);
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("UNKNOWN spline ");
                        sb2.append(str);
                        break;
                    }
            }
        }
    }

    public void d(View view) {
        this.g0 = view.getVisibility();
        this.a = view.getVisibility() != 0 ? Utils.FLOAT_EPSILON : view.getAlpha();
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            this.h0 = view.getElevation();
        }
        this.i0 = view.getRotation();
        this.j0 = view.getRotationX();
        this.k0 = view.getRotationY();
        this.l0 = view.getScaleX();
        this.m0 = view.getScaleY();
        this.n0 = view.getPivotX();
        this.o0 = view.getPivotY();
        this.p0 = view.getTranslationX();
        this.q0 = view.getTranslationY();
        if (i >= 21) {
            this.r0 = view.getTranslationZ();
        }
    }

    public void e(a.C0021a c0021a) {
        a.d dVar = c0021a.c;
        int i = dVar.c;
        this.f0 = i;
        int i2 = dVar.b;
        this.g0 = i2;
        this.a = (i2 == 0 || i != 0) ? dVar.d : Utils.FLOAT_EPSILON;
        a.e eVar = c0021a.f;
        boolean z = eVar.m;
        this.h0 = eVar.n;
        this.i0 = eVar.b;
        this.j0 = eVar.c;
        this.k0 = eVar.d;
        this.l0 = eVar.e;
        this.m0 = eVar.f;
        this.n0 = eVar.g;
        this.o0 = eVar.h;
        this.p0 = eVar.j;
        this.q0 = eVar.k;
        this.r0 = eVar.l;
        yt0.c(c0021a.d.d);
        a.c cVar = c0021a.d;
        this.t0 = cVar.i;
        int i3 = cVar.f;
        int i4 = cVar.b;
        this.u0 = c0021a.c.e;
        for (String str : c0021a.g.keySet()) {
            ConstraintAttribute constraintAttribute = c0021a.g.get(str);
            if (constraintAttribute.g()) {
                this.v0.put(str, constraintAttribute);
            }
        }
    }

    @Override // java.lang.Comparable
    /* renamed from: f */
    public int compareTo(t92 t92Var) {
        return Float.compare(this.s0, t92Var.s0);
    }

    public final boolean g(float f, float f2) {
        return (Float.isNaN(f) || Float.isNaN(f2)) ? Float.isNaN(f) != Float.isNaN(f2) : Math.abs(f - f2) > 1.0E-6f;
    }

    public void h(t92 t92Var, HashSet<String> hashSet) {
        if (g(this.a, t92Var.a)) {
            hashSet.add("alpha");
        }
        if (g(this.h0, t92Var.h0)) {
            hashSet.add("elevation");
        }
        int i = this.g0;
        int i2 = t92Var.g0;
        if (i != i2 && this.f0 == 0 && (i == 0 || i2 == 0)) {
            hashSet.add("alpha");
        }
        if (g(this.i0, t92Var.i0)) {
            hashSet.add("rotation");
        }
        if (!Float.isNaN(this.t0) || !Float.isNaN(t92Var.t0)) {
            hashSet.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.u0) || !Float.isNaN(t92Var.u0)) {
            hashSet.add("progress");
        }
        if (g(this.j0, t92Var.j0)) {
            hashSet.add("rotationX");
        }
        if (g(this.k0, t92Var.k0)) {
            hashSet.add("rotationY");
        }
        if (g(this.n0, t92Var.n0)) {
            hashSet.add("transformPivotX");
        }
        if (g(this.o0, t92Var.o0)) {
            hashSet.add("transformPivotY");
        }
        if (g(this.l0, t92Var.l0)) {
            hashSet.add("scaleX");
        }
        if (g(this.m0, t92Var.m0)) {
            hashSet.add("scaleY");
        }
        if (g(this.p0, t92Var.p0)) {
            hashSet.add("translationX");
        }
        if (g(this.q0, t92Var.q0)) {
            hashSet.add("translationY");
        }
        if (g(this.r0, t92Var.r0)) {
            hashSet.add("translationZ");
        }
    }

    public void j(float f, float f2, float f3, float f4) {
    }

    public void k(Rect rect, View view, int i, float f) {
        j(rect.left, rect.top, rect.width(), rect.height());
        d(view);
        this.n0 = Float.NaN;
        this.o0 = Float.NaN;
        if (i == 1) {
            this.i0 = f - 90.0f;
        } else if (i != 2) {
        } else {
            this.i0 = f + 90.0f;
        }
    }

    public void l(Rect rect, a aVar, int i, int i2) {
        j(rect.left, rect.top, rect.width(), rect.height());
        e(aVar.z(i2));
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        return;
                    }
                }
            }
            float f = this.i0 + 90.0f;
            this.i0 = f;
            if (f > 180.0f) {
                this.i0 = f - 360.0f;
                return;
            }
            return;
        }
        this.i0 -= 90.0f;
    }

    public void o(View view) {
        j(view.getX(), view.getY(), view.getWidth(), view.getHeight());
        d(view);
    }
}
