package defpackage;

import android.content.Context;
import android.os.Build;
import com.facebook.cache.disk.f;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.image.a;

/* compiled from: ImagePipelineFactory.java */
/* renamed from: uo1  reason: default package */
/* loaded from: classes.dex */
public class uo1 {
    public static final Class<?> t = uo1.class;
    public static uo1 u;
    public static boolean v;
    public static qo1 w;
    public final h54 a;
    public final so1 b;
    public final a00 c;
    public i90<wt, a> d;
    public jr1<wt, a> e;
    public i90<wt, PooledByteBuffer> f;
    public jr1<wt, PooledByteBuffer> g;
    public xr h;
    public f i;
    public tn1 j;
    public qo1 k;
    public bp1 l;
    public hv2 m;
    public lv2 n;
    public xr o;
    public f p;
    public br2 q;
    public dr2 r;
    public qd s;

    public uo1(so1 so1Var) {
        h54 i54Var;
        if (nc1.d()) {
            nc1.a("ImagePipelineConfig()");
        }
        so1 so1Var2 = (so1) xt2.g(so1Var);
        this.b = so1Var2;
        if (so1Var2.C().u()) {
            i54Var = new b11(so1Var.E().b());
        } else {
            i54Var = new i54(so1Var.E().b());
        }
        this.a = i54Var;
        com.facebook.common.references.a.N(so1Var.C().b());
        this.c = new a00(so1Var.f());
        if (nc1.d()) {
            nc1.b();
        }
    }

    public static uo1 l() {
        return (uo1) xt2.h(u, "ImagePipelineFactory was not initialized!");
    }

    public static synchronized void u(so1 so1Var) {
        synchronized (uo1.class) {
            if (u != null) {
                v11.t(t, "ImagePipelineFactory has already been initialized! `ImagePipelineFactory.initialize(...)` should only be called once to avoid unexpected behavior.");
            }
            u = new uo1(so1Var);
        }
    }

    public static synchronized void v(Context context) {
        synchronized (uo1.class) {
            if (nc1.d()) {
                nc1.a("ImagePipelineFactory#initialize");
            }
            u(ro1.J(context).K());
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public final qo1 a() {
        return new qo1(r(), this.b.k(), this.b.b(), this.b.d(), e(), h(), m(), s(), this.b.l(), this.a, this.b.C().i(), this.b.C().w(), this.b.z(), this.b);
    }

    public uq0 b(Context context) {
        qd c = c();
        if (c == null) {
            return null;
        }
        return c.a(context);
    }

    public final qd c() {
        if (this.s == null) {
            this.s = rd.a(o(), this.b.E(), d(), this.b.C().B(), this.b.t());
        }
        return this.s;
    }

    public i90<wt, a> d() {
        if (this.d == null) {
            this.d = this.b.g().a(this.b.A(), this.b.w(), this.b.n(), this.b.C().E(), this.b.C().C(), this.b.r());
        }
        return this.d;
    }

    public jr1<wt, a> e() {
        if (this.e == null) {
            this.e = kr1.a(d(), this.b.q());
        }
        return this.e;
    }

    public a00 f() {
        return this.c;
    }

    public i90<wt, PooledByteBuffer> g() {
        if (this.f == null) {
            this.f = xu0.a(this.b.D(), this.b.w());
        }
        return this.f;
    }

    public jr1<wt, PooledByteBuffer> h() {
        l72<wt, PooledByteBuffer> g;
        if (this.g == null) {
            if (this.b.i() != null) {
                g = this.b.i();
            } else {
                g = g();
            }
            this.g = av0.a(g, this.b.q());
        }
        return this.g;
    }

    public final tn1 i() {
        tn1 tn1Var;
        if (this.j == null) {
            if (this.b.B() != null) {
                this.j = this.b.B();
            } else {
                qd c = c();
                tn1 tn1Var2 = null;
                if (c != null) {
                    tn1Var2 = c.b();
                    tn1Var = c.c();
                } else {
                    tn1Var = null;
                }
                this.b.x();
                this.j = new uj0(tn1Var2, tn1Var, p());
            }
        }
        return this.j;
    }

    public qo1 j() {
        if (v) {
            if (w == null) {
                qo1 a = a();
                w = a;
                this.k = a;
            }
            return w;
        }
        if (this.k == null) {
            this.k = a();
        }
        return this.k;
    }

    public final bp1 k() {
        if (this.l == null) {
            if (this.b.v() == null && this.b.u() == null && this.b.C().x()) {
                this.l = new ip3(this.b.C().f());
            } else {
                this.l = new oa2(this.b.C().f(), this.b.C().l(), this.b.v(), this.b.u(), this.b.C().t());
            }
        }
        return this.l;
    }

    public xr m() {
        if (this.h == null) {
            this.h = new xr(n(), this.b.a().i(this.b.c()), this.b.a().j(), this.b.E().e(), this.b.E().d(), this.b.q());
        }
        return this.h;
    }

    public f n() {
        if (this.i == null) {
            this.i = this.b.e().a(this.b.j());
        }
        return this.i;
    }

    public br2 o() {
        if (this.q == null) {
            this.q = cr2.a(this.b.a(), p(), f());
        }
        return this.q;
    }

    public dr2 p() {
        if (this.r == null) {
            this.r = er2.a(this.b.a(), this.b.C().v());
        }
        return this.r;
    }

    public final hv2 q() {
        if (this.m == null) {
            this.m = this.b.C().h().a(this.b.getContext(), this.b.a().k(), i(), this.b.o(), this.b.s(), this.b.m(), this.b.C().p(), this.b.E(), this.b.a().i(this.b.c()), this.b.a().j(), e(), h(), m(), s(), this.b.l(), o(), this.b.C().e(), this.b.C().d(), this.b.C().c(), this.b.C().f(), f(), this.b.C().D(), this.b.C().j());
        }
        return this.m;
    }

    public final lv2 r() {
        boolean z = Build.VERSION.SDK_INT >= 24 && this.b.C().k();
        if (this.n == null) {
            this.n = new lv2(this.b.getContext().getApplicationContext().getContentResolver(), q(), this.b.h(), this.b.m(), this.b.C().z(), this.a, this.b.s(), z, this.b.C().y(), this.b.y(), k(), this.b.C().s(), this.b.C().q(), this.b.C().a());
        }
        return this.n;
    }

    public final xr s() {
        if (this.o == null) {
            this.o = new xr(t(), this.b.a().i(this.b.c()), this.b.a().j(), this.b.E().e(), this.b.E().d(), this.b.q());
        }
        return this.o;
    }

    public f t() {
        if (this.p == null) {
            this.p = this.b.e().a(this.b.p());
        }
        return this.p;
    }
}
