package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentNotificationBinding.java */
/* renamed from: oa1  reason: default package */
/* loaded from: classes2.dex */
public final class oa1 {
    public final ConstraintLayout a;
    public final LinearLayoutCompat b;
    public final View c;
    public final ConstraintLayout d;
    public final ImageView e;
    public final RecyclerView f;
    public final NestedScrollView g;
    public final zd3 h;
    public final zd3 i;
    public final FrameLayout j;
    public final SwitchMaterial k;
    public final MaterialButton l;

    public oa1(ConstraintLayout constraintLayout, LinearLayoutCompat linearLayoutCompat, View view, CardView cardView, CardView cardView2, ConstraintLayout constraintLayout2, ImageView imageView, RecyclerView recyclerView, NestedScrollView nestedScrollView, zd3 zd3Var, zd3 zd3Var2, FrameLayout frameLayout, SwitchMaterial switchMaterial, TextView textView, TextView textView2, MaterialButton materialButton) {
        this.a = constraintLayout;
        this.b = linearLayoutCompat;
        this.c = view;
        this.d = constraintLayout2;
        this.e = imageView;
        this.f = recyclerView;
        this.g = nestedScrollView;
        this.h = zd3Var;
        this.i = zd3Var2;
        this.j = frameLayout;
        this.k = switchMaterial;
        this.l = materialButton;
    }

    public static oa1 a(View view) {
        int i = R.id.chainWrapper;
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.chainWrapper);
        if (linearLayoutCompat != null) {
            i = R.id.constraintLayout;
            View a = ai4.a(view, R.id.constraintLayout);
            if (a != null) {
                i = R.id.cv_1;
                CardView cardView = (CardView) ai4.a(view, R.id.cv_1);
                if (cardView != null) {
                    i = R.id.cvPriceAlert;
                    CardView cardView2 = (CardView) ai4.a(view, R.id.cvPriceAlert);
                    if (cardView2 != null) {
                        i = R.id.cvTokenWrapper;
                        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.cvTokenWrapper);
                        if (constraintLayout != null) {
                            i = R.id.iv_back;
                            ImageView imageView = (ImageView) ai4.a(view, R.id.iv_back);
                            if (imageView != null) {
                                i = R.id.rvTokenList;
                                RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvTokenList);
                                if (recyclerView != null) {
                                    i = R.id.scrollView;
                                    NestedScrollView nestedScrollView = (NestedScrollView) ai4.a(view, R.id.scrollView);
                                    if (nestedScrollView != null) {
                                        i = R.id.searchBar;
                                        View a2 = ai4.a(view, R.id.searchBar);
                                        if (a2 != null) {
                                            zd3 a3 = zd3.a(a2);
                                            i = R.id.stickySearchBar;
                                            View a4 = ai4.a(view, R.id.stickySearchBar);
                                            if (a4 != null) {
                                                zd3 a5 = zd3.a(a4);
                                                i = R.id.stickySearchWrapper;
                                                FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.stickySearchWrapper);
                                                if (frameLayout != null) {
                                                    i = R.id.switchEnableNotificationConfirm;
                                                    SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchEnableNotificationConfirm);
                                                    if (switchMaterial != null) {
                                                        i = R.id.textView7;
                                                        TextView textView = (TextView) ai4.a(view, R.id.textView7);
                                                        if (textView != null) {
                                                            i = R.id.txtHeaderCPA;
                                                            TextView textView2 = (TextView) ai4.a(view, R.id.txtHeaderCPA);
                                                            if (textView2 != null) {
                                                                i = R.id.txtSubHeaderCPA;
                                                                MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.txtSubHeaderCPA);
                                                                if (materialButton != null) {
                                                                    return new oa1((ConstraintLayout) view, linearLayoutCompat, a, cardView, cardView2, constraintLayout, imageView, recyclerView, nestedScrollView, a3, a5, frameLayout, switchMaterial, textView, textView2, materialButton);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
