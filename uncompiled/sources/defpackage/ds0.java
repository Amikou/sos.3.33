package defpackage;

/* compiled from: DropFramesFrameScheduler.java */
/* renamed from: ds0  reason: default package */
/* loaded from: classes.dex */
public class ds0 implements ac1 {
    public final ke a;
    public long b = -1;

    public ds0(ke keVar) {
        this.a = keVar;
    }

    @Override // defpackage.ac1
    public long a(long j) {
        long d = d();
        long j2 = 0;
        if (d == 0) {
            return -1L;
        }
        if (e() || j / d() < this.a.b()) {
            long j3 = j % d;
            int a = this.a.a();
            for (int i = 0; i < a && j2 <= j3; i++) {
                j2 += this.a.h(i);
            }
            return j + (j2 - j3);
        }
        return -1L;
    }

    @Override // defpackage.ac1
    public int b(long j, long j2) {
        long d = d();
        if (d == 0) {
            return c(0L);
        }
        if (e() || j / d < this.a.b()) {
            return c(j % d);
        }
        return -1;
    }

    public int c(long j) {
        int i = 0;
        long j2 = 0;
        do {
            j2 += this.a.h(i);
            i++;
        } while (j >= j2);
        return i - 1;
    }

    public long d() {
        long j = this.b;
        if (j != -1) {
            return j;
        }
        this.b = 0L;
        int a = this.a.a();
        for (int i = 0; i < a; i++) {
            this.b += this.a.h(i);
        }
        return this.b;
    }

    public boolean e() {
        return this.a.b() == 0;
    }
}
