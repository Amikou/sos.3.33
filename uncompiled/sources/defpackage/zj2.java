package defpackage;

import android.content.Context;
import android.net.Uri;
import com.onesignal.OneSignal;
import com.onesignal.g0;
import java.security.SecureRandom;
import org.json.JSONObject;

/* compiled from: OSNotificationGenerationJob.java */
/* renamed from: zj2  reason: default package */
/* loaded from: classes2.dex */
public class zj2 {
    public g0 a;
    public Context b;
    public JSONObject c;
    public boolean d;
    public boolean e;
    public Long f;
    public CharSequence g;
    public CharSequence h;
    public Uri i;
    public Integer j;
    public Integer k;
    public Uri l;

    public zj2(Context context) {
        this.b = context;
    }

    public Integer a() {
        return Integer.valueOf(this.a.d());
    }

    public String b() {
        return OneSignal.f0(this.c);
    }

    public CharSequence c() {
        CharSequence charSequence = this.g;
        return charSequence != null ? charSequence : this.a.e();
    }

    public Context d() {
        return this.b;
    }

    public JSONObject e() {
        return this.c;
    }

    public g0 f() {
        return this.a;
    }

    public Uri g() {
        return this.l;
    }

    public Integer h() {
        return this.j;
    }

    public Uri i() {
        return this.i;
    }

    public Long j() {
        return this.f;
    }

    public CharSequence k() {
        CharSequence charSequence = this.h;
        return charSequence != null ? charSequence : this.a.k();
    }

    public boolean l() {
        return this.a.f() != null;
    }

    public boolean m() {
        return this.e;
    }

    public boolean n() {
        return this.d;
    }

    public void o(Context context) {
        this.b = context;
    }

    public void p(boolean z) {
        this.e = z;
    }

    public void q(JSONObject jSONObject) {
        this.c = jSONObject;
    }

    public void r(g0 g0Var) {
        if (g0Var != null && !g0Var.m()) {
            g0 g0Var2 = this.a;
            if (g0Var2 != null && g0Var2.m()) {
                g0Var.r(this.a.d());
            } else {
                g0Var.r(new SecureRandom().nextInt());
            }
        }
        this.a = g0Var;
    }

    public void s(Integer num) {
        this.k = num;
    }

    public void t(Uri uri) {
        this.l = uri;
    }

    public String toString() {
        return "OSNotificationGenerationJob{jsonPayload=" + this.c + ", isRestoring=" + this.d + ", isNotificationToDisplay=" + this.e + ", shownTimeStamp=" + this.f + ", overriddenBodyFromExtender=" + ((Object) this.g) + ", overriddenTitleFromExtender=" + ((Object) this.h) + ", overriddenSound=" + this.i + ", overriddenFlags=" + this.j + ", orgFlags=" + this.k + ", orgSound=" + this.l + ", notification=" + this.a + '}';
    }

    public void u(CharSequence charSequence) {
        this.g = charSequence;
    }

    public void v(Integer num) {
        this.j = num;
    }

    public void w(Uri uri) {
        this.i = uri;
    }

    public void x(CharSequence charSequence) {
        this.h = charSequence;
    }

    public void y(boolean z) {
        this.d = z;
    }

    public void z(Long l) {
        this.f = l;
    }

    public zj2(Context context, JSONObject jSONObject) {
        this(context, new g0(jSONObject), jSONObject);
    }

    public zj2(Context context, g0 g0Var, JSONObject jSONObject) {
        this.b = context;
        this.c = jSONObject;
        r(g0Var);
    }
}
