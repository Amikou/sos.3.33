package defpackage;

import java.math.BigInteger;

/* compiled from: EthHashrate.java */
/* renamed from: dx0  reason: default package */
/* loaded from: classes3.dex */
public class dx0 extends i83<String> {
    public BigInteger getHashrate() {
        return ej2.decodeQuantity(getResult());
    }
}
