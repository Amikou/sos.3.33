package defpackage;

import android.util.Log;
import android.util.SparseArray;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: w15  reason: default package */
/* loaded from: classes.dex */
public class w15 extends d25 {
    public final SparseArray<a> j0;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: w15$a */
    /* loaded from: classes.dex */
    public class a implements GoogleApiClient.c {
        public final int a;
        public final GoogleApiClient b;
        public final GoogleApiClient.c c;

        public a(int i, GoogleApiClient googleApiClient, GoogleApiClient.c cVar) {
            this.a = i;
            this.b = googleApiClient;
            this.c = cVar;
        }

        @Override // defpackage.jm2
        public final void onConnectionFailed(ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(valueOf.length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            w15.this.o(connectionResult, this.a);
        }
    }

    public w15(nz1 nz1Var) {
        super(nz1Var);
        this.j0 = new SparseArray<>();
        this.a.a("AutoManageHelper", this);
    }

    public static w15 p(lz1 lz1Var) {
        nz1 c = LifecycleCallback.c(lz1Var);
        w15 w15Var = (w15) c.b("AutoManageHelper", w15.class);
        return w15Var != null ? w15Var : new w15(c);
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.j0.size(); i++) {
            a s = s(i);
            if (s != null) {
                printWriter.append((CharSequence) str).append("GoogleApiClient #").print(s.a);
                printWriter.println(":");
                s.b.e(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @Override // defpackage.d25, com.google.android.gms.common.api.internal.LifecycleCallback
    public void i() {
        super.i();
        boolean z = this.f0;
        String valueOf = String.valueOf(this.j0);
        StringBuilder sb = new StringBuilder(valueOf.length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        sb.append(valueOf);
        if (this.g0.get() == null) {
            for (int i = 0; i < this.j0.size(); i++) {
                a s = s(i);
                if (s != null) {
                    s.b.connect();
                }
            }
        }
    }

    @Override // defpackage.d25, com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        for (int i = 0; i < this.j0.size(); i++) {
            a s = s(i);
            if (s != null) {
                s.b.disconnect();
            }
        }
    }

    @Override // defpackage.d25
    public final void l() {
        for (int i = 0; i < this.j0.size(); i++) {
            a s = s(i);
            if (s != null) {
                s.b.connect();
            }
        }
    }

    @Override // defpackage.d25
    public final void m(ConnectionResult connectionResult, int i) {
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.j0.get(i);
        if (aVar != null) {
            q(i);
            GoogleApiClient.c cVar = aVar.c;
            if (cVar != null) {
                cVar.onConnectionFailed(connectionResult);
            }
        }
    }

    public final void q(int i) {
        a aVar = this.j0.get(i);
        this.j0.remove(i);
        if (aVar != null) {
            aVar.b.j(aVar);
            aVar.b.disconnect();
        }
    }

    public final void r(int i, GoogleApiClient googleApiClient, GoogleApiClient.c cVar) {
        zt2.k(googleApiClient, "GoogleApiClient instance cannot be null");
        boolean z = this.j0.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        zt2.n(z, sb.toString());
        j25 j25Var = this.g0.get();
        boolean z2 = this.f0;
        String valueOf = String.valueOf(j25Var);
        StringBuilder sb2 = new StringBuilder(valueOf.length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        sb2.append(z2);
        sb2.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        sb2.append(valueOf);
        a aVar = new a(i, googleApiClient, cVar);
        googleApiClient.i(aVar);
        this.j0.put(i, aVar);
        if (this.f0 && j25Var == null) {
            String valueOf2 = String.valueOf(googleApiClient);
            StringBuilder sb3 = new StringBuilder(valueOf2.length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            googleApiClient.connect();
        }
    }

    public final a s(int i) {
        if (this.j0.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.j0;
        return sparseArray.get(sparseArray.keyAt(i));
    }
}
