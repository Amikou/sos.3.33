package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: UiThreadImmediateExecutorService.java */
/* renamed from: me4  reason: default package */
/* loaded from: classes.dex */
public class me4 extends nj1 {
    public static me4 f0;

    public me4() {
        super(new Handler(Looper.getMainLooper()));
    }

    public static me4 g() {
        if (f0 == null) {
            f0 = new me4();
        }
        return f0;
    }

    @Override // defpackage.nj1, java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        if (a()) {
            runnable.run();
        } else {
            super.execute(runnable);
        }
    }
}
