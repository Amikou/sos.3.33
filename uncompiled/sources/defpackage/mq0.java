package defpackage;

/* compiled from: DownsampleUtil.java */
/* renamed from: mq0  reason: default package */
/* loaded from: classes.dex */
public class mq0 {
    public static float a(p93 p93Var, p73 p73Var, zu0 zu0Var) {
        xt2.b(Boolean.valueOf(zu0.A(zu0Var)));
        return 1.0f;
    }

    public static int b(p93 p93Var, p73 p73Var, zu0 zu0Var, int i) {
        int d;
        if (zu0.A(zu0Var)) {
            float a = a(p93Var, p73Var, zu0Var);
            if (zu0Var.l() == wj0.a) {
                d = e(a);
            } else {
                d = d(a);
            }
            int max = Math.max(zu0Var.j(), zu0Var.v());
            float f = i;
            while (max / d > f) {
                d = zu0Var.l() == wj0.a ? d * 2 : d + 1;
            }
            return d;
        }
        return 1;
    }

    public static int c(zu0 zu0Var, int i, int i2) {
        int r = zu0Var.r();
        while ((((zu0Var.v() * zu0Var.j()) * i) / r) / r > i2) {
            r *= 2;
        }
        return r;
    }

    public static int d(float f) {
        if (f > 0.6666667f) {
            return 1;
        }
        int i = 2;
        while (true) {
            double d = i;
            if ((1.0d / d) + ((1.0d / (Math.pow(d, 2.0d) - d)) * 0.3333333432674408d) <= f) {
                return i - 1;
            }
            i++;
        }
    }

    public static int e(float f) {
        if (f > 0.6666667f) {
            return 1;
        }
        int i = 2;
        while (true) {
            int i2 = i * 2;
            double d = 1.0d / i2;
            if (d + (0.3333333432674408d * d) <= f) {
                return i;
            }
            i = i2;
        }
    }
}
