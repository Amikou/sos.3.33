package defpackage;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.StateSet;
import com.github.mikephil.charting.utils.Utils;
import defpackage.at3;
import defpackage.sq0;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: AnimatedStateListDrawableCompat.java */
@SuppressLint({"RestrictedAPI"})
/* renamed from: ae  reason: default package */
/* loaded from: classes.dex */
public class ae extends at3 implements i64 {
    public c s0;
    public g t0;
    public int u0;
    public int v0;
    public boolean w0;

    /* compiled from: AnimatedStateListDrawableCompat.java */
    /* renamed from: ae$b */
    /* loaded from: classes.dex */
    public static class b extends g {
        public final Animatable a;

        public b(Animatable animatable) {
            super();
            this.a = animatable;
        }

        @Override // defpackage.ae.g
        public void c() {
            this.a.start();
        }

        @Override // defpackage.ae.g
        public void d() {
            this.a.stop();
        }
    }

    /* compiled from: AnimatedStateListDrawableCompat.java */
    /* renamed from: ae$c */
    /* loaded from: classes.dex */
    public static class c extends at3.a {
        public i22<Long> K;
        public lr3<Integer> L;

        public c(c cVar, ae aeVar, Resources resources) {
            super(cVar, aeVar, resources);
            if (cVar != null) {
                this.K = cVar.K;
                this.L = cVar.L;
                return;
            }
            this.K = new i22<>();
            this.L = new lr3<>();
        }

        public static long D(int i, int i2) {
            return i2 | (i << 32);
        }

        public int B(int[] iArr, Drawable drawable, int i) {
            int z = super.z(iArr, drawable);
            this.L.k(z, Integer.valueOf(i));
            return z;
        }

        public int C(int i, int i2, Drawable drawable, boolean z) {
            int a = super.a(drawable);
            long D = D(i, i2);
            long j = z ? 8589934592L : 0L;
            long j2 = a;
            this.K.a(D, Long.valueOf(j2 | j));
            if (z) {
                this.K.a(D(i2, i), Long.valueOf(4294967296L | j2 | j));
            }
            return a;
        }

        public int E(int i) {
            if (i < 0) {
                return 0;
            }
            return this.L.g(i, 0).intValue();
        }

        public int F(int[] iArr) {
            int A = super.A(iArr);
            return A >= 0 ? A : super.A(StateSet.WILD_CARD);
        }

        public int G(int i, int i2) {
            return (int) this.K.h(D(i, i2), -1L).longValue();
        }

        public boolean H(int i, int i2) {
            return (this.K.h(D(i, i2), -1L).longValue() & 4294967296L) != 0;
        }

        public boolean I(int i, int i2) {
            return (this.K.h(D(i, i2), -1L).longValue() & 8589934592L) != 0;
        }

        @Override // defpackage.at3.a, android.graphics.drawable.Drawable.ConstantState
        public Drawable newDrawable() {
            return new ae(this, null);
        }

        @Override // defpackage.at3.a, defpackage.sq0.d
        public void r() {
            this.K = this.K.clone();
            this.L = this.L.clone();
        }

        @Override // defpackage.at3.a, android.graphics.drawable.Drawable.ConstantState
        public Drawable newDrawable(Resources resources) {
            return new ae(this, resources);
        }
    }

    /* compiled from: AnimatedStateListDrawableCompat.java */
    /* renamed from: ae$d */
    /* loaded from: classes.dex */
    public static class d extends g {
        public final be a;

        public d(be beVar) {
            super();
            this.a = beVar;
        }

        @Override // defpackage.ae.g
        public void c() {
            this.a.start();
        }

        @Override // defpackage.ae.g
        public void d() {
            this.a.stop();
        }
    }

    /* compiled from: AnimatedStateListDrawableCompat.java */
    /* renamed from: ae$e */
    /* loaded from: classes.dex */
    public static class e extends g {
        public final ObjectAnimator a;
        public final boolean b;

        public e(AnimationDrawable animationDrawable, boolean z, boolean z2) {
            super();
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            int i = z ? numberOfFrames - 1 : 0;
            int i2 = z ? 0 : numberOfFrames - 1;
            f fVar = new f(animationDrawable, z);
            ObjectAnimator ofInt = ObjectAnimator.ofInt(animationDrawable, "currentIndex", i, i2);
            if (Build.VERSION.SDK_INT >= 18) {
                ofInt.setAutoCancel(true);
            }
            ofInt.setDuration(fVar.a());
            ofInt.setInterpolator(fVar);
            this.b = z2;
            this.a = ofInt;
        }

        @Override // defpackage.ae.g
        public boolean a() {
            return this.b;
        }

        @Override // defpackage.ae.g
        public void b() {
            this.a.reverse();
        }

        @Override // defpackage.ae.g
        public void c() {
            this.a.start();
        }

        @Override // defpackage.ae.g
        public void d() {
            this.a.cancel();
        }
    }

    /* compiled from: AnimatedStateListDrawableCompat.java */
    /* renamed from: ae$f */
    /* loaded from: classes.dex */
    public static class f implements TimeInterpolator {
        public int[] a;
        public int b;
        public int c;

        public f(AnimationDrawable animationDrawable, boolean z) {
            b(animationDrawable, z);
        }

        public int a() {
            return this.c;
        }

        public int b(AnimationDrawable animationDrawable, boolean z) {
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            this.b = numberOfFrames;
            int[] iArr = this.a;
            if (iArr == null || iArr.length < numberOfFrames) {
                this.a = new int[numberOfFrames];
            }
            int[] iArr2 = this.a;
            int i = 0;
            for (int i2 = 0; i2 < numberOfFrames; i2++) {
                int duration = animationDrawable.getDuration(z ? (numberOfFrames - i2) - 1 : i2);
                iArr2[i2] = duration;
                i += duration;
            }
            this.c = i;
            return i;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            int i = (int) ((f * this.c) + 0.5f);
            int i2 = this.b;
            int[] iArr = this.a;
            int i3 = 0;
            while (i3 < i2 && i >= iArr[i3]) {
                i -= iArr[i3];
                i3++;
            }
            return (i3 / i2) + (i3 < i2 ? i / this.c : Utils.FLOAT_EPSILON);
        }
    }

    /* compiled from: AnimatedStateListDrawableCompat.java */
    /* renamed from: ae$g */
    /* loaded from: classes.dex */
    public static abstract class g {
        public g() {
        }

        public boolean a() {
            return false;
        }

        public void b() {
        }

        public abstract void c();

        public abstract void d();
    }

    public ae() {
        this(null, null);
    }

    public static ae m(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("animated-selector")) {
            ae aeVar = new ae();
            aeVar.n(context, resources, xmlPullParser, attributeSet, theme);
            return aeVar;
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid animated-selector tag " + name);
    }

    @Override // defpackage.at3, defpackage.sq0
    public void h(sq0.d dVar) {
        super.h(dVar);
        if (dVar instanceof c) {
            this.s0 = (c) dVar;
        }
    }

    @Override // defpackage.at3, android.graphics.drawable.Drawable
    public boolean isStateful() {
        return true;
    }

    @Override // defpackage.sq0, android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        g gVar = this.t0;
        if (gVar != null) {
            gVar.d();
            this.t0 = null;
            g(this.u0);
            this.u0 = -1;
            this.v0 = -1;
        }
    }

    @Override // defpackage.at3
    /* renamed from: l */
    public c j() {
        return new c(this.s0, this, null);
    }

    @Override // defpackage.at3, defpackage.sq0, android.graphics.drawable.Drawable
    public Drawable mutate() {
        if (!this.w0 && super.mutate() == this) {
            this.s0.r();
            this.w0 = true;
        }
        return this;
    }

    public void n(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        TypedArray k = xd4.k(resources, theme, attributeSet, m23.AnimatedStateListDrawableCompat);
        setVisible(k.getBoolean(m23.AnimatedStateListDrawableCompat_android_visible, true), true);
        t(k);
        i(resources);
        k.recycle();
        o(context, resources, xmlPullParser, attributeSet, theme);
        p();
    }

    public final void o(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth = xmlPullParser.getDepth() + 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 1) {
                return;
            }
            int depth2 = xmlPullParser.getDepth();
            if (depth2 < depth && next == 3) {
                return;
            }
            if (next == 2 && depth2 <= depth) {
                if (xmlPullParser.getName().equals("item")) {
                    q(context, resources, xmlPullParser, attributeSet, theme);
                } else if (xmlPullParser.getName().equals("transition")) {
                    r(context, resources, xmlPullParser, attributeSet, theme);
                }
            }
        }
    }

    @Override // defpackage.at3, defpackage.sq0, android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        int F = this.s0.F(iArr);
        boolean z = F != c() && (s(F) || g(F));
        Drawable current = getCurrent();
        return current != null ? z | current.setState(iArr) : z;
    }

    public final void p() {
        onStateChange(getState());
    }

    public final int q(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray k = xd4.k(resources, theme, attributeSet, m23.AnimatedStateListDrawableItem);
        int resourceId = k.getResourceId(m23.AnimatedStateListDrawableItem_android_id, 0);
        int resourceId2 = k.getResourceId(m23.AnimatedStateListDrawableItem_android_drawable, -1);
        Drawable j = resourceId2 > 0 ? b83.h().j(context, resourceId2) : null;
        k.recycle();
        int[] k2 = k(attributeSet);
        if (j == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next == 2) {
                if (xmlPullParser.getName().equals("vector")) {
                    j = eh4.c(resources, xmlPullParser, attributeSet, theme);
                } else if (Build.VERSION.SDK_INT >= 21) {
                    j = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
                } else {
                    j = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
                }
            } else {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
            }
        }
        if (j != null) {
            return this.s0.B(k2, j, resourceId);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
    }

    public final int r(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray k = xd4.k(resources, theme, attributeSet, m23.AnimatedStateListDrawableTransition);
        int resourceId = k.getResourceId(m23.AnimatedStateListDrawableTransition_android_fromId, -1);
        int resourceId2 = k.getResourceId(m23.AnimatedStateListDrawableTransition_android_toId, -1);
        int resourceId3 = k.getResourceId(m23.AnimatedStateListDrawableTransition_android_drawable, -1);
        Drawable j = resourceId3 > 0 ? b83.h().j(context, resourceId3) : null;
        boolean z = k.getBoolean(m23.AnimatedStateListDrawableTransition_android_reversible, false);
        k.recycle();
        if (j == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next == 2) {
                if (xmlPullParser.getName().equals("animated-vector")) {
                    j = be.b(context, resources, xmlPullParser, attributeSet, theme);
                } else if (Build.VERSION.SDK_INT >= 21) {
                    j = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
                } else {
                    j = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
                }
            } else {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
            }
        }
        if (j == null) {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
        } else if (resourceId != -1 && resourceId2 != -1) {
            return this.s0.C(resourceId, resourceId2, j, z);
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires 'fromId' & 'toId' attributes");
        }
    }

    public final boolean s(int i) {
        int c2;
        int G;
        g bVar;
        g gVar = this.t0;
        if (gVar != null) {
            if (i == this.u0) {
                return true;
            }
            if (i == this.v0 && gVar.a()) {
                gVar.b();
                this.u0 = this.v0;
                this.v0 = i;
                return true;
            }
            c2 = this.u0;
            gVar.d();
        } else {
            c2 = c();
        }
        this.t0 = null;
        this.v0 = -1;
        this.u0 = -1;
        c cVar = this.s0;
        int E = cVar.E(c2);
        int E2 = cVar.E(i);
        if (E2 == 0 || E == 0 || (G = cVar.G(E, E2)) < 0) {
            return false;
        }
        boolean I = cVar.I(E, E2);
        g(G);
        Drawable current = getCurrent();
        if (current instanceof AnimationDrawable) {
            bVar = new e((AnimationDrawable) current, cVar.H(E, E2), I);
        } else if (current instanceof be) {
            bVar = new d((be) current);
        } else {
            if (current instanceof Animatable) {
                bVar = new b((Animatable) current);
            }
            return false;
        }
        bVar.c();
        this.t0 = bVar;
        this.v0 = c2;
        this.u0 = i;
        return true;
    }

    @Override // defpackage.sq0, android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        g gVar = this.t0;
        if (gVar != null && (visible || z2)) {
            if (z) {
                gVar.c();
            } else {
                jumpToCurrentState();
            }
        }
        return visible;
    }

    public final void t(TypedArray typedArray) {
        c cVar = this.s0;
        if (Build.VERSION.SDK_INT >= 21) {
            cVar.d |= typedArray.getChangingConfigurations();
        }
        cVar.x(typedArray.getBoolean(m23.AnimatedStateListDrawableCompat_android_variablePadding, cVar.i));
        cVar.t(typedArray.getBoolean(m23.AnimatedStateListDrawableCompat_android_constantSize, cVar.l));
        cVar.u(typedArray.getInt(m23.AnimatedStateListDrawableCompat_android_enterFadeDuration, cVar.A));
        cVar.v(typedArray.getInt(m23.AnimatedStateListDrawableCompat_android_exitFadeDuration, cVar.B));
        setDither(typedArray.getBoolean(m23.AnimatedStateListDrawableCompat_android_dither, cVar.x));
    }

    public ae(c cVar, Resources resources) {
        super(null);
        this.u0 = -1;
        this.v0 = -1;
        h(new c(cVar, this, resources));
        onStateChange(getState());
        jumpToCurrentState();
    }
}
