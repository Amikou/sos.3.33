package defpackage;

import android.content.Context;
import com.google.firebase.FirebaseCommonRegistrar;
import defpackage.jz1;

/* renamed from: d51  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class d51 implements jz1.a {
    public static final /* synthetic */ d51 a = new d51();

    @Override // defpackage.jz1.a
    public final String extract(Object obj) {
        String h;
        h = FirebaseCommonRegistrar.h((Context) obj);
        return h;
    }
}
