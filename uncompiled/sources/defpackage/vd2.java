package defpackage;

import java.util.Comparator;

/* compiled from: Comparisons.kt */
/* renamed from: vd2  reason: default package */
/* loaded from: classes2.dex */
public final class vd2 implements Comparator<Comparable<? super Object>> {
    public static final vd2 a = new vd2();

    @Override // java.util.Comparator
    /* renamed from: a */
    public int compare(Comparable<Object> comparable, Comparable<Object> comparable2) {
        fs1.f(comparable, "a");
        fs1.f(comparable2, "b");
        return comparable.compareTo(comparable2);
    }

    @Override // java.util.Comparator
    public final Comparator<Comparable<? super Object>> reversed() {
        return z83.a;
    }
}
