package defpackage;

import java.math.BigInteger;

/* renamed from: rf3  reason: default package */
/* loaded from: classes2.dex */
public class rf3 {
    public static final int[] a = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 511};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        int a2 = kd2.a(16, iArr, iArr2, iArr3) + iArr[16] + iArr2[16];
        if (a2 > 511 || (a2 == 511 && kd2.n(16, iArr3, a))) {
            a2 = (a2 + kd2.r(16, iArr3)) & 511;
        }
        iArr3[16] = a2;
    }

    public static void b(int[] iArr, int[] iArr2) {
        int s = kd2.s(16, iArr, iArr2) + iArr[16];
        if (s > 511 || (s == 511 && kd2.n(16, iArr2, a))) {
            s = (s + kd2.r(16, iArr2)) & 511;
        }
        iArr2[16] = s;
    }

    public static int[] c(BigInteger bigInteger) {
        int[] o = kd2.o(521, bigInteger);
        if (kd2.n(17, o, a)) {
            kd2.Q(17, o);
        }
        return o;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        id2.a(iArr, iArr2, iArr3);
        int i = iArr[16];
        int i2 = iArr2[16];
        iArr3[32] = kd2.x(16, i, iArr2, i2, iArr, iArr3, 16) + (i * i2);
    }

    public static void e(int[] iArr, int[] iArr2) {
        id2.b(iArr, iArr2);
        int i = iArr[16];
        iArr2[32] = kd2.y(16, i << 1, iArr, 0, iArr2, 16) + (i * i);
    }

    public static void f(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] j = kd2.j(33);
        d(iArr, iArr2, j);
        h(j, iArr3);
    }

    public static void g(int[] iArr, int[] iArr2) {
        if (kd2.w(17, iArr)) {
            kd2.Q(17, iArr2);
        } else {
            kd2.K(17, a, iArr, iArr2);
        }
    }

    public static void h(int[] iArr, int[] iArr2) {
        int i = iArr[32];
        int B = (kd2.B(16, iArr, 16, 9, i, iArr2, 0) >>> 23) + (i >>> 9) + kd2.e(16, iArr, iArr2);
        if (B > 511 || (B == 511 && kd2.n(16, iArr2, a))) {
            B = (B + kd2.r(16, iArr2)) & 511;
        }
        iArr2[16] = B;
    }

    public static void i(int[] iArr) {
        int i = iArr[16];
        int g = kd2.g(16, i >>> 9, iArr) + (i & 511);
        if (g > 511 || (g == 511 && kd2.n(16, iArr, a))) {
            g = (g + kd2.r(16, iArr)) & 511;
        }
        iArr[16] = g;
    }

    public static void j(int[] iArr, int[] iArr2) {
        int[] j = kd2.j(33);
        e(iArr, j);
        h(j, iArr2);
    }

    public static void k(int[] iArr, int i, int[] iArr2) {
        int[] j = kd2.j(33);
        e(iArr, j);
        while (true) {
            h(j, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            e(iArr2, j);
        }
    }

    public static void l(int[] iArr, int[] iArr2, int[] iArr3) {
        int K = (kd2.K(16, iArr, iArr2, iArr3) + iArr[16]) - iArr2[16];
        if (K < 0) {
            K = (K + kd2.l(16, iArr3)) & 511;
        }
        iArr3[16] = K;
    }

    public static void m(int[] iArr, int[] iArr2) {
        int i = iArr[16];
        iArr2[16] = (kd2.E(16, iArr, i << 23, iArr2) | (i << 1)) & 511;
    }
}
