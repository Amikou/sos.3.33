package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentReflectionsBinding.java */
/* renamed from: ta1  reason: default package */
/* loaded from: classes2.dex */
public final class ta1 {
    public final AppBarLayout a;
    public final CollapsingToolbarLayout b;
    public final AppCompatImageView c;
    public final ry1 d;
    public final RecyclerView e;
    public final wf f;

    public ta1(FrameLayout frameLayout, AppBarLayout appBarLayout, CollapsingToolbarLayout collapsingToolbarLayout, FrameLayout frameLayout2, CoordinatorLayout coordinatorLayout, AppCompatImageView appCompatImageView, ry1 ry1Var, RecyclerView recyclerView, wf wfVar, TextView textView) {
        this.a = appBarLayout;
        this.b = collapsingToolbarLayout;
        this.c = appCompatImageView;
        this.d = ry1Var;
        this.e = recyclerView;
        this.f = wfVar;
    }

    public static ta1 a(View view) {
        int i = R.id.appBar;
        AppBarLayout appBarLayout = (AppBarLayout) ai4.a(view, R.id.appBar);
        if (appBarLayout != null) {
            i = R.id.ccToolBar;
            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) ai4.a(view, R.id.ccToolBar);
            if (collapsingToolbarLayout != null) {
                FrameLayout frameLayout = (FrameLayout) view;
                i = R.id.coordinator;
                CoordinatorLayout coordinatorLayout = (CoordinatorLayout) ai4.a(view, R.id.coordinator);
                if (coordinatorLayout != null) {
                    i = R.id.imgInfo;
                    AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgInfo);
                    if (appCompatImageView != null) {
                        i = R.id.lSelectTokenType;
                        View a = ai4.a(view, R.id.lSelectTokenType);
                        if (a != null) {
                            ry1 a2 = ry1.a(a);
                            i = R.id.rvReflectionToken;
                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvReflectionToken);
                            if (recyclerView != null) {
                                i = R.id.topBar;
                                View a3 = ai4.a(view, R.id.topBar);
                                if (a3 != null) {
                                    wf a4 = wf.a(a3);
                                    i = R.id.tvTitle;
                                    TextView textView = (TextView) ai4.a(view, R.id.tvTitle);
                                    if (textView != null) {
                                        return new ta1(frameLayout, appBarLayout, collapsingToolbarLayout, frameLayout, coordinatorLayout, appCompatImageView, a2, recyclerView, a4, textView);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
