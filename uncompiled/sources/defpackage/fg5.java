package defpackage;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* renamed from: fg5  reason: default package */
/* loaded from: classes.dex */
public class fg5<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public final int a;
    public List<xg5> f0;
    public Map<K, V> g0;
    public boolean h0;
    public volatile ch5 i0;
    public Map<K, V> j0;
    public volatile mg5 k0;

    public fg5(int i) {
        this.a = i;
        this.f0 = Collections.emptyList();
        this.g0 = Collections.emptyMap();
        this.j0 = Collections.emptyMap();
    }

    public /* synthetic */ fg5(int i, hg5 hg5Var) {
        this(i);
    }

    public static <FieldDescriptorType extends ta5<FieldDescriptorType>> fg5<FieldDescriptorType, Object> f(int i) {
        return new hg5(i);
    }

    public final boolean a() {
        return this.h0;
    }

    public final int b(K k) {
        int size = this.f0.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.f0.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo((Comparable) this.f0.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    @Override // java.util.AbstractMap, java.util.Map
    /* renamed from: c */
    public final V put(K k, V v) {
        o();
        int b = b(k);
        if (b >= 0) {
            return (V) this.f0.get(b).setValue(v);
        }
        o();
        if (this.f0.isEmpty() && !(this.f0 instanceof ArrayList)) {
            this.f0 = new ArrayList(this.a);
        }
        int i = -(b + 1);
        if (i >= this.a) {
            return p().put(k, v);
        }
        int size = this.f0.size();
        int i2 = this.a;
        if (size == i2) {
            xg5 remove = this.f0.remove(i2 - 1);
            p().put((K) remove.getKey(), (V) remove.getValue());
        }
        this.f0.add(i, new xg5(this, k, v));
        return null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        o();
        if (!this.f0.isEmpty()) {
            this.f0.clear();
        }
        if (this.g0.isEmpty()) {
            return;
        }
        this.g0.clear();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return b(comparable) >= 0 || this.g0.containsKey(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.i0 == null) {
            this.i0 = new ch5(this, null);
        }
        return this.i0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof fg5) {
            fg5 fg5Var = (fg5) obj;
            int size = size();
            if (size != fg5Var.size()) {
                return false;
            }
            int l = l();
            if (l != fg5Var.l()) {
                return entrySet().equals(fg5Var.entrySet());
            }
            for (int i = 0; i < l; i++) {
                if (!g(i).equals(fg5Var.g(i))) {
                    return false;
                }
            }
            if (l != size) {
                return this.g0.equals(fg5Var.g0);
            }
            return true;
        }
        return super.equals(obj);
    }

    public final Map.Entry<K, V> g(int i) {
        return this.f0.get(i);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int b = b(comparable);
        return b >= 0 ? (V) this.f0.get(b).getValue() : this.g0.get(comparable);
    }

    public final V h(int i) {
        o();
        V v = (V) this.f0.remove(i).getValue();
        if (!this.g0.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = p().entrySet().iterator();
            this.f0.add(new xg5(this, it.next()));
            it.remove();
        }
        return v;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int hashCode() {
        int l = l();
        int i = 0;
        for (int i2 = 0; i2 < l; i2++) {
            i += this.f0.get(i2).hashCode();
        }
        return this.g0.size() > 0 ? i + this.g0.hashCode() : i;
    }

    public final int l() {
        return this.f0.size();
    }

    public final Iterable<Map.Entry<K, V>> m() {
        return this.g0.isEmpty() ? pg5.a() : this.g0.entrySet();
    }

    public final Set<Map.Entry<K, V>> n() {
        if (this.k0 == null) {
            this.k0 = new mg5(this, null);
        }
        return this.k0;
    }

    public final void o() {
        if (this.h0) {
            throw new UnsupportedOperationException();
        }
    }

    public final SortedMap<K, V> p() {
        o();
        if (this.g0.isEmpty() && !(this.g0 instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.g0 = treeMap;
            this.j0 = treeMap.descendingMap();
        }
        return (SortedMap) this.g0;
    }

    public void q() {
        if (this.h0) {
            return;
        }
        this.g0 = this.g0.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.g0);
        this.j0 = this.j0.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.j0);
        this.h0 = true;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        o();
        Comparable comparable = (Comparable) obj;
        int b = b(comparable);
        if (b >= 0) {
            return (V) h(b);
        }
        if (this.g0.isEmpty()) {
            return null;
        }
        return this.g0.remove(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.f0.size() + this.g0.size();
    }
}
