package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: to5  reason: default package */
/* loaded from: classes.dex */
public final class to5 implements Runnable {
    public final /* synthetic */ t45 a;
    public final /* synthetic */ int f0;
    public final /* synthetic */ long g0;
    public final /* synthetic */ boolean h0;
    public final /* synthetic */ dp5 i0;

    public to5(dp5 dp5Var, t45 t45Var, int i, long j, boolean z) {
        this.i0 = dp5Var;
        this.a = t45Var;
        this.f0 = i;
        this.g0 = j;
        this.h0 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.i0.W(this.a);
        dp5.J(this.i0, this.a, this.f0, this.g0, false, this.h0);
    }
}
