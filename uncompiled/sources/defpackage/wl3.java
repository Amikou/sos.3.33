package defpackage;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/* compiled from: SerialExecutor.java */
/* renamed from: wl3  reason: default package */
/* loaded from: classes.dex */
public class wl3 implements Executor {
    public final Executor f0;
    public volatile Runnable h0;
    public final ArrayDeque<a> a = new ArrayDeque<>();
    public final Object g0 = new Object();

    /* compiled from: SerialExecutor.java */
    /* renamed from: wl3$a */
    /* loaded from: classes.dex */
    public static class a implements Runnable {
        public final wl3 a;
        public final Runnable f0;

        public a(wl3 wl3Var, Runnable runnable) {
            this.a = wl3Var;
            this.f0 = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                this.f0.run();
            } finally {
                this.a.b();
            }
        }
    }

    public wl3(Executor executor) {
        this.f0 = executor;
    }

    public boolean a() {
        boolean z;
        synchronized (this.g0) {
            z = !this.a.isEmpty();
        }
        return z;
    }

    public void b() {
        synchronized (this.g0) {
            a poll = this.a.poll();
            this.h0 = poll;
            if (poll != null) {
                this.f0.execute(this.h0);
            }
        }
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        synchronized (this.g0) {
            this.a.add(new a(this, runnable));
            if (this.h0 == null) {
                b();
            }
        }
    }
}
