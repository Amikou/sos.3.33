package defpackage;

import androidx.media3.common.util.b;
import defpackage.fu3;
import defpackage.s61;
import java.util.Arrays;

/* compiled from: FlacReader.java */
/* renamed from: q61  reason: default package */
/* loaded from: classes.dex */
public final class q61 extends fu3 {
    public s61 n;
    public a o;

    /* compiled from: FlacReader.java */
    /* renamed from: q61$a */
    /* loaded from: classes.dex */
    public static final class a implements zl2 {
        public s61 a;
        public s61.a b;
        public long c = -1;
        public long d = -1;

        public a(s61 s61Var, s61.a aVar) {
            this.a = s61Var;
            this.b = aVar;
        }

        @Override // defpackage.zl2
        public wi3 a() {
            ii.g(this.c != -1);
            return new r61(this.a, this.c);
        }

        @Override // defpackage.zl2
        public long b(q11 q11Var) {
            long j = this.d;
            if (j >= 0) {
                long j2 = -(j + 2);
                this.d = -1L;
                return j2;
            }
            return -1L;
        }

        @Override // defpackage.zl2
        public void c(long j) {
            long[] jArr = this.b.a;
            this.d = jArr[b.i(jArr, j, true, true)];
        }

        public void d(long j) {
            this.c = j;
        }
    }

    public static boolean o(byte[] bArr) {
        return bArr[0] == -1;
    }

    public static boolean p(op2 op2Var) {
        return op2Var.a() >= 5 && op2Var.D() == 127 && op2Var.F() == 1179402563;
    }

    @Override // defpackage.fu3
    public long f(op2 op2Var) {
        if (o(op2Var.d())) {
            return n(op2Var);
        }
        return -1L;
    }

    @Override // defpackage.fu3
    public boolean i(op2 op2Var, long j, fu3.b bVar) {
        byte[] d = op2Var.d();
        s61 s61Var = this.n;
        if (s61Var == null) {
            s61 s61Var2 = new s61(d, 17);
            this.n = s61Var2;
            bVar.a = s61Var2.g(Arrays.copyOfRange(d, 9, op2Var.f()), null);
            return true;
        } else if ((d[0] & Byte.MAX_VALUE) == 3) {
            s61.a g = p61.g(op2Var);
            s61 b = s61Var.b(g);
            this.n = b;
            this.o = new a(b, g);
            return true;
        } else if (o(d)) {
            a aVar = this.o;
            if (aVar != null) {
                aVar.d(j);
                bVar.b = this.o;
            }
            ii.e(bVar.a);
            return false;
        } else {
            return true;
        }
    }

    @Override // defpackage.fu3
    public void l(boolean z) {
        super.l(z);
        if (z) {
            this.n = null;
            this.o = null;
        }
    }

    public final int n(op2 op2Var) {
        int i = (op2Var.d()[2] & 255) >> 4;
        if (i == 6 || i == 7) {
            op2Var.Q(4);
            op2Var.K();
        }
        int j = o61.j(op2Var, i);
        op2Var.P(0);
        return j;
    }
}
