package defpackage;

import com.google.android.gms.internal.vision.n0;
import com.google.android.gms.internal.vision.zzml;
import com.google.android.gms.internal.vision.zzmo;
import defpackage.pr5;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: pr5  reason: default package */
/* loaded from: classes.dex */
public interface pr5<T extends pr5<T>> extends Comparable<T> {
    gw5 F0(gw5 gw5Var, n0 n0Var);

    boolean b();

    boolean c();

    ow5 c1(ow5 ow5Var, ow5 ow5Var2);

    int zza();

    zzml zzb();

    zzmo zzc();
}
