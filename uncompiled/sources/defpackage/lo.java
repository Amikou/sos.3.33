package defpackage;

import java.util.List;
import org.web3j.protocol.core.c;

/* compiled from: BatchResponse.java */
/* renamed from: lo  reason: default package */
/* loaded from: classes3.dex */
public class lo {
    private List<c<?, ? extends i83<?>>> requests;
    private List<? extends i83<?>> responses;

    public lo(List<c<?, ? extends i83<?>>> list, List<? extends i83<?>> list2) {
        this.requests = list;
        this.responses = list2;
    }

    public List<c<?, ? extends i83<?>>> getRequests() {
        return this.requests;
    }

    public List<? extends i83<?>> getResponses() {
        return this.responses;
    }
}
