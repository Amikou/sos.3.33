package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: hq2  reason: default package */
/* loaded from: classes.dex */
public final class hq2 {
    @RecentlyNonNull
    public static gq2<Status> a(@RecentlyNonNull Status status, @RecentlyNonNull GoogleApiClient googleApiClient) {
        zt2.k(status, "Result must not be null");
        ot3 ot3Var = new ot3(googleApiClient);
        ot3Var.g(status);
        return ot3Var;
    }
}
