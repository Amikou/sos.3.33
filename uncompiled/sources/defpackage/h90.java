package defpackage;

import java.util.LinkedHashMap;

/* compiled from: CountingLruMap.java */
/* renamed from: h90  reason: default package */
/* loaded from: classes.dex */
public class h90<K, V> {
    public final yg4<V> a;
    public final LinkedHashMap<K, V> b = new LinkedHashMap<>();
    public int c = 0;

    public h90(yg4<V> yg4Var) {
        this.a = yg4Var;
    }

    public synchronized boolean a(K k) {
        return this.b.containsKey(k);
    }

    public synchronized V b(K k) {
        return this.b.get(k);
    }

    public synchronized int c() {
        return this.b.size();
    }

    public synchronized K d() {
        return this.b.isEmpty() ? null : this.b.keySet().iterator().next();
    }

    public synchronized int e() {
        return this.c;
    }

    public final int f(V v) {
        if (v == null) {
            return 0;
        }
        return this.a.a(v);
    }

    public synchronized V g(K k, V v) {
        V remove;
        remove = this.b.remove(k);
        this.c -= f(remove);
        this.b.put(k, v);
        this.c += f(v);
        return remove;
    }

    public synchronized V h(K k) {
        V remove;
        remove = this.b.remove(k);
        this.c -= f(remove);
        return remove;
    }

    public synchronized void i() {
        if (this.b.isEmpty()) {
            this.c = 0;
        }
    }
}
