package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.n;
import com.google.crypto.tink.proto.HashType;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.u;
import com.google.crypto.tink.proto.v;
import com.google.crypto.tink.proto.w;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: HmacKeyManager.java */
/* renamed from: rk1  reason: default package */
/* loaded from: classes2.dex */
public final class rk1 extends g<u> {

    /* compiled from: HmacKeyManager.java */
    /* renamed from: rk1$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<n, u> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public n a(u uVar) throws GeneralSecurityException {
            HashType E = uVar.J().E();
            SecretKeySpec secretKeySpec = new SecretKeySpec(uVar.I().toByteArray(), "HMAC");
            int G = uVar.J().G();
            int i = c.a[E.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i == 3) {
                        return new ru2(new qu2("HMACSHA512", secretKeySpec), G);
                    }
                    throw new GeneralSecurityException("unknown hash");
                }
                return new ru2(new qu2("HMACSHA256", secretKeySpec), G);
            }
            return new ru2(new qu2("HMACSHA1", secretKeySpec), G);
        }
    }

    /* compiled from: HmacKeyManager.java */
    /* renamed from: rk1$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<v, u> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public u a(v vVar) throws GeneralSecurityException {
            return u.L().u(rk1.this.k()).t(vVar.G()).s(ByteString.copyFrom(p33.c(vVar.E()))).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public v c(ByteString byteString) throws InvalidProtocolBufferException {
            return v.H(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(v vVar) throws GeneralSecurityException {
            if (vVar.E() >= 16) {
                rk1.o(vVar.G());
                return;
            }
            throw new GeneralSecurityException("key too short");
        }
    }

    /* compiled from: HmacKeyManager.java */
    /* renamed from: rk1$c */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class c {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[HashType.values().length];
            a = iArr;
            try {
                iArr[HashType.SHA1.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[HashType.SHA256.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[HashType.SHA512.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public rk1() {
        super(u.class, new a(n.class));
    }

    public static void m(boolean z) throws GeneralSecurityException {
        q.q(new rk1(), z);
    }

    public static void o(w wVar) throws GeneralSecurityException {
        if (wVar.G() >= 10) {
            int i = c.a[wVar.E().ordinal()];
            if (i == 1) {
                if (wVar.G() > 20) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            } else if (i == 2) {
                if (wVar.G() > 32) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            } else if (i == 3) {
                if (wVar.G() > 64) {
                    throw new GeneralSecurityException("tag size too big");
                }
                return;
            } else {
                throw new GeneralSecurityException("unknown hash type");
            }
        }
        throw new GeneralSecurityException("tag size too small");
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.HmacKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, u> e() {
        return new b(v.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int k() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: l */
    public u g(ByteString byteString) throws InvalidProtocolBufferException {
        return u.M(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: n */
    public void i(u uVar) throws GeneralSecurityException {
        ug4.c(uVar.K(), k());
        if (uVar.I().size() >= 16) {
            o(uVar.J());
            return;
        }
        throw new GeneralSecurityException("key too short");
    }
}
