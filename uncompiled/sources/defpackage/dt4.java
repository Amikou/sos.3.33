package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.play.core.assetpacks.a;
import com.google.android.play.core.internal.p;

/* renamed from: dt4  reason: default package */
/* loaded from: classes2.dex */
public final class dt4 extends jt4 {
    public final /* synthetic */ int f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ String h0;
    public final /* synthetic */ int i0;
    public final /* synthetic */ tx4 j0;
    public final /* synthetic */ st4 k0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public dt4(st4 st4Var, tx4 tx4Var, int i, String str, String str2, int i2, tx4 tx4Var2) {
        super(tx4Var);
        this.k0 = st4Var;
        this.f0 = i;
        this.g0 = str;
        this.h0 = str2;
        this.i0 = i2;
        this.j0 = tx4Var2;
    }

    @Override // defpackage.jt4
    public final void a() {
        it4 it4Var;
        zt4 zt4Var;
        String str;
        Bundle j;
        try {
            zt4Var = this.k0.c;
            str = this.k0.a;
            Bundle q = st4.q(this.f0, this.g0, this.h0, this.i0);
            j = st4.j();
            ((p) zt4Var.c()).o1(str, q, j, new a(this.k0, this.j0, (char[]) null));
        } catch (RemoteException e) {
            it4Var = st4.f;
            it4Var.c(e, "notifyChunkTransferred", new Object[0]);
        }
    }
}
