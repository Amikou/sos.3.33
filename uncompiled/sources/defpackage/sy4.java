package defpackage;

import java.io.File;
import java.util.Objects;

/* renamed from: sy4  reason: default package */
/* loaded from: classes2.dex */
public final class sy4 {
    public final File a;
    public final String b;

    public sy4() {
    }

    public sy4(File file, String str) {
        this();
        Objects.requireNonNull(file, "Null splitFile");
        this.a = file;
        Objects.requireNonNull(str, "Null splitId");
        this.b = str;
    }

    public File a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof sy4) {
            sy4 sy4Var = (sy4) obj;
            if (this.a.equals(sy4Var.a()) && this.b.equals(sy4Var.b())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    public String toString() {
        String valueOf = String.valueOf(this.a);
        String str = this.b;
        StringBuilder sb = new StringBuilder(valueOf.length() + 35 + str.length());
        sb.append("SplitFileInfo{splitFile=");
        sb.append(valueOf);
        sb.append(", splitId=");
        sb.append(str);
        sb.append("}");
        return sb.toString();
    }
}
