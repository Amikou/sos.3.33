package defpackage;

import com.facebook.common.references.a;

/* compiled from: InstrumentedMemoryCache.java */
/* renamed from: jr1  reason: default package */
/* loaded from: classes.dex */
public class jr1<K, V> implements l72<K, V> {
    public final l72<K, V> a;
    public final n72 b;

    public jr1(l72<K, V> l72Var, n72 n72Var) {
        this.a = l72Var;
        this.b = n72Var;
    }

    @Override // defpackage.l72
    public void b(K k) {
        this.a.b(k);
    }

    @Override // defpackage.l72
    public a<V> c(K k, a<V> aVar) {
        this.b.c(k);
        return this.a.c(k, aVar);
    }

    @Override // defpackage.l72
    public a<V> get(K k) {
        a<V> aVar = this.a.get(k);
        if (aVar == null) {
            this.b.b(k);
        } else {
            this.b.a(k);
        }
        return aVar;
    }
}
