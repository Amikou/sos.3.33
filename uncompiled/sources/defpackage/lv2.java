package defpackage;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Build;
import com.facebook.common.references.a;
import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.producers.ThumbnailProducer;
import com.facebook.imagepipeline.producers.c;
import com.facebook.imagepipeline.producers.n;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProducerSequenceFactory.java */
/* renamed from: lv2  reason: default package */
/* loaded from: classes.dex */
public class lv2 {
    public final ContentResolver a;
    public final hv2 b;
    public final n c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final h54 g;
    public final boolean h;
    public final boolean i;
    public final boolean j;
    public final bp1 k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public dv2<a<com.facebook.imagepipeline.image.a>> o;
    public dv2<zu0> p;
    public dv2<a<com.facebook.imagepipeline.image.a>> q;
    public dv2<a<com.facebook.imagepipeline.image.a>> r;
    public dv2<a<com.facebook.imagepipeline.image.a>> s;
    public dv2<a<com.facebook.imagepipeline.image.a>> t;
    public dv2<a<com.facebook.imagepipeline.image.a>> u;
    public dv2<a<com.facebook.imagepipeline.image.a>> v;
    public dv2<a<com.facebook.imagepipeline.image.a>> w;
    public dv2<a<com.facebook.imagepipeline.image.a>> x;
    public Map<dv2<a<com.facebook.imagepipeline.image.a>>, dv2<a<com.facebook.imagepipeline.image.a>>> y = new HashMap();
    public Map<dv2<a<com.facebook.imagepipeline.image.a>>, dv2<a<com.facebook.imagepipeline.image.a>>> z;

    public lv2(ContentResolver contentResolver, hv2 hv2Var, n nVar, boolean z, boolean z2, h54 h54Var, boolean z3, boolean z4, boolean z5, boolean z6, bp1 bp1Var, boolean z7, boolean z8, boolean z9) {
        this.a = contentResolver;
        this.b = hv2Var;
        this.c = nVar;
        this.d = z;
        this.e = z2;
        new HashMap();
        this.z = new HashMap();
        this.g = h54Var;
        this.h = z3;
        this.i = z4;
        this.f = z5;
        this.j = z6;
        this.k = bp1Var;
        this.l = z7;
        this.m = z8;
        this.n = z9;
    }

    public static String p(Uri uri) {
        String valueOf = String.valueOf(uri);
        if (valueOf.length() > 30) {
            return valueOf.substring(0, 30) + "...";
        }
        return valueOf;
    }

    public final dv2<a<com.facebook.imagepipeline.image.a>> a(ImageRequest imageRequest) {
        try {
            if (nc1.d()) {
                nc1.a("ProducerSequenceFactory#getBasicDecodedImageSequence");
            }
            xt2.g(imageRequest);
            Uri u = imageRequest.u();
            xt2.h(u, "Uri is null.");
            int v = imageRequest.v();
            if (v != 0) {
                switch (v) {
                    case 2:
                        dv2<a<com.facebook.imagepipeline.image.a>> l = l();
                        if (nc1.d()) {
                            nc1.b();
                        }
                        return l;
                    case 3:
                        dv2<a<com.facebook.imagepipeline.image.a>> j = j();
                        if (nc1.d()) {
                            nc1.b();
                        }
                        return j;
                    case 4:
                        if (imageRequest.h() && Build.VERSION.SDK_INT >= 29) {
                            dv2<a<com.facebook.imagepipeline.image.a>> i = i();
                            if (nc1.d()) {
                                nc1.b();
                            }
                            return i;
                        } else if (f72.c(this.a.getType(u))) {
                            dv2<a<com.facebook.imagepipeline.image.a>> l2 = l();
                            if (nc1.d()) {
                                nc1.b();
                            }
                            return l2;
                        } else {
                            dv2<a<com.facebook.imagepipeline.image.a>> h = h();
                            if (nc1.d()) {
                                nc1.b();
                            }
                            return h;
                        }
                    case 5:
                        dv2<a<com.facebook.imagepipeline.image.a>> g = g();
                        if (nc1.d()) {
                            nc1.b();
                        }
                        return g;
                    case 6:
                        dv2<a<com.facebook.imagepipeline.image.a>> k = k();
                        if (nc1.d()) {
                            nc1.b();
                        }
                        return k;
                    case 7:
                        dv2<a<com.facebook.imagepipeline.image.a>> d = d();
                        if (nc1.d()) {
                            nc1.b();
                        }
                        return d;
                    case 8:
                        return o();
                    default:
                        throw new IllegalArgumentException("Unsupported uri scheme! Uri is: " + p(u));
                }
            }
            dv2<a<com.facebook.imagepipeline.image.a>> m = m();
            if (nc1.d()) {
                nc1.b();
            }
            return m;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> b(dv2<a<com.facebook.imagepipeline.image.a>> dv2Var) {
        dv2<a<com.facebook.imagepipeline.image.a>> dv2Var2;
        dv2Var2 = this.z.get(dv2Var);
        if (dv2Var2 == null) {
            dv2Var2 = this.b.f(dv2Var);
            this.z.put(dv2Var, dv2Var2);
        }
        return dv2Var2;
    }

    public final synchronized dv2<zu0> c() {
        if (nc1.d()) {
            nc1.a("ProducerSequenceFactory#getCommonNetworkFetchToEncodedMemorySequence");
        }
        if (this.p == null) {
            if (nc1.d()) {
                nc1.a("ProducerSequenceFactory#getCommonNetworkFetchToEncodedMemorySequence:init");
            }
            f9 a = hv2.a((dv2) xt2.g(v(this.b.y(this.c))));
            this.p = a;
            this.p = this.b.D(a, this.d && !this.h, this.k);
            if (nc1.d()) {
                nc1.b();
            }
        }
        if (nc1.d()) {
            nc1.b();
        }
        return this.p;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> d() {
        if (this.v == null) {
            dv2<zu0> i = this.b.i();
            if (po4.a && (!this.e || po4.c == null)) {
                i = this.b.G(i);
            }
            this.v = r(this.b.D(hv2.a(i), true, this.k));
        }
        return this.v;
    }

    public dv2<a<com.facebook.imagepipeline.image.a>> e(ImageRequest imageRequest) {
        if (nc1.d()) {
            nc1.a("ProducerSequenceFactory#getDecodedImageProducerSequence");
        }
        dv2<a<com.facebook.imagepipeline.image.a>> a = a(imageRequest);
        if (imageRequest.k() != null) {
            a = n(a);
        }
        if (this.i) {
            a = b(a);
        }
        if (this.n && imageRequest.f() > 0) {
            a = f(a);
        }
        if (nc1.d()) {
            nc1.b();
        }
        return a;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> f(dv2<a<com.facebook.imagepipeline.image.a>> dv2Var) {
        return this.b.k(dv2Var);
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> g() {
        if (this.u == null) {
            this.u = s(this.b.q());
        }
        return this.u;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> h() {
        if (this.s == null) {
            this.s = t(this.b.r(), new s54[]{this.b.s(), this.b.t()});
        }
        return this.s;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> i() {
        if (this.w == null) {
            this.w = q(this.b.w());
        }
        return this.w;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> j() {
        if (this.q == null) {
            this.q = s(this.b.u());
        }
        return this.q;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> k() {
        if (this.t == null) {
            this.t = s(this.b.v());
        }
        return this.t;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> l() {
        if (this.r == null) {
            this.r = q(this.b.x());
        }
        return this.r;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> m() {
        if (nc1.d()) {
            nc1.a("ProducerSequenceFactory#getNetworkFetchSequence");
        }
        if (this.o == null) {
            if (nc1.d()) {
                nc1.a("ProducerSequenceFactory#getNetworkFetchSequence:init");
            }
            this.o = r(c());
            if (nc1.d()) {
                nc1.b();
            }
        }
        if (nc1.d()) {
            nc1.b();
        }
        return this.o;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> n(dv2<a<com.facebook.imagepipeline.image.a>> dv2Var) {
        dv2<a<com.facebook.imagepipeline.image.a>> dv2Var2;
        dv2Var2 = this.y.get(dv2Var);
        if (dv2Var2 == null) {
            dv2Var2 = this.b.A(this.b.B(dv2Var));
            this.y.put(dv2Var, dv2Var2);
        }
        return dv2Var2;
    }

    public final synchronized dv2<a<com.facebook.imagepipeline.image.a>> o() {
        if (this.x == null) {
            this.x = s(this.b.C());
        }
        return this.x;
    }

    public final dv2<a<com.facebook.imagepipeline.image.a>> q(dv2<a<com.facebook.imagepipeline.image.a>> dv2Var) {
        dv2<a<com.facebook.imagepipeline.image.a>> b = this.b.b(this.b.d(this.b.e(dv2Var)), this.g);
        if (!this.l && !this.m) {
            return this.b.c(b);
        }
        return this.b.g(this.b.c(b));
    }

    public final dv2<a<com.facebook.imagepipeline.image.a>> r(dv2<zu0> dv2Var) {
        if (nc1.d()) {
            nc1.a("ProducerSequenceFactory#newBitmapCacheGetToDecodeSequence");
        }
        dv2<a<com.facebook.imagepipeline.image.a>> q = q(this.b.j(dv2Var));
        if (nc1.d()) {
            nc1.b();
        }
        return q;
    }

    public final dv2<a<com.facebook.imagepipeline.image.a>> s(dv2<zu0> dv2Var) {
        return t(dv2Var, new s54[]{this.b.t()});
    }

    public final dv2<a<com.facebook.imagepipeline.image.a>> t(dv2<zu0> dv2Var, ThumbnailProducer<EncodedImage>[] thumbnailProducerArr) {
        return r(x(v(dv2Var), thumbnailProducerArr));
    }

    public final dv2<zu0> u(dv2<zu0> dv2Var) {
        dp0 m;
        if (nc1.d()) {
            nc1.a("ProducerSequenceFactory#newDiskCacheSequence");
        }
        if (this.f) {
            m = this.b.m(this.b.z(dv2Var));
        } else {
            m = this.b.m(dv2Var);
        }
        c l = this.b.l(m);
        if (nc1.d()) {
            nc1.b();
        }
        return l;
    }

    public final dv2<zu0> v(dv2<zu0> dv2Var) {
        if (po4.a && (!this.e || po4.c == null)) {
            dv2Var = this.b.G(dv2Var);
        }
        if (this.j) {
            dv2Var = u(dv2Var);
        }
        bv0 o = this.b.o(dv2Var);
        if (this.m) {
            return this.b.n(this.b.p(o));
        }
        return this.b.n(o);
    }

    public final dv2<zu0> w(ThumbnailProducer<EncodedImage>[] thumbnailProducerArr) {
        return this.b.D(this.b.F(thumbnailProducerArr), true, this.k);
    }

    public final dv2<zu0> x(dv2<zu0> dv2Var, ThumbnailProducer<EncodedImage>[] thumbnailProducerArr) {
        return hv2.h(w(thumbnailProducerArr), this.b.E(this.b.D(hv2.a(dv2Var), true, this.k)));
    }
}
