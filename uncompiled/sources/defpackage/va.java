package defpackage;

import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: va  reason: default package */
/* loaded from: classes2.dex */
public class va extends h {
    public i a;
    public c4 f0;

    public va(h4 h4Var) {
        if (h4Var.size() >= 1 && h4Var.size() <= 2) {
            this.a = i.G(h4Var.D(0));
            this.f0 = h4Var.size() == 2 ? h4Var.D(1) : null;
            return;
        }
        throw new IllegalArgumentException("Bad sequence size: " + h4Var.size());
    }

    public va(i iVar) {
        this.a = iVar;
    }

    public va(i iVar, c4 c4Var) {
        this.a = iVar;
        this.f0 = c4Var;
    }

    public static va p(Object obj) {
        if (obj instanceof va) {
            return (va) obj;
        }
        if (obj != null) {
            return new va(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(this.a);
        c4 c4Var = this.f0;
        if (c4Var != null) {
            d4Var.a(c4Var);
        }
        return new n0(d4Var);
    }

    public i o() {
        return this.a;
    }

    public c4 q() {
        return this.f0;
    }
}
