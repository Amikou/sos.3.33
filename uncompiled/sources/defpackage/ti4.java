package defpackage;

import android.view.View;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: ViewHolderSwapBinding.java */
/* renamed from: ti4  reason: default package */
/* loaded from: classes2.dex */
public final class ti4 {
    public final MaterialCardView a;
    public final ShapeableImageView b;
    public final MaterialTextView c;
    public final MaterialTextView d;

    public ti4(MaterialCardView materialCardView, ShapeableImageView shapeableImageView, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5) {
        this.a = materialCardView;
        this.b = shapeableImageView;
        this.c = materialTextView;
        this.d = materialTextView4;
    }

    public static ti4 a(View view) {
        int i = R.id.imgTokenLogo;
        ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgTokenLogo);
        if (shapeableImageView != null) {
            i = R.id.txtTokenBal;
            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtTokenBal);
            if (materialTextView != null) {
                i = R.id.txtTokenBalance;
                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtTokenBalance);
                if (materialTextView2 != null) {
                    i = R.id.txtTokenBalanceTitle;
                    MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtTokenBalanceTitle);
                    if (materialTextView3 != null) {
                        i = R.id.txtTokenName;
                        MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtTokenName);
                        if (materialTextView4 != null) {
                            i = R.id.txtTokenPrice;
                            MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txtTokenPrice);
                            if (materialTextView5 != null) {
                                return new ti4((MaterialCardView) view, shapeableImageView, materialTextView, materialTextView2, materialTextView3, materialTextView4, materialTextView5);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public MaterialCardView b() {
        return this.a;
    }
}
