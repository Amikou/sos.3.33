package defpackage;

import com.google.crypto.tink.n;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;

/* compiled from: PrfMac.java */
/* renamed from: ru2  reason: default package */
/* loaded from: classes2.dex */
public class ru2 implements n {
    public final ou2 a;
    public final int b;

    public ru2(ou2 ou2Var, int i) throws GeneralSecurityException {
        this.a = ou2Var;
        this.b = i;
        if (i >= 10) {
            ou2Var.a(new byte[0], i);
            return;
        }
        throw new InvalidAlgorithmParameterException("tag size too small, need at least 10 bytes");
    }

    @Override // com.google.crypto.tink.n
    public void a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        if (!at.b(b(bArr2), bArr)) {
            throw new GeneralSecurityException("invalid MAC");
        }
    }

    @Override // com.google.crypto.tink.n
    public byte[] b(byte[] bArr) throws GeneralSecurityException {
        return this.a.a(bArr, this.b);
    }
}
