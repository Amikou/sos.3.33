package defpackage;

import io.reactivex.internal.schedulers.RxThreadFactory;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: IoScheduler.java */
/* renamed from: ps1  reason: default package */
/* loaded from: classes2.dex */
public final class ps1 extends bd3 {
    public static final RxThreadFactory c;
    public static final RxThreadFactory d;
    public static final TimeUnit e = TimeUnit.SECONDS;
    public static final b f;
    public static final a g;
    public final ThreadFactory a;
    public final AtomicReference<a> b;

    /* compiled from: IoScheduler.java */
    /* renamed from: ps1$a */
    /* loaded from: classes2.dex */
    public static final class a implements Runnable {
        public final long a;
        public final ConcurrentLinkedQueue<b> f0;
        public final n40 g0;
        public final ScheduledExecutorService h0;
        public final Future<?> i0;

        public a(long j, TimeUnit timeUnit, ThreadFactory threadFactory) {
            ScheduledFuture<?> scheduledFuture;
            long nanos = timeUnit != null ? timeUnit.toNanos(j) : 0L;
            this.a = nanos;
            this.f0 = new ConcurrentLinkedQueue<>();
            this.g0 = new n40();
            ScheduledExecutorService scheduledExecutorService = null;
            if (timeUnit != null) {
                scheduledExecutorService = Executors.newScheduledThreadPool(1, ps1.d);
                scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(this, nanos, nanos, TimeUnit.NANOSECONDS);
            } else {
                scheduledFuture = null;
            }
            this.h0 = scheduledExecutorService;
            this.i0 = scheduledFuture;
        }

        public void a() {
            if (this.f0.isEmpty()) {
                return;
            }
            long b = b();
            Iterator<b> it = this.f0.iterator();
            while (it.hasNext()) {
                b next = it.next();
                if (next.b() > b) {
                    return;
                }
                if (this.f0.remove(next)) {
                    this.g0.d(next);
                }
            }
        }

        public long b() {
            return System.nanoTime();
        }

        public void c() {
            this.g0.a();
            Future<?> future = this.i0;
            if (future != null) {
                future.cancel(true);
            }
            ScheduledExecutorService scheduledExecutorService = this.h0;
            if (scheduledExecutorService != null) {
                scheduledExecutorService.shutdownNow();
            }
        }

        @Override // java.lang.Runnable
        public void run() {
            a();
        }
    }

    /* compiled from: IoScheduler.java */
    /* renamed from: ps1$b */
    /* loaded from: classes2.dex */
    public static final class b extends nf2 {
        public long c;

        public b(ThreadFactory threadFactory) {
            super(threadFactory);
            this.c = 0L;
        }

        public long b() {
            return this.c;
        }
    }

    static {
        b bVar = new b(new RxThreadFactory("RxCachedThreadSchedulerShutdown"));
        f = bVar;
        bVar.a();
        int max = Math.max(1, Math.min(10, Integer.getInteger("rx2.io-priority", 5).intValue()));
        RxThreadFactory rxThreadFactory = new RxThreadFactory("RxCachedThreadScheduler", max);
        c = rxThreadFactory;
        d = new RxThreadFactory("RxCachedWorkerPoolEvictor", max);
        a aVar = new a(0L, null, rxThreadFactory);
        g = aVar;
        aVar.c();
    }

    public ps1() {
        this(c);
    }

    public void a() {
        a aVar = new a(60L, e, this.a);
        if (this.b.compareAndSet(g, aVar)) {
            return;
        }
        aVar.c();
    }

    public ps1(ThreadFactory threadFactory) {
        this.a = threadFactory;
        this.b = new AtomicReference<>(g);
        a();
    }
}
