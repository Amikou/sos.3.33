package defpackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import androidx.biometric.d;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;

/* compiled from: UtilityMethods.java */
/* renamed from: pg4  reason: default package */
/* loaded from: classes2.dex */
public class pg4 {
    public static ProgressDialog a;

    public static boolean a(String str, Context context) {
        return context.getSharedPreferences("Safemoon", 0).getBoolean(str, false);
    }

    public static void b(Activity activity, Boolean bool) {
        ((MyApplicationClass) activity.getApplication()).f0 = bool.booleanValue();
    }

    public static void c() {
        ProgressDialog progressDialog = a;
        if (progressDialog == null || !progressDialog.isShowing()) {
            return;
        }
        a.dismiss();
    }

    public static boolean d(Context context) {
        int a2 = d.g(context).a(33023);
        return (a2 == 12 || a2 == 11) ? false : true;
    }

    public static void e(Activity activity) {
        if (activity == null) {
            return;
        }
        View findViewById = activity.findViewById(R.id.container);
        if (findViewById != null) {
            findViewById.requestFocus();
        }
        activity.getWindow().setSoftInputMode(3);
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService("input_method");
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }

    public static void f(Context context) {
        if (context instanceof Activity) {
            e((Activity) context);
        }
    }

    public static void g(EditText editText) {
        editText.requestFocus();
        ((InputMethodManager) editText.getContext().getSystemService("input_method")).showSoftInput(editText, 1);
    }
}
