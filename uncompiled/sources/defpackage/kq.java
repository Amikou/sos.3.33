package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapPoolAdapter.java */
/* renamed from: kq  reason: default package */
/* loaded from: classes.dex */
public class kq implements jq {
    @Override // defpackage.jq
    public void a(int i) {
    }

    @Override // defpackage.jq
    public void b() {
    }

    @Override // defpackage.jq
    public void c(Bitmap bitmap) {
        bitmap.recycle();
    }

    @Override // defpackage.jq
    public Bitmap d(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }

    @Override // defpackage.jq
    public Bitmap e(int i, int i2, Bitmap.Config config) {
        return d(i, i2, config);
    }
}
