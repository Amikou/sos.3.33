package defpackage;

import android.os.Bundle;

/* compiled from: CrashlyticsOriginAnalyticsEventLogger.java */
/* renamed from: p90  reason: default package */
/* loaded from: classes2.dex */
public class p90 implements qb {
    public final kb a;

    public p90(kb kbVar) {
        this.a = kbVar;
    }

    @Override // defpackage.qb
    public void a(String str, Bundle bundle) {
        this.a.b("clx", str, bundle);
    }
}
