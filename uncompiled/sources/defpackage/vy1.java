package defpackage;

import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1ParsingException;
import org.bouncycastle.asn1.f;

/* renamed from: vy1  reason: default package */
/* loaded from: classes2.dex */
public class vy1 implements Enumeration {
    public f a;
    public Object b = a();

    public vy1(byte[] bArr) {
        this.a = new f(bArr, true);
    }

    public final Object a() {
        try {
            return this.a.j();
        } catch (IOException e) {
            throw new ASN1ParsingException("malformed DER construction: " + e, e);
        }
    }

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return this.b != null;
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        Object obj = this.b;
        this.b = a();
        return obj;
    }
}
