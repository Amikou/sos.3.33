package defpackage;

import com.google.android.gms.internal.vision.t0;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: zw5  reason: default package */
/* loaded from: classes.dex */
public final class zw5 {
    public static final zw5 c = new zw5();
    public final ConcurrentMap<Class<?>, t0<?>> b = new ConcurrentHashMap();
    public final qx5 a = new dv5();

    public static zw5 c() {
        return c;
    }

    public final <T> t0<T> a(Class<T> cls) {
        vs5.f(cls, "messageType");
        t0<T> t0Var = (t0<T>) this.b.get(cls);
        if (t0Var == null) {
            t0<T> a = this.a.a(cls);
            vs5.f(cls, "messageType");
            vs5.f(a, "schema");
            t0<T> t0Var2 = (t0<T>) this.b.putIfAbsent(cls, a);
            return t0Var2 != null ? t0Var2 : a;
        }
        return t0Var;
    }

    public final <T> t0<T> b(T t) {
        return a(t.getClass());
    }
}
