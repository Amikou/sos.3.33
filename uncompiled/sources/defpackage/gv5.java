package defpackage;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: gv5  reason: default package */
/* loaded from: classes.dex */
public final class gv5 extends jv5 {
    public final AlarmManager d;
    public n55 e;
    public Integer f;

    public gv5(fw5 fw5Var) {
        super(fw5Var);
        this.d = (AlarmManager) this.a.m().getSystemService("alarm");
    }

    @Override // defpackage.jv5
    public final boolean h() {
        AlarmManager alarmManager = this.d;
        if (alarmManager != null) {
            alarmManager.cancel(p());
        }
        if (Build.VERSION.SDK_INT >= 24) {
            n();
            return false;
        }
        return false;
    }

    public final void j(long j) {
        g();
        this.a.b();
        Context m = this.a.m();
        if (!sw5.a0(m)) {
            this.a.w().u().a("Receiver not registered/enabled");
        }
        if (!sw5.D(m, false)) {
            this.a.w().u().a("Service not registered/enabled");
        }
        k();
        this.a.w().v().b("Scheduling upload, millis", Long.valueOf(j));
        long b = this.a.a().b() + j;
        this.a.z();
        if (j < Math.max(0L, qf5.x.b(null).longValue()) && !l().c()) {
            l().b(j);
        }
        this.a.b();
        if (Build.VERSION.SDK_INT < 24) {
            AlarmManager alarmManager = this.d;
            if (alarmManager != null) {
                this.a.z();
                alarmManager.setInexactRepeating(2, b, Math.max(qf5.s.b(null).longValue(), j), p());
                return;
            }
            return;
        }
        Context m2 = this.a.m();
        ComponentName componentName = new ComponentName(m2, "com.google.android.gms.measurement.AppMeasurementJobService");
        int o = o();
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("action", "com.google.android.gms.measurement.UPLOAD");
        l95.a(m2, new JobInfo.Builder(o, componentName).setMinimumLatency(j).setOverrideDeadline(j + j).setExtras(persistableBundle).build(), "com.google.android.gms", "UploadAlarm");
    }

    public final void k() {
        g();
        this.a.w().v().a("Unscheduling upload");
        AlarmManager alarmManager = this.d;
        if (alarmManager != null) {
            alarmManager.cancel(p());
        }
        l().d();
        if (Build.VERSION.SDK_INT >= 24) {
            n();
        }
    }

    public final n55 l() {
        if (this.e == null) {
            this.e = new ev5(this, this.b.r());
        }
        return this.e;
    }

    @TargetApi(24)
    public final void n() {
        JobScheduler jobScheduler = (JobScheduler) this.a.m().getSystemService("jobscheduler");
        if (jobScheduler != null) {
            jobScheduler.cancel(o());
        }
    }

    public final int o() {
        if (this.f == null) {
            String valueOf = String.valueOf(this.a.m().getPackageName());
            this.f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f.intValue();
    }

    public final PendingIntent p() {
        Context m = this.a.m();
        return i95.a(m, 0, new Intent().setClassName(m, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), i95.a);
    }
}
