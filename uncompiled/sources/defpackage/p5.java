package defpackage;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import org.java_websocket.WebSocket;
import org.java_websocket.b;

/* compiled from: AbstractWebSocket.java */
/* renamed from: p5  reason: default package */
/* loaded from: classes2.dex */
public abstract class p5 extends org.java_websocket.a {
    public int connectionLostTimeout = 60;
    public Timer connectionLostTimer;
    public TimerTask connectionLostTimerTask;
    public boolean reuseAddr;
    public boolean tcpNoDelay;

    /* compiled from: AbstractWebSocket.java */
    /* renamed from: p5$a */
    /* loaded from: classes2.dex */
    public class a extends TimerTask {
        public ArrayList<WebSocket> a = new ArrayList<>();

        public a() {
        }

        @Override // java.util.TimerTask, java.lang.Runnable
        public void run() {
            this.a.clear();
            this.a.addAll(p5.this.getConnections());
            long currentTimeMillis = System.currentTimeMillis() - (p5.this.connectionLostTimeout * 1500);
            Iterator<WebSocket> it = this.a.iterator();
            while (it.hasNext()) {
                WebSocket next = it.next();
                if (next instanceof b) {
                    b bVar = (b) next;
                    if (bVar.p() < currentTimeMillis) {
                        if (b.w0) {
                            PrintStream printStream = System.out;
                            printStream.println("Closing connection due to no pong received: " + next.toString());
                        }
                        bVar.e(1006, "The connection was closed because the other endpoint did not respond with a pong in time. For more information check: https://github.com/TooTallNate/Java-WebSocket/wiki/Lost-connection-detection");
                    } else if (bVar.u()) {
                        bVar.y();
                    } else if (b.w0) {
                        PrintStream printStream2 = System.out;
                        printStream2.println("Trying to ping a non open connection: " + next.toString());
                    }
                }
            }
            this.a.clear();
        }
    }

    public final void cancelConnectionLostTimer() {
        Timer timer = this.connectionLostTimer;
        if (timer != null) {
            timer.cancel();
            this.connectionLostTimer = null;
        }
        TimerTask timerTask = this.connectionLostTimerTask;
        if (timerTask != null) {
            timerTask.cancel();
            this.connectionLostTimerTask = null;
        }
    }

    public abstract Collection<WebSocket> getConnections();

    public boolean isReuseAddr() {
        return this.reuseAddr;
    }

    public boolean isTcpNoDelay() {
        return this.tcpNoDelay;
    }

    public final void restartConnectionLostTimer() {
        cancelConnectionLostTimer();
        this.connectionLostTimer = new Timer("WebSocketTimer");
        a aVar = new a();
        this.connectionLostTimerTask = aVar;
        Timer timer = this.connectionLostTimer;
        int i = this.connectionLostTimeout;
        timer.scheduleAtFixedRate(aVar, i * 1000, i * 1000);
    }

    public void setReuseAddr(boolean z) {
        this.reuseAddr = z;
    }

    public void setTcpNoDelay(boolean z) {
        this.tcpNoDelay = z;
    }

    public void startConnectionLostTimer() {
        if (this.connectionLostTimeout <= 0) {
            if (b.w0) {
                System.out.println("Connection lost timer deactivated");
                return;
            }
            return;
        }
        if (b.w0) {
            System.out.println("Connection lost timer started");
        }
        restartConnectionLostTimer();
    }

    public void stopConnectionLostTimer() {
        if (this.connectionLostTimer == null && this.connectionLostTimerTask == null) {
            return;
        }
        if (b.w0) {
            System.out.println("Connection lost timer stopped");
        }
        cancelConnectionLostTimer();
    }
}
