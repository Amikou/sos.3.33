package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentWalletConnectInterfaceBinding.java */
/* renamed from: sb1  reason: default package */
/* loaded from: classes2.dex */
public final class sb1 {
    public final CardView a;
    public final ShapeableImageView b;
    public final ShapeableImageView c;
    public final AppCompatTextView d;
    public final MaterialButton e;
    public final SwitchMaterial f;
    public final AppCompatTextView g;
    public final rp3 h;
    public final MaterialTextView i;
    public final MaterialTextView j;

    public sb1(ConstraintLayout constraintLayout, CardView cardView, MaterialCardView materialCardView, MaterialCardView materialCardView2, View view, ShapeableImageView shapeableImageView, AppCompatImageView appCompatImageView, ShapeableImageView shapeableImageView2, AppCompatTextView appCompatTextView, MaterialButton materialButton, SwitchMaterial switchMaterial, AppCompatTextView appCompatTextView2, rp3 rp3Var, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5) {
        this.a = cardView;
        this.b = shapeableImageView;
        this.c = shapeableImageView2;
        this.d = appCompatTextView;
        this.e = materialButton;
        this.f = switchMaterial;
        this.g = appCompatTextView2;
        this.h = rp3Var;
        this.i = materialTextView3;
        this.j = materialTextView4;
    }

    public static sb1 a(View view) {
        int i = R.id.ccChainSelected;
        CardView cardView = (CardView) ai4.a(view, R.id.ccChainSelected);
        if (cardView != null) {
            i = R.id.ccWrapperApproval;
            MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ccWrapperApproval);
            if (materialCardView != null) {
                i = R.id.ccWrapperConnectionSecurity;
                MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.ccWrapperConnectionSecurity);
                if (materialCardView2 != null) {
                    i = R.id.dividerTxtTitleSecurity;
                    View a = ai4.a(view, R.id.dividerTxtTitleSecurity);
                    if (a != null) {
                        i = R.id.imgChainSelected;
                        ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgChainSelected);
                        if (shapeableImageView != null) {
                            i = R.id.imgRightIndicator;
                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgRightIndicator);
                            if (appCompatImageView != null) {
                                i = R.id.imgThumb;
                                ShapeableImageView shapeableImageView2 = (ShapeableImageView) ai4.a(view, R.id.imgThumb);
                                if (shapeableImageView2 != null) {
                                    i = R.id.linkWcInterface;
                                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.linkWcInterface);
                                    if (appCompatTextView != null) {
                                        i = R.id.sessionApproval;
                                        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.sessionApproval);
                                        if (materialButton != null) {
                                            i = R.id.switchTwoLayerConfirmation;
                                            SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchTwoLayerConfirmation);
                                            if (switchMaterial != null) {
                                                i = R.id.titleWcInterface;
                                                AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.titleWcInterface);
                                                if (appCompatTextView2 != null) {
                                                    i = R.id.toolbarWrapper;
                                                    View a2 = ai4.a(view, R.id.toolbarWrapper);
                                                    if (a2 != null) {
                                                        rp3 a3 = rp3.a(a2);
                                                        i = R.id.txt_request_approval;
                                                        MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txt_request_approval);
                                                        if (materialTextView != null) {
                                                            i = R.id.txtTitleSecurity;
                                                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtTitleSecurity);
                                                            if (materialTextView2 != null) {
                                                                i = R.id.txtWalletAddress;
                                                                MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtWalletAddress);
                                                                if (materialTextView3 != null) {
                                                                    i = R.id.txtWalletName;
                                                                    MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtWalletName);
                                                                    if (materialTextView4 != null) {
                                                                        i = R.id.txt_your_wallet_balance;
                                                                        MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txt_your_wallet_balance);
                                                                        if (materialTextView5 != null) {
                                                                            return new sb1((ConstraintLayout) view, cardView, materialCardView, materialCardView2, a, shapeableImageView, appCompatImageView, shapeableImageView2, appCompatTextView, materialButton, switchMaterial, appCompatTextView2, a3, materialTextView, materialTextView2, materialTextView3, materialTextView4, materialTextView5);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
