package defpackage;

import android.graphics.drawable.Drawable;

/* compiled from: DrawableUtils.java */
/* renamed from: br0  reason: default package */
/* loaded from: classes.dex */
public class br0 {
    public static void a(Drawable drawable, Drawable drawable2) {
        if (drawable2 == null || drawable == null || drawable == drawable2) {
            return;
        }
        drawable.setBounds(drawable2.getBounds());
        drawable.setChangingConfigurations(drawable2.getChangingConfigurations());
        drawable.setLevel(drawable2.getLevel());
        drawable.setVisible(drawable2.isVisible(), false);
        drawable.setState(drawable2.getState());
    }

    public static int b(int i) {
        int i2 = i >>> 24;
        if (i2 == 255) {
            return -1;
        }
        return i2 == 0 ? -2 : -3;
    }

    public static int c(int i, int i2) {
        if (i2 == 255) {
            return i;
        }
        if (i2 == 0) {
            return i & 16777215;
        }
        return (i & 16777215) | ((((i >>> 24) * (i2 + (i2 >> 7))) >> 8) << 24);
    }

    public static void d(Drawable drawable, Drawable.Callback callback, wa4 wa4Var) {
        if (drawable != null) {
            drawable.setCallback(callback);
            if (drawable instanceof va4) {
                ((va4) drawable).j(wa4Var);
            }
        }
    }

    public static void e(Drawable drawable, xq0 xq0Var) {
        if (drawable == null || xq0Var == null) {
            return;
        }
        xq0Var.a(drawable);
    }
}
