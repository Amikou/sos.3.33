package defpackage;

import androidx.media3.common.j;
import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.drm.c;
import androidx.media3.exoplayer.source.k;
import androidx.media3.exoplayer.source.q;
import androidx.media3.exoplayer.source.r;
import androidx.media3.exoplayer.source.s;
import androidx.media3.exoplayer.upstream.Loader;
import defpackage.sy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: ChunkSampleStream.java */
/* renamed from: ry  reason: default package */
/* loaded from: classes.dex */
public class ry<T extends sy> implements r, s, Loader.b<my>, Loader.f {
    public boolean A0;
    public final int a;
    public final int[] f0;
    public final j[] g0;
    public final boolean[] h0;
    public final T i0;
    public final s.a<ry<T>> j0;
    public final k.a k0;
    public final androidx.media3.exoplayer.upstream.b l0;
    public final Loader m0;
    public final oy n0;
    public final ArrayList<ln> o0;
    public final List<ln> p0;
    public final q q0;
    public final q[] r0;
    public final nn s0;
    public my t0;
    public j u0;
    public b<T> v0;
    public long w0;
    public long x0;
    public int y0;
    public ln z0;

    /* compiled from: ChunkSampleStream.java */
    /* renamed from: ry$a */
    /* loaded from: classes.dex */
    public final class a implements r {
        public final ry<T> a;
        public final q f0;
        public final int g0;
        public boolean h0;

        public a(ry<T> ryVar, q qVar, int i) {
            this.a = ryVar;
            this.f0 = qVar;
            this.g0 = i;
        }

        public final void a() {
            if (this.h0) {
                return;
            }
            ry.this.k0.i(ry.this.f0[this.g0], ry.this.g0[this.g0], 0, null, ry.this.x0);
            this.h0 = true;
        }

        @Override // androidx.media3.exoplayer.source.r
        public void b() {
        }

        public void c() {
            ii.g(ry.this.h0[this.g0]);
            ry.this.h0[this.g0] = false;
        }

        @Override // androidx.media3.exoplayer.source.r
        public boolean f() {
            return !ry.this.I() && this.f0.K(ry.this.A0);
        }

        @Override // androidx.media3.exoplayer.source.r
        public int m(long j) {
            if (ry.this.I()) {
                return 0;
            }
            int E = this.f0.E(j, ry.this.A0);
            if (ry.this.z0 != null) {
                E = Math.min(E, ry.this.z0.h(this.g0 + 1) - this.f0.C());
            }
            this.f0.d0(E);
            if (E > 0) {
                a();
            }
            return E;
        }

        @Override // androidx.media3.exoplayer.source.r
        public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
            if (ry.this.I()) {
                return -3;
            }
            if (ry.this.z0 == null || ry.this.z0.h(this.g0 + 1) > this.f0.C()) {
                a();
                return this.f0.R(y81Var, decoderInputBuffer, i, ry.this.A0);
            }
            return -3;
        }
    }

    /* compiled from: ChunkSampleStream.java */
    /* renamed from: ry$b */
    /* loaded from: classes.dex */
    public interface b<T extends sy> {
        void b(ry<T> ryVar);
    }

    public ry(int i, int[] iArr, j[] jVarArr, T t, s.a<ry<T>> aVar, gb gbVar, long j, c cVar, b.a aVar2, androidx.media3.exoplayer.upstream.b bVar, k.a aVar3) {
        this.a = i;
        int i2 = 0;
        iArr = iArr == null ? new int[0] : iArr;
        this.f0 = iArr;
        this.g0 = jVarArr == null ? new j[0] : jVarArr;
        this.i0 = t;
        this.j0 = aVar;
        this.k0 = aVar3;
        this.l0 = bVar;
        this.m0 = new Loader("ChunkSampleStream");
        this.n0 = new oy();
        ArrayList<ln> arrayList = new ArrayList<>();
        this.o0 = arrayList;
        this.p0 = Collections.unmodifiableList(arrayList);
        int length = iArr.length;
        this.r0 = new q[length];
        this.h0 = new boolean[length];
        int i3 = length + 1;
        int[] iArr2 = new int[i3];
        q[] qVarArr = new q[i3];
        q k = q.k(gbVar, cVar, aVar2);
        this.q0 = k;
        iArr2[0] = i;
        qVarArr[0] = k;
        while (i2 < length) {
            q l = q.l(gbVar);
            this.r0[i2] = l;
            int i4 = i2 + 1;
            qVarArr[i4] = l;
            iArr2[i4] = this.f0[i2];
            i2 = i4;
        }
        this.s0 = new nn(iArr2, qVarArr);
        this.w0 = j;
        this.x0 = j;
    }

    public final void B(int i) {
        int min = Math.min(O(i, 0), this.y0);
        if (min > 0) {
            androidx.media3.common.util.b.I0(this.o0, 0, min);
            this.y0 -= min;
        }
    }

    public final void C(int i) {
        ii.g(!this.m0.j());
        int size = this.o0.size();
        while (true) {
            if (i >= size) {
                i = -1;
                break;
            } else if (!G(i)) {
                break;
            } else {
                i++;
            }
        }
        if (i == -1) {
            return;
        }
        long j = F().h;
        ln D = D(i);
        if (this.o0.isEmpty()) {
            this.w0 = this.x0;
        }
        this.A0 = false;
        this.k0.D(this.a, D.g, j);
    }

    public final ln D(int i) {
        ln lnVar = this.o0.get(i);
        ArrayList<ln> arrayList = this.o0;
        androidx.media3.common.util.b.I0(arrayList, i, arrayList.size());
        this.y0 = Math.max(this.y0, this.o0.size());
        int i2 = 0;
        this.q0.u(lnVar.h(0));
        while (true) {
            q[] qVarArr = this.r0;
            if (i2 >= qVarArr.length) {
                return lnVar;
            }
            q qVar = qVarArr[i2];
            i2++;
            qVar.u(lnVar.h(i2));
        }
    }

    public T E() {
        return this.i0;
    }

    public final ln F() {
        ArrayList<ln> arrayList = this.o0;
        return arrayList.get(arrayList.size() - 1);
    }

    public final boolean G(int i) {
        int C;
        ln lnVar = this.o0.get(i);
        if (this.q0.C() > lnVar.h(0)) {
            return true;
        }
        int i2 = 0;
        do {
            q[] qVarArr = this.r0;
            if (i2 >= qVarArr.length) {
                return false;
            }
            C = qVarArr[i2].C();
            i2++;
        } while (C <= lnVar.h(i2));
        return true;
    }

    public final boolean H(my myVar) {
        return myVar instanceof ln;
    }

    public boolean I() {
        return this.w0 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final void J() {
        int O = O(this.q0.C(), this.y0 - 1);
        while (true) {
            int i = this.y0;
            if (i > O) {
                return;
            }
            this.y0 = i + 1;
            K(i);
        }
    }

    public final void K(int i) {
        ln lnVar = this.o0.get(i);
        j jVar = lnVar.d;
        if (!jVar.equals(this.u0)) {
            this.k0.i(this.a, jVar, lnVar.e, lnVar.f, lnVar.g);
        }
        this.u0 = jVar;
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: L */
    public void u(my myVar, long j, long j2, boolean z) {
        this.t0 = null;
        this.z0 = null;
        u02 u02Var = new u02(myVar.a, myVar.b, myVar.e(), myVar.d(), j, j2, myVar.b());
        this.l0.b(myVar.a);
        this.k0.r(u02Var, myVar.c, this.a, myVar.d, myVar.e, myVar.f, myVar.g, myVar.h);
        if (z) {
            return;
        }
        if (I()) {
            Q();
        } else if (H(myVar)) {
            D(this.o0.size() - 1);
            if (this.o0.isEmpty()) {
                this.w0 = this.x0;
            }
        }
        this.j0.i(this);
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: M */
    public void s(my myVar, long j, long j2) {
        this.t0 = null;
        this.i0.d(myVar);
        u02 u02Var = new u02(myVar.a, myVar.b, myVar.e(), myVar.d(), j, j2, myVar.b());
        this.l0.b(myVar.a);
        this.k0.u(u02Var, myVar.c, this.a, myVar.d, myVar.e, myVar.f, myVar.g, myVar.h);
        this.j0.i(this);
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00f1  */
    @Override // androidx.media3.exoplayer.upstream.Loader.b
    /* renamed from: N */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public androidx.media3.exoplayer.upstream.Loader.c j(defpackage.my r31, long r32, long r34, java.io.IOException r36, int r37) {
        /*
            Method dump skipped, instructions count: 257
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ry.j(my, long, long, java.io.IOException, int):androidx.media3.exoplayer.upstream.Loader$c");
    }

    public final int O(int i, int i2) {
        do {
            i2++;
            if (i2 >= this.o0.size()) {
                return this.o0.size() - 1;
            }
        } while (this.o0.get(i2).h(0) <= i);
        return i2 - 1;
    }

    public void P(b<T> bVar) {
        this.v0 = bVar;
        this.q0.Q();
        for (q qVar : this.r0) {
            qVar.Q();
        }
        this.m0.m(this);
    }

    public final void Q() {
        this.q0.U();
        for (q qVar : this.r0) {
            qVar.U();
        }
    }

    public void R(long j) {
        boolean Y;
        this.x0 = j;
        if (I()) {
            this.w0 = j;
            return;
        }
        ln lnVar = null;
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i2 >= this.o0.size()) {
                break;
            }
            ln lnVar2 = this.o0.get(i2);
            int i3 = (lnVar2.g > j ? 1 : (lnVar2.g == j ? 0 : -1));
            if (i3 == 0 && lnVar2.k == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                lnVar = lnVar2;
                break;
            } else if (i3 > 0) {
                break;
            } else {
                i2++;
            }
        }
        if (lnVar != null) {
            Y = this.q0.X(lnVar.h(0));
        } else {
            Y = this.q0.Y(j, j < a());
        }
        if (Y) {
            this.y0 = O(this.q0.C(), 0);
            q[] qVarArr = this.r0;
            int length = qVarArr.length;
            while (i < length) {
                qVarArr[i].Y(j, true);
                i++;
            }
            return;
        }
        this.w0 = j;
        this.A0 = false;
        this.o0.clear();
        this.y0 = 0;
        if (this.m0.j()) {
            this.q0.r();
            q[] qVarArr2 = this.r0;
            int length2 = qVarArr2.length;
            while (i < length2) {
                qVarArr2[i].r();
                i++;
            }
            this.m0.f();
            return;
        }
        this.m0.g();
        Q();
    }

    public ry<T>.a S(long j, int i) {
        for (int i2 = 0; i2 < this.r0.length; i2++) {
            if (this.f0[i2] == i) {
                ii.g(!this.h0[i2]);
                this.h0[i2] = true;
                this.r0[i2].Y(j, true);
                return new a(this, this.r0[i2], i2);
            }
        }
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.source.s
    public long a() {
        if (I()) {
            return this.w0;
        }
        if (this.A0) {
            return Long.MIN_VALUE;
        }
        return F().h;
    }

    @Override // androidx.media3.exoplayer.source.r
    public void b() throws IOException {
        this.m0.b();
        this.q0.N();
        if (this.m0.j()) {
            return;
        }
        this.i0.b();
    }

    public long c(long j, xi3 xi3Var) {
        return this.i0.c(j, xi3Var);
    }

    @Override // androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        List<ln> list;
        long j2;
        if (this.A0 || this.m0.j() || this.m0.i()) {
            return false;
        }
        boolean I = I();
        if (I) {
            list = Collections.emptyList();
            j2 = this.w0;
        } else {
            list = this.p0;
            j2 = F().h;
        }
        this.i0.f(j, j2, list, this.n0);
        oy oyVar = this.n0;
        boolean z = oyVar.b;
        my myVar = oyVar.a;
        oyVar.a();
        if (z) {
            this.w0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            this.A0 = true;
            return true;
        } else if (myVar == null) {
            return false;
        } else {
            this.t0 = myVar;
            if (H(myVar)) {
                ln lnVar = (ln) myVar;
                if (I) {
                    long j3 = lnVar.g;
                    long j4 = this.w0;
                    if (j3 != j4) {
                        this.q0.a0(j4);
                        for (q qVar : this.r0) {
                            qVar.a0(this.w0);
                        }
                    }
                    this.w0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                }
                lnVar.j(this.s0);
                this.o0.add(lnVar);
            } else if (myVar instanceof pq1) {
                ((pq1) myVar).f(this.s0);
            }
            this.k0.A(new u02(myVar.a, myVar.b, this.m0.n(myVar, this, this.l0.c(myVar.c))), myVar.c, this.a, myVar.d, myVar.e, myVar.f, myVar.g, myVar.h);
            return true;
        }
    }

    @Override // androidx.media3.exoplayer.source.s
    public boolean e() {
        return this.m0.j();
    }

    @Override // androidx.media3.exoplayer.source.r
    public boolean f() {
        return !I() && this.q0.K(this.A0);
    }

    @Override // androidx.media3.exoplayer.source.s
    public long g() {
        if (this.A0) {
            return Long.MIN_VALUE;
        }
        if (I()) {
            return this.w0;
        }
        long j = this.x0;
        ln F = F();
        if (!F.g()) {
            if (this.o0.size() > 1) {
                ArrayList<ln> arrayList = this.o0;
                F = arrayList.get(arrayList.size() - 2);
            } else {
                F = null;
            }
        }
        if (F != null) {
            j = Math.max(j, F.h);
        }
        return Math.max(j, this.q0.z());
    }

    @Override // androidx.media3.exoplayer.source.s
    public void h(long j) {
        if (this.m0.i() || I()) {
            return;
        }
        if (this.m0.j()) {
            my myVar = (my) ii.e(this.t0);
            if (!(H(myVar) && G(this.o0.size() - 1)) && this.i0.e(j, myVar, this.p0)) {
                this.m0.f();
                if (H(myVar)) {
                    this.z0 = (ln) myVar;
                    return;
                }
                return;
            }
            return;
        }
        int j2 = this.i0.j(j, this.p0);
        if (j2 < this.o0.size()) {
            C(j2);
        }
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.f
    public void i() {
        this.q0.S();
        for (q qVar : this.r0) {
            qVar.S();
        }
        this.i0.a();
        b<T> bVar = this.v0;
        if (bVar != null) {
            bVar.b(this);
        }
    }

    @Override // androidx.media3.exoplayer.source.r
    public int m(long j) {
        if (I()) {
            return 0;
        }
        int E = this.q0.E(j, this.A0);
        ln lnVar = this.z0;
        if (lnVar != null) {
            E = Math.min(E, lnVar.h(0) - this.q0.C());
        }
        this.q0.d0(E);
        J();
        return E;
    }

    @Override // androidx.media3.exoplayer.source.r
    public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
        if (I()) {
            return -3;
        }
        ln lnVar = this.z0;
        if (lnVar == null || lnVar.h(0) > this.q0.C()) {
            J();
            return this.q0.R(y81Var, decoderInputBuffer, i, this.A0);
        }
        return -3;
    }

    public void t(long j, boolean z) {
        if (I()) {
            return;
        }
        int x = this.q0.x();
        this.q0.q(j, z, true);
        int x2 = this.q0.x();
        if (x2 > x) {
            long y = this.q0.y();
            int i = 0;
            while (true) {
                q[] qVarArr = this.r0;
                if (i >= qVarArr.length) {
                    break;
                }
                qVarArr[i].q(y, z, this.h0[i]);
                i++;
            }
        }
        B(x2);
    }
}
