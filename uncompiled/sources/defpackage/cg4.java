package defpackage;

import com.onesignal.n1;

/* compiled from: UserStateSMS.java */
/* renamed from: cg4  reason: default package */
/* loaded from: classes2.dex */
public class cg4 extends n1 {
    public cg4(String str, boolean z) {
        super("sms" + str, z);
    }

    @Override // com.onesignal.n1
    public void a() {
    }

    @Override // com.onesignal.n1
    public n1 p(String str) {
        return new cg4(str, false);
    }
}
