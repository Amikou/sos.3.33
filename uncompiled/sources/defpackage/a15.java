package defpackage;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.TextUtils;
import java.util.Locale;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: a15  reason: default package */
/* loaded from: classes.dex */
public final class a15 {
    public static final vo3<String, String> a = new vo3<>();
    public static Locale b;

    public static String a(Context context) {
        return context.getResources().getString(p13.common_google_play_services_notification_channel_name);
    }

    public static String b(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case 1:
                return resources.getString(p13.common_google_play_services_install_title);
            case 2:
                return resources.getString(p13.common_google_play_services_update_title);
            case 3:
                return resources.getString(p13.common_google_play_services_enable_title);
            case 4:
            case 6:
            case 18:
                return null;
            case 5:
                return c(context, "common_google_play_services_invalid_account_title");
            case 7:
                return c(context, "common_google_play_services_network_error_title");
            case 8:
            case 9:
            case 10:
            case 11:
            case 16:
                return null;
            case 12:
            case 13:
            case 14:
            case 15:
            case 19:
            default:
                StringBuilder sb = new StringBuilder(33);
                sb.append("Unexpected error code ");
                sb.append(i);
                return null;
            case 17:
                return c(context, "common_google_play_services_sign_in_failed_title");
            case 20:
                return c(context, "common_google_play_services_restricted_profile_title");
        }
    }

    public static String c(Context context, String str) {
        vo3<String, String> vo3Var = a;
        synchronized (vo3Var) {
            Locale c = y40.a(context.getResources().getConfiguration()).c(0);
            if (!c.equals(b)) {
                vo3Var.clear();
                b = c;
            }
            String str2 = vo3Var.get(str);
            if (str2 != null) {
                return str2;
            }
            Resources e = ph1.e(context);
            if (e == null) {
                return null;
            }
            int identifier = e.getIdentifier(str, Utf8String.TYPE_NAME, "com.google.android.gms");
            if (identifier == 0) {
                String valueOf = String.valueOf(str);
                if (valueOf.length() != 0) {
                    "Missing resource: ".concat(valueOf);
                }
                return null;
            }
            String string = e.getString(identifier);
            if (TextUtils.isEmpty(string)) {
                String valueOf2 = String.valueOf(str);
                if (valueOf2.length() != 0) {
                    "Got empty resource: ".concat(valueOf2);
                }
                return null;
            }
            vo3Var.put(str, string);
            return string;
        }
    }

    public static String d(Context context, String str, String str2) {
        Resources resources = context.getResources();
        String c = c(context, str);
        if (c == null) {
            c = resources.getString(q13.common_google_play_services_unknown_issue);
        }
        return String.format(resources.getConfiguration().locale, c, str2);
    }

    public static String e(Context context) {
        String packageName = context.getPackageName();
        try {
            return kr4.a(context).d(packageName).toString();
        } catch (PackageManager.NameNotFoundException | NullPointerException unused) {
            String str = context.getApplicationInfo().name;
            return TextUtils.isEmpty(str) ? packageName : str;
        }
    }

    public static String f(Context context, int i) {
        String b2;
        if (i == 6) {
            b2 = c(context, "common_google_play_services_resolution_required_title");
        } else {
            b2 = b(context, i);
        }
        return b2 == null ? context.getResources().getString(p13.common_google_play_services_notification_ticker) : b2;
    }

    public static String g(Context context, int i) {
        Resources resources = context.getResources();
        String e = e(context);
        if (i != 1) {
            if (i == 2) {
                return vm0.f(context) ? resources.getString(p13.common_google_play_services_wear_update_text) : resources.getString(p13.common_google_play_services_update_text, e);
            } else if (i != 3) {
                if (i != 5) {
                    if (i != 7) {
                        if (i != 9) {
                            if (i != 20) {
                                switch (i) {
                                    case 16:
                                        return d(context, "common_google_play_services_api_unavailable_text", e);
                                    case 17:
                                        return d(context, "common_google_play_services_sign_in_failed_text", e);
                                    case 18:
                                        return resources.getString(p13.common_google_play_services_updating_text, e);
                                    default:
                                        return resources.getString(q13.common_google_play_services_unknown_issue, e);
                                }
                            }
                            return d(context, "common_google_play_services_restricted_profile_text", e);
                        }
                        return resources.getString(p13.common_google_play_services_unsupported_text, e);
                    }
                    return d(context, "common_google_play_services_network_error_text", e);
                }
                return d(context, "common_google_play_services_invalid_account_text", e);
            } else {
                return resources.getString(p13.common_google_play_services_enable_text, e);
            }
        }
        return resources.getString(p13.common_google_play_services_install_text, e);
    }

    public static String h(Context context, int i) {
        if (i != 6 && i != 19) {
            return g(context, i);
        }
        return d(context, "common_google_play_services_resolution_required_text", e(context));
    }

    public static String i(Context context, int i) {
        Resources resources = context.getResources();
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    return resources.getString(17039370);
                }
                return resources.getString(p13.common_google_play_services_enable_button);
            }
            return resources.getString(p13.common_google_play_services_update_button);
        }
        return resources.getString(p13.common_google_play_services_install_button);
    }
}
