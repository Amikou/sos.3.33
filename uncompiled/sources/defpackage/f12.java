package defpackage;

import android.os.LocaleList;
import java.util.Locale;

/* compiled from: LocaleListPlatformWrapper.java */
/* renamed from: f12  reason: default package */
/* loaded from: classes.dex */
public final class f12 implements e12 {
    public final LocaleList a;

    public f12(LocaleList localeList) {
        this.a = localeList;
    }

    @Override // defpackage.e12
    public Object a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        return this.a.equals(((e12) obj).a());
    }

    @Override // defpackage.e12
    public Locale get(int i) {
        return this.a.get(i);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.a.toString();
    }
}
