package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;

/* compiled from: GithubIdenticonDrawable.kt */
/* renamed from: gg1  reason: default package */
/* loaded from: classes2.dex */
public final class gg1 extends on1 {
    public static final a h = new a(null);
    public static int[] i = new int[60];
    public final Paint g;

    /* compiled from: GithubIdenticonDrawable.kt */
    /* renamed from: gg1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = i2 + 1;
            int i5 = 0;
            while (true) {
                int i6 = i5 + 1;
                int i7 = 0;
                while (true) {
                    int i8 = i7 + 1;
                    if (i2 != i5 || i5 != i7) {
                        i[i3] = ((((i2 * 25) + 150) * 65536) - 16777216) + (((i5 * 25) + 150) * 256) + 150 + (i7 * 25);
                        i3++;
                    }
                    if (i8 >= 4) {
                        break;
                    }
                    i7 = i8;
                }
                if (i6 >= 4) {
                    break;
                }
                i5 = i6;
            }
            if (i4 >= 4) {
                return;
            }
            i2 = i4;
        }
    }

    public gg1(int i2, int i3, int i4) {
        super(i2, i3, i4, 5);
        Paint paint = new Paint(1);
        paint.setStyle(Paint.Style.FILL);
        te4 te4Var = te4.a;
        this.g = paint;
        c();
    }

    @Override // defpackage.on1
    public void a(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        fs1.f(canvas, "canvas");
        this.g.setColor(f());
        int width = (int) (canvas.getWidth() / 5.0f);
        int height = (int) (canvas.getHeight() / 5.0f);
        canvas.drawColor(-1);
        int i6 = 0;
        int i7 = 0;
        while (true) {
            int i8 = i7 + 1;
            int i9 = i7 * width;
            int i10 = i9 + width;
            int i11 = (4 - i7) * width;
            int i12 = i11 + width;
            int i13 = 0;
            while (true) {
                int i14 = i13 + 1;
                i2 = i6 + 1;
                if ((b() >> i6) % 2 == 0) {
                    float f = i13 * height;
                    float f2 = f + height;
                    i3 = 2;
                    i4 = i14;
                    int i15 = i12;
                    canvas.drawRect(i9, f, i10, f2, this.g);
                    if (i7 != 2) {
                        i5 = i15;
                        canvas.drawRect(i11, f, i5, f2, this.g);
                    } else {
                        i5 = i15;
                    }
                } else {
                    i3 = 2;
                    i4 = i14;
                    i5 = i12;
                }
                i13 = i4;
                if (i13 > 4) {
                    break;
                }
                i12 = i5;
                i6 = i2;
            }
            if (i8 > i3) {
                return;
            }
            i7 = i8;
            i6 = i2;
        }
    }

    public final int f() {
        return i[Math.abs((b() >> 15) % i.length)];
    }
}
