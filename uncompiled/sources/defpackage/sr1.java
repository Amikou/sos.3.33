package defpackage;

/* compiled from: Ranges.kt */
/* renamed from: sr1  reason: default package */
/* loaded from: classes2.dex */
public final class sr1 extends qr1 implements f00<Integer> {
    public static final a j0 = new a(null);
    public static final sr1 i0 = new sr1(1, 0);

    /* compiled from: Ranges.kt */
    /* renamed from: sr1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final sr1 a() {
            return sr1.i0;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public sr1(int i, int i2) {
        super(i, i2, 1);
    }

    @Override // defpackage.qr1
    public boolean equals(Object obj) {
        if (obj instanceof sr1) {
            if (!isEmpty() || !((sr1) obj).isEmpty()) {
                sr1 sr1Var = (sr1) obj;
                if (m() != sr1Var.m() || n() != sr1Var.n()) {
                }
            }
            return true;
        }
        return false;
    }

    @Override // defpackage.qr1
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (m() * 31) + n();
    }

    @Override // defpackage.qr1, defpackage.f00
    public boolean isEmpty() {
        return m() > n();
    }

    public boolean s(int i) {
        return m() <= i && i <= n();
    }

    @Override // defpackage.f00
    /* renamed from: t */
    public Integer k() {
        return Integer.valueOf(n());
    }

    @Override // defpackage.qr1
    public String toString() {
        return m() + ".." + n();
    }

    @Override // defpackage.f00
    /* renamed from: w */
    public Integer i() {
        return Integer.valueOf(m());
    }
}
