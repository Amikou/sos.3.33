package defpackage;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import com.github.mikephil.charting.utils.Utils;
import defpackage.u21;
import java.util.Arrays;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.Coin;

/* compiled from: CoinPagingAdapter.kt */
/* renamed from: k00  reason: default package */
/* loaded from: classes2.dex */
public final class k00 extends PagingDataAdapter<Coin, b> {
    public final c d;
    public Context e;

    /* compiled from: CoinPagingAdapter.kt */
    /* renamed from: k00$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<Coin> {
        public static final a a = new a();

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(Coin coin, Coin coin2) {
            fs1.f(coin, "oldItem");
            fs1.f(coin2, "newItem");
            return fs1.b(coin.getId(), coin2.getId());
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(Coin coin, Coin coin2) {
            fs1.f(coin, "oldItem");
            fs1.f(coin2, "newItem");
            return fs1.b(coin.getId(), coin2.getId());
        }
    }

    /* compiled from: CoinPagingAdapter.kt */
    /* renamed from: k00$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final View a;
        public final TextView b;
        public final TextView c;
        public final TextView d;
        public final ImageView e;
        public final ImageView f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(k00 k00Var, View view) {
            super(view);
            fs1.f(k00Var, "this$0");
            fs1.f(view, "itemView");
            View findViewById = view.findViewById(R.id.cv_item_container);
            fs1.e(findViewById, "itemView.findViewById(R.id.cv_item_container)");
            this.a = findViewById;
            View findViewById2 = view.findViewById(R.id.name);
            fs1.e(findViewById2, "itemView.findViewById(R.id.name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.price);
            fs1.e(findViewById3, "itemView.findViewById(R.id.price)");
            this.c = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.percent);
            fs1.e(findViewById4, "itemView.findViewById(R.id.percent)");
            this.d = (TextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.IvPercent);
            fs1.e(findViewById5, "itemView.findViewById(R.id.IvPercent)");
            this.e = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(R.id.img);
            fs1.e(findViewById6, "itemView.findViewById(R.id.img)");
            this.f = (ImageView) findViewById6;
        }

        public final View a() {
            return this.a;
        }

        public final ImageView b() {
            return this.f;
        }

        public final ImageView c() {
            return this.e;
        }

        public final TextView d() {
            return this.b;
        }

        public final TextView e() {
            return this.d;
        }

        public final TextView f() {
            return this.c;
        }
    }

    /* compiled from: CoinPagingAdapter.kt */
    /* renamed from: k00$c */
    /* loaded from: classes2.dex */
    public interface c {
        void a(Coin coin);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k00(c cVar) {
        super(a.a, null, null, 6, null);
        fs1.f(cVar, "itemClickListener");
        this.d = cVar;
    }

    public static final void h(k00 k00Var, Coin coin, View view) {
        fs1.f(k00Var, "this$0");
        fs1.f(coin, "$datum");
        k00Var.d.a(coin);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: g */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        Coin item = getItem(i);
        fs1.d(item);
        final Coin coin = item;
        Integer id = coin.getId();
        fs1.e(id, "datum.id");
        if (id.intValue() > 0) {
            k73 u = com.bumptech.glide.a.u(bVar.b());
            Integer id2 = coin.getId();
            fs1.e(id2, "datum.id");
            u.x(a4.f(id2.intValue(), coin.getSymbol())).a(n73.v0()).I0(bVar.b());
        }
        bVar.d().setSelected(true);
        TextView d = bVar.d();
        StringBuilder sb = new StringBuilder();
        sb.append((Object) coin.getName());
        sb.append('(');
        sb.append((Object) coin.getSymbol());
        sb.append(')');
        d.setText(sb.toString());
        bVar.f().setSelected(true);
        u21.a aVar = u21.a;
        String t = e30.t(aVar.b());
        TextView f = bVar.f();
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        Double price = coin.getQuote().getUSD().getPrice();
        fs1.e(price, "datum.quote.usd.price");
        String format = String.format(locale, t, Arrays.copyOf(new Object[]{aVar.b(), e30.p(v21.a(price.doubleValue()), 0, null, false, 7, null)}, 2));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        f.setText(format);
        TextView e = bVar.e();
        String format2 = String.format("%.2f", Arrays.copyOf(new Object[]{coin.getQuote().getUSD().getPercentChange1h()}, 1));
        fs1.e(format2, "java.lang.String.format(format, *args)");
        e.setText(fs1.l(format2, "%"));
        bVar.a().setOnClickListener(new View.OnClickListener() { // from class: j00
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                k00.h(k00.this, coin, view);
            }
        });
        j(bVar, coin);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: i */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        this.e = viewGroup.getContext();
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.top_gainers_list_item, viewGroup, false);
        fs1.e(inflate, "from(parent.context).inf…list_item, parent, false)");
        return new b(this, inflate);
    }

    public final void j(b bVar, Coin coin) {
        fs1.f(bVar, "holder");
        fs1.f(coin, "datum");
        try {
            Double percentChange1h = coin.getQuote().getUSD().getPercentChange1h();
            fs1.e(percentChange1h, "datum.quote.usd.percentChange1h");
            Drawable drawable = null;
            if (percentChange1h.doubleValue() < Utils.DOUBLE_EPSILON) {
                ImageView c2 = bVar.c();
                Context context = this.e;
                if (context != null) {
                    drawable = m70.f(context, R.drawable.arrow_down);
                }
                c2.setBackground(drawable);
            } else {
                ImageView c3 = bVar.c();
                Context context2 = this.e;
                if (context2 != null) {
                    drawable = m70.f(context2, R.drawable.arrow_up);
                }
                c3.setBackground(drawable);
            }
            if (coin.getQuote().getUSD() == null) {
                bVar.c().setVisibility(8);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
