package defpackage;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.StrictMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: bn5  reason: default package */
/* loaded from: classes.dex */
public final class bn5 implements ln5 {
    public static final Map<Uri, bn5> g = new rh();
    public static final String[] h = {"key", "value"};
    public final ContentResolver a;
    public final Uri b;
    public final ContentObserver c;
    public final Object d;
    public volatile Map<String, String> e;
    public final List<en5> f;

    public bn5(ContentResolver contentResolver, Uri uri) {
        om5 om5Var = new om5(this, null);
        this.c = om5Var;
        this.d = new Object();
        this.f = new ArrayList();
        Objects.requireNonNull(contentResolver);
        Objects.requireNonNull(uri);
        this.a = contentResolver;
        this.b = uri;
        contentResolver.registerContentObserver(uri, false, om5Var);
    }

    public static bn5 b(ContentResolver contentResolver, Uri uri) {
        bn5 bn5Var;
        synchronized (bn5.class) {
            Map<Uri, bn5> map = g;
            bn5Var = map.get(uri);
            if (bn5Var == null) {
                try {
                    bn5 bn5Var2 = new bn5(contentResolver, uri);
                    try {
                        map.put(uri, bn5Var2);
                    } catch (SecurityException unused) {
                    }
                    bn5Var = bn5Var2;
                } catch (SecurityException unused2) {
                }
            }
        }
        return bn5Var;
    }

    public static synchronized void e() {
        synchronized (bn5.class) {
            for (bn5 bn5Var : g.values()) {
                bn5Var.a.unregisterContentObserver(bn5Var.c);
            }
            g.clear();
        }
    }

    @Override // defpackage.ln5
    public final /* bridge */ /* synthetic */ Object a(String str) {
        return c().get(str);
    }

    public final Map<String, String> c() {
        Map<String, String> map;
        Map<String, String> map2 = this.e;
        if (map2 == null) {
            synchronized (this.d) {
                map2 = this.e;
                if (map2 == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        map = (Map) hn5.a(new kn5(this) { // from class: mm5
                            public final bn5 a;

                            {
                                this.a = this;
                            }

                            @Override // defpackage.kn5
                            public final Object zza() {
                                return this.a.f();
                            }
                        });
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                    } catch (SQLiteException | IllegalStateException | SecurityException unused) {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        map = null;
                    } catch (Throwable th) {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        throw th;
                    }
                    this.e = map;
                    map2 = map;
                }
            }
        }
        return map2 != null ? map2 : Collections.emptyMap();
    }

    public final void d() {
        synchronized (this.d) {
            this.e = null;
            wo5.c();
        }
        synchronized (this) {
            for (en5 en5Var : this.f) {
                en5Var.zza();
            }
        }
    }

    public final /* synthetic */ Map f() {
        Map hashMap;
        Cursor query = this.a.query(this.b, h, null, null, null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            if (count <= 256) {
                hashMap = new rh(count);
            } else {
                hashMap = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                hashMap.put(query.getString(0), query.getString(1));
            }
            return hashMap;
        } finally {
            query.close();
        }
    }
}
