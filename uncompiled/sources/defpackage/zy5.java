package defpackage;

import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: zy5  reason: default package */
/* loaded from: classes.dex */
public final class zy5 implements Runnable {
    public final /* synthetic */ m a;
    public final /* synthetic */ AppMeasurementDynamiteService f0;

    public zy5(AppMeasurementDynamiteService appMeasurementDynamiteService, m mVar) {
        this.f0 = appMeasurementDynamiteService;
        this.a = mVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.a.G().V(this.a, this.f0.a.g());
    }
}
