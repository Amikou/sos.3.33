package defpackage;

import android.os.Looper;

/* compiled from: DeferredReleaser.java */
/* renamed from: tl0  reason: default package */
/* loaded from: classes.dex */
public abstract class tl0 {
    public static tl0 a;

    /* compiled from: DeferredReleaser.java */
    /* renamed from: tl0$a */
    /* loaded from: classes.dex */
    public interface a {
        void a();
    }

    public static synchronized tl0 b() {
        tl0 tl0Var;
        synchronized (tl0.class) {
            if (a == null) {
                a = new ul0();
            }
            tl0Var = a;
        }
        return tl0Var;
    }

    public static boolean c() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public abstract void a(a aVar);

    public abstract void d(a aVar);
}
