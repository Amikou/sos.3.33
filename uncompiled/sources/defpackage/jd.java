package defpackage;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;

/* compiled from: AnimatedDrawable2.java */
/* renamed from: jd  reason: default package */
/* loaded from: classes.dex */
public class jd extends Drawable implements Animatable, fr0 {
    public static final Class<?> v0 = jd.class;
    public static final le w0 = new om();
    public de a;
    public ac1 f0;
    public volatile boolean g0;
    public long h0;
    public long i0;
    public long j0;
    public int k0;
    public long l0;
    public long m0;
    public int n0;
    public long o0;
    public long p0;
    public int q0;
    public volatile le r0;
    public volatile b s0;
    public xq0 t0;
    public final Runnable u0;

    /* compiled from: AnimatedDrawable2.java */
    /* renamed from: jd$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            jd jdVar = jd.this;
            jdVar.unscheduleSelf(jdVar.u0);
            jd.this.invalidateSelf();
        }
    }

    /* compiled from: AnimatedDrawable2.java */
    /* renamed from: jd$b */
    /* loaded from: classes.dex */
    public interface b {
        void a(jd jdVar, ac1 ac1Var, int i, boolean z, boolean z2, long j, long j2, long j3, long j4, long j5, long j6, long j7);
    }

    public jd() {
        this(null);
    }

    public static ac1 c(de deVar) {
        if (deVar == null) {
            return null;
        }
        return new ds0(deVar);
    }

    @Override // defpackage.fr0
    public void a() {
        de deVar = this.a;
        if (deVar != null) {
            deVar.clear();
        }
    }

    public final long d() {
        return SystemClock.uptimeMillis();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        long j;
        long j2;
        jd jdVar;
        long j3;
        if (this.a == null || this.f0 == null) {
            return;
        }
        long d = d();
        long max = this.g0 ? (d - this.h0) + this.p0 : Math.max(this.i0, 0L);
        int b2 = this.f0.b(max, this.i0);
        if (b2 == -1) {
            b2 = this.a.a() - 1;
            this.r0.c(this);
            this.g0 = false;
        } else if (b2 == 0 && this.k0 != -1 && d >= this.j0) {
            this.r0.d(this);
        }
        int i = b2;
        boolean j4 = this.a.j(this, canvas, i);
        if (j4) {
            this.r0.a(this, i);
            this.k0 = i;
        }
        if (!j4) {
            e();
        }
        long d2 = d();
        if (this.g0) {
            long a2 = this.f0.a(d2 - this.h0);
            if (a2 != -1) {
                long j5 = this.o0 + a2;
                f(j5);
                j2 = j5;
            } else {
                this.r0.c(this);
                this.g0 = false;
                j2 = -1;
            }
            j = a2;
        } else {
            j = -1;
            j2 = -1;
        }
        b bVar = this.s0;
        if (bVar != null) {
            bVar.a(this, this.f0, i, j4, this.g0, this.h0, max, this.i0, d, d2, j, j2);
            jdVar = this;
            j3 = max;
        } else {
            jdVar = this;
            j3 = max;
        }
        jdVar.i0 = j3;
    }

    public final void e() {
        this.q0++;
        if (v11.m(2)) {
            v11.o(v0, "Dropped a frame. Count: %s", Integer.valueOf(this.q0));
        }
    }

    public final void f(long j) {
        long j2 = this.h0 + j;
        this.j0 = j2;
        scheduleSelf(this.u0, j2);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        de deVar = this.a;
        if (deVar == null) {
            return super.getIntrinsicHeight();
        }
        return deVar.c();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        de deVar = this.a;
        if (deVar == null) {
            return super.getIntrinsicWidth();
        }
        return deVar.e();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.g0;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        de deVar = this.a;
        if (deVar != null) {
            deVar.d(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        if (this.g0) {
            return false;
        }
        long j = i;
        if (this.i0 != j) {
            this.i0 = j;
            invalidateSelf();
            return true;
        }
        return false;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (this.t0 == null) {
            this.t0 = new xq0();
        }
        this.t0.b(i);
        de deVar = this.a;
        if (deVar != null) {
            deVar.i(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        if (this.t0 == null) {
            this.t0 = new xq0();
        }
        this.t0.c(colorFilter);
        de deVar = this.a;
        if (deVar != null) {
            deVar.g(colorFilter);
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        de deVar;
        if (this.g0 || (deVar = this.a) == null || deVar.a() <= 1) {
            return;
        }
        this.g0 = true;
        long d = d();
        long j = d - this.l0;
        this.h0 = j;
        this.j0 = j;
        this.i0 = d - this.m0;
        this.k0 = this.n0;
        invalidateSelf();
        this.r0.b(this);
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        if (this.g0) {
            long d = d();
            this.l0 = d - this.h0;
            this.m0 = d - this.i0;
            this.n0 = this.k0;
            this.g0 = false;
            this.h0 = 0L;
            this.j0 = 0L;
            this.i0 = -1L;
            this.k0 = -1;
            unscheduleSelf(this.u0);
            this.r0.c(this);
        }
    }

    public jd(de deVar) {
        this.o0 = 8L;
        this.p0 = 0L;
        this.r0 = w0;
        this.s0 = null;
        this.u0 = new a();
        this.a = deVar;
        this.f0 = c(deVar);
    }
}
