package defpackage;

/* compiled from: MediaPeriodId.java */
/* renamed from: j62  reason: default package */
/* loaded from: classes.dex */
public class j62 {
    public final Object a;
    public final int b;
    public final int c;
    public final long d;
    public final int e;

    public j62(Object obj) {
        this(obj, -1L);
    }

    public j62 a(Object obj) {
        return this.a.equals(obj) ? this : new j62(obj, this.b, this.c, this.d, this.e);
    }

    public boolean b() {
        return this.b != -1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof j62) {
            j62 j62Var = (j62) obj;
            return this.a.equals(j62Var.a) && this.b == j62Var.b && this.c == j62Var.c && this.d == j62Var.d && this.e == j62Var.e;
        }
        return false;
    }

    public int hashCode() {
        return ((((((((527 + this.a.hashCode()) * 31) + this.b) * 31) + this.c) * 31) + ((int) this.d)) * 31) + this.e;
    }

    public j62(Object obj, long j) {
        this(obj, -1, -1, j, -1);
    }

    public j62(Object obj, long j, int i) {
        this(obj, -1, -1, j, i);
    }

    public j62(Object obj, int i, int i2, long j) {
        this(obj, i, i2, j, -1);
    }

    public j62(j62 j62Var) {
        this.a = j62Var.a;
        this.b = j62Var.b;
        this.c = j62Var.c;
        this.d = j62Var.d;
        this.e = j62Var.e;
    }

    public j62(Object obj, int i, int i2, long j, int i3) {
        this.a = obj;
        this.b = i;
        this.c = i2;
        this.d = j;
        this.e = i3;
    }
}
