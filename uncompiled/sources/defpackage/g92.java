package defpackage;

import java.util.Random;

/* renamed from: g92  reason: default package */
/* loaded from: classes2.dex */
public abstract class g92 {
    public static int a(int i) {
        int i2 = 0;
        while ((i & 1) == 0) {
            i >>>= 1;
            i2++;
        }
        return i2;
    }

    public static void b(int[] iArr, int i, int[] iArr2, int[] iArr3) {
        if (i < 0) {
            kd2.a(iArr.length, iArr2, iArr, iArr3);
        } else {
            System.arraycopy(iArr2, 0, iArr3, 0, iArr.length);
        }
    }

    public static int c(int[] iArr, int[] iArr2, int i, int[] iArr3, int i2) {
        int length = iArr.length;
        int i3 = 0;
        while (iArr2[0] == 0) {
            kd2.C(i, iArr2, 0);
            i3 += 32;
        }
        int a = a(iArr2[0]);
        if (a > 0) {
            kd2.A(i, iArr2, a, 0);
            i3 += a;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if ((iArr3[0] & 1) != 0) {
                i2 += i2 < 0 ? kd2.e(length, iArr, iArr3) : kd2.N(length, iArr, iArr3);
            }
            kd2.z(length, iArr3, i2);
        }
        return i2;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        int length = iArr.length;
        if (kd2.w(length, iArr2)) {
            throw new IllegalArgumentException("'x' cannot be 0");
        }
        int i = 0;
        if (kd2.v(length, iArr2)) {
            System.arraycopy(iArr2, 0, iArr3, 0, length);
            return;
        }
        int[] i2 = kd2.i(length, iArr2);
        int[] j = kd2.j(length);
        j[0] = 1;
        int c = (1 & i2[0]) == 0 ? c(iArr, i2, length, j, 0) : 0;
        if (kd2.v(length, i2)) {
            b(iArr, c, j, iArr3);
            return;
        }
        int[] i3 = kd2.i(length, iArr);
        int[] j2 = kd2.j(length);
        int i4 = length;
        while (true) {
            int i5 = i4 - 1;
            if (i2[i5] == 0 && i3[i5] == 0) {
                i4--;
            } else if (kd2.q(i4, i2, i3)) {
                kd2.N(i4, i3, i2);
                c = c(iArr, i2, i4, j, c + (kd2.N(length, j2, j) - i));
                if (kd2.v(i4, i2)) {
                    b(iArr, c, j, iArr3);
                    return;
                }
            } else {
                kd2.N(i4, i2, i3);
                i = c(iArr, i3, i4, j2, i + (kd2.N(length, j, j2) - c));
                if (kd2.v(i4, i3)) {
                    b(iArr, i, j2, iArr3);
                    return;
                }
            }
        }
    }

    public static int[] e(int[] iArr) {
        int length = iArr.length;
        Random random = new Random();
        int[] j = kd2.j(length);
        int i = length - 1;
        int i2 = iArr[i];
        int i3 = i2 | (i2 >>> 1);
        int i4 = i3 | (i3 >>> 2);
        int i5 = i4 | (i4 >>> 4);
        int i6 = i5 | (i5 >>> 8);
        int i7 = i6 | (i6 >>> 16);
        do {
            for (int i8 = 0; i8 != length; i8++) {
                j[i8] = random.nextInt();
            }
            j[i] = j[i] & i7;
        } while (kd2.q(length, j, iArr));
        return j;
    }
}
