package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAlertBinding.java */
/* renamed from: ym0  reason: default package */
/* loaded from: classes2.dex */
public final class ym0 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialTextView c;
    public final MaterialTextView d;

    public ym0(ConstraintLayout constraintLayout, MaterialCardView materialCardView, MaterialButton materialButton, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialTextView;
        this.d = materialTextView2;
    }

    public static ym0 a(View view) {
        int i = R.id.contentWrapper;
        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.contentWrapper);
        if (materialCardView != null) {
            i = R.id.dialogCross;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialogCross);
            if (materialButton != null) {
                i = R.id.txtDescribe;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtDescribe);
                if (materialTextView != null) {
                    i = R.id.txtTitle;
                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtTitle);
                    if (materialTextView2 != null) {
                        return new ym0((ConstraintLayout) view, materialCardView, materialButton, materialTextView, materialTextView2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
