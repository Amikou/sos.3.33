package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;

/* compiled from: SearchBar1Binding.java */
/* renamed from: yd3  reason: default package */
/* loaded from: classes2.dex */
public final class yd3 {
    public final TextInputEditText a;

    public yd3(LinearLayout linearLayout, TextInputEditText textInputEditText, LinearLayout linearLayout2) {
        this.a = textInputEditText;
    }

    public static yd3 a(View view) {
        TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.etSearch);
        if (textInputEditText != null) {
            LinearLayout linearLayout = (LinearLayout) view;
            return new yd3(linearLayout, textInputEditText, linearLayout);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.etSearch)));
    }
}
