package defpackage;

import com.google.android.gms.internal.vision.zzht;
import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ap5  reason: default package */
/* loaded from: classes.dex */
public final class ap5 implements Comparator<zzht> {
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(zzht zzhtVar, zzht zzhtVar2) {
        int e;
        int e2;
        zzht zzhtVar3 = zzhtVar;
        zzht zzhtVar4 = zzhtVar2;
        hp5 hp5Var = (hp5) zzhtVar3.iterator();
        hp5 hp5Var2 = (hp5) zzhtVar4.iterator();
        while (hp5Var.hasNext() && hp5Var2.hasNext()) {
            e = zzht.e(hp5Var.zza());
            e2 = zzht.e(hp5Var2.zza());
            int compare = Integer.compare(e, e2);
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzhtVar3.zza(), zzhtVar4.zza());
    }
}
