package defpackage;

import java.math.BigInteger;

/* renamed from: pe3  reason: default package */
/* loaded from: classes2.dex */
public class pe3 {
    public static final int[] a = {-4553, -2, -1, -1, -1, -1};
    public static final int[] b = {20729809, 9106, 1, 0, 0, 0, -9106, -3, -1, -1, -1, -1};
    public static final int[] c = {-20729809, -9107, -2, -1, -1, -1, 9105, 2};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (cd2.a(iArr, iArr2, iArr3) != 0 || (iArr3[5] == -1 && cd2.r(iArr3, a))) {
            kd2.b(6, 4553, iArr3);
        }
    }

    public static void b(int[] iArr, int[] iArr2) {
        if (kd2.s(6, iArr, iArr2) != 0 || (iArr2[5] == -1 && cd2.r(iArr2, a))) {
            kd2.b(6, 4553, iArr2);
        }
    }

    public static int[] c(BigInteger bigInteger) {
        int[] n = cd2.n(bigInteger);
        if (n[5] == -1) {
            int[] iArr = a;
            if (cd2.r(n, iArr)) {
                cd2.G(iArr, n);
            }
        }
        return n;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] i = cd2.i();
        cd2.x(iArr, iArr2, i);
        g(i, iArr3);
    }

    public static void e(int[] iArr, int[] iArr2, int[] iArr3) {
        if (cd2.B(iArr, iArr2, iArr3) != 0 || (iArr3[11] == -1 && kd2.q(12, iArr3, b))) {
            int[] iArr4 = c;
            if (kd2.e(iArr4.length, iArr4, iArr3) != 0) {
                kd2.t(12, iArr3, iArr4.length);
            }
        }
    }

    public static void f(int[] iArr, int[] iArr2) {
        if (cd2.u(iArr)) {
            cd2.J(iArr2);
        } else {
            cd2.F(a, iArr, iArr2);
        }
    }

    public static void g(int[] iArr, int[] iArr2) {
        if (cd2.z(4553, cd2.y(4553, iArr, 6, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[5] == -1 && cd2.r(iArr2, a))) {
            kd2.b(6, 4553, iArr2);
        }
    }

    public static void h(int i, int[] iArr) {
        if ((i == 0 || cd2.A(4553, i, iArr, 0) == 0) && !(iArr[5] == -1 && cd2.r(iArr, a))) {
            return;
        }
        kd2.b(6, 4553, iArr);
    }

    public static void i(int[] iArr, int[] iArr2) {
        int[] i = cd2.i();
        cd2.D(iArr, i);
        g(i, iArr2);
    }

    public static void j(int[] iArr, int i, int[] iArr2) {
        int[] i2 = cd2.i();
        cd2.D(iArr, i2);
        while (true) {
            g(i2, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            cd2.D(iArr2, i2);
        }
    }

    public static void k(int[] iArr, int[] iArr2, int[] iArr3) {
        if (cd2.F(iArr, iArr2, iArr3) != 0) {
            kd2.L(6, 4553, iArr3);
        }
    }

    public static void l(int[] iArr, int[] iArr2) {
        if (kd2.E(6, iArr, 0, iArr2) != 0 || (iArr2[5] == -1 && cd2.r(iArr2, a))) {
            kd2.b(6, 4553, iArr2);
        }
    }
}
