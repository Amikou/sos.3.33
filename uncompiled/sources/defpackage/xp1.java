package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.slider.Slider;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: IncludeCpNotifiyBinding.java */
/* renamed from: xp1  reason: default package */
/* loaded from: classes2.dex */
public final class xp1 {
    public final MaterialCheckBox a;
    public final MaterialCheckBox b;
    public final MaterialCheckBox c;
    public final AutofitEdittext d;
    public final Slider e;
    public final MaterialTextView f;
    public final MaterialTextView g;

    public xp1(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, MaterialCheckBox materialCheckBox2, MaterialCheckBox materialCheckBox3, AutofitEdittext autofitEdittext, Slider slider, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = materialCheckBox;
        this.b = materialCheckBox2;
        this.c = materialCheckBox3;
        this.d = autofitEdittext;
        this.e = slider;
        this.f = materialTextView;
        this.g = materialTextView2;
    }

    public static xp1 a(View view) {
        int i = R.id.cbEnableBox;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbEnableBox);
        if (materialCheckBox != null) {
            i = R.id.cbEnablepriceReachesOrHigher;
            MaterialCheckBox materialCheckBox2 = (MaterialCheckBox) ai4.a(view, R.id.cbEnablepriceReachesOrHigher);
            if (materialCheckBox2 != null) {
                i = R.id.cbEnablepriceReachesOrLower;
                MaterialCheckBox materialCheckBox3 = (MaterialCheckBox) ai4.a(view, R.id.cbEnablepriceReachesOrLower);
                if (materialCheckBox3 != null) {
                    i = R.id.newValue;
                    AutofitEdittext autofitEdittext = (AutofitEdittext) ai4.a(view, R.id.newValue);
                    if (autofitEdittext != null) {
                        i = R.id.slider;
                        Slider slider = (Slider) ai4.a(view, R.id.slider);
                        if (slider != null) {
                            i = R.id.txtPercentage;
                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtPercentage);
                            if (materialTextView != null) {
                                i = R.id.txtTitle;
                                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtTitle);
                                if (materialTextView2 != null) {
                                    return new xp1((ConstraintLayout) view, materialCheckBox, materialCheckBox2, materialCheckBox3, autofitEdittext, slider, materialTextView, materialTextView2);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
