package defpackage;

import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.k;

/* renamed from: nr4  reason: default package */
/* loaded from: classes2.dex */
public class nr4 extends h implements b4 {
    public k a;

    public nr4(e4 e4Var) {
        this.a = null;
        this.a = e4Var;
    }

    public nr4(i iVar) {
        this.a = null;
        this.a = iVar;
    }

    public nr4(k kVar) {
        this.a = null;
        this.a = kVar;
    }

    public nr4(pr4 pr4Var) {
        this.a = null;
        this.a = pr4Var.i();
    }

    public static nr4 o(Object obj) {
        if (obj == null || (obj instanceof nr4)) {
            return (nr4) obj;
        }
        if (obj instanceof k) {
            return new nr4((k) obj);
        }
        if (obj instanceof byte[]) {
            try {
                return new nr4(k.s((byte[]) obj));
            } catch (Exception e) {
                throw new IllegalArgumentException("unable to parse encoded data: " + e.getMessage());
            }
        }
        throw new IllegalArgumentException("unknown object in getInstance()");
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        return this.a;
    }

    public k p() {
        return this.a;
    }

    public boolean q() {
        return this.a instanceof e4;
    }

    public boolean s() {
        return this.a instanceof i;
    }
}
