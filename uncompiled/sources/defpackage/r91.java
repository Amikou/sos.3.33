package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.helper.widget.Flow;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.imageview.ShapeableImageView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentCalculatorBinding.java */
/* renamed from: r91  reason: default package */
/* loaded from: classes2.dex */
public final class r91 {
    public final AppCompatTextView A;
    public final AppCompatTextView B;
    public final LinearLayout C;
    public final wf D;
    public final ScrollView a;
    public final HorizontalScrollView b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final MaterialButton f;
    public final MaterialButton g;
    public final MaterialButton h;
    public final MaterialButton i;
    public final MaterialButton j;
    public final MaterialButton k;
    public final MaterialButton l;
    public final MaterialButton m;
    public final MaterialButton n;
    public final MaterialButton o;
    public final MaterialButton p;
    public final MaterialButton q;
    public final MaterialButton r;
    public final MaterialButton s;
    public final ShapeableImageView t;
    public final MaterialButton u;
    public final MaterialButton v;
    public final MaterialButton w;
    public final MaterialButton x;
    public final MaterialButton y;
    public final ConstraintLayout z;

    public r91(ScrollView scrollView, HorizontalScrollView horizontalScrollView, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, MaterialButton materialButton5, MaterialButton materialButton6, MaterialButton materialButton7, MaterialButton materialButton8, MaterialButton materialButton9, MaterialButton materialButton10, MaterialButton materialButton11, MaterialButton materialButton12, MaterialButton materialButton13, MaterialButton materialButton14, MaterialButton materialButton15, MaterialButton materialButton16, MaterialButton materialButton17, ShapeableImageView shapeableImageView, MaterialButton materialButton18, MaterialButton materialButton19, MaterialButton materialButton20, MaterialButton materialButton21, MaterialButton materialButton22, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, View view, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, LinearLayout linearLayout, Flow flow, View view2, View view3, ScrollView scrollView2, wf wfVar, FrameLayout frameLayout) {
        this.a = scrollView;
        this.b = horizontalScrollView;
        this.c = materialButton;
        this.d = materialButton2;
        this.e = materialButton3;
        this.f = materialButton4;
        this.g = materialButton5;
        this.h = materialButton6;
        this.i = materialButton7;
        this.j = materialButton8;
        this.k = materialButton9;
        this.l = materialButton10;
        this.m = materialButton11;
        this.n = materialButton12;
        this.o = materialButton13;
        this.p = materialButton14;
        this.q = materialButton15;
        this.r = materialButton16;
        this.s = materialButton17;
        this.t = shapeableImageView;
        this.u = materialButton18;
        this.v = materialButton19;
        this.w = materialButton20;
        this.x = materialButton21;
        this.y = materialButton22;
        this.z = constraintLayout;
        this.A = appCompatTextView;
        this.B = appCompatTextView2;
        this.C = linearLayout;
        this.D = wfVar;
    }

    public static r91 a(View view) {
        int i = R.id.HS_e2;
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) ai4.a(view, R.id.HS_e2);
        if (horizontalScrollView != null) {
            i = R.id.btn_0;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btn_0);
            if (materialButton != null) {
                i = R.id.btn_1;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btn_1);
                if (materialButton2 != null) {
                    i = R.id.btn_2;
                    MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btn_2);
                    if (materialButton3 != null) {
                        i = R.id.btn_3;
                        MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.btn_3);
                        if (materialButton4 != null) {
                            i = R.id.btn_4;
                            MaterialButton materialButton5 = (MaterialButton) ai4.a(view, R.id.btn_4);
                            if (materialButton5 != null) {
                                i = R.id.btn_5;
                                MaterialButton materialButton6 = (MaterialButton) ai4.a(view, R.id.btn_5);
                                if (materialButton6 != null) {
                                    i = R.id.btn_6;
                                    MaterialButton materialButton7 = (MaterialButton) ai4.a(view, R.id.btn_6);
                                    if (materialButton7 != null) {
                                        i = R.id.btn_7;
                                        MaterialButton materialButton8 = (MaterialButton) ai4.a(view, R.id.btn_7);
                                        if (materialButton8 != null) {
                                            i = R.id.btn_8;
                                            MaterialButton materialButton9 = (MaterialButton) ai4.a(view, R.id.btn_8);
                                            if (materialButton9 != null) {
                                                i = R.id.btn_9;
                                                MaterialButton materialButton10 = (MaterialButton) ai4.a(view, R.id.btn_9);
                                                if (materialButton10 != null) {
                                                    i = R.id.btn_bag;
                                                    MaterialButton materialButton11 = (MaterialButton) ai4.a(view, R.id.btn_bag);
                                                    if (materialButton11 != null) {
                                                        i = R.id.btn_bil;
                                                        MaterialButton materialButton12 = (MaterialButton) ai4.a(view, R.id.btn_bil);
                                                        if (materialButton12 != null) {
                                                            i = R.id.btn_c;
                                                            MaterialButton materialButton13 = (MaterialButton) ai4.a(view, R.id.btn_c);
                                                            if (materialButton13 != null) {
                                                                i = R.id.btn_del;
                                                                MaterialButton materialButton14 = (MaterialButton) ai4.a(view, R.id.btn_del);
                                                                if (materialButton14 != null) {
                                                                    i = R.id.btn_divide;
                                                                    MaterialButton materialButton15 = (MaterialButton) ai4.a(view, R.id.btn_divide);
                                                                    if (materialButton15 != null) {
                                                                        i = R.id.btn_dot;
                                                                        MaterialButton materialButton16 = (MaterialButton) ai4.a(view, R.id.btn_dot);
                                                                        if (materialButton16 != null) {
                                                                            i = R.id.btn_equal;
                                                                            MaterialButton materialButton17 = (MaterialButton) ai4.a(view, R.id.btn_equal);
                                                                            if (materialButton17 != null) {
                                                                                i = R.id.btnLatestPrice;
                                                                                ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.btnLatestPrice);
                                                                                if (shapeableImageView != null) {
                                                                                    i = R.id.btn_mil;
                                                                                    MaterialButton materialButton18 = (MaterialButton) ai4.a(view, R.id.btn_mil);
                                                                                    if (materialButton18 != null) {
                                                                                        i = R.id.btn_minus;
                                                                                        MaterialButton materialButton19 = (MaterialButton) ai4.a(view, R.id.btn_minus);
                                                                                        if (materialButton19 != null) {
                                                                                            i = R.id.btn_multi;
                                                                                            MaterialButton materialButton20 = (MaterialButton) ai4.a(view, R.id.btn_multi);
                                                                                            if (materialButton20 != null) {
                                                                                                i = R.id.btn_plus;
                                                                                                MaterialButton materialButton21 = (MaterialButton) ai4.a(view, R.id.btn_plus);
                                                                                                if (materialButton21 != null) {
                                                                                                    i = R.id.btn_ths;
                                                                                                    MaterialButton materialButton22 = (MaterialButton) ai4.a(view, R.id.btn_ths);
                                                                                                    if (materialButton22 != null) {
                                                                                                        i = R.id.buttonWrapper;
                                                                                                        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.buttonWrapper);
                                                                                                        if (constraintLayout != null) {
                                                                                                            i = R.id.constraintLayout2;
                                                                                                            ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.constraintLayout2);
                                                                                                            if (constraintLayout2 != null) {
                                                                                                                i = R.id.dividerView;
                                                                                                                View a = ai4.a(view, R.id.dividerView);
                                                                                                                if (a != null) {
                                                                                                                    i = R.id.e1;
                                                                                                                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.e1);
                                                                                                                    if (appCompatTextView != null) {
                                                                                                                        i = R.id.e2;
                                                                                                                        AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.e2);
                                                                                                                        if (appCompatTextView2 != null) {
                                                                                                                            i = R.id.editWrapper;
                                                                                                                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.editWrapper);
                                                                                                                            if (linearLayout != null) {
                                                                                                                                i = R.id.flow;
                                                                                                                                Flow flow = (Flow) ai4.a(view, R.id.flow);
                                                                                                                                if (flow != null) {
                                                                                                                                    i = R.id.latestBottom;
                                                                                                                                    View a2 = ai4.a(view, R.id.latestBottom);
                                                                                                                                    if (a2 != null) {
                                                                                                                                        i = R.id.latestTop;
                                                                                                                                        View a3 = ai4.a(view, R.id.latestTop);
                                                                                                                                        if (a3 != null) {
                                                                                                                                            ScrollView scrollView = (ScrollView) view;
                                                                                                                                            i = R.id.toolbar;
                                                                                                                                            View a4 = ai4.a(view, R.id.toolbar);
                                                                                                                                            if (a4 != null) {
                                                                                                                                                wf a5 = wf.a(a4);
                                                                                                                                                i = R.id.topBarContainer;
                                                                                                                                                FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.topBarContainer);
                                                                                                                                                if (frameLayout != null) {
                                                                                                                                                    return new r91(scrollView, horizontalScrollView, materialButton, materialButton2, materialButton3, materialButton4, materialButton5, materialButton6, materialButton7, materialButton8, materialButton9, materialButton10, materialButton11, materialButton12, materialButton13, materialButton14, materialButton15, materialButton16, materialButton17, shapeableImageView, materialButton18, materialButton19, materialButton20, materialButton21, materialButton22, constraintLayout, constraintLayout2, a, appCompatTextView, appCompatTextView2, linearLayout, flow, a2, a3, scrollView, a5, frameLayout);
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ScrollView b() {
        return this.a;
    }
}
