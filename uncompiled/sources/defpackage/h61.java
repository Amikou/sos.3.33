package defpackage;

import java.math.BigInteger;

/* renamed from: h61  reason: default package */
/* loaded from: classes2.dex */
public class h61 {

    /* renamed from: h61$a */
    /* loaded from: classes2.dex */
    public static class a implements tt2 {
        public final /* synthetic */ xs0 a;
        public final /* synthetic */ pt0 b;

        public a(xs0 xs0Var, pt0 pt0Var) {
            this.a = xs0Var;
            this.b = pt0Var;
        }

        @Override // defpackage.tt2
        public ut2 a(ut2 ut2Var) {
            g61 g61Var = ut2Var instanceof g61 ? (g61) ut2Var : null;
            int a = h61.a(this.a);
            int i = a > 257 ? 6 : 5;
            int i2 = 1 << i;
            if (b(g61Var, i2)) {
                return g61Var;
            }
            int i3 = ((a + i) - 1) / i;
            pt0[] pt0VarArr = new pt0[i + 1];
            pt0VarArr[0] = this.b;
            for (int i4 = 1; i4 < i; i4++) {
                pt0VarArr[i4] = pt0VarArr[i4 - 1].I(i3);
            }
            pt0VarArr[i] = pt0VarArr[0].G(pt0VarArr[1]);
            this.a.A(pt0VarArr);
            pt0[] pt0VarArr2 = new pt0[i2];
            pt0VarArr2[0] = pt0VarArr[0];
            for (int i5 = i - 1; i5 >= 0; i5--) {
                pt0 pt0Var = pt0VarArr[i5];
                int i6 = 1 << i5;
                for (int i7 = i6; i7 < i2; i7 += i6 << 1) {
                    pt0VarArr2[i7] = pt0VarArr2[i7 - i6].a(pt0Var);
                }
            }
            this.a.A(pt0VarArr2);
            g61 g61Var2 = new g61();
            g61Var2.d(this.a.e(pt0VarArr2, 0, i2));
            g61Var2.e(pt0VarArr[i]);
            g61Var2.f(i);
            return g61Var2;
        }

        public final boolean b(g61 g61Var, int i) {
            return g61Var != null && c(g61Var.a(), i);
        }

        public final boolean c(ht0 ht0Var, int i) {
            return ht0Var != null && ht0Var.a() >= i;
        }
    }

    public static int a(xs0 xs0Var) {
        BigInteger x = xs0Var.x();
        return x == null ? xs0Var.u() + 1 : x.bitLength();
    }

    public static g61 b(pt0 pt0Var) {
        xs0 i = pt0Var.i();
        return (g61) i.C(pt0Var, "bc_fixed_point", new a(i, pt0Var));
    }
}
