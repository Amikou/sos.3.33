package defpackage;

import com.google.android.gms.internal.clearcut.zzbb;

/* renamed from: gh5  reason: default package */
/* loaded from: classes.dex */
public final class gh5 {
    public static String a(zzbb zzbbVar) {
        String str;
        jh5 jh5Var = new jh5(zzbbVar);
        StringBuilder sb = new StringBuilder(jh5Var.size());
        for (int i = 0; i < jh5Var.size(); i++) {
            int a = jh5Var.a(i);
            if (a == 34) {
                str = "\\\"";
            } else if (a == 39) {
                str = "\\'";
            } else if (a != 92) {
                switch (a) {
                    case 7:
                        str = "\\a";
                        break;
                    case 8:
                        str = "\\b";
                        break;
                    case 9:
                        str = "\\t";
                        break;
                    case 10:
                        str = "\\n";
                        break;
                    case 11:
                        str = "\\v";
                        break;
                    case 12:
                        str = "\\f";
                        break;
                    case 13:
                        str = "\\r";
                        break;
                    default:
                        if (a < 32 || a > 126) {
                            sb.append('\\');
                            sb.append((char) (((a >>> 6) & 3) + 48));
                            sb.append((char) (((a >>> 3) & 7) + 48));
                            a = (a & 7) + 48;
                        }
                        sb.append((char) a);
                        continue;
                        break;
                }
            } else {
                str = "\\\\";
            }
            sb.append(str);
        }
        return sb.toString();
    }
}
