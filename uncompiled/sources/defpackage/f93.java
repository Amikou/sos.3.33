package defpackage;

import java.math.BigInteger;
import java.util.Arrays;

/* compiled from: RlpString.java */
/* renamed from: f93  reason: default package */
/* loaded from: classes3.dex */
public class f93 implements g93 {
    private static final byte[] EMPTY = new byte[0];
    private final byte[] value;

    private f93(byte[] bArr) {
        this.value = bArr;
    }

    public static f93 create(byte[] bArr) {
        return new f93(bArr);
    }

    public BigInteger asPositiveBigInteger() {
        if (this.value.length == 0) {
            return BigInteger.ZERO;
        }
        return new BigInteger(1, this.value);
    }

    public String asString() {
        return ej2.toHexString(this.value);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f93.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.value, ((f93) obj).value);
    }

    public byte[] getBytes() {
        return this.value;
    }

    public int hashCode() {
        return Arrays.hashCode(this.value);
    }

    public static f93 create(byte b) {
        return new f93(new byte[]{b});
    }

    public static f93 create(BigInteger bigInteger) {
        if (bigInteger != null && bigInteger.signum() >= 1) {
            byte[] byteArray = bigInteger.toByteArray();
            if (byteArray[0] == 0) {
                return new f93(Arrays.copyOfRange(byteArray, 1, byteArray.length));
            }
            return new f93(byteArray);
        }
        return new f93(EMPTY);
    }

    public static f93 create(long j) {
        return create(BigInteger.valueOf(j));
    }

    public static f93 create(String str) {
        return new f93(str.getBytes());
    }
}
