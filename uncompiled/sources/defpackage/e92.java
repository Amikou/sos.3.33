package defpackage;

import net.safemoon.androidwallet.R;

/* compiled from: MobileNavigationDirections.java */
/* renamed from: e92  reason: default package */
/* loaded from: classes2.dex */
public class e92 {
    public static ce2 a() {
        return new l6(R.id.action_navigation_wallet_to_SwapMigrationFragment);
    }

    public static ce2 b() {
        return new l6(R.id.action_navigation_wallet_to_walletConnectFragment);
    }

    public static ce2 c() {
        return new l6(R.id.action_to_calculator);
    }

    public static ce2 d() {
        return new l6(R.id.action_to_reflection);
    }

    public static ce2 e() {
        return new l6(R.id.action_to_wallet);
    }
}
