package defpackage;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAlertOneButtonBinding.java */
/* renamed from: an0  reason: default package */
/* loaded from: classes2.dex */
public final class an0 {
    public final CardView a;
    public final MaterialButton b;
    public final MaterialCheckBox c;
    public final MaterialButton d;
    public final TextView e;
    public final TextView f;

    public an0(CardView cardView, MaterialButton materialButton, MaterialCheckBox materialCheckBox, MaterialButton materialButton2, TextView textView, TextView textView2, RelativeLayout relativeLayout) {
        this.a = cardView;
        this.b = materialButton;
        this.c = materialCheckBox;
        this.d = materialButton2;
        this.e = textView;
        this.f = textView2;
    }

    public static an0 a(View view) {
        int i = R.id.btnAction;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAction);
        if (materialButton != null) {
            i = R.id.chkDontShowMe;
            MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chkDontShowMe);
            if (materialCheckBox != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton2 != null) {
                    i = R.id.tvDialogContent;
                    TextView textView = (TextView) ai4.a(view, R.id.tvDialogContent);
                    if (textView != null) {
                        i = R.id.tvDialogTitle;
                        TextView textView2 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                        if (textView2 != null) {
                            i = R.id.vDialogContentContainer;
                            RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.vDialogContentContainer);
                            if (relativeLayout != null) {
                                return new an0((CardView) view, materialButton, materialCheckBox, materialButton2, textView, textView2, relativeLayout);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
