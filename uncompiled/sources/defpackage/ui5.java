package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ui5  reason: default package */
/* loaded from: classes.dex */
public final class ui5 {
    public final ck5 a;

    public ui5(ck5 ck5Var) {
        this.a = ck5Var;
    }

    public final boolean a() {
        try {
            to2 a = kr4.a(this.a.m());
            if (a != null) {
                return a.e("com.android.vending", 128).versionCode >= 80837300;
            }
            this.a.w().v().a("Failed to get PackageManager for Install Referrer Play Store compatibility check");
            return false;
        } catch (Exception e) {
            this.a.w().v().b("Failed to retrieve Play Store version for Install Referrer", e);
            return false;
        }
    }
}
