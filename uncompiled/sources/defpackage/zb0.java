package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: zb0  reason: default package */
/* loaded from: classes2.dex */
public class zb0 extends ct0.b {
    public static final BigInteger g = xb0.j;
    public static final int[] h = {1242472624, -991028441, -1389370248, 792926214, 1039914919, 726466713, 1338105611, 730014848};
    public int[] f;

    public zb0() {
        this.f = ed2.h();
    }

    public zb0(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for Curve25519FieldElement");
        }
        this.f = yb0.d(bigInteger);
    }

    public zb0(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] h2 = ed2.h();
        yb0.a(this.f, ((zb0) ct0Var).f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] h2 = ed2.h();
        yb0.b(this.f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] h2 = ed2.h();
        g92.d(yb0.a, ((zb0) ct0Var).f, h2);
        yb0.e(h2, this.f, h2);
        return new zb0(h2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof zb0) {
            return ed2.m(this.f, ((zb0) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] h2 = ed2.h();
        g92.d(yb0.a, this.f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ed2.t(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 8);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ed2.v(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] h2 = ed2.h();
        yb0.e(this.f, ((zb0) ct0Var).f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] h2 = ed2.h();
        yb0.g(this.f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (ed2.v(iArr) || ed2.t(iArr)) {
            return this;
        }
        int[] h2 = ed2.h();
        yb0.j(iArr, h2);
        yb0.e(h2, iArr, h2);
        yb0.j(h2, h2);
        yb0.e(h2, iArr, h2);
        int[] h3 = ed2.h();
        yb0.j(h2, h3);
        yb0.e(h3, iArr, h3);
        int[] h4 = ed2.h();
        yb0.k(h3, 3, h4);
        yb0.e(h4, h2, h4);
        yb0.k(h4, 4, h2);
        yb0.e(h2, h3, h2);
        yb0.k(h2, 4, h4);
        yb0.e(h4, h3, h4);
        yb0.k(h4, 15, h3);
        yb0.e(h3, h4, h3);
        yb0.k(h3, 30, h4);
        yb0.e(h4, h3, h4);
        yb0.k(h4, 60, h3);
        yb0.e(h3, h4, h3);
        yb0.k(h3, 11, h4);
        yb0.e(h4, h2, h4);
        yb0.k(h4, 120, h2);
        yb0.e(h2, h3, h2);
        yb0.j(h2, h2);
        yb0.j(h2, h3);
        if (ed2.m(iArr, h3)) {
            return new zb0(h2);
        }
        yb0.e(h2, h, h2);
        yb0.j(h2, h3);
        if (ed2.m(iArr, h3)) {
            return new zb0(h2);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] h2 = ed2.h();
        yb0.j(this.f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] h2 = ed2.h();
        yb0.n(this.f, ((zb0) ct0Var).f, h2);
        return new zb0(h2);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return ed2.q(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ed2.J(this.f);
    }
}
