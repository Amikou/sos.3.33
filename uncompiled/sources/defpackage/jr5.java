package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jr5  reason: default package */
/* loaded from: classes.dex */
public final class jr5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ Bundle f0;
    public final /* synthetic */ p g0;

    public jr5(p pVar, zzp zzpVar, Bundle bundle) {
        this.g0 = pVar;
        this.a = zzpVar;
        this.f0 = bundle;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d dVar;
        dVar = this.g0.d;
        if (dVar == null) {
            this.g0.a.w().l().a("Failed to send default event parameters to service");
            return;
        }
        try {
            zt2.j(this.a);
            dVar.h0(this.f0, this.a);
        } catch (RemoteException e) {
            this.g0.a.w().l().b("Failed to send default event parameters to service", e);
        }
    }
}
