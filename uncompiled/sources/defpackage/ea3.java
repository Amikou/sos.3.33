package defpackage;

/* renamed from: ea3  reason: default package */
/* loaded from: classes2.dex */
public class ea3 {
    public static void a(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4, int i) {
        System.arraycopy(iArr, iArr.length - 16, iArr2, 0, 16);
        int length = iArr.length >>> 1;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = i * 2; i4 > 0; i4--) {
            h(iArr2, iArr, i2, iArr3);
            bc3.b(8, iArr3, iArr2);
            System.arraycopy(iArr2, 0, iArr4, i3, 16);
            i3 = (length + i2) - i3;
            i2 += 16;
        }
        System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
    }

    public static void b(byte[] bArr) {
        if (bArr != null) {
            wh.n(bArr, (byte) 0);
        }
    }

    public static void c(int[] iArr) {
        if (iArr != null) {
            wh.o(iArr, 0);
        }
    }

    public static void d(int[][] iArr) {
        for (int[] iArr2 : iArr) {
            c(iArr2);
        }
    }

    public static byte[] e(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4) {
        int i5 = i2 * 128;
        byte[] g = g(bArr, bArr2, i3 * i5);
        int[] iArr = null;
        try {
            int length = g.length >>> 2;
            iArr = new int[length];
            ro2.g(g, 0, iArr);
            int i6 = i5 >>> 2;
            for (int i7 = 0; i7 < length; i7 += i6) {
                f(iArr, i7, i, i2);
            }
            ro2.e(iArr, g, 0);
            return g(bArr, g, i4);
        } finally {
            b(g);
            c(iArr);
        }
    }

    public static void f(int[] iArr, int i, int i2, int i3) {
        int i4 = i3 * 32;
        int[] iArr2 = new int[16];
        int[] iArr3 = new int[16];
        int[] iArr4 = new int[i4];
        int[] iArr5 = new int[i4];
        int[][] iArr6 = new int[i2];
        try {
            System.arraycopy(iArr, i, iArr5, 0, i4);
            for (int i5 = 0; i5 < i2; i5++) {
                iArr6[i5] = wh.f(iArr5);
                a(iArr5, iArr2, iArr3, iArr4, i3);
            }
            int i6 = i2 - 1;
            for (int i7 = 0; i7 < i2; i7++) {
                h(iArr5, iArr6[iArr5[i4 - 16] & i6], 0, iArr5);
                a(iArr5, iArr2, iArr3, iArr4, i3);
            }
            System.arraycopy(iArr5, 0, iArr, i, i4);
            d(iArr6);
            d(new int[][]{iArr5, iArr2, iArr3, iArr4});
        } catch (Throwable th) {
            d(iArr6);
            d(new int[][]{iArr5, iArr2, iArr3, iArr4});
            throw th;
        }
    }

    public static byte[] g(byte[] bArr, byte[] bArr2, int i) {
        lo2 lo2Var = new lo2(new ka3());
        lo2Var.b(bArr, bArr2, 1);
        return ((kx1) lo2Var.a(i * 8)).a();
    }

    public static void h(int[] iArr, int[] iArr2, int i, int[] iArr3) {
        for (int length = iArr3.length - 1; length >= 0; length--) {
            iArr3[length] = iArr[length] ^ iArr2[i + length];
        }
    }

    public static byte[] i(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4) {
        if (bArr != null) {
            if (bArr2 != null) {
                if (i <= 1 || !j(i)) {
                    throw new IllegalArgumentException("Cost parameter N must be > 1 and a power of 2");
                }
                if (i2 != 1 || i < 65536) {
                    if (i2 >= 1) {
                        int i5 = Integer.MAX_VALUE / ((i2 * 128) * 8);
                        if (i3 >= 1 && i3 <= i5) {
                            if (i4 >= 1) {
                                return e(bArr, bArr2, i, i2, i3, i4);
                            }
                            throw new IllegalArgumentException("Generated key length dkLen must be >= 1.");
                        }
                        throw new IllegalArgumentException("Parallelisation parameter p must be >= 1 and <= " + i5 + " (based on block size r of " + i2 + ")");
                    }
                    throw new IllegalArgumentException("Block size r must be >= 1.");
                }
                throw new IllegalArgumentException("Cost parameter N must be > 1 and < 65536.");
            }
            throw new IllegalArgumentException("Salt S must be provided.");
        }
        throw new IllegalArgumentException("Passphrase P must be provided.");
    }

    public static boolean j(int i) {
        return (i & (i + (-1))) == 0;
    }
}
