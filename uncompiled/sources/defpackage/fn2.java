package defpackage;

/* compiled from: Atomic.kt */
/* renamed from: fn2  reason: default package */
/* loaded from: classes2.dex */
public abstract class fn2 {
    public abstract cj<?> a();

    public final boolean b(fn2 fn2Var) {
        cj<?> a;
        cj<?> a2 = a();
        return (a2 == null || (a = fn2Var.a()) == null || a2.f() >= a.f()) ? false : true;
    }

    public abstract Object c(Object obj);

    public String toString() {
        return ff0.a(this) + '@' + ff0.b(this);
    }
}
