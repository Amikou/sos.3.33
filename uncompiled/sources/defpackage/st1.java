package defpackage;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;

/* compiled from: Job.kt */
/* renamed from: st1  reason: default package */
/* loaded from: classes2.dex */
public interface st1 extends CoroutineContext.a {
    public static final b f = b.a;

    /* compiled from: Job.kt */
    /* renamed from: st1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ void a(st1 st1Var, CancellationException cancellationException, int i, Object obj) {
            if (obj != null) {
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: cancel");
            }
            if ((i & 1) != 0) {
                cancellationException = null;
            }
            st1Var.a(cancellationException);
        }

        public static <R> R b(st1 st1Var, R r, hd1<? super R, ? super CoroutineContext.a, ? extends R> hd1Var) {
            return (R) CoroutineContext.a.C0196a.a(st1Var, r, hd1Var);
        }

        public static <E extends CoroutineContext.a> E c(st1 st1Var, CoroutineContext.b<E> bVar) {
            return (E) CoroutineContext.a.C0196a.b(st1Var, bVar);
        }

        public static /* synthetic */ yp0 d(st1 st1Var, boolean z, boolean z2, tc1 tc1Var, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return st1Var.d(z, z2, tc1Var);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }

        public static CoroutineContext e(st1 st1Var, CoroutineContext.b<?> bVar) {
            return CoroutineContext.a.C0196a.c(st1Var, bVar);
        }

        public static CoroutineContext f(st1 st1Var, CoroutineContext coroutineContext) {
            return CoroutineContext.a.C0196a.d(st1Var, coroutineContext);
        }
    }

    /* compiled from: Job.kt */
    /* renamed from: st1$b */
    /* loaded from: classes2.dex */
    public static final class b implements CoroutineContext.b<st1> {
        public static final /* synthetic */ b a = new b();
    }

    Object F(q70<? super te4> q70Var);

    void a(CancellationException cancellationException);

    boolean b();

    yp0 d(boolean z, boolean z2, tc1<? super Throwable, te4> tc1Var);

    CancellationException g();

    boolean isCancelled();

    boolean start();

    fy x(hy hyVar);

    yp0 z(tc1<? super Throwable, te4> tc1Var);
}
