package defpackage;

/* compiled from: ZFunc2.java */
/* renamed from: ps4  reason: default package */
/* loaded from: classes2.dex */
public interface ps4<Param1T, Param2T, ReturnT> {
    ReturnT apply(Param1T param1t, Param2T param2t);
}
