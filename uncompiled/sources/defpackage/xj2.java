package defpackage;

import com.onesignal.c1;
import com.onesignal.influence.domain.OSInfluenceType;
import okhttp3.HttpUrl;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: OSInfluenceDataRepository.kt */
/* renamed from: xj2  reason: default package */
/* loaded from: classes2.dex */
public final class xj2 {
    public final vk2 a;

    public xj2(vk2 vk2Var) {
        fs1.f(vk2Var, "preferences");
        this.a = vk2Var;
    }

    public final void a(OSInfluenceType oSInfluenceType) {
        fs1.f(oSInfluenceType, "influenceType");
        vk2 vk2Var = this.a;
        vk2Var.i(vk2Var.f(), "PREFS_OS_OUTCOMES_CURRENT_IAM_INFLUENCE", oSInfluenceType.toString());
    }

    public final void b(OSInfluenceType oSInfluenceType) {
        fs1.f(oSInfluenceType, "influenceType");
        vk2 vk2Var = this.a;
        vk2Var.i(vk2Var.f(), "PREFS_OS_OUTCOMES_CURRENT_SESSION", oSInfluenceType.toString());
    }

    public final void c(String str) {
        vk2 vk2Var = this.a;
        vk2Var.i(vk2Var.f(), "PREFS_OS_LAST_ATTRIBUTED_NOTIFICATION_OPEN", str);
    }

    public final String d() {
        vk2 vk2Var = this.a;
        return vk2Var.e(vk2Var.f(), "PREFS_OS_LAST_ATTRIBUTED_NOTIFICATION_OPEN", null);
    }

    public final OSInfluenceType e() {
        String str = OSInfluenceType.UNATTRIBUTED.toString();
        vk2 vk2Var = this.a;
        return OSInfluenceType.Companion.a(vk2Var.e(vk2Var.f(), "PREFS_OS_OUTCOMES_CURRENT_IAM_INFLUENCE", str));
    }

    public final int f() {
        vk2 vk2Var = this.a;
        return vk2Var.d(vk2Var.f(), "PREFS_OS_IAM_INDIRECT_ATTRIBUTION_WINDOW", 1440);
    }

    public final int g() {
        vk2 vk2Var = this.a;
        return vk2Var.d(vk2Var.f(), "PREFS_OS_IAM_LIMIT", 10);
    }

    public final JSONArray h() throws JSONException {
        vk2 vk2Var = this.a;
        String e = vk2Var.e(vk2Var.f(), "PREFS_OS_LAST_IAMS_RECEIVED", HttpUrl.PATH_SEGMENT_ENCODE_SET_URI);
        return e != null ? new JSONArray(e) : new JSONArray();
    }

    public final JSONArray i() throws JSONException {
        vk2 vk2Var = this.a;
        String e = vk2Var.e(vk2Var.f(), "PREFS_OS_LAST_NOTIFICATIONS_RECEIVED", HttpUrl.PATH_SEGMENT_ENCODE_SET_URI);
        return e != null ? new JSONArray(e) : new JSONArray();
    }

    public final OSInfluenceType j() {
        vk2 vk2Var = this.a;
        return OSInfluenceType.Companion.a(vk2Var.e(vk2Var.f(), "PREFS_OS_OUTCOMES_CURRENT_SESSION", OSInfluenceType.UNATTRIBUTED.toString()));
    }

    public final int k() {
        vk2 vk2Var = this.a;
        return vk2Var.d(vk2Var.f(), "PREFS_OS_INDIRECT_ATTRIBUTION_WINDOW", 1440);
    }

    public final int l() {
        vk2 vk2Var = this.a;
        return vk2Var.d(vk2Var.f(), "PREFS_OS_NOTIFICATION_LIMIT", 10);
    }

    public final boolean m() {
        vk2 vk2Var = this.a;
        return vk2Var.j(vk2Var.f(), "PREFS_OS_DIRECT_ENABLED", false);
    }

    public final boolean n() {
        vk2 vk2Var = this.a;
        return vk2Var.j(vk2Var.f(), "PREFS_OS_INDIRECT_ENABLED", false);
    }

    public final boolean o() {
        vk2 vk2Var = this.a;
        return vk2Var.j(vk2Var.f(), "PREFS_OS_UNATTRIBUTED_ENABLED", false);
    }

    public final void p(JSONArray jSONArray) {
        fs1.f(jSONArray, "iams");
        vk2 vk2Var = this.a;
        vk2Var.i(vk2Var.f(), "PREFS_OS_LAST_IAMS_RECEIVED", jSONArray.toString());
    }

    public final void q(c1.e eVar) {
        fs1.f(eVar, "influenceParams");
        vk2 vk2Var = this.a;
        vk2Var.b(vk2Var.f(), "PREFS_OS_DIRECT_ENABLED", eVar.e());
        vk2 vk2Var2 = this.a;
        vk2Var2.b(vk2Var2.f(), "PREFS_OS_INDIRECT_ENABLED", eVar.f());
        vk2 vk2Var3 = this.a;
        vk2Var3.b(vk2Var3.f(), "PREFS_OS_UNATTRIBUTED_ENABLED", eVar.g());
        vk2 vk2Var4 = this.a;
        vk2Var4.a(vk2Var4.f(), "PREFS_OS_NOTIFICATION_LIMIT", eVar.d());
        vk2 vk2Var5 = this.a;
        vk2Var5.a(vk2Var5.f(), "PREFS_OS_INDIRECT_ATTRIBUTION_WINDOW", eVar.c());
        vk2 vk2Var6 = this.a;
        vk2Var6.a(vk2Var6.f(), "PREFS_OS_IAM_LIMIT", eVar.a());
        vk2 vk2Var7 = this.a;
        vk2Var7.a(vk2Var7.f(), "PREFS_OS_IAM_INDIRECT_ATTRIBUTION_WINDOW", eVar.b());
    }

    public final void r(JSONArray jSONArray) {
        fs1.f(jSONArray, "notifications");
        vk2 vk2Var = this.a;
        vk2Var.i(vk2Var.f(), "PREFS_OS_LAST_NOTIFICATIONS_RECEIVED", jSONArray.toString());
    }
}
