package defpackage;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: SequencesJVM.kt */
/* renamed from: c60  reason: default package */
/* loaded from: classes2.dex */
public final class c60<T> implements ol3<T> {
    public final AtomicReference<ol3<T>> a;

    public c60(ol3<? extends T> ol3Var) {
        fs1.f(ol3Var, "sequence");
        this.a = new AtomicReference<>(ol3Var);
    }

    @Override // defpackage.ol3
    public Iterator<T> iterator() {
        ol3<T> andSet = this.a.getAndSet(null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
