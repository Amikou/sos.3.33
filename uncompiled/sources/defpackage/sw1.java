package defpackage;

/* compiled from: KFunction.kt */
/* renamed from: sw1  reason: default package */
/* loaded from: classes2.dex */
public interface sw1<R> extends pw1<R>, pd1<R> {
    boolean isExternal();

    boolean isInfix();

    boolean isInline();

    boolean isOperator();

    @Override // defpackage.pw1
    boolean isSuspend();
}
