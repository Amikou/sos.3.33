package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkInfo;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/* compiled from: WorkerWrapper.java */
/* renamed from: cr4  reason: default package */
/* loaded from: classes.dex */
public class cr4 implements Runnable {
    public static final String x0 = v12.f("WorkerWrapper");
    public Context a;
    public String f0;
    public List<cd3> g0;
    public WorkerParameters.a h0;
    public tq4 i0;
    public ListenableWorker j0;
    public q34 k0;
    public androidx.work.a m0;
    public t81 n0;
    public WorkDatabase o0;
    public uq4 p0;
    public jm0 q0;
    public xq4 r0;
    public List<String> s0;
    public String t0;
    public volatile boolean w0;
    public ListenableWorker.a l0 = ListenableWorker.a.a();
    public wm3<Boolean> u0 = wm3.t();
    public l02<ListenableWorker.a> v0 = null;

    /* compiled from: WorkerWrapper.java */
    /* renamed from: cr4$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ l02 a;
        public final /* synthetic */ wm3 f0;

        public a(l02 l02Var, wm3 wm3Var) {
            this.a = l02Var;
            this.f0 = wm3Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                this.a.get();
                v12.c().a(cr4.x0, String.format("Starting work for %s", cr4.this.i0.c), new Throwable[0]);
                cr4 cr4Var = cr4.this;
                cr4Var.v0 = cr4Var.j0.p();
                this.f0.r(cr4.this.v0);
            } catch (Throwable th) {
                this.f0.q(th);
            }
        }
    }

    /* compiled from: WorkerWrapper.java */
    /* renamed from: cr4$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ wm3 a;
        public final /* synthetic */ String f0;

        public b(wm3 wm3Var, String str) {
            this.a = wm3Var;
            this.f0 = str;
        }

        @Override // java.lang.Runnable
        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                try {
                    ListenableWorker.a aVar = (ListenableWorker.a) this.a.get();
                    if (aVar == null) {
                        v12.c().b(cr4.x0, String.format("%s returned a null result. Treating it as a failure.", cr4.this.i0.c), new Throwable[0]);
                    } else {
                        v12.c().a(cr4.x0, String.format("%s returned a %s result.", cr4.this.i0.c, aVar), new Throwable[0]);
                        cr4.this.l0 = aVar;
                    }
                } catch (InterruptedException e) {
                    e = e;
                    v12.c().b(cr4.x0, String.format("%s failed because it threw an exception/error", this.f0), e);
                } catch (CancellationException e2) {
                    v12.c().d(cr4.x0, String.format("%s was cancelled", this.f0), e2);
                } catch (ExecutionException e3) {
                    e = e3;
                    v12.c().b(cr4.x0, String.format("%s failed because it threw an exception/error", this.f0), e);
                }
            } finally {
                cr4.this.f();
            }
        }
    }

    /* compiled from: WorkerWrapper.java */
    /* renamed from: cr4$c */
    /* loaded from: classes.dex */
    public static class c {
        public Context a;
        public ListenableWorker b;
        public t81 c;
        public q34 d;
        public androidx.work.a e;
        public WorkDatabase f;
        public String g;
        public List<cd3> h;
        public WorkerParameters.a i = new WorkerParameters.a();

        public c(Context context, androidx.work.a aVar, q34 q34Var, t81 t81Var, WorkDatabase workDatabase, String str) {
            this.a = context.getApplicationContext();
            this.d = q34Var;
            this.c = t81Var;
            this.e = aVar;
            this.f = workDatabase;
            this.g = str;
        }

        public cr4 a() {
            return new cr4(this);
        }

        public c b(WorkerParameters.a aVar) {
            if (aVar != null) {
                this.i = aVar;
            }
            return this;
        }

        public c c(List<cd3> list) {
            this.h = list;
            return this;
        }
    }

    public cr4(c cVar) {
        this.a = cVar.a;
        this.k0 = cVar.d;
        this.n0 = cVar.c;
        this.f0 = cVar.g;
        this.g0 = cVar.h;
        this.h0 = cVar.i;
        this.j0 = cVar.b;
        this.m0 = cVar.e;
        WorkDatabase workDatabase = cVar.f;
        this.o0 = workDatabase;
        this.p0 = workDatabase.P();
        this.q0 = this.o0.H();
        this.r0 = this.o0.Q();
    }

    public final String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.f0);
        sb.append(", tags={ ");
        boolean z = true;
        for (String str : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(" } ]");
        return sb.toString();
    }

    public l02<Boolean> b() {
        return this.u0;
    }

    public final void c(ListenableWorker.a aVar) {
        if (aVar instanceof ListenableWorker.a.c) {
            v12.c().d(x0, String.format("Worker result SUCCESS for %s", this.t0), new Throwable[0]);
            if (this.i0.d()) {
                h();
            } else {
                m();
            }
        } else if (aVar instanceof ListenableWorker.a.b) {
            v12.c().d(x0, String.format("Worker result RETRY for %s", this.t0), new Throwable[0]);
            g();
        } else {
            v12.c().d(x0, String.format("Worker result FAILURE for %s", this.t0), new Throwable[0]);
            if (this.i0.d()) {
                h();
            } else {
                l();
            }
        }
    }

    public void d() {
        boolean z;
        this.w0 = true;
        n();
        l02<ListenableWorker.a> l02Var = this.v0;
        if (l02Var != null) {
            z = l02Var.isDone();
            this.v0.cancel(true);
        } else {
            z = false;
        }
        ListenableWorker listenableWorker = this.j0;
        if (listenableWorker != null && !z) {
            listenableWorker.q();
        } else {
            v12.c().a(x0, String.format("WorkSpec %s is already done. Not interrupting.", this.i0), new Throwable[0]);
        }
    }

    public final void e(String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            if (this.p0.k(str2) != WorkInfo.State.CANCELLED) {
                this.p0.a(WorkInfo.State.FAILED, str2);
            }
            linkedList.addAll(this.q0.a(str2));
        }
    }

    public void f() {
        if (!n()) {
            this.o0.e();
            try {
                WorkInfo.State k = this.p0.k(this.f0);
                this.o0.O().delete(this.f0);
                if (k == null) {
                    i(false);
                } else if (k == WorkInfo.State.RUNNING) {
                    c(this.l0);
                } else if (!k.isFinished()) {
                    g();
                }
                this.o0.E();
            } finally {
                this.o0.j();
            }
        }
        List<cd3> list = this.g0;
        if (list != null) {
            for (cd3 cd3Var : list) {
                cd3Var.d(this.f0);
            }
            gd3.b(this.m0, this.o0, this.g0);
        }
    }

    public final void g() {
        this.o0.e();
        try {
            this.p0.a(WorkInfo.State.ENQUEUED, this.f0);
            this.p0.r(this.f0, System.currentTimeMillis());
            this.p0.b(this.f0, -1L);
            this.o0.E();
        } finally {
            this.o0.j();
            i(true);
        }
    }

    public final void h() {
        this.o0.e();
        try {
            this.p0.r(this.f0, System.currentTimeMillis());
            this.p0.a(WorkInfo.State.ENQUEUED, this.f0);
            this.p0.m(this.f0);
            this.p0.b(this.f0, -1L);
            this.o0.E();
        } finally {
            this.o0.j();
            i(false);
        }
    }

    public final void i(boolean z) {
        ListenableWorker listenableWorker;
        this.o0.e();
        try {
            if (!this.o0.P().i()) {
                so2.a(this.a, RescheduleReceiver.class, false);
            }
            if (z) {
                this.p0.a(WorkInfo.State.ENQUEUED, this.f0);
                this.p0.b(this.f0, -1L);
            }
            if (this.i0 != null && (listenableWorker = this.j0) != null && listenableWorker.j()) {
                this.n0.b(this.f0);
            }
            this.o0.E();
            this.o0.j();
            this.u0.p(Boolean.valueOf(z));
        } catch (Throwable th) {
            this.o0.j();
            throw th;
        }
    }

    public final void j() {
        WorkInfo.State k = this.p0.k(this.f0);
        if (k == WorkInfo.State.RUNNING) {
            v12.c().a(x0, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", this.f0), new Throwable[0]);
            i(true);
            return;
        }
        v12.c().a(x0, String.format("Status for %s is %s; not doing any work", this.f0, k), new Throwable[0]);
        i(false);
    }

    public final void k() {
        androidx.work.b b2;
        if (n()) {
            return;
        }
        this.o0.e();
        try {
            tq4 l = this.p0.l(this.f0);
            this.i0 = l;
            if (l == null) {
                v12.c().b(x0, String.format("Didn't find WorkSpec for id %s", this.f0), new Throwable[0]);
                i(false);
                this.o0.E();
            } else if (l.b != WorkInfo.State.ENQUEUED) {
                j();
                this.o0.E();
                v12.c().a(x0, String.format("%s is not in ENQUEUED state. Nothing more to do.", this.i0.c), new Throwable[0]);
            } else {
                if (l.d() || this.i0.c()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    tq4 tq4Var = this.i0;
                    if (!(tq4Var.n == 0) && currentTimeMillis < tq4Var.a()) {
                        v12.c().a(x0, String.format("Delaying execution for %s because it is being executed before schedule.", this.i0.c), new Throwable[0]);
                        i(true);
                        this.o0.E();
                        return;
                    }
                }
                this.o0.E();
                this.o0.j();
                if (this.i0.d()) {
                    b2 = this.i0.e;
                } else {
                    yq1 b3 = this.m0.f().b(this.i0.d);
                    if (b3 == null) {
                        v12.c().b(x0, String.format("Could not create Input Merger %s", this.i0.d), new Throwable[0]);
                        l();
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(this.i0.e);
                    arrayList.addAll(this.p0.p(this.f0));
                    b2 = b3.b(arrayList);
                }
                WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.f0), b2, this.s0, this.h0, this.i0.k, this.m0.e(), this.k0, this.m0.m(), new pq4(this.o0, this.k0), new bq4(this.o0, this.n0, this.k0));
                if (this.j0 == null) {
                    this.j0 = this.m0.m().b(this.a, this.i0.c, workerParameters);
                }
                ListenableWorker listenableWorker = this.j0;
                if (listenableWorker == null) {
                    v12.c().b(x0, String.format("Could not create Worker %s", this.i0.c), new Throwable[0]);
                    l();
                } else if (listenableWorker.l()) {
                    v12.c().b(x0, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", this.i0.c), new Throwable[0]);
                    l();
                } else {
                    this.j0.o();
                    if (o()) {
                        if (n()) {
                            return;
                        }
                        wm3 t = wm3.t();
                        aq4 aq4Var = new aq4(this.a, this.i0, this.j0, workerParameters.b(), this.k0);
                        this.k0.a().execute(aq4Var);
                        l02<Void> a2 = aq4Var.a();
                        a2.d(new a(a2, t), this.k0.a());
                        t.d(new b(t, this.t0), this.k0.c());
                        return;
                    }
                    j();
                }
            }
        } finally {
            this.o0.j();
        }
    }

    public void l() {
        this.o0.e();
        try {
            e(this.f0);
            this.p0.g(this.f0, ((ListenableWorker.a.C0068a) this.l0).e());
            this.o0.E();
        } finally {
            this.o0.j();
            i(false);
        }
    }

    public final void m() {
        this.o0.e();
        try {
            this.p0.a(WorkInfo.State.SUCCEEDED, this.f0);
            this.p0.g(this.f0, ((ListenableWorker.a.c) this.l0).e());
            long currentTimeMillis = System.currentTimeMillis();
            for (String str : this.q0.a(this.f0)) {
                if (this.p0.k(str) == WorkInfo.State.BLOCKED && this.q0.b(str)) {
                    v12.c().d(x0, String.format("Setting status to enqueued for %s", str), new Throwable[0]);
                    this.p0.a(WorkInfo.State.ENQUEUED, str);
                    this.p0.r(str, currentTimeMillis);
                }
            }
            this.o0.E();
        } finally {
            this.o0.j();
            i(false);
        }
    }

    public final boolean n() {
        if (this.w0) {
            v12.c().a(x0, String.format("Work interrupted for %s", this.t0), new Throwable[0]);
            WorkInfo.State k = this.p0.k(this.f0);
            if (k == null) {
                i(false);
            } else {
                i(!k.isFinished());
            }
            return true;
        }
        return false;
    }

    public final boolean o() {
        this.o0.e();
        try {
            boolean z = true;
            if (this.p0.k(this.f0) == WorkInfo.State.ENQUEUED) {
                this.p0.a(WorkInfo.State.RUNNING, this.f0);
                this.p0.q(this.f0);
            } else {
                z = false;
            }
            this.o0.E();
            return z;
        } finally {
            this.o0.j();
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        List<String> b2 = this.r0.b(this.f0);
        this.s0 = b2;
        this.t0 = a(b2);
        k();
    }
}
