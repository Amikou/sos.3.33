package defpackage;

import com.google.android.gms.tasks.a;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.e;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: da5  reason: default package */
/* loaded from: classes.dex */
public final class da5<TResult, TContinuationResult> implements l46<TResult> {
    public final Executor a;
    public final a<TResult, TContinuationResult> b;
    public final e<TContinuationResult> c;

    public da5(Executor executor, a<TResult, TContinuationResult> aVar, e<TContinuationResult> eVar) {
        this.a = executor;
        this.b = aVar;
        this.c = eVar;
    }

    @Override // defpackage.l46
    public final void c(c<TResult> cVar) {
        this.a.execute(new df5(this, cVar));
    }
}
