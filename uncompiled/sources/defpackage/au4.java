package defpackage;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.play.core.assetpacks.AssetPackState;
import java.util.ArrayList;
import java.util.concurrent.Executor;

/* renamed from: au4  reason: default package */
/* loaded from: classes2.dex */
public final class au4 extends mu4<AssetPackState> {
    public final zv4 g;
    public final dv4 h;
    public final cw4<zy4> i;
    public final vu4 j;
    public final fv4 k;
    public final cw4<Executor> l;
    public final cw4<Executor> m;
    public final Handler n;

    public au4(Context context, zv4 zv4Var, dv4 dv4Var, cw4<zy4> cw4Var, fv4 fv4Var, vu4 vu4Var, cw4<Executor> cw4Var2, cw4<Executor> cw4Var3) {
        super(new it4("AssetPackServiceListenerRegistry"), new IntentFilter("com.google.android.play.core.assetpacks.receiver.ACTION_SESSION_UPDATE"), context);
        this.n = new Handler(Looper.getMainLooper());
        this.g = zv4Var;
        this.h = dv4Var;
        this.i = cw4Var;
        this.k = fv4Var;
        this.j = vu4Var;
        this.l = cw4Var2;
        this.m = cw4Var3;
    }

    @Override // defpackage.mu4
    public final void a(Context context, Intent intent) {
        final Bundle bundleExtra = intent.getBundleExtra("com.google.android.play.core.assetpacks.receiver.EXTRA_SESSION_STATE");
        if (bundleExtra == null) {
            this.a.b("Empty bundle received from broadcast.", new Object[0]);
            return;
        }
        ArrayList<String> stringArrayList = bundleExtra.getStringArrayList("pack_names");
        if (stringArrayList == null || stringArrayList.size() != 1) {
            this.a.b("Corrupt bundle received from broadcast.", new Object[0]);
            return;
        }
        final AssetPackState d = AssetPackState.d(bundleExtra, stringArrayList.get(0), this.k, du4.c);
        this.a.a("ListenerRegistryBroadcastReceiver.onReceive: %s", d);
        PendingIntent pendingIntent = (PendingIntent) bundleExtra.getParcelable("confirmation_intent");
        if (pendingIntent != null) {
            this.j.a(pendingIntent);
        }
        this.m.a().execute(new Runnable(this, bundleExtra, d) { // from class: wt4
            public final au4 a;
            public final Bundle f0;
            public final AssetPackState g0;

            {
                this.a = this;
                this.f0 = bundleExtra;
                this.g0 = d;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.h(this.f0, this.g0);
            }
        });
        this.l.a().execute(new Runnable(this, bundleExtra) { // from class: yt4
            public final au4 a;
            public final Bundle f0;

            {
                this.a = this;
                this.f0 = bundleExtra;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.g(this.f0);
            }
        });
    }

    public final void f(final AssetPackState assetPackState) {
        this.n.post(new Runnable(this, assetPackState) { // from class: ut4
            public final au4 a;
            public final AssetPackState f0;

            {
                this.a = this;
                this.f0 = assetPackState;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.d(this.f0);
            }
        });
    }

    public final /* synthetic */ void g(Bundle bundle) {
        if (this.g.d(bundle)) {
            this.h.a();
        }
    }

    public final /* synthetic */ void h(Bundle bundle, AssetPackState assetPackState) {
        if (this.g.e(bundle)) {
            f(assetPackState);
            this.i.a().a();
        }
    }
}
