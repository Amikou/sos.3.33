package defpackage;

import com.onesignal.influence.domain.a;
import java.util.List;
import java.util.Set;

/* compiled from: OSOutcomeEventsRepository.kt */
/* renamed from: ik2  reason: default package */
/* loaded from: classes2.dex */
public interface ik2 {
    List<a> a(String str, List<a> list);

    void b(dk2 dk2Var);

    List<dk2> c();

    void d(Set<String> set);

    void e(dk2 dk2Var);

    void f(String str, String str2);

    void g(String str, int i, dk2 dk2Var, ym2 ym2Var);

    Set<String> h();

    void i(dk2 dk2Var);
}
