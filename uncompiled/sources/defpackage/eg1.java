package defpackage;

import com.bumptech.glide.load.DecodeFormat;

/* compiled from: GifOptions.java */
/* renamed from: eg1  reason: default package */
/* loaded from: classes.dex */
public final class eg1 {
    public static final mn2<DecodeFormat> a = mn2.f("com.bumptech.glide.load.resource.gif.GifOptions.DecodeFormat", DecodeFormat.DEFAULT);
    public static final mn2<Boolean> b = mn2.f("com.bumptech.glide.load.resource.gif.GifOptions.DisableAnimation", Boolean.FALSE);
}
