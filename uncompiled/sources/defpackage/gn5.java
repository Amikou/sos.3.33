package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: gn5  reason: default package */
/* loaded from: classes.dex */
public final class gn5 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ long g0;
    public final /* synthetic */ Bundle h0;
    public final /* synthetic */ boolean i0;
    public final /* synthetic */ boolean j0;
    public final /* synthetic */ boolean k0;
    public final /* synthetic */ String l0;
    public final /* synthetic */ dp5 m0;

    public gn5(dp5 dp5Var, String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.m0 = dp5Var;
        this.a = str;
        this.f0 = str2;
        this.g0 = j;
        this.h0 = bundle;
        this.i0 = z;
        this.j0 = z2;
        this.k0 = z3;
        this.l0 = str3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.m0.Z(this.a, this.f0, this.g0, this.h0, this.i0, this.j0, this.k0, this.l0);
    }
}
