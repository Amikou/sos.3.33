package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogPurchaseMethodBinding.java */
/* renamed from: wn0  reason: default package */
/* loaded from: classes2.dex */
public final class wn0 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final AppCompatTextView d;

    public wn0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialCardView materialCardView, MaterialButton materialButton2, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, AppCompatImageView appCompatImageView, MaterialTextView materialTextView, AppCompatTextView appCompatTextView3) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = appCompatTextView3;
    }

    public static wn0 a(View view) {
        int i = R.id.btn_mp_purchase;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btn_mp_purchase);
        if (materialButton != null) {
            i = R.id.cc_moonpay;
            MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.cc_moonpay);
            if (materialCardView != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton2 != null) {
                    i = R.id.img_mp_content;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.img_mp_content);
                    if (appCompatTextView != null) {
                        i = R.id.img_mp_fee_platform;
                        AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.img_mp_fee_platform);
                        if (appCompatTextView2 != null) {
                            i = R.id.img_mp_logo;
                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_mp_logo);
                            if (appCompatImageView != null) {
                                i = R.id.tvDialogTitle;
                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tvDialogTitle);
                                if (materialTextView != null) {
                                    i = R.id.txt_mp_fee;
                                    AppCompatTextView appCompatTextView3 = (AppCompatTextView) ai4.a(view, R.id.txt_mp_fee);
                                    if (appCompatTextView3 != null) {
                                        return new wn0((ConstraintLayout) view, materialButton, materialCardView, materialButton2, appCompatTextView, appCompatTextView2, appCompatImageView, materialTextView, appCompatTextView3);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
