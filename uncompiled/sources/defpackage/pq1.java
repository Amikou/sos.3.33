package defpackage;

import androidx.media3.common.j;
import androidx.media3.datasource.b;
import androidx.media3.datasource.f;
import defpackage.ny;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: InitializationChunk.java */
/* renamed from: pq1  reason: default package */
/* loaded from: classes.dex */
public final class pq1 extends my {
    public final ny j;
    public ny.b k;
    public long l;
    public volatile boolean m;

    public pq1(b bVar, je0 je0Var, j jVar, int i, Object obj, ny nyVar) {
        super(bVar, je0Var, 2, jVar, i, obj, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        this.j = nyVar;
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public void a() throws IOException {
        if (this.l == 0) {
            this.j.c(this.k, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }
        try {
            je0 e = this.b.e(this.l);
            f fVar = this.i;
            hj0 hj0Var = new hj0(fVar, e.f, fVar.b(e));
            while (!this.m && this.j.b(hj0Var)) {
            }
            this.l = hj0Var.getPosition() - this.b.f;
        } finally {
            he0.a(this.i);
        }
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public void c() {
        this.m = true;
    }

    public void f(ny.b bVar) {
        this.k = bVar;
    }
}
