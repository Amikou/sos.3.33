package defpackage;

import androidx.media3.common.j;
import androidx.media3.datasource.b;

/* compiled from: MediaChunk.java */
/* renamed from: s52  reason: default package */
/* loaded from: classes.dex */
public abstract class s52 extends my {
    public final long j;

    public s52(b bVar, je0 je0Var, j jVar, int i, Object obj, long j, long j2, long j3) {
        super(bVar, je0Var, 1, jVar, i, obj, j, j2);
        ii.e(jVar);
        this.j = j3;
    }

    public long f() {
        long j = this.j;
        if (j != -1) {
            return 1 + j;
        }
        return -1L;
    }

    public abstract boolean g();
}
