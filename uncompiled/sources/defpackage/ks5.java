package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ks5  reason: default package */
/* loaded from: classes.dex */
public final class ks5 extends wn5<Float> implements gt5<Float>, vw5 {
    public float[] f0;
    public int g0;

    static {
        new ks5(new float[0], 0).zzb();
    }

    public ks5() {
        this(new float[10], 0);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        float floatValue = ((Float) obj).floatValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            float[] fArr = this.f0;
            if (i2 < fArr.length) {
                System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
            } else {
                float[] fArr2 = new float[((i2 * 3) / 2) + 1];
                System.arraycopy(fArr, 0, fArr2, 0, i);
                System.arraycopy(this.f0, i, fArr2, i + 1, this.g0 - i);
                this.f0 = fArr2;
            }
            this.f0[i] = floatValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(m(i));
    }

    @Override // defpackage.wn5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Float> collection) {
        e();
        vs5.d(collection);
        if (!(collection instanceof ks5)) {
            return super.addAll(collection);
        }
        ks5 ks5Var = (ks5) collection;
        int i = ks5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.f0;
            if (i3 > fArr.length) {
                this.f0 = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(ks5Var.f0, 0, this.f0, this.g0, ks5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.gt5
    public final /* synthetic */ gt5<Float> d(int i) {
        if (i >= this.g0) {
            return new ks5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ks5)) {
            return super.equals(obj);
        }
        ks5 ks5Var = (ks5) obj;
        if (this.g0 != ks5Var.g0) {
            return false;
        }
        float[] fArr = ks5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (Float.floatToIntBits(this.f0[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        k(i);
        return Float.valueOf(this.f0[i]);
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f0[i2]);
        }
        return i;
    }

    public final void i(float f) {
        e();
        int i = this.g0;
        float[] fArr = this.f0;
        if (i == fArr.length) {
            float[] fArr2 = new float[((i * 3) / 2) + 1];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.f0 = fArr2;
        }
        float[] fArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        fArr3[i2] = f;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Float) {
            float floatValue = ((Float) obj).floatValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == floatValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    public final void k(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(m(i));
        }
    }

    public final String m(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        e();
        k(i);
        float[] fArr = this.f0;
        float f = fArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Float.valueOf(f);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            float[] fArr = this.f0;
            System.arraycopy(fArr, i2, fArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        e();
        k(i);
        float[] fArr = this.f0;
        float f = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    public ks5(float[] fArr, int i) {
        this.f0 = fArr;
        this.g0 = i;
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* synthetic */ boolean add(Object obj) {
        i(((Float) obj).floatValue());
        return true;
    }
}
