package defpackage;

import java.util.concurrent.Executor;

/* renamed from: mx4  reason: default package */
/* loaded from: classes2.dex */
public final class mx4<ResultT> implements px4<ResultT> {
    public final Executor a;
    public final Object b = new Object();
    public final tm2<? super ResultT> c;

    public mx4(Executor executor, tm2<? super ResultT> tm2Var) {
        this.a = executor;
        this.c = tm2Var;
    }

    @Override // defpackage.px4
    public final void a(l34<ResultT> l34Var) {
        if (l34Var.f()) {
            synchronized (this.b) {
                if (this.c == null) {
                    return;
                }
                this.a.execute(new jx4(this, l34Var));
            }
        }
    }
}
