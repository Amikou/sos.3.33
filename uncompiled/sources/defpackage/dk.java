package defpackage;

import androidx.room.a;
import androidx.room.b;
import defpackage.tw3;

/* compiled from: AutoClosingRoomOpenHelperFactory.java */
/* renamed from: dk  reason: default package */
/* loaded from: classes.dex */
public final class dk implements tw3.c {
    public final tw3.c a;
    public final a b;

    public dk(tw3.c cVar, a aVar) {
        this.a = cVar;
        this.b = aVar;
    }

    @Override // defpackage.tw3.c
    /* renamed from: b */
    public b a(tw3.b bVar) {
        return new b(this.a.a(bVar), this.b);
    }
}
