package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;

/* compiled from: WalletConnectFragmentDirections.java */
/* renamed from: bm4  reason: default package */
/* loaded from: classes2.dex */
public class bm4 {

    /* compiled from: WalletConnectFragmentDirections.java */
    /* renamed from: bm4$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("roomConnectedInfo")) {
                RoomConnectedInfo roomConnectedInfo = (RoomConnectedInfo) this.a.get("roomConnectedInfo");
                if (!Parcelable.class.isAssignableFrom(RoomConnectedInfo.class) && roomConnectedInfo != null) {
                    if (Serializable.class.isAssignableFrom(RoomConnectedInfo.class)) {
                        bundle.putSerializable("roomConnectedInfo", (Serializable) Serializable.class.cast(roomConnectedInfo));
                    } else {
                        throw new UnsupportedOperationException(RoomConnectedInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("roomConnectedInfo", (Parcelable) Parcelable.class.cast(roomConnectedInfo));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_to_walletConnectDetailFragment;
        }

        public RoomConnectedInfo c() {
            return (RoomConnectedInfo) this.a.get("roomConnectedInfo");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("roomConnectedInfo") != bVar.a.containsKey("roomConnectedInfo")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionToWalletConnectDetailFragment(actionId=" + b() + "){roomConnectedInfo=" + c() + "}";
        }

        public b(RoomConnectedInfo roomConnectedInfo) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (roomConnectedInfo != null) {
                hashMap.put("roomConnectedInfo", roomConnectedInfo);
                return;
            }
            throw new IllegalArgumentException("Argument \"roomConnectedInfo\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(RoomConnectedInfo roomConnectedInfo) {
        return new b(roomConnectedInfo);
    }
}
