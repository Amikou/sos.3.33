package defpackage;

import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.c;

/* compiled from: BeanUtil.java */
/* renamed from: yo  reason: default package */
/* loaded from: classes.dex */
public class yo {
    public static boolean a(AnnotatedMethod annotatedMethod) {
        String D;
        Class<?> rawType = annotatedMethod.getRawType();
        return rawType != null && rawType.isArray() && (D = c.D(rawType.getComponentType())) != null && D.contains(".cglib") && (D.startsWith("net.sf.cglib") || D.startsWith("org.hibernate.repackage.cglib") || D.startsWith("org.springframework.cglib"));
    }

    public static boolean b(AnnotatedMethod annotatedMethod) {
        String D;
        Class<?> rawType = annotatedMethod.getRawType();
        return (rawType == null || rawType.isArray() || (D = c.D(rawType)) == null || !D.startsWith("groovy.lang")) ? false : true;
    }

    public static String c(String str, int i) {
        int length = str.length();
        if (length == i) {
            return null;
        }
        char charAt = str.charAt(i);
        char lowerCase = Character.toLowerCase(charAt);
        if (charAt == lowerCase) {
            return str.substring(i);
        }
        StringBuilder sb = new StringBuilder(length - i);
        sb.append(lowerCase);
        while (true) {
            i++;
            if (i >= length) {
                break;
            }
            char charAt2 = str.charAt(i);
            char lowerCase2 = Character.toLowerCase(charAt2);
            if (charAt2 == lowerCase2) {
                sb.append((CharSequence) str, i, length);
                break;
            }
            sb.append(lowerCase2);
        }
        return sb.toString();
    }

    public static String d(AnnotatedMethod annotatedMethod, boolean z) {
        String name = annotatedMethod.getName();
        String e = e(annotatedMethod, name, z);
        return e == null ? g(annotatedMethod, name, z) : e;
    }

    public static String e(AnnotatedMethod annotatedMethod, String str, boolean z) {
        if (str.startsWith("is")) {
            Class<?> rawType = annotatedMethod.getRawType();
            if (rawType == Boolean.class || rawType == Boolean.TYPE) {
                if (z) {
                    return h(str, 2);
                }
                return c(str, 2);
            }
            return null;
        }
        return null;
    }

    public static String f(AnnotatedMethod annotatedMethod, String str, boolean z) {
        String name = annotatedMethod.getName();
        if (name.startsWith(str)) {
            if (z) {
                return h(name, str.length());
            }
            return c(name, str.length());
        }
        return null;
    }

    public static String g(AnnotatedMethod annotatedMethod, String str, boolean z) {
        if (str.startsWith("get")) {
            if ("getCallbacks".equals(str)) {
                if (a(annotatedMethod)) {
                    return null;
                }
            } else if ("getMetaClass".equals(str) && b(annotatedMethod)) {
                return null;
            }
            if (z) {
                return h(str, 3);
            }
            return c(str, 3);
        }
        return null;
    }

    public static String h(String str, int i) {
        int length = str.length();
        if (length == i) {
            return null;
        }
        char charAt = str.charAt(i);
        char lowerCase = Character.toLowerCase(charAt);
        if (charAt == lowerCase) {
            return str.substring(i);
        }
        int i2 = i + 1;
        if (i2 < length && Character.isUpperCase(str.charAt(i2))) {
            return str.substring(i);
        }
        StringBuilder sb = new StringBuilder(length - i);
        sb.append(lowerCase);
        sb.append((CharSequence) str, i2, length);
        return sb.toString();
    }
}
