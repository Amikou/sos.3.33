package defpackage;

/* compiled from: MarkerEdgeTreatment.java */
/* renamed from: d42  reason: default package */
/* loaded from: classes2.dex */
public final class d42 extends cu0 {
    public final float a;

    public d42(float f) {
        this.a = f - 0.001f;
    }

    @Override // defpackage.cu0
    public boolean a() {
        return true;
    }

    @Override // defpackage.cu0
    public void b(float f, float f2, float f3, rn3 rn3Var) {
        float sqrt = (float) ((this.a * Math.sqrt(2.0d)) / 2.0d);
        float sqrt2 = (float) Math.sqrt(Math.pow(this.a, 2.0d) - Math.pow(sqrt, 2.0d));
        rn3Var.n(f2 - sqrt, ((float) (-((this.a * Math.sqrt(2.0d)) - this.a))) + sqrt2);
        rn3Var.m(f2, (float) (-((this.a * Math.sqrt(2.0d)) - this.a)));
        rn3Var.m(f2 + sqrt, ((float) (-((this.a * Math.sqrt(2.0d)) - this.a))) + sqrt2);
    }
}
