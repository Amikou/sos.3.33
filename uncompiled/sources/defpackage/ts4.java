package defpackage;

import android.graphics.Matrix;
import android.graphics.RectF;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ZoomBounds.java */
/* renamed from: ts4  reason: default package */
/* loaded from: classes.dex */
public class ts4 {
    public static final Matrix e = new Matrix();
    public static final RectF f = new RectF();
    public final Settings a;
    public float b;
    public float c;
    public float d;

    /* compiled from: ZoomBounds.java */
    /* renamed from: ts4$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Settings.Fit.values().length];
            a = iArr;
            try {
                iArr[Settings.Fit.HORIZONTAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Settings.Fit.VERTICAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Settings.Fit.INSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[Settings.Fit.OUTSIDE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[Settings.Fit.NONE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public ts4(Settings settings) {
        this.a = settings;
    }

    public float a() {
        return this.d;
    }

    public float b() {
        return this.c;
    }

    public float c() {
        return this.b;
    }

    public float d(float f2, float f3) {
        return v42.f(f2, this.b / f3, this.c * f3);
    }

    public ts4 e(us3 us3Var) {
        float l = this.a.l();
        float k = this.a.k();
        float p = this.a.p();
        float o = this.a.o();
        if (l != Utils.FLOAT_EPSILON && k != Utils.FLOAT_EPSILON && p != Utils.FLOAT_EPSILON && o != Utils.FLOAT_EPSILON) {
            this.b = this.a.n();
            this.c = this.a.m();
            float e2 = us3Var.e();
            if (!us3.c(e2, Utils.FLOAT_EPSILON)) {
                if (this.a.i() == Settings.Fit.OUTSIDE) {
                    Matrix matrix = e;
                    matrix.setRotate(-e2);
                    RectF rectF = f;
                    rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, p, o);
                    matrix.mapRect(rectF);
                    p = rectF.width();
                    o = rectF.height();
                } else {
                    Matrix matrix2 = e;
                    matrix2.setRotate(e2);
                    RectF rectF2 = f;
                    rectF2.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, l, k);
                    matrix2.mapRect(rectF2);
                    l = rectF2.width();
                    k = rectF2.height();
                }
            }
            int i = a.a[this.a.i().ordinal()];
            if (i == 1) {
                this.d = p / l;
            } else if (i == 2) {
                this.d = o / k;
            } else if (i == 3) {
                this.d = Math.min(p / l, o / k);
            } else if (i != 4) {
                float f2 = this.b;
                this.d = f2 > Utils.FLOAT_EPSILON ? f2 : 1.0f;
            } else {
                this.d = Math.max(p / l, o / k);
            }
            if (this.b <= Utils.FLOAT_EPSILON) {
                this.b = this.d;
            }
            if (this.c <= Utils.FLOAT_EPSILON) {
                this.c = this.d;
            }
            if (this.d > this.c) {
                if (this.a.B()) {
                    this.c = this.d;
                } else {
                    this.d = this.c;
                }
            }
            float f3 = this.b;
            float f4 = this.c;
            if (f3 > f4) {
                this.b = f4;
            }
            if (this.d < this.b) {
                if (this.a.B()) {
                    this.b = this.d;
                } else {
                    this.d = this.b;
                }
            }
            return this;
        }
        this.d = 1.0f;
        this.c = 1.0f;
        this.b = 1.0f;
        return this;
    }
}
