package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: DoubleSummaryStatistics.java */
/* renamed from: iq0  reason: default package */
/* loaded from: classes2.dex */
public class iq0 implements hq0 {
    public long a;
    public double b;
    public double c;
    public double d;
    public double e = Double.POSITIVE_INFINITY;
    public double f = Double.NEGATIVE_INFINITY;

    public final double a() {
        return b() > 0 ? e() / b() : Utils.DOUBLE_EPSILON;
    }

    public final long b() {
        return this.a;
    }

    public final double c() {
        return this.f;
    }

    public final double d() {
        return this.e;
    }

    public final double e() {
        double d = this.b + this.c;
        return (Double.isNaN(d) && Double.isInfinite(this.d)) ? this.d : d;
    }

    public String toString() {
        return String.format("%s{count=%d, sum=%f, min=%f, average=%f, max=%f}", iq0.class.getSimpleName(), Long.valueOf(b()), Double.valueOf(e()), Double.valueOf(d()), Double.valueOf(a()), Double.valueOf(c()));
    }
}
