package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: a56  reason: default package */
/* loaded from: classes.dex */
public final class a56 extends c55 {
    public final b66 g0;

    public a56(b66 b66Var) {
        super("internal.registerCallback");
        this.g0 = b66Var;
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        vm5.a(this.a, 3, list);
        String zzc = wk5Var.a(list.get(0)).zzc();
        z55 a = wk5Var.a(list.get(1));
        if (a instanceof u55) {
            z55 a2 = wk5Var.a(list.get(2));
            if (a2 instanceof p55) {
                p55 p55Var = (p55) a2;
                if (p55Var.o("type")) {
                    this.g0.a(zzc, p55Var.o("priority") ? vm5.g(p55Var.e("priority").b().doubleValue()) : 1000, (u55) a, p55Var.e("type").zzc());
                    return z55.X;
                }
                throw new IllegalArgumentException("Undefined rule type");
            }
            throw new IllegalArgumentException("Invalid callback params");
        }
        throw new IllegalArgumentException("Invalid callback type");
    }
}
