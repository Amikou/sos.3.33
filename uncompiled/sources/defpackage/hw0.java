package defpackage;

import java.math.BigInteger;

/* compiled from: EthBlockNumber.java */
/* renamed from: hw0  reason: default package */
/* loaded from: classes3.dex */
public class hw0 extends i83<String> {
    public BigInteger getBlockNumber() {
        return ej2.decodeQuantity(getResult());
    }
}
