package defpackage;

import com.google.android.gms.measurement.internal.zzaa;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: hk5  reason: default package */
/* loaded from: classes.dex */
public final class hk5 implements Runnable {
    public final /* synthetic */ zzaa a;
    public final /* synthetic */ pl5 f0;

    public hk5(pl5 pl5Var, zzaa zzaaVar) {
        this.f0 = pl5Var;
        this.a = zzaaVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        fw5 fw5Var2;
        fw5 fw5Var3;
        fw5Var = this.f0.a;
        fw5Var.i();
        if (this.a.g0.I1() == null) {
            fw5Var3 = this.f0.a;
            fw5Var3.v(this.a);
            return;
        }
        fw5Var2 = this.f0.a;
        fw5Var2.t(this.a);
    }
}
