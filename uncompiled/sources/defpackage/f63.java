package defpackage;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import kotlin.text.MatcherMatchResult;

/* compiled from: Regex.kt */
/* renamed from: f63  reason: default package */
/* loaded from: classes2.dex */
public final class f63 {
    public static final /* synthetic */ h42 a(Matcher matcher, int i, CharSequence charSequence) {
        return f(matcher, i, charSequence);
    }

    public static final /* synthetic */ h42 b(Matcher matcher, CharSequence charSequence) {
        return g(matcher, charSequence);
    }

    public static final /* synthetic */ int e(Iterable iterable) {
        return j(iterable);
    }

    public static final h42 f(Matcher matcher, int i, CharSequence charSequence) {
        if (matcher.find(i)) {
            return new MatcherMatchResult(matcher, charSequence);
        }
        return null;
    }

    public static final h42 g(Matcher matcher, CharSequence charSequence) {
        if (matcher.matches()) {
            return new MatcherMatchResult(matcher, charSequence);
        }
        return null;
    }

    public static final sr1 h(MatchResult matchResult) {
        return u33.k(matchResult.start(), matchResult.end());
    }

    public static final sr1 i(MatchResult matchResult, int i) {
        return u33.k(matchResult.start(i), matchResult.end(i));
    }

    public static final int j(Iterable<? extends u61> iterable) {
        int i = 0;
        for (u61 u61Var : iterable) {
            i |= u61Var.getValue();
        }
        return i;
    }
}
