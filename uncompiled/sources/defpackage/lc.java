package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.CalculatorViewModel;

/* compiled from: AnchorPopUpBagButton.kt */
/* renamed from: lc  reason: default package */
/* loaded from: classes2.dex */
public final class lc {
    public final CalculatorViewModel a;
    public PopupWindow b;
    public final int c;
    public int d;

    /* compiled from: AnchorPopUpBagButton.kt */
    /* renamed from: lc$a */
    /* loaded from: classes2.dex */
    public static final class a implements SeekBar.OnSeekBarChangeListener {
        public final /* synthetic */ aq1 b;

        public a(aq1 aq1Var) {
            this.b = aq1Var;
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            lc.this.d = i + 1;
            MaterialButton materialButton = this.b.c;
            StringBuilder sb = new StringBuilder();
            sb.append(lc.this.d);
            sb.append('%');
            materialButton.setText(sb.toString());
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    public lc(CalculatorViewModel calculatorViewModel) {
        fs1.f(calculatorViewModel, "calculatorViewModel");
        this.a = calculatorViewModel;
        this.c = R.color.curve_green;
        this.d = 50;
    }

    public static final void l(fn0 fn0Var, View view) {
        fs1.f(fn0Var, "$this_run");
        CardView cardView = fn0Var.b;
        fs1.e(cardView, "ccToggleItemWrapper");
        cardView.setVisibility(8);
        CardView cardView2 = fn0Var.a;
        fs1.e(cardView2, "ccManageBagItemWrapper");
        cardView2.setVisibility(0);
    }

    public static final void m(lc lcVar, View view) {
        fs1.f(lcVar, "this$0");
        lcVar.a.s().postValue(Integer.valueOf(view.getId()));
        lcVar.h();
    }

    public static final void n(lc lcVar, View view) {
        fs1.f(lcVar, "this$0");
        lcVar.a.s().postValue(Integer.valueOf(view.getId()));
        lcVar.h();
    }

    public static final void o(lc lcVar, View view) {
        fs1.f(lcVar, "this$0");
        lcVar.j();
        lcVar.h();
    }

    public final PopupWindow g(View view, View view2) {
        PopupWindow popupWindow = new PopupWindow(view, (int) ((view2 == null ? -1 : view2.getWidth()) * 0.8d), -2);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void h() {
        PopupWindow popupWindow;
        if (!i() || (popupWindow = this.b) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final boolean i() {
        PopupWindow popupWindow = this.b;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final void j() {
        int i = this.d;
        CalculatorViewModel calculatorViewModel = this.a;
        BigDecimal multiply = calculatorViewModel.q().multiply(new BigDecimal(String.valueOf(i)));
        fs1.e(multiply, "this.multiply(other)");
        BigDecimal valueOf = BigDecimal.valueOf(100L);
        fs1.e(valueOf, "valueOf(100)");
        BigDecimal divide = multiply.divide(valueOf, RoundingMode.HALF_EVEN);
        fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
        calculatorViewModel.d(divide);
    }

    public final lc k(Context context, View view, View view2) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        View inflate = ((LayoutInflater) systemService).inflate(R.layout.dialog_anchor_pop_up_bag, (ViewGroup) null);
        final fn0 a2 = fn0.a(inflate);
        fs1.e(a2, "bind(view)");
        zp1 zp1Var = a2.d;
        zp1Var.b.setOnClickListener(new View.OnClickListener() { // from class: kc
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                lc.l(fn0.this, view3);
            }
        });
        zp1Var.a.setOnClickListener(new View.OnClickListener() { // from class: ic
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                lc.m(lc.this, view3);
            }
        });
        zp1Var.c.setOnClickListener(new View.OnClickListener() { // from class: hc
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                lc.n(lc.this, view3);
            }
        });
        Integer value = this.a.s().getValue();
        if (value != null && value.intValue() == R.id.btnBag) {
            zp1Var.a.setTextColor(m70.d(context, this.c));
        } else if (value != null && value.intValue() == R.id.btnPercentageOfTraditional) {
            zp1Var.c.setTextColor(m70.d(context, this.c));
        }
        aq1 aq1Var = a2.c;
        aq1Var.a.setOnSeekBarChangeListener(new a(aq1Var));
        aq1Var.a.setProgress(this.d - 1);
        MaterialButton materialButton = aq1Var.c;
        StringBuilder sb = new StringBuilder();
        sb.append(this.d);
        sb.append('%');
        materialButton.setText(sb.toString());
        aq1Var.b.setOnClickListener(new View.OnClickListener() { // from class: jc
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                lc.o(lc.this, view3);
            }
        });
        fs1.e(inflate, "view");
        PopupWindow g = g(inflate, view2);
        this.b = g;
        if (g != null) {
            g.showAsDropDown(view);
        }
        return this;
    }
}
