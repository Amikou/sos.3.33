package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: mx5  reason: default package */
/* loaded from: classes.dex */
public final class mx5<E> extends wn5<E> implements RandomAccess {
    public static final mx5<Object> h0;
    public E[] f0;
    public int g0;

    static {
        mx5<Object> mx5Var = new mx5<>(new Object[0], 0);
        h0 = mx5Var;
        mx5Var.zzb();
    }

    public mx5(E[] eArr, int i) {
        this.f0 = eArr;
        this.g0 = i;
    }

    public static <E> mx5<E> m() {
        return (mx5<E>) h0;
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean add(E e) {
        e();
        int i = this.g0;
        E[] eArr = this.f0;
        if (i == eArr.length) {
            this.f0 = (E[]) Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        eArr2[i2] = e;
        ((AbstractList) this).modCount++;
        return true;
    }

    @Override // defpackage.gt5
    public final /* synthetic */ gt5 d(int i) {
        if (i >= this.g0) {
            return new mx5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final E get(int i) {
        i(i);
        return this.f0[i];
    }

    public final void i(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(k(i));
        }
    }

    public final String k(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final E remove(int i) {
        int i2;
        e();
        i(i);
        E[] eArr = this.f0;
        E e = eArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return e;
    }

    @Override // java.util.AbstractList, java.util.List
    public final E set(int i, E e) {
        e();
        i(i);
        E[] eArr = this.f0;
        E e2 = eArr[i];
        eArr[i] = e;
        ((AbstractList) this).modCount++;
        return e2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    @Override // java.util.AbstractList, java.util.List
    public final void add(int i, E e) {
        int i2;
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            E[] eArr = this.f0;
            if (i2 < eArr.length) {
                System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
            } else {
                E[] eArr2 = (E[]) new Object[((i2 * 3) / 2) + 1];
                System.arraycopy(eArr, 0, eArr2, 0, i);
                System.arraycopy(this.f0, i, eArr2, i + 1, this.g0 - i);
                this.f0 = eArr2;
            }
            this.f0[i] = e;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(k(i));
    }
}
