package defpackage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.CircularProgressIndicatorSpec;
import com.google.android.material.progressindicator.LinearProgressIndicatorSpec;
import defpackage.wn;

/* compiled from: DeterminateDrawable.java */
/* renamed from: sm0  reason: default package */
/* loaded from: classes2.dex */
public final class sm0<S extends wn> extends er0 {
    public static final d71<sm0> y0 = new a("indicatorLevel");
    public nr0<S> t0;
    public final as3 u0;
    public final zr3 v0;
    public float w0;
    public boolean x0;

    /* compiled from: DeterminateDrawable.java */
    /* renamed from: sm0$a */
    /* loaded from: classes2.dex */
    public static class a extends d71<sm0> {
        public a(String str) {
            super(str);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(sm0 sm0Var) {
            return sm0Var.x() * 10000.0f;
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(sm0 sm0Var, float f) {
            sm0Var.z(f / 10000.0f);
        }
    }

    public sm0(Context context, wn wnVar, nr0<S> nr0Var) {
        super(context, wnVar);
        this.x0 = false;
        y(nr0Var);
        as3 as3Var = new as3();
        this.u0 = as3Var;
        as3Var.d(1.0f);
        as3Var.f(50.0f);
        zr3 zr3Var = new zr3(this, y0);
        this.v0 = zr3Var;
        zr3Var.p(as3Var);
        m(1.0f);
    }

    public static sm0<CircularProgressIndicatorSpec> u(Context context, CircularProgressIndicatorSpec circularProgressIndicatorSpec) {
        return new sm0<>(context, circularProgressIndicatorSpec, new wy(circularProgressIndicatorSpec));
    }

    public static sm0<LinearProgressIndicatorSpec> v(Context context, LinearProgressIndicatorSpec linearProgressIndicatorSpec) {
        return new sm0<>(context, linearProgressIndicatorSpec, new yz1(linearProgressIndicatorSpec));
    }

    public void A(float f) {
        setLevel((int) (f * 10000.0f));
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect rect = new Rect();
        if (!getBounds().isEmpty() && isVisible() && canvas.getClipBounds(rect)) {
            canvas.save();
            this.t0.g(canvas, g());
            this.t0.c(canvas, this.q0);
            this.t0.b(canvas, this.q0, Utils.FLOAT_EPSILON, x(), l42.a(this.f0.c[0], getAlpha()));
            canvas.restore();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.t0.d();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.t0.e();
    }

    @Override // android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        this.v0.b();
        z(getLevel() / 10000.0f);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        if (this.x0) {
            this.v0.b();
            z(i / 10000.0f);
            return true;
        }
        this.v0.i(x() * 10000.0f);
        this.v0.m(i);
        return true;
    }

    @Override // defpackage.er0
    public boolean q(boolean z, boolean z2, boolean z3) {
        boolean q = super.q(z, z2, z3);
        float a2 = this.g0.a(this.a.getContentResolver());
        if (a2 == Utils.FLOAT_EPSILON) {
            this.x0 = true;
        } else {
            this.x0 = false;
            this.u0.f(50.0f / a2);
        }
        return q;
    }

    public nr0<S> w() {
        return this.t0;
    }

    public final float x() {
        return this.w0;
    }

    public void y(nr0<S> nr0Var) {
        this.t0 = nr0Var;
        nr0Var.f(this);
    }

    public final void z(float f) {
        this.w0 = f;
        invalidateSelf();
    }
}
