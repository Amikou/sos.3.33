package defpackage;

import android.os.IBinder;
import android.os.IInterface;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: lm1  reason: default package */
/* loaded from: classes.dex */
public interface lm1 extends IInterface {

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* renamed from: lm1$a */
    /* loaded from: classes.dex */
    public static abstract class a extends j35 implements lm1 {

        /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
        /* renamed from: lm1$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0199a extends a75 implements lm1 {
            public C0199a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        public a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        @RecentlyNonNull
        public static lm1 F1(@RecentlyNonNull IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            if (queryLocalInterface instanceof lm1) {
                return (lm1) queryLocalInterface;
            }
            return new C0199a(iBinder);
        }
    }
}
