package defpackage;

import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: DataSource.java */
/* renamed from: ge0  reason: default package */
/* loaded from: classes.dex */
public interface ge0<T> {
    boolean a();

    boolean b();

    Throwable c();

    boolean close();

    float d();

    void e(ke0<T> ke0Var, Executor executor);

    boolean f();

    T g();

    Map<String, Object> getExtras();
}
