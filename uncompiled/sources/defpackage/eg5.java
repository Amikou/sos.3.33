package defpackage;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.measurement.internal.d;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: eg5  reason: default package */
/* loaded from: classes.dex */
public final class eg5 extends b<d> {
    public eg5(Context context, Looper looper, b.a aVar, b.InterfaceC0111b interfaceC0111b) {
        super(context, looper, 93, aVar, interfaceC0111b, null);
    }

    @Override // com.google.android.gms.common.internal.b
    public final String H() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @Override // com.google.android.gms.common.internal.b
    public final String I() {
        return "com.google.android.gms.measurement.START";
    }

    @Override // com.google.android.gms.common.internal.b, com.google.android.gms.common.api.a.f
    public final int q() {
        return qh1.a;
    }

    @Override // com.google.android.gms.common.internal.b
    public final /* bridge */ /* synthetic */ d y(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof d) {
            return (d) queryLocalInterface;
        }
        return new com.google.android.gms.measurement.internal.b(iBinder);
    }
}
