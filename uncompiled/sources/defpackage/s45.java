package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: s45  reason: default package */
/* loaded from: classes.dex */
public final class s45 implements z55 {
    public final boolean a;

    public s45(Boolean bool) {
        if (bool == null) {
            this.a = false;
        } else {
            this.a = bool.booleanValue();
        }
    }

    @Override // defpackage.z55
    public final Double b() {
        return Double.valueOf(true != this.a ? Utils.DOUBLE_EPSILON : 1.0d);
    }

    @Override // defpackage.z55
    public final Boolean c() {
        return Boolean.valueOf(this.a);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof s45) && this.a == ((s45) obj).a;
    }

    public final int hashCode() {
        return Boolean.valueOf(this.a).hashCode();
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return null;
    }

    @Override // defpackage.z55
    public final z55 m() {
        return new s45(Boolean.valueOf(this.a));
    }

    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        if ("toString".equals(str)) {
            return new f65(Boolean.toString(this.a));
        }
        throw new IllegalArgumentException(String.format("%s.%s is not a function.", Boolean.toString(this.a), str));
    }

    public final String toString() {
        return String.valueOf(this.a);
    }

    @Override // defpackage.z55
    public final String zzc() {
        return Boolean.toString(this.a);
    }
}
