package defpackage;

import com.google.android.datatransport.a;

/* compiled from: TransportImpl.java */
/* renamed from: sb4  reason: default package */
/* loaded from: classes.dex */
public final class sb4<T> implements mb4<T> {
    public final ob4 a;
    public final String b;
    public final hv0 c;
    public final fb4<T, byte[]> d;
    public final tb4 e;

    public sb4(ob4 ob4Var, String str, hv0 hv0Var, fb4<T, byte[]> fb4Var, tb4 tb4Var) {
        this.a = ob4Var;
        this.b = str;
        this.c = hv0Var;
        this.d = fb4Var;
        this.e = tb4Var;
    }

    public static /* synthetic */ void d(Exception exc) {
    }

    @Override // defpackage.mb4
    public void a(a<T> aVar) {
        b(aVar, rb4.a);
    }

    @Override // defpackage.mb4
    public void b(a<T> aVar, yb4 yb4Var) {
        this.e.a(ck3.a().e(this.a).c(aVar).f(this.b).d(this.d).b(this.c).a(), yb4Var);
    }
}
