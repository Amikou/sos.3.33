package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: du5  reason: default package */
/* loaded from: classes.dex */
public final class du5 implements Runnable {
    public final long a;
    public final long f0;
    public final /* synthetic */ fu5 g0;

    public du5(fu5 fu5Var, long j, long j2) {
        this.g0 = fu5Var;
        this.a = j;
        this.f0 = j2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.g0.b.a.q().p(new Runnable(this) { // from class: bu5
            public final du5 a;

            {
                this.a = this;
            }

            @Override // java.lang.Runnable
            public final void run() {
                du5 du5Var = this.a;
                fu5 fu5Var = du5Var.g0;
                long j = du5Var.a;
                long j2 = du5Var.f0;
                fu5Var.b.e();
                fu5Var.b.a.w().u().a("Application going to the background");
                boolean z = true;
                if (fu5Var.b.a.z().v(null, qf5.s0)) {
                    fu5Var.b.a.A().q.b(true);
                }
                Bundle bundle = new Bundle();
                if (!fu5Var.b.a.z().C()) {
                    fu5Var.b.e.b(j2);
                    if (fu5Var.b.a.z().v(null, qf5.k0)) {
                        lu5 lu5Var = fu5Var.b.e;
                        long j3 = lu5Var.b;
                        lu5Var.b = j2;
                        bundle.putLong("_et", j2 - j3);
                        qq5.x(fu5Var.b.a.Q().r(true), bundle, true);
                    } else {
                        z = false;
                    }
                    fu5Var.b.e.d(false, z, j2);
                }
                fu5Var.b.a.F().Y("auto", "_ab", j, bundle);
            }
        });
    }
}
