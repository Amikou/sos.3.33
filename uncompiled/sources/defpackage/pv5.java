package defpackage;

import com.google.android.gms.internal.vision.m0;
import com.google.android.gms.internal.vision.zzke;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: pv5  reason: default package */
/* loaded from: classes.dex */
public final class pv5 implements qv5 {
    @Override // defpackage.qv5
    public final Map<?, ?> a(Object obj) {
        return (zzke) obj;
    }

    @Override // defpackage.qv5
    public final Map<?, ?> b(Object obj) {
        return (zzke) obj;
    }

    @Override // defpackage.qv5
    public final Object c(Object obj) {
        return zzke.zza().zzb();
    }

    @Override // defpackage.qv5
    public final Object d(Object obj) {
        ((zzke) obj).zzc();
        return obj;
    }

    @Override // defpackage.qv5
    public final boolean e(Object obj) {
        return !((zzke) obj).zzd();
    }

    @Override // defpackage.qv5
    public final lv5<?, ?> f(Object obj) {
        m0 m0Var = (m0) obj;
        throw new NoSuchMethodError();
    }

    @Override // defpackage.qv5
    public final Object g(Object obj, Object obj2) {
        zzke zzkeVar = (zzke) obj;
        zzke zzkeVar2 = (zzke) obj2;
        if (!zzkeVar2.isEmpty()) {
            if (!zzkeVar.zzd()) {
                zzkeVar = zzkeVar.zzb();
            }
            zzkeVar.zza(zzkeVar2);
        }
        return zzkeVar;
    }

    @Override // defpackage.qv5
    public final int h(int i, Object obj, Object obj2) {
        zzke zzkeVar = (zzke) obj;
        m0 m0Var = (m0) obj2;
        if (zzkeVar.isEmpty()) {
            return 0;
        }
        Iterator it = zzkeVar.entrySet().iterator();
        if (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            entry.getKey();
            entry.getValue();
            throw new NoSuchMethodError();
        }
        return 0;
    }
}
