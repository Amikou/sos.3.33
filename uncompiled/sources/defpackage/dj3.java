package defpackage;

import java.util.concurrent.atomic.AtomicReference;
import okio.Segment;

/* compiled from: SegmentPool.kt */
/* renamed from: dj3  reason: default package */
/* loaded from: classes2.dex */
public final class dj3 {
    public static final int c;
    public static final AtomicReference<Segment>[] d;
    public static final dj3 e = new dj3();
    public static final int a = 65536;
    public static final bj3 b = new bj3(new byte[0], 0, 0, false, false);

    static {
        int highestOneBit = Integer.highestOneBit((Runtime.getRuntime().availableProcessors() * 2) - 1);
        c = highestOneBit;
        AtomicReference<Segment>[] atomicReferenceArr = new AtomicReference[highestOneBit];
        for (int i = 0; i < highestOneBit; i++) {
            atomicReferenceArr[i] = new AtomicReference<>();
        }
        d = atomicReferenceArr;
    }

    public static final void b(bj3 bj3Var) {
        AtomicReference<bj3> a2;
        bj3 bj3Var2;
        fs1.f(bj3Var, "segment");
        if (bj3Var.f == null && bj3Var.g == null) {
            if (bj3Var.d || (bj3Var2 = (a2 = e.a()).get()) == b) {
                return;
            }
            int i = bj3Var2 != null ? bj3Var2.c : 0;
            if (i >= a) {
                return;
            }
            bj3Var.f = bj3Var2;
            bj3Var.b = 0;
            bj3Var.c = i + 8192;
            if (a2.compareAndSet(bj3Var2, bj3Var)) {
                return;
            }
            bj3Var.f = null;
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    public static final bj3 c() {
        AtomicReference<bj3> a2 = e.a();
        bj3 bj3Var = b;
        bj3 andSet = a2.getAndSet(bj3Var);
        if (andSet == bj3Var) {
            return new bj3();
        }
        if (andSet == null) {
            a2.set(null);
            return new bj3();
        }
        a2.set(andSet.f);
        andSet.f = null;
        andSet.c = 0;
        return andSet;
    }

    public final AtomicReference<bj3> a() {
        Thread currentThread = Thread.currentThread();
        fs1.e(currentThread, "Thread.currentThread()");
        return d[(int) (currentThread.getId() & (c - 1))];
    }
}
