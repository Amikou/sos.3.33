package defpackage;

/* compiled from: FrescoInstrumenter.java */
/* renamed from: mc1  reason: default package */
/* loaded from: classes.dex */
public final class mc1 {
    public static volatile a a;

    /* compiled from: FrescoInstrumenter.java */
    /* renamed from: mc1$a */
    /* loaded from: classes.dex */
    public interface a {
        Runnable a(Runnable runnable, String str);

        boolean b();

        void c(Object obj, Throwable th);

        Object d(String str);

        void e(Object obj);

        Object f(Object obj, String str);
    }

    public static Runnable a(Runnable runnable, String str) {
        a aVar = a;
        if (aVar == null || runnable == null) {
            return runnable;
        }
        if (str == null) {
            str = "";
        }
        return aVar.a(runnable, str);
    }

    public static boolean b() {
        a aVar = a;
        if (aVar == null) {
            return false;
        }
        return aVar.b();
    }

    public static void c(Object obj, Throwable th) {
        a aVar = a;
        if (aVar == null || obj == null) {
            return;
        }
        aVar.c(obj, th);
    }

    public static Object d(String str) {
        a aVar = a;
        if (aVar == null || str == null) {
            return null;
        }
        return aVar.d(str);
    }

    public static Object e(Object obj, String str) {
        a aVar = a;
        if (aVar == null || obj == null) {
            return null;
        }
        return aVar.f(obj, str);
    }

    public static void f(Object obj) {
        a aVar = a;
        if (aVar == null || obj == null) {
            return;
        }
        aVar.e(obj);
    }
}
