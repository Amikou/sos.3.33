package defpackage;

import android.content.Context;
import java.util.Collections;
import java.util.Set;

/* compiled from: TransportRuntime.java */
/* renamed from: vb4  reason: default package */
/* loaded from: classes.dex */
public class vb4 implements tb4 {
    public static volatile wb4 e;
    public final qz a;
    public final qz b;
    public final ad3 c;
    public final mf4 d;

    public vb4(qz qzVar, qz qzVar2, ad3 ad3Var, mf4 mf4Var, eq4 eq4Var) {
        this.a = qzVar;
        this.b = qzVar2;
        this.c = ad3Var;
        this.d = mf4Var;
        eq4Var.c();
    }

    public static vb4 c() {
        wb4 wb4Var = e;
        if (wb4Var != null) {
            return wb4Var.b();
        }
        throw new IllegalStateException("Not initialized!");
    }

    public static Set<hv0> d(qm0 qm0Var) {
        if (qm0Var instanceof yu0) {
            return Collections.unmodifiableSet(((yu0) qm0Var).a());
        }
        return Collections.singleton(hv0.b("proto"));
    }

    public static void f(Context context) {
        if (e == null) {
            synchronized (vb4.class) {
                if (e == null) {
                    e = rd0.c().a(context).build();
                }
            }
        }
    }

    @Override // defpackage.tb4
    public void a(ck3 ck3Var, yb4 yb4Var) {
        this.c.a(ck3Var.f().e(ck3Var.c().c()), b(ck3Var), yb4Var);
    }

    public final wx0 b(ck3 ck3Var) {
        return wx0.a().i(this.a.a()).k(this.b.a()).j(ck3Var.g()).h(new cv0(ck3Var.b(), ck3Var.d())).g(ck3Var.c().a()).d();
    }

    public mf4 e() {
        return this.d;
    }

    public pb4 g(qm0 qm0Var) {
        return new qb4(d(qm0Var), ob4.a().b(qm0Var.getName()).c(qm0Var.getExtras()).a(), this);
    }
}
