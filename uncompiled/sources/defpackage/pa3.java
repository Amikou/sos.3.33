package defpackage;

/* renamed from: pa3  reason: default package */
/* loaded from: classes2.dex */
public class pa3 extends dx1 implements ls4 {
    public pa3(int i) {
        super(q(i));
    }

    public static int q(int i) {
        if (i == 128 || i == 256) {
            return i;
        }
        throw new IllegalArgumentException("'bitLength' " + i + " not supported for SHAKE");
    }

    @Override // defpackage.dx1, defpackage.qo0
    public int a(byte[] bArr, int i) {
        return e(bArr, i, h());
    }

    @Override // defpackage.ls4
    public int e(byte[] bArr, int i, int i2) {
        int r = r(bArr, i, i2);
        reset();
        return r;
    }

    @Override // defpackage.dx1, defpackage.qo0
    public String g() {
        return "SHAKE" + this.e;
    }

    public int r(byte[] bArr, int i, int i2) {
        if (!this.f) {
            l(15, 4);
        }
        p(bArr, i, i2 * 8);
        return i2;
    }
}
