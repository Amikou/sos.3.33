package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSwapTransactionLimitBinding.java */
/* renamed from: ho0  reason: default package */
/* loaded from: classes2.dex */
public final class ho0 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final TextInputEditText c;

    public ho0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialTextView materialTextView, TextInputEditText textInputEditText, TextInputLayout textInputLayout, MaterialTextView materialTextView2) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = textInputEditText;
    }

    public static ho0 a(View view) {
        int i = R.id.btnConfirm;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConfirm);
        if (materialButton != null) {
            i = R.id.dialog_cross;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
            if (materialButton2 != null) {
                i = R.id.dialog_title;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_title);
                if (materialTextView != null) {
                    i = R.id.edtTime;
                    TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.edtTime);
                    if (textInputEditText != null) {
                        i = R.id.edtTimeParent;
                        TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.edtTimeParent);
                        if (textInputLayout != null) {
                            i = R.id.helpPrompt;
                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.helpPrompt);
                            if (materialTextView2 != null) {
                                return new ho0((ConstraintLayout) view, materialButton, materialButton2, materialTextView, textInputEditText, textInputLayout, materialTextView2);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
