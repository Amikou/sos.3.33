package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: f46  reason: default package */
/* loaded from: classes.dex */
public final class f46 implements yp5<g46> {
    public static final f46 f0 = new f46();
    public final yp5<g46> a = gq5.a(gq5.b(new h46()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final g46 zza() {
        return this.a.zza();
    }
}
