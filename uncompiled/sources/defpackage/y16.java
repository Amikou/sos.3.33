package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: y16  reason: default package */
/* loaded from: classes.dex */
public final class y16 implements x16 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.collection.firebase_global_collection_flag_enabled", true);

    @Override // defpackage.x16
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
