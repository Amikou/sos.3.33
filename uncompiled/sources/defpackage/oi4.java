package defpackage;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

/* compiled from: ViewGroupOverlayApi18.java */
/* renamed from: oi4  reason: default package */
/* loaded from: classes.dex */
public class oi4 implements pi4 {
    public final ViewGroupOverlay a;

    public oi4(ViewGroup viewGroup) {
        this.a = viewGroup.getOverlay();
    }

    @Override // defpackage.qj4
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @Override // defpackage.qj4
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }

    @Override // defpackage.pi4
    public void c(View view) {
        this.a.add(view);
    }

    @Override // defpackage.pi4
    public void d(View view) {
        this.a.remove(view);
    }
}
