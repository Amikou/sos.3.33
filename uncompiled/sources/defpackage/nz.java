package defpackage;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ClipHelper.java */
/* renamed from: nz  reason: default package */
/* loaded from: classes.dex */
public class nz implements oz {
    public final View a;
    public boolean f0;
    public final RectF g0 = new RectF();
    public float h0;

    public nz(View view) {
        this.a = view;
    }

    @Override // defpackage.oz
    public void a(RectF rectF, float f) {
        if (rectF == null) {
            if (this.f0) {
                this.f0 = false;
                this.a.invalidate();
                return;
            }
            return;
        }
        this.f0 = true;
        this.g0.set(rectF);
        this.h0 = f;
        this.a.invalidate();
    }

    public void b(Canvas canvas) {
        if (this.f0) {
            canvas.restore();
        }
    }

    public void c(Canvas canvas) {
        if (this.f0) {
            canvas.save();
            if (us3.c(this.h0, Utils.FLOAT_EPSILON)) {
                canvas.clipRect(this.g0);
                return;
            }
            canvas.rotate(this.h0, this.g0.centerX(), this.g0.centerY());
            canvas.clipRect(this.g0);
            canvas.rotate(-this.h0, this.g0.centerX(), this.g0.centerY());
        }
    }
}
