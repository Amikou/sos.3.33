package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityNftDetailBinding.java */
/* renamed from: n7  reason: default package */
/* loaded from: classes2.dex */
public final class n7 {
    public final LinearLayout a;
    public final AppCompatImageView b;
    public final AppCompatImageView c;
    public final ii4 d;

    public n7(LinearLayout linearLayout, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, AppCompatTextView appCompatTextView, ii4 ii4Var) {
        this.a = linearLayout;
        this.b = appCompatImageView2;
        this.c = appCompatImageView3;
        this.d = ii4Var;
    }

    public static n7 a(View view) {
        int i = R.id.btnBack;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnBack);
        if (appCompatImageView != null) {
            i = R.id.btnOpen;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.btnOpen);
            if (appCompatImageView2 != null) {
                i = R.id.btnShare;
                AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.btnShare);
                if (appCompatImageView3 != null) {
                    i = R.id.title;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.title);
                    if (appCompatTextView != null) {
                        i = R.id.viewDetailNFC;
                        View a = ai4.a(view, R.id.viewDetailNFC);
                        if (a != null) {
                            return new n7((LinearLayout) view, appCompatImageView, appCompatImageView2, appCompatImageView3, appCompatTextView, ii4.a(a));
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static n7 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_nft_detail, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public LinearLayout b() {
        return this.a;
    }
}
