package defpackage;

import androidx.paging.LoadType;
import defpackage.w02;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: LoadStates.kt */
/* renamed from: x02  reason: default package */
/* loaded from: classes.dex */
public final class x02 {
    public static final x02 d;
    public static final a e = new a(null);
    public final w02 a;
    public final w02 b;
    public final w02 c;

    /* compiled from: LoadStates.kt */
    /* renamed from: x02$a */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public final x02 a() {
            return x02.d;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        w02.c.a aVar = w02.c.d;
        d = new x02(aVar.b(), aVar.b(), aVar.b());
    }

    public x02(w02 w02Var, w02 w02Var2, w02 w02Var3) {
        fs1.f(w02Var, "refresh");
        fs1.f(w02Var2, "prepend");
        fs1.f(w02Var3, "append");
        this.a = w02Var;
        this.b = w02Var2;
        this.c = w02Var3;
    }

    public static /* synthetic */ x02 c(x02 x02Var, w02 w02Var, w02 w02Var2, w02 w02Var3, int i, Object obj) {
        if ((i & 1) != 0) {
            w02Var = x02Var.a;
        }
        if ((i & 2) != 0) {
            w02Var2 = x02Var.b;
        }
        if ((i & 4) != 0) {
            w02Var3 = x02Var.c;
        }
        return x02Var.b(w02Var, w02Var2, w02Var3);
    }

    public final x02 b(w02 w02Var, w02 w02Var2, w02 w02Var3) {
        fs1.f(w02Var, "refresh");
        fs1.f(w02Var2, "prepend");
        fs1.f(w02Var3, "append");
        return new x02(w02Var, w02Var2, w02Var3);
    }

    public final w02 d(LoadType loadType) {
        fs1.f(loadType, "loadType");
        int i = y02.b[loadType.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return this.b;
                }
                throw new NoWhenBranchMatchedException();
            }
            return this.c;
        }
        return this.a;
    }

    public final w02 e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof x02) {
                x02 x02Var = (x02) obj;
                return fs1.b(this.a, x02Var.a) && fs1.b(this.b, x02Var.b) && fs1.b(this.c, x02Var.c);
            }
            return false;
        }
        return true;
    }

    public final w02 f() {
        return this.b;
    }

    public final w02 g() {
        return this.a;
    }

    public final x02 h(LoadType loadType, w02 w02Var) {
        fs1.f(loadType, "loadType");
        fs1.f(w02Var, "newState");
        int i = y02.a[loadType.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return c(this, w02Var, null, null, 6, null);
                }
                throw new NoWhenBranchMatchedException();
            }
            return c(this, null, w02Var, null, 5, null);
        }
        return c(this, null, null, w02Var, 3, null);
    }

    public int hashCode() {
        w02 w02Var = this.a;
        int hashCode = (w02Var != null ? w02Var.hashCode() : 0) * 31;
        w02 w02Var2 = this.b;
        int hashCode2 = (hashCode + (w02Var2 != null ? w02Var2.hashCode() : 0)) * 31;
        w02 w02Var3 = this.c;
        return hashCode2 + (w02Var3 != null ? w02Var3.hashCode() : 0);
    }

    public String toString() {
        return "LoadStates(refresh=" + this.a + ", prepend=" + this.b + ", append=" + this.c + ")";
    }
}
