package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.Coin;
import net.safemoon.androidwallet.utils.PreFetchData;

/* compiled from: CMC.kt */
/* renamed from: kt  reason: default package */
/* loaded from: classes2.dex */
public final class kt {

    /* compiled from: CMC.kt */
    /* renamed from: kt$a */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TokenType.values().length];
            iArr[TokenType.BEP_20.ordinal()] = 1;
            iArr[TokenType.ERC_20.ordinal()] = 2;
            iArr[TokenType.BEP_20_TEST.ordinal()] = 3;
            iArr[TokenType.ERC_20_TEST.ordinal()] = 4;
            a = iArr;
        }
    }

    public static final rt a(String str, String str2) {
        Object obj;
        boolean b;
        fs1.f(str, "<this>");
        if (fs1.b(str2, "SAFEMOON")) {
            return new rt("0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3", "8757", "safemoon", "SAFEMOON", 9, 56, Utils.DOUBLE_EPSILON, false);
        }
        Iterator<T> it = PreFetchData.a.a().iterator();
        while (true) {
            obj = null;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            rt rtVar = (rt) next;
            if (str.length() == 0) {
                if (str2 != null) {
                    obj = str2.toLowerCase(Locale.ROOT);
                    fs1.e(obj, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                String h = rtVar.h();
                Objects.requireNonNull(h, "null cannot be cast to non-null type java.lang.String");
                String lowerCase = h.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                b = fs1.b(obj, lowerCase);
                continue;
            } else {
                String a2 = rtVar.a();
                Objects.requireNonNull(a2, "null cannot be cast to non-null type java.lang.String");
                Locale locale = Locale.ROOT;
                String lowerCase2 = a2.toLowerCase(locale);
                fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                String lowerCase3 = str.toLowerCase(locale);
                fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                b = fs1.b(lowerCase2, lowerCase3);
                continue;
            }
            if (b) {
                obj = next;
                break;
            }
        }
        return (rt) obj;
    }

    public static final String b(String str, String str2, Integer num) {
        Object obj;
        boolean b;
        String lowerCase;
        fs1.f(str, "<this>");
        if (fs1.b(str2, "SAFEMOON")) {
            return "8757";
        }
        String num2 = num == null ? null : num.toString();
        if (num2 == null) {
            Iterator<T> it = PreFetchData.a.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                rt rtVar = (rt) obj;
                if (str.length() == 0) {
                    if (str2 == null) {
                        lowerCase = null;
                    } else {
                        lowerCase = str2.toLowerCase(Locale.ROOT);
                        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    }
                    String h = rtVar.h();
                    Objects.requireNonNull(h, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase2 = h.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    b = fs1.b(lowerCase, lowerCase2);
                    continue;
                } else {
                    String a2 = rtVar.a();
                    Objects.requireNonNull(a2, "null cannot be cast to non-null type java.lang.String");
                    Locale locale = Locale.ROOT;
                    String lowerCase3 = a2.toLowerCase(locale);
                    fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String lowerCase4 = str.toLowerCase(locale);
                    fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    b = fs1.b(lowerCase3, lowerCase4);
                    continue;
                }
                if (b) {
                    break;
                }
            }
            rt rtVar2 = (rt) obj;
            if (rtVar2 == null) {
                return null;
            }
            return rtVar2.c();
        }
        return num2;
    }

    public static /* synthetic */ String c(String str, String str2, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        return b(str, str2, num);
    }

    public static final String d(Coin coin) {
        fs1.f(coin, "<this>");
        String name = coin.getName();
        fs1.e(name, "this.name");
        return name;
    }

    public static final String e(String str) {
        fs1.f(str, "<this>");
        String upperCase = str.toUpperCase(Locale.ROOT);
        fs1.e(upperCase, "(this as java.lang.Strin….toUpperCase(Locale.ROOT)");
        return fs1.b(upperCase, "PSAFEMOON") ? "SAFEMOON" : str;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (r0.equals("SFM") == false) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x003d, code lost:
        if (r0.equals("SAFEMOON") == false) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x0065, code lost:
        return java.lang.Integer.valueOf((int) net.safemoon.androidwallet.R.drawable.safemoon);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final java.lang.Object f(defpackage.i00 r3) {
        /*
            java.lang.String r0 = "<this>"
            defpackage.fs1.f(r3, r0)
            java.lang.String r0 = r3.b()
            int r1 = r0.hashCode()
            r2 = -1747297266(0xffffffff97da5c0e, float:-1.4111158E-24)
            if (r1 == r2) goto L37
            r2 = -314766242(0xffffffffed3d0c5e, float:-3.656726E27)
            if (r1 == r2) goto L26
            r2 = 82010(0x1405a, float:1.1492E-40)
            if (r1 == r2) goto L1d
            goto L3f
        L1d:
            java.lang.String r1 = "SFM"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L5e
            goto L3f
        L26:
            java.lang.String r1 = "PSAFEMOON"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L2f
            goto L3f
        L2f:
            r3 = 2131231258(0x7f08021a, float:1.8078592E38)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            goto L65
        L37:
            java.lang.String r1 = "SAFEMOON"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L5e
        L3f:
            java.lang.String r0 = r3.c()
            if (r0 == 0) goto L4e
            boolean r0 = defpackage.dv3.w(r0)
            if (r0 == 0) goto L4c
            goto L4e
        L4c:
            r0 = 0
            goto L4f
        L4e:
            r0 = 1
        L4f:
            if (r0 != 0) goto L56
            java.lang.String r3 = r3.c()
            goto L5a
        L56:
            java.lang.Integer r3 = r3.a()
        L5a:
            defpackage.fs1.d(r3)
            goto L65
        L5e:
            r3 = 2131231295(0x7f08023f, float:1.8078667E38)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
        L65:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.kt.f(i00):java.lang.Object");
    }

    public static final String g(String str, String str2) {
        fs1.f(str, "<this>");
        fs1.f(str2, "symbol");
        return fs1.b(str2, "PSAFEMOON") ? "PSAFEMOON" : str;
    }

    public static final Object h(String str, String str2, TokenType tokenType) {
        fs1.f(str, "<this>");
        fs1.f(str2, "symbol");
        fs1.f(tokenType, "selectedTokenType");
        int hashCode = str2.hashCode();
        if (hashCode == -1747297266 ? str2.equals("SAFEMOON") : hashCode == -314766242 ? str2.equals("PSAFEMOON") : hashCode == 82010 && str2.equals("SFM")) {
            int i = a.a[tokenType.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return i != 4 ? str : Integer.valueOf((int) R.drawable.p_safemoon);
                    }
                    return Integer.valueOf((int) R.drawable.safemoon);
                }
                return Integer.valueOf((int) R.drawable.p_safemoon);
            }
            return Integer.valueOf((int) R.drawable.safemoon);
        }
        return str;
    }
}
