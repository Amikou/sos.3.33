package defpackage;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: uk5  reason: default package */
/* loaded from: classes.dex */
public final class uk5 implements gi5 {
    public uk5() {
    }

    @Override // defpackage.gi5
    public final ScheduledExecutorService i(int i, int i2) {
        return Executors.unconfigurableScheduledExecutorService(Executors.newScheduledThreadPool(1));
    }
}
