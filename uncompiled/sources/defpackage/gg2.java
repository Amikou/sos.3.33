package defpackage;

import androidx.recyclerview.widget.g;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: NftsAdapter.kt */
/* renamed from: gg2  reason: default package */
/* loaded from: classes2.dex */
public final class gg2 {
    public static final a a = new a();

    /* compiled from: NftsAdapter.kt */
    /* renamed from: gg2$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<RoomNFT> {
        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(RoomNFT roomNFT, RoomNFT roomNFT2) {
            fs1.f(roomNFT, "oldItem");
            fs1.f(roomNFT2, "newItem");
            return fs1.b(roomNFT.getName(), roomNFT2.getName()) && fs1.b(roomNFT.getImage_preview_url(), roomNFT2.getImage_preview_url()) && roomNFT.getOrder() == roomNFT2.getOrder();
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(RoomNFT roomNFT, RoomNFT roomNFT2) {
            fs1.f(roomNFT, "oldItem");
            fs1.f(roomNFT2, "newItem");
            return fs1.b(roomNFT.getToken_id(), roomNFT2.getToken_id());
        }
    }
}
