package defpackage;

import defpackage.pv0;
import java.security.GeneralSecurityException;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* compiled from: EngineFactory.java */
/* renamed from: lv0  reason: default package */
/* loaded from: classes2.dex */
public final class lv0<T_WRAPPER extends pv0<T_ENGINE>, T_ENGINE> {
    public static final Logger d = Logger.getLogger(lv0.class.getName());
    public static final List<Provider> e;
    public static final lv0<pv0.a, Cipher> f;
    public static final lv0<pv0.e, Mac> g;
    public T_WRAPPER a;
    public List<Provider> b = e;
    public boolean c = true;

    static {
        if (zv3.b()) {
            e = b("GmsCore_OpenSSL", "AndroidOpenSSL");
        } else {
            e = new ArrayList();
        }
        f = new lv0<>(new pv0.a());
        g = new lv0<>(new pv0.e());
        new lv0(new pv0.g());
        new lv0(new pv0.f());
        new lv0(new pv0.b());
        new lv0(new pv0.d());
        new lv0(new pv0.c());
    }

    public lv0(T_WRAPPER t_wrapper) {
        this.a = t_wrapper;
    }

    public static List<Provider> b(String... strArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            Provider provider = Security.getProvider(str);
            if (provider != null) {
                arrayList.add(provider);
            } else {
                d.info(String.format("Provider %s not available", str));
            }
        }
        return arrayList;
    }

    public T_ENGINE a(String str) throws GeneralSecurityException {
        Exception exc = null;
        for (Provider provider : this.b) {
            try {
                return (T_ENGINE) this.a.a(str, provider);
            } catch (Exception e2) {
                if (exc == null) {
                    exc = e2;
                }
            }
        }
        if (this.c) {
            return (T_ENGINE) this.a.a(str, null);
        }
        throw new GeneralSecurityException("No good Provider found.", exc);
    }
}
