package defpackage;

import retrofit2.n;

/* compiled from: RetrofitZendeskCallbackAdapter.java */
/* renamed from: w83  reason: default package */
/* loaded from: classes2.dex */
public class w83<E, F> implements wu<E> {
    public static final b c = new a();
    public final rs4<F> a;
    public final b<E, F> b;

    /* compiled from: RetrofitZendeskCallbackAdapter.java */
    /* renamed from: w83$a */
    /* loaded from: classes2.dex */
    public static final class a<E> implements b<E, E> {
        @Override // defpackage.w83.b
        public E extract(E e) {
            return e;
        }
    }

    /* compiled from: RetrofitZendeskCallbackAdapter.java */
    /* renamed from: w83$b */
    /* loaded from: classes2.dex */
    public interface b<E, F> {
        F extract(E e);
    }

    public w83(rs4<F> rs4Var, b<E, F> bVar) {
        this.a = rs4Var;
        this.b = bVar;
    }

    @Override // defpackage.wu
    public void a(retrofit2.b<E> bVar, Throwable th) {
        rs4<F> rs4Var = this.a;
        if (rs4Var != null) {
            rs4Var.onError(v83.b(th));
        }
    }

    @Override // defpackage.wu
    public void b(retrofit2.b<E> bVar, n<E> nVar) {
        if (this.a != null) {
            if (nVar.e()) {
                this.a.onSuccess(this.b.extract(nVar.a()));
            } else {
                this.a.onError(v83.a(nVar));
            }
        }
    }

    public w83(rs4<F> rs4Var) {
        this(rs4Var, c);
    }
}
