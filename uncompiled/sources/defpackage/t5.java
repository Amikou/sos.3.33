package defpackage;

import androidx.media3.common.DrmInitData;
import androidx.media3.common.j;
import androidx.recyclerview.widget.RecyclerView;
import java.nio.ByteBuffer;

/* compiled from: Ac3Util.java */
/* renamed from: t5  reason: default package */
/* loaded from: classes.dex */
public final class t5 {
    public static final int[] a = {1, 2, 3, 6};
    public static final int[] b = {48000, 44100, 32000};
    public static final int[] c = {24000, 22050, 16000};
    public static final int[] d = {2, 1, 2, 3, 3, 4, 4, 5};
    public static final int[] e = {32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, 448, RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN, 576, 640};
    public static final int[] f = {69, 87, 104, 121, 139, 174, 208, 243, 278, 348, 417, 487, 557, 696, 835, 975, 1114, 1253, 1393};

    /* compiled from: Ac3Util.java */
    /* renamed from: t5$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final String a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;

        public b(String str, int i, int i2, int i3, int i4, int i5) {
            this.a = str;
            this.c = i2;
            this.b = i3;
            this.d = i4;
            this.e = i5;
        }
    }

    public static int a(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit() - 10;
        for (int i = position; i <= limit; i++) {
            if ((androidx.media3.common.util.b.G(byteBuffer, i + 4) & (-2)) == -126718022) {
                return i - position;
            }
        }
        return -1;
    }

    public static int b(int i, int i2) {
        int i3 = i2 / 2;
        if (i >= 0) {
            int[] iArr = b;
            if (i >= iArr.length || i2 < 0) {
                return -1;
            }
            int[] iArr2 = f;
            if (i3 >= iArr2.length) {
                return -1;
            }
            int i4 = iArr[i];
            if (i4 == 44100) {
                return (iArr2[i3] + (i2 % 2)) * 2;
            }
            int i5 = e[i3];
            return i4 == 32000 ? i5 * 6 : i5 * 4;
        }
        return -1;
    }

    public static j c(op2 op2Var, String str, String str2, DrmInitData drmInitData) {
        int i = b[(op2Var.D() & 192) >> 6];
        int D = op2Var.D();
        int i2 = d[(D & 56) >> 3];
        if ((D & 4) != 0) {
            i2++;
        }
        return new j.b().S(str).e0("audio/ac3").H(i2).f0(i).M(drmInitData).V(str2).E();
    }

    public static int d(ByteBuffer byteBuffer) {
        if (((byteBuffer.get(byteBuffer.position() + 5) & 248) >> 3) > 10) {
            return a[((byteBuffer.get(byteBuffer.position() + 4) & 192) >> 6) != 3 ? (byteBuffer.get(byteBuffer.position() + 4) & 48) >> 4 : 3] * 256;
        }
        return 1536;
    }

    public static b e(np2 np2Var) {
        int b2;
        int i;
        int i2;
        int i3;
        int i4;
        String str;
        int h;
        int i5;
        int i6;
        int i7;
        int i8;
        int e2 = np2Var.e();
        np2Var.r(40);
        boolean z = np2Var.h(5) > 10;
        np2Var.p(e2);
        int i9 = -1;
        if (z) {
            np2Var.r(16);
            int h2 = np2Var.h(2);
            if (h2 == 0) {
                i9 = 0;
            } else if (h2 == 1) {
                i9 = 1;
            } else if (h2 == 2) {
                i9 = 2;
            }
            np2Var.r(3);
            b2 = (np2Var.h(11) + 1) * 2;
            int h3 = np2Var.h(2);
            if (h3 == 3) {
                i = c[np2Var.h(2)];
                i5 = 6;
                h = 3;
            } else {
                h = np2Var.h(2);
                i5 = a[h];
                i = b[h3];
            }
            i2 = i5 * 256;
            int h4 = np2Var.h(3);
            boolean g = np2Var.g();
            i3 = d[h4] + (g ? 1 : 0);
            np2Var.r(10);
            if (np2Var.g()) {
                np2Var.r(8);
            }
            if (h4 == 0) {
                np2Var.r(5);
                if (np2Var.g()) {
                    np2Var.r(8);
                }
            }
            if (i9 == 1 && np2Var.g()) {
                np2Var.r(16);
            }
            if (np2Var.g()) {
                if (h4 > 2) {
                    np2Var.r(2);
                }
                if ((h4 & 1) == 0 || h4 <= 2) {
                    i7 = 6;
                } else {
                    i7 = 6;
                    np2Var.r(6);
                }
                if ((h4 & 4) != 0) {
                    np2Var.r(i7);
                }
                if (g && np2Var.g()) {
                    np2Var.r(5);
                }
                if (i9 == 0) {
                    if (np2Var.g()) {
                        i8 = 6;
                        np2Var.r(6);
                    } else {
                        i8 = 6;
                    }
                    if (h4 == 0 && np2Var.g()) {
                        np2Var.r(i8);
                    }
                    if (np2Var.g()) {
                        np2Var.r(i8);
                    }
                    int h5 = np2Var.h(2);
                    if (h5 == 1) {
                        np2Var.r(5);
                    } else if (h5 == 2) {
                        np2Var.r(12);
                    } else if (h5 == 3) {
                        int h6 = np2Var.h(5);
                        if (np2Var.g()) {
                            np2Var.r(5);
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                np2Var.r(4);
                            }
                            if (np2Var.g()) {
                                if (np2Var.g()) {
                                    np2Var.r(4);
                                }
                                if (np2Var.g()) {
                                    np2Var.r(4);
                                }
                            }
                        }
                        if (np2Var.g()) {
                            np2Var.r(5);
                            if (np2Var.g()) {
                                np2Var.r(7);
                                if (np2Var.g()) {
                                    np2Var.r(8);
                                }
                            }
                        }
                        np2Var.r((h6 + 2) * 8);
                        np2Var.c();
                    }
                    if (h4 < 2) {
                        if (np2Var.g()) {
                            np2Var.r(14);
                        }
                        if (h4 == 0 && np2Var.g()) {
                            np2Var.r(14);
                        }
                    }
                    if (np2Var.g()) {
                        if (h == 0) {
                            np2Var.r(5);
                        } else {
                            for (int i10 = 0; i10 < i5; i10++) {
                                if (np2Var.g()) {
                                    np2Var.r(5);
                                }
                            }
                        }
                    }
                }
            }
            if (np2Var.g()) {
                np2Var.r(5);
                if (h4 == 2) {
                    np2Var.r(4);
                }
                if (h4 >= 6) {
                    np2Var.r(2);
                }
                if (np2Var.g()) {
                    np2Var.r(8);
                }
                if (h4 == 0 && np2Var.g()) {
                    np2Var.r(8);
                }
                if (h3 < 3) {
                    np2Var.q();
                }
            }
            if (i9 == 0 && h != 3) {
                np2Var.q();
            }
            if (i9 == 2 && (h == 3 || np2Var.g())) {
                i6 = 6;
                np2Var.r(6);
            } else {
                i6 = 6;
            }
            str = (np2Var.g() && np2Var.h(i6) == 1 && np2Var.h(8) == 1) ? "audio/eac3-joc" : "audio/eac3";
            i4 = i9;
        } else {
            np2Var.r(32);
            int h7 = np2Var.h(2);
            String str2 = h7 == 3 ? null : "audio/ac3";
            b2 = b(h7, np2Var.h(6));
            np2Var.r(8);
            int h8 = np2Var.h(3);
            if ((h8 & 1) != 0 && h8 != 1) {
                np2Var.r(2);
            }
            if ((h8 & 4) != 0) {
                np2Var.r(2);
            }
            if (h8 == 2) {
                np2Var.r(2);
            }
            int[] iArr = b;
            i = h7 < iArr.length ? iArr[h7] : -1;
            i2 = 1536;
            i3 = d[h8] + (np2Var.g() ? 1 : 0);
            i4 = -1;
            str = str2;
        }
        return new b(str, i4, i3, i, b2, i2);
    }

    public static int f(byte[] bArr) {
        if (bArr.length < 6) {
            return -1;
        }
        if (((bArr[5] & 248) >> 3) > 10) {
            return (((bArr[3] & 255) | ((bArr[2] & 7) << 8)) + 1) * 2;
        }
        return b((bArr[4] & 192) >> 6, bArr[4] & 63);
    }

    public static j g(op2 op2Var, String str, String str2, DrmInitData drmInitData) {
        op2Var.Q(2);
        int i = b[(op2Var.D() & 192) >> 6];
        int D = op2Var.D();
        int i2 = d[(D & 14) >> 1];
        if ((D & 1) != 0) {
            i2++;
        }
        if (((op2Var.D() & 30) >> 1) > 0 && (2 & op2Var.D()) != 0) {
            i2 += 2;
        }
        return new j.b().S(str).e0((op2Var.a() <= 0 || (op2Var.D() & 1) == 0) ? "audio/eac3" : "audio/eac3-joc").H(i2).f0(i).M(drmInitData).V(str2).E();
    }

    public static int h(ByteBuffer byteBuffer, int i) {
        return 40 << ((byteBuffer.get((byteBuffer.position() + i) + ((byteBuffer.get((byteBuffer.position() + i) + 7) & 255) == 187 ? 9 : 8)) >> 4) & 7);
    }

    public static int i(byte[] bArr) {
        if (bArr[4] == -8 && bArr[5] == 114 && bArr[6] == 111 && (bArr[7] & 254) == 186) {
            return 40 << ((bArr[(bArr[7] & 255) == 187 ? '\t' : '\b'] >> 4) & 7);
        }
        return 0;
    }
}
