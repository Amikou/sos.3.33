package defpackage;

/* compiled from: SingleCheck.java */
/* renamed from: tp3  reason: default package */
/* loaded from: classes2.dex */
public final class tp3<T> implements ew2<T> {
    public static final Object c = new Object();
    public volatile ew2<T> a;
    public volatile Object b = c;

    public tp3(ew2<T> ew2Var) {
        this.a = ew2Var;
    }

    public static <P extends ew2<T>, T> ew2<T> a(P p) {
        return ((p instanceof tp3) || (p instanceof fq0)) ? p : new tp3((ew2) cu2.b(p));
    }

    @Override // defpackage.ew2
    public T get() {
        T t = (T) this.b;
        if (t == c) {
            ew2<T> ew2Var = this.a;
            if (ew2Var == null) {
                return (T) this.b;
            }
            T t2 = ew2Var.get();
            this.b = t2;
            this.a = null;
            return t2;
        }
        return t;
    }
}
