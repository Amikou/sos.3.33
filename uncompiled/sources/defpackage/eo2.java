package defpackage;

import androidx.paging.DiffingChangePayload;

/* compiled from: NullPaddedListDiffHelper.kt */
/* renamed from: eo2  reason: default package */
/* loaded from: classes.dex */
public final class eo2 {
    public static final eo2 a = new eo2();

    /* compiled from: NullPaddedListDiffHelper.kt */
    /* renamed from: eo2$a */
    /* loaded from: classes.dex */
    public static final class a<T> implements i02 {
        public int a;
        public int f0;
        public int g0;
        public int h0;
        public int i0;
        public final vi2<T> j0;
        public final vi2<T> k0;
        public final i02 l0;

        /* compiled from: NullPaddedListDiffHelper.kt */
        /* renamed from: eo2$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0169a {
            public C0169a() {
            }

            public /* synthetic */ C0169a(qi0 qi0Var) {
                this();
            }
        }

        static {
            new C0169a(null);
        }

        public a(vi2<T> vi2Var, vi2<T> vi2Var2, i02 i02Var) {
            fs1.f(vi2Var, "oldList");
            fs1.f(vi2Var2, "newList");
            fs1.f(i02Var, "callback");
            this.j0 = vi2Var;
            this.k0 = vi2Var2;
            this.l0 = i02Var;
            this.a = vi2Var.c();
            this.f0 = vi2Var.d();
            this.g0 = vi2Var.b();
            this.h0 = 1;
            this.i0 = 1;
        }

        public final boolean b(int i, int i2) {
            if (i >= this.g0 && this.i0 != 2) {
                int min = Math.min(i2, this.f0);
                if (min > 0) {
                    this.i0 = 3;
                    this.l0.onChanged(this.a + i, min, DiffingChangePayload.PLACEHOLDER_TO_ITEM);
                    this.f0 -= min;
                }
                int i3 = i2 - min;
                if (i3 > 0) {
                    this.l0.onInserted(i + min + this.a, i3);
                    return true;
                }
                return true;
            }
            return false;
        }

        public final boolean c(int i, int i2) {
            if (i <= 0 && this.h0 != 2) {
                int min = Math.min(i2, this.a);
                if (min > 0) {
                    this.h0 = 3;
                    this.l0.onChanged((0 - min) + this.a, min, DiffingChangePayload.PLACEHOLDER_TO_ITEM);
                    this.a -= min;
                }
                int i3 = i2 - min;
                if (i3 > 0) {
                    this.l0.onInserted(this.a + 0, i3);
                    return true;
                }
                return true;
            }
            return false;
        }

        public final boolean d(int i, int i2) {
            if (i + i2 >= this.g0 && this.i0 != 3) {
                int b = u33.b(Math.min(this.k0.d() - this.f0, i2), 0);
                int i3 = i2 - b;
                if (b > 0) {
                    this.i0 = 2;
                    this.l0.onChanged(this.a + i, b, DiffingChangePayload.ITEM_TO_PLACEHOLDER);
                    this.f0 += b;
                }
                if (i3 > 0) {
                    this.l0.onRemoved(i + b + this.a, i3);
                    return true;
                }
                return true;
            }
            return false;
        }

        public final boolean e(int i, int i2) {
            if (i <= 0 && this.h0 != 3) {
                int b = u33.b(Math.min(this.k0.c() - this.a, i2), 0);
                int i3 = i2 - b;
                if (i3 > 0) {
                    this.l0.onRemoved(this.a + 0, i3);
                }
                if (b > 0) {
                    this.h0 = 2;
                    this.l0.onChanged(this.a + 0, b, DiffingChangePayload.ITEM_TO_PLACEHOLDER);
                    this.a += b;
                    return true;
                }
                return true;
            }
            return false;
        }

        public final void f() {
            int min = Math.min(this.j0.c(), this.a);
            int c = this.k0.c() - this.a;
            if (c > 0) {
                if (min > 0) {
                    this.l0.onChanged(0, min, DiffingChangePayload.PLACEHOLDER_POSITION_CHANGE);
                }
                this.l0.onInserted(0, c);
            } else if (c < 0) {
                this.l0.onRemoved(0, -c);
                int i = min + c;
                if (i > 0) {
                    this.l0.onChanged(0, i, DiffingChangePayload.PLACEHOLDER_POSITION_CHANGE);
                }
            }
            this.a = this.k0.c();
        }

        public final void g() {
            f();
            h();
        }

        public final void h() {
            int min = Math.min(this.j0.d(), this.f0);
            int d = this.k0.d();
            int i = this.f0;
            int i2 = d - i;
            int i3 = this.a + this.g0 + i;
            int i4 = i3 - min;
            boolean z = i4 != this.j0.a() - min;
            if (i2 > 0) {
                this.l0.onInserted(i3, i2);
            } else if (i2 < 0) {
                this.l0.onRemoved(i3 + i2, -i2);
                min += i2;
            }
            if (min > 0 && z) {
                this.l0.onChanged(i4, min, DiffingChangePayload.PLACEHOLDER_POSITION_CHANGE);
            }
            this.f0 = this.k0.d();
        }

        @Override // defpackage.i02
        public void onChanged(int i, int i2, Object obj) {
            this.l0.onChanged(i + this.a, i2, obj);
        }

        @Override // defpackage.i02
        public void onInserted(int i, int i2) {
            if (!b(i, i2) && !c(i, i2)) {
                this.l0.onInserted(i + this.a, i2);
            }
            this.g0 += i2;
        }

        @Override // defpackage.i02
        public void onMoved(int i, int i2) {
            this.l0.onMoved(i + this.a, i2 + this.a);
        }

        @Override // defpackage.i02
        public void onRemoved(int i, int i2) {
            if (!d(i, i2) && !e(i, i2)) {
                this.l0.onRemoved(i + this.a, i2);
            }
            this.g0 -= i2;
        }
    }

    public final <T> void a(vi2<T> vi2Var, vi2<T> vi2Var2, i02 i02Var, ui2 ui2Var) {
        fs1.f(vi2Var, "oldList");
        fs1.f(vi2Var2, "newList");
        fs1.f(i02Var, "callback");
        fs1.f(ui2Var, "diffResult");
        a aVar = new a(vi2Var, vi2Var2, i02Var);
        ui2Var.a().c(aVar);
        aVar.g();
    }
}
