package defpackage;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: EthLog.java */
/* renamed from: ex0  reason: default package */
/* loaded from: classes3.dex */
public class ex0 extends i83<List<c>> {

    /* compiled from: EthLog.java */
    /* renamed from: ex0$a */
    /* loaded from: classes3.dex */
    public static class a implements c<String> {
        private String value;

        public a() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                String str = this.value;
                String str2 = ((a) obj).value;
                return str != null ? str.equals(str2) : str2 == null;
            }
            return false;
        }

        public int hashCode() {
            String str = this.value;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public void setValue(String str) {
            this.value = str;
        }

        public a(String str) {
            this.value = str;
        }

        @Override // defpackage.ex0.c
        public String get() {
            return this.value;
        }
    }

    /* compiled from: EthLog.java */
    /* renamed from: ex0$b */
    /* loaded from: classes3.dex */
    public static class b extends q12 implements c<q12> {
        public b() {
        }

        @Override // defpackage.ex0.c
        public q12 get() {
            return this;
        }

        public b(boolean z, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, List<String> list) {
            super(z, str, str2, str3, str4, str5, str6, str7, str8, list);
        }
    }

    /* compiled from: EthLog.java */
    /* renamed from: ex0$c */
    /* loaded from: classes3.dex */
    public interface c<T> {
        T get();
    }

    /* compiled from: EthLog.java */
    /* renamed from: ex0$d */
    /* loaded from: classes3.dex */
    public static class d extends com.fasterxml.jackson.databind.c<List<c>> {
        private ObjectReader objectReader = ml2.getObjectReader();

        @Override // com.fasterxml.jackson.databind.c
        public List<c> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            ArrayList arrayList = new ArrayList();
            JsonToken T0 = jsonParser.T0();
            if (T0 == JsonToken.START_OBJECT) {
                Iterator readValues = this.objectReader.readValues(jsonParser, b.class);
                while (readValues.hasNext()) {
                    arrayList.add(readValues.next());
                }
            } else if (T0 == JsonToken.VALUE_STRING) {
                jsonParser.x0();
                Iterator readValues2 = this.objectReader.readValues(jsonParser, a.class);
                while (readValues2.hasNext()) {
                    arrayList.add(readValues2.next());
                }
            }
            return arrayList;
        }
    }

    public List<c> getLogs() {
        return getResult();
    }

    @Override // defpackage.i83
    @com.fasterxml.jackson.databind.annotation.b(using = d.class)
    public void setResult(List<c> list) {
        super.setResult((ex0) list);
    }
}
