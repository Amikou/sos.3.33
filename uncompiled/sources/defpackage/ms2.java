package defpackage;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;

/* compiled from: PointerIconCompat.java */
/* renamed from: ms2  reason: default package */
/* loaded from: classes.dex */
public final class ms2 {
    public Object a;

    public ms2(Object obj) {
        this.a = obj;
    }

    public static ms2 b(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new ms2(PointerIcon.getSystemIcon(context, i));
        }
        return new ms2(null);
    }

    public Object a() {
        return this.a;
    }
}
