package defpackage;

import android.graphics.Point;
import android.graphics.RectF;
import android.view.View;
import com.alexvasilkov.gestures.GestureController;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ExitController.java */
/* renamed from: bz0  reason: default package */
/* loaded from: classes.dex */
public class bz0 {
    public static final RectF q = new RectF();
    public static final Point r = new Point();
    public final float a;
    public final GestureController b;
    public final te c;
    public boolean e;
    public boolean f;
    public boolean g;
    public boolean h;
    public boolean i;
    public boolean j;
    public float k;
    public float l;
    public float n;
    public float o;
    public float p;
    public float d = 1.0f;
    public float m = 1.0f;

    public bz0(View view, GestureController gestureController) {
        this.b = gestureController;
        this.c = view instanceof te ? (te) view : null;
        this.a = ye4.a(view.getContext(), 30.0f);
    }

    public void a() {
        this.p = this.b.p().b(this.p);
    }

    public final boolean b() {
        te teVar;
        return (!this.b.n().A() || (teVar = this.c) == null || teVar.getPositionAnimator().x()) ? false : true;
    }

    public final boolean c() {
        Settings.ExitType h = this.b.n().h();
        return (h == Settings.ExitType.ALL || h == Settings.ExitType.SCROLL) && !this.e && !this.f && h();
    }

    public final boolean d() {
        Settings.ExitType h = this.b.n().h();
        return (h == Settings.ExitType.ALL || h == Settings.ExitType.ZOOM) && !this.f && h();
    }

    public final boolean e(float f) {
        if (this.b.n().F()) {
            us3 o = this.b.o();
            vs3 p = this.b.p();
            RectF rectF = q;
            p.g(o, rectF);
            if (f <= Utils.FLOAT_EPSILON || us3.a(o.g(), rectF.bottom) >= Utils.FLOAT_EPSILON) {
                return f < Utils.FLOAT_EPSILON && ((float) us3.a(o.g(), rectF.top)) > Utils.FLOAT_EPSILON;
            }
            return true;
        }
        return true;
    }

    public final void f() {
        if (g()) {
            GestureController gestureController = this.b;
            if (gestureController instanceof df1) {
                ((df1) gestureController).Y(false);
            }
            this.b.n().c();
            tj4 positionAnimator = this.c.getPositionAnimator();
            if (!positionAnimator.w() && b()) {
                float u = positionAnimator.u();
                if (u < 0.75f) {
                    positionAnimator.r(true);
                } else {
                    float g = this.b.o().g();
                    float h = this.b.o().h();
                    boolean z = this.i && us3.c(g, this.o);
                    boolean z2 = this.j && us3.c(h, this.p);
                    if (u < 1.0f) {
                        positionAnimator.C(u, false, true);
                        if (!z && !z2) {
                            this.b.n().c();
                            this.b.k();
                            this.b.n().a();
                        }
                    }
                }
            }
        }
        this.i = false;
        this.j = false;
        this.g = false;
        this.d = 1.0f;
        this.n = Utils.FLOAT_EPSILON;
        this.k = Utils.FLOAT_EPSILON;
        this.l = Utils.FLOAT_EPSILON;
        this.m = 1.0f;
    }

    public boolean g() {
        return this.i || this.j;
    }

    public final boolean h() {
        us3 o = this.b.o();
        return us3.a(o.h(), this.b.p().f(o)) <= 0;
    }

    public boolean i() {
        return g();
    }

    public boolean j() {
        return g();
    }

    public void k() {
        this.f = true;
    }

    public void l() {
        this.f = false;
    }

    public boolean m(float f) {
        if (!d()) {
            this.h = true;
        }
        if (!this.h && !g() && b() && f < 1.0f) {
            float f2 = this.m * f;
            this.m = f2;
            if (f2 < 0.75f) {
                this.j = true;
                this.p = this.b.o().h();
                r();
            }
        }
        if (this.j) {
            float h = (this.b.o().h() * f) / this.p;
            this.d = h;
            this.d = v42.f(h, 0.01f, 1.0f);
            Settings n = this.b.n();
            Point point = r;
            si1.a(n, point);
            if (this.d == 1.0f) {
                this.b.o().r(this.p, point.x, point.y);
            } else {
                this.b.o().q(((f - 1.0f) * 0.75f) + 1.0f, point.x, point.y);
            }
            t();
            if (this.d == 1.0f) {
                f();
                return true;
            }
        }
        return g();
    }

    public void n() {
        this.e = true;
    }

    public void o() {
        this.e = false;
        this.h = false;
        if (this.j) {
            f();
        }
    }

    public boolean p(float f, float f2) {
        if (!this.g && !g() && b() && c() && !e(f2)) {
            this.k += f;
            float f3 = this.l + f2;
            this.l = f3;
            if (Math.abs(f3) > this.a) {
                this.i = true;
                this.o = this.b.o().g();
                r();
            } else if (Math.abs(this.k) > this.a) {
                this.g = true;
            }
        }
        if (this.i) {
            if (this.n == Utils.FLOAT_EPSILON) {
                this.n = Math.signum(f2);
            }
            if (this.d < 0.75f && Math.signum(f2) == this.n) {
                f2 *= this.d / 0.75f;
            }
            float g = 1.0f - (((this.b.o().g() + f2) - this.o) / ((this.n * 0.5f) * Math.max(this.b.n().p(), this.b.n().o())));
            this.d = g;
            float f4 = v42.f(g, 0.01f, 1.0f);
            this.d = f4;
            if (f4 == 1.0f) {
                this.b.o().o(this.b.o().f(), this.o);
            } else {
                this.b.o().n(Utils.FLOAT_EPSILON, f2);
            }
            t();
            if (this.d == 1.0f) {
                f();
            }
            return true;
        }
        return g();
    }

    public void q() {
        f();
    }

    public final void r() {
        this.b.n().a();
        GestureController gestureController = this.b;
        if (gestureController instanceof df1) {
            ((df1) gestureController).Y(true);
        }
    }

    public void s() {
        if (g()) {
            this.d = 1.0f;
            t();
            f();
        }
    }

    public final void t() {
        if (b()) {
            this.c.getPositionAnimator().D(this.b.o(), this.d);
            this.c.getPositionAnimator().C(this.d, false, false);
        }
    }
}
