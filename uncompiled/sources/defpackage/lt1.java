package defpackage;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

/* compiled from: Iterables.java */
/* renamed from: lt1  reason: default package */
/* loaded from: classes2.dex */
public final class lt1 {
    public static <T> boolean a(Iterable<T> iterable, gu2<? super T> gu2Var) {
        return Iterators.b(iterable.iterator(), gu2Var);
    }

    public static <E> Collection<E> b(Iterable<E> iterable) {
        if (iterable instanceof Collection) {
            return (Collection) iterable;
        }
        return Lists.j(iterable.iterator());
    }

    public static <T> T c(Iterable<? extends T> iterable, T t) {
        return (T) Iterators.l(iterable.iterator(), t);
    }

    public static <T> T d(Iterable<T> iterable) {
        if (iterable instanceof List) {
            List list = (List) iterable;
            if (!list.isEmpty()) {
                return (T) e(list);
            }
            throw new NoSuchElementException();
        }
        return (T) Iterators.k(iterable.iterator());
    }

    public static <T> T e(List<T> list) {
        return list.get(list.size() - 1);
    }

    public static <T> boolean f(Iterable<T> iterable, gu2<? super T> gu2Var) {
        if ((iterable instanceof RandomAccess) && (iterable instanceof List)) {
            return g((List) iterable, (gu2) au2.k(gu2Var));
        }
        return Iterators.p(iterable.iterator(), gu2Var);
    }

    public static <T> boolean g(List<T> list, gu2<? super T> gu2Var) {
        int i = 0;
        int i2 = 0;
        while (i < list.size()) {
            T t = list.get(i);
            if (!gu2Var.apply(t)) {
                if (i > i2) {
                    try {
                        list.set(i2, t);
                    } catch (IllegalArgumentException unused) {
                        h(list, gu2Var, i2, i);
                        return true;
                    } catch (UnsupportedOperationException unused2) {
                        h(list, gu2Var, i2, i);
                        return true;
                    }
                }
                i2++;
            }
            i++;
        }
        list.subList(i2, list.size()).clear();
        return i != i2;
    }

    public static <T> void h(List<T> list, gu2<? super T> gu2Var, int i, int i2) {
        for (int size = list.size() - 1; size > i2; size--) {
            if (gu2Var.apply(list.get(size))) {
                list.remove(size);
            }
        }
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            list.remove(i3);
        }
    }

    public static Object[] i(Iterable<?> iterable) {
        return b(iterable).toArray();
    }

    public static <T> T[] j(Iterable<? extends T> iterable, T[] tArr) {
        return (T[]) b(iterable).toArray(tArr);
    }
}
