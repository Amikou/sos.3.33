package defpackage;

import android.content.Context;

/* compiled from: BatteryChargingController.java */
/* renamed from: no  reason: default package */
/* loaded from: classes.dex */
public class no extends d60<Boolean> {
    public no(Context context, q34 q34Var) {
        super(j84.c(context, q34Var).a());
    }

    @Override // defpackage.d60
    public boolean b(tq4 tq4Var) {
        return tq4Var.j.g();
    }

    @Override // defpackage.d60
    /* renamed from: i */
    public boolean c(Boolean bool) {
        return !bool.booleanValue();
    }
}
