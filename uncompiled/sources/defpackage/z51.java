package defpackage;

import com.facebook.datasource.AbstractDataSource;
import java.util.List;

/* compiled from: FirstAvailableDataSourceSupplier.java */
/* renamed from: z51  reason: default package */
/* loaded from: classes.dex */
public class z51<T> implements fw3<ge0<T>> {
    public final List<fw3<ge0<T>>> a;

    /* compiled from: FirstAvailableDataSourceSupplier.java */
    /* renamed from: z51$b */
    /* loaded from: classes.dex */
    public class b extends AbstractDataSource<T> {
        public int i = 0;
        public ge0<T> j = null;
        public ge0<T> k = null;

        /* compiled from: FirstAvailableDataSourceSupplier.java */
        /* renamed from: z51$b$a */
        /* loaded from: classes.dex */
        public class a implements ke0<T> {
            public a() {
            }

            @Override // defpackage.ke0
            public void a(ge0<T> ge0Var) {
                b.this.s(Math.max(b.this.d(), ge0Var.d()));
            }

            @Override // defpackage.ke0
            public void b(ge0<T> ge0Var) {
                b.this.E(ge0Var);
            }

            @Override // defpackage.ke0
            public void c(ge0<T> ge0Var) {
            }

            @Override // defpackage.ke0
            public void d(ge0<T> ge0Var) {
                if (ge0Var.a()) {
                    b.this.F(ge0Var);
                } else if (ge0Var.b()) {
                    b.this.E(ge0Var);
                }
            }
        }

        public b() {
            if (H()) {
                return;
            }
            p(new RuntimeException("No data source supplier or supplier returned null."));
        }

        public final void A(ge0<T> ge0Var) {
            if (ge0Var != null) {
                ge0Var.close();
            }
        }

        public final synchronized ge0<T> B() {
            return this.k;
        }

        public final synchronized fw3<ge0<T>> C() {
            if (k() || this.i >= z51.this.a.size()) {
                return null;
            }
            List list = z51.this.a;
            int i = this.i;
            this.i = i + 1;
            return (fw3) list.get(i);
        }

        public final void D(ge0<T> ge0Var, boolean z) {
            ge0<T> ge0Var2;
            synchronized (this) {
                if (ge0Var == this.j && ge0Var != (ge0Var2 = this.k)) {
                    if (ge0Var2 != null && !z) {
                        ge0Var2 = null;
                        A(ge0Var2);
                    }
                    this.k = ge0Var;
                    A(ge0Var2);
                }
            }
        }

        public final void E(ge0<T> ge0Var) {
            if (z(ge0Var)) {
                if (ge0Var != B()) {
                    A(ge0Var);
                }
                if (H()) {
                    return;
                }
                q(ge0Var.c(), ge0Var.getExtras());
            }
        }

        public final void F(ge0<T> ge0Var) {
            D(ge0Var, ge0Var.b());
            if (ge0Var == B()) {
                u(null, ge0Var.b(), ge0Var.getExtras());
            }
        }

        public final synchronized boolean G(ge0<T> ge0Var) {
            if (k()) {
                return false;
            }
            this.j = ge0Var;
            return true;
        }

        public final boolean H() {
            fw3<ge0<T>> C = C();
            ge0<T> ge0Var = C != null ? C.get() : null;
            if (G(ge0Var) && ge0Var != null) {
                ge0Var.e(new a(), bv.a());
                return true;
            }
            A(ge0Var);
            return false;
        }

        @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
        public synchronized boolean a() {
            boolean z;
            ge0<T> B = B();
            if (B != null) {
                z = B.a();
            }
            return z;
        }

        @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
        public boolean close() {
            synchronized (this) {
                if (super.close()) {
                    ge0<T> ge0Var = this.j;
                    this.j = null;
                    ge0<T> ge0Var2 = this.k;
                    this.k = null;
                    A(ge0Var2);
                    A(ge0Var);
                    return true;
                }
                return false;
            }
        }

        @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
        public synchronized T g() {
            ge0<T> B;
            B = B();
            return B != null ? B.g() : null;
        }

        public final synchronized boolean z(ge0<T> ge0Var) {
            if (!k() && ge0Var == this.j) {
                this.j = null;
                return true;
            }
            return false;
        }
    }

    public z51(List<fw3<ge0<T>>> list) {
        xt2.c(!list.isEmpty(), "List of suppliers is empty!");
        this.a = list;
    }

    public static <T> z51<T> b(List<fw3<ge0<T>>> list) {
        return new z51<>(list);
    }

    @Override // defpackage.fw3
    /* renamed from: c */
    public ge0<T> get() {
        return new b();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof z51) {
            return ol2.a(this.a, ((z51) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return ol2.c(this).b("list", this.a).toString();
    }
}
