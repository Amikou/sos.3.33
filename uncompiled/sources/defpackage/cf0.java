package defpackage;

import android.util.SparseIntArray;

/* compiled from: DebugOverlayImageOriginColor.java */
/* renamed from: cf0  reason: default package */
/* loaded from: classes.dex */
public class cf0 {
    public static final SparseIntArray a;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray(7);
        a = sparseIntArray;
        sparseIntArray.append(1, -7829368);
        sparseIntArray.append(2, -65536);
        sparseIntArray.append(3, -256);
        sparseIntArray.append(4, -256);
        sparseIntArray.append(5, -16711936);
        sparseIntArray.append(6, -16711936);
        sparseIntArray.append(7, -16711936);
    }

    public static int a(int i) {
        return a.get(i, -1);
    }
}
