package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* compiled from: BroadcastReceiverConstraintTracker.java */
/* renamed from: nr  reason: default package */
/* loaded from: classes.dex */
public abstract class nr<T> extends g60<T> {
    public static final String h = v12.f("BrdcstRcvrCnstrntTrckr");
    public final BroadcastReceiver g;

    /* compiled from: BroadcastReceiverConstraintTracker.java */
    /* renamed from: nr$a */
    /* loaded from: classes.dex */
    public class a extends BroadcastReceiver {
        public a() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                nr.this.h(context, intent);
            }
        }
    }

    public nr(Context context, q34 q34Var) {
        super(context, q34Var);
        this.g = new a();
    }

    @Override // defpackage.g60
    public void e() {
        v12.c().a(h, String.format("%s: registering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.b.registerReceiver(this.g, g());
    }

    @Override // defpackage.g60
    public void f() {
        v12.c().a(h, String.format("%s: unregistering receiver", getClass().getSimpleName()), new Throwable[0]);
        this.b.unregisterReceiver(this.g);
    }

    public abstract IntentFilter g();

    public abstract void h(Context context, Intent intent);
}
