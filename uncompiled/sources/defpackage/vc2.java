package defpackage;

import java.util.Arrays;

/* compiled from: NalUnitTargetBuffer.java */
/* renamed from: vc2  reason: default package */
/* loaded from: classes.dex */
public final class vc2 {
    public final int a;
    public boolean b;
    public boolean c;
    public byte[] d;
    public int e;

    public vc2(int i, int i2) {
        this.a = i;
        byte[] bArr = new byte[i2 + 3];
        this.d = bArr;
        bArr[2] = 1;
    }

    public void a(byte[] bArr, int i, int i2) {
        if (this.b) {
            int i3 = i2 - i;
            byte[] bArr2 = this.d;
            int length = bArr2.length;
            int i4 = this.e;
            if (length < i4 + i3) {
                this.d = Arrays.copyOf(bArr2, (i4 + i3) * 2);
            }
            System.arraycopy(bArr, i, this.d, this.e, i3);
            this.e += i3;
        }
    }

    public boolean b(int i) {
        if (this.b) {
            this.e -= i;
            this.b = false;
            this.c = true;
            return true;
        }
        return false;
    }

    public boolean c() {
        return this.c;
    }

    public void d() {
        this.b = false;
        this.c = false;
    }

    public void e(int i) {
        ii.g(!this.b);
        boolean z = i == this.a;
        this.b = z;
        if (z) {
            this.e = 3;
            this.c = false;
        }
    }
}
