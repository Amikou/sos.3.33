package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: vh5  reason: default package */
/* loaded from: classes.dex */
public abstract class vh5 extends bf5 {
    public boolean b;

    public vh5(ck5 ck5Var) {
        super(ck5Var);
        this.a.l();
    }

    public final boolean f() {
        return this.b;
    }

    public final void g() {
        if (!f()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void h() {
        if (!this.b) {
            if (j()) {
                return;
            }
            this.a.n();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    public final void i() {
        if (!this.b) {
            k();
            this.a.n();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    public abstract boolean j();

    public void k() {
    }
}
