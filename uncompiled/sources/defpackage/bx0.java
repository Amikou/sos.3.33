package defpackage;

import java.math.BigInteger;

/* compiled from: EthGetUncleCountByBlockNumber.java */
/* renamed from: bx0  reason: default package */
/* loaded from: classes3.dex */
public class bx0 extends i83<String> {
    public BigInteger getUncleCount() {
        return ej2.decodeQuantity(getResult());
    }
}
