package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: pa5  reason: default package */
/* loaded from: classes.dex */
public final class pa5 implements Runnable {
    public final /* synthetic */ long a;
    public final /* synthetic */ pc5 f0;

    public pa5(pc5 pc5Var, long j) {
        this.f0 = pc5Var;
        this.a = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.o(this.a);
    }
}
