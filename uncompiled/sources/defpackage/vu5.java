package defpackage;

import com.google.android.gms.internal.measurement.b;
import java.util.HashMap;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: vu5  reason: default package */
/* loaded from: classes.dex */
public final class vu5 extends c55 {
    public final b g0;

    public vu5(b bVar) {
        super("internal.eventLogger");
        this.g0 = bVar;
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        vm5.a(this.a, 3, list);
        String zzc = wk5Var.a(list.get(0)).zzc();
        long i = (long) vm5.i(wk5Var.a(list.get(1)).b().doubleValue());
        z55 a = wk5Var.a(list.get(2));
        HashMap hashMap = new HashMap();
        if (a instanceof p55) {
            p55 p55Var = (p55) a;
            for (String str : p55Var.a()) {
                Object j = vm5.j(p55Var.e(str));
                if (j != null) {
                    hashMap.put(str, j);
                }
            }
        }
        this.g0.e(zzc, i, hashMap);
        return z55.X;
    }
}
