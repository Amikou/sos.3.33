package defpackage;

import com.google.android.play.core.internal.d;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: wy4  reason: default package */
/* loaded from: classes2.dex */
public final class wy4 implements jw4<Executor> {
    public final /* synthetic */ int a = 0;

    public wy4() {
    }

    public wy4(byte[] bArr) {
    }

    public wy4(char[] cArr) {
    }

    public wy4(short[] sArr) {
    }

    public static Executor b() {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(zx4.f0);
        d.k(newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    public static Executor c() {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(zx4.g0);
        d.k(newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    public static vu4 d() {
        return new vu4();
    }

    public static fv4 e() {
        return new fv4();
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [vu4, java.util.concurrent.Executor] */
    /* JADX WARN: Type inference failed for: r0v4, types: [fv4, java.util.concurrent.Executor] */
    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ Executor a() {
        int i = this.a;
        return i != 0 ? i != 1 ? i != 2 ? e() : d() : c() : b();
    }
}
