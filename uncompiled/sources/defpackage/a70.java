package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.database.mainRoom.MainRoomDatabase;

/* compiled from: ContactTokenProvider.kt */
/* renamed from: a70  reason: default package */
/* loaded from: classes2.dex */
public final class a70 {
    public static final a70 a = new a70();
    public static v60 b;

    public final v60 a(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new v60(MainRoomDatabase.n.b(context).O());
                }
                te4 te4Var = te4.a;
            }
        }
        v60 v60Var = b;
        fs1.d(v60Var);
        return v60Var;
    }

    public final void b() {
        b = null;
    }
}
