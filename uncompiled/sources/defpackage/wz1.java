package defpackage;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;

/* compiled from: Dispatcher.kt */
/* renamed from: wz1  reason: default package */
/* loaded from: classes2.dex */
public final class wz1 extends ExecutorCoroutineDispatcher implements p34, Executor {
    public static final /* synthetic */ AtomicIntegerFieldUpdater k0 = AtomicIntegerFieldUpdater.newUpdater(wz1.class, "inFlightTasks");
    public final z01 f0;
    public final int g0;
    public final String h0;
    public final int i0;
    public final ConcurrentLinkedQueue<Runnable> j0 = new ConcurrentLinkedQueue<>();
    private volatile /* synthetic */ int inFlightTasks = 0;

    public wz1(z01 z01Var, int i, String str, int i2) {
        this.f0 = z01Var;
        this.g0 = i;
        this.h0 = str;
        this.i0 = i2;
    }

    @Override // defpackage.p34
    public void c() {
        Runnable poll = this.j0.poll();
        if (poll != null) {
            this.f0.M(poll, this, true);
            return;
        }
        k0.decrementAndGet(this);
        Runnable poll2 = this.j0.poll();
        if (poll2 == null) {
            return;
        }
        m(poll2, true);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @Override // defpackage.p34
    public int e() {
        return this.i0;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        m(runnable, false);
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        m(runnable, false);
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void i(CoroutineContext coroutineContext, Runnable runnable) {
        m(runnable, true);
    }

    public final void m(Runnable runnable, boolean z) {
        do {
            AtomicIntegerFieldUpdater atomicIntegerFieldUpdater = k0;
            if (atomicIntegerFieldUpdater.incrementAndGet(this) <= this.g0) {
                this.f0.M(runnable, this, z);
                return;
            }
            this.j0.add(runnable);
            if (atomicIntegerFieldUpdater.decrementAndGet(this) >= this.g0) {
                return;
            }
            runnable = this.j0.poll();
        } while (runnable != null);
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        String str = this.h0;
        if (str == null) {
            return super.toString() + "[dispatcher = " + this.f0 + ']';
        }
        return str;
    }
}
