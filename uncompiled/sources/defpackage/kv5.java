package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: kv5  reason: default package */
/* loaded from: classes.dex */
public final class kv5 extends br5<Integer> implements RandomAccess, vv5, hy5 {
    public static final kv5 h0;
    public int[] f0;
    public int g0;

    static {
        kv5 kv5Var = new kv5(new int[0], 0);
        h0 = kv5Var;
        kv5Var.zzb();
    }

    public kv5() {
        this(new int[10], 0);
    }

    public static kv5 i() {
        return h0;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ void add(int i, Object obj) {
        int i2;
        int intValue = ((Integer) obj).intValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            int[] iArr = this.f0;
            if (i2 < iArr.length) {
                System.arraycopy(iArr, i, iArr, i + 1, i2 - i);
            } else {
                int[] iArr2 = new int[((i2 * 3) / 2) + 1];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                System.arraycopy(this.f0, i, iArr2, i + 1, this.g0 - i);
                this.f0 = iArr2;
            }
            this.f0[i] = intValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(o(i));
    }

    @Override // defpackage.br5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Integer> collection) {
        e();
        cw5.a(collection);
        if (!(collection instanceof kv5)) {
            return super.addAll(collection);
        }
        kv5 kv5Var = (kv5) collection;
        int i = kv5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.f0;
            if (i3 > iArr.length) {
                this.f0 = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(kv5Var.f0, 0, this.f0, this.g0, kv5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kv5)) {
            return super.equals(obj);
        }
        kv5 kv5Var = (kv5) obj;
        if (this.g0 != kv5Var.g0) {
            return false;
        }
        int[] iArr = kv5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // defpackage.zv5
    /* renamed from: g0 */
    public final vv5 Q(int i) {
        if (i >= this.g0) {
            return new kv5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object get(int i) {
        n(i);
        return Integer.valueOf(this.f0[i]);
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + this.f0[i2];
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            int i = this.g0;
            for (int i2 = 0; i2 < i; i2++) {
                if (this.f0[i2] == intValue) {
                    return i2;
                }
            }
            return -1;
        }
        return -1;
    }

    public final int k(int i) {
        n(i);
        return this.f0[i];
    }

    public final void m(int i) {
        e();
        int i2 = this.g0;
        int[] iArr = this.f0;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[((i2 * 3) / 2) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.f0 = iArr2;
        }
        int[] iArr3 = this.f0;
        int i3 = this.g0;
        this.g0 = i3 + 1;
        iArr3[i3] = i;
    }

    public final void n(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(o(i));
        }
    }

    public final String o(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object remove(int i) {
        int i2;
        e();
        n(i);
        int[] iArr = this.f0;
        int i3 = iArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i3);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            int[] iArr = this.f0;
            System.arraycopy(iArr, i2, iArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        e();
        n(i);
        int[] iArr = this.f0;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    public kv5(int[] iArr, int i) {
        this.f0 = iArr;
        this.g0 = i;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* bridge */ /* synthetic */ boolean add(Object obj) {
        m(((Integer) obj).intValue());
        return true;
    }
}
