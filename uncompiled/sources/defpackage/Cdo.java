package defpackage;

import android.os.SystemClock;
import android.util.Pair;
import androidx.media3.common.util.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* compiled from: BaseUrlExclusionList.java */
/* renamed from: do  reason: invalid class name and default package */
/* loaded from: classes.dex */
public final class Cdo {
    public final Map<String, Long> a;
    public final Map<Integer, Long> b;
    public final Map<List<Pair<String, Integer>>, bo> c;
    public final Random d;

    public Cdo() {
        this(new Random());
    }

    public static <T> void b(T t, long j, Map<T, Long> map) {
        if (map.containsKey(t)) {
            j = Math.max(j, ((Long) b.j(map.get(t))).longValue());
        }
        map.put(t, Long.valueOf(j));
    }

    public static int d(bo boVar, bo boVar2) {
        int compare = Integer.compare(boVar.c, boVar2.c);
        return compare != 0 ? compare : boVar.b.compareTo(boVar2.b);
    }

    public static int f(List<bo> list) {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < list.size(); i++) {
            hashSet.add(Integer.valueOf(list.get(i).c));
        }
        return hashSet.size();
    }

    public static <T> void h(long j, Map<T, Long> map) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<T, Long> entry : map.entrySet()) {
            if (entry.getValue().longValue() <= j) {
                arrayList.add(entry.getKey());
            }
        }
        for (int i = 0; i < arrayList.size(); i++) {
            map.remove(arrayList.get(i));
        }
    }

    public final List<bo> c(List<bo> list) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        h(elapsedRealtime, this.a);
        h(elapsedRealtime, this.b);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            bo boVar = list.get(i);
            if (!this.a.containsKey(boVar.b) && !this.b.containsKey(Integer.valueOf(boVar.c))) {
                arrayList.add(boVar);
            }
        }
        return arrayList;
    }

    public void e(bo boVar, long j) {
        long elapsedRealtime = SystemClock.elapsedRealtime() + j;
        b(boVar.b, elapsedRealtime, this.a);
        int i = boVar.c;
        if (i != Integer.MIN_VALUE) {
            b(Integer.valueOf(i), elapsedRealtime, this.b);
        }
    }

    public int g(List<bo> list) {
        HashSet hashSet = new HashSet();
        List<bo> c = c(list);
        for (int i = 0; i < c.size(); i++) {
            hashSet.add(Integer.valueOf(c.get(i).c));
        }
        return hashSet.size();
    }

    public void i() {
        this.a.clear();
        this.b.clear();
        this.c.clear();
    }

    public bo j(List<bo> list) {
        List<bo> c = c(list);
        if (c.size() < 2) {
            return (bo) lt1.c(c, null);
        }
        Collections.sort(c, co.a);
        ArrayList arrayList = new ArrayList();
        int i = c.get(0).c;
        int i2 = 0;
        while (true) {
            if (i2 >= c.size()) {
                break;
            }
            bo boVar = c.get(i2);
            if (i != boVar.c) {
                if (arrayList.size() == 1) {
                    return c.get(0);
                }
            } else {
                arrayList.add(new Pair(boVar.b, Integer.valueOf(boVar.d)));
                i2++;
            }
        }
        bo boVar2 = this.c.get(arrayList);
        if (boVar2 == null) {
            bo k = k(c.subList(0, arrayList.size()));
            this.c.put(arrayList, k);
            return k;
        }
        return boVar2;
    }

    public final bo k(List<bo> list) {
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            i += list.get(i2).d;
        }
        int nextInt = this.d.nextInt(i);
        int i3 = 0;
        for (int i4 = 0; i4 < list.size(); i4++) {
            bo boVar = list.get(i4);
            i3 += boVar.d;
            if (nextInt < i3) {
                return boVar;
            }
        }
        return (bo) lt1.d(list);
    }

    public Cdo(Random random) {
        this.c = new HashMap();
        this.d = random;
        this.a = new HashMap();
        this.b = new HashMap();
    }
}
