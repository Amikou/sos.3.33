package defpackage;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuPresenter;
import androidx.appcompat.widget.ActionMenuView;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: AbsActionBarView.java */
/* renamed from: n4  reason: default package */
/* loaded from: classes.dex */
public abstract class n4 extends ViewGroup {
    public final a a;
    public final Context f0;
    public ActionMenuView g0;
    public ActionMenuPresenter h0;
    public int i0;
    public vj4 j0;
    public boolean k0;
    public boolean l0;

    /* compiled from: AbsActionBarView.java */
    /* renamed from: n4$a */
    /* loaded from: classes.dex */
    public class a implements xj4 {
        public boolean a = false;
        public int b;

        public a() {
        }

        @Override // defpackage.xj4
        public void a(View view) {
            this.a = true;
        }

        @Override // defpackage.xj4
        public void b(View view) {
            if (this.a) {
                return;
            }
            n4 n4Var = n4.this;
            n4Var.j0 = null;
            n4.super.setVisibility(this.b);
        }

        @Override // defpackage.xj4
        public void c(View view) {
            n4.super.setVisibility(0);
            this.a = false;
        }

        public a d(vj4 vj4Var, int i) {
            n4.this.j0 = vj4Var;
            this.b = i;
            return this;
        }
    }

    public n4(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public static int d(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    public int c(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    public int e(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 + ((i3 - measuredHeight) / 2);
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }

    public vj4 f(int i, long j) {
        vj4 vj4Var = this.j0;
        if (vj4Var != null) {
            vj4Var.b();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                setAlpha(Utils.FLOAT_EPSILON);
            }
            vj4 a2 = ei4.e(this).a(1.0f);
            a2.d(j);
            a2.f(this.a.d(a2, i));
            return a2;
        }
        vj4 a3 = ei4.e(this).a(Utils.FLOAT_EPSILON);
        a3.d(j);
        a3.f(this.a.d(a3, i));
        return a3;
    }

    public int getAnimatedVisibility() {
        if (this.j0 != null) {
            return this.a.b;
        }
        return getVisibility();
    }

    public int getContentHeight() {
        return this.i0;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, d33.ActionBar, jy2.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(d33.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        ActionMenuPresenter actionMenuPresenter = this.h0;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.I(configuration);
        }
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.l0 = false;
        }
        if (!this.l0) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.l0 = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.l0 = false;
        }
        return true;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.k0 = false;
        }
        if (!this.k0) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.k0 = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.k0 = false;
        }
        return true;
    }

    public abstract void setContentHeight(int i);

    @Override // android.view.View
    public void setVisibility(int i) {
        if (i != getVisibility()) {
            vj4 vj4Var = this.j0;
            if (vj4Var != null) {
                vj4Var.b();
            }
            super.setVisibility(i);
        }
    }

    public n4(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new a();
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(jy2.actionBarPopupTheme, typedValue, true) && typedValue.resourceId != 0) {
            this.f0 = new ContextThemeWrapper(context, typedValue.resourceId);
        } else {
            this.f0 = context;
        }
    }
}
