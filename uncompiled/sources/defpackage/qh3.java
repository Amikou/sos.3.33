package defpackage;

import java.math.BigInteger;

/* renamed from: qh3  reason: default package */
/* loaded from: classes2.dex */
public class qh3 {
    public static final long[] a = {3161836309350906777L, -7642453882179322845L, -3821226941089661423L, 7312758566309945096L, -556661012383879292L, 8945041530681231562L, -4750851271514160027L, 6847946401097695794L, 541669439031730457L};

    public static void a(long[] jArr, int i, long[] jArr2, int i2, long[] jArr3, int i3) {
        for (int i4 = 0; i4 < 9; i4++) {
            jArr3[i3 + i4] = jArr[i + i4] ^ jArr2[i2 + i4];
        }
    }

    public static void b(long[] jArr, long[] jArr2, long[] jArr3) {
        for (int i = 0; i < 9; i++) {
            jArr3[i] = jArr[i] ^ jArr2[i];
        }
    }

    public static void c(long[] jArr, int i, long[] jArr2, int i2, long[] jArr3, int i3) {
        for (int i4 = 0; i4 < 9; i4++) {
            int i5 = i3 + i4;
            jArr3[i5] = jArr3[i5] ^ (jArr[i + i4] ^ jArr2[i2 + i4]);
        }
    }

    public static void d(long[] jArr, long[] jArr2, long[] jArr3) {
        for (int i = 0; i < 9; i++) {
            jArr3[i] = jArr3[i] ^ (jArr[i] ^ jArr2[i]);
        }
    }

    public static void e(long[] jArr, long[] jArr2, long[] jArr3) {
        for (int i = 0; i < 18; i++) {
            jArr3[i] = jArr[i] ^ jArr2[i];
        }
    }

    public static void f(long[] jArr, long[] jArr2) {
        jArr2[0] = jArr[0] ^ 1;
        for (int i = 1; i < 9; i++) {
            jArr2[i] = jArr[i];
        }
    }

    public static long[] g(BigInteger bigInteger) {
        long[] e = jd2.e(bigInteger);
        r(e, 0);
        return e;
    }

    public static void h(long[] jArr, long[] jArr2, long[] jArr3) {
        i(jArr, p(jArr2), jArr3);
    }

    public static void i(long[] jArr, long[] jArr2, long[] jArr3) {
        for (int i = 56; i >= 0; i -= 8) {
            for (int i2 = 1; i2 < 9; i2 += 2) {
                int i3 = (int) (jArr[i2] >>> i);
                c(jArr2, (i3 & 15) * 9, jArr2, (((i3 >>> 4) & 15) + 16) * 9, jArr3, i2 - 1);
            }
            kd2.I(16, jArr3, 0, 8, 0L);
        }
        for (int i4 = 56; i4 >= 0; i4 -= 8) {
            for (int i5 = 0; i5 < 9; i5 += 2) {
                int i6 = (int) (jArr[i5] >>> i4);
                c(jArr2, (i6 & 15) * 9, jArr2, (((i6 >>> 4) & 15) + 16) * 9, jArr3, i5);
            }
            if (i4 > 0) {
                kd2.I(18, jArr3, 0, 8, 0L);
            }
        }
    }

    public static void j(long[] jArr, long[] jArr2) {
        for (int i = 0; i < 9; i++) {
            bs1.c(jArr[i], jArr2, i << 1);
        }
    }

    public static void k(long[] jArr, long[] jArr2) {
        if (jd2.g(jArr)) {
            throw new IllegalStateException();
        }
        long[] b = jd2.b();
        long[] b2 = jd2.b();
        long[] b3 = jd2.b();
        t(jArr, b3);
        t(b3, b);
        t(b, b2);
        l(b, b2, b);
        v(b, 2, b2);
        l(b, b2, b);
        l(b, b3, b);
        v(b, 5, b2);
        l(b, b2, b);
        v(b2, 5, b2);
        l(b, b2, b);
        v(b, 15, b2);
        l(b, b2, b3);
        v(b3, 30, b);
        v(b, 30, b2);
        l(b, b2, b);
        v(b, 60, b2);
        l(b, b2, b);
        v(b2, 60, b2);
        l(b, b2, b);
        v(b, 180, b2);
        l(b, b2, b);
        v(b2, 180, b2);
        l(b, b2, b);
        l(b, b3, jArr2);
    }

    public static void l(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] c = jd2.c();
        h(jArr, jArr2, c);
        q(c, jArr3);
    }

    public static void m(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] c = jd2.c();
        h(jArr, jArr2, c);
        e(jArr3, c, jArr3);
    }

    public static void n(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] c = jd2.c();
        i(jArr, jArr2, c);
        q(c, jArr3);
    }

    public static void o(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] c = jd2.c();
        i(jArr, jArr2, c);
        e(jArr3, c, jArr3);
    }

    public static long[] p(long[] jArr) {
        long[] jArr2 = new long[288];
        int i = 0;
        System.arraycopy(jArr, 0, jArr2, 9, 9);
        int i2 = 7;
        while (i2 > 0) {
            int i3 = i + 18;
            kd2.F(9, jArr2, i3 >>> 1, 0L, jArr2, i3);
            r(jArr2, i3);
            a(jArr2, 9, jArr2, i3, jArr2, i3 + 9);
            i2--;
            i = i3;
        }
        kd2.J(144, jArr2, 0, 4, 0L, jArr2, 144);
        return jArr2;
    }

    public static void q(long[] jArr, long[] jArr2) {
        long j = jArr[9];
        long j2 = jArr[17];
        long j3 = (((j ^ (j2 >>> 59)) ^ (j2 >>> 57)) ^ (j2 >>> 54)) ^ (j2 >>> 49);
        long j4 = (j2 << 15) ^ (((jArr[8] ^ (j2 << 5)) ^ (j2 << 7)) ^ (j2 << 10));
        for (int i = 16; i >= 10; i--) {
            long j5 = jArr[i];
            jArr2[i - 8] = (((j4 ^ (j5 >>> 59)) ^ (j5 >>> 57)) ^ (j5 >>> 54)) ^ (j5 >>> 49);
            j4 = (((jArr[i - 9] ^ (j5 << 5)) ^ (j5 << 7)) ^ (j5 << 10)) ^ (j5 << 15);
        }
        jArr2[1] = (((j4 ^ (j3 >>> 59)) ^ (j3 >>> 57)) ^ (j3 >>> 54)) ^ (j3 >>> 49);
        long j6 = (j3 << 15) ^ (((jArr[0] ^ (j3 << 5)) ^ (j3 << 7)) ^ (j3 << 10));
        long j7 = jArr2[8];
        long j8 = j7 >>> 59;
        jArr2[0] = (((j6 ^ j8) ^ (j8 << 2)) ^ (j8 << 5)) ^ (j8 << 10);
        jArr2[8] = 576460752303423487L & j7;
    }

    public static void r(long[] jArr, int i) {
        int i2 = i + 8;
        long j = jArr[i2];
        long j2 = j >>> 59;
        jArr[i] = ((j2 << 10) ^ (((j2 << 2) ^ j2) ^ (j2 << 5))) ^ jArr[i];
        jArr[i2] = j & 576460752303423487L;
    }

    public static void s(long[] jArr, long[] jArr2) {
        long[] b = jd2.b();
        long[] b2 = jd2.b();
        int i = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            int i3 = i + 1;
            long e = bs1.e(jArr[i]);
            i = i3 + 1;
            long e2 = bs1.e(jArr[i3]);
            b[i2] = (4294967295L & e) | (e2 << 32);
            b2[i2] = (e >>> 32) | ((-4294967296L) & e2);
        }
        long e3 = bs1.e(jArr[i]);
        b[4] = 4294967295L & e3;
        b2[4] = e3 >>> 32;
        l(b2, a, jArr2);
        b(jArr2, b, jArr2);
    }

    public static void t(long[] jArr, long[] jArr2) {
        long[] c = jd2.c();
        j(jArr, c);
        q(c, jArr2);
    }

    public static void u(long[] jArr, long[] jArr2) {
        long[] c = jd2.c();
        j(jArr, c);
        e(jArr2, c, jArr2);
    }

    public static void v(long[] jArr, int i, long[] jArr2) {
        long[] c = jd2.c();
        j(jArr, c);
        while (true) {
            q(c, jArr2);
            i--;
            if (i <= 0) {
                return;
            }
            j(jArr2, c);
        }
    }

    public static int w(long[] jArr) {
        return ((int) ((jArr[0] ^ (jArr[8] >>> 49)) ^ (jArr[8] >>> 57))) & 1;
    }
}
