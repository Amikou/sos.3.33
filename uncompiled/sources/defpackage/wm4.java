package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: WalletFragmentDirections.java */
/* renamed from: wm4  reason: default package */
/* loaded from: classes2.dex */
public class wm4 {

    /* compiled from: WalletFragmentDirections.java */
    /* renamed from: wm4$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("isOnlyMoonPay")) {
                bundle.putBoolean("isOnlyMoonPay", ((Boolean) this.a.get("isOnlyMoonPay")).booleanValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_navigation_wallet_to_tokenListFragment;
        }

        public boolean c() {
            return ((Boolean) this.a.get("isOnlyMoonPay")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            return this.a.containsKey("isOnlyMoonPay") == bVar.a.containsKey("isOnlyMoonPay") && c() == bVar.c() && b() == bVar.b();
        }

        public int hashCode() {
            return (((c() ? 1 : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionNavigationWalletToTokenListFragment(actionId=" + b() + "){isOnlyMoonPay=" + c() + "}";
        }

        public b(boolean z) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            hashMap.put("isOnlyMoonPay", Boolean.valueOf(z));
        }
    }

    /* compiled from: WalletFragmentDirections.java */
    /* renamed from: wm4$c */
    /* loaded from: classes2.dex */
    public static class c implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("userTokenData")) {
                UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) this.a.get("userTokenData");
                if (!Parcelable.class.isAssignableFrom(UserTokenItemDisplayModel.class) && userTokenItemDisplayModel != null) {
                    if (Serializable.class.isAssignableFrom(UserTokenItemDisplayModel.class)) {
                        bundle.putSerializable("userTokenData", (Serializable) Serializable.class.cast(userTokenItemDisplayModel));
                    } else {
                        throw new UnsupportedOperationException(UserTokenItemDisplayModel.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("userTokenData", (Parcelable) Parcelable.class.cast(userTokenItemDisplayModel));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_navigation_wallet_to_transferHistoryFragment;
        }

        public UserTokenItemDisplayModel c() {
            return (UserTokenItemDisplayModel) this.a.get("userTokenData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.a.containsKey("userTokenData") != cVar.a.containsKey("userTokenData")) {
                return false;
            }
            if (c() == null ? cVar.c() == null : c().equals(cVar.c())) {
                return b() == cVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionNavigationWalletToTransferHistoryFragment(actionId=" + b() + "){userTokenData=" + c() + "}";
        }

        public c(UserTokenItemDisplayModel userTokenItemDisplayModel) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (userTokenItemDisplayModel != null) {
                hashMap.put("userTokenData", userTokenItemDisplayModel);
                return;
            }
            throw new IllegalArgumentException("Argument \"userTokenData\" is marked as non-null but was passed a null value.");
        }
    }

    /* compiled from: WalletFragmentDirections.java */
    /* renamed from: wm4$d */
    /* loaded from: classes2.dex */
    public static class d implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("transactionHash")) {
                bundle.putString("transactionHash", (String) this.a.get("transactionHash"));
            }
            if (this.a.containsKey("isNewTransaction")) {
                bundle.putBoolean("isNewTransaction", ((Boolean) this.a.get("isNewTransaction")).booleanValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_navigation_wallet_to_transferNotificationDetailsFragment;
        }

        public boolean c() {
            return ((Boolean) this.a.get("isNewTransaction")).booleanValue();
        }

        public String d() {
            return (String) this.a.get("transactionHash");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.a.containsKey("transactionHash") != dVar.a.containsKey("transactionHash")) {
                return false;
            }
            if (d() == null ? dVar.d() == null : d().equals(dVar.d())) {
                return this.a.containsKey("isNewTransaction") == dVar.a.containsKey("isNewTransaction") && c() == dVar.c() && b() == dVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((((d() != null ? d().hashCode() : 0) + 31) * 31) + (c() ? 1 : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionNavigationWalletToTransferNotificationDetailsFragment(actionId=" + b() + "){transactionHash=" + d() + ", isNewTransaction=" + c() + "}";
        }

        public d(String str, boolean z) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (str != null) {
                hashMap.put("transactionHash", str);
                hashMap.put("isNewTransaction", Boolean.valueOf(z));
                return;
            }
            throw new IllegalArgumentException("Argument \"transactionHash\" is marked as non-null but was passed a null value.");
        }
    }

    public static ce2 a() {
        return new l6(R.id.action_navigation_wallet_to_allTokensListFragment);
    }

    public static ce2 b() {
        return new l6(R.id.action_navigation_wallet_to_myTokensListFragment);
    }

    public static ce2 c() {
        return new l6(R.id.action_navigation_wallet_to_notificationHistoryFragment);
    }

    public static ce2 d() {
        return new l6(R.id.action_navigation_wallet_to_ReceiveFragment);
    }

    public static ce2 e() {
        return new l6(R.id.action_navigation_wallet_to_sendFragment);
    }

    public static ce2 f() {
        return e92.a();
    }

    public static b g(boolean z) {
        return new b(z);
    }

    public static c h(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        return new c(userTokenItemDisplayModel);
    }

    public static d i(String str, boolean z) {
        return new d(str, z);
    }

    public static ce2 j() {
        return e92.b();
    }
}
