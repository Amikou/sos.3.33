package defpackage;

import java.util.Arrays;

/* compiled from: Bytes.java */
/* renamed from: zs  reason: default package */
/* loaded from: classes3.dex */
public class zs {
    private zs() {
    }

    public static byte[] trimLeadingBytes(byte[] bArr, byte b) {
        int i = 0;
        while (i < bArr.length - 1 && bArr[i] == b) {
            i++;
        }
        return Arrays.copyOfRange(bArr, i, bArr.length);
    }

    public static byte[] trimLeadingZeroes(byte[] bArr) {
        return trimLeadingBytes(bArr, (byte) 0);
    }
}
