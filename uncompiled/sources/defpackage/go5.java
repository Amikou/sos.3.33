package defpackage;

import com.google.android.gms.internal.vision.h0;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: go5  reason: default package */
/* loaded from: classes.dex */
public final class go5 {
    public int a;
    public long b;
    public Object c;
    public final h0 d;

    public go5(h0 h0Var) {
        Objects.requireNonNull(h0Var);
        this.d = h0Var;
    }
}
