package defpackage;

import android.content.Context;

/* renamed from: oy4  reason: default package */
/* loaded from: classes2.dex */
public final class oy4 {
    static {
        new it4("SplitInstallInfoProvider");
    }

    public oy4(Context context) {
        context.getPackageName();
    }

    public static boolean a(String str) {
        return str.startsWith("config.");
    }

    public static boolean b(String str) {
        return a(str) || str.contains(".config.");
    }
}
