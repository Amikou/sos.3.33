package defpackage;

import android.content.Context;
import android.graphics.Typeface;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* compiled from: TypefaceProvider.java */
/* renamed from: ge4  reason: default package */
/* loaded from: classes2.dex */
public class ge4 {
    public static final Map<CharSequence, Typeface> a = new HashMap();
    public static final Map<CharSequence, fn1> b = new HashMap();

    public static Collection<fn1> a() {
        return b.values();
    }

    public static Typeface b(Context context, fn1 fn1Var) {
        String charSequence = fn1Var.b().toString();
        Map<CharSequence, Typeface> map = a;
        if (map.get(charSequence) == null) {
            map.put(charSequence, Typeface.createFromAsset(context.getAssets(), charSequence));
        }
        return map.get(charSequence);
    }

    public static fn1 c(String str, boolean z) {
        fn1 fn1Var = b.get(str);
        if (fn1Var != null || z) {
            return fn1Var;
        }
        throw new RuntimeException(String.format("Font '%s' not properly registered", str));
    }
}
