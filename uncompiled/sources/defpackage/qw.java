package defpackage;

import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import defpackage.kb0;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* compiled from: Cea708Decoder.java */
/* renamed from: qw  reason: default package */
/* loaded from: classes.dex */
public final class qw extends sw {
    public final op2 g = new op2();
    public final np2 h = new np2();
    public int i = -1;
    public final int j;
    public final b[] k;
    public b l;
    public List<kb0> m;
    public List<kb0> n;
    public c o;
    public int p;

    /* compiled from: Cea708Decoder.java */
    /* renamed from: qw$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final Comparator<a> c = pw.a;
        public final kb0 a;
        public final int b;

        public a(CharSequence charSequence, Layout.Alignment alignment, float f, int i, int i2, float f2, int i3, float f3, boolean z, int i4, int i5) {
            kb0.b n = new kb0.b().o(charSequence).p(alignment).h(f, i).i(i2).k(f2).l(i3).n(f3);
            if (z) {
                n.s(i4);
            }
            this.a = n.a();
            this.b = i5;
        }

        public static /* synthetic */ int c(a aVar, a aVar2) {
            return Integer.compare(aVar2.b, aVar.b);
        }
    }

    /* compiled from: Cea708Decoder.java */
    /* renamed from: qw$b */
    /* loaded from: classes.dex */
    public static final class b {
        public static final int[] A;
        public static final int[] B;
        public static final boolean[] C;
        public static final int[] D;
        public static final int[] E;
        public static final int[] F;
        public static final int[] G;
        public static final int w = h(2, 2, 2, 0);
        public static final int x;
        public static final int y;
        public static final int[] z;
        public final List<SpannableString> a = new ArrayList();
        public final SpannableStringBuilder b = new SpannableStringBuilder();
        public boolean c;
        public boolean d;
        public int e;
        public boolean f;
        public int g;
        public int h;
        public int i;
        public int j;
        public boolean k;
        public int l;
        public int m;
        public int n;
        public int o;
        public int p;
        public int q;
        public int r;
        public int s;
        public int t;
        public int u;
        public int v;

        static {
            int h = h(0, 0, 0, 0);
            x = h;
            int h2 = h(0, 0, 0, 3);
            y = h2;
            z = new int[]{0, 0, 0, 0, 0, 2, 0};
            A = new int[]{0, 0, 0, 0, 0, 0, 2};
            B = new int[]{3, 3, 3, 3, 3, 3, 1};
            C = new boolean[]{false, false, false, true, true, true, false};
            D = new int[]{h, h2, h, h, h2, h, h};
            E = new int[]{0, 1, 2, 3, 4, 3, 4};
            F = new int[]{0, 0, 0, 0, 0, 3, 3};
            G = new int[]{h, h, h, h, h, h2, h2};
        }

        public b() {
            l();
        }

        public static int g(int i, int i2, int i3) {
            return h(i, i2, i3, 0);
        }

        /* JADX WARN: Removed duplicated region for block: B:14:0x0024  */
        /* JADX WARN: Removed duplicated region for block: B:15:0x0026  */
        /* JADX WARN: Removed duplicated region for block: B:17:0x0029  */
        /* JADX WARN: Removed duplicated region for block: B:18:0x002b  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x002e  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public static int h(int r4, int r5, int r6, int r7) {
            /*
                r0 = 0
                r1 = 4
                defpackage.ii.c(r4, r0, r1)
                defpackage.ii.c(r5, r0, r1)
                defpackage.ii.c(r6, r0, r1)
                defpackage.ii.c(r7, r0, r1)
                r1 = 1
                r2 = 255(0xff, float:3.57E-43)
                if (r7 == 0) goto L21
                if (r7 == r1) goto L21
                r3 = 2
                if (r7 == r3) goto L1e
                r3 = 3
                if (r7 == r3) goto L1c
                goto L21
            L1c:
                r7 = r0
                goto L22
            L1e:
                r7 = 127(0x7f, float:1.78E-43)
                goto L22
            L21:
                r7 = r2
            L22:
                if (r4 <= r1) goto L26
                r4 = r2
                goto L27
            L26:
                r4 = r0
            L27:
                if (r5 <= r1) goto L2b
                r5 = r2
                goto L2c
            L2b:
                r5 = r0
            L2c:
                if (r6 <= r1) goto L2f
                r0 = r2
            L2f:
                int r4 = android.graphics.Color.argb(r7, r4, r5, r0)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.qw.b.h(int, int, int, int):int");
        }

        public void a(char c) {
            if (c == '\n') {
                this.a.add(d());
                this.b.clear();
                if (this.p != -1) {
                    this.p = 0;
                }
                if (this.q != -1) {
                    this.q = 0;
                }
                if (this.r != -1) {
                    this.r = 0;
                }
                if (this.t != -1) {
                    this.t = 0;
                }
                while (true) {
                    if ((!this.k || this.a.size() < this.j) && this.a.size() < 15) {
                        return;
                    }
                    this.a.remove(0);
                }
            } else {
                this.b.append(c);
            }
        }

        public void b() {
            int length = this.b.length();
            if (length > 0) {
                this.b.delete(length - 1, length);
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:23:0x0065  */
        /* JADX WARN: Removed duplicated region for block: B:24:0x0070  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x008f  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x0091  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x009c  */
        /* JADX WARN: Removed duplicated region for block: B:35:0x009e  */
        /* JADX WARN: Removed duplicated region for block: B:41:0x00aa  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public defpackage.qw.a c() {
            /*
                Method dump skipped, instructions count: 195
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.qw.b.c():qw$a");
        }

        public SpannableString d() {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.b);
            int length = spannableStringBuilder.length();
            if (length > 0) {
                if (this.p != -1) {
                    spannableStringBuilder.setSpan(new StyleSpan(2), this.p, length, 33);
                }
                if (this.q != -1) {
                    spannableStringBuilder.setSpan(new UnderlineSpan(), this.q, length, 33);
                }
                if (this.r != -1) {
                    spannableStringBuilder.setSpan(new ForegroundColorSpan(this.s), this.r, length, 33);
                }
                if (this.t != -1) {
                    spannableStringBuilder.setSpan(new BackgroundColorSpan(this.u), this.t, length, 33);
                }
            }
            return new SpannableString(spannableStringBuilder);
        }

        public void e() {
            this.a.clear();
            this.b.clear();
            this.p = -1;
            this.q = -1;
            this.r = -1;
            this.t = -1;
            this.v = 0;
        }

        public void f(boolean z2, boolean z3, boolean z4, int i, boolean z5, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            this.c = true;
            this.d = z2;
            this.k = z3;
            this.e = i;
            this.f = z5;
            this.g = i2;
            this.h = i3;
            this.i = i6;
            int i9 = i4 + 1;
            if (this.j != i9) {
                this.j = i9;
                while (true) {
                    if ((!z3 || this.a.size() < this.j) && this.a.size() < 15) {
                        break;
                    }
                    this.a.remove(0);
                }
            }
            if (i7 != 0 && this.m != i7) {
                this.m = i7;
                int i10 = i7 - 1;
                q(D[i10], y, C[i10], 0, A[i10], B[i10], z[i10]);
            }
            if (i8 == 0 || this.n == i8) {
                return;
            }
            this.n = i8;
            int i11 = i8 - 1;
            m(0, 1, 1, false, false, F[i11], E[i11]);
            n(w, G[i11], x);
        }

        public boolean i() {
            return this.c;
        }

        public boolean j() {
            return !i() || (this.a.isEmpty() && this.b.length() == 0);
        }

        public boolean k() {
            return this.d;
        }

        public void l() {
            e();
            this.c = false;
            this.d = false;
            this.e = 4;
            this.f = false;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.j = 15;
            this.k = true;
            this.l = 0;
            this.m = 0;
            this.n = 0;
            int i = x;
            this.o = i;
            this.s = w;
            this.u = i;
        }

        public void m(int i, int i2, int i3, boolean z2, boolean z3, int i4, int i5) {
            if (this.p != -1) {
                if (!z2) {
                    this.b.setSpan(new StyleSpan(2), this.p, this.b.length(), 33);
                    this.p = -1;
                }
            } else if (z2) {
                this.p = this.b.length();
            }
            if (this.q == -1) {
                if (z3) {
                    this.q = this.b.length();
                }
            } else if (z3) {
            } else {
                this.b.setSpan(new UnderlineSpan(), this.q, this.b.length(), 33);
                this.q = -1;
            }
        }

        public void n(int i, int i2, int i3) {
            if (this.r != -1 && this.s != i) {
                this.b.setSpan(new ForegroundColorSpan(this.s), this.r, this.b.length(), 33);
            }
            if (i != w) {
                this.r = this.b.length();
                this.s = i;
            }
            if (this.t != -1 && this.u != i2) {
                this.b.setSpan(new BackgroundColorSpan(this.u), this.t, this.b.length(), 33);
            }
            if (i2 != x) {
                this.t = this.b.length();
                this.u = i2;
            }
        }

        public void o(int i, int i2) {
            if (this.v != i) {
                a('\n');
            }
            this.v = i;
        }

        public void p(boolean z2) {
            this.d = z2;
        }

        public void q(int i, int i2, boolean z2, int i3, int i4, int i5, int i6) {
            this.o = i;
            this.l = i6;
        }
    }

    /* compiled from: Cea708Decoder.java */
    /* renamed from: qw$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final int a;
        public final int b;
        public final byte[] c;
        public int d = 0;

        public c(int i, int i2) {
            this.a = i;
            this.b = i2;
            this.c = new byte[(i2 * 2) - 1];
        }
    }

    public qw(int i, List<byte[]> list) {
        this.j = i == -1 ? 1 : i;
        if (list != null) {
            h00.f(list);
        }
        this.k = new b[8];
        for (int i2 = 0; i2 < 8; i2++) {
            this.k[i2] = new b();
        }
        this.l = this.k[0];
    }

    public final void A() {
        this.l.m(this.h.h(4), this.h.h(2), this.h.h(2), this.h.g(), this.h.g(), this.h.h(3), this.h.h(3));
    }

    public final void B() {
        int h = b.h(this.h.h(2), this.h.h(2), this.h.h(2), this.h.h(2));
        int h2 = b.h(this.h.h(2), this.h.h(2), this.h.h(2), this.h.h(2));
        this.h.r(2);
        this.l.n(h, h2, b.g(this.h.h(2), this.h.h(2), this.h.h(2)));
    }

    public final void C() {
        this.h.r(4);
        int h = this.h.h(4);
        this.h.r(2);
        this.l.o(h, this.h.h(6));
    }

    public final void D() {
        int h = b.h(this.h.h(2), this.h.h(2), this.h.h(2), this.h.h(2));
        int h2 = this.h.h(2);
        int g = b.g(this.h.h(2), this.h.h(2), this.h.h(2));
        if (this.h.g()) {
            h2 |= 4;
        }
        boolean g2 = this.h.g();
        int h3 = this.h.h(2);
        int h4 = this.h.h(2);
        int h5 = this.h.h(2);
        this.h.r(8);
        this.l.q(h, g, g2, h2, h3, h4, h5);
    }

    public final void E() {
        c cVar = this.o;
        if (cVar.d != (cVar.b * 2) - 1) {
            p12.b("Cea708Decoder", "DtvCcPacket ended prematurely; size is " + ((this.o.b * 2) - 1) + ", but current index is " + this.o.d + " (sequence number " + this.o.a + ");");
        }
        boolean z = false;
        np2 np2Var = this.h;
        c cVar2 = this.o;
        np2Var.o(cVar2.c, cVar2.d);
        while (true) {
            if (this.h.b() <= 0) {
                break;
            }
            int h = this.h.h(3);
            int h2 = this.h.h(5);
            if (h == 7) {
                this.h.r(2);
                h = this.h.h(6);
                if (h < 7) {
                    p12.i("Cea708Decoder", "Invalid extended service number: " + h);
                }
            }
            if (h2 == 0) {
                if (h != 0) {
                    p12.i("Cea708Decoder", "serviceNumber is non-zero (" + h + ") when blockSize is 0");
                }
            } else if (h != this.j) {
                this.h.s(h2);
            } else {
                int e = this.h.e() + (h2 * 8);
                while (this.h.e() < e) {
                    int h3 = this.h.h(8);
                    if (h3 == 16) {
                        int h4 = this.h.h(8);
                        if (h4 <= 31) {
                            t(h4);
                        } else {
                            if (h4 <= 127) {
                                y(h4);
                            } else if (h4 <= 159) {
                                u(h4);
                            } else if (h4 <= 255) {
                                z(h4);
                            } else {
                                p12.i("Cea708Decoder", "Invalid extended command: " + h4);
                            }
                            z = true;
                        }
                    } else if (h3 <= 31) {
                        r(h3);
                    } else {
                        if (h3 <= 127) {
                            w(h3);
                        } else if (h3 <= 159) {
                            s(h3);
                        } else if (h3 <= 255) {
                            x(h3);
                        } else {
                            p12.i("Cea708Decoder", "Invalid base command: " + h3);
                        }
                        z = true;
                    }
                }
            }
        }
        if (z) {
            this.m = q();
        }
    }

    public final void F() {
        for (int i = 0; i < 8; i++) {
            this.k[i].l();
        }
    }

    @Override // defpackage.sw
    public qv3 f() {
        List<kb0> list = this.m;
        this.n = list;
        return new tw((List) ii.e(list));
    }

    @Override // defpackage.sw, androidx.media3.decoder.a
    public void flush() {
        super.flush();
        this.m = null;
        this.n = null;
        this.p = 0;
        this.l = this.k[0];
        F();
        this.o = null;
    }

    @Override // defpackage.sw
    public void g(tv3 tv3Var) {
        ByteBuffer byteBuffer = (ByteBuffer) ii.e(tv3Var.g0);
        this.g.N(byteBuffer.array(), byteBuffer.limit());
        while (this.g.a() >= 3) {
            int D = this.g.D() & 7;
            int i = D & 3;
            boolean z = (D & 4) == 4;
            byte D2 = (byte) this.g.D();
            byte D3 = (byte) this.g.D();
            if (i == 2 || i == 3) {
                if (z) {
                    if (i == 3) {
                        p();
                        int i2 = (D2 & 192) >> 6;
                        int i3 = this.i;
                        if (i3 != -1 && i2 != (i3 + 1) % 4) {
                            F();
                            p12.i("Cea708Decoder", "Sequence number discontinuity. previous=" + this.i + " current=" + i2);
                        }
                        this.i = i2;
                        int i4 = D2 & 63;
                        if (i4 == 0) {
                            i4 = 64;
                        }
                        c cVar = new c(i2, i4);
                        this.o = cVar;
                        byte[] bArr = cVar.c;
                        int i5 = cVar.d;
                        cVar.d = i5 + 1;
                        bArr[i5] = D3;
                    } else {
                        ii.a(i == 2);
                        c cVar2 = this.o;
                        if (cVar2 == null) {
                            p12.c("Cea708Decoder", "Encountered DTVCC_PACKET_DATA before DTVCC_PACKET_START");
                        } else {
                            byte[] bArr2 = cVar2.c;
                            int i6 = cVar2.d;
                            int i7 = i6 + 1;
                            cVar2.d = i7;
                            bArr2[i6] = D2;
                            cVar2.d = i7 + 1;
                            bArr2[i7] = D3;
                        }
                    }
                    c cVar3 = this.o;
                    if (cVar3.d == (cVar3.b * 2) - 1) {
                        p();
                    }
                }
            }
        }
    }

    @Override // defpackage.sw
    public boolean l() {
        return this.m != this.n;
    }

    public final void p() {
        if (this.o == null) {
            return;
        }
        E();
        this.o = null;
    }

    public final List<kb0> q() {
        a c2;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 8; i++) {
            if (!this.k[i].j() && this.k[i].k() && (c2 = this.k[i].c()) != null) {
                arrayList.add(c2);
            }
        }
        Collections.sort(arrayList, a.c);
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            arrayList2.add(((a) arrayList.get(i2)).a);
        }
        return Collections.unmodifiableList(arrayList2);
    }

    public final void r(int i) {
        if (i != 0) {
            if (i == 3) {
                this.m = q();
            } else if (i != 8) {
                switch (i) {
                    case 12:
                        F();
                        return;
                    case 13:
                        this.l.a('\n');
                        return;
                    case 14:
                        return;
                    default:
                        if (i >= 17 && i <= 23) {
                            p12.i("Cea708Decoder", "Currently unsupported COMMAND_EXT1 Command: " + i);
                            this.h.r(8);
                            return;
                        } else if (i >= 24 && i <= 31) {
                            p12.i("Cea708Decoder", "Currently unsupported COMMAND_P16 Command: " + i);
                            this.h.r(16);
                            return;
                        } else {
                            p12.i("Cea708Decoder", "Invalid C0 command: " + i);
                            return;
                        }
                }
            } else {
                this.l.b();
            }
        }
    }

    public final void s(int i) {
        b bVar;
        int i2 = 1;
        switch (i) {
            case 128:
            case 129:
            case 130:
            case 131:
            case 132:
            case 133:
            case 134:
            case 135:
                int i3 = i - 128;
                if (this.p != i3) {
                    this.p = i3;
                    this.l = this.k[i3];
                    return;
                }
                return;
            case 136:
                while (i2 <= 8) {
                    if (this.h.g()) {
                        this.k[8 - i2].e();
                    }
                    i2++;
                }
                return;
            case 137:
                for (int i4 = 1; i4 <= 8; i4++) {
                    if (this.h.g()) {
                        this.k[8 - i4].p(true);
                    }
                }
                return;
            case 138:
                while (i2 <= 8) {
                    if (this.h.g()) {
                        this.k[8 - i2].p(false);
                    }
                    i2++;
                }
                return;
            case 139:
                for (int i5 = 1; i5 <= 8; i5++) {
                    if (this.h.g()) {
                        this.k[8 - i5].p(!bVar.k());
                    }
                }
                return;
            case 140:
                while (i2 <= 8) {
                    if (this.h.g()) {
                        this.k[8 - i2].l();
                    }
                    i2++;
                }
                return;
            case 141:
                this.h.r(8);
                return;
            case 142:
                return;
            case 143:
                F();
                return;
            case 144:
                if (!this.l.i()) {
                    this.h.r(16);
                    return;
                } else {
                    A();
                    return;
                }
            case 145:
                if (!this.l.i()) {
                    this.h.r(24);
                    return;
                } else {
                    B();
                    return;
                }
            case 146:
                if (!this.l.i()) {
                    this.h.r(16);
                    return;
                } else {
                    C();
                    return;
                }
            case 147:
            case 148:
            case 149:
            case 150:
            default:
                p12.i("Cea708Decoder", "Invalid C1 command: " + i);
                return;
            case 151:
                if (!this.l.i()) {
                    this.h.r(32);
                    return;
                } else {
                    D();
                    return;
                }
            case 152:
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 159:
                int i6 = i - 152;
                v(i6);
                if (this.p != i6) {
                    this.p = i6;
                    this.l = this.k[i6];
                    return;
                }
                return;
        }
    }

    public final void t(int i) {
        if (i <= 7) {
            return;
        }
        if (i <= 15) {
            this.h.r(8);
        } else if (i <= 23) {
            this.h.r(16);
        } else if (i <= 31) {
            this.h.r(24);
        }
    }

    public final void u(int i) {
        if (i <= 135) {
            this.h.r(32);
        } else if (i <= 143) {
            this.h.r(40);
        } else if (i <= 159) {
            this.h.r(2);
            this.h.r(this.h.h(6) * 8);
        }
    }

    public final void v(int i) {
        b bVar = this.k[i];
        this.h.r(2);
        boolean g = this.h.g();
        boolean g2 = this.h.g();
        boolean g3 = this.h.g();
        int h = this.h.h(3);
        boolean g4 = this.h.g();
        int h2 = this.h.h(7);
        int h3 = this.h.h(8);
        int h4 = this.h.h(4);
        int h5 = this.h.h(4);
        this.h.r(2);
        int h6 = this.h.h(6);
        this.h.r(2);
        bVar.f(g, g2, g3, h, g4, h2, h3, h5, h6, h4, this.h.h(3), this.h.h(3));
    }

    public final void w(int i) {
        if (i == 127) {
            this.l.a((char) 9835);
        } else {
            this.l.a((char) (i & 255));
        }
    }

    public final void x(int i) {
        this.l.a((char) (i & 255));
    }

    public final void y(int i) {
        if (i == 32) {
            this.l.a(' ');
        } else if (i == 33) {
            this.l.a((char) 160);
        } else if (i == 37) {
            this.l.a((char) 8230);
        } else if (i == 42) {
            this.l.a((char) 352);
        } else if (i == 44) {
            this.l.a((char) 338);
        } else if (i == 63) {
            this.l.a((char) 376);
        } else if (i == 57) {
            this.l.a((char) 8482);
        } else if (i == 58) {
            this.l.a((char) 353);
        } else if (i == 60) {
            this.l.a((char) 339);
        } else if (i != 61) {
            switch (i) {
                case 48:
                    this.l.a((char) 9608);
                    return;
                case 49:
                    this.l.a((char) 8216);
                    return;
                case 50:
                    this.l.a((char) 8217);
                    return;
                case 51:
                    this.l.a((char) 8220);
                    return;
                case 52:
                    this.l.a((char) 8221);
                    return;
                case 53:
                    this.l.a((char) 8226);
                    return;
                default:
                    switch (i) {
                        case 118:
                            this.l.a((char) 8539);
                            return;
                        case 119:
                            this.l.a((char) 8540);
                            return;
                        case 120:
                            this.l.a((char) 8541);
                            return;
                        case 121:
                            this.l.a((char) 8542);
                            return;
                        case 122:
                            this.l.a((char) 9474);
                            return;
                        case 123:
                            this.l.a((char) 9488);
                            return;
                        case 124:
                            this.l.a((char) 9492);
                            return;
                        case 125:
                            this.l.a((char) 9472);
                            return;
                        case 126:
                            this.l.a((char) 9496);
                            return;
                        case 127:
                            this.l.a((char) 9484);
                            return;
                        default:
                            p12.i("Cea708Decoder", "Invalid G2 character: " + i);
                            return;
                    }
            }
        } else {
            this.l.a((char) 8480);
        }
    }

    public final void z(int i) {
        if (i == 160) {
            this.l.a((char) 13252);
            return;
        }
        p12.i("Cea708Decoder", "Invalid G3 character: " + i);
        this.l.a('_');
    }
}
