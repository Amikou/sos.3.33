package defpackage;

import android.util.Pair;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.extractor.metadata.mp4.MotionPhotoMetadata;
import defpackage.wi3;
import defpackage.zi;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: Mp4Extractor.java */
/* renamed from: ha2  reason: default package */
/* loaded from: classes.dex */
public final class ha2 implements p11, wi3 {
    public final int a;
    public final op2 b;
    public final op2 c;
    public final op2 d;
    public final op2 e;
    public final ArrayDeque<zi.a> f;
    public final aj3 g;
    public final List<Metadata.Entry> h;
    public int i;
    public int j;
    public long k;
    public int l;
    public op2 m;
    public int n;
    public int o;
    public int p;
    public int q;
    public r11 r;
    public a[] s;
    public long[][] t;
    public int u;
    public long v;
    public int w;
    public MotionPhotoMetadata x;

    /* compiled from: Mp4Extractor.java */
    /* renamed from: ha2$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final w74 a;
        public final g84 b;
        public final f84 c;
        public final ac4 d;
        public int e;

        public a(w74 w74Var, g84 g84Var, f84 f84Var) {
            this.a = w74Var;
            this.b = g84Var;
            this.c = f84Var;
            this.d = "audio/true-hd".equals(w74Var.f.p0) ? new ac4() : null;
        }
    }

    static {
        fa2 fa2Var = fa2.b;
    }

    public ha2() {
        this(0);
    }

    public static boolean F(int i) {
        return i == 1836019574 || i == 1953653099 || i == 1835297121 || i == 1835626086 || i == 1937007212 || i == 1701082227 || i == 1835365473;
    }

    public static boolean G(int i) {
        return i == 1835296868 || i == 1836476516 || i == 1751411826 || i == 1937011556 || i == 1937011827 || i == 1937011571 || i == 1668576371 || i == 1701606260 || i == 1937011555 || i == 1937011578 || i == 1937013298 || i == 1937007471 || i == 1668232756 || i == 1953196132 || i == 1718909296 || i == 1969517665 || i == 1801812339 || i == 1768715124;
    }

    public static int m(int i) {
        if (i != 1751476579) {
            return i != 1903435808 ? 0 : 1;
        }
        return 2;
    }

    public static long[][] n(a[] aVarArr) {
        long[][] jArr = new long[aVarArr.length];
        int[] iArr = new int[aVarArr.length];
        long[] jArr2 = new long[aVarArr.length];
        boolean[] zArr = new boolean[aVarArr.length];
        for (int i = 0; i < aVarArr.length; i++) {
            jArr[i] = new long[aVarArr[i].b.b];
            jArr2[i] = aVarArr[i].b.f[0];
        }
        long j = 0;
        int i2 = 0;
        while (i2 < aVarArr.length) {
            long j2 = Long.MAX_VALUE;
            int i3 = -1;
            for (int i4 = 0; i4 < aVarArr.length; i4++) {
                if (!zArr[i4] && jArr2[i4] <= j2) {
                    j2 = jArr2[i4];
                    i3 = i4;
                }
            }
            int i5 = iArr[i3];
            jArr[i3][i5] = j;
            j += aVarArr[i3].b.d[i5];
            int i6 = i5 + 1;
            iArr[i3] = i6;
            if (i6 < jArr[i3].length) {
                jArr2[i3] = aVarArr[i3].b.f[i6];
            } else {
                zArr[i3] = true;
                i2++;
            }
        }
        return jArr;
    }

    public static int q(g84 g84Var, long j) {
        int a2 = g84Var.a(j);
        return a2 == -1 ? g84Var.b(j) : a2;
    }

    public static /* synthetic */ w74 s(w74 w74Var) {
        return w74Var;
    }

    public static /* synthetic */ p11[] t() {
        return new p11[]{new ha2()};
    }

    public static long u(g84 g84Var, long j, long j2) {
        int q = q(g84Var, j);
        return q == -1 ? j2 : Math.min(g84Var.c[q], j2);
    }

    public static int y(op2 op2Var) {
        op2Var.P(8);
        int m = m(op2Var.n());
        if (m != 0) {
            return m;
        }
        op2Var.Q(4);
        while (op2Var.a() > 0) {
            int m2 = m(op2Var.n());
            if (m2 != 0) {
                return m2;
            }
        }
        return 0;
    }

    public final void A(long j) {
        if (this.j == 1836086884) {
            int i = this.l;
            this.x = new MotionPhotoMetadata(0L, j, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, j + i, this.k - i);
        }
    }

    public final boolean B(q11 q11Var) throws IOException {
        zi.a peek;
        if (this.l == 0) {
            if (!q11Var.a(this.e.d(), 0, 8, true)) {
                x();
                return false;
            }
            this.l = 8;
            this.e.P(0);
            this.k = this.e.F();
            this.j = this.e.n();
        }
        long j = this.k;
        if (j == 1) {
            q11Var.readFully(this.e.d(), 8, 8);
            this.l += 8;
            this.k = this.e.I();
        } else if (j == 0) {
            long length = q11Var.getLength();
            if (length == -1 && (peek = this.f.peek()) != null) {
                length = peek.b;
            }
            if (length != -1) {
                this.k = (length - q11Var.getPosition()) + this.l;
            }
        }
        if (this.k >= this.l) {
            if (F(this.j)) {
                long position = q11Var.getPosition();
                long j2 = this.k;
                int i = this.l;
                long j3 = (position + j2) - i;
                if (j2 != i && this.j == 1835365473) {
                    v(q11Var);
                }
                this.f.push(new zi.a(this.j, j3));
                if (this.k == this.l) {
                    w(j3);
                } else {
                    o();
                }
            } else if (G(this.j)) {
                ii.g(this.l == 8);
                ii.g(this.k <= 2147483647L);
                op2 op2Var = new op2((int) this.k);
                System.arraycopy(this.e.d(), 0, op2Var.d(), 0, 8);
                this.m = op2Var;
                this.i = 1;
            } else {
                A(q11Var.getPosition() - this.l);
                this.m = null;
                this.i = 1;
            }
            return true;
        }
        throw ParserException.createForUnsupportedContainerFeature("Atom size less than header length (unsupported).");
    }

    public final boolean C(q11 q11Var, ot2 ot2Var) throws IOException {
        boolean z;
        long j = this.k - this.l;
        long position = q11Var.getPosition() + j;
        op2 op2Var = this.m;
        if (op2Var != null) {
            q11Var.readFully(op2Var.d(), this.l, (int) j);
            if (this.j == 1718909296) {
                this.w = y(op2Var);
            } else if (!this.f.isEmpty()) {
                this.f.peek().e(new zi.b(this.j, op2Var));
            }
        } else if (j < 262144) {
            q11Var.k((int) j);
        } else {
            ot2Var.a = q11Var.getPosition() + j;
            z = true;
            w(position);
            return (z || this.i == 2) ? false : true;
        }
        z = false;
        w(position);
        if (z) {
        }
    }

    public final int D(q11 q11Var, ot2 ot2Var) throws IOException {
        int i;
        ot2 ot2Var2;
        long position = q11Var.getPosition();
        if (this.n == -1) {
            int r = r(position);
            this.n = r;
            if (r == -1) {
                return -1;
            }
        }
        a aVar = this.s[this.n];
        f84 f84Var = aVar.c;
        int i2 = aVar.e;
        g84 g84Var = aVar.b;
        long j = g84Var.c[i2];
        int i3 = g84Var.d[i2];
        ac4 ac4Var = aVar.d;
        long j2 = (j - position) + this.o;
        if (j2 < 0) {
            i = 1;
            ot2Var2 = ot2Var;
        } else if (j2 < 262144) {
            if (aVar.a.g == 1) {
                j2 += 8;
                i3 -= 8;
            }
            q11Var.k((int) j2);
            w74 w74Var = aVar.a;
            if (w74Var.j != 0) {
                byte[] d = this.c.d();
                d[0] = 0;
                d[1] = 0;
                d[2] = 0;
                int i4 = aVar.a.j;
                int i5 = 4 - i4;
                while (this.p < i3) {
                    int i6 = this.q;
                    if (i6 == 0) {
                        q11Var.readFully(d, i5, i4);
                        this.o += i4;
                        this.c.P(0);
                        int n = this.c.n();
                        if (n >= 0) {
                            this.q = n;
                            this.b.P(0);
                            f84Var.a(this.b, 4);
                            this.p += 4;
                            i3 += i5;
                        } else {
                            throw ParserException.createForMalformedContainer("Invalid NAL length", null);
                        }
                    } else {
                        int d2 = f84Var.d(q11Var, i6, false);
                        this.o += d2;
                        this.p += d2;
                        this.q -= d2;
                    }
                }
            } else {
                if ("audio/ac4".equals(w74Var.f.p0)) {
                    if (this.p == 0) {
                        x5.a(i3, this.d);
                        f84Var.a(this.d, 7);
                        this.p += 7;
                    }
                    i3 += 7;
                } else if (ac4Var != null) {
                    ac4Var.d(q11Var);
                }
                while (true) {
                    int i7 = this.p;
                    if (i7 >= i3) {
                        break;
                    }
                    int d3 = f84Var.d(q11Var, i3 - i7, false);
                    this.o += d3;
                    this.p += d3;
                    this.q -= d3;
                }
            }
            int i8 = i3;
            g84 g84Var2 = aVar.b;
            long j3 = g84Var2.f[i2];
            int i9 = g84Var2.g[i2];
            if (ac4Var != null) {
                ac4Var.c(f84Var, j3, i9, i8, 0, null);
                if (i2 + 1 == aVar.b.b) {
                    ac4Var.a(f84Var, null);
                }
            } else {
                f84Var.b(j3, i9, i8, 0, null);
            }
            aVar.e++;
            this.n = -1;
            this.o = 0;
            this.p = 0;
            this.q = 0;
            return 0;
        } else {
            ot2Var2 = ot2Var;
            i = 1;
        }
        ot2Var2.a = j;
        return i;
    }

    public final int E(q11 q11Var, ot2 ot2Var) throws IOException {
        int c = this.g.c(q11Var, ot2Var, this.h);
        if (c == 1 && ot2Var.a == 0) {
            o();
        }
        return c;
    }

    public final void H(a aVar, long j) {
        g84 g84Var = aVar.b;
        int a2 = g84Var.a(j);
        if (a2 == -1) {
            a2 = g84Var.b(j);
        }
        aVar.e = a2;
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        a[] aVarArr;
        this.f.clear();
        this.l = 0;
        this.n = -1;
        this.o = 0;
        this.p = 0;
        this.q = 0;
        if (j == 0) {
            if (this.i != 3) {
                o();
                return;
            }
            this.g.g();
            this.h.clear();
            return;
        }
        for (a aVar : this.s) {
            H(aVar, j2);
            ac4 ac4Var = aVar.d;
            if (ac4Var != null) {
                ac4Var.b();
            }
        }
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        while (true) {
            int i = this.i;
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            return E(q11Var, ot2Var);
                        }
                        throw new IllegalStateException();
                    }
                    return D(q11Var, ot2Var);
                } else if (C(q11Var, ot2Var)) {
                    return 1;
                }
            } else if (!B(q11Var)) {
                return -1;
            }
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        return xq3.d(q11Var, (this.a & 2) != 0);
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        return p(j, -1);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.v;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.r = r11Var;
    }

    public final void o() {
        this.i = 0;
        this.l = 0;
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0062  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x0088  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x008e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.wi3.a p(long r17, int r19) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r19
            ha2$a[] r4 = r0.s
            int r5 = r4.length
            if (r5 != 0) goto L13
            wi3$a r1 = new wi3$a
            yi3 r2 = defpackage.yi3.c
            r1.<init>(r2)
            return r1
        L13:
            r5 = -1
            r7 = -1
            if (r3 == r7) goto L1a
            r8 = r3
            goto L1c
        L1a:
            int r8 = r0.u
        L1c:
            r9 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r8 == r7) goto L58
            r4 = r4[r8]
            g84 r4 = r4.b
            int r8 = q(r4, r1)
            if (r8 != r7) goto L35
            wi3$a r1 = new wi3$a
            yi3 r2 = defpackage.yi3.c
            r1.<init>(r2)
            return r1
        L35:
            long[] r11 = r4.f
            r12 = r11[r8]
            long[] r11 = r4.c
            r14 = r11[r8]
            int r11 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r11 >= 0) goto L5e
            int r11 = r4.b
            int r11 = r11 + (-1)
            if (r8 >= r11) goto L5e
            int r1 = r4.b(r1)
            if (r1 == r7) goto L5e
            if (r1 == r8) goto L5e
            long[] r2 = r4.f
            r5 = r2[r1]
            long[] r2 = r4.c
            r1 = r2[r1]
            goto L60
        L58:
            r14 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r12 = r1
        L5e:
            r1 = r5
            r5 = r9
        L60:
            if (r3 != r7) goto L7f
            r3 = 0
        L63:
            ha2$a[] r4 = r0.s
            int r7 = r4.length
            if (r3 >= r7) goto L7f
            int r7 = r0.u
            if (r3 == r7) goto L7c
            r4 = r4[r3]
            g84 r4 = r4.b
            long r14 = u(r4, r12, r14)
            int r7 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r7 == 0) goto L7c
            long r1 = u(r4, r5, r1)
        L7c:
            int r3 = r3 + 1
            goto L63
        L7f:
            yi3 r3 = new yi3
            r3.<init>(r12, r14)
            int r4 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r4 != 0) goto L8e
            wi3$a r1 = new wi3$a
            r1.<init>(r3)
            return r1
        L8e:
            yi3 r4 = new yi3
            r4.<init>(r5, r1)
            wi3$a r1 = new wi3$a
            r1.<init>(r3, r4)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ha2.p(long, int):wi3$a");
    }

    public final int r(long j) {
        int i = -1;
        int i2 = -1;
        int i3 = 0;
        long j2 = Long.MAX_VALUE;
        boolean z = true;
        long j3 = Long.MAX_VALUE;
        boolean z2 = true;
        long j4 = Long.MAX_VALUE;
        while (true) {
            a[] aVarArr = this.s;
            if (i3 >= aVarArr.length) {
                break;
            }
            a aVar = aVarArr[i3];
            int i4 = aVar.e;
            g84 g84Var = aVar.b;
            if (i4 != g84Var.b) {
                long j5 = g84Var.c[i4];
                long j6 = ((long[][]) b.j(this.t))[i3][i4];
                long j7 = j5 - j;
                boolean z3 = j7 < 0 || j7 >= 262144;
                if ((!z3 && z2) || (z3 == z2 && j7 < j4)) {
                    z2 = z3;
                    j4 = j7;
                    i2 = i3;
                    j3 = j6;
                }
                if (j6 < j2) {
                    z = z3;
                    i = i3;
                    j2 = j6;
                }
            }
            i3++;
        }
        return (j2 == Long.MAX_VALUE || !z || j3 < j2 + 10485760) ? i2 : i;
    }

    public final void v(q11 q11Var) throws IOException {
        this.d.L(8);
        q11Var.n(this.d.d(), 0, 8);
        aj.e(this.d);
        q11Var.k(this.d.e());
        q11Var.j();
    }

    public final void w(long j) throws ParserException {
        while (!this.f.isEmpty() && this.f.peek().b == j) {
            zi.a pop = this.f.pop();
            if (pop.a == 1836019574) {
                z(pop);
                this.f.clear();
                this.i = 2;
            } else if (!this.f.isEmpty()) {
                this.f.peek().d(pop);
            }
        }
        if (this.i != 2) {
            o();
        }
    }

    public final void x() {
        if (this.w != 2 || (this.a & 2) == 0) {
            return;
        }
        this.r.f(0, 4).f(new j.b().X(this.x == null ? null : new Metadata(this.x)).E());
        this.r.m();
        this.r.p(new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
    }

    public final void z(zi.a aVar) throws ParserException {
        Metadata metadata;
        Metadata metadata2;
        List<g84> list;
        int i;
        int i2;
        int i3;
        ArrayList arrayList = new ArrayList();
        boolean z = this.w == 1;
        qe1 qe1Var = new qe1();
        zi.b g = aVar.g(1969517665);
        if (g != null) {
            Pair<Metadata, Metadata> B = aj.B(g);
            Metadata metadata3 = (Metadata) B.first;
            Metadata metadata4 = (Metadata) B.second;
            if (metadata3 != null) {
                qe1Var.c(metadata3);
            }
            metadata = metadata4;
            metadata2 = metadata3;
        } else {
            metadata = null;
            metadata2 = null;
        }
        zi.a f = aVar.f(1835365473);
        Metadata n = f != null ? aj.n(f) : null;
        boolean z2 = (this.a & 1) != 0;
        long j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        Metadata metadata5 = n;
        List<g84> A = aj.A(aVar, qe1Var, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, null, z2, z, ga2.a);
        int size = A.size();
        long j2 = -9223372036854775807L;
        int i4 = 0;
        int i5 = -1;
        while (i4 < size) {
            g84 g84Var = A.get(i4);
            if (g84Var.b == 0) {
                list = A;
                i = size;
            } else {
                w74 w74Var = g84Var.a;
                list = A;
                i = size;
                long j3 = w74Var.e;
                if (j3 == j) {
                    j3 = g84Var.h;
                }
                long max = Math.max(j2, j3);
                a aVar2 = new a(w74Var, g84Var, this.r.f(i4, w74Var.b));
                if ("audio/true-hd".equals(w74Var.f.p0)) {
                    i2 = g84Var.e * 16;
                } else {
                    i2 = g84Var.e + 30;
                }
                j.b b = w74Var.f.b();
                b.W(i2);
                if (w74Var.b == 2 && j3 > 0 && (i3 = g84Var.b) > 1) {
                    b.P(i3 / (((float) j3) / 1000000.0f));
                }
                q82.k(w74Var.b, qe1Var, b);
                int i6 = w74Var.b;
                Metadata[] metadataArr = new Metadata[2];
                metadataArr[0] = metadata;
                metadataArr[1] = this.h.isEmpty() ? null : new Metadata(this.h);
                q82.l(i6, metadata2, metadata5, b, metadataArr);
                aVar2.c.f(b.E());
                if (w74Var.b == 2 && i5 == -1) {
                    i5 = arrayList.size();
                }
                arrayList.add(aVar2);
                j2 = max;
            }
            i4++;
            A = list;
            size = i;
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        this.u = i5;
        this.v = j2;
        a[] aVarArr = (a[]) arrayList.toArray(new a[0]);
        this.s = aVarArr;
        this.t = n(aVarArr);
        this.r.m();
        this.r.p(this);
    }

    public ha2(int i) {
        this.a = i;
        this.i = (i & 4) != 0 ? 3 : 0;
        this.g = new aj3();
        this.h = new ArrayList();
        this.e = new op2(16);
        this.f = new ArrayDeque<>();
        this.b = new op2(wc2.a);
        this.c = new op2(4);
        this.d = new op2();
        this.n = -1;
        this.r = r11.e;
        this.s = new a[0];
    }
}
