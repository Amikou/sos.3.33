package defpackage;

import java.util.HashMap;
import java.util.Map;

/* compiled from: DelayedWorkTracker.java */
/* renamed from: am0  reason: default package */
/* loaded from: classes.dex */
public class am0 {
    public static final String d = v12.f("DelayedWorkTracker");
    public final ti1 a;
    public final ba3 b;
    public final Map<String, Runnable> c = new HashMap();

    /* compiled from: DelayedWorkTracker.java */
    /* renamed from: am0$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ tq4 a;

        public a(tq4 tq4Var) {
            this.a = tq4Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            v12.c().a(am0.d, String.format("Scheduling work %s", this.a.a), new Throwable[0]);
            am0.this.a.e(this.a);
        }
    }

    public am0(ti1 ti1Var, ba3 ba3Var) {
        this.a = ti1Var;
        this.b = ba3Var;
    }

    public void a(tq4 tq4Var) {
        Runnable remove = this.c.remove(tq4Var.a);
        if (remove != null) {
            this.b.b(remove);
        }
        a aVar = new a(tq4Var);
        this.c.put(tq4Var.a, aVar);
        long currentTimeMillis = System.currentTimeMillis();
        this.b.a(tq4Var.a() - currentTimeMillis, aVar);
    }

    public void b(String str) {
        Runnable remove = this.c.remove(str);
        if (remove != null) {
            this.b.b(remove);
        }
    }
}
