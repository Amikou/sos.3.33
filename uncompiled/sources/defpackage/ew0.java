package defpackage;

import android.content.Context;

/* compiled from: ErrorUtils.java */
/* renamed from: ew0  reason: default package */
/* loaded from: classes.dex */
public class ew0 {
    public static String a(Context context, int i) {
        if (context == null) {
            return "";
        }
        if (i != 1) {
            if (i != 7) {
                switch (i) {
                    case 9:
                        break;
                    case 10:
                        return context.getString(l13.fingerprint_error_user_canceled);
                    case 11:
                        return context.getString(l13.fingerprint_error_no_fingerprints);
                    case 12:
                        return context.getString(l13.fingerprint_error_hw_not_present);
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unknown error code: ");
                        sb.append(i);
                        return context.getString(l13.default_error_msg);
                }
            }
            return context.getString(l13.fingerprint_error_lockout);
        }
        return context.getString(l13.fingerprint_error_hw_not_available);
    }

    public static boolean b(int i) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return true;
            case 6:
            default:
                return false;
        }
    }

    public static boolean c(int i) {
        return i == 7 || i == 9;
    }
}
