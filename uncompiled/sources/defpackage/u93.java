package defpackage;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* compiled from: RoundedColorDrawable.java */
/* renamed from: u93  reason: default package */
/* loaded from: classes.dex */
public class u93 extends Drawable implements s93 {
    public float[] g0;
    public final float[] a = new float[8];
    public final float[] f0 = new float[8];
    public final Paint h0 = new Paint(1);
    public boolean i0 = false;
    public float j0 = Utils.FLOAT_EPSILON;
    public float k0 = Utils.FLOAT_EPSILON;
    public int l0 = 0;
    public boolean m0 = false;
    public boolean n0 = false;
    public final Path o0 = new Path();
    public final Path p0 = new Path();
    public int q0 = 0;
    public final RectF r0 = new RectF();
    public int s0 = 255;

    public u93(int i) {
        g(i);
    }

    @TargetApi(11)
    public static u93 c(ColorDrawable colorDrawable) {
        return new u93(colorDrawable.getColor());
    }

    @Override // defpackage.s93
    public void a(int i, float f) {
        if (this.l0 != i) {
            this.l0 = i;
            invalidateSelf();
        }
        if (this.j0 != f) {
            this.j0 = f;
            i();
            invalidateSelf();
        }
    }

    @Override // defpackage.s93
    public void b(boolean z) {
        this.i0 = z;
        i();
        invalidateSelf();
    }

    @Override // defpackage.s93
    public void d(boolean z) {
        if (this.n0 != z) {
            this.n0 = z;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.h0.setColor(br0.c(this.q0, this.s0));
        this.h0.setStyle(Paint.Style.FILL);
        this.h0.setFilterBitmap(f());
        canvas.drawPath(this.o0, this.h0);
        if (this.j0 != Utils.FLOAT_EPSILON) {
            this.h0.setColor(br0.c(this.l0, this.s0));
            this.h0.setStyle(Paint.Style.STROKE);
            this.h0.setStrokeWidth(this.j0);
            canvas.drawPath(this.p0, this.h0);
        }
    }

    @Override // defpackage.s93
    public void e(boolean z) {
        if (this.m0 != z) {
            this.m0 = z;
            i();
            invalidateSelf();
        }
    }

    public boolean f() {
        return this.n0;
    }

    public void g(int i) {
        if (this.q0 != i) {
            this.q0 = i;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.s0;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return br0.b(br0.c(this.q0, this.s0));
    }

    @Override // defpackage.s93
    public void h(float f) {
        if (this.k0 != f) {
            this.k0 = f;
            i();
            invalidateSelf();
        }
    }

    public final void i() {
        float[] fArr;
        float[] fArr2;
        this.o0.reset();
        this.p0.reset();
        this.r0.set(getBounds());
        RectF rectF = this.r0;
        float f = this.j0;
        rectF.inset(f / 2.0f, f / 2.0f);
        int i = 0;
        if (this.i0) {
            this.p0.addCircle(this.r0.centerX(), this.r0.centerY(), Math.min(this.r0.width(), this.r0.height()) / 2.0f, Path.Direction.CW);
        } else {
            int i2 = 0;
            while (true) {
                fArr = this.f0;
                if (i2 >= fArr.length) {
                    break;
                }
                fArr[i2] = (this.a[i2] + this.k0) - (this.j0 / 2.0f);
                i2++;
            }
            this.p0.addRoundRect(this.r0, fArr, Path.Direction.CW);
        }
        RectF rectF2 = this.r0;
        float f2 = this.j0;
        rectF2.inset((-f2) / 2.0f, (-f2) / 2.0f);
        float f3 = this.k0 + (this.m0 ? this.j0 : Utils.FLOAT_EPSILON);
        this.r0.inset(f3, f3);
        if (this.i0) {
            this.o0.addCircle(this.r0.centerX(), this.r0.centerY(), Math.min(this.r0.width(), this.r0.height()) / 2.0f, Path.Direction.CW);
        } else if (this.m0) {
            if (this.g0 == null) {
                this.g0 = new float[8];
            }
            while (true) {
                fArr2 = this.g0;
                if (i >= fArr2.length) {
                    break;
                }
                fArr2[i] = this.a[i] - this.j0;
                i++;
            }
            this.o0.addRoundRect(this.r0, fArr2, Path.Direction.CW);
        } else {
            this.o0.addRoundRect(this.r0, this.a, Path.Direction.CW);
        }
        float f4 = -f3;
        this.r0.inset(f4, f4);
    }

    @Override // defpackage.s93
    public void l(float[] fArr) {
        if (fArr == null) {
            Arrays.fill(this.a, (float) Utils.FLOAT_EPSILON);
        } else {
            xt2.c(fArr.length == 8, "radii should have exactly 8 values");
            System.arraycopy(fArr, 0, this.a, 0, 8);
        }
        i();
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        i();
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (i != this.s0) {
            this.s0 = i;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }
}
