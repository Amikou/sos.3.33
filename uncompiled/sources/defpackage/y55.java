package defpackage;

import android.os.Bundle;
import com.google.android.gms.measurement.internal.zzaq;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: y55  reason: default package */
/* loaded from: classes.dex */
public final class y55 implements Iterator<String> {
    public final Iterator<String> a;
    public final /* synthetic */ zzaq f0;

    public y55(zzaq zzaqVar) {
        Bundle bundle;
        this.f0 = zzaqVar;
        bundle = zzaqVar.a;
        this.a = bundle.keySet().iterator();
    }

    @Override // java.util.Iterator
    /* renamed from: a */
    public final String next() {
        return this.a.next();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
