package defpackage;

import kotlin.Result;

/* compiled from: FastServiceLoader.kt */
/* renamed from: i21  reason: default package */
/* loaded from: classes2.dex */
public final class i21 {
    public static final boolean a;

    static {
        Object m52constructorimpl;
        try {
            Result.a aVar = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(Class.forName("android.os.Build"));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(o83.a(th));
        }
        a = Result.m58isSuccessimpl(m52constructorimpl);
    }

    public static final boolean a() {
        return a;
    }
}
