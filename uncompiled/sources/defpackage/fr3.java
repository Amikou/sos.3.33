package defpackage;

import java.util.Comparator;

/* compiled from: SortedIterable.java */
/* renamed from: fr3  reason: default package */
/* loaded from: classes2.dex */
public interface fr3<T> extends Iterable<T> {
    Comparator<? super T> comparator();
}
