package defpackage;

import java.util.ArrayDeque;
import java.util.Queue;

/* renamed from: rx4  reason: default package */
/* loaded from: classes2.dex */
public final class rx4<ResultT> {
    public final Object a = new Object();
    public Queue<px4<ResultT>> b;
    public boolean c;

    public final void a(px4<ResultT> px4Var) {
        synchronized (this.a) {
            if (this.b == null) {
                this.b = new ArrayDeque();
            }
            this.b.add(px4Var);
        }
    }

    public final void b(l34<ResultT> l34Var) {
        px4<ResultT> poll;
        synchronized (this.a) {
            if (this.b != null && !this.c) {
                this.c = true;
                while (true) {
                    synchronized (this.a) {
                        poll = this.b.poll();
                        if (poll == null) {
                            this.c = false;
                            return;
                        }
                    }
                    poll.a(l34Var);
                }
            }
        }
    }
}
