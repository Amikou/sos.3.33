package defpackage;

import com.google.android.gms.internal.clearcut.zzfl;
import com.google.android.gms.internal.clearcut.zzfq;

/* renamed from: ca5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class ca5 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[zzfl.values().length];
        b = iArr;
        try {
            iArr[zzfl.zzqc.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            b[zzfl.zzqd.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            b[zzfl.zzqe.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            b[zzfl.zzqf.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            b[zzfl.zzqg.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            b[zzfl.zzqh.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            b[zzfl.zzqi.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            b[zzfl.zzqj.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        try {
            b[zzfl.zzql.ordinal()] = 9;
        } catch (NoSuchFieldError unused9) {
        }
        try {
            b[zzfl.zzqm.ordinal()] = 10;
        } catch (NoSuchFieldError unused10) {
        }
        try {
            b[zzfl.zzqk.ordinal()] = 11;
        } catch (NoSuchFieldError unused11) {
        }
        try {
            b[zzfl.zzqn.ordinal()] = 12;
        } catch (NoSuchFieldError unused12) {
        }
        try {
            b[zzfl.zzqo.ordinal()] = 13;
        } catch (NoSuchFieldError unused13) {
        }
        try {
            b[zzfl.zzqq.ordinal()] = 14;
        } catch (NoSuchFieldError unused14) {
        }
        try {
            b[zzfl.zzqr.ordinal()] = 15;
        } catch (NoSuchFieldError unused15) {
        }
        try {
            b[zzfl.zzqs.ordinal()] = 16;
        } catch (NoSuchFieldError unused16) {
        }
        try {
            b[zzfl.zzqt.ordinal()] = 17;
        } catch (NoSuchFieldError unused17) {
        }
        try {
            b[zzfl.zzqp.ordinal()] = 18;
        } catch (NoSuchFieldError unused18) {
        }
        int[] iArr2 = new int[zzfq.values().length];
        a = iArr2;
        try {
            iArr2[zzfq.INT.ordinal()] = 1;
        } catch (NoSuchFieldError unused19) {
        }
        try {
            a[zzfq.LONG.ordinal()] = 2;
        } catch (NoSuchFieldError unused20) {
        }
        try {
            a[zzfq.FLOAT.ordinal()] = 3;
        } catch (NoSuchFieldError unused21) {
        }
        try {
            a[zzfq.DOUBLE.ordinal()] = 4;
        } catch (NoSuchFieldError unused22) {
        }
        try {
            a[zzfq.BOOLEAN.ordinal()] = 5;
        } catch (NoSuchFieldError unused23) {
        }
        try {
            a[zzfq.STRING.ordinal()] = 6;
        } catch (NoSuchFieldError unused24) {
        }
        try {
            a[zzfq.BYTE_STRING.ordinal()] = 7;
        } catch (NoSuchFieldError unused25) {
        }
        try {
            a[zzfq.ENUM.ordinal()] = 8;
        } catch (NoSuchFieldError unused26) {
        }
        try {
            a[zzfq.MESSAGE.ordinal()] = 9;
        } catch (NoSuchFieldError unused27) {
        }
    }
}
