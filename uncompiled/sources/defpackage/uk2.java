package defpackage;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSSMSSubscriptionStateChanges.java */
/* renamed from: uk2  reason: default package */
/* loaded from: classes2.dex */
public class uk2 {
    public tk2 a;
    public tk2 b;

    public uk2(tk2 tk2Var, tk2 tk2Var2) {
        this.a = tk2Var;
        this.b = tk2Var2;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("from", this.a.f());
            jSONObject.put("to", this.b.f());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return a().toString();
    }
}
