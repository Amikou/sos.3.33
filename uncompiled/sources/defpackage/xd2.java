package defpackage;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.d;
import androidx.lifecycle.f;
import androidx.lifecycle.j;
import androidx.lifecycle.l;
import androidx.savedstate.SavedStateRegistry;
import java.util.UUID;

/* compiled from: NavBackStackEntry.java */
/* renamed from: xd2  reason: default package */
/* loaded from: classes.dex */
public final class xd2 implements rz1, hj4, d, fc3 {
    public final Context a;
    public final androidx.navigation.d f0;
    public Bundle g0;
    public final f h0;
    public final androidx.savedstate.a i0;
    public final UUID j0;
    public Lifecycle.State k0;
    public Lifecycle.State l0;
    public zd2 m0;
    public l.b n0;
    public ec3 o0;

    /* compiled from: NavBackStackEntry.java */
    /* renamed from: xd2$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Lifecycle.Event.values().length];
            a = iArr;
            try {
                iArr[Lifecycle.Event.ON_CREATE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Lifecycle.Event.ON_STOP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Lifecycle.Event.ON_START.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[Lifecycle.Event.ON_PAUSE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[Lifecycle.Event.ON_RESUME.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[Lifecycle.Event.ON_DESTROY.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[Lifecycle.Event.ON_ANY.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* compiled from: NavBackStackEntry.java */
    /* renamed from: xd2$b */
    /* loaded from: classes.dex */
    public static class b extends androidx.lifecycle.a {
        public b(fc3 fc3Var, Bundle bundle) {
            super(fc3Var, bundle);
        }

        @Override // androidx.lifecycle.a
        public <T extends dj4> T d(String str, Class<T> cls, ec3 ec3Var) {
            return new c(ec3Var);
        }
    }

    /* compiled from: NavBackStackEntry.java */
    /* renamed from: xd2$c */
    /* loaded from: classes.dex */
    public static class c extends dj4 {
        public ec3 a;

        public c(ec3 ec3Var) {
            this.a = ec3Var;
        }

        public ec3 a() {
            return this.a;
        }
    }

    public xd2(Context context, androidx.navigation.d dVar, Bundle bundle, rz1 rz1Var, zd2 zd2Var) {
        this(context, dVar, bundle, rz1Var, zd2Var, UUID.randomUUID(), null);
    }

    public static Lifecycle.State e(Lifecycle.Event event) {
        switch (a.a[event.ordinal()]) {
            case 1:
            case 2:
                return Lifecycle.State.CREATED;
            case 3:
            case 4:
                return Lifecycle.State.STARTED;
            case 5:
                return Lifecycle.State.RESUMED;
            case 6:
                return Lifecycle.State.DESTROYED;
            default:
                throw new IllegalArgumentException("Unexpected event value " + event);
        }
    }

    public Bundle a() {
        return this.g0;
    }

    public androidx.navigation.d b() {
        return this.f0;
    }

    public Lifecycle.State c() {
        return this.l0;
    }

    public ec3 d() {
        if (this.o0 == null) {
            this.o0 = ((c) new l(this, new b(this, null)).a(c.class)).a();
        }
        return this.o0;
    }

    public void f(Lifecycle.Event event) {
        this.k0 = e(event);
        j();
    }

    public void g(Bundle bundle) {
        this.g0 = bundle;
    }

    @Override // androidx.lifecycle.d
    public l.b getDefaultViewModelProviderFactory() {
        if (this.n0 == null) {
            this.n0 = new j((Application) this.a.getApplicationContext(), this, this.g0);
        }
        return this.n0;
    }

    @Override // defpackage.rz1
    public Lifecycle getLifecycle() {
        return this.h0;
    }

    @Override // defpackage.fc3
    public SavedStateRegistry getSavedStateRegistry() {
        return this.i0.b();
    }

    @Override // defpackage.hj4
    public gj4 getViewModelStore() {
        zd2 zd2Var = this.m0;
        if (zd2Var != null) {
            return zd2Var.c(this.j0);
        }
        throw new IllegalStateException("You must call setViewModelStore() on your NavHostController before accessing the ViewModelStore of a navigation graph.");
    }

    public void h(Bundle bundle) {
        this.i0.d(bundle);
    }

    public void i(Lifecycle.State state) {
        this.l0 = state;
        j();
    }

    public void j() {
        if (this.k0.ordinal() < this.l0.ordinal()) {
            this.h0.o(this.k0);
        } else {
            this.h0.o(this.l0);
        }
    }

    public xd2(Context context, androidx.navigation.d dVar, Bundle bundle, rz1 rz1Var, zd2 zd2Var, UUID uuid, Bundle bundle2) {
        this.h0 = new f(this);
        androidx.savedstate.a a2 = androidx.savedstate.a.a(this);
        this.i0 = a2;
        this.k0 = Lifecycle.State.CREATED;
        this.l0 = Lifecycle.State.RESUMED;
        this.a = context;
        this.j0 = uuid;
        this.f0 = dVar;
        this.g0 = bundle;
        this.m0 = zd2Var;
        a2.c(bundle2);
        if (rz1Var != null) {
            this.k0 = rz1Var.getLifecycle().b();
        }
    }
}
