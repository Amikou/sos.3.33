package defpackage;

/* compiled from: SimpleResource.java */
/* renamed from: op3  reason: default package */
/* loaded from: classes.dex */
public class op3<T> implements s73<T> {
    public final T a;

    public op3(T t) {
        this.a = (T) wt2.d(t);
    }

    @Override // defpackage.s73
    public final int a() {
        return 1;
    }

    @Override // defpackage.s73
    public void b() {
    }

    @Override // defpackage.s73
    public Class<T> d() {
        return (Class<T>) this.a.getClass();
    }

    @Override // defpackage.s73
    public final T get() {
        return this.a;
    }
}
