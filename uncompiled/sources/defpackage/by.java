package defpackage;

import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.charset.Charset;

/* compiled from: Charsets.kt */
/* renamed from: by  reason: default package */
/* loaded from: classes2.dex */
public final class by {
    public static final Charset a;
    public static Charset b;
    public static Charset c;
    public static final by d = new by();

    static {
        Charset forName = Charset.forName("UTF-8");
        fs1.e(forName, "Charset.forName(\"UTF-8\")");
        a = forName;
        fs1.e(Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        fs1.e(Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        fs1.e(Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        fs1.e(Charset.forName("US-ASCII"), "Charset.forName(\"US-ASCII\")");
        fs1.e(Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING), "Charset.forName(\"ISO-8859-1\")");
    }

    public final Charset a() {
        Charset charset = c;
        if (charset != null) {
            return charset;
        }
        Charset forName = Charset.forName("UTF-32BE");
        fs1.e(forName, "Charset.forName(\"UTF-32BE\")");
        c = forName;
        return forName;
    }

    public final Charset b() {
        Charset charset = b;
        if (charset != null) {
            return charset;
        }
        Charset forName = Charset.forName("UTF-32LE");
        fs1.e(forName, "Charset.forName(\"UTF-32LE\")");
        b = forName;
        return forName;
    }
}
