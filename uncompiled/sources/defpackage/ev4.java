package defpackage;

import android.content.Context;
import java.util.concurrent.Executor;

/* renamed from: ev4  reason: default package */
/* loaded from: classes2.dex */
public final class ev4 implements jw4<dv4> {
    public final jw4 a;
    public final jw4 b;
    public final jw4 c;
    public final jw4 d;
    public final jw4 e;
    public final jw4 f;
    public final jw4 g;
    public final jw4 h;
    public final /* synthetic */ int i = 0;

    public ev4(jw4<zv4> jw4Var, jw4<zy4> jw4Var2, jw4<cv4> jw4Var3, jw4<fx4> jw4Var4, jw4<pw4> jw4Var5, jw4<uw4> jw4Var6, jw4<yw4> jw4Var7, jw4<hw4> jw4Var8) {
        this.a = jw4Var;
        this.b = jw4Var2;
        this.c = jw4Var3;
        this.d = jw4Var4;
        this.e = jw4Var5;
        this.f = jw4Var6;
        this.g = jw4Var7;
        this.h = jw4Var8;
    }

    public ev4(jw4<Context> jw4Var, jw4<zv4> jw4Var2, jw4<dv4> jw4Var3, jw4<zy4> jw4Var4, jw4<fv4> jw4Var5, jw4<vu4> jw4Var6, jw4<Executor> jw4Var7, jw4<Executor> jw4Var8, byte[] bArr) {
        this.a = jw4Var;
        this.g = jw4Var2;
        this.h = jw4Var3;
        this.b = jw4Var4;
        this.e = jw4Var5;
        this.f = jw4Var6;
        this.c = jw4Var7;
        this.d = jw4Var8;
    }

    /* JADX WARN: Type inference failed for: r10v3, types: [au4, dv4] */
    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ dv4 a() {
        if (this.i == 0) {
            Object a = this.a.a();
            return new dv4((zv4) a, gw4.c(this.b), (cv4) this.c.a(), (fx4) this.d.a(), (pw4) this.e.a(), (uw4) this.f.a(), (yw4) this.g.a(), (hw4) this.h.a());
        }
        Context a2 = ((my4) this.a).a();
        Object a3 = this.g.a();
        Object a4 = this.h.a();
        cw4 c = gw4.c(this.b);
        Object a5 = this.e.a();
        Object a6 = this.f.a();
        return new au4(a2, (zv4) a3, (dv4) a4, c, (fv4) a5, (vu4) a6, gw4.c(this.c), gw4.c(this.d));
    }
}
