package defpackage;

import defpackage.je0;

/* compiled from: DashUtil.java */
/* renamed from: xd0  reason: default package */
/* loaded from: classes.dex */
public final class xd0 {
    public static je0 a(b73 b73Var, String str, s33 s33Var, int i) {
        return new je0.b().i(s33Var.b(str)).h(s33Var.a).g(s33Var.b).f(b(b73Var, s33Var)).b(i).a();
    }

    public static String b(b73 b73Var, s33 s33Var) {
        String a = b73Var.a();
        return a != null ? a : s33Var.b(b73Var.b.get(0).a).toString();
    }
}
