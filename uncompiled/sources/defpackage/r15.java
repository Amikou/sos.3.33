package defpackage;

import android.app.Activity;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: r15  reason: default package */
/* loaded from: classes.dex */
public final class r15 extends m05 {
    public final /* synthetic */ Intent a;
    public final /* synthetic */ Activity f0;
    public final /* synthetic */ int g0;

    public r15(Intent intent, Activity activity, int i) {
        this.a = intent;
        this.f0 = activity;
        this.g0 = i;
    }

    @Override // defpackage.m05
    public final void c() {
        Intent intent = this.a;
        if (intent != null) {
            this.f0.startActivityForResult(intent, this.g0);
        }
    }
}
