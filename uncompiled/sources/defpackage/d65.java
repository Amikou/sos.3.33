package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: d65  reason: default package */
/* loaded from: classes.dex */
public final class d65 implements Iterator<z55> {
    public int a = 0;
    public final /* synthetic */ f65 f0;

    public d65(f65 f65Var) {
        this.f0 = f65Var;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        String str;
        int i = this.a;
        str = this.f0.a;
        return i < str.length();
    }

    @Override // java.util.Iterator
    public final /* bridge */ /* synthetic */ z55 next() {
        String str;
        int i = this.a;
        str = this.f0.a;
        if (i < str.length()) {
            int i2 = this.a;
            this.a = i2 + 1;
            return new f65(String.valueOf(i2));
        }
        throw new NoSuchElementException();
    }
}
