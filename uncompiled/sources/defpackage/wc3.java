package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: Scene.java */
/* renamed from: wc3  reason: default package */
/* loaded from: classes.dex */
public class wc3 {
    public Context a;
    public int b = -1;
    public ViewGroup c;
    public View d;
    public Runnable e;
    public Runnable f;

    public wc3(ViewGroup viewGroup, View view) {
        this.c = viewGroup;
        this.d = view;
    }

    public static wc3 c(ViewGroup viewGroup) {
        return (wc3) viewGroup.getTag(zz2.transition_current_scene);
    }

    public static void f(ViewGroup viewGroup, wc3 wc3Var) {
        viewGroup.setTag(zz2.transition_current_scene, wc3Var);
    }

    public void a() {
        if (this.b > 0 || this.d != null) {
            d().removeAllViews();
            if (this.b > 0) {
                LayoutInflater.from(this.a).inflate(this.b, this.c);
            } else {
                this.c.addView(this.d);
            }
        }
        Runnable runnable = this.e;
        if (runnable != null) {
            runnable.run();
        }
        f(this.c, this);
    }

    public void b() {
        Runnable runnable;
        if (c(this.c) != this || (runnable = this.f) == null) {
            return;
        }
        runnable.run();
    }

    public ViewGroup d() {
        return this.c;
    }

    public boolean e() {
        return this.b > 0;
    }
}
