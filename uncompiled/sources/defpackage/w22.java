package defpackage;

import android.annotation.SuppressLint;
import defpackage.k72;

/* compiled from: LruResourceCache.java */
/* renamed from: w22  reason: default package */
/* loaded from: classes.dex */
public class w22 extends s22<fx1, s73<?>> implements k72 {
    public k72.a d;

    public w22(long j) {
        super(j);
    }

    @Override // defpackage.k72
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            b();
        } else if (i >= 20 || i == 15) {
            m(h() / 2);
        }
    }

    @Override // defpackage.k72
    public /* bridge */ /* synthetic */ s73 c(fx1 fx1Var, s73 s73Var) {
        return (s73) super.k(fx1Var, s73Var);
    }

    @Override // defpackage.k72
    public void d(k72.a aVar) {
        this.d = aVar;
    }

    @Override // defpackage.k72
    public /* bridge */ /* synthetic */ s73 e(fx1 fx1Var) {
        return (s73) super.l(fx1Var);
    }

    @Override // defpackage.s22
    /* renamed from: n */
    public int i(s73<?> s73Var) {
        if (s73Var == null) {
            return super.i(null);
        }
        return s73Var.a();
    }

    @Override // defpackage.s22
    /* renamed from: o */
    public void j(fx1 fx1Var, s73<?> s73Var) {
        k72.a aVar = this.d;
        if (aVar == null || s73Var == null) {
            return;
        }
        aVar.d(s73Var);
    }
}
