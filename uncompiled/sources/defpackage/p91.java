package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentAddNewTokenBinding.java */
/* renamed from: p91  reason: default package */
/* loaded from: classes2.dex */
public final class p91 {
    public final ConstraintLayout a;
    public final RecyclerView b;
    public final zd3 c;
    public final SwipeRefreshLayout d;
    public final g74 e;
    public final TextView f;

    public p91(ConstraintLayout constraintLayout, RecyclerView recyclerView, zd3 zd3Var, SwipeRefreshLayout swipeRefreshLayout, g74 g74Var, TextView textView) {
        this.a = constraintLayout;
        this.b = recyclerView;
        this.c = zd3Var;
        this.d = swipeRefreshLayout;
        this.e = g74Var;
        this.f = textView;
    }

    public static p91 a(View view) {
        int i = R.id.rvTokenList;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvTokenList);
        if (recyclerView != null) {
            i = R.id.searchBar;
            View a = ai4.a(view, R.id.searchBar);
            if (a != null) {
                zd3 a2 = zd3.a(a);
                i = R.id.swipeRefreshLayout;
                SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swipeRefreshLayout);
                if (swipeRefreshLayout != null) {
                    i = R.id.toolbar;
                    View a3 = ai4.a(view, R.id.toolbar);
                    if (a3 != null) {
                        g74 a4 = g74.a(a3);
                        i = R.id.tv_not;
                        TextView textView = (TextView) ai4.a(view, R.id.tv_not);
                        if (textView != null) {
                            return new p91((ConstraintLayout) view, recyclerView, a2, swipeRefreshLayout, a4, textView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
