package defpackage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.Property;
import android.view.animation.Interpolator;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.LinearProgressIndicatorSpec;
import java.util.Arrays;

/* compiled from: LinearIndeterminateDisjointAnimatorDelegate.java */
/* renamed from: a02  reason: default package */
/* loaded from: classes2.dex */
public final class a02 extends hq1<ObjectAnimator> {
    public static final int[] l = {533, 567, 850, 750};
    public static final int[] m = {1267, 1000, 333, 0};
    public static final Property<a02, Float> n = new b(Float.class, "animationFraction");
    public ObjectAnimator d;
    public final Interpolator[] e;
    public final wn f;
    public int g;
    public boolean h;
    public float i;
    public boolean j;
    public hd k;

    /* compiled from: LinearIndeterminateDisjointAnimatorDelegate.java */
    /* renamed from: a02$a */
    /* loaded from: classes2.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            if (a02.this.j) {
                a02.this.d.setRepeatCount(-1);
                a02 a02Var = a02.this;
                a02Var.k.onAnimationEnd(a02Var.a);
                a02.this.j = false;
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
            super.onAnimationRepeat(animator);
            a02 a02Var = a02.this;
            a02Var.g = (a02Var.g + 1) % a02.this.f.c.length;
            a02.this.h = true;
        }
    }

    /* compiled from: LinearIndeterminateDisjointAnimatorDelegate.java */
    /* renamed from: a02$b */
    /* loaded from: classes2.dex */
    public static class b extends Property<a02, Float> {
        public b(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(a02 a02Var) {
            return Float.valueOf(a02Var.q());
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(a02 a02Var, Float f) {
            a02Var.u(f.floatValue());
        }
    }

    public a02(Context context, LinearProgressIndicatorSpec linearProgressIndicatorSpec) {
        super(2);
        this.g = 0;
        this.k = null;
        this.f = linearProgressIndicatorSpec;
        this.e = new Interpolator[]{oe.b(context, vx2.linear_indeterminate_line1_head_interpolator), oe.b(context, vx2.linear_indeterminate_line1_tail_interpolator), oe.b(context, vx2.linear_indeterminate_line2_head_interpolator), oe.b(context, vx2.linear_indeterminate_line2_tail_interpolator)};
    }

    @Override // defpackage.hq1
    public void a() {
        ObjectAnimator objectAnimator = this.d;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
    }

    @Override // defpackage.hq1
    public void c() {
        t();
    }

    @Override // defpackage.hq1
    public void d(hd hdVar) {
        this.k = hdVar;
    }

    @Override // defpackage.hq1
    public void f() {
        if (this.a.isVisible()) {
            this.j = true;
            this.d.setRepeatCount(0);
            return;
        }
        a();
    }

    @Override // defpackage.hq1
    public void g() {
        r();
        t();
        this.d.start();
    }

    @Override // defpackage.hq1
    public void h() {
        this.k = null;
    }

    public final float q() {
        return this.i;
    }

    public final void r() {
        if (this.d == null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, n, Utils.FLOAT_EPSILON, 1.0f);
            this.d = ofFloat;
            ofFloat.setDuration(1800L);
            this.d.setInterpolator(null);
            this.d.setRepeatCount(-1);
            this.d.addListener(new a());
        }
    }

    public final void s() {
        if (this.h) {
            Arrays.fill(this.c, l42.a(this.f.c[this.g], this.a.getAlpha()));
            this.h = false;
        }
    }

    public void t() {
        this.g = 0;
        int a2 = l42.a(this.f.c[0], this.a.getAlpha());
        int[] iArr = this.c;
        iArr[0] = a2;
        iArr[1] = a2;
    }

    public void u(float f) {
        this.i = f;
        v((int) (f * 1800.0f));
        s();
        this.a.invalidateSelf();
    }

    public final void v(int i) {
        for (int i2 = 0; i2 < 4; i2++) {
            this.b[i2] = Math.max((float) Utils.FLOAT_EPSILON, Math.min(1.0f, this.e[i2].getInterpolation(b(i, m[i2], l[i2]))));
        }
    }
}
