package defpackage;

import net.safemoon.androidwallet.model.transaction.details.TransactionDetails;
import net.safemoon.androidwallet.model.transaction.details.TransactionDetailsData;
import retrofit2.b;
import retrofit2.n;

/* compiled from: TransferNotificationDetailsViewModel.kt */
/* renamed from: ua4  reason: default package */
/* loaded from: classes2.dex */
public final class ua4 extends dj4 {
    public final ac3 a;
    public final gb2<TransactionDetailsData> b;

    /* compiled from: TransferNotificationDetailsViewModel.kt */
    /* renamed from: ua4$a */
    /* loaded from: classes2.dex */
    public static final class a extends p83<TransactionDetails> {
        public a() {
        }

        @Override // defpackage.wu
        public void b(b<TransactionDetails> bVar, n<TransactionDetails> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.a() != null) {
                TransactionDetails a = nVar.a();
                if ((a == null ? null : a.getData()) != null) {
                    gb2<TransactionDetailsData> b = ua4.this.b();
                    TransactionDetails a2 = nVar.a();
                    fs1.d(a2);
                    b.setValue(a2.getData());
                }
            }
        }
    }

    public ua4(ac3 ac3Var) {
        fs1.f(ac3Var, "service");
        this.a = ac3Var;
        this.b = new gb2<>();
    }

    public final void a(String str) {
        fs1.f(str, "hash");
        this.a.b(str).n(new a());
    }

    public final gb2<TransactionDetailsData> b() {
        return this.b;
    }
}
