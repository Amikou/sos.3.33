package defpackage;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jo5  reason: default package */
/* loaded from: classes.dex */
public final class jo5 implements Runnable {
    public final /* synthetic */ AtomicReference a;
    public final /* synthetic */ dp5 f0;

    public jo5(dp5 dp5Var, AtomicReference atomicReference) {
        this.f0 = dp5Var;
        this.a = atomicReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        synchronized (this.a) {
            this.a.set(Integer.valueOf(this.f0.a.z().s(this.f0.a.c().n(), qf5.N)));
            this.a.notify();
        }
    }
}
