package defpackage;

import com.onesignal.s0;
import com.onesignal.w0;
import java.util.Arrays;
import java.util.HashSet;

/* compiled from: OSTaskRemoteController.java */
/* renamed from: yk2  reason: default package */
/* loaded from: classes2.dex */
public class yk2 extends w0 {
    public static final HashSet<String> f = new HashSet<>(Arrays.asList("getTags()", "setSMSNumber()", "setEmail()", "logoutSMSNumber()", "logoutEmail()", "syncHashedEmail()", "setExternalUserId()", "setLanguage()", "setSubscription()", "promptLocation()", "idsAvailable()", "sendTag()", "sendTags()", "setLocationShared()", "setDisableGMSMissingPrompt()", "setRequiresUserPrivacyConsent()", "unsubscribeWhenNotificationsAreDisabled()", "handleNotificationOpen()", "onAppLostFocus()", "sendOutcome()", "sendUniqueOutcome()", "sendOutcomeWithValue()", "removeGroupedNotifications()", "removeNotification()", "clearOneSignalNotifications()"));
    public final s0 e;

    public yk2(s0 s0Var, yj2 yj2Var) {
        super(yj2Var);
        this.e = s0Var;
    }

    public boolean g(String str) {
        return !this.e.k() && f.contains(str);
    }
}
