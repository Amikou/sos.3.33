package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.n;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.c;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import java.security.GeneralSecurityException;

/* compiled from: AesCmacKeyManager.java */
/* renamed from: ea  reason: default package */
/* loaded from: classes2.dex */
public final class ea extends g<com.google.crypto.tink.proto.a> {

    /* compiled from: AesCmacKeyManager.java */
    /* renamed from: ea$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<n, com.google.crypto.tink.proto.a> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public n a(com.google.crypto.tink.proto.a aVar) throws GeneralSecurityException {
            return new ru2(new pu2(aVar.H().toByteArray()), aVar.I().E());
        }
    }

    /* compiled from: AesCmacKeyManager.java */
    /* renamed from: ea$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<com.google.crypto.tink.proto.b, com.google.crypto.tink.proto.a> {
        public b(ea eaVar, Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public com.google.crypto.tink.proto.a a(com.google.crypto.tink.proto.b bVar) throws GeneralSecurityException {
            return com.google.crypto.tink.proto.a.K().u(0).s(ByteString.copyFrom(p33.c(bVar.D()))).t(bVar.E()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public com.google.crypto.tink.proto.b c(ByteString byteString) throws InvalidProtocolBufferException {
            return com.google.crypto.tink.proto.b.G(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(com.google.crypto.tink.proto.b bVar) throws GeneralSecurityException {
            ea.p(bVar.E());
            ea.q(bVar.D());
        }
    }

    public ea() {
        super(com.google.crypto.tink.proto.a.class, new a(n.class));
    }

    public static void n(boolean z) throws GeneralSecurityException {
        q.q(new ea(), z);
    }

    public static void p(c cVar) throws GeneralSecurityException {
        if (cVar.E() >= 10) {
            if (cVar.E() > 16) {
                throw new GeneralSecurityException("tag size too long");
            }
            return;
        }
        throw new GeneralSecurityException("tag size too short");
    }

    public static void q(int i) throws GeneralSecurityException {
        if (i != 32) {
            throw new GeneralSecurityException("AesCmacKey size wrong, must be 32 bytes");
        }
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesCmacKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, com.google.crypto.tink.proto.a> e() {
        return new b(this, com.google.crypto.tink.proto.b.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int l() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public com.google.crypto.tink.proto.a g(ByteString byteString) throws InvalidProtocolBufferException {
        return com.google.crypto.tink.proto.a.L(byteString, com.google.crypto.tink.shaded.protobuf.n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: o */
    public void i(com.google.crypto.tink.proto.a aVar) throws GeneralSecurityException {
        ug4.c(aVar.J(), l());
        q(aVar.H().size());
        p(aVar.I());
    }
}
