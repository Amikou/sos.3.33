package defpackage;

/* compiled from: OptionalInt.java */
/* renamed from: on2  reason: default package */
/* loaded from: classes2.dex */
public final class on2 {
    public static final on2 c = new on2();
    public final boolean a;
    public final int b;

    /* compiled from: OptionalInt.java */
    /* renamed from: on2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static final on2[] a = new on2[256];

        static {
            int i = 0;
            while (true) {
                on2[] on2VarArr = a;
                if (i >= on2VarArr.length) {
                    return;
                }
                on2VarArr[i] = new on2(i - 128);
                i++;
            }
        }
    }

    public on2() {
        this.a = false;
        this.b = 0;
    }

    public static on2 a() {
        return c;
    }

    public static on2 c(int i) {
        if (i >= -128 && i <= 127) {
            return a.a[i + 128];
        }
        return new on2(i);
    }

    public boolean b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof on2) {
            on2 on2Var = (on2) obj;
            boolean z = this.a;
            if (z && on2Var.a) {
                if (this.b == on2Var.b) {
                    return true;
                }
            } else if (z == on2Var.a) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        if (this.a) {
            return this.b;
        }
        return 0;
    }

    public String toString() {
        return this.a ? String.format("OptionalInt[%s]", Integer.valueOf(this.b)) : "OptionalInt.empty";
    }

    public on2(int i) {
        this.a = true;
        this.b = i;
    }
}
