package defpackage;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import com.google.firebase.components.InvalidRegistrarException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ComponentDiscovery.java */
/* renamed from: d40  reason: default package */
/* loaded from: classes2.dex */
public final class d40<T> {
    public final T a;
    public final c<T> b;

    /* compiled from: ComponentDiscovery.java */
    /* renamed from: d40$b */
    /* loaded from: classes2.dex */
    public static class b implements c<Context> {
        public final Class<? extends Service> a;

        public final Bundle b(Context context) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    return null;
                }
                ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, this.a), 128);
                if (serviceInfo == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.a);
                    sb.append(" has no service info.");
                    return null;
                }
                return serviceInfo.metaData;
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }

        @Override // defpackage.d40.c
        /* renamed from: c */
        public List<String> a(Context context) {
            Bundle b = b(context);
            if (b == null) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList();
            for (String str : b.keySet()) {
                if ("com.google.firebase.components.ComponentRegistrar".equals(b.get(str)) && str.startsWith("com.google.firebase.components:")) {
                    arrayList.add(str.substring(31));
                }
            }
            return arrayList;
        }

        public b(Class<? extends Service> cls) {
            this.a = cls;
        }
    }

    /* compiled from: ComponentDiscovery.java */
    /* renamed from: d40$c */
    /* loaded from: classes2.dex */
    public interface c<T> {
        List<String> a(T t);
    }

    public d40(T t, c<T> cVar) {
        this.a = t;
        this.b = cVar;
    }

    public static d40<Context> c(Context context, Class<? extends Service> cls) {
        return new d40<>(context, new b(cls));
    }

    public static g40 d(String str) {
        try {
            Class<?> cls = Class.forName(str);
            if (g40.class.isAssignableFrom(cls)) {
                return (g40) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            }
            throw new InvalidRegistrarException(String.format("Class %s is not an instance of %s", str, "com.google.firebase.components.ComponentRegistrar"));
        } catch (ClassNotFoundException unused) {
            String.format("Class %s is not an found.", str);
            return null;
        } catch (IllegalAccessException e) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s.", str), e);
        } catch (InstantiationException e2) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s.", str), e2);
        } catch (NoSuchMethodException e3) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s", str), e3);
        } catch (InvocationTargetException e4) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s", str), e4);
        }
    }

    public List<fw2<g40>> b() {
        ArrayList arrayList = new ArrayList();
        for (final String str : this.b.a(this.a)) {
            arrayList.add(new fw2() { // from class: c40
                @Override // defpackage.fw2
                public final Object get() {
                    g40 d;
                    d = d40.d(str);
                    return d;
                }
            });
        }
        return arrayList;
    }
}
