package defpackage;

import android.text.TextUtils;
import com.google.common.collect.ImmutableSet;
import java.util.regex.Pattern;

/* compiled from: TextEmphasis.java */
/* renamed from: j44  reason: default package */
/* loaded from: classes.dex */
public final class j44 {
    public static final Pattern d = Pattern.compile("\\s+");
    public static final ImmutableSet<String> e = ImmutableSet.of("auto", "none");
    public static final ImmutableSet<String> f = ImmutableSet.of("dot", "sesame", "circle");
    public static final ImmutableSet<String> g = ImmutableSet.of("filled", "open");
    public static final ImmutableSet<String> h = ImmutableSet.of("after", "before", "outside");
    public final int a;
    public final int b;
    public final int c;

    public j44(int i, int i2, int i3) {
        this.a = i;
        this.b = i2;
        this.c = i3;
    }

    public static j44 a(String str) {
        if (str == null) {
            return null;
        }
        String e2 = ei.e(str.trim());
        if (e2.isEmpty()) {
            return null;
        }
        return b(ImmutableSet.copyOf(TextUtils.split(e2, d)));
    }

    /* JADX WARN: Code restructure failed: missing block: B:35:0x0081, code lost:
        if (r9.equals("auto") != false) goto L22;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.j44 b(com.google.common.collect.ImmutableSet<java.lang.String> r9) {
        /*
            Method dump skipped, instructions count: 288
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.j44.b(com.google.common.collect.ImmutableSet):j44");
    }
}
