package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import com.squareup.picasso.t;

/* compiled from: LruCache.java */
/* renamed from: r22  reason: default package */
/* loaded from: classes2.dex */
public final class r22 implements tt {
    public final LruCache<String, b> a;

    /* compiled from: LruCache.java */
    /* renamed from: r22$a */
    /* loaded from: classes2.dex */
    public class a extends LruCache<String, b> {
        public a(r22 r22Var, int i) {
            super(i);
        }

        @Override // android.util.LruCache
        /* renamed from: a */
        public int sizeOf(String str, b bVar) {
            return bVar.b;
        }
    }

    /* compiled from: LruCache.java */
    /* renamed from: r22$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public final Bitmap a;
        public final int b;

        public b(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    public r22(Context context) {
        this(t.b(context));
    }

    @Override // defpackage.tt
    public int a() {
        return this.a.maxSize();
    }

    @Override // defpackage.tt
    public void b(String str, Bitmap bitmap) {
        if (str != null && bitmap != null) {
            int i = t.i(bitmap);
            if (i > a()) {
                this.a.remove(str);
                return;
            } else {
                this.a.put(str, new b(bitmap, i));
                return;
            }
        }
        throw new NullPointerException("key == null || bitmap == null");
    }

    @Override // defpackage.tt
    public Bitmap get(String str) {
        b bVar = this.a.get(str);
        if (bVar != null) {
            return bVar.a;
        }
        return null;
    }

    @Override // defpackage.tt
    public int size() {
        return this.a.size();
    }

    public r22(int i) {
        this.a = new a(this, i);
    }
}
