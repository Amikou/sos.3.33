package defpackage;

import android.app.job.JobParameters;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: pt5  reason: default package */
/* loaded from: classes.dex */
public interface pt5 {
    void a(Intent intent);

    void b(JobParameters jobParameters, boolean z);

    boolean d(int i);
}
