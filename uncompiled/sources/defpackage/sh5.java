package defpackage;

import com.google.android.gms.internal.measurement.w0;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: sh5  reason: default package */
/* loaded from: classes.dex */
public final class sh5 extends w1<w0, sh5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public sh5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.w0 r0 = com.google.android.gms.internal.measurement.w0.C()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.sh5.<init>():void");
    }

    public final boolean A() {
        return ((w0) this.f0).z();
    }

    public final boolean B() {
        return ((w0) this.f0).A();
    }

    public final int C() {
        return ((w0) this.f0).B();
    }

    public final String v() {
        return ((w0) this.f0).x();
    }

    public final sh5 x(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        w0.D((w0) this.f0, str);
        return this;
    }

    public final boolean y() {
        return ((w0) this.f0).y();
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ sh5(defpackage.qh5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.w0 r1 = com.google.android.gms.internal.measurement.w0.C()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.sh5.<init>(qh5):void");
    }
}
