package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: xf5  reason: default package */
/* loaded from: classes.dex */
public final class xf5 extends vh5 {
    public String c;
    public String d;
    public int e;
    public String f;
    public long g;
    public final long h;
    public List<String> i;
    public int j;
    public String k;
    public String l;
    public String m;

    public xf5(ck5 ck5Var, long j) {
        super(ck5Var);
        this.h = j;
    }

    @Override // defpackage.vh5
    public final boolean j() {
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x00cc  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00dd  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00ed  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00fd  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x011d  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x012d  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x013d  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x014d  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0169  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x018a  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x018c  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x01a2 A[Catch: IllegalStateException -> 0x022d, TRY_ENTER, TryCatch #3 {IllegalStateException -> 0x022d, blocks: (B:46:0x0172, B:50:0x018d, B:53:0x01a2, B:57:0x01c0, B:60:0x01cd, B:62:0x01d5, B:73:0x020e, B:75:0x0224, B:77:0x0229, B:76:0x0227, B:64:0x01db, B:56:0x01bc, B:65:0x01e2, B:67:0x01e8, B:71:0x0206, B:70:0x0202), top: B:105:0x0172 }] */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01e2 A[Catch: IllegalStateException -> 0x022d, TryCatch #3 {IllegalStateException -> 0x022d, blocks: (B:46:0x0172, B:50:0x018d, B:53:0x01a2, B:57:0x01c0, B:60:0x01cd, B:62:0x01d5, B:73:0x020e, B:75:0x0224, B:77:0x0229, B:76:0x0227, B:64:0x01db, B:56:0x01bc, B:65:0x01e2, B:67:0x01e8, B:71:0x0206, B:70:0x0202), top: B:105:0x0172 }] */
    /* JADX WARN: Removed duplicated region for block: B:73:0x020e A[Catch: IllegalStateException -> 0x022d, TryCatch #3 {IllegalStateException -> 0x022d, blocks: (B:46:0x0172, B:50:0x018d, B:53:0x01a2, B:57:0x01c0, B:60:0x01cd, B:62:0x01d5, B:73:0x020e, B:75:0x0224, B:77:0x0229, B:76:0x0227, B:64:0x01db, B:56:0x01bc, B:65:0x01e2, B:67:0x01e8, B:71:0x0206, B:70:0x0202), top: B:105:0x0172 }] */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0257  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x0290  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x029d  */
    @Override // defpackage.vh5
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void k() {
        /*
            Method dump skipped, instructions count: 692
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.xf5.k():void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:42:0x018a  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0190  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x01cf  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x01d2  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x01fa  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x0201  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.google.android.gms.measurement.internal.zzp l(java.lang.String r35) {
        /*
            Method dump skipped, instructions count: 563
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.xf5.l(java.lang.String):com.google.android.gms.measurement.internal.zzp");
    }

    public final String n() {
        g();
        zt2.j(this.c);
        return this.c;
    }

    public final String o() {
        g();
        zt2.j(this.k);
        return this.k;
    }

    public final String p() {
        g();
        return this.l;
    }

    public final String r() {
        g();
        zt2.j(this.m);
        return this.m;
    }

    public final int s() {
        g();
        return this.e;
    }

    public final int t() {
        g();
        return this.j;
    }

    public final List<String> u() {
        return this.i;
    }
}
