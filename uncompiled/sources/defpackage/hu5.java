package defpackage;

import com.google.android.gms.internal.vision.z0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: hu5  reason: default package */
/* loaded from: classes.dex */
public final class hu5 extends eu5 {
    public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

    public hu5() {
        super();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static <L> List<L> e(Object obj, long j, int i) {
        xt5 xt5Var;
        List<L> arrayList;
        List<L> f = f(obj, j);
        if (f.isEmpty()) {
            if (f instanceof gu5) {
                arrayList = new xt5(i);
            } else if ((f instanceof vw5) && (f instanceof gt5)) {
                arrayList = ((gt5) f).d(i);
            } else {
                arrayList = new ArrayList<>(i);
            }
            z0.j(obj, j, arrayList);
            return arrayList;
        }
        if (c.isAssignableFrom(f.getClass())) {
            ArrayList arrayList2 = new ArrayList(f.size() + i);
            arrayList2.addAll(f);
            z0.j(obj, j, arrayList2);
            xt5Var = arrayList2;
        } else if (f instanceof uy5) {
            xt5 xt5Var2 = new xt5(f.size() + i);
            xt5Var2.addAll((uy5) f);
            z0.j(obj, j, xt5Var2);
            xt5Var = xt5Var2;
        } else if ((f instanceof vw5) && (f instanceof gt5)) {
            gt5 gt5Var = (gt5) f;
            if (gt5Var.zza()) {
                return f;
            }
            gt5 d = gt5Var.d(f.size() + i);
            z0.j(obj, j, d);
            return d;
        } else {
            return f;
        }
        return xt5Var;
    }

    public static <E> List<E> f(Object obj, long j) {
        return (List) z0.F(obj, j);
    }

    @Override // defpackage.eu5
    public final <E> void b(Object obj, Object obj2, long j) {
        List f = f(obj2, j);
        List e = e(obj, j, f.size());
        int size = e.size();
        int size2 = f.size();
        if (size > 0 && size2 > 0) {
            e.addAll(f);
        }
        if (size > 0) {
            f = e;
        }
        z0.j(obj, j, f);
    }

    @Override // defpackage.eu5
    public final void d(Object obj, long j) {
        Object unmodifiableList;
        List list = (List) z0.F(obj, j);
        if (list instanceof gu5) {
            unmodifiableList = ((gu5) list).c();
        } else if (c.isAssignableFrom(list.getClass())) {
            return;
        } else {
            if ((list instanceof vw5) && (list instanceof gt5)) {
                gt5 gt5Var = (gt5) list;
                if (gt5Var.zza()) {
                    gt5Var.zzb();
                    return;
                }
                return;
            }
            unmodifiableList = Collections.unmodifiableList(list);
        }
        z0.j(obj, j, unmodifiableList);
    }
}
