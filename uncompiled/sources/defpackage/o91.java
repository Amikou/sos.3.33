package defpackage;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentAddEditContactBinding.java */
/* renamed from: o91  reason: default package */
/* loaded from: classes2.dex */
public final class o91 {
    public final ConstraintLayout a;
    public final TextView b;
    public final AppCompatImageView c;
    public final MaterialButton d;
    public final LinearLayout e;
    public final EditText f;
    public final EditText g;
    public final ImageView h;
    public final ImageView i;
    public final AppCompatImageView j;
    public final ConstraintLayout k;
    public final MaterialCheckBox l;
    public final MaterialButton m;
    public final g74 n;

    public o91(ConstraintLayout constraintLayout, TextView textView, AppCompatImageView appCompatImageView, MaterialButton materialButton, LinearLayout linearLayout, CardView cardView, EditText editText, EditText editText2, ImageView imageView, ImageView imageView2, AppCompatImageView appCompatImageView2, LinearLayoutCompat linearLayoutCompat, ConstraintLayout constraintLayout2, MaterialCheckBox materialCheckBox, MaterialButton materialButton2, LinearLayout linearLayout2, g74 g74Var, TextView textView2, TextView textView3, TextView textView4, View view, View view2, View view3, View view4, View view5) {
        this.a = constraintLayout;
        this.b = textView;
        this.c = appCompatImageView;
        this.d = materialButton;
        this.e = linearLayout;
        this.f = editText;
        this.g = editText2;
        this.h = imageView;
        this.i = imageView2;
        this.j = appCompatImageView2;
        this.k = constraintLayout2;
        this.l = materialCheckBox;
        this.m = materialButton2;
        this.n = g74Var;
    }

    public static o91 a(View view) {
        int i = R.id.btnPaste;
        TextView textView = (TextView) ai4.a(view, R.id.btnPaste);
        if (textView != null) {
            i = R.id.btnQr;
            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnQr);
            if (appCompatImageView != null) {
                i = R.id.btnSave;
                MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnSave);
                if (materialButton != null) {
                    i = R.id.childWrapper;
                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.childWrapper);
                    if (linearLayout != null) {
                        i = R.id.cvContactIconContainer;
                        CardView cardView = (CardView) ai4.a(view, R.id.cvContactIconContainer);
                        if (cardView != null) {
                            i = R.id.etContactAddress;
                            EditText editText = (EditText) ai4.a(view, R.id.etContactAddress);
                            if (editText != null) {
                                i = R.id.etContactName;
                                EditText editText2 = (EditText) ai4.a(view, R.id.etContactName);
                                if (editText2 != null) {
                                    i = R.id.ivClear;
                                    ImageView imageView = (ImageView) ai4.a(view, R.id.ivClear);
                                    if (imageView != null) {
                                        i = R.id.ivContactIcon;
                                        ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivContactIcon);
                                        if (imageView2 != null) {
                                            i = R.id.ivCopy;
                                            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.ivCopy);
                                            if (appCompatImageView2 != null) {
                                                i = R.id.lContactAddressContainer;
                                                LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.lContactAddressContainer);
                                                if (linearLayoutCompat != null) {
                                                    ConstraintLayout constraintLayout = (ConstraintLayout) view;
                                                    i = R.id.selectAllChains;
                                                    MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.selectAllChains);
                                                    if (materialCheckBox != null) {
                                                        i = R.id.selectChain;
                                                        MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.selectChain);
                                                        if (materialButton2 != null) {
                                                            i = R.id.selectedChainWrapper;
                                                            LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.selectedChainWrapper);
                                                            if (linearLayout2 != null) {
                                                                i = R.id.toolbar;
                                                                View a = ai4.a(view, R.id.toolbar);
                                                                if (a != null) {
                                                                    g74 a2 = g74.a(a);
                                                                    i = R.id.tvContactAddressText;
                                                                    TextView textView2 = (TextView) ai4.a(view, R.id.tvContactAddressText);
                                                                    if (textView2 != null) {
                                                                        i = R.id.tvContactNameText;
                                                                        TextView textView3 = (TextView) ai4.a(view, R.id.tvContactNameText);
                                                                        if (textView3 != null) {
                                                                            i = R.id.tvHintEnableForUseOnTheseChains;
                                                                            TextView textView4 = (TextView) ai4.a(view, R.id.tvHintEnableForUseOnTheseChains);
                                                                            if (textView4 != null) {
                                                                                i = R.id.vContactAddressDivider;
                                                                                View a3 = ai4.a(view, R.id.vContactAddressDivider);
                                                                                if (a3 != null) {
                                                                                    i = R.id.vContactAllChainDivider;
                                                                                    View a4 = ai4.a(view, R.id.vContactAllChainDivider);
                                                                                    if (a4 != null) {
                                                                                        i = R.id.vContactNameDivider;
                                                                                        View a5 = ai4.a(view, R.id.vContactNameDivider);
                                                                                        if (a5 != null) {
                                                                                            i = R.id.vContactSelectChainDivider;
                                                                                            View a6 = ai4.a(view, R.id.vContactSelectChainDivider);
                                                                                            if (a6 != null) {
                                                                                                i = R.id.vSelectChainDivider;
                                                                                                View a7 = ai4.a(view, R.id.vSelectChainDivider);
                                                                                                if (a7 != null) {
                                                                                                    return new o91(constraintLayout, textView, appCompatImageView, materialButton, linearLayout, cardView, editText, editText2, imageView, imageView2, appCompatImageView2, linearLayoutCompat, constraintLayout, materialCheckBox, materialButton2, linearLayout2, a2, textView2, textView3, textView4, a3, a4, a5, a6, a7);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
