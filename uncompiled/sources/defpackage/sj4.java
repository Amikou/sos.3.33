package defpackage;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.github.mikephil.charting.utils.Utils;
import java.util.regex.Pattern;

/* compiled from: ViewPosition.java */
/* renamed from: sj4  reason: default package */
/* loaded from: classes.dex */
public class sj4 {
    public static final int[] e;
    public static final Matrix f;
    public static final Rect g;
    public final Rect a = new Rect();
    public final Rect b = new Rect();
    public final Rect c = new Rect();
    public final Rect d = new Rect();

    static {
        Pattern.compile("#");
        e = new int[2];
        f = new Matrix();
        g = new Rect();
    }

    public static void a(sj4 sj4Var, Point point) {
        Rect rect = sj4Var.a;
        int i = point.x;
        int i2 = point.y;
        rect.set(i, i2, i + 1, i2 + 1);
        sj4Var.b.set(sj4Var.a);
        sj4Var.c.set(sj4Var.a);
        sj4Var.d.set(sj4Var.a);
    }

    public static boolean b(sj4 sj4Var, View view) {
        return sj4Var.e(view);
    }

    public static float c(Object obj) {
        if (obj instanceof View) {
            View view = (View) obj;
            return view.getScaleX() * c(view.getParent());
        }
        return 1.0f;
    }

    public static float d(Object obj) {
        if (obj instanceof View) {
            View view = (View) obj;
            return view.getScaleY() * d(view.getParent());
        }
        return 1.0f;
    }

    public static sj4 f() {
        return new sj4();
    }

    public final boolean e(View view) {
        if (view.getWindowToken() == null) {
            return false;
        }
        Matrix matrix = f;
        matrix.setScale(c(view), d(view), Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        Rect rect = g;
        rect.set(this.a);
        int[] iArr = e;
        view.getLocationInWindow(iArr);
        this.a.set(0, 0, view.getWidth(), view.getHeight());
        v42.e(matrix, this.a);
        this.a.offset(iArr[0], iArr[1]);
        this.b.set(view.getPaddingLeft(), view.getPaddingTop(), view.getWidth() - view.getPaddingRight(), view.getHeight() - view.getPaddingBottom());
        v42.e(matrix, this.b);
        this.b.offset(iArr[0], iArr[1]);
        if (!view.getGlobalVisibleRect(this.c)) {
            this.c.set(this.a.centerX(), this.a.centerY(), this.a.centerX() + 1, this.a.centerY() + 1);
        }
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            Drawable drawable = imageView.getDrawable();
            if (drawable == null) {
                this.d.set(this.b);
            } else {
                int intrinsicWidth = drawable.getIntrinsicWidth();
                int intrinsicHeight = drawable.getIntrinsicHeight();
                dp1.a(imageView.getScaleType(), intrinsicWidth, intrinsicHeight, this.b.width(), this.b.height(), imageView.getImageMatrix(), matrix);
                this.d.set(0, 0, intrinsicWidth, intrinsicHeight);
                v42.e(matrix, this.d);
                Rect rect2 = this.d;
                Rect rect3 = this.b;
                rect2.offset(rect3.left, rect3.top);
            }
        } else {
            this.d.set(this.b);
        }
        return !rect.equals(this.a);
    }

    public String g() {
        return TextUtils.join("#", new String[]{this.a.flattenToString(), this.b.flattenToString(), this.c.flattenToString(), this.d.flattenToString()});
    }
}
