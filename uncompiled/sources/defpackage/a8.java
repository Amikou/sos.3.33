package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityStartWalletBinding.java */
/* renamed from: a8  reason: default package */
/* loaded from: classes2.dex */
public final class a8 {
    public final ConstraintLayout a;

    public a8(ConstraintLayout constraintLayout, bq1 bq1Var) {
        this.a = constraintLayout;
    }

    public static a8 a(View view) {
        View a = ai4.a(view, R.id.layoutSafeMoonBrand);
        if (a != null) {
            return new a8((ConstraintLayout) view, bq1.a(a));
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.layoutSafeMoonBrand)));
    }

    public static a8 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static a8 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_start_wallet, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
