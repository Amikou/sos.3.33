package defpackage;

import com.google.firebase.installations.local.b;

/* compiled from: GetIdListener.java */
/* renamed from: jf1  reason: default package */
/* loaded from: classes2.dex */
public class jf1 implements bt3 {
    public final n34<String> a;

    public jf1(n34<String> n34Var) {
        this.a = n34Var;
    }

    @Override // defpackage.bt3
    public boolean a(Exception exc) {
        return false;
    }

    @Override // defpackage.bt3
    public boolean b(b bVar) {
        if (bVar.l() || bVar.k() || bVar.i()) {
            this.a.e(bVar.d());
            return true;
        }
        return false;
    }
}
