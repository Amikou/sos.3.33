package defpackage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks;
import android.content.res.Configuration;
import android.os.Bundle;
import com.onesignal.OSFocusHandler;

/* compiled from: ActivityLifecycleListener.java */
/* renamed from: k7  reason: default package */
/* loaded from: classes2.dex */
public class k7 implements Application.ActivityLifecycleCallbacks {
    public static k7 a;
    @SuppressLint({"StaticFieldLeak"})
    public static com.onesignal.a f0;
    public static ComponentCallbacks g0;

    /* compiled from: ActivityLifecycleListener.java */
    /* renamed from: k7$a */
    /* loaded from: classes2.dex */
    public class a implements ComponentCallbacks {
        @Override // android.content.ComponentCallbacks
        public void onConfigurationChanged(Configuration configuration) {
            k7.f0.o(configuration, k7.f0.d());
        }

        @Override // android.content.ComponentCallbacks
        public void onLowMemory() {
        }
    }

    public static com.onesignal.a b() {
        return f0;
    }

    public static void c(Application application) {
        if (a == null) {
            k7 k7Var = new k7();
            a = k7Var;
            application.registerActivityLifecycleCallbacks(k7Var);
        }
        if (f0 == null) {
            f0 = new com.onesignal.a(new OSFocusHandler());
        }
        if (g0 == null) {
            ComponentCallbacks aVar = new a();
            g0 = aVar;
            application.registerComponentCallbacks(aVar);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        com.onesignal.a aVar = f0;
        if (aVar != null) {
            aVar.i(activity);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        com.onesignal.a aVar = f0;
        if (aVar != null) {
            aVar.j(activity);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
        com.onesignal.a aVar = f0;
        if (aVar != null) {
            aVar.k(activity);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
        com.onesignal.a aVar = f0;
        if (aVar != null) {
            aVar.l(activity);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
        com.onesignal.a aVar = f0;
        if (aVar != null) {
            aVar.m(activity);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
        com.onesignal.a aVar = f0;
        if (aVar != null) {
            aVar.n(activity);
        }
    }
}
