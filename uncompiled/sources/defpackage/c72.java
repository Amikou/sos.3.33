package defpackage;

import android.content.Context;
import android.net.Uri;
import defpackage.j92;
import java.io.InputStream;

/* compiled from: MediaStoreImageThumbLoader.java */
/* renamed from: c72  reason: default package */
/* loaded from: classes.dex */
public class c72 implements j92<Uri, InputStream> {
    public final Context a;

    /* compiled from: MediaStoreImageThumbLoader.java */
    /* renamed from: c72$a */
    /* loaded from: classes.dex */
    public static class a implements k92<Uri, InputStream> {
        public final Context a;

        public a(Context context) {
            this.a = context;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Uri, InputStream> c(qa2 qa2Var) {
            return new c72(this.a);
        }
    }

    public c72(Context context) {
        this.a = context.getApplicationContext();
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<InputStream> b(Uri uri, int i, int i2, vn2 vn2Var) {
        if (d72.d(i, i2)) {
            return new j92.a<>(new ll2(uri), p54.f(this.a, uri));
        }
        return null;
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(Uri uri) {
        return d72.a(uri);
    }
}
