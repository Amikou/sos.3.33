package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext.a;

/* compiled from: CoroutineContextImpl.kt */
/* renamed from: v4  reason: default package */
/* loaded from: classes2.dex */
public abstract class v4<B extends CoroutineContext.a, E extends B> implements CoroutineContext.b<E> {
    public final CoroutineContext.b<?> a;
    public final tc1<CoroutineContext.a, E> f0;

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v0, types: [tc1<kotlin.coroutines.CoroutineContext$a, E extends B>, tc1<? super kotlin.coroutines.CoroutineContext$a, ? extends E extends B>, java.lang.Object] */
    public v4(CoroutineContext.b<B> bVar, tc1<? super CoroutineContext.a, ? extends E> tc1Var) {
        fs1.f(bVar, "baseKey");
        fs1.f(tc1Var, "safeCast");
        this.f0 = tc1Var;
        this.a = bVar instanceof v4 ? (CoroutineContext.b<B>) ((v4) bVar).a : bVar;
    }

    public final boolean a(CoroutineContext.b<?> bVar) {
        fs1.f(bVar, "key");
        return bVar == this || this.a == bVar;
    }

    /* JADX WARN: Incorrect return type in method signature: (Lkotlin/coroutines/CoroutineContext$a;)TE; */
    public final CoroutineContext.a b(CoroutineContext.a aVar) {
        fs1.f(aVar, "element");
        return (CoroutineContext.a) this.f0.invoke(aVar);
    }
}
