package defpackage;

import java.math.BigInteger;
import java.util.Objects;

/* renamed from: at0  reason: default package */
/* loaded from: classes2.dex */
public class at0 implements ws0 {
    public xs0 f;
    public byte[] g;
    public pt0 h;
    public BigInteger i;
    public BigInteger j;

    public at0(xs0 xs0Var, pt0 pt0Var, BigInteger bigInteger) {
        this(xs0Var, pt0Var, bigInteger, ws0.b, null);
    }

    public at0(xs0 xs0Var, pt0 pt0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        this(xs0Var, pt0Var, bigInteger, bigInteger2, null);
    }

    public at0(xs0 xs0Var, pt0 pt0Var, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        Objects.requireNonNull(xs0Var, "curve");
        Objects.requireNonNull(bigInteger, "n");
        this.f = xs0Var;
        this.h = f(xs0Var, pt0Var);
        this.i = bigInteger;
        this.j = bigInteger2;
        this.g = bArr;
    }

    public static pt0 f(xs0 xs0Var, pt0 pt0Var) {
        if (pt0Var != null) {
            if (pt0Var.u()) {
                throw new IllegalArgumentException("point at infinity");
            }
            pt0 A = pt0Var.A();
            if (A.w()) {
                return vs0.i(xs0Var, A);
            }
            throw new IllegalArgumentException("point not on curve");
        }
        throw new IllegalArgumentException("point has null value");
    }

    public xs0 a() {
        return this.f;
    }

    public pt0 b() {
        return this.h;
    }

    public BigInteger c() {
        return this.j;
    }

    public BigInteger d() {
        return this.i;
    }

    public byte[] e() {
        return wh.e(this.g);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof at0) {
            at0 at0Var = (at0) obj;
            return this.f.m(at0Var.f) && this.h.e(at0Var.h) && this.i.equals(at0Var.i) && this.j.equals(at0Var.j);
        }
        return false;
    }

    public int hashCode() {
        return (((((this.f.hashCode() * 37) ^ this.h.hashCode()) * 37) ^ this.i.hashCode()) * 37) ^ this.j.hashCode();
    }
}
