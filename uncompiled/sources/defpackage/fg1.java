package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;

/* compiled from: GingerbreadBitmapFactory.java */
/* renamed from: fg1  reason: default package */
/* loaded from: classes.dex */
public class fg1 extends br2 {
    @Override // defpackage.br2
    public a<Bitmap> c(int i, int i2, Bitmap.Config config) {
        return a.A(Bitmap.createBitmap(i, i2, config), yo3.b());
    }
}
