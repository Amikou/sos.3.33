package defpackage;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;

/* renamed from: oi  reason: default package */
/* loaded from: classes2.dex */
public interface oi {
    PublicKey a(jv3 jv3Var) throws IOException;

    PrivateKey b(zu2 zu2Var) throws IOException;
}
