package defpackage;

import android.app.Application;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.model.MyTokenType;

/* compiled from: SelectedChainViewModel.kt */
/* renamed from: sj3  reason: default package */
/* loaded from: classes2.dex */
public final class sj3 extends gd {
    public final gb2<List<MyTokenType>> b;
    public final gb2<String> c;
    public final g72<List<MyTokenType>> d;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public sj3(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>();
        this.c = new gb2<>("");
        final g72<List<MyTokenType>> g72Var = new g72<>();
        g72Var.a(i(), new tl2() { // from class: qj3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                sj3.d(g72.this, this, (String) obj);
            }
        });
        g72Var.a(g(), new tl2() { // from class: rj3
            @Override // defpackage.tl2
            public final void onChanged(Object obj) {
                sj3.e(g72.this, this, (List) obj);
            }
        });
        f(g72Var, this);
        te4 te4Var = te4.a;
        this.d = g72Var;
    }

    public static final void d(g72 g72Var, sj3 sj3Var, String str) {
        fs1.f(g72Var, "$this_apply");
        fs1.f(sj3Var, "this$0");
        f(g72Var, sj3Var);
    }

    public static final void e(g72 g72Var, sj3 sj3Var, List list) {
        fs1.f(g72Var, "$this_apply");
        fs1.f(sj3Var, "this$0");
        f(g72Var, sj3Var);
    }

    public static final void f(g72<List<MyTokenType>> g72Var, sj3 sj3Var) {
        List<MyTokenType> value = sj3Var.b.getValue();
        ArrayList arrayList = null;
        if (value != null) {
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : value) {
                MyTokenType myTokenType = (MyTokenType) obj;
                String value2 = sj3Var.i().getValue();
                if (value2 == null) {
                    value2 = "";
                }
                boolean z = true;
                if (!(value2.length() == 0)) {
                    String chainTitle = myTokenType.getTokenType().getChainTitle();
                    Objects.requireNonNull(chainTitle, "null cannot be cast to non-null type java.lang.String");
                    Locale locale = Locale.ROOT;
                    String lowerCase = chainTitle.toLowerCase(locale);
                    fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                    String value3 = sj3Var.i().getValue();
                    if (value3 == null) {
                        value3 = "";
                    }
                    if (!StringsKt__StringsKt.M(lowerCase, value3, false, 2, null)) {
                        String displayName = myTokenType.getTokenType().getDisplayName();
                        Objects.requireNonNull(displayName, "null cannot be cast to non-null type java.lang.String");
                        String lowerCase2 = displayName.toLowerCase(locale);
                        fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                        String value4 = sj3Var.i().getValue();
                        if (!StringsKt__StringsKt.M(lowerCase2, value4 != null ? value4 : "", false, 2, null)) {
                            z = false;
                        }
                    }
                }
                if (z) {
                    arrayList2.add(obj);
                }
            }
            arrayList = arrayList2;
        }
        g72Var.postValue(arrayList);
    }

    public final gb2<List<MyTokenType>> g() {
        return this.b;
    }

    public final g72<List<MyTokenType>> h() {
        return this.d;
    }

    public final gb2<String> i() {
        return this.c;
    }
}
