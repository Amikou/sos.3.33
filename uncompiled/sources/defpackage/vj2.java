package defpackage;

import com.onesignal.influence.domain.OSInfluenceChannel;
import com.onesignal.influence.domain.OSInfluenceType;
import com.onesignal.influence.domain.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessageTracker.kt */
/* renamed from: vj2  reason: default package */
/* loaded from: classes2.dex */
public final class vj2 extends mj2 {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public vj2(xj2 xj2Var, yj2 yj2Var, zk2 zk2Var) {
        super(xj2Var, yj2Var, zk2Var);
        fs1.f(xj2Var, "dataRepository");
        fs1.f(yj2Var, "logger");
        fs1.f(zk2Var, "timeProvider");
    }

    @Override // defpackage.mj2
    public void a(JSONObject jSONObject, a aVar) {
        fs1.f(jSONObject, "jsonObject");
        fs1.f(aVar, "influence");
    }

    @Override // defpackage.mj2
    public void b() {
        OSInfluenceType k = k();
        if (k == null) {
            k = OSInfluenceType.UNATTRIBUTED;
        }
        xj2 f = f();
        if (k == OSInfluenceType.DIRECT) {
            k = OSInfluenceType.INDIRECT;
        }
        f.a(k);
    }

    @Override // defpackage.mj2
    public int c() {
        return f().g();
    }

    @Override // defpackage.mj2
    public OSInfluenceChannel d() {
        return OSInfluenceChannel.IAM;
    }

    @Override // defpackage.mj2
    public String h() {
        return "iam_id";
    }

    @Override // defpackage.mj2
    public int i() {
        return f().f();
    }

    @Override // defpackage.mj2
    public JSONArray l() throws JSONException {
        return f().h();
    }

    @Override // defpackage.mj2
    public JSONArray m(String str) {
        try {
            JSONArray l = l();
            try {
                JSONArray jSONArray = new JSONArray();
                int length = l.length();
                for (int i = 0; i < length; i++) {
                    if (!fs1.b(str, l.getJSONObject(i).getString(h()))) {
                        jSONArray.put(l.getJSONObject(i));
                    }
                }
                return jSONArray;
            } catch (JSONException e) {
                o().error("Generating tracker lastChannelObjectReceived get JSONObject ", e);
                return l;
            }
        } catch (JSONException e2) {
            o().error("Generating IAM tracker getLastChannelObjects JSONObject ", e2);
            return new JSONArray();
        }
    }

    @Override // defpackage.mj2
    public void p() {
        OSInfluenceType e = f().e();
        if (e.isIndirect()) {
            x(n());
        }
        te4 te4Var = te4.a;
        y(e);
        yj2 o = o();
        o.debug("OneSignal InAppMessageTracker initInfluencedTypeFromCache: " + this);
    }

    @Override // defpackage.mj2
    public void u(JSONArray jSONArray) {
        fs1.f(jSONArray, "channelObjects");
        f().p(jSONArray);
    }
}
