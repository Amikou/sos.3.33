package defpackage;

/* compiled from: ThumbnailSizeChecker.java */
/* renamed from: u54  reason: default package */
/* loaded from: classes.dex */
public final class u54 {
    public static int a(int i) {
        return (int) (i * 1.3333334f);
    }

    public static boolean b(int i, int i2, p73 p73Var) {
        return ((float) a(i)) >= 2048.0f && a(i2) >= 2048;
    }

    public static boolean c(zu0 zu0Var, p73 p73Var) {
        if (zu0Var == null) {
            return false;
        }
        int q = zu0Var.q();
        if (q != 90 && q != 270) {
            return b(zu0Var.v(), zu0Var.j(), p73Var);
        }
        return b(zu0Var.j(), zu0Var.v(), p73Var);
    }
}
