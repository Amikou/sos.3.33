package defpackage;

import com.facebook.cache.common.CacheEventListener;
import com.facebook.cache.common.a;
import java.io.IOException;

/* compiled from: SettableCacheEvent.java */
/* renamed from: um3  reason: default package */
/* loaded from: classes.dex */
public class um3 implements a {
    public static final Object c = new Object();
    public static um3 d;
    public static int e;
    public wt a;
    public um3 b;

    public static um3 a() {
        synchronized (c) {
            um3 um3Var = d;
            if (um3Var != null) {
                d = um3Var.b;
                um3Var.b = null;
                e--;
                return um3Var;
            }
            return new um3();
        }
    }

    public void b() {
        synchronized (c) {
            if (e < 5) {
                c();
                e++;
                um3 um3Var = d;
                if (um3Var != null) {
                    this.b = um3Var;
                }
                d = this;
            }
        }
    }

    public final void c() {
    }

    public um3 d(wt wtVar) {
        this.a = wtVar;
        return this;
    }

    public um3 e(long j) {
        return this;
    }

    public um3 f(long j) {
        return this;
    }

    public um3 g(CacheEventListener.EvictionReason evictionReason) {
        return this;
    }

    public um3 h(IOException iOException) {
        return this;
    }

    public um3 i(long j) {
        return this;
    }

    public um3 j(String str) {
        return this;
    }
}
