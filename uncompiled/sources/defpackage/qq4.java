package defpackage;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* compiled from: WorkQueue.kt */
/* renamed from: qq4  reason: default package */
/* loaded from: classes2.dex */
public final class qq4 {
    public static final /* synthetic */ AtomicReferenceFieldUpdater b = AtomicReferenceFieldUpdater.newUpdater(qq4.class, Object.class, "lastScheduledTask");
    public static final /* synthetic */ AtomicIntegerFieldUpdater c = AtomicIntegerFieldUpdater.newUpdater(qq4.class, "producerIndex");
    public static final /* synthetic */ AtomicIntegerFieldUpdater d = AtomicIntegerFieldUpdater.newUpdater(qq4.class, "consumerIndex");
    public static final /* synthetic */ AtomicIntegerFieldUpdater e = AtomicIntegerFieldUpdater.newUpdater(qq4.class, "blockingTasksInBuffer");
    public final AtomicReferenceArray<k34> a = new AtomicReferenceArray<>(128);
    private volatile /* synthetic */ Object lastScheduledTask = null;
    private volatile /* synthetic */ int producerIndex = 0;
    private volatile /* synthetic */ int consumerIndex = 0;
    private volatile /* synthetic */ int blockingTasksInBuffer = 0;

    public static /* synthetic */ k34 b(qq4 qq4Var, k34 k34Var, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return qq4Var.a(k34Var, z);
    }

    public final k34 a(k34 k34Var, boolean z) {
        if (z) {
            return c(k34Var);
        }
        k34 k34Var2 = (k34) b.getAndSet(this, k34Var);
        if (k34Var2 == null) {
            return null;
        }
        return c(k34Var2);
    }

    public final k34 c(k34 k34Var) {
        if (k34Var.f0.e() == 1) {
            e.incrementAndGet(this);
        }
        if (e() == 127) {
            return k34Var;
        }
        int i = this.producerIndex & 127;
        while (this.a.get(i) != null) {
            Thread.yield();
        }
        this.a.lazySet(i, k34Var);
        c.incrementAndGet(this);
        return null;
    }

    public final void d(k34 k34Var) {
        if (k34Var != null) {
            if (k34Var.f0.e() == 1) {
                int decrementAndGet = e.decrementAndGet(this);
                if (ze0.a()) {
                    if (!(decrementAndGet >= 0)) {
                        throw new AssertionError();
                    }
                }
            }
        }
    }

    public final int e() {
        return this.producerIndex - this.consumerIndex;
    }

    public final int f() {
        return this.lastScheduledTask != null ? e() + 1 : e();
    }

    public final void g(pg1 pg1Var) {
        k34 k34Var = (k34) b.getAndSet(this, null);
        if (k34Var != null) {
            pg1Var.a(k34Var);
        }
        do {
        } while (j(pg1Var));
    }

    public final k34 h() {
        k34 k34Var = (k34) b.getAndSet(this, null);
        return k34Var == null ? i() : k34Var;
    }

    public final k34 i() {
        k34 andSet;
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & 127;
            if (d.compareAndSet(this, i, i + 1) && (andSet = this.a.getAndSet(i2, null)) != null) {
                d(andSet);
                return andSet;
            }
        }
    }

    public final boolean j(pg1 pg1Var) {
        k34 i = i();
        if (i == null) {
            return false;
        }
        pg1Var.a(i);
        return true;
    }

    public final long k(qq4 qq4Var) {
        if (ze0.a()) {
            if (!(e() == 0)) {
                throw new AssertionError();
            }
        }
        int i = qq4Var.producerIndex;
        AtomicReferenceArray<k34> atomicReferenceArray = qq4Var.a;
        for (int i2 = qq4Var.consumerIndex; i2 != i; i2++) {
            int i3 = i2 & 127;
            if (qq4Var.blockingTasksInBuffer == 0) {
                break;
            }
            k34 k34Var = atomicReferenceArray.get(i3);
            if (k34Var != null) {
                if ((k34Var.f0.e() == 1) && atomicReferenceArray.compareAndSet(i3, k34Var, null)) {
                    e.decrementAndGet(qq4Var);
                    b(this, k34Var, false, 2, null);
                    return -1L;
                }
            }
        }
        return m(qq4Var, true);
    }

    public final long l(qq4 qq4Var) {
        if (ze0.a()) {
            if (!(e() == 0)) {
                throw new AssertionError();
            }
        }
        k34 i = qq4Var.i();
        if (i != null) {
            k34 b2 = b(this, i, false, 2, null);
            if (ze0.a()) {
                if (b2 == null) {
                    return -1L;
                }
                throw new AssertionError();
            }
            return -1L;
        }
        return m(qq4Var, false);
    }

    public final long m(qq4 qq4Var, boolean z) {
        k34 k34Var;
        do {
            k34Var = (k34) qq4Var.lastScheduledTask;
            if (k34Var == null) {
                return -2L;
            }
            if (z) {
                if (!(k34Var.f0.e() == 1)) {
                    return -2L;
                }
            }
            long a = w34.e.a() - k34Var.a;
            long j = w34.a;
            if (a < j) {
                return j - a;
            }
        } while (!b.compareAndSet(qq4Var, k34Var, null));
        b(this, k34Var, false, 2, null);
        return -1L;
    }
}
