package defpackage;

import android.graphics.Bitmap;
import android.graphics.Rect;
import com.facebook.imagepipeline.image.a;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: ExperimentalBitmapAnimationDrawableFactory.java */
/* renamed from: y01  reason: default package */
/* loaded from: classes.dex */
public class y01 implements uq0 {
    public final od a;
    public final ScheduledExecutorService b;
    public final ExecutorService c;
    public final o92 d;
    public final br2 e;
    public final i90<wt, a> f;
    public final fw3<Integer> g;
    public final fw3<Integer> h;
    public final fw3<Boolean> i;

    public y01(od odVar, ScheduledExecutorService scheduledExecutorService, ExecutorService executorService, o92 o92Var, br2 br2Var, i90<wt, a> i90Var, fw3<Integer> fw3Var, fw3<Integer> fw3Var2, fw3<Boolean> fw3Var3) {
        this.a = odVar;
        this.b = scheduledExecutorService;
        this.c = executorService;
        this.d = o92Var;
        this.e = br2Var;
        this.f = i90Var;
        this.g = fw3Var;
        this.h = fw3Var2;
        this.i = fw3Var3;
    }

    @Override // defpackage.uq0
    public boolean a(a aVar) {
        return aVar instanceof vz;
    }

    public final kd c(yd ydVar) {
        td d = ydVar.d();
        return this.a.a(ydVar, new Rect(0, 0, d.getWidth(), d.getHeight()));
    }

    public final sd d(yd ydVar) {
        return new sd(new ie(ydVar.hashCode(), this.i.get().booleanValue()), this.f);
    }

    public final de e(yd ydVar, Bitmap.Config config) {
        e61 e61Var;
        zp zpVar;
        kd c = c(ydVar);
        xp f = f(ydVar);
        md mdVar = new md(f, c);
        int intValue = this.h.get().intValue();
        if (intValue > 0) {
            e61 e61Var2 = new e61(intValue);
            zpVar = g(mdVar, config);
            e61Var = e61Var2;
        } else {
            e61Var = null;
            zpVar = null;
        }
        return fe.o(new pp(this.e, f, new ld(c), mdVar, e61Var, zpVar), this.d, this.b);
    }

    public final xp f(yd ydVar) {
        int intValue = this.g.get().intValue();
        if (intValue != 1) {
            if (intValue != 2) {
                if (intValue != 3) {
                    return new kg2();
                }
                return new ex1();
            }
            return new lc1(d(ydVar), false);
        }
        return new lc1(d(ydVar), true);
    }

    public final zp g(aq aqVar, Bitmap.Config config) {
        br2 br2Var = this.e;
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        return new ci0(br2Var, aqVar, config, this.c);
    }

    @Override // defpackage.uq0
    /* renamed from: h */
    public jd b(a aVar) {
        vz vzVar = (vz) aVar;
        td f = vzVar.f();
        return new jd(e((yd) xt2.g(vzVar.g()), f != null ? f.f() : null));
    }
}
