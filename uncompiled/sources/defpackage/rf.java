package defpackage;

import android.content.Context;
import android.content.ContextWrapper;
import net.safemoon.androidwallet.EasyAppInitializer;

/* compiled from: AppInitializer.kt */
/* renamed from: rf  reason: default package */
/* loaded from: classes2.dex */
public class rf extends ContextWrapper {
    public static final a a = new a(null);

    /* compiled from: AppInitializer.kt */
    /* renamed from: rf$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final EasyAppInitializer a(Context context) {
            fs1.f(context, "context");
            return new EasyAppInitializer(context);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public rf(Context context) {
        super(context);
        fs1.f(context, "context");
    }
}
