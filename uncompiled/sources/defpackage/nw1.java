package defpackage;

import java.util.Objects;
import org.web3j.abi.datatypes.Int;

/* compiled from: JvmClassMapping.kt */
/* renamed from: nw1  reason: default package */
/* loaded from: classes2.dex */
public final class nw1 {
    public static final <T> Class<T> a(qw1<T> qw1Var) {
        fs1.f(qw1Var, "$this$java");
        Class<T> cls = (Class<T>) ((az) qw1Var).a();
        Objects.requireNonNull(cls, "null cannot be cast to non-null type java.lang.Class<T>");
        return cls;
    }

    public static final <T> Class<T> b(qw1<T> qw1Var) {
        fs1.f(qw1Var, "$this$javaObjectType");
        Class<T> cls = (Class<T>) ((az) qw1Var).a();
        if (cls.isPrimitive()) {
            String name = cls.getName();
            switch (name.hashCode()) {
                case -1325958191:
                    return name.equals("double") ? Double.class : cls;
                case 104431:
                    return name.equals(Int.TYPE_NAME) ? Integer.class : cls;
                case 3039496:
                    return name.equals("byte") ? Byte.class : cls;
                case 3052374:
                    return name.equals("char") ? Character.class : cls;
                case 3327612:
                    return name.equals("long") ? Long.class : cls;
                case 3625364:
                    return name.equals("void") ? Void.class : cls;
                case 64711720:
                    return name.equals("boolean") ? Boolean.class : cls;
                case 97526364:
                    return name.equals("float") ? Float.class : cls;
                case 109413500:
                    return name.equals("short") ? Short.class : cls;
                default:
                    return cls;
            }
        }
        return cls;
    }

    public static final <T> qw1<T> c(Class<T> cls) {
        fs1.f(cls, "$this$kotlin");
        return d53.b(cls);
    }
}
