package defpackage;

import android.content.Context;

/* compiled from: BatteryNotLowController.java */
/* renamed from: po  reason: default package */
/* loaded from: classes.dex */
public class po extends d60<Boolean> {
    public po(Context context, q34 q34Var) {
        super(j84.c(context, q34Var).b());
    }

    @Override // defpackage.d60
    public boolean b(tq4 tq4Var) {
        return tq4Var.j.f();
    }

    @Override // defpackage.d60
    /* renamed from: i */
    public boolean c(Boolean bool) {
        return !bool.booleanValue();
    }
}
