package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: zg5  reason: default package */
/* loaded from: classes.dex */
public final class zg5 implements Iterator<Map.Entry<K, V>> {
    public int a;
    public boolean f0;
    public Iterator<Map.Entry<K, V>> g0;
    public final /* synthetic */ fg5 h0;

    public zg5(fg5 fg5Var) {
        this.h0 = fg5Var;
        this.a = -1;
    }

    public /* synthetic */ zg5(fg5 fg5Var, hg5 hg5Var) {
        this(fg5Var);
    }

    public final Iterator<Map.Entry<K, V>> a() {
        Map map;
        if (this.g0 == null) {
            map = this.h0.g0;
            this.g0 = map.entrySet().iterator();
        }
        return this.g0;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        List list;
        Map map;
        int i = this.a + 1;
        list = this.h0.f0;
        if (i >= list.size()) {
            map = this.h0.g0;
            if (map.isEmpty() || !a().hasNext()) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        List list;
        Object next;
        List list2;
        this.f0 = true;
        int i = this.a + 1;
        this.a = i;
        list = this.h0.f0;
        if (i < list.size()) {
            list2 = this.h0.f0;
            next = list2.get(this.a);
        } else {
            next = a().next();
        }
        return (Map.Entry) next;
    }

    @Override // java.util.Iterator
    public final void remove() {
        List list;
        if (!this.f0) {
            throw new IllegalStateException("remove() was called before next()");
        }
        this.f0 = false;
        this.h0.o();
        int i = this.a;
        list = this.h0.f0;
        if (i >= list.size()) {
            a().remove();
            return;
        }
        fg5 fg5Var = this.h0;
        int i2 = this.a;
        this.a = i2 - 1;
        fg5Var.h(i2);
    }
}
