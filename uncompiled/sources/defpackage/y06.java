package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: y06  reason: default package */
/* loaded from: classes.dex */
public final class y06 implements x06 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.upload.file_truncate_fix", false);

    @Override // defpackage.x06
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
