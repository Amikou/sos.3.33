package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.rd.draw.data.Orientation;

/* compiled from: SlideDrawer.java */
/* renamed from: iq3  reason: default package */
/* loaded from: classes2.dex */
public class iq3 extends hn {
    public iq3(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2) {
        if (wg4Var instanceof hq3) {
            int a = ((hq3) wg4Var).a();
            int t = this.b.t();
            int p = this.b.p();
            int m = this.b.m();
            this.a.setColor(t);
            float f = i;
            float f2 = i2;
            float f3 = m;
            canvas.drawCircle(f, f2, f3, this.a);
            this.a.setColor(p);
            if (this.b.g() == Orientation.HORIZONTAL) {
                canvas.drawCircle(a, f2, f3, this.a);
            } else {
                canvas.drawCircle(f, a, f3, this.a);
            }
        }
    }
}
