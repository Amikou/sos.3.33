package defpackage;

import com.google.android.gms.internal.measurement.z1;
import com.google.android.gms.internal.measurement.zzjb;
import com.google.android.gms.internal.measurement.zzjd;
import com.google.android.gms.internal.measurement.zzkn;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: iw5  reason: default package */
/* loaded from: classes.dex */
public class iw5 {
    public volatile z1 a;
    public volatile zzjd b;

    static {
        qt5.a();
    }

    public final int a() {
        if (this.b != null) {
            return ((zzjb) this.b).zza.length;
        }
        if (this.a != null) {
            return this.a.e();
        }
        return 0;
    }

    public final zzjd b() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                return this.b;
            }
            if (this.a == null) {
                this.b = zzjd.zzb;
            } else {
                this.b = this.a.d();
            }
            return this.b;
        }
    }

    public final void c(z1 z1Var) {
        if (this.a != null) {
            return;
        }
        synchronized (this) {
            if (this.a == null) {
                try {
                    this.a = z1Var;
                    this.b = zzjd.zzb;
                } catch (zzkn unused) {
                    this.a = z1Var;
                    this.b = zzjd.zzb;
                }
            }
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof iw5) {
            iw5 iw5Var = (iw5) obj;
            z1 z1Var = this.a;
            z1 z1Var2 = iw5Var.a;
            if (z1Var == null && z1Var2 == null) {
                return b().equals(iw5Var.b());
            }
            if (z1Var == null || z1Var2 == null) {
                if (z1Var != null) {
                    iw5Var.c(z1Var.g());
                    return z1Var.equals(iw5Var.a);
                }
                c(z1Var2.g());
                return this.a.equals(z1Var2);
            }
            return z1Var.equals(z1Var2);
        }
        return false;
    }

    public int hashCode() {
        return 1;
    }
}
