package defpackage;

import java.util.concurrent.Executor;

/* compiled from: ExperimentalThreadHandoffProducerQueueImpl.java */
/* renamed from: b11  reason: default package */
/* loaded from: classes.dex */
public class b11 implements h54 {
    public final Executor a;

    public b11(Executor executor) {
        this.a = (Executor) xt2.g(executor);
    }

    @Override // defpackage.h54
    public void a(Runnable runnable) {
    }

    @Override // defpackage.h54
    public void b(Runnable runnable) {
        this.a.execute(runnable);
    }
}
