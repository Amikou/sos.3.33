package defpackage;

import android.util.SparseIntArray;

/* compiled from: DefaultBitmapPoolParams.java */
/* renamed from: ei0  reason: default package */
/* loaded from: classes.dex */
public class ei0 {
    public static final SparseIntArray a = new SparseIntArray(0);

    public static ys2 a() {
        return new ys2(0, b(), a);
    }

    public static int b() {
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min > 16777216) {
            return (min / 4) * 3;
        }
        return min / 2;
    }
}
