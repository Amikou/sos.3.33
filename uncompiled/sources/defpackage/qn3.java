package defpackage;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Build;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ShapeAppearancePathProvider.java */
/* renamed from: qn3  reason: default package */
/* loaded from: classes2.dex */
public class qn3 {
    public final rn3[] a = new rn3[4];
    public final Matrix[] b = new Matrix[4];
    public final Matrix[] c = new Matrix[4];
    public final PointF d = new PointF();
    public final Path e = new Path();
    public final Path f = new Path();
    public final rn3 g = new rn3();
    public final float[] h = new float[2];
    public final float[] i = new float[2];
    public final Path j = new Path();
    public final Path k = new Path();
    public boolean l = true;

    /* compiled from: ShapeAppearancePathProvider.java */
    /* renamed from: qn3$a */
    /* loaded from: classes2.dex */
    public static class a {
        public static final qn3 a = new qn3();
    }

    /* compiled from: ShapeAppearancePathProvider.java */
    /* renamed from: qn3$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a(rn3 rn3Var, Matrix matrix, int i);

        void b(rn3 rn3Var, Matrix matrix, int i);
    }

    /* compiled from: ShapeAppearancePathProvider.java */
    /* renamed from: qn3$c */
    /* loaded from: classes2.dex */
    public static final class c {
        public final pn3 a;
        public final Path b;
        public final RectF c;
        public final b d;
        public final float e;

        public c(pn3 pn3Var, float f, RectF rectF, b bVar, Path path) {
            this.d = bVar;
            this.a = pn3Var;
            this.e = f;
            this.c = rectF;
            this.b = path;
        }
    }

    public qn3() {
        for (int i = 0; i < 4; i++) {
            this.a[i] = new rn3();
            this.b[i] = new Matrix();
            this.c[i] = new Matrix();
        }
    }

    public static qn3 k() {
        return a.a;
    }

    public final float a(int i) {
        return (i + 1) * 90;
    }

    public final void b(c cVar, int i) {
        this.h[0] = this.a[i].k();
        this.h[1] = this.a[i].l();
        this.b[i].mapPoints(this.h);
        if (i == 0) {
            Path path = cVar.b;
            float[] fArr = this.h;
            path.moveTo(fArr[0], fArr[1]);
        } else {
            Path path2 = cVar.b;
            float[] fArr2 = this.h;
            path2.lineTo(fArr2[0], fArr2[1]);
        }
        this.a[i].d(this.b[i], cVar.b);
        b bVar = cVar.d;
        if (bVar != null) {
            bVar.a(this.a[i], this.b[i], i);
        }
    }

    public final void c(c cVar, int i) {
        int i2 = (i + 1) % 4;
        this.h[0] = this.a[i].i();
        this.h[1] = this.a[i].j();
        this.b[i].mapPoints(this.h);
        this.i[0] = this.a[i2].k();
        this.i[1] = this.a[i2].l();
        this.b[i2].mapPoints(this.i);
        float[] fArr = this.h;
        float f = fArr[0];
        float[] fArr2 = this.i;
        float max = Math.max(((float) Math.hypot(f - fArr2[0], fArr[1] - fArr2[1])) - 0.001f, (float) Utils.FLOAT_EPSILON);
        float i3 = i(cVar.c, i);
        this.g.n(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
        cu0 j = j(i, cVar.a);
        j.b(max, i3, cVar.e, this.g);
        this.j.reset();
        this.g.d(this.c[i], this.j);
        if (this.l && Build.VERSION.SDK_INT >= 19 && (j.a() || l(this.j, i) || l(this.j, i2))) {
            Path path = this.j;
            path.op(path, this.f, Path.Op.DIFFERENCE);
            this.h[0] = this.g.k();
            this.h[1] = this.g.l();
            this.c[i].mapPoints(this.h);
            Path path2 = this.e;
            float[] fArr3 = this.h;
            path2.moveTo(fArr3[0], fArr3[1]);
            this.g.d(this.c[i], this.e);
        } else {
            this.g.d(this.c[i], cVar.b);
        }
        b bVar = cVar.d;
        if (bVar != null) {
            bVar.b(this.g, this.c[i], i);
        }
    }

    public void d(pn3 pn3Var, float f, RectF rectF, b bVar, Path path) {
        path.rewind();
        this.e.rewind();
        this.f.rewind();
        this.f.addRect(rectF, Path.Direction.CW);
        c cVar = new c(pn3Var, f, rectF, bVar, path);
        for (int i = 0; i < 4; i++) {
            m(cVar, i);
            n(i);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            b(cVar, i2);
            c(cVar, i2);
        }
        path.close();
        this.e.close();
        if (Build.VERSION.SDK_INT < 19 || this.e.isEmpty()) {
            return;
        }
        path.op(this.e, Path.Op.UNION);
    }

    public void e(pn3 pn3Var, float f, RectF rectF, Path path) {
        d(pn3Var, f, rectF, null, path);
    }

    public final void f(int i, RectF rectF, PointF pointF) {
        if (i == 1) {
            pointF.set(rectF.right, rectF.bottom);
        } else if (i == 2) {
            pointF.set(rectF.left, rectF.bottom);
        } else if (i != 3) {
            pointF.set(rectF.right, rectF.top);
        } else {
            pointF.set(rectF.left, rectF.top);
        }
    }

    public final v80 g(int i, pn3 pn3Var) {
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    return pn3Var.t();
                }
                return pn3Var.r();
            }
            return pn3Var.j();
        }
        return pn3Var.l();
    }

    public final w80 h(int i, pn3 pn3Var) {
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    return pn3Var.s();
                }
                return pn3Var.q();
            }
            return pn3Var.i();
        }
        return pn3Var.k();
    }

    public final float i(RectF rectF, int i) {
        float[] fArr = this.h;
        rn3[] rn3VarArr = this.a;
        fArr[0] = rn3VarArr[i].c;
        fArr[1] = rn3VarArr[i].d;
        this.b[i].mapPoints(fArr);
        if (i != 1 && i != 3) {
            return Math.abs(rectF.centerY() - this.h[1]);
        }
        return Math.abs(rectF.centerX() - this.h[0]);
    }

    public final cu0 j(int i, pn3 pn3Var) {
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    return pn3Var.o();
                }
                return pn3Var.p();
            }
            return pn3Var.n();
        }
        return pn3Var.h();
    }

    public final boolean l(Path path, int i) {
        this.k.reset();
        this.a[i].d(this.b[i], this.k);
        RectF rectF = new RectF();
        path.computeBounds(rectF, true);
        this.k.computeBounds(rectF, true);
        path.op(this.k, Path.Op.INTERSECT);
        path.computeBounds(rectF, true);
        if (rectF.isEmpty()) {
            return rectF.width() > 1.0f && rectF.height() > 1.0f;
        }
        return true;
    }

    public final void m(c cVar, int i) {
        h(i, cVar.a).b(this.a[i], 90.0f, cVar.e, cVar.c, g(i, cVar.a));
        float a2 = a(i);
        this.b[i].reset();
        f(i, cVar.c, this.d);
        Matrix matrix = this.b[i];
        PointF pointF = this.d;
        matrix.setTranslate(pointF.x, pointF.y);
        this.b[i].preRotate(a2);
    }

    public final void n(int i) {
        this.h[0] = this.a[i].i();
        this.h[1] = this.a[i].j();
        this.b[i].mapPoints(this.h);
        float a2 = a(i);
        this.c[i].reset();
        Matrix matrix = this.c[i];
        float[] fArr = this.h;
        matrix.setTranslate(fArr[0], fArr[1]);
        this.c[i].preRotate(a2);
    }
}
