package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: vt5  reason: default package */
/* loaded from: classes.dex */
public final class vt5 implements Runnable {
    public final /* synthetic */ long a;
    public final /* synthetic */ qu5 f0;

    public vt5(qu5 qu5Var, long j) {
        this.f0 = qu5Var;
        this.a = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        qu5.l(this.f0, this.a);
    }
}
