package defpackage;

import java.util.List;

/* compiled from: PgsSubtitle.java */
/* renamed from: rq2  reason: default package */
/* loaded from: classes.dex */
public final class rq2 implements qv3 {
    public final List<kb0> a;

    public rq2(List<kb0> list) {
        this.a = list;
    }

    @Override // defpackage.qv3
    public int a(long j) {
        return -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        return 0L;
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return this.a;
    }

    @Override // defpackage.qv3
    public int f() {
        return 1;
    }
}
