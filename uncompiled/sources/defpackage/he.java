package defpackage;

import android.os.Build;
import android.view.View;

/* compiled from: AnimationEngine.java */
/* renamed from: he  reason: default package */
/* loaded from: classes.dex */
public abstract class he implements Runnable {
    public final View a;
    public final m91 f0;

    public he(View view) {
        this.a = view;
        this.f0 = ef1.b() ? new m91() : null;
    }

    public abstract boolean a();

    public final void b() {
        this.a.removeCallbacks(this);
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.postOnAnimationDelayed(this, 10L);
        } else {
            this.a.postDelayed(this, 10L);
        }
    }

    public void c() {
        m91 m91Var = this.f0;
        if (m91Var != null) {
            m91Var.a();
        }
        b();
    }

    @Override // java.lang.Runnable
    public final void run() {
        boolean a = a();
        m91 m91Var = this.f0;
        if (m91Var != null) {
            m91Var.b();
            if (!a) {
                this.f0.c();
            }
        }
        if (a) {
            b();
        }
    }
}
