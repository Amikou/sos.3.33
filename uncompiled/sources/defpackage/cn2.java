package defpackage;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import androidx.core.app.c;
import com.onesignal.a1;
import com.onesignal.m;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: OneSignalNotificationManager.java */
/* renamed from: cn2  reason: default package */
/* loaded from: classes2.dex */
public class cn2 {
    public static boolean a(Context context, String str) {
        if (c.d(context).a()) {
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = i(context).getNotificationChannel(str);
                return notificationChannel == null || notificationChannel.getImportance() != 0;
            }
            return true;
        }
        return false;
    }

    public static void b(Context context, ArrayList<StatusBarNotification> arrayList) {
        Iterator<StatusBarNotification> it = arrayList.iterator();
        while (it.hasNext()) {
            StatusBarNotification next = it.next();
            c.d(context).f(next.getId(), Notification.Builder.recoverBuilder(context, next.getNotification()).setGroup("os_group_undefined").setOnlyAlertOnce(true).build());
        }
    }

    public static ArrayList<StatusBarNotification> c(Context context) {
        StatusBarNotification[] d;
        ArrayList<StatusBarNotification> arrayList = new ArrayList<>();
        for (StatusBarNotification statusBarNotification : d(context)) {
            Notification notification = statusBarNotification.getNotification();
            boolean f = m.f(statusBarNotification);
            boolean z = notification.getGroup() == null || notification.getGroup().equals(g());
            if (!f && z) {
                arrayList.add(statusBarNotification);
            }
        }
        return arrayList;
    }

    public static StatusBarNotification[] d(Context context) {
        StatusBarNotification[] statusBarNotificationArr = new StatusBarNotification[0];
        try {
            return i(context).getActiveNotifications();
        } catch (Throwable unused) {
            return statusBarNotificationArr;
        }
    }

    public static Integer e(Context context) {
        StatusBarNotification[] d;
        int i = 0;
        for (StatusBarNotification statusBarNotification : d(context)) {
            if (!dh2.b(statusBarNotification.getNotification()) && "os_group_undefined".equals(statusBarNotification.getNotification().getGroup())) {
                i++;
            }
        }
        return Integer.valueOf(i);
    }

    public static int f() {
        return -718463522;
    }

    public static String g() {
        return "os_group_undefined";
    }

    public static Integer h(a1 a1Var, String str, boolean z) {
        String str2 = z ? "group_id IS NULL" : "group_id = ?";
        Cursor b = a1Var.b("notification", null, str2 + " AND dismissed = 0 AND opened = 0 AND is_summary = 0", z ? null : new String[]{str}, null, null, "created_time DESC", "1");
        if (!b.moveToFirst()) {
            b.close();
            return null;
        }
        Integer valueOf = Integer.valueOf(b.getInt(b.getColumnIndex("android_notification_id")));
        b.close();
        return valueOf;
    }

    public static NotificationManager i(Context context) {
        return (NotificationManager) context.getSystemService("notification");
    }
}
