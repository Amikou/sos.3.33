package defpackage;

import android.content.Context;
import java.util.LinkedHashMap;
import java.util.Map;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: GetTokenTypeMapUseCase.kt */
/* renamed from: mf1  reason: default package */
/* loaded from: classes2.dex */
public final class mf1 implements em1 {
    public final Context a;

    public mf1(Context context) {
        fs1.f(context, "context");
        this.a = context;
    }

    @Override // defpackage.em1
    public Map<String, TokenType> get() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Boolean bool = zr.b;
        fs1.e(bool, "IS_TEST_NET");
        if (bool.booleanValue()) {
            String string = this.a.getString(R.string.my_tokens_screen_select_bep);
            fs1.e(string, "context.getString(R.stri…tokens_screen_select_bep)");
            linkedHashMap.put(string, TokenType.BEP_20_TEST);
            String string2 = this.a.getString(R.string.my_tokens_screen_select_erc);
            fs1.e(string2, "context.getString(R.stri…tokens_screen_select_erc)");
            linkedHashMap.put(string2, TokenType.ERC_20_TEST);
            String string3 = this.a.getString(R.string.my_tokens_screen_select_matic);
            fs1.e(string3, "context.getString(R.stri…kens_screen_select_matic)");
            linkedHashMap.put(string3, TokenType.POLYGON_TEST);
            String string4 = this.a.getString(R.string.my_tokens_screen_select_avalanche_c);
            fs1.e(string4, "context.getString(R.stri…creen_select_avalanche_c)");
            linkedHashMap.put(string4, TokenType.AVALANCHE_FUJI_TEST);
        } else {
            String string5 = this.a.getString(R.string.my_tokens_screen_select_bep);
            fs1.e(string5, "context.getString(R.stri…tokens_screen_select_bep)");
            linkedHashMap.put(string5, TokenType.BEP_20);
            String string6 = this.a.getString(R.string.my_tokens_screen_select_erc);
            fs1.e(string6, "context.getString(R.stri…tokens_screen_select_erc)");
            linkedHashMap.put(string6, TokenType.ERC_20);
            String string7 = this.a.getString(R.string.my_tokens_screen_select_matic);
            fs1.e(string7, "context.getString(R.stri…kens_screen_select_matic)");
            linkedHashMap.put(string7, TokenType.POLYGON);
            String string8 = this.a.getString(R.string.my_tokens_screen_select_avalanche_c);
            fs1.e(string8, "context.getString(R.stri…creen_select_avalanche_c)");
            linkedHashMap.put(string8, TokenType.AVALANCHE_C);
        }
        return linkedHashMap;
    }
}
