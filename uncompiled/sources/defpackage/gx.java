package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import androidx.constraintlayout.core.widgets.d;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ChainRun.java */
/* renamed from: gx  reason: default package */
/* loaded from: classes.dex */
public class gx extends WidgetRun {
    public ArrayList<WidgetRun> k;
    public int l;

    public gx(ConstraintWidget constraintWidget, int i) {
        super(constraintWidget);
        this.k = new ArrayList<>();
        this.f = i;
        q();
    }

    /* JADX WARN: Code restructure failed: missing block: B:271:0x0400, code lost:
        r7 = r7 - r10;
     */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x00e9  */
    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun, defpackage.im0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void a(defpackage.im0 r27) {
        /*
            Method dump skipped, instructions count: 1064
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.gx.a(im0):void");
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void d() {
        Iterator<WidgetRun> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().d();
        }
        int size = this.k.size();
        if (size < 1) {
            return;
        }
        ConstraintWidget constraintWidget = this.k.get(0).b;
        ConstraintWidget constraintWidget2 = this.k.get(size - 1).b;
        if (this.f == 0) {
            ConstraintAnchor constraintAnchor = constraintWidget.M;
            ConstraintAnchor constraintAnchor2 = constraintWidget2.O;
            DependencyNode i = i(constraintAnchor, 0);
            int f = constraintAnchor.f();
            ConstraintWidget r = r();
            if (r != null) {
                f = r.M.f();
            }
            if (i != null) {
                b(this.h, i, f);
            }
            DependencyNode i2 = i(constraintAnchor2, 0);
            int f2 = constraintAnchor2.f();
            ConstraintWidget s = s();
            if (s != null) {
                f2 = s.O.f();
            }
            if (i2 != null) {
                b(this.i, i2, -f2);
            }
        } else {
            ConstraintAnchor constraintAnchor3 = constraintWidget.N;
            ConstraintAnchor constraintAnchor4 = constraintWidget2.P;
            DependencyNode i3 = i(constraintAnchor3, 1);
            int f3 = constraintAnchor3.f();
            ConstraintWidget r2 = r();
            if (r2 != null) {
                f3 = r2.N.f();
            }
            if (i3 != null) {
                b(this.h, i3, f3);
            }
            DependencyNode i4 = i(constraintAnchor4, 1);
            int f4 = constraintAnchor4.f();
            ConstraintWidget s2 = s();
            if (s2 != null) {
                f4 = s2.P.f();
            }
            if (i4 != null) {
                b(this.i, i4, -f4);
            }
        }
        this.h.a = this;
        this.i.a = this;
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void e() {
        for (int i = 0; i < this.k.size(); i++) {
            this.k.get(i).e();
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void f() {
        this.c = null;
        Iterator<WidgetRun> it = this.k.iterator();
        while (it.hasNext()) {
            it.next().f();
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public long j() {
        int size = this.k.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            WidgetRun widgetRun = this.k.get(i);
            j = j + widgetRun.h.f + widgetRun.j() + widgetRun.i.f;
        }
        return j;
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public boolean m() {
        int size = this.k.size();
        for (int i = 0; i < size; i++) {
            if (!this.k.get(i).m()) {
                return false;
            }
        }
        return true;
    }

    public final void q() {
        ConstraintWidget constraintWidget;
        ConstraintWidget constraintWidget2 = this.b;
        ConstraintWidget N = constraintWidget2.N(this.f);
        while (true) {
            ConstraintWidget constraintWidget3 = N;
            constraintWidget = constraintWidget2;
            constraintWidget2 = constraintWidget3;
            if (constraintWidget2 == null) {
                break;
            }
            N = constraintWidget2.N(this.f);
        }
        this.b = constraintWidget;
        this.k.add(constraintWidget.P(this.f));
        ConstraintWidget L = constraintWidget.L(this.f);
        while (L != null) {
            this.k.add(L.P(this.f));
            L = L.L(this.f);
        }
        Iterator<WidgetRun> it = this.k.iterator();
        while (it.hasNext()) {
            WidgetRun next = it.next();
            int i = this.f;
            if (i == 0) {
                next.b.b = this;
            } else if (i == 1) {
                next.b.c = this;
            }
        }
        if ((this.f == 0 && ((d) this.b.M()).M1()) && this.k.size() > 1) {
            ArrayList<WidgetRun> arrayList = this.k;
            this.b = arrayList.get(arrayList.size() - 1).b;
        }
        this.l = this.f == 0 ? this.b.B() : this.b.R();
    }

    public final ConstraintWidget r() {
        for (int i = 0; i < this.k.size(); i++) {
            WidgetRun widgetRun = this.k.get(i);
            if (widgetRun.b.U() != 8) {
                return widgetRun.b;
            }
        }
        return null;
    }

    public final ConstraintWidget s() {
        for (int size = this.k.size() - 1; size >= 0; size--) {
            WidgetRun widgetRun = this.k.get(size);
            if (widgetRun.b.U() != 8) {
                return widgetRun.b;
            }
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ChainRun ");
        sb.append(this.f == 0 ? "horizontal : " : "vertical : ");
        Iterator<WidgetRun> it = this.k.iterator();
        while (it.hasNext()) {
            sb.append("<");
            sb.append(it.next());
            sb.append("> ");
        }
        return sb.toString();
    }
}
