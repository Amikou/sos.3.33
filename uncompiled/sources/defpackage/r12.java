package defpackage;

import android.content.Context;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.io.File;

/* compiled from: LogFileManager.java */
/* renamed from: r12  reason: default package */
/* loaded from: classes2.dex */
public class r12 {
    public static final c d = new c();
    public final Context a;
    public final b b;
    public o31 c;

    /* compiled from: LogFileManager.java */
    /* renamed from: r12$b */
    /* loaded from: classes2.dex */
    public interface b {
        File a();
    }

    /* compiled from: LogFileManager.java */
    /* renamed from: r12$c */
    /* loaded from: classes2.dex */
    public static final class c implements o31 {
        public c() {
        }

        @Override // defpackage.o31
        public void a() {
        }

        @Override // defpackage.o31
        public String b() {
            return null;
        }

        @Override // defpackage.o31
        public byte[] c() {
            return null;
        }

        @Override // defpackage.o31
        public void d() {
        }

        @Override // defpackage.o31
        public void e(long j, String str) {
        }
    }

    public r12(Context context, b bVar) {
        this(context, bVar, null);
    }

    public void a() {
        this.c.d();
    }

    public byte[] b() {
        return this.c.c();
    }

    public String c() {
        return this.c.b();
    }

    public final File d(String str) {
        return new File(this.b.a(), "crashlytics-userlog-" + str + ".temp");
    }

    public final void e(String str) {
        this.c.a();
        this.c = d;
        if (str == null) {
            return;
        }
        if (!CommonUtils.k(this.a, "com.crashlytics.CollectCustomLogs", true)) {
            w12.f().b("Preferences requested no custom logs. Aborting log file creation.");
        } else {
            f(d(str), 65536);
        }
    }

    public void f(File file, int i) {
        this.c = new ox2(file, i);
    }

    public void g(long j, String str) {
        this.c.e(j, str);
    }

    public r12(Context context, b bVar, String str) {
        this.a = context;
        this.b = bVar;
        this.c = d;
        e(str);
    }
}
