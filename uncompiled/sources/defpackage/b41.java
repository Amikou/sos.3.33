package defpackage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: FilePathComponents.kt */
/* renamed from: b41  reason: default package */
/* loaded from: classes2.dex */
public class b41 {
    public static final int a(String str) {
        int Y;
        int Y2 = StringsKt__StringsKt.Y(str, File.separatorChar, 0, false, 4, null);
        if (Y2 == 0) {
            if (str.length() > 1) {
                char charAt = str.charAt(1);
                char c = File.separatorChar;
                if (charAt == c && (Y = StringsKt__StringsKt.Y(str, c, 2, false, 4, null)) >= 0) {
                    int Y3 = StringsKt__StringsKt.Y(str, File.separatorChar, Y + 1, false, 4, null);
                    return Y3 >= 0 ? Y3 + 1 : str.length();
                }
            }
            return 1;
        } else if (Y2 <= 0 || str.charAt(Y2 - 1) != ':') {
            if (Y2 == -1 && StringsKt__StringsKt.P(str, ':', false, 2, null)) {
                return str.length();
            }
            return 0;
        } else {
            return Y2 + 1;
        }
    }

    public static final p31 b(File file) {
        List list;
        fs1.f(file, "$this$toComponents");
        String path = file.getPath();
        fs1.e(path, "path");
        int a = a(path);
        String substring = path.substring(0, a);
        fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        String substring2 = path.substring(a);
        fs1.e(substring2, "(this as java.lang.String).substring(startIndex)");
        if (substring2.length() == 0) {
            list = b20.g();
        } else {
            List<String> v0 = StringsKt__StringsKt.v0(substring2, new char[]{File.separatorChar}, false, 0, 6, null);
            ArrayList arrayList = new ArrayList(c20.q(v0, 10));
            for (String str : v0) {
                arrayList.add(new File(str));
            }
            list = arrayList;
        }
        return new p31(new File(substring), list);
    }
}
