package defpackage;

import android.database.Cursor;

/* compiled from: CloseHelper.java */
/* renamed from: uz  reason: default package */
/* loaded from: classes2.dex */
public class uz {
    public static void a(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return;
        }
        cursor.close();
    }
}
