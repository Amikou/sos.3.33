package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: ViewChooseStatsBinding.java */
/* renamed from: ci4  reason: default package */
/* loaded from: classes2.dex */
public final class ci4 {
    public final AppCompatTextView a;
    public final AppCompatTextView b;

    public ci4(MaterialCardView materialCardView, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2) {
        this.a = appCompatTextView;
        this.b = appCompatTextView2;
    }

    public static ci4 a(View view) {
        int i = R.id.txtCmcStats;
        AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtCmcStats);
        if (appCompatTextView != null) {
            i = R.id.txtSwapStats;
            AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txtSwapStats);
            if (appCompatTextView2 != null) {
                return new ci4((MaterialCardView) view, appCompatTextView, appCompatTextView2);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
