package defpackage;

import androidx.media3.common.util.b;
import java.util.Collections;
import java.util.PriorityQueue;

/* compiled from: PriorityTaskManager.java */
/* renamed from: xu2  reason: default package */
/* loaded from: classes.dex */
public final class xu2 {
    public final Object a = new Object();
    public final PriorityQueue<Integer> b = new PriorityQueue<>(10, Collections.reverseOrder());
    public int c = Integer.MIN_VALUE;

    public void a(int i) {
        synchronized (this.a) {
            this.b.add(Integer.valueOf(i));
            this.c = Math.max(this.c, i);
        }
    }

    public void b(int i) {
        synchronized (this.a) {
            this.b.remove(Integer.valueOf(i));
            this.c = this.b.isEmpty() ? Integer.MIN_VALUE : ((Integer) b.j(this.b.peek())).intValue();
            this.a.notifyAll();
        }
    }
}
