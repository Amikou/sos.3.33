package defpackage;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.d;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: h15  reason: default package */
/* loaded from: classes.dex */
public interface h15 {
    void a(ConnectionResult connectionResult);

    void c(d dVar, Set<Scope> set);
}
