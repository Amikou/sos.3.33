package defpackage;

import android.os.SystemClock;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: mi0  reason: default package */
/* loaded from: classes.dex */
public class mi0 implements rz {
    public static final mi0 a = new mi0();

    @RecentlyNonNull
    public static rz c() {
        return a;
    }

    @Override // defpackage.rz
    @RecentlyNonNull
    public long a() {
        return System.currentTimeMillis();
    }

    @Override // defpackage.rz
    @RecentlyNonNull
    public long b() {
        return SystemClock.elapsedRealtime();
    }

    @Override // defpackage.rz
    @RecentlyNonNull
    public long nanoTime() {
        return System.nanoTime();
    }
}
