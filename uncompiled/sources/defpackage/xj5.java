package defpackage;

import android.os.Process;
import com.google.android.gms.measurement.internal.h;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: xj5  reason: default package */
/* loaded from: classes.dex */
public final class xj5 extends Thread {
    public final Object a;
    public final BlockingQueue<vj5<?>> f0;
    public boolean g0 = false;
    public final /* synthetic */ h h0;

    public xj5(h hVar, String str, BlockingQueue<vj5<?>> blockingQueue) {
        this.h0 = hVar;
        zt2.j(str);
        zt2.j(blockingQueue);
        this.a = new Object();
        this.f0 = blockingQueue;
        setName(str);
    }

    public final void a() {
        synchronized (this.a) {
            this.a.notifyAll();
        }
    }

    public final void b() {
        Object obj;
        Semaphore semaphore;
        Object obj2;
        xj5 xj5Var;
        xj5 xj5Var2;
        obj = this.h0.i;
        synchronized (obj) {
            if (!this.g0) {
                semaphore = this.h0.j;
                semaphore.release();
                obj2 = this.h0.i;
                obj2.notifyAll();
                xj5Var = this.h0.c;
                if (this == xj5Var) {
                    this.h0.c = null;
                } else {
                    xj5Var2 = this.h0.d;
                    if (this == xj5Var2) {
                        this.h0.d = null;
                    } else {
                        this.h0.a.w().l().a("Current scheduler thread is neither worker nor network");
                    }
                }
                this.g0 = true;
            }
        }
    }

    public final void c(InterruptedException interruptedException) {
        this.h0.a.w().p().b(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public final void run() {
        Semaphore semaphore;
        Object obj;
        boolean z = false;
        while (!z) {
            try {
                semaphore = this.h0.j;
                semaphore.acquire();
                z = true;
            } catch (InterruptedException e) {
                c(e);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                vj5<?> poll = this.f0.poll();
                if (poll != null) {
                    Process.setThreadPriority(true != poll.f0 ? 10 : threadPriority);
                    poll.run();
                } else {
                    synchronized (this.a) {
                        if (this.f0.peek() == null) {
                            boolean unused = this.h0.k;
                            try {
                                this.a.wait(30000L);
                            } catch (InterruptedException e2) {
                                c(e2);
                            }
                        }
                    }
                    obj = this.h0.i;
                    synchronized (obj) {
                        if (this.f0.peek() == null) {
                            break;
                        }
                    }
                }
            }
            if (this.h0.a.z().v(null, qf5.p0)) {
                b();
            }
        } finally {
            b();
        }
    }
}
