package defpackage;

import android.os.Bundle;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: fp5  reason: default package */
/* loaded from: classes.dex */
public interface fp5 {
    long a();

    String f();

    String g();

    String h();

    void i(String str, String str2, Bundle bundle);

    String l();

    void m(String str);

    List<Bundle> n(String str, String str2);

    void o(Bundle bundle);

    void p(String str);

    void q(String str, String str2, Bundle bundle);

    int r(String str);

    Map<String, Object> s(String str, String str2, boolean z);
}
