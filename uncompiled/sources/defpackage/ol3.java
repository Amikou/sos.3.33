package defpackage;

import java.util.Iterator;

/* compiled from: Sequence.kt */
/* renamed from: ol3  reason: default package */
/* loaded from: classes2.dex */
public interface ol3<T> {
    Iterator<T> iterator();
}
