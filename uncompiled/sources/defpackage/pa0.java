package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import com.github.mikephil.charting.utils.Utils;
import defpackage.u21;
import java.util.Arrays;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.Coin;

/* compiled from: CryptoAllTokensPagingAdapter.kt */
/* renamed from: pa0  reason: default package */
/* loaded from: classes2.dex */
public final class pa0 extends PagingDataAdapter<Coin, b> {
    public final tc1<Coin, te4> d;
    public final String e;
    public final String f;
    public final String g;

    /* compiled from: CryptoAllTokensPagingAdapter.kt */
    /* renamed from: pa0$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<Coin> {
        public static final a a = new a();

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(Coin coin, Coin coin2) {
            fs1.f(coin, "oldItem");
            fs1.f(coin2, "newItem");
            return fs1.b(coin, coin2);
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(Coin coin, Coin coin2) {
            fs1.f(coin, "oldItem");
            fs1.f(coin2, "newItem");
            return fs1.b(coin.getId(), coin2.getId());
        }
    }

    /* compiled from: CryptoAllTokensPagingAdapter.kt */
    /* renamed from: pa0$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final TextView a;
        public final TextView b;
        public final TextView c;
        public final ImageView d;
        public final ImageView e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(pa0 pa0Var, View view) {
            super(view);
            fs1.f(pa0Var, "this$0");
            fs1.f(view, "itemView");
            View findViewById = view.findViewById(R.id.tvTokenName);
            fs1.d(findViewById);
            this.a = (TextView) findViewById;
            View findViewById2 = view.findViewById(R.id.tvTokenPrice);
            fs1.d(findViewById2);
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.tvPercent);
            fs1.d(findViewById3);
            this.c = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.IvPercent);
            fs1.d(findViewById4);
            this.d = (ImageView) findViewById4;
            View findViewById5 = view.findViewById(R.id.ivToken);
            fs1.d(findViewById5);
            this.e = (ImageView) findViewById5;
        }

        public final ImageView a() {
            return this.d;
        }

        public final ImageView b() {
            return this.e;
        }

        public final TextView c() {
            return this.c;
        }

        public final TextView d() {
            return this.a;
        }

        public final TextView e() {
            return this.b;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public pa0(tc1<? super Coin, te4> tc1Var) {
        super(a.a, null, null, 6, null);
        fs1.f(tc1Var, "onClick");
        this.d = tc1Var;
        this.e = "1h";
        this.f = "24h";
        this.g = "7d";
    }

    public static final void h(pa0 pa0Var, Coin coin, View view) {
        fs1.f(pa0Var, "this$0");
        fs1.f(coin, "$coin");
        pa0Var.d.invoke(coin);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: g */
    public void onBindViewHolder(b bVar, int i) {
        Double percentChange7d;
        fs1.f(bVar, "holder");
        Coin item = getItem(i);
        fs1.d(item);
        final Coin coin = item;
        k73 u = com.bumptech.glide.a.u(bVar.b());
        Integer id = coin.getId();
        fs1.e(id, "coin.id");
        u.x(a4.f(id.intValue(), coin.getSymbol())).a(n73.u0(new uy())).I0(bVar.b());
        bVar.d().setText(coin.getName());
        u21.a aVar = u21.a;
        String t = e30.t(aVar.b());
        TextView e = bVar.e();
        lu3 lu3Var = lu3.a;
        Locale locale = Locale.getDefault();
        Double price = coin.getQuote().getUSD().getPrice();
        fs1.e(price, "coin.quote.usd.price");
        String format = String.format(locale, t, Arrays.copyOf(new Object[]{aVar.b(), e30.p(v21.a(price.doubleValue()), 0, null, false, 7, null)}, 2));
        fs1.e(format, "java.lang.String.format(locale, format, *args)");
        e.setText(format);
        String sortType = coin.getSortType();
        if (fs1.b(sortType, this.e)) {
            percentChange7d = coin.getQuote().getUSD().getPercentChange1h();
        } else {
            percentChange7d = fs1.b(sortType, this.g) ? coin.getQuote().getUSD().getPercentChange7d() : coin.getQuote().getUSD().getPercentChange24h();
        }
        if (percentChange7d != null) {
            TextView c = bVar.c();
            String format2 = String.format(Locale.getDefault(), "%s%%", Arrays.copyOf(new Object[]{e30.m(percentChange7d.doubleValue())}, 1));
            fs1.e(format2, "java.lang.String.format(locale, format, *args)");
            c.setText(format2);
            com.bumptech.glide.a.u(bVar.a()).w(Integer.valueOf(percentChange7d.doubleValue() < Utils.DOUBLE_EPSILON ? R.drawable.arrow_down : R.drawable.arrow_up)).I0(bVar.a());
        }
        bVar.itemView.setOnClickListener(new View.OnClickListener() { // from class: oa0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                pa0.h(pa0.this, coin, view);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: i */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        viewGroup.getContext();
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        fs1.e(inflate, "from(parent.context)\n   …list_item, parent, false)");
        return new b(this, inflate);
    }

    public final void j(String str) {
        fs1.f(str, "valueSort");
    }
}
