package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.phenotype.ExperimentTokens;

/* renamed from: ym5  reason: default package */
/* loaded from: classes.dex */
public final class ym5 implements Parcelable.Creator<ExperimentTokens> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ExperimentTokens createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        String str = null;
        byte[] bArr = null;
        byte[][] bArr2 = null;
        byte[][] bArr3 = null;
        byte[][] bArr4 = null;
        byte[][] bArr5 = null;
        int[] iArr = null;
        byte[][] bArr6 = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 3:
                    bArr = SafeParcelReader.g(parcel, C);
                    break;
                case 4:
                    bArr2 = SafeParcelReader.h(parcel, C);
                    break;
                case 5:
                    bArr3 = SafeParcelReader.h(parcel, C);
                    break;
                case 6:
                    bArr4 = SafeParcelReader.h(parcel, C);
                    break;
                case 7:
                    bArr5 = SafeParcelReader.h(parcel, C);
                    break;
                case 8:
                    iArr = SafeParcelReader.k(parcel, C);
                    break;
                case 9:
                    bArr6 = SafeParcelReader.h(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new ExperimentTokens(str, bArr, bArr2, bArr3, bArr4, bArr5, iArr, bArr6);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ExperimentTokens[] newArray(int i) {
        return new ExperimentTokens[i];
    }
}
