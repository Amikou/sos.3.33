package defpackage;

import androidx.media3.common.StreamKey;
import java.util.List;

/* compiled from: FilterableManifest.java */
/* renamed from: l41  reason: default package */
/* loaded from: classes.dex */
public interface l41<T> {
    T a(List<StreamKey> list);
}
