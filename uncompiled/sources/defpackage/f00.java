package defpackage;

import java.lang.Comparable;

/* compiled from: Range.kt */
/* renamed from: f00  reason: default package */
/* loaded from: classes2.dex */
public interface f00<T extends Comparable<? super T>> {
    T i();

    boolean isEmpty();

    T k();
}
