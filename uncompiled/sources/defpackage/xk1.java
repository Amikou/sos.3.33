package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: HolderReflectionBinding.java */
/* renamed from: xk1  reason: default package */
/* loaded from: classes2.dex */
public final class xk1 {
    public final ConstraintLayout a;
    public final TextView b;
    public final TextView c;
    public final TextView d;
    public final View e;

    public xk1(ConstraintLayout constraintLayout, TextView textView, TextView textView2, TextView textView3, View view) {
        this.a = constraintLayout;
        this.b = textView;
        this.c = textView2;
        this.d = textView3;
        this.e = view;
    }

    public static xk1 a(View view) {
        int i = R.id.tvTime;
        TextView textView = (TextView) ai4.a(view, R.id.tvTime);
        if (textView != null) {
            i = R.id.tvTokenBalance;
            TextView textView2 = (TextView) ai4.a(view, R.id.tvTokenBalance);
            if (textView2 != null) {
                i = R.id.tvTokenNativeBalance;
                TextView textView3 = (TextView) ai4.a(view, R.id.tvTokenNativeBalance);
                if (textView3 != null) {
                    i = R.id.vDivider;
                    View a = ai4.a(view, R.id.vDivider);
                    if (a != null) {
                        return new xk1((ConstraintLayout) view, textView, textView2, textView3, a);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
