package defpackage;

/* compiled from: OffsetEdgeTreatment.java */
/* renamed from: ul2  reason: default package */
/* loaded from: classes2.dex */
public final class ul2 extends cu0 {
    public final cu0 a;
    public final float b;

    public ul2(cu0 cu0Var, float f) {
        this.a = cu0Var;
        this.b = f;
    }

    @Override // defpackage.cu0
    public boolean a() {
        return this.a.a();
    }

    @Override // defpackage.cu0
    public void b(float f, float f2, float f3, rn3 rn3Var) {
        this.a.b(f, f2 - this.b, f3, rn3Var);
    }
}
