package defpackage;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.Image;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: yb1  reason: default package */
/* loaded from: classes.dex */
public class yb1 {
    public final b a;
    public ByteBuffer b;
    public c c;
    public Bitmap d;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: yb1$a */
    /* loaded from: classes.dex */
    public static class a {
        public final yb1 a = new yb1();

        @RecentlyNonNull
        public yb1 a() {
            if (this.a.b == null && this.a.d == null) {
                c unused = this.a.c;
                throw new IllegalStateException("Missing image data.  Call either setBitmap or setImageData to specify the image");
            }
            return this.a;
        }

        @RecentlyNonNull
        public a b(@RecentlyNonNull Bitmap bitmap) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            this.a.d = bitmap;
            b c = this.a.c();
            c.a = width;
            c.b = height;
            return this;
        }

        @RecentlyNonNull
        public a c(int i) {
            this.a.c().c = i;
            return this;
        }

        @RecentlyNonNull
        public a d(@RecentlyNonNull ByteBuffer byteBuffer, int i, int i2, int i3) {
            if (byteBuffer != null) {
                if (byteBuffer.capacity() >= i * i2) {
                    if (i3 == 16 || i3 == 17 || i3 == 842094169) {
                        this.a.b = byteBuffer;
                        b c = this.a.c();
                        c.a = i;
                        c.b = i2;
                        c.f = i3;
                        return this;
                    }
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Unsupported image format: ");
                    sb.append(i3);
                    throw new IllegalArgumentException(sb.toString());
                }
                throw new IllegalArgumentException("Invalid image data size.");
            }
            throw new IllegalArgumentException("Null image data supplied.");
        }

        @RecentlyNonNull
        public a e(int i) {
            this.a.c().e = i;
            return this;
        }

        @RecentlyNonNull
        public a f(long j) {
            this.a.c().d = j;
            return this;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: yb1$c */
    /* loaded from: classes.dex */
    public static class c {
    }

    public yb1() {
        this.a = new b();
        this.b = null;
        this.d = null;
    }

    @RecentlyNullable
    public Bitmap a() {
        return this.d;
    }

    @RecentlyNullable
    public ByteBuffer b() {
        Bitmap bitmap = this.d;
        if (bitmap != null) {
            if (bitmap == null) {
                return null;
            }
            int width = bitmap.getWidth();
            int height = this.d.getHeight();
            int i = width * height;
            int[] iArr = new int[i];
            this.d.getPixels(iArr, 0, width, 0, 0, width, height);
            byte[] bArr = new byte[i];
            for (int i2 = 0; i2 < i; i2++) {
                bArr[i2] = (byte) ((Color.red(iArr[i2]) * 0.299f) + (Color.green(iArr[i2]) * 0.587f) + (Color.blue(iArr[i2]) * 0.114f));
            }
            return ByteBuffer.wrap(bArr);
        }
        return this.b;
    }

    @RecentlyNonNull
    public b c() {
        return this.a;
    }

    @RecentlyNullable
    public Image.Plane[] d() {
        return null;
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: yb1$b */
    /* loaded from: classes.dex */
    public static class b {
        public int a;
        public int b;
        public int c;
        public long d;
        public int e;
        public int f;

        public b() {
            this.f = -1;
        }

        public int a() {
            return this.f;
        }

        public int b() {
            return this.b;
        }

        public int c() {
            return this.c;
        }

        public int d() {
            return this.e;
        }

        public long e() {
            return this.d;
        }

        public int f() {
            return this.a;
        }

        public final void i() {
            if (this.e % 2 != 0) {
                int i = this.a;
                this.a = this.b;
                this.b = i;
            }
            this.e = 0;
        }

        public b(@RecentlyNonNull b bVar) {
            this.f = -1;
            this.a = bVar.f();
            this.b = bVar.b();
            this.c = bVar.c();
            this.d = bVar.e();
            this.e = bVar.d();
            this.f = bVar.a();
        }
    }
}
