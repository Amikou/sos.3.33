package defpackage;

/* compiled from: LockFreeLinkedList.kt */
/* renamed from: k12  reason: default package */
/* loaded from: classes2.dex */
public final class k12 {
    public static final Object a = new k24("CONDITION_FALSE");

    static {
        new k24("LIST_EMPTY");
    }

    public static final Object a() {
        return a;
    }

    public static final l12 b(Object obj) {
        t63 t63Var = obj instanceof t63 ? (t63) obj : null;
        l12 l12Var = t63Var != null ? t63Var.a : null;
        return l12Var == null ? (l12) obj : l12Var;
    }
}
