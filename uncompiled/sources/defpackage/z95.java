package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: z95  reason: default package */
/* loaded from: classes.dex */
public final class z95 extends Handler {
    public z95(Looper looper) {
        super(looper);
    }
}
