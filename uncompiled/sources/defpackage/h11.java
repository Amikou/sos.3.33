package defpackage;

import android.util.Pair;
import androidx.lifecycle.Lifecycle;
import java.util.ArrayList;
import java.util.List;
import org.web3j.ens.contracts.generated.ENS;

/* compiled from: ExtendedLiveData.kt */
/* renamed from: h11  reason: default package */
/* loaded from: classes2.dex */
public final class h11<T> extends gb2<T> {
    public final List<Pair<rz1, tl2<? super T>>> a = new ArrayList();

    public final void a() {
        for (Pair<rz1, tl2<? super T>> pair : this.a) {
            removeObservers((rz1) pair.first);
        }
    }

    public final void b() {
        for (Pair<rz1, tl2<? super T>> pair : this.a) {
            super.observe((rz1) pair.first, (tl2) pair.second);
        }
    }

    public final void c(T t) {
        a();
        super.setValue(t);
        System.out.println((Object) fs1.l("Not notifying value ", getValue()));
    }

    @Override // androidx.lifecycle.LiveData
    public void observe(rz1 rz1Var, tl2<? super T> tl2Var) {
        fs1.f(rz1Var, ENS.FUNC_OWNER);
        fs1.f(tl2Var, "observer");
        this.a.add(new Pair<>(rz1Var, tl2Var));
        super.observe(rz1Var, tl2Var);
    }

    @Override // androidx.lifecycle.LiveData
    public void removeObserver(tl2<? super T> tl2Var) {
        fs1.f(tl2Var, "observer");
        for (Pair<rz1, tl2<? super T>> pair : this.a) {
            if (fs1.b(pair.second, tl2Var) && ((rz1) pair.first).getLifecycle().b() == Lifecycle.State.DESTROYED) {
                this.a.remove(pair);
            }
        }
        super.removeObserver(tl2Var);
    }

    @Override // defpackage.gb2, androidx.lifecycle.LiveData
    public void setValue(T t) {
        super.setValue(t);
        if (hasObservers()) {
            return;
        }
        b();
    }
}
