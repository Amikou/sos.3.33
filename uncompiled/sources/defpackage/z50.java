package defpackage;

import defpackage.wi3;
import zendesk.support.request.CellBase;

/* compiled from: ConstantBitrateSeekMap.java */
/* renamed from: z50  reason: default package */
/* loaded from: classes.dex */
public class z50 implements wi3 {
    public final long a;
    public final long b;
    public final int c;
    public final long d;
    public final int e;
    public final long f;
    public final boolean g;

    public z50(long j, long j2, int i, int i2, boolean z) {
        this.a = j;
        this.b = j2;
        this.c = i2 == -1 ? 1 : i2;
        this.e = i;
        this.g = z;
        if (j == -1) {
            this.d = -1L;
            this.f = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            return;
        }
        this.d = j - j2;
        this.f = f(j, j2, i);
    }

    public static long f(long j, long j2, int i) {
        return ((Math.max(0L, j - j2) * 8) * 1000000) / i;
    }

    public final long a(long j) {
        int i = this.c;
        long j2 = (((j * this.e) / 8000000) / i) * i;
        long j3 = this.d;
        if (j3 != -1) {
            j2 = Math.min(j2, j3 - i);
        }
        return this.b + Math.max(j2, 0L);
    }

    public long c(long j) {
        return f(j, this.b, this.e);
    }

    @Override // defpackage.wi3
    public boolean e() {
        return this.d != -1 || this.g;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        if (this.d == -1 && !this.g) {
            return new wi3.a(new yi3(0L, this.b));
        }
        long a = a(j);
        long c = c(a);
        yi3 yi3Var = new yi3(c, a);
        if (this.d != -1 && c < j) {
            int i = this.c;
            if (i + a < this.a) {
                long j2 = a + i;
                return new wi3.a(yi3Var, new yi3(c(j2), j2));
            }
        }
        return new wi3.a(yi3Var);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.f;
    }
}
