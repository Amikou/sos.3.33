package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: a36  reason: default package */
/* loaded from: classes.dex */
public final class a36 implements yp5<b36> {
    public static final a36 f0 = new a36();
    public final yp5<b36> a = gq5.a(gq5.b(new c36()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final b36 zza() {
        return this.a.zza();
    }
}
