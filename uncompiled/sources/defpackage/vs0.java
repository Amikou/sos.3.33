package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: vs0  reason: default package */
/* loaded from: classes2.dex */
public class vs0 {
    public static pt0 a(pt0 pt0Var) {
        if (pt0Var.x()) {
            return pt0Var;
        }
        throw new IllegalStateException("Invalid result");
    }

    public static pt0 b(pt0 pt0Var, BigInteger bigInteger, pt0 pt0Var2, BigInteger bigInteger2) {
        boolean z = bigInteger.signum() < 0;
        boolean z2 = bigInteger2.signum() < 0;
        BigInteger abs = bigInteger.abs();
        BigInteger abs2 = bigInteger2.abs();
        int max = Math.max(2, Math.min(16, hl4.i(abs.bitLength())));
        int max2 = Math.max(2, Math.min(16, hl4.i(abs2.bitLength())));
        gl4 l = hl4.l(pt0Var, max, true);
        gl4 l2 = hl4.l(pt0Var2, max2, true);
        return d(z ? l.b() : l.a(), z ? l.a() : l.b(), hl4.f(max, abs), z2 ? l2.b() : l2.a(), z2 ? l2.a() : l2.b(), hl4.f(max2, abs2));
    }

    public static pt0 c(pt0 pt0Var, BigInteger bigInteger, qt0 qt0Var, BigInteger bigInteger2) {
        boolean z = bigInteger.signum() < 0;
        boolean z2 = bigInteger2.signum() < 0;
        BigInteger abs = bigInteger.abs();
        BigInteger abs2 = bigInteger2.abs();
        int max = Math.max(2, Math.min(16, hl4.i(Math.max(abs.bitLength(), abs2.bitLength()))));
        pt0 k = hl4.k(pt0Var, max, true, qt0Var);
        gl4 g = hl4.g(pt0Var);
        gl4 g2 = hl4.g(k);
        return d(z ? g.b() : g.a(), z ? g.a() : g.b(), hl4.f(max, abs), z2 ? g2.b() : g2.a(), z2 ? g2.a() : g2.b(), hl4.f(max, abs2));
    }

    public static pt0 d(pt0[] pt0VarArr, pt0[] pt0VarArr2, byte[] bArr, pt0[] pt0VarArr3, pt0[] pt0VarArr4, byte[] bArr2) {
        pt0 pt0Var;
        int max = Math.max(bArr.length, bArr2.length);
        pt0 v = pt0VarArr[0].i().v();
        int i = max - 1;
        int i2 = 0;
        pt0 pt0Var2 = v;
        while (i >= 0) {
            byte b = i < bArr.length ? bArr[i] : (byte) 0;
            byte b2 = i < bArr2.length ? bArr2[i] : (byte) 0;
            if ((b | b2) == 0) {
                i2++;
            } else {
                if (b != 0) {
                    pt0Var = v.a((b < 0 ? pt0VarArr2 : pt0VarArr)[Math.abs((int) b) >>> 1]);
                } else {
                    pt0Var = v;
                }
                if (b2 != 0) {
                    pt0Var = pt0Var.a((b2 < 0 ? pt0VarArr4 : pt0VarArr3)[Math.abs((int) b2) >>> 1]);
                }
                if (i2 > 0) {
                    pt0Var2 = pt0Var2.I(i2);
                    i2 = 0;
                }
                pt0Var2 = pt0Var2.K(pt0Var);
            }
            i--;
        }
        return i2 > 0 ? pt0Var2.I(i2) : pt0Var2;
    }

    public static pt0 e(pt0[] pt0VarArr, qt0 qt0Var, BigInteger[] bigIntegerArr) {
        int length = pt0VarArr.length;
        int i = length << 1;
        boolean[] zArr = new boolean[i];
        gl4[] gl4VarArr = new gl4[i];
        byte[][] bArr = new byte[i];
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i2 << 1;
            int i4 = i3 + 1;
            BigInteger bigInteger = bigIntegerArr[i3];
            zArr[i3] = bigInteger.signum() < 0;
            BigInteger abs = bigInteger.abs();
            BigInteger bigInteger2 = bigIntegerArr[i4];
            zArr[i4] = bigInteger2.signum() < 0;
            BigInteger abs2 = bigInteger2.abs();
            int max = Math.max(2, Math.min(16, hl4.i(Math.max(abs.bitLength(), abs2.bitLength()))));
            pt0 pt0Var = pt0VarArr[i2];
            pt0 k = hl4.k(pt0Var, max, true, qt0Var);
            gl4VarArr[i3] = hl4.g(pt0Var);
            gl4VarArr[i4] = hl4.g(k);
            bArr[i3] = hl4.f(max, abs);
            bArr[i4] = hl4.f(max, abs2);
        }
        return g(zArr, gl4VarArr, bArr);
    }

    public static pt0 f(pt0[] pt0VarArr, BigInteger[] bigIntegerArr) {
        int length = pt0VarArr.length;
        boolean[] zArr = new boolean[length];
        gl4[] gl4VarArr = new gl4[length];
        byte[][] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            BigInteger bigInteger = bigIntegerArr[i];
            zArr[i] = bigInteger.signum() < 0;
            BigInteger abs = bigInteger.abs();
            int max = Math.max(2, Math.min(16, hl4.i(abs.bitLength())));
            gl4VarArr[i] = hl4.l(pt0VarArr[i], max, true);
            bArr[i] = hl4.f(max, abs);
        }
        return g(zArr, gl4VarArr, bArr);
    }

    public static pt0 g(boolean[] zArr, gl4[] gl4VarArr, byte[][] bArr) {
        int length = bArr.length;
        int i = 0;
        for (byte[] bArr2 : bArr) {
            i = Math.max(i, bArr2.length);
        }
        pt0 v = gl4VarArr[0].a()[0].i().v();
        int i2 = i - 1;
        int i3 = 0;
        pt0 pt0Var = v;
        while (i2 >= 0) {
            pt0 pt0Var2 = v;
            for (int i4 = 0; i4 < length; i4++) {
                byte[] bArr3 = bArr[i4];
                byte b = i2 < bArr3.length ? bArr3[i2] : (byte) 0;
                if (b != 0) {
                    int abs = Math.abs((int) b);
                    gl4 gl4Var = gl4VarArr[i4];
                    pt0Var2 = pt0Var2.a(((b < 0) == zArr[i4] ? gl4Var.a() : gl4Var.b())[abs >>> 1]);
                }
            }
            if (pt0Var2 == v) {
                i3++;
            } else {
                if (i3 > 0) {
                    pt0Var = pt0Var.I(i3);
                    i3 = 0;
                }
                pt0Var = pt0Var.K(pt0Var2);
            }
            i2--;
        }
        return i3 > 0 ? pt0Var.I(i3) : pt0Var;
    }

    public static pt0 h(pt0[] pt0VarArr, BigInteger[] bigIntegerArr, ke1 ke1Var) {
        BigInteger x = pt0VarArr[0].i().x();
        int length = pt0VarArr.length;
        int i = length << 1;
        BigInteger[] bigIntegerArr2 = new BigInteger[i];
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            BigInteger[] c = ke1Var.c(bigIntegerArr[i3].mod(x));
            int i4 = i2 + 1;
            bigIntegerArr2[i2] = c[0];
            i2 = i4 + 1;
            bigIntegerArr2[i4] = c[1];
        }
        qt0 a = ke1Var.a();
        if (ke1Var.b()) {
            return e(pt0VarArr, a, bigIntegerArr2);
        }
        pt0[] pt0VarArr2 = new pt0[i];
        int i5 = 0;
        for (pt0 pt0Var : pt0VarArr) {
            pt0 a2 = a.a(pt0Var);
            int i6 = i5 + 1;
            pt0VarArr2[i5] = pt0Var;
            i5 = i6 + 1;
            pt0VarArr2[i6] = a2;
        }
        return f(pt0VarArr2, bigIntegerArr2);
    }

    public static pt0 i(xs0 xs0Var, pt0 pt0Var) {
        if (xs0Var.m(pt0Var.i())) {
            return xs0Var.z(pt0Var);
        }
        throw new IllegalArgumentException("Point must be on the same curve");
    }

    public static boolean j(xs0 xs0Var) {
        return k(xs0Var.t());
    }

    public static boolean k(z41 z41Var) {
        return z41Var.b() > 1 && z41Var.c().equals(ws0.c) && (z41Var instanceof qs2);
    }

    public static boolean l(xs0 xs0Var) {
        return m(xs0Var.t());
    }

    public static boolean m(z41 z41Var) {
        return z41Var.b() == 1;
    }

    public static void n(ct0[] ct0VarArr, int i, int i2, ct0 ct0Var) {
        ct0[] ct0VarArr2 = new ct0[i2];
        int i3 = 0;
        ct0VarArr2[0] = ct0VarArr[i];
        while (true) {
            i3++;
            if (i3 >= i2) {
                break;
            }
            ct0VarArr2[i3] = ct0VarArr2[i3 - 1].j(ct0VarArr[i + i3]);
        }
        int i4 = i3 - 1;
        if (ct0Var != null) {
            ct0VarArr2[i4] = ct0VarArr2[i4].j(ct0Var);
        }
        ct0 g = ct0VarArr2[i4].g();
        while (i4 > 0) {
            int i5 = i4 - 1;
            int i6 = i4 + i;
            ct0 ct0Var2 = ct0VarArr[i6];
            ct0VarArr[i6] = ct0VarArr2[i5].j(g);
            g = g.j(ct0Var2);
            i4 = i5;
        }
        ct0VarArr[i] = g;
    }

    public static pt0 o(pt0 pt0Var, BigInteger bigInteger) {
        BigInteger abs = bigInteger.abs();
        pt0 v = pt0Var.i().v();
        int bitLength = abs.bitLength();
        if (bitLength > 0) {
            if (abs.testBit(0)) {
                v = pt0Var;
            }
            for (int i = 1; i < bitLength; i++) {
                pt0Var = pt0Var.J();
                if (abs.testBit(i)) {
                    v = v.a(pt0Var);
                }
            }
        }
        return bigInteger.signum() < 0 ? v.z() : v;
    }

    public static pt0 p(pt0 pt0Var, BigInteger bigInteger, pt0 pt0Var2, BigInteger bigInteger2) {
        pt0 h;
        xs0 i = pt0Var.i();
        pt0 i2 = i(i, pt0Var2);
        if ((i instanceof xs0.b) && ((xs0.b) i).H()) {
            h = pt0Var.y(bigInteger).a(i2.y(bigInteger2));
        } else {
            bt0 s = i.s();
            h = s instanceof ke1 ? h(new pt0[]{pt0Var, i2}, new BigInteger[]{bigInteger, bigInteger2}, (ke1) s) : b(pt0Var, bigInteger, i2, bigInteger2);
        }
        return a(h);
    }
}
