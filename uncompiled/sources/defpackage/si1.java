package defpackage;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.Gravity;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: GravityUtils.java */
/* renamed from: si1  reason: default package */
/* loaded from: classes.dex */
public class si1 {
    public static final Matrix a = new Matrix();
    public static final RectF b = new RectF();
    public static final Rect c = new Rect();
    public static final Rect d = new Rect();

    public static void a(Settings settings, Point point) {
        Rect rect = d;
        d(settings, rect);
        int j = settings.j();
        Rect rect2 = c;
        Gravity.apply(j, 0, 0, rect, rect2);
        point.set(rect2.left, rect2.top);
    }

    public static void b(us3 us3Var, Settings settings, Rect rect) {
        Matrix matrix = a;
        us3Var.d(matrix);
        c(matrix, settings, rect);
    }

    public static void c(Matrix matrix, Settings settings, Rect rect) {
        RectF rectF = b;
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, settings.l(), settings.k());
        matrix.mapRect(rectF);
        int round = Math.round(rectF.width());
        int round2 = Math.round(rectF.height());
        Rect rect2 = c;
        rect2.set(0, 0, settings.u(), settings.t());
        Gravity.apply(settings.j(), round, round2, rect2, rect);
    }

    public static void d(Settings settings, Rect rect) {
        Rect rect2 = c;
        rect2.set(0, 0, settings.u(), settings.t());
        Gravity.apply(settings.j(), settings.p(), settings.o(), rect2, rect);
    }
}
