package defpackage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import androidx.media3.ui.DefaultTimeBar;
import androidx.media3.ui.PlayerControlView;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PlayerControlViewLayoutManager.java */
/* renamed from: js2  reason: default package */
/* loaded from: classes.dex */
public final class js2 {
    public boolean A;
    public boolean B;
    public final PlayerControlView a;
    public final View b;
    public final ViewGroup c;
    public final ViewGroup d;
    public final ViewGroup e;
    public final ViewGroup f;
    public final ViewGroup g;
    public final ViewGroup h;
    public final ViewGroup i;
    public final View j;
    public final View k;
    public final AnimatorSet l;
    public final AnimatorSet m;
    public final AnimatorSet n;
    public final AnimatorSet o;
    public final AnimatorSet p;
    public final ValueAnimator q;
    public final ValueAnimator r;
    public final Runnable s = new Runnable() { // from class: fs2
        @Override // java.lang.Runnable
        public final void run() {
            js2.this.c0();
        }
    };
    public final Runnable t = new Runnable() { // from class: yr2
        @Override // java.lang.Runnable
        public final void run() {
            js2.this.D();
        }
    };
    public final Runnable u = new Runnable() { // from class: zr2
        @Override // java.lang.Runnable
        public final void run() {
            js2.this.H();
        }
    };
    public final Runnable v = new Runnable() { // from class: xr2
        @Override // java.lang.Runnable
        public final void run() {
            js2.this.G();
        }
    };
    public final Runnable w = new Runnable() { // from class: gs2
        @Override // java.lang.Runnable
        public final void run() {
            js2.this.E();
        }
    };
    public final View.OnLayoutChangeListener x = new View.OnLayoutChangeListener() { // from class: es2
        @Override // android.view.View.OnLayoutChangeListener
        public final void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
            js2.this.R(view, i2, i3, i4, i5, i6, i7, i8, i9);
        }
    };
    public boolean C = true;
    public int z = 0;
    public final List<View> y = new ArrayList();

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$a */
    /* loaded from: classes.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (js2.this.b != null) {
                js2.this.b.setVisibility(4);
            }
            if (js2.this.c != null) {
                js2.this.c.setVisibility(4);
            }
            if (js2.this.e != null) {
                js2.this.e.setVisibility(4);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (!(js2.this.j instanceof DefaultTimeBar) || js2.this.A) {
                return;
            }
            ((DefaultTimeBar) js2.this.j).g(250L);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$b */
    /* loaded from: classes.dex */
    public class b extends AnimatorListenerAdapter {
        public b() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (js2.this.b != null) {
                js2.this.b.setVisibility(0);
            }
            if (js2.this.c != null) {
                js2.this.c.setVisibility(0);
            }
            if (js2.this.e != null) {
                js2.this.e.setVisibility(js2.this.A ? 0 : 4);
            }
            if (!(js2.this.j instanceof DefaultTimeBar) || js2.this.A) {
                return;
            }
            ((DefaultTimeBar) js2.this.j).t(250L);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$c */
    /* loaded from: classes.dex */
    public class c extends AnimatorListenerAdapter {
        public final /* synthetic */ PlayerControlView a;

        public c(PlayerControlView playerControlView) {
            this.a = playerControlView;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            js2.this.Z(1);
            if (js2.this.B) {
                this.a.post(js2.this.s);
                js2.this.B = false;
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            js2.this.Z(3);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$d */
    /* loaded from: classes.dex */
    public class d extends AnimatorListenerAdapter {
        public final /* synthetic */ PlayerControlView a;

        public d(PlayerControlView playerControlView) {
            this.a = playerControlView;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            js2.this.Z(2);
            if (js2.this.B) {
                this.a.post(js2.this.s);
                js2.this.B = false;
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            js2.this.Z(3);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$e */
    /* loaded from: classes.dex */
    public class e extends AnimatorListenerAdapter {
        public final /* synthetic */ PlayerControlView a;

        public e(PlayerControlView playerControlView) {
            this.a = playerControlView;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            js2.this.Z(2);
            if (js2.this.B) {
                this.a.post(js2.this.s);
                js2.this.B = false;
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            js2.this.Z(3);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$f */
    /* loaded from: classes.dex */
    public class f extends AnimatorListenerAdapter {
        public f() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            js2.this.Z(0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            js2.this.Z(4);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$g */
    /* loaded from: classes.dex */
    public class g extends AnimatorListenerAdapter {
        public g() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            js2.this.Z(0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            js2.this.Z(4);
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$h */
    /* loaded from: classes.dex */
    public class h extends AnimatorListenerAdapter {
        public h() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (js2.this.f != null) {
                js2.this.f.setVisibility(4);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (js2.this.h != null) {
                js2.this.h.setVisibility(0);
                js2.this.h.setTranslationX(js2.this.h.getWidth());
                js2.this.h.scrollTo(js2.this.h.getWidth(), 0);
            }
        }
    }

    /* compiled from: PlayerControlViewLayoutManager.java */
    /* renamed from: js2$i */
    /* loaded from: classes.dex */
    public class i extends AnimatorListenerAdapter {
        public i() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            if (js2.this.h != null) {
                js2.this.h.setVisibility(4);
            }
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            if (js2.this.f != null) {
                js2.this.f.setVisibility(0);
            }
        }
    }

    public js2(PlayerControlView playerControlView) {
        this.a = playerControlView;
        this.b = playerControlView.findViewById(n03.exo_controls_background);
        this.c = (ViewGroup) playerControlView.findViewById(n03.exo_center_controls);
        this.e = (ViewGroup) playerControlView.findViewById(n03.exo_minimal_controls);
        ViewGroup viewGroup = (ViewGroup) playerControlView.findViewById(n03.exo_bottom_bar);
        this.d = viewGroup;
        this.i = (ViewGroup) playerControlView.findViewById(n03.exo_time);
        View findViewById = playerControlView.findViewById(n03.exo_progress);
        this.j = findViewById;
        this.f = (ViewGroup) playerControlView.findViewById(n03.exo_basic_controls);
        this.g = (ViewGroup) playerControlView.findViewById(n03.exo_extra_controls);
        this.h = (ViewGroup) playerControlView.findViewById(n03.exo_extra_controls_scroll_view);
        View findViewById2 = playerControlView.findViewById(n03.exo_overflow_show);
        this.k = findViewById2;
        View findViewById3 = playerControlView.findViewById(n03.exo_overflow_hide);
        if (findViewById2 != null && findViewById3 != null) {
            findViewById2.setOnClickListener(new View.OnClickListener() { // from class: ds2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    js2.this.T(view);
                }
            });
            findViewById3.setOnClickListener(new View.OnClickListener() { // from class: ds2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    js2.this.T(view);
                }
            });
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, Utils.FLOAT_EPSILON);
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: bs2
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                js2.this.J(valueAnimator);
            }
        });
        ofFloat.addListener(new a());
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(Utils.FLOAT_EPSILON, 1.0f);
        ofFloat2.setInterpolator(new LinearInterpolator());
        ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: as2
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                js2.this.K(valueAnimator);
            }
        });
        ofFloat2.addListener(new b());
        Resources resources = playerControlView.getResources();
        int i2 = gz2.exo_styled_bottom_bar_height;
        float dimension = resources.getDimension(i2) - resources.getDimension(gz2.exo_styled_progress_bar_height);
        float dimension2 = resources.getDimension(i2);
        AnimatorSet animatorSet = new AnimatorSet();
        this.l = animatorSet;
        animatorSet.setDuration(250L);
        animatorSet.addListener(new c(playerControlView));
        animatorSet.play(ofFloat).with(N(Utils.FLOAT_EPSILON, dimension, findViewById)).with(N(Utils.FLOAT_EPSILON, dimension, viewGroup));
        AnimatorSet animatorSet2 = new AnimatorSet();
        this.m = animatorSet2;
        animatorSet2.setDuration(250L);
        animatorSet2.addListener(new d(playerControlView));
        animatorSet2.play(N(dimension, dimension2, findViewById)).with(N(dimension, dimension2, viewGroup));
        AnimatorSet animatorSet3 = new AnimatorSet();
        this.n = animatorSet3;
        animatorSet3.setDuration(250L);
        animatorSet3.addListener(new e(playerControlView));
        animatorSet3.play(ofFloat).with(N(Utils.FLOAT_EPSILON, dimension2, findViewById)).with(N(Utils.FLOAT_EPSILON, dimension2, viewGroup));
        AnimatorSet animatorSet4 = new AnimatorSet();
        this.o = animatorSet4;
        animatorSet4.setDuration(250L);
        animatorSet4.addListener(new f());
        animatorSet4.play(ofFloat2).with(N(dimension, Utils.FLOAT_EPSILON, findViewById)).with(N(dimension, Utils.FLOAT_EPSILON, viewGroup));
        AnimatorSet animatorSet5 = new AnimatorSet();
        this.p = animatorSet5;
        animatorSet5.setDuration(250L);
        animatorSet5.addListener(new g());
        animatorSet5.play(ofFloat2).with(N(dimension2, Utils.FLOAT_EPSILON, findViewById)).with(N(dimension2, Utils.FLOAT_EPSILON, viewGroup));
        ValueAnimator ofFloat3 = ValueAnimator.ofFloat(Utils.FLOAT_EPSILON, 1.0f);
        this.q = ofFloat3;
        ofFloat3.setDuration(250L);
        ofFloat3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: wr2
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                js2.this.L(valueAnimator);
            }
        });
        ofFloat3.addListener(new h());
        ValueAnimator ofFloat4 = ValueAnimator.ofFloat(1.0f, Utils.FLOAT_EPSILON);
        this.r = ofFloat4;
        ofFloat4.setDuration(250L);
        ofFloat4.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: cs2
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                js2.this.M(valueAnimator);
            }
        });
        ofFloat4.addListener(new i());
    }

    public static int B(View view) {
        if (view == null) {
            return 0;
        }
        int width = view.getWidth();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            return width + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
        }
        return width;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void J(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        View view = this.b;
        if (view != null) {
            view.setAlpha(floatValue);
        }
        ViewGroup viewGroup = this.c;
        if (viewGroup != null) {
            viewGroup.setAlpha(floatValue);
        }
        ViewGroup viewGroup2 = this.e;
        if (viewGroup2 != null) {
            viewGroup2.setAlpha(floatValue);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void K(ValueAnimator valueAnimator) {
        float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        View view = this.b;
        if (view != null) {
            view.setAlpha(floatValue);
        }
        ViewGroup viewGroup = this.c;
        if (viewGroup != null) {
            viewGroup.setAlpha(floatValue);
        }
        ViewGroup viewGroup2 = this.e;
        if (viewGroup2 != null) {
            viewGroup2.setAlpha(floatValue);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void L(ValueAnimator valueAnimator) {
        y(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void M(ValueAnimator valueAnimator) {
        y(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    public static ObjectAnimator N(float f2, float f3, View view) {
        return ObjectAnimator.ofFloat(view, "translationY", f2, f3);
    }

    public static int z(View view) {
        if (view == null) {
            return 0;
        }
        int height = view.getHeight();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            return height + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
        }
        return height;
    }

    public boolean A(View view) {
        return view != null && this.y.contains(view);
    }

    public void C() {
        int i2 = this.z;
        if (i2 == 3 || i2 == 2) {
            return;
        }
        V();
        if (!this.C) {
            E();
        } else if (this.z == 1) {
            H();
        } else {
            D();
        }
    }

    public final void D() {
        this.n.start();
    }

    public final void E() {
        Z(2);
    }

    public void F() {
        int i2 = this.z;
        if (i2 == 3 || i2 == 2) {
            return;
        }
        V();
        E();
    }

    public final void G() {
        this.l.start();
        U(this.u, 2000L);
    }

    public final void H() {
        this.m.start();
    }

    public boolean I() {
        return this.z == 0 && this.a.h0();
    }

    public void O() {
        this.a.addOnLayoutChangeListener(this.x);
    }

    public void P() {
        this.a.removeOnLayoutChangeListener(this.x);
    }

    public void Q(boolean z, int i2, int i3, int i4, int i5) {
        View view = this.b;
        if (view != null) {
            view.layout(0, 0, i4 - i2, i5 - i3);
        }
    }

    public final void R(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        boolean e0 = e0();
        if (this.A != e0) {
            this.A = e0;
            view.post(new Runnable() { // from class: hs2
                @Override // java.lang.Runnable
                public final void run() {
                    js2.this.d0();
                }
            });
        }
        boolean z = i4 - i2 != i8 - i6;
        if (this.A || !z) {
            return;
        }
        view.post(new Runnable() { // from class: is2
            @Override // java.lang.Runnable
            public final void run() {
                js2.this.S();
            }
        });
    }

    public final void S() {
        int i2;
        if (this.f == null || this.g == null) {
            return;
        }
        int width = (this.a.getWidth() - this.a.getPaddingLeft()) - this.a.getPaddingRight();
        while (true) {
            if (this.g.getChildCount() <= 1) {
                break;
            }
            int childCount = this.g.getChildCount() - 2;
            View childAt = this.g.getChildAt(childCount);
            this.g.removeViewAt(childCount);
            this.f.addView(childAt, 0);
        }
        View view = this.k;
        if (view != null) {
            view.setVisibility(8);
        }
        int B = B(this.i);
        int childCount2 = this.f.getChildCount() - 1;
        for (int i3 = 0; i3 < childCount2; i3++) {
            B += B(this.f.getChildAt(i3));
        }
        if (B > width) {
            View view2 = this.k;
            if (view2 != null) {
                view2.setVisibility(0);
                B += B(this.k);
            }
            ArrayList arrayList = new ArrayList();
            for (int i4 = 0; i4 < childCount2; i4++) {
                View childAt2 = this.f.getChildAt(i4);
                B -= B(childAt2);
                arrayList.add(childAt2);
                if (B <= width) {
                    break;
                }
            }
            if (arrayList.isEmpty()) {
                return;
            }
            this.f.removeViews(0, arrayList.size());
            for (i2 = 0; i2 < arrayList.size(); i2++) {
                this.g.addView((View) arrayList.get(i2), this.g.getChildCount() - 1);
            }
            return;
        }
        ViewGroup viewGroup = this.h;
        if (viewGroup == null || viewGroup.getVisibility() != 0 || this.r.isStarted()) {
            return;
        }
        this.q.cancel();
        this.r.start();
    }

    public final void T(View view) {
        W();
        if (view.getId() == n03.exo_overflow_show) {
            this.q.start();
        } else if (view.getId() == n03.exo_overflow_hide) {
            this.r.start();
        }
    }

    public final void U(Runnable runnable, long j) {
        if (j >= 0) {
            this.a.postDelayed(runnable, j);
        }
    }

    public void V() {
        this.a.removeCallbacks(this.w);
        this.a.removeCallbacks(this.t);
        this.a.removeCallbacks(this.v);
        this.a.removeCallbacks(this.u);
    }

    public void W() {
        if (this.z == 3) {
            return;
        }
        V();
        int showTimeoutMs = this.a.getShowTimeoutMs();
        if (showTimeoutMs > 0) {
            if (!this.C) {
                U(this.w, showTimeoutMs);
            } else if (this.z == 1) {
                U(this.u, 2000L);
            } else {
                U(this.v, showTimeoutMs);
            }
        }
    }

    public void X(boolean z) {
        this.C = z;
    }

    public void Y(View view, boolean z) {
        if (view == null) {
            return;
        }
        if (!z) {
            view.setVisibility(8);
            this.y.remove(view);
            return;
        }
        if (this.A && a0(view)) {
            view.setVisibility(4);
        } else {
            view.setVisibility(0);
        }
        this.y.add(view);
    }

    public final void Z(int i2) {
        int i3 = this.z;
        this.z = i2;
        if (i2 == 2) {
            this.a.setVisibility(8);
        } else if (i3 == 2) {
            this.a.setVisibility(0);
        }
        if (i3 != i2) {
            this.a.i0();
        }
    }

    public final boolean a0(View view) {
        int id = view.getId();
        return id == n03.exo_bottom_bar || id == n03.exo_prev || id == n03.exo_next || id == n03.exo_rew || id == n03.exo_rew_with_amount || id == n03.exo_ffwd || id == n03.exo_ffwd_with_amount;
    }

    public void b0() {
        if (!this.a.h0()) {
            this.a.setVisibility(0);
            this.a.s0();
            this.a.n0();
        }
        c0();
    }

    public final void c0() {
        if (!this.C) {
            Z(0);
            W();
            return;
        }
        int i2 = this.z;
        if (i2 == 1) {
            this.o.start();
        } else if (i2 == 2) {
            this.p.start();
        } else if (i2 == 3) {
            this.B = true;
        } else if (i2 == 4) {
            return;
        }
        W();
    }

    public final void d0() {
        ViewGroup viewGroup = this.e;
        if (viewGroup != null) {
            viewGroup.setVisibility(this.A ? 0 : 4);
        }
        if (this.j != null) {
            int dimensionPixelSize = this.a.getResources().getDimensionPixelSize(gz2.exo_styled_progress_margin_bottom);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.j.getLayoutParams();
            if (marginLayoutParams != null) {
                if (this.A) {
                    dimensionPixelSize = 0;
                }
                marginLayoutParams.bottomMargin = dimensionPixelSize;
                this.j.setLayoutParams(marginLayoutParams);
            }
            View view = this.j;
            if (view instanceof DefaultTimeBar) {
                DefaultTimeBar defaultTimeBar = (DefaultTimeBar) view;
                if (this.A) {
                    defaultTimeBar.h(true);
                } else {
                    int i2 = this.z;
                    if (i2 == 1) {
                        defaultTimeBar.h(false);
                    } else if (i2 != 3) {
                        defaultTimeBar.s();
                    }
                }
            }
        }
        for (View view2 : this.y) {
            view2.setVisibility((this.A && a0(view2)) ? 4 : 0);
        }
    }

    public final boolean e0() {
        int width = (this.a.getWidth() - this.a.getPaddingLeft()) - this.a.getPaddingRight();
        int height = (this.a.getHeight() - this.a.getPaddingBottom()) - this.a.getPaddingTop();
        int B = B(this.c);
        ViewGroup viewGroup = this.c;
        int paddingLeft = B - (viewGroup != null ? viewGroup.getPaddingLeft() + this.c.getPaddingRight() : 0);
        int z = z(this.c);
        ViewGroup viewGroup2 = this.c;
        return width <= Math.max(paddingLeft, B(this.i) + B(this.k)) || height <= (z - (viewGroup2 != null ? viewGroup2.getPaddingTop() + this.c.getPaddingBottom() : 0)) + (z(this.d) * 2);
    }

    public final void y(float f2) {
        ViewGroup viewGroup = this.h;
        if (viewGroup != null) {
            this.h.setTranslationX((int) (viewGroup.getWidth() * (1.0f - f2)));
        }
        ViewGroup viewGroup2 = this.i;
        if (viewGroup2 != null) {
            viewGroup2.setAlpha(1.0f - f2);
        }
        ViewGroup viewGroup3 = this.f;
        if (viewGroup3 != null) {
            viewGroup3.setAlpha(1.0f - f2);
        }
    }
}
