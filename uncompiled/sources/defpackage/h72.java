package defpackage;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/* compiled from: MemberKey.java */
/* renamed from: h72  reason: default package */
/* loaded from: classes.dex */
public final class h72 {
    public static final Class<?>[] c = new Class[0];
    public final String a;
    public final Class<?>[] b;

    public h72(Method method) {
        this(method.getName(), method.getParameterTypes());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && obj.getClass() == h72.class) {
            h72 h72Var = (h72) obj;
            if (this.a.equals(h72Var.a)) {
                Class<?>[] clsArr = h72Var.b;
                int length = this.b.length;
                if (clsArr.length != length) {
                    return false;
                }
                for (int i = 0; i < length; i++) {
                    if (clsArr[i] != this.b[i]) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() + this.b.length;
    }

    public String toString() {
        return this.a + "(" + this.b.length + "-args)";
    }

    public h72(Constructor<?> constructor) {
        this("", constructor.getParameterTypes());
    }

    public h72(String str, Class<?>[] clsArr) {
        this.a = str;
        this.b = clsArr == null ? c : clsArr;
    }
}
