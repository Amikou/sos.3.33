package defpackage;

import com.google.android.gms.measurement.internal.zzas;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: gl5  reason: default package */
/* loaded from: classes.dex */
public final class gl5 implements Runnable {
    public final /* synthetic */ zzas a;
    public final /* synthetic */ zzp f0;
    public final /* synthetic */ pl5 g0;

    public gl5(pl5 pl5Var, zzas zzasVar, zzp zzpVar) {
        this.g0 = pl5Var;
        this.a = zzasVar;
        this.f0 = zzpVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        zzas I1 = this.g0.I1(this.a, this.f0);
        a36.a();
        fw5Var = this.g0.a;
        if (!fw5Var.S().v(null, qf5.B0)) {
            this.g0.N1(I1, this.f0);
        } else {
            this.g0.H1(I1, this.f0);
        }
    }
}
