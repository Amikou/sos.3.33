package defpackage;

import android.content.Context;
import androidx.work.impl.constraints.controllers.ConstraintController;
import defpackage.d60;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WorkConstraintsTracker.java */
/* renamed from: wp4  reason: default package */
/* loaded from: classes.dex */
public class wp4 implements d60.a {
    public static final String d = v12.f("WorkConstraintsTracker");
    public final vp4 a;
    public final ConstraintController<?>[] b;
    public final Object c;

    public wp4(Context context, q34 q34Var, vp4 vp4Var) {
        Context applicationContext = context.getApplicationContext();
        this.a = vp4Var;
        this.b = new d60[]{new no(applicationContext, q34Var), new po(applicationContext, q34Var), new xt3(applicationContext, q34Var), new ze2(applicationContext, q34Var), new gf2(applicationContext, q34Var), new bf2(applicationContext, q34Var), new af2(applicationContext, q34Var)};
        this.c = new Object();
    }

    @Override // defpackage.d60.a
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String str : list) {
                if (c(str)) {
                    v12.c().a(d, String.format("Constraints met for %s", str), new Throwable[0]);
                    arrayList.add(str);
                }
            }
            vp4 vp4Var = this.a;
            if (vp4Var != null) {
                vp4Var.f(arrayList);
            }
        }
    }

    @Override // defpackage.d60.a
    public void b(List<String> list) {
        synchronized (this.c) {
            vp4 vp4Var = this.a;
            if (vp4Var != null) {
                vp4Var.b(list);
            }
        }
    }

    public boolean c(String str) {
        d60[] d60VarArr;
        synchronized (this.c) {
            for (d60 d60Var : this.b) {
                if (d60Var.d(str)) {
                    v12.c().a(d, String.format("Work %s constrained by %s", str, d60Var.getClass().getSimpleName()), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    public void d(Iterable<tq4> iterable) {
        synchronized (this.c) {
            for (d60 d60Var : this.b) {
                d60Var.g(null);
            }
            for (d60 d60Var2 : this.b) {
                d60Var2.e(iterable);
            }
            for (d60 d60Var3 : this.b) {
                d60Var3.g(this);
            }
        }
    }

    public void e() {
        synchronized (this.c) {
            for (d60 d60Var : this.b) {
                d60Var.f();
            }
        }
    }
}
