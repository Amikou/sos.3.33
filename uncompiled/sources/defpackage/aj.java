package defpackage;

import android.util.Pair;
import androidx.media3.common.DrmInitData;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.extractor.metadata.mp4.MdtaMetadataEntry;
import androidx.media3.extractor.metadata.mp4.SmtaMetadataEntry;
import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Ints;
import defpackage.zi;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: AtomParsers.java */
/* renamed from: aj  reason: default package */
/* loaded from: classes.dex */
public final class aj {
    public static final byte[] a = androidx.media3.common.util.b.j0("OpusHead");

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public int b;
        public int c;
        public long d;
        public final boolean e;
        public final op2 f;
        public final op2 g;
        public int h;
        public int i;

        public a(op2 op2Var, op2 op2Var2, boolean z) throws ParserException {
            this.g = op2Var;
            this.f = op2Var2;
            this.e = z;
            op2Var2.P(12);
            this.a = op2Var2.H();
            op2Var.P(12);
            this.i = op2Var.H();
            s11.a(op2Var.n() == 1, "first_chunk must be 1");
            this.b = -1;
        }

        public boolean a() {
            long F;
            int i = this.b + 1;
            this.b = i;
            if (i == this.a) {
                return false;
            }
            if (this.e) {
                F = this.f.I();
            } else {
                F = this.f.F();
            }
            this.d = F;
            if (this.b == this.h) {
                this.c = this.g.H();
                this.g.Q(4);
                int i2 = this.i - 1;
                this.i = i2;
                this.h = i2 > 0 ? this.g.H() - 1 : -1;
            }
            return true;
        }
    }

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final String a;
        public final byte[] b;
        public final long c;
        public final long d;

        public b(String str, byte[] bArr, long j, long j2) {
            this.a = str;
            this.b = bArr;
            this.c = j;
            this.d = j2;
        }
    }

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$c */
    /* loaded from: classes.dex */
    public interface c {
        int a();

        int b();

        int c();
    }

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$d */
    /* loaded from: classes.dex */
    public static final class d {
        public final x74[] a;
        public j b;
        public int c;
        public int d = 0;

        public d(int i) {
            this.a = new x74[i];
        }
    }

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$e */
    /* loaded from: classes.dex */
    public static final class e implements c {
        public final int a;
        public final int b;
        public final op2 c;

        public e(zi.b bVar, j jVar) {
            op2 op2Var = bVar.b;
            this.c = op2Var;
            op2Var.P(12);
            int H = op2Var.H();
            if ("audio/raw".equals(jVar.p0)) {
                int a0 = androidx.media3.common.util.b.a0(jVar.E0, jVar.C0);
                if (H == 0 || H % a0 != 0) {
                    p12.i("AtomParsers", "Audio sample size mismatch. stsd sample size: " + a0 + ", stsz sample size: " + H);
                    H = a0;
                }
            }
            this.a = H == 0 ? -1 : H;
            this.b = op2Var.H();
        }

        @Override // defpackage.aj.c
        public int a() {
            return this.a;
        }

        @Override // defpackage.aj.c
        public int b() {
            return this.b;
        }

        @Override // defpackage.aj.c
        public int c() {
            int i = this.a;
            return i == -1 ? this.c.H() : i;
        }
    }

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$f */
    /* loaded from: classes.dex */
    public static final class f implements c {
        public final op2 a;
        public final int b;
        public final int c;
        public int d;
        public int e;

        public f(zi.b bVar) {
            op2 op2Var = bVar.b;
            this.a = op2Var;
            op2Var.P(12);
            this.c = op2Var.H() & 255;
            this.b = op2Var.H();
        }

        @Override // defpackage.aj.c
        public int a() {
            return -1;
        }

        @Override // defpackage.aj.c
        public int b() {
            return this.b;
        }

        @Override // defpackage.aj.c
        public int c() {
            int i = this.c;
            if (i == 8) {
                return this.a.D();
            }
            if (i == 16) {
                return this.a.J();
            }
            int i2 = this.d;
            this.d = i2 + 1;
            if (i2 % 2 == 0) {
                int D = this.a.D();
                this.e = D;
                return (D & 240) >> 4;
            }
            return this.e & 15;
        }
    }

    /* compiled from: AtomParsers.java */
    /* renamed from: aj$g */
    /* loaded from: classes.dex */
    public static final class g {
        public final int a;
        public final long b;
        public final int c;

        public g(int i, long j, int i2) {
            this.a = i;
            this.b = j;
            this.c = i2;
        }
    }

    public static List<g84> A(zi.a aVar, qe1 qe1Var, long j, DrmInitData drmInitData, boolean z, boolean z2, jd1<w74, w74> jd1Var) throws ParserException {
        w74 apply;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < aVar.d.size(); i++) {
            zi.a aVar2 = aVar.d.get(i);
            if (aVar2.a == 1953653099 && (apply = jd1Var.apply(z(aVar2, (zi.b) ii.e(aVar.g(1836476516)), j, drmInitData, z, z2))) != null) {
                arrayList.add(v(apply, (zi.a) ii.e(((zi.a) ii.e(((zi.a) ii.e(aVar2.f(1835297121))).f(1835626086))).f(1937007212)), qe1Var));
            }
        }
        return arrayList;
    }

    public static Pair<Metadata, Metadata> B(zi.b bVar) {
        op2 op2Var = bVar.b;
        op2Var.P(8);
        Metadata metadata = null;
        Metadata metadata2 = null;
        while (op2Var.a() >= 8) {
            int e2 = op2Var.e();
            int n = op2Var.n();
            int n2 = op2Var.n();
            if (n2 == 1835365473) {
                op2Var.P(e2);
                metadata = C(op2Var, e2 + n);
            } else if (n2 == 1936553057) {
                op2Var.P(e2);
                metadata2 = u(op2Var, e2 + n);
            }
            op2Var.P(e2 + n);
        }
        return Pair.create(metadata, metadata2);
    }

    public static Metadata C(op2 op2Var, int i) {
        op2Var.Q(8);
        e(op2Var);
        while (op2Var.e() < i) {
            int e2 = op2Var.e();
            int n = op2Var.n();
            if (op2Var.n() == 1768715124) {
                op2Var.P(e2);
                return l(op2Var, e2 + n);
            }
            op2Var.P(e2 + n);
        }
        return null;
    }

    public static void D(op2 op2Var, int i, int i2, int i3, int i4, int i5, DrmInitData drmInitData, d dVar, int i6) throws ParserException {
        DrmInitData drmInitData2;
        int i7;
        int i8;
        byte[] bArr;
        float f2;
        List<byte[]> list;
        String str;
        int i9 = i2;
        int i10 = i3;
        DrmInitData drmInitData3 = drmInitData;
        d dVar2 = dVar;
        op2Var.P(i9 + 8 + 8);
        op2Var.Q(16);
        int J = op2Var.J();
        int J2 = op2Var.J();
        op2Var.Q(50);
        int e2 = op2Var.e();
        int i11 = i;
        if (i11 == 1701733238) {
            Pair<Integer, x74> s = s(op2Var, i9, i10);
            if (s != null) {
                i11 = ((Integer) s.first).intValue();
                drmInitData3 = drmInitData3 == null ? null : drmInitData3.c(((x74) s.second).b);
                dVar2.a[i6] = (x74) s.second;
            }
            op2Var.P(e2);
        }
        String str2 = "video/3gpp";
        String str3 = i11 == 1831958048 ? "video/mpeg" : i11 == 1211250227 ? "video/3gpp" : null;
        float f3 = 1.0f;
        byte[] bArr2 = null;
        String str4 = null;
        List<byte[]> list2 = null;
        int i12 = -1;
        int i13 = -1;
        int i14 = -1;
        int i15 = -1;
        ByteBuffer byteBuffer = null;
        b bVar = null;
        boolean z = false;
        while (true) {
            if (e2 - i9 >= i10) {
                drmInitData2 = drmInitData3;
                break;
            }
            op2Var.P(e2);
            int e3 = op2Var.e();
            String str5 = str2;
            int n = op2Var.n();
            if (n == 0) {
                drmInitData2 = drmInitData3;
                if (op2Var.e() - i9 == i10) {
                    break;
                }
            } else {
                drmInitData2 = drmInitData3;
            }
            s11.a(n > 0, "childAtomSize must be positive");
            int n2 = op2Var.n();
            if (n2 == 1635148611) {
                s11.a(str3 == null, null);
                op2Var.P(e3 + 8);
                rl b2 = rl.b(op2Var);
                list2 = b2.a;
                dVar2.c = b2.b;
                if (!z) {
                    f3 = b2.e;
                }
                str4 = b2.f;
                str = "video/avc";
            } else if (n2 == 1752589123) {
                s11.a(str3 == null, null);
                op2Var.P(e3 + 8);
                nk1 a2 = nk1.a(op2Var);
                list2 = a2.a;
                dVar2.c = a2.b;
                if (!z) {
                    f3 = a2.c;
                }
                str4 = a2.d;
                str = "video/hevc";
            } else {
                if (n2 == 1685480259 || n2 == 1685485123) {
                    i7 = J2;
                    i8 = i11;
                    bArr = bArr2;
                    f2 = f3;
                    list = list2;
                    eq0 a3 = eq0.a(op2Var);
                    if (a3 != null) {
                        str4 = a3.a;
                        str3 = "video/dolby-vision";
                    }
                } else if (n2 == 1987076931) {
                    s11.a(str3 == null, null);
                    str = i11 == 1987063864 ? "video/x-vnd.on2.vp8" : "video/x-vnd.on2.vp9";
                } else if (n2 == 1635135811) {
                    s11.a(str3 == null, null);
                    str = "video/av01";
                } else if (n2 == 1668050025) {
                    ByteBuffer a4 = byteBuffer == null ? a() : byteBuffer;
                    a4.position(21);
                    a4.putShort(op2Var.z());
                    a4.putShort(op2Var.z());
                    byteBuffer = a4;
                    i7 = J2;
                    i8 = i11;
                    e2 += n;
                    i9 = i2;
                    i10 = i3;
                    dVar2 = dVar;
                    str2 = str5;
                    drmInitData3 = drmInitData2;
                    i11 = i8;
                    J2 = i7;
                } else if (n2 == 1835295606) {
                    ByteBuffer a5 = byteBuffer == null ? a() : byteBuffer;
                    short z2 = op2Var.z();
                    short z3 = op2Var.z();
                    short z4 = op2Var.z();
                    i8 = i11;
                    short z5 = op2Var.z();
                    short z6 = op2Var.z();
                    List<byte[]> list3 = list2;
                    short z7 = op2Var.z();
                    byte[] bArr3 = bArr2;
                    short z8 = op2Var.z();
                    float f4 = f3;
                    short z9 = op2Var.z();
                    long F = op2Var.F();
                    long F2 = op2Var.F();
                    i7 = J2;
                    a5.position(1);
                    a5.putShort(z6);
                    a5.putShort(z7);
                    a5.putShort(z2);
                    a5.putShort(z3);
                    a5.putShort(z4);
                    a5.putShort(z5);
                    a5.putShort(z8);
                    a5.putShort(z9);
                    a5.putShort((short) (F / 10000));
                    a5.putShort((short) (F2 / 10000));
                    byteBuffer = a5;
                    list2 = list3;
                    bArr2 = bArr3;
                    f3 = f4;
                    e2 += n;
                    i9 = i2;
                    i10 = i3;
                    dVar2 = dVar;
                    str2 = str5;
                    drmInitData3 = drmInitData2;
                    i11 = i8;
                    J2 = i7;
                } else {
                    i7 = J2;
                    i8 = i11;
                    bArr = bArr2;
                    f2 = f3;
                    list = list2;
                    if (n2 == 1681012275) {
                        s11.a(str3 == null, null);
                        str3 = str5;
                    } else if (n2 == 1702061171) {
                        s11.a(str3 == null, null);
                        bVar = i(op2Var, e3);
                        String str6 = bVar.a;
                        byte[] bArr4 = bVar.b;
                        list2 = bArr4 != null ? ImmutableList.of(bArr4) : list;
                        str3 = str6;
                        bArr2 = bArr;
                        f3 = f2;
                        e2 += n;
                        i9 = i2;
                        i10 = i3;
                        dVar2 = dVar;
                        str2 = str5;
                        drmInitData3 = drmInitData2;
                        i11 = i8;
                        J2 = i7;
                    } else if (n2 == 1885434736) {
                        f3 = q(op2Var, e3);
                        list2 = list;
                        bArr2 = bArr;
                        z = true;
                        e2 += n;
                        i9 = i2;
                        i10 = i3;
                        dVar2 = dVar;
                        str2 = str5;
                        drmInitData3 = drmInitData2;
                        i11 = i8;
                        J2 = i7;
                    } else if (n2 == 1937126244) {
                        bArr2 = r(op2Var, e3, n);
                        list2 = list;
                        f3 = f2;
                        e2 += n;
                        i9 = i2;
                        i10 = i3;
                        dVar2 = dVar;
                        str2 = str5;
                        drmInitData3 = drmInitData2;
                        i11 = i8;
                        J2 = i7;
                    } else if (n2 == 1936995172) {
                        int D = op2Var.D();
                        op2Var.Q(3);
                        if (D == 0) {
                            int D2 = op2Var.D();
                            if (D2 == 0) {
                                i12 = 0;
                            } else if (D2 == 1) {
                                i12 = 1;
                            } else if (D2 == 2) {
                                i12 = 2;
                            } else if (D2 == 3) {
                                i12 = 3;
                            }
                        }
                    } else if (n2 == 1668246642) {
                        int n3 = op2Var.n();
                        if (n3 != 1852009592 && n3 != 1852009571) {
                            p12.i("AtomParsers", "Unsupported color type: " + zi.a(n3));
                        } else {
                            int J3 = op2Var.J();
                            int J4 = op2Var.J();
                            op2Var.Q(2);
                            boolean z10 = n == 19 && (op2Var.D() & 128) != 0;
                            i13 = androidx.media3.common.f.b(J3);
                            i14 = z10 ? 1 : 2;
                            i15 = androidx.media3.common.f.c(J4);
                        }
                    }
                }
                list2 = list;
                bArr2 = bArr;
                f3 = f2;
                e2 += n;
                i9 = i2;
                i10 = i3;
                dVar2 = dVar;
                str2 = str5;
                drmInitData3 = drmInitData2;
                i11 = i8;
                J2 = i7;
            }
            str3 = str;
            i7 = J2;
            i8 = i11;
            e2 += n;
            i9 = i2;
            i10 = i3;
            dVar2 = dVar;
            str2 = str5;
            drmInitData3 = drmInitData2;
            i11 = i8;
            J2 = i7;
        }
        int i16 = J2;
        byte[] bArr5 = bArr2;
        float f5 = f3;
        List<byte[]> list4 = list2;
        if (str3 == null) {
            return;
        }
        j.b M = new j.b().R(i4).e0(str3).I(str4).j0(J).Q(i16).a0(f5).d0(i5).b0(bArr5).h0(i12).T(list4).M(drmInitData2);
        int i17 = i13;
        int i18 = i14;
        int i19 = i15;
        if (i17 != -1 || i18 != -1 || i19 != -1 || byteBuffer != null) {
            M.J(new androidx.media3.common.f(i17, i18, i19, byteBuffer != null ? byteBuffer.array() : null));
        }
        if (bVar != null) {
            M.G(Ints.j(bVar.c)).Z(Ints.j(bVar.d));
        }
        dVar.b = M.E();
    }

    public static ByteBuffer a() {
        return ByteBuffer.allocate(25).order(ByteOrder.LITTLE_ENDIAN);
    }

    public static boolean b(long[] jArr, long j, long j2, long j3) {
        int length = jArr.length - 1;
        return jArr[0] <= j2 && j2 < jArr[androidx.media3.common.util.b.q(4, 0, length)] && jArr[androidx.media3.common.util.b.q(jArr.length - 4, 0, length)] < j3 && j3 <= j;
    }

    public static int c(op2 op2Var, int i, int i2, int i3) throws ParserException {
        int e2 = op2Var.e();
        s11.a(e2 >= i2, null);
        while (e2 - i2 < i3) {
            op2Var.P(e2);
            int n = op2Var.n();
            s11.a(n > 0, "childAtomSize must be positive");
            if (op2Var.n() == i) {
                return e2;
            }
            e2 += n;
        }
        return -1;
    }

    public static int d(int i) {
        if (i == 1936684398) {
            return 1;
        }
        if (i == 1986618469) {
            return 2;
        }
        if (i == 1952807028 || i == 1935832172 || i == 1937072756 || i == 1668047728) {
            return 3;
        }
        return i == 1835365473 ? 5 : -1;
    }

    public static void e(op2 op2Var) {
        int e2 = op2Var.e();
        op2Var.Q(4);
        if (op2Var.n() != 1751411826) {
            e2 += 4;
        }
        op2Var.P(e2);
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x0165  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void f(defpackage.op2 r22, int r23, int r24, int r25, int r26, java.lang.String r27, boolean r28, androidx.media3.common.DrmInitData r29, defpackage.aj.d r30, int r31) throws androidx.media3.common.ParserException {
        /*
            Method dump skipped, instructions count: 862
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.aj.f(op2, int, int, int, int, java.lang.String, boolean, androidx.media3.common.DrmInitData, aj$d, int):void");
    }

    public static Pair<Integer, x74> g(op2 op2Var, int i, int i2) throws ParserException {
        int i3 = i + 8;
        int i4 = -1;
        String str = null;
        Integer num = null;
        int i5 = 0;
        while (i3 - i < i2) {
            op2Var.P(i3);
            int n = op2Var.n();
            int n2 = op2Var.n();
            if (n2 == 1718775137) {
                num = Integer.valueOf(op2Var.n());
            } else if (n2 == 1935894637) {
                op2Var.Q(4);
                str = op2Var.A(4);
            } else if (n2 == 1935894633) {
                i4 = i3;
                i5 = n;
            }
            i3 += n;
        }
        if ("cenc".equals(str) || "cbc1".equals(str) || "cens".equals(str) || "cbcs".equals(str)) {
            s11.a(num != null, "frma atom is mandatory");
            s11.a(i4 != -1, "schi atom is mandatory");
            x74 t = t(op2Var, i4, i5, str);
            s11.a(t != null, "tenc atom is mandatory");
            return Pair.create(num, (x74) androidx.media3.common.util.b.j(t));
        }
        return null;
    }

    public static Pair<long[], long[]> h(zi.a aVar) {
        zi.b g2 = aVar.g(1701606260);
        if (g2 == null) {
            return null;
        }
        op2 op2Var = g2.b;
        op2Var.P(8);
        int c2 = zi.c(op2Var.n());
        int H = op2Var.H();
        long[] jArr = new long[H];
        long[] jArr2 = new long[H];
        for (int i = 0; i < H; i++) {
            jArr[i] = c2 == 1 ? op2Var.I() : op2Var.F();
            jArr2[i] = c2 == 1 ? op2Var.w() : op2Var.n();
            if (op2Var.z() == 1) {
                op2Var.Q(2);
            } else {
                throw new IllegalArgumentException("Unsupported media rate.");
            }
        }
        return Pair.create(jArr, jArr2);
    }

    public static b i(op2 op2Var, int i) {
        op2Var.P(i + 8 + 4);
        op2Var.Q(1);
        j(op2Var);
        op2Var.Q(2);
        int D = op2Var.D();
        if ((D & 128) != 0) {
            op2Var.Q(2);
        }
        if ((D & 64) != 0) {
            op2Var.Q(op2Var.D());
        }
        if ((D & 32) != 0) {
            op2Var.Q(2);
        }
        op2Var.Q(1);
        j(op2Var);
        String f2 = y82.f(op2Var.D());
        if (!"audio/mpeg".equals(f2) && !"audio/vnd.dts".equals(f2) && !"audio/vnd.dts.hd".equals(f2)) {
            op2Var.Q(4);
            long F = op2Var.F();
            long F2 = op2Var.F();
            op2Var.Q(1);
            int j = j(op2Var);
            byte[] bArr = new byte[j];
            op2Var.j(bArr, 0, j);
            return new b(f2, bArr, F2 > 0 ? F2 : -1L, F > 0 ? F : -1L);
        }
        return new b(f2, null, -1L, -1L);
    }

    public static int j(op2 op2Var) {
        int D = op2Var.D();
        int i = D & 127;
        while ((D & 128) == 128) {
            D = op2Var.D();
            i = (i << 7) | (D & 127);
        }
        return i;
    }

    public static int k(op2 op2Var) {
        op2Var.P(16);
        return op2Var.n();
    }

    public static Metadata l(op2 op2Var, int i) {
        op2Var.Q(8);
        ArrayList arrayList = new ArrayList();
        while (op2Var.e() < i) {
            Metadata.Entry c2 = q82.c(op2Var);
            if (c2 != null) {
                arrayList.add(c2);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata(arrayList);
    }

    public static Pair<Long, String> m(op2 op2Var) {
        op2Var.P(8);
        int c2 = zi.c(op2Var.n());
        op2Var.Q(c2 == 0 ? 8 : 16);
        long F = op2Var.F();
        op2Var.Q(c2 == 0 ? 4 : 8);
        int J = op2Var.J();
        return Pair.create(Long.valueOf(F), "" + ((char) (((J >> 10) & 31) + 96)) + ((char) (((J >> 5) & 31) + 96)) + ((char) ((J & 31) + 96)));
    }

    public static Metadata n(zi.a aVar) {
        zi.b g2 = aVar.g(1751411826);
        zi.b g3 = aVar.g(1801812339);
        zi.b g4 = aVar.g(1768715124);
        if (g2 == null || g3 == null || g4 == null || k(g2.b) != 1835299937) {
            return null;
        }
        op2 op2Var = g3.b;
        op2Var.P(12);
        int n = op2Var.n();
        String[] strArr = new String[n];
        for (int i = 0; i < n; i++) {
            int n2 = op2Var.n();
            op2Var.Q(4);
            strArr[i] = op2Var.A(n2 - 8);
        }
        op2 op2Var2 = g4.b;
        op2Var2.P(8);
        ArrayList arrayList = new ArrayList();
        while (op2Var2.a() > 8) {
            int e2 = op2Var2.e();
            int n3 = op2Var2.n();
            int n4 = op2Var2.n() - 1;
            if (n4 >= 0 && n4 < n) {
                MdtaMetadataEntry f2 = q82.f(op2Var2, e2 + n3, strArr[n4]);
                if (f2 != null) {
                    arrayList.add(f2);
                }
            } else {
                p12.i("AtomParsers", "Skipped metadata with unknown key index: " + n4);
            }
            op2Var2.P(e2 + n3);
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata(arrayList);
    }

    public static void o(op2 op2Var, int i, int i2, int i3, d dVar) {
        op2Var.P(i2 + 8 + 8);
        if (i == 1835365492) {
            op2Var.x();
            String x = op2Var.x();
            if (x != null) {
                dVar.b = new j.b().R(i3).e0(x).E();
            }
        }
    }

    public static long p(op2 op2Var) {
        op2Var.P(8);
        op2Var.Q(zi.c(op2Var.n()) != 0 ? 16 : 8);
        return op2Var.F();
    }

    public static float q(op2 op2Var, int i) {
        op2Var.P(i + 8);
        return op2Var.H() / op2Var.H();
    }

    public static byte[] r(op2 op2Var, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            op2Var.P(i3);
            int n = op2Var.n();
            if (op2Var.n() == 1886547818) {
                return Arrays.copyOfRange(op2Var.d(), i3, n + i3);
            }
            i3 += n;
        }
        return null;
    }

    public static Pair<Integer, x74> s(op2 op2Var, int i, int i2) throws ParserException {
        Pair<Integer, x74> g2;
        int e2 = op2Var.e();
        while (e2 - i < i2) {
            op2Var.P(e2);
            int n = op2Var.n();
            s11.a(n > 0, "childAtomSize must be positive");
            if (op2Var.n() == 1936289382 && (g2 = g(op2Var, e2, n)) != null) {
                return g2;
            }
            e2 += n;
        }
        return null;
    }

    public static x74 t(op2 op2Var, int i, int i2, String str) {
        int i3;
        int i4;
        int i5 = i + 8;
        while (true) {
            byte[] bArr = null;
            if (i5 - i >= i2) {
                return null;
            }
            op2Var.P(i5);
            int n = op2Var.n();
            if (op2Var.n() == 1952804451) {
                int c2 = zi.c(op2Var.n());
                op2Var.Q(1);
                if (c2 == 0) {
                    op2Var.Q(1);
                    i4 = 0;
                    i3 = 0;
                } else {
                    int D = op2Var.D();
                    i3 = D & 15;
                    i4 = (D & 240) >> 4;
                }
                boolean z = op2Var.D() == 1;
                int D2 = op2Var.D();
                byte[] bArr2 = new byte[16];
                op2Var.j(bArr2, 0, 16);
                if (z && D2 == 0) {
                    int D3 = op2Var.D();
                    bArr = new byte[D3];
                    op2Var.j(bArr, 0, D3);
                }
                return new x74(z, str, D2, bArr2, i4, i3, bArr);
            }
            i5 += n;
        }
    }

    public static Metadata u(op2 op2Var, int i) {
        op2Var.Q(12);
        while (op2Var.e() < i) {
            int e2 = op2Var.e();
            int n = op2Var.n();
            if (op2Var.n() == 1935766900) {
                if (n < 14) {
                    return null;
                }
                op2Var.Q(5);
                int D = op2Var.D();
                if (D == 12 || D == 13) {
                    float f2 = D == 12 ? 240.0f : 120.0f;
                    op2Var.Q(1);
                    return new Metadata(new SmtaMetadataEntry(f2, op2Var.D()));
                }
                return null;
            }
            op2Var.P(e2 + n);
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:149:0x03b4  */
    /* JADX WARN: Removed duplicated region for block: B:150:0x03b6  */
    /* JADX WARN: Removed duplicated region for block: B:154:0x03ce  */
    /* JADX WARN: Removed duplicated region for block: B:172:0x043e  */
    /* JADX WARN: Removed duplicated region for block: B:175:0x0443  */
    /* JADX WARN: Removed duplicated region for block: B:176:0x0446  */
    /* JADX WARN: Removed duplicated region for block: B:178:0x0449  */
    /* JADX WARN: Removed duplicated region for block: B:179:0x044c  */
    /* JADX WARN: Removed duplicated region for block: B:181:0x044f  */
    /* JADX WARN: Removed duplicated region for block: B:182:0x0451  */
    /* JADX WARN: Removed duplicated region for block: B:184:0x0455  */
    /* JADX WARN: Removed duplicated region for block: B:185:0x0458  */
    /* JADX WARN: Removed duplicated region for block: B:189:0x0467  */
    /* JADX WARN: Removed duplicated region for block: B:210:0x0433 A[EDGE_INSN: B:210:0x0433->B:169:0x0433 ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.g84 v(defpackage.w74 r38, defpackage.zi.a r39, defpackage.qe1 r40) throws androidx.media3.common.ParserException {
        /*
            Method dump skipped, instructions count: 1321
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.aj.v(w74, zi$a, qe1):g84");
    }

    public static d w(op2 op2Var, int i, int i2, String str, DrmInitData drmInitData, boolean z) throws ParserException {
        int i3;
        op2Var.P(12);
        int n = op2Var.n();
        d dVar = new d(n);
        for (int i4 = 0; i4 < n; i4++) {
            int e2 = op2Var.e();
            int n2 = op2Var.n();
            s11.a(n2 > 0, "childAtomSize must be positive");
            int n3 = op2Var.n();
            if (n3 == 1635148593 || n3 == 1635148595 || n3 == 1701733238 || n3 == 1831958048 || n3 == 1836070006 || n3 == 1752589105 || n3 == 1751479857 || n3 == 1932670515 || n3 == 1211250227 || n3 == 1987063864 || n3 == 1987063865 || n3 == 1635135537 || n3 == 1685479798 || n3 == 1685479729 || n3 == 1685481573 || n3 == 1685481521) {
                i3 = e2;
                D(op2Var, n3, i3, n2, i, i2, drmInitData, dVar, i4);
            } else if (n3 == 1836069985 || n3 == 1701733217 || n3 == 1633889587 || n3 == 1700998451 || n3 == 1633889588 || n3 == 1835823201 || n3 == 1685353315 || n3 == 1685353317 || n3 == 1685353320 || n3 == 1685353324 || n3 == 1685353336 || n3 == 1935764850 || n3 == 1935767394 || n3 == 1819304813 || n3 == 1936684916 || n3 == 1953984371 || n3 == 778924082 || n3 == 778924083 || n3 == 1835557169 || n3 == 1835560241 || n3 == 1634492771 || n3 == 1634492791 || n3 == 1970037111 || n3 == 1332770163 || n3 == 1716281667) {
                i3 = e2;
                f(op2Var, n3, e2, n2, i, str, z, drmInitData, dVar, i4);
            } else {
                if (n3 == 1414810956 || n3 == 1954034535 || n3 == 2004251764 || n3 == 1937010800 || n3 == 1664495672) {
                    x(op2Var, n3, e2, n2, i, str, dVar);
                } else if (n3 == 1835365492) {
                    o(op2Var, n3, e2, i, dVar);
                } else if (n3 == 1667329389) {
                    dVar.b = new j.b().R(i).e0("application/x-camera-motion").E();
                }
                i3 = e2;
            }
            op2Var.P(i3 + n2);
        }
        return dVar;
    }

    public static void x(op2 op2Var, int i, int i2, int i3, int i4, String str, d dVar) {
        op2Var.P(i2 + 8 + 8);
        String str2 = "application/ttml+xml";
        ImmutableList immutableList = null;
        long j = Long.MAX_VALUE;
        if (i != 1414810956) {
            if (i == 1954034535) {
                int i5 = (i3 - 8) - 8;
                byte[] bArr = new byte[i5];
                op2Var.j(bArr, 0, i5);
                immutableList = ImmutableList.of(bArr);
                str2 = "application/x-quicktime-tx3g";
            } else if (i == 2004251764) {
                str2 = "application/x-mp4-vtt";
            } else if (i == 1937010800) {
                j = 0;
            } else if (i == 1664495672) {
                dVar.d = 1;
                str2 = "application/x-mp4-cea-608";
            } else {
                throw new IllegalStateException();
            }
        }
        dVar.b = new j.b().R(i4).e0(str2).V(str).i0(j).T(immutableList).E();
    }

    public static g y(op2 op2Var) {
        boolean z;
        op2Var.P(8);
        int c2 = zi.c(op2Var.n());
        op2Var.Q(c2 == 0 ? 8 : 16);
        int n = op2Var.n();
        op2Var.Q(4);
        int e2 = op2Var.e();
        int i = c2 == 0 ? 4 : 8;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i3 >= i) {
                z = true;
                break;
            } else if (op2Var.d()[e2 + i3] != -1) {
                z = false;
                break;
            } else {
                i3++;
            }
        }
        long j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        if (z) {
            op2Var.Q(i);
        } else {
            long F = c2 == 0 ? op2Var.F() : op2Var.I();
            if (F != 0) {
                j = F;
            }
        }
        op2Var.Q(16);
        int n2 = op2Var.n();
        int n3 = op2Var.n();
        op2Var.Q(4);
        int n4 = op2Var.n();
        int n5 = op2Var.n();
        if (n2 == 0 && n3 == 65536 && n4 == -65536 && n5 == 0) {
            i2 = 90;
        } else if (n2 == 0 && n3 == -65536 && n4 == 65536 && n5 == 0) {
            i2 = 270;
        } else if (n2 == -65536 && n3 == 0 && n4 == 0 && n5 == -65536) {
            i2 = 180;
        }
        return new g(n, j, i2);
    }

    public static w74 z(zi.a aVar, zi.b bVar, long j, DrmInitData drmInitData, boolean z, boolean z2) throws ParserException {
        zi.b bVar2;
        long j2;
        long[] jArr;
        long[] jArr2;
        zi.a f2;
        Pair<long[], long[]> h;
        zi.a aVar2 = (zi.a) ii.e(aVar.f(1835297121));
        int d2 = d(k(((zi.b) ii.e(aVar2.g(1751411826))).b));
        if (d2 == -1) {
            return null;
        }
        g y = y(((zi.b) ii.e(aVar.g(1953196132))).b);
        long j3 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            bVar2 = bVar;
            j2 = y.b;
        } else {
            bVar2 = bVar;
            j2 = j;
        }
        long p = p(bVar2.b);
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            j3 = androidx.media3.common.util.b.J0(j2, 1000000L, p);
        }
        long j4 = j3;
        Pair<Long, String> m = m(((zi.b) ii.e(aVar2.g(1835296868))).b);
        d w = w(((zi.b) ii.e(((zi.a) ii.e(((zi.a) ii.e(aVar2.f(1835626086))).f(1937007212))).g(1937011556))).b, y.a, y.c, (String) m.second, drmInitData, z2);
        if (z || (f2 = aVar.f(1701082227)) == null || (h = h(f2)) == null) {
            jArr = null;
            jArr2 = null;
        } else {
            jArr2 = (long[]) h.second;
            jArr = (long[]) h.first;
        }
        if (w.b == null) {
            return null;
        }
        return new w74(y.a, d2, ((Long) m.first).longValue(), p, j4, w.b, w.d, w.a, w.c, jArr, jArr2);
    }
}
