package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.fragments.ReflectionsFragment;

/* renamed from: w53  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class w53 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ w53 a = new w53();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        ReflectionsFragment.I(dialogInterface);
    }
}
