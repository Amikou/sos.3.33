package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;

/* compiled from: IdGenerator.java */
/* renamed from: kn1  reason: default package */
/* loaded from: classes.dex */
public class kn1 {
    public final WorkDatabase a;

    public kn1(WorkDatabase workDatabase) {
        this.a = workDatabase;
    }

    public static void a(Context context, sw3 sw3Var) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.id", 0);
        if (sharedPreferences.contains("next_job_scheduler_id") || sharedPreferences.contains("next_job_scheduler_id")) {
            int i = sharedPreferences.getInt("next_job_scheduler_id", 0);
            int i2 = sharedPreferences.getInt("next_alarm_manager_id", 0);
            sw3Var.B();
            try {
                sw3Var.u0("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"next_job_scheduler_id", Integer.valueOf(i)});
                sw3Var.u0("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"next_alarm_manager_id", Integer.valueOf(i2)});
                sharedPreferences.edit().clear().apply();
                sw3Var.s0();
            } finally {
                sw3Var.N0();
            }
        }
    }

    public int b() {
        int c;
        synchronized (kn1.class) {
            c = c("next_alarm_manager_id");
        }
        return c;
    }

    public final int c(String str) {
        this.a.e();
        try {
            Long a = this.a.L().a(str);
            int i = 0;
            int intValue = a != null ? a.intValue() : 0;
            if (intValue != Integer.MAX_VALUE) {
                i = intValue + 1;
            }
            e(str, i);
            this.a.E();
            return intValue;
        } finally {
            this.a.j();
        }
    }

    public int d(int i, int i2) {
        synchronized (kn1.class) {
            int c = c("next_job_scheduler_id");
            if (c >= i && c <= i2) {
                i = c;
            }
            e("next_job_scheduler_id", i + 1);
        }
        return i;
    }

    public final void e(String str, int i) {
        this.a.L().b(new hu2(str, i));
    }
}
