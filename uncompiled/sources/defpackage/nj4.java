package defpackage;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;

/* compiled from: ViewOverlayApi18.java */
/* renamed from: nj4  reason: default package */
/* loaded from: classes2.dex */
public class nj4 implements pj4 {
    public final ViewOverlay a;

    public nj4(View view) {
        this.a = view.getOverlay();
    }

    @Override // defpackage.pj4
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @Override // defpackage.pj4
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }
}
