package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: IAllTokenListRepository.kt */
/* renamed from: pl1  reason: default package */
/* loaded from: classes2.dex */
public interface pl1 {
    List<IToken> a(TokenType tokenType);

    LiveData<List<IToken>> b(TokenType tokenType, String str);
}
