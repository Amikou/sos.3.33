package defpackage;

import android.os.SystemClock;
import androidx.media3.exoplayer.upstream.Loader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import zendesk.support.request.CellBase;

/* compiled from: SntpClient.java */
/* renamed from: zq3  reason: default package */
/* loaded from: classes.dex */
public final class zq3 {
    public static final Object a = new Object();
    public static final Object b = new Object();
    public static boolean c = false;
    public static long d = 0;
    public static String e = "time.android.com";

    /* compiled from: SntpClient.java */
    /* renamed from: zq3$b */
    /* loaded from: classes.dex */
    public interface b {
        void a(IOException iOException);

        void b();
    }

    /* compiled from: SntpClient.java */
    /* renamed from: zq3$c */
    /* loaded from: classes.dex */
    public static final class c implements Loader.b<Loader.e> {
        public final b a;

        public c(b bVar) {
            this.a = bVar;
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        public Loader.c j(Loader.e eVar, long j, long j2, IOException iOException, int i) {
            b bVar = this.a;
            if (bVar != null) {
                bVar.a(iOException);
            }
            return Loader.d;
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        public void s(Loader.e eVar, long j, long j2) {
            if (this.a != null) {
                if (!zq3.k()) {
                    this.a.a(new IOException(new ConcurrentModificationException()));
                } else {
                    this.a.b();
                }
            }
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.b
        public void u(Loader.e eVar, long j, long j2, boolean z) {
        }
    }

    /* compiled from: SntpClient.java */
    /* renamed from: zq3$d */
    /* loaded from: classes.dex */
    public static final class d implements Loader.e {
        public d() {
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.e
        public void a() throws IOException {
            synchronized (zq3.a) {
                synchronized (zq3.b) {
                    if (zq3.c) {
                        return;
                    }
                    long l = zq3.l();
                    synchronized (zq3.b) {
                        long unused = zq3.d = l;
                        boolean unused2 = zq3.c = true;
                    }
                }
            }
        }

        @Override // androidx.media3.exoplayer.upstream.Loader.e
        public void c() {
        }
    }

    public static void g(byte b2, byte b3, int i, long j) throws IOException {
        if (b2 == 3) {
            throw new IOException("SNTP: Unsynchronized server");
        }
        if (b3 != 4 && b3 != 5) {
            throw new IOException("SNTP: Untrusted mode: " + ((int) b3));
        } else if (i != 0 && i <= 15) {
            if (j == 0) {
                throw new IOException("SNTP: Zero transmitTime");
            }
        } else {
            throw new IOException("SNTP: Untrusted stratum: " + i);
        }
    }

    public static long h() {
        long j;
        synchronized (b) {
            j = c ? d : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return j;
    }

    public static String i() {
        String str;
        synchronized (b) {
            str = e;
        }
        return str;
    }

    public static void j(Loader loader, b bVar) {
        if (k()) {
            if (bVar != null) {
                bVar.b();
                return;
            }
            return;
        }
        if (loader == null) {
            loader = new Loader("SntpClient");
        }
        loader.n(new d(), new c(bVar), 1);
    }

    public static boolean k() {
        boolean z;
        synchronized (b) {
            z = c;
        }
        return z;
    }

    public static long l() throws IOException {
        InetAddress byName = InetAddress.getByName(i());
        DatagramSocket datagramSocket = new DatagramSocket();
        try {
            datagramSocket.setSoTimeout(10000);
            byte[] bArr = new byte[48];
            DatagramPacket datagramPacket = new DatagramPacket(bArr, 48, byName, 123);
            bArr[0] = 27;
            long currentTimeMillis = System.currentTimeMillis();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            o(bArr, 40, currentTimeMillis);
            datagramSocket.send(datagramPacket);
            datagramSocket.receive(new DatagramPacket(bArr, 48));
            long elapsedRealtime2 = SystemClock.elapsedRealtime();
            long j = currentTimeMillis + (elapsedRealtime2 - elapsedRealtime);
            long n = n(bArr, 24);
            long n2 = n(bArr, 32);
            long n3 = n(bArr, 40);
            g((byte) ((bArr[0] >> 6) & 3), (byte) (bArr[0] & 7), bArr[1] & 255, n3);
            long j2 = (j + (((n2 - n) + (n3 - j)) / 2)) - elapsedRealtime2;
            datagramSocket.close();
            return j2;
        } catch (Throwable th) {
            try {
                datagramSocket.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static long m(byte[] bArr, int i) {
        int i2 = bArr[i];
        int i3 = bArr[i + 1];
        int i4 = bArr[i + 2];
        int i5 = bArr[i + 3];
        if ((i2 & 128) == 128) {
            i2 = (i2 & 127) + 128;
        }
        if ((i3 & 128) == 128) {
            i3 = (i3 & 127) + 128;
        }
        if ((i4 & 128) == 128) {
            i4 = (i4 & 127) + 128;
        }
        if ((i5 & 128) == 128) {
            i5 = (i5 & 127) + 128;
        }
        return (i2 << 24) + (i3 << 16) + (i4 << 8) + i5;
    }

    public static long n(byte[] bArr, int i) {
        long m = m(bArr, i);
        long m2 = m(bArr, i + 4);
        if (m == 0 && m2 == 0) {
            return 0L;
        }
        return ((m - 2208988800L) * 1000) + ((m2 * 1000) / 4294967296L);
    }

    public static void o(byte[] bArr, int i, long j) {
        if (j == 0) {
            Arrays.fill(bArr, i, i + 8, (byte) 0);
            return;
        }
        long j2 = j / 1000;
        long j3 = j2 + 2208988800L;
        int i2 = i + 1;
        bArr[i] = (byte) (j3 >> 24);
        int i3 = i2 + 1;
        bArr[i2] = (byte) (j3 >> 16);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (j3 >> 8);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (j3 >> 0);
        long j4 = ((j - (j2 * 1000)) * 4294967296L) / 1000;
        int i6 = i5 + 1;
        bArr[i5] = (byte) (j4 >> 24);
        int i7 = i6 + 1;
        bArr[i6] = (byte) (j4 >> 16);
        bArr[i7] = (byte) (j4 >> 8);
        bArr[i7 + 1] = (byte) (Math.random() * 255.0d);
    }
}
