package defpackage;

import com.google.android.gms.measurement.internal.h;
import java.lang.Thread;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: uj5  reason: default package */
/* loaded from: classes.dex */
public final class uj5 implements Thread.UncaughtExceptionHandler {
    public final String a;
    public final /* synthetic */ h b;

    public uj5(h hVar, String str) {
        this.b = hVar;
        zt2.j(str);
        this.a = str;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.b.a.w().l().b(this.a, th);
    }
}
