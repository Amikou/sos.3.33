package defpackage;

import java.util.Arrays;

/* compiled from: Segment.kt */
/* renamed from: bj3  reason: default package */
/* loaded from: classes2.dex */
public final class bj3 {
    public final byte[] a;
    public int b;
    public int c;
    public boolean d;
    public boolean e;
    public bj3 f;
    public bj3 g;

    /* compiled from: Segment.kt */
    /* renamed from: bj3$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public bj3() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    public final void a() {
        bj3 bj3Var = this.g;
        int i = 0;
        if (bj3Var != this) {
            fs1.d(bj3Var);
            if (bj3Var.e) {
                int i2 = this.c - this.b;
                bj3 bj3Var2 = this.g;
                fs1.d(bj3Var2);
                int i3 = 8192 - bj3Var2.c;
                bj3 bj3Var3 = this.g;
                fs1.d(bj3Var3);
                if (!bj3Var3.d) {
                    bj3 bj3Var4 = this.g;
                    fs1.d(bj3Var4);
                    i = bj3Var4.b;
                }
                if (i2 > i3 + i) {
                    return;
                }
                bj3 bj3Var5 = this.g;
                fs1.d(bj3Var5);
                g(bj3Var5, i2);
                b();
                dj3.b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("cannot compact".toString());
    }

    public final bj3 b() {
        bj3 bj3Var = this.f;
        if (bj3Var == this) {
            bj3Var = null;
        }
        bj3 bj3Var2 = this.g;
        fs1.d(bj3Var2);
        bj3Var2.f = this.f;
        bj3 bj3Var3 = this.f;
        fs1.d(bj3Var3);
        bj3Var3.g = this.g;
        this.f = null;
        this.g = null;
        return bj3Var;
    }

    public final bj3 c(bj3 bj3Var) {
        fs1.f(bj3Var, "segment");
        bj3Var.g = this;
        bj3Var.f = this.f;
        bj3 bj3Var2 = this.f;
        fs1.d(bj3Var2);
        bj3Var2.g = bj3Var;
        this.f = bj3Var;
        return bj3Var;
    }

    public final bj3 d() {
        this.d = true;
        return new bj3(this.a, this.b, this.c, true, false);
    }

    public final bj3 e(int i) {
        bj3 c;
        if (i > 0 && i <= this.c - this.b) {
            if (i >= 1024) {
                c = d();
            } else {
                c = dj3.c();
                byte[] bArr = this.a;
                byte[] bArr2 = c.a;
                int i2 = this.b;
                zh.f(bArr, bArr2, 0, i2, i2 + i, 2, null);
            }
            c.c = c.b + i;
            this.b += i;
            bj3 bj3Var = this.g;
            fs1.d(bj3Var);
            bj3Var.c(c);
            return c;
        }
        throw new IllegalArgumentException("byteCount out of range".toString());
    }

    public final bj3 f() {
        byte[] bArr = this.a;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        fs1.e(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new bj3(copyOf, this.b, this.c, false, true);
    }

    public final void g(bj3 bj3Var, int i) {
        fs1.f(bj3Var, "sink");
        if (bj3Var.e) {
            int i2 = bj3Var.c;
            if (i2 + i > 8192) {
                if (!bj3Var.d) {
                    int i3 = bj3Var.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = bj3Var.a;
                        zh.f(bArr, bArr, 0, i3, i2, 2, null);
                        bj3Var.c -= bj3Var.b;
                        bj3Var.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            byte[] bArr2 = this.a;
            byte[] bArr3 = bj3Var.a;
            int i4 = bj3Var.c;
            int i5 = this.b;
            zh.d(bArr2, bArr3, i4, i5, i5 + i);
            bj3Var.c += i;
            this.b += i;
            return;
        }
        throw new IllegalStateException("only owner can write".toString());
    }

    public bj3(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        fs1.f(bArr, "data");
        this.a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }
}
