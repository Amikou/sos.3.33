package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import java.util.Collections;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: e05  reason: default package */
/* loaded from: classes.dex */
public final class e05 implements j05 {
    public final i05 a;

    public e05(i05 i05Var) {
        this.a = i05Var;
    }

    @Override // defpackage.j05
    public final void a() {
        for (a.f fVar : this.a.f.values()) {
            fVar.b();
        }
        this.a.m.p = Collections.emptySet();
    }

    @Override // defpackage.j05
    public final void b(ConnectionResult connectionResult, a<?> aVar, boolean z) {
    }

    @Override // defpackage.j05
    public final void c() {
        this.a.m();
    }

    @Override // defpackage.j05
    public final void d(Bundle bundle) {
    }

    @Override // defpackage.j05
    public final void e(int i) {
    }

    @Override // defpackage.j05
    public final boolean f() {
        return true;
    }

    @Override // defpackage.j05
    public final <A extends a.b, T extends b<? extends l83, A>> T g(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }
}
