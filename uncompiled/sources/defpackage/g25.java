package defpackage;

import android.app.Dialog;
import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiActivity;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: g25  reason: default package */
/* loaded from: classes.dex */
public final class g25 implements Runnable {
    public final j25 a;
    public final /* synthetic */ d25 f0;

    public g25(d25 d25Var, j25 j25Var) {
        this.f0 = d25Var;
        this.a = j25Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        if (this.f0.f0) {
            ConnectionResult b = this.a.b();
            if (b.L1()) {
                d25 d25Var = this.f0;
                d25Var.a.startActivityForResult(GoogleApiActivity.b(d25Var.b(), (PendingIntent) zt2.j(b.K1()), this.a.a(), false), 1);
                return;
            }
            d25 d25Var2 = this.f0;
            if (d25Var2.i0.d(d25Var2.b(), b.I1(), null) != null) {
                d25 d25Var3 = this.f0;
                d25Var3.i0.A(d25Var3.b(), this.f0.a, b.I1(), 2, this.f0);
            } else if (b.I1() == 18) {
                Dialog t = dh1.t(this.f0.b(), this.f0);
                d25 d25Var4 = this.f0;
                d25Var4.i0.v(d25Var4.b().getApplicationContext(), new l25(this, t));
            } else {
                this.f0.m(b, this.a.a());
            }
        }
    }
}
