package defpackage;

/* compiled from: InstrumentedMemoryCacheBitmapMemoryCacheFactory.java */
/* renamed from: kr1  reason: default package */
/* loaded from: classes.dex */
public class kr1 {

    /* compiled from: InstrumentedMemoryCacheBitmapMemoryCacheFactory.java */
    /* renamed from: kr1$a */
    /* loaded from: classes.dex */
    public static class a implements n72<wt> {
        public final /* synthetic */ qn1 a;

        public a(qn1 qn1Var) {
            this.a = qn1Var;
        }

        @Override // defpackage.n72
        /* renamed from: d */
        public void a(wt wtVar) {
            this.a.i(wtVar);
        }

        @Override // defpackage.n72
        /* renamed from: e */
        public void b(wt wtVar) {
            this.a.c(wtVar);
        }

        @Override // defpackage.n72
        /* renamed from: f */
        public void c(wt wtVar) {
            this.a.n(wtVar);
        }
    }

    public static jr1<wt, com.facebook.imagepipeline.image.a> a(l72<wt, com.facebook.imagepipeline.image.a> l72Var, qn1 qn1Var) {
        qn1Var.j(l72Var);
        return new jr1<>(l72Var, new a(qn1Var));
    }
}
