package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: gf3  reason: default package */
/* loaded from: classes2.dex */
public class gf3 extends ct0.b {
    public static final BigInteger g = ef3.j;
    public int[] f;

    public gf3() {
        this.f = ed2.h();
    }

    public gf3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP256K1FieldElement");
        }
        this.f = ff3.c(bigInteger);
    }

    public gf3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] h = ed2.h();
        ff3.a(this.f, ((gf3) ct0Var).f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] h = ed2.h();
        ff3.b(this.f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] h = ed2.h();
        g92.d(ff3.a, ((gf3) ct0Var).f, h);
        ff3.d(h, this.f, h);
        return new gf3(h);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof gf3) {
            return ed2.m(this.f, ((gf3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] h = ed2.h();
        g92.d(ff3.a, this.f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ed2.t(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 8);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ed2.v(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] h = ed2.h();
        ff3.d(this.f, ((gf3) ct0Var).f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] h = ed2.h();
        ff3.f(this.f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (ed2.v(iArr) || ed2.t(iArr)) {
            return this;
        }
        int[] h = ed2.h();
        ff3.i(iArr, h);
        ff3.d(h, iArr, h);
        int[] h2 = ed2.h();
        ff3.i(h, h2);
        ff3.d(h2, iArr, h2);
        int[] h3 = ed2.h();
        ff3.j(h2, 3, h3);
        ff3.d(h3, h2, h3);
        ff3.j(h3, 3, h3);
        ff3.d(h3, h2, h3);
        ff3.j(h3, 2, h3);
        ff3.d(h3, h, h3);
        int[] h4 = ed2.h();
        ff3.j(h3, 11, h4);
        ff3.d(h4, h3, h4);
        ff3.j(h4, 22, h3);
        ff3.d(h3, h4, h3);
        int[] h5 = ed2.h();
        ff3.j(h3, 44, h5);
        ff3.d(h5, h3, h5);
        int[] h6 = ed2.h();
        ff3.j(h5, 88, h6);
        ff3.d(h6, h5, h6);
        ff3.j(h6, 44, h5);
        ff3.d(h5, h3, h5);
        ff3.j(h5, 3, h3);
        ff3.d(h3, h2, h3);
        ff3.j(h3, 23, h3);
        ff3.d(h3, h4, h3);
        ff3.j(h3, 6, h3);
        ff3.d(h3, h, h3);
        ff3.j(h3, 2, h3);
        ff3.i(h3, h);
        if (ed2.m(iArr, h)) {
            return new gf3(h3);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] h = ed2.h();
        ff3.i(this.f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] h = ed2.h();
        ff3.k(this.f, ((gf3) ct0Var).f, h);
        return new gf3(h);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return ed2.q(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ed2.J(this.f);
    }
}
