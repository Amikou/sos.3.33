package defpackage;

import androidx.recyclerview.widget.g;
import java.util.Collection;
import java.util.Iterator;

/* compiled from: NullPaddedListDiffHelper.kt */
/* renamed from: wi2  reason: default package */
/* loaded from: classes.dex */
public final class wi2 {

    /* compiled from: NullPaddedListDiffHelper.kt */
    /* renamed from: wi2$a */
    /* loaded from: classes.dex */
    public static final class a extends g.b {
        public final /* synthetic */ vi2 a;
        public final /* synthetic */ vi2 b;
        public final /* synthetic */ g.f c;
        public final /* synthetic */ int d;
        public final /* synthetic */ int e;

        public a(vi2<T> vi2Var, vi2 vi2Var2, g.f fVar, int i, int i2) {
            this.a = vi2Var;
            this.b = vi2Var2;
            this.c = fVar;
            this.d = i;
            this.e = i2;
        }

        @Override // androidx.recyclerview.widget.g.b
        public boolean areContentsTheSame(int i, int i2) {
            Object e = this.a.e(i);
            Object e2 = this.b.e(i2);
            if (e == e2) {
                return true;
            }
            return this.c.areContentsTheSame(e, e2);
        }

        @Override // androidx.recyclerview.widget.g.b
        public boolean areItemsTheSame(int i, int i2) {
            Object e = this.a.e(i);
            Object e2 = this.b.e(i2);
            if (e == e2) {
                return true;
            }
            return this.c.areItemsTheSame(e, e2);
        }

        @Override // androidx.recyclerview.widget.g.b
        public Object getChangePayload(int i, int i2) {
            Object e = this.a.e(i);
            Object e2 = this.b.e(i2);
            if (e == e2) {
                return Boolean.TRUE;
            }
            return this.c.getChangePayload(e, e2);
        }

        @Override // androidx.recyclerview.widget.g.b
        public int getNewListSize() {
            return this.e;
        }

        @Override // androidx.recyclerview.widget.g.b
        public int getOldListSize() {
            return this.d;
        }
    }

    public static final <T> ui2 a(vi2<T> vi2Var, vi2<T> vi2Var2, g.f<T> fVar) {
        boolean z;
        fs1.f(vi2Var, "$this$computeDiff");
        fs1.f(vi2Var2, "newList");
        fs1.f(fVar, "diffCallback");
        a aVar = new a(vi2Var, vi2Var2, fVar, vi2Var.b(), vi2Var2.b());
        boolean z2 = true;
        g.e c = g.c(aVar, true);
        fs1.e(c, "DiffUtil.calculateDiff(\n…    },\n        true\n    )");
        sr1 k = u33.k(0, vi2Var.b());
        if (!(k instanceof Collection) || !((Collection) k).isEmpty()) {
            Iterator<Integer> it = k.iterator();
            while (it.hasNext()) {
                if (c.b(((or1) it).b()) != -1) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
                if (z) {
                    break;
                }
            }
        }
        z2 = false;
        return new ui2(c, z2);
    }

    public static final <T> void b(vi2<T> vi2Var, i02 i02Var, vi2<T> vi2Var2, ui2 ui2Var) {
        fs1.f(vi2Var, "$this$dispatchDiff");
        fs1.f(i02Var, "callback");
        fs1.f(vi2Var2, "newList");
        fs1.f(ui2Var, "diffResult");
        if (ui2Var.b()) {
            eo2.a.a(vi2Var, vi2Var2, i02Var, ui2Var);
        } else {
            aq0.a.b(i02Var, vi2Var, vi2Var2);
        }
    }

    public static final int c(vi2<?> vi2Var, ui2 ui2Var, vi2<?> vi2Var2, int i) {
        int b;
        fs1.f(vi2Var, "$this$transformAnchorIndex");
        fs1.f(ui2Var, "diffResult");
        fs1.f(vi2Var2, "newList");
        if (!ui2Var.b()) {
            return u33.g(i, u33.k(0, vi2Var2.a()));
        }
        int c = i - vi2Var.c();
        int b2 = vi2Var.b();
        if (c >= 0 && b2 > c) {
            for (int i2 = 0; i2 <= 29; i2++) {
                int i3 = ((i2 / 2) * (i2 % 2 == 1 ? -1 : 1)) + c;
                if (i3 >= 0 && i3 < vi2Var.b() && (b = ui2Var.a().b(i3)) != -1) {
                    return b + vi2Var2.c();
                }
            }
        }
        return u33.g(i, u33.k(0, vi2Var2.a()));
    }
}
