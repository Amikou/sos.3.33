package defpackage;

import java.util.List;
import org.web3j.abi.TypeReference;

/* compiled from: EventEncoder.java */
/* renamed from: ux0  reason: default package */
/* loaded from: classes2.dex */
public class ux0 {
    private ux0() {
    }

    public static String buildEventSignature(String str) {
        return ej2.toHexString(ak1.sha3(str.getBytes()));
    }

    public static <T extends zc4> String buildMethodSignature(String str, List<TypeReference<T>> list) {
        return str + "(" + i10.join(list, ",", tx0.a) + ")";
    }

    public static String encode(px0 px0Var) {
        return buildEventSignature(buildMethodSignature(px0Var.getName(), px0Var.getParameters()));
    }
}
