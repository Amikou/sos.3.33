package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.LinearProgressIndicatorSpec;

/* compiled from: LinearDrawingDelegate.java */
/* renamed from: yz1  reason: default package */
/* loaded from: classes2.dex */
public final class yz1 extends nr0<LinearProgressIndicatorSpec> {
    public float c;
    public float d;
    public float e;

    public yz1(LinearProgressIndicatorSpec linearProgressIndicatorSpec) {
        super(linearProgressIndicatorSpec);
        this.c = 300.0f;
    }

    public static void h(Canvas canvas, Paint paint, float f, float f2, float f3, boolean z, RectF rectF) {
        canvas.save();
        canvas.translate(f3, Utils.FLOAT_EPSILON);
        if (!z) {
            canvas.rotate(180.0f);
        }
        float f4 = ((-f) / 2.0f) + f2;
        float f5 = (f / 2.0f) - f2;
        canvas.drawRect(-f2, f4, Utils.FLOAT_EPSILON, f5, paint);
        canvas.save();
        canvas.translate(Utils.FLOAT_EPSILON, f4);
        canvas.drawArc(rectF, 180.0f, 90.0f, true, paint);
        canvas.restore();
        canvas.translate(Utils.FLOAT_EPSILON, f5);
        canvas.drawArc(rectF, 180.0f, -90.0f, true, paint);
        canvas.restore();
    }

    @Override // defpackage.nr0
    public void a(Canvas canvas, float f) {
        Rect clipBounds = canvas.getClipBounds();
        this.c = clipBounds.width();
        float f2 = ((LinearProgressIndicatorSpec) this.a).a;
        canvas.translate(clipBounds.left + (clipBounds.width() / 2.0f), clipBounds.top + (clipBounds.height() / 2.0f) + Math.max((float) Utils.FLOAT_EPSILON, (clipBounds.height() - ((LinearProgressIndicatorSpec) this.a).a) / 2.0f));
        if (((LinearProgressIndicatorSpec) this.a).i) {
            canvas.scale(-1.0f, 1.0f);
        }
        if ((this.b.j() && ((LinearProgressIndicatorSpec) this.a).e == 1) || (this.b.i() && ((LinearProgressIndicatorSpec) this.a).f == 2)) {
            canvas.scale(1.0f, -1.0f);
        }
        if (this.b.j() || this.b.i()) {
            canvas.translate(Utils.FLOAT_EPSILON, (((LinearProgressIndicatorSpec) this.a).a * (f - 1.0f)) / 2.0f);
        }
        float f3 = this.c;
        canvas.clipRect((-f3) / 2.0f, (-f2) / 2.0f, f3 / 2.0f, f2 / 2.0f);
        S s = this.a;
        this.d = ((LinearProgressIndicatorSpec) s).a * f;
        this.e = ((LinearProgressIndicatorSpec) s).b * f;
    }

    @Override // defpackage.nr0
    public void b(Canvas canvas, Paint paint, float f, float f2, int i) {
        if (f == f2) {
            return;
        }
        float f3 = this.c;
        float f4 = this.e;
        float f5 = ((-f3) / 2.0f) + f4 + ((f3 - (f4 * 2.0f)) * f);
        float f6 = ((-f3) / 2.0f) + f4 + ((f3 - (f4 * 2.0f)) * f2);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(i);
        float f7 = this.d;
        canvas.drawRect(f5, (-f7) / 2.0f, f6, f7 / 2.0f, paint);
        float f8 = this.e;
        RectF rectF = new RectF(-f8, -f8, f8, f8);
        h(canvas, paint, this.d, this.e, f5, true, rectF);
        h(canvas, paint, this.d, this.e, f6, false, rectF);
    }

    @Override // defpackage.nr0
    public void c(Canvas canvas, Paint paint) {
        int a = l42.a(((LinearProgressIndicatorSpec) this.a).d, this.b.getAlpha());
        float f = ((-this.c) / 2.0f) + this.e;
        float f2 = -f;
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(a);
        float f3 = this.d;
        canvas.drawRect(f, (-f3) / 2.0f, f2, f3 / 2.0f, paint);
        float f4 = this.e;
        RectF rectF = new RectF(-f4, -f4, f4, f4);
        h(canvas, paint, this.d, this.e, f, true, rectF);
        h(canvas, paint, this.d, this.e, f2, false, rectF);
    }

    @Override // defpackage.nr0
    public int d() {
        return ((LinearProgressIndicatorSpec) this.a).a;
    }

    @Override // defpackage.nr0
    public int e() {
        return -1;
    }
}
