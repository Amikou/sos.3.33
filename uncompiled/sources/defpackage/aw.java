package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import defpackage.r93;

/* compiled from: CardViewApi17Impl.java */
/* renamed from: aw  reason: default package */
/* loaded from: classes.dex */
public class aw extends cw {

    /* compiled from: CardViewApi17Impl.java */
    /* renamed from: aw$a */
    /* loaded from: classes.dex */
    public class a implements r93.a {
        public a(aw awVar) {
        }

        @Override // defpackage.r93.a
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @Override // defpackage.cw, defpackage.ew
    public void l() {
        r93.r = new a(this);
    }
}
