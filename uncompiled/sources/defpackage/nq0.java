package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: SafeCollector.kt */
/* renamed from: nq0  reason: default package */
/* loaded from: classes2.dex */
public final class nq0 implements CoroutineContext.a {
    public static final a g0 = new a(null);
    public final Throwable a;
    public final CoroutineContext.b<?> f0 = g0;

    /* compiled from: SafeCollector.kt */
    /* renamed from: nq0$a */
    /* loaded from: classes2.dex */
    public static final class a implements CoroutineContext.b<nq0> {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public nq0(Throwable th) {
        this.a = th;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public <R> R fold(R r, hd1<? super R, ? super CoroutineContext.a, ? extends R> hd1Var) {
        return (R) CoroutineContext.a.C0196a.a(this, r, hd1Var);
    }

    @Override // kotlin.coroutines.CoroutineContext.a, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        return (E) CoroutineContext.a.C0196a.b(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext.a
    public CoroutineContext.b<?> getKey() {
        return this.f0;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        return CoroutineContext.a.C0196a.c(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        return CoroutineContext.a.C0196a.d(this, coroutineContext);
    }
}
