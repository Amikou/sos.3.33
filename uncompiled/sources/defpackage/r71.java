package defpackage;

import org.reactivestreams.Publisher;

/* compiled from: FlowableConcatArray.java */
/* renamed from: r71  reason: default package */
/* loaded from: classes2.dex */
public final class r71<T> extends q71<T> {
    public final Publisher<? extends T>[] b;

    public r71(Publisher<? extends T>[] publisherArr, boolean z) {
        this.b = publisherArr;
    }
}
