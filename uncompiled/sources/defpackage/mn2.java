package defpackage;

import java.security.MessageDigest;

/* compiled from: Option.java */
/* renamed from: mn2  reason: default package */
/* loaded from: classes.dex */
public final class mn2<T> {
    public static final b<Object> e = new a();
    public final T a;
    public final b<T> b;
    public final String c;
    public volatile byte[] d;

    /* compiled from: Option.java */
    /* renamed from: mn2$a */
    /* loaded from: classes.dex */
    public class a implements b<Object> {
        @Override // defpackage.mn2.b
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    }

    /* compiled from: Option.java */
    /* renamed from: mn2$b */
    /* loaded from: classes.dex */
    public interface b<T> {
        void a(byte[] bArr, T t, MessageDigest messageDigest);
    }

    public mn2(String str, T t, b<T> bVar) {
        this.c = wt2.b(str);
        this.a = t;
        this.b = (b) wt2.d(bVar);
    }

    public static <T> mn2<T> a(String str, T t, b<T> bVar) {
        return new mn2<>(str, t, bVar);
    }

    public static <T> b<T> b() {
        return (b<T>) e;
    }

    public static <T> mn2<T> e(String str) {
        return new mn2<>(str, null, b());
    }

    public static <T> mn2<T> f(String str, T t) {
        return new mn2<>(str, t, b());
    }

    public T c() {
        return this.a;
    }

    public final byte[] d() {
        if (this.d == null) {
            this.d = this.c.getBytes(fx1.a);
        }
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj instanceof mn2) {
            return this.c.equals(((mn2) obj).c);
        }
        return false;
    }

    public void g(T t, MessageDigest messageDigest) {
        this.b.a(d(), t, messageDigest);
    }

    public int hashCode() {
        return this.c.hashCode();
    }

    public String toString() {
        return "Option{key='" + this.c + "'}";
    }
}
