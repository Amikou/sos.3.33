package defpackage;

/* compiled from: CachedHashCodeArrayMap.java */
/* renamed from: yt  reason: default package */
/* loaded from: classes.dex */
public final class yt<K, V> extends rh<K, V> {
    public int m0;

    @Override // defpackage.vo3, java.util.Map
    public void clear() {
        this.m0 = 0;
        super.clear();
    }

    @Override // defpackage.vo3, java.util.Map
    public int hashCode() {
        if (this.m0 == 0) {
            this.m0 = super.hashCode();
        }
        return this.m0;
    }

    @Override // defpackage.vo3
    public void j(vo3<? extends K, ? extends V> vo3Var) {
        this.m0 = 0;
        super.j(vo3Var);
    }

    @Override // defpackage.vo3
    public V k(int i) {
        this.m0 = 0;
        return (V) super.k(i);
    }

    @Override // defpackage.vo3
    public V l(int i, V v) {
        this.m0 = 0;
        return (V) super.l(i, v);
    }

    @Override // defpackage.vo3, java.util.Map
    public V put(K k, V v) {
        this.m0 = 0;
        return (V) super.put(k, v);
    }
}
