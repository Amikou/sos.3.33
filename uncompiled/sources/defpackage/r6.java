package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktConfigurationsBinding.java */
/* renamed from: r6  reason: default package */
/* loaded from: classes2.dex */
public final class r6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final LinearLayoutCompat c;
    public final LinearLayoutCompat d;
    public final AppCompatTextView e;
    public final AppCompatTextView f;

    public r6(ConstraintLayout constraintLayout, MaterialButton materialButton, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, LinearLayoutCompat linearLayoutCompat, LinearLayoutCompat linearLayoutCompat2, bq1 bq1Var, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = linearLayoutCompat;
        this.d = linearLayoutCompat2;
        this.e = appCompatTextView;
        this.f = appCompatTextView2;
    }

    public static r6 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            i = R.id.imgSelectCurrency;
            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgSelectCurrency);
            if (appCompatImageView != null) {
                i = R.id.imgSelectLanguage;
                AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgSelectLanguage);
                if (appCompatImageView2 != null) {
                    i = R.id.layoutCurrency;
                    LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.layoutCurrency);
                    if (linearLayoutCompat != null) {
                        i = R.id.layoutLanguage;
                        LinearLayoutCompat linearLayoutCompat2 = (LinearLayoutCompat) ai4.a(view, R.id.layoutLanguage);
                        if (linearLayoutCompat2 != null) {
                            i = R.id.layoutSafeMoonBrand;
                            View a = ai4.a(view, R.id.layoutSafeMoonBrand);
                            if (a != null) {
                                bq1 a2 = bq1.a(a);
                                i = R.id.tvSelectCurrency;
                                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.tvSelectCurrency);
                                if (appCompatTextView != null) {
                                    i = R.id.tvSelectLanguage;
                                    AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.tvSelectLanguage);
                                    if (appCompatTextView2 != null) {
                                        return new r6((ConstraintLayout) view, materialButton, appCompatImageView, appCompatImageView2, linearLayoutCompat, linearLayoutCompat2, a2, appCompatTextView, appCompatTextView2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static r6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static r6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_configurations, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
