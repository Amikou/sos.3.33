package defpackage;

import androidx.recyclerview.widget.g;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: PATokenAdapter.kt */
/* renamed from: wl1  reason: default package */
/* loaded from: classes2.dex */
public final class wl1 extends g.f<PAToken> {
    public static final wl1 a = new wl1();

    @Override // androidx.recyclerview.widget.g.f
    /* renamed from: a */
    public boolean areContentsTheSame(PAToken pAToken, PAToken pAToken2) {
        fs1.f(pAToken, "oldItem");
        fs1.f(pAToken2, "newItem");
        return pAToken.getHasPriceAlert() == pAToken2.getHasPriceAlert() || fs1.b(pAToken.getContractAddress(), pAToken2.getContractAddress()) || fs1.b(pAToken.getSymbol(), pAToken2.getSymbol()) || fs1.b(pAToken.getName(), pAToken2.getName());
    }

    @Override // androidx.recyclerview.widget.g.f
    /* renamed from: b */
    public boolean areItemsTheSame(PAToken pAToken, PAToken pAToken2) {
        fs1.f(pAToken, "oldItem");
        fs1.f(pAToken2, "newItem");
        return fs1.b(pAToken, pAToken2);
    }
}
