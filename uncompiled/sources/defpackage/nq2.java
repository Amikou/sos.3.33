package defpackage;

import com.google.auto.value.AutoValue;

/* compiled from: PersistedEvent.java */
@AutoValue
/* renamed from: nq2  reason: default package */
/* loaded from: classes.dex */
public abstract class nq2 {
    public static nq2 a(long j, ob4 ob4Var, wx0 wx0Var) {
        return new nl(j, ob4Var, wx0Var);
    }

    public abstract wx0 b();

    public abstract long c();

    public abstract ob4 d();
}
