package defpackage;

/* compiled from: Buffer.java */
/* renamed from: sr  reason: default package */
/* loaded from: classes.dex */
public abstract class sr {
    public int a;

    public final void g(int i) {
        this.a = i | this.a;
    }

    public void h() {
        this.a = 0;
    }

    public final void j(int i) {
        this.a = (~i) & this.a;
    }

    public final boolean k(int i) {
        return (this.a & i) == i;
    }

    public final boolean l() {
        return k(268435456);
    }

    public final boolean o() {
        return k(Integer.MIN_VALUE);
    }

    public final boolean p() {
        return k(4);
    }

    public final boolean r() {
        return k(134217728);
    }

    public final boolean s() {
        return k(1);
    }

    public final void t(int i) {
        this.a = i;
    }
}
