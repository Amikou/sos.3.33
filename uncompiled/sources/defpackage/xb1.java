package defpackage;

import android.util.Pair;
import android.util.SparseArray;
import androidx.media3.common.DrmInitData;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.extractor.metadata.emsg.EventMessage;
import defpackage.wi3;
import defpackage.zi;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import zendesk.support.request.CellBase;

/* compiled from: FragmentedMp4Extractor.java */
/* renamed from: xb1  reason: default package */
/* loaded from: classes.dex */
public class xb1 implements p11 {
    public static final byte[] I;
    public static final j J;
    public int A;
    public int B;
    public int C;
    public boolean D;
    public r11 E;
    public f84[] F;
    public f84[] G;
    public boolean H;
    public final int a;
    public final w74 b;
    public final List<j> c;
    public final SparseArray<b> d;
    public final op2 e;
    public final op2 f;
    public final op2 g;
    public final byte[] h;
    public final op2 i;
    public final h64 j;
    public final androidx.media3.extractor.metadata.emsg.a k;
    public final op2 l;
    public final ArrayDeque<zi.a> m;
    public final ArrayDeque<a> n;
    public final f84 o;
    public int p;
    public int q;
    public long r;
    public int s;
    public op2 t;
    public long u;
    public int v;
    public long w;
    public long x;
    public long y;
    public b z;

    /* compiled from: FragmentedMp4Extractor.java */
    /* renamed from: xb1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final long a;
        public final boolean b;
        public final int c;

        public a(long j, boolean z, int i) {
            this.a = j;
            this.b = z;
            this.c = i;
        }
    }

    /* compiled from: FragmentedMp4Extractor.java */
    /* renamed from: xb1$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final f84 a;
        public g84 d;
        public mk0 e;
        public int f;
        public int g;
        public int h;
        public int i;
        public boolean l;
        public final z74 b = new z74();
        public final op2 c = new op2();
        public final op2 j = new op2(1);
        public final op2 k = new op2();

        public b(f84 f84Var, g84 g84Var, mk0 mk0Var) {
            this.a = f84Var;
            this.d = g84Var;
            this.e = mk0Var;
            j(g84Var, mk0Var);
        }

        public int c() {
            int i;
            if (!this.l) {
                i = this.d.g[this.f];
            } else {
                i = this.b.k[this.f] ? 1 : 0;
            }
            return g() != null ? i | 1073741824 : i;
        }

        public long d() {
            if (!this.l) {
                return this.d.c[this.f];
            }
            return this.b.g[this.h];
        }

        public long e() {
            if (!this.l) {
                return this.d.f[this.f];
            }
            return this.b.c(this.f);
        }

        public int f() {
            if (!this.l) {
                return this.d.d[this.f];
            }
            return this.b.i[this.f];
        }

        public x74 g() {
            if (this.l) {
                int i = ((mk0) androidx.media3.common.util.b.j(this.b.a)).a;
                x74 x74Var = this.b.n;
                if (x74Var == null) {
                    x74Var = this.d.a.a(i);
                }
                if (x74Var == null || !x74Var.a) {
                    return null;
                }
                return x74Var;
            }
            return null;
        }

        public boolean h() {
            this.f++;
            if (this.l) {
                int i = this.g + 1;
                this.g = i;
                int[] iArr = this.b.h;
                int i2 = this.h;
                if (i == iArr[i2]) {
                    this.h = i2 + 1;
                    this.g = 0;
                    return false;
                }
                return true;
            }
            return false;
        }

        public int i(int i, int i2) {
            op2 op2Var;
            x74 g = g();
            if (g == null) {
                return 0;
            }
            int i3 = g.d;
            if (i3 != 0) {
                op2Var = this.b.o;
            } else {
                byte[] bArr = (byte[]) androidx.media3.common.util.b.j(g.e);
                this.k.N(bArr, bArr.length);
                op2 op2Var2 = this.k;
                i3 = bArr.length;
                op2Var = op2Var2;
            }
            boolean g2 = this.b.g(this.f);
            boolean z = g2 || i2 != 0;
            this.j.d()[0] = (byte) ((z ? 128 : 0) | i3);
            this.j.P(0);
            this.a.c(this.j, 1, 1);
            this.a.c(op2Var, i3, 1);
            if (z) {
                if (!g2) {
                    this.c.L(8);
                    byte[] d = this.c.d();
                    d[0] = 0;
                    d[1] = 1;
                    d[2] = (byte) ((i2 >> 8) & 255);
                    d[3] = (byte) (i2 & 255);
                    d[4] = (byte) ((i >> 24) & 255);
                    d[5] = (byte) ((i >> 16) & 255);
                    d[6] = (byte) ((i >> 8) & 255);
                    d[7] = (byte) (i & 255);
                    this.a.c(this.c, 8, 1);
                    return i3 + 1 + 8;
                }
                op2 op2Var3 = this.b.o;
                int J = op2Var3.J();
                op2Var3.Q(-2);
                int i4 = (J * 6) + 2;
                if (i2 != 0) {
                    this.c.L(i4);
                    byte[] d2 = this.c.d();
                    op2Var3.j(d2, 0, i4);
                    int i5 = (((d2[2] & 255) << 8) | (d2[3] & 255)) + i2;
                    d2[2] = (byte) ((i5 >> 8) & 255);
                    d2[3] = (byte) (i5 & 255);
                    op2Var3 = this.c;
                }
                this.a.c(op2Var3, i4, 1);
                return i3 + 1 + i4;
            }
            return i3 + 1;
        }

        public void j(g84 g84Var, mk0 mk0Var) {
            this.d = g84Var;
            this.e = mk0Var;
            this.a.f(g84Var.a.f);
            k();
        }

        public void k() {
            this.b.f();
            this.f = 0;
            this.h = 0;
            this.g = 0;
            this.i = 0;
            this.l = false;
        }

        public void l(long j) {
            int i = this.f;
            while (true) {
                z74 z74Var = this.b;
                if (i >= z74Var.f || z74Var.c(i) >= j) {
                    return;
                }
                if (this.b.k[i]) {
                    this.i = i;
                }
                i++;
            }
        }

        public void m() {
            x74 g = g();
            if (g == null) {
                return;
            }
            op2 op2Var = this.b.o;
            int i = g.d;
            if (i != 0) {
                op2Var.Q(i);
            }
            if (this.b.g(this.f)) {
                op2Var.Q(op2Var.J() * 6);
            }
        }

        public void n(DrmInitData drmInitData) {
            x74 a = this.d.a.a(((mk0) androidx.media3.common.util.b.j(this.b.a)).a);
            this.a.f(this.d.a.f.b().M(drmInitData.c(a != null ? a.b : null)).E());
        }
    }

    static {
        vb1 vb1Var = vb1.b;
        I = new byte[]{-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};
        J = new j.b().e0("application/x-emsg").E();
    }

    public xb1() {
        this(0);
    }

    public static void A(op2 op2Var, z74 z74Var) throws ParserException {
        z(op2Var, 0, z74Var);
    }

    public static Pair<Long, py> B(op2 op2Var, long j) throws ParserException {
        long I2;
        long I3;
        int[] iArr;
        op2Var.P(8);
        int c = zi.c(op2Var.n());
        op2Var.Q(4);
        long F = op2Var.F();
        if (c == 0) {
            I2 = op2Var.F();
            I3 = op2Var.F();
        } else {
            I2 = op2Var.I();
            I3 = op2Var.I();
        }
        long j2 = I2;
        long j3 = j + I3;
        long J0 = androidx.media3.common.util.b.J0(j2, 1000000L, F);
        op2Var.Q(2);
        int J2 = op2Var.J();
        int[] iArr2 = new int[J2];
        long[] jArr = new long[J2];
        long[] jArr2 = new long[J2];
        long[] jArr3 = new long[J2];
        long j4 = J0;
        int i = 0;
        long j5 = j2;
        while (i < J2) {
            int n = op2Var.n();
            if ((n & Integer.MIN_VALUE) == 0) {
                long F2 = op2Var.F();
                iArr2[i] = n & Integer.MAX_VALUE;
                jArr[i] = j3;
                jArr3[i] = j4;
                long j6 = j5 + F2;
                long[] jArr4 = jArr2;
                long[] jArr5 = jArr3;
                int i2 = J2;
                long J02 = androidx.media3.common.util.b.J0(j6, 1000000L, F);
                jArr4[i] = J02 - jArr5[i];
                op2Var.Q(4);
                j3 += iArr[i];
                i++;
                iArr2 = iArr2;
                jArr3 = jArr5;
                jArr2 = jArr4;
                jArr = jArr;
                J2 = i2;
                j5 = j6;
                j4 = J02;
            } else {
                throw ParserException.createForMalformedContainer("Unhandled indirect reference", null);
            }
        }
        return Pair.create(Long.valueOf(J0), new py(iArr2, jArr, jArr2, jArr3));
    }

    public static long C(op2 op2Var) {
        op2Var.P(8);
        return zi.c(op2Var.n()) == 1 ? op2Var.I() : op2Var.F();
    }

    public static b D(op2 op2Var, SparseArray<b> sparseArray, boolean z) {
        int i;
        int i2;
        int i3;
        int i4;
        op2Var.P(8);
        int b2 = zi.b(op2Var.n());
        b valueAt = z ? sparseArray.valueAt(0) : sparseArray.get(op2Var.n());
        if (valueAt == null) {
            return null;
        }
        if ((b2 & 1) != 0) {
            long I2 = op2Var.I();
            z74 z74Var = valueAt.b;
            z74Var.c = I2;
            z74Var.d = I2;
        }
        mk0 mk0Var = valueAt.e;
        if ((b2 & 2) != 0) {
            i = op2Var.n() - 1;
        } else {
            i = mk0Var.a;
        }
        if ((b2 & 8) != 0) {
            i2 = op2Var.n();
        } else {
            i2 = mk0Var.b;
        }
        if ((b2 & 16) != 0) {
            i3 = op2Var.n();
        } else {
            i3 = mk0Var.c;
        }
        if ((b2 & 32) != 0) {
            i4 = op2Var.n();
        } else {
            i4 = mk0Var.d;
        }
        valueAt.b.a = new mk0(i, i2, i3, i4);
        return valueAt;
    }

    public static void E(zi.a aVar, SparseArray<b> sparseArray, boolean z, int i, byte[] bArr) throws ParserException {
        b D = D(((zi.b) ii.e(aVar.g(1952868452))).b, sparseArray, z);
        if (D == null) {
            return;
        }
        z74 z74Var = D.b;
        long j = z74Var.q;
        boolean z2 = z74Var.r;
        D.k();
        D.l = true;
        zi.b g = aVar.g(1952867444);
        if (g != null && (i & 2) == 0) {
            z74Var.q = C(g.b);
            z74Var.r = true;
        } else {
            z74Var.q = j;
            z74Var.r = z2;
        }
        H(aVar, D, i);
        x74 a2 = D.d.a.a(((mk0) ii.e(z74Var.a)).a);
        zi.b g2 = aVar.g(1935763834);
        if (g2 != null) {
            x((x74) ii.e(a2), g2.b, z74Var);
        }
        zi.b g3 = aVar.g(1935763823);
        if (g3 != null) {
            w(g3.b, z74Var);
        }
        zi.b g4 = aVar.g(1936027235);
        if (g4 != null) {
            A(g4.b, z74Var);
        }
        y(aVar, a2 != null ? a2.b : null, z74Var);
        int size = aVar.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            zi.b bVar = aVar.c.get(i2);
            if (bVar.a == 1970628964) {
                I(bVar.b, z74Var, bArr);
            }
        }
    }

    public static Pair<Integer, mk0> F(op2 op2Var) {
        op2Var.P(12);
        return Pair.create(Integer.valueOf(op2Var.n()), new mk0(op2Var.n() - 1, op2Var.n(), op2Var.n(), op2Var.n()));
    }

    /* JADX WARN: Removed duplicated region for block: B:45:0x00b2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static int G(defpackage.xb1.b r34, int r35, int r36, defpackage.op2 r37, int r38) throws androidx.media3.common.ParserException {
        /*
            Method dump skipped, instructions count: 322
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.xb1.G(xb1$b, int, int, op2, int):int");
    }

    public static void H(zi.a aVar, b bVar, int i) throws ParserException {
        List<zi.b> list = aVar.c;
        int size = list.size();
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            zi.b bVar2 = list.get(i4);
            if (bVar2.a == 1953658222) {
                op2 op2Var = bVar2.b;
                op2Var.P(12);
                int H = op2Var.H();
                if (H > 0) {
                    i3 += H;
                    i2++;
                }
            }
        }
        bVar.h = 0;
        bVar.g = 0;
        bVar.f = 0;
        bVar.b.e(i2, i3);
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 0; i7 < size; i7++) {
            zi.b bVar3 = list.get(i7);
            if (bVar3.a == 1953658222) {
                i6 = G(bVar, i5, i, bVar3.b, i6);
                i5++;
            }
        }
    }

    public static void I(op2 op2Var, z74 z74Var, byte[] bArr) throws ParserException {
        op2Var.P(8);
        op2Var.j(bArr, 0, 16);
        if (Arrays.equals(bArr, I)) {
            z(op2Var, 16, z74Var);
        }
    }

    public static boolean O(int i) {
        return i == 1836019574 || i == 1953653099 || i == 1835297121 || i == 1835626086 || i == 1937007212 || i == 1836019558 || i == 1953653094 || i == 1836475768 || i == 1701082227;
    }

    public static boolean P(int i) {
        return i == 1751411826 || i == 1835296868 || i == 1836476516 || i == 1936286840 || i == 1937011556 || i == 1937011827 || i == 1668576371 || i == 1937011555 || i == 1937011578 || i == 1937013298 || i == 1937007471 || i == 1668232756 || i == 1937011571 || i == 1952867444 || i == 1952868452 || i == 1953196132 || i == 1953654136 || i == 1953658222 || i == 1886614376 || i == 1935763834 || i == 1935763823 || i == 1936027235 || i == 1970628964 || i == 1935828848 || i == 1936158820 || i == 1701606260 || i == 1835362404 || i == 1701671783;
    }

    public static int d(int i) throws ParserException {
        if (i >= 0) {
            return i;
        }
        throw ParserException.createForMalformedContainer("Unexpected negative value: " + i, null);
    }

    public static DrmInitData i(List<zi.b> list) {
        int size = list.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            zi.b bVar = list.get(i);
            if (bVar.a == 1886614376) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                byte[] d = bVar.b.d();
                UUID f = mw2.f(d);
                if (f == null) {
                    p12.i("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
                } else {
                    arrayList.add(new DrmInitData.SchemeData(f, "video/mp4", d));
                }
            }
        }
        if (arrayList == null) {
            return null;
        }
        return new DrmInitData(arrayList);
    }

    public static b k(SparseArray<b> sparseArray) {
        int size = sparseArray.size();
        b bVar = null;
        long j = Long.MAX_VALUE;
        for (int i = 0; i < size; i++) {
            b valueAt = sparseArray.valueAt(i);
            if ((valueAt.l || valueAt.f != valueAt.d.b) && (!valueAt.l || valueAt.h != valueAt.b.e)) {
                long d = valueAt.d();
                if (d < j) {
                    bVar = valueAt;
                    j = d;
                }
            }
        }
        return bVar;
    }

    public static /* synthetic */ p11[] m() {
        return new p11[]{new xb1()};
    }

    public static long u(op2 op2Var) {
        op2Var.P(8);
        return zi.c(op2Var.n()) == 0 ? op2Var.F() : op2Var.I();
    }

    public static void v(zi.a aVar, SparseArray<b> sparseArray, boolean z, int i, byte[] bArr) throws ParserException {
        int size = aVar.d.size();
        for (int i2 = 0; i2 < size; i2++) {
            zi.a aVar2 = aVar.d.get(i2);
            if (aVar2.a == 1953653094) {
                E(aVar2, sparseArray, z, i, bArr);
            }
        }
    }

    public static void w(op2 op2Var, z74 z74Var) throws ParserException {
        op2Var.P(8);
        int n = op2Var.n();
        if ((zi.b(n) & 1) == 1) {
            op2Var.Q(8);
        }
        int H = op2Var.H();
        if (H == 1) {
            z74Var.d += zi.c(n) == 0 ? op2Var.F() : op2Var.I();
            return;
        }
        throw ParserException.createForMalformedContainer("Unexpected saio entry count: " + H, null);
    }

    public static void x(x74 x74Var, op2 op2Var, z74 z74Var) throws ParserException {
        int i;
        int i2 = x74Var.d;
        op2Var.P(8);
        if ((zi.b(op2Var.n()) & 1) == 1) {
            op2Var.Q(8);
        }
        int D = op2Var.D();
        int H = op2Var.H();
        if (H <= z74Var.f) {
            if (D == 0) {
                boolean[] zArr = z74Var.m;
                i = 0;
                for (int i3 = 0; i3 < H; i3++) {
                    int D2 = op2Var.D();
                    i += D2;
                    zArr[i3] = D2 > i2;
                }
            } else {
                i = (D * H) + 0;
                Arrays.fill(z74Var.m, 0, H, D > i2);
            }
            Arrays.fill(z74Var.m, H, z74Var.f, false);
            if (i > 0) {
                z74Var.d(i);
                return;
            }
            return;
        }
        throw ParserException.createForMalformedContainer("Saiz sample count " + H + " is greater than fragment sample count" + z74Var.f, null);
    }

    public static void y(zi.a aVar, String str, z74 z74Var) throws ParserException {
        byte[] bArr = null;
        op2 op2Var = null;
        op2 op2Var2 = null;
        for (int i = 0; i < aVar.c.size(); i++) {
            zi.b bVar = aVar.c.get(i);
            op2 op2Var3 = bVar.b;
            int i2 = bVar.a;
            if (i2 == 1935828848) {
                op2Var3.P(12);
                if (op2Var3.n() == 1936025959) {
                    op2Var = op2Var3;
                }
            } else if (i2 == 1936158820) {
                op2Var3.P(12);
                if (op2Var3.n() == 1936025959) {
                    op2Var2 = op2Var3;
                }
            }
        }
        if (op2Var == null || op2Var2 == null) {
            return;
        }
        op2Var.P(8);
        int c = zi.c(op2Var.n());
        op2Var.Q(4);
        if (c == 1) {
            op2Var.Q(4);
        }
        if (op2Var.n() == 1) {
            op2Var2.P(8);
            int c2 = zi.c(op2Var2.n());
            op2Var2.Q(4);
            if (c2 == 1) {
                if (op2Var2.F() == 0) {
                    throw ParserException.createForUnsupportedContainerFeature("Variable length description in sgpd found (unsupported)");
                }
            } else if (c2 >= 2) {
                op2Var2.Q(4);
            }
            if (op2Var2.F() == 1) {
                op2Var2.Q(1);
                int D = op2Var2.D();
                int i3 = (D & 240) >> 4;
                int i4 = D & 15;
                boolean z = op2Var2.D() == 1;
                if (z) {
                    int D2 = op2Var2.D();
                    byte[] bArr2 = new byte[16];
                    op2Var2.j(bArr2, 0, 16);
                    if (D2 == 0) {
                        int D3 = op2Var2.D();
                        bArr = new byte[D3];
                        op2Var2.j(bArr, 0, D3);
                    }
                    z74Var.l = true;
                    z74Var.n = new x74(z, str, D2, bArr2, i3, i4, bArr);
                    return;
                }
                return;
            }
            throw ParserException.createForUnsupportedContainerFeature("Entry count in sgpd != 1 (unsupported).");
        }
        throw ParserException.createForUnsupportedContainerFeature("Entry count in sbgp != 1 (unsupported).");
    }

    public static void z(op2 op2Var, int i, z74 z74Var) throws ParserException {
        op2Var.P(i + 8);
        int b2 = zi.b(op2Var.n());
        if ((b2 & 1) == 0) {
            boolean z = (b2 & 2) != 0;
            int H = op2Var.H();
            if (H == 0) {
                Arrays.fill(z74Var.m, 0, z74Var.f, false);
                return;
            } else if (H == z74Var.f) {
                Arrays.fill(z74Var.m, 0, H, z);
                z74Var.d(op2Var.a());
                z74Var.b(op2Var);
                return;
            } else {
                throw ParserException.createForMalformedContainer("Senc sample count " + H + " is different from fragment sample count" + z74Var.f, null);
            }
        }
        throw ParserException.createForUnsupportedContainerFeature("Overriding TrackEncryptionBox parameters is unsupported.");
    }

    public final void J(long j) throws ParserException {
        while (!this.m.isEmpty() && this.m.peek().b == j) {
            o(this.m.pop());
        }
        e();
    }

    public final boolean K(q11 q11Var) throws IOException {
        if (this.s == 0) {
            if (!q11Var.a(this.l.d(), 0, 8, true)) {
                return false;
            }
            this.s = 8;
            this.l.P(0);
            this.r = this.l.F();
            this.q = this.l.n();
        }
        long j = this.r;
        if (j == 1) {
            q11Var.readFully(this.l.d(), 8, 8);
            this.s += 8;
            this.r = this.l.I();
        } else if (j == 0) {
            long length = q11Var.getLength();
            if (length == -1 && !this.m.isEmpty()) {
                length = this.m.peek().b;
            }
            if (length != -1) {
                this.r = (length - q11Var.getPosition()) + this.s;
            }
        }
        if (this.r >= this.s) {
            long position = q11Var.getPosition() - this.s;
            int i = this.q;
            if ((i == 1836019558 || i == 1835295092) && !this.H) {
                this.E.p(new wi3.b(this.x, position));
                this.H = true;
            }
            if (this.q == 1836019558) {
                int size = this.d.size();
                for (int i2 = 0; i2 < size; i2++) {
                    z74 z74Var = this.d.valueAt(i2).b;
                    z74Var.b = position;
                    z74Var.d = position;
                    z74Var.c = position;
                }
            }
            int i3 = this.q;
            if (i3 == 1835295092) {
                this.z = null;
                this.u = position + this.r;
                this.p = 2;
                return true;
            }
            if (O(i3)) {
                long position2 = (q11Var.getPosition() + this.r) - 8;
                this.m.push(new zi.a(this.q, position2));
                if (this.r == this.s) {
                    J(position2);
                } else {
                    e();
                }
            } else if (P(this.q)) {
                if (this.s == 8) {
                    long j2 = this.r;
                    if (j2 <= 2147483647L) {
                        op2 op2Var = new op2((int) j2);
                        System.arraycopy(this.l.d(), 0, op2Var.d(), 0, 8);
                        this.t = op2Var;
                        this.p = 1;
                    } else {
                        throw ParserException.createForUnsupportedContainerFeature("Leaf atom with length > 2147483647 (unsupported).");
                    }
                } else {
                    throw ParserException.createForUnsupportedContainerFeature("Leaf atom defines extended atom size (unsupported).");
                }
            } else if (this.r <= 2147483647L) {
                this.t = null;
                this.p = 1;
            } else {
                throw ParserException.createForUnsupportedContainerFeature("Skipping atom with length > 2147483647 (unsupported).");
            }
            return true;
        }
        throw ParserException.createForUnsupportedContainerFeature("Atom size less than header length (unsupported).");
    }

    public final void L(q11 q11Var) throws IOException {
        int i = ((int) this.r) - this.s;
        op2 op2Var = this.t;
        if (op2Var != null) {
            q11Var.readFully(op2Var.d(), 8, i);
            q(new zi.b(this.q, op2Var), q11Var.getPosition());
        } else {
            q11Var.k(i);
        }
        J(q11Var.getPosition());
    }

    public final void M(q11 q11Var) throws IOException {
        int size = this.d.size();
        long j = Long.MAX_VALUE;
        b bVar = null;
        for (int i = 0; i < size; i++) {
            z74 z74Var = this.d.valueAt(i).b;
            if (z74Var.p) {
                long j2 = z74Var.d;
                if (j2 < j) {
                    bVar = this.d.valueAt(i);
                    j = j2;
                }
            }
        }
        if (bVar == null) {
            this.p = 3;
            return;
        }
        int position = (int) (j - q11Var.getPosition());
        if (position >= 0) {
            q11Var.k(position);
            bVar.b.a(q11Var);
            return;
        }
        throw ParserException.createForMalformedContainer("Offset to encryption data was negative.", null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final boolean N(q11 q11Var) throws IOException {
        int d;
        b bVar = this.z;
        Throwable th = null;
        if (bVar == null) {
            bVar = k(this.d);
            if (bVar == null) {
                int position = (int) (this.u - q11Var.getPosition());
                if (position >= 0) {
                    q11Var.k(position);
                    e();
                    return false;
                }
                throw ParserException.createForMalformedContainer("Offset to end of mdat was negative.", null);
            }
            int d2 = (int) (bVar.d() - q11Var.getPosition());
            if (d2 < 0) {
                p12.i("FragmentedMp4Extractor", "Ignoring negative offset to sample data.");
                d2 = 0;
            }
            q11Var.k(d2);
            this.z = bVar;
        }
        int i = 4;
        int i2 = 1;
        if (this.p == 3) {
            int f = bVar.f();
            this.A = f;
            if (bVar.f < bVar.i) {
                q11Var.k(f);
                bVar.m();
                if (!bVar.h()) {
                    this.z = null;
                }
                this.p = 3;
                return true;
            }
            if (bVar.d.a.g == 1) {
                this.A = f - 8;
                q11Var.k(8);
            }
            if ("audio/ac4".equals(bVar.d.a.f.p0)) {
                this.B = bVar.i(this.A, 7);
                x5.a(this.A, this.i);
                bVar.a.a(this.i, 7);
                this.B += 7;
            } else {
                this.B = bVar.i(this.A, 0);
            }
            this.A += this.B;
            this.p = 4;
            this.C = 0;
        }
        w74 w74Var = bVar.d.a;
        f84 f84Var = bVar.a;
        long e = bVar.e();
        h64 h64Var = this.j;
        if (h64Var != null) {
            e = h64Var.a(e);
        }
        long j = e;
        if (w74Var.j == 0) {
            while (true) {
                int i3 = this.B;
                int i4 = this.A;
                if (i3 >= i4) {
                    break;
                }
                this.B += f84Var.d(q11Var, i4 - i3, false);
            }
        } else {
            byte[] d3 = this.f.d();
            d3[0] = 0;
            d3[1] = 0;
            d3[2] = 0;
            int i5 = w74Var.j;
            int i6 = i5 + 1;
            int i7 = 4 - i5;
            while (this.B < this.A) {
                int i8 = this.C;
                if (i8 == 0) {
                    q11Var.readFully(d3, i7, i6);
                    this.f.P(0);
                    int n = this.f.n();
                    if (n >= i2) {
                        this.C = n - 1;
                        this.e.P(0);
                        f84Var.a(this.e, i);
                        f84Var.a(this.f, i2);
                        this.D = (this.G.length <= 0 || !wc2.g(w74Var.f.p0, d3[i])) ? 0 : i2;
                        this.B += 5;
                        this.A += i7;
                    } else {
                        throw ParserException.createForMalformedContainer("Invalid NAL length", th);
                    }
                } else {
                    if (this.D) {
                        this.g.L(i8);
                        q11Var.readFully(this.g.d(), 0, this.C);
                        f84Var.a(this.g, this.C);
                        d = this.C;
                        int q = wc2.q(this.g.d(), this.g.f());
                        this.g.P("video/hevc".equals(w74Var.f.p0) ? 1 : 0);
                        this.g.O(q);
                        uw.a(j, this.g, this.G);
                    } else {
                        d = f84Var.d(q11Var, i8, false);
                    }
                    this.B += d;
                    this.C -= d;
                    th = null;
                    i = 4;
                    i2 = 1;
                }
            }
        }
        int c = bVar.c();
        x74 g = bVar.g();
        f84Var.b(j, c, this.A, 0, g != null ? g.c : null);
        t(j);
        if (!bVar.h()) {
            this.z = null;
        }
        this.p = 3;
        return true;
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        int size = this.d.size();
        for (int i = 0; i < size; i++) {
            this.d.valueAt(i).k();
        }
        this.n.clear();
        this.v = 0;
        this.w = j2;
        this.m.clear();
        e();
    }

    public final void e() {
        this.p = 0;
        this.s = 0;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        while (true) {
            int i = this.p;
            if (i != 0) {
                if (i == 1) {
                    L(q11Var);
                } else if (i != 2) {
                    if (N(q11Var)) {
                        return 0;
                    }
                } else {
                    M(q11Var);
                }
            } else if (!K(q11Var)) {
                return -1;
            }
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        return xq3.b(q11Var);
    }

    public final mk0 h(SparseArray<mk0> sparseArray, int i) {
        if (sparseArray.size() == 1) {
            return sparseArray.valueAt(0);
        }
        return (mk0) ii.e(sparseArray.get(i));
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.E = r11Var;
        e();
        l();
        w74 w74Var = this.b;
        if (w74Var != null) {
            this.d.put(0, new b(r11Var.f(0, w74Var.b), new g84(this.b, new long[0], new int[0], 0, new long[0], new int[0], 0L), new mk0(0, 0, 0, 0)));
            this.E.m();
        }
    }

    public final void l() {
        int i;
        f84[] f84VarArr = new f84[2];
        this.F = f84VarArr;
        f84 f84Var = this.o;
        int i2 = 0;
        if (f84Var != null) {
            f84VarArr[0] = f84Var;
            i = 1;
        } else {
            i = 0;
        }
        int i3 = 100;
        if ((this.a & 4) != 0) {
            f84VarArr[i] = this.E.f(100, 5);
            i++;
            i3 = 101;
        }
        f84[] f84VarArr2 = (f84[]) androidx.media3.common.util.b.C0(this.F, i);
        this.F = f84VarArr2;
        for (f84 f84Var2 : f84VarArr2) {
            f84Var2.f(J);
        }
        this.G = new f84[this.c.size()];
        while (i2 < this.G.length) {
            f84 f = this.E.f(i3, 3);
            f.f(this.c.get(i2));
            this.G[i2] = f;
            i2++;
            i3++;
        }
    }

    public w74 n(w74 w74Var) {
        return w74Var;
    }

    public final void o(zi.a aVar) throws ParserException {
        int i = aVar.a;
        if (i == 1836019574) {
            s(aVar);
        } else if (i == 1836019558) {
            r(aVar);
        } else if (this.m.isEmpty()) {
        } else {
            this.m.peek().d(aVar);
        }
    }

    public final void p(op2 op2Var) {
        long J0;
        String str;
        long J02;
        String str2;
        long F;
        long j;
        f84[] f84VarArr;
        if (this.F.length == 0) {
            return;
        }
        op2Var.P(8);
        int c = zi.c(op2Var.n());
        if (c == 0) {
            String str3 = (String) ii.e(op2Var.x());
            String str4 = (String) ii.e(op2Var.x());
            long F2 = op2Var.F();
            J0 = androidx.media3.common.util.b.J0(op2Var.F(), 1000000L, F2);
            long j2 = this.y;
            long j3 = j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? j2 + J0 : -9223372036854775807L;
            str = str3;
            J02 = androidx.media3.common.util.b.J0(op2Var.F(), 1000L, F2);
            str2 = str4;
            F = op2Var.F();
            j = j3;
        } else if (c != 1) {
            p12.i("FragmentedMp4Extractor", "Skipping unsupported emsg version: " + c);
            return;
        } else {
            long F3 = op2Var.F();
            j = androidx.media3.common.util.b.J0(op2Var.I(), 1000000L, F3);
            long J03 = androidx.media3.common.util.b.J0(op2Var.F(), 1000L, F3);
            long F4 = op2Var.F();
            str = (String) ii.e(op2Var.x());
            J02 = J03;
            F = F4;
            str2 = (String) ii.e(op2Var.x());
            J0 = -9223372036854775807L;
        }
        byte[] bArr = new byte[op2Var.a()];
        op2Var.j(bArr, 0, op2Var.a());
        op2 op2Var2 = new op2(this.k.a(new EventMessage(str, str2, J02, F, bArr)));
        int a2 = op2Var2.a();
        for (f84 f84Var : this.F) {
            op2Var2.P(0);
            f84Var.a(op2Var2, a2);
        }
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.n.addLast(new a(J0, true, a2));
            this.v += a2;
        } else if (!this.n.isEmpty()) {
            this.n.addLast(new a(j, false, a2));
            this.v += a2;
        } else {
            h64 h64Var = this.j;
            if (h64Var != null) {
                j = h64Var.a(j);
            }
            for (f84 f84Var2 : this.F) {
                f84Var2.b(j, 1, a2, 0, null);
            }
        }
    }

    public final void q(zi.b bVar, long j) throws ParserException {
        if (!this.m.isEmpty()) {
            this.m.peek().e(bVar);
            return;
        }
        int i = bVar.a;
        if (i != 1936286840) {
            if (i == 1701671783) {
                p(bVar.b);
                return;
            }
            return;
        }
        Pair<Long, py> B = B(bVar.b, j);
        this.y = ((Long) B.first).longValue();
        this.E.p((wi3) B.second);
        this.H = true;
    }

    public final void r(zi.a aVar) throws ParserException {
        v(aVar, this.d, this.b != null, this.a, this.h);
        DrmInitData i = i(aVar.c);
        if (i != null) {
            int size = this.d.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.d.valueAt(i2).n(i);
            }
        }
        if (this.w != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            int size2 = this.d.size();
            for (int i3 = 0; i3 < size2; i3++) {
                this.d.valueAt(i3).l(this.w);
            }
            this.w = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
    }

    public final void s(zi.a aVar) throws ParserException {
        int i = 0;
        ii.h(this.b == null, "Unexpected moov box.");
        DrmInitData i2 = i(aVar.c);
        zi.a aVar2 = (zi.a) ii.e(aVar.f(1836475768));
        SparseArray<mk0> sparseArray = new SparseArray<>();
        int size = aVar2.c.size();
        long j = -9223372036854775807L;
        for (int i3 = 0; i3 < size; i3++) {
            zi.b bVar = aVar2.c.get(i3);
            int i4 = bVar.a;
            if (i4 == 1953654136) {
                Pair<Integer, mk0> F = F(bVar.b);
                sparseArray.put(((Integer) F.first).intValue(), (mk0) F.second);
            } else if (i4 == 1835362404) {
                j = u(bVar.b);
            }
        }
        List<g84> A = aj.A(aVar, new qe1(), j, i2, (this.a & 16) != 0, false, new jd1() { // from class: wb1
            @Override // defpackage.jd1
            public final Object apply(Object obj) {
                return xb1.this.n((w74) obj);
            }
        });
        int size2 = A.size();
        if (this.d.size() == 0) {
            while (i < size2) {
                g84 g84Var = A.get(i);
                w74 w74Var = g84Var.a;
                this.d.put(w74Var.a, new b(this.E.f(i, w74Var.b), g84Var, h(sparseArray, w74Var.a)));
                this.x = Math.max(this.x, w74Var.e);
                i++;
            }
            this.E.m();
            return;
        }
        ii.g(this.d.size() == size2);
        while (i < size2) {
            g84 g84Var2 = A.get(i);
            w74 w74Var2 = g84Var2.a;
            this.d.get(w74Var2.a).j(g84Var2, h(sparseArray, w74Var2.a));
            i++;
        }
    }

    public final void t(long j) {
        while (!this.n.isEmpty()) {
            a removeFirst = this.n.removeFirst();
            this.v -= removeFirst.c;
            long j2 = removeFirst.a;
            if (removeFirst.b) {
                j2 += j;
            }
            h64 h64Var = this.j;
            if (h64Var != null) {
                j2 = h64Var.a(j2);
            }
            for (f84 f84Var : this.F) {
                f84Var.b(j2, 1, removeFirst.c, this.v, null);
            }
        }
    }

    public xb1(int i) {
        this(i, null);
    }

    public xb1(int i, h64 h64Var) {
        this(i, h64Var, null, Collections.emptyList());
    }

    public xb1(int i, h64 h64Var, w74 w74Var, List<j> list) {
        this(i, h64Var, w74Var, list, null);
    }

    public xb1(int i, h64 h64Var, w74 w74Var, List<j> list, f84 f84Var) {
        this.a = i;
        this.j = h64Var;
        this.b = w74Var;
        this.c = Collections.unmodifiableList(list);
        this.o = f84Var;
        this.k = new androidx.media3.extractor.metadata.emsg.a();
        this.l = new op2(16);
        this.e = new op2(wc2.a);
        this.f = new op2(5);
        this.g = new op2();
        byte[] bArr = new byte[16];
        this.h = bArr;
        this.i = new op2(bArr);
        this.m = new ArrayDeque<>();
        this.n = new ArrayDeque<>();
        this.d = new SparseArray<>();
        this.x = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.w = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.y = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.E = r11.e;
        this.F = new f84[0];
        this.G = new f84[0];
    }
}
