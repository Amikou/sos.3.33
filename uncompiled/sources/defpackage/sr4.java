package defpackage;

import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;

/* renamed from: sr4  reason: default package */
/* loaded from: classes2.dex */
public class sr4 extends h {
    public static ur4 f0 = new ur4();
    public ct0 a;

    public sr4(ct0 ct0Var) {
        this.a = ct0Var;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        return new j0(f0.c(this.a.t(), f0.b(this.a)));
    }
}
