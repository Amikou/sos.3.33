package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: oq5  reason: default package */
/* loaded from: classes.dex */
public final class oq5 extends iq5 {
    public final mq5 a = new mq5();

    @Override // defpackage.iq5
    public final void a(Throwable th, Throwable th2) {
        if (th2 != th) {
            this.a.a(th, true).add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }
}
