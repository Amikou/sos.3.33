package defpackage;

import zendesk.support.request.CellBase;

/* compiled from: StringNumberConversions.kt */
/* renamed from: cv3  reason: default package */
/* loaded from: classes2.dex */
public class cv3 extends bv3 {
    public static final Integer l(String str) {
        fs1.f(str, "$this$toIntOrNull");
        return m(str, 10);
    }

    public static final Integer m(String str, int i) {
        boolean z;
        int i2;
        fs1.f(str, "$this$toIntOrNull");
        xx.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        int i3 = 0;
        char charAt = str.charAt(0);
        int h = fs1.h(charAt, 48);
        int i4 = CellBase.GROUP_ID_END_USER;
        int i5 = 1;
        if (h >= 0) {
            z = false;
            i5 = 0;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                i4 = Integer.MIN_VALUE;
                z = true;
            } else if (charAt != '+') {
                return null;
            } else {
                z = false;
            }
        }
        int i6 = -59652323;
        while (i5 < length) {
            int b = xx.b(str.charAt(i5), i);
            if (b < 0) {
                return null;
            }
            if ((i3 < i6 && (i6 != -59652323 || i3 < (i6 = i4 / i))) || (i2 = i3 * i) < i4 + b) {
                return null;
            }
            i3 = i2 - b;
            i5++;
        }
        return z ? Integer.valueOf(i3) : Integer.valueOf(-i3);
    }

    public static final Long n(String str) {
        fs1.f(str, "$this$toLongOrNull");
        return o(str, 10);
    }

    public static final Long o(String str, int i) {
        fs1.f(str, "$this$toLongOrNull");
        xx.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        int i2 = 0;
        char charAt = str.charAt(0);
        int h = fs1.h(charAt, 48);
        long j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        boolean z = true;
        if (h >= 0) {
            z = false;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                j = Long.MIN_VALUE;
                i2 = 1;
            } else if (charAt != '+') {
                return null;
            } else {
                z = false;
                i2 = 1;
            }
        }
        long j2 = -256204778801521550L;
        long j3 = 0;
        long j4 = -256204778801521550L;
        while (i2 < length) {
            int b = xx.b(str.charAt(i2), i);
            if (b < 0) {
                return null;
            }
            if (j3 < j4) {
                if (j4 == j2) {
                    j4 = j / i;
                    if (j3 < j4) {
                    }
                }
                return null;
            }
            long j5 = j3 * i;
            long j6 = b;
            if (j5 < j + j6) {
                return null;
            }
            j3 = j5 - j6;
            i2++;
            j2 = -256204778801521550L;
        }
        return z ? Long.valueOf(j3) : Long.valueOf(-j3);
    }
}
