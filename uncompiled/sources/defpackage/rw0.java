package defpackage;

import java.math.BigInteger;

/* compiled from: EthGasPrice.java */
/* renamed from: rw0  reason: default package */
/* loaded from: classes3.dex */
public class rw0 extends i83<String> {
    public BigInteger getGasPrice() {
        return ej2.decodeQuantity(getResult());
    }
}
