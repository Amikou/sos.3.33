package defpackage;

import com.google.android.play.core.tasks.RuntimeExecutionException;
import java.util.concurrent.Executor;

/* renamed from: fy4  reason: default package */
/* loaded from: classes2.dex */
public final class fy4<ResultT> extends l34<ResultT> {
    public final Object a = new Object();
    public final rx4<ResultT> b = new rx4<>();
    public boolean c;
    public ResultT d;
    public Exception e;

    @Override // defpackage.l34
    public final l34<ResultT> a(Executor executor, mm2 mm2Var) {
        this.b.a(new bx4(executor, mm2Var));
        m();
        return this;
    }

    @Override // defpackage.l34
    public final l34<ResultT> b(Executor executor, tm2<? super ResultT> tm2Var) {
        this.b.a(new mx4(executor, tm2Var));
        m();
        return this;
    }

    @Override // defpackage.l34
    public final Exception c() {
        Exception exc;
        synchronized (this.a) {
            exc = this.e;
        }
        return exc;
    }

    @Override // defpackage.l34
    public final ResultT d() {
        ResultT resultt;
        synchronized (this.a) {
            k();
            Exception exc = this.e;
            if (exc != null) {
                throw new RuntimeExecutionException(exc);
            }
            resultt = this.d;
        }
        return resultt;
    }

    @Override // defpackage.l34
    public final boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @Override // defpackage.l34
    public final boolean f() {
        boolean z;
        synchronized (this.a) {
            z = false;
            if (this.c && this.e == null) {
                z = true;
            }
        }
        return z;
    }

    public final void g(ResultT resultt) {
        synchronized (this.a) {
            l();
            this.c = true;
            this.d = resultt;
        }
        this.b.b(this);
    }

    public final boolean h(ResultT resultt) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = resultt;
            this.b.b(this);
            return true;
        }
    }

    public final void i(Exception exc) {
        synchronized (this.a) {
            l();
            this.c = true;
            this.e = exc;
        }
        this.b.b(this);
    }

    public final boolean j(Exception exc) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = exc;
            this.b.b(this);
            return true;
        }
    }

    public final void k() {
        gu4.c(this.c, "Task is not yet complete");
    }

    public final void l() {
        gu4.c(!this.c, "Task is already complete");
    }

    public final void m() {
        synchronized (this.a) {
            if (this.c) {
                this.b.b(this);
            }
        }
    }
}
