package defpackage;

import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import com.google.crypto.tink.l;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.Arrays;
import javax.crypto.KeyGenerator;

/* compiled from: AndroidKeystoreKmsClient.java */
/* renamed from: zc  reason: default package */
/* loaded from: classes2.dex */
public final class zc implements l {
    public final String a;
    public KeyStore b;

    /* compiled from: AndroidKeystoreKmsClient.java */
    /* renamed from: zc$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public String a = null;
        public KeyStore b;

        public b() {
            this.b = null;
            if (zc.f()) {
                try {
                    KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
                    this.b = keyStore;
                    keyStore.load(null);
                    return;
                } catch (IOException | GeneralSecurityException e) {
                    throw new IllegalStateException(e);
                }
            }
            throw new IllegalStateException("need Android Keystore on Android M or newer");
        }

        public zc a() {
            return new zc(this);
        }

        public b b(KeyStore keyStore) {
            if (keyStore != null) {
                this.b = keyStore;
                return this;
            }
            throw new IllegalArgumentException("val cannot be null");
        }
    }

    public static void d(String str) throws GeneralSecurityException {
        if (!new zc().e(str)) {
            String b2 = ug4.b("android-keystore://", str);
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES", "AndroidKeyStore");
            keyGenerator.init(new KeyGenParameterSpec.Builder(b2, 3).setKeySize(256).setBlockModes("GCM").setEncryptionPaddings("NoPadding").build());
            keyGenerator.generateKey();
            return;
        }
        throw new IllegalArgumentException(String.format("cannot generate a new key %s because it already exists; please delete it with deleteKey() and try again", str));
    }

    public static boolean f() {
        return Build.VERSION.SDK_INT >= 23;
    }

    public static com.google.crypto.tink.a g(com.google.crypto.tink.a aVar) throws GeneralSecurityException {
        byte[] c = p33.c(10);
        byte[] bArr = new byte[0];
        if (Arrays.equals(c, aVar.b(aVar.a(c, bArr), bArr))) {
            return aVar;
        }
        throw new KeyStoreException("cannot use Android Keystore: encryption/decryption of non-empty message and empty aad returns an incorrect result");
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x001e, code lost:
        if (r3.toLowerCase(java.util.Locale.US).startsWith("android-keystore://") != false) goto L14;
     */
    @Override // com.google.crypto.tink.l
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public synchronized boolean a(java.lang.String r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.String r0 = r2.a     // Catch: java.lang.Throwable -> L24
            r1 = 1
            if (r0 == 0) goto Le
            boolean r0 = r0.equals(r3)     // Catch: java.lang.Throwable -> L24
            if (r0 == 0) goto Le
            monitor-exit(r2)
            return r1
        Le:
            java.lang.String r0 = r2.a     // Catch: java.lang.Throwable -> L24
            if (r0 != 0) goto L21
            java.util.Locale r0 = java.util.Locale.US     // Catch: java.lang.Throwable -> L24
            java.lang.String r3 = r3.toLowerCase(r0)     // Catch: java.lang.Throwable -> L24
            java.lang.String r0 = "android-keystore://"
            boolean r3 = r3.startsWith(r0)     // Catch: java.lang.Throwable -> L24
            if (r3 == 0) goto L21
            goto L22
        L21:
            r1 = 0
        L22:
            monitor-exit(r2)
            return r1
        L24:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.zc.a(java.lang.String):boolean");
    }

    @Override // com.google.crypto.tink.l
    public synchronized com.google.crypto.tink.a b(String str) throws GeneralSecurityException {
        String str2 = this.a;
        if (str2 != null && !str2.equals(str)) {
            throw new GeneralSecurityException(String.format("this client is bound to %s, cannot load keys bound to %s", this.a, str));
        }
        return g(new yc(ug4.b("android-keystore://", str), this.b));
    }

    public synchronized boolean e(String str) throws GeneralSecurityException {
        String str2;
        try {
        } catch (NullPointerException unused) {
            try {
                Thread.sleep(20L);
                KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
                this.b = keyStore;
                keyStore.load(null);
            } catch (IOException e) {
                throw new GeneralSecurityException(e);
            } catch (InterruptedException unused2) {
            }
            return this.b.containsAlias(str2);
        }
        return this.b.containsAlias(ug4.b("android-keystore://", str));
    }

    public zc() throws GeneralSecurityException {
        this(new b());
    }

    public zc(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
    }
}
