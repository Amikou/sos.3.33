package defpackage;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.onesignal.NotificationOpenedReceiver;
import com.onesignal.NotificationOpenedReceiverAndroid22AndOlder;

/* compiled from: IntentGeneratorForAttachingToNotifications.kt */
/* renamed from: zr1  reason: default package */
/* loaded from: classes2.dex */
public final class zr1 {
    public final Class<?> a;
    public final Class<?> b;
    public final Context c;

    public zr1(Context context) {
        fs1.f(context, "context");
        this.c = context;
        this.a = NotificationOpenedReceiver.class;
        this.b = NotificationOpenedReceiverAndroid22AndOlder.class;
    }

    public final PendingIntent a(int i, Intent intent) {
        fs1.f(intent, "oneSignalIntent");
        return PendingIntent.getActivity(this.c, i, intent, 201326592);
    }

    public final Intent b(int i) {
        Intent c;
        if (Build.VERSION.SDK_INT >= 23) {
            c = d();
        } else {
            c = c();
        }
        Intent addFlags = c.putExtra("androidNotificationId", i).addFlags(603979776);
        fs1.e(addFlags, "intent\n            .putE…Y_CLEAR_TOP\n            )");
        return addFlags;
    }

    public final Intent c() {
        Intent intent = new Intent(this.c, this.b);
        intent.addFlags(403177472);
        return intent;
    }

    public final Intent d() {
        return new Intent(this.c, this.a);
    }
}
