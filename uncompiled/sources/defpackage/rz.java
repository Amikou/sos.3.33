package defpackage;

import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: rz  reason: default package */
/* loaded from: classes.dex */
public interface rz {
    @RecentlyNonNull
    long a();

    @RecentlyNonNull
    long b();

    @RecentlyNonNull
    long nanoTime();
}
