package defpackage;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import androidx.media3.common.util.b;

/* compiled from: NotificationUtil.java */
@SuppressLint({"InlinedApi"})
/* renamed from: si2  reason: default package */
/* loaded from: classes.dex */
public final class si2 {
    public static void a(Context context, String str, int i, int i2, int i3) {
        if (b.a >= 26) {
            NotificationManager notificationManager = (NotificationManager) ii.e((NotificationManager) context.getSystemService("notification"));
            NotificationChannel notificationChannel = new NotificationChannel(str, context.getString(i), i3);
            if (i2 != 0) {
                notificationChannel.setDescription(context.getString(i2));
            }
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
