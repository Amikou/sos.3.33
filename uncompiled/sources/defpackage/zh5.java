package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.cloudmessaging.c;
import com.google.android.gms.cloudmessaging.zzp;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: zh5  reason: default package */
/* loaded from: classes.dex */
public final class zh5 implements ServiceConnection {
    public int a;
    public final Messenger f0;
    public c g0;
    public final Queue<n36<?>> h0;
    public final SparseArray<n36<?>> i0;
    public final /* synthetic */ pf5 j0;

    public zh5(pf5 pf5Var) {
        this.j0 = pf5Var;
        this.a = 0;
        this.f0 = new Messenger(new lf5(Looper.getMainLooper(), new Handler.Callback(this) { // from class: vp5
            public final zh5 a;

            {
                this.a = this;
            }

            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                return this.a.d(message);
            }
        }));
        this.h0 = new ArrayDeque();
        this.i0 = new SparseArray<>();
    }

    public final void a() {
        pf5.g(this.j0).execute(new Runnable(this) { // from class: is5
            public final zh5 a;

            {
                this.a = this;
            }

            @Override // java.lang.Runnable
            public final void run() {
                final n36<?> poll;
                final zh5 zh5Var = this.a;
                while (true) {
                    synchronized (zh5Var) {
                        if (zh5Var.a != 2) {
                            return;
                        }
                        if (zh5Var.h0.isEmpty()) {
                            zh5Var.f();
                            return;
                        }
                        poll = zh5Var.h0.poll();
                        zh5Var.i0.put(poll.a, poll);
                        pf5.g(zh5Var.j0).schedule(new Runnable(zh5Var, poll) { // from class: jx5
                            public final zh5 a;
                            public final n36 f0;

                            {
                                this.a = zh5Var;
                                this.f0 = poll;
                            }

                            @Override // java.lang.Runnable
                            public final void run() {
                                this.a.b(this.f0.a);
                            }
                        }, 30L, TimeUnit.SECONDS);
                    }
                    if (Log.isLoggable("MessengerIpcClient", 3)) {
                        String valueOf = String.valueOf(poll);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 8);
                        sb.append("Sending ");
                        sb.append(valueOf);
                    }
                    Context b = pf5.b(zh5Var.j0);
                    Messenger messenger = zh5Var.f0;
                    Message obtain = Message.obtain();
                    obtain.what = poll.c;
                    obtain.arg1 = poll.a;
                    obtain.replyTo = messenger;
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("oneWay", poll.d());
                    bundle.putString("pkg", b.getPackageName());
                    bundle.putBundle("data", poll.d);
                    obtain.setData(bundle);
                    try {
                        zh5Var.g0.a(obtain);
                    } catch (RemoteException e) {
                        zh5Var.c(2, e.getMessage());
                    }
                }
            }
        });
    }

    public final synchronized void b(int i) {
        n36<?> n36Var = this.i0.get(i);
        if (n36Var != null) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Timing out request: ");
            sb.append(i);
            this.i0.remove(i);
            n36Var.b(new zzp(3, "Timed out waiting for response"));
            f();
        }
    }

    public final synchronized void c(int i, String str) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                "Disconnected: ".concat(valueOf);
            }
        }
        int i2 = this.a;
        if (i2 == 0) {
            throw new IllegalStateException();
        }
        if (i2 != 1 && i2 != 2) {
            if (i2 == 3) {
                this.a = 4;
                return;
            } else if (i2 == 4) {
                return;
            } else {
                int i3 = this.a;
                StringBuilder sb = new StringBuilder(26);
                sb.append("Unknown state: ");
                sb.append(i3);
                throw new IllegalStateException(sb.toString());
            }
        }
        Log.isLoggable("MessengerIpcClient", 2);
        this.a = 4;
        v50.b().c(pf5.b(this.j0), this);
        zzp zzpVar = new zzp(i, str);
        for (n36<?> n36Var : this.h0) {
            n36Var.b(zzpVar);
        }
        this.h0.clear();
        for (int i4 = 0; i4 < this.i0.size(); i4++) {
            this.i0.valueAt(i4).b(zzpVar);
        }
        this.i0.clear();
    }

    public final boolean d(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Received response to request: ");
            sb.append(i);
        }
        synchronized (this) {
            n36<?> n36Var = this.i0.get(i);
            if (n36Var == null) {
                StringBuilder sb2 = new StringBuilder(50);
                sb2.append("Received response for unknown request: ");
                sb2.append(i);
                return true;
            }
            this.i0.remove(i);
            f();
            Bundle data = message.getData();
            if (data.getBoolean("unsupported", false)) {
                n36Var.b(new zzp(4, "Not supported by GmsCore"));
            } else {
                n36Var.a(data);
            }
            return true;
        }
    }

    public final synchronized boolean e(n36<?> n36Var) {
        int i = this.a;
        if (i == 0) {
            this.h0.add(n36Var);
            zt2.m(this.a == 0);
            Log.isLoggable("MessengerIpcClient", 2);
            this.a = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!v50.b().a(pf5.b(this.j0), intent, this, 1)) {
                c(0, "Unable to bind to service");
            } else {
                pf5.g(this.j0).schedule(new Runnable(this) { // from class: zm5
                    public final zh5 a;

                    {
                        this.a = this;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        this.a.g();
                    }
                }, 30L, TimeUnit.SECONDS);
            }
            return true;
        } else if (i == 1) {
            this.h0.add(n36Var);
            return true;
        } else if (i == 2) {
            this.h0.add(n36Var);
            a();
            return true;
        } else {
            if (i != 3 && i != 4) {
                int i2 = this.a;
                StringBuilder sb = new StringBuilder(26);
                sb.append("Unknown state: ");
                sb.append(i2);
                throw new IllegalStateException(sb.toString());
            }
            return false;
        }
    }

    public final synchronized void f() {
        if (this.a == 2 && this.h0.isEmpty() && this.i0.size() == 0) {
            Log.isLoggable("MessengerIpcClient", 2);
            this.a = 3;
            v50.b().c(pf5.b(this.j0), this);
        }
    }

    public final synchronized void g() {
        if (this.a == 1) {
            c(1, "Timed out while binding");
        }
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, final IBinder iBinder) {
        pf5.g(this.j0).execute(new Runnable(this, iBinder) { // from class: zu5
            public final zh5 a;
            public final IBinder f0;

            {
                this.a = this;
                this.f0 = iBinder;
            }

            @Override // java.lang.Runnable
            public final void run() {
                zh5 zh5Var = this.a;
                IBinder iBinder2 = this.f0;
                synchronized (zh5Var) {
                    try {
                        if (iBinder2 == null) {
                            zh5Var.c(0, "Null service connection");
                            return;
                        }
                        try {
                            zh5Var.g0 = new c(iBinder2);
                            zh5Var.a = 2;
                            zh5Var.a();
                        } catch (RemoteException e) {
                            zh5Var.c(0, e.getMessage());
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
            }
        });
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        pf5.g(this.j0).execute(new Runnable(this) { // from class: cz5
            public final zh5 a;

            {
                this.a = this;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.c(2, "Service disconnected");
            }
        });
    }
}
