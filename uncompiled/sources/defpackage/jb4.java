package defpackage;

import android.view.ViewGroup;
import androidx.transition.Transition;

/* compiled from: TransitionPropagation.java */
/* renamed from: jb4  reason: default package */
/* loaded from: classes.dex */
public abstract class jb4 {
    public abstract void a(kb4 kb4Var);

    public abstract String[] b();

    public abstract long c(ViewGroup viewGroup, Transition transition, kb4 kb4Var, kb4 kb4Var2);
}
