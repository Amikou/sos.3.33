package defpackage;

import com.google.android.gms.tasks.e;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: y56  reason: default package */
/* loaded from: classes.dex */
public final class y56 implements Runnable {
    public final /* synthetic */ e a;
    public final /* synthetic */ Callable f0;

    public y56(e eVar, Callable callable) {
        this.a = eVar;
        this.f0 = callable;
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            this.a.t(this.f0.call());
        } catch (Exception e) {
            this.a.s(e);
        } catch (Throwable th) {
            this.a.s(new RuntimeException(th));
        }
    }
}
