package defpackage;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

/* compiled from: SystemClock.java */
/* renamed from: q24  reason: default package */
/* loaded from: classes.dex */
public class q24 implements tz {
    @Override // defpackage.tz
    public long a() {
        return SystemClock.uptimeMillis();
    }

    @Override // defpackage.tz
    public long b() {
        return SystemClock.elapsedRealtime();
    }

    @Override // defpackage.tz
    public pj1 c(Looper looper, Handler.Callback callback) {
        return new t24(new Handler(looper, callback));
    }

    @Override // defpackage.tz
    public void d() {
    }
}
