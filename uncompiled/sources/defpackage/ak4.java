package defpackage;

import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ViewSpline.java */
/* renamed from: ak4  reason: default package */
/* loaded from: classes.dex */
public abstract class ak4 extends sr3 {

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$a */
    /* loaded from: classes.dex */
    public static class a extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setAlpha(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$b */
    /* loaded from: classes.dex */
    public static class b extends ak4 {
        public SparseArray<ConstraintAttribute> f;
        public float[] g;

        public b(String str, SparseArray<ConstraintAttribute> sparseArray) {
            String str2 = str.split(",")[1];
            this.f = sparseArray;
        }

        @Override // defpackage.sr3
        public void c(int i, float f) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute)");
        }

        @Override // defpackage.sr3
        public void e(int i) {
            float[] fArr;
            int size = this.f.size();
            int h = this.f.valueAt(0).h();
            double[] dArr = new double[size];
            this.g = new float[h];
            double[][] dArr2 = (double[][]) Array.newInstance(double.class, size, h);
            for (int i2 = 0; i2 < size; i2++) {
                dArr[i2] = this.f.keyAt(i2) * 0.01d;
                this.f.valueAt(i2).f(this.g);
                int i3 = 0;
                while (true) {
                    if (i3 < this.g.length) {
                        dArr2[i2][i3] = fArr[i3];
                        i3++;
                    }
                }
            }
            this.a = bc0.a(i, dArr, dArr2);
        }

        @Override // defpackage.ak4
        public void h(View view, float f) {
            this.a.e(f, this.g);
            this.f.valueAt(0).k(view, this.g);
        }

        public void i(int i, ConstraintAttribute constraintAttribute) {
            this.f.append(i, constraintAttribute);
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$c */
    /* loaded from: classes.dex */
    public static class c extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setElevation(a(f));
            }
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$d */
    /* loaded from: classes.dex */
    public static class d extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
        }

        public void i(View view, float f, double d, double d2) {
            view.setRotation(a(f) + ((float) Math.toDegrees(Math.atan2(d2, d))));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$e */
    /* loaded from: classes.dex */
    public static class e extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setPivotX(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$f */
    /* loaded from: classes.dex */
    public static class f extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setPivotY(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$g */
    /* loaded from: classes.dex */
    public static class g extends ak4 {
        public boolean f = false;

        @Override // defpackage.ak4
        public void h(View view, float f) {
            if (view instanceof MotionLayout) {
                ((MotionLayout) view).setProgress(a(f));
            } else if (this.f) {
            } else {
                Method method = null;
                try {
                    method = view.getClass().getMethod("setProgress", Float.TYPE);
                } catch (NoSuchMethodException unused) {
                    this.f = true;
                }
                if (method != null) {
                    try {
                        method.invoke(view, Float.valueOf(a(f)));
                    } catch (IllegalAccessException | InvocationTargetException unused2) {
                    }
                }
            }
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$h */
    /* loaded from: classes.dex */
    public static class h extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setRotation(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$i */
    /* loaded from: classes.dex */
    public static class i extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setRotationX(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$j */
    /* loaded from: classes.dex */
    public static class j extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setRotationY(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$k */
    /* loaded from: classes.dex */
    public static class k extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setScaleX(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$l */
    /* loaded from: classes.dex */
    public static class l extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setScaleY(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$m */
    /* loaded from: classes.dex */
    public static class m extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setTranslationX(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$n */
    /* loaded from: classes.dex */
    public static class n extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            view.setTranslationY(a(f));
        }
    }

    /* compiled from: ViewSpline.java */
    /* renamed from: ak4$o */
    /* loaded from: classes.dex */
    public static class o extends ak4 {
        @Override // defpackage.ak4
        public void h(View view, float f) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setTranslationZ(a(f));
            }
        }
    }

    public static ak4 f(String str, SparseArray<ConstraintAttribute> sparseArray) {
        return new b(str, sparseArray);
    }

    public static ak4 g(String str) {
        str.hashCode();
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1249320806:
                if (str.equals("rotationX")) {
                    c2 = 0;
                    break;
                }
                break;
            case -1249320805:
                if (str.equals("rotationY")) {
                    c2 = 1;
                    break;
                }
                break;
            case -1225497657:
                if (str.equals("translationX")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1225497656:
                if (str.equals("translationY")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1225497655:
                if (str.equals("translationZ")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1001078227:
                if (str.equals("progress")) {
                    c2 = 5;
                    break;
                }
                break;
            case -908189618:
                if (str.equals("scaleX")) {
                    c2 = 6;
                    break;
                }
                break;
            case -908189617:
                if (str.equals("scaleY")) {
                    c2 = 7;
                    break;
                }
                break;
            case -797520672:
                if (str.equals("waveVariesBy")) {
                    c2 = '\b';
                    break;
                }
                break;
            case -760884510:
                if (str.equals("transformPivotX")) {
                    c2 = '\t';
                    break;
                }
                break;
            case -760884509:
                if (str.equals("transformPivotY")) {
                    c2 = '\n';
                    break;
                }
                break;
            case -40300674:
                if (str.equals("rotation")) {
                    c2 = 11;
                    break;
                }
                break;
            case -4379043:
                if (str.equals("elevation")) {
                    c2 = '\f';
                    break;
                }
                break;
            case 37232917:
                if (str.equals("transitionPathRotate")) {
                    c2 = '\r';
                    break;
                }
                break;
            case 92909918:
                if (str.equals("alpha")) {
                    c2 = 14;
                    break;
                }
                break;
            case 156108012:
                if (str.equals("waveOffset")) {
                    c2 = 15;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return new i();
            case 1:
                return new j();
            case 2:
                return new m();
            case 3:
                return new n();
            case 4:
                return new o();
            case 5:
                return new g();
            case 6:
                return new k();
            case 7:
                return new l();
            case '\b':
                return new a();
            case '\t':
                return new e();
            case '\n':
                return new f();
            case 11:
                return new h();
            case '\f':
                return new c();
            case '\r':
                return new d();
            case 14:
                return new a();
            case 15:
                return new a();
            default:
                return null;
        }
    }

    public abstract void h(View view, float f2);
}
