package defpackage;

import java.math.BigInteger;

/* renamed from: xo3  reason: default package */
/* loaded from: classes2.dex */
public class xo3 {
    public final BigInteger a;
    public final int b;

    public xo3(BigInteger bigInteger, int i) {
        if (i < 0) {
            throw new IllegalArgumentException("scale may not be negative");
        }
        this.a = bigInteger;
        this.b = i;
    }

    public xo3 a(xo3 xo3Var) {
        c(xo3Var);
        return new xo3(this.a.add(xo3Var.a), this.b);
    }

    public xo3 b(int i) {
        if (i >= 0) {
            int i2 = this.b;
            return i == i2 ? this : new xo3(this.a.shiftLeft(i - i2), i);
        }
        throw new IllegalArgumentException("scale may not be negative");
    }

    public final void c(xo3 xo3Var) {
        if (this.b != xo3Var.b) {
            throw new IllegalArgumentException("Only SimpleBigDecimal of same scale allowed in arithmetic operations");
        }
    }

    public int d(BigInteger bigInteger) {
        return this.a.compareTo(bigInteger.shiftLeft(this.b));
    }

    public BigInteger e() {
        return this.a.shiftRight(this.b);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof xo3) {
            xo3 xo3Var = (xo3) obj;
            return this.a.equals(xo3Var.a) && this.b == xo3Var.b;
        }
        return false;
    }

    public int f() {
        return this.b;
    }

    public xo3 g() {
        return new xo3(this.a.negate(), this.b);
    }

    public BigInteger h() {
        return a(new xo3(ws0.b, 1).b(this.b)).e();
    }

    public int hashCode() {
        return this.a.hashCode() ^ this.b;
    }

    public xo3 i(xo3 xo3Var) {
        return a(xo3Var.g());
    }

    public xo3 j(BigInteger bigInteger) {
        return new xo3(this.a.subtract(bigInteger.shiftLeft(this.b)), this.b);
    }

    public String toString() {
        if (this.b == 0) {
            return this.a.toString();
        }
        BigInteger e = e();
        BigInteger subtract = this.a.subtract(e.shiftLeft(this.b));
        if (this.a.signum() == -1) {
            subtract = ws0.b.shiftLeft(this.b).subtract(subtract);
        }
        if (e.signum() == -1 && !subtract.equals(ws0.a)) {
            e = e.add(ws0.b);
        }
        String bigInteger = e.toString();
        char[] cArr = new char[this.b];
        String bigInteger2 = subtract.toString(2);
        int length = bigInteger2.length();
        int i = this.b - length;
        for (int i2 = 0; i2 < i; i2++) {
            cArr[i2] = '0';
        }
        for (int i3 = 0; i3 < length; i3++) {
            cArr[i + i3] = bigInteger2.charAt(i3);
        }
        String str = new String(cArr);
        StringBuffer stringBuffer = new StringBuffer(bigInteger);
        stringBuffer.append(".");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }
}
