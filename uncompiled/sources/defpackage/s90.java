package defpackage;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.crashlytics.internal.common.a;
import defpackage.r90;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: CrashlyticsReportDataCapture.java */
/* renamed from: s90  reason: default package */
/* loaded from: classes2.dex */
public class s90 {
    public static final Map<String, Integer> e;
    public static final String f;
    public final Context a;
    public final ln1 b;
    public final a c;
    public final is3 d;

    static {
        HashMap hashMap = new HashMap();
        e = hashMap;
        hashMap.put("armeabi", 5);
        hashMap.put("armeabi-v7a", 6);
        hashMap.put("arm64-v8a", 9);
        hashMap.put("x86", 0);
        hashMap.put("x86_64", 1);
        f = String.format(Locale.US, "Crashlytics Android SDK/%s", "18.2.0");
    }

    public s90(Context context, ln1 ln1Var, a aVar, is3 is3Var) {
        this.a = context;
        this.b = ln1Var;
        this.c = aVar;
        this.d = is3Var;
    }

    public static int e() {
        Integer num;
        String str = Build.CPU_ABI;
        if (TextUtils.isEmpty(str) || (num = e.get(str.toLowerCase(Locale.US))) == null) {
            return 7;
        }
        return num.intValue();
    }

    public final r90.b a() {
        return r90.b().h("18.2.0").d(this.c.a).e(this.b.a()).b(this.c.e).c(this.c.f).g(4);
    }

    public r90.e.d b(r90.a aVar) {
        int i = this.a.getResources().getConfiguration().orientation;
        return r90.e.d.a().f("anr").e(aVar.h()).b(h(i, aVar)).c(j(i)).a();
    }

    public r90.e.d c(Throwable th, Thread thread, String str, long j, int i, int i2, boolean z) {
        int i3 = this.a.getResources().getConfiguration().orientation;
        return r90.e.d.a().f(str).e(j).b(i(i3, new zb4(th, this.d), thread, i, i2, z)).c(j(i3)).a();
    }

    public r90 d(String str, long j) {
        return a().i(r(str, j)).a();
    }

    public final r90.e.d.a.b.AbstractC0266a f() {
        return r90.e.d.a.b.AbstractC0266a.a().b(0L).d(0L).c(this.c.d).e(this.c.b).a();
    }

    public final ip1<r90.e.d.a.b.AbstractC0266a> g() {
        return ip1.i(f());
    }

    public final r90.e.d.a h(int i, r90.a aVar) {
        return r90.e.d.a.a().b(Boolean.valueOf(aVar.b() != 100)).f(i).d(m(aVar)).a();
    }

    public final r90.e.d.a i(int i, zb4 zb4Var, Thread thread, int i2, int i3, boolean z) {
        Boolean bool;
        ActivityManager.RunningAppProcessInfo j = CommonUtils.j(this.c.d, this.a);
        if (j != null) {
            bool = Boolean.valueOf(j.importance != 100);
        } else {
            bool = null;
        }
        return r90.e.d.a.a().b(bool).f(i).d(n(zb4Var, thread, i2, i3, z)).a();
    }

    public final r90.e.d.c j(int i) {
        ro a = ro.a(this.a);
        Float b = a.b();
        Double valueOf = b != null ? Double.valueOf(b.doubleValue()) : null;
        int c = a.c();
        boolean o = CommonUtils.o(this.a);
        return r90.e.d.c.a().b(valueOf).c(c).f(o).e(i).g(CommonUtils.s() - CommonUtils.a(this.a)).d(CommonUtils.b(Environment.getDataDirectory().getPath())).a();
    }

    public final r90.e.d.a.b.c k(zb4 zb4Var, int i, int i2) {
        return l(zb4Var, i, i2, 0);
    }

    public final r90.e.d.a.b.c l(zb4 zb4Var, int i, int i2, int i3) {
        String str = zb4Var.b;
        String str2 = zb4Var.a;
        StackTraceElement[] stackTraceElementArr = zb4Var.c;
        int i4 = 0;
        if (stackTraceElementArr == null) {
            stackTraceElementArr = new StackTraceElement[0];
        }
        zb4 zb4Var2 = zb4Var.d;
        if (i3 >= i2) {
            zb4 zb4Var3 = zb4Var2;
            while (zb4Var3 != null) {
                zb4Var3 = zb4Var3.d;
                i4++;
            }
        }
        r90.e.d.a.b.c.AbstractC0269a d = r90.e.d.a.b.c.a().f(str).e(str2).c(ip1.e(p(stackTraceElementArr, i))).d(i4);
        if (zb4Var2 != null && i4 == 0) {
            d.b(l(zb4Var2, i, i2, i3 + 1));
        }
        return d.a();
    }

    public final r90.e.d.a.b m(r90.a aVar) {
        return r90.e.d.a.b.a().b(aVar).e(u()).c(g()).a();
    }

    public final r90.e.d.a.b n(zb4 zb4Var, Thread thread, int i, int i2, boolean z) {
        return r90.e.d.a.b.a().f(x(zb4Var, thread, i, z)).d(k(zb4Var, i, i2)).e(u()).c(g()).a();
    }

    public final r90.e.d.a.b.AbstractC0272e.AbstractC0274b o(StackTraceElement stackTraceElement, r90.e.d.a.b.AbstractC0272e.AbstractC0274b.AbstractC0275a abstractC0275a) {
        long j = 0;
        long max = stackTraceElement.isNativeMethod() ? Math.max(stackTraceElement.getLineNumber(), 0L) : 0L;
        String str = stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName();
        String fileName = stackTraceElement.getFileName();
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            j = stackTraceElement.getLineNumber();
        }
        return abstractC0275a.e(max).f(str).b(fileName).d(j).a();
    }

    public final ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> p(StackTraceElement[] stackTraceElementArr, int i) {
        ArrayList arrayList = new ArrayList();
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            arrayList.add(o(stackTraceElement, r90.e.d.a.b.AbstractC0272e.AbstractC0274b.a().c(i)));
        }
        return ip1.e(arrayList);
    }

    public final r90.e.a q() {
        r90.e.a.AbstractC0264a f2 = r90.e.a.a().e(this.b.f()).g(this.c.e).d(this.c.f).f(this.b.a());
        String a = this.c.g.a();
        if (a != null) {
            f2.b("Unity").c(a);
        }
        return f2.a();
    }

    public final r90.e r(String str, long j) {
        return r90.e.a().l(j).i(str).g(f).b(q()).k(t()).d(s()).h(3).a();
    }

    public final r90.e.c s() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int e2 = e();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long s = CommonUtils.s();
        long blockCount = statFs.getBlockCount() * statFs.getBlockSize();
        boolean x = CommonUtils.x(this.a);
        int m = CommonUtils.m(this.a);
        String str = Build.MANUFACTURER;
        return r90.e.c.a().b(e2).f(Build.MODEL).c(availableProcessors).h(s).d(blockCount).i(x).j(m).e(str).g(Build.PRODUCT).a();
    }

    public final r90.e.AbstractC0277e t() {
        return r90.e.AbstractC0277e.a().d(3).e(Build.VERSION.RELEASE).b(Build.VERSION.CODENAME).c(CommonUtils.y(this.a)).a();
    }

    public final r90.e.d.a.b.AbstractC0270d u() {
        return r90.e.d.a.b.AbstractC0270d.a().d("0").c("0").b(0L).a();
    }

    public final r90.e.d.a.b.AbstractC0272e v(Thread thread, StackTraceElement[] stackTraceElementArr) {
        return w(thread, stackTraceElementArr, 0);
    }

    public final r90.e.d.a.b.AbstractC0272e w(Thread thread, StackTraceElement[] stackTraceElementArr, int i) {
        return r90.e.d.a.b.AbstractC0272e.a().d(thread.getName()).c(i).b(ip1.e(p(stackTraceElementArr, i))).a();
    }

    public final ip1<r90.e.d.a.b.AbstractC0272e> x(zb4 zb4Var, Thread thread, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(w(thread, zb4Var.c, i));
        if (z) {
            for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
                Thread key = entry.getKey();
                if (!key.equals(thread)) {
                    arrayList.add(v(key, this.d.a(entry.getValue())));
                }
            }
        }
        return ip1.e(arrayList);
    }
}
