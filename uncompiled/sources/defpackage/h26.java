package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: h26  reason: default package */
/* loaded from: classes.dex */
public final class h26 implements d26 {
    public static final wo5<Boolean> a;
    public static final wo5<Boolean> b;
    public static final wo5<Boolean> c;
    public static final wo5<Boolean> d;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.sdk.collection.enable_extend_user_property_size", true);
        b = ro5Var.b("measurement.sdk.collection.last_deep_link_referrer2", true);
        c = ro5Var.b("measurement.sdk.collection.last_deep_link_referrer_campaign2", false);
        d = ro5Var.b("measurement.sdk.collection.last_gclid_from_referrer2", false);
        ro5Var.a("measurement.id.sdk.collection.last_deep_link_referrer2", 0L);
    }

    @Override // defpackage.d26
    public final boolean b() {
        return d.e().booleanValue();
    }

    @Override // defpackage.d26
    public final boolean zza() {
        return a.e().booleanValue();
    }

    @Override // defpackage.d26
    public final boolean zzb() {
        return b.e().booleanValue();
    }

    @Override // defpackage.d26
    public final boolean zzc() {
        return c.e().booleanValue();
    }
}
