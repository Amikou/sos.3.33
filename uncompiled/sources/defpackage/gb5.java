package defpackage;

import com.google.android.gms.internal.clearcut.g;
import com.google.android.gms.internal.clearcut.o;
import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;

/* renamed from: gb5  reason: default package */
/* loaded from: classes.dex */
public final class gb5 {
    public static final Charset a = Charset.forName("UTF-8");
    public static final byte[] b;

    static {
        Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING);
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        g.b(bArr, 0, bArr.length, false);
    }

    public static <T> T a(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static int b(byte[] bArr) {
        int length = bArr.length;
        int c = c(length, bArr, 0, length);
        if (c == 0) {
            return 1;
        }
        return c;
    }

    public static int c(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i2 + i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    public static Object d(Object obj, Object obj2) {
        return ((o) obj).f().z((o) obj2).w();
    }

    public static <T> T e(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static int f(boolean z) {
        return z ? 1231 : 1237;
    }

    public static boolean g(byte[] bArr) {
        return zi5.h(bArr);
    }

    public static String h(byte[] bArr) {
        return new String(bArr, a);
    }

    public static boolean i(o oVar) {
        return false;
    }

    public static int j(long j) {
        return (int) (j ^ (j >>> 32));
    }
}
