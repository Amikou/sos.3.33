package defpackage;

/* compiled from: SerializableString.java */
/* renamed from: yl3  reason: default package */
/* loaded from: classes.dex */
public interface yl3 {
    int appendQuotedUTF8(byte[] bArr, int i);

    char[] asQuotedChars();

    byte[] asQuotedUTF8();

    byte[] asUnquotedUTF8();

    String getValue();
}
