package defpackage;

import java.security.InvalidKeyException;

/* compiled from: XChaCha20Poly1305.java */
/* renamed from: xr4  reason: default package */
/* loaded from: classes2.dex */
public final class xr4 extends ax {
    public xr4(byte[] bArr) throws InvalidKeyException {
        super(bArr);
    }

    @Override // defpackage.ax
    public yw g(byte[] bArr, int i) throws InvalidKeyException {
        return new wr4(bArr, i);
    }
}
