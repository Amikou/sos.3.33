package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: gh3  reason: default package */
/* loaded from: classes2.dex */
public class gh3 extends xs0.b {
    public hh3 j;

    /* renamed from: gh3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] b = fd2.b();
            long[] b2 = fd2.b();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                long j = ((i3 ^ i) - 1) >> 31;
                for (int i4 = 0; i4 < 5; i4++) {
                    long j2 = b[i4];
                    long[] jArr = this.b;
                    b[i4] = j2 ^ (jArr[i2 + i4] & j);
                    b2[i4] = b2[i4] ^ (jArr[(i2 + 5) + i4] & j);
                }
                i2 += 10;
            }
            return gh3.this.i(new fh3(b), new fh3(b2), false);
        }
    }

    public gh3() {
        super(283, 5, 7, 12);
        this.j = new hh3(this, null, null);
        this.b = n(BigInteger.valueOf(0L));
        this.c = n(BigInteger.valueOf(1L));
        this.d = new BigInteger(1, pk1.a("01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9AE2ED07577265DFF7F94451E061E163C61"));
        this.e = BigInteger.valueOf(4L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return true;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new gh3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 5 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            fd2.a(((fh3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 5;
            fd2.a(((fh3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 5;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public it0 f() {
        return new ll4();
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new hh3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new hh3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new fh3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 283;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
