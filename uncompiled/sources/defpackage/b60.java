package defpackage;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ConstrainedExecutorService.java */
/* renamed from: b60  reason: default package */
/* loaded from: classes.dex */
public class b60 extends AbstractExecutorService {
    public static final Class<?> l0 = b60.class;
    public final String a;
    public final Executor f0;
    public volatile int g0;
    public final BlockingQueue<Runnable> h0;
    public final b i0;
    public final AtomicInteger j0;
    public final AtomicInteger k0;

    /* compiled from: ConstrainedExecutorService.java */
    /* renamed from: b60$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                Runnable runnable = (Runnable) b60.this.h0.poll();
                if (runnable == null) {
                    v11.o(b60.l0, "%s: Worker has nothing to run", b60.this.a);
                } else {
                    runnable.run();
                }
                int decrementAndGet = b60.this.j0.decrementAndGet();
                if (!b60.this.h0.isEmpty()) {
                    b60.this.f();
                } else {
                    v11.p(b60.l0, "%s: worker finished; %d workers left", b60.this.a, Integer.valueOf(decrementAndGet));
                }
            } catch (Throwable th) {
                int decrementAndGet2 = b60.this.j0.decrementAndGet();
                if (!b60.this.h0.isEmpty()) {
                    b60.this.f();
                } else {
                    v11.p(b60.l0, "%s: worker finished; %d workers left", b60.this.a, Integer.valueOf(decrementAndGet2));
                }
                throw th;
            }
        }
    }

    public b60(String str, int i, Executor executor, BlockingQueue<Runnable> blockingQueue) {
        if (i > 0) {
            this.a = str;
            this.f0 = executor;
            this.g0 = i;
            this.h0 = blockingQueue;
            this.i0 = new b();
            this.j0 = new AtomicInteger(0);
            this.k0 = new AtomicInteger(0);
            return;
        }
        throw new IllegalArgumentException("max concurrency must be > 0");
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        Objects.requireNonNull(runnable, "runnable parameter is null");
        if (this.h0.offer(runnable)) {
            int size = this.h0.size();
            int i = this.k0.get();
            if (size > i && this.k0.compareAndSet(i, size)) {
                v11.p(l0, "%s: max pending work in queue = %d", this.a, Integer.valueOf(size));
            }
            f();
            return;
        }
        throw new RejectedExecutionException(this.a + " queue is full, size=" + this.h0.size());
    }

    public final void f() {
        int i = this.j0.get();
        while (i < this.g0) {
            int i2 = i + 1;
            if (this.j0.compareAndSet(i, i2)) {
                v11.q(l0, "%s: starting worker %d of %d", this.a, Integer.valueOf(i2), Integer.valueOf(this.g0));
                this.f0.execute(this.i0);
                return;
            }
            v11.o(l0, "%s: race in startWorkerIfNeeded; retrying", this.a);
            i = this.j0.get();
        }
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isShutdown() {
        return false;
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isTerminated() {
        return false;
    }

    @Override // java.util.concurrent.ExecutorService
    public void shutdown() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.concurrent.ExecutorService
    public List<Runnable> shutdownNow() {
        throw new UnsupportedOperationException();
    }
}
