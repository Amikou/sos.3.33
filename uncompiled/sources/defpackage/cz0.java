package defpackage;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.a;
import net.safemoon.androidwallet.R;

/* compiled from: ExitDialog.java */
/* renamed from: cz0  reason: default package */
/* loaded from: classes2.dex */
public class cz0 extends a {
    public static a c(Context context, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        a.C0008a c0008a = new a.C0008a(context);
        c0008a.b(true);
        c0008a.k(R.string.logout);
        c0008a.f(context.getString(R.string.logout_msg));
        c0008a.setPositiveButton(R.string.action_yes, onClickListener);
        c0008a.setNegativeButton(R.string.action_no, onClickListener2);
        return c0008a.create();
    }
}
