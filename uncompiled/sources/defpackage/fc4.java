package defpackage;

import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import androidx.media3.common.ParserException;
import defpackage.gc4;
import defpackage.wi3;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: TsExtractor.java */
/* renamed from: fc4  reason: default package */
/* loaded from: classes.dex */
public final class fc4 implements p11 {
    public final int a;
    public final int b;
    public final List<h64> c;
    public final op2 d;
    public final SparseIntArray e;
    public final gc4.c f;
    public final SparseArray<gc4> g;
    public final SparseBooleanArray h;
    public final SparseBooleanArray i;
    public final dc4 j;
    public cc4 k;
    public r11 l;
    public int m;
    public boolean n;
    public boolean o;
    public boolean p;
    public gc4 q;
    public int r;
    public int s;

    /* compiled from: TsExtractor.java */
    /* renamed from: fc4$a */
    /* loaded from: classes.dex */
    public class a implements wh3 {
        public final np2 a = new np2(new byte[4]);

        public a() {
        }

        @Override // defpackage.wh3
        public void a(op2 op2Var) {
            if (op2Var.D() == 0 && (op2Var.D() & 128) != 0) {
                op2Var.Q(6);
                int a = op2Var.a() / 4;
                for (int i = 0; i < a; i++) {
                    op2Var.i(this.a, 4);
                    int h = this.a.h(16);
                    this.a.r(3);
                    if (h == 0) {
                        this.a.r(13);
                    } else {
                        int h2 = this.a.h(13);
                        if (fc4.this.g.get(h2) == null) {
                            fc4.this.g.put(h2, new xh3(new b(h2)));
                            fc4.l(fc4.this);
                        }
                    }
                }
                if (fc4.this.a != 2) {
                    fc4.this.g.remove(0);
                }
            }
        }

        @Override // defpackage.wh3
        public void b(h64 h64Var, r11 r11Var, gc4.d dVar) {
        }
    }

    /* compiled from: TsExtractor.java */
    /* renamed from: fc4$b */
    /* loaded from: classes.dex */
    public class b implements wh3 {
        public final np2 a = new np2(new byte[5]);
        public final SparseArray<gc4> b = new SparseArray<>();
        public final SparseIntArray c = new SparseIntArray();
        public final int d;

        public b(int i) {
            this.d = i;
        }

        @Override // defpackage.wh3
        public void a(op2 op2Var) {
            h64 h64Var;
            if (op2Var.D() != 2) {
                return;
            }
            if (fc4.this.a == 1 || fc4.this.a == 2 || fc4.this.m == 1) {
                h64Var = (h64) fc4.this.c.get(0);
            } else {
                h64Var = new h64(((h64) fc4.this.c.get(0)).c());
                fc4.this.c.add(h64Var);
            }
            if ((op2Var.D() & 128) == 0) {
                return;
            }
            op2Var.Q(1);
            int J = op2Var.J();
            int i = 3;
            op2Var.Q(3);
            op2Var.i(this.a, 2);
            this.a.r(3);
            int i2 = 13;
            fc4.this.s = this.a.h(13);
            op2Var.i(this.a, 2);
            int i3 = 4;
            this.a.r(4);
            op2Var.Q(this.a.h(12));
            if (fc4.this.a == 2 && fc4.this.q == null) {
                gc4.b bVar = new gc4.b(21, null, null, androidx.media3.common.util.b.f);
                fc4 fc4Var = fc4.this;
                fc4Var.q = fc4Var.f.b(21, bVar);
                if (fc4.this.q != null) {
                    fc4.this.q.b(h64Var, fc4.this.l, new gc4.d(J, 21, 8192));
                }
            }
            this.b.clear();
            this.c.clear();
            int a = op2Var.a();
            while (a > 0) {
                op2Var.i(this.a, 5);
                int h = this.a.h(8);
                this.a.r(i);
                int h2 = this.a.h(i2);
                this.a.r(i3);
                int h3 = this.a.h(12);
                gc4.b c = c(op2Var, h3);
                if (h == 6 || h == 5) {
                    h = c.a;
                }
                a -= h3 + 5;
                int i4 = fc4.this.a == 2 ? h : h2;
                if (!fc4.this.h.get(i4)) {
                    gc4 b = (fc4.this.a == 2 && h == 21) ? fc4.this.q : fc4.this.f.b(h, c);
                    if (fc4.this.a != 2 || h2 < this.c.get(i4, 8192)) {
                        this.c.put(i4, h2);
                        this.b.put(i4, b);
                    }
                }
                i = 3;
                i3 = 4;
                i2 = 13;
            }
            int size = this.c.size();
            for (int i5 = 0; i5 < size; i5++) {
                int keyAt = this.c.keyAt(i5);
                int valueAt = this.c.valueAt(i5);
                fc4.this.h.put(keyAt, true);
                fc4.this.i.put(valueAt, true);
                gc4 valueAt2 = this.b.valueAt(i5);
                if (valueAt2 != null) {
                    if (valueAt2 != fc4.this.q) {
                        valueAt2.b(h64Var, fc4.this.l, new gc4.d(J, keyAt, 8192));
                    }
                    fc4.this.g.put(valueAt, valueAt2);
                }
            }
            if (fc4.this.a == 2) {
                if (fc4.this.n) {
                    return;
                }
                fc4.this.l.m();
                fc4.this.m = 0;
                fc4.this.n = true;
                return;
            }
            fc4.this.g.remove(this.d);
            fc4 fc4Var2 = fc4.this;
            fc4Var2.m = fc4Var2.a == 1 ? 0 : fc4.this.m - 1;
            if (fc4.this.m == 0) {
                fc4.this.l.m();
                fc4.this.n = true;
            }
        }

        @Override // defpackage.wh3
        public void b(h64 h64Var, r11 r11Var, gc4.d dVar) {
        }

        public final gc4.b c(op2 op2Var, int i) {
            int e = op2Var.e();
            int i2 = i + e;
            String str = null;
            int i3 = -1;
            ArrayList arrayList = null;
            while (op2Var.e() < i2) {
                int D = op2Var.D();
                int e2 = op2Var.e() + op2Var.D();
                if (e2 > i2) {
                    break;
                }
                if (D == 5) {
                    long F = op2Var.F();
                    if (F != 1094921523) {
                        if (F != 1161904947) {
                            if (F != 1094921524) {
                                if (F == 1212503619) {
                                    i3 = 36;
                                }
                            }
                            i3 = 172;
                        }
                        i3 = 135;
                    }
                    i3 = 129;
                } else {
                    if (D != 106) {
                        if (D != 122) {
                            if (D == 127) {
                                if (op2Var.D() != 21) {
                                }
                                i3 = 172;
                            } else if (D == 123) {
                                i3 = 138;
                            } else if (D == 10) {
                                str = op2Var.A(3).trim();
                            } else if (D == 89) {
                                arrayList = new ArrayList();
                                while (op2Var.e() < e2) {
                                    String trim = op2Var.A(3).trim();
                                    int D2 = op2Var.D();
                                    byte[] bArr = new byte[4];
                                    op2Var.j(bArr, 0, 4);
                                    arrayList.add(new gc4.a(trim, D2, bArr));
                                }
                                i3 = 89;
                            } else if (D == 111) {
                                i3 = 257;
                            }
                        }
                        i3 = 135;
                    }
                    i3 = 129;
                }
                op2Var.Q(e2 - op2Var.e());
            }
            op2Var.P(i2);
            return new gc4.b(i3, str, arrayList, Arrays.copyOfRange(op2Var.d(), e, i2));
        }
    }

    static {
        ec4 ec4Var = ec4.b;
    }

    public fc4() {
        this(0);
    }

    public static /* synthetic */ int l(fc4 fc4Var) {
        int i = fc4Var.m;
        fc4Var.m = i + 1;
        return i;
    }

    public static /* synthetic */ p11[] x() {
        return new p11[]{new fc4()};
    }

    public final boolean A(int i) {
        return this.a == 2 || this.n || !this.i.get(i, false);
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        cc4 cc4Var;
        ii.g(this.a != 2);
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            h64 h64Var = this.c.get(i);
            boolean z = h64Var.e() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            if (!z) {
                long c = h64Var.c();
                z = (c == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || c == 0 || c == j2) ? false : true;
            }
            if (z) {
                h64Var.g(j2);
            }
        }
        if (j2 != 0 && (cc4Var = this.k) != null) {
            cc4Var.h(j2);
        }
        this.d.L(0);
        this.e.clear();
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            this.g.valueAt(i2).c();
        }
        this.r = 0;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        long length = q11Var.getLength();
        if (this.n) {
            if (((length == -1 || this.a == 2) ? false : true) && !this.j.d()) {
                return this.j.e(q11Var, ot2Var, this.s);
            }
            y(length);
            if (this.p) {
                this.p = false;
                c(0L, 0L);
                if (q11Var.getPosition() != 0) {
                    ot2Var.a = 0L;
                    return 1;
                }
            }
            cc4 cc4Var = this.k;
            if (cc4Var != null && cc4Var.d()) {
                return this.k.c(q11Var, ot2Var);
            }
        }
        if (v(q11Var)) {
            int w = w();
            int f = this.d.f();
            if (w > f) {
                return 0;
            }
            int n = this.d.n();
            if ((8388608 & n) != 0) {
                this.d.P(w);
                return 0;
            }
            int i = ((4194304 & n) != 0 ? 1 : 0) | 0;
            int i2 = (2096896 & n) >> 8;
            boolean z = (n & 32) != 0;
            gc4 gc4Var = (n & 16) != 0 ? this.g.get(i2) : null;
            if (gc4Var == null) {
                this.d.P(w);
                return 0;
            }
            if (this.a != 2) {
                int i3 = n & 15;
                int i4 = this.e.get(i2, i3 - 1);
                this.e.put(i2, i3);
                if (i4 == i3) {
                    this.d.P(w);
                    return 0;
                } else if (i3 != ((i4 + 1) & 15)) {
                    gc4Var.c();
                }
            }
            if (z) {
                int D = this.d.D();
                i |= (this.d.D() & 64) != 0 ? 2 : 0;
                this.d.Q(D - 1);
            }
            boolean z2 = this.n;
            if (A(i2)) {
                this.d.O(w);
                gc4Var.a(this.d, i);
                this.d.O(f);
            }
            if (this.a != 2 && !z2 && this.n && length != -1) {
                this.p = true;
            }
            this.d.P(w);
            return 0;
        }
        return -1;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        boolean z;
        byte[] d = this.d.d();
        q11Var.n(d, 0, 940);
        for (int i = 0; i < 188; i++) {
            int i2 = 0;
            while (true) {
                if (i2 >= 5) {
                    z = true;
                    break;
                } else if (d[(i2 * 188) + i] != 71) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                q11Var.k(i);
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.l = r11Var;
    }

    public final boolean v(q11 q11Var) throws IOException {
        byte[] d = this.d.d();
        if (9400 - this.d.e() < 188) {
            int a2 = this.d.a();
            if (a2 > 0) {
                System.arraycopy(d, this.d.e(), d, 0, a2);
            }
            this.d.N(d, a2);
        }
        while (this.d.a() < 188) {
            int f = this.d.f();
            int read = q11Var.read(d, f, 9400 - f);
            if (read == -1) {
                return false;
            }
            this.d.O(f + read);
        }
        return true;
    }

    public final int w() throws ParserException {
        int e = this.d.e();
        int f = this.d.f();
        int a2 = hc4.a(this.d.d(), e, f);
        this.d.P(a2);
        int i = a2 + 188;
        if (i > f) {
            int i2 = this.r + (a2 - e);
            this.r = i2;
            if (this.a == 2 && i2 > 376) {
                throw ParserException.createForMalformedContainer("Cannot find sync byte. Most likely not a Transport Stream.", null);
            }
        } else {
            this.r = 0;
        }
        return i;
    }

    public final void y(long j) {
        if (this.o) {
            return;
        }
        this.o = true;
        if (this.j.b() != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            cc4 cc4Var = new cc4(this.j.c(), this.j.b(), j, this.s, this.b);
            this.k = cc4Var;
            this.l.p(cc4Var.b());
            return;
        }
        this.l.p(new wi3.b(this.j.b()));
    }

    public final void z() {
        this.h.clear();
        this.g.clear();
        SparseArray<gc4> a2 = this.f.a();
        int size = a2.size();
        for (int i = 0; i < size; i++) {
            this.g.put(a2.keyAt(i), a2.valueAt(i));
        }
        this.g.put(0, new xh3(new a()));
        this.q = null;
    }

    public fc4(int i) {
        this(1, i, 112800);
    }

    public fc4(int i, int i2, int i3) {
        this(i, new h64(0L), new ml0(i2), i3);
    }

    public fc4(int i, h64 h64Var, gc4.c cVar, int i2) {
        this.f = (gc4.c) ii.e(cVar);
        this.b = i2;
        this.a = i;
        if (i != 1 && i != 2) {
            ArrayList arrayList = new ArrayList();
            this.c = arrayList;
            arrayList.add(h64Var);
        } else {
            this.c = Collections.singletonList(h64Var);
        }
        this.d = new op2(new byte[9400], 0);
        this.h = new SparseBooleanArray();
        this.i = new SparseBooleanArray();
        this.g = new SparseArray<>();
        this.e = new SparseIntArray();
        this.j = new dc4(i2);
        this.l = r11.e;
        this.s = -1;
        z();
    }
}
