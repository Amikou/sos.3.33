package defpackage;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: MovementBounds.java */
/* renamed from: ba2  reason: default package */
/* loaded from: classes.dex */
public class ba2 {
    public static final Matrix f = new Matrix();
    public static final float[] g = new float[2];
    public static final Point h = new Point();
    public static final Rect i = new Rect();
    public static final RectF j = new RectF();
    public final Settings a;
    public final RectF b = new RectF();
    public float c;
    public float d;
    public float e;

    /* compiled from: MovementBounds.java */
    /* renamed from: ba2$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[Settings.Bounds.values().length];
            a = iArr;
            try {
                iArr[Settings.Bounds.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[Settings.Bounds.INSIDE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[Settings.Bounds.OUTSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[Settings.Bounds.PIVOT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[Settings.Bounds.NONE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    public ba2(Settings settings) {
        this.a = settings;
    }

    public final void a(RectF rectF, Rect rect) {
        if (rectF.width() < rect.width()) {
            this.b.left = rectF.left - (rect.width() - rectF.width());
            this.b.right = rectF.left;
        } else {
            RectF rectF2 = this.b;
            rectF2.left = rectF.left;
            rectF2.right = rectF.right - rect.width();
        }
        if (rectF.height() < rect.height()) {
            this.b.top = rectF.top - (rect.height() - rectF.height());
            this.b.bottom = rectF.top;
            return;
        }
        RectF rectF3 = this.b;
        rectF3.top = rectF.top;
        rectF3.bottom = rectF.bottom - rect.height();
    }

    public final void b(RectF rectF, Rect rect) {
        if (rectF.width() < rect.width()) {
            this.b.left = rectF.left - (rect.width() - rectF.width());
            this.b.right = rectF.left;
        } else {
            RectF rectF2 = this.b;
            float f2 = rect.left;
            rectF2.right = f2;
            rectF2.left = f2;
        }
        if (rectF.height() < rect.height()) {
            this.b.top = rectF.top - (rect.height() - rectF.height());
            this.b.bottom = rectF.top;
            return;
        }
        RectF rectF3 = this.b;
        float f3 = rect.top;
        rectF3.bottom = f3;
        rectF3.top = f3;
    }

    public final void c(RectF rectF, Rect rect) {
        this.b.left = rectF.left - rect.width();
        RectF rectF2 = this.b;
        rectF2.right = rectF.right;
        rectF2.top = rectF.top - rect.height();
        this.b.bottom = rectF.bottom;
    }

    public final void d(Rect rect) {
        Settings settings = this.a;
        Point point = h;
        si1.a(settings, point);
        float[] fArr = g;
        fArr[0] = point.x;
        fArr[1] = point.y;
        if (!us3.c(this.c, Utils.FLOAT_EPSILON)) {
            Matrix matrix = f;
            matrix.setRotate(-this.c, this.d, this.e);
            matrix.mapPoints(fArr);
        }
        this.b.left = fArr[0] - rect.width();
        RectF rectF = this.b;
        rectF.right = fArr[0];
        rectF.top = fArr[1] - rect.height();
        this.b.bottom = fArr[1];
    }

    public void e(float f2, float f3) {
        float[] fArr = g;
        fArr[0] = f2;
        fArr[1] = f3;
        float f4 = this.c;
        if (f4 != Utils.FLOAT_EPSILON) {
            Matrix matrix = f;
            matrix.setRotate(-f4, this.d, this.e);
            matrix.mapPoints(fArr);
        }
        this.b.union(fArr[0], fArr[1]);
    }

    public void f(RectF rectF) {
        float f2 = this.c;
        if (f2 == Utils.FLOAT_EPSILON) {
            rectF.set(this.b);
            return;
        }
        Matrix matrix = f;
        matrix.setRotate(f2, this.d, this.e);
        matrix.mapRect(rectF, this.b);
    }

    public void g(float f2, float f3, float f4, float f5, PointF pointF) {
        float[] fArr = g;
        fArr[0] = f2;
        fArr[1] = f3;
        float f6 = this.c;
        if (f6 != Utils.FLOAT_EPSILON) {
            Matrix matrix = f;
            matrix.setRotate(-f6, this.d, this.e);
            matrix.mapPoints(fArr);
        }
        float f7 = fArr[0];
        RectF rectF = this.b;
        fArr[0] = v42.f(f7, rectF.left - f4, rectF.right + f4);
        float f8 = fArr[1];
        RectF rectF2 = this.b;
        fArr[1] = v42.f(f8, rectF2.top - f5, rectF2.bottom + f5);
        float f9 = this.c;
        if (f9 != Utils.FLOAT_EPSILON) {
            Matrix matrix2 = f;
            matrix2.setRotate(f9, this.d, this.e);
            matrix2.mapPoints(fArr);
        }
        pointF.set(fArr[0], fArr[1]);
    }

    public void h(float f2, float f3, PointF pointF) {
        g(f2, f3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, pointF);
    }

    public ba2 i(us3 us3Var) {
        RectF rectF = j;
        Settings settings = this.a;
        Rect rect = i;
        si1.d(settings, rect);
        rectF.set(rect);
        Settings.Fit i2 = this.a.i();
        Settings.Fit fit = Settings.Fit.OUTSIDE;
        if (i2 == fit) {
            this.c = us3Var.e();
            this.d = rectF.centerX();
            this.e = rectF.centerY();
            if (!us3.c(this.c, Utils.FLOAT_EPSILON)) {
                Matrix matrix = f;
                matrix.setRotate(-this.c, this.d, this.e);
                matrix.mapRect(rectF);
            }
        } else {
            this.c = Utils.FLOAT_EPSILON;
            this.e = Utils.FLOAT_EPSILON;
            this.d = Utils.FLOAT_EPSILON;
        }
        Matrix matrix2 = f;
        us3Var.d(matrix2);
        if (!us3.c(this.c, Utils.FLOAT_EPSILON)) {
            matrix2.postRotate(-this.c, this.d, this.e);
        }
        si1.c(matrix2, this.a, rect);
        int i3 = a.a[this.a.f().ordinal()];
        if (i3 == 1) {
            b(rectF, rect);
        } else if (i3 == 2) {
            a(rectF, rect);
        } else if (i3 == 3) {
            c(rectF, rect);
        } else if (i3 != 4) {
            this.b.set(-5.368709E8f, -5.368709E8f, 5.368709E8f, 5.368709E8f);
        } else {
            d(rect);
        }
        if (this.a.i() != fit) {
            us3Var.d(matrix2);
            rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.a.l(), this.a.k());
            matrix2.mapRect(rectF);
            float[] fArr = g;
            fArr[1] = 0.0f;
            fArr[0] = 0.0f;
            matrix2.mapPoints(fArr);
            this.b.offset(fArr[0] - rectF.left, fArr[1] - rectF.top);
        }
        return this;
    }
}
