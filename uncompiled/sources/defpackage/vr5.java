package defpackage;

import com.google.android.gms.internal.vision.zzix;
import com.google.android.gms.internal.vision.zzjm;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: vr5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class vr5 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[zzjm.values().length];
        b = iArr;
        try {
            iArr[zzjm.zzh.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            b[zzjm.zzj.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            b[zzjm.zzg.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        int[] iArr2 = new int[zzix.values().length];
        a = iArr2;
        try {
            iArr2[zzix.MAP.ordinal()] = 1;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            a[zzix.VECTOR.ordinal()] = 2;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            a[zzix.SCALAR.ordinal()] = 3;
        } catch (NoSuchFieldError unused6) {
        }
    }
}
