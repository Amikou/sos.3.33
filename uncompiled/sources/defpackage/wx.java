package defpackage;

/* compiled from: CharMatcher.java */
/* renamed from: wx  reason: default package */
/* loaded from: classes2.dex */
public abstract class wx implements gu2<Character> {

    /* compiled from: CharMatcher.java */
    /* renamed from: wx$a */
    /* loaded from: classes2.dex */
    public static abstract class a extends wx {
        @Override // defpackage.gu2
        @Deprecated
        public /* bridge */ /* synthetic */ boolean apply(Character ch) {
            return super.b(ch);
        }
    }

    /* compiled from: CharMatcher.java */
    /* renamed from: wx$b */
    /* loaded from: classes2.dex */
    public static final class b extends a {
        public final char a;

        public b(char c) {
            this.a = c;
        }

        @Override // defpackage.wx
        public boolean e(char c) {
            return c == this.a;
        }

        public String toString() {
            String g = wx.g(this.a);
            StringBuilder sb = new StringBuilder(String.valueOf(g).length() + 18);
            sb.append("CharMatcher.is('");
            sb.append(g);
            sb.append("')");
            return sb.toString();
        }
    }

    /* compiled from: CharMatcher.java */
    /* renamed from: wx$c */
    /* loaded from: classes2.dex */
    public static abstract class c extends a {
        public final String a;

        public c(String str) {
            this.a = (String) au2.k(str);
        }

        public final String toString() {
            return this.a;
        }
    }

    /* compiled from: CharMatcher.java */
    /* renamed from: wx$d */
    /* loaded from: classes2.dex */
    public static final class d extends c {
        public static final d f0 = new d();

        public d() {
            super("CharMatcher.none()");
        }

        @Override // defpackage.wx
        public int c(CharSequence charSequence, int i) {
            au2.m(i, charSequence.length());
            return -1;
        }

        @Override // defpackage.wx
        public boolean e(char c) {
            return false;
        }
    }

    public static wx d(char c2) {
        return new b(c2);
    }

    public static wx f() {
        return d.f0;
    }

    public static String g(char c2) {
        char[] cArr = {'\\', 'u', 0, 0, 0, 0};
        for (int i = 0; i < 4; i++) {
            cArr[5 - i] = "0123456789ABCDEF".charAt(c2 & 15);
            c2 = (char) (c2 >> 4);
        }
        return String.copyValueOf(cArr);
    }

    @Deprecated
    public boolean b(Character ch) {
        return e(ch.charValue());
    }

    public int c(CharSequence charSequence, int i) {
        int length = charSequence.length();
        au2.m(i, length);
        while (i < length) {
            if (e(charSequence.charAt(i))) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public abstract boolean e(char c2);
}
