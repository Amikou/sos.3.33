package defpackage;

/* compiled from: MemoryCacheParams.java */
/* renamed from: m72  reason: default package */
/* loaded from: classes.dex */
public class m72 {
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final long f;

    public m72(int i, int i2, int i3, int i4, int i5, long j) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = j;
    }
}
