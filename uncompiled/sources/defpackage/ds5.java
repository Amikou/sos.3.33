package defpackage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ds5  reason: default package */
/* loaded from: classes.dex */
public final class ds5 {
    public final Map<String, Callable<? extends c55>> a = new HashMap();

    public final void a(String str, Callable<? extends c55> callable) {
        this.a.put(str, callable);
    }
}
