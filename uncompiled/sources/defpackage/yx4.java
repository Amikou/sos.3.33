package defpackage;

import java.util.concurrent.Executor;

/* renamed from: yx4  reason: default package */
/* loaded from: classes2.dex */
public final class yx4 implements Executor {
    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
