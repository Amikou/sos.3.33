package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: ot3  reason: default package */
/* loaded from: classes.dex */
public class ot3 extends BasePendingResult<Status> {
    public ot3(@RecentlyNonNull GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    @RecentlyNonNull
    public /* synthetic */ Status d(@RecentlyNonNull Status status) {
        return status;
    }
}
