package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: g85  reason: default package */
/* loaded from: classes.dex */
public final class g85 implements h85 {
    public final wk5 a;
    public final String b;

    public g85(wk5 wk5Var, String str) {
        this.a = wk5Var;
        this.b = str;
    }

    @Override // defpackage.h85
    public final wk5 a(z55 z55Var) {
        wk5 c = this.a.c();
        c.f(this.b, z55Var);
        return c;
    }
}
