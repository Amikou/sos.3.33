package defpackage;

import android.util.SparseIntArray;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.internal.http2.Http2;

/* compiled from: DefaultNativeMemoryChunkPoolParams.java */
/* renamed from: fk0  reason: default package */
/* loaded from: classes.dex */
public class fk0 {
    public static ys2 a() {
        SparseIntArray sparseIntArray = new SparseIntArray();
        sparseIntArray.put(RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE, 5);
        sparseIntArray.put(2048, 5);
        sparseIntArray.put(4096, 5);
        sparseIntArray.put(8192, 5);
        sparseIntArray.put(Http2.INITIAL_MAX_FRAME_SIZE, 5);
        sparseIntArray.put(32768, 5);
        sparseIntArray.put(65536, 5);
        sparseIntArray.put(131072, 5);
        sparseIntArray.put(262144, 2);
        sparseIntArray.put(524288, 2);
        sparseIntArray.put(1048576, 2);
        return new ys2(c(), b(), sparseIntArray);
    }

    public static int b() {
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min < 16777216) {
            return min / 2;
        }
        return (min / 4) * 3;
    }

    public static int c() {
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min < 16777216) {
            return 3145728;
        }
        return min < 33554432 ? 6291456 : 12582912;
    }
}
