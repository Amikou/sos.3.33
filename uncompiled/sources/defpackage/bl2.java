package defpackage;

import com.onesignal.influence.domain.OSInfluenceChannel;

/* renamed from: bl2  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class bl2 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[OSInfluenceChannel.values().length];
        a = iArr;
        iArr[OSInfluenceChannel.NOTIFICATION.ordinal()] = 1;
        iArr[OSInfluenceChannel.IAM.ordinal()] = 2;
    }
}
