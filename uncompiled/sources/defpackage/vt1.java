package defpackage;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.util.Base64;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.zip.Adler32;

/* compiled from: JobInfoScheduler.java */
/* renamed from: vt1  reason: default package */
/* loaded from: classes.dex */
public class vt1 implements rq4 {
    public final Context a;
    public final dy0 b;
    public final SchedulerConfig c;

    public vt1(Context context, dy0 dy0Var, SchedulerConfig schedulerConfig) {
        this.a = context;
        this.b = dy0Var;
        this.c = schedulerConfig;
    }

    @Override // defpackage.rq4
    public void a(ob4 ob4Var, int i) {
        b(ob4Var, i, false);
    }

    @Override // defpackage.rq4
    public void b(ob4 ob4Var, int i, boolean z) {
        ComponentName componentName = new ComponentName(this.a, JobInfoSchedulerService.class);
        JobScheduler jobScheduler = (JobScheduler) this.a.getSystemService("jobscheduler");
        int c = c(ob4Var);
        if (!z && d(jobScheduler, c, i)) {
            z12.a("JobInfoScheduler", "Upload for context %s is already scheduled. Returning...", ob4Var);
            return;
        }
        long l0 = this.b.l0(ob4Var);
        JobInfo.Builder c2 = this.c.c(new JobInfo.Builder(c, componentName), ob4Var.d(), l0, i);
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putInt("attemptNumber", i);
        persistableBundle.putString("backendName", ob4Var.b());
        persistableBundle.putInt("priority", wu2.a(ob4Var.d()));
        if (ob4Var.c() != null) {
            persistableBundle.putString("extras", Base64.encodeToString(ob4Var.c(), 0));
        }
        c2.setExtras(persistableBundle);
        z12.b("JobInfoScheduler", "Scheduling upload for context %s with jobId=%d in %dms(Backend next call timestamp %d). Attempt %d", ob4Var, Integer.valueOf(c), Long.valueOf(this.c.g(ob4Var.d(), l0, i)), Long.valueOf(l0), Integer.valueOf(i));
        jobScheduler.schedule(c2.build());
    }

    public int c(ob4 ob4Var) {
        Adler32 adler32 = new Adler32();
        adler32.update(this.a.getPackageName().getBytes(Charset.forName("UTF-8")));
        adler32.update(ob4Var.b().getBytes(Charset.forName("UTF-8")));
        adler32.update(ByteBuffer.allocate(4).putInt(wu2.a(ob4Var.d())).array());
        if (ob4Var.c() != null) {
            adler32.update(ob4Var.c());
        }
        return (int) adler32.getValue();
    }

    public final boolean d(JobScheduler jobScheduler, int i, int i2) {
        for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
            int i3 = jobInfo.getExtras().getInt("attemptNumber");
            if (jobInfo.getId() == i) {
                return i3 >= i2;
            }
        }
        return false;
    }
}
