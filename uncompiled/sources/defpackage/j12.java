package defpackage;

/* compiled from: LockFreeLinkedList.kt */
/* renamed from: j12  reason: default package */
/* loaded from: classes2.dex */
public class j12 extends l12 {
    @Override // defpackage.l12
    public boolean s() {
        return false;
    }

    @Override // defpackage.l12
    public final boolean t() {
        throw new IllegalStateException("head cannot be removed".toString());
    }

    public final boolean y() {
        return n() == this;
    }
}
