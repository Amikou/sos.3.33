package defpackage;

import android.content.Context;
import java.util.Set;

/* compiled from: PipelineDraweeControllerBuilderSupplier.java */
/* renamed from: yq2  reason: default package */
/* loaded from: classes.dex */
public class yq2 implements fw3<xq2> {
    public final Context a;
    public final qo1 b;
    public final zq2 c;
    public final Set<m80> d;
    public final Set<l80> e;
    public final ko1 f;

    public yq2(Context context, hr0 hr0Var) {
        this(context, uo1.l(), hr0Var);
    }

    @Override // defpackage.fw3
    /* renamed from: a */
    public xq2 get() {
        return new xq2(this.a, this.c, this.b, this.d, this.e).J(this.f);
    }

    public yq2(Context context, uo1 uo1Var, hr0 hr0Var) {
        this(context, uo1Var, null, null, hr0Var);
    }

    public yq2(Context context, uo1 uo1Var, Set<m80> set, Set<l80> set2, hr0 hr0Var) {
        this.a = context;
        qo1 j = uo1Var.j();
        this.b = j;
        zq2 zq2Var = new zq2();
        this.c = zq2Var;
        zq2Var.a(context.getResources(), tl0.b(), uo1Var.b(context), me4.g(), j.c(), null, null);
        this.d = set;
        this.e = set2;
        this.f = null;
    }
}
