package defpackage;

import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: r05  reason: default package */
/* loaded from: classes.dex */
public final class r05 {
    public static final ExecutorService a = e25.a().a(2, new dj2("GAC_Executor"), s25.b);

    public static ExecutorService a() {
        return a;
    }
}
