package defpackage;

import androidx.media3.common.j;
import androidx.media3.datasource.b;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: SingleSampleMediaChunk.java */
/* renamed from: wp3  reason: default package */
/* loaded from: classes.dex */
public final class wp3 extends ln {
    public final int o;
    public final j p;
    public long q;
    public boolean r;

    public wp3(b bVar, je0 je0Var, j jVar, int i, Object obj, long j, long j2, long j3, int i2, j jVar2) {
        super(bVar, je0Var, jVar, i, obj, j, j2, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, j3);
        this.o = i2;
        this.p = jVar2;
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public void a() throws IOException {
        nn i = i();
        i.b(0L);
        f84 f = i.f(0, this.o);
        f.f(this.p);
        try {
            long b = this.i.b(this.b.e(this.q));
            if (b != -1) {
                b += this.q;
            }
            hj0 hj0Var = new hj0(this.i, this.q, b);
            for (int i2 = 0; i2 != -1; i2 = f.d(hj0Var, Integer.MAX_VALUE, true)) {
                this.q += i2;
            }
            f.b(this.g, 1, (int) this.q, 0, null);
            he0.a(this.i);
            this.r = true;
        } catch (Throwable th) {
            he0.a(this.i);
            throw th;
        }
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public void c() {
    }

    @Override // defpackage.s52
    public boolean g() {
        return this.r;
    }
}
