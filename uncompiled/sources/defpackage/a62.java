package defpackage;

import android.media.MediaFormat;
import androidx.media3.common.f;
import java.nio.ByteBuffer;
import java.util.List;

/* compiled from: MediaFormatUtil.java */
/* renamed from: a62  reason: default package */
/* loaded from: classes.dex */
public final class a62 {
    public static void a(MediaFormat mediaFormat, String str, byte[] bArr) {
        if (bArr != null) {
            mediaFormat.setByteBuffer(str, ByteBuffer.wrap(bArr));
        }
    }

    public static void b(MediaFormat mediaFormat, f fVar) {
        if (fVar != null) {
            d(mediaFormat, "color-transfer", fVar.g0);
            d(mediaFormat, "color-standard", fVar.a);
            d(mediaFormat, "color-range", fVar.f0);
            a(mediaFormat, "hdr-static-info", fVar.h0);
        }
    }

    public static void c(MediaFormat mediaFormat, String str, float f) {
        if (f != -1.0f) {
            mediaFormat.setFloat(str, f);
        }
    }

    public static void d(MediaFormat mediaFormat, String str, int i) {
        if (i != -1) {
            mediaFormat.setInteger(str, i);
        }
    }

    public static void e(MediaFormat mediaFormat, List<byte[]> list) {
        for (int i = 0; i < list.size(); i++) {
            mediaFormat.setByteBuffer("csd-" + i, ByteBuffer.wrap(list.get(i)));
        }
    }
}
