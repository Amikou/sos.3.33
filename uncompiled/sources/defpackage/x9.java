package defpackage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.annotation.b;
import com.fasterxml.jackson.databind.c;
import java.io.IOException;
import java.util.List;

/* compiled from: AdminPeers.java */
/* renamed from: x9  reason: default package */
/* loaded from: classes3.dex */
public class x9 extends i83<List<Object>> {

    /* compiled from: AdminPeers.java */
    /* renamed from: x9$a */
    /* loaded from: classes3.dex */
    public static class a extends c<List<Object>> {
        private ObjectReader objectReader = ml2.getObjectReader();

        /* compiled from: AdminPeers.java */
        /* renamed from: x9$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0289a extends ud4<List<Object>> {
            public C0289a() {
            }
        }

        @Override // com.fasterxml.jackson.databind.c
        public List<Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            if (jsonParser.u() != JsonToken.VALUE_NULL) {
                return (List) this.objectReader.readValue(jsonParser, new C0289a());
            }
            return null;
        }
    }

    @Override // defpackage.i83
    @JsonIgnoreProperties(ignoreUnknown = true)
    @b(using = a.class)
    public void setResult(List<Object> list) {
        super.setResult((x9) list);
    }
}
