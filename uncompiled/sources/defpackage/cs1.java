package defpackage;

import android.content.Context;
import defpackage.gp0;
import java.io.File;

/* compiled from: InternalCacheDiskCacheFactory.java */
/* renamed from: cs1  reason: default package */
/* loaded from: classes.dex */
public final class cs1 extends gp0 {

    /* compiled from: InternalCacheDiskCacheFactory.java */
    /* renamed from: cs1$a */
    /* loaded from: classes.dex */
    public class a implements gp0.a {
        public final /* synthetic */ Context a;
        public final /* synthetic */ String b;

        public a(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        @Override // defpackage.gp0.a
        public File a() {
            File cacheDir = this.a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            return this.b != null ? new File(cacheDir, this.b) : cacheDir;
        }
    }

    public cs1(Context context) {
        this(context, "image_manager_disk_cache", 262144000L);
    }

    public cs1(Context context, String str, long j) {
        super(new a(context, str), j);
    }
}
