package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.RecoverWalletActivity;

/* renamed from: x43  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class x43 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ x43 a = new x43();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        RecoverWalletActivity.U(dialogInterface);
    }
}
