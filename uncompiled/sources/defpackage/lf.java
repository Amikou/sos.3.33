package defpackage;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.text.Selection;
import android.text.Spannable;
import android.view.DragEvent;
import android.view.View;
import android.view.inputmethod.InputContentInfo;
import android.widget.TextView;
import defpackage.f70;
import defpackage.vq1;

/* compiled from: AppCompatReceiveContentHelper.java */
/* renamed from: lf  reason: default package */
/* loaded from: classes.dex */
public final class lf {

    /* compiled from: AppCompatReceiveContentHelper.java */
    /* renamed from: lf$a */
    /* loaded from: classes.dex */
    public class a implements vq1.c {
        public final /* synthetic */ View a;

        public a(View view) {
            this.a = view;
        }

        @Override // defpackage.vq1.c
        public boolean a(wq1 wq1Var, int i, Bundle bundle) {
            if (Build.VERSION.SDK_INT >= 25 && (i & 1) != 0) {
                try {
                    wq1Var.d();
                    InputContentInfo inputContentInfo = (InputContentInfo) wq1Var.e();
                    bundle = bundle == null ? new Bundle() : new Bundle(bundle);
                    bundle.putParcelable("androidx.core.view.extra.INPUT_CONTENT_INFO", inputContentInfo);
                } catch (Exception unused) {
                    return false;
                }
            }
            return ei4.i0(this.a, new f70.a(new ClipData(wq1Var.b(), new ClipData.Item(wq1Var.a())), 2).d(wq1Var.c()).b(bundle).a()) == null;
        }
    }

    /* compiled from: AppCompatReceiveContentHelper.java */
    /* renamed from: lf$b */
    /* loaded from: classes.dex */
    public static final class b {
        public static boolean a(DragEvent dragEvent, TextView textView, Activity activity) {
            activity.requestDragAndDropPermissions(dragEvent);
            int offsetForPosition = textView.getOffsetForPosition(dragEvent.getX(), dragEvent.getY());
            textView.beginBatchEdit();
            try {
                Selection.setSelection((Spannable) textView.getText(), offsetForPosition);
                ei4.i0(textView, new f70.a(dragEvent.getClipData(), 3).a());
                textView.endBatchEdit();
                return true;
            } catch (Throwable th) {
                textView.endBatchEdit();
                throw th;
            }
        }

        public static boolean b(DragEvent dragEvent, View view, Activity activity) {
            activity.requestDragAndDropPermissions(dragEvent);
            ei4.i0(view, new f70.a(dragEvent.getClipData(), 3).a());
            return true;
        }
    }

    public static vq1.c a(View view) {
        return new a(view);
    }

    public static boolean b(View view, DragEvent dragEvent) {
        if (Build.VERSION.SDK_INT >= 24 && dragEvent.getLocalState() == null && ei4.H(view) != null) {
            Activity d = d(view);
            if (d == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Can't handle drop: no activity: view=");
                sb.append(view);
                return false;
            } else if (dragEvent.getAction() == 1) {
                return !(view instanceof TextView);
            } else {
                if (dragEvent.getAction() == 3) {
                    if (view instanceof TextView) {
                        return b.a(dragEvent, (TextView) view, d);
                    }
                    return b.b(dragEvent, view, d);
                }
            }
        }
        return false;
    }

    public static boolean c(TextView textView, int i) {
        if ((i == 16908322 || i == 16908337) && ei4.H(textView) != null) {
            ClipboardManager clipboardManager = (ClipboardManager) textView.getContext().getSystemService("clipboard");
            ClipData primaryClip = clipboardManager == null ? null : clipboardManager.getPrimaryClip();
            if (primaryClip != null && primaryClip.getItemCount() > 0) {
                ei4.i0(textView, new f70.a(primaryClip, 1).c(i != 16908322 ? 1 : 0).a());
            }
            return true;
        }
        return false;
    }

    public static Activity d(View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper) context).getBaseContext()) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
        }
        return null;
    }
}
