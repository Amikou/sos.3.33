package defpackage;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import java.util.NoSuchElementException;

/* compiled from: Element.kt */
/* renamed from: ju0  reason: default package */
/* loaded from: classes.dex */
public final class ju0 {
    static {
        fs1.c(JsonNull.INSTANCE, "JsonNull.INSTANCE");
    }

    public static final void a(JsonObject jsonObject, String str, JsonElement jsonElement) {
        fs1.g(jsonObject, "$receiver");
        fs1.g(str, "property");
        jsonObject.add(str, jsonElement);
    }

    public static final JsonElement b(JsonElement jsonElement, String str) {
        fs1.g(jsonElement, "$receiver");
        fs1.g(str, "key");
        return f(g(jsonElement), str);
    }

    public static final JsonArray c(JsonElement jsonElement) {
        fs1.g(jsonElement, "$receiver");
        JsonArray asJsonArray = jsonElement.getAsJsonArray();
        fs1.c(asJsonArray, "asJsonArray");
        return asJsonArray;
    }

    public static final int d(JsonElement jsonElement) {
        fs1.g(jsonElement, "$receiver");
        return jsonElement.getAsInt();
    }

    public static final long e(JsonElement jsonElement) {
        fs1.g(jsonElement, "$receiver");
        return jsonElement.getAsLong();
    }

    public static final JsonElement f(JsonObject jsonObject, String str) {
        fs1.g(jsonObject, "$receiver");
        fs1.g(str, "key");
        JsonElement jsonElement = jsonObject.get(str);
        if (jsonElement != null) {
            return jsonElement;
        }
        throw new NoSuchElementException("'" + str + "' is not found");
    }

    public static final JsonObject g(JsonElement jsonElement) {
        fs1.g(jsonElement, "$receiver");
        JsonObject asJsonObject = jsonElement.getAsJsonObject();
        fs1.c(asJsonObject, "asJsonObject");
        return asJsonObject;
    }

    public static final String h(JsonElement jsonElement) {
        fs1.g(jsonElement, "$receiver");
        String asString = jsonElement.getAsString();
        fs1.c(asString, "asString");
        return asString;
    }
}
