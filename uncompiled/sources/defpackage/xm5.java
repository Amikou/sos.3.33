package defpackage;

import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: xm5  reason: default package */
/* loaded from: classes.dex */
public final class xm5 implements Runnable {
    public final /* synthetic */ m a;
    public final /* synthetic */ AppMeasurementDynamiteService f0;

    public xm5(AppMeasurementDynamiteService appMeasurementDynamiteService, m mVar) {
        this.f0 = appMeasurementDynamiteService;
        this.a = mVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.a.R().U(this.a);
    }
}
