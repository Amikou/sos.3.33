package defpackage;

import android.net.Uri;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: MediaStoreUtil.java */
/* renamed from: d72  reason: default package */
/* loaded from: classes.dex */
public final class d72 {
    public static boolean a(Uri uri) {
        return b(uri) && !e(uri);
    }

    public static boolean b(Uri uri) {
        return uri != null && PublicResolver.FUNC_CONTENT.equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    public static boolean c(Uri uri) {
        return b(uri) && e(uri);
    }

    public static boolean d(int i, int i2) {
        return i != Integer.MIN_VALUE && i2 != Integer.MIN_VALUE && i <= 512 && i2 <= 384;
    }

    public static boolean e(Uri uri) {
        return uri.getPathSegments().contains("video");
    }
}
