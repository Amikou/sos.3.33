package defpackage;

import com.bumptech.glide.load.b;
import java.io.File;

/* compiled from: FileDecoder.java */
/* renamed from: m31  reason: default package */
/* loaded from: classes.dex */
public class m31 implements b<File, File> {
    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<File> b(File file, int i, int i2, vn2 vn2Var) {
        return new q31(file);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(File file, vn2 vn2Var) {
        return true;
    }
}
