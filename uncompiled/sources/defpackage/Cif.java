package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: AppCompatImageHelper.java */
/* renamed from: if  reason: invalid class name and default package */
/* loaded from: classes.dex */
public class Cif {
    public final ImageView a;
    public k64 b;
    public k64 c;
    public k64 d;

    public Cif(ImageView imageView) {
        this.a = imageView;
    }

    public final boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new k64();
        }
        k64 k64Var = this.d;
        k64Var.a();
        ColorStateList a = cp1.a(this.a);
        if (a != null) {
            k64Var.d = true;
            k64Var.a = a;
        }
        PorterDuff.Mode b = cp1.b(this.a);
        if (b != null) {
            k64Var.c = true;
            k64Var.b = b;
        }
        if (k64Var.d || k64Var.c) {
            gf.i(drawable, k64Var, this.a.getDrawableState());
            return true;
        }
        return false;
    }

    public void b() {
        Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            dr0.b(drawable);
        }
        if (drawable != null) {
            if (j() && a(drawable)) {
                return;
            }
            k64 k64Var = this.c;
            if (k64Var != null) {
                gf.i(drawable, k64Var, this.a.getDrawableState());
                return;
            }
            k64 k64Var2 = this.b;
            if (k64Var2 != null) {
                gf.i(drawable, k64Var2, this.a.getDrawableState());
            }
        }
    }

    public ColorStateList c() {
        k64 k64Var = this.c;
        if (k64Var != null) {
            return k64Var.a;
        }
        return null;
    }

    public PorterDuff.Mode d() {
        k64 k64Var = this.c;
        if (k64Var != null) {
            return k64Var.b;
        }
        return null;
    }

    public boolean e() {
        return Build.VERSION.SDK_INT < 21 || !(this.a.getBackground() instanceof RippleDrawable);
    }

    public void f(AttributeSet attributeSet, int i) {
        int n;
        Context context = this.a.getContext();
        int[] iArr = d33.AppCompatImageView;
        l64 v = l64.v(context, attributeSet, iArr, i, 0);
        ImageView imageView = this.a;
        ei4.r0(imageView, imageView.getContext(), iArr, attributeSet, v.r(), i, 0);
        try {
            Drawable drawable = this.a.getDrawable();
            if (drawable == null && (n = v.n(d33.AppCompatImageView_srcCompat, -1)) != -1 && (drawable = mf.d(this.a.getContext(), n)) != null) {
                this.a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                dr0.b(drawable);
            }
            int i2 = d33.AppCompatImageView_tint;
            if (v.s(i2)) {
                cp1.c(this.a, v.c(i2));
            }
            int i3 = d33.AppCompatImageView_tintMode;
            if (v.s(i3)) {
                cp1.d(this.a, dr0.e(v.k(i3, -1), null));
            }
        } finally {
            v.w();
        }
    }

    public void g(int i) {
        if (i != 0) {
            Drawable d = mf.d(this.a.getContext(), i);
            if (d != null) {
                dr0.b(d);
            }
            this.a.setImageDrawable(d);
        } else {
            this.a.setImageDrawable(null);
        }
        b();
    }

    public void h(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new k64();
        }
        k64 k64Var = this.c;
        k64Var.a = colorStateList;
        k64Var.d = true;
        b();
    }

    public void i(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new k64();
        }
        k64 k64Var = this.c;
        k64Var.b = mode;
        k64Var.c = true;
        b();
    }

    public final boolean j() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.b != null : i == 21;
    }
}
