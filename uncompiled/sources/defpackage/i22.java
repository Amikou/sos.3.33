package defpackage;

/* compiled from: LongSparseArray.java */
/* renamed from: i22  reason: default package */
/* loaded from: classes.dex */
public class i22<E> implements Cloneable {
    public static final Object i0 = new Object();
    public boolean a;
    public long[] f0;
    public Object[] g0;
    public int h0;

    public i22() {
        this(10);
    }

    public void a(long j, E e) {
        int i = this.h0;
        if (i != 0 && j <= this.f0[i - 1]) {
            o(j, e);
            return;
        }
        if (this.a && i >= this.f0.length) {
            f();
        }
        int i2 = this.h0;
        if (i2 >= this.f0.length) {
            int f = c70.f(i2 + 1);
            long[] jArr = new long[f];
            Object[] objArr = new Object[f];
            long[] jArr2 = this.f0;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr2 = this.g0;
            System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
            this.f0 = jArr;
            this.g0 = objArr;
        }
        this.f0[i2] = j;
        this.g0[i2] = e;
        this.h0 = i2 + 1;
    }

    public void b() {
        int i = this.h0;
        Object[] objArr = this.g0;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.h0 = 0;
        this.a = false;
    }

    /* renamed from: d */
    public i22<E> clone() {
        try {
            i22<E> i22Var = (i22) super.clone();
            i22Var.f0 = (long[]) this.f0.clone();
            i22Var.g0 = (Object[]) this.g0.clone();
            return i22Var;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public boolean e(long j) {
        return j(j) >= 0;
    }

    public final void f() {
        int i = this.h0;
        long[] jArr = this.f0;
        Object[] objArr = this.g0;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != i0) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.a = false;
        this.h0 = i2;
    }

    public E g(long j) {
        return h(j, null);
    }

    public E h(long j, E e) {
        int b = c70.b(this.f0, this.h0, j);
        if (b >= 0) {
            Object[] objArr = this.g0;
            if (objArr[b] != i0) {
                return (E) objArr[b];
            }
        }
        return e;
    }

    public int j(long j) {
        if (this.a) {
            f();
        }
        return c70.b(this.f0, this.h0, j);
    }

    public boolean k() {
        return t() == 0;
    }

    public long l(int i) {
        if (this.a) {
            f();
        }
        return this.f0[i];
    }

    public void o(long j, E e) {
        int b = c70.b(this.f0, this.h0, j);
        if (b >= 0) {
            this.g0[b] = e;
            return;
        }
        int i = ~b;
        int i2 = this.h0;
        if (i < i2) {
            Object[] objArr = this.g0;
            if (objArr[i] == i0) {
                this.f0[i] = j;
                objArr[i] = e;
                return;
            }
        }
        if (this.a && i2 >= this.f0.length) {
            f();
            i = ~c70.b(this.f0, this.h0, j);
        }
        int i3 = this.h0;
        if (i3 >= this.f0.length) {
            int f = c70.f(i3 + 1);
            long[] jArr = new long[f];
            Object[] objArr2 = new Object[f];
            long[] jArr2 = this.f0;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.g0;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f0 = jArr;
            this.g0 = objArr2;
        }
        int i4 = this.h0;
        if (i4 - i != 0) {
            long[] jArr3 = this.f0;
            int i5 = i + 1;
            System.arraycopy(jArr3, i, jArr3, i5, i4 - i);
            Object[] objArr4 = this.g0;
            System.arraycopy(objArr4, i, objArr4, i5, this.h0 - i);
        }
        this.f0[i] = j;
        this.g0[i] = e;
        this.h0++;
    }

    public void p(i22<? extends E> i22Var) {
        int t = i22Var.t();
        for (int i = 0; i < t; i++) {
            o(i22Var.l(i), i22Var.u(i));
        }
    }

    public void r(long j) {
        int b = c70.b(this.f0, this.h0, j);
        if (b >= 0) {
            Object[] objArr = this.g0;
            Object obj = objArr[b];
            Object obj2 = i0;
            if (obj != obj2) {
                objArr[b] = obj2;
                this.a = true;
            }
        }
    }

    public void s(int i) {
        Object[] objArr = this.g0;
        Object obj = objArr[i];
        Object obj2 = i0;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.a = true;
        }
    }

    public int t() {
        if (this.a) {
            f();
        }
        return this.h0;
    }

    public String toString() {
        if (t() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.h0 * 28);
        sb.append('{');
        for (int i = 0; i < this.h0; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(l(i));
            sb.append('=');
            E u = u(i);
            if (u != this) {
                sb.append(u);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public E u(int i) {
        if (this.a) {
            f();
        }
        return (E) this.g0[i];
    }

    public i22(int i) {
        this.a = false;
        if (i == 0) {
            this.f0 = c70.b;
            this.g0 = c70.c;
            return;
        }
        int f = c70.f(i);
        this.f0 = new long[f];
        this.g0 = new Object[f];
    }
}
