package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentTokenslistBinding.java */
/* renamed from: hb1  reason: default package */
/* loaded from: classes2.dex */
public final class hb1 {
    public final ConstraintLayout a;
    public final AppBarLayout b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final ry1 e;
    public final RecyclerView f;
    public final SwipeRefreshLayout g;
    public final wf h;
    public final TextView i;
    public final TextView j;
    public final TextView k;

    public hb1(ConstraintLayout constraintLayout, AppBarLayout appBarLayout, MaterialButton materialButton, MaterialButton materialButton2, CollapsingToolbarLayout collapsingToolbarLayout, CoordinatorLayout coordinatorLayout, LinearLayout linearLayout, ry1 ry1Var, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, wf wfVar, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
        this.a = constraintLayout;
        this.b = appBarLayout;
        this.c = materialButton;
        this.d = materialButton2;
        this.e = ry1Var;
        this.f = recyclerView;
        this.g = swipeRefreshLayout;
        this.h = wfVar;
        this.i = textView;
        this.j = textView3;
        this.k = textView4;
    }

    public static hb1 a(View view) {
        int i = R.id.appBar;
        AppBarLayout appBarLayout = (AppBarLayout) ai4.a(view, R.id.appBar);
        if (appBarLayout != null) {
            i = R.id.btnReceive;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnReceive);
            if (materialButton != null) {
                i = R.id.btnSend;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnSend);
                if (materialButton2 != null) {
                    i = R.id.ccToolBar;
                    CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) ai4.a(view, R.id.ccToolBar);
                    if (collapsingToolbarLayout != null) {
                        i = R.id.coordinator;
                        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) ai4.a(view, R.id.coordinator);
                        if (coordinatorLayout != null) {
                            i = R.id.lButtonsContainer;
                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.lButtonsContainer);
                            if (linearLayout != null) {
                                i = R.id.lSelectTokenType;
                                View a = ai4.a(view, R.id.lSelectTokenType);
                                if (a != null) {
                                    ry1 a2 = ry1.a(a);
                                    i = R.id.rvMyTokenList;
                                    RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvMyTokenList);
                                    if (recyclerView != null) {
                                        i = R.id.swipeRefreshLayout;
                                        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swipeRefreshLayout);
                                        if (swipeRefreshLayout != null) {
                                            i = R.id.topBar;
                                            View a3 = ai4.a(view, R.id.topBar);
                                            if (a3 != null) {
                                                wf a4 = wf.a(a3);
                                                i = R.id.tvMainWallet;
                                                TextView textView = (TextView) ai4.a(view, R.id.tvMainWallet);
                                                if (textView != null) {
                                                    i = R.id.tvTitle;
                                                    TextView textView2 = (TextView) ai4.a(view, R.id.tvTitle);
                                                    if (textView2 != null) {
                                                        i = R.id.tvWalletBlnc;
                                                        TextView textView3 = (TextView) ai4.a(view, R.id.tvWalletBlnc);
                                                        if (textView3 != null) {
                                                            i = R.id.txtSymbol;
                                                            TextView textView4 = (TextView) ai4.a(view, R.id.txtSymbol);
                                                            if (textView4 != null) {
                                                                return new hb1((ConstraintLayout) view, appBarLayout, materialButton, materialButton2, collapsingToolbarLayout, coordinatorLayout, linearLayout, a2, recyclerView, swipeRefreshLayout, a4, textView, textView2, textView3, textView4);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
