package defpackage;

/* compiled from: SlidingWindow.kt */
/* renamed from: uq3  reason: default package */
/* loaded from: classes2.dex */
public final class uq3 {
    public static final void a(int i, int i2) {
        String str;
        if (i > 0 && i2 > 0) {
            return;
        }
        if (i != i2) {
            str = "Both size " + i + " and step " + i2 + " must be greater than zero.";
        } else {
            str = "size " + i + " must be greater than zero.";
        }
        throw new IllegalArgumentException(str.toString());
    }
}
