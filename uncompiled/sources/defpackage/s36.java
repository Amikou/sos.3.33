package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: s36  reason: default package */
/* loaded from: classes.dex */
public final class s36 implements r36 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.scheduler.task_thread.cleanup_on_exit", false);

    @Override // defpackage.r36
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
