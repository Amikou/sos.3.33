package defpackage;

/* renamed from: kx1  reason: default package */
/* loaded from: classes2.dex */
public class kx1 implements ty {
    public byte[] a;

    public kx1(byte[] bArr) {
        this(bArr, 0, bArr.length);
    }

    public kx1(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        this.a = bArr2;
        System.arraycopy(bArr, i, bArr2, 0, i2);
    }

    public byte[] a() {
        return this.a;
    }
}
