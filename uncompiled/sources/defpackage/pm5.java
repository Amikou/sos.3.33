package defpackage;

import com.google.android.gms.tasks.c;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: pm5  reason: default package */
/* loaded from: classes.dex */
public final class pm5<TResult> implements l46<TResult> {
    public final Executor a;
    public final Object b = new Object();
    public hm2 c;

    public pm5(Executor executor, hm2 hm2Var) {
        this.a = executor;
        this.c = hm2Var;
    }

    @Override // defpackage.l46
    public final void c(c<TResult> cVar) {
        if (cVar.n()) {
            synchronized (this.b) {
                if (this.c == null) {
                    return;
                }
                this.a.execute(new ok5(this));
            }
        }
    }
}
