package defpackage;

import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;

/* compiled from: TooltipCompatHandler.java */
/* renamed from: k74  reason: default package */
/* loaded from: classes.dex */
public class k74 implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {
    public static k74 n0;
    public static k74 o0;
    public final View a;
    public final CharSequence f0;
    public final int g0;
    public final Runnable h0 = new a();
    public final Runnable i0 = new b();
    public int j0;
    public int k0;
    public m74 l0;
    public boolean m0;

    /* compiled from: TooltipCompatHandler.java */
    /* renamed from: k74$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            k74.this.g(false);
        }
    }

    /* compiled from: TooltipCompatHandler.java */
    /* renamed from: k74$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            k74.this.c();
        }
    }

    public k74(View view, CharSequence charSequence) {
        this.a = view;
        this.f0 = charSequence;
        this.g0 = gi4.c(ViewConfiguration.get(view.getContext()));
        b();
        view.setOnLongClickListener(this);
        view.setOnHoverListener(this);
    }

    public static void e(k74 k74Var) {
        k74 k74Var2 = n0;
        if (k74Var2 != null) {
            k74Var2.a();
        }
        n0 = k74Var;
        if (k74Var != null) {
            k74Var.d();
        }
    }

    public static void f(View view, CharSequence charSequence) {
        k74 k74Var = n0;
        if (k74Var != null && k74Var.a == view) {
            e(null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            k74 k74Var2 = o0;
            if (k74Var2 != null && k74Var2.a == view) {
                k74Var2.c();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new k74(view, charSequence);
    }

    public final void a() {
        this.a.removeCallbacks(this.h0);
    }

    public final void b() {
        this.j0 = Integer.MAX_VALUE;
        this.k0 = Integer.MAX_VALUE;
    }

    public void c() {
        if (o0 == this) {
            o0 = null;
            m74 m74Var = this.l0;
            if (m74Var != null) {
                m74Var.c();
                this.l0 = null;
                b();
                this.a.removeOnAttachStateChangeListener(this);
            }
        }
        if (n0 == this) {
            e(null);
        }
        this.a.removeCallbacks(this.i0);
    }

    public final void d() {
        this.a.postDelayed(this.h0, ViewConfiguration.getLongPressTimeout());
    }

    public void g(boolean z) {
        long j;
        int longPressTimeout;
        long j2;
        if (ei4.V(this.a)) {
            e(null);
            k74 k74Var = o0;
            if (k74Var != null) {
                k74Var.c();
            }
            o0 = this;
            this.m0 = z;
            m74 m74Var = new m74(this.a.getContext());
            this.l0 = m74Var;
            m74Var.e(this.a, this.j0, this.k0, this.m0, this.f0);
            this.a.addOnAttachStateChangeListener(this);
            if (this.m0) {
                j2 = 2500;
            } else {
                if ((ei4.P(this.a) & 1) == 1) {
                    j = 3000;
                    longPressTimeout = ViewConfiguration.getLongPressTimeout();
                } else {
                    j = u84.DEFAULT_POLLING_FREQUENCY;
                    longPressTimeout = ViewConfiguration.getLongPressTimeout();
                }
                j2 = j - longPressTimeout;
            }
            this.a.removeCallbacks(this.i0);
            this.a.postDelayed(this.i0, j2);
        }
    }

    public final boolean h(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.j0) > this.g0 || Math.abs(y - this.k0) > this.g0) {
            this.j0 = x;
            this.k0 = y;
            return true;
        }
        return false;
    }

    @Override // android.view.View.OnHoverListener
    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.l0 == null || !this.m0) {
            AccessibilityManager accessibilityManager = (AccessibilityManager) this.a.getContext().getSystemService("accessibility");
            if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
                return false;
            }
            int action = motionEvent.getAction();
            if (action != 7) {
                if (action == 10) {
                    b();
                    c();
                }
            } else if (this.a.isEnabled() && this.l0 == null && h(motionEvent)) {
                e(this);
            }
            return false;
        }
        return false;
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        this.j0 = view.getWidth() / 2;
        this.k0 = view.getHeight() / 2;
        g(true);
        return true;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        c();
    }
}
