package defpackage;

import defpackage.lp;
import defpackage.o61;
import java.io.IOException;
import java.util.Objects;

/* compiled from: FlacBinarySearchSeeker.java */
/* renamed from: l61  reason: default package */
/* loaded from: classes.dex */
public final class l61 extends lp {

    /* compiled from: FlacBinarySearchSeeker.java */
    /* renamed from: l61$b */
    /* loaded from: classes.dex */
    public static final class b implements lp.f {
        public final s61 a;
        public final int b;
        public final o61.a c;

        @Override // defpackage.lp.f
        public lp.e a(q11 q11Var, long j) throws IOException {
            long position = q11Var.getPosition();
            long c = c(q11Var);
            long e = q11Var.e();
            q11Var.f(Math.max(6, this.a.c));
            long c2 = c(q11Var);
            long e2 = q11Var.e();
            if (c > j || c2 <= j) {
                if (c2 <= j) {
                    return lp.e.f(c2, e2);
                }
                return lp.e.d(c, position);
            }
            return lp.e.e(e);
        }

        @Override // defpackage.lp.f
        public /* synthetic */ void b() {
            mp.a(this);
        }

        public final long c(q11 q11Var) throws IOException {
            while (q11Var.e() < q11Var.getLength() - 6 && !o61.h(q11Var, this.a, this.b, this.c)) {
                q11Var.f(1);
            }
            if (q11Var.e() >= q11Var.getLength() - 6) {
                q11Var.f((int) (q11Var.getLength() - q11Var.e()));
                return this.a.j;
            }
            return this.c.a;
        }

        public b(s61 s61Var, int i) {
            this.a = s61Var;
            this.b = i;
            this.c = new o61.a();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l61(final s61 s61Var, int i, long j, long j2) {
        super(new lp.d() { // from class: k61
            @Override // defpackage.lp.d
            public final long a(long j3) {
                return s61.this.i(j3);
            }
        }, new b(s61Var, i), s61Var.f(), 0L, s61Var.j, j, j2, s61Var.d(), Math.max(6, s61Var.c));
        Objects.requireNonNull(s61Var);
    }
}
