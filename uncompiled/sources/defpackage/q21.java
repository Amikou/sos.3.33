package defpackage;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: q21  reason: default package */
/* loaded from: classes2.dex */
public class q21 {
    public static Executor a(String str) {
        return new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new yc2("Firebase-Messaging-Trigger-Topics-Io"));
    }

    public static ScheduledExecutorService b() {
        return new ScheduledThreadPoolExecutor(1, new yc2("Firebase-Messaging-Init"));
    }

    public static ExecutorService c() {
        qp5.a();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new yc2("Firebase-Messaging-Intent-Handle"));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return Executors.unconfigurableExecutorService(threadPoolExecutor);
    }

    public static ExecutorService d() {
        return Executors.newSingleThreadExecutor(new yc2("Firebase-Messaging-Network-Io"));
    }

    public static ExecutorService e() {
        return Executors.newSingleThreadExecutor(new yc2("Firebase-Messaging-Task"));
    }

    public static ScheduledExecutorService f() {
        return new ScheduledThreadPoolExecutor(1, new yc2("Firebase-Messaging-Topics-Io"));
    }

    public static Executor g() {
        return a("Firebase-Messaging-Trigger-Topics-Io");
    }
}
