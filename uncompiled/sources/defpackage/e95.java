package defpackage;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.internal.measurement.d;
import com.google.android.gms.internal.measurement.e;
import com.google.android.gms.internal.measurement.f;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: e95  reason: default package */
/* loaded from: classes.dex */
public abstract class e95 extends d implements f {
    public static f F1(IBinder iBinder) {
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
        if (queryLocalInterface instanceof f) {
            return (f) queryLocalInterface;
        }
        return new e(iBinder);
    }
}
