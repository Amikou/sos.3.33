package defpackage;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Pair;
import kotlin.collections.EmptyMap;

/* compiled from: Maps.kt */
/* renamed from: z32  reason: default package */
/* loaded from: classes2.dex */
public class z32 extends y32 {
    public static final <K, V> Map<K, V> d() {
        EmptyMap emptyMap = EmptyMap.INSTANCE;
        Objects.requireNonNull(emptyMap, "null cannot be cast to non-null type kotlin.collections.Map<K, V>");
        return emptyMap;
    }

    public static final <K, V> Map<K, V> e(Pair<? extends K, ? extends V>... pairArr) {
        fs1.f(pairArr, "pairs");
        return pairArr.length > 0 ? l(pairArr, new LinkedHashMap(y32.a(pairArr.length))) : d();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <K, V> Map<K, V> f(Map<K, ? extends V> map) {
        fs1.f(map, "$this$optimizeReadOnlyMap");
        int size = map.size();
        if (size != 0) {
            return size != 1 ? map : y32.c(map);
        }
        return d();
    }

    public static final <K, V> void g(Map<? super K, ? super V> map, Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        fs1.f(map, "$this$putAll");
        fs1.f(iterable, "pairs");
        for (Pair<? extends K, ? extends V> pair : iterable) {
            map.put((K) pair.component1(), (V) pair.component2());
        }
    }

    public static final <K, V> void h(Map<? super K, ? super V> map, Pair<? extends K, ? extends V>[] pairArr) {
        fs1.f(map, "$this$putAll");
        fs1.f(pairArr, "pairs");
        for (Pair<? extends K, ? extends V> pair : pairArr) {
            map.put((K) pair.component1(), (V) pair.component2());
        }
    }

    public static final <K, V> Map<K, V> i(Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        fs1.f(iterable, "$this$toMap");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size != 0) {
                if (size != 1) {
                    return j(iterable, new LinkedHashMap(y32.a(collection.size())));
                }
                return y32.b(iterable instanceof List ? (Pair<? extends K, ? extends V>) ((List) iterable).get(0) : iterable.iterator().next());
            }
            return d();
        }
        return f(j(iterable, new LinkedHashMap()));
    }

    public static final <K, V, M extends Map<? super K, ? super V>> M j(Iterable<? extends Pair<? extends K, ? extends V>> iterable, M m) {
        fs1.f(iterable, "$this$toMap");
        fs1.f(m, "destination");
        g(m, iterable);
        return m;
    }

    public static final <K, V> Map<K, V> k(Map<? extends K, ? extends V> map) {
        fs1.f(map, "$this$toMap");
        int size = map.size();
        if (size != 0) {
            if (size != 1) {
                return m(map);
            }
            return y32.c(map);
        }
        return d();
    }

    public static final <K, V, M extends Map<? super K, ? super V>> M l(Pair<? extends K, ? extends V>[] pairArr, M m) {
        fs1.f(pairArr, "$this$toMap");
        fs1.f(m, "destination");
        h(m, pairArr);
        return m;
    }

    public static final <K, V> Map<K, V> m(Map<? extends K, ? extends V> map) {
        fs1.f(map, "$this$toMutableMap");
        return new LinkedHashMap(map);
    }
}
