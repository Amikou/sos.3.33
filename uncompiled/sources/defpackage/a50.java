package defpackage;

import android.content.Intent;
import android.os.Bundle;
import java.util.List;
import java.util.Map;
import zendesk.configurations.Configuration;

/* compiled from: ConfigurationUtil.java */
@Deprecated
/* renamed from: a50  reason: default package */
/* loaded from: classes3.dex */
public class a50 {
    public static z40 a = new z40();

    public static List<Configuration> a(List<Configuration> list, Configuration configuration) {
        return a.a(list, configuration);
    }

    public static void b(Bundle bundle, Configuration configuration) {
        a.b(bundle, configuration);
    }

    public static void c(Intent intent, Configuration configuration) {
        a.c(intent, configuration);
    }

    public static <E extends Configuration> E d(List<Configuration> list, Class<E> cls) {
        return (E) a.e(list, cls);
    }

    public static <E extends Configuration> E e(Bundle bundle, Class<E> cls) {
        return (E) a.f(bundle, cls);
    }

    public static <E extends Configuration> E f(Map<String, Object> map, Class<E> cls) {
        return (E) a.g(map, cls);
    }
}
