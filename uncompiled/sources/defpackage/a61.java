package defpackage;

import android.app.Activity;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;

/* compiled from: FirstFrameAndAfterTrimMemoryWaiter.java */
/* renamed from: a61  reason: default package */
/* loaded from: classes.dex */
public final class a61 implements bc1, ComponentCallbacks2 {
    @Override // defpackage.bc1
    public void a(Activity activity) {
    }

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
        onTrimMemory(20);
    }

    @Override // android.content.ComponentCallbacks2
    public void onTrimMemory(int i) {
    }
}
