package defpackage;

import android.content.Context;
import android.content.Intent;
import defpackage.s7;

/* compiled from: ActivityResultContracts.java */
/* renamed from: u7  reason: default package */
/* loaded from: classes.dex */
public final class u7 extends s7<String, Boolean> {
    @Override // defpackage.s7
    /* renamed from: d */
    public Intent a(Context context, String str) {
        return t7.e(new String[]{str});
    }

    @Override // defpackage.s7
    /* renamed from: e */
    public s7.a<Boolean> b(Context context, String str) {
        if (str == null) {
            return new s7.a<>(Boolean.FALSE);
        }
        if (m70.a(context, str) == 0) {
            return new s7.a<>(Boolean.TRUE);
        }
        return null;
    }

    @Override // defpackage.s7
    /* renamed from: f */
    public Boolean c(int i, Intent intent) {
        if (intent != null && i == -1) {
            int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
            if (intArrayExtra == null || intArrayExtra.length == 0) {
                return Boolean.FALSE;
            }
            return Boolean.valueOf(intArrayExtra[0] == 0);
        }
        return Boolean.FALSE;
    }
}
