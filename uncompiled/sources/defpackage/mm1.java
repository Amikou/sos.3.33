package defpackage;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.tl1;

/* compiled from: IPostMessageService.java */
/* renamed from: mm1  reason: default package */
/* loaded from: classes.dex */
public interface mm1 extends IInterface {

    /* compiled from: IPostMessageService.java */
    /* renamed from: mm1$a */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements mm1 {
        public a() {
            attachInterface(this, "android.support.customtabs.IPostMessageService");
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 2) {
                parcel.enforceInterface("android.support.customtabs.IPostMessageService");
                t1(tl1.a.b(parcel.readStrongBinder()), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                parcel2.writeNoException();
                return true;
            } else if (i != 3) {
                if (i != 1598968902) {
                    return super.onTransact(i, parcel, parcel2, i2);
                }
                parcel2.writeString("android.support.customtabs.IPostMessageService");
                return true;
            } else {
                parcel.enforceInterface("android.support.customtabs.IPostMessageService");
                V0(tl1.a.b(parcel.readStrongBinder()), parcel.readString(), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                parcel2.writeNoException();
                return true;
            }
        }
    }

    void V0(tl1 tl1Var, String str, Bundle bundle) throws RemoteException;

    void t1(tl1 tl1Var, Bundle bundle) throws RemoteException;
}
