package defpackage;

import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: GhostViewPlatform.java */
/* renamed from: pf1  reason: default package */
/* loaded from: classes.dex */
public class pf1 implements nf1 {
    public static Class<?> f0;
    public static boolean g0;
    public static Method h0;
    public static boolean i0;
    public static Method j0;
    public static boolean k0;
    public final View a;

    public pf1(View view) {
        this.a = view;
    }

    public static nf1 b(View view, ViewGroup viewGroup, Matrix matrix) {
        c();
        Method method = h0;
        if (method != null) {
            try {
                return new pf1((View) method.invoke(null, view, viewGroup, matrix));
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e.getCause());
            }
        }
        return null;
    }

    public static void c() {
        if (i0) {
            return;
        }
        try {
            d();
            Method declaredMethod = f0.getDeclaredMethod("addGhost", View.class, ViewGroup.class, Matrix.class);
            h0 = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (NoSuchMethodException unused) {
        }
        i0 = true;
    }

    public static void d() {
        if (g0) {
            return;
        }
        try {
            f0 = Class.forName("android.view.GhostView");
        } catch (ClassNotFoundException unused) {
        }
        g0 = true;
    }

    public static void e() {
        if (k0) {
            return;
        }
        try {
            d();
            Method declaredMethod = f0.getDeclaredMethod("removeGhost", View.class);
            j0 = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (NoSuchMethodException unused) {
        }
        k0 = true;
    }

    public static void f(View view) {
        e();
        Method method = j0;
        if (method != null) {
            try {
                method.invoke(null, view);
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e.getCause());
            }
        }
    }

    @Override // defpackage.nf1
    public void a(ViewGroup viewGroup, View view) {
    }

    @Override // defpackage.nf1
    public void setVisibility(int i) {
        this.a.setVisibility(i);
    }
}
