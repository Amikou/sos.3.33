package defpackage;

/* compiled from: UtcTimingElement.java */
/* renamed from: ig4  reason: default package */
/* loaded from: classes.dex */
public final class ig4 {
    public final String a;
    public final String b;

    public ig4(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String toString() {
        return this.a + ", " + this.b;
    }
}
