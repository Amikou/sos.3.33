package defpackage;

import android.os.Bundle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* compiled from: BlockingAnalyticsEventLogger.java */
/* renamed from: xq  reason: default package */
/* loaded from: classes2.dex */
public class xq implements rb, qb {
    public final p90 a;
    public final int b;
    public final TimeUnit c;
    public final Object d = new Object();
    public CountDownLatch e;

    public xq(p90 p90Var, int i, TimeUnit timeUnit) {
        this.a = p90Var;
        this.b = i;
        this.c = timeUnit;
    }

    @Override // defpackage.qb
    public void a(String str, Bundle bundle) {
        synchronized (this.d) {
            w12 f = w12.f();
            f.i("Logging event " + str + " to Firebase Analytics with params " + bundle);
            this.e = new CountDownLatch(1);
            this.a.a(str, bundle);
            w12.f().i("Awaiting app exception callback from Analytics...");
            try {
                if (this.e.await(this.b, this.c)) {
                    w12.f().i("App exception callback received from Analytics listener.");
                } else {
                    w12.f().k("Timeout exceeded while awaiting app exception callback from Analytics listener.");
                }
            } catch (InterruptedException unused) {
                w12.f().d("Interrupted while awaiting app exception callback from Analytics listener.");
            }
            this.e = null;
        }
    }

    @Override // defpackage.rb
    public void d(String str, Bundle bundle) {
        CountDownLatch countDownLatch = this.e;
        if (countDownLatch != null && "_ae".equals(str)) {
            countDownLatch.countDown();
        }
    }
}
