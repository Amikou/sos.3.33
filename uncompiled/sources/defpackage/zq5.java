package defpackage;

import com.google.android.gms.internal.measurement.o1;
import com.google.android.gms.internal.measurement.t1;
import com.google.android.gms.internal.measurement.z1;
import com.google.android.gms.internal.measurement.zzjb;
import com.google.android.gms.internal.measurement.zzjd;
import defpackage.zq5;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: zq5  reason: default package */
/* loaded from: classes.dex */
public abstract class zq5<MessageType extends zq5<MessageType, BuilderType>, BuilderType extends o1<MessageType, BuilderType>> implements z1 {
    public int zzb = 0;

    /* JADX WARN: Multi-variable type inference failed */
    public static <T> void j(Iterable<T> iterable, List<? super T> list) {
        cw5.a(iterable);
        if (iterable instanceof nw5) {
            List<?> f = ((nw5) iterable).f();
            nw5 nw5Var = (nw5) list;
            int size = list.size();
            for (Object obj : f) {
                if (obj == null) {
                    int size2 = nw5Var.size();
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(size2 - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    int size3 = nw5Var.size();
                    while (true) {
                        size3--;
                        if (size3 < size) {
                            break;
                        }
                        nw5Var.remove(size3);
                    }
                    throw new NullPointerException(sb2);
                } else if (obj instanceof zzjd) {
                    nw5Var.M((zzjd) obj);
                } else {
                    nw5Var.add((String) obj);
                }
            }
        } else if (!(iterable instanceof hy5)) {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + iterable.size());
            }
            int size4 = list.size();
            for (T t : iterable) {
                if (t == 0) {
                    int size5 = list.size();
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(size5 - size4);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    int size6 = list.size();
                    while (true) {
                        size6--;
                        if (size6 < size4) {
                            break;
                        }
                        list.remove(size6);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(t);
            }
        } else {
            list.addAll(iterable);
        }
    }

    public final byte[] c() {
        try {
            byte[] bArr = new byte[e()];
            t1 x = t1.x(bArr);
            f(x);
            x.c();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(name.length() + 72);
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a byte array threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @Override // com.google.android.gms.internal.measurement.z1
    public final zzjd d() {
        try {
            int e = e();
            zzjd zzjdVar = zzjd.zzb;
            byte[] bArr = new byte[e];
            t1 x = t1.x(bArr);
            f(x);
            x.c();
            return new zzjb(bArr);
        } catch (IOException e2) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(name.length() + 72);
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ByteString threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e2);
        }
    }

    public int h() {
        throw null;
    }

    public void i(int i) {
        throw null;
    }
}
