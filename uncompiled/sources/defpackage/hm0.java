package defpackage;

/* compiled from: Dependency.java */
/* renamed from: hm0  reason: default package */
/* loaded from: classes2.dex */
public final class hm0 {
    public final Class<?> a;
    public final int b;
    public final int c;

    public hm0(Class<?> cls, int i, int i2) {
        this.a = (Class) bu2.c(cls, "Null dependency anInterface.");
        this.b = i;
        this.c = i2;
    }

    public static hm0 a(Class<?> cls) {
        return new hm0(cls, 0, 2);
    }

    public static String b(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    return "deferred";
                }
                throw new AssertionError("Unsupported injection: " + i);
            }
            return "provider";
        }
        return "direct";
    }

    @Deprecated
    public static hm0 h(Class<?> cls) {
        return new hm0(cls, 0, 0);
    }

    public static hm0 i(Class<?> cls) {
        return new hm0(cls, 0, 1);
    }

    public static hm0 j(Class<?> cls) {
        return new hm0(cls, 1, 0);
    }

    public static hm0 k(Class<?> cls) {
        return new hm0(cls, 2, 0);
    }

    public Class<?> c() {
        return this.a;
    }

    public boolean d() {
        return this.c == 2;
    }

    public boolean e() {
        return this.c == 0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof hm0) {
            hm0 hm0Var = (hm0) obj;
            return this.a == hm0Var.a && this.b == hm0Var.b && this.c == hm0Var.c;
        }
        return false;
    }

    public boolean f() {
        return this.b == 1;
    }

    public boolean g() {
        return this.b == 2;
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", type=");
        int i = this.b;
        sb.append(i == 1 ? "required" : i == 0 ? "optional" : "set");
        sb.append(", injection=");
        sb.append(b(this.c));
        sb.append("}");
        return sb.toString();
    }
}
