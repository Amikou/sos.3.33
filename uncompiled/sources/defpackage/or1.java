package defpackage;

import java.util.Iterator;

/* compiled from: Iterators.kt */
/* renamed from: or1  reason: default package */
/* loaded from: classes2.dex */
public abstract class or1 implements Iterator<Integer>, tw1 {
    @Override // java.util.Iterator
    /* renamed from: a */
    public final Integer next() {
        return Integer.valueOf(b());
    }

    public abstract int b();

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
