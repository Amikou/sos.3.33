package defpackage;

import com.facebook.common.references.a;

/* compiled from: CloseableProducerToDataSourceAdapter.java */
/* renamed from: zz  reason: default package */
/* loaded from: classes.dex */
public class zz<T> extends g5<a<T>> {
    public zz(dv2<a<T>> dv2Var, xm3 xm3Var, i73 i73Var) {
        super(dv2Var, xm3Var, i73Var);
    }

    public static <T> ge0<a<T>> I(dv2<a<T>> dv2Var, xm3 xm3Var, i73 i73Var) {
        if (nc1.d()) {
            nc1.a("CloseableProducerToDataSourceAdapter#create");
        }
        zz zzVar = new zz(dv2Var, xm3Var, i73Var);
        if (nc1.d()) {
            nc1.b();
        }
        return zzVar;
    }

    @Override // com.facebook.datasource.AbstractDataSource
    /* renamed from: H */
    public void h(a<T> aVar) {
        a.g(aVar);
    }

    @Override // com.facebook.datasource.AbstractDataSource, defpackage.ge0
    /* renamed from: J */
    public a<T> g() {
        return a.e((a) super.g());
    }

    @Override // defpackage.g5
    /* renamed from: K */
    public void F(a<T> aVar, int i, ev2 ev2Var) {
        super.F(a.e(aVar), i, ev2Var);
    }
}
