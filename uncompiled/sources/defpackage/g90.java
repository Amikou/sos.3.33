package defpackage;

import defpackage.i90;
import defpackage.l72;

/* compiled from: CountingLruBitmapMemoryCacheFactory.java */
/* renamed from: g90  reason: default package */
/* loaded from: classes.dex */
public class g90 implements cq {

    /* compiled from: CountingLruBitmapMemoryCacheFactory.java */
    /* renamed from: g90$a */
    /* loaded from: classes.dex */
    public class a implements yg4<com.facebook.imagepipeline.image.a> {
        public a(g90 g90Var) {
        }

        @Override // defpackage.yg4
        /* renamed from: b */
        public int a(com.facebook.imagepipeline.image.a aVar) {
            return aVar.b();
        }
    }

    @Override // defpackage.cq
    public i90<wt, com.facebook.imagepipeline.image.a> a(fw3<m72> fw3Var, r72 r72Var, l72.a aVar, boolean z, boolean z2, i90.b<wt> bVar) {
        u22 u22Var = new u22(new a(this), aVar, fw3Var, bVar, z, z2);
        r72Var.a(u22Var);
        return u22Var;
    }
}
