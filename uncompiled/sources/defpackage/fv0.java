package defpackage;

import defpackage.fv0;

/* compiled from: EncoderConfig.java */
/* renamed from: fv0  reason: default package */
/* loaded from: classes2.dex */
public interface fv0<T extends fv0<T>> {
    <U> T a(Class<U> cls, hl2<? super U> hl2Var);
}
