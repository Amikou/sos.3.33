package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogConfirmationBinding.java */
/* renamed from: mn0  reason: default package */
/* loaded from: classes2.dex */
public final class mn0 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialTextView e;
    public final MaterialTextView f;

    public mn0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = materialTextView;
        this.f = materialTextView2;
    }

    public static mn0 a(View view) {
        int i = R.id.btnNegative;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnNegative);
        if (materialButton != null) {
            i = R.id.btnPositive;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPositive);
            if (materialButton2 != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton3 != null) {
                    i = R.id.tvDialogContent;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tvDialogContent);
                    if (materialTextView != null) {
                        i = R.id.tvDialogTitle;
                        MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.tvDialogTitle);
                        if (materialTextView2 != null) {
                            return new mn0((ConstraintLayout) view, materialButton, materialButton2, materialButton3, materialTextView, materialTextView2);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
