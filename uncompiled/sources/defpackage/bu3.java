package defpackage;

import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: StreamEncoder.java */
/* renamed from: bu3  reason: default package */
/* loaded from: classes.dex */
public class bu3 implements ev0<InputStream> {
    public final sh a;

    public bu3(sh shVar) {
        this.a = shVar;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v10 */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v9 */
    @Override // defpackage.ev0
    /* renamed from: c */
    public boolean a(InputStream inputStream, File file, vn2 vn2Var) {
        byte[] bArr = (byte[]) this.a.e(65536, byte[].class);
        boolean z = false;
        FileOutputStream fileOutputStream = 0;
        try {
            try {
                try {
                    FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                    while (true) {
                        try {
                            int read = inputStream.read(bArr);
                            fileOutputStream = -1;
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream2.write(bArr, 0, read);
                        } catch (IOException unused) {
                            fileOutputStream = fileOutputStream2;
                            Log.isLoggable("StreamEncoder", 3);
                            if (fileOutputStream != null) {
                                fileOutputStream.close();
                                fileOutputStream = fileOutputStream;
                            }
                            this.a.c(bArr);
                            return z;
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream = fileOutputStream2;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException unused2) {
                                }
                            }
                            this.a.c(bArr);
                            throw th;
                        }
                    }
                    fileOutputStream2.close();
                    z = true;
                    fileOutputStream2.close();
                } catch (IOException unused3) {
                }
            } catch (IOException unused4) {
            }
            this.a.c(bArr);
            return z;
        } catch (Throwable th2) {
            th = th2;
        }
    }
}
