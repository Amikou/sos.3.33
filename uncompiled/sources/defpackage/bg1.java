package defpackage;

import android.graphics.Bitmap;
import com.bumptech.glide.load.b;

/* compiled from: GifFrameResourceDecoder.java */
/* renamed from: bg1  reason: default package */
/* loaded from: classes.dex */
public final class bg1 implements b<tf1, Bitmap> {
    public final jq a;

    public bg1(jq jqVar) {
        this.a = jqVar;
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Bitmap> b(tf1 tf1Var, int i, int i2, vn2 vn2Var) {
        return oq.f(tf1Var.b(), this.a);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(tf1 tf1Var, vn2 vn2Var) {
        return true;
    }
}
