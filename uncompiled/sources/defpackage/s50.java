package defpackage;

import java.util.concurrent.locks.ReentrantLock;
import kotlinx.coroutines.channels.AbstractChannel;
import kotlinx.coroutines.internal.OnUndeliveredElementKt;
import kotlinx.coroutines.internal.UndeliveredElementException;

/* compiled from: ConflatedChannel.kt */
/* renamed from: s50  reason: default package */
/* loaded from: classes2.dex */
public class s50<E> extends AbstractChannel<E> {
    public final ReentrantLock h0;
    public Object i0;

    public s50(tc1<? super E, te4> tc1Var) {
        super(tc1Var);
        this.h0 = new ReentrantLock();
        this.i0 = q4.a;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public boolean F(e43<? super E> e43Var) {
        ReentrantLock reentrantLock = this.h0;
        reentrantLock.lock();
        try {
            return super.F(e43Var);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean G() {
        return false;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean H() {
        return this.i0 == q4.a;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public void J(boolean z) {
        ReentrantLock reentrantLock = this.h0;
        reentrantLock.lock();
        try {
            UndeliveredElementException Q = Q(q4.a);
            te4 te4Var = te4.a;
            reentrantLock.unlock();
            super.J(z);
            if (Q != null) {
                throw Q;
            }
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public Object N() {
        ReentrantLock reentrantLock = this.h0;
        reentrantLock.lock();
        try {
            Object obj = this.i0;
            k24 k24Var = q4.a;
            if (obj != k24Var) {
                this.i0 = k24Var;
                te4 te4Var = te4.a;
                return obj;
            }
            Object j = j();
            if (j == null) {
                j = q4.d;
            }
            return j;
        } finally {
            reentrantLock.unlock();
        }
    }

    public final UndeliveredElementException Q(Object obj) {
        tc1<E, te4> tc1Var;
        Object obj2 = this.i0;
        UndeliveredElementException undeliveredElementException = null;
        if (obj2 != q4.a && (tc1Var = this.a) != null) {
            undeliveredElementException = OnUndeliveredElementKt.d(tc1Var, obj2, null, 2, null);
        }
        this.i0 = obj;
        return undeliveredElementException;
    }

    @Override // defpackage.h5
    public String g() {
        return "(value=" + this.i0 + ')';
    }

    @Override // defpackage.h5
    public final boolean s() {
        return false;
    }

    @Override // defpackage.h5
    public final boolean t() {
        return false;
    }

    @Override // defpackage.h5
    public Object v(E e) {
        l43<E> z;
        k24 g;
        ReentrantLock reentrantLock = this.h0;
        reentrantLock.lock();
        try {
            d00<?> j = j();
            if (j == null) {
                if (this.i0 == q4.a) {
                    do {
                        z = z();
                        if (z != null) {
                            if (z instanceof d00) {
                                return z;
                            }
                            g = z.g(e, null);
                        }
                    } while (g == null);
                    if (ze0.a()) {
                        if (!(g == qv.a)) {
                            throw new AssertionError();
                        }
                    }
                    te4 te4Var = te4.a;
                    reentrantLock.unlock();
                    z.f(e);
                    return z.c();
                }
                UndeliveredElementException Q = Q(e);
                if (Q == null) {
                    return q4.b;
                }
                throw Q;
            }
            return j;
        } finally {
            reentrantLock.unlock();
        }
    }
}
