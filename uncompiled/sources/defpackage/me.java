package defpackage;

import defpackage.xg4;

/* compiled from: AnimationManager.java */
/* renamed from: me  reason: default package */
/* loaded from: classes2.dex */
public class me {
    public ge a;

    public me(mq1 mq1Var, xg4.a aVar) {
        this.a = new ge(mq1Var, aVar);
    }

    public void a() {
        ge geVar = this.a;
        if (geVar != null) {
            geVar.e();
            this.a.b();
        }
    }

    public void b() {
        ge geVar = this.a;
        if (geVar != null) {
            geVar.e();
        }
    }

    public void c(float f) {
        ge geVar = this.a;
        if (geVar != null) {
            geVar.g(f);
        }
    }
}
