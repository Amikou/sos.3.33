package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: qo5  reason: default package */
/* loaded from: classes.dex */
public final class qo5 implements Runnable {
    public final /* synthetic */ t45 a;
    public final /* synthetic */ long f0;
    public final /* synthetic */ int g0;
    public final /* synthetic */ long h0;
    public final /* synthetic */ boolean i0;
    public final /* synthetic */ dp5 j0;

    public qo5(dp5 dp5Var, t45 t45Var, long j, int i, long j2, boolean z) {
        this.j0 = dp5Var;
        this.a = t45Var;
        this.f0 = j;
        this.g0 = i;
        this.h0 = j2;
        this.i0 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.j0.W(this.a);
        this.j0.s(this.f0, false);
        dp5.J(this.j0, this.a, this.g0, this.h0, true, this.i0);
    }
}
