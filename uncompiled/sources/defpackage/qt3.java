package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.Array;
import java.util.Arrays;

/* compiled from: StepCurve.java */
/* renamed from: qt3  reason: default package */
/* loaded from: classes.dex */
public class qt3 extends yt0 {
    public p92 d;

    public qt3(String str) {
        this.a = str;
        double[] dArr = new double[str.length() / 2];
        int indexOf = str.indexOf(40) + 1;
        int indexOf2 = str.indexOf(44, indexOf);
        int i = 0;
        while (indexOf2 != -1) {
            dArr[i] = Double.parseDouble(str.substring(indexOf, indexOf2).trim());
            indexOf = indexOf2 + 1;
            indexOf2 = str.indexOf(44, indexOf);
            i++;
        }
        dArr[i] = Double.parseDouble(str.substring(indexOf, str.indexOf(41, indexOf)).trim());
        this.d = d(Arrays.copyOf(dArr, i + 1));
    }

    public static p92 d(double[] dArr) {
        int length = (dArr.length * 3) - 2;
        int length2 = dArr.length - 1;
        double d = 1.0d / length2;
        double[][] dArr2 = (double[][]) Array.newInstance(double.class, length, 1);
        double[] dArr3 = new double[length];
        for (int i = 0; i < dArr.length; i++) {
            double d2 = dArr[i];
            int i2 = i + length2;
            dArr2[i2][0] = d2;
            double d3 = i * d;
            dArr3[i2] = d3;
            if (i > 0) {
                int i3 = (length2 * 2) + i;
                dArr2[i3][0] = d2 + 1.0d;
                dArr3[i3] = d3 + 1.0d;
                int i4 = i - 1;
                dArr2[i4][0] = (d2 - 1.0d) - d;
                dArr3[i4] = (d3 - 1.0d) - d;
            }
        }
        p92 p92Var = new p92(dArr3, dArr2);
        System.out.println(" 0 " + p92Var.c(Utils.DOUBLE_EPSILON, 0));
        System.out.println(" 1 " + p92Var.c(1.0d, 0));
        return p92Var;
    }

    @Override // defpackage.yt0
    public double a(double d) {
        return this.d.c(d, 0);
    }

    @Override // defpackage.yt0
    public double b(double d) {
        return this.d.f(d, 0);
    }
}
