package defpackage;

import java.net.URI;

/* compiled from: AKTWebSocketUtill.kt */
/* renamed from: no4  reason: default package */
/* loaded from: classes2.dex */
public final class no4 {
    public mc2 a;
    public final a b;

    /* compiled from: AKTWebSocketUtill.kt */
    /* renamed from: no4$a */
    /* loaded from: classes2.dex */
    public static final class a extends mo4 {
        public a(URI uri, org.java_websocket.drafts.a aVar) {
            super(uri, aVar, null, 30000);
        }

        @Override // defpackage.mo4
        public void onClose(int i, String str, boolean z) {
            mc2 c = no4.this.c();
            if (c != null) {
                c.a(i, str, z);
            }
            if (str == null) {
                return;
            }
            e30.c0(str, "WebSocket");
        }

        @Override // defpackage.mo4
        public void onError(Exception exc) {
            mc2 c = no4.this.c();
            if (c != null) {
                c.b(exc);
            }
            String str = null;
            String localizedMessage = exc == null ? null : exc.getLocalizedMessage();
            if (localizedMessage != null) {
                str = localizedMessage;
            } else if (exc != null) {
                str = exc.getMessage();
            }
            if (str == null) {
                return;
            }
            e30.c0(str, "WebSocket");
        }

        @Override // defpackage.mo4
        public void onMessage(String str) {
            mc2 c = no4.this.c();
            if (c != null) {
                c.c(str);
            }
            if (str == null) {
                return;
            }
            e30.c0(str, "WebSocket");
        }

        @Override // defpackage.mo4
        public void onOpen(bm3 bm3Var) {
            mc2 c = no4.this.c();
            if (c != null) {
                c.d(bm3Var);
            }
            e30.c0("Opened", "WebSocket");
        }
    }

    public no4() {
        this(null, 1, null);
    }

    public no4(mc2 mc2Var) {
        this.a = mc2Var;
        a aVar = new a(new URI(t3.a()), new org.java_websocket.drafts.a());
        this.b = aVar;
        aVar.connect();
    }

    public final void a() {
        this.b.close();
    }

    public final mo4 b() {
        return this.b;
    }

    public final mc2 c() {
        return this.a;
    }

    public final void d() {
        this.b.reconnect();
    }

    public /* synthetic */ no4(mc2 mc2Var, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? null : mc2Var);
    }
}
