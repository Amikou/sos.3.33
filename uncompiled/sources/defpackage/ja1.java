package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentJoinCommunityBinding.java */
/* renamed from: ja1  reason: default package */
/* loaded from: classes2.dex */
public final class ja1 {
    public final LinearLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final MaterialButton f;
    public final MaterialButton g;
    public final MaterialButton h;
    public final MaterialButton i;
    public final rp3 j;

    public ja1(LinearLayout linearLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, MaterialButton materialButton5, MaterialButton materialButton6, MaterialButton materialButton7, MaterialButton materialButton8, rp3 rp3Var) {
        this.a = linearLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = materialButton4;
        this.f = materialButton5;
        this.g = materialButton6;
        this.h = materialButton7;
        this.i = materialButton8;
        this.j = rp3Var;
    }

    public static ja1 a(View view) {
        int i = R.id.joinCommunityButtonDiscord;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonDiscord);
        if (materialButton != null) {
            i = R.id.joinCommunityButtonFacebook;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonFacebook);
            if (materialButton2 != null) {
                i = R.id.joinCommunityButtonInstagram;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonInstagram);
                if (materialButton3 != null) {
                    i = R.id.joinCommunityButtonLinkedin;
                    MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonLinkedin);
                    if (materialButton4 != null) {
                        i = R.id.joinCommunityButtonReddit;
                        MaterialButton materialButton5 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonReddit);
                        if (materialButton5 != null) {
                            i = R.id.joinCommunityButtonTwitter;
                            MaterialButton materialButton6 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonTwitter);
                            if (materialButton6 != null) {
                                i = R.id.joinCommunityButtonWeb;
                                MaterialButton materialButton7 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonWeb);
                                if (materialButton7 != null) {
                                    i = R.id.joinCommunityButtonYoutube;
                                    MaterialButton materialButton8 = (MaterialButton) ai4.a(view, R.id.joinCommunityButtonYoutube);
                                    if (materialButton8 != null) {
                                        i = R.id.toolbar;
                                        View a = ai4.a(view, R.id.toolbar);
                                        if (a != null) {
                                            return new ja1((LinearLayout) view, materialButton, materialButton2, materialButton3, materialButton4, materialButton5, materialButton6, materialButton7, materialButton8, rp3.a(a));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public LinearLayout b() {
        return this.a;
    }
}
