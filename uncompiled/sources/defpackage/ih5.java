package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ih5  reason: default package */
/* loaded from: classes.dex */
public final class ih5 extends BroadcastReceiver {
    public final fw5 a;
    public boolean b;
    public boolean c;

    public ih5(fw5 fw5Var) {
        zt2.j(fw5Var);
        this.a = fw5Var;
    }

    public final void a() {
        this.a.d0();
        this.a.q().e();
        if (this.b) {
            return;
        }
        this.a.m().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        this.c = this.a.U().j();
        this.a.w().v().b("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.c));
        this.b = true;
    }

    public final void b() {
        this.a.d0();
        this.a.q().e();
        this.a.q().e();
        if (this.b) {
            this.a.w().v().a("Unregistering connectivity change receiver");
            this.b = false;
            this.c = false;
            try {
                this.a.m().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.a.w().l().b("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @Override // android.content.BroadcastReceiver
    public final void onReceive(Context context, Intent intent) {
        this.a.d0();
        String action = intent.getAction();
        this.a.w().v().b("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean j = this.a.U().j();
            if (this.c != j) {
                this.c = j;
                this.a.q().p(new fh5(this, j));
                return;
            }
            return;
        }
        this.a.w().p().b("NetworkBroadcastReceiver received unknown action", action);
    }
}
