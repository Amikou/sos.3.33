package defpackage;

import com.github.mikephil.charting.utils.Utils;
import defpackage.ss0;

/* compiled from: SpringAnimation.java */
/* renamed from: zr3  reason: default package */
/* loaded from: classes.dex */
public final class zr3 extends ss0<zr3> {
    public as3 s;
    public float t;
    public boolean u;

    public <K> zr3(K k, d71<K> d71Var) {
        super(k, d71Var);
        this.s = null;
        this.t = Float.MAX_VALUE;
        this.u = false;
    }

    @Override // defpackage.ss0
    public void j() {
        o();
        this.s.g(e());
        super.j();
    }

    @Override // defpackage.ss0
    public boolean l(long j) {
        if (this.u) {
            float f = this.t;
            if (f != Float.MAX_VALUE) {
                this.s.e(f);
                this.t = Float.MAX_VALUE;
            }
            this.b = this.s.a();
            this.a = Utils.FLOAT_EPSILON;
            this.u = false;
            return true;
        }
        if (this.t != Float.MAX_VALUE) {
            this.s.a();
            long j2 = j / 2;
            ss0.o h = this.s.h(this.b, this.a, j2);
            this.s.e(this.t);
            this.t = Float.MAX_VALUE;
            ss0.o h2 = this.s.h(h.a, h.b, j2);
            this.b = h2.a;
            this.a = h2.b;
        } else {
            ss0.o h3 = this.s.h(this.b, this.a, j);
            this.b = h3.a;
            this.a = h3.b;
        }
        float max = Math.max(this.b, this.h);
        this.b = max;
        float min = Math.min(max, this.g);
        this.b = min;
        if (n(min, this.a)) {
            this.b = this.s.a();
            this.a = Utils.FLOAT_EPSILON;
            return true;
        }
        return false;
    }

    public void m(float f) {
        if (f()) {
            this.t = f;
            return;
        }
        if (this.s == null) {
            this.s = new as3(f);
        }
        this.s.e(f);
        j();
    }

    public boolean n(float f, float f2) {
        return this.s.c(f, f2);
    }

    public final void o() {
        as3 as3Var = this.s;
        if (as3Var != null) {
            double a = as3Var.a();
            if (a <= this.g) {
                if (a < this.h) {
                    throw new UnsupportedOperationException("Final position of the spring cannot be less than the min value.");
                }
                return;
            }
            throw new UnsupportedOperationException("Final position of the spring cannot be greater than the max value.");
        }
        throw new UnsupportedOperationException("Incomplete SpringAnimation: Either final position or a spring force needs to be set.");
    }

    public zr3 p(as3 as3Var) {
        this.s = as3Var;
        return this;
    }
}
