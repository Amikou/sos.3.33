package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: q25  reason: default package */
/* loaded from: classes.dex */
public class q25 extends Handler {
    public q25(Looper looper) {
        super(looper);
    }

    public q25(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
