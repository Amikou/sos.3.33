package defpackage;

import com.fasterxml.jackson.databind.JavaType;

/* compiled from: TypeKey.java */
/* renamed from: rd4  reason: default package */
/* loaded from: classes.dex */
public class rd4 {
    public int a;
    public Class<?> b;
    public JavaType c;
    public boolean d;

    public rd4() {
    }

    public static final int d(JavaType javaType) {
        return javaType.hashCode() - 2;
    }

    public static final int e(Class<?> cls) {
        return cls.getName().hashCode() + 1;
    }

    public static final int f(JavaType javaType) {
        return javaType.hashCode() - 1;
    }

    public static final int g(Class<?> cls) {
        return cls.getName().hashCode();
    }

    public Class<?> a() {
        return this.b;
    }

    public JavaType b() {
        return this.c;
    }

    public boolean c() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != rd4.class) {
            return false;
        }
        rd4 rd4Var = (rd4) obj;
        if (rd4Var.d == this.d) {
            Class<?> cls = this.b;
            if (cls != null) {
                return rd4Var.b == cls;
            }
            return this.c.equals(rd4Var.c);
        }
        return false;
    }

    public final int hashCode() {
        return this.a;
    }

    public final String toString() {
        if (this.b != null) {
            return "{class: " + this.b.getName() + ", typed? " + this.d + "}";
        }
        return "{type: " + this.c + ", typed? " + this.d + "}";
    }

    public rd4(Class<?> cls, boolean z) {
        this.b = cls;
        this.c = null;
        this.d = z;
        this.a = z ? e(cls) : g(cls);
    }

    public rd4(JavaType javaType, boolean z) {
        this.c = javaType;
        this.b = null;
        this.d = z;
        this.a = z ? d(javaType) : f(javaType);
    }
}
