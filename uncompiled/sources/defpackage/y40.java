package defpackage;

import android.content.res.Configuration;
import android.os.Build;

/* compiled from: ConfigurationCompat.java */
/* renamed from: y40  reason: default package */
/* loaded from: classes.dex */
public final class y40 {
    public static c12 a(Configuration configuration) {
        return Build.VERSION.SDK_INT >= 24 ? c12.d(configuration.getLocales()) : c12.a(configuration.locale);
    }
}
