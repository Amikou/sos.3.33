package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: wp1  reason: default package */
/* loaded from: classes2.dex */
public final class wp1 implements dq1 {
    public final tg2 a;

    public wp1(tg2 tg2Var) {
        this.a = tg2Var;
    }

    @Override // defpackage.dq1
    public boolean b() {
        return false;
    }

    @Override // defpackage.dq1
    public tg2 e() {
        return this.a;
    }

    public String toString() {
        return ze0.c() ? e().z("New") : super.toString();
    }
}
