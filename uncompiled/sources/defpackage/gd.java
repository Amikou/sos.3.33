package defpackage;

import android.annotation.SuppressLint;
import android.app.Application;

/* compiled from: AndroidViewModel.java */
/* renamed from: gd  reason: default package */
/* loaded from: classes.dex */
public class gd extends dj4 {
    @SuppressLint({"StaticFieldLeak"})
    public Application a;

    public gd(Application application) {
        this.a = application;
    }

    public <T extends Application> T a() {
        return (T) this.a;
    }
}
