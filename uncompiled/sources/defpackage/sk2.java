package defpackage;

import com.onesignal.q0;
import org.json.JSONObject;

/* compiled from: OSPermissionStateChanges.java */
/* renamed from: sk2  reason: default package */
/* loaded from: classes2.dex */
public class sk2 {
    public q0 a;
    public q0 b;

    public sk2(q0 q0Var, q0 q0Var2) {
        this.a = q0Var;
        this.b = q0Var2;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("from", this.a.g());
            jSONObject.put("to", this.b.g());
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return a().toString();
    }
}
