package defpackage;

/* compiled from: NoOpMemoryTrimmableRegistry.java */
/* renamed from: qg2  reason: default package */
/* loaded from: classes.dex */
public class qg2 implements r72 {
    public static qg2 a;

    public static synchronized qg2 b() {
        qg2 qg2Var;
        synchronized (qg2.class) {
            if (a == null) {
                a = new qg2();
            }
            qg2Var = a;
        }
        return qg2Var;
    }

    @Override // defpackage.r72
    public void a(q72 q72Var) {
    }
}
