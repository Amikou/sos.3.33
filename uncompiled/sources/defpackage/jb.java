package defpackage;

import android.os.Looper;
import androidx.media3.common.q;
import androidx.media3.exoplayer.drm.b;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.source.k;
import defpackage.gm;
import java.util.List;

/* compiled from: AnalyticsCollector.java */
/* renamed from: jb  reason: default package */
/* loaded from: classes.dex */
public interface jb extends q.d, k, gm.a, b {
    void B(hf0 hf0Var);

    void C(long j, int i);

    void G(List<j.b> list, j.b bVar);

    void M();

    void P(tb tbVar);

    void a();

    void c(Exception exc);

    void d(androidx.media3.common.j jVar, jf0 jf0Var);

    void e(String str);

    void e0(q qVar, Looper looper);

    void f(String str, long j, long j2);

    void g(hf0 hf0Var);

    void h(hf0 hf0Var);

    void j(hf0 hf0Var);

    void l(String str);

    void m(String str, long j, long j2);

    void p(int i, long j);

    void q(androidx.media3.common.j jVar, jf0 jf0Var);

    void r(Object obj, long j);

    void v(long j);

    void w(Exception exc);

    void x(Exception exc);

    void z(int i, long j, long j2);
}
