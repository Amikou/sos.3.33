package defpackage;

import androidx.lifecycle.l;
import net.safemoon.androidwallet.viewmodels.GraphViewModel;

/* compiled from: GraphViewModelFactory.kt */
/* renamed from: qi1  reason: default package */
/* loaded from: classes2.dex */
public final class qi1 implements l.b {
    @Override // androidx.lifecycle.l.b
    public <T extends dj4> T a(Class<T> cls) {
        fs1.f(cls, "modelClass");
        jt e = a4.e();
        fs1.e(e, "getCMCClient()");
        e42 k = a4.k();
        fs1.e(k, "getMarketClient()");
        return new GraphViewModel(e, k);
    }
}
