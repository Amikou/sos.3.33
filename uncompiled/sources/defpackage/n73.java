package defpackage;

import android.graphics.Bitmap;
import com.bumptech.glide.request.a;

/* compiled from: RequestOptions.java */
/* renamed from: n73  reason: default package */
/* loaded from: classes.dex */
public class n73 extends a<n73> {
    public static n73 E0;
    public static n73 F0;
    public static n73 G0;

    public static n73 A0(boolean z) {
        if (z) {
            if (E0 == null) {
                E0 = new n73().o0(true).b();
            }
            return E0;
        }
        if (F0 == null) {
            F0 = new n73().o0(false).b();
        }
        return F0;
    }

    public static n73 u0(za4<Bitmap> za4Var) {
        return new n73().p0(za4Var);
    }

    public static n73 v0() {
        if (G0 == null) {
            G0 = new n73().f().b();
        }
        return G0;
    }

    public static n73 w0(Class<?> cls) {
        return new n73().h(cls);
    }

    public static n73 x0(bp0 bp0Var) {
        return new n73().j(bp0Var);
    }

    public static n73 z0(fx1 fx1Var) {
        return new n73().m0(fx1Var);
    }
}
