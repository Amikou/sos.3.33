package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: k16  reason: default package */
/* loaded from: classes.dex */
public final class k16 implements yp5<l16> {
    public static final k16 f0 = new k16();
    public final yp5<l16> a = gq5.a(gq5.b(new m16()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    public static boolean c() {
        return f0.zza().zzc();
    }

    public static boolean d() {
        return f0.zza().b();
    }

    @Override // defpackage.yp5
    /* renamed from: e */
    public final l16 zza() {
        return this.a.zza();
    }
}
