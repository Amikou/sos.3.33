package defpackage;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: q55  reason: default package */
/* loaded from: classes.dex */
public final class q55 extends sl5 {
    public long c;
    public String d;
    public AccountManager e;
    public Boolean f;
    public long g;

    public q55(ck5 ck5Var) {
        super(ck5Var);
    }

    @Override // defpackage.sl5
    public final boolean f() {
        Calendar calendar = Calendar.getInstance();
        this.c = TimeUnit.MINUTES.convert(calendar.get(15) + calendar.get(16), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        Locale locale2 = Locale.ENGLISH;
        String lowerCase = language.toLowerCase(locale2);
        String lowerCase2 = locale.getCountry().toLowerCase(locale2);
        StringBuilder sb = new StringBuilder(String.valueOf(lowerCase).length() + 1 + String.valueOf(lowerCase2).length());
        sb.append(lowerCase);
        sb.append("-");
        sb.append(lowerCase2);
        this.d = sb.toString();
        return false;
    }

    public final long l() {
        i();
        return this.c;
    }

    public final String n() {
        i();
        return this.d;
    }

    public final long o() {
        e();
        return this.g;
    }

    public final void p() {
        e();
        this.f = null;
        this.g = 0L;
    }

    public final boolean r() {
        Account[] result;
        e();
        long a = this.a.a().a();
        if (a - this.g > 86400000) {
            this.f = null;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (m70.a(this.a.m(), "android.permission.GET_ACCOUNTS") != 0) {
                this.a.w().r().a("Permission error checking for dasher/unicorn accounts");
                this.g = a;
                this.f = Boolean.FALSE;
                return false;
            }
            if (this.e == null) {
                this.e = AccountManager.get(this.a.m());
            }
            try {
                result = this.e.getAccountsByTypeAndFeatures("com.google", new String[]{"service_HOSTED"}, null, null).getResult();
            } catch (AuthenticatorException | OperationCanceledException | IOException e) {
                this.a.w().n().b("Exception checking account types", e);
            }
            if (result != null && result.length > 0) {
                this.f = Boolean.TRUE;
                this.g = a;
                return true;
            }
            Account[] result2 = this.e.getAccountsByTypeAndFeatures("com.google", new String[]{"service_uca"}, null, null).getResult();
            if (result2 != null && result2.length > 0) {
                this.f = Boolean.TRUE;
                this.g = a;
                return true;
            }
            this.g = a;
            this.f = Boolean.FALSE;
            return false;
        }
        return bool.booleanValue();
    }
}
