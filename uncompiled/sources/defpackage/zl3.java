package defpackage;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import java.lang.reflect.Type;

/* compiled from: GsonBuilder.kt */
/* renamed from: zl3  reason: default package */
/* loaded from: classes.dex */
public final class zl3<T> {
    public final T a;
    public final Type b;
    public final a c;

    /* compiled from: GsonBuilder.kt */
    /* renamed from: zl3$a */
    /* loaded from: classes.dex */
    public static final class a implements JsonSerializationContext {
        public final JsonSerializationContext a;

        public a(JsonSerializationContext jsonSerializationContext) {
            fs1.g(jsonSerializationContext, "gsonContext");
            this.a = jsonSerializationContext;
        }

        @Override // com.google.gson.JsonSerializationContext
        public JsonElement serialize(Object obj) {
            return this.a.serialize(obj);
        }

        @Override // com.google.gson.JsonSerializationContext
        public JsonElement serialize(Object obj, Type type) {
            return this.a.serialize(obj, type);
        }
    }

    public zl3(T t, Type type, a aVar) {
        fs1.g(type, "type");
        fs1.g(aVar, "context");
        this.a = t;
        this.b = type;
        this.c = aVar;
    }

    public final a a() {
        return this.c;
    }

    public final T b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof zl3) {
                zl3 zl3Var = (zl3) obj;
                return fs1.b(this.a, zl3Var.a) && fs1.b(this.b, zl3Var.b) && fs1.b(this.c, zl3Var.c);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        T t = this.a;
        int hashCode = (t != null ? t.hashCode() : 0) * 31;
        Type type = this.b;
        int hashCode2 = (hashCode + (type != null ? type.hashCode() : 0)) * 31;
        a aVar = this.c;
        return hashCode2 + (aVar != null ? aVar.hashCode() : 0);
    }

    public String toString() {
        return "SerializerArg(src=" + this.a + ", type=" + this.b + ", context=" + this.c + ")";
    }
}
