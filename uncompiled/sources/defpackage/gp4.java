package defpackage;

import android.os.IBinder;

/* compiled from: WindowIdApi14.java */
/* renamed from: gp4  reason: default package */
/* loaded from: classes.dex */
public class gp4 implements ip4 {
    public final IBinder a;

    public gp4(IBinder iBinder) {
        this.a = iBinder;
    }

    public boolean equals(Object obj) {
        return (obj instanceof gp4) && ((gp4) obj).a.equals(this.a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
