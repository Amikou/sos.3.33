package defpackage;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.i;

/* renamed from: is4  reason: default package */
/* loaded from: classes2.dex */
public final class is4 extends pi {
    public final fs4 a;
    public final byte[] f0;
    public final byte[] g0;

    /* renamed from: is4$b */
    /* loaded from: classes2.dex */
    public static class b {
        public final fs4 a;
        public byte[] b = null;
        public byte[] c = null;
        public byte[] d = null;

        public b(fs4 fs4Var) {
            this.a = fs4Var;
        }

        public is4 e() {
            return new is4(this);
        }

        public b f(byte[] bArr) {
            this.c = i.c(bArr);
            return this;
        }

        public b g(byte[] bArr) {
            this.b = i.c(bArr);
            return this;
        }
    }

    public is4(b bVar) {
        super(false);
        fs4 fs4Var = bVar.a;
        this.a = fs4Var;
        Objects.requireNonNull(fs4Var, "params == null");
        int c = fs4Var.c();
        byte[] bArr = bVar.d;
        if (bArr != null) {
            if (bArr.length != c + c) {
                throw new IllegalArgumentException("public key has wrong size");
            }
            this.f0 = i.g(bArr, 0, c);
            this.g0 = i.g(bArr, c + 0, c);
            return;
        }
        byte[] bArr2 = bVar.b;
        if (bArr2 == null) {
            this.f0 = new byte[c];
        } else if (bArr2.length != c) {
            throw new IllegalArgumentException("length of root must be equal to length of digest");
        } else {
            this.f0 = bArr2;
        }
        byte[] bArr3 = bVar.c;
        if (bArr3 == null) {
            this.g0 = new byte[c];
        } else if (bArr3.length != c) {
            throw new IllegalArgumentException("length of publicSeed must be equal to length of digest");
        } else {
            this.g0 = bArr3;
        }
    }

    public fs4 a() {
        return this.a;
    }

    public byte[] b() {
        return i.c(this.g0);
    }

    public byte[] c() {
        return i.c(this.f0);
    }

    public byte[] d() {
        int c = this.a.c();
        byte[] bArr = new byte[c + c];
        i.e(bArr, this.f0, 0);
        i.e(bArr, this.g0, c + 0);
        return bArr;
    }
}
