package defpackage;

import android.app.Notification;

/* compiled from: ForegroundInfo.java */
/* renamed from: s81  reason: default package */
/* loaded from: classes.dex */
public final class s81 {
    public final int a;
    public final int b;
    public final Notification c;

    public s81(int i, Notification notification, int i2) {
        this.a = i;
        this.c = notification;
        this.b = i2;
    }

    public int a() {
        return this.b;
    }

    public Notification b() {
        return this.c;
    }

    public int c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || s81.class != obj.getClass()) {
            return false;
        }
        s81 s81Var = (s81) obj;
        if (this.a == s81Var.a && this.b == s81Var.b) {
            return this.c.equals(s81Var.c);
        }
        return false;
    }

    public int hashCode() {
        return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
    }

    public String toString() {
        return "ForegroundInfo{mNotificationId=" + this.a + ", mForegroundServiceType=" + this.b + ", mNotification=" + this.c + '}';
    }
}
