package defpackage;

import java.security.spec.KeySpec;

/* renamed from: n33  reason: default package */
/* loaded from: classes2.dex */
public class n33 implements KeySpec {
    public short[][] a;
    public short[][] f0;
    public short[] g0;
    public int h0;

    public n33(int i, short[][] sArr, short[][] sArr2, short[] sArr3) {
        this.h0 = i;
        this.a = sArr;
        this.f0 = sArr2;
        this.g0 = sArr3;
    }

    public short[][] a() {
        return this.a;
    }

    public short[] b() {
        return this.g0;
    }

    public short[][] c() {
        return this.f0;
    }

    public int d() {
        return this.h0;
    }
}
