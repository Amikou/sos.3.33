package defpackage;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;

/* compiled from: GlideUrl.java */
/* renamed from: ng1  reason: default package */
/* loaded from: classes.dex */
public class ng1 implements fx1 {
    public final gk1 b;
    public final URL c;
    public final String d;
    public String e;
    public URL f;
    public volatile byte[] g;
    public int h;

    public ng1(URL url) {
        this(url, gk1.a);
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        messageDigest.update(d());
    }

    public String c() {
        String str = this.d;
        return str != null ? str : ((URL) wt2.d(this.c)).toString();
    }

    public final byte[] d() {
        if (this.g == null) {
            this.g = c().getBytes(fx1.a);
        }
        return this.g;
    }

    public Map<String, String> e() {
        return this.b.a();
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof ng1) {
            ng1 ng1Var = (ng1) obj;
            return c().equals(ng1Var.c()) && this.b.equals(ng1Var.b);
        }
        return false;
    }

    public final String f() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                str = ((URL) wt2.d(this.c)).toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    public final URL g() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(f());
        }
        return this.f;
    }

    public String h() {
        return f();
    }

    @Override // defpackage.fx1
    public int hashCode() {
        if (this.h == 0) {
            int hashCode = c().hashCode();
            this.h = hashCode;
            this.h = (hashCode * 31) + this.b.hashCode();
        }
        return this.h;
    }

    public URL i() throws MalformedURLException {
        return g();
    }

    public String toString() {
        return c();
    }

    public ng1(String str) {
        this(str, gk1.a);
    }

    public ng1(URL url, gk1 gk1Var) {
        this.c = (URL) wt2.d(url);
        this.d = null;
        this.b = (gk1) wt2.d(gk1Var);
    }

    public ng1(String str, gk1 gk1Var) {
        this.c = null;
        this.d = wt2.b(str);
        this.b = (gk1) wt2.d(gk1Var);
    }
}
