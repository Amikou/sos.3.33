package defpackage;

import java.math.BigInteger;
import java.security.SecureRandom;

/* renamed from: od0  reason: default package */
/* loaded from: classes2.dex */
public interface od0 {
    BigInteger a();

    boolean b();

    void c(BigInteger bigInteger, SecureRandom secureRandom);

    void d(BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr);
}
