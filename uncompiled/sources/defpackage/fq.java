package defpackage;

import android.util.Pair;
import com.facebook.common.references.a;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: BitmapMemoryCacheKeyMultiplexProducer.java */
/* renamed from: fq  reason: default package */
/* loaded from: classes.dex */
public class fq extends eb2<Pair<wt, ImageRequest.RequestLevel>, a<com.facebook.imagepipeline.image.a>> {
    public final xt f;

    public fq(xt xtVar, dv2 dv2Var) {
        super(dv2Var, "BitmapMemoryCacheKeyMultiplexProducer", "multiplex_bmp_cnt");
        this.f = xtVar;
    }

    @Override // defpackage.eb2
    /* renamed from: k */
    public a<com.facebook.imagepipeline.image.a> f(a<com.facebook.imagepipeline.image.a> aVar) {
        return a.e(aVar);
    }

    @Override // defpackage.eb2
    /* renamed from: l */
    public Pair<wt, ImageRequest.RequestLevel> i(ev2 ev2Var) {
        return Pair.create(this.f.a(ev2Var.c(), ev2Var.a()), ev2Var.n());
    }
}
