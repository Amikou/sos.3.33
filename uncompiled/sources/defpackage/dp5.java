package defpackage;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.measurement.internal.zzkq;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: dp5  reason: default package */
/* loaded from: classes.dex */
public final class dp5 extends vh5 {
    public bp5 c;
    public cm5 d;
    public final Set<em5> e;
    public boolean f;
    public final AtomicReference<String> g;
    public final Object h;
    public t45 i;
    public int j;
    public final AtomicLong k;
    public long l;
    public int m;
    public final k46 n;
    public boolean o;
    public final pw5 p;

    public dp5(ck5 ck5Var) {
        super(ck5Var);
        this.e = new CopyOnWriteArraySet();
        this.h = new Object();
        this.o = true;
        this.p = new co5(this);
        this.g = new AtomicReference<>();
        this.i = new t45(null, null);
        this.j = 100;
        this.l = -1L;
        this.m = 100;
        this.k = new AtomicLong(0L);
        this.n = new k46(ck5Var);
    }

    public static /* synthetic */ void J(dp5 dp5Var, t45 t45Var, int i, long j, boolean z, boolean z2) {
        dp5Var.e();
        dp5Var.g();
        if (j <= dp5Var.l && t45.m(dp5Var.m, i)) {
            dp5Var.a.w().t().b("Dropped out-of-date consent setting, proposed settings", t45Var);
            return;
        }
        mi5 A = dp5Var.a.A();
        ck5 ck5Var = A.a;
        A.e();
        if (A.r(i)) {
            SharedPreferences.Editor edit = A.n().edit();
            edit.putString("consent_settings", t45Var.d());
            edit.putInt("consent_source", i);
            edit.apply();
            dp5Var.l = j;
            dp5Var.m = i;
            dp5Var.a.R().J(z);
            if (z2) {
                dp5Var.a.R().T(new AtomicReference<>());
                return;
            }
            return;
        }
        dp5Var.a.w().t().b("Lower precedence consent source ignored, proposed source", Integer.valueOf(i));
    }

    public final void A(Bundle bundle, long j) {
        zt2.j(bundle);
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            this.a.w().p().a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        zt2.j(bundle2);
        vl5.b(bundle2, "app_id", String.class, null);
        vl5.b(bundle2, "origin", String.class, null);
        vl5.b(bundle2, PublicResolver.FUNC_NAME, String.class, null);
        vl5.b(bundle2, "value", Object.class, null);
        vl5.b(bundle2, "trigger_event_name", String.class, null);
        vl5.b(bundle2, "trigger_timeout", Long.class, 0L);
        vl5.b(bundle2, "timed_out_event_name", String.class, null);
        vl5.b(bundle2, "timed_out_event_params", Bundle.class, null);
        vl5.b(bundle2, "triggered_event_name", String.class, null);
        vl5.b(bundle2, "triggered_event_params", Bundle.class, null);
        vl5.b(bundle2, "time_to_live", Long.class, 0L);
        vl5.b(bundle2, "expired_event_name", String.class, null);
        vl5.b(bundle2, "expired_event_params", Bundle.class, null);
        zt2.f(bundle2.getString(PublicResolver.FUNC_NAME));
        zt2.f(bundle2.getString("origin"));
        zt2.j(bundle2.get("value"));
        bundle2.putLong("creation_timestamp", j);
        String string = bundle2.getString(PublicResolver.FUNC_NAME);
        Object obj = bundle2.get("value");
        if (this.a.G().q0(string) == 0) {
            if (this.a.G().x(string, obj) == 0) {
                Object y = this.a.G().y(string, obj);
                if (y == null) {
                    this.a.w().l().c("Unable to normalize conditional user property value", this.a.H().p(string), obj);
                    return;
                }
                vl5.a(bundle2, y);
                long j2 = bundle2.getLong("trigger_timeout");
                if (!TextUtils.isEmpty(bundle2.getString("trigger_event_name"))) {
                    this.a.z();
                    if (j2 > 15552000000L || j2 < 1) {
                        this.a.w().l().c("Invalid conditional user property timeout", this.a.H().p(string), Long.valueOf(j2));
                        return;
                    }
                }
                long j3 = bundle2.getLong("time_to_live");
                this.a.z();
                if (j3 <= 15552000000L && j3 >= 1) {
                    this.a.q().p(new pn5(this, bundle2));
                    return;
                } else {
                    this.a.w().l().c("Invalid conditional user property time to live", this.a.H().p(string), Long.valueOf(j3));
                    return;
                }
            }
            this.a.w().l().c("Invalid conditional user property value", this.a.H().p(string), obj);
            return;
        }
        this.a.w().l().b("Invalid conditional user property name", this.a.H().p(string));
    }

    public final void B(String str, String str2, Bundle bundle) {
        long a = this.a.a().a();
        zt2.f(str);
        Bundle bundle2 = new Bundle();
        bundle2.putString(PublicResolver.FUNC_NAME, str);
        bundle2.putLong("creation_timestamp", a);
        if (str2 != null) {
            bundle2.putString("expired_event_name", str2);
            bundle2.putBundle("expired_event_params", bundle);
        }
        this.a.q().p(new rn5(this, bundle2));
    }

    public final ArrayList<Bundle> C(String str, String str2) {
        if (this.a.q().l()) {
            this.a.w().l().a("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        }
        this.a.b();
        if (c66.a()) {
            this.a.w().l().a("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        }
        AtomicReference atomicReference = new AtomicReference();
        this.a.q().r(atomicReference, 5000L, "get conditional user properties", new xn5(this, atomicReference, null, str, str2));
        List list = (List) atomicReference.get();
        if (list == null) {
            this.a.w().l().b("Timed out waiting for get conditional user properties", null);
            return new ArrayList<>();
        }
        return sw5.Y(list);
    }

    public final Map<String, Object> D(String str, String str2, boolean z) {
        if (this.a.q().l()) {
            this.a.w().l().a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        }
        this.a.b();
        if (c66.a()) {
            this.a.w().l().a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        }
        AtomicReference atomicReference = new AtomicReference();
        this.a.q().r(atomicReference, 5000L, "get user properties", new ao5(this, atomicReference, null, str, str2, z));
        List<zzkq> list = (List) atomicReference.get();
        if (list == null) {
            this.a.w().l().b("Timed out waiting for handle get user properties, includeInternal", Boolean.valueOf(z));
            return Collections.emptyMap();
        }
        rh rhVar = new rh(list.size());
        for (zzkq zzkqVar : list) {
            Object I1 = zzkqVar.I1();
            if (I1 != null) {
                rhVar.put(zzkqVar.f0, I1);
            }
        }
        return rhVar;
    }

    public final String E() {
        bq5 v = this.a.Q().v();
        if (v != null) {
            return v.a;
        }
        return null;
    }

    public final String F() {
        bq5 v = this.a.Q().v();
        if (v != null) {
            return v.b;
        }
        return null;
    }

    public final String G() {
        if (this.a.L() == null) {
            try {
                return zp5.a(this.a.m(), "google_app_id", this.a.P());
            } catch (IllegalStateException e) {
                this.a.w().l().b("getGoogleAppId failed with exception", e);
                return null;
            }
        }
        return this.a.L();
    }

    public final /* synthetic */ void H(Bundle bundle) {
        if (bundle == null) {
            this.a.A().w.b(new Bundle());
            return;
        }
        Bundle a = this.a.A().w.a();
        for (String str : bundle.keySet()) {
            Object obj = bundle.get(str);
            if (obj != null && !(obj instanceof String) && !(obj instanceof Long) && !(obj instanceof Double)) {
                if (this.a.G().t0(obj)) {
                    this.a.G().A(this.p, null, 27, null, null, 0, this.a.z().v(null, qf5.w0));
                }
                this.a.w().s().c("Invalid default event parameter type. Name, value", str, obj);
            } else if (sw5.F(str)) {
                this.a.w().s().b("Invalid default event parameter name. Name", str);
            } else if (obj == null) {
                a.remove(str);
            } else {
                sw5 G = this.a.G();
                this.a.z();
                if (G.u0("param", str, 100, obj)) {
                    this.a.G().z(a, str, obj);
                }
            }
        }
        this.a.G();
        int j = this.a.z().j();
        if (a.size() > j) {
            int i = 0;
            for (String str2 : new TreeSet(a.keySet())) {
                i++;
                if (i > j) {
                    a.remove(str2);
                }
            }
            this.a.G().A(this.p, null, 26, null, null, 0, this.a.z().v(null, qf5.w0));
            this.a.w().s().a("Too many default event parameters set. Discarding beyond event parameter limit");
        }
        this.a.A().w.b(a);
        this.a.R().l(a);
    }

    public final void L(Boolean bool, boolean z) {
        e();
        g();
        this.a.w().u().b("Setting app measurement enabled (FE)", bool);
        this.a.A().o(bool);
        if (z) {
            mi5 A = this.a.A();
            ck5 ck5Var = A.a;
            A.e();
            SharedPreferences.Editor edit = A.n().edit();
            if (bool != null) {
                edit.putBoolean("measurement_enabled_from_api", bool.booleanValue());
            } else {
                edit.remove("measurement_enabled_from_api");
            }
            edit.apply();
        }
        if (this.a.k() || !(bool == null || bool.booleanValue())) {
            M();
        }
    }

    public final void M() {
        e();
        String a = this.a.A().m.a();
        if (a != null) {
            if ("unset".equals(a)) {
                n("app", "_npa", null, this.a.a().a());
            } else {
                n("app", "_npa", Long.valueOf(true != "true".equals(a) ? 0L : 1L), this.a.a().a());
            }
        }
        if (this.a.h() && this.o) {
            this.a.w().u().a("Recording app launch after enabling measurement for the first time (FE)");
            t();
            q16.a();
            if (this.a.z().v(null, qf5.o0)) {
                this.a.C().d.a();
            }
            this.a.q().p(new an5(this));
            return;
        }
        this.a.w().u().a("Updating Scion state (FE)");
        this.a.R().I();
    }

    public final void N() {
        if (!(this.a.m().getApplicationContext() instanceof Application) || this.c == null) {
            return;
        }
        ((Application) this.a.m().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.c);
    }

    public final Boolean O() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) this.a.q().r(atomicReference, u84.DEFAULT_POLLING_FREQUENCY, "boolean test flag value", new un5(this, atomicReference));
    }

    public final String P() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) this.a.q().r(atomicReference, u84.DEFAULT_POLLING_FREQUENCY, "String test flag value", new eo5(this, atomicReference));
    }

    public final Long Q() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) this.a.q().r(atomicReference, u84.DEFAULT_POLLING_FREQUENCY, "long test flag value", new ho5(this, atomicReference));
    }

    public final Integer R() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) this.a.q().r(atomicReference, u84.DEFAULT_POLLING_FREQUENCY, "int test flag value", new jo5(this, atomicReference));
    }

    public final Double S() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) this.a.q().r(atomicReference, u84.DEFAULT_POLLING_FREQUENCY, "double test flag value", new lo5(this, atomicReference));
    }

    public final void T(Boolean bool) {
        g();
        this.a.q().p(new no5(this, bool));
    }

    public final void U(Bundle bundle, int i, long j) {
        g();
        String a = t45.a(bundle);
        if (a != null) {
            this.a.w().s().b("Ignoring invalid consent setting", a);
            this.a.w().s().a("Valid consent values are 'granted', 'denied'");
        }
        V(t45.b(bundle), i, j);
    }

    public final void V(t45 t45Var, int i, long j) {
        boolean z;
        boolean z2;
        t45 t45Var2;
        boolean z3;
        g();
        if (i != -10 && t45Var.e() == null && t45Var.g() == null) {
            this.a.w().s().a("Discarding empty consent settings");
            return;
        }
        synchronized (this.h) {
            z = true;
            z2 = false;
            if (t45.m(i, this.j)) {
                boolean i2 = t45Var.i(this.i);
                if (t45Var.h() && !this.i.h()) {
                    z2 = true;
                }
                t45 l = t45Var.l(this.i);
                this.i = l;
                this.j = i;
                t45Var2 = l;
                z3 = z2;
                z2 = i2;
            } else {
                t45Var2 = t45Var;
                z3 = false;
                z = false;
            }
        }
        if (!z) {
            this.a.w().t().b("Ignoring lower-priority consent settings, proposed settings", t45Var2);
            return;
        }
        long andIncrement = this.k.getAndIncrement();
        if (z2) {
            this.g.set(null);
            this.a.q().s(new qo5(this, t45Var2, j, i, andIncrement, z3));
        } else if (i != 30 && i != -10) {
            this.a.q().p(new vo5(this, t45Var2, i, andIncrement, z3));
        } else {
            this.a.q().s(new to5(this, t45Var2, i, andIncrement, z3));
        }
    }

    public final void W(t45 t45Var) {
        e();
        boolean z = (t45Var.h() && t45Var.f()) || this.a.R().u();
        if (z != this.a.k()) {
            this.a.j(z);
            mi5 A = this.a.A();
            ck5 ck5Var = A.a;
            A.e();
            Boolean valueOf = A.n().contains("measurement_enabled_from_api") ? Boolean.valueOf(A.n().getBoolean("measurement_enabled_from_api", true)) : null;
            if (!z || valueOf == null || valueOf.booleanValue()) {
                L(Boolean.valueOf(z), false);
            }
        }
    }

    public final void X(String str, String str2, Bundle bundle) {
        a0(str, str2, bundle, true, true, this.a.a().a());
    }

    public final void Y(String str, String str2, long j, Bundle bundle) {
        e();
        Z(str, str2, j, bundle, true, this.d == null || sw5.F(str2), false, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:129:0x041f  */
    /* JADX WARN: Removed duplicated region for block: B:132:0x044d  */
    /* JADX WARN: Removed duplicated region for block: B:150:0x04a0  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void Z(java.lang.String r21, java.lang.String r22, long r23, android.os.Bundle r25, boolean r26, boolean r27, boolean r28, java.lang.String r29) {
        /*
            Method dump skipped, instructions count: 1350
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dp5.Z(java.lang.String, java.lang.String, long, android.os.Bundle, boolean, boolean, boolean, java.lang.String):void");
    }

    public final void a0(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        String str3 = str == null ? "app" : str;
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        if (this.a.z().v(null, qf5.s0) && sw5.G(str2, "screen_view")) {
            this.a.Q().t(bundle2, j);
            return;
        }
        b0(str3, str2, j, bundle2, z2, !z2 || this.d == null || sw5.F(str2), !z, null);
    }

    public final void b0(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        Bundle bundle2 = new Bundle(bundle);
        for (String str4 : bundle2.keySet()) {
            Object obj = bundle2.get(str4);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str4, new Bundle((Bundle) obj));
            } else {
                int i = 0;
                if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    while (i < parcelableArr.length) {
                        Parcelable parcelable = parcelableArr[i];
                        if (parcelable instanceof Bundle) {
                            parcelableArr[i] = new Bundle((Bundle) parcelable);
                        }
                        i++;
                    }
                } else if (obj instanceof List) {
                    List list = (List) obj;
                    while (i < list.size()) {
                        Object obj2 = list.get(i);
                        if (obj2 instanceof Bundle) {
                            list.set(i, new Bundle((Bundle) obj2));
                        }
                        i++;
                    }
                }
            }
        }
        this.a.q().p(new gn5(this, str, str2, j, bundle2, z, z2, z3, str3));
    }

    public final void c0(String str, String str2, Object obj, boolean z) {
        d0("auto", str2, obj, true, this.a.a().a());
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x004a  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x007b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void d0(java.lang.String r18, java.lang.String r19, java.lang.Object r20, boolean r21, long r22) {
        /*
            r17 = this;
            r6 = r17
            r2 = r19
            r0 = r20
            if (r18 != 0) goto Lb
            java.lang.String r1 = "app"
            goto Ld
        Lb:
            r1 = r18
        Ld:
            r3 = 6
            r4 = 0
            r5 = 24
            r7 = 0
            if (r21 == 0) goto L20
            ck5 r3 = r6.a
            sw5 r3 = r3.G()
            int r3 = r3.q0(r2)
        L1e:
            r12 = r3
            goto L47
        L20:
            ck5 r8 = r6.a
            sw5 r8 = r8.G()
            java.lang.String r9 = "user property"
            boolean r10 = r8.l0(r9, r2)
            if (r10 != 0) goto L2f
        L2e:
            goto L1e
        L2f:
            java.lang.String[] r10 = defpackage.bm5.a
            boolean r10 = r8.n0(r9, r10, r7, r2)
            if (r10 != 0) goto L3a
            r3 = 15
            goto L1e
        L3a:
            ck5 r10 = r8.a
            r10.z()
            boolean r8 = r8.o0(r9, r5, r2)
            if (r8 != 0) goto L46
            goto L2e
        L46:
            r12 = r4
        L47:
            r3 = 1
            if (r12 == 0) goto L7b
            ck5 r0 = r6.a
            sw5 r0 = r0.G()
            ck5 r1 = r6.a
            r1.z()
            java.lang.String r14 = r0.o(r2, r5, r3)
            if (r2 == 0) goto L5f
            int r4 = r19.length()
        L5f:
            r15 = r4
            ck5 r0 = r6.a
            sw5 r9 = r0.G()
            pw5 r10 = r6.p
            r11 = 0
            ck5 r0 = r6.a
            q45 r0 = r0.z()
            we5<java.lang.Boolean> r1 = defpackage.qf5.w0
            boolean r16 = r0.v(r7, r1)
            java.lang.String r13 = "_ev"
            r9.A(r10, r11, r12, r13, r14, r15, r16)
            return
        L7b:
            if (r0 == 0) goto Lda
            ck5 r8 = r6.a
            sw5 r8 = r8.G()
            int r12 = r8.x(r2, r0)
            if (r12 == 0) goto Lc4
            ck5 r1 = r6.a
            sw5 r1 = r1.G()
            ck5 r8 = r6.a
            r8.z()
            java.lang.String r14 = r1.o(r2, r5, r3)
            boolean r1 = r0 instanceof java.lang.String
            if (r1 != 0) goto La0
            boolean r1 = r0 instanceof java.lang.CharSequence
            if (r1 == 0) goto La8
        La0:
            java.lang.String r0 = java.lang.String.valueOf(r20)
            int r4 = r0.length()
        La8:
            r15 = r4
            ck5 r0 = r6.a
            sw5 r9 = r0.G()
            pw5 r10 = r6.p
            r11 = 0
            ck5 r0 = r6.a
            q45 r0 = r0.z()
            we5<java.lang.Boolean> r1 = defpackage.qf5.w0
            boolean r16 = r0.v(r7, r1)
            java.lang.String r13 = "_ev"
            r9.A(r10, r11, r12, r13, r14, r15, r16)
            return
        Lc4:
            ck5 r3 = r6.a
            sw5 r3 = r3.G()
            java.lang.Object r5 = r3.y(r2, r0)
            if (r5 == 0) goto Ld9
            r0 = r17
            r2 = r19
            r3 = r22
            r0.l(r1, r2, r3, r5)
        Ld9:
            return
        Lda:
            r5 = 0
            r0 = r17
            r2 = r19
            r3 = r22
            r0.l(r1, r2, r3, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dp5.d0(java.lang.String, java.lang.String, java.lang.Object, boolean, long):void");
    }

    @Override // defpackage.vh5
    public final boolean j() {
        return false;
    }

    public final void l(String str, String str2, long j, Object obj) {
        this.a.q().p(new jn5(this, str, str2, obj, j));
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x006e  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x007e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void n(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            defpackage.zt2.f(r9)
            defpackage.zt2.f(r10)
            r8.e()
            r8.g()
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            java.lang.String r1 = "_npa"
            if (r0 == 0) goto L64
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L52
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L52
            r10 = 1
            java.util.Locale r11 = java.util.Locale.ENGLISH
            java.lang.String r11 = r0.toLowerCase(r11)
            java.lang.String r0 = "false"
            boolean r11 = r0.equals(r11)
            r2 = 1
            if (r10 == r11) goto L37
            r10 = 0
            goto L38
        L37:
            r10 = r2
        L38:
            java.lang.Long r11 = java.lang.Long.valueOf(r10)
            ck5 r10 = r8.a
            mi5 r10 = r10.A()
            ji5 r10 = r10.m
            long r4 = r11.longValue()
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 != 0) goto L4e
            java.lang.String r0 = "true"
        L4e:
            r10.b(r0)
            goto L61
        L52:
            if (r11 != 0) goto L64
            ck5 r10 = r8.a
            mi5 r10 = r10.A()
            ji5 r10 = r10.m
            java.lang.String r0 = "unset"
            r10.b(r0)
        L61:
            r6 = r11
            r3 = r1
            goto L66
        L64:
            r3 = r10
            r6 = r11
        L66:
            ck5 r10 = r8.a
            boolean r10 = r10.h()
            if (r10 != 0) goto L7e
            ck5 r9 = r8.a
            og5 r9 = r9.w()
            jg5 r9 = r9.v()
            java.lang.String r10 = "User property not set since app measurement is disabled"
            r9.a(r10)
            return
        L7e:
            ck5 r10 = r8.a
            boolean r10 = r10.o()
            if (r10 != 0) goto L87
            return
        L87:
            com.google.android.gms.measurement.internal.zzkq r10 = new com.google.android.gms.measurement.internal.zzkq
            r2 = r10
            r4 = r12
            r7 = r9
            r2.<init>(r3, r4, r6, r7)
            ck5 r9 = r8.a
            com.google.android.gms.measurement.internal.p r9 = r9.R()
            r9.R(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dp5.n(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    public final String o() {
        return this.g.get();
    }

    public final void p(String str) {
        this.g.set(str);
    }

    public final void r(long j) {
        this.g.set(null);
        this.a.q().p(new mn5(this, j));
    }

    public final void s(long j, boolean z) {
        e();
        g();
        this.a.w().u().a("Resetting analytics data (FE)");
        qu5 C = this.a.C();
        C.e();
        C.e.c();
        boolean h = this.a.h();
        mi5 A = this.a.A();
        A.e.b(j);
        if (!TextUtils.isEmpty(A.a.A().t.a())) {
            A.t.b(null);
        }
        q16.a();
        q45 z2 = A.a.z();
        we5<Boolean> we5Var = qf5.o0;
        if (z2.v(null, we5Var)) {
            A.o.b(0L);
        }
        if (!A.a.z().A()) {
            A.t(!h);
        }
        A.u.b(null);
        A.v.b(0L);
        A.w.b(null);
        if (z) {
            this.a.R().S();
        }
        q16.a();
        if (this.a.z().v(null, we5Var)) {
            this.a.C().d.a();
        }
        this.o = !h;
    }

    public final void t() {
        e();
        g();
        if (this.a.o()) {
            if (this.a.z().v(null, qf5.b0)) {
                q45 z = this.a.z();
                z.a.b();
                Boolean y = z.y("google_analytics_deferred_deep_link_enabled");
                if (y != null && y.booleanValue()) {
                    this.a.w().u().a("Deferred Deep Link feature enabled.");
                    this.a.q().p(new Runnable(this) { // from class: lm5
                        public final dp5 a;

                        {
                            this.a = this;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            dp5 dp5Var = this.a;
                            dp5Var.e();
                            if (!dp5Var.a.A().r.a()) {
                                long a = dp5Var.a.A().s.a();
                                dp5Var.a.A().s.b(1 + a);
                                dp5Var.a.z();
                                if (a >= 5) {
                                    dp5Var.a.w().p().a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
                                    dp5Var.a.A().r.b(true);
                                    return;
                                }
                                dp5Var.a.p();
                                return;
                            }
                            dp5Var.a.w().u().a("Deferred Deep Link already retrieved. Not fetching again.");
                        }
                    });
                }
            }
            this.a.R().V();
            this.o = false;
            mi5 A = this.a.A();
            A.e();
            String string = A.n().getString("previous_os_version", null);
            A.a.S().i();
            String str = Build.VERSION.RELEASE;
            if (!TextUtils.isEmpty(str) && !str.equals(string)) {
                SharedPreferences.Editor edit = A.n().edit();
                edit.putString("previous_os_version", str);
                edit.apply();
            }
            if (TextUtils.isEmpty(string)) {
                return;
            }
            this.a.S().i();
            if (string.equals(str)) {
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("_po", string);
            X("auto", "_ou", bundle);
        }
    }

    public final void u(cm5 cm5Var) {
        cm5 cm5Var2;
        e();
        g();
        if (cm5Var != null && cm5Var != (cm5Var2 = this.d)) {
            zt2.n(cm5Var2 == null, "EventInterceptor already set.");
        }
        this.d = cm5Var;
    }

    public final void v(em5 em5Var) {
        g();
        zt2.j(em5Var);
        if (this.e.add(em5Var)) {
            return;
        }
        this.a.w().p().a("OnEventListener already registered");
    }

    public final void x(em5 em5Var) {
        g();
        zt2.j(em5Var);
        if (this.e.remove(em5Var)) {
            return;
        }
        this.a.w().p().a("OnEventListener had not been registered");
    }

    public final int y(String str) {
        zt2.f(str);
        this.a.z();
        return 25;
    }

    public final void z(Bundle bundle) {
        A(bundle, this.a.a().a());
    }
}
