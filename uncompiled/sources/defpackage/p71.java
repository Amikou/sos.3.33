package defpackage;

import kotlin.jvm.internal.Ref$IntRef;

/* compiled from: Limit.kt */
/* renamed from: p71  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class p71 {

    /* compiled from: SafeCollector.common.kt */
    /* renamed from: p71$a */
    /* loaded from: classes2.dex */
    public static final class a implements j71<T> {
        public final /* synthetic */ j71 a;
        public final /* synthetic */ int f0;

        public a(j71 j71Var, int i) {
            this.a = j71Var;
            this.f0 = i;
        }

        @Override // defpackage.j71
        public Object a(k71<? super T> k71Var, q70<? super te4> q70Var) {
            Object a = this.a.a(new b(new Ref$IntRef(), this.f0, k71Var), q70Var);
            return a == gs1.d() ? a : te4.a;
        }
    }

    /* compiled from: Collect.kt */
    /* renamed from: p71$b */
    /* loaded from: classes2.dex */
    public static final class b implements k71<T> {
        public final /* synthetic */ Ref$IntRef a;
        public final /* synthetic */ int f0;
        public final /* synthetic */ k71 g0;

        public b(Ref$IntRef ref$IntRef, int i, k71 k71Var) {
            this.a = ref$IntRef;
            this.f0 = i;
            this.g0 = k71Var;
        }

        @Override // defpackage.k71
        public Object emit(T t, q70<? super te4> q70Var) {
            Ref$IntRef ref$IntRef = this.a;
            int i = ref$IntRef.element;
            if (i >= this.f0) {
                Object emit = this.g0.emit(t, q70Var);
                if (emit == gs1.d()) {
                    return emit;
                }
            } else {
                ref$IntRef.element = i + 1;
            }
            return te4.a;
        }
    }

    public static final <T> j71<T> a(j71<? extends T> j71Var, int i) {
        if (i >= 0) {
            return new a(j71Var, i);
        }
        throw new IllegalArgumentException(fs1.l("Drop count should be non-negative, but had ", Integer.valueOf(i)).toString());
    }
}
