package defpackage;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.Choreographer;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import androidx.media3.exoplayer.video.PlaceholderSurface;
import com.github.mikephil.charting.utils.Utils;
import zendesk.support.request.CellBase;

/* compiled from: VideoFrameReleaseHelper.java */
/* renamed from: nh4  reason: default package */
/* loaded from: classes.dex */
public final class nh4 {
    public final d61 a = new d61();
    public final b b;
    public final e c;
    public boolean d;
    public Surface e;
    public float f;
    public float g;
    public float h;
    public float i;
    public int j;
    public long k;
    public long l;
    public long m;
    public long n;
    public long o;
    public long p;
    public long q;

    /* compiled from: VideoFrameReleaseHelper.java */
    /* renamed from: nh4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static void a(Surface surface, float f) {
            try {
                surface.setFrameRate(f, f == Utils.FLOAT_EPSILON ? 0 : 1);
            } catch (IllegalStateException e) {
                p12.d("VideoFrameReleaseHelper", "Failed to call Surface.setFrameRate", e);
            }
        }
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* renamed from: nh4$b */
    /* loaded from: classes.dex */
    public interface b {

        /* compiled from: VideoFrameReleaseHelper.java */
        /* renamed from: nh4$b$a */
        /* loaded from: classes.dex */
        public interface a {
            void a(Display display);
        }

        void a(a aVar);

        void unregister();
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* renamed from: nh4$c */
    /* loaded from: classes.dex */
    public static final class c implements b {
        public final WindowManager a;

        public c(WindowManager windowManager) {
            this.a = windowManager;
        }

        public static b b(Context context) {
            WindowManager windowManager = (WindowManager) context.getSystemService("window");
            if (windowManager != null) {
                return new c(windowManager);
            }
            return null;
        }

        @Override // defpackage.nh4.b
        public void a(b.a aVar) {
            aVar.a(this.a.getDefaultDisplay());
        }

        @Override // defpackage.nh4.b
        public void unregister() {
        }
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* renamed from: nh4$d */
    /* loaded from: classes.dex */
    public static final class d implements b, DisplayManager.DisplayListener {
        public final DisplayManager a;
        public b.a b;

        public d(DisplayManager displayManager) {
            this.a = displayManager;
        }

        public static b c(Context context) {
            DisplayManager displayManager = (DisplayManager) context.getSystemService("display");
            if (displayManager != null) {
                return new d(displayManager);
            }
            return null;
        }

        @Override // defpackage.nh4.b
        public void a(b.a aVar) {
            this.b = aVar;
            this.a.registerDisplayListener(this, androidx.media3.common.util.b.v());
            aVar.a(b());
        }

        public final Display b() {
            return this.a.getDisplay(0);
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayAdded(int i) {
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayChanged(int i) {
            b.a aVar = this.b;
            if (aVar == null || i != 0) {
                return;
            }
            aVar.a(b());
        }

        @Override // android.hardware.display.DisplayManager.DisplayListener
        public void onDisplayRemoved(int i) {
        }

        @Override // defpackage.nh4.b
        public void unregister() {
            this.a.unregisterDisplayListener(this);
            this.b = null;
        }
    }

    /* compiled from: VideoFrameReleaseHelper.java */
    /* renamed from: nh4$e */
    /* loaded from: classes.dex */
    public static final class e implements Choreographer.FrameCallback, Handler.Callback {
        public static final e j0 = new e();
        public volatile long a = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        public final Handler f0;
        public final HandlerThread g0;
        public Choreographer h0;
        public int i0;

        public e() {
            HandlerThread handlerThread = new HandlerThread("ExoPlayer:FrameReleaseChoreographer");
            this.g0 = handlerThread;
            handlerThread.start();
            Handler u = androidx.media3.common.util.b.u(handlerThread.getLooper(), this);
            this.f0 = u;
            u.sendEmptyMessage(0);
        }

        public static e d() {
            return j0;
        }

        public void a() {
            this.f0.sendEmptyMessage(1);
        }

        public final void b() {
            Choreographer choreographer = this.h0;
            if (choreographer != null) {
                int i = this.i0 + 1;
                this.i0 = i;
                if (i == 1) {
                    choreographer.postFrameCallback(this);
                }
            }
        }

        public final void c() {
            try {
                this.h0 = Choreographer.getInstance();
            } catch (RuntimeException e) {
                p12.j("VideoFrameReleaseHelper", "Vsync sampling disabled due to platform error", e);
            }
        }

        @Override // android.view.Choreographer.FrameCallback
        public void doFrame(long j) {
            this.a = j;
            ((Choreographer) ii.e(this.h0)).postFrameCallbackDelayed(this, 500L);
        }

        public void e() {
            this.f0.sendEmptyMessage(2);
        }

        public final void f() {
            Choreographer choreographer = this.h0;
            if (choreographer != null) {
                int i = this.i0 - 1;
                this.i0 = i;
                if (i == 0) {
                    choreographer.removeFrameCallback(this);
                    this.a = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
                }
            }
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                c();
                return true;
            } else if (i == 1) {
                b();
                return true;
            } else if (i != 2) {
                return false;
            } else {
                f();
                return true;
            }
        }
    }

    public nh4(Context context) {
        b f = f(context);
        this.b = f;
        this.c = f != null ? e.d() : null;
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.f = -1.0f;
        this.i = 1.0f;
        this.j = 0;
    }

    public static boolean c(long j, long j2) {
        return Math.abs(j - j2) <= 20000000;
    }

    public static long e(long j, long j2, long j3) {
        long j4;
        long j5 = j2 + (((j - j2) / j3) * j3);
        if (j <= j5) {
            j4 = j5 - j3;
        } else {
            j5 = j3 + j5;
            j4 = j5;
        }
        return j5 - j < j - j4 ? j5 : j4;
    }

    public static b f(Context context) {
        if (context != null) {
            Context applicationContext = context.getApplicationContext();
            b c2 = androidx.media3.common.util.b.a >= 17 ? d.c(applicationContext) : null;
            return c2 == null ? c.b(applicationContext) : c2;
        }
        return null;
    }

    public long b(long j) {
        long j2;
        e eVar;
        if (this.p != -1 && this.a.e()) {
            long a2 = this.q + (((float) (this.a.a() * (this.m - this.p))) / this.i);
            if (!c(j, a2)) {
                n();
            } else {
                j2 = a2;
                this.n = this.m;
                this.o = j2;
                eVar = this.c;
                if (eVar != null || this.k == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return j2;
                }
                long j3 = eVar.a;
                return j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? j2 : e(j2, j3, this.k) - this.l;
            }
        }
        j2 = j;
        this.n = this.m;
        this.o = j2;
        eVar = this.c;
        if (eVar != null) {
        }
        return j2;
    }

    public final void d() {
        Surface surface;
        if (androidx.media3.common.util.b.a < 30 || (surface = this.e) == null || this.j == Integer.MIN_VALUE || this.h == Utils.FLOAT_EPSILON) {
            return;
        }
        this.h = Utils.FLOAT_EPSILON;
        a.a(surface, Utils.FLOAT_EPSILON);
    }

    public void g(float f) {
        this.f = f;
        this.a.g();
        q();
    }

    public void h(long j) {
        long j2 = this.n;
        if (j2 != -1) {
            this.p = j2;
            this.q = this.o;
        }
        this.m++;
        this.a.f(j * 1000);
        q();
    }

    public void i(float f) {
        this.i = f;
        n();
        r(false);
    }

    public void j() {
        n();
    }

    public void k() {
        this.d = true;
        n();
        if (this.b != null) {
            ((e) ii.e(this.c)).a();
            this.b.a(new b.a() { // from class: mh4
                @Override // defpackage.nh4.b.a
                public final void a(Display display) {
                    nh4.this.p(display);
                }
            });
        }
        r(false);
    }

    public void l() {
        this.d = false;
        b bVar = this.b;
        if (bVar != null) {
            bVar.unregister();
            ((e) ii.e(this.c)).e();
        }
        d();
    }

    public void m(Surface surface) {
        if (surface instanceof PlaceholderSurface) {
            surface = null;
        }
        if (this.e == surface) {
            return;
        }
        d();
        this.e = surface;
        r(true);
    }

    public final void n() {
        this.m = 0L;
        this.p = -1L;
        this.n = -1L;
    }

    public void o(int i) {
        if (this.j == i) {
            return;
        }
        this.j = i;
        r(true);
    }

    public final void p(Display display) {
        if (display != null) {
            long refreshRate = (long) (1.0E9d / display.getRefreshRate());
            this.k = refreshRate;
            this.l = (refreshRate * 80) / 100;
            return;
        }
        p12.i("VideoFrameReleaseHelper", "Unable to query display refresh rate");
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    /* JADX WARN: Code restructure failed: missing block: B:28:0x005c, code lost:
        if (java.lang.Math.abs(r0 - r7.g) >= (r7.a.e() && (r7.a.d() > 5000000000L ? 1 : (r7.a.d() == 5000000000L ? 0 : -1)) >= 0 ? 0.02f : 1.0f)) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x005f, code lost:
        r6 = false;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x006a, code lost:
        if (r7.a.c() >= 30) goto L26;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void q() {
        /*
            r7 = this;
            int r0 = androidx.media3.common.util.b.a
            r1 = 30
            if (r0 < r1) goto L73
            android.view.Surface r0 = r7.e
            if (r0 != 0) goto Lc
            goto L73
        Lc:
            d61 r0 = r7.a
            boolean r0 = r0.e()
            if (r0 == 0) goto L1b
            d61 r0 = r7.a
            float r0 = r0.b()
            goto L1d
        L1b:
            float r0 = r7.f
        L1d:
            float r2 = r7.g
            int r3 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r3 != 0) goto L24
            return
        L24:
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            int r4 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            r5 = 0
            r6 = 1
            if (r4 == 0) goto L61
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 == 0) goto L61
            d61 r1 = r7.a
            boolean r1 = r1.e()
            if (r1 == 0) goto L49
            d61 r1 = r7.a
            long r1 = r1.d()
            r3 = 5000000000(0x12a05f200, double:2.470328229E-314)
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 < 0) goto L49
            r1 = r6
            goto L4a
        L49:
            r1 = r5
        L4a:
            if (r1 == 0) goto L50
            r1 = 1017370378(0x3ca3d70a, float:0.02)
            goto L52
        L50:
            r1 = 1065353216(0x3f800000, float:1.0)
        L52:
            float r2 = r7.g
            float r2 = r0 - r2
            float r2 = java.lang.Math.abs(r2)
            int r1 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r1 < 0) goto L5f
            goto L6c
        L5f:
            r6 = r5
            goto L6c
        L61:
            if (r4 == 0) goto L64
            goto L6c
        L64:
            d61 r2 = r7.a
            int r2 = r2.c()
            if (r2 < r1) goto L5f
        L6c:
            if (r6 == 0) goto L73
            r7.g = r0
            r7.r(r5)
        L73:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.nh4.q():void");
    }

    public final void r(boolean z) {
        Surface surface;
        if (androidx.media3.common.util.b.a < 30 || (surface = this.e) == null || this.j == Integer.MIN_VALUE) {
            return;
        }
        float f = Utils.FLOAT_EPSILON;
        if (this.d) {
            float f2 = this.g;
            if (f2 != -1.0f) {
                f = this.i * f2;
            }
        }
        if (z || this.h != f) {
            this.h = f;
            a.a(surface, f);
        }
    }
}
