package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: v25  reason: default package */
/* loaded from: classes.dex */
public final class v25 implements u05 {
    public final /* synthetic */ t25 a;

    public v25(t25 t25Var) {
        this.a = t25Var;
    }

    @Override // defpackage.u05
    public final void a(ConnectionResult connectionResult) {
        t25.h(this.a).lock();
        try {
            t25.b(this.a, connectionResult);
            t25.q(this.a);
        } finally {
            t25.h(this.a).unlock();
        }
    }

    @Override // defpackage.u05
    public final void b(int i, boolean z) {
        t25.h(this.a).lock();
        try {
            if (!t25.t(this.a) && t25.u(this.a) != null && t25.u(this.a).M1()) {
                t25.o(this.a, true);
                t25.v(this.a).onConnectionSuspended(i);
                return;
            }
            t25.o(this.a, false);
            t25.m(this.a, i, z);
        } finally {
            t25.h(this.a).unlock();
        }
    }

    @Override // defpackage.u05
    public final void d(Bundle bundle) {
        t25.h(this.a).lock();
        try {
            t25.n(this.a, bundle);
            t25.b(this.a, ConnectionResult.i0);
            t25.q(this.a);
        } finally {
            t25.h(this.a).unlock();
        }
    }

    public /* synthetic */ v25(t25 t25Var, x25 x25Var) {
        this(t25Var);
    }
}
