package defpackage;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: GetConcatCarouselDataUseCase.kt */
/* renamed from: if1  reason: default package */
/* loaded from: classes2.dex */
public final class if1 implements bm1 {
    public List<? extends IContact> a;

    /* compiled from: Comparisons.kt */
    /* renamed from: if1$a */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(((IContact) t).getName(), ((IContact) t2).getName());
        }
    }

    @Override // defpackage.bm1
    public s60 a(List<? extends IContact> list) {
        fs1.f(list, "origin");
        if (list.isEmpty()) {
            return new s60(0, list);
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (hashSet.add(((IContact) obj).getAddress())) {
                arrayList.add(obj);
            }
        }
        List<? extends IContact> e0 = j20.e0(arrayList, new a());
        this.a = e0;
        List<? extends IContact> list2 = null;
        if (e0 == null) {
            fs1.r("sortedContactList");
            e0 = null;
        }
        IContact b = b(e0);
        if (b == null) {
            return new s60(0, list);
        }
        List<? extends IContact> list3 = this.a;
        if (list3 == null) {
            fs1.r("sortedContactList");
        } else {
            list2 = list3;
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj2 : list2) {
            if (((IContact) obj2).getId() != b.getId()) {
                arrayList2.add(obj2);
            }
        }
        LinkedList linkedList = new LinkedList(arrayList2);
        int size = linkedList.size() / 2;
        linkedList.add(size, b);
        return new s60(size, linkedList);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0040, code lost:
        if (r5 < (r7 == null ? 0 : r7.longValue())) goto L37;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final net.safemoon.androidwallet.model.contact.abstraction.IContact b(java.util.List<? extends net.safemoon.androidwallet.model.contact.abstraction.IContact> r10) {
        /*
            r9 = this;
            int r0 = r10.size()
            r1 = 1
            if (r0 > r1) goto Lf
            r0 = 0
            java.lang.Object r10 = r10.get(r0)
            net.safemoon.androidwallet.model.contact.abstraction.IContact r10 = (net.safemoon.androidwallet.model.contact.abstraction.IContact) r10
            return r10
        Lf:
            java.util.Iterator r10 = r10.iterator()
            r0 = 0
            r1 = r0
        L15:
            boolean r2 = r10.hasNext()
            r3 = 0
            if (r2 == 0) goto L63
            java.lang.Object r2 = r10.next()
            net.safemoon.androidwallet.model.contact.abstraction.IContact r2 = (net.safemoon.androidwallet.model.contact.abstraction.IContact) r2
            if (r0 != 0) goto L26
            goto L42
        L26:
            java.lang.Long r5 = r0.getContactCreate()
            if (r5 != 0) goto L2e
            r5 = r3
            goto L32
        L2e:
            long r5 = r5.longValue()
        L32:
            java.lang.Long r7 = r2.getContactCreate()
            if (r7 != 0) goto L3a
            r7 = r3
            goto L3e
        L3a:
            long r7 = r7.longValue()
        L3e:
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 >= 0) goto L43
        L42:
            r0 = r2
        L43:
            if (r1 != 0) goto L46
            goto L61
        L46:
            java.lang.Long r5 = r1.getLastSent()
            if (r5 != 0) goto L4e
            r5 = r3
            goto L52
        L4e:
            long r5 = r5.longValue()
        L52:
            java.lang.Long r7 = r2.getLastSent()
            if (r7 != 0) goto L59
            goto L5d
        L59:
            long r3 = r7.longValue()
        L5d:
            int r3 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r3 >= 0) goto L15
        L61:
            r1 = r2
            goto L15
        L63:
            boolean r10 = defpackage.fs1.b(r0, r1)
            if (r10 == 0) goto L6a
            goto L8d
        L6a:
            if (r0 != 0) goto L6e
        L6c:
            r5 = r3
            goto L79
        L6e:
            java.lang.Long r10 = r0.getContactCreate()
            if (r10 != 0) goto L75
            goto L6c
        L75:
            long r5 = r10.longValue()
        L79:
            if (r1 != 0) goto L7c
            goto L87
        L7c:
            java.lang.Long r10 = r1.getLastSent()
            if (r10 != 0) goto L83
            goto L87
        L83:
            long r3 = r10.longValue()
        L87:
            int r10 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r10 <= 0) goto L8c
            goto L8d
        L8c:
            r0 = r1
        L8d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.if1.b(java.util.List):net.safemoon.androidwallet.model.contact.abstraction.IContact");
    }
}
