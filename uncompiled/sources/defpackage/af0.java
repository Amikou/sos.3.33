package defpackage;

import java.lang.reflect.Field;
import kotlin.coroutines.jvm.internal.BaseContinuationImpl;
import kotlin.coroutines.jvm.internal.a;

/* compiled from: DebugMetadata.kt */
/* renamed from: af0  reason: default package */
/* loaded from: classes2.dex */
public final class af0 {
    public static final void a(int i, int i2) {
        if (i2 <= i) {
            return;
        }
        throw new IllegalStateException(("Debug metadata version mismatch. Expected: " + i + ", got " + i2 + ". Please update the Kotlin standard library.").toString());
    }

    public static final a b(BaseContinuationImpl baseContinuationImpl) {
        return (a) baseContinuationImpl.getClass().getAnnotation(a.class);
    }

    public static final int c(BaseContinuationImpl baseContinuationImpl) {
        try {
            Field declaredField = baseContinuationImpl.getClass().getDeclaredField("label");
            fs1.e(declaredField, "field");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(baseContinuationImpl);
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            return (num != null ? num.intValue() : 0) - 1;
        } catch (Exception unused) {
            return -1;
        }
    }

    public static final StackTraceElement d(BaseContinuationImpl baseContinuationImpl) {
        String str;
        fs1.f(baseContinuationImpl, "$this$getStackTraceElementImpl");
        a b = b(baseContinuationImpl);
        if (b != null) {
            a(1, b.v());
            int c = c(baseContinuationImpl);
            int i = c < 0 ? -1 : b.l()[c];
            String b2 = n92.c.b(baseContinuationImpl);
            if (b2 == null) {
                str = b.c();
            } else {
                str = b2 + '/' + b.c();
            }
            return new StackTraceElement(str, b.m(), b.f(), i);
        }
        return null;
    }
}
