package defpackage;

import android.os.Bundle;
import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: bw5  reason: default package */
/* loaded from: classes.dex */
public final class bw5 implements pw5 {
    public final /* synthetic */ fw5 a;

    public bw5(fw5 fw5Var) {
        this.a = fw5Var;
    }

    @Override // defpackage.pw5
    public final void i(String str, String str2, Bundle bundle) {
        ck5 ck5Var;
        ck5 ck5Var2;
        if (!TextUtils.isEmpty(str)) {
            this.a.q().p(new yv5(this, str, "_err", bundle));
            return;
        }
        ck5Var = this.a.k;
        if (ck5Var != null) {
            ck5Var2 = this.a.k;
            ck5Var2.w().l().b("AppId not known when logging event", "_err");
        }
    }
}
