package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: HolderRecoveryWalletBinding.java */
/* renamed from: wk1  reason: default package */
/* loaded from: classes2.dex */
public final class wk1 {
    public final AppCompatImageView a;
    public final AppCompatImageView b;
    public final ConstraintLayout c;
    public final TextView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;
    public final TextView h;
    public final TextView i;
    public final TextView j;

    public wk1(LinearLayoutCompat linearLayoutCompat, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, TextView textView9) {
        this.a = appCompatImageView;
        this.b = appCompatImageView2;
        this.c = constraintLayout2;
        this.d = textView;
        this.e = textView2;
        this.f = textView4;
        this.g = textView5;
        this.h = textView7;
        this.i = textView8;
        this.j = textView9;
    }

    public static wk1 a(View view) {
        int i = R.id.imPrivateKeyDropdown;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imPrivateKeyDropdown);
        if (appCompatImageView != null) {
            i = R.id.imRecoveryPhraseDropdown;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imRecoveryPhraseDropdown);
            if (appCompatImageView2 != null) {
                i = R.id.layoutPrivateKey;
                ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.layoutPrivateKey);
                if (constraintLayout != null) {
                    i = R.id.layoutRecoveryPhase;
                    ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.layoutRecoveryPhase);
                    if (constraintLayout2 != null) {
                        i = R.id.tvExistingWalletsHeader;
                        TextView textView = (TextView) ai4.a(view, R.id.tvExistingWalletsHeader);
                        if (textView != null) {
                            i = R.id.tvPrivateKeyContent;
                            TextView textView2 = (TextView) ai4.a(view, R.id.tvPrivateKeyContent);
                            if (textView2 != null) {
                                i = R.id.tvPrivateKeyHeader;
                                TextView textView3 = (TextView) ai4.a(view, R.id.tvPrivateKeyHeader);
                                if (textView3 != null) {
                                    i = R.id.tvPrivateKeyNotice;
                                    TextView textView4 = (TextView) ai4.a(view, R.id.tvPrivateKeyNotice);
                                    if (textView4 != null) {
                                        i = R.id.tvRecoveryPhraseContent;
                                        TextView textView5 = (TextView) ai4.a(view, R.id.tvRecoveryPhraseContent);
                                        if (textView5 != null) {
                                            i = R.id.tvRecoveryPhraseHeader;
                                            TextView textView6 = (TextView) ai4.a(view, R.id.tvRecoveryPhraseHeader);
                                            if (textView6 != null) {
                                                i = R.id.tvRecoveryPhraseNotice;
                                                TextView textView7 = (TextView) ai4.a(view, R.id.tvRecoveryPhraseNotice);
                                                if (textView7 != null) {
                                                    i = R.id.tvRevealPrivateKey;
                                                    TextView textView8 = (TextView) ai4.a(view, R.id.tvRevealPrivateKey);
                                                    if (textView8 != null) {
                                                        i = R.id.tvRevealRecoveryPhrase;
                                                        TextView textView9 = (TextView) ai4.a(view, R.id.tvRevealRecoveryPhrase);
                                                        if (textView9 != null) {
                                                            return new wk1((LinearLayoutCompat) view, appCompatImageView, appCompatImageView2, constraintLayout, constraintLayout2, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
