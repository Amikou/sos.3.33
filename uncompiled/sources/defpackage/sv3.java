package defpackage;

import androidx.media3.common.j;

/* compiled from: SubtitleDecoderFactory.java */
/* renamed from: sv3  reason: default package */
/* loaded from: classes.dex */
public interface sv3 {
    public static final sv3 a = new a();

    /* compiled from: SubtitleDecoderFactory.java */
    /* renamed from: sv3$a */
    /* loaded from: classes.dex */
    public class a implements sv3 {
        @Override // defpackage.sv3
        public boolean a(j jVar) {
            String str = jVar.p0;
            return "text/vtt".equals(str) || "text/x-ssa".equals(str) || "application/ttml+xml".equals(str) || "application/x-mp4-vtt".equals(str) || "application/x-subrip".equals(str) || "application/x-quicktime-tx3g".equals(str) || "application/cea-608".equals(str) || "application/x-mp4-cea-608".equals(str) || "application/cea-708".equals(str) || "application/dvbsubs".equals(str) || "application/pgs".equals(str) || "text/x-exoplayer-cues".equals(str);
        }

        @Override // defpackage.sv3
        public rv3 b(j jVar) {
            String str = jVar.p0;
            if (str != null) {
                char c = 65535;
                switch (str.hashCode()) {
                    case -1351681404:
                        if (str.equals("application/dvbsubs")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -1248334819:
                        if (str.equals("application/pgs")) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1026075066:
                        if (str.equals("application/x-mp4-vtt")) {
                            c = 2;
                            break;
                        }
                        break;
                    case -1004728940:
                        if (str.equals("text/vtt")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 691401887:
                        if (str.equals("application/x-quicktime-tx3g")) {
                            c = 4;
                            break;
                        }
                        break;
                    case 822864842:
                        if (str.equals("text/x-ssa")) {
                            c = 5;
                            break;
                        }
                        break;
                    case 930165504:
                        if (str.equals("application/x-mp4-cea-608")) {
                            c = 6;
                            break;
                        }
                        break;
                    case 1201784583:
                        if (str.equals("text/x-exoplayer-cues")) {
                            c = 7;
                            break;
                        }
                        break;
                    case 1566015601:
                        if (str.equals("application/cea-608")) {
                            c = '\b';
                            break;
                        }
                        break;
                    case 1566016562:
                        if (str.equals("application/cea-708")) {
                            c = '\t';
                            break;
                        }
                        break;
                    case 1668750253:
                        if (str.equals("application/x-subrip")) {
                            c = '\n';
                            break;
                        }
                        break;
                    case 1693976202:
                        if (str.equals("application/ttml+xml")) {
                            c = 11;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        return new ns0(jVar.r0);
                    case 1:
                        return new qq2();
                    case 2:
                        return new ia2();
                    case 3:
                        return new wo4();
                    case 4:
                        return new tc4(jVar.r0);
                    case 5:
                        return new cs3(jVar.r0);
                    case 6:
                    case '\b':
                        return new ow(str, jVar.H0, 16000L);
                    case 7:
                        return new v01();
                    case '\t':
                        return new qw(jVar.H0, jVar.r0);
                    case '\n':
                        return new kv3();
                    case 11:
                        return new ic4();
                }
            }
            throw new IllegalArgumentException("Attempted to create decoder for unsupported MIME type: " + str);
        }
    }

    boolean a(j jVar);

    rv3 b(j jVar);
}
