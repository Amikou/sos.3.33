package defpackage;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.CircularProgressIndicatorSpec;
import com.google.android.material.progressindicator.LinearProgressIndicatorSpec;
import defpackage.wn;

/* compiled from: IndeterminateDrawable.java */
/* renamed from: iq1  reason: default package */
/* loaded from: classes2.dex */
public final class iq1<S extends wn> extends er0 {
    public nr0<S> t0;
    public hq1<ObjectAnimator> u0;

    public iq1(Context context, wn wnVar, nr0<S> nr0Var, hq1<ObjectAnimator> hq1Var) {
        super(context, wnVar);
        x(nr0Var);
        w(hq1Var);
    }

    public static iq1<CircularProgressIndicatorSpec> s(Context context, CircularProgressIndicatorSpec circularProgressIndicatorSpec) {
        return new iq1<>(context, circularProgressIndicatorSpec, new wy(circularProgressIndicatorSpec), new xy(circularProgressIndicatorSpec));
    }

    public static iq1<LinearProgressIndicatorSpec> t(Context context, LinearProgressIndicatorSpec linearProgressIndicatorSpec) {
        return new iq1<>(context, linearProgressIndicatorSpec, new yz1(linearProgressIndicatorSpec), linearProgressIndicatorSpec.g == 0 ? new zz1(linearProgressIndicatorSpec) : new a02(context, linearProgressIndicatorSpec));
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect rect = new Rect();
        if (getBounds().isEmpty() || !isVisible() || !canvas.getClipBounds(rect)) {
            return;
        }
        canvas.save();
        this.t0.g(canvas, g());
        this.t0.c(canvas, this.q0);
        int i = 0;
        while (true) {
            hq1<ObjectAnimator> hq1Var = this.u0;
            int[] iArr = hq1Var.c;
            if (i < iArr.length) {
                nr0<S> nr0Var = this.t0;
                Paint paint = this.q0;
                float[] fArr = hq1Var.b;
                int i2 = i * 2;
                nr0Var.b(canvas, paint, fArr[i2], fArr[i2 + 1], iArr[i]);
                i++;
            } else {
                canvas.restore();
                return;
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.t0.d();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.t0.e();
    }

    @Override // defpackage.er0
    public boolean q(boolean z, boolean z2, boolean z3) {
        boolean q = super.q(z, z2, z3);
        if (!isRunning()) {
            this.u0.a();
        }
        float a = this.g0.a(this.a.getContentResolver());
        if (z && (z3 || (Build.VERSION.SDK_INT <= 21 && a > Utils.FLOAT_EPSILON))) {
            this.u0.g();
        }
        return q;
    }

    public hq1<ObjectAnimator> u() {
        return this.u0;
    }

    public nr0<S> v() {
        return this.t0;
    }

    public void w(hq1<ObjectAnimator> hq1Var) {
        this.u0 = hq1Var;
        hq1Var.e(this);
    }

    public void x(nr0<S> nr0Var) {
        this.t0 = nr0Var;
        nr0Var.f(this);
    }
}
