package defpackage;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: zj5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class zj5 {
    public final ck5 a;

    public zj5(ck5 ck5Var) {
        this.a = ck5Var;
    }

    public final void a(String str, int i, Throwable th, byte[] bArr, Map map) {
        this.a.r(str, i, th, bArr, map);
    }
}
