package defpackage;

import com.google.auto.value.AutoValue;
import defpackage.al;
import defpackage.bl;
import defpackage.cl;
import defpackage.dl;
import defpackage.el;
import defpackage.fl;
import defpackage.gl;
import defpackage.lk;
import defpackage.nk;
import defpackage.ok;
import defpackage.pk;
import defpackage.qk;
import defpackage.rk;
import defpackage.sk;
import defpackage.uk;
import defpackage.vk;
import defpackage.wk;
import defpackage.xk;
import defpackage.yk;
import defpackage.zk;
import java.nio.charset.Charset;

/* compiled from: CrashlyticsReport.java */
@AutoValue
/* renamed from: r90  reason: default package */
/* loaded from: classes2.dex */
public abstract class r90 {
    public static final Charset a = Charset.forName("UTF-8");

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* renamed from: r90$a */
    /* loaded from: classes2.dex */
    public static abstract class a {

        /* compiled from: CrashlyticsReport.java */
        @AutoValue.Builder
        /* renamed from: r90$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static abstract class AbstractC0263a {
            public abstract a a();

            public abstract AbstractC0263a b(int i);

            public abstract AbstractC0263a c(int i);

            public abstract AbstractC0263a d(String str);

            public abstract AbstractC0263a e(long j);

            public abstract AbstractC0263a f(int i);

            public abstract AbstractC0263a g(long j);

            public abstract AbstractC0263a h(long j);

            public abstract AbstractC0263a i(String str);
        }

        public static AbstractC0263a a() {
            return new nk.b();
        }

        public abstract int b();

        public abstract int c();

        public abstract String d();

        public abstract long e();

        public abstract int f();

        public abstract long g();

        public abstract long h();

        public abstract String i();
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue.Builder
    /* renamed from: r90$b */
    /* loaded from: classes2.dex */
    public static abstract class b {
        public abstract r90 a();

        public abstract b b(String str);

        public abstract b c(String str);

        public abstract b d(String str);

        public abstract b e(String str);

        public abstract b f(d dVar);

        public abstract b g(int i);

        public abstract b h(String str);

        public abstract b i(e eVar);
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* renamed from: r90$c */
    /* loaded from: classes2.dex */
    public static abstract class c {

        /* compiled from: CrashlyticsReport.java */
        @AutoValue.Builder
        /* renamed from: r90$c$a */
        /* loaded from: classes2.dex */
        public static abstract class a {
            public abstract c a();

            public abstract a b(String str);

            public abstract a c(String str);
        }

        public static a a() {
            return new ok.b();
        }

        public abstract String b();

        public abstract String c();
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* renamed from: r90$d */
    /* loaded from: classes2.dex */
    public static abstract class d {

        /* compiled from: CrashlyticsReport.java */
        @AutoValue.Builder
        /* renamed from: r90$d$a */
        /* loaded from: classes2.dex */
        public static abstract class a {
            public abstract d a();

            public abstract a b(ip1<b> ip1Var);

            public abstract a c(String str);
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: r90$d$b */
        /* loaded from: classes2.dex */
        public static abstract class b {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* renamed from: r90$d$b$a */
            /* loaded from: classes2.dex */
            public static abstract class a {
                public abstract b a();

                public abstract a b(byte[] bArr);

                public abstract a c(String str);
            }

            public static a a() {
                return new qk.b();
            }

            public abstract byte[] b();

            public abstract String c();
        }

        public static a a() {
            return new pk.b();
        }

        public abstract ip1<b> b();

        public abstract String c();
    }

    /* compiled from: CrashlyticsReport.java */
    @AutoValue
    /* renamed from: r90$e */
    /* loaded from: classes2.dex */
    public static abstract class e {

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: r90$e$a */
        /* loaded from: classes2.dex */
        public static abstract class a {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* renamed from: r90$e$a$a  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public static abstract class AbstractC0264a {
                public abstract a a();

                public abstract AbstractC0264a b(String str);

                public abstract AbstractC0264a c(String str);

                public abstract AbstractC0264a d(String str);

                public abstract AbstractC0264a e(String str);

                public abstract AbstractC0264a f(String str);

                public abstract AbstractC0264a g(String str);
            }

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: r90$e$a$b */
            /* loaded from: classes2.dex */
            public static abstract class b {
                public abstract String a();
            }

            public static AbstractC0264a a() {
                return new sk.b();
            }

            public abstract String b();

            public abstract String c();

            public abstract String d();

            public abstract String e();

            public abstract String f();

            public abstract b g();

            public abstract String h();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue.Builder
        /* renamed from: r90$e$b */
        /* loaded from: classes2.dex */
        public static abstract class b {
            public abstract e a();

            public abstract b b(a aVar);

            public abstract b c(boolean z);

            public abstract b d(c cVar);

            public abstract b e(Long l);

            public abstract b f(ip1<d> ip1Var);

            public abstract b g(String str);

            public abstract b h(int i);

            public abstract b i(String str);

            public b j(byte[] bArr) {
                return i(new String(bArr, r90.a));
            }

            public abstract b k(AbstractC0277e abstractC0277e);

            public abstract b l(long j);

            public abstract b m(f fVar);
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: r90$e$c */
        /* loaded from: classes2.dex */
        public static abstract class c {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* renamed from: r90$e$c$a */
            /* loaded from: classes2.dex */
            public static abstract class a {
                public abstract c a();

                public abstract a b(int i);

                public abstract a c(int i);

                public abstract a d(long j);

                public abstract a e(String str);

                public abstract a f(String str);

                public abstract a g(String str);

                public abstract a h(long j);

                public abstract a i(boolean z);

                public abstract a j(int i);
            }

            public static a a() {
                return new uk.b();
            }

            public abstract int b();

            public abstract int c();

            public abstract long d();

            public abstract String e();

            public abstract String f();

            public abstract String g();

            public abstract long h();

            public abstract int i();

            public abstract boolean j();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: r90$e$d */
        /* loaded from: classes2.dex */
        public static abstract class d {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: r90$e$d$a */
            /* loaded from: classes2.dex */
            public static abstract class a {

                /* compiled from: CrashlyticsReport.java */
                @AutoValue.Builder
                /* renamed from: r90$e$d$a$a  reason: collision with other inner class name */
                /* loaded from: classes2.dex */
                public static abstract class AbstractC0265a {
                    public abstract a a();

                    public abstract AbstractC0265a b(Boolean bool);

                    public abstract AbstractC0265a c(ip1<c> ip1Var);

                    public abstract AbstractC0265a d(b bVar);

                    public abstract AbstractC0265a e(ip1<c> ip1Var);

                    public abstract AbstractC0265a f(int i);
                }

                /* compiled from: CrashlyticsReport.java */
                @AutoValue
                /* renamed from: r90$e$d$a$b */
                /* loaded from: classes2.dex */
                public static abstract class b {

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: r90$e$d$a$b$a  reason: collision with other inner class name */
                    /* loaded from: classes2.dex */
                    public static abstract class AbstractC0266a {

                        /* compiled from: CrashlyticsReport.java */
                        @AutoValue.Builder
                        /* renamed from: r90$e$d$a$b$a$a  reason: collision with other inner class name */
                        /* loaded from: classes2.dex */
                        public static abstract class AbstractC0267a {
                            public abstract AbstractC0266a a();

                            public abstract AbstractC0267a b(long j);

                            public abstract AbstractC0267a c(String str);

                            public abstract AbstractC0267a d(long j);

                            public abstract AbstractC0267a e(String str);

                            public AbstractC0267a f(byte[] bArr) {
                                return e(new String(bArr, r90.a));
                            }
                        }

                        public static AbstractC0267a a() {
                            return new yk.b();
                        }

                        public abstract long b();

                        public abstract String c();

                        public abstract long d();

                        public abstract String e();

                        public byte[] f() {
                            String e = e();
                            if (e != null) {
                                return e.getBytes(r90.a);
                            }
                            return null;
                        }
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue.Builder
                    /* renamed from: r90$e$d$a$b$b  reason: collision with other inner class name */
                    /* loaded from: classes2.dex */
                    public static abstract class AbstractC0268b {
                        public abstract b a();

                        public abstract AbstractC0268b b(a aVar);

                        public abstract AbstractC0268b c(ip1<AbstractC0266a> ip1Var);

                        public abstract AbstractC0268b d(c cVar);

                        public abstract AbstractC0268b e(AbstractC0270d abstractC0270d);

                        public abstract AbstractC0268b f(ip1<AbstractC0272e> ip1Var);
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: r90$e$d$a$b$c */
                    /* loaded from: classes2.dex */
                    public static abstract class c {

                        /* compiled from: CrashlyticsReport.java */
                        @AutoValue.Builder
                        /* renamed from: r90$e$d$a$b$c$a  reason: collision with other inner class name */
                        /* loaded from: classes2.dex */
                        public static abstract class AbstractC0269a {
                            public abstract c a();

                            public abstract AbstractC0269a b(c cVar);

                            public abstract AbstractC0269a c(ip1<AbstractC0272e.AbstractC0274b> ip1Var);

                            public abstract AbstractC0269a d(int i);

                            public abstract AbstractC0269a e(String str);

                            public abstract AbstractC0269a f(String str);
                        }

                        public static AbstractC0269a a() {
                            return new zk.b();
                        }

                        public abstract c b();

                        public abstract ip1<AbstractC0272e.AbstractC0274b> c();

                        public abstract int d();

                        public abstract String e();

                        public abstract String f();
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: r90$e$d$a$b$d  reason: collision with other inner class name */
                    /* loaded from: classes2.dex */
                    public static abstract class AbstractC0270d {

                        /* compiled from: CrashlyticsReport.java */
                        @AutoValue.Builder
                        /* renamed from: r90$e$d$a$b$d$a  reason: collision with other inner class name */
                        /* loaded from: classes2.dex */
                        public static abstract class AbstractC0271a {
                            public abstract AbstractC0270d a();

                            public abstract AbstractC0271a b(long j);

                            public abstract AbstractC0271a c(String str);

                            public abstract AbstractC0271a d(String str);
                        }

                        public static AbstractC0271a a() {
                            return new al.b();
                        }

                        public abstract long b();

                        public abstract String c();

                        public abstract String d();
                    }

                    /* compiled from: CrashlyticsReport.java */
                    @AutoValue
                    /* renamed from: r90$e$d$a$b$e  reason: collision with other inner class name */
                    /* loaded from: classes2.dex */
                    public static abstract class AbstractC0272e {

                        /* compiled from: CrashlyticsReport.java */
                        @AutoValue.Builder
                        /* renamed from: r90$e$d$a$b$e$a  reason: collision with other inner class name */
                        /* loaded from: classes2.dex */
                        public static abstract class AbstractC0273a {
                            public abstract AbstractC0272e a();

                            public abstract AbstractC0273a b(ip1<AbstractC0274b> ip1Var);

                            public abstract AbstractC0273a c(int i);

                            public abstract AbstractC0273a d(String str);
                        }

                        /* compiled from: CrashlyticsReport.java */
                        @AutoValue
                        /* renamed from: r90$e$d$a$b$e$b  reason: collision with other inner class name */
                        /* loaded from: classes2.dex */
                        public static abstract class AbstractC0274b {

                            /* compiled from: CrashlyticsReport.java */
                            @AutoValue.Builder
                            /* renamed from: r90$e$d$a$b$e$b$a  reason: collision with other inner class name */
                            /* loaded from: classes2.dex */
                            public static abstract class AbstractC0275a {
                                public abstract AbstractC0274b a();

                                public abstract AbstractC0275a b(String str);

                                public abstract AbstractC0275a c(int i);

                                public abstract AbstractC0275a d(long j);

                                public abstract AbstractC0275a e(long j);

                                public abstract AbstractC0275a f(String str);
                            }

                            public static AbstractC0275a a() {
                                return new cl.b();
                            }

                            public abstract String b();

                            public abstract int c();

                            public abstract long d();

                            public abstract long e();

                            public abstract String f();
                        }

                        public static AbstractC0273a a() {
                            return new bl.b();
                        }

                        public abstract ip1<AbstractC0274b> b();

                        public abstract int c();

                        public abstract String d();
                    }

                    public static AbstractC0268b a() {
                        return new xk.b();
                    }

                    public abstract a b();

                    public abstract ip1<AbstractC0266a> c();

                    public abstract c d();

                    public abstract AbstractC0270d e();

                    public abstract ip1<AbstractC0272e> f();
                }

                public static AbstractC0265a a() {
                    return new wk.b();
                }

                public abstract Boolean b();

                public abstract ip1<c> c();

                public abstract b d();

                public abstract ip1<c> e();

                public abstract int f();

                public abstract AbstractC0265a g();
            }

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* renamed from: r90$e$d$b */
            /* loaded from: classes2.dex */
            public static abstract class b {
                public abstract d a();

                public abstract b b(a aVar);

                public abstract b c(c cVar);

                public abstract b d(AbstractC0276d abstractC0276d);

                public abstract b e(long j);

                public abstract b f(String str);
            }

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: r90$e$d$c */
            /* loaded from: classes2.dex */
            public static abstract class c {

                /* compiled from: CrashlyticsReport.java */
                @AutoValue.Builder
                /* renamed from: r90$e$d$c$a */
                /* loaded from: classes2.dex */
                public static abstract class a {
                    public abstract c a();

                    public abstract a b(Double d);

                    public abstract a c(int i);

                    public abstract a d(long j);

                    public abstract a e(int i);

                    public abstract a f(boolean z);

                    public abstract a g(long j);
                }

                public static a a() {
                    return new dl.b();
                }

                public abstract Double b();

                public abstract int c();

                public abstract long d();

                public abstract int e();

                public abstract long f();

                public abstract boolean g();
            }

            /* compiled from: CrashlyticsReport.java */
            @AutoValue
            /* renamed from: r90$e$d$d  reason: collision with other inner class name */
            /* loaded from: classes2.dex */
            public static abstract class AbstractC0276d {

                /* compiled from: CrashlyticsReport.java */
                @AutoValue.Builder
                /* renamed from: r90$e$d$d$a */
                /* loaded from: classes2.dex */
                public static abstract class a {
                    public abstract AbstractC0276d a();

                    public abstract a b(String str);
                }

                public static a a() {
                    return new el.b();
                }

                public abstract String b();
            }

            public static b a() {
                return new vk.b();
            }

            public abstract a b();

            public abstract c c();

            public abstract AbstractC0276d d();

            public abstract long e();

            public abstract String f();

            public abstract b g();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: r90$e$e  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public static abstract class AbstractC0277e {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* renamed from: r90$e$e$a */
            /* loaded from: classes2.dex */
            public static abstract class a {
                public abstract AbstractC0277e a();

                public abstract a b(String str);

                public abstract a c(boolean z);

                public abstract a d(int i);

                public abstract a e(String str);
            }

            public static a a() {
                return new fl.b();
            }

            public abstract String b();

            public abstract int c();

            public abstract String d();

            public abstract boolean e();
        }

        /* compiled from: CrashlyticsReport.java */
        @AutoValue
        /* renamed from: r90$e$f */
        /* loaded from: classes2.dex */
        public static abstract class f {

            /* compiled from: CrashlyticsReport.java */
            @AutoValue.Builder
            /* renamed from: r90$e$f$a */
            /* loaded from: classes2.dex */
            public static abstract class a {
                public abstract f a();

                public abstract a b(String str);
            }

            public static a a() {
                return new gl.b();
            }

            public abstract String b();
        }

        public static b a() {
            return new rk.b().c(false);
        }

        public abstract a b();

        public abstract c c();

        public abstract Long d();

        public abstract ip1<d> e();

        public abstract String f();

        public abstract int g();

        public abstract String h();

        public byte[] i() {
            return h().getBytes(r90.a);
        }

        public abstract AbstractC0277e j();

        public abstract long k();

        public abstract f l();

        public abstract boolean m();

        public abstract b n();

        public e o(ip1<d> ip1Var) {
            return n().f(ip1Var).a();
        }

        public e p(long j, boolean z, String str) {
            b n = n();
            n.e(Long.valueOf(j));
            n.c(z);
            if (str != null) {
                n.m(f.a().b(str).a()).a();
            }
            return n.a();
        }
    }

    public static b b() {
        return new lk.b();
    }

    public abstract String c();

    public abstract String d();

    public abstract String e();

    public abstract String f();

    public abstract d g();

    public abstract int h();

    public abstract String i();

    public abstract e j();

    public abstract b k();

    public r90 l(ip1<e.d> ip1Var) {
        if (j() != null) {
            return k().i(j().o(ip1Var)).a();
        }
        throw new IllegalStateException("Reports without sessions cannot have events added to them.");
    }

    public r90 m(d dVar) {
        return k().i(null).f(dVar).a();
    }

    public r90 n(long j, boolean z, String str) {
        b k = k();
        if (j() != null) {
            k.i(j().p(j, z, str));
        }
        return k.a();
    }
}
