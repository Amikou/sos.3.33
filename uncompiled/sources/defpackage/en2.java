package defpackage;

import java.lang.reflect.Field;

/* compiled from: OneofInfo.java */
/* renamed from: en2  reason: default package */
/* loaded from: classes2.dex */
public final class en2 {
    public final Field a;
    public final Field b;

    public en2(int i, Field field, Field field2) {
        this.a = field;
        this.b = field2;
    }

    public Field a() {
        return this.a;
    }

    public Field b() {
        return this.b;
    }
}
