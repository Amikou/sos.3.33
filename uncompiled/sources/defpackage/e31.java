package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: FiatTokenProvider.kt */
/* renamed from: e31  reason: default package */
/* loaded from: classes2.dex */
public final class e31 implements rm1 {
    public static final e31 a = new e31();
    public static yl1 b;

    @Override // defpackage.rm1
    public void a() {
        b = null;
    }

    public final yl1 b(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new d31(ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, context, null, 2, null).V());
                }
                te4 te4Var = te4.a;
            }
        }
        yl1 yl1Var = b;
        fs1.d(yl1Var);
        return yl1Var;
    }
}
