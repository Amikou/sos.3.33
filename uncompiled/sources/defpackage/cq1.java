package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import net.safemoon.androidwallet.R;

/* compiled from: IncludeWordBinding.java */
/* renamed from: cq1  reason: default package */
/* loaded from: classes2.dex */
public final class cq1 {
    public final TextView a;

    public cq1(LinearLayout linearLayout, TextView textView, LinearLayout linearLayout2) {
        this.a = textView;
    }

    public static cq1 a(View view) {
        int i = R.id.index;
        TextView textView = (TextView) ai4.a(view, R.id.index);
        if (textView != null) {
            i = R.id.itemParent;
            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.itemParent);
            if (linearLayout != null) {
                return new cq1((LinearLayout) view, textView, linearLayout);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
