package defpackage;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/* renamed from: pl0  reason: default package */
/* loaded from: classes2.dex */
public final class pl0 implements es4 {
    public static final Map<String, pl0> b;
    public final String a;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(a("SHA-256", 32, 16, 67, 20, 2), new pl0(16777217, "XMSSMT_SHA2-256_W16_H20_D2"));
        hashMap.put(a("SHA-256", 32, 16, 67, 20, 4), new pl0(16777217, "XMSSMT_SHA2-256_W16_H20_D4"));
        hashMap.put(a("SHA-256", 32, 16, 67, 40, 2), new pl0(16777217, "XMSSMT_SHA2-256_W16_H40_D2"));
        hashMap.put(a("SHA-256", 32, 16, 67, 40, 2), new pl0(16777217, "XMSSMT_SHA2-256_W16_H40_D4"));
        hashMap.put(a("SHA-256", 32, 16, 67, 40, 4), new pl0(16777217, "XMSSMT_SHA2-256_W16_H40_D8"));
        hashMap.put(a("SHA-256", 32, 16, 67, 60, 8), new pl0(16777217, "XMSSMT_SHA2-256_W16_H60_D3"));
        hashMap.put(a("SHA-256", 32, 16, 67, 60, 6), new pl0(16777217, "XMSSMT_SHA2-256_W16_H60_D6"));
        hashMap.put(a("SHA-256", 32, 16, 67, 60, 12), new pl0(16777217, "XMSSMT_SHA2-256_W16_H60_D12"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 20, 2), new pl0(16777217, "XMSSMT_SHA2-512_W16_H20_D2"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 20, 4), new pl0(16777217, "XMSSMT_SHA2-512_W16_H20_D4"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 40, 2), new pl0(16777217, "XMSSMT_SHA2-512_W16_H40_D2"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 40, 4), new pl0(16777217, "XMSSMT_SHA2-512_W16_H40_D4"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 40, 8), new pl0(16777217, "XMSSMT_SHA2-512_W16_H40_D8"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 60, 3), new pl0(16777217, "XMSSMT_SHA2-512_W16_H60_D3"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 60, 6), new pl0(16777217, "XMSSMT_SHA2-512_W16_H60_D6"));
        hashMap.put(a("SHA2-512", 64, 16, 131, 60, 12), new pl0(16777217, "XMSSMT_SHA2-512_W16_H60_D12"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 20, 2), new pl0(16777217, "XMSSMT_SHAKE128_W16_H20_D2"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 20, 4), new pl0(16777217, "XMSSMT_SHAKE128_W16_H20_D4"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 40, 2), new pl0(16777217, "XMSSMT_SHAKE128_W16_H40_D2"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 40, 4), new pl0(16777217, "XMSSMT_SHAKE128_W16_H40_D4"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 40, 8), new pl0(16777217, "XMSSMT_SHAKE128_W16_H40_D8"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 60, 3), new pl0(16777217, "XMSSMT_SHAKE128_W16_H60_D3"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 60, 6), new pl0(16777217, "XMSSMT_SHAKE128_W16_H60_D6"));
        hashMap.put(a("SHAKE128", 32, 16, 67, 60, 12), new pl0(16777217, "XMSSMT_SHAKE128_W16_H60_D12"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 20, 2), new pl0(16777217, "XMSSMT_SHAKE256_W16_H20_D2"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 20, 4), new pl0(16777217, "XMSSMT_SHAKE256_W16_H20_D4"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 40, 2), new pl0(16777217, "XMSSMT_SHAKE256_W16_H40_D2"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 40, 4), new pl0(16777217, "XMSSMT_SHAKE256_W16_H40_D4"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 40, 8), new pl0(16777217, "XMSSMT_SHAKE256_W16_H40_D8"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 60, 3), new pl0(16777217, "XMSSMT_SHAKE256_W16_H60_D3"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 60, 6), new pl0(16777217, "XMSSMT_SHAKE256_W16_H60_D6"));
        hashMap.put(a("SHAKE256", 64, 16, 131, 60, 12), new pl0(16777217, "XMSSMT_SHAKE256_W16_H60_D12"));
        b = Collections.unmodifiableMap(hashMap);
    }

    public pl0(int i, String str) {
        this.a = str;
    }

    public static String a(String str, int i, int i2, int i3, int i4, int i5) {
        Objects.requireNonNull(str, "algorithmName == null");
        return str + "-" + i + "-" + i2 + "-" + i3 + "-" + i4 + "-" + i5;
    }

    public static pl0 b(String str, int i, int i2, int i3, int i4, int i5) {
        Objects.requireNonNull(str, "algorithmName == null");
        return b.get(a(str, i, i2, i3, i4, i5));
    }

    public String toString() {
        return this.a;
    }
}
