package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/* compiled from: Sequences.kt */
/* renamed from: xe1  reason: default package */
/* loaded from: classes2.dex */
public final class xe1<T> implements ol3<T> {
    public final rc1<T> a;
    public final tc1<T, T> b;

    /* compiled from: Sequences.kt */
    /* renamed from: xe1$a */
    /* loaded from: classes2.dex */
    public static final class a implements Iterator<T>, tw1 {
        public T a;
        public int f0 = -2;

        public a() {
        }

        public final void a() {
            T t;
            if (this.f0 == -2) {
                t = (T) xe1.this.a.invoke();
            } else {
                tc1 tc1Var = xe1.this.b;
                T t2 = this.a;
                fs1.d(t2);
                t = (T) tc1Var.invoke(t2);
            }
            this.a = t;
            this.f0 = t == null ? 0 : 1;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.f0 < 0) {
                a();
            }
            return this.f0 == 1;
        }

        @Override // java.util.Iterator
        public T next() {
            if (this.f0 < 0) {
                a();
            }
            if (this.f0 != 0) {
                T t = this.a;
                Objects.requireNonNull(t, "null cannot be cast to non-null type T");
                this.f0 = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public xe1(rc1<? extends T> rc1Var, tc1<? super T, ? extends T> tc1Var) {
        fs1.f(rc1Var, "getInitialValue");
        fs1.f(tc1Var, "getNextValue");
        this.a = rc1Var;
        this.b = tc1Var;
    }

    @Override // defpackage.ol3
    public Iterator<T> iterator() {
        return new a();
    }
}
