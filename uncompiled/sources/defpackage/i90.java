package defpackage;

/* compiled from: CountingMemoryCache.java */
/* renamed from: i90  reason: default package */
/* loaded from: classes.dex */
public interface i90<K, V> extends l72<K, V>, q72 {

    /* compiled from: CountingMemoryCache.java */
    /* renamed from: i90$a */
    /* loaded from: classes.dex */
    public static class a<K, V> {
        public final K a;
        public final com.facebook.common.references.a<V> b;
        public int c = 0;
        public boolean d = false;
        public final b<K> e;
        public int f;

        public a(K k, com.facebook.common.references.a<V> aVar, b<K> bVar, int i) {
            this.a = (K) xt2.g(k);
            this.b = (com.facebook.common.references.a) xt2.g(com.facebook.common.references.a.e(aVar));
            this.e = bVar;
            this.f = i;
        }

        public static <K, V> a<K, V> a(K k, com.facebook.common.references.a<V> aVar, int i, b<K> bVar) {
            return new a<>(k, aVar, bVar, i);
        }

        public static <K, V> a<K, V> b(K k, com.facebook.common.references.a<V> aVar, b<K> bVar) {
            return a(k, aVar, -1, bVar);
        }
    }

    /* compiled from: CountingMemoryCache.java */
    /* renamed from: i90$b */
    /* loaded from: classes.dex */
    public interface b<K> {
        void a(K k, boolean z);
    }

    com.facebook.common.references.a<V> d(K k);

    com.facebook.common.references.a<V> e(K k, com.facebook.common.references.a<V> aVar, b<K> bVar);
}
