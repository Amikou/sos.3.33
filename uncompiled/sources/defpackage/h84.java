package defpackage;

import androidx.media3.common.j;
import androidx.media3.common.v;

/* compiled from: TrackSelection.java */
/* renamed from: h84  reason: default package */
/* loaded from: classes.dex */
public interface h84 {
    int b(j jVar);

    v c();

    j j(int i);

    int l(int i);

    int length();

    int t(int i);
}
