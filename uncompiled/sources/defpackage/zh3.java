package defpackage;

import java.security.SecureRandom;
import org.web3j.crypto.LinuxSecureRandom;

/* compiled from: SecureRandomUtils.java */
/* renamed from: zh3  reason: default package */
/* loaded from: classes3.dex */
public final class zh3 {
    private static final SecureRandom SECURE_RANDOM;
    private static int isAndroid;

    static {
        if (isAndroidRuntime()) {
            new LinuxSecureRandom();
        }
        SECURE_RANDOM = new SecureRandom();
        isAndroid = -1;
    }

    private zh3() {
    }

    public static boolean isAndroidRuntime() {
        if (isAndroid == -1) {
            String property = System.getProperty("java.runtime.name");
            isAndroid = (property == null || !property.equals("Android Runtime")) ? 0 : 1;
        }
        return isAndroid == 1;
    }

    public static SecureRandom secureRandom() {
        return SECURE_RANDOM;
    }
}
