package defpackage;

import java.math.BigInteger;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.asn1.h0;
import org.bouncycastle.asn1.i;

/* renamed from: xt0  reason: default package */
/* loaded from: classes2.dex */
public class xt0 {
    public static nr4 a(ECParameterSpec eCParameterSpec, boolean z) {
        if (!(eCParameterSpec instanceof kt0)) {
            if (eCParameterSpec == null) {
                return new nr4((e4) h0.a);
            }
            xs0 a = us0.a(eCParameterSpec.getCurve());
            return new nr4(new pr4(a, us0.d(a, eCParameterSpec.getGenerator(), z), eCParameterSpec.getOrder(), BigInteger.valueOf(eCParameterSpec.getCofactor()), eCParameterSpec.getCurve().getSeed()));
        }
        kt0 kt0Var = (kt0) eCParameterSpec;
        i h = wt0.h(kt0Var.a());
        if (h == null) {
            h = new i(kt0Var.a());
        }
        return new nr4(h);
    }
}
