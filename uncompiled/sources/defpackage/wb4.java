package defpackage;

import android.content.Context;
import java.io.Closeable;
import java.io.IOException;

/* compiled from: TransportRuntimeComponent.java */
/* renamed from: wb4  reason: default package */
/* loaded from: classes.dex */
public abstract class wb4 implements Closeable {

    /* compiled from: TransportRuntimeComponent.java */
    /* renamed from: wb4$a */
    /* loaded from: classes.dex */
    public interface a {
        a a(Context context);

        wb4 build();
    }

    public abstract dy0 a();

    public abstract vb4 b();

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        a().close();
    }
}
