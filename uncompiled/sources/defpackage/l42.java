package defpackage;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;

/* compiled from: MaterialColors.java */
/* renamed from: l42  reason: default package */
/* loaded from: classes2.dex */
public class l42 {
    public static int a(int i, int i2) {
        return z20.j(i, (Color.alpha(i) * i2) / 255);
    }

    public static int b(Context context, int i, int i2) {
        TypedValue a = i42.a(context, i);
        return a != null ? a.data : i2;
    }

    public static int c(Context context, int i, String str) {
        return i42.c(context, i, str);
    }

    public static int d(View view, int i) {
        return i42.d(view, i);
    }

    public static int e(View view, int i, int i2) {
        return b(view.getContext(), i, i2);
    }

    public static boolean f(int i) {
        return i != 0 && z20.c(i) > 0.5d;
    }

    public static int g(int i, int i2) {
        return z20.f(i2, i);
    }

    public static int h(int i, int i2, float f) {
        return g(i, z20.j(i2, Math.round(Color.alpha(i2) * f)));
    }

    public static int i(View view, int i, int i2, float f) {
        return h(d(view, i), d(view, i2), f);
    }
}
