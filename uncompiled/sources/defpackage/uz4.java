package defpackage;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: uz4  reason: default package */
/* loaded from: classes.dex */
public final class uz4 implements Runnable {
    public final /* synthetic */ rz4 a;

    public uz4(rz4 rz4Var) {
        this.a = rz4Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        eh1 eh1Var;
        Context context;
        eh1Var = this.a.d;
        context = this.a.c;
        eh1Var.a(context);
    }
}
