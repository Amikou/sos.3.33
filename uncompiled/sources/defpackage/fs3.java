package defpackage;

import androidx.media3.common.util.b;
import java.util.Collections;
import java.util.List;

/* compiled from: SsaSubtitle.java */
/* renamed from: fs3  reason: default package */
/* loaded from: classes.dex */
public final class fs3 implements qv3 {
    public final List<List<kb0>> a;
    public final List<Long> f0;

    public fs3(List<List<kb0>> list, List<Long> list2) {
        this.a = list;
        this.f0 = list2;
    }

    @Override // defpackage.qv3
    public int a(long j) {
        int d = b.d(this.f0, Long.valueOf(j), false, false);
        if (d < this.f0.size()) {
            return d;
        }
        return -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        ii.a(i >= 0);
        ii.a(i < this.f0.size());
        return this.f0.get(i).longValue();
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        int g = b.g(this.f0, Long.valueOf(j), true, false);
        if (g == -1) {
            return Collections.emptyList();
        }
        return this.a.get(g);
    }

    @Override // defpackage.qv3
    public int f() {
        return this.f0.size();
    }
}
