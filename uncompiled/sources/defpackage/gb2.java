package defpackage;

import androidx.lifecycle.LiveData;

/* compiled from: MutableLiveData.java */
/* renamed from: gb2  reason: default package */
/* loaded from: classes.dex */
public class gb2<T> extends LiveData<T> {
    public gb2(T t) {
        super(t);
    }

    @Override // androidx.lifecycle.LiveData
    public void postValue(T t) {
        super.postValue(t);
    }

    @Override // androidx.lifecycle.LiveData
    public void setValue(T t) {
        super.setValue(t);
    }

    public gb2() {
    }
}
