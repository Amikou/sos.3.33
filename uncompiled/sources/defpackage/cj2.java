package defpackage;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/* compiled from: NumberTextWatcherForThousand.kt */
/* renamed from: cj2  reason: default package */
/* loaded from: classes2.dex */
public class cj2 implements TextWatcher {
    public final EditText a;

    public cj2(EditText editText) {
        fs1.f(editText, "etAmount");
        this.a = editText;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        fs1.f(editable, "s");
        int selectionEnd = this.a.getSelectionEnd();
        String obj = this.a.getText().toString();
        try {
            this.a.removeTextChangedListener(this);
            String obj2 = this.a.getText().toString();
            if (!fs1.b(obj2, "")) {
                String v = e30.v(this.a.getText().toString());
                if (!fs1.b(obj2, "")) {
                    this.a.setText(v);
                }
                this.a.setSelection(selectionEnd + (this.a.getText().toString().length() - obj.length()));
            }
            this.a.addTextChangedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
            this.a.addTextChangedListener(this);
        }
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        fs1.f(charSequence, "s");
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        fs1.f(charSequence, "s");
    }
}
