package defpackage;

import java.util.concurrent.atomic.AtomicBoolean;
import org.web3j.ens.contracts.generated.ENS;

/* compiled from: SingleLiveEvent.kt */
/* renamed from: up3  reason: default package */
/* loaded from: classes2.dex */
public final class up3<T> extends gb2<T> {
    public static final String b;
    public final AtomicBoolean a = new AtomicBoolean(false);

    /* compiled from: SingleLiveEvent.kt */
    /* renamed from: up3$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* compiled from: SingleLiveEvent.kt */
    /* renamed from: up3$b */
    /* loaded from: classes2.dex */
    public static final class b implements tl2<T> {
        public final /* synthetic */ up3<T> a;
        public final /* synthetic */ tl2<? super T> b;

        public b(up3<T> up3Var, tl2<? super T> tl2Var) {
            this.a = up3Var;
            this.b = tl2Var;
        }

        @Override // defpackage.tl2
        public void onChanged(T t) {
            if (this.a.a.compareAndSet(true, false)) {
                this.b.onChanged(t);
            }
        }
    }

    static {
        new a(null);
        b = "SingleLiveEvent";
    }

    @Override // androidx.lifecycle.LiveData
    public void observe(rz1 rz1Var, tl2<? super T> tl2Var) {
        fs1.f(rz1Var, ENS.FUNC_OWNER);
        fs1.f(tl2Var, "observer");
        hasActiveObservers();
        super.observe(rz1Var, new b(this, tl2Var));
    }

    @Override // defpackage.gb2, androidx.lifecycle.LiveData
    public void setValue(T t) {
        this.a.set(true);
        super.setValue(t);
    }
}
