package defpackage;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.media3.common.e;
import androidx.media3.common.util.b;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: BundleableUtil.java */
/* renamed from: is  reason: default package */
/* loaded from: classes.dex */
public final class is {
    public static void a(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader((ClassLoader) b.j(is.class.getClassLoader()));
        }
    }

    public static <T extends e> ImmutableList<T> b(e.a<T> aVar, List<Bundle> list) {
        ImmutableList.a builder = ImmutableList.builder();
        for (int i = 0; i < list.size(); i++) {
            builder.a(aVar.a((Bundle) ii.e(list.get(i))));
        }
        return builder.l();
    }

    public static <T extends e> ArrayList<Bundle> c(Collection<T> collection) {
        ArrayList<Bundle> arrayList = new ArrayList<>(collection.size());
        for (T t : collection) {
            arrayList.add(t.toBundle());
        }
        return arrayList;
    }

    public static <T extends e> SparseArray<Bundle> d(SparseArray<T> sparseArray) {
        SparseArray<Bundle> sparseArray2 = new SparseArray<>(sparseArray.size());
        for (int i = 0; i < sparseArray.size(); i++) {
            sparseArray2.put(sparseArray.keyAt(i), sparseArray.valueAt(i).toBundle());
        }
        return sparseArray2;
    }
}
