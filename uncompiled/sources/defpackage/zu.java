package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: CalleeHandler.java */
/* renamed from: zu  reason: default package */
/* loaded from: classes.dex */
public class zu {
    public static Handler a() {
        if (Looper.myLooper() == null) {
            return new Handler(Looper.getMainLooper());
        }
        return new Handler();
    }
}
