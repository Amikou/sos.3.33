package defpackage;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.b;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.jsontype.impl.AsArrayTypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.impl.AsExternalTypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.impl.AsPropertyTypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.impl.AsWrapperTypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.impl.d;
import com.fasterxml.jackson.databind.jsontype.impl.e;
import com.fasterxml.jackson.databind.jsontype.impl.f;
import com.fasterxml.jackson.databind.jsontype.impl.g;
import java.util.Collection;

/* compiled from: StdTypeResolverBuilder.java */
/* renamed from: pt3  reason: default package */
/* loaded from: classes.dex */
public class pt3 implements vd4<pt3> {
    public b _customIdResolver;
    public Class<?> _defaultImpl;
    public JsonTypeInfo.Id _idType;
    public JsonTypeInfo.As _includeAs;
    public boolean _typeIdVisible = false;
    public String _typeProperty;

    /* compiled from: StdTypeResolverBuilder.java */
    /* renamed from: pt3$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        static {
            int[] iArr = new int[JsonTypeInfo.Id.values().length];
            b = iArr;
            try {
                iArr[JsonTypeInfo.Id.CLASS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[JsonTypeInfo.Id.MINIMAL_CLASS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[JsonTypeInfo.Id.NAME.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[JsonTypeInfo.Id.NONE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                b[JsonTypeInfo.Id.CUSTOM.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            int[] iArr2 = new int[JsonTypeInfo.As.values().length];
            a = iArr2;
            try {
                iArr2[JsonTypeInfo.As.WRAPPER_ARRAY.ordinal()] = 1;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[JsonTypeInfo.As.PROPERTY.ordinal()] = 2;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[JsonTypeInfo.As.WRAPPER_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[JsonTypeInfo.As.EXTERNAL_PROPERTY.ordinal()] = 4;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[JsonTypeInfo.As.EXISTING_PROPERTY.ordinal()] = 5;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    public static pt3 noTypeInfoBuilder() {
        return new pt3().init(JsonTypeInfo.Id.NONE, (b) null);
    }

    @Override // defpackage.vd4
    public com.fasterxml.jackson.databind.jsontype.a buildTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, Collection<NamedType> collection) {
        JavaType javaType2 = null;
        if (this._idType == JsonTypeInfo.Id.NONE || javaType.isPrimitive()) {
            return null;
        }
        b idResolver = idResolver(deserializationConfig, javaType, collection, false, true);
        Class<?> cls = this._defaultImpl;
        if (cls != null) {
            if (cls != Void.class && cls != ig2.class) {
                javaType2 = deserializationConfig.getTypeFactory().constructSpecializedType(javaType, this._defaultImpl);
            } else {
                javaType2 = deserializationConfig.getTypeFactory().constructType(this._defaultImpl);
            }
        }
        JavaType javaType3 = javaType2;
        int i = a.a[this._includeAs.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return new AsWrapperTypeDeserializer(javaType, idResolver, this._typeProperty, this._typeIdVisible, javaType3);
                }
                if (i == 4) {
                    return new AsExternalTypeDeserializer(javaType, idResolver, this._typeProperty, this._typeIdVisible, javaType3);
                }
                if (i != 5) {
                    throw new IllegalStateException("Do not know how to construct standard type serializer for inclusion type: " + this._includeAs);
                }
            }
            return new AsPropertyTypeDeserializer(javaType, idResolver, this._typeProperty, this._typeIdVisible, javaType3, this._includeAs);
        }
        return new AsArrayTypeDeserializer(javaType, idResolver, this._typeProperty, this._typeIdVisible, javaType3);
    }

    @Override // defpackage.vd4
    public c buildTypeSerializer(SerializationConfig serializationConfig, JavaType javaType, Collection<NamedType> collection) {
        if (this._idType == JsonTypeInfo.Id.NONE || javaType.isPrimitive()) {
            return null;
        }
        b idResolver = idResolver(serializationConfig, javaType, collection, true, false);
        int i = a.a[this._includeAs.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        if (i == 5) {
                            return new com.fasterxml.jackson.databind.jsontype.impl.b(idResolver, null, this._typeProperty);
                        }
                        throw new IllegalStateException("Do not know how to construct standard type serializer for inclusion type: " + this._includeAs);
                    }
                    return new com.fasterxml.jackson.databind.jsontype.impl.c(idResolver, null, this._typeProperty);
                }
                return new e(idResolver, null);
            }
            return new d(idResolver, null, this._typeProperty);
        }
        return new com.fasterxml.jackson.databind.jsontype.impl.a(idResolver, null);
    }

    @Override // defpackage.vd4
    public /* bridge */ /* synthetic */ pt3 defaultImpl(Class cls) {
        return defaultImpl((Class<?>) cls);
    }

    @Override // defpackage.vd4
    public Class<?> getDefaultImpl() {
        return this._defaultImpl;
    }

    public String getTypeProperty() {
        return this._typeProperty;
    }

    public b idResolver(MapperConfig<?> mapperConfig, JavaType javaType, Collection<NamedType> collection, boolean z, boolean z2) {
        b bVar = this._customIdResolver;
        if (bVar != null) {
            return bVar;
        }
        JsonTypeInfo.Id id = this._idType;
        if (id != null) {
            int i = a.b[id.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i == 4) {
                            return null;
                        }
                        throw new IllegalStateException("Do not know how to construct standard type id resolver for idType: " + this._idType);
                    }
                    return td4.i(mapperConfig, javaType, collection, z, z2);
                }
                return new g(javaType, mapperConfig.getTypeFactory());
            }
            return new f(javaType, mapperConfig.getTypeFactory());
        }
        throw new IllegalStateException("Can not build, 'init()' not yet called");
    }

    public boolean isTypeIdVisible() {
        return this._typeIdVisible;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.vd4
    public pt3 defaultImpl(Class<?> cls) {
        this._defaultImpl = cls;
        return this;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.vd4
    public pt3 inclusion(JsonTypeInfo.As as) {
        if (as != null) {
            this._includeAs = as;
            return this;
        }
        throw new IllegalArgumentException("includeAs can not be null");
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.vd4
    public pt3 init(JsonTypeInfo.Id id, b bVar) {
        if (id != null) {
            this._idType = id;
            this._customIdResolver = bVar;
            this._typeProperty = id.getDefaultPropertyName();
            return this;
        }
        throw new IllegalArgumentException("idType can not be null");
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.vd4
    public pt3 typeIdVisibility(boolean z) {
        this._typeIdVisible = z;
        return this;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // defpackage.vd4
    public pt3 typeProperty(String str) {
        if (str == null || str.length() == 0) {
            str = this._idType.getDefaultPropertyName();
        }
        this._typeProperty = str;
        return this;
    }
}
