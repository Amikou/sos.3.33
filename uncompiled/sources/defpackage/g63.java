package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.model.DeviceModel;
import net.safemoon.androidwallet.model.SingleFCMRegister;
import retrofit2.b;
import retrofit2.n;

/* compiled from: RegisterDeviceTokenUseCase.kt */
/* renamed from: g63  reason: default package */
/* loaded from: classes2.dex */
public final class g63 implements pm1 {
    public final ac3 a;
    public final Context b;
    public final String c;

    /* compiled from: RegisterDeviceTokenUseCase.kt */
    /* renamed from: g63$a */
    /* loaded from: classes2.dex */
    public static final class a implements wu<DeviceModel> {
        public final /* synthetic */ tc1<Boolean, te4> a;
        public final /* synthetic */ g63 b;

        /* JADX WARN: Multi-variable type inference failed */
        public a(tc1<? super Boolean, te4> tc1Var, g63 g63Var) {
            this.a = tc1Var;
            this.b = g63Var;
        }

        @Override // defpackage.wu
        public void a(b<DeviceModel> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            tc1<Boolean, te4> tc1Var = this.a;
            if (tc1Var != null) {
                tc1Var.invoke(Boolean.FALSE);
            }
            String localizedMessage = th.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = th.getMessage();
            }
            if (localizedMessage == null) {
                return;
            }
            String str = this.b.c;
            fs1.e(str, "TAG");
            e30.c0(localizedMessage, str);
        }

        @Override // defpackage.wu
        public void b(b<DeviceModel> bVar, n<DeviceModel> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            try {
                tc1<Boolean, te4> tc1Var = this.a;
                if (tc1Var != null) {
                    tc1Var.invoke(Boolean.TRUE);
                }
                String valueOf = String.valueOf(nVar.a());
                String str = this.b.c;
                fs1.e(str, "TAG");
                e30.c0(valueOf, str);
            } catch (Exception e) {
                tc1<Boolean, te4> tc1Var2 = this.a;
                if (tc1Var2 != null) {
                    tc1Var2.invoke(Boolean.FALSE);
                }
                String localizedMessage = e.getLocalizedMessage();
                if (localizedMessage == null) {
                    localizedMessage = e.getMessage();
                }
                if (localizedMessage == null) {
                    return;
                }
                String str2 = this.b.c;
                fs1.e(str2, "TAG");
                e30.c0(localizedMessage, str2);
            }
        }
    }

    public g63(ac3 ac3Var, Context context) {
        fs1.f(ac3Var, "api");
        fs1.f(context, "context");
        this.a = ac3Var;
        this.b = context;
        this.c = g63.class.getSimpleName();
    }

    @Override // defpackage.pm1
    public void a(String str, String[] strArr, tc1<? super Boolean, te4> tc1Var) {
        fs1.f(str, "deviceToken");
        fs1.f(strArr, "_xAddress");
        for (String str2 : strArr) {
            if (str2.length() > 0) {
                if (str.length() > 0) {
                    this.a.c(new SingleFCMRegister(str, str2, null, b30.g(b30.a, this.b, null, 2, null), u21.a.a().getSymbol(), Boolean.valueOf(bo3.e(this.b, "FCM_TOKEN", true)), 4, null)).n(new a(tc1Var, this));
                }
            }
        }
    }
}
