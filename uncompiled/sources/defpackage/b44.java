package defpackage;

import java8.util.s;
import java8.util.stream.e;

/* compiled from: TerminalOp.java */
/* renamed from: b44  reason: default package */
/* loaded from: classes2.dex */
public interface b44<E_IN, R> {
    <P_IN> R a(e<E_IN> eVar, s<P_IN> sVar);

    <P_IN> R e(e<E_IN> eVar, s<P_IN> sVar);

    int f();
}
