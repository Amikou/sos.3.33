package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import com.github.mikephil.charting.utils.Utils;
import defpackage.g83;

/* compiled from: TextAppearance.java */
/* renamed from: d44  reason: default package */
/* loaded from: classes2.dex */
public class d44 {
    public final ColorStateList a;
    public final ColorStateList b;
    public final String c;
    public final int d;
    public final int e;
    public final float f;
    public final float g;
    public final float h;
    public final boolean i;
    public final float j;
    public float k;
    public final int l;
    public boolean m = false;
    public Typeface n;

    /* compiled from: TextAppearance.java */
    /* renamed from: d44$a */
    /* loaded from: classes2.dex */
    public class a extends g83.d {
        public final /* synthetic */ f44 a;

        public a(f44 f44Var) {
            this.a = f44Var;
        }

        @Override // defpackage.g83.d
        public void d(int i) {
            d44.this.m = true;
            this.a.a(i);
        }

        @Override // defpackage.g83.d
        public void e(Typeface typeface) {
            d44 d44Var = d44.this;
            d44Var.n = Typeface.create(typeface, d44Var.d);
            d44.this.m = true;
            this.a.b(d44.this.n, false);
        }
    }

    /* compiled from: TextAppearance.java */
    /* renamed from: d44$b */
    /* loaded from: classes2.dex */
    public class b extends f44 {
        public final /* synthetic */ TextPaint a;
        public final /* synthetic */ f44 b;

        public b(TextPaint textPaint, f44 f44Var) {
            this.a = textPaint;
            this.b = f44Var;
        }

        @Override // defpackage.f44
        public void a(int i) {
            this.b.a(i);
        }

        @Override // defpackage.f44
        public void b(Typeface typeface, boolean z) {
            d44.this.l(this.a, typeface);
            this.b.b(typeface, z);
        }
    }

    public d44(Context context, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, o23.TextAppearance);
        this.k = obtainStyledAttributes.getDimension(o23.TextAppearance_android_textSize, Utils.FLOAT_EPSILON);
        this.a = n42.b(context, obtainStyledAttributes, o23.TextAppearance_android_textColor);
        n42.b(context, obtainStyledAttributes, o23.TextAppearance_android_textColorHint);
        n42.b(context, obtainStyledAttributes, o23.TextAppearance_android_textColorLink);
        this.d = obtainStyledAttributes.getInt(o23.TextAppearance_android_textStyle, 0);
        this.e = obtainStyledAttributes.getInt(o23.TextAppearance_android_typeface, 1);
        int e = n42.e(obtainStyledAttributes, o23.TextAppearance_fontFamily, o23.TextAppearance_android_fontFamily);
        this.l = obtainStyledAttributes.getResourceId(e, 0);
        this.c = obtainStyledAttributes.getString(e);
        obtainStyledAttributes.getBoolean(o23.TextAppearance_textAllCaps, false);
        this.b = n42.b(context, obtainStyledAttributes, o23.TextAppearance_android_shadowColor);
        this.f = obtainStyledAttributes.getFloat(o23.TextAppearance_android_shadowDx, Utils.FLOAT_EPSILON);
        this.g = obtainStyledAttributes.getFloat(o23.TextAppearance_android_shadowDy, Utils.FLOAT_EPSILON);
        this.h = obtainStyledAttributes.getFloat(o23.TextAppearance_android_shadowRadius, Utils.FLOAT_EPSILON);
        obtainStyledAttributes.recycle();
        if (Build.VERSION.SDK_INT >= 21) {
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(i, o23.MaterialTextAppearance);
            int i2 = o23.MaterialTextAppearance_android_letterSpacing;
            this.i = obtainStyledAttributes2.hasValue(i2);
            this.j = obtainStyledAttributes2.getFloat(i2, Utils.FLOAT_EPSILON);
            obtainStyledAttributes2.recycle();
            return;
        }
        this.i = false;
        this.j = Utils.FLOAT_EPSILON;
    }

    public final void d() {
        String str;
        if (this.n == null && (str = this.c) != null) {
            this.n = Typeface.create(str, this.d);
        }
        if (this.n == null) {
            int i = this.e;
            if (i == 1) {
                this.n = Typeface.SANS_SERIF;
            } else if (i == 2) {
                this.n = Typeface.SERIF;
            } else if (i != 3) {
                this.n = Typeface.DEFAULT;
            } else {
                this.n = Typeface.MONOSPACE;
            }
            this.n = Typeface.create(this.n, this.d);
        }
    }

    public Typeface e() {
        d();
        return this.n;
    }

    public Typeface f(Context context) {
        if (this.m) {
            return this.n;
        }
        if (!context.isRestricted()) {
            try {
                Typeface g = g83.g(context, this.l);
                this.n = g;
                if (g != null) {
                    this.n = Typeface.create(g, this.d);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException unused) {
            } catch (Exception unused2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Error loading font ");
                sb.append(this.c);
            }
        }
        d();
        this.m = true;
        return this.n;
    }

    public void g(Context context, f44 f44Var) {
        if (i(context)) {
            f(context);
        } else {
            d();
        }
        int i = this.l;
        if (i == 0) {
            this.m = true;
        }
        if (this.m) {
            f44Var.b(this.n, true);
            return;
        }
        try {
            g83.i(context, i, new a(f44Var), null);
        } catch (Resources.NotFoundException unused) {
            this.m = true;
            f44Var.a(1);
        } catch (Exception unused2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error loading font ");
            sb.append(this.c);
            this.m = true;
            f44Var.a(-3);
        }
    }

    public void h(Context context, TextPaint textPaint, f44 f44Var) {
        l(textPaint, e());
        g(context, new b(textPaint, f44Var));
    }

    public final boolean i(Context context) {
        if (e44.a()) {
            return true;
        }
        int i = this.l;
        return (i != 0 ? g83.c(context, i) : null) != null;
    }

    public void j(Context context, TextPaint textPaint, f44 f44Var) {
        k(context, textPaint, f44Var);
        ColorStateList colorStateList = this.a;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f = this.h;
        float f2 = this.f;
        float f3 = this.g;
        ColorStateList colorStateList2 = this.b;
        textPaint.setShadowLayer(f, f2, f3, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    public void k(Context context, TextPaint textPaint, f44 f44Var) {
        if (i(context)) {
            l(textPaint, f(context));
        } else {
            h(context, textPaint, f44Var);
        }
    }

    public void l(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int i = (~typeface.getStyle()) & this.d;
        textPaint.setFakeBoldText((i & 1) != 0);
        textPaint.setTextSkewX((i & 2) != 0 ? -0.25f : Utils.FLOAT_EPSILON);
        textPaint.setTextSize(this.k);
        if (Build.VERSION.SDK_INT < 21 || !this.i) {
            return;
        }
        textPaint.setLetterSpacing(this.j);
    }
}
