package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;

/* compiled from: CardViewImpl.java */
/* renamed from: ew  reason: default package */
/* loaded from: classes.dex */
public interface ew {
    void a(dw dwVar, float f);

    float b(dw dwVar);

    void c(dw dwVar);

    void d(dw dwVar);

    float e(dw dwVar);

    ColorStateList f(dw dwVar);

    float g(dw dwVar);

    void h(dw dwVar, float f);

    float i(dw dwVar);

    void j(dw dwVar);

    float k(dw dwVar);

    void l();

    void m(dw dwVar, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    void n(dw dwVar, ColorStateList colorStateList);

    void o(dw dwVar, float f);
}
