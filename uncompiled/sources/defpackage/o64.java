package defpackage;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintableCompoundDrawablesView.java */
/* renamed from: o64  reason: default package */
/* loaded from: classes.dex */
public interface o64 {
    void setSupportCompoundDrawablesTintList(ColorStateList colorStateList);

    void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode);
}
