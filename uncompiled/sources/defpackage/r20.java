package defpackage;

import java.util.List;

/* renamed from: r20  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class r20 implements jp {
    public static final r20 a = new r20();

    public static jp a() {
        return a;
    }

    @Override // defpackage.fp
    public Object apply(Object obj, Object obj2) {
        return ((List) obj).addAll((List) obj2);
    }
}
