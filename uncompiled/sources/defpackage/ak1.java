package defpackage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: Hash.java */
/* renamed from: ak1  reason: default package */
/* loaded from: classes3.dex */
public class ak1 {
    private ak1() {
    }

    public static byte[] blake2b256(byte[] bArr) {
        return new uq().digest(bArr);
    }

    public static byte[] hash(byte[] bArr, String str) {
        try {
            return MessageDigest.getInstance(str.toUpperCase()).digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Couldn't find a " + str + " provider", e);
        }
    }

    public static byte[] hmacSha512(byte[] bArr, byte[] bArr2) {
        hj1 hj1Var = new hj1(new na3());
        hj1Var.d(new kx1(bArr));
        hj1Var.b(bArr2, 0, bArr2.length);
        byte[] bArr3 = new byte[64];
        hj1Var.a(bArr3, 0);
        return bArr3;
    }

    public static byte[] sha256(byte[] bArr) {
        try {
            return MessageDigest.getInstance("SHA-256").digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Couldn't find a SHA-256 provider", e);
        }
    }

    public static byte[] sha256hash160(byte[] bArr) {
        byte[] sha256 = sha256(bArr);
        e33 e33Var = new e33();
        e33Var.b(sha256, 0, sha256.length);
        byte[] bArr2 = new byte[20];
        e33Var.a(bArr2, 0);
        return bArr2;
    }

    public static String sha3(String str) {
        return ej2.toHexString(sha3(ej2.hexStringToByteArray(str)));
    }

    public static String sha3String(String str) {
        return ej2.toHexString(sha3(str.getBytes(m30.UTF_8)));
    }

    public static byte[] sha3(byte[] bArr, int i, int i2) {
        bx1 bx1Var = new bx1();
        bx1Var.update(bArr, i, i2);
        return bx1Var.digest();
    }

    public static byte[] sha3(byte[] bArr) {
        return sha3(bArr, 0, bArr.length);
    }
}
