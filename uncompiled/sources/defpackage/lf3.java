package defpackage;

import defpackage.pt0;

/* renamed from: lf3  reason: default package */
/* loaded from: classes2.dex */
public class lf3 extends pt0.c {
    public lf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public lf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public lf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        kf3 kf3Var = (kf3) this.c;
        if (kf3Var.i()) {
            return i.v();
        }
        kf3 kf3Var2 = (kf3) this.b;
        kf3 kf3Var3 = (kf3) this.d[0];
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        int[] h3 = ed2.h();
        jf3.j(kf3Var.f, h3);
        int[] h4 = ed2.h();
        jf3.j(h3, h4);
        boolean h5 = kf3Var3.h();
        int[] iArr = kf3Var3.f;
        if (!h5) {
            jf3.j(iArr, h2);
            iArr = h2;
        }
        jf3.m(kf3Var2.f, iArr, h);
        jf3.a(kf3Var2.f, iArr, h2);
        jf3.e(h2, h, h2);
        jf3.i(ed2.b(h2, h2, h2), h2);
        jf3.e(h3, kf3Var2.f, h3);
        jf3.i(kd2.G(8, h3, 2, 0), h3);
        jf3.i(kd2.H(8, h4, 3, 0, h), h);
        kf3 kf3Var4 = new kf3(h4);
        jf3.j(h2, kf3Var4.f);
        int[] iArr2 = kf3Var4.f;
        jf3.m(iArr2, h3, iArr2);
        int[] iArr3 = kf3Var4.f;
        jf3.m(iArr3, h3, iArr3);
        kf3 kf3Var5 = new kf3(h3);
        jf3.m(h3, kf3Var4.f, kf3Var5.f);
        int[] iArr4 = kf3Var5.f;
        jf3.e(iArr4, h2, iArr4);
        int[] iArr5 = kf3Var5.f;
        jf3.m(iArr5, h, iArr5);
        kf3 kf3Var6 = new kf3(h2);
        jf3.n(kf3Var.f, kf3Var6.f);
        if (!h5) {
            int[] iArr6 = kf3Var6.f;
            jf3.e(iArr6, kf3Var3.f, iArr6);
        }
        return new lf3(i, kf3Var4, kf3Var5, new ct0[]{kf3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        kf3 kf3Var = (kf3) this.b;
        kf3 kf3Var2 = (kf3) this.c;
        kf3 kf3Var3 = (kf3) pt0Var.q();
        kf3 kf3Var4 = (kf3) pt0Var.r();
        kf3 kf3Var5 = (kf3) this.d[0];
        kf3 kf3Var6 = (kf3) pt0Var.s(0);
        int[] j = ed2.j();
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        int[] h3 = ed2.h();
        boolean h4 = kf3Var5.h();
        if (h4) {
            iArr = kf3Var3.f;
            iArr2 = kf3Var4.f;
        } else {
            jf3.j(kf3Var5.f, h2);
            jf3.e(h2, kf3Var3.f, h);
            jf3.e(h2, kf3Var5.f, h2);
            jf3.e(h2, kf3Var4.f, h2);
            iArr = h;
            iArr2 = h2;
        }
        boolean h5 = kf3Var6.h();
        if (h5) {
            iArr3 = kf3Var.f;
            iArr4 = kf3Var2.f;
        } else {
            jf3.j(kf3Var6.f, h3);
            jf3.e(h3, kf3Var.f, j);
            jf3.e(h3, kf3Var6.f, h3);
            jf3.e(h3, kf3Var2.f, h3);
            iArr3 = j;
            iArr4 = h3;
        }
        int[] h6 = ed2.h();
        jf3.m(iArr3, iArr, h6);
        jf3.m(iArr4, iArr2, h);
        if (ed2.v(h6)) {
            return ed2.v(h) ? J() : i.v();
        }
        jf3.j(h6, h2);
        int[] h7 = ed2.h();
        jf3.e(h2, h6, h7);
        jf3.e(h2, iArr3, h2);
        jf3.g(h7, h7);
        ed2.y(iArr4, h7, j);
        jf3.i(ed2.b(h2, h2, h7), h7);
        kf3 kf3Var7 = new kf3(h3);
        jf3.j(h, kf3Var7.f);
        int[] iArr5 = kf3Var7.f;
        jf3.m(iArr5, h7, iArr5);
        kf3 kf3Var8 = new kf3(h7);
        jf3.m(h2, kf3Var7.f, kf3Var8.f);
        jf3.f(kf3Var8.f, h, j);
        jf3.h(j, kf3Var8.f);
        kf3 kf3Var9 = new kf3(h6);
        if (!h4) {
            int[] iArr6 = kf3Var9.f;
            jf3.e(iArr6, kf3Var5.f, iArr6);
        }
        if (!h5) {
            int[] iArr7 = kf3Var9.f;
            jf3.e(iArr7, kf3Var6.f, iArr7);
        }
        return new lf3(i, kf3Var7, kf3Var8, new ct0[]{kf3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new lf3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new lf3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
