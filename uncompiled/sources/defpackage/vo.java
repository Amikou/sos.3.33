package defpackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.util.c;
import java.util.Iterator;

/* compiled from: BeanPropertyDefinition.java */
/* renamed from: vo  reason: default package */
/* loaded from: classes.dex */
public abstract class vo {
    public static final JsonInclude.Value a = JsonInclude.Value.empty();

    public abstract boolean A();

    public abstract boolean B();

    public abstract boolean C();

    public boolean D(PropertyName propertyName) {
        return o().equals(propertyName);
    }

    public abstract boolean E();

    public abstract boolean G();

    public boolean H() {
        return G();
    }

    public boolean I() {
        return false;
    }

    public boolean a() {
        return s() != null;
    }

    public boolean d() {
        return j() != null;
    }

    public JsonInclude.Value e() {
        return a;
    }

    public jl2 f() {
        return null;
    }

    public AnnotationIntrospector.ReferenceProperty g() {
        return null;
    }

    public Class<?>[] h() {
        return null;
    }

    public abstract AnnotatedMember j();

    public Iterator<AnnotatedParameter> k() {
        return c.k();
    }

    public abstract AnnotatedField l();

    public abstract PropertyName o();

    public abstract AnnotatedMethod p();

    public abstract PropertyMetadata r();

    public abstract AnnotatedMember s();

    public abstract String t();

    public abstract AnnotatedMember u();

    public abstract AnnotatedMember v();

    public abstract AnnotatedMethod x();

    public abstract PropertyName y();
}
