package defpackage;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IInterface;

/* renamed from: pt4  reason: default package */
/* loaded from: classes2.dex */
public final class pt4 extends jt4 {
    public final /* synthetic */ zt4 f0;

    public pt4(zt4 zt4Var) {
        this.f0 = zt4Var;
    }

    @Override // defpackage.jt4
    public final void a() {
        IInterface iInterface;
        it4 it4Var;
        Context context;
        ServiceConnection serviceConnection;
        iInterface = this.f0.k;
        if (iInterface != null) {
            it4Var = this.f0.b;
            it4Var.d("Unbind from service.", new Object[0]);
            context = this.f0.a;
            serviceConnection = this.f0.j;
            context.unbindService(serviceConnection);
            this.f0.e = false;
            this.f0.k = null;
            this.f0.j = null;
        }
    }
}
