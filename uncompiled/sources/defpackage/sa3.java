package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: sa3  reason: default package */
/* loaded from: classes2.dex */
public class sa3 extends ct0.b {
    public static final BigInteger g = qa3.j;
    public int[] f;

    public sa3() {
        this.f = ed2.h();
    }

    public sa3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SM2P256V1FieldElement");
        }
        this.f = ra3.d(bigInteger);
    }

    public sa3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] h = ed2.h();
        ra3.a(this.f, ((sa3) ct0Var).f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] h = ed2.h();
        ra3.b(this.f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] h = ed2.h();
        g92.d(ra3.a, ((sa3) ct0Var).f, h);
        ra3.e(h, this.f, h);
        return new sa3(h);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof sa3) {
            return ed2.m(this.f, ((sa3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] h = ed2.h();
        g92.d(ra3.a, this.f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ed2.t(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 8);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ed2.v(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] h = ed2.h();
        ra3.e(this.f, ((sa3) ct0Var).f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] h = ed2.h();
        ra3.g(this.f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (ed2.v(iArr) || ed2.t(iArr)) {
            return this;
        }
        int[] h = ed2.h();
        ra3.j(iArr, h);
        ra3.e(h, iArr, h);
        int[] h2 = ed2.h();
        ra3.k(h, 2, h2);
        ra3.e(h2, h, h2);
        int[] h3 = ed2.h();
        ra3.k(h2, 2, h3);
        ra3.e(h3, h, h3);
        ra3.k(h3, 6, h);
        ra3.e(h, h3, h);
        int[] h4 = ed2.h();
        ra3.k(h, 12, h4);
        ra3.e(h4, h, h4);
        ra3.k(h4, 6, h);
        ra3.e(h, h3, h);
        ra3.j(h, h3);
        ra3.e(h3, iArr, h3);
        ra3.k(h3, 31, h4);
        ra3.e(h4, h3, h);
        ra3.k(h4, 32, h4);
        ra3.e(h4, h, h4);
        ra3.k(h4, 62, h4);
        ra3.e(h4, h, h4);
        ra3.k(h4, 4, h4);
        ra3.e(h4, h2, h4);
        ra3.k(h4, 32, h4);
        ra3.e(h4, iArr, h4);
        ra3.k(h4, 62, h4);
        ra3.j(h4, h2);
        if (ed2.m(iArr, h2)) {
            return new sa3(h4);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] h = ed2.h();
        ra3.j(this.f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] h = ed2.h();
        ra3.m(this.f, ((sa3) ct0Var).f, h);
        return new sa3(h);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return ed2.q(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ed2.J(this.f);
    }
}
