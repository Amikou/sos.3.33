package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.play.core.internal.p;

/* renamed from: et4  reason: default package */
/* loaded from: classes2.dex */
public final class et4 extends jt4 {
    public final /* synthetic */ int f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ tx4 h0;
    public final /* synthetic */ int i0;
    public final /* synthetic */ st4 j0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public et4(st4 st4Var, tx4 tx4Var, int i, String str, tx4 tx4Var2, int i2) {
        super(tx4Var);
        this.j0 = st4Var;
        this.f0 = i;
        this.g0 = str;
        this.h0 = tx4Var2;
        this.i0 = i2;
    }

    @Override // defpackage.jt4
    public final void a() {
        it4 it4Var;
        zt4 zt4Var;
        String str;
        Bundle h;
        Bundle j;
        try {
            zt4Var = this.j0.c;
            str = this.j0.a;
            h = st4.h(this.f0, this.g0);
            j = st4.j();
            ((p) zt4Var.c()).n(str, h, j, new ot4(this.j0, this.h0, this.f0, this.g0, this.i0));
        } catch (RemoteException e) {
            it4Var = st4.f;
            it4Var.c(e, "notifyModuleCompleted", new Object[0]);
        }
    }
}
