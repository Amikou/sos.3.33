package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Scopes.kt */
/* renamed from: vd3  reason: default package */
/* loaded from: classes2.dex */
public class vd3<T> extends t4<T> implements e90 {
    public final q70<T> g0;

    /* JADX WARN: Multi-variable type inference failed */
    public vd3(CoroutineContext coroutineContext, q70<? super T> q70Var) {
        super(coroutineContext, true, true);
        this.g0 = q70Var;
    }

    @Override // defpackage.bu1
    public void E(Object obj) {
        op0.c(IntrinsicsKt__IntrinsicsJvmKt.c(this.g0), w30.a(obj, this.g0), null, 2, null);
    }

    @Override // defpackage.t4
    public void H0(Object obj) {
        q70<T> q70Var = this.g0;
        q70Var.resumeWith(w30.a(obj, q70Var));
    }

    public final st1 L0() {
        fy Z = Z();
        if (Z == null) {
            return null;
        }
        return Z.getParent();
    }

    @Override // defpackage.bu1
    public final boolean f0() {
        return true;
    }

    @Override // defpackage.e90
    public final e90 getCallerFrame() {
        q70<T> q70Var = this.g0;
        if (q70Var instanceof e90) {
            return (e90) q70Var;
        }
        return null;
    }

    @Override // defpackage.e90
    public final StackTraceElement getStackTraceElement() {
        return null;
    }
}
