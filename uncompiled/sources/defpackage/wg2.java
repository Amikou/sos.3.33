package defpackage;

import android.graphics.drawable.Drawable;

/* compiled from: NonOwnedDrawableResource.java */
/* renamed from: wg2  reason: default package */
/* loaded from: classes.dex */
public final class wg2 extends yq0<Drawable> {
    public wg2(Drawable drawable) {
        super(drawable);
    }

    public static s73<Drawable> f(Drawable drawable) {
        if (drawable != null) {
            return new wg2(drawable);
        }
        return null;
    }

    @Override // defpackage.s73
    public int a() {
        return Math.max(1, this.a.getIntrinsicWidth() * this.a.getIntrinsicHeight() * 4);
    }

    @Override // defpackage.s73
    public void b() {
    }

    @Override // defpackage.s73
    public Class<Drawable> d() {
        return this.a.getClass();
    }
}
