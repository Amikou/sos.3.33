package defpackage;

import java.util.Random;

/* compiled from: PlatformRandom.kt */
/* renamed from: d21  reason: default package */
/* loaded from: classes2.dex */
public final class d21 extends f5 {
    public final a f0 = new a();

    /* compiled from: PlatformRandom.kt */
    /* renamed from: d21$a */
    /* loaded from: classes2.dex */
    public static final class a extends ThreadLocal<Random> {
        @Override // java.lang.ThreadLocal
        /* renamed from: a */
        public Random initialValue() {
            return new Random();
        }
    }

    @Override // defpackage.f5
    public Random getImpl() {
        Random random = this.f0.get();
        fs1.e(random, "implStorage.get()");
        return random;
    }
}
