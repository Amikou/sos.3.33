package defpackage;

import zendesk.support.request.CellBase;

/* compiled from: SingleSegmentIndex.java */
/* renamed from: yp3  reason: default package */
/* loaded from: classes.dex */
public final class yp3 implements wd0 {
    public final s33 a;

    public yp3(s33 s33Var) {
        this.a = s33Var;
    }

    @Override // defpackage.wd0
    public long b(long j) {
        return 0L;
    }

    @Override // defpackage.wd0
    public long c(long j, long j2) {
        return j2;
    }

    @Override // defpackage.wd0
    public long d(long j, long j2) {
        return 0L;
    }

    @Override // defpackage.wd0
    public long e(long j, long j2) {
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.wd0
    public s33 f(long j) {
        return this.a;
    }

    @Override // defpackage.wd0
    public long g(long j, long j2) {
        return 0L;
    }

    @Override // defpackage.wd0
    public boolean h() {
        return true;
    }

    @Override // defpackage.wd0
    public long i() {
        return 0L;
    }

    @Override // defpackage.wd0
    public long j(long j) {
        return 1L;
    }

    @Override // defpackage.wd0
    public long k(long j, long j2) {
        return 1L;
    }
}
