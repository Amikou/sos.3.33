package defpackage;

/* compiled from: SettingsRequest.java */
/* renamed from: ln3  reason: default package */
/* loaded from: classes2.dex */
public class ln3 {
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final dr1 e;
    public final String f;
    public final String g;
    public final String h;
    public final int i;

    public ln3(String str, String str2, String str3, String str4, dr1 dr1Var, String str5, String str6, String str7, int i) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = dr1Var;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = i;
    }
}
