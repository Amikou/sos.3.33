package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jn5  reason: default package */
/* loaded from: classes.dex */
public final class jn5 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ Object g0;
    public final /* synthetic */ long h0;
    public final /* synthetic */ dp5 i0;

    public jn5(dp5 dp5Var, String str, String str2, Object obj, long j) {
        this.i0 = dp5Var;
        this.a = str;
        this.f0 = str2;
        this.g0 = obj;
        this.h0 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.i0.n(this.a, this.f0, this.g0, this.h0);
    }
}
