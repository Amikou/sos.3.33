package defpackage;

import java.io.InputStream;

/* renamed from: bv4  reason: default package */
/* loaded from: classes2.dex */
public final class bv4 extends fw4 {
    public final int c;
    public final long d;
    public final String e;
    public final int f;
    public final int g;
    public final int h;
    public final long i;
    public final int j;
    public final InputStream k;

    public bv4(int i, String str, int i2, long j, String str2, int i3, int i4, int i5, long j2, int i6, InputStream inputStream) {
        super(i, str);
        this.c = i2;
        this.d = j;
        this.e = str2;
        this.f = i3;
        this.g = i4;
        this.h = i5;
        this.i = j2;
        this.j = i6;
        this.k = inputStream;
    }

    public final boolean a() {
        return this.g + 1 == this.h;
    }
}
