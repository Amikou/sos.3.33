package defpackage;

import java.util.Queue;
import org.slf4j.Marker;
import org.slf4j.event.Level;

/* compiled from: EventRecodingLogger.java */
/* renamed from: cy0  reason: default package */
/* loaded from: classes2.dex */
public class cy0 implements x12 {
    public String a;
    public nv3 f0;
    public Queue<pv3> g0;

    public cy0(nv3 nv3Var, Queue<pv3> queue) {
        this.f0 = nv3Var;
        this.a = nv3Var.getName();
        this.g0 = queue;
    }

    public final void a(Level level, String str, Object[] objArr, Throwable th) {
        b(level, null, str, objArr, th);
    }

    public final void b(Level level, Marker marker, String str, Object[] objArr, Throwable th) {
        pv3 pv3Var = new pv3();
        pv3Var.j(System.currentTimeMillis());
        pv3Var.c(level);
        pv3Var.d(this.f0);
        pv3Var.e(this.a);
        pv3Var.f(marker);
        pv3Var.g(str);
        pv3Var.b(objArr);
        pv3Var.i(th);
        pv3Var.h(Thread.currentThread().getName());
        this.g0.add(pv3Var);
    }

    @Override // defpackage.x12
    public void debug(String str) {
        a(Level.TRACE, str, null, null);
    }

    @Override // defpackage.x12
    public void error(String str) {
        a(Level.ERROR, str, null, null);
    }

    @Override // defpackage.x12
    public String getName() {
        return this.a;
    }

    @Override // defpackage.x12
    public void info(String str) {
        a(Level.INFO, str, null, null);
    }

    @Override // defpackage.x12
    public boolean isDebugEnabled() {
        return true;
    }

    @Override // defpackage.x12
    public void warn(String str) {
        a(Level.WARN, str, null, null);
    }

    @Override // defpackage.x12
    public void error(String str, Throwable th) {
        a(Level.ERROR, str, null, th);
    }

    @Override // defpackage.x12
    public void info(String str, Object obj) {
        a(Level.INFO, str, new Object[]{obj}, null);
    }
}
