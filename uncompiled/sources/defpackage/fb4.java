package defpackage;

/* compiled from: Transformer.java */
/* renamed from: fb4  reason: default package */
/* loaded from: classes.dex */
public interface fb4<T, U> {
    U apply(T t);
}
