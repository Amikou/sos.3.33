package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import zendesk.support.request.CellBase;

/* compiled from: DtsReader.java */
/* renamed from: gs0  reason: default package */
/* loaded from: classes.dex */
public final class gs0 implements ku0 {
    public final String b;
    public String c;
    public f84 d;
    public int f;
    public int g;
    public long h;
    public j i;
    public int j;
    public final op2 a = new op2(new byte[18]);
    public int e = 0;
    public long k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    public gs0(String str) {
        this.b = str;
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        ii.i(this.d);
        while (op2Var.a() > 0) {
            int i = this.e;
            if (i != 0) {
                if (i != 1) {
                    if (i == 2) {
                        int min = Math.min(op2Var.a(), this.j - this.f);
                        this.d.a(op2Var, min);
                        int i2 = this.f + min;
                        this.f = i2;
                        int i3 = this.j;
                        if (i2 == i3) {
                            long j = this.k;
                            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                this.d.b(j, 1, i3, 0, null);
                                this.k += this.h;
                            }
                            this.e = 0;
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                } else if (b(op2Var, this.a.d(), 18)) {
                    g();
                    this.a.P(0);
                    this.d.a(this.a, 18);
                    this.e = 2;
                }
            } else if (h(op2Var)) {
                this.e = 1;
            }
        }
    }

    public final boolean b(op2 op2Var, byte[] bArr, int i) {
        int min = Math.min(op2Var.a(), i - this.f);
        op2Var.j(bArr, this.f, min);
        int i2 = this.f + min;
        this.f = i2;
        return i2 == i;
    }

    @Override // defpackage.ku0
    public void c() {
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.k = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.c = dVar.b();
        this.d = r11Var.f(dVar.c(), 1);
    }

    public final void g() {
        byte[] d = this.a.d();
        if (this.i == null) {
            j g = hs0.g(d, this.c, this.b, null);
            this.i = g;
            this.d.f(g);
        }
        this.j = hs0.a(d);
        this.h = (int) ((hs0.f(d) * 1000000) / this.i.D0);
    }

    public final boolean h(op2 op2Var) {
        while (op2Var.a() > 0) {
            int i = this.g << 8;
            this.g = i;
            int D = i | op2Var.D();
            this.g = D;
            if (hs0.d(D)) {
                byte[] d = this.a.d();
                int i2 = this.g;
                d[0] = (byte) ((i2 >> 24) & 255);
                d[1] = (byte) ((i2 >> 16) & 255);
                d[2] = (byte) ((i2 >> 8) & 255);
                d[3] = (byte) (i2 & 255);
                this.f = 4;
                this.g = 0;
                return true;
            }
        }
        return false;
    }
}
