package defpackage;

import com.google.android.gms.internal.measurement.n2;
import com.google.android.gms.internal.measurement.r1;
import com.google.android.gms.internal.measurement.z1;
import com.google.android.gms.internal.measurement.zzkn;
import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: cw5  reason: default package */
/* loaded from: classes.dex */
public final class cw5 {
    public static final Charset a = Charset.forName("UTF-8");
    public static final byte[] b;

    static {
        Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING);
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        try {
            new r1(bArr, 0, 0, false, null).c(0);
        } catch (zzkn e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static <T> T a(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static <T> T b(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static boolean c(byte[] bArr) {
        return n2.a(bArr);
    }

    public static String d(byte[] bArr) {
        return new String(bArr, a);
    }

    public static int e(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static int f(boolean z) {
        return z ? 1231 : 1237;
    }

    public static int g(byte[] bArr) {
        int length = bArr.length;
        int h = h(length, bArr, 0, length);
        if (h == 0) {
            return 1;
        }
        return h;
    }

    public static int h(int i, byte[] bArr, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            i = (i * 31) + bArr[i4];
        }
        return i;
    }

    public static Object i(Object obj, Object obj2) {
        return ((z1) obj).b().i0((z1) obj2).q();
    }
}
