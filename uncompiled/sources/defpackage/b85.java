package defpackage;

import com.google.android.gms.internal.clearcut.zzbb;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: b85  reason: default package */
/* loaded from: classes.dex */
public final class b85 implements Iterator {
    public int a = 0;
    public final int f0;
    public final /* synthetic */ zzbb g0;

    public b85(zzbb zzbbVar) {
        this.g0 = zzbbVar;
        this.f0 = zzbbVar.size();
    }

    public final byte e() {
        try {
            zzbb zzbbVar = this.g0;
            int i = this.a;
            this.a = i + 1;
            return zzbbVar.zzj(i);
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a < this.f0;
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        return Byte.valueOf(e());
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
