package defpackage;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.drawable.AnimatedImageDrawable;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* compiled from: AnimatedWebpDecoder.java */
/* renamed from: ce  reason: default package */
/* loaded from: classes.dex */
public final class ce {
    public final List<ImageHeaderParser> a;
    public final sh b;

    /* compiled from: AnimatedWebpDecoder.java */
    /* renamed from: ce$a */
    /* loaded from: classes.dex */
    public static final class a implements s73<Drawable> {
        public final AnimatedImageDrawable a;

        public a(AnimatedImageDrawable animatedImageDrawable) {
            this.a = animatedImageDrawable;
        }

        @Override // defpackage.s73
        public int a() {
            return this.a.getIntrinsicWidth() * this.a.getIntrinsicHeight() * mg4.i(Bitmap.Config.ARGB_8888) * 2;
        }

        @Override // defpackage.s73
        public void b() {
            this.a.stop();
            this.a.clearAnimationCallbacks();
        }

        @Override // defpackage.s73
        /* renamed from: c */
        public AnimatedImageDrawable get() {
            return this.a;
        }

        @Override // defpackage.s73
        public Class<Drawable> d() {
            return Drawable.class;
        }
    }

    /* compiled from: AnimatedWebpDecoder.java */
    /* renamed from: ce$b */
    /* loaded from: classes.dex */
    public static final class b implements com.bumptech.glide.load.b<ByteBuffer, Drawable> {
        public final ce a;

        public b(ce ceVar) {
            this.a = ceVar;
        }

        @Override // com.bumptech.glide.load.b
        /* renamed from: c */
        public s73<Drawable> b(ByteBuffer byteBuffer, int i, int i2, vn2 vn2Var) throws IOException {
            return this.a.b(ImageDecoder.createSource(byteBuffer), i, i2, vn2Var);
        }

        @Override // com.bumptech.glide.load.b
        /* renamed from: d */
        public boolean a(ByteBuffer byteBuffer, vn2 vn2Var) throws IOException {
            return this.a.d(byteBuffer);
        }
    }

    /* compiled from: AnimatedWebpDecoder.java */
    /* renamed from: ce$c */
    /* loaded from: classes.dex */
    public static final class c implements com.bumptech.glide.load.b<InputStream, Drawable> {
        public final ce a;

        public c(ce ceVar) {
            this.a = ceVar;
        }

        @Override // com.bumptech.glide.load.b
        /* renamed from: c */
        public s73<Drawable> b(InputStream inputStream, int i, int i2, vn2 vn2Var) throws IOException {
            return this.a.b(ImageDecoder.createSource(ts.b(inputStream)), i, i2, vn2Var);
        }

        @Override // com.bumptech.glide.load.b
        /* renamed from: d */
        public boolean a(InputStream inputStream, vn2 vn2Var) throws IOException {
            return this.a.c(inputStream);
        }
    }

    public ce(List<ImageHeaderParser> list, sh shVar) {
        this.a = list;
        this.b = shVar;
    }

    public static com.bumptech.glide.load.b<ByteBuffer, Drawable> a(List<ImageHeaderParser> list, sh shVar) {
        return new b(new ce(list, shVar));
    }

    public static com.bumptech.glide.load.b<InputStream, Drawable> f(List<ImageHeaderParser> list, sh shVar) {
        return new c(new ce(list, shVar));
    }

    public s73<Drawable> b(ImageDecoder.Source source, int i, int i2, vn2 vn2Var) throws IOException {
        Drawable decodeDrawable = ImageDecoder.decodeDrawable(source, new hk0(i, i2, vn2Var));
        if (decodeDrawable instanceof AnimatedImageDrawable) {
            return new a((AnimatedImageDrawable) decodeDrawable);
        }
        throw new IOException("Received unexpected drawable type for animated webp, failing: " + decodeDrawable);
    }

    public boolean c(InputStream inputStream) throws IOException {
        return e(com.bumptech.glide.load.a.f(this.a, inputStream, this.b));
    }

    public boolean d(ByteBuffer byteBuffer) throws IOException {
        return e(com.bumptech.glide.load.a.g(this.a, byteBuffer));
    }

    public final boolean e(ImageHeaderParser.ImageType imageType) {
        return imageType == ImageHeaderParser.ImageType.ANIMATED_WEBP;
    }
}
