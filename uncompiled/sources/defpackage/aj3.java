package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.extractor.metadata.mp4.SlowMotionData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SefReader.java */
/* renamed from: aj3  reason: default package */
/* loaded from: classes.dex */
public final class aj3 {
    public static final yr3 d = yr3.d(':');
    public static final yr3 e = yr3.d('*');
    public final List<a> a = new ArrayList();
    public int b = 0;
    public int c;

    /* compiled from: SefReader.java */
    /* renamed from: aj3$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final long a;
        public final int b;

        public a(int i, long j, int i2) {
            this.a = j;
            this.b = i2;
        }
    }

    public static int b(String str) throws ParserException {
        str.hashCode();
        char c = 65535;
        switch (str.hashCode()) {
            case -1711564334:
                if (str.equals("SlowMotion_Data")) {
                    c = 0;
                    break;
                }
                break;
            case -1332107749:
                if (str.equals("Super_SlowMotion_Edit_Data")) {
                    c = 1;
                    break;
                }
                break;
            case -1251387154:
                if (str.equals("Super_SlowMotion_Data")) {
                    c = 2;
                    break;
                }
                break;
            case -830665521:
                if (str.equals("Super_SlowMotion_Deflickering_On")) {
                    c = 3;
                    break;
                }
                break;
            case 1760745220:
                if (str.equals("Super_SlowMotion_BGM")) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return 2192;
            case 1:
                return 2819;
            case 2:
                return 2816;
            case 3:
                return 2820;
            case 4:
                return 2817;
            default:
                throw ParserException.createForMalformedContainer("Invalid SEF name", null);
        }
    }

    public static SlowMotionData f(op2 op2Var, int i) throws ParserException {
        ArrayList arrayList = new ArrayList();
        List<String> f = e.f(op2Var.A(i));
        for (int i2 = 0; i2 < f.size(); i2++) {
            List<String> f2 = d.f(f.get(i2));
            if (f2.size() == 3) {
                try {
                    arrayList.add(new SlowMotionData.Segment(Long.parseLong(f2.get(0)), Long.parseLong(f2.get(1)), 1 << (Integer.parseInt(f2.get(2)) - 1)));
                } catch (NumberFormatException e2) {
                    throw ParserException.createForMalformedContainer(null, e2);
                }
            } else {
                throw ParserException.createForMalformedContainer(null, null);
            }
        }
        return new SlowMotionData(arrayList);
    }

    public final void a(q11 q11Var, ot2 ot2Var) throws IOException {
        op2 op2Var = new op2(8);
        q11Var.readFully(op2Var.d(), 0, 8);
        this.c = op2Var.q() + 8;
        if (op2Var.n() != 1397048916) {
            ot2Var.a = 0L;
            return;
        }
        ot2Var.a = q11Var.getPosition() - (this.c - 12);
        this.b = 2;
    }

    public int c(q11 q11Var, ot2 ot2Var, List<Metadata.Entry> list) throws IOException {
        int i = this.b;
        long j = 0;
        if (i == 0) {
            long length = q11Var.getLength();
            if (length != -1 && length >= 8) {
                j = length - 8;
            }
            ot2Var.a = j;
            this.b = 1;
        } else if (i == 1) {
            a(q11Var, ot2Var);
        } else if (i == 2) {
            d(q11Var, ot2Var);
        } else if (i == 3) {
            e(q11Var, list);
            ot2Var.a = 0L;
        } else {
            throw new IllegalStateException();
        }
        return 1;
    }

    public final void d(q11 q11Var, ot2 ot2Var) throws IOException {
        long length = q11Var.getLength();
        int i = (this.c - 12) - 8;
        op2 op2Var = new op2(i);
        q11Var.readFully(op2Var.d(), 0, i);
        for (int i2 = 0; i2 < i / 12; i2++) {
            op2Var.Q(2);
            short s = op2Var.s();
            if (s != 2192 && s != 2816 && s != 2817 && s != 2819 && s != 2820) {
                op2Var.Q(8);
            } else {
                this.a.add(new a(s, (length - this.c) - op2Var.q(), op2Var.q()));
            }
        }
        if (this.a.isEmpty()) {
            ot2Var.a = 0L;
            return;
        }
        this.b = 3;
        ot2Var.a = this.a.get(0).a;
    }

    public final void e(q11 q11Var, List<Metadata.Entry> list) throws IOException {
        long position = q11Var.getPosition();
        int length = (int) ((q11Var.getLength() - q11Var.getPosition()) - this.c);
        op2 op2Var = new op2(length);
        q11Var.readFully(op2Var.d(), 0, length);
        for (int i = 0; i < this.a.size(); i++) {
            a aVar = this.a.get(i);
            op2Var.P((int) (aVar.a - position));
            op2Var.Q(4);
            int q = op2Var.q();
            int b = b(op2Var.A(q));
            int i2 = aVar.b - (q + 8);
            if (b == 2192) {
                list.add(f(op2Var, i2));
            } else if (b != 2816 && b != 2817 && b != 2819 && b != 2820) {
                throw new IllegalStateException();
            }
        }
    }

    public void g() {
        this.a.clear();
        this.b = 0;
    }
}
