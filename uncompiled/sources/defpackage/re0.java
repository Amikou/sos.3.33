package defpackage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* compiled from: DateTimeUtils.kt */
/* renamed from: re0  reason: default package */
/* loaded from: classes2.dex */
public final class re0 {
    public static final re0 a = new re0();
    public static final Locale b;
    public static final SimpleDateFormat c;
    public static final SimpleDateFormat d;

    static {
        Locale locale = Locale.US;
        b = locale;
        c = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aa", locale);
        d = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", locale);
    }

    public final String a(long j) {
        String format = c.format(new Date(j));
        fs1.e(format, "FORMAT_DATE_TIME.format(Date(timeStamp))");
        return format;
    }

    public final long b(String str) {
        fs1.f(str, "<this>");
        Date parse = d.parse(str);
        if (parse == null) {
            parse = new Date();
        }
        return parse.getTime();
    }
}
