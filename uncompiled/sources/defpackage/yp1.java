package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: IncludeCurrencyConverterLayoutBinding.java */
/* renamed from: yp1  reason: default package */
/* loaded from: classes2.dex */
public final class yp1 {
    public final LinearLayout a;
    public final ImageView b;
    public final AppCompatImageView c;
    public final AppCompatTextView d;
    public final AppCompatTextView e;
    public final AutofitEdittext f;

    public yp1(FrameLayout frameLayout, LinearLayout linearLayout, ImageView imageView, AppCompatImageView appCompatImageView, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, AutofitEdittext autofitEdittext, LinearLayout linearLayout2) {
        this.a = linearLayout;
        this.b = imageView;
        this.c = appCompatImageView;
        this.d = appCompatTextView;
        this.e = appCompatTextView2;
        this.f = autofitEdittext;
    }

    public static yp1 a(View view) {
        int i = R.id.buttonWrapper;
        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.buttonWrapper);
        if (linearLayout != null) {
            i = R.id.ivClear;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivClear);
            if (imageView != null) {
                i = R.id.ivCopy;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.ivCopy);
                if (appCompatImageView != null) {
                    i = R.id.selectedCurrencyFullName;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.selectedCurrencyFullName);
                    if (appCompatTextView != null) {
                        i = R.id.selectedCurrencyShortName;
                        AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.selectedCurrencyShortName);
                        if (appCompatTextView2 != null) {
                            i = R.id.txtValue;
                            AutofitEdittext autofitEdittext = (AutofitEdittext) ai4.a(view, R.id.txtValue);
                            if (autofitEdittext != null) {
                                i = R.id.txtValueWrapper;
                                LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.txtValueWrapper);
                                if (linearLayout2 != null) {
                                    return new yp1((FrameLayout) view, linearLayout, imageView, appCompatImageView, appCompatTextView, appCompatTextView2, autofitEdittext, linearLayout2);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
