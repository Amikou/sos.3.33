package defpackage;

import android.graphics.drawable.Animatable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ForwardingControllerListener.java */
/* renamed from: c91  reason: default package */
/* loaded from: classes.dex */
public class c91<INFO> implements m80<INFO> {
    public final List<m80<? super INFO>> a = new ArrayList(2);

    @Override // defpackage.m80
    public void a(String str, INFO info2) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                m80<? super INFO> m80Var = this.a.get(i);
                if (m80Var != null) {
                    m80Var.a(str, info2);
                }
            } catch (Exception e) {
                i("InternalListener exception in onIntermediateImageSet", e);
            }
        }
    }

    @Override // defpackage.m80
    public synchronized void b(String str, INFO info2, Animatable animatable) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                m80<? super INFO> m80Var = this.a.get(i);
                if (m80Var != null) {
                    m80Var.b(str, info2, animatable);
                }
            } catch (Exception e) {
                i("InternalListener exception in onFinalImageSet", e);
            }
        }
    }

    @Override // defpackage.m80
    public synchronized void c(String str, Throwable th) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                m80<? super INFO> m80Var = this.a.get(i);
                if (m80Var != null) {
                    m80Var.c(str, th);
                }
            } catch (Exception e) {
                i("InternalListener exception in onFailure", e);
            }
        }
    }

    @Override // defpackage.m80
    public synchronized void d(String str) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                m80<? super INFO> m80Var = this.a.get(i);
                if (m80Var != null) {
                    m80Var.d(str);
                }
            } catch (Exception e) {
                i("InternalListener exception in onRelease", e);
            }
        }
    }

    @Override // defpackage.m80
    public synchronized void e(String str, Object obj) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                m80<? super INFO> m80Var = this.a.get(i);
                if (m80Var != null) {
                    m80Var.e(str, obj);
                }
            } catch (Exception e) {
                i("InternalListener exception in onSubmit", e);
            }
        }
    }

    @Override // defpackage.m80
    public void f(String str, Throwable th) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                m80<? super INFO> m80Var = this.a.get(i);
                if (m80Var != null) {
                    m80Var.f(str, th);
                }
            } catch (Exception e) {
                i("InternalListener exception in onIntermediateImageFailed", e);
            }
        }
    }

    public synchronized void g(m80<? super INFO> m80Var) {
        this.a.add(m80Var);
    }

    public synchronized void h() {
        this.a.clear();
    }

    public final synchronized void i(String str, Throwable th) {
    }
}
