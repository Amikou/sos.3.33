package defpackage;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;
import java8.util.s;
import java8.util.t;

/* compiled from: PBQueueSpliterator.java */
/* renamed from: jo2  reason: default package */
/* loaded from: classes2.dex */
public final class jo2<E> implements s<E> {
    public final PriorityBlockingQueue<E> a;
    public Object[] b;
    public int c;
    public int d;

    public jo2(PriorityBlockingQueue<E> priorityBlockingQueue, Object[] objArr, int i, int i2) {
        this.a = priorityBlockingQueue;
        this.b = objArr;
        this.c = i;
        this.d = i2;
    }

    public static <T> s<T> p(PriorityBlockingQueue<T> priorityBlockingQueue) {
        return new jo2(priorityBlockingQueue, null, 0, -1);
    }

    @Override // java8.util.s
    public void a(m60<? super E> m60Var) {
        rl2.f(m60Var);
        int o = o();
        Object[] objArr = this.b;
        this.c = o;
        for (int i = this.c; i < o; i++) {
            m60Var.accept(objArr[i]);
        }
    }

    @Override // java8.util.s
    public int b() {
        return 16704;
    }

    @Override // java8.util.s
    public boolean d(m60<? super E> m60Var) {
        rl2.f(m60Var);
        int o = o();
        int i = this.c;
        if (o <= i || i < 0) {
            return false;
        }
        Object[] objArr = this.b;
        this.c = i + 1;
        m60Var.accept(objArr[i]);
        return true;
    }

    @Override // java8.util.s
    public Comparator<? super E> f() {
        return t.h(this);
    }

    @Override // java8.util.s
    public boolean h(int i) {
        return t.k(this, i);
    }

    @Override // java8.util.s
    public long i() {
        return t.i(this);
    }

    @Override // java8.util.s
    public long l() {
        return o() - this.c;
    }

    public final int o() {
        if (this.b == null) {
            Object[] array = this.a.toArray();
            this.b = array;
            this.d = array.length;
        }
        return this.d;
    }

    @Override // java8.util.s
    /* renamed from: q */
    public jo2<E> c() {
        int o = o();
        int i = this.c;
        int i2 = (o + i) >>> 1;
        if (i >= i2) {
            return null;
        }
        PriorityBlockingQueue<E> priorityBlockingQueue = this.a;
        Object[] objArr = this.b;
        this.c = i2;
        return new jo2<>(priorityBlockingQueue, objArr, i, i2);
    }
}
