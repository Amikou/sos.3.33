package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: NotificationFragmentDirections.java */
/* renamed from: th2  reason: default package */
/* loaded from: classes2.dex */
public class th2 {

    /* compiled from: NotificationFragmentDirections.java */
    /* renamed from: th2$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("token")) {
                PAToken pAToken = (PAToken) this.a.get("token");
                if (!Parcelable.class.isAssignableFrom(PAToken.class) && pAToken != null) {
                    if (Serializable.class.isAssignableFrom(PAToken.class)) {
                        bundle.putSerializable("token", (Serializable) Serializable.class.cast(pAToken));
                    } else {
                        throw new UnsupportedOperationException(PAToken.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("token", (Parcelable) Parcelable.class.cast(pAToken));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_to_cryptoPriceAlertFragment;
        }

        public PAToken c() {
            return (PAToken) this.a.get("token");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("token") != bVar.a.containsKey("token")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionToCryptoPriceAlertFragment(actionId=" + b() + "){token=" + c() + "}";
        }

        public b(PAToken pAToken) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (pAToken != null) {
                hashMap.put("token", pAToken);
                return;
            }
            throw new IllegalArgumentException("Argument \"token\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(PAToken pAToken) {
        return new b(pAToken);
    }
}
