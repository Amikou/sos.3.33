package defpackage;

import android.animation.IntEvaluator;
import android.animation.PropertyValuesHolder;
import defpackage.xg4;

/* compiled from: ScaleDownAnimation.java */
/* renamed from: jc3  reason: default package */
/* loaded from: classes2.dex */
public class jc3 extends hc3 {
    public jc3(xg4.a aVar) {
        super(aVar);
    }

    @Override // defpackage.hc3
    public PropertyValuesHolder n(boolean z) {
        int i;
        int i2;
        String str;
        if (z) {
            i2 = this.g;
            i = (int) (i2 * this.h);
            str = "ANIMATION_SCALE_REVERSE";
        } else {
            i = this.g;
            i2 = (int) (i * this.h);
            str = "ANIMATION_SCALE";
        }
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt(str, i, i2);
        ofInt.setEvaluator(new IntEvaluator());
        return ofInt;
    }
}
