package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.room.RoomToken;

/* compiled from: IUserTokenDataSource.kt */
/* renamed from: zm1  reason: default package */
/* loaded from: classes2.dex */
public interface zm1 {
    LiveData<List<RoomToken>> a(TokenType tokenType);

    Object b(String str, String str2, q70<? super te4> q70Var);

    void c(String str, double d);

    LiveData<RoomToken> d(String str);

    Object e(String str, q70<? super RoomToken> q70Var);

    void f(String str, double d, double d2);

    boolean g(IToken iToken, TokenType tokenType);

    void h(IToken iToken);

    void i(IToken iToken);

    void j(Pair<String, Integer>... pairArr);

    LiveData<List<RoomToken>> k();
}
