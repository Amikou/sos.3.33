package defpackage;

import defpackage.xs0;
import java.math.BigInteger;
import org.bouncycastle.asn1.a0;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.i;

/* renamed from: or4  reason: default package */
/* loaded from: classes2.dex */
public class or4 extends h implements vr4 {
    public xs0 a;
    public byte[] f0;
    public i g0;

    public or4(tr4 tr4Var, BigInteger bigInteger, BigInteger bigInteger2, h4 h4Var) {
        int intValue;
        int i;
        int i2;
        xs0 eVar;
        this.g0 = null;
        i o = tr4Var.o();
        this.g0 = o;
        if (o.equals(vr4.i)) {
            eVar = new xs0.f(((g) tr4Var.q()).B(), new BigInteger(1, f4.B(h4Var.D(0)).D()), new BigInteger(1, f4.B(h4Var.D(1)).D()), bigInteger, bigInteger2);
        } else if (!this.g0.equals(vr4.j)) {
            throw new IllegalArgumentException("This type of ECCurve is not implemented");
        } else {
            h4 z = h4.z(tr4Var.q());
            int intValue2 = ((g) z.D(0)).B().intValue();
            i iVar = (i) z.D(1);
            if (iVar.equals(vr4.k)) {
                i = g.z(z.D(2)).B().intValue();
                i2 = 0;
                intValue = 0;
            } else if (!iVar.equals(vr4.l)) {
                throw new IllegalArgumentException("This type of EC basis is not implemented");
            } else {
                h4 z2 = h4.z(z.D(2));
                int intValue3 = g.z(z2.D(0)).B().intValue();
                int intValue4 = g.z(z2.D(1)).B().intValue();
                intValue = g.z(z2.D(2)).B().intValue();
                i = intValue3;
                i2 = intValue4;
            }
            eVar = new xs0.e(intValue2, i, i2, intValue, new BigInteger(1, f4.B(h4Var.D(0)).D()), new BigInteger(1, f4.B(h4Var.D(1)).D()), bigInteger, bigInteger2);
        }
        this.a = eVar;
        if (h4Var.size() == 3) {
            this.f0 = ((a0) h4Var.D(2)).D();
        }
    }

    public or4(xs0 xs0Var, byte[] bArr) {
        this.g0 = null;
        this.a = xs0Var;
        this.f0 = bArr;
        q();
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x0060  */
    @Override // org.bouncycastle.asn1.h, defpackage.c4
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public org.bouncycastle.asn1.k i() {
        /*
            r3 = this;
            d4 r0 = new d4
            r0.<init>()
            org.bouncycastle.asn1.i r1 = r3.g0
            org.bouncycastle.asn1.i r2 = defpackage.vr4.i
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L34
            sr4 r1 = new sr4
            xs0 r2 = r3.a
            ct0 r2 = r2.o()
            r1.<init>(r2)
            org.bouncycastle.asn1.k r1 = r1.i()
            r0.a(r1)
            sr4 r1 = new sr4
            xs0 r2 = r3.a
            ct0 r2 = r2.p()
            r1.<init>(r2)
        L2c:
            org.bouncycastle.asn1.k r1 = r1.i()
            r0.a(r1)
            goto L5c
        L34:
            org.bouncycastle.asn1.i r1 = r3.g0
            org.bouncycastle.asn1.i r2 = defpackage.vr4.j
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L5c
            sr4 r1 = new sr4
            xs0 r2 = r3.a
            ct0 r2 = r2.o()
            r1.<init>(r2)
            org.bouncycastle.asn1.k r1 = r1.i()
            r0.a(r1)
            sr4 r1 = new sr4
            xs0 r2 = r3.a
            ct0 r2 = r2.p()
            r1.<init>(r2)
            goto L2c
        L5c:
            byte[] r1 = r3.f0
            if (r1 == 0) goto L6a
            org.bouncycastle.asn1.a0 r1 = new org.bouncycastle.asn1.a0
            byte[] r2 = r3.f0
            r1.<init>(r2)
            r0.a(r1)
        L6a:
            org.bouncycastle.asn1.n0 r1 = new org.bouncycastle.asn1.n0
            r1.<init>(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.or4.i():org.bouncycastle.asn1.k");
    }

    public xs0 o() {
        return this.a;
    }

    public byte[] p() {
        return this.f0;
    }

    public final void q() {
        i iVar;
        if (vs0.l(this.a)) {
            iVar = vr4.i;
        } else if (!vs0.j(this.a)) {
            throw new IllegalArgumentException("This type of ECCurve is not implemented");
        } else {
            iVar = vr4.j;
        }
        this.g0 = iVar;
    }
}
