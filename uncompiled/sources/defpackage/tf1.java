package defpackage;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

/* compiled from: GifDecoder.java */
/* renamed from: tf1  reason: default package */
/* loaded from: classes.dex */
public interface tf1 {

    /* compiled from: GifDecoder.java */
    /* renamed from: tf1$a */
    /* loaded from: classes.dex */
    public interface a {
        Bitmap a(int i, int i2, Bitmap.Config config);

        int[] b(int i);

        void c(Bitmap bitmap);

        void d(byte[] bArr);

        byte[] e(int i);

        void f(int[] iArr);
    }

    int a();

    Bitmap b();

    void c();

    void clear();

    int d();

    void e(Bitmap.Config config);

    void f();

    int g();

    ByteBuffer getData();

    int h();
}
