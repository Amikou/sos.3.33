package defpackage;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* compiled from: Atomic.kt */
/* renamed from: cj  reason: default package */
/* loaded from: classes2.dex */
public abstract class cj<T> extends fn2 {
    public static final /* synthetic */ AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(cj.class, Object.class, "_consensus");
    private volatile /* synthetic */ Object _consensus = bj.a;

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.fn2
    public cj<?> a() {
        return this;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.fn2
    public final Object c(Object obj) {
        Object obj2 = this._consensus;
        if (obj2 == bj.a) {
            obj2 = e(g(obj));
        }
        d(obj, obj2);
        return obj2;
    }

    public abstract void d(T t, Object obj);

    public final Object e(Object obj) {
        if (ze0.a()) {
            if (!(obj != bj.a)) {
                throw new AssertionError();
            }
        }
        Object obj2 = this._consensus;
        Object obj3 = bj.a;
        return obj2 != obj3 ? obj2 : a.compareAndSet(this, obj3, obj) ? obj : this._consensus;
    }

    public long f() {
        return 0L;
    }

    public abstract Object g(T t);
}
