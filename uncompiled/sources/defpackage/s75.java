package defpackage;

import android.content.SharedPreferences;
import com.google.android.gms.flags.impl.a;

/* renamed from: s75  reason: default package */
/* loaded from: classes.dex */
public final class s75 extends g35<Boolean> {
    public static Boolean a(SharedPreferences sharedPreferences, String str, Boolean bool) {
        try {
            return (Boolean) of5.a(new a(sharedPreferences, str, bool));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() != 0) {
                "Flag value not available, returning default: ".concat(valueOf);
            }
            return bool;
        }
    }
}
