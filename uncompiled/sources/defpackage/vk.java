package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event.java */
/* renamed from: vk  reason: default package */
/* loaded from: classes2.dex */
public final class vk extends r90.e.d {
    public final long a;
    public final String b;
    public final r90.e.d.a c;
    public final r90.e.d.c d;
    public final r90.e.d.AbstractC0276d e;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event.java */
    /* renamed from: vk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.b {
        public Long a;
        public String b;
        public r90.e.d.a c;
        public r90.e.d.c d;
        public r90.e.d.AbstractC0276d e;

        @Override // defpackage.r90.e.d.b
        public r90.e.d a() {
            String str = "";
            if (this.a == null) {
                str = " timestamp";
            }
            if (this.b == null) {
                str = str + " type";
            }
            if (this.c == null) {
                str = str + " app";
            }
            if (this.d == null) {
                str = str + " device";
            }
            if (str.isEmpty()) {
                return new vk(this.a.longValue(), this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.b
        public r90.e.d.b b(r90.e.d.a aVar) {
            Objects.requireNonNull(aVar, "Null app");
            this.c = aVar;
            return this;
        }

        @Override // defpackage.r90.e.d.b
        public r90.e.d.b c(r90.e.d.c cVar) {
            Objects.requireNonNull(cVar, "Null device");
            this.d = cVar;
            return this;
        }

        @Override // defpackage.r90.e.d.b
        public r90.e.d.b d(r90.e.d.AbstractC0276d abstractC0276d) {
            this.e = abstractC0276d;
            return this;
        }

        @Override // defpackage.r90.e.d.b
        public r90.e.d.b e(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.b
        public r90.e.d.b f(String str) {
            Objects.requireNonNull(str, "Null type");
            this.b = str;
            return this;
        }

        public b() {
        }

        public b(r90.e.d dVar) {
            this.a = Long.valueOf(dVar.e());
            this.b = dVar.f();
            this.c = dVar.b();
            this.d = dVar.c();
            this.e = dVar.d();
        }
    }

    @Override // defpackage.r90.e.d
    public r90.e.d.a b() {
        return this.c;
    }

    @Override // defpackage.r90.e.d
    public r90.e.d.c c() {
        return this.d;
    }

    @Override // defpackage.r90.e.d
    public r90.e.d.AbstractC0276d d() {
        return this.e;
    }

    @Override // defpackage.r90.e.d
    public long e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d) {
            r90.e.d dVar = (r90.e.d) obj;
            if (this.a == dVar.e() && this.b.equals(dVar.f()) && this.c.equals(dVar.b()) && this.d.equals(dVar.c())) {
                r90.e.d.AbstractC0276d abstractC0276d = this.e;
                if (abstractC0276d == null) {
                    if (dVar.d() == null) {
                        return true;
                    }
                } else if (abstractC0276d.equals(dVar.d())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override // defpackage.r90.e.d
    public String f() {
        return this.b;
    }

    @Override // defpackage.r90.e.d
    public r90.e.d.b g() {
        return new b(this);
    }

    public int hashCode() {
        long j = this.a;
        int hashCode = (((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003;
        r90.e.d.AbstractC0276d abstractC0276d = this.e;
        return hashCode ^ (abstractC0276d == null ? 0 : abstractC0276d.hashCode());
    }

    public String toString() {
        return "Event{timestamp=" + this.a + ", type=" + this.b + ", app=" + this.c + ", device=" + this.d + ", log=" + this.e + "}";
    }

    public vk(long j, String str, r90.e.d.a aVar, r90.e.d.c cVar, r90.e.d.AbstractC0276d abstractC0276d) {
        this.a = j;
        this.b = str;
        this.c = aVar;
        this.d = cVar;
        this.e = abstractC0276d;
    }
}
