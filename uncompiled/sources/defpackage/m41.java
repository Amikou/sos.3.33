package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.util.a;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: FilteringParserDelegate.java */
/* renamed from: m41  reason: default package */
/* loaded from: classes.dex */
public class m41 extends a {
    public boolean h0;
    public boolean i0;
    @Deprecated
    public boolean j0;
    public JsonToken k0;
    public w64 l0;
    public w64 m0;
    public v64 n0;

    public m41(JsonParser jsonParser, v64 v64Var, boolean z, boolean z2) {
        super(jsonParser);
        this.n0 = v64Var;
        this.l0 = w64.n(v64Var);
        this.i0 = z;
        this.h0 = z2;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public float A() throws IOException {
        return this.g0.A();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public boolean B0() {
        return this.g0.B0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public int F() throws IOException {
        return this.g0.F();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public final boolean F0(JsonToken jsonToken) {
        return this.k0 == jsonToken;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public boolean H0(int i) {
        JsonToken jsonToken = this.k0;
        return jsonToken == null ? i == 0 : jsonToken.id() == i;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public boolean K0() {
        return this.k0 == JsonToken.START_ARRAY;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public boolean L0() {
        return this.k0 == JsonToken.START_OBJECT;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public long M() throws IOException {
        return this.g0.M();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonParser.NumberType N() throws IOException {
        return this.g0.N();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public Number Q() throws IOException {
        return this.g0.Q();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public cw1 S() {
        return m1();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken T0() throws IOException {
        JsonToken r1;
        JsonToken r12;
        JsonToken r13;
        v64 k;
        JsonToken jsonToken;
        if (!this.h0 && (jsonToken = this.k0) != null && this.m0 == null) {
            if (jsonToken.isStructEnd() && this.l0.r()) {
                this.k0 = null;
                return null;
            } else if (this.k0.isScalarValue() && !this.l0.r() && !this.i0 && this.n0 == v64.a) {
                this.k0 = null;
                return null;
            }
        }
        w64 w64Var = this.m0;
        if (w64Var != null) {
            do {
                JsonToken s = w64Var.s();
                if (s != null) {
                    this.k0 = s;
                    return s;
                }
                w64 w64Var2 = this.l0;
                if (w64Var == w64Var2) {
                    this.m0 = null;
                    if (w64Var.e()) {
                        JsonToken u = this.g0.u();
                        this.k0 = u;
                        return u;
                    }
                } else {
                    w64Var = w64Var2.o(w64Var);
                    this.m0 = w64Var;
                }
            } while (w64Var != null);
            throw a("Unexpected problem: chain of filtered context broken");
        }
        JsonToken T0 = this.g0.T0();
        if (T0 == null) {
            this.k0 = T0;
            return T0;
        }
        int id = T0.id();
        if (id != 1) {
            if (id != 2) {
                if (id == 3) {
                    v64 v64Var = this.n0;
                    v64 v64Var2 = v64.a;
                    if (v64Var == v64Var2) {
                        this.l0 = this.l0.l(v64Var, true);
                        this.k0 = T0;
                        return T0;
                    } else if (v64Var == null) {
                        this.g0.k1();
                    } else {
                        v64 k2 = this.l0.k(v64Var);
                        if (k2 == null) {
                            this.g0.k1();
                        } else {
                            if (k2 != v64Var2) {
                                k2 = k2.c();
                            }
                            this.n0 = k2;
                            if (k2 == v64Var2) {
                                this.l0 = this.l0.l(k2, true);
                                this.k0 = T0;
                                return T0;
                            }
                            w64 l = this.l0.l(k2, false);
                            this.l0 = l;
                            if (this.i0 && (r12 = r1(l)) != null) {
                                this.k0 = r12;
                                return r12;
                            }
                        }
                    }
                } else if (id != 4) {
                    if (id != 5) {
                        v64 v64Var3 = this.n0;
                        v64 v64Var4 = v64.a;
                        if (v64Var3 == v64Var4) {
                            this.k0 = T0;
                            return T0;
                        } else if (v64Var3 != null && ((k = this.l0.k(v64Var3)) == v64Var4 || (k != null && k.h(this.g0)))) {
                            this.k0 = T0;
                            return T0;
                        }
                    } else {
                        String r = this.g0.r();
                        v64 u2 = this.l0.u(r);
                        v64 v64Var5 = v64.a;
                        if (u2 == v64Var5) {
                            this.n0 = u2;
                            if (!this.i0 && this.j0 && !this.l0.r()) {
                                T0 = this.l0.s();
                                this.m0 = this.l0;
                            }
                            this.k0 = T0;
                            return T0;
                        } else if (u2 == null) {
                            this.g0.T0();
                            this.g0.k1();
                        } else {
                            v64 f = u2.f(r);
                            if (f == null) {
                                this.g0.T0();
                                this.g0.k1();
                            } else {
                                this.n0 = f;
                                if (f == v64Var5 && this.i0) {
                                    this.k0 = T0;
                                    return T0;
                                } else if (this.i0 && (r13 = r1(this.l0)) != null) {
                                    this.k0 = r13;
                                    return r13;
                                }
                            }
                        }
                    }
                }
            }
            boolean r2 = this.l0.r();
            v64 p = this.l0.p();
            if (p != null && p != v64.a) {
                p.b();
            }
            w64 d = this.l0.d();
            this.l0 = d;
            this.n0 = d.p();
            if (r2) {
                this.k0 = T0;
                return T0;
            }
        } else {
            v64 v64Var6 = this.n0;
            v64 v64Var7 = v64.a;
            if (v64Var6 == v64Var7) {
                this.l0 = this.l0.m(v64Var6, true);
                this.k0 = T0;
                return T0;
            } else if (v64Var6 == null) {
                this.g0.k1();
            } else {
                v64 k3 = this.l0.k(v64Var6);
                if (k3 == null) {
                    this.g0.k1();
                } else {
                    if (k3 != v64Var7) {
                        k3 = k3.d();
                    }
                    this.n0 = k3;
                    if (k3 == v64Var7) {
                        this.l0 = this.l0.m(k3, true);
                        this.k0 = T0;
                        return T0;
                    }
                    w64 m = this.l0.m(k3, false);
                    this.l0 = m;
                    if (this.i0 && (r1 = r1(m)) != null) {
                        this.k0 = r1;
                        return r1;
                    }
                }
            }
        }
        return p1();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonToken U0() throws IOException {
        JsonToken T0 = T0();
        return T0 == JsonToken.FIELD_NAME ? T0() : T0;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public short W() throws IOException {
        return this.g0.W();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public String X() throws IOException {
        return this.g0.X();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
        return this.g0.Z0(base64Variant, outputStream);
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public char[] a0() throws IOException {
        return this.g0.a0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public int b0() throws IOException {
        return this.g0.b0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public void e() {
        if (this.k0 != null) {
            this.k0 = null;
        }
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public int e0() throws IOException {
        return this.g0.e0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonToken f() {
        return this.k0;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonLocation f0() {
        return this.g0.f0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public BigInteger h() throws IOException {
        return this.g0.h();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public int i0() throws IOException {
        return this.g0.i0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public byte[] j(Base64Variant base64Variant) throws IOException {
        return this.g0.j(base64Variant);
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonParser k1() throws IOException {
        JsonToken jsonToken = this.k0;
        if (jsonToken != JsonToken.START_OBJECT && jsonToken != JsonToken.START_ARRAY) {
            return this;
        }
        int i = 1;
        while (true) {
            JsonToken T0 = T0();
            if (T0 == null) {
                return this;
            }
            if (T0.isStructStart()) {
                i++;
            } else if (T0.isStructEnd() && i - 1 == 0) {
                return this;
            }
        }
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public boolean l() throws IOException {
        return this.g0.l();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public byte m() throws IOException {
        return this.g0.m();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public int m0(int i) throws IOException {
        return this.g0.m0(i);
    }

    public cw1 m1() {
        w64 w64Var = this.m0;
        return w64Var != null ? w64Var : this.l0;
    }

    public final JsonToken o1(w64 w64Var) throws IOException {
        this.m0 = w64Var;
        JsonToken s = w64Var.s();
        if (s != null) {
            return s;
        }
        while (w64Var != this.l0) {
            w64Var = this.m0.o(w64Var);
            this.m0 = w64Var;
            if (w64Var != null) {
                JsonToken s2 = w64Var.s();
                if (s2 != null) {
                    return s2;
                }
            } else {
                throw a("Unexpected problem: chain of filtered context broken");
            }
        }
        throw a("Internal error: failed to locate expected buffered tokens");
    }

    /* JADX WARN: Code restructure failed: missing block: B:26:0x003c, code lost:
        r6.k0 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x003e, code lost:
        return r0;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.fasterxml.jackson.core.JsonToken p1() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 346
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.m41.p1():com.fasterxml.jackson.core.JsonToken");
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonLocation q() {
        return this.g0.q();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public String r() throws IOException {
        cw1 m1 = m1();
        JsonToken jsonToken = this.k0;
        if (jsonToken != JsonToken.START_OBJECT && jsonToken != JsonToken.START_ARRAY) {
            return m1.b();
        }
        cw1 d = m1.d();
        if (d == null) {
            return null;
        }
        return d.b();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public long r0() throws IOException {
        return this.g0.r0();
    }

    /* JADX WARN: Code restructure failed: missing block: B:26:0x0040, code lost:
        return o1(r6);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.fasterxml.jackson.core.JsonToken r1(defpackage.w64 r6) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 307
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.m41.r1(w64):com.fasterxml.jackson.core.JsonToken");
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public JsonToken u() {
        return this.k0;
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public final int v() {
        JsonToken jsonToken = this.k0;
        if (jsonToken == null) {
            return 0;
        }
        return jsonToken.id();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public BigDecimal w() throws IOException {
        return this.g0.w();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public long w0(long j) throws IOException {
        return this.g0.w0(j);
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public double x() throws IOException {
        return this.g0.x();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public String x0() throws IOException {
        return this.g0.x0();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public String y0(String str) throws IOException {
        return this.g0.y0(str);
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public Object z() throws IOException {
        return this.g0.z();
    }

    @Override // com.fasterxml.jackson.core.util.a, com.fasterxml.jackson.core.JsonParser
    public boolean z0() {
        return this.k0 != null;
    }
}
