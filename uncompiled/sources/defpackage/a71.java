package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.a;
import java.util.List;

/* compiled from: FlexboxItemDecoration.java */
/* renamed from: a71  reason: default package */
/* loaded from: classes.dex */
public class a71 extends RecyclerView.n {
    public static final int[] c = {16843284};
    public Drawable a;
    public int b;

    public a71(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(c);
        this.a = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        n(3);
    }

    public final void f(Canvas canvas, RecyclerView recyclerView) {
        int top;
        int intrinsicHeight;
        int left;
        int right;
        int i;
        int i2;
        int i3;
        if (i()) {
            FlexboxLayoutManager flexboxLayoutManager = (FlexboxLayoutManager) recyclerView.getLayoutManager();
            int flexDirection = flexboxLayoutManager.getFlexDirection();
            int left2 = recyclerView.getLeft() - recyclerView.getPaddingLeft();
            int right2 = recyclerView.getRight() + recyclerView.getPaddingRight();
            int childCount = recyclerView.getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = recyclerView.getChildAt(i4);
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) childAt.getLayoutParams();
                if (flexDirection == 3) {
                    intrinsicHeight = childAt.getBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                    top = this.a.getIntrinsicHeight() + intrinsicHeight;
                } else {
                    top = childAt.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                    intrinsicHeight = top - this.a.getIntrinsicHeight();
                }
                if (flexboxLayoutManager.k()) {
                    if (flexboxLayoutManager.E2()) {
                        i2 = Math.min(childAt.getRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + this.a.getIntrinsicWidth(), right2);
                        i3 = childAt.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
                        this.a.setBounds(i3, intrinsicHeight, i2, top);
                        this.a.draw(canvas);
                    } else {
                        left = Math.max((childAt.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin) - this.a.getIntrinsicWidth(), left2);
                        right = childAt.getRight();
                        i = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                    }
                } else {
                    left = childAt.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
                    right = childAt.getRight();
                    i = ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                }
                int i5 = left;
                i2 = right + i;
                i3 = i5;
                this.a.setBounds(i3, intrinsicHeight, i2, top);
                this.a.draw(canvas);
            }
        }
    }

    public final void g(Canvas canvas, RecyclerView recyclerView) {
        int left;
        int intrinsicWidth;
        int max;
        int bottom;
        int i;
        int i2;
        if (j()) {
            FlexboxLayoutManager flexboxLayoutManager = (FlexboxLayoutManager) recyclerView.getLayoutManager();
            int top = recyclerView.getTop() - recyclerView.getPaddingTop();
            int bottom2 = recyclerView.getBottom() + recyclerView.getPaddingBottom();
            int childCount = recyclerView.getChildCount();
            int flexDirection = flexboxLayoutManager.getFlexDirection();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = recyclerView.getChildAt(i3);
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) childAt.getLayoutParams();
                if (flexboxLayoutManager.E2()) {
                    intrinsicWidth = childAt.getRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                    left = this.a.getIntrinsicWidth() + intrinsicWidth;
                } else {
                    left = childAt.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
                    intrinsicWidth = left - this.a.getIntrinsicWidth();
                }
                if (flexboxLayoutManager.k()) {
                    max = childAt.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                    bottom = childAt.getBottom();
                    i = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                } else if (flexDirection == 3) {
                    int min = Math.min(childAt.getBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + this.a.getIntrinsicHeight(), bottom2);
                    max = childAt.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                    i2 = min;
                    this.a.setBounds(intrinsicWidth, max, left, i2);
                    this.a.draw(canvas);
                } else {
                    max = Math.max((childAt.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) - this.a.getIntrinsicHeight(), top);
                    bottom = childAt.getBottom();
                    i = ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                }
                i2 = bottom + i;
                this.a.setBounds(intrinsicWidth, max, left, i2);
                this.a.draw(canvas);
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.x xVar) {
        int f0 = recyclerView.f0(view);
        if (f0 == 0) {
            return;
        }
        if (!i() && !j()) {
            rect.set(0, 0, 0, 0);
            return;
        }
        FlexboxLayoutManager flexboxLayoutManager = (FlexboxLayoutManager) recyclerView.getLayoutManager();
        List<a> A2 = flexboxLayoutManager.A2();
        m(rect, f0, flexboxLayoutManager, A2, flexboxLayoutManager.getFlexDirection());
        l(rect, f0, flexboxLayoutManager, A2);
    }

    public final boolean h(int i, List<a> list, FlexboxLayoutManager flexboxLayoutManager) {
        int B2 = flexboxLayoutManager.B2(i);
        if ((B2 == -1 || B2 >= flexboxLayoutManager.getFlexLinesInternal().size() || flexboxLayoutManager.getFlexLinesInternal().get(B2).o != i) && i != 0) {
            return list.size() != 0 && list.get(list.size() - 1).p == i - 1;
        }
        return true;
    }

    public final boolean i() {
        return (this.b & 1) > 0;
    }

    public final boolean j() {
        return (this.b & 2) > 0;
    }

    public void k(Drawable drawable) {
        if (drawable != null) {
            this.a = drawable;
            return;
        }
        throw new IllegalArgumentException("Drawable cannot be null.");
    }

    public final void l(Rect rect, int i, FlexboxLayoutManager flexboxLayoutManager, List<a> list) {
        if (list.size() == 0 || flexboxLayoutManager.B2(i) == 0) {
            return;
        }
        if (flexboxLayoutManager.k()) {
            if (!i()) {
                rect.top = 0;
                rect.bottom = 0;
                return;
            }
            rect.top = this.a.getIntrinsicHeight();
            rect.bottom = 0;
        } else if (j()) {
            if (flexboxLayoutManager.E2()) {
                rect.right = this.a.getIntrinsicWidth();
                rect.left = 0;
                return;
            }
            rect.left = this.a.getIntrinsicWidth();
            rect.right = 0;
        }
    }

    public final void m(Rect rect, int i, FlexboxLayoutManager flexboxLayoutManager, List<a> list, int i2) {
        if (h(i, list, flexboxLayoutManager)) {
            return;
        }
        if (flexboxLayoutManager.k()) {
            if (!j()) {
                rect.left = 0;
                rect.right = 0;
            } else if (flexboxLayoutManager.E2()) {
                rect.right = this.a.getIntrinsicWidth();
                rect.left = 0;
            } else {
                rect.left = this.a.getIntrinsicWidth();
                rect.right = 0;
            }
        } else if (!i()) {
            rect.top = 0;
            rect.bottom = 0;
        } else if (i2 == 3) {
            rect.bottom = this.a.getIntrinsicHeight();
            rect.top = 0;
        } else {
            rect.top = this.a.getIntrinsicHeight();
            rect.bottom = 0;
        }
    }

    public void n(int i) {
        this.b = i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.n
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.x xVar) {
        f(canvas, recyclerView);
        g(canvas, recyclerView);
    }
}
