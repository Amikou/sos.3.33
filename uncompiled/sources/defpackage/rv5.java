package defpackage;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: rv5  reason: default package */
/* loaded from: classes.dex */
public final class rv5 implements wg5 {
    public final /* synthetic */ String a;
    public final /* synthetic */ fw5 b;

    public rv5(fw5 fw5Var, String str) {
        this.b = fw5Var;
        this.a = str;
    }

    @Override // defpackage.wg5
    public final void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.b.e(i, th, bArr, this.a);
    }
}
