package defpackage;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;

/* compiled from: SchedulingConfigModule_ConfigFactory.java */
/* renamed from: id3  reason: default package */
/* loaded from: classes.dex */
public final class id3 implements z11<SchedulerConfig> {
    public final ew2<qz> a;

    public id3(ew2<qz> ew2Var) {
        this.a = ew2Var;
    }

    public static SchedulerConfig a(qz qzVar) {
        return (SchedulerConfig) yt2.c(hd3.a(qzVar), "Cannot return null from a non-@Nullable @Provides method");
    }

    public static id3 b(ew2<qz> ew2Var) {
        return new id3(ew2Var);
    }

    @Override // defpackage.ew2
    /* renamed from: c */
    public SchedulerConfig get() {
        return a(this.a.get());
    }
}
