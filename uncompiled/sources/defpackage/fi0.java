package defpackage;

import java.math.BigInteger;
import org.web3j.protocol.core.DefaultBlockParameterName;

/* compiled from: DefaultBlockParameter.java */
/* renamed from: fi0  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class fi0 {
    public static gi0 a(String str) {
        return DefaultBlockParameterName.fromString(str);
    }

    public static gi0 b(BigInteger bigInteger) {
        BigInteger bigInteger2 = BigInteger.ZERO;
        if (bigInteger2.compareTo(bigInteger) >= 0) {
            bigInteger = bigInteger2;
        }
        return new hi0(bigInteger);
    }
}
