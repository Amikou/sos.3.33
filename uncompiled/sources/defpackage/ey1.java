package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.ey1;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.LanguageItem;

/* compiled from: LanguageListAdapter.kt */
/* renamed from: ey1  reason: default package */
/* loaded from: classes2.dex */
public abstract class ey1 extends RecyclerView.Adapter<b> {
    public final a a;
    public final List<LanguageItem> b;

    /* compiled from: LanguageListAdapter.kt */
    /* renamed from: ey1$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(LanguageItem languageItem);
    }

    /* compiled from: LanguageListAdapter.kt */
    /* renamed from: ey1$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final at1 a;
        public final /* synthetic */ ey1 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(ey1 ey1Var, at1 at1Var) {
            super(at1Var.b());
            fs1.f(ey1Var, "this$0");
            fs1.f(at1Var, "binding");
            this.b = ey1Var;
            this.a = at1Var;
        }

        public static final void c(ey1 ey1Var, LanguageItem languageItem, View view) {
            fs1.f(ey1Var, "this$0");
            fs1.f(languageItem, "$item");
            ey1Var.a.a(languageItem);
            ey1Var.b();
        }

        public final void b(final LanguageItem languageItem, boolean z, boolean z2) {
            fs1.f(languageItem, "item");
            at1 at1Var = this.a;
            final ey1 ey1Var = this.b;
            at1Var.c.setText(languageItem.getTitleResId());
            at1Var.d.setText(languageItem.getRegionResId());
            at1Var.b.setChecked(fs1.b(ey1Var.e(), languageItem.getLanguageCode()));
            at1Var.e.setVisibility(e30.l0(z));
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: fy1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ey1.b.c(ey1.this, languageItem, view);
                }
            });
            if (z) {
                d().b().setBackgroundResource(R.drawable.history_item_footer);
                d().b().setBackgroundTintList(at1Var.b().getContext().getColorStateList(R.color.card_bg_1));
            }
            if (z2) {
                d().b().setBackgroundResource(R.drawable.history_item_header);
                d().b().setBackgroundTintList(at1Var.b().getContext().getColorStateList(R.color.card_bg_1));
            }
        }

        public final at1 d() {
            return this.a;
        }
    }

    public ey1(a aVar) {
        fs1.f(aVar, "onDefaultLanguageSelectedListener");
        this.a = aVar;
        this.b = new ArrayList();
    }

    public final void b() {
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.b(this.b.get(i), i + 1 == this.b.size(), i == 0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        at1 a2 = at1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_language, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        return new b(this, a2);
    }

    public abstract String e();

    public final void f(List<LanguageItem> list) {
        fs1.f(list, "items");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.b.size();
    }
}
