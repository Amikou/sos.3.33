package defpackage;

import java.util.concurrent.Executor;

/* compiled from: DefaultScheduler_Factory.java */
/* renamed from: rk0  reason: default package */
/* loaded from: classes.dex */
public final class rk0 implements z11<qk0> {
    public final ew2<Executor> a;
    public final ew2<bm> b;
    public final ew2<rq4> c;
    public final ew2<dy0> d;
    public final ew2<l24> e;

    public rk0(ew2<Executor> ew2Var, ew2<bm> ew2Var2, ew2<rq4> ew2Var3, ew2<dy0> ew2Var4, ew2<l24> ew2Var5) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
        this.d = ew2Var4;
        this.e = ew2Var5;
    }

    public static rk0 a(ew2<Executor> ew2Var, ew2<bm> ew2Var2, ew2<rq4> ew2Var3, ew2<dy0> ew2Var4, ew2<l24> ew2Var5) {
        return new rk0(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static qk0 c(Executor executor, bm bmVar, rq4 rq4Var, dy0 dy0Var, l24 l24Var) {
        return new qk0(executor, bmVar, rq4Var, dy0Var, l24Var);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public qk0 get() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
