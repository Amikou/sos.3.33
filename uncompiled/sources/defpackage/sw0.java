package defpackage;

import java.math.BigInteger;

/* compiled from: EthGetBalance.java */
/* renamed from: sw0  reason: default package */
/* loaded from: classes3.dex */
public class sw0 extends i83<String> {
    public BigInteger getBalance() {
        return ej2.decodeQuantity(getResult());
    }
}
