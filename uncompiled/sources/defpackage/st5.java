package defpackage;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import defpackage.pt5;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: st5  reason: default package */
/* loaded from: classes.dex */
public final class st5<T extends Context & pt5> {
    public final T a;

    public st5(T t) {
        zt2.j(t);
        this.a = t;
    }

    public final void a() {
        ck5 e = ck5.e(this.a, null, null);
        og5 w = e.w();
        e.b();
        w.v().a("Local AppMeasurementService is starting up");
    }

    public final void b() {
        ck5 e = ck5.e(this.a, null, null);
        og5 w = e.w();
        e.b();
        w.v().a("Local AppMeasurementService is shutting down");
    }

    public final int c(final Intent intent, int i, final int i2) {
        ck5 e = ck5.e(this.a, null, null);
        final og5 w = e.w();
        if (intent == null) {
            w.p().a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        e.b();
        w.v().c("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            d(new Runnable(this, i2, w, intent) { // from class: ht5
                public final st5 a;
                public final int f0;
                public final og5 g0;
                public final Intent h0;

                {
                    this.a = this;
                    this.f0 = i2;
                    this.g0 = w;
                    this.h0 = intent;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.a.j(this.f0, this.g0, this.h0);
                }
            });
        }
        return 2;
    }

    public final void d(Runnable runnable) {
        fw5 F = fw5.F(this.a);
        F.q().p(new mt5(this, F, runnable));
    }

    public final IBinder e(Intent intent) {
        if (intent == null) {
            k().l().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new pl5(fw5.F(this.a), null);
        }
        k().p().b("onBind received unknown action", action);
        return null;
    }

    public final boolean f(Intent intent) {
        if (intent == null) {
            k().l().a("onUnbind called with null intent");
            return true;
        }
        k().v().b("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    @TargetApi(24)
    public final boolean g(final JobParameters jobParameters) {
        ck5 e = ck5.e(this.a, null, null);
        final og5 w = e.w();
        String string = jobParameters.getExtras().getString("action");
        e.b();
        w.v().b("Local AppMeasurementJobService called. action", string);
        if ("com.google.android.gms.measurement.UPLOAD".equals(string)) {
            d(new Runnable(this, w, jobParameters) { // from class: jt5
                public final st5 a;
                public final og5 f0;
                public final JobParameters g0;

                {
                    this.a = this;
                    this.f0 = w;
                    this.g0 = jobParameters;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.a.i(this.f0, this.g0);
                }
            });
            return true;
        }
        return true;
    }

    public final void h(Intent intent) {
        if (intent == null) {
            k().l().a("onRebind called with null intent");
            return;
        }
        k().v().b("onRebind called. action", intent.getAction());
    }

    public final /* synthetic */ void i(og5 og5Var, JobParameters jobParameters) {
        og5Var.v().a("AppMeasurementJobService processed last upload request.");
        this.a.b(jobParameters, false);
    }

    public final /* synthetic */ void j(int i, og5 og5Var, Intent intent) {
        if (this.a.d(i)) {
            og5Var.v().b("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            k().v().a("Completed wakeful intent.");
            this.a.a(intent);
        }
    }

    public final og5 k() {
        return ck5.e(this.a, null, null).w();
    }
}
