package defpackage;

import android.content.Context;
import com.google.android.play.core.assetpacks.AssetPackExtractionService;
import com.google.android.play.core.assetpacks.ExtractionForegroundService;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.f;
import com.google.android.play.core.assetpacks.j;
import java.util.concurrent.Executor;

/* renamed from: av4  reason: default package */
/* loaded from: classes2.dex */
public final class av4 implements us4 {
    public final dy4 a;
    public jw4<Context> b;
    public jw4<ww4> c;
    public jw4<c> d;
    public jw4<fv4> e;
    public jw4<st4> f;
    public jw4<String> g;
    public jw4<zy4> h;
    public jw4<Executor> i;
    public jw4<zv4> j;
    public jw4<au4> k;
    public jw4<cv4> l;
    public jw4<fx4> m;
    public jw4<pw4> n;
    public jw4<ws4> o;
    public jw4<uw4> p;
    public jw4<yw4> q;
    public jw4<su4> r;
    public jw4<hw4> s;
    public jw4<dv4> t;
    public jw4<vu4> u;
    public jw4<Executor> v;
    public jw4<j> w;
    public jw4<oy4> x;
    public jw4<ux4> y;
    public jw4<f> z;

    public /* synthetic */ av4(dy4 dy4Var) {
        wy4 wy4Var;
        wy4 wy4Var2;
        wy4 wy4Var3;
        wy4 wy4Var4;
        this.a = dy4Var;
        my4 my4Var = new my4(dy4Var);
        this.b = my4Var;
        jw4<ww4> b = gw4.b(new qw4(my4Var, (char[]) null));
        this.c = b;
        this.d = gw4.b(new jy4(this.b, b, (short[]) null));
        wy4Var = gv4.a;
        jw4<fv4> b2 = gw4.b(wy4Var);
        this.e = b2;
        this.f = gw4.b(new jy4(this.b, b2, (char[]) null));
        this.g = gw4.b(new py4(this.b));
        this.h = new ew4();
        wy4Var2 = gy4.a;
        jw4<Executor> b3 = gw4.b(wy4Var2);
        this.i = b3;
        this.j = gw4.b(new dw4(this.d, this.h, this.e, b3));
        ew4 ew4Var = new ew4();
        this.k = ew4Var;
        this.l = gw4.b(new dw4(this.d, this.h, ew4Var, this.e, null));
        this.m = gw4.b(new qw4(this.d, (short[]) null));
        this.n = gw4.b(new qw4(this.d));
        jw4<ws4> b4 = gw4.b(bw4.b());
        this.o = b4;
        this.p = gw4.b(new vw4(this.d, this.h, this.j, this.i, this.e, b4));
        this.q = gw4.b(new jy4(this.d, this.h, (int[]) null));
        jw4<su4> b5 = gw4.b(new qw4(this.h, (byte[]) null));
        this.r = b5;
        jw4<hw4> b6 = gw4.b(new iw4(this.j, this.d, b5));
        this.s = b6;
        this.t = gw4.b(new ev4(this.j, this.h, this.l, this.m, this.n, this.p, this.q, b6));
        wy4Var3 = xu4.a;
        this.u = gw4.b(wy4Var3);
        wy4Var4 = ry4.a;
        jw4<Executor> b7 = gw4.b(wy4Var4);
        this.v = b7;
        ew4.b(this.k, gw4.b(new ev4(this.b, this.j, this.t, this.h, this.e, this.u, this.i, b7, null)));
        jw4<j> b8 = gw4.b(new vw4(this.g, this.k, this.e, this.b, this.c, this.i, null));
        this.w = b8;
        ew4.b(this.h, gw4.b(new iw4(this.b, this.f, b8, null)));
        jw4<oy4> b9 = gw4.b(qx4.b(this.b));
        this.x = b9;
        jw4<ux4> b10 = gw4.b(new wx4(this.d, this.h, this.k, b9, this.j, this.e, this.u, this.i, this.o));
        this.y = b10;
        gw4.b(new jy4(b10, this.b));
        this.z = gw4.b(new jy4(this.b, this.d, (byte[]) null));
    }

    @Override // defpackage.us4
    public final void a(ExtractionForegroundService extractionForegroundService) {
        extractionForegroundService.f0 = my4.c(this.a);
        extractionForegroundService.g0 = this.y.a();
    }

    @Override // defpackage.us4
    public final void b(AssetPackExtractionService assetPackExtractionService) {
        assetPackExtractionService.a = this.z.a();
    }
}
