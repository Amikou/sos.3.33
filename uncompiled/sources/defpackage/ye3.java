package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: ye3  reason: default package */
/* loaded from: classes2.dex */
public class ye3 extends ct0.b {
    public static final BigInteger g = we3.j;
    public static final int[] h = {868209154, -587542221, 579297866, -1014948952, -1470801668, 514782679, -1897982644};
    public int[] f;

    public ye3() {
        this.f = dd2.e();
    }

    public ye3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP224K1FieldElement");
        }
        this.f = xe3.c(bigInteger);
    }

    public ye3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] e = dd2.e();
        xe3.a(this.f, ((ye3) ct0Var).f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] e = dd2.e();
        xe3.b(this.f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] e = dd2.e();
        g92.d(xe3.a, ((ye3) ct0Var).f, e);
        xe3.d(e, this.f, e);
        return new ye3(e);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ye3) {
            return dd2.g(this.f, ((ye3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] e = dd2.e();
        g92.d(xe3.a, this.f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return dd2.k(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 7);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return dd2.l(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] e = dd2.e();
        xe3.d(this.f, ((ye3) ct0Var).f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] e = dd2.e();
        xe3.f(this.f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (dd2.l(iArr) || dd2.k(iArr)) {
            return this;
        }
        int[] e = dd2.e();
        xe3.i(iArr, e);
        xe3.d(e, iArr, e);
        xe3.i(e, e);
        xe3.d(e, iArr, e);
        int[] e2 = dd2.e();
        xe3.i(e, e2);
        xe3.d(e2, iArr, e2);
        int[] e3 = dd2.e();
        xe3.j(e2, 4, e3);
        xe3.d(e3, e2, e3);
        int[] e4 = dd2.e();
        xe3.j(e3, 3, e4);
        xe3.d(e4, e, e4);
        xe3.j(e4, 8, e4);
        xe3.d(e4, e3, e4);
        xe3.j(e4, 4, e3);
        xe3.d(e3, e2, e3);
        xe3.j(e3, 19, e2);
        xe3.d(e2, e4, e2);
        int[] e5 = dd2.e();
        xe3.j(e2, 42, e5);
        xe3.d(e5, e2, e5);
        xe3.j(e5, 23, e2);
        xe3.d(e2, e3, e2);
        xe3.j(e2, 84, e3);
        xe3.d(e3, e5, e3);
        xe3.j(e3, 20, e3);
        xe3.d(e3, e4, e3);
        xe3.j(e3, 3, e3);
        xe3.d(e3, iArr, e3);
        xe3.j(e3, 2, e3);
        xe3.d(e3, iArr, e3);
        xe3.j(e3, 4, e3);
        xe3.d(e3, e, e3);
        xe3.i(e3, e3);
        xe3.i(e3, e5);
        if (dd2.g(iArr, e5)) {
            return new ye3(e3);
        }
        xe3.d(e3, h, e3);
        xe3.i(e3, e5);
        if (dd2.g(iArr, e5)) {
            return new ye3(e3);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] e = dd2.e();
        xe3.i(this.f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] e = dd2.e();
        xe3.k(this.f, ((ye3) ct0Var).f, e);
        return new ye3(e);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return dd2.i(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return dd2.u(this.f);
    }
}
