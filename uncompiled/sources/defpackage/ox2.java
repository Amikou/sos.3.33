package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import defpackage.nx2;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Locale;

/* compiled from: QueueFileLogStore.java */
/* renamed from: ox2  reason: default package */
/* loaded from: classes2.dex */
public class ox2 implements o31 {
    public static final Charset d = Charset.forName("UTF-8");
    public final File a;
    public final int b;
    public nx2 c;

    /* compiled from: QueueFileLogStore.java */
    /* renamed from: ox2$a */
    /* loaded from: classes2.dex */
    public class a implements nx2.d {
        public final /* synthetic */ byte[] a;
        public final /* synthetic */ int[] b;

        public a(ox2 ox2Var, byte[] bArr, int[] iArr) {
            this.a = bArr;
            this.b = iArr;
        }

        @Override // defpackage.nx2.d
        public void a(InputStream inputStream, int i) throws IOException {
            try {
                inputStream.read(this.a, this.b[0], i);
                int[] iArr = this.b;
                iArr[0] = iArr[0] + i;
            } finally {
                inputStream.close();
            }
        }
    }

    /* compiled from: QueueFileLogStore.java */
    /* renamed from: ox2$b */
    /* loaded from: classes2.dex */
    public static class b {
        public final byte[] a;
        public final int b;

        public b(byte[] bArr, int i) {
            this.a = bArr;
            this.b = i;
        }
    }

    public ox2(File file, int i) {
        this.a = file;
        this.b = i;
    }

    @Override // defpackage.o31
    public void a() {
        CommonUtils.e(this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }

    @Override // defpackage.o31
    public String b() {
        byte[] c = c();
        if (c != null) {
            return new String(c, d);
        }
        return null;
    }

    @Override // defpackage.o31
    public byte[] c() {
        b g = g();
        if (g == null) {
            return null;
        }
        int i = g.b;
        byte[] bArr = new byte[i];
        System.arraycopy(g.a, 0, bArr, 0, i);
        return bArr;
    }

    @Override // defpackage.o31
    public void d() {
        a();
        this.a.delete();
    }

    @Override // defpackage.o31
    public void e(long j, String str) {
        h();
        f(j, str);
    }

    public final void f(long j, String str) {
        int i;
        if (this.c == null) {
            return;
        }
        if (str == null) {
            str = "null";
        }
        try {
            if (str.length() > this.b / 4) {
                str = "..." + str.substring(str.length() - i);
            }
            this.c.e(String.format(Locale.US, "%d %s%n", Long.valueOf(j), str.replaceAll("\r", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).replaceAll("\n", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)).getBytes(d));
            while (!this.c.l() && this.c.C() > this.b) {
                this.c.w();
            }
        } catch (IOException e) {
            w12.f().e("There was a problem writing to the Crashlytics log.", e);
        }
    }

    public final b g() {
        if (this.a.exists()) {
            h();
            nx2 nx2Var = this.c;
            if (nx2Var == null) {
                return null;
            }
            int[] iArr = {0};
            byte[] bArr = new byte[nx2Var.C()];
            try {
                this.c.i(new a(this, bArr, iArr));
            } catch (IOException e) {
                w12.f().e("A problem occurred while reading the Crashlytics log file.", e);
            }
            return new b(bArr, iArr[0]);
        }
        return null;
    }

    public final void h() {
        if (this.c == null) {
            try {
                this.c = new nx2(this.a);
            } catch (IOException e) {
                w12 f = w12.f();
                f.e("Could not open log file: " + this.a, e);
            }
        }
    }
}
