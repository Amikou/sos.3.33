package defpackage;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;

/* compiled from: ResourceCallback.java */
/* renamed from: u73  reason: default package */
/* loaded from: classes.dex */
public interface u73 {
    void a(GlideException glideException);

    void b(s73<?> s73Var, DataSource dataSource, boolean z);

    Object g();
}
