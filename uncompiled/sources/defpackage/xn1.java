package defpackage;

import com.facebook.common.internal.a;
import com.facebook.common.internal.d;
import defpackage.wn1;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* compiled from: ImageFormatChecker.java */
/* renamed from: xn1  reason: default package */
/* loaded from: classes.dex */
public class xn1 {
    public static xn1 d;
    public int a;
    public List<wn1.a> b;
    public final vj0 c = new vj0();

    public xn1() {
        f();
    }

    public static wn1 b(InputStream inputStream) throws IOException {
        return d().a(inputStream);
    }

    public static wn1 c(InputStream inputStream) {
        try {
            return b(inputStream);
        } catch (IOException e) {
            throw d.a(e);
        }
    }

    public static synchronized xn1 d() {
        xn1 xn1Var;
        synchronized (xn1.class) {
            if (d == null) {
                d = new xn1();
            }
            xn1Var = d;
        }
        return xn1Var;
    }

    public static int e(int i, InputStream inputStream, byte[] bArr) throws IOException {
        xt2.g(inputStream);
        xt2.g(bArr);
        xt2.b(Boolean.valueOf(bArr.length >= i));
        if (inputStream.markSupported()) {
            try {
                inputStream.mark(i);
                return a.b(inputStream, bArr, 0, i);
            } finally {
                inputStream.reset();
            }
        }
        return a.b(inputStream, bArr, 0, i);
    }

    public wn1 a(InputStream inputStream) throws IOException {
        xt2.g(inputStream);
        int i = this.a;
        byte[] bArr = new byte[i];
        int e = e(i, inputStream, bArr);
        wn1 b = this.c.b(bArr, e);
        if (b == null || b == wn1.b) {
            List<wn1.a> list = this.b;
            if (list != null) {
                for (wn1.a aVar : list) {
                    wn1 b2 = aVar.b(bArr, e);
                    if (b2 != null && b2 != wn1.b) {
                        return b2;
                    }
                }
            }
            return wn1.b;
        }
        return b;
    }

    public final void f() {
        this.a = this.c.a();
        List<wn1.a> list = this.b;
        if (list != null) {
            for (wn1.a aVar : list) {
                this.a = Math.max(this.a, aVar.a());
            }
        }
    }
}
