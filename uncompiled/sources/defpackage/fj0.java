package defpackage;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: DefaultExecutorSupplier.java */
/* renamed from: fj0  reason: default package */
/* loaded from: classes.dex */
public class fj0 implements wy0 {
    public final Executor b;
    public final Executor c;
    public final ScheduledExecutorService e;
    public final Executor a = Executors.newFixedThreadPool(2, new yu2(10, "FrescoIoBoundExecutor", true));
    public final Executor d = Executors.newFixedThreadPool(1, new yu2(10, "FrescoLightWeightBackgroundExecutor", true));

    public fj0(int i) {
        this.b = Executors.newFixedThreadPool(i, new yu2(10, "FrescoDecodeExecutor", true));
        this.c = Executors.newFixedThreadPool(i, new yu2(10, "FrescoBackgroundExecutor", true));
        this.e = Executors.newScheduledThreadPool(i, new yu2(10, "FrescoBackgroundExecutor", true));
    }

    @Override // defpackage.wy0
    public Executor a() {
        return this.b;
    }

    @Override // defpackage.wy0
    public Executor b() {
        return this.d;
    }

    @Override // defpackage.wy0
    public Executor c() {
        return this.c;
    }

    @Override // defpackage.wy0
    public Executor d() {
        return this.a;
    }

    @Override // defpackage.wy0
    public Executor e() {
        return this.a;
    }

    @Override // defpackage.wy0
    public Executor f() {
        return this.a;
    }

    @Override // defpackage.wy0
    public ScheduledExecutorService g() {
        return this.e;
    }
}
