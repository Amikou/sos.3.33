package defpackage;

import android.content.Context;
import androidx.lifecycle.l;
import java.lang.ref.WeakReference;
import net.safemoon.androidwallet.mapper.token.TokenDisplayModelMapper;
import net.safemoon.androidwallet.viewmodels.AddNewTokensViewModel;

/* compiled from: AddNewTokensViewModelFactory.kt */
/* renamed from: u9  reason: default package */
/* loaded from: classes2.dex */
public final class u9 implements l.b {
    public final WeakReference<Context> a;

    public u9(WeakReference<Context> weakReference) {
        fs1.f(weakReference, "context");
        this.a = weakReference;
    }

    @Override // androidx.lifecycle.l.b
    public <T extends dj4> T a(Class<T> cls) {
        fs1.f(cls, "modelClass");
        Context context = this.a.get();
        fs1.d(context);
        Context applicationContext = context.getApplicationContext();
        fs1.d(applicationContext);
        pl1 b = za.a.b(applicationContext);
        gg4 gg4Var = gg4.a;
        return new AddNewTokensViewModel(b, new TokenDisplayModelMapper(applicationContext, new kf1(gg4Var.b(applicationContext))), gg4Var.b(applicationContext), zc0.a.a(applicationContext));
    }
}
