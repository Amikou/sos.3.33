package defpackage;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.RecentlyNonNull;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: yb3  reason: default package */
/* loaded from: classes.dex */
public class yb3 {
    public static void A(Parcel parcel, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(i - 4);
        parcel.writeInt(dataPosition - i);
        parcel.setDataPosition(dataPosition);
    }

    @RecentlyNonNull
    public static int a(@RecentlyNonNull Parcel parcel) {
        return x(parcel, 20293);
    }

    public static void b(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i) {
        A(parcel, i);
    }

    public static void c(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull boolean z) {
        y(parcel, i, 4);
        parcel.writeInt(z ? 1 : 0);
    }

    public static void d(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Boolean bool, @RecentlyNonNull boolean z) {
        if (bool != null) {
            y(parcel, i, 4);
            parcel.writeInt(bool.booleanValue() ? 1 : 0);
        } else if (z) {
            y(parcel, i, 0);
        }
    }

    public static void e(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Bundle bundle, @RecentlyNonNull boolean z) {
        if (bundle == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeBundle(bundle);
        A(parcel, x);
    }

    public static void f(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull byte[] bArr, @RecentlyNonNull boolean z) {
        if (bArr == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeByteArray(bArr);
        A(parcel, x);
    }

    public static void g(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull byte[][] bArr, @RecentlyNonNull boolean z) {
        if (bArr == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeInt(bArr.length);
        for (byte[] bArr2 : bArr) {
            parcel.writeByteArray(bArr2);
        }
        A(parcel, x);
    }

    public static void h(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull double d) {
        y(parcel, i, 8);
        parcel.writeDouble(d);
    }

    public static void i(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Double d, @RecentlyNonNull boolean z) {
        if (d != null) {
            y(parcel, i, 8);
            parcel.writeDouble(d.doubleValue());
        } else if (z) {
            y(parcel, i, 0);
        }
    }

    public static void j(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull float f) {
        y(parcel, i, 4);
        parcel.writeFloat(f);
    }

    public static void k(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Float f, @RecentlyNonNull boolean z) {
        if (f != null) {
            y(parcel, i, 4);
            parcel.writeFloat(f.floatValue());
        } else if (z) {
            y(parcel, i, 0);
        }
    }

    public static void l(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull IBinder iBinder, @RecentlyNonNull boolean z) {
        if (iBinder == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeStrongBinder(iBinder);
        A(parcel, x);
    }

    public static void m(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull int i2) {
        y(parcel, i, 4);
        parcel.writeInt(i2);
    }

    public static void n(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull int[] iArr, @RecentlyNonNull boolean z) {
        if (iArr == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeIntArray(iArr);
        A(parcel, x);
    }

    public static void o(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull long j) {
        y(parcel, i, 8);
        parcel.writeLong(j);
    }

    public static void p(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Long l, @RecentlyNonNull boolean z) {
        if (l != null) {
            y(parcel, i, 8);
            parcel.writeLong(l.longValue());
        } else if (z) {
            y(parcel, i, 0);
        }
    }

    public static void q(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Parcel parcel2, @RecentlyNonNull boolean z) {
        if (parcel2 == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.appendFrom(parcel2, 0, parcel2.dataSize());
        A(parcel, x);
    }

    public static void r(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull Parcelable parcelable, @RecentlyNonNull int i2, @RecentlyNonNull boolean z) {
        if (parcelable == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcelable.writeToParcel(parcel, i2);
        A(parcel, x);
    }

    public static void s(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull String str, @RecentlyNonNull boolean z) {
        if (str == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeString(str);
        A(parcel, x);
    }

    public static void t(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull String[] strArr, @RecentlyNonNull boolean z) {
        if (strArr == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeStringArray(strArr);
        A(parcel, x);
    }

    public static void u(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull List<String> list, @RecentlyNonNull boolean z) {
        if (list == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeStringList(list);
        A(parcel, x);
    }

    public static <T extends Parcelable> void v(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull T[] tArr, @RecentlyNonNull int i2, @RecentlyNonNull boolean z) {
        if (tArr == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        parcel.writeInt(tArr.length);
        for (T t : tArr) {
            if (t == null) {
                parcel.writeInt(0);
            } else {
                z(parcel, t, i2);
            }
        }
        A(parcel, x);
    }

    public static <T extends Parcelable> void w(@RecentlyNonNull Parcel parcel, @RecentlyNonNull int i, @RecentlyNonNull List<T> list, @RecentlyNonNull boolean z) {
        if (list == null) {
            if (z) {
                y(parcel, i, 0);
                return;
            }
            return;
        }
        int x = x(parcel, i);
        int size = list.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            T t = list.get(i2);
            if (t == null) {
                parcel.writeInt(0);
            } else {
                z(parcel, t, 0);
            }
        }
        A(parcel, x);
    }

    public static int x(Parcel parcel, int i) {
        parcel.writeInt(i | (-65536));
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    public static void y(Parcel parcel, int i, int i2) {
        if (i2 >= 65535) {
            parcel.writeInt(i | (-65536));
            parcel.writeInt(i2);
            return;
        }
        parcel.writeInt(i | (i2 << 16));
    }

    public static <T extends Parcelable> void z(Parcel parcel, T t, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        t.writeToParcel(parcel, i);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }
}
