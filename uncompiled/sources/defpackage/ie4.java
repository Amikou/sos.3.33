package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.io.a;
import java.io.DataInput;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: UTF8DataInputJsonParser.java */
/* renamed from: ie4  reason: default package */
/* loaded from: classes.dex */
public class ie4 extends qp2 {
    public static final int[] V0 = a.i();
    public static final int[] W0 = a.g();
    public c O0;
    public final vs P0;
    public int[] Q0;
    public boolean R0;
    public int S0;
    public DataInput T0;
    public int U0;

    public ie4(jm1 jm1Var, int i, DataInput dataInput, c cVar, vs vsVar, int i2) {
        super(jm1Var, i);
        this.Q0 = new int[16];
        this.U0 = -1;
        this.O0 = cVar;
        this.P0 = vsVar;
        this.T0 = dataInput;
        this.U0 = i2;
    }

    public static int[] A2(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        return Arrays.copyOf(iArr, iArr.length + i);
    }

    public static final int p3(int i, int i2) {
        return i2 == 4 ? i : i | ((-1) << (i2 << 3));
    }

    public JsonToken B2() throws IOException {
        char[] k = this.t0.k();
        int[] iArr = V0;
        int i = 0;
        while (true) {
            int length = k.length;
            if (i >= k.length) {
                k = this.t0.n();
                length = k.length;
                i = 0;
            }
            while (true) {
                int readUnsignedByte = this.T0.readUnsignedByte();
                if (readUnsignedByte == 39) {
                    this.t0.z(i);
                    return JsonToken.VALUE_STRING;
                } else if (iArr[readUnsignedByte] != 0) {
                    int i2 = iArr[readUnsignedByte];
                    if (i2 == 1) {
                        readUnsignedByte = R1();
                    } else if (i2 == 2) {
                        readUnsignedByte = t2(readUnsignedByte);
                    } else if (i2 == 3) {
                        readUnsignedByte = u2(readUnsignedByte);
                    } else if (i2 != 4) {
                        if (readUnsignedByte < 32) {
                            L1(readUnsignedByte, "string value");
                        }
                        S2(readUnsignedByte);
                    } else {
                        int v2 = v2(readUnsignedByte);
                        int i3 = i + 1;
                        k[i] = (char) (55296 | (v2 >> 10));
                        if (i3 >= k.length) {
                            k = this.t0.n();
                            i = 0;
                        } else {
                            i = i3;
                        }
                        readUnsignedByte = 56320 | (v2 & 1023);
                    }
                    if (i >= k.length) {
                        k = this.t0.n();
                        i = 0;
                    }
                    k[i] = (char) readUnsignedByte;
                    i++;
                } else {
                    int i4 = i + 1;
                    k[i] = (char) readUnsignedByte;
                    i = i4;
                    if (i4 >= length) {
                        break;
                    }
                }
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object C() {
        return this.T0;
    }

    public JsonToken C2(int i, boolean z) throws IOException {
        String str;
        while (i == 73) {
            i = this.T0.readUnsignedByte();
            if (i != 78) {
                if (i != 110) {
                    break;
                }
                str = z ? "-Infinity" : "+Infinity";
            } else {
                str = z ? "-INF" : "+INF";
            }
            G2(str, 3);
            if (J0(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS)) {
                return n2(str, z ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);
            }
            w1("Non-standard token '" + str + "': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow");
        }
        l2(i, "expected digit (0-9) to follow minus sign, for valid numeric value");
        return null;
    }

    public final int D2() throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (readUnsignedByte >= 48 && readUnsignedByte <= 57) {
            if (!J0(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS)) {
                i2("Leading zeroes not allowed");
            }
            while (readUnsignedByte == 48) {
                readUnsignedByte = this.T0.readUnsignedByte();
            }
        }
        return readUnsignedByte;
    }

    public String E2(int i) throws IOException {
        if (i == 39 && J0(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return J2();
        }
        if (!J0(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)) {
            I1((char) s2(i), "was expecting double-quote to start field name");
        }
        int[] j = a.j();
        if (j[i] != 0) {
            I1(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int[] iArr = this.Q0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        do {
            if (i2 < 4) {
                i2++;
                i4 = i | (i4 << 8);
            } else {
                if (i3 >= iArr.length) {
                    iArr = A2(iArr, iArr.length);
                    this.Q0 = iArr;
                }
                iArr[i3] = i4;
                i4 = i;
                i3++;
                i2 = 1;
            }
            i = this.T0.readUnsignedByte();
        } while (j[i] == 0);
        this.U0 = i;
        if (i2 > 0) {
            if (i3 >= iArr.length) {
                int[] A2 = A2(iArr, iArr.length);
                this.Q0 = A2;
                iArr = A2;
            }
            iArr[i3] = i4;
            i3++;
        }
        String F = this.P0.F(iArr, i3);
        return F == null ? k3(iArr, i3, i2) : F;
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x001b, code lost:
        if (r4 != 44) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0030, code lost:
        if (r3.r0.e() == false) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0039, code lost:
        if (J0(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_MISSING_VALUES) == false) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x003b, code lost:
        r3.U0 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x003f, code lost:
        return com.fasterxml.jackson.core.JsonToken.VALUE_NULL;
     */
    /* JADX WARN: Removed duplicated region for block: B:43:0x008d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.core.JsonToken F2(int r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 39
            if (r4 == r0) goto L7a
            r0 = 73
            r1 = 1
            if (r4 == r0) goto L60
            r0 = 78
            if (r4 == r0) goto L46
            r0 = 93
            if (r4 == r0) goto L2a
            r0 = 125(0x7d, float:1.75E-43)
            if (r4 == r0) goto L40
            r0 = 43
            if (r4 == r0) goto L1e
            r0 = 44
            if (r4 == r0) goto L33
            goto L87
        L1e:
            java.io.DataInput r4 = r3.T0
            int r4 = r4.readUnsignedByte()
            r0 = 0
            com.fasterxml.jackson.core.JsonToken r4 = r3.C2(r4, r0)
            return r4
        L2a:
            hv1 r0 = r3.r0
            boolean r0 = r0.e()
            if (r0 != 0) goto L33
            goto L87
        L33:
            com.fasterxml.jackson.core.JsonParser$Feature r0 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_MISSING_VALUES
            boolean r0 = r3.J0(r0)
            if (r0 == 0) goto L40
            r3.U0 = r4
            com.fasterxml.jackson.core.JsonToken r4 = com.fasterxml.jackson.core.JsonToken.VALUE_NULL
            return r4
        L40:
            java.lang.String r0 = "expected a value"
            r3.I1(r4, r0)
            goto L7a
        L46:
            java.lang.String r0 = "NaN"
            r3.G2(r0, r1)
            com.fasterxml.jackson.core.JsonParser$Feature r1 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS
            boolean r1 = r3.J0(r1)
            if (r1 == 0) goto L5a
            r1 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            com.fasterxml.jackson.core.JsonToken r4 = r3.n2(r0, r1)
            return r4
        L5a:
            java.lang.String r0 = "Non-standard token 'NaN': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r3.w1(r0)
            goto L87
        L60:
            java.lang.String r0 = "Infinity"
            r3.G2(r0, r1)
            com.fasterxml.jackson.core.JsonParser$Feature r1 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS
            boolean r1 = r3.J0(r1)
            if (r1 == 0) goto L74
            r1 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            com.fasterxml.jackson.core.JsonToken r4 = r3.n2(r0, r1)
            return r4
        L74:
            java.lang.String r0 = "Non-standard token 'Infinity': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r3.w1(r0)
            goto L87
        L7a:
            com.fasterxml.jackson.core.JsonParser$Feature r0 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_SINGLE_QUOTES
            boolean r0 = r3.J0(r0)
            if (r0 == 0) goto L87
            com.fasterxml.jackson.core.JsonToken r4 = r3.B2()
            return r4
        L87:
            boolean r0 = java.lang.Character.isJavaIdentifierStart(r4)
            if (r0 == 0) goto La4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = ""
            r0.append(r1)
            char r1 = (char) r4
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "('true', 'false' or 'null')"
            r3.W2(r4, r0, r1)
        La4:
            java.lang.String r0 = "expected a valid value (number, String, array, object, 'true', 'false' or 'null')"
            r3.I1(r4, r0)
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ie4.F2(int):com.fasterxml.jackson.core.JsonToken");
    }

    public final void G2(String str, int i) throws IOException {
        int length = str.length();
        do {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (readUnsignedByte != str.charAt(i)) {
                V2(readUnsignedByte, str.substring(0, i));
            }
            i++;
        } while (i < length);
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if (readUnsignedByte2 >= 48 && readUnsignedByte2 != 93 && readUnsignedByte2 != 125) {
            q2(str, i, readUnsignedByte2);
        }
        this.U0 = readUnsignedByte2;
    }

    public final JsonToken H2() {
        this.v0 = false;
        JsonToken jsonToken = this.s0;
        this.s0 = null;
        if (jsonToken == JsonToken.START_ARRAY) {
            this.r0 = this.r0.l(this.p0, this.q0);
        } else if (jsonToken == JsonToken.START_OBJECT) {
            this.r0 = this.r0.m(this.p0, this.q0);
        }
        this.g0 = jsonToken;
        return jsonToken;
    }

    public final JsonToken I2(int i) throws IOException {
        if (i == 34) {
            this.R0 = true;
            JsonToken jsonToken = JsonToken.VALUE_STRING;
            this.g0 = jsonToken;
            return jsonToken;
        } else if (i == 45) {
            JsonToken P2 = P2();
            this.g0 = P2;
            return P2;
        } else if (i == 91) {
            this.r0 = this.r0.l(this.p0, this.q0);
            JsonToken jsonToken2 = JsonToken.START_ARRAY;
            this.g0 = jsonToken2;
            return jsonToken2;
        } else if (i == 102) {
            G2("false", 1);
            JsonToken jsonToken3 = JsonToken.VALUE_FALSE;
            this.g0 = jsonToken3;
            return jsonToken3;
        } else if (i == 110) {
            G2("null", 1);
            JsonToken jsonToken4 = JsonToken.VALUE_NULL;
            this.g0 = jsonToken4;
            return jsonToken4;
        } else if (i == 116) {
            G2("true", 1);
            JsonToken jsonToken5 = JsonToken.VALUE_TRUE;
            this.g0 = jsonToken5;
            return jsonToken5;
        } else if (i != 123) {
            switch (i) {
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                    JsonToken Q2 = Q2(i);
                    this.g0 = Q2;
                    return Q2;
                default:
                    JsonToken F2 = F2(i);
                    this.g0 = F2;
                    return F2;
            }
        } else {
            this.r0 = this.r0.m(this.p0, this.q0);
            JsonToken jsonToken6 = JsonToken.START_OBJECT;
            this.g0 = jsonToken6;
            return jsonToken6;
        }
    }

    public String J2() throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (readUnsignedByte == 39) {
            return "";
        }
        int[] iArr = this.Q0;
        int[] iArr2 = W0;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (readUnsignedByte != 39) {
            if (readUnsignedByte != 34 && iArr2[readUnsignedByte] != 0) {
                if (readUnsignedByte != 92) {
                    L1(readUnsignedByte, PublicResolver.FUNC_NAME);
                } else {
                    readUnsignedByte = R1();
                }
                if (readUnsignedByte > 127) {
                    if (i >= 4) {
                        if (i2 >= iArr.length) {
                            iArr = A2(iArr, iArr.length);
                            this.Q0 = iArr;
                        }
                        iArr[i2] = i3;
                        i3 = 0;
                        i2++;
                        i = 0;
                    }
                    if (readUnsignedByte < 2048) {
                        i3 = (i3 << 8) | (readUnsignedByte >> 6) | 192;
                        i++;
                    } else {
                        int i4 = (i3 << 8) | (readUnsignedByte >> 12) | 224;
                        int i5 = i + 1;
                        if (i5 >= 4) {
                            if (i2 >= iArr.length) {
                                iArr = A2(iArr, iArr.length);
                                this.Q0 = iArr;
                            }
                            iArr[i2] = i4;
                            i4 = 0;
                            i2++;
                            i5 = 0;
                        }
                        i3 = (i4 << 8) | ((readUnsignedByte >> 6) & 63) | 128;
                        i = i5 + 1;
                    }
                    readUnsignedByte = (readUnsignedByte & 63) | 128;
                }
            }
            if (i < 4) {
                i++;
                i3 = readUnsignedByte | (i3 << 8);
            } else {
                if (i2 >= iArr.length) {
                    iArr = A2(iArr, iArr.length);
                    this.Q0 = iArr;
                }
                iArr[i2] = i3;
                i3 = readUnsignedByte;
                i2++;
                i = 1;
            }
            readUnsignedByte = this.T0.readUnsignedByte();
        }
        if (i > 0) {
            if (i2 >= iArr.length) {
                int[] A2 = A2(iArr, iArr.length);
                this.Q0 = A2;
                iArr = A2;
            }
            iArr[i2] = p3(i3, i);
            i2++;
        }
        String F = this.P0.F(iArr, i2);
        return F == null ? k3(iArr, i2, i) : F;
    }

    public final JsonToken K2(char[] cArr, int i, int i2, boolean z, int i3) throws IOException {
        int i4;
        int i5;
        int readUnsignedByte;
        int i6 = 0;
        if (i2 == 46) {
            cArr[i] = (char) i2;
            int i7 = 0;
            i++;
            while (true) {
                readUnsignedByte = this.T0.readUnsignedByte();
                if (readUnsignedByte < 48 || readUnsignedByte > 57) {
                    break;
                }
                i7++;
                if (i >= cArr.length) {
                    cArr = this.t0.n();
                    i = 0;
                }
                cArr[i] = (char) readUnsignedByte;
                i++;
            }
            if (i7 == 0) {
                l2(readUnsignedByte, "Decimal point not followed by a digit");
            }
            i4 = i7;
            i2 = readUnsignedByte;
        } else {
            i4 = 0;
        }
        if (i2 == 101 || i2 == 69) {
            if (i >= cArr.length) {
                cArr = this.t0.n();
                i = 0;
            }
            int i8 = i + 1;
            cArr[i] = (char) i2;
            int readUnsignedByte2 = this.T0.readUnsignedByte();
            if (readUnsignedByte2 == 45 || readUnsignedByte2 == 43) {
                if (i8 >= cArr.length) {
                    cArr = this.t0.n();
                    i8 = 0;
                }
                int i9 = i8 + 1;
                cArr[i8] = (char) readUnsignedByte2;
                i5 = 0;
                i2 = this.T0.readUnsignedByte();
                i = i9;
            } else {
                i2 = readUnsignedByte2;
                i = i8;
                i5 = 0;
            }
            while (i2 <= 57 && i2 >= 48) {
                i5++;
                if (i >= cArr.length) {
                    cArr = this.t0.n();
                    i = 0;
                }
                cArr[i] = (char) i2;
                i2 = this.T0.readUnsignedByte();
                i++;
            }
            if (i5 == 0) {
                l2(i2, "Exponent indicator not followed by a digit");
            }
            i6 = i5;
        }
        this.U0 = i2;
        if (this.r0.g()) {
            j3();
        }
        this.t0.z(i);
        return o2(z, i3, i4, i6);
    }

    public final String L2(int i, int i2, int i3) throws IOException {
        int[] iArr = this.Q0;
        iArr[0] = this.S0;
        iArr[1] = i2;
        iArr[2] = i3;
        int[] iArr2 = W0;
        int i4 = i;
        int i5 = 3;
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (iArr2[readUnsignedByte] != 0) {
                if (readUnsignedByte == 34) {
                    return o3(this.Q0, i5, i4, 1);
                }
                return q3(this.Q0, i5, i4, readUnsignedByte, 1);
            }
            int i6 = (i4 << 8) | readUnsignedByte;
            int readUnsignedByte2 = this.T0.readUnsignedByte();
            if (iArr2[readUnsignedByte2] != 0) {
                if (readUnsignedByte2 == 34) {
                    return o3(this.Q0, i5, i6, 2);
                }
                return q3(this.Q0, i5, i6, readUnsignedByte2, 2);
            }
            int i7 = (i6 << 8) | readUnsignedByte2;
            int readUnsignedByte3 = this.T0.readUnsignedByte();
            if (iArr2[readUnsignedByte3] != 0) {
                if (readUnsignedByte3 == 34) {
                    return o3(this.Q0, i5, i7, 3);
                }
                return q3(this.Q0, i5, i7, readUnsignedByte3, 3);
            }
            int i8 = (i7 << 8) | readUnsignedByte3;
            int readUnsignedByte4 = this.T0.readUnsignedByte();
            if (iArr2[readUnsignedByte4] != 0) {
                if (readUnsignedByte4 == 34) {
                    return o3(this.Q0, i5, i8, 4);
                }
                return q3(this.Q0, i5, i8, readUnsignedByte4, 4);
            }
            int[] iArr3 = this.Q0;
            if (i5 >= iArr3.length) {
                this.Q0 = A2(iArr3, i5);
            }
            this.Q0[i5] = i8;
            i5++;
            i4 = readUnsignedByte4;
        }
    }

    public final String M2(int i) throws IOException {
        int[] iArr = W0;
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte] != 0) {
            if (readUnsignedByte == 34) {
                return m3(this.S0, i, 1);
            }
            return s3(this.S0, i, readUnsignedByte, 1);
        }
        int i2 = (i << 8) | readUnsignedByte;
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte2] != 0) {
            if (readUnsignedByte2 == 34) {
                return m3(this.S0, i2, 2);
            }
            return s3(this.S0, i2, readUnsignedByte2, 2);
        }
        int i3 = (i2 << 8) | readUnsignedByte2;
        int readUnsignedByte3 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte3] != 0) {
            if (readUnsignedByte3 == 34) {
                return m3(this.S0, i3, 3);
            }
            return s3(this.S0, i3, readUnsignedByte3, 3);
        }
        int i4 = (i3 << 8) | readUnsignedByte3;
        int readUnsignedByte4 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte4] != 0) {
            if (readUnsignedByte4 == 34) {
                return m3(this.S0, i4, 4);
            }
            return s3(this.S0, i4, readUnsignedByte4, 4);
        }
        return N2(readUnsignedByte4, i4);
    }

    public final String N2(int i, int i2) throws IOException {
        int[] iArr = W0;
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte] != 0) {
            if (readUnsignedByte == 34) {
                return n3(this.S0, i2, i, 1);
            }
            return t3(this.S0, i2, i, readUnsignedByte, 1);
        }
        int i3 = (i << 8) | readUnsignedByte;
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte2] != 0) {
            if (readUnsignedByte2 == 34) {
                return n3(this.S0, i2, i3, 2);
            }
            return t3(this.S0, i2, i3, readUnsignedByte2, 2);
        }
        int i4 = (i3 << 8) | readUnsignedByte2;
        int readUnsignedByte3 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte3] != 0) {
            if (readUnsignedByte3 == 34) {
                return n3(this.S0, i2, i4, 3);
            }
            return t3(this.S0, i2, i4, readUnsignedByte3, 3);
        }
        int i5 = (i4 << 8) | readUnsignedByte3;
        int readUnsignedByte4 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte4] != 0) {
            if (readUnsignedByte4 == 34) {
                return n3(this.S0, i2, i5, 4);
            }
            return t3(this.S0, i2, i5, readUnsignedByte4, 4);
        }
        return L2(readUnsignedByte4, i2, i5);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String O0() throws IOException {
        JsonToken P2;
        this.y0 = 0;
        JsonToken jsonToken = this.g0;
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (jsonToken == jsonToken2) {
            H2();
            return null;
        }
        if (this.R0) {
            c3();
        }
        int g3 = g3();
        this.x0 = null;
        this.p0 = this.m0;
        if (g3 == 93) {
            if (!this.r0.e()) {
                Z1(g3, '}');
            }
            this.r0 = this.r0.k();
            this.g0 = JsonToken.END_ARRAY;
            return null;
        } else if (g3 == 125) {
            if (!this.r0.f()) {
                Z1(g3, ']');
            }
            this.r0 = this.r0.k();
            this.g0 = JsonToken.END_OBJECT;
            return null;
        } else {
            if (this.r0.o()) {
                if (g3 != 44) {
                    I1(g3, "was expecting comma to separate " + this.r0.i() + " entries");
                }
                g3 = g3();
            }
            if (!this.r0.f()) {
                I2(g3);
                return null;
            }
            String O2 = O2(g3);
            this.r0.t(O2);
            this.g0 = jsonToken2;
            int Y2 = Y2();
            if (Y2 == 34) {
                this.R0 = true;
                this.s0 = JsonToken.VALUE_STRING;
                return O2;
            }
            if (Y2 == 45) {
                P2 = P2();
            } else if (Y2 == 91) {
                P2 = JsonToken.START_ARRAY;
            } else if (Y2 == 102) {
                G2("false", 1);
                P2 = JsonToken.VALUE_FALSE;
            } else if (Y2 == 110) {
                G2("null", 1);
                P2 = JsonToken.VALUE_NULL;
            } else if (Y2 == 116) {
                G2("true", 1);
                P2 = JsonToken.VALUE_TRUE;
            } else if (Y2 != 123) {
                switch (Y2) {
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                        P2 = Q2(Y2);
                        break;
                    default:
                        P2 = F2(Y2);
                        break;
                }
            } else {
                P2 = JsonToken.START_OBJECT;
            }
            this.s0 = P2;
            return O2;
        }
    }

    @Override // defpackage.qp2
    public void O1() throws IOException {
    }

    public final String O2(int i) throws IOException {
        if (i != 34) {
            return E2(i);
        }
        int[] iArr = W0;
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte] != 0) {
            return readUnsignedByte == 34 ? "" : r3(0, readUnsignedByte, 0);
        }
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte2] != 0) {
            if (readUnsignedByte2 == 34) {
                return l3(readUnsignedByte, 1);
            }
            return r3(readUnsignedByte, readUnsignedByte2, 1);
        }
        int i2 = (readUnsignedByte << 8) | readUnsignedByte2;
        int readUnsignedByte3 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte3] != 0) {
            if (readUnsignedByte3 == 34) {
                return l3(i2, 2);
            }
            return r3(i2, readUnsignedByte3, 2);
        }
        int i3 = (i2 << 8) | readUnsignedByte3;
        int readUnsignedByte4 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte4] != 0) {
            if (readUnsignedByte4 == 34) {
                return l3(i3, 3);
            }
            return r3(i3, readUnsignedByte4, 3);
        }
        int i4 = (i3 << 8) | readUnsignedByte4;
        int readUnsignedByte5 = this.T0.readUnsignedByte();
        if (iArr[readUnsignedByte5] == 0) {
            this.S0 = i4;
            return M2(readUnsignedByte5);
        } else if (readUnsignedByte5 == 34) {
            return l3(i4, 4);
        } else {
            return r3(i4, readUnsignedByte5, 4);
        }
    }

    public JsonToken P2() throws IOException {
        int readUnsignedByte;
        char[] k = this.t0.k();
        k[0] = '-';
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        k[1] = (char) readUnsignedByte2;
        if (readUnsignedByte2 <= 48) {
            if (readUnsignedByte2 == 48) {
                readUnsignedByte = D2();
            } else {
                return C2(readUnsignedByte2, true);
            }
        } else if (readUnsignedByte2 > 57) {
            return C2(readUnsignedByte2, true);
        } else {
            readUnsignedByte = this.T0.readUnsignedByte();
        }
        int i = 2;
        int i2 = 1;
        while (readUnsignedByte <= 57 && readUnsignedByte >= 48) {
            i2++;
            k[i] = (char) readUnsignedByte;
            readUnsignedByte = this.T0.readUnsignedByte();
            i++;
        }
        if (readUnsignedByte != 46 && readUnsignedByte != 101 && readUnsignedByte != 69) {
            this.t0.z(i);
            this.U0 = readUnsignedByte;
            if (this.r0.g()) {
                j3();
            }
            return p2(true, i2);
        }
        return K2(k, i, readUnsignedByte, true, i2);
    }

    public JsonToken Q2(int i) throws IOException {
        int readUnsignedByte;
        char[] k = this.t0.k();
        int i2 = 1;
        if (i == 48) {
            readUnsignedByte = D2();
            if (readUnsignedByte > 57 || readUnsignedByte < 48) {
                k[0] = '0';
            } else {
                i2 = 0;
            }
        } else {
            k[0] = (char) i;
            readUnsignedByte = this.T0.readUnsignedByte();
        }
        int i3 = readUnsignedByte;
        int i4 = i2;
        int i5 = i4;
        while (i3 <= 57 && i3 >= 48) {
            i5++;
            k[i4] = (char) i3;
            i3 = this.T0.readUnsignedByte();
            i4++;
        }
        if (i3 != 46 && i3 != 101 && i3 != 69) {
            this.t0.z(i4);
            if (this.r0.g()) {
                j3();
            } else {
                this.U0 = i3;
            }
            return p2(false, i5);
        }
        return K2(k, i4, i3, false, i5);
    }

    @Override // defpackage.qp2
    public char R1() throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (readUnsignedByte == 34 || readUnsignedByte == 47 || readUnsignedByte == 92) {
            return (char) readUnsignedByte;
        }
        if (readUnsignedByte != 98) {
            if (readUnsignedByte != 102) {
                if (readUnsignedByte != 110) {
                    if (readUnsignedByte != 114) {
                        if (readUnsignedByte != 116) {
                            if (readUnsignedByte != 117) {
                                return s1((char) s2(readUnsignedByte));
                            }
                            int i = 0;
                            for (int i2 = 0; i2 < 4; i2++) {
                                int readUnsignedByte2 = this.T0.readUnsignedByte();
                                int b = a.b(readUnsignedByte2);
                                if (b < 0) {
                                    I1(readUnsignedByte2, "expected a hex-digit for character escape sequence");
                                }
                                i = (i << 4) | b;
                            }
                            return (char) i;
                        }
                        return '\t';
                    }
                    return '\r';
                }
                return '\n';
            }
            return '\f';
        }
        return '\b';
    }

    public int R2(Base64Variant base64Variant, OutputStream outputStream, byte[] bArr) throws IOException {
        int i;
        int length = bArr.length - 3;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (readUnsignedByte > 32) {
                int decodeBase64Char = base64Variant.decodeBase64Char(readUnsignedByte);
                if (decodeBase64Char < 0) {
                    if (readUnsignedByte == 34) {
                        break;
                    }
                    decodeBase64Char = Q1(base64Variant, readUnsignedByte, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                if (i2 > length) {
                    i3 += i2;
                    outputStream.write(bArr, 0, i2);
                    i2 = 0;
                }
                int readUnsignedByte2 = this.T0.readUnsignedByte();
                int decodeBase64Char2 = base64Variant.decodeBase64Char(readUnsignedByte2);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = Q1(base64Variant, readUnsignedByte2, 1);
                }
                int i4 = (decodeBase64Char << 6) | decodeBase64Char2;
                int readUnsignedByte3 = this.T0.readUnsignedByte();
                int decodeBase64Char3 = base64Variant.decodeBase64Char(readUnsignedByte3);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (readUnsignedByte3 == 34 && !base64Variant.usesPadding()) {
                            bArr[i2] = (byte) (i4 >> 4);
                            i2++;
                            break;
                        }
                        decodeBase64Char3 = Q1(base64Variant, readUnsignedByte3, 2);
                    }
                    if (decodeBase64Char3 == -2) {
                        int readUnsignedByte4 = this.T0.readUnsignedByte();
                        if (base64Variant.usesPaddingChar(readUnsignedByte4)) {
                            i = i2 + 1;
                            bArr[i2] = (byte) (i4 >> 4);
                            i2 = i;
                        } else {
                            throw h2(base64Variant, readUnsignedByte4, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                    }
                }
                int i5 = (i4 << 6) | decodeBase64Char3;
                int readUnsignedByte5 = this.T0.readUnsignedByte();
                int decodeBase64Char4 = base64Variant.decodeBase64Char(readUnsignedByte5);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (readUnsignedByte5 == 34 && !base64Variant.usesPadding()) {
                            int i6 = i5 >> 2;
                            int i7 = i2 + 1;
                            bArr[i2] = (byte) (i6 >> 8);
                            i2 = i7 + 1;
                            bArr[i7] = (byte) i6;
                            break;
                        }
                        decodeBase64Char4 = Q1(base64Variant, readUnsignedByte5, 3);
                    }
                    if (decodeBase64Char4 == -2) {
                        int i8 = i5 >> 2;
                        int i9 = i2 + 1;
                        bArr[i2] = (byte) (i8 >> 8);
                        i2 = i9 + 1;
                        bArr[i9] = (byte) i8;
                    }
                }
                int i10 = (i5 << 6) | decodeBase64Char4;
                int i11 = i2 + 1;
                bArr[i2] = (byte) (i10 >> 16);
                int i12 = i11 + 1;
                bArr[i11] = (byte) (i10 >> 8);
                i = i12 + 1;
                bArr[i12] = (byte) i10;
                i2 = i;
            }
        }
        this.R0 = false;
        if (i2 > 0) {
            int i13 = i3 + i2;
            outputStream.write(bArr, 0, i2);
            return i13;
        }
        return i3;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String S0() throws IOException {
        if (this.g0 == JsonToken.FIELD_NAME) {
            this.v0 = false;
            JsonToken jsonToken = this.s0;
            this.s0 = null;
            this.g0 = jsonToken;
            if (jsonToken == JsonToken.VALUE_STRING) {
                if (this.R0) {
                    this.R0 = false;
                    return w2();
                }
                return this.t0.j();
            }
            if (jsonToken == JsonToken.START_ARRAY) {
                this.r0 = this.r0.l(this.p0, this.q0);
            } else if (jsonToken == JsonToken.START_OBJECT) {
                this.r0 = this.r0.m(this.p0, this.q0);
            }
            return null;
        } else if (T0() == JsonToken.VALUE_STRING) {
            return X();
        } else {
            return null;
        }
    }

    public void S2(int i) throws JsonParseException {
        if (i < 32) {
            K1(i);
        }
        T2(i);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken T0() throws IOException {
        JsonToken P2;
        JsonToken jsonToken = this.g0;
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (jsonToken == jsonToken2) {
            return H2();
        }
        this.y0 = 0;
        if (this.R0) {
            c3();
        }
        int g3 = g3();
        this.x0 = null;
        this.p0 = this.m0;
        if (g3 == 93) {
            if (!this.r0.e()) {
                Z1(g3, '}');
            }
            this.r0 = this.r0.k();
            JsonToken jsonToken3 = JsonToken.END_ARRAY;
            this.g0 = jsonToken3;
            return jsonToken3;
        } else if (g3 == 125) {
            if (!this.r0.f()) {
                Z1(g3, ']');
            }
            this.r0 = this.r0.k();
            JsonToken jsonToken4 = JsonToken.END_OBJECT;
            this.g0 = jsonToken4;
            return jsonToken4;
        } else {
            if (this.r0.o()) {
                if (g3 != 44) {
                    I1(g3, "was expecting comma to separate " + this.r0.i() + " entries");
                }
                g3 = g3();
            }
            if (!this.r0.f()) {
                return I2(g3);
            }
            this.r0.t(O2(g3));
            this.g0 = jsonToken2;
            int Y2 = Y2();
            if (Y2 == 34) {
                this.R0 = true;
                this.s0 = JsonToken.VALUE_STRING;
                return this.g0;
            }
            if (Y2 == 45) {
                P2 = P2();
            } else if (Y2 == 91) {
                P2 = JsonToken.START_ARRAY;
            } else if (Y2 == 102) {
                G2("false", 1);
                P2 = JsonToken.VALUE_FALSE;
            } else if (Y2 == 110) {
                G2("null", 1);
                P2 = JsonToken.VALUE_NULL;
            } else if (Y2 == 116) {
                G2("true", 1);
                P2 = JsonToken.VALUE_TRUE;
            } else if (Y2 != 123) {
                switch (Y2) {
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                        P2 = Q2(Y2);
                        break;
                    default:
                        P2 = F2(Y2);
                        break;
                }
            } else {
                P2 = JsonToken.START_OBJECT;
            }
            this.s0 = P2;
            return this.g0;
        }
    }

    public void T2(int i) throws JsonParseException {
        w1("Invalid UTF-8 start byte 0x" + Integer.toHexString(i));
    }

    public final void U2(int i) throws JsonParseException {
        w1("Invalid UTF-8 middle byte 0x" + Integer.toHexString(i));
    }

    public void V2(int i, String str) throws IOException {
        W2(i, str, "'null', 'true', 'false' or NaN");
    }

    public void W2(int i, String str, String str2) throws IOException {
        StringBuilder sb = new StringBuilder(str);
        while (true) {
            char s2 = (char) s2(i);
            if (!Character.isJavaIdentifierPart(s2)) {
                w1("Unrecognized token '" + sb.toString() + "': was expecting " + str2);
                return;
            }
            sb.append(s2);
            i = this.T0.readUnsignedByte();
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String X() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                return w2();
            }
            return this.t0.j();
        }
        return z2(jsonToken);
    }

    public final void X2() throws IOException {
        int[] f = a.f();
        int readUnsignedByte = this.T0.readUnsignedByte();
        while (true) {
            int i = f[readUnsignedByte];
            if (i != 0) {
                if (i == 2) {
                    d3();
                } else if (i == 3) {
                    e3();
                } else if (i == 4) {
                    f3();
                } else if (i == 10 || i == 13) {
                    this.m0++;
                } else if (i != 42) {
                    S2(readUnsignedByte);
                } else {
                    readUnsignedByte = this.T0.readUnsignedByte();
                    if (readUnsignedByte == 47) {
                        return;
                    }
                }
            }
            readUnsignedByte = this.T0.readUnsignedByte();
        }
    }

    @Override // defpackage.qp2
    public void Y1() throws IOException {
        super.Y1();
        this.P0.M();
    }

    public final int Y2() throws IOException {
        int i = this.U0;
        if (i < 0) {
            i = this.T0.readUnsignedByte();
        } else {
            this.U0 = -1;
        }
        if (i == 58) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (readUnsignedByte > 32) {
                return (readUnsignedByte == 47 || readUnsignedByte == 35) ? Z2(readUnsignedByte, true) : readUnsignedByte;
            } else if ((readUnsignedByte == 32 || readUnsignedByte == 9) && (readUnsignedByte = this.T0.readUnsignedByte()) > 32) {
                return (readUnsignedByte == 47 || readUnsignedByte == 35) ? Z2(readUnsignedByte, true) : readUnsignedByte;
            } else {
                return Z2(readUnsignedByte, true);
            }
        }
        if (i == 32 || i == 9) {
            i = this.T0.readUnsignedByte();
        }
        if (i == 58) {
            int readUnsignedByte2 = this.T0.readUnsignedByte();
            if (readUnsignedByte2 > 32) {
                return (readUnsignedByte2 == 47 || readUnsignedByte2 == 35) ? Z2(readUnsignedByte2, true) : readUnsignedByte2;
            } else if ((readUnsignedByte2 == 32 || readUnsignedByte2 == 9) && (readUnsignedByte2 = this.T0.readUnsignedByte()) > 32) {
                return (readUnsignedByte2 == 47 || readUnsignedByte2 == 35) ? Z2(readUnsignedByte2, true) : readUnsignedByte2;
            } else {
                return Z2(readUnsignedByte2, true);
            }
        }
        return Z2(i, false);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
        if (this.R0 && this.g0 == JsonToken.VALUE_STRING) {
            byte[] d = this.h0.d();
            try {
                return R2(base64Variant, outputStream, d);
            } finally {
                this.h0.o(d);
            }
        }
        byte[] j = j(base64Variant);
        outputStream.write(j);
        return j.length;
    }

    public final int Z2(int i, boolean z) throws IOException {
        while (true) {
            if (i > 32) {
                if (i == 47) {
                    a3();
                } else if (i != 35 || !i3()) {
                    if (z) {
                        return i;
                    }
                    if (i != 58) {
                        I1(i, "was expecting a colon to separate field name and value");
                    }
                    z = true;
                }
            } else if (i == 13 || i == 10) {
                this.m0++;
            }
            i = this.T0.readUnsignedByte();
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public char[] a0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 5) {
                if (id != 6) {
                    if (id != 7 && id != 8) {
                        return this.g0.asCharArray();
                    }
                } else if (this.R0) {
                    this.R0 = false;
                    x2();
                }
                return this.t0.q();
            }
            if (!this.v0) {
                String b = this.r0.b();
                int length = b.length();
                char[] cArr = this.u0;
                if (cArr == null) {
                    this.u0 = this.h0.f(length);
                } else if (cArr.length < length) {
                    this.u0 = new char[length];
                }
                b.getChars(0, length, this.u0, 0);
                this.v0 = true;
            }
            return this.u0;
        }
        return null;
    }

    public final void a3() throws IOException {
        if (!J0(JsonParser.Feature.ALLOW_COMMENTS)) {
            I1(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        int readUnsignedByte = this.T0.readUnsignedByte();
        if (readUnsignedByte == 47) {
            b3();
        } else if (readUnsignedByte == 42) {
            X2();
        } else {
            I1(readUnsignedByte, "was expecting either '*' or '/' for a comment");
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int b0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                x2();
            }
            return this.t0.A();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return this.r0.b().length();
        } else {
            if (jsonToken != null) {
                if (jsonToken.isNumeric()) {
                    return this.t0.A();
                }
                return this.g0.asCharArray().length;
            }
            return 0;
        }
    }

    public final void b3() throws IOException {
        int[] f = a.f();
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            int i = f[readUnsignedByte];
            if (i != 0) {
                if (i == 2) {
                    d3();
                } else if (i == 3) {
                    e3();
                } else if (i == 4) {
                    f3();
                } else if (i == 10 || i == 13) {
                    break;
                } else if (i != 42 && i < 0) {
                    S2(readUnsignedByte);
                }
            }
        }
        this.m0++;
    }

    public void c3() throws IOException {
        this.R0 = false;
        int[] iArr = V0;
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (iArr[readUnsignedByte] != 0) {
                if (readUnsignedByte == 34) {
                    return;
                }
                int i = iArr[readUnsignedByte];
                if (i == 1) {
                    R1();
                } else if (i == 2) {
                    d3();
                } else if (i == 3) {
                    e3();
                } else if (i == 4) {
                    f3();
                } else if (readUnsignedByte < 32) {
                    L1(readUnsignedByte, "string value");
                } else {
                    S2(readUnsignedByte);
                }
            }
        }
    }

    public final void d3() throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if ((readUnsignedByte & 192) != 128) {
            U2(readUnsignedByte & 255);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r0 != 8) goto L15;
     */
    @Override // com.fasterxml.jackson.core.JsonParser
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public int e0() throws java.io.IOException {
        /*
            r3 = this;
            com.fasterxml.jackson.core.JsonToken r0 = r3.g0
            r1 = 0
            if (r0 == 0) goto L24
            int r0 = r0.id()
            r2 = 6
            if (r0 == r2) goto L14
            r2 = 7
            if (r0 == r2) goto L1d
            r2 = 8
            if (r0 == r2) goto L1d
            goto L24
        L14:
            boolean r0 = r3.R0
            if (r0 == 0) goto L1d
            r3.R0 = r1
            r3.x2()
        L1d:
            com.fasterxml.jackson.core.util.c r0 = r3.t0
            int r0 = r0.r()
            return r0
        L24:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ie4.e0():int");
    }

    public final void e3() throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if ((readUnsignedByte & 192) != 128) {
            U2(readUnsignedByte & 255);
        }
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if ((readUnsignedByte2 & 192) != 128) {
            U2(readUnsignedByte2 & 255);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation f0() {
        return new JsonLocation(this.h0.m(), -1L, -1L, this.p0, -1);
    }

    public final void f3() throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if ((readUnsignedByte & 192) != 128) {
            U2(readUnsignedByte & 255);
        }
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if ((readUnsignedByte2 & 192) != 128) {
            U2(readUnsignedByte2 & 255);
        }
        int readUnsignedByte3 = this.T0.readUnsignedByte();
        if ((readUnsignedByte3 & 192) != 128) {
            U2(readUnsignedByte3 & 255);
        }
    }

    public final int g3() throws IOException {
        int i = this.U0;
        if (i < 0) {
            i = this.T0.readUnsignedByte();
        } else {
            this.U0 = -1;
        }
        while (i <= 32) {
            if (i == 13 || i == 10) {
                this.m0++;
            }
            i = this.T0.readUnsignedByte();
        }
        return (i == 47 || i == 35) ? h3(i) : i;
    }

    public final int h3(int i) throws IOException {
        while (true) {
            if (i > 32) {
                if (i == 47) {
                    a3();
                } else if (i != 35 || !i3()) {
                    break;
                }
            } else if (i == 13 || i == 10) {
                this.m0++;
            }
            i = this.T0.readUnsignedByte();
        }
        return i;
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public int i0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_NUMBER_INT && jsonToken != JsonToken.VALUE_NUMBER_FLOAT) {
            return super.m0(0);
        }
        int i = this.y0;
        if ((i & 1) == 0) {
            if (i == 0) {
                return U1();
            }
            if ((i & 1) == 0) {
                d2();
            }
        }
        return this.z0;
    }

    public final boolean i3() throws IOException {
        if (J0(JsonParser.Feature.ALLOW_YAML_COMMENTS)) {
            b3();
            return true;
        }
        return false;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public byte[] j(Base64Variant base64Variant) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_STRING && (jsonToken != JsonToken.VALUE_EMBEDDED_OBJECT || this.x0 == null)) {
            w1("Current token (" + this.g0 + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this.R0) {
            try {
                this.x0 = r2(base64Variant);
                this.R0 = false;
            } catch (IllegalArgumentException e) {
                throw a("Failed to decode VALUE_STRING as base64 (" + base64Variant + "): " + e.getMessage());
            }
        } else if (this.x0 == null) {
            ms T1 = T1();
            o1(X(), T1, base64Variant);
            this.x0 = T1.n();
        }
        return this.x0;
    }

    public final void j3() throws IOException {
        int i = this.U0;
        if (i <= 32) {
            this.U0 = -1;
            if (i == 13 || i == 10) {
                this.m0++;
                return;
            }
            return;
        }
        H1(i);
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x00c2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.String k3(int[] r17, int r18, int r19) throws com.fasterxml.jackson.core.JsonParseException {
        /*
            Method dump skipped, instructions count: 265
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ie4.k3(int[], int, int):java.lang.String");
    }

    public final String l3(int i, int i2) throws JsonParseException {
        int p3 = p3(i, i2);
        String C = this.P0.C(p3);
        if (C != null) {
            return C;
        }
        int[] iArr = this.Q0;
        iArr[0] = p3;
        return k3(iArr, 1, i2);
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public int m0(int i) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_NUMBER_INT && jsonToken != JsonToken.VALUE_NUMBER_FLOAT) {
            return super.m0(i);
        }
        int i2 = this.y0;
        if ((i2 & 1) == 0) {
            if (i2 == 0) {
                return U1();
            }
            if ((i2 & 1) == 0) {
                d2();
            }
        }
        return this.z0;
    }

    public final String m3(int i, int i2, int i3) throws JsonParseException {
        int p3 = p3(i2, i3);
        String D = this.P0.D(i, p3);
        if (D != null) {
            return D;
        }
        int[] iArr = this.Q0;
        iArr[0] = i;
        iArr[1] = p3;
        return k3(iArr, 2, i3);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public c n() {
        return this.O0;
    }

    public final String n3(int i, int i2, int i3, int i4) throws JsonParseException {
        int p3 = p3(i3, i4);
        String E = this.P0.E(i, i2, p3);
        if (E != null) {
            return E;
        }
        int[] iArr = this.Q0;
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = p3(p3, i4);
        return k3(iArr, 3, i4);
    }

    public final String o3(int[] iArr, int i, int i2, int i3) throws JsonParseException {
        if (i >= iArr.length) {
            iArr = A2(iArr, iArr.length);
            this.Q0 = iArr;
        }
        int i4 = i + 1;
        iArr[i] = p3(i2, i3);
        String F = this.P0.F(iArr, i4);
        return F == null ? k3(iArr, i4, i3) : F;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation q() {
        return new JsonLocation(this.h0.m(), -1L, -1L, this.m0, -1);
    }

    public final void q2(String str, int i, int i2) throws IOException {
        char s2 = (char) s2(i2);
        if (Character.isJavaIdentifierPart(s2)) {
            V2(s2, str.substring(0, i));
        }
    }

    public final String q3(int[] iArr, int i, int i2, int i3, int i4) throws IOException {
        int[] iArr2 = W0;
        while (true) {
            if (iArr2[i3] != 0) {
                if (i3 == 34) {
                    break;
                }
                if (i3 != 92) {
                    L1(i3, PublicResolver.FUNC_NAME);
                } else {
                    i3 = R1();
                }
                if (i3 > 127) {
                    int i5 = 0;
                    if (i4 >= 4) {
                        if (i >= iArr.length) {
                            iArr = A2(iArr, iArr.length);
                            this.Q0 = iArr;
                        }
                        iArr[i] = i2;
                        i++;
                        i2 = 0;
                        i4 = 0;
                    }
                    if (i3 < 2048) {
                        i2 = (i2 << 8) | (i3 >> 6) | 192;
                        i4++;
                    } else {
                        int i6 = (i2 << 8) | (i3 >> 12) | 224;
                        int i7 = i4 + 1;
                        if (i7 >= 4) {
                            if (i >= iArr.length) {
                                iArr = A2(iArr, iArr.length);
                                this.Q0 = iArr;
                            }
                            iArr[i] = i6;
                            i++;
                            i7 = 0;
                        } else {
                            i5 = i6;
                        }
                        i2 = (i5 << 8) | ((i3 >> 6) & 63) | 128;
                        i4 = i7 + 1;
                    }
                    i3 = (i3 & 63) | 128;
                }
            }
            if (i4 < 4) {
                i4++;
                i2 = (i2 << 8) | i3;
            } else {
                if (i >= iArr.length) {
                    iArr = A2(iArr, iArr.length);
                    this.Q0 = iArr;
                }
                iArr[i] = i2;
                i2 = i3;
                i++;
                i4 = 1;
            }
            i3 = this.T0.readUnsignedByte();
        }
        if (i4 > 0) {
            if (i >= iArr.length) {
                iArr = A2(iArr, iArr.length);
                this.Q0 = iArr;
            }
            iArr[i] = p3(i2, i4);
            i++;
        }
        String F = this.P0.F(iArr, i);
        return F == null ? k3(iArr, i, i4) : F;
    }

    public final byte[] r2(Base64Variant base64Variant) throws IOException {
        ms T1 = T1();
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (readUnsignedByte > 32) {
                int decodeBase64Char = base64Variant.decodeBase64Char(readUnsignedByte);
                if (decodeBase64Char < 0) {
                    if (readUnsignedByte == 34) {
                        return T1.n();
                    }
                    decodeBase64Char = Q1(base64Variant, readUnsignedByte, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                int readUnsignedByte2 = this.T0.readUnsignedByte();
                int decodeBase64Char2 = base64Variant.decodeBase64Char(readUnsignedByte2);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = Q1(base64Variant, readUnsignedByte2, 1);
                }
                int i = (decodeBase64Char << 6) | decodeBase64Char2;
                int readUnsignedByte3 = this.T0.readUnsignedByte();
                int decodeBase64Char3 = base64Variant.decodeBase64Char(readUnsignedByte3);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (readUnsignedByte3 == 34 && !base64Variant.usesPadding()) {
                            T1.b(i >> 4);
                            return T1.n();
                        }
                        decodeBase64Char3 = Q1(base64Variant, readUnsignedByte3, 2);
                    }
                    if (decodeBase64Char3 == -2) {
                        int readUnsignedByte4 = this.T0.readUnsignedByte();
                        if (base64Variant.usesPaddingChar(readUnsignedByte4)) {
                            T1.b(i >> 4);
                        } else {
                            throw h2(base64Variant, readUnsignedByte4, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                    }
                }
                int i2 = (i << 6) | decodeBase64Char3;
                int readUnsignedByte5 = this.T0.readUnsignedByte();
                int decodeBase64Char4 = base64Variant.decodeBase64Char(readUnsignedByte5);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (readUnsignedByte5 == 34 && !base64Variant.usesPadding()) {
                            T1.d(i2 >> 2);
                            return T1.n();
                        }
                        decodeBase64Char4 = Q1(base64Variant, readUnsignedByte5, 3);
                    }
                    if (decodeBase64Char4 == -2) {
                        T1.d(i2 >> 2);
                    }
                }
                T1.c((i2 << 6) | decodeBase64Char4);
            }
        }
    }

    public final String r3(int i, int i2, int i3) throws IOException {
        return q3(this.Q0, 0, i, i2, i3);
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0038  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public int s2(int r7) throws java.io.IOException {
        /*
            r6 = this;
            r7 = r7 & 255(0xff, float:3.57E-43)
            r0 = 127(0x7f, float:1.78E-43)
            if (r7 <= r0) goto L6e
            r0 = r7 & 224(0xe0, float:3.14E-43)
            r1 = 2
            r2 = 1
            r3 = 192(0xc0, float:2.69E-43)
            if (r0 != r3) goto L12
            r7 = r7 & 31
        L10:
            r0 = r2
            goto L2c
        L12:
            r0 = r7 & 240(0xf0, float:3.36E-43)
            r3 = 224(0xe0, float:3.14E-43)
            if (r0 != r3) goto L1c
            r7 = r7 & 15
            r0 = r1
            goto L2c
        L1c:
            r0 = r7 & 248(0xf8, float:3.48E-43)
            r3 = 240(0xf0, float:3.36E-43)
            if (r0 != r3) goto L26
            r7 = r7 & 7
            r0 = 3
            goto L2c
        L26:
            r0 = r7 & 255(0xff, float:3.57E-43)
            r6.T2(r0)
            goto L10
        L2c:
            java.io.DataInput r3 = r6.T0
            int r3 = r3.readUnsignedByte()
            r4 = r3 & 192(0xc0, float:2.69E-43)
            r5 = 128(0x80, float:1.8E-43)
            if (r4 == r5) goto L3d
            r4 = r3 & 255(0xff, float:3.57E-43)
            r6.U2(r4)
        L3d:
            int r7 = r7 << 6
            r3 = r3 & 63
            r7 = r7 | r3
            if (r0 <= r2) goto L6e
            java.io.DataInput r2 = r6.T0
            int r2 = r2.readUnsignedByte()
            r3 = r2 & 192(0xc0, float:2.69E-43)
            if (r3 == r5) goto L53
            r3 = r2 & 255(0xff, float:3.57E-43)
            r6.U2(r3)
        L53:
            int r7 = r7 << 6
            r2 = r2 & 63
            r7 = r7 | r2
            if (r0 <= r1) goto L6e
            java.io.DataInput r0 = r6.T0
            int r0 = r0.readUnsignedByte()
            r1 = r0 & 192(0xc0, float:2.69E-43)
            if (r1 == r5) goto L69
            r1 = r0 & 255(0xff, float:3.57E-43)
            r6.U2(r1)
        L69:
            int r7 = r7 << 6
            r0 = r0 & 63
            r7 = r7 | r0
        L6e:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ie4.s2(int):int");
    }

    public final String s3(int i, int i2, int i3, int i4) throws IOException {
        int[] iArr = this.Q0;
        iArr[0] = i;
        return q3(iArr, 1, i2, i3, i4);
    }

    public final int t2(int i) throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if ((readUnsignedByte & 192) != 128) {
            U2(readUnsignedByte & 255);
        }
        return ((i & 31) << 6) | (readUnsignedByte & 63);
    }

    public final String t3(int i, int i2, int i3, int i4, int i5) throws IOException {
        int[] iArr = this.Q0;
        iArr[0] = i;
        iArr[1] = i2;
        return q3(iArr, 2, i3, i4, i5);
    }

    public final int u2(int i) throws IOException {
        int i2 = i & 15;
        int readUnsignedByte = this.T0.readUnsignedByte();
        if ((readUnsignedByte & 192) != 128) {
            U2(readUnsignedByte & 255);
        }
        int i3 = (i2 << 6) | (readUnsignedByte & 63);
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if ((readUnsignedByte2 & 192) != 128) {
            U2(readUnsignedByte2 & 255);
        }
        return (i3 << 6) | (readUnsignedByte2 & 63);
    }

    public final int v2(int i) throws IOException {
        int readUnsignedByte = this.T0.readUnsignedByte();
        if ((readUnsignedByte & 192) != 128) {
            U2(readUnsignedByte & 255);
        }
        int i2 = ((i & 7) << 6) | (readUnsignedByte & 63);
        int readUnsignedByte2 = this.T0.readUnsignedByte();
        if ((readUnsignedByte2 & 192) != 128) {
            U2(readUnsignedByte2 & 255);
        }
        int i3 = (i2 << 6) | (readUnsignedByte2 & 63);
        int readUnsignedByte3 = this.T0.readUnsignedByte();
        if ((readUnsignedByte3 & 192) != 128) {
            U2(readUnsignedByte3 & 255);
        }
        return ((i3 << 6) | (readUnsignedByte3 & 63)) - 65536;
    }

    public final String w2() throws IOException {
        char[] k = this.t0.k();
        int[] iArr = V0;
        int length = k.length;
        int i = 0;
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (iArr[readUnsignedByte] != 0) {
                if (readUnsignedByte == 34) {
                    return this.t0.y(i);
                }
                y2(k, i, readUnsignedByte);
                return this.t0.j();
            }
            int i2 = i + 1;
            k[i] = (char) readUnsignedByte;
            if (i2 >= length) {
                y2(k, i2, this.T0.readUnsignedByte());
                return this.t0.j();
            }
            i = i2;
        }
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public String x0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                return w2();
            }
            return this.t0.j();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        } else {
            return super.y0(null);
        }
    }

    public void x2() throws IOException {
        char[] k = this.t0.k();
        int[] iArr = V0;
        int length = k.length;
        int i = 0;
        while (true) {
            int readUnsignedByte = this.T0.readUnsignedByte();
            if (iArr[readUnsignedByte] != 0) {
                if (readUnsignedByte == 34) {
                    this.t0.z(i);
                    return;
                } else {
                    y2(k, i, readUnsignedByte);
                    return;
                }
            }
            int i2 = i + 1;
            k[i] = (char) readUnsignedByte;
            if (i2 >= length) {
                y2(k, i2, this.T0.readUnsignedByte());
                return;
            }
            i = i2;
        }
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public String y0(String str) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                return w2();
            }
            return this.t0.j();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        } else {
            return super.y0(str);
        }
    }

    public final void y2(char[] cArr, int i, int i2) throws IOException {
        int[] iArr = V0;
        int length = cArr.length;
        while (true) {
            int i3 = 0;
            if (iArr[i2] == 0) {
                if (i >= length) {
                    cArr = this.t0.n();
                    length = cArr.length;
                    i = 0;
                }
                cArr[i] = (char) i2;
                i2 = this.T0.readUnsignedByte();
                i++;
            } else if (i2 == 34) {
                this.t0.z(i);
                return;
            } else {
                int i4 = iArr[i2];
                if (i4 == 1) {
                    i2 = R1();
                } else if (i4 == 2) {
                    i2 = t2(i2);
                } else if (i4 == 3) {
                    i2 = u2(i2);
                } else if (i4 == 4) {
                    int v2 = v2(i2);
                    int i5 = i + 1;
                    cArr[i] = (char) (55296 | (v2 >> 10));
                    if (i5 >= cArr.length) {
                        cArr = this.t0.n();
                        length = cArr.length;
                        i = 0;
                    } else {
                        i = i5;
                    }
                    i2 = (v2 & 1023) | 56320;
                } else if (i2 < 32) {
                    L1(i2, "string value");
                } else {
                    S2(i2);
                }
                if (i >= cArr.length) {
                    cArr = this.t0.n();
                    length = cArr.length;
                } else {
                    i3 = i;
                }
                i = i3 + 1;
                cArr[i3] = (char) i2;
                i2 = this.T0.readUnsignedByte();
            }
        }
    }

    public final String z2(JsonToken jsonToken) {
        if (jsonToken == null) {
            return null;
        }
        int id = jsonToken.id();
        if (id != 5) {
            if (id != 6 && id != 7 && id != 8) {
                return jsonToken.asString();
            }
            return this.t0.j();
        }
        return this.r0.b();
    }
}
