package defpackage;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import androidx.core.graphics.drawable.a;
import com.zendesk.logger.Logger;
import java.util.Locale;

/* compiled from: UiUtils.java */
/* renamed from: ne4  reason: default package */
/* loaded from: classes3.dex */
public class ne4 {
    public static CharSequence a(String str) {
        if (Build.VERSION.SDK_INT >= 24) {
            return Html.fromHtml(str, 0);
        }
        return Html.fromHtml(str);
    }

    public static int b(int i, Context context) {
        return m70.d(context, i);
    }

    public static void c(int i, Drawable drawable, View view) {
        if (drawable == null) {
            Logger.e("UiUtils", "Drawable is null, cannot apply a tint", new Object[0]);
            return;
        }
        a.n(a.r(drawable).mutate(), i);
        if (view != null) {
            view.invalidate();
        }
    }

    public static void d(View view, int i) {
        if (view == null) {
            Logger.k("UiUtils", "View is null and can't change visibility", new Object[0]);
        } else {
            view.setVisibility(i);
        }
    }

    public static int e(int i, Context context, int i2) {
        if (i != 0 && context != null && i2 != 0) {
            TypedValue typedValue = new TypedValue();
            if (!context.getTheme().resolveAttribute(i, typedValue, true)) {
                Logger.e("UiUtils", String.format(Locale.US, "Resource %d not found. Resource is either missing or you are using a non-ui context.", Integer.valueOf(i)), new Object[0]);
                return b(i2, context);
            }
            int i3 = typedValue.resourceId;
            if (i3 == 0) {
                return typedValue.data;
            }
            return b(i3, context);
        }
        Logger.b("UiUtils", "themeAttributeId, context, and fallbackColorId are required.", new Object[0]);
        return -16777216;
    }
}
