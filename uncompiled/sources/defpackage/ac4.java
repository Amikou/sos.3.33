package defpackage;

import defpackage.f84;
import java.io.IOException;

/* compiled from: TrueHdSampleRechunker.java */
/* renamed from: ac4  reason: default package */
/* loaded from: classes.dex */
public final class ac4 {
    public final byte[] a = new byte[10];
    public boolean b;
    public int c;
    public long d;
    public int e;
    public int f;
    public int g;

    public void a(f84 f84Var, f84.a aVar) {
        if (this.c > 0) {
            f84Var.b(this.d, this.e, this.f, this.g, aVar);
            this.c = 0;
        }
    }

    public void b() {
        this.b = false;
        this.c = 0;
    }

    public void c(f84 f84Var, long j, int i, int i2, int i3, f84.a aVar) {
        ii.h(this.g <= i2 + i3, "TrueHD chunk samples must be contiguous in the sample queue.");
        if (this.b) {
            int i4 = this.c;
            int i5 = i4 + 1;
            this.c = i5;
            if (i4 == 0) {
                this.d = j;
                this.e = i;
                this.f = 0;
            }
            this.f += i2;
            this.g = i3;
            if (i5 >= 16) {
                a(f84Var, aVar);
            }
        }
    }

    public void d(q11 q11Var) throws IOException {
        if (this.b) {
            return;
        }
        q11Var.n(this.a, 0, 10);
        q11Var.j();
        if (t5.i(this.a) == 0) {
            return;
        }
        this.b = true;
    }
}
