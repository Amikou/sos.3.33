package defpackage;

import android.graphics.Bitmap;

/* compiled from: AttributeStrategy.java */
/* renamed from: ej  reason: default package */
/* loaded from: classes.dex */
public class ej implements v22 {
    public final b a = new b();
    public final ui1<a, Bitmap> b = new ui1<>();

    /* compiled from: AttributeStrategy.java */
    /* renamed from: ej$a */
    /* loaded from: classes.dex */
    public static class a implements at2 {
        public final b a;
        public int b;
        public int c;
        public Bitmap.Config d;

        public a(b bVar) {
            this.a = bVar;
        }

        @Override // defpackage.at2
        public void a() {
            this.a.c(this);
        }

        public void b(int i, int i2, Bitmap.Config config) {
            this.b = i;
            this.c = i2;
            this.d = config;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                return this.b == aVar.b && this.c == aVar.c && this.d == aVar.d;
            }
            return false;
        }

        public int hashCode() {
            int i = ((this.b * 31) + this.c) * 31;
            Bitmap.Config config = this.d;
            return i + (config != null ? config.hashCode() : 0);
        }

        public String toString() {
            return ej.f(this.b, this.c, this.d);
        }
    }

    /* compiled from: AttributeStrategy.java */
    /* renamed from: ej$b */
    /* loaded from: classes.dex */
    public static class b extends kn<a> {
        @Override // defpackage.kn
        /* renamed from: d */
        public a a() {
            return new a(this);
        }

        public a e(int i, int i2, Bitmap.Config config) {
            a b = b();
            b.b(i, i2, config);
            return b;
        }
    }

    public static String f(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    public static String g(Bitmap bitmap) {
        return f(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    @Override // defpackage.v22
    public String a(int i, int i2, Bitmap.Config config) {
        return f(i, i2, config);
    }

    @Override // defpackage.v22
    public int b(Bitmap bitmap) {
        return mg4.h(bitmap);
    }

    @Override // defpackage.v22
    public void c(Bitmap bitmap) {
        this.b.d(this.a.e(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    @Override // defpackage.v22
    public Bitmap d(int i, int i2, Bitmap.Config config) {
        return this.b.a(this.a.e(i, i2, config));
    }

    @Override // defpackage.v22
    public String e(Bitmap bitmap) {
        return g(bitmap);
    }

    @Override // defpackage.v22
    public Bitmap removeLast() {
        return this.b.f();
    }

    public String toString() {
        return "AttributeStrategy:\n  " + this.b;
    }
}
