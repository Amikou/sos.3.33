package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.rd.draw.data.Orientation;

/* compiled from: WormDrawer.java */
/* renamed from: fr4  reason: default package */
/* loaded from: classes2.dex */
public class fr4 extends hn {
    public RectF c;

    public fr4(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
        this.c = new RectF();
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2) {
        if (wg4Var instanceof er4) {
            er4 er4Var = (er4) wg4Var;
            int b = er4Var.b();
            int a = er4Var.a();
            int m = this.b.m();
            int t = this.b.t();
            int p = this.b.p();
            if (this.b.g() == Orientation.HORIZONTAL) {
                RectF rectF = this.c;
                rectF.left = b;
                rectF.right = a;
                rectF.top = i2 - m;
                rectF.bottom = i2 + m;
            } else {
                RectF rectF2 = this.c;
                rectF2.left = i - m;
                rectF2.right = i + m;
                rectF2.top = b;
                rectF2.bottom = a;
            }
            this.a.setColor(t);
            float f = i;
            float f2 = i2;
            float f3 = m;
            canvas.drawCircle(f, f2, f3, this.a);
            this.a.setColor(p);
            canvas.drawRoundRect(this.c, f3, f3, this.a);
        }
    }
}
