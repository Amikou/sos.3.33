package defpackage;

/* compiled from: BaseDataSubscriber.java */
/* renamed from: gn  reason: default package */
/* loaded from: classes.dex */
public abstract class gn<T> implements ke0<T> {
    @Override // defpackage.ke0
    public void b(ge0<T> ge0Var) {
        try {
            e(ge0Var);
        } finally {
            ge0Var.close();
        }
    }

    @Override // defpackage.ke0
    public void c(ge0<T> ge0Var) {
    }

    @Override // defpackage.ke0
    public void d(ge0<T> ge0Var) {
        boolean b = ge0Var.b();
        try {
            f(ge0Var);
        } finally {
            if (b) {
                ge0Var.close();
            }
        }
    }

    public abstract void e(ge0<T> ge0Var);

    public abstract void f(ge0<T> ge0Var);
}
