package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: v95  reason: default package */
/* loaded from: classes.dex */
public final class v95 {
    public static final p95 a;
    public static volatile p95 b;

    static {
        s95 s95Var = new s95(null);
        a = s95Var;
        b = s95Var;
    }

    public static p95 a() {
        return b;
    }
}
