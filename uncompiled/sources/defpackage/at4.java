package defpackage;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.android.play.core.internal.d;
import com.google.android.play.core.splitcompat.b;
import java.io.File;
import java.util.Set;

/* renamed from: at4  reason: default package */
/* loaded from: classes2.dex */
public final class at4 {
    public at4(b bVar) {
    }

    public static final int b(AssetManager assetManager, File file) {
        int intValue = ((Integer) d.a(assetManager, "addAssetPath", Integer.class, String.class, file.getPath())).intValue();
        StringBuilder sb = new StringBuilder(39);
        sb.append("addAssetPath completed with ");
        sb.append(intValue);
        return intValue;
    }

    public final synchronized void a(Context context, Set<File> set) {
        AssetManager assets = context.getAssets();
        for (File file : set) {
            b(assets, file);
        }
    }
}
