package defpackage;

import java.util.List;
import zendesk.belvedere.MediaIntent;
import zendesk.belvedere.MediaResult;

/* compiled from: ImageStreamMvp.java */
/* renamed from: xo1  reason: default package */
/* loaded from: classes3.dex */
public interface xo1 {
    MediaIntent a();

    MediaIntent b();

    long c();

    List<MediaResult> d(MediaResult mediaResult);

    boolean e();

    boolean f();

    List<MediaResult> g();

    boolean h();

    List<MediaResult> i(MediaResult mediaResult);

    List<MediaResult> j();

    MediaIntent k();

    boolean l();
}
