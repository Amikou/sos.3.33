package defpackage;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.zzcl;
import com.google.android.gms.measurement.internal.f;
import com.google.android.gms.measurement.internal.h;
import com.google.android.gms.measurement.internal.o;
import com.google.android.gms.measurement.internal.p;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ck5  reason: default package */
/* loaded from: classes.dex */
public final class ck5 implements tl5 {
    public static volatile ck5 I;
    public long A;
    public volatile Boolean B;
    public Boolean C;
    public Boolean D;
    public volatile boolean E;
    public int F;
    public final long H;
    public final Context a;
    public final String b;
    public final String c;
    public final String d;
    public final boolean e;
    public final c66 f;
    public final q45 g;
    public final mi5 h;
    public final og5 i;
    public final h j;
    public final qu5 k;
    public final sw5 l;
    public final cg5 m;
    public final rz n;
    public final qq5 o;
    public final dp5 p;
    public final pc5 q;
    public final o r;
    public final String s;
    public f t;
    public p u;
    public q55 v;
    public xf5 w;
    public ui5 x;
    public Boolean z;
    public boolean y = false;
    public final AtomicInteger G = new AtomicInteger(0);

    public ck5(gm5 gm5Var) {
        long a;
        Bundle bundle;
        boolean z = false;
        zt2.j(gm5Var);
        c66 c66Var = new c66(gm5Var.a);
        this.f = c66Var;
        ue5.a = c66Var;
        Context context = gm5Var.a;
        this.a = context;
        this.b = gm5Var.b;
        this.c = gm5Var.c;
        this.d = gm5Var.d;
        this.e = gm5Var.h;
        this.B = gm5Var.e;
        this.s = gm5Var.j;
        this.E = true;
        zzcl zzclVar = gm5Var.g;
        if (zzclVar != null && (bundle = zzclVar.k0) != null) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.C = (Boolean) obj;
            }
            Object obj2 = zzclVar.k0.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.D = (Boolean) obj2;
            }
        }
        wo5.b(context);
        rz c = mi0.c();
        this.n = c;
        Long l = gm5Var.i;
        if (l != null) {
            a = l.longValue();
        } else {
            a = c.a();
        }
        this.H = a;
        this.g = new q45(this);
        mi5 mi5Var = new mi5(this);
        mi5Var.j();
        this.h = mi5Var;
        og5 og5Var = new og5(this);
        og5Var.j();
        this.i = og5Var;
        sw5 sw5Var = new sw5(this);
        sw5Var.j();
        this.l = sw5Var;
        cg5 cg5Var = new cg5(this);
        cg5Var.j();
        this.m = cg5Var;
        this.q = new pc5(this);
        qq5 qq5Var = new qq5(this);
        qq5Var.h();
        this.o = qq5Var;
        dp5 dp5Var = new dp5(this);
        dp5Var.h();
        this.p = dp5Var;
        qu5 qu5Var = new qu5(this);
        qu5Var.h();
        this.k = qu5Var;
        o oVar = new o(this);
        oVar.j();
        this.r = oVar;
        h hVar = new h(this);
        hVar.j();
        this.j = hVar;
        zzcl zzclVar2 = gm5Var.g;
        z = (zzclVar2 == null || zzclVar2.f0 == 0) ? true : z;
        if (context.getApplicationContext() instanceof Application) {
            dp5 F = F();
            if (F.a.a.getApplicationContext() instanceof Application) {
                Application application = (Application) F.a.a.getApplicationContext();
                if (F.c == null) {
                    F.c = new bp5(F, null);
                }
                if (z) {
                    application.unregisterActivityLifecycleCallbacks(F.c);
                    application.registerActivityLifecycleCallbacks(F.c);
                    F.a.w().v().a("Registered activity lifecycle callback");
                }
            }
        } else {
            w().p().a("Application context is not an Application");
        }
        hVar.p(new ak5(this, gm5Var));
    }

    public static ck5 e(Context context, zzcl zzclVar, Long l) {
        Bundle bundle;
        if (zzclVar != null && (zzclVar.i0 == null || zzclVar.j0 == null)) {
            zzclVar = new zzcl(zzclVar.a, zzclVar.f0, zzclVar.g0, zzclVar.h0, null, null, zzclVar.k0, null);
        }
        zt2.j(context);
        zt2.j(context.getApplicationContext());
        if (I == null) {
            synchronized (ck5.class) {
                if (I == null) {
                    I = new ck5(new gm5(context, zzclVar, l));
                }
            }
        } else if (zzclVar != null && (bundle = zzclVar.k0) != null && bundle.containsKey("dataCollectionDefaultEnabled")) {
            zt2.j(I);
            I.B = Boolean.valueOf(zzclVar.k0.getBoolean("dataCollectionDefaultEnabled"));
        }
        zt2.j(I);
        return I;
    }

    public static /* synthetic */ void s(ck5 ck5Var, gm5 gm5Var) {
        ck5Var.q().e();
        ck5Var.g.i();
        q55 q55Var = new q55(ck5Var);
        q55Var.j();
        ck5Var.v = q55Var;
        xf5 xf5Var = new xf5(ck5Var, gm5Var.f);
        xf5Var.h();
        ck5Var.w = xf5Var;
        f fVar = new f(ck5Var);
        fVar.h();
        ck5Var.t = fVar;
        p pVar = new p(ck5Var);
        pVar.h();
        ck5Var.u = pVar;
        ck5Var.l.k();
        ck5Var.h.k();
        ck5Var.x = new ui5(ck5Var);
        ck5Var.w.i();
        jg5 t = ck5Var.w().t();
        ck5Var.g.n();
        t.b("App measurement initialized, version", 42004L);
        ck5Var.w().t().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String n = xf5Var.n();
        if (TextUtils.isEmpty(ck5Var.b)) {
            if (ck5Var.G().H(n)) {
                ck5Var.w().t().a("Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.");
            } else {
                jg5 t2 = ck5Var.w().t();
                String valueOf = String.valueOf(n);
                t2.a(valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app "));
            }
        }
        ck5Var.w().u().a("Debug-level message logging enabled");
        if (ck5Var.F != ck5Var.G.get()) {
            ck5Var.w().l().c("Not all components initialized", Integer.valueOf(ck5Var.F), Integer.valueOf(ck5Var.G.get()));
        }
        ck5Var.y = true;
    }

    public static final void t() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    public static final void u(ql5 ql5Var) {
        if (ql5Var == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    public static final void v(vh5 vh5Var) {
        if (vh5Var != null) {
            if (vh5Var.f()) {
                return;
            }
            String valueOf = String.valueOf(vh5Var.getClass());
            StringBuilder sb = new StringBuilder(valueOf.length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
        throw new IllegalStateException("Component not created");
    }

    public static final void x(sl5 sl5Var) {
        if (sl5Var != null) {
            if (sl5Var.h()) {
                return;
            }
            String valueOf = String.valueOf(sl5Var.getClass());
            StringBuilder sb = new StringBuilder(valueOf.length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
        throw new IllegalStateException("Component not created");
    }

    public final mi5 A() {
        u(this.h);
        return this.h;
    }

    public final og5 B() {
        og5 og5Var = this.i;
        if (og5Var == null || !og5Var.h()) {
            return null;
        }
        return this.i;
    }

    public final qu5 C() {
        v(this.k);
        return this.k;
    }

    public final ui5 D() {
        return this.x;
    }

    public final h E() {
        return this.j;
    }

    public final dp5 F() {
        v(this.p);
        return this.p;
    }

    public final sw5 G() {
        u(this.l);
        return this.l;
    }

    public final cg5 H() {
        u(this.m);
        return this.m;
    }

    public final f I() {
        v(this.t);
        return this.t;
    }

    public final o J() {
        x(this.r);
        return this.r;
    }

    public final boolean K() {
        return TextUtils.isEmpty(this.b);
    }

    public final String L() {
        return this.b;
    }

    public final String M() {
        return this.c;
    }

    public final String N() {
        return this.d;
    }

    public final boolean O() {
        return this.e;
    }

    public final String P() {
        return this.s;
    }

    public final qq5 Q() {
        v(this.o);
        return this.o;
    }

    public final p R() {
        v(this.u);
        return this.u;
    }

    public final q55 S() {
        x(this.v);
        return this.v;
    }

    @Override // defpackage.tl5
    public final rz a() {
        return this.n;
    }

    @Override // defpackage.tl5
    public final c66 b() {
        return this.f;
    }

    public final xf5 c() {
        v(this.w);
        return this.w;
    }

    public final pc5 d() {
        pc5 pc5Var = this.q;
        if (pc5Var != null) {
            return pc5Var;
        }
        throw new IllegalStateException("Component not created");
    }

    public final void f(boolean z) {
        this.B = Boolean.valueOf(z);
    }

    public final boolean g() {
        return this.B != null && this.B.booleanValue();
    }

    public final boolean h() {
        return i() == 0;
    }

    public final int i() {
        q().e();
        if (this.g.A()) {
            return 1;
        }
        Boolean bool = this.D;
        if (bool == null || !bool.booleanValue()) {
            q().e();
            if (this.E) {
                Boolean p = A().p();
                if (p != null) {
                    return p.booleanValue() ? 0 : 3;
                }
                q45 q45Var = this.g;
                c66 c66Var = q45Var.a.f;
                Boolean y = q45Var.y("firebase_analytics_collection_enabled");
                if (y != null) {
                    return y.booleanValue() ? 0 : 4;
                }
                Boolean bool2 = this.C;
                return bool2 != null ? bool2.booleanValue() ? 0 : 5 : (!this.g.v(null, qf5.T) || this.B == null || this.B.booleanValue()) ? 0 : 7;
            }
            return 8;
        }
        return 2;
    }

    public final void j(boolean z) {
        q().e();
        this.E = z;
    }

    public final boolean k() {
        q().e();
        return this.E;
    }

    public final void l() {
        this.F++;
    }

    @Override // defpackage.tl5
    public final Context m() {
        return this.a;
    }

    public final void n() {
        this.G.incrementAndGet();
    }

    public final boolean o() {
        if (this.y) {
            q().e();
            Boolean bool = this.z;
            if (bool == null || this.A == 0 || (!bool.booleanValue() && Math.abs(this.n.b() - this.A) > 1000)) {
                this.A = this.n.b();
                boolean z = true;
                Boolean valueOf = Boolean.valueOf(G().E("android.permission.INTERNET") && G().E("android.permission.ACCESS_NETWORK_STATE") && (kr4.a(this.a).f() || this.g.H() || (sw5.a0(this.a) && sw5.D(this.a, false))));
                this.z = valueOf;
                if (valueOf.booleanValue()) {
                    if (!G().l(c().o(), c().p(), c().r()) && TextUtils.isEmpty(c().p())) {
                        z = false;
                    }
                    this.z = Boolean.valueOf(z);
                }
            }
            return this.z.booleanValue();
        }
        throw new IllegalStateException("AppMeasurement is not initialized");
    }

    public final void p() {
        q().e();
        x(J());
        String n = c().n();
        Pair<String, Boolean> l = A().l(n);
        if (this.g.B() && !((Boolean) l.second).booleanValue() && !TextUtils.isEmpty((CharSequence) l.first)) {
            o J = J();
            J.i();
            ConnectivityManager connectivityManager = (ConnectivityManager) J.a.a.getSystemService("connectivity");
            NetworkInfo networkInfo = null;
            if (connectivityManager != null) {
                try {
                    networkInfo = connectivityManager.getActiveNetworkInfo();
                } catch (SecurityException unused) {
                }
            }
            if (networkInfo != null && networkInfo.isConnected()) {
                sw5 G = G();
                c().a.g.n();
                URL Z = G.Z(42004L, n, (String) l.first, A().s.a() - 1);
                if (Z != null) {
                    o J2 = J();
                    zj5 zj5Var = new zj5(this);
                    J2.e();
                    J2.i();
                    zt2.j(Z);
                    zt2.j(zj5Var);
                    J2.a.q().t(new kp5(J2, n, Z, null, null, zj5Var, null));
                    return;
                }
                return;
            }
            w().p().a("Network is not available for Deferred Deep Link request. Skipping");
            return;
        }
        w().u().a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
    }

    @Override // defpackage.tl5
    public final h q() {
        x(this.j);
        return this.j;
    }

    public final /* synthetic */ void r(String str, int i, Throwable th, byte[] bArr, Map map) {
        List<ResolveInfo> queryIntentActivities;
        if (i != 200 && i != 204) {
            if (i == 304) {
                i = 304;
            }
            w().p().c("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i), th);
        }
        if (th == null) {
            A().r.b(true);
            if (bArr != null && bArr.length != 0) {
                try {
                    JSONObject jSONObject = new JSONObject(new String(bArr));
                    String optString = jSONObject.optString("deeplink", "");
                    String optString2 = jSONObject.optString("gclid", "");
                    double optDouble = jSONObject.optDouble("timestamp", Utils.DOUBLE_EPSILON);
                    if (TextUtils.isEmpty(optString)) {
                        w().u().a("Deferred Deep Link is empty.");
                        return;
                    }
                    sw5 G = G();
                    ck5 ck5Var = G.a;
                    if (!TextUtils.isEmpty(optString) && (queryIntentActivities = G.a.a.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) != null && !queryIntentActivities.isEmpty()) {
                        Bundle bundle = new Bundle();
                        bundle.putString("gclid", optString2);
                        bundle.putString("_cis", "ddp");
                        this.p.X("auto", "_cmp", bundle);
                        sw5 G2 = G();
                        if (TextUtils.isEmpty(optString)) {
                            return;
                        }
                        try {
                            SharedPreferences.Editor edit = G2.a.a.getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
                            edit.putString("deeplink", optString);
                            edit.putLong("timestamp", Double.doubleToRawLongBits(optDouble));
                            if (edit.commit()) {
                                G2.a.a.sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
                                return;
                            }
                            return;
                        } catch (RuntimeException e) {
                            G2.a.w().l().b("Failed to persist Deferred Deep Link. exception", e);
                            return;
                        }
                    }
                    w().p().c("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                    return;
                } catch (JSONException e2) {
                    w().l().b("Failed to parse the Deferred Deep Link response. exception", e2);
                    return;
                }
            }
            w().u().a("Deferred Deep Link response empty.");
            return;
        }
        w().p().c("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i), th);
    }

    @Override // defpackage.tl5
    public final og5 w() {
        x(this.i);
        return this.i;
    }

    public final void y(zzcl zzclVar) {
        t45 t45Var;
        q().e();
        t45 s = A().s();
        mi5 A = A();
        ck5 ck5Var = A.a;
        A.e();
        int i = 100;
        int i2 = A.n().getInt("consent_source", 100);
        q45 q45Var = this.g;
        ck5 ck5Var2 = q45Var.a;
        Boolean y = q45Var.y("google_analytics_default_allow_ad_storage");
        q45 q45Var2 = this.g;
        ck5 ck5Var3 = q45Var2.a;
        Boolean y2 = q45Var2.y("google_analytics_default_allow_analytics_storage");
        if ((y != null || y2 != null) && A().r(-10)) {
            t45Var = new t45(y, y2);
            i = -10;
        } else {
            if (!TextUtils.isEmpty(c().o()) && (i2 == 0 || i2 == 30 || i2 == 10 || i2 == 30 || i2 == 30 || i2 == 40)) {
                F().V(t45.c, -10, this.H);
            } else {
                h16.a();
                if ((!this.g.v(null, qf5.A0) || TextUtils.isEmpty(c().o())) && zzclVar != null && zzclVar.k0 != null && A().r(30)) {
                    t45Var = t45.b(zzclVar.k0);
                    if (!t45Var.equals(t45.c)) {
                        i = 30;
                    }
                }
            }
            t45Var = null;
        }
        if (t45Var != null) {
            F().V(t45Var, i, this.H);
            s = t45Var;
        }
        F().W(s);
        if (A().e.a() == 0) {
            w().v().b("Persisting first open", Long.valueOf(this.H));
            A().e.b(this.H);
        }
        F().n.c();
        if (!o()) {
            if (h()) {
                if (!G().E("android.permission.INTERNET")) {
                    w().l().a("App is missing INTERNET permission");
                }
                if (!G().E("android.permission.ACCESS_NETWORK_STATE")) {
                    w().l().a("App is missing ACCESS_NETWORK_STATE permission");
                }
                if (!kr4.a(this.a).f() && !this.g.H()) {
                    if (!sw5.a0(this.a)) {
                        w().l().a("AppMeasurementReceiver not registered/enabled");
                    }
                    if (!sw5.D(this.a, false)) {
                        w().l().a("AppMeasurementService not registered/enabled");
                    }
                }
                w().l().a("Uploading is not possible. App measurement disabled");
            }
        } else {
            if (!TextUtils.isEmpty(c().o()) || !TextUtils.isEmpty(c().p())) {
                sw5 G = G();
                String o = c().o();
                mi5 A2 = A();
                A2.e();
                String string = A2.n().getString("gmp_app_id", null);
                String p = c().p();
                mi5 A3 = A();
                A3.e();
                if (G.n(o, string, p, A3.n().getString("admob_app_id", null))) {
                    w().t().a("Rechecking which service to use due to a GMP App Id change");
                    mi5 A4 = A();
                    A4.e();
                    Boolean p2 = A4.p();
                    SharedPreferences.Editor edit = A4.n().edit();
                    edit.clear();
                    edit.apply();
                    if (p2 != null) {
                        A4.o(p2);
                    }
                    I().l();
                    this.u.s();
                    this.u.n();
                    A().e.b(this.H);
                    A().g.b(null);
                }
                mi5 A5 = A();
                String o2 = c().o();
                A5.e();
                SharedPreferences.Editor edit2 = A5.n().edit();
                edit2.putString("gmp_app_id", o2);
                edit2.apply();
                mi5 A6 = A();
                String p3 = c().p();
                A6.e();
                SharedPreferences.Editor edit3 = A6.n().edit();
                edit3.putString("admob_app_id", p3);
                edit3.apply();
            }
            if (!A().s().h()) {
                A().g.b(null);
            }
            F().p(A().g.a());
            e16.a();
            if (this.g.v(null, qf5.n0)) {
                try {
                    G().a.a.getClassLoader().loadClass("com.google.firebase.remoteconfig.FirebaseRemoteConfig");
                } catch (ClassNotFoundException unused) {
                    if (!TextUtils.isEmpty(A().t.a())) {
                        w().p().a("Remote config removed with active feature rollouts");
                        A().t.b(null);
                    }
                }
            }
            if (!TextUtils.isEmpty(c().o()) || !TextUtils.isEmpty(c().p())) {
                boolean h = h();
                if (!A().u() && !this.g.A()) {
                    A().t(!h);
                }
                if (h) {
                    F().t();
                }
                C().d.a();
                R().T(new AtomicReference<>());
                R().l(A().w.a());
            }
        }
        A().n.b(true);
    }

    public final q45 z() {
        return this.g;
    }
}
