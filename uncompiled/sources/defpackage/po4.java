package defpackage;

import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import com.facebook.webpsupport.WebpBitmapFactoryImpl;
import java.io.UnsupportedEncodingException;

/* compiled from: WebpSupportStatus.java */
/* renamed from: po4  reason: default package */
/* loaded from: classes.dex */
public class po4 {
    public static final boolean a;
    public static final boolean b;
    public static oo4 c;
    public static boolean d;
    public static final byte[] e;
    public static final byte[] f;
    public static final byte[] g;
    public static final byte[] h;
    public static final byte[] i;

    static {
        a = Build.VERSION.SDK_INT <= 17;
        b = e();
        c = null;
        d = false;
        e = a("RIFF");
        f = a("WEBP");
        g = a("VP8 ");
        h = a("VP8L");
        i = a("VP8X");
    }

    public static byte[] a(String str) {
        try {
            return str.getBytes("ASCII");
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("ASCII not found!", e2);
        }
    }

    public static boolean b(byte[] bArr, int i2) {
        return j(bArr, i2 + 12, i) && ((bArr[i2 + 20] & 2) == 2);
    }

    public static boolean c(byte[] bArr, int i2, int i3) {
        return i3 >= 21 && j(bArr, i2 + 12, i);
    }

    public static boolean d(byte[] bArr, int i2) {
        return j(bArr, i2 + 12, i) && ((bArr[i2 + 20] & 16) == 16);
    }

    public static boolean e() {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 17) {
            return false;
        }
        if (i2 == 17) {
            byte[] decode = Base64.decode("UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA==", 0);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(decode, 0, decode.length, options);
            if (options.outHeight != 1 || options.outWidth != 1) {
                return false;
            }
        }
        return true;
    }

    public static boolean f(byte[] bArr, int i2) {
        return j(bArr, i2 + 12, h);
    }

    public static boolean g(byte[] bArr, int i2) {
        return j(bArr, i2 + 12, g);
    }

    public static boolean h(byte[] bArr, int i2, int i3) {
        return i3 >= 20 && j(bArr, i2, e) && j(bArr, i2 + 8, f);
    }

    public static oo4 i() {
        if (d) {
            return c;
        }
        oo4 oo4Var = null;
        try {
            boolean z = WebpBitmapFactoryImpl.a;
            oo4Var = (oo4) WebpBitmapFactoryImpl.class.newInstance();
        } catch (Throwable unused) {
        }
        d = true;
        return oo4Var;
    }

    public static boolean j(byte[] bArr, int i2, byte[] bArr2) {
        if (bArr2 == null || bArr == null || bArr2.length + i2 > bArr.length) {
            return false;
        }
        for (int i3 = 0; i3 < bArr2.length; i3++) {
            if (bArr[i3 + i2] != bArr2[i3]) {
                return false;
            }
        }
        return true;
    }
}
