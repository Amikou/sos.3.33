package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jq5  reason: default package */
/* loaded from: classes.dex */
public final class jq5 implements Runnable {
    public final /* synthetic */ long a;
    public final /* synthetic */ qq5 f0;

    public jq5(qq5 qq5Var, long j) {
        this.f0 = qq5Var;
        this.a = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.a.d().h(this.a);
        this.f0.e = null;
    }
}
