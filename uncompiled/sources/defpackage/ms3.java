package defpackage;

import androidx.media3.common.p;
import androidx.media3.common.util.b;

/* compiled from: StandaloneMediaClock.java */
/* renamed from: ms3  reason: default package */
/* loaded from: classes.dex */
public final class ms3 implements u52 {
    public final tz a;
    public boolean f0;
    public long g0;
    public long h0;
    public p i0 = p.h0;

    public ms3(tz tzVar) {
        this.a = tzVar;
    }

    public void a(long j) {
        this.g0 = j;
        if (this.f0) {
            this.h0 = this.a.b();
        }
    }

    @Override // defpackage.u52
    public void b(p pVar) {
        if (this.f0) {
            a(n());
        }
        this.i0 = pVar;
    }

    public void c() {
        if (this.f0) {
            return;
        }
        this.h0 = this.a.b();
        this.f0 = true;
    }

    public void d() {
        if (this.f0) {
            a(n());
            this.f0 = false;
        }
    }

    @Override // defpackage.u52
    public p e() {
        return this.i0;
    }

    @Override // defpackage.u52
    public long n() {
        long a;
        long j = this.g0;
        if (this.f0) {
            long b = this.a.b() - this.h0;
            p pVar = this.i0;
            if (pVar.a == 1.0f) {
                a = b.y0(b);
            } else {
                a = pVar.a(b);
            }
            return j + a;
        }
        return j;
    }
}
