package defpackage;

/* compiled from: IntegerArrayAdapter.java */
/* renamed from: vr1  reason: default package */
/* loaded from: classes.dex */
public final class vr1 implements jh<int[]> {
    @Override // defpackage.jh
    public String a() {
        return "IntegerArrayPool";
    }

    @Override // defpackage.jh
    public int b() {
        return 4;
    }

    @Override // defpackage.jh
    /* renamed from: d */
    public int c(int[] iArr) {
        return iArr.length;
    }

    @Override // defpackage.jh
    /* renamed from: e */
    public int[] newArray(int i) {
        return new int[i];
    }
}
