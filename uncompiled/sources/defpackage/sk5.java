package defpackage;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* renamed from: sk5  reason: default package */
/* loaded from: classes.dex */
public final class sk5 extends h35 implements jf5 {
    public sk5(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    @Override // defpackage.jf5
    public final boolean O(boolean z) throws RemoteException {
        Parcel b = b();
        ka5.a(b, true);
        Parcel F1 = F1(2, b);
        boolean b2 = ka5.b(F1);
        F1.recycle();
        return b2;
    }

    @Override // defpackage.jf5
    public final String getId() throws RemoteException {
        Parcel F1 = F1(1, b());
        String readString = F1.readString();
        F1.recycle();
        return readString;
    }
}
