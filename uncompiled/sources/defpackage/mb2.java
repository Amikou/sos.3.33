package defpackage;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Build;
import java.util.List;
import java.util.Locale;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: MyContextWrapper.kt */
/* renamed from: mb2  reason: default package */
/* loaded from: classes2.dex */
public final class mb2 extends ContextWrapper {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public mb2(Context context) {
        super(context);
        fs1.f(context, "context");
    }

    @TargetApi(24)
    public final Locale a(Configuration configuration) {
        return configuration.getLocales().get(0);
    }

    public final Locale b(Configuration configuration) {
        return configuration.locale;
    }

    @TargetApi(24)
    public final void c(Configuration configuration, Locale locale) {
        configuration.setLocale(locale);
    }

    public final void d(Configuration configuration, Locale locale) {
        configuration.locale = locale;
    }

    public final ContextWrapper e(Context context, String str) {
        Locale b;
        Locale locale;
        fs1.f(context, "context");
        fs1.f(str, "language");
        Configuration configuration = context.getResources().getConfiguration();
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            fs1.e(configuration, "config");
            b = a(configuration);
        } else {
            fs1.e(configuration, "config");
            b = b(configuration);
        }
        if (!fs1.b(str, "")) {
            fs1.d(b);
            if (!fs1.b(b.getLanguage(), str)) {
                if (StringsKt__StringsKt.M(str, "-r", false, 2, null)) {
                    List w0 = StringsKt__StringsKt.w0(str, new String[]{"-r"}, false, 0, 6, null);
                    locale = new Locale((String) w0.get(0), (String) w0.get(1));
                } else {
                    locale = new Locale(str);
                }
                Locale.setDefault(locale);
                if (i >= 24) {
                    c(configuration, locale);
                } else {
                    d(configuration, locale);
                }
            }
        }
        Context createConfigurationContext = context.createConfigurationContext(configuration);
        fs1.e(createConfigurationContext, "newContext.createConfigurationContext(config)");
        return new mb2(createConfigurationContext);
    }
}
