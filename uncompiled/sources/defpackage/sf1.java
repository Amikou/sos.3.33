package defpackage;

import android.graphics.Bitmap;
import defpackage.tf1;

/* compiled from: GifBitmapProvider.java */
/* renamed from: sf1  reason: default package */
/* loaded from: classes.dex */
public final class sf1 implements tf1.a {
    public final jq a;
    public final sh b;

    public sf1(jq jqVar, sh shVar) {
        this.a = jqVar;
        this.b = shVar;
    }

    @Override // defpackage.tf1.a
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.a.e(i, i2, config);
    }

    @Override // defpackage.tf1.a
    public int[] b(int i) {
        sh shVar = this.b;
        if (shVar == null) {
            return new int[i];
        }
        return (int[]) shVar.e(i, int[].class);
    }

    @Override // defpackage.tf1.a
    public void c(Bitmap bitmap) {
        this.a.c(bitmap);
    }

    @Override // defpackage.tf1.a
    public void d(byte[] bArr) {
        sh shVar = this.b;
        if (shVar == null) {
            return;
        }
        shVar.c(bArr);
    }

    @Override // defpackage.tf1.a
    public byte[] e(int i) {
        sh shVar = this.b;
        if (shVar == null) {
            return new byte[i];
        }
        return (byte[]) shVar.e(i, byte[].class);
    }

    @Override // defpackage.tf1.a
    public void f(int[] iArr) {
        sh shVar = this.b;
        if (shVar == null) {
            return;
        }
        shVar.c(iArr);
    }
}
