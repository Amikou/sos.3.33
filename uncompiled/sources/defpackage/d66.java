package defpackage;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: d66  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class d66 implements Executor {
    public static final Executor a = new d66();

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
