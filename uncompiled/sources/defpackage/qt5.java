package defpackage;

import com.google.android.gms.internal.measurement.z1;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: qt5  reason: default package */
/* loaded from: classes.dex */
public final class qt5 {
    public static volatile qt5 b;
    public static volatile qt5 c;
    public static final qt5 d = new qt5(true);
    public final Map<nt5, fv5<?, ?>> a;

    public qt5() {
        this.a = new HashMap();
    }

    public static qt5 a() {
        qt5 qt5Var = b;
        if (qt5Var == null) {
            synchronized (qt5.class) {
                qt5Var = b;
                if (qt5Var == null) {
                    qt5Var = d;
                    b = qt5Var;
                }
            }
        }
        return qt5Var;
    }

    public static qt5 b() {
        qt5 qt5Var = c;
        if (qt5Var != null) {
            return qt5Var;
        }
        synchronized (qt5.class) {
            qt5 qt5Var2 = c;
            if (qt5Var2 != null) {
                return qt5Var2;
            }
            qt5 b2 = mu5.b(qt5.class);
            c = b2;
            return b2;
        }
    }

    public final <ContainingType extends z1> fv5<ContainingType, ?> c(ContainingType containingtype, int i) {
        return (fv5<ContainingType, ?>) this.a.get(new nt5(containingtype, i));
    }

    public qt5(boolean z) {
        this.a = Collections.emptyMap();
    }
}
