package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: CustomTokenProvider.kt */
/* renamed from: zc0  reason: default package */
/* loaded from: classes2.dex */
public final class zc0 {
    public static final zc0 a = new zc0();
    public static yc0 b;

    public final yc0 a(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new yc0(ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, context, null, 2, null).U());
                }
                te4 te4Var = te4.a;
            }
        }
        yc0 yc0Var = b;
        fs1.d(yc0Var);
        return yc0Var;
    }

    public final void b() {
        b = null;
    }
}
