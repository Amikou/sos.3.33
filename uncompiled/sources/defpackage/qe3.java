package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: qe3  reason: default package */
/* loaded from: classes2.dex */
public class qe3 extends ct0.b {
    public static final BigInteger g = oe3.j;
    public int[] f;

    public qe3() {
        this.f = cd2.g();
    }

    public qe3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP192K1FieldElement");
        }
        this.f = pe3.c(bigInteger);
    }

    public qe3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] g2 = cd2.g();
        pe3.a(this.f, ((qe3) ct0Var).f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] g2 = cd2.g();
        pe3.b(this.f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] g2 = cd2.g();
        g92.d(pe3.a, ((qe3) ct0Var).f, g2);
        pe3.d(g2, this.f, g2);
        return new qe3(g2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof qe3) {
            return cd2.l(this.f, ((qe3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] g2 = cd2.g();
        g92.d(pe3.a, this.f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return cd2.s(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 6);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return cd2.u(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] g2 = cd2.g();
        pe3.d(this.f, ((qe3) ct0Var).f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] g2 = cd2.g();
        pe3.f(this.f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (cd2.u(iArr) || cd2.s(iArr)) {
            return this;
        }
        int[] g2 = cd2.g();
        pe3.i(iArr, g2);
        pe3.d(g2, iArr, g2);
        int[] g3 = cd2.g();
        pe3.i(g2, g3);
        pe3.d(g3, iArr, g3);
        int[] g4 = cd2.g();
        pe3.j(g3, 3, g4);
        pe3.d(g4, g3, g4);
        pe3.j(g4, 2, g4);
        pe3.d(g4, g2, g4);
        pe3.j(g4, 8, g2);
        pe3.d(g2, g4, g2);
        pe3.j(g2, 3, g4);
        pe3.d(g4, g3, g4);
        int[] g5 = cd2.g();
        pe3.j(g4, 16, g5);
        pe3.d(g5, g2, g5);
        pe3.j(g5, 35, g2);
        pe3.d(g2, g5, g2);
        pe3.j(g2, 70, g5);
        pe3.d(g5, g2, g5);
        pe3.j(g5, 19, g2);
        pe3.d(g2, g4, g2);
        pe3.j(g2, 20, g2);
        pe3.d(g2, g4, g2);
        pe3.j(g2, 4, g2);
        pe3.d(g2, g3, g2);
        pe3.j(g2, 6, g2);
        pe3.d(g2, g3, g2);
        pe3.i(g2, g2);
        pe3.i(g2, g3);
        if (cd2.l(iArr, g3)) {
            return new qe3(g2);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] g2 = cd2.g();
        pe3.i(this.f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] g2 = cd2.g();
        pe3.k(this.f, ((qe3) ct0Var).f, g2);
        return new qe3(g2);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return cd2.p(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return cd2.H(this.f);
    }
}
