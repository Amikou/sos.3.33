package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: v16  reason: default package */
/* loaded from: classes.dex */
public final class v16 implements u16 {
    public static final wo5<Boolean> a;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        ro5Var.b("measurement.collection.efficient_engagement_reporting_enabled_2", true);
        a = ro5Var.b("measurement.collection.redundant_engagement_removal_enabled", false);
        ro5Var.a("measurement.id.collection.redundant_engagement_removal_enabled", 0L);
    }

    @Override // defpackage.u16
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
