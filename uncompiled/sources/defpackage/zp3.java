package defpackage;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import defpackage.lg1;
import defpackage.x50;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/* compiled from: SingletonConnectivityReceiver.java */
/* renamed from: zp3  reason: default package */
/* loaded from: classes.dex */
public final class zp3 {
    public static volatile zp3 d;
    public final c a;
    public final Set<x50.a> b = new HashSet();
    public boolean c;

    /* compiled from: SingletonConnectivityReceiver.java */
    /* renamed from: zp3$a */
    /* loaded from: classes.dex */
    public class a implements lg1.b<ConnectivityManager> {
        public final /* synthetic */ Context a;

        public a(zp3 zp3Var, Context context) {
            this.a = context;
        }

        @Override // defpackage.lg1.b
        /* renamed from: a */
        public ConnectivityManager get() {
            return (ConnectivityManager) this.a.getSystemService("connectivity");
        }
    }

    /* compiled from: SingletonConnectivityReceiver.java */
    /* renamed from: zp3$b */
    /* loaded from: classes.dex */
    public class b implements x50.a {
        public b() {
        }

        @Override // defpackage.x50.a
        public void a(boolean z) {
            ArrayList<x50.a> arrayList;
            synchronized (zp3.this) {
                arrayList = new ArrayList(zp3.this.b);
            }
            for (x50.a aVar : arrayList) {
                aVar.a(z);
            }
        }
    }

    /* compiled from: SingletonConnectivityReceiver.java */
    /* renamed from: zp3$c */
    /* loaded from: classes.dex */
    public interface c {
        boolean register();

        void unregister();
    }

    /* compiled from: SingletonConnectivityReceiver.java */
    /* renamed from: zp3$d */
    /* loaded from: classes.dex */
    public static final class d implements c {
        public boolean a;
        public final x50.a b;
        public final lg1.b<ConnectivityManager> c;
        public final ConnectivityManager.NetworkCallback d = new a();

        /* compiled from: SingletonConnectivityReceiver.java */
        /* renamed from: zp3$d$a */
        /* loaded from: classes.dex */
        public class a extends ConnectivityManager.NetworkCallback {

            /* compiled from: SingletonConnectivityReceiver.java */
            /* renamed from: zp3$d$a$a  reason: collision with other inner class name */
            /* loaded from: classes.dex */
            public class RunnableC0313a implements Runnable {
                public final /* synthetic */ boolean a;

                public RunnableC0313a(boolean z) {
                    this.a = z;
                }

                @Override // java.lang.Runnable
                public void run() {
                    a.this.a(this.a);
                }
            }

            public a() {
            }

            public void a(boolean z) {
                mg4.b();
                d dVar = d.this;
                boolean z2 = dVar.a;
                dVar.a = z;
                if (z2 != z) {
                    dVar.b.a(z);
                }
            }

            public final void b(boolean z) {
                mg4.u(new RunnableC0313a(z));
            }

            @Override // android.net.ConnectivityManager.NetworkCallback
            public void onAvailable(Network network) {
                b(true);
            }

            @Override // android.net.ConnectivityManager.NetworkCallback
            public void onLost(Network network) {
                b(false);
            }
        }

        public d(lg1.b<ConnectivityManager> bVar, x50.a aVar) {
            this.c = bVar;
            this.b = aVar;
        }

        @Override // defpackage.zp3.c
        @SuppressLint({"MissingPermission"})
        public boolean register() {
            this.a = this.c.get().getActiveNetwork() != null;
            try {
                this.c.get().registerDefaultNetworkCallback(this.d);
                return true;
            } catch (RuntimeException unused) {
                return false;
            }
        }

        @Override // defpackage.zp3.c
        public void unregister() {
            this.c.get().unregisterNetworkCallback(this.d);
        }
    }

    /* compiled from: SingletonConnectivityReceiver.java */
    /* renamed from: zp3$e */
    /* loaded from: classes.dex */
    public static final class e implements c {
        public final Context a;
        public final x50.a b;
        public final lg1.b<ConnectivityManager> c;
        public boolean d;
        public final BroadcastReceiver e = new a();

        /* compiled from: SingletonConnectivityReceiver.java */
        /* renamed from: zp3$e$a */
        /* loaded from: classes.dex */
        public class a extends BroadcastReceiver {
            public a() {
            }

            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context, Intent intent) {
                e eVar = e.this;
                boolean z = eVar.d;
                eVar.d = eVar.a();
                if (z != e.this.d) {
                    if (Log.isLoggable("ConnectivityMonitor", 3)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("connectivity changed, isConnected: ");
                        sb.append(e.this.d);
                    }
                    e eVar2 = e.this;
                    eVar2.b.a(eVar2.d);
                }
            }
        }

        public e(Context context, lg1.b<ConnectivityManager> bVar, x50.a aVar) {
            this.a = context.getApplicationContext();
            this.c = bVar;
            this.b = aVar;
        }

        @SuppressLint({"MissingPermission"})
        public boolean a() {
            try {
                NetworkInfo activeNetworkInfo = this.c.get().getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            } catch (RuntimeException unused) {
                return true;
            }
        }

        @Override // defpackage.zp3.c
        public boolean register() {
            this.d = a();
            try {
                this.a.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                return true;
            } catch (SecurityException unused) {
                return false;
            }
        }

        @Override // defpackage.zp3.c
        public void unregister() {
            this.a.unregisterReceiver(this.e);
        }
    }

    public zp3(Context context) {
        c eVar;
        lg1.b a2 = lg1.a(new a(this, context));
        b bVar = new b();
        if (Build.VERSION.SDK_INT >= 24) {
            eVar = new d(a2, bVar);
        } else {
            eVar = new e(context, a2, bVar);
        }
        this.a = eVar;
    }

    public static zp3 a(Context context) {
        if (d == null) {
            synchronized (zp3.class) {
                if (d == null) {
                    d = new zp3(context.getApplicationContext());
                }
            }
        }
        return d;
    }

    public final void b() {
        if (this.c || this.b.isEmpty()) {
            return;
        }
        this.c = this.a.register();
    }

    public final void c() {
        if (this.c && this.b.isEmpty()) {
            this.a.unregister();
            this.c = false;
        }
    }

    public synchronized void d(x50.a aVar) {
        this.b.add(aVar);
        b();
    }

    public synchronized void e(x50.a aVar) {
        this.b.remove(aVar);
        c();
    }
}
