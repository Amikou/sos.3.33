package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogResetPasswordConfirmationBinding.java */
/* renamed from: yn0  reason: default package */
/* loaded from: classes2.dex */
public final class yn0 {
    public final ConstraintLayout a;
    public final AppCompatTextView b;
    public final AppCompatTextView c;
    public final MaterialTextView d;
    public final MaterialTextView e;

    public yn0(ConstraintLayout constraintLayout, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = constraintLayout;
        this.b = appCompatTextView;
        this.c = appCompatTextView2;
        this.d = materialTextView;
        this.e = materialTextView2;
    }

    public static yn0 a(View view) {
        int i = R.id.btnNegative;
        AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.btnNegative);
        if (appCompatTextView != null) {
            i = R.id.btnPositive;
            AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.btnPositive);
            if (appCompatTextView2 != null) {
                i = R.id.tvDialogContent;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tvDialogContent);
                if (materialTextView != null) {
                    i = R.id.tvDialogTitle;
                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.tvDialogTitle);
                    if (materialTextView2 != null) {
                        return new yn0((ConstraintLayout) view, appCompatTextView, appCompatTextView2, materialTextView, materialTextView2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
