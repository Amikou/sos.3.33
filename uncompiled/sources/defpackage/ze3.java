package defpackage;

import defpackage.pt0;

/* renamed from: ze3  reason: default package */
/* loaded from: classes2.dex */
public class ze3 extends pt0.c {
    public ze3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public ze3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public ze3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        ye3 ye3Var = (ye3) this.c;
        if (ye3Var.i()) {
            return i.v();
        }
        ye3 ye3Var2 = (ye3) this.b;
        ye3 ye3Var3 = (ye3) this.d[0];
        int[] e = dd2.e();
        xe3.i(ye3Var.f, e);
        int[] e2 = dd2.e();
        xe3.i(e, e2);
        int[] e3 = dd2.e();
        xe3.i(ye3Var2.f, e3);
        xe3.h(dd2.b(e3, e3, e3), e3);
        xe3.d(e, ye3Var2.f, e);
        xe3.h(kd2.G(7, e, 2, 0), e);
        int[] e4 = dd2.e();
        xe3.h(kd2.H(7, e2, 3, 0, e4), e4);
        ye3 ye3Var4 = new ye3(e2);
        xe3.i(e3, ye3Var4.f);
        int[] iArr = ye3Var4.f;
        xe3.k(iArr, e, iArr);
        int[] iArr2 = ye3Var4.f;
        xe3.k(iArr2, e, iArr2);
        ye3 ye3Var5 = new ye3(e);
        xe3.k(e, ye3Var4.f, ye3Var5.f);
        int[] iArr3 = ye3Var5.f;
        xe3.d(iArr3, e3, iArr3);
        int[] iArr4 = ye3Var5.f;
        xe3.k(iArr4, e4, iArr4);
        ye3 ye3Var6 = new ye3(e3);
        xe3.l(ye3Var.f, ye3Var6.f);
        if (!ye3Var3.h()) {
            int[] iArr5 = ye3Var6.f;
            xe3.d(iArr5, ye3Var3.f, iArr5);
        }
        return new ze3(i, ye3Var4, ye3Var5, new ct0[]{ye3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        ye3 ye3Var = (ye3) this.b;
        ye3 ye3Var2 = (ye3) this.c;
        ye3 ye3Var3 = (ye3) pt0Var.q();
        ye3 ye3Var4 = (ye3) pt0Var.r();
        ye3 ye3Var5 = (ye3) this.d[0];
        ye3 ye3Var6 = (ye3) pt0Var.s(0);
        int[] f = dd2.f();
        int[] e = dd2.e();
        int[] e2 = dd2.e();
        int[] e3 = dd2.e();
        boolean h = ye3Var5.h();
        if (h) {
            iArr = ye3Var3.f;
            iArr2 = ye3Var4.f;
        } else {
            xe3.i(ye3Var5.f, e2);
            xe3.d(e2, ye3Var3.f, e);
            xe3.d(e2, ye3Var5.f, e2);
            xe3.d(e2, ye3Var4.f, e2);
            iArr = e;
            iArr2 = e2;
        }
        boolean h2 = ye3Var6.h();
        if (h2) {
            iArr3 = ye3Var.f;
            iArr4 = ye3Var2.f;
        } else {
            xe3.i(ye3Var6.f, e3);
            xe3.d(e3, ye3Var.f, f);
            xe3.d(e3, ye3Var6.f, e3);
            xe3.d(e3, ye3Var2.f, e3);
            iArr3 = f;
            iArr4 = e3;
        }
        int[] e4 = dd2.e();
        xe3.k(iArr3, iArr, e4);
        xe3.k(iArr4, iArr2, e);
        if (dd2.l(e4)) {
            return dd2.l(e) ? J() : i.v();
        }
        xe3.i(e4, e2);
        int[] e5 = dd2.e();
        xe3.d(e2, e4, e5);
        xe3.d(e2, iArr3, e2);
        xe3.f(e5, e5);
        dd2.m(iArr4, e5, f);
        xe3.h(dd2.b(e2, e2, e5), e5);
        ye3 ye3Var7 = new ye3(e3);
        xe3.i(e, ye3Var7.f);
        int[] iArr5 = ye3Var7.f;
        xe3.k(iArr5, e5, iArr5);
        ye3 ye3Var8 = new ye3(e5);
        xe3.k(e2, ye3Var7.f, ye3Var8.f);
        xe3.e(ye3Var8.f, e, f);
        xe3.g(f, ye3Var8.f);
        ye3 ye3Var9 = new ye3(e4);
        if (!h) {
            int[] iArr6 = ye3Var9.f;
            xe3.d(iArr6, ye3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = ye3Var9.f;
            xe3.d(iArr7, ye3Var6.f, iArr7);
        }
        return new ze3(i, ye3Var7, ye3Var8, new ct0[]{ye3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new ze3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new ze3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
