package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: Scopes.kt */
/* renamed from: n70  reason: default package */
/* loaded from: classes2.dex */
public final class n70 implements c90 {
    public final CoroutineContext a;

    public n70(CoroutineContext coroutineContext) {
        this.a = coroutineContext;
    }

    @Override // defpackage.c90
    public CoroutineContext m() {
        return this.a;
    }

    public String toString() {
        return "CoroutineScope(coroutineContext=" + m() + ')';
    }
}
