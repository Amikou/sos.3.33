package defpackage;

import android.os.SystemClock;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.common.v;
import androidx.media3.exoplayer.trackselection.c;
import java.util.Arrays;
import java.util.List;

/* compiled from: BaseTrackSelection.java */
/* renamed from: ao  reason: default package */
/* loaded from: classes.dex */
public abstract class ao implements c {
    public final v a;
    public final int b;
    public final int[] c;
    public final j[] d;
    public final long[] e;
    public int f;

    public ao(v vVar, int[] iArr, int i) {
        int i2 = 0;
        ii.g(iArr.length > 0);
        this.a = (v) ii.e(vVar);
        int length = iArr.length;
        this.b = length;
        this.d = new j[length];
        for (int i3 = 0; i3 < iArr.length; i3++) {
            this.d[i3] = vVar.c(iArr[i3]);
        }
        Arrays.sort(this.d, zn.a);
        this.c = new int[this.b];
        while (true) {
            int i4 = this.b;
            if (i2 < i4) {
                this.c[i2] = vVar.d(this.d[i2]);
                i2++;
            } else {
                this.e = new long[i4];
                return;
            }
        }
    }

    public static /* synthetic */ int v(j jVar, j jVar2) {
        return jVar2.l0 - jVar.l0;
    }

    @Override // defpackage.h84
    public final int b(j jVar) {
        for (int i = 0; i < this.b; i++) {
            if (this.d[i] == jVar) {
                return i;
            }
        }
        return -1;
    }

    @Override // defpackage.h84
    public final v c() {
        return this.a;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public /* synthetic */ boolean e(long j, my myVar, List list) {
        return u01.d(this, j, myVar, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ao aoVar = (ao) obj;
        return this.a == aoVar.a && Arrays.equals(this.c, aoVar.c);
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public boolean f(int i, long j) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        boolean h = h(i, elapsedRealtime);
        int i2 = 0;
        while (i2 < this.b && !h) {
            h = (i2 == i || h(i2, elapsedRealtime)) ? false : true;
            i2++;
        }
        if (h) {
            long[] jArr = this.e;
            jArr[i] = Math.max(jArr[i], b.b(elapsedRealtime, j, Long.MAX_VALUE));
            return true;
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public void g() {
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public boolean h(int i, long j) {
        return this.e[i] > j;
    }

    public int hashCode() {
        if (this.f == 0) {
            this.f = (System.identityHashCode(this.a) * 31) + Arrays.hashCode(this.c);
        }
        return this.f;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public /* synthetic */ void i(boolean z) {
        u01.b(this, z);
    }

    @Override // defpackage.h84
    public final j j(int i) {
        return this.d[i];
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public void k() {
    }

    @Override // defpackage.h84
    public final int l(int i) {
        return this.c[i];
    }

    @Override // defpackage.h84
    public final int length() {
        return this.c.length;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public int m(long j, List<? extends s52> list) {
        return list.size();
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public final j n() {
        return this.d[d()];
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public void p(float f) {
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public /* synthetic */ void r() {
        u01.a(this);
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public /* synthetic */ void s() {
        u01.c(this);
    }

    @Override // defpackage.h84
    public final int t(int i) {
        for (int i2 = 0; i2 < this.b; i2++) {
            if (this.c[i2] == i) {
                return i2;
            }
        }
        return -1;
    }
}
