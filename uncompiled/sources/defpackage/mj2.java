package defpackage;

import com.onesignal.influence.domain.OSInfluenceChannel;
import com.onesignal.influence.domain.OSInfluenceType;
import com.onesignal.influence.domain.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSChannelTracker.kt */
/* renamed from: mj2  reason: default package */
/* loaded from: classes2.dex */
public abstract class mj2 {
    public OSInfluenceType a;
    public JSONArray b;
    public String c;
    public xj2 d;
    public yj2 e;
    public zk2 f;

    public mj2(xj2 xj2Var, yj2 yj2Var, zk2 zk2Var) {
        fs1.f(xj2Var, "dataRepository");
        fs1.f(yj2Var, "logger");
        fs1.f(zk2Var, "timeProvider");
        this.d = xj2Var;
        this.e = yj2Var;
        this.f = zk2Var;
    }

    public abstract void a(JSONObject jSONObject, a aVar);

    public abstract void b();

    public abstract int c();

    public abstract OSInfluenceChannel d();

    public final a e() {
        OSInfluenceChannel d = d();
        OSInfluenceType oSInfluenceType = OSInfluenceType.DISABLED;
        a aVar = new a(d, oSInfluenceType, null);
        if (this.a == null) {
            p();
        }
        OSInfluenceType oSInfluenceType2 = this.a;
        if (oSInfluenceType2 != null) {
            oSInfluenceType = oSInfluenceType2;
        }
        if (oSInfluenceType.isDirect()) {
            if (q()) {
                aVar.e(new JSONArray().put(this.c));
                aVar.f(OSInfluenceType.DIRECT);
            }
        } else if (oSInfluenceType.isIndirect()) {
            if (r()) {
                aVar.e(this.b);
                aVar.f(OSInfluenceType.INDIRECT);
            }
        } else if (s()) {
            aVar.f(OSInfluenceType.UNATTRIBUTED);
        }
        return aVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || (!fs1.b(getClass(), obj.getClass()))) {
            return false;
        }
        mj2 mj2Var = (mj2) obj;
        return this.a == mj2Var.a && fs1.b(mj2Var.h(), h());
    }

    public final xj2 f() {
        return this.d;
    }

    public final String g() {
        return this.c;
    }

    public abstract String h();

    public int hashCode() {
        OSInfluenceType oSInfluenceType = this.a;
        return ((oSInfluenceType != null ? oSInfluenceType.hashCode() : 0) * 31) + h().hashCode();
    }

    public abstract int i();

    public final JSONArray j() {
        return this.b;
    }

    public final OSInfluenceType k() {
        return this.a;
    }

    public abstract JSONArray l() throws JSONException;

    public abstract JSONArray m(String str);

    public final JSONArray n() {
        JSONArray jSONArray = new JSONArray();
        try {
            JSONArray l = l();
            yj2 yj2Var = this.e;
            yj2Var.debug("OneSignal ChannelTracker getLastReceivedIds lastChannelObjectReceived: " + l);
            long i = ((long) (i() * 60)) * 1000;
            long a = this.f.a();
            int length = l.length();
            for (int i2 = 0; i2 < length; i2++) {
                JSONObject jSONObject = l.getJSONObject(i2);
                if (a - jSONObject.getLong("time") <= i) {
                    jSONArray.put(jSONObject.getString(h()));
                }
            }
        } catch (JSONException e) {
            this.e.error("Generating tracker getLastReceivedIds JSONObject ", e);
        }
        return jSONArray;
    }

    public final yj2 o() {
        return this.e;
    }

    public abstract void p();

    public final boolean q() {
        return this.d.m();
    }

    public final boolean r() {
        return this.d.n();
    }

    public final boolean s() {
        return this.d.o();
    }

    public final void t() {
        this.c = null;
        JSONArray n = n();
        this.b = n;
        this.a = (n != null ? n.length() : 0) > 0 ? OSInfluenceType.INDIRECT : OSInfluenceType.UNATTRIBUTED;
        b();
        yj2 yj2Var = this.e;
        yj2Var.debug("OneSignal OSChannelTracker resetAndInitInfluence: " + h() + " finish with influenceType: " + this.a);
    }

    public String toString() {
        return "OSChannelTracker{tag=" + h() + ", influenceType=" + this.a + ", indirectIds=" + this.b + ", directId=" + this.c + '}';
    }

    public abstract void u(JSONArray jSONArray);

    public final void v(String str) {
        yj2 yj2Var = this.e;
        yj2Var.debug("OneSignal OSChannelTracker for: " + h() + " saveLastId: " + str);
        if (str != null) {
            if (str.length() == 0) {
                return;
            }
            JSONArray m = m(str);
            yj2 yj2Var2 = this.e;
            yj2Var2.debug("OneSignal OSChannelTracker for: " + h() + " saveLastId with lastChannelObjectsReceived: " + m);
            try {
                m.put(new JSONObject().put(h(), str).put("time", this.f.a()));
                if (m.length() > c()) {
                    JSONArray jSONArray = new JSONArray();
                    int length = m.length();
                    for (int length2 = m.length() - c(); length2 < length; length2++) {
                        try {
                            jSONArray.put(m.get(length2));
                        } catch (JSONException e) {
                            this.e.error("Generating tracker lastChannelObjectsReceived get JSONObject ", e);
                        }
                    }
                    m = jSONArray;
                }
                yj2 yj2Var3 = this.e;
                yj2Var3.debug("OneSignal OSChannelTracker for: " + h() + " with channelObjectToSave: " + m);
                u(m);
            } catch (JSONException e2) {
                this.e.error("Generating tracker newInfluenceId JSONObject ", e2);
            }
        }
    }

    public final void w(String str) {
        this.c = str;
    }

    public final void x(JSONArray jSONArray) {
        this.b = jSONArray;
    }

    public final void y(OSInfluenceType oSInfluenceType) {
        this.a = oSInfluenceType;
    }
}
