package defpackage;

import com.google.android.gms.measurement.internal.p;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: qr5  reason: default package */
/* loaded from: classes.dex */
public final class qr5 extends n55 {
    public final /* synthetic */ p e;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public qr5(p pVar, tl5 tl5Var) {
        super(tl5Var);
        this.e = pVar;
    }

    @Override // defpackage.n55
    public final void a() {
        this.e.a.w().p().a("Tasks have been queued for a long time");
    }
}
