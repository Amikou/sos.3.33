package defpackage;

import com.google.android.gms.internal.firebase_messaging.i;
import com.google.firebase.messaging.reporting.MessagingClientEvent;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: g82  reason: default package */
/* loaded from: classes2.dex */
public final class g82 {
    public final MessagingClientEvent a;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* renamed from: g82$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public MessagingClientEvent a = null;

        public g82 a() {
            return new g82(this.a);
        }

        public a b(MessagingClientEvent messagingClientEvent) {
            this.a = messagingClientEvent;
            return this;
        }
    }

    static {
        new a().a();
    }

    public g82(MessagingClientEvent messagingClientEvent) {
        this.a = messagingClientEvent;
    }

    public static a b() {
        return new a();
    }

    @i(zza = 1)
    public MessagingClientEvent a() {
        return this.a;
    }

    public byte[] c() {
        return nf5.a(this);
    }
}
