package defpackage;

import androidx.media3.common.util.b;
import com.github.mikephil.charting.utils.Utils;
import defpackage.la2;
import defpackage.wi3;

/* compiled from: XingSeeker.java */
/* renamed from: js4  reason: default package */
/* loaded from: classes.dex */
public final class js4 implements zi3 {
    public final long a;
    public final int b;
    public final long c;
    public final long d;
    public final long e;
    public final long[] f;

    public js4(long j, int i, long j2) {
        this(j, i, j2, -1L, null);
    }

    public static js4 a(long j, long j2, la2.a aVar, op2 op2Var) {
        int H;
        int i = aVar.g;
        int i2 = aVar.d;
        int n = op2Var.n();
        if ((n & 1) != 1 || (H = op2Var.H()) == 0) {
            return null;
        }
        long J0 = b.J0(H, i * 1000000, i2);
        if ((n & 6) != 6) {
            return new js4(j2, aVar.c, J0);
        }
        long F = op2Var.F();
        long[] jArr = new long[100];
        for (int i3 = 0; i3 < 100; i3++) {
            jArr[i3] = op2Var.D();
        }
        if (j != -1) {
            long j3 = j2 + F;
            if (j != j3) {
                p12.i("XingSeeker", "XING data size mismatch: " + j + ", " + j3);
            }
        }
        return new js4(j2, aVar.c, J0, F, jArr);
    }

    @Override // defpackage.zi3
    public long b(long j) {
        long j2 = j - this.a;
        if (!e() || j2 <= this.b) {
            return 0L;
        }
        long[] jArr = (long[]) ii.i(this.f);
        double d = (j2 * 256.0d) / this.d;
        int i = b.i(jArr, (long) d, true, true);
        long c = c(i);
        long j3 = jArr[i];
        int i2 = i + 1;
        long c2 = c(i2);
        long j4 = i == 99 ? 256L : jArr[i2];
        return c + Math.round((j3 == j4 ? Utils.DOUBLE_EPSILON : (d - j3) / (j4 - j3)) * (c2 - c));
    }

    public final long c(int i) {
        return (this.c * i) / 100;
    }

    @Override // defpackage.zi3
    public long d() {
        return this.e;
    }

    @Override // defpackage.wi3
    public boolean e() {
        return this.f != null;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        long[] jArr;
        if (!e()) {
            return new wi3.a(new yi3(0L, this.a + this.b));
        }
        long r = b.r(j, 0L, this.c);
        double d = (r * 100.0d) / this.c;
        double d2 = Utils.DOUBLE_EPSILON;
        if (d > Utils.DOUBLE_EPSILON) {
            if (d >= 100.0d) {
                d2 = 256.0d;
            } else {
                int i = (int) d;
                double d3 = ((long[]) ii.i(this.f))[i];
                d2 = d3 + ((d - i) * ((i == 99 ? 256.0d : jArr[i + 1]) - d3));
            }
        }
        return new wi3.a(new yi3(r, this.a + b.r(Math.round((d2 / 256.0d) * this.d), this.b, this.d - 1)));
    }

    @Override // defpackage.wi3
    public long i() {
        return this.c;
    }

    public js4(long j, int i, long j2, long j3, long[] jArr) {
        this.a = j;
        this.b = i;
        this.c = j2;
        this.f = jArr;
        this.d = j3;
        this.e = j3 != -1 ? j + j3 : -1L;
    }
}
