package defpackage;

import java.math.BigInteger;
import zendesk.support.request.CellBase;

/* renamed from: he3  reason: default package */
/* loaded from: classes2.dex */
public class he3 {
    public static final int[] a = {Integer.MAX_VALUE, -1, -1, -1, -1};
    public static final int[] b = {1, 1073741825, 0, 0, 0, -2, -2, -1, -1, -1};
    public static final int[] c = {-1, -1073741826, -1, -1, -1, 1, 1};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (bd2.a(iArr, iArr2, iArr3) != 0 || (iArr3[4] == -1 && bd2.i(iArr3, a))) {
            kd2.g(5, CellBase.GROUP_ID_END_USER, iArr3);
        }
    }

    public static void b(int[] iArr, int[] iArr2) {
        if (kd2.s(5, iArr, iArr2) != 0 || (iArr2[4] == -1 && bd2.i(iArr2, a))) {
            kd2.g(5, CellBase.GROUP_ID_END_USER, iArr2);
        }
    }

    public static int[] c(BigInteger bigInteger) {
        int[] g = bd2.g(bigInteger);
        if (g[4] == -1) {
            int[] iArr = a;
            if (bd2.i(g, iArr)) {
                bd2.t(iArr, g);
            }
        }
        return g;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] e = bd2.e();
        bd2.l(iArr, iArr2, e);
        g(e, iArr3);
    }

    public static void e(int[] iArr, int[] iArr2, int[] iArr3) {
        if (bd2.p(iArr, iArr2, iArr3) != 0 || (iArr3[9] == -1 && kd2.q(10, iArr3, b))) {
            int[] iArr4 = c;
            if (kd2.e(iArr4.length, iArr4, iArr3) != 0) {
                kd2.t(10, iArr3, iArr4.length);
            }
        }
    }

    public static void f(int[] iArr, int[] iArr2) {
        if (bd2.k(iArr)) {
            bd2.v(iArr2);
        } else {
            bd2.s(a, iArr, iArr2);
        }
    }

    public static void g(int[] iArr, int[] iArr2) {
        long j = iArr[5] & 4294967295L;
        long j2 = iArr[6] & 4294967295L;
        long j3 = iArr[7] & 4294967295L;
        long j4 = iArr[8] & 4294967295L;
        long j5 = iArr[9] & 4294967295L;
        long j6 = (iArr[0] & 4294967295L) + j + (j << 31) + 0;
        iArr2[0] = (int) j6;
        long j7 = (j6 >>> 32) + (iArr[1] & 4294967295L) + j2 + (j2 << 31);
        iArr2[1] = (int) j7;
        long j8 = (j7 >>> 32) + (iArr[2] & 4294967295L) + j3 + (j3 << 31);
        iArr2[2] = (int) j8;
        long j9 = (j8 >>> 32) + (iArr[3] & 4294967295L) + j4 + (j4 << 31);
        iArr2[3] = (int) j9;
        long j10 = (j9 >>> 32) + (4294967295L & iArr[4]) + j5 + (j5 << 31);
        iArr2[4] = (int) j10;
        h((int) (j10 >>> 32), iArr2);
    }

    public static void h(int i, int[] iArr) {
        if ((i == 0 || bd2.q(CellBase.GROUP_ID_END_USER, i, iArr, 0) == 0) && !(iArr[4] == -1 && bd2.i(iArr, a))) {
            return;
        }
        kd2.g(5, CellBase.GROUP_ID_END_USER, iArr);
    }

    public static void i(int[] iArr, int[] iArr2) {
        int[] e = bd2.e();
        bd2.r(iArr, e);
        g(e, iArr2);
    }

    public static void j(int[] iArr, int i, int[] iArr2) {
        int[] e = bd2.e();
        bd2.r(iArr, e);
        while (true) {
            g(e, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            bd2.r(iArr2, e);
        }
    }

    public static void k(int[] iArr, int[] iArr2, int[] iArr3) {
        if (bd2.s(iArr, iArr2, iArr3) != 0) {
            kd2.O(5, CellBase.GROUP_ID_END_USER, iArr3);
        }
    }

    public static void l(int[] iArr, int[] iArr2) {
        if (kd2.E(5, iArr, 0, iArr2) != 0 || (iArr2[4] == -1 && bd2.i(iArr2, a))) {
            kd2.g(5, CellBase.GROUP_ID_END_USER, iArr2);
        }
    }
}
