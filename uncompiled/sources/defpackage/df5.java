package defpackage;

import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.a;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.e;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: df5  reason: default package */
/* loaded from: classes.dex */
public final class df5 implements Runnable {
    public final /* synthetic */ c a;
    public final /* synthetic */ da5 f0;

    public df5(da5 da5Var, c cVar) {
        this.f0 = da5Var;
        this.a = cVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        e eVar;
        e eVar2;
        e eVar3;
        a aVar;
        e eVar4;
        e eVar5;
        if (this.a.n()) {
            eVar5 = this.f0.c;
            eVar5.u();
            return;
        }
        try {
            aVar = this.f0.b;
            Object a = aVar.a(this.a);
            eVar4 = this.f0.c;
            eVar4.t(a);
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                eVar3 = this.f0.c;
                eVar3.s((Exception) e.getCause());
                return;
            }
            eVar2 = this.f0.c;
            eVar2.s(e);
        } catch (Exception e2) {
            eVar = this.f0.c;
            eVar.s(e2);
        }
    }
}
