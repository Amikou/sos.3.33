package defpackage;

import android.net.Uri;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: DefaultCacheKeyFactory.java */
/* renamed from: ji0  reason: default package */
/* loaded from: classes.dex */
public class ji0 implements xt {
    public static ji0 a;

    public static synchronized ji0 f() {
        ji0 ji0Var;
        synchronized (ji0.class) {
            if (a == null) {
                a = new ji0();
            }
            ji0Var = a;
        }
        return ji0Var;
    }

    @Override // defpackage.xt
    public wt a(ImageRequest imageRequest, Object obj) {
        return new eq(e(imageRequest.u()).toString(), imageRequest.q(), imageRequest.s(), imageRequest.g(), null, null, obj);
    }

    @Override // defpackage.xt
    public wt b(ImageRequest imageRequest, Uri uri, Object obj) {
        return new zo3(e(uri).toString());
    }

    @Override // defpackage.xt
    public wt c(ImageRequest imageRequest, Object obj) {
        wt wtVar;
        String str;
        rt2 k = imageRequest.k();
        if (k != null) {
            wt c = k.c();
            str = k.getClass().getName();
            wtVar = c;
        } else {
            wtVar = null;
            str = null;
        }
        return new eq(e(imageRequest.u()).toString(), imageRequest.q(), imageRequest.s(), imageRequest.g(), wtVar, str, obj);
    }

    @Override // defpackage.xt
    public wt d(ImageRequest imageRequest, Object obj) {
        return b(imageRequest, imageRequest.u(), obj);
    }

    public Uri e(Uri uri) {
        return uri;
    }
}
