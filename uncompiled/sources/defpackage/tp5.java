package defpackage;

import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import com.google.android.gms.measurement.internal.zzas;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: tp5  reason: default package */
/* loaded from: classes.dex */
public final class tp5 implements Runnable {
    public final /* synthetic */ m a;
    public final /* synthetic */ zzas f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ AppMeasurementDynamiteService h0;

    public tp5(AppMeasurementDynamiteService appMeasurementDynamiteService, m mVar, zzas zzasVar, String str) {
        this.h0 = appMeasurementDynamiteService;
        this.a = mVar;
        this.f0 = zzasVar;
        this.g0 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.h0.a.R().t(this.a, this.f0, this.g0);
    }
}
