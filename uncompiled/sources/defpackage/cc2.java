package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: MyTokensListFragmentDirections.java */
/* renamed from: cc2  reason: default package */
/* loaded from: classes2.dex */
public class cc2 {

    /* compiled from: MyTokensListFragmentDirections.java */
    /* renamed from: cc2$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("userTokenData")) {
                UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) this.a.get("userTokenData");
                if (!Parcelable.class.isAssignableFrom(UserTokenItemDisplayModel.class) && userTokenItemDisplayModel != null) {
                    if (Serializable.class.isAssignableFrom(UserTokenItemDisplayModel.class)) {
                        bundle.putSerializable("userTokenData", (Serializable) Serializable.class.cast(userTokenItemDisplayModel));
                    } else {
                        throw new UnsupportedOperationException(UserTokenItemDisplayModel.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("userTokenData", (Parcelable) Parcelable.class.cast(userTokenItemDisplayModel));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_myTokensListFragment_to_transferHistoryFragment;
        }

        public UserTokenItemDisplayModel c() {
            return (UserTokenItemDisplayModel) this.a.get("userTokenData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("userTokenData") != bVar.a.containsKey("userTokenData")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionMyTokensListFragmentToTransferHistoryFragment(actionId=" + b() + "){userTokenData=" + c() + "}";
        }

        public b(UserTokenItemDisplayModel userTokenItemDisplayModel) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (userTokenItemDisplayModel != null) {
                hashMap.put("userTokenData", userTokenItemDisplayModel);
                return;
            }
            throw new IllegalArgumentException("Argument \"userTokenData\" is marked as non-null but was passed a null value.");
        }
    }

    public static ce2 a() {
        return new l6(R.id.action_myTokensListFragment_to_addNewTokenFragment);
    }

    public static ce2 b() {
        return new l6(R.id.action_myTokensListFragment_to_ReceiveFragment);
    }

    public static ce2 c() {
        return new l6(R.id.action_myTokensListFragment_to_sendFragment);
    }

    public static b d(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        return new b(userTokenItemDisplayModel);
    }

    public static ce2 e() {
        return new l6(R.id.action_navigation_my_tokens_to_notificationHistoryFragment);
    }
}
