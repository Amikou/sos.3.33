package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.p;
import androidx.media3.common.u;
import androidx.media3.exoplayer.ExoPlaybackException;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.trackselection.g;
import com.google.common.collect.ImmutableList;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: PlaybackInfo.java */
/* renamed from: lr2  reason: default package */
/* loaded from: classes.dex */
public final class lr2 {
    public static final j.b s = new j.b(new Object());
    public final u a;
    public final j.b b;
    public final long c;
    public final long d;
    public final int e;
    public final ExoPlaybackException f;
    public final boolean g;
    public final c84 h;
    public final g i;
    public final List<Metadata> j;
    public final j.b k;
    public final boolean l;
    public final int m;
    public final p n;
    public final boolean o;
    public volatile long p;
    public volatile long q;
    public volatile long r;

    public lr2(u uVar, j.b bVar, long j, long j2, int i, ExoPlaybackException exoPlaybackException, boolean z, c84 c84Var, g gVar, List<Metadata> list, j.b bVar2, boolean z2, int i2, p pVar, long j3, long j4, long j5, boolean z3) {
        this.a = uVar;
        this.b = bVar;
        this.c = j;
        this.d = j2;
        this.e = i;
        this.f = exoPlaybackException;
        this.g = z;
        this.h = c84Var;
        this.i = gVar;
        this.j = list;
        this.k = bVar2;
        this.l = z2;
        this.m = i2;
        this.n = pVar;
        this.p = j3;
        this.q = j4;
        this.r = j5;
        this.o = z3;
    }

    public static lr2 j(g gVar) {
        u uVar = u.a;
        j.b bVar = s;
        return new lr2(uVar, bVar, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, 0L, 1, null, false, c84.h0, gVar, ImmutableList.of(), bVar, false, 0, p.h0, 0L, 0L, 0L, false);
    }

    public static j.b k() {
        return s;
    }

    public lr2 a(boolean z) {
        return new lr2(this.a, this.b, this.c, this.d, this.e, this.f, z, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.p, this.q, this.r, this.o);
    }

    public lr2 b(j.b bVar) {
        return new lr2(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, bVar, this.l, this.m, this.n, this.p, this.q, this.r, this.o);
    }

    public lr2 c(j.b bVar, long j, long j2, long j3, long j4, c84 c84Var, g gVar, List<Metadata> list) {
        return new lr2(this.a, bVar, j2, j3, this.e, this.f, this.g, c84Var, gVar, list, this.k, this.l, this.m, this.n, this.p, j4, j, this.o);
    }

    public lr2 d(boolean z, int i) {
        return new lr2(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, z, i, this.n, this.p, this.q, this.r, this.o);
    }

    public lr2 e(ExoPlaybackException exoPlaybackException) {
        return new lr2(this.a, this.b, this.c, this.d, this.e, exoPlaybackException, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.p, this.q, this.r, this.o);
    }

    public lr2 f(p pVar) {
        return new lr2(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, pVar, this.p, this.q, this.r, this.o);
    }

    public lr2 g(int i) {
        return new lr2(this.a, this.b, this.c, this.d, i, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.p, this.q, this.r, this.o);
    }

    public lr2 h(boolean z) {
        return new lr2(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.p, this.q, this.r, z);
    }

    public lr2 i(u uVar) {
        return new lr2(uVar, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.p, this.q, this.r, this.o);
    }
}
