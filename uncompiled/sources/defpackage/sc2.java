package defpackage;

import java.util.Hashtable;
import org.bouncycastle.asn1.i;

/* renamed from: sc2  reason: default package */
/* loaded from: classes2.dex */
public class sc2 {
    public static final Hashtable a = new Hashtable();
    public static final Hashtable b = new Hashtable();

    static {
        a("B-571", ga3.F);
        a("B-409", ga3.D);
        a("B-283", ga3.n);
        a("B-233", ga3.t);
        a("B-163", ga3.l);
        a("K-571", ga3.E);
        a("K-409", ga3.C);
        a("K-283", ga3.m);
        a("K-233", ga3.s);
        a("K-163", ga3.b);
        a("P-521", ga3.B);
        a("P-384", ga3.A);
        a("P-256", ga3.H);
        a("P-224", ga3.z);
        a("P-192", ga3.G);
    }

    public static void a(String str, i iVar) {
        a.put(str, iVar);
        b.put(iVar, str);
    }

    public static pr4 b(String str) {
        i iVar = (i) a.get(su3.g(str));
        if (iVar != null) {
            return c(iVar);
        }
        return null;
    }

    public static pr4 c(i iVar) {
        return fa3.i(iVar);
    }

    public static String d(i iVar) {
        return (String) b.get(iVar);
    }

    public static i e(String str) {
        return (i) a.get(su3.g(str));
    }
}
