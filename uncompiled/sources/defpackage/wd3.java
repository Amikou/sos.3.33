package defpackage;

import kotlin.text.Regex;

/* compiled from: StringNumberConversionsJVM.kt */
/* renamed from: wd3  reason: default package */
/* loaded from: classes2.dex */
public final class wd3 {
    public static final Regex a;

    static {
        new wd3();
        String str = "[eE][+-]?(\\p{Digit}+)";
        a = new Regex("[\\x00-\\x20]*[+-]?(NaN|Infinity|((" + ("((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)(" + str + ")?)|(\\.((\\p{Digit}+))(" + str + ")?)|((" + ("(0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+))") + ")[pP][+-]?(\\p{Digit}+))") + ")[fFdD]?))[\\x00-\\x20]*");
    }
}
