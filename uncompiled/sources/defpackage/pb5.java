package defpackage;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.measurement.zzcl;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: pb5  reason: default package */
/* loaded from: classes.dex */
public final class pb5 implements Parcelable.Creator<zzcl> {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzcl createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        String str4 = null;
        boolean z = false;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 1:
                    j = SafeParcelReader.F(parcel, C);
                    break;
                case 2:
                    j2 = SafeParcelReader.F(parcel, C);
                    break;
                case 3:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 4:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 5:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 6:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 7:
                    bundle = SafeParcelReader.f(parcel, C);
                    break;
                case 8:
                    str4 = SafeParcelReader.p(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzcl(j, j2, z, str, str2, str3, bundle, str4);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzcl[] newArray(int i) {
        return new zzcl[i];
    }
}
