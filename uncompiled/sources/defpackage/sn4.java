package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.LinkedHashSet;
import java.util.Set;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: WalletPrefs.kt */
/* renamed from: sn4  reason: default package */
/* loaded from: classes2.dex */
public final class sn4 {
    public static final sn4 a = new sn4();
    public static String b = "safemoon";

    public final boolean a(Context context, String str, boolean z) {
        fs1.f(context, "context");
        return c(context).getBoolean(str, z);
    }

    public final Set<String> b(Context context, String str) {
        fs1.f(context, "context");
        fs1.f(str, "key");
        Set<String> stringSet = c(context).getStringSet(str, tm3.b());
        Set<String> n0 = stringSet == null ? null : j20.n0(stringSet);
        return n0 == null ? new LinkedHashSet() : n0;
    }

    public final SharedPreferences c(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(b, 0);
        fs1.e(sharedPreferences, "context.getSharedPrefere…ME, Context.MODE_PRIVATE)");
        return sharedPreferences;
    }

    public final void d(Context context, String str, Boolean bool) {
        fs1.f(context, "context");
        SharedPreferences.Editor edit = c(context).edit();
        fs1.d(bool);
        edit.putBoolean(str, bool.booleanValue()).apply();
    }

    public final void e(Context context, String str, String str2) {
        fs1.f(context, "context");
        fs1.f(str, "key");
        fs1.f(str2, "value");
        Set<String> b2 = b(context, str);
        b2.add(str2);
        c(context).edit().putStringSet(str, b2).apply();
    }

    public final void f(String str) {
        fs1.f(str, PublicResolver.FUNC_NAME);
        b = str;
    }
}
