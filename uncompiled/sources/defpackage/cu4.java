package defpackage;

import com.google.android.play.core.internal.b;
import java.util.List;

/* renamed from: cu4  reason: default package */
/* loaded from: classes2.dex */
public final class cu4 implements Runnable {
    public final /* synthetic */ List a;
    public final /* synthetic */ ax4 f0;
    public final /* synthetic */ eu4 g0;

    public cu4(eu4 eu4Var, List list, ax4 ax4Var) {
        this.g0 = eu4Var;
        this.a = list;
        this.f0 = ax4Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        b bVar;
        try {
            bVar = this.g0.c;
            if (bVar.c(this.a)) {
                eu4.c(this.g0, this.f0);
            } else {
                eu4.d(this.g0, this.a, this.f0);
            }
        } catch (Exception unused) {
            this.f0.c(-11);
        }
    }
}
