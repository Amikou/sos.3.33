package defpackage;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.e;

/* compiled from: FragmentController.java */
/* renamed from: y91  reason: default package */
/* loaded from: classes.dex */
public class y91 {
    public final e<?> a;

    public y91(e<?> eVar) {
        this.a = eVar;
    }

    public static y91 b(e<?> eVar) {
        return new y91((e) du2.f(eVar, "callbacks == null"));
    }

    public void a(Fragment fragment) {
        e<?> eVar = this.a;
        eVar.h0.l(eVar, eVar, fragment);
    }

    public void c() {
        this.a.h0.A();
    }

    public void d(Configuration configuration) {
        this.a.h0.C(configuration);
    }

    public boolean e(MenuItem menuItem) {
        return this.a.h0.D(menuItem);
    }

    public void f() {
        this.a.h0.E();
    }

    public boolean g(Menu menu, MenuInflater menuInflater) {
        return this.a.h0.F(menu, menuInflater);
    }

    public void h() {
        this.a.h0.G();
    }

    public void i() {
        this.a.h0.I();
    }

    public void j(boolean z) {
        this.a.h0.J(z);
    }

    public boolean k(MenuItem menuItem) {
        return this.a.h0.L(menuItem);
    }

    public void l(Menu menu) {
        this.a.h0.M(menu);
    }

    public void m() {
        this.a.h0.O();
    }

    public void n(boolean z) {
        this.a.h0.P(z);
    }

    public boolean o(Menu menu) {
        return this.a.h0.Q(menu);
    }

    public void p() {
        this.a.h0.S();
    }

    public void q() {
        this.a.h0.T();
    }

    public void r() {
        this.a.h0.V();
    }

    public boolean s() {
        return this.a.h0.c0(true);
    }

    public FragmentManager t() {
        return this.a.h0;
    }

    public void u() {
        this.a.h0.V0();
    }

    public View v(View view, String str, Context context, AttributeSet attributeSet) {
        return this.a.h0.w0().onCreateView(view, str, context, attributeSet);
    }

    public void w(Parcelable parcelable) {
        e<?> eVar = this.a;
        if (eVar instanceof hj4) {
            eVar.h0.l1(parcelable);
            return;
        }
        throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
    }

    public Parcelable x() {
        return this.a.h0.n1();
    }
}
