package defpackage;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;

/* compiled from: DiskCacheStrategy.java */
/* renamed from: bp0  reason: default package */
/* loaded from: classes.dex */
public abstract class bp0 {
    public static final bp0 a;
    public static final bp0 b;
    public static final bp0 c;

    /* compiled from: DiskCacheStrategy.java */
    /* renamed from: bp0$a */
    /* loaded from: classes.dex */
    public class a extends bp0 {
        @Override // defpackage.bp0
        public boolean a() {
            return true;
        }

        @Override // defpackage.bp0
        public boolean b() {
            return true;
        }

        @Override // defpackage.bp0
        public boolean c(DataSource dataSource) {
            return dataSource == DataSource.REMOTE;
        }

        @Override // defpackage.bp0
        public boolean d(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return (dataSource == DataSource.RESOURCE_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }
    }

    /* compiled from: DiskCacheStrategy.java */
    /* renamed from: bp0$b */
    /* loaded from: classes.dex */
    public class b extends bp0 {
        @Override // defpackage.bp0
        public boolean a() {
            return false;
        }

        @Override // defpackage.bp0
        public boolean b() {
            return false;
        }

        @Override // defpackage.bp0
        public boolean c(DataSource dataSource) {
            return false;
        }

        @Override // defpackage.bp0
        public boolean d(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return false;
        }
    }

    /* compiled from: DiskCacheStrategy.java */
    /* renamed from: bp0$c */
    /* loaded from: classes.dex */
    public class c extends bp0 {
        @Override // defpackage.bp0
        public boolean a() {
            return true;
        }

        @Override // defpackage.bp0
        public boolean b() {
            return false;
        }

        @Override // defpackage.bp0
        public boolean c(DataSource dataSource) {
            return (dataSource == DataSource.DATA_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }

        @Override // defpackage.bp0
        public boolean d(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return false;
        }
    }

    /* compiled from: DiskCacheStrategy.java */
    /* renamed from: bp0$d */
    /* loaded from: classes.dex */
    public class d extends bp0 {
        @Override // defpackage.bp0
        public boolean a() {
            return false;
        }

        @Override // defpackage.bp0
        public boolean b() {
            return true;
        }

        @Override // defpackage.bp0
        public boolean c(DataSource dataSource) {
            return false;
        }

        @Override // defpackage.bp0
        public boolean d(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return (dataSource == DataSource.RESOURCE_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }
    }

    /* compiled from: DiskCacheStrategy.java */
    /* renamed from: bp0$e */
    /* loaded from: classes.dex */
    public class e extends bp0 {
        @Override // defpackage.bp0
        public boolean a() {
            return true;
        }

        @Override // defpackage.bp0
        public boolean b() {
            return true;
        }

        @Override // defpackage.bp0
        public boolean c(DataSource dataSource) {
            return dataSource == DataSource.REMOTE;
        }

        @Override // defpackage.bp0
        public boolean d(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return ((z && dataSource == DataSource.DATA_DISK_CACHE) || dataSource == DataSource.LOCAL) && encodeStrategy == EncodeStrategy.TRANSFORMED;
        }
    }

    static {
        new a();
        a = new b();
        b = new c();
        new d();
        c = new e();
    }

    public abstract boolean a();

    public abstract boolean b();

    public abstract boolean c(DataSource dataSource);

    public abstract boolean d(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy);
}
