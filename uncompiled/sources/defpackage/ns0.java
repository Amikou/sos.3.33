package defpackage;

import androidx.media3.extractor.text.a;
import java.util.List;

/* compiled from: DvbDecoder.java */
/* renamed from: ns0  reason: default package */
/* loaded from: classes.dex */
public final class ns0 extends a {
    public final os0 n;

    public ns0(List<byte[]> list) {
        super("DvbDecoder");
        op2 op2Var = new op2(list.get(0));
        this.n = new os0(op2Var.J(), op2Var.J());
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) {
        if (z) {
            this.n.r();
        }
        return new ps0(this.n.b(bArr, i));
    }
}
