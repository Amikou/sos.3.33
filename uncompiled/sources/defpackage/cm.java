package defpackage;

import android.os.Process;

/* compiled from: BackgroundPriorityRunnable.java */
/* renamed from: cm  reason: default package */
/* loaded from: classes2.dex */
public abstract class cm implements Runnable {
    public abstract void a();

    @Override // java.lang.Runnable
    public final void run() {
        Process.setThreadPriority(10);
        a();
    }
}
