package defpackage;

import android.os.Build;
import android.os.CancellationSignal;

/* compiled from: CancellationSignalProvider.java */
/* renamed from: uv  reason: default package */
/* loaded from: classes.dex */
public class uv {
    public final c a = new a(this);
    public CancellationSignal b;
    public tv c;

    /* compiled from: CancellationSignalProvider.java */
    /* renamed from: uv$a */
    /* loaded from: classes.dex */
    public class a implements c {
        public a(uv uvVar) {
        }

        @Override // defpackage.uv.c
        public tv a() {
            return new tv();
        }

        @Override // defpackage.uv.c
        public CancellationSignal b() {
            return b.b();
        }
    }

    /* compiled from: CancellationSignalProvider.java */
    /* renamed from: uv$b */
    /* loaded from: classes.dex */
    public static class b {
        public static void a(CancellationSignal cancellationSignal) {
            cancellationSignal.cancel();
        }

        public static CancellationSignal b() {
            return new CancellationSignal();
        }
    }

    /* compiled from: CancellationSignalProvider.java */
    /* renamed from: uv$c */
    /* loaded from: classes.dex */
    public interface c {
        tv a();

        CancellationSignal b();
    }

    public void a() {
        CancellationSignal cancellationSignal;
        if (Build.VERSION.SDK_INT >= 16 && (cancellationSignal = this.b) != null) {
            try {
                b.a(cancellationSignal);
            } catch (NullPointerException unused) {
            }
            this.b = null;
        }
        tv tvVar = this.c;
        if (tvVar != null) {
            try {
                tvVar.a();
            } catch (NullPointerException unused2) {
            }
            this.c = null;
        }
    }

    public CancellationSignal b() {
        if (this.b == null) {
            this.b = this.a.b();
        }
        return this.b;
    }

    public tv c() {
        if (this.c == null) {
            this.c = this.a.a();
        }
        return this.c;
    }
}
