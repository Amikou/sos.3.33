package defpackage;

import androidx.media3.common.j;

/* compiled from: MetadataDecoderFactory.java */
/* renamed from: m82  reason: default package */
/* loaded from: classes.dex */
public interface m82 {
    public static final m82 a = new a();

    /* compiled from: MetadataDecoderFactory.java */
    /* renamed from: m82$a */
    /* loaded from: classes.dex */
    public class a implements m82 {
        @Override // defpackage.m82
        public boolean a(j jVar) {
            String str = jVar.p0;
            return "application/id3".equals(str) || "application/x-emsg".equals(str) || "application/x-scte35".equals(str) || "application/x-icy".equals(str) || "application/vnd.dvb.ait".equals(str);
        }

        @Override // defpackage.m82
        public l82 b(j jVar) {
            String str = jVar.p0;
            if (str != null) {
                char c = 65535;
                switch (str.hashCode()) {
                    case -1354451219:
                        if (str.equals("application/vnd.dvb.ait")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -1348231605:
                        if (str.equals("application/x-icy")) {
                            c = 1;
                            break;
                        }
                        break;
                    case -1248341703:
                        if (str.equals("application/id3")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1154383568:
                        if (str.equals("application/x-emsg")) {
                            c = 3;
                            break;
                        }
                        break;
                    case 1652648887:
                        if (str.equals("application/x-scte35")) {
                            c = 4;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        return new qf();
                    case 1:
                        return new gn1();
                    case 2:
                        return new androidx.media3.extractor.metadata.id3.a();
                    case 3:
                        return new by0();
                    case 4:
                        return new rr3();
                }
            }
            throw new IllegalArgumentException("Attempted to create decoder for unsupported MIME type: " + str);
        }
    }

    boolean a(j jVar);

    l82 b(j jVar);
}
