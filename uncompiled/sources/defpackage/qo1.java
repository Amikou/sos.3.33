package defpackage;

import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.image.a;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: ImagePipeline.java */
/* renamed from: qo1  reason: default package */
/* loaded from: classes.dex */
public class qo1 {
    public final lv2 a;
    public final h73 b;
    public final i73 c;
    public final fw3<Boolean> d;
    public final l72<wt, a> e;
    public final l72<wt, PooledByteBuffer> f;
    public final xt g;
    public final fw3<Boolean> h;
    public AtomicLong i = new AtomicLong();
    public final fw3<Boolean> j;
    public final av k;
    public final so1 l;

    static {
        new CancellationException("Prefetching is not enabled");
    }

    public qo1(lv2 lv2Var, Set<h73> set, Set<i73> set2, fw3<Boolean> fw3Var, l72<wt, a> l72Var, l72<wt, PooledByteBuffer> l72Var2, xr xrVar, xr xrVar2, xt xtVar, h54 h54Var, fw3<Boolean> fw3Var2, fw3<Boolean> fw3Var3, av avVar, so1 so1Var) {
        this.a = lv2Var;
        this.b = new k91(set);
        this.c = new j91(set2);
        this.d = fw3Var;
        this.e = l72Var;
        this.f = l72Var2;
        this.g = xtVar;
        this.h = fw3Var2;
        this.j = fw3Var3;
        this.k = avVar;
        this.l = so1Var;
    }

    public ge0<com.facebook.common.references.a<a>> a(ImageRequest imageRequest, Object obj, ImageRequest.RequestLevel requestLevel, h73 h73Var, String str) {
        try {
            return f(this.a.e(imageRequest), imageRequest, requestLevel, obj, h73Var, str);
        } catch (Exception e) {
            return ie0.b(e);
        }
    }

    public String b() {
        return String.valueOf(this.i.getAndIncrement());
    }

    public l72<wt, a> c() {
        return this.e;
    }

    public xt d() {
        return this.g;
    }

    public h73 e(ImageRequest imageRequest, h73 h73Var) {
        if (h73Var == null) {
            if (imageRequest.p() == null) {
                return this.b;
            }
            return new k91(this.b, imageRequest.p());
        } else if (imageRequest.p() == null) {
            return new k91(this.b, h73Var);
        } else {
            return new k91(this.b, h73Var, imageRequest.p());
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0068  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final <T> defpackage.ge0<com.facebook.common.references.a<T>> f(defpackage.dv2<com.facebook.common.references.a<T>> r15, com.facebook.imagepipeline.request.ImageRequest r16, com.facebook.imagepipeline.request.ImageRequest.RequestLevel r17, java.lang.Object r18, defpackage.h73 r19, java.lang.String r20) {
        /*
            r14 = this;
            r1 = r14
            boolean r0 = defpackage.nc1.d()
            if (r0 == 0) goto Lc
            java.lang.String r0 = "ImagePipeline#submitFetchRequest"
            defpackage.nc1.a(r0)
        Lc:
            es1 r0 = new es1
            r3 = r16
            r2 = r19
            h73 r2 = r14.e(r3, r2)
            i73 r4 = r1.c
            r0.<init>(r2, r4)
            av r2 = r1.k
            r4 = 0
            r7 = r18
            if (r2 == 0) goto L25
            r2.a(r7, r4)
        L25:
            com.facebook.imagepipeline.request.ImageRequest$RequestLevel r2 = r16.j()     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            r5 = r17
            com.facebook.imagepipeline.request.ImageRequest$RequestLevel r8 = com.facebook.imagepipeline.request.ImageRequest.RequestLevel.getMax(r2, r5)     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            xm3 r13 = new xm3     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            java.lang.String r5 = r14.b()     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            r9 = 0
            boolean r2 = r16.o()     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            if (r2 != 0) goto L49
            android.net.Uri r2 = r16.u()     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            boolean r2 = defpackage.qf4.l(r2)     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            if (r2 != 0) goto L47
            goto L49
        L47:
            r10 = r4
            goto L4b
        L49:
            r2 = 1
            r10 = r2
        L4b:
            com.facebook.imagepipeline.common.Priority r11 = r16.n()     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            so1 r12 = r1.l     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            r2 = r13
            r3 = r16
            r4 = r5
            r5 = r20
            r6 = r0
            r7 = r18
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            r2 = r15
            ge0 r0 = defpackage.zz.I(r15, r13, r0)     // Catch: java.lang.Throwable -> L6c java.lang.Exception -> L6e
            boolean r2 = defpackage.nc1.d()
            if (r2 == 0) goto L6b
            defpackage.nc1.b()
        L6b:
            return r0
        L6c:
            r0 = move-exception
            goto L7d
        L6e:
            r0 = move-exception
            ge0 r0 = defpackage.ie0.b(r0)     // Catch: java.lang.Throwable -> L6c
            boolean r2 = defpackage.nc1.d()
            if (r2 == 0) goto L7c
            defpackage.nc1.b()
        L7c:
            return r0
        L7d:
            boolean r2 = defpackage.nc1.d()
            if (r2 == 0) goto L86
            defpackage.nc1.b()
        L86:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.qo1.f(dv2, com.facebook.imagepipeline.request.ImageRequest, com.facebook.imagepipeline.request.ImageRequest$RequestLevel, java.lang.Object, h73, java.lang.String):ge0");
    }
}
