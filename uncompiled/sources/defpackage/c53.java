package defpackage;

/* renamed from: c53  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class c53 implements m60 {
    public final ep a;
    public final Object b;

    public c53(ep epVar, Object obj) {
        this.a = epVar;
        this.b = obj;
    }

    public static m60 a(ep epVar, Object obj) {
        return new c53(epVar, obj);
    }

    @Override // defpackage.m60
    public void accept(Object obj) {
        this.a.a(this.b, obj);
    }
}
