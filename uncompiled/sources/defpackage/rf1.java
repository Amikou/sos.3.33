package defpackage;

import android.graphics.Matrix;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: GhostViewUtils.java */
/* renamed from: rf1  reason: default package */
/* loaded from: classes.dex */
public class rf1 {
    public static nf1 a(View view, ViewGroup viewGroup, Matrix matrix) {
        if (Build.VERSION.SDK_INT == 28) {
            return pf1.b(view, viewGroup, matrix);
        }
        return qf1.b(view, viewGroup, matrix);
    }

    public static void b(View view) {
        if (Build.VERSION.SDK_INT == 28) {
            pf1.f(view);
        } else {
            qf1.f(view);
        }
    }
}
