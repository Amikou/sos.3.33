package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ko5  reason: default package */
/* loaded from: classes.dex */
public final class ko5 extends wo5<Boolean> {
    public ko5(ro5 ro5Var, String str, Boolean bool, boolean z) {
        super(ro5Var, str, bool, true, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.wo5
    public final /* bridge */ /* synthetic */ Boolean a(Object obj) {
        if (fm5.c.matcher(obj).matches()) {
            return Boolean.TRUE;
        }
        if (fm5.d.matcher(obj).matches()) {
            return Boolean.FALSE;
        }
        String d = super.d();
        String str = (String) obj;
        StringBuilder sb = new StringBuilder(String.valueOf(d).length() + 28 + str.length());
        sb.append("Invalid boolean value for ");
        sb.append(d);
        sb.append(": ");
        sb.append(str);
        return null;
    }
}
