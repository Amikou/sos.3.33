package defpackage;

import com.google.android.gms.internal.clearcut.y;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: fd5  reason: default package */
/* loaded from: classes.dex */
public final class fd5 extends lc5 {
    public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

    public fd5() {
        super();
    }

    public static <E> List<E> e(Object obj, long j) {
        return (List) y.M(obj, j);
    }

    @Override // defpackage.lc5
    public final void a(Object obj, long j) {
        Object unmodifiableList;
        List list = (List) y.M(obj, j);
        if (list instanceof jc5) {
            unmodifiableList = ((jc5) list).s1();
        } else if (c.isAssignableFrom(list.getClass())) {
            return;
        } else {
            unmodifiableList = Collections.unmodifiableList(list);
        }
        y.i(obj, j, unmodifiableList);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.lc5
    public final <E> void b(Object obj, Object obj2, long j) {
        gc5 gc5Var;
        List e = e(obj2, j);
        int size = e.size();
        List e2 = e(obj, j);
        if (e2.isEmpty()) {
            e2 = e2 instanceof jc5 ? new gc5(size) : new ArrayList(size);
            y.i(obj, j, e2);
        } else {
            if (c.isAssignableFrom(e2.getClass())) {
                ArrayList arrayList = new ArrayList(e2.size() + size);
                arrayList.addAll(e2);
                gc5Var = arrayList;
            } else if (e2 instanceof ki5) {
                gc5 gc5Var2 = new gc5(e2.size() + size);
                gc5Var2.addAll((ki5) e2);
                gc5Var = gc5Var2;
            }
            y.i(obj, j, gc5Var);
            e2 = gc5Var;
        }
        int size2 = e2.size();
        int size3 = e.size();
        if (size2 > 0 && size3 > 0) {
            e2.addAll(e);
        }
        if (size2 > 0) {
            e = e2;
        }
        y.i(obj, j, e);
    }
}
