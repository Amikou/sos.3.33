package defpackage;

import android.animation.PropertyValuesHolder;
import android.animation.TypeConverter;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Build;
import android.util.Property;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: PropertyValuesHolderUtils.java */
/* renamed from: bw2  reason: default package */
/* loaded from: classes.dex */
public class bw2 {
    public static PropertyValuesHolder a(Property<?, PointF> property, Path path) {
        if (Build.VERSION.SDK_INT >= 21) {
            return PropertyValuesHolder.ofObject(property, (TypeConverter) null, path);
        }
        return PropertyValuesHolder.ofFloat(new bq2(property, path), Utils.FLOAT_EPSILON, 1.0f);
    }
}
