package defpackage;

import android.content.Context;
import android.graphics.Color;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ElevationOverlayProvider.java */
/* renamed from: lu0  reason: default package */
/* loaded from: classes2.dex */
public class lu0 {
    public final boolean a;
    public final int b;
    public final int c;
    public final float d;

    public lu0(Context context) {
        this.a = i42.b(context, gy2.elevationOverlayEnabled, false);
        this.b = l42.b(context, gy2.elevationOverlayColor, 0);
        this.c = l42.b(context, gy2.colorSurface, 0);
        this.d = context.getResources().getDisplayMetrics().density;
    }

    public float a(float f) {
        float f2 = this.d;
        return (f2 <= Utils.FLOAT_EPSILON || f <= Utils.FLOAT_EPSILON) ? Utils.FLOAT_EPSILON : Math.min(((((float) Math.log1p(f / f2)) * 4.5f) + 2.0f) / 100.0f, 1.0f);
    }

    public int b(int i, float f) {
        float a = a(f);
        return z20.j(l42.h(z20.j(i, 255), this.b, a), Color.alpha(i));
    }

    public int c(int i, float f) {
        return (this.a && f(i)) ? b(i, f) : i;
    }

    public int d(float f) {
        return c(this.c, f);
    }

    public boolean e() {
        return this.a;
    }

    public final boolean f(int i) {
        return z20.j(i, 255) == this.c;
    }
}
