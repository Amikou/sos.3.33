package defpackage;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;
import java.text.DecimalFormatSymbols;

/* compiled from: DoubleTextChangedListener.kt */
/* renamed from: jq0  reason: default package */
/* loaded from: classes2.dex */
public class jq0 implements TextWatcher {
    public final EditText a;

    public jq0(EditText editText) {
        fs1.f(editText, "et");
        this.a = editText;
        editText.setInputType(8194);
        editText.setKeyListener(DigitsKeyListener.getInstance("0123456789.,"));
        DecimalFormatSymbols.getInstance().getDecimalSeparator();
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        fs1.f(charSequence, "s");
    }
}
