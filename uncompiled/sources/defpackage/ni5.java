package defpackage;

import java.util.ListIterator;

/* renamed from: ni5  reason: default package */
/* loaded from: classes.dex */
public final class ni5 implements ListIterator<String> {
    public ListIterator<String> a;
    public final /* synthetic */ int f0;
    public final /* synthetic */ ki5 g0;

    public ni5(ki5 ki5Var, int i) {
        jc5 jc5Var;
        this.g0 = ki5Var;
        this.f0 = i;
        jc5Var = ki5Var.a;
        this.a = jc5Var.listIterator(i);
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.ListIterator
    public final boolean hasPrevious() {
        return this.a.hasPrevious();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final /* synthetic */ Object next() {
        return this.a.next();
    }

    @Override // java.util.ListIterator
    public final int nextIndex() {
        return this.a.nextIndex();
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ String previous() {
        return this.a.previous();
    }

    @Override // java.util.ListIterator
    public final int previousIndex() {
        return this.a.previousIndex();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }
}
