package defpackage;

import com.facebook.datasource.AbstractDataSource;

/* compiled from: SimpleDataSource.java */
/* renamed from: ap3  reason: default package */
/* loaded from: classes.dex */
public class ap3<T> extends AbstractDataSource<T> {
    public static <T> ap3<T> x() {
        return new ap3<>();
    }

    @Override // com.facebook.datasource.AbstractDataSource
    public boolean p(Throwable th) {
        return super.p((Throwable) xt2.g(th));
    }
}
