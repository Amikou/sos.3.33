package defpackage;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ho5  reason: default package */
/* loaded from: classes.dex */
public final class ho5 implements Runnable {
    public final /* synthetic */ AtomicReference a;
    public final /* synthetic */ dp5 f0;

    public ho5(dp5 dp5Var, AtomicReference atomicReference) {
        this.f0 = dp5Var;
        this.a = atomicReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        synchronized (this.a) {
            this.a.set(Long.valueOf(this.f0.a.z().r(this.f0.a.c().n(), qf5.M)));
            this.a.notify();
        }
    }
}
