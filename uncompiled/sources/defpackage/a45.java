package defpackage;

import android.os.Process;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: a45  reason: default package */
/* loaded from: classes.dex */
public final class a45 implements Runnable {
    public final Runnable a;
    public final int f0 = 0;

    public a45(Runnable runnable, int i) {
        this.a = runnable;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Process.setThreadPriority(this.f0);
        this.a.run();
    }
}
