package defpackage;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: RoomSQLiteQuery.java */
/* renamed from: k93  reason: default package */
/* loaded from: classes.dex */
public class k93 implements vw3, uw3 {
    public static final TreeMap<Integer, k93> m0 = new TreeMap<>();
    public volatile String a;
    public final long[] f0;
    public final double[] g0;
    public final String[] h0;
    public final byte[][] i0;
    public final int[] j0;
    public final int k0;
    public int l0;

    public k93(int i) {
        this.k0 = i;
        int i2 = i + 1;
        this.j0 = new int[i2];
        this.f0 = new long[i2];
        this.g0 = new double[i2];
        this.h0 = new String[i2];
        this.i0 = new byte[i2];
    }

    public static k93 c(String str, int i) {
        TreeMap<Integer, k93> treeMap = m0;
        synchronized (treeMap) {
            Map.Entry<Integer, k93> ceilingEntry = treeMap.ceilingEntry(Integer.valueOf(i));
            if (ceilingEntry != null) {
                treeMap.remove(ceilingEntry.getKey());
                k93 value = ceilingEntry.getValue();
                value.d(str, i);
                return value;
            }
            k93 k93Var = new k93(i);
            k93Var.d(str, i);
            return k93Var;
        }
    }

    public static void e() {
        TreeMap<Integer, k93> treeMap = m0;
        if (treeMap.size() <= 15) {
            return;
        }
        int size = treeMap.size() - 10;
        Iterator<Integer> it = treeMap.descendingKeySet().iterator();
        while (true) {
            int i = size - 1;
            if (size <= 0) {
                return;
            }
            it.next();
            it.remove();
            size = i;
        }
    }

    @Override // defpackage.uw3
    public void A0(int i, byte[] bArr) {
        this.j0[i] = 5;
        this.i0[i] = bArr;
    }

    @Override // defpackage.uw3
    public void L(int i, String str) {
        this.j0[i] = 4;
        this.h0[i] = str;
    }

    @Override // defpackage.uw3
    public void Y(int i, double d) {
        this.j0[i] = 3;
        this.g0[i] = d;
    }

    @Override // defpackage.uw3
    public void Y0(int i) {
        this.j0[i] = 1;
    }

    @Override // defpackage.vw3
    public String a() {
        return this.a;
    }

    @Override // defpackage.vw3
    public void b(uw3 uw3Var) {
        for (int i = 1; i <= this.l0; i++) {
            int i2 = this.j0[i];
            if (i2 == 1) {
                uw3Var.Y0(i);
            } else if (i2 == 2) {
                uw3Var.q0(i, this.f0[i]);
            } else if (i2 == 3) {
                uw3Var.Y(i, this.g0[i]);
            } else if (i2 == 4) {
                uw3Var.L(i, this.h0[i]);
            } else if (i2 == 5) {
                uw3Var.A0(i, this.i0[i]);
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public void d(String str, int i) {
        this.a = str;
        this.l0 = i;
    }

    public void f() {
        TreeMap<Integer, k93> treeMap = m0;
        synchronized (treeMap) {
            treeMap.put(Integer.valueOf(this.k0), this);
            e();
        }
    }

    @Override // defpackage.uw3
    public void q0(int i, long j) {
        this.j0[i] = 2;
        this.f0[i] = j;
    }
}
