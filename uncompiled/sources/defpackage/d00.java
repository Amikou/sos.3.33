package defpackage;

import defpackage.l12;
import kotlinx.coroutines.channels.ClosedReceiveChannelException;
import kotlinx.coroutines.channels.ClosedSendChannelException;

/* compiled from: AbstractChannel.kt */
/* renamed from: d00  reason: default package */
/* loaded from: classes2.dex */
public final class d00<E> extends tj3 implements l43<E> {
    public final Throwable h0;

    public d00(Throwable th) {
        this.h0 = th;
    }

    @Override // defpackage.tj3
    public void A(d00<?> d00Var) {
        if (ze0.a()) {
            throw new AssertionError();
        }
    }

    @Override // defpackage.tj3
    public k24 B(l12.b bVar) {
        return qv.a;
    }

    @Override // defpackage.l43
    /* renamed from: D */
    public d00<E> c() {
        return this;
    }

    @Override // defpackage.tj3
    /* renamed from: E */
    public d00<E> z() {
        return this;
    }

    public final Throwable F() {
        Throwable th = this.h0;
        return th == null ? new ClosedReceiveChannelException("Channel was closed") : th;
    }

    public final Throwable G() {
        Throwable th = this.h0;
        return th == null ? new ClosedSendChannelException("Channel was closed") : th;
    }

    @Override // defpackage.l43
    public void f(E e) {
    }

    @Override // defpackage.l43
    public k24 g(E e, l12.b bVar) {
        return qv.a;
    }

    @Override // defpackage.l12
    public String toString() {
        return "Closed@" + ff0.b(this) + '[' + this.h0 + ']';
    }

    @Override // defpackage.tj3
    public void y() {
    }
}
