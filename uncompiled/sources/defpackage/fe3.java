package defpackage;

import defpackage.pt0;

/* renamed from: fe3  reason: default package */
/* loaded from: classes2.dex */
public class fe3 extends pt0.c {
    public fe3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public fe3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public fe3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        me3 me3Var = (me3) this.c;
        if (me3Var.i()) {
            return i.v();
        }
        me3 me3Var2 = (me3) this.b;
        me3 me3Var3 = (me3) this.d[0];
        int[] d = bd2.d();
        le3.i(me3Var.f, d);
        int[] d2 = bd2.d();
        le3.i(d, d2);
        int[] d3 = bd2.d();
        le3.i(me3Var2.f, d3);
        le3.h(bd2.b(d3, d3, d3), d3);
        le3.d(d, me3Var2.f, d);
        le3.h(kd2.G(5, d, 2, 0), d);
        int[] d4 = bd2.d();
        le3.h(kd2.H(5, d2, 3, 0, d4), d4);
        me3 me3Var4 = new me3(d2);
        le3.i(d3, me3Var4.f);
        int[] iArr = me3Var4.f;
        le3.k(iArr, d, iArr);
        int[] iArr2 = me3Var4.f;
        le3.k(iArr2, d, iArr2);
        me3 me3Var5 = new me3(d);
        le3.k(d, me3Var4.f, me3Var5.f);
        int[] iArr3 = me3Var5.f;
        le3.d(iArr3, d3, iArr3);
        int[] iArr4 = me3Var5.f;
        le3.k(iArr4, d4, iArr4);
        me3 me3Var6 = new me3(d3);
        le3.l(me3Var.f, me3Var6.f);
        if (!me3Var3.h()) {
            int[] iArr5 = me3Var6.f;
            le3.d(iArr5, me3Var3.f, iArr5);
        }
        return new fe3(i, me3Var4, me3Var5, new ct0[]{me3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        me3 me3Var = (me3) this.b;
        me3 me3Var2 = (me3) this.c;
        me3 me3Var3 = (me3) pt0Var.q();
        me3 me3Var4 = (me3) pt0Var.r();
        me3 me3Var5 = (me3) this.d[0];
        me3 me3Var6 = (me3) pt0Var.s(0);
        int[] e = bd2.e();
        int[] d = bd2.d();
        int[] d2 = bd2.d();
        int[] d3 = bd2.d();
        boolean h = me3Var5.h();
        if (h) {
            iArr = me3Var3.f;
            iArr2 = me3Var4.f;
        } else {
            le3.i(me3Var5.f, d2);
            le3.d(d2, me3Var3.f, d);
            le3.d(d2, me3Var5.f, d2);
            le3.d(d2, me3Var4.f, d2);
            iArr = d;
            iArr2 = d2;
        }
        boolean h2 = me3Var6.h();
        if (h2) {
            iArr3 = me3Var.f;
            iArr4 = me3Var2.f;
        } else {
            le3.i(me3Var6.f, d3);
            le3.d(d3, me3Var.f, e);
            le3.d(d3, me3Var6.f, d3);
            le3.d(d3, me3Var2.f, d3);
            iArr3 = e;
            iArr4 = d3;
        }
        int[] d4 = bd2.d();
        le3.k(iArr3, iArr, d4);
        le3.k(iArr4, iArr2, d);
        if (bd2.k(d4)) {
            return bd2.k(d) ? J() : i.v();
        }
        le3.i(d4, d2);
        int[] d5 = bd2.d();
        le3.d(d2, d4, d5);
        le3.d(d2, iArr3, d2);
        le3.f(d5, d5);
        bd2.l(iArr4, d5, e);
        le3.h(bd2.b(d2, d2, d5), d5);
        me3 me3Var7 = new me3(d3);
        le3.i(d, me3Var7.f);
        int[] iArr5 = me3Var7.f;
        le3.k(iArr5, d5, iArr5);
        me3 me3Var8 = new me3(d5);
        le3.k(d2, me3Var7.f, me3Var8.f);
        le3.e(me3Var8.f, d, e);
        le3.g(e, me3Var8.f);
        me3 me3Var9 = new me3(d4);
        if (!h) {
            int[] iArr6 = me3Var9.f;
            le3.d(iArr6, me3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = me3Var9.f;
            le3.d(iArr7, me3Var6.f, iArr7);
        }
        return new fe3(i, me3Var7, me3Var8, new ct0[]{me3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new fe3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new fe3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
