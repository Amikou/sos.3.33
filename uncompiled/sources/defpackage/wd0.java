package defpackage;

/* compiled from: DashSegmentIndex.java */
/* renamed from: wd0  reason: default package */
/* loaded from: classes.dex */
public interface wd0 {
    long b(long j);

    long c(long j, long j2);

    long d(long j, long j2);

    long e(long j, long j2);

    s33 f(long j);

    long g(long j, long j2);

    boolean h();

    long i();

    long j(long j);

    long k(long j, long j2);
}
