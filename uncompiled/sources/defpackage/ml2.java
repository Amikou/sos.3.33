package defpackage;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.web3j.protocol.deserializer.RawResponseDeserializer;

/* compiled from: ObjectMapperFactory.java */
/* renamed from: ml2  reason: default package */
/* loaded from: classes3.dex */
public class ml2 {
    private static final ObjectMapper DEFAULT_OBJECT_MAPPER;

    /* compiled from: ObjectMapperFactory.java */
    /* renamed from: ml2$a */
    /* loaded from: classes3.dex */
    public static class a extends uo {
        @Override // defpackage.uo
        public c<?> modifyDeserializer(DeserializationConfig deserializationConfig, so soVar, c<?> cVar) {
            return i83.class.isAssignableFrom(soVar.r()) ? new RawResponseDeserializer(cVar) : cVar;
        }
    }

    static {
        ObjectMapper objectMapper = new ObjectMapper();
        DEFAULT_OBJECT_MAPPER = objectMapper;
        configureObjectMapper(objectMapper, false);
    }

    private static ObjectMapper configureObjectMapper(ObjectMapper objectMapper, boolean z) {
        if (z) {
            SimpleModule simpleModule = new SimpleModule();
            simpleModule.setDeserializerModifier(new a());
            objectMapper.registerModule(simpleModule);
        }
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    public static ObjectMapper getObjectMapper() {
        return getObjectMapper(false);
    }

    public static ObjectReader getObjectReader() {
        return DEFAULT_OBJECT_MAPPER.reader();
    }

    public static ObjectMapper getObjectMapper(boolean z) {
        if (!z) {
            return DEFAULT_OBJECT_MAPPER;
        }
        return configureObjectMapper(new ObjectMapper(), true);
    }
}
