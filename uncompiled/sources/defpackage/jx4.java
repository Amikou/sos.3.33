package defpackage;

/* renamed from: jx4  reason: default package */
/* loaded from: classes2.dex */
public final class jx4 implements Runnable {
    public final /* synthetic */ l34 a;
    public final /* synthetic */ mx4 f0;

    public jx4(mx4 mx4Var, l34 l34Var) {
        this.f0 = mx4Var;
        this.a = l34Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        tm2 tm2Var;
        tm2 tm2Var2;
        obj = this.f0.b;
        synchronized (obj) {
            tm2Var = this.f0.c;
            if (tm2Var != null) {
                tm2Var2 = this.f0.c;
                tm2Var2.a(this.a.d());
            }
        }
    }
}
