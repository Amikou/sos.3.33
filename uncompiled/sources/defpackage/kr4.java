package defpackage;

import android.content.Context;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: kr4  reason: default package */
/* loaded from: classes.dex */
public class kr4 {
    public static kr4 b = new kr4();
    public to2 a = null;

    @RecentlyNonNull
    public static to2 a(@RecentlyNonNull Context context) {
        return b.b(context);
    }

    public final synchronized to2 b(Context context) {
        if (this.a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.a = new to2(context);
        }
        return this.a;
    }
}
