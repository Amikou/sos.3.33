package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: i33  reason: default package */
/* loaded from: classes2.dex */
public class i33 extends h {
    public g a;
    public i f0;
    public byte[][] g0;
    public byte[] h0;
    public byte[][] i0;
    public byte[] j0;
    public byte[] k0;
    public ny1[] l0;

    public i33(h4 h4Var) {
        int i = 0;
        if (h4Var.D(0) instanceof g) {
            this.a = g.z(h4Var.D(0));
        } else {
            this.f0 = i.G(h4Var.D(0));
        }
        h4 h4Var2 = (h4) h4Var.D(1);
        this.g0 = new byte[h4Var2.size()];
        for (int i2 = 0; i2 < h4Var2.size(); i2++) {
            this.g0[i2] = ((f4) h4Var2.D(i2)).D();
        }
        this.h0 = ((f4) ((h4) h4Var.D(2)).D(0)).D();
        h4 h4Var3 = (h4) h4Var.D(3);
        this.i0 = new byte[h4Var3.size()];
        for (int i3 = 0; i3 < h4Var3.size(); i3++) {
            this.i0[i3] = ((f4) h4Var3.D(i3)).D();
        }
        this.j0 = ((f4) ((h4) h4Var.D(4)).D(0)).D();
        this.k0 = ((f4) ((h4) h4Var.D(5)).D(0)).D();
        h4 h4Var4 = (h4) h4Var.D(6);
        byte[][][][] bArr = new byte[h4Var4.size()][][];
        byte[][][][] bArr2 = new byte[h4Var4.size()][][];
        byte[][][] bArr3 = new byte[h4Var4.size()][];
        byte[][] bArr4 = new byte[h4Var4.size()];
        int i4 = 0;
        while (i4 < h4Var4.size()) {
            h4 h4Var5 = (h4) h4Var4.D(i4);
            h4 h4Var6 = (h4) h4Var5.D(i);
            bArr[i4] = new byte[h4Var6.size()][];
            for (int i5 = i; i5 < h4Var6.size(); i5++) {
                h4 h4Var7 = (h4) h4Var6.D(i5);
                bArr[i4][i5] = new byte[h4Var7.size()];
                for (int i6 = 0; i6 < h4Var7.size(); i6++) {
                    bArr[i4][i5][i6] = ((f4) h4Var7.D(i6)).D();
                }
            }
            h4 h4Var8 = (h4) h4Var5.D(1);
            bArr2[i4] = new byte[h4Var8.size()][];
            for (int i7 = 0; i7 < h4Var8.size(); i7++) {
                h4 h4Var9 = (h4) h4Var8.D(i7);
                bArr2[i4][i7] = new byte[h4Var9.size()];
                for (int i8 = 0; i8 < h4Var9.size(); i8++) {
                    bArr2[i4][i7][i8] = ((f4) h4Var9.D(i8)).D();
                }
            }
            h4 h4Var10 = (h4) h4Var5.D(2);
            bArr3[i4] = new byte[h4Var10.size()];
            for (int i9 = 0; i9 < h4Var10.size(); i9++) {
                bArr3[i4][i9] = ((f4) h4Var10.D(i9)).D();
            }
            bArr4[i4] = ((f4) h4Var5.D(3)).D();
            i4++;
            i = 0;
        }
        int length = this.k0.length - 1;
        this.l0 = new ny1[length];
        int i10 = 0;
        while (i10 < length) {
            byte[] bArr5 = this.k0;
            int i11 = i10 + 1;
            this.l0[i10] = new ny1(bArr5[i10], bArr5[i11], o33.f(bArr[i10]), o33.f(bArr2[i10]), o33.d(bArr3[i10]), o33.b(bArr4[i10]));
            i10 = i11;
        }
    }

    public i33(short[][] sArr, short[] sArr2, short[][] sArr3, short[] sArr4, int[] iArr, ny1[] ny1VarArr) {
        this.a = new g(1L);
        this.g0 = o33.c(sArr);
        this.h0 = o33.a(sArr2);
        this.i0 = o33.c(sArr3);
        this.j0 = o33.a(sArr4);
        this.k0 = o33.h(iArr);
        this.l0 = ny1VarArr;
    }

    public static i33 q(Object obj) {
        if (obj instanceof i33) {
            return (i33) obj;
        }
        if (obj != null) {
            return new i33(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        c4 c4Var = this.a;
        if (c4Var == null) {
            c4Var = this.f0;
        }
        d4Var.a(c4Var);
        d4 d4Var2 = new d4();
        int i = 0;
        while (true) {
            byte[][] bArr = this.g0;
            if (i >= bArr.length) {
                break;
            }
            d4Var2.a(new j0(bArr[i]));
            i++;
        }
        d4Var.a(new n0(d4Var2));
        d4 d4Var3 = new d4();
        d4Var3.a(new j0(this.h0));
        d4Var.a(new n0(d4Var3));
        d4 d4Var4 = new d4();
        int i2 = 0;
        while (true) {
            byte[][] bArr2 = this.i0;
            if (i2 >= bArr2.length) {
                break;
            }
            d4Var4.a(new j0(bArr2[i2]));
            i2++;
        }
        d4Var.a(new n0(d4Var4));
        d4 d4Var5 = new d4();
        d4Var5.a(new j0(this.j0));
        d4Var.a(new n0(d4Var5));
        d4 d4Var6 = new d4();
        d4Var6.a(new j0(this.k0));
        d4Var.a(new n0(d4Var6));
        d4 d4Var7 = new d4();
        for (int i3 = 0; i3 < this.l0.length; i3++) {
            d4 d4Var8 = new d4();
            byte[][][] e = o33.e(this.l0[i3].a());
            d4 d4Var9 = new d4();
            for (int i4 = 0; i4 < e.length; i4++) {
                d4 d4Var10 = new d4();
                for (int i5 = 0; i5 < e[i4].length; i5++) {
                    d4Var10.a(new j0(e[i4][i5]));
                }
                d4Var9.a(new n0(d4Var10));
            }
            d4Var8.a(new n0(d4Var9));
            byte[][][] e2 = o33.e(this.l0[i3].b());
            d4 d4Var11 = new d4();
            for (int i6 = 0; i6 < e2.length; i6++) {
                d4 d4Var12 = new d4();
                for (int i7 = 0; i7 < e2[i6].length; i7++) {
                    d4Var12.a(new j0(e2[i6][i7]));
                }
                d4Var11.a(new n0(d4Var12));
            }
            d4Var8.a(new n0(d4Var11));
            byte[][] c = o33.c(this.l0[i3].d());
            d4 d4Var13 = new d4();
            for (byte[] bArr3 : c) {
                d4Var13.a(new j0(bArr3));
            }
            d4Var8.a(new n0(d4Var13));
            d4Var8.a(new j0(o33.a(this.l0[i3].c())));
            d4Var7.a(new n0(d4Var8));
        }
        d4Var.a(new n0(d4Var7));
        return new n0(d4Var);
    }

    public short[] o() {
        return o33.b(this.h0);
    }

    public short[] p() {
        return o33.b(this.j0);
    }

    public short[][] s() {
        return o33.d(this.g0);
    }

    public short[][] t() {
        return o33.d(this.i0);
    }

    public ny1[] w() {
        return this.l0;
    }

    public int[] y() {
        return o33.g(this.k0);
    }
}
