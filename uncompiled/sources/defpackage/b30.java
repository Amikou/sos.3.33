package defpackage;

import android.content.Context;
import android.net.Uri;
import android.text.format.DateFormat;
import com.AKT.anonymouskey.ui.login.AKTServerFunctions;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.onesignal.OneSignal;
import defpackage.rc0;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Pattern;
import kotlin.NoWhenBranchMatchedException;
import kotlin.text.StringsKt__StringsKt;
import kotlin.text.StringsKt___StringsKt;
import net.safemoon.androidwallet.MyApplicationClass;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.swap.Swap;
import org.web3j.abi.datatypes.Address;

/* compiled from: Common.kt */
/* renamed from: b30 */
/* loaded from: classes2.dex */
public final class b30 {
    public static final b30 a = new b30();
    public static final SimpleDateFormat b;
    public static final SimpleDateFormat c;
    public static final List<Swap> d;
    public static final List<String> e;

    /* compiled from: Common.kt */
    /* renamed from: b30$a */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TokenType.values().length];
            iArr[TokenType.BEP_20.ordinal()] = 1;
            iArr[TokenType.BEP_20_TEST.ordinal()] = 2;
            iArr[TokenType.ERC_20.ordinal()] = 3;
            iArr[TokenType.ERC_20_TEST.ordinal()] = 4;
            iArr[TokenType.POLYGON.ordinal()] = 5;
            iArr[TokenType.POLYGON_TEST.ordinal()] = 6;
            iArr[TokenType.AVALANCHE_C.ordinal()] = 7;
            iArr[TokenType.AVALANCHE_FUJI_TEST.ordinal()] = 8;
            a = iArr;
        }
    }

    static {
        Locale locale = Locale.US;
        b = new SimpleDateFormat(StdDateFormat.DATE_FORMAT_STR_ISO8601, locale);
        c = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", locale);
        Swap swap = new Swap();
        swap.address = "";
        swap.chainId = 56;
        swap.decimals = 18;
        swap.name = "Smart Chain";
        swap.symbol = "BNB";
        swap.logoURI = "";
        swap.imageResource = R.drawable.binance;
        swap.cmcId = "1839";
        swap.cmcSlug = "bnb";
        te4 te4Var = te4.a;
        Swap swap2 = new Swap();
        swap2.address = "";
        swap2.chainId = 1;
        swap2.decimals = 18;
        swap2.name = "Ethereum";
        swap2.symbol = "ETH";
        swap2.logoURI = "";
        swap2.imageResource = R.drawable.ethereum;
        swap2.cmcId = "1027";
        swap2.cmcSlug = "ethereum";
        d = b20.j(swap, swap2);
        e = a20.b("BEP_SAFEMOON");
    }

    public static /* synthetic */ String g(b30 b30Var, Context context, String str, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        return b30Var.f(context, str);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:68:0x004f A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void A(net.safemoon.androidwallet.model.swap.Swap r6) {
        /*
            r5 = this;
            if (r6 != 0) goto L3
            goto L5f
        L3:
            net.safemoon.androidwallet.MyApplicationClass r0 = net.safemoon.androidwallet.MyApplicationClass.c()
            java.lang.String r1 = r6.address
            r0.o0 = r1
            java.util.List<net.safemoon.androidwallet.model.swap.PairsData> r6 = r6.pairs
            r0 = 0
            if (r6 != 0) goto L11
            goto L52
        L11:
            java.util.Iterator r6 = r6.iterator()
        L15:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L50
            java.lang.Object r1 = r6.next()
            r2 = r1
            net.safemoon.androidwallet.model.swap.PairsData r2 = (net.safemoon.androidwallet.model.swap.PairsData) r2
            net.safemoon.androidwallet.model.swap.Token r3 = r2.getToken0()
            if (r3 != 0) goto L2a
            r3 = r0
            goto L2e
        L2a:
            java.lang.String r3 = r3.getSymbol()
        L2e:
            java.lang.String r4 = "USDC"
            boolean r3 = defpackage.fs1.b(r3, r4)
            if (r3 == 0) goto L4c
            net.safemoon.androidwallet.model.swap.Token r2 = r2.getToken1()
            if (r2 != 0) goto L3e
            r2 = r0
            goto L42
        L3e:
            java.lang.String r2 = r2.getSymbol()
        L42:
            java.lang.String r3 = "WETH"
            boolean r2 = defpackage.fs1.b(r2, r3)
            if (r2 == 0) goto L4c
            r2 = 1
            goto L4d
        L4c:
            r2 = 0
        L4d:
            if (r2 == 0) goto L15
            r0 = r1
        L50:
            net.safemoon.androidwallet.model.swap.PairsData r0 = (net.safemoon.androidwallet.model.swap.PairsData) r0
        L52:
            if (r0 != 0) goto L55
            goto L5f
        L55:
            net.safemoon.androidwallet.MyApplicationClass r6 = net.safemoon.androidwallet.MyApplicationClass.c()
            java.lang.String r0 = r0.getPairAddress()
            r6.n0 = r0
        L5f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b30.A(net.safemoon.androidwallet.model.swap.Swap):void");
    }

    public final List<TokenType> a() {
        TokenType[] values = TokenType.values();
        ArrayList arrayList = new ArrayList();
        for (TokenType tokenType : values) {
            if (fs1.b(Boolean.valueOf(tokenType.isTestNet()), zr.b) && tokenType.isEnable()) {
                arrayList.add(tokenType);
            }
        }
        return arrayList;
    }

    public final String b(String str) {
        fs1.f(str, "<this>");
        String replaceAll = Pattern.compile("[\\x{10000}-\\x{10FFFF}]", 66).matcher(str).replaceAll("");
        fs1.e(replaceAll, "emojiSecondClear");
        return replaceAll;
    }

    public final void c(Context context) {
        fs1.f(context, "context");
        String d2 = jv0.d(context, "API_KEY", "");
        fs1.e(d2, "apiKey");
        if (d2.length() == 0) {
            return;
        }
        dz4 dz4Var = new dz4();
        dz4Var.t(ay1.f(q.i(d2)));
        TokenType.a aVar = TokenType.Companion;
        String b2 = dz4Var.b("e3fYo7c6GT[SMRu2rzSxCu2WLLcwORIjH[lu[jXUwAVeUMTRC.XHtfVIT1yPXtRh9Q[5XIDRD5YO8nL.IKDN7g]]");
        fs1.e(b2, "apiCm.DecryptText(Consts…ApiKeys.explorerKeyBep20)");
        aVar.e(b2);
        String b3 = dz4Var.b("8t9oReQC59Xlc4hCFIsipRuP3IXvHaK8GtOLddNlkcLRvEJ7iL2[gWcoLZRsuteN8Ry70.RSohaTcyY.yG.CQQ]]");
        fs1.e(b3, "apiCm.DecryptText(Consts…ApiKeys.explorerKeyErc20)");
        aVar.f(b3);
        String b4 = dz4Var.b("1DJ1iFkcATP0Tztt6lMnnpv4.g7iv6F3mwoPezC6M4QbfTuPgG8W5O0TA2F9qY4CGbKJuVwPUU7UBQS07.Ai6w]]");
        fs1.e(b4, "apiCm.DecryptText(Consts…ApiKeys.explorerKeyMatic)");
        aVar.g(b4);
        String b5 = dz4Var.b("7QOOwL74FFM8TeCgxgOb8FwcNNF95Y5xxE246HYMw.Y4wTfUrLWnu7Z4OxUaeM[c4dDXeNg[TsFnKAeDqByqZA]]");
        fs1.e(b5, "apiCm.DecryptText(Consts…ys.explorerKeyAvalancheC)");
        aVar.d(b5);
        a4.a = dz4Var.b("FOX.FyH.dJ0Go49tTZUDr8Q.a4QqXu8.o3XE86UL8Xht4uIFDES07hod[xzGEpGOXGK1dyD7BTuaEK28OQYFXA]]");
        a4.b = dz4Var.b("fMCLo.oLrNj8mBvjkSB9CXmLRPdG3opGK9orJmfVUfFQ5K3kqiCv1Y[gHTjZjtxR3DaNrGHKzmJWVy3gqDa3ml1OZJ0u1IZo8r7y.eH24tjhenDgAsFuJwUr7jqNQ2Nm");
        a4.d = dz4Var.b("vkD.hvXhdxXb.7ADB1K..XIGZcoYJDvUW[kYobY6xKp.nWoOsKtGFWQFImg5ASuSXPS2P4oX5TDuqGAf57NTMA]]");
        a4.c = dz4Var.b("z0NYeEAKccTrcaK3oxpw7c0Amr7VDS5.W.mzH[Y[zRpu182x106aTC9VLiu4Qld5.MlWd1iCgi3ZVnzcp[AAfj3[Cw0hUngYJW.9WDuBvPg]");
        a4.e = dz4Var.b("6LNE5OGvnPt4j7YCNdxU7RGNqt6MBICyxq2jW9n[hiIsW2IvK4xMfxz1tpX.0XBTmPemx8vELLsg0ec.jEftbg]]");
        a4.f = dz4Var.b("adV9nO5I9KjECc1TIAtICHeiqNK6sq8UMzZX7nUoEk[xrmU[zQZP4cwc1DUQ482.ppOFzYYfAMMmlKl0Wa7t59[DSte5AFGKxIL9EFROMFE]");
        a4.g = dz4Var.b("kWxvrKN.YTSdXIEJCoVC.IWAE2tNiwuH[7hTu[rGgBeNZKtUEUsD1HIO0nOG6XyC9oJjUWcy.vUqyOxwH8XAXA]]");
        OneSignal.B1(dz4Var.b("8TtBFdsIUpbVC[WNy5ZmyAn.RgS5ZtNvZHZ7INDicL77KyC32sYd33.P.fz2KtTETXn037jWf5Rgw[[iqszCZQ]]"));
    }

    public final String d(Date date, Context context) {
        fs1.f(date, "value");
        fs1.f(context, "context");
        String format = new SimpleDateFormat(pe0.a.c(context) + ' ' + context.getString(R.string.common_format_read_string_extend), Locale.getDefault()).format(date);
        fs1.e(format, "SimpleDateFormat(pattern…tDefault()).format(value)");
        return format;
    }

    public final String e(Date date, Context context) {
        fs1.f(date, "value");
        fs1.f(context, "context");
        String format = new SimpleDateFormat(pe0.a.c(context), Locale.getDefault()).format(date);
        fs1.e(format, "SimpleDateFormat(getDefa…tDefault()).format(value)");
        return format;
    }

    public final String f(Context context, String str) {
        fs1.f(context, "context");
        if (str == null) {
            str = do3.a.a(context);
        }
        switch (str.hashCode()) {
            case -1295765506:
                if (str.equals("es-rMX")) {
                    return "mx";
                }
                break;
            case -979921671:
                if (str.equals("pt-rBR")) {
                    return "pt-BR";
                }
                break;
            case -979921235:
                if (str.equals("pt-rPT")) {
                    return "pt";
                }
                break;
            case -704712234:
                if (str.equals("zh-rHK")) {
                    return "zh-hk";
                }
                break;
            case 3201:
                if (str.equals("de")) {
                    return "de";
                }
                break;
            case 3241:
                str.equals("en");
                break;
            case 3246:
                if (str.equals("es")) {
                    return "es";
                }
                break;
            case 3259:
                if (str.equals("fa")) {
                    return "fa";
                }
                break;
            case 3276:
                if (str.equals("fr")) {
                    return "fr";
                }
                break;
            case 3329:
                if (str.equals("hi")) {
                    return "hi";
                }
                break;
            case 3365:
                if (str.equals("in")) {
                    return "id";
                }
                break;
            case 3371:
                if (str.equals("it")) {
                    return "it";
                }
                break;
            case 3518:
                if (str.equals("nl")) {
                    return "nl";
                }
                break;
            case 3580:
                if (str.equals("pl")) {
                    return "pl";
                }
                break;
            case 3710:
                if (str.equals("tr")) {
                    return "tr";
                }
                break;
            case 3763:
                if (str.equals("vi")) {
                    return "vi";
                }
                break;
            case 3886:
                if (str.equals("zh")) {
                    return "zh-cn";
                }
                break;
        }
        return "en";
    }

    public final String h() {
        SimpleDateFormat simpleDateFormat = b;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance();
        calendar.add(12, -1);
        String format = simpleDateFormat.format(calendar.getTime());
        fs1.e(format, "sdf.format(calendar.time)");
        return format;
    }

    public final List<Date> i(Date date, Date date2) {
        fs1.f(date, "startDate");
        fs1.f(date2, "endDate");
        ArrayList arrayList = new ArrayList();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        while (gregorianCalendar.getTime().compareTo(t(date2)) <= 0) {
            Date time = gregorianCalendar.getTime();
            if (!fs1.b(date, time)) {
                fs1.e(time, "result");
                arrayList.add(t(time));
            }
            gregorianCalendar.add(5, 1);
        }
        if (arrayList.size() > 0 && !fs1.b(date2, j20.U(arrayList))) {
            arrayList.add(t(date2));
        }
        return arrayList;
    }

    public final String j(long j) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(j);
        Calendar calendar2 = Calendar.getInstance();
        if (calendar2.get(5) == calendar.get(5)) {
            return fs1.l("Today at ", DateFormat.format("h:mm aa", calendar));
        }
        if (calendar2.get(5) - calendar.get(5) == 1) {
            return fs1.l("Yesterday at ", DateFormat.format("h:mm aa", calendar));
        }
        if (calendar2.get(1) == calendar.get(1)) {
            return DateFormat.format("EEEE, MMMM d, h:mm aa", calendar).toString();
        }
        return DateFormat.format("MMMM dd yyyy, h:mm aa", calendar).toString();
    }

    public final BigDecimal k(BigDecimal bigDecimal) {
        fs1.f(bigDecimal, "<this>");
        return bigDecimal.subtract(bigDecimal.setScale(0, RoundingMode.FLOOR)).movePointRight(bigDecimal.scale());
    }

    public final List<Swap> l() {
        return d;
    }

    public final String m(String str) {
        fs1.f(str, Address.TYPE_NAME);
        Locale locale = Locale.ROOT;
        String lowerCase = str.toLowerCase(locale);
        fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        String str2 = MyApplicationClass.c().o0;
        fs1.e(str2, "get().wETHTokenAddress");
        String lowerCase2 = str2.toLowerCase(locale);
        fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        if (fs1.b(lowerCase, lowerCase2)) {
            return MyApplicationClass.c().n0;
        }
        return null;
    }

    public final String n(String str) {
        fs1.f(str, "<this>");
        Charset charset = StandardCharsets.UTF_8;
        fs1.e(charset, "UTF_8");
        byte[] bytes = str.getBytes(charset);
        fs1.e(bytes, "(this as java.lang.String).getBytes(charset)");
        String f = ay1.f(bytes);
        fs1.e(f, "GetStringFromBytes(asByte)");
        return f;
    }

    public final String o(TokenType tokenType) {
        fs1.f(tokenType, "myType");
        switch (a.a[tokenType.ordinal()]) {
            case 1:
                return "https://bscscan.com/tx/";
            case 2:
                return "https://testnet.bscscan.com/tx/";
            case 3:
                return "https://etherscan.io/tx/";
            case 4:
                return "https://rinkeby.etherscan.io/tx/";
            case 5:
                return "https://polygonscan.com/tx/";
            case 6:
                return "https://mumbai.polygonscan.com/tx/";
            case 7:
                return "https://snowtrace.io/tx/";
            case 8:
                return "https://testnet.explorer.avax.network/tx/";
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public final long p() {
        return System.currentTimeMillis() / 1000;
    }

    public final String q(String str) {
        String str2;
        fs1.f(str, "qrScanCode");
        String D = dv3.D(str, MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "", false, 4, null);
        if (StringsKt__StringsKt.M(str, "ethereum:", false, 2, null) && StringsKt__StringsKt.M(str, "@1", false, 2, null)) {
            str2 = dv3.D(dv3.D(D, "ethereum:", "", false, 4, null), "@1", "", false, 4, null);
        } else if (StringsKt__StringsKt.M(str, "ethereum:", false, 2, null) && StringsKt__StringsKt.M(str, "/transfer?address=", false, 2, null)) {
            str2 = dv3.D(D, "ethereum:", "", false, 4, null);
            List w0 = StringsKt__StringsKt.w0(str2, new String[]{"/transfer?address="}, false, 0, 6, null);
            if (w0.size() == 1) {
                str2 = (String) w0.get(0);
            } else if (w0.size() > 1) {
                str2 = (String) j20.U(w0);
            }
        } else {
            String D2 = dv3.D(D, "ethereum:", "", false, 4, null);
            str2 = StringsKt__StringsKt.M(D2, "@", false, 2, null) ? (String) StringsKt__StringsKt.w0(D2, new String[]{"@"}, false, 0, 6, null).get(0) : D2;
        }
        return StringsKt___StringsKt.P0(str2, 42);
    }

    public final char r() {
        return DecimalFormatSymbols.getInstance().getGroupingSeparator();
    }

    public final boolean s(String str) {
        Object obj;
        fs1.f(str, "symbolWithType");
        Iterator<T> it = e.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (fs1.b((String) obj, str)) {
                break;
            }
        }
        return obj != null || dv3.H(str, "CUSTOM_", false, 2, null);
    }

    public final Date t(Date date) {
        fs1.f(date, "date");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 1);
        calendar.set(14, 0);
        Date time = calendar.getTime();
        fs1.e(time, "getInstance().apply {\n  …ECOND, 0);\n        }.time");
        return time;
    }

    public final void u(Context context, String str) {
        fs1.f(context, "<this>");
        fs1.f(str, "url");
        rc0 a2 = new rc0.a().a();
        fs1.e(a2, "builder.build()");
        a2.a(context, Uri.parse(str));
    }

    public final String v(Context context, String str) {
        String str2;
        fs1.f(context, "context");
        if (str == null) {
            str = "";
        }
        if (dv3.F(str, "LSON", false)) {
            List w0 = StringsKt__StringsKt.w0(str, new String[]{"="}, false, 0, 6, null);
            if (w0.size() > 1) {
                str = AKTServerFunctions.a0(context, (String) w0.get(1));
                fs1.e(str, "receiveIceBerg(context, firstItem)");
            }
        }
        int d0 = StringsKt__StringsKt.d0(str, '|', 0, false, 6, null);
        int length = str.length();
        if (d0 > length - 3) {
            str2 = str.substring(0, length - 1);
            fs1.e(str2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        } else {
            str2 = str;
        }
        int Y = StringsKt__StringsKt.Y(str2, '|', 0, false, 6, null);
        if (Y < 12) {
            Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
            String substring = str2.substring(Y + 1);
            fs1.e(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        return str;
    }

    public final Date w(String str) {
        fs1.f(str, "value");
        try {
            return c.parse(str);
        } catch (Exception unused) {
            return null;
        }
    }

    public final Date x(String str) {
        fs1.f(str, "value");
        try {
            return b.parse(str);
        } catch (Exception unused) {
            return null;
        }
    }

    public final char y() {
        return DecimalFormatSymbols.getInstance().getDecimalSeparator();
    }

    public final long z(Date date) {
        fs1.f(date, "date");
        return date.getTime() / 1000;
    }
}
