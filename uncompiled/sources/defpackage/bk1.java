package defpackage;

import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: bk1  reason: default package */
/* loaded from: classes.dex */
public class bk1 {
    public static int b = 31;
    public int a = 1;

    @RecentlyNonNull
    public bk1 a(Object obj) {
        this.a = (b * this.a) + (obj == null ? 0 : obj.hashCode());
        return this;
    }

    @RecentlyNonNull
    public int b() {
        return this.a;
    }

    @RecentlyNonNull
    public final bk1 c(@RecentlyNonNull boolean z) {
        this.a = (b * this.a) + (z ? 1 : 0);
        return this;
    }
}
