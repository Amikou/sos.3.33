package defpackage;

import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: q75  reason: default package */
/* loaded from: classes.dex */
public final class q75 {
    public static boolean a() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
