package defpackage;

import androidx.media3.common.PlaybackException;

/* compiled from: BufferRecycler.java */
/* renamed from: wr  reason: default package */
/* loaded from: classes.dex */
public class wr {
    public static final int[] c = {8000, 8000, PlaybackException.ERROR_CODE_IO_UNSPECIFIED, PlaybackException.ERROR_CODE_IO_UNSPECIFIED};
    public static final int[] d = {4000, 4000, 200, 200};
    public final byte[][] a;
    public final char[][] b;

    public wr() {
        this(4, 4);
    }

    public final byte[] a(int i) {
        return b(i, 0);
    }

    public byte[] b(int i, int i2) {
        int f = f(i);
        if (i2 < f) {
            i2 = f;
        }
        byte[][] bArr = this.a;
        byte[] bArr2 = bArr[i];
        if (bArr2 != null && bArr2.length >= i2) {
            bArr[i] = null;
            return bArr2;
        }
        return e(i2);
    }

    public final char[] c(int i) {
        return d(i, 0);
    }

    public char[] d(int i, int i2) {
        int h = h(i);
        if (i2 < h) {
            i2 = h;
        }
        char[][] cArr = this.b;
        char[] cArr2 = cArr[i];
        if (cArr2 != null && cArr2.length >= i2) {
            cArr[i] = null;
            return cArr2;
        }
        return g(i2);
    }

    public byte[] e(int i) {
        return new byte[i];
    }

    public int f(int i) {
        return c[i];
    }

    public char[] g(int i) {
        return new char[i];
    }

    public int h(int i) {
        return d[i];
    }

    public final void i(int i, byte[] bArr) {
        this.a[i] = bArr;
    }

    public void j(int i, char[] cArr) {
        this.b[i] = cArr;
    }

    public wr(int i, int i2) {
        this.a = new byte[i];
        this.b = new char[i2];
    }
}
