package defpackage;

import java.util.Iterator;
import java.util.Set;

/* compiled from: DefaultUserAgentPublisher.java */
/* renamed from: ol0  reason: default package */
/* loaded from: classes2.dex */
public class ol0 implements wf4 {
    public final String a;
    public final og1 b;

    public ol0(Set<hz1> set, og1 og1Var) {
        this.a = e(set);
        this.b = og1Var;
    }

    public static a40<wf4> c() {
        return a40.c(wf4.class).b(hm0.k(hz1.class)).f(nl0.a).d();
    }

    public static /* synthetic */ wf4 d(b40 b40Var) {
        return new ol0(b40Var.d(hz1.class), og1.a());
    }

    public static String e(Set<hz1> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<hz1> it = set.iterator();
        while (it.hasNext()) {
            hz1 next = it.next();
            sb.append(next.b());
            sb.append('/');
            sb.append(next.c());
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    @Override // defpackage.wf4
    public String a() {
        if (this.b.b().isEmpty()) {
            return this.a;
        }
        return this.a + ' ' + e(this.b.b());
    }
}
