package defpackage;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.g;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;
import java.util.List;

/* compiled from: BeanDeserializerModifier.java */
/* renamed from: uo  reason: default package */
/* loaded from: classes.dex */
public abstract class uo {
    public c<?> modifyArrayDeserializer(DeserializationConfig deserializationConfig, ArrayType arrayType, so soVar, c<?> cVar) {
        return cVar;
    }

    public c<?> modifyCollectionDeserializer(DeserializationConfig deserializationConfig, CollectionType collectionType, so soVar, c<?> cVar) {
        return cVar;
    }

    public c<?> modifyCollectionLikeDeserializer(DeserializationConfig deserializationConfig, CollectionLikeType collectionLikeType, so soVar, c<?> cVar) {
        return cVar;
    }

    public c<?> modifyDeserializer(DeserializationConfig deserializationConfig, so soVar, c<?> cVar) {
        return cVar;
    }

    public c<?> modifyEnumDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, so soVar, c<?> cVar) {
        return cVar;
    }

    public g modifyKeyDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, g gVar) {
        return gVar;
    }

    public c<?> modifyMapDeserializer(DeserializationConfig deserializationConfig, MapType mapType, so soVar, c<?> cVar) {
        return cVar;
    }

    public c<?> modifyMapLikeDeserializer(DeserializationConfig deserializationConfig, MapLikeType mapLikeType, so soVar, c<?> cVar) {
        return cVar;
    }

    public c<?> modifyReferenceDeserializer(DeserializationConfig deserializationConfig, ReferenceType referenceType, so soVar, c<?> cVar) {
        return cVar;
    }

    public to updateBuilder(DeserializationConfig deserializationConfig, so soVar, to toVar) {
        return toVar;
    }

    public List<vo> updateProperties(DeserializationConfig deserializationConfig, so soVar, List<vo> list) {
        return list;
    }
}
