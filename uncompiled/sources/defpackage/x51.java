package defpackage;

import com.google.firebase.messaging.FirebaseMessagingRegistrar;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: x51  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class x51 implements e40 {
    public static final e40 a = new x51();

    @Override // defpackage.e40
    public Object a(b40 b40Var) {
        return FirebaseMessagingRegistrar.lambda$getComponents$0$FirebaseMessagingRegistrar(b40Var);
    }
}
