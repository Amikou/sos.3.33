package defpackage;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResult;

/* compiled from: ActivityResultContracts.java */
/* renamed from: v7  reason: default package */
/* loaded from: classes.dex */
public final class v7 extends s7<Intent, ActivityResult> {
    @Override // defpackage.s7
    /* renamed from: d */
    public Intent a(Context context, Intent intent) {
        return intent;
    }

    @Override // defpackage.s7
    /* renamed from: e */
    public ActivityResult c(int i, Intent intent) {
        return new ActivityResult(i, intent);
    }
}
