package defpackage;

import android.graphics.Canvas;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: ItemTouchUIUtil.java */
/* renamed from: it1  reason: default package */
/* loaded from: classes.dex */
public interface it1 {
    void a(View view);

    void b(View view);

    void c(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z);

    void d(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z);
}
