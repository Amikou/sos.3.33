package defpackage;

import androidx.media3.extractor.text.SubtitleDecoderException;
import defpackage.if0;
import defpackage.sw;
import java.util.ArrayDeque;
import java.util.PriorityQueue;

/* compiled from: CeaDecoder.java */
/* renamed from: sw  reason: default package */
/* loaded from: classes.dex */
public abstract class sw implements rv3 {
    public final ArrayDeque<b> a = new ArrayDeque<>();
    public final ArrayDeque<uv3> b;
    public final PriorityQueue<b> c;
    public b d;
    public long e;
    public long f;

    /* compiled from: CeaDecoder.java */
    /* renamed from: sw$b */
    /* loaded from: classes.dex */
    public static final class b extends tv3 implements Comparable<b> {
        public long n0;

        public b() {
        }

        @Override // java.lang.Comparable
        /* renamed from: D */
        public int compareTo(b bVar) {
            if (p() != bVar.p()) {
                return p() ? 1 : -1;
            }
            long j = this.i0 - bVar.i0;
            if (j == 0) {
                j = this.n0 - bVar.n0;
                if (j == 0) {
                    return 0;
                }
            }
            return j > 0 ? 1 : -1;
        }
    }

    /* compiled from: CeaDecoder.java */
    /* renamed from: sw$c */
    /* loaded from: classes.dex */
    public static final class c extends uv3 {
        public if0.a<c> i0;

        public c(if0.a<c> aVar) {
            this.i0 = aVar;
        }

        @Override // defpackage.if0
        public final void u() {
            this.i0.a(this);
        }
    }

    public sw() {
        for (int i = 0; i < 10; i++) {
            this.a.add(new b());
        }
        this.b = new ArrayDeque<>();
        for (int i2 = 0; i2 < 2; i2++) {
            this.b.add(new c(new if0.a() { // from class: rw
                @Override // defpackage.if0.a
                public final void a(if0 if0Var) {
                    sw.this.o((sw.c) if0Var);
                }
            }));
        }
        this.c = new PriorityQueue<>();
    }

    @Override // androidx.media3.decoder.a
    public void a() {
    }

    @Override // defpackage.rv3
    public void b(long j) {
        this.e = j;
    }

    public abstract qv3 f();

    @Override // androidx.media3.decoder.a
    public void flush() {
        this.f = 0L;
        this.e = 0L;
        while (!this.c.isEmpty()) {
            n((b) androidx.media3.common.util.b.j(this.c.poll()));
        }
        b bVar = this.d;
        if (bVar != null) {
            n(bVar);
            this.d = null;
        }
    }

    public abstract void g(tv3 tv3Var);

    @Override // androidx.media3.decoder.a
    /* renamed from: h */
    public tv3 d() throws SubtitleDecoderException {
        ii.g(this.d == null);
        if (this.a.isEmpty()) {
            return null;
        }
        b pollFirst = this.a.pollFirst();
        this.d = pollFirst;
        return pollFirst;
    }

    @Override // androidx.media3.decoder.a
    /* renamed from: i */
    public uv3 c() throws SubtitleDecoderException {
        if (this.b.isEmpty()) {
            return null;
        }
        while (!this.c.isEmpty() && ((b) androidx.media3.common.util.b.j(this.c.peek())).i0 <= this.e) {
            b bVar = (b) androidx.media3.common.util.b.j(this.c.poll());
            if (bVar.p()) {
                uv3 uv3Var = (uv3) androidx.media3.common.util.b.j(this.b.pollFirst());
                uv3Var.g(4);
                n(bVar);
                return uv3Var;
            }
            g(bVar);
            if (l()) {
                qv3 f = f();
                uv3 uv3Var2 = (uv3) androidx.media3.common.util.b.j(this.b.pollFirst());
                uv3Var2.v(bVar.i0, f, Long.MAX_VALUE);
                n(bVar);
                return uv3Var2;
            }
            n(bVar);
        }
        return null;
    }

    public final uv3 j() {
        return this.b.pollFirst();
    }

    public final long k() {
        return this.e;
    }

    public abstract boolean l();

    @Override // androidx.media3.decoder.a
    /* renamed from: m */
    public void e(tv3 tv3Var) throws SubtitleDecoderException {
        ii.a(tv3Var == this.d);
        b bVar = (b) tv3Var;
        if (bVar.o()) {
            n(bVar);
        } else {
            long j = this.f;
            this.f = 1 + j;
            bVar.n0 = j;
            this.c.add(bVar);
        }
        this.d = null;
    }

    public final void n(b bVar) {
        bVar.h();
        this.a.add(bVar);
    }

    public void o(uv3 uv3Var) {
        uv3Var.h();
        this.b.add(uv3Var);
    }
}
