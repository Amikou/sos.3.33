package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: zo5  reason: default package */
/* loaded from: classes.dex */
public final class zo5 implements ln5 {
    public static final Map<String, zo5> b = new rh();
    public final SharedPreferences a;

    public static zo5 b(Context context, String str) {
        zo5 zo5Var;
        if (!hm5.a()) {
            synchronized (zo5.class) {
                zo5Var = b.get(null);
                if (zo5Var == null) {
                    StrictMode.allowThreadDiskReads();
                    throw null;
                }
            }
            return zo5Var;
        }
        throw null;
    }

    public static synchronized void c() {
        synchronized (zo5.class) {
            Map<String, zo5> map = b;
            Iterator<zo5> it = map.values().iterator();
            if (!it.hasNext()) {
                map.clear();
            } else {
                SharedPreferences sharedPreferences = it.next().a;
                throw null;
            }
        }
    }

    @Override // defpackage.ln5
    public final Object a(String str) {
        throw null;
    }
}
