package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import androidx.recyclerview.widget.o;
import defpackage.xn4;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletsAdapter.kt */
/* renamed from: xn4  reason: default package */
/* loaded from: classes2.dex */
public final class xn4 extends o<Wallet, b> {
    public final Context a;
    public final Wallet b;
    public final tc1<Wallet, te4> c;
    public final tc1<Wallet, te4> d;
    public final tc1<Wallet, te4> e;
    public int f;

    /* compiled from: WalletsAdapter.kt */
    /* renamed from: xn4$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<Wallet> {
        public static final a a = new a();

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(Wallet wallet2, Wallet wallet3) {
            fs1.f(wallet2, "oldItem");
            fs1.f(wallet3, "newItem");
            return fs1.b(wallet2, wallet3);
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(Wallet wallet2, Wallet wallet3) {
            fs1.f(wallet2, "oldItem");
            fs1.f(wallet3, "newItem");
            return fs1.b(wallet2.getId(), wallet3.getId());
        }
    }

    /* compiled from: WalletsAdapter.kt */
    /* renamed from: xn4$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final al1 a;
        public final /* synthetic */ xn4 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(xn4 xn4Var, al1 al1Var) {
            super(al1Var.b());
            fs1.f(xn4Var, "this$0");
            fs1.f(al1Var, "binding");
            this.b = xn4Var;
            this.a = al1Var;
        }

        public static final void e(xn4 xn4Var, Wallet wallet2, View view) {
            fs1.f(xn4Var, "this$0");
            tc1<Wallet, te4> c = xn4Var.c();
            if (c == null) {
                return;
            }
            fs1.e(wallet2, "wallet");
            c.invoke(wallet2);
        }

        public static final void f(xn4 xn4Var, Wallet wallet2, View view) {
            fs1.f(xn4Var, "this$0");
            tc1<Wallet, te4> e = xn4Var.e();
            if (e == null) {
                return;
            }
            fs1.e(wallet2, "wallet");
            e.invoke(wallet2);
        }

        public static final void g(al1 al1Var, xn4 xn4Var, Wallet wallet2, View view) {
            tc1<Wallet, te4> d;
            fs1.f(al1Var, "$this_apply");
            fs1.f(xn4Var, "this$0");
            if (!(al1Var.d.getAlpha() == 1.0f) || (d = xn4Var.d()) == null) {
                return;
            }
            fs1.e(wallet2, "wallet");
            d.invoke(wallet2);
        }

        public final void d(int i, int i2) {
            final Wallet a = xn4.a(this.b, i);
            final al1 al1Var = this.a;
            final xn4 xn4Var = this.b;
            al1Var.f.setText(a.displayName());
            al1Var.e.setOnClickListener(new View.OnClickListener() { // from class: zn4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    xn4.b.e(xn4.this, a, view);
                }
            });
            al1Var.b.setOnClickListener(new View.OnClickListener() { // from class: ao4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    xn4.b.f(xn4.this, a, view);
                }
            });
            al1Var.d.setOnClickListener(new View.OnClickListener() { // from class: yn4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    xn4.b.g(al1.this, xn4Var, a, view);
                }
            });
            Wallet b = xn4Var.b();
            if (fs1.b(b == null ? null : b.getAddress(), a.getAddress())) {
                TextView textView = al1Var.f;
                textView.setTypeface(textView.getTypeface(), 1);
                al1Var.f.setTextColor(xn4Var.f().getColorStateList(R.color.dark_green));
                al1Var.c.setBackgroundTintList(al1Var.b().getContext().getColorStateList(R.color.active_wallet));
            } else {
                al1Var.c.setBackgroundTintList(al1Var.b().getContext().getColorStateList(R.color.card_bg_1));
                TextView textView2 = al1Var.f;
                textView2.setTypeface(textView2.getTypeface(), 0);
                al1Var.f.setTextColor(xn4Var.f().getColorStateList(R.color.t1));
            }
            if (a.isPrimaryWallet()) {
                al1Var.d.setImageResource(R.drawable.ic_wallet_link_all);
                if (do3.a.d(xn4Var.f()) && i2 > 1) {
                    al1Var.d.setImageTintList(xn4Var.f().getColorStateList(R.color.dark_green));
                    return;
                }
                al1Var.d.setImageTintList(xn4Var.f().getColorStateList(R.color.t1));
                if (i2 == 1) {
                    al1Var.d.setAlpha(0.3f);
                    return;
                } else {
                    al1Var.d.setAlpha(1.0f);
                    return;
                }
            }
            al1Var.d.setAlpha(1.0f);
            if (a.isLinked()) {
                al1Var.d.setImageResource(R.drawable.ic_wallet_link_individual);
                al1Var.d.setImageTintList(xn4Var.f().getColorStateList(R.color.dark_green));
                return;
            }
            al1Var.d.setImageResource(R.drawable.ic_wallet_unlink_individual);
            al1Var.d.setImageTintList(xn4Var.f().getColorStateList(R.color.t1));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public xn4(Context context, Wallet wallet2, tc1<? super Wallet, te4> tc1Var, tc1<? super Wallet, te4> tc1Var2, tc1<? super Wallet, te4> tc1Var3) {
        super(a.a);
        fs1.f(context, "context");
        this.a = context;
        this.b = wallet2;
        this.c = tc1Var;
        this.d = tc1Var2;
        this.e = tc1Var3;
        this.f = getItemCount();
    }

    public static final /* synthetic */ Wallet a(xn4 xn4Var, int i) {
        return xn4Var.getItem(i);
    }

    public final Wallet b() {
        return this.b;
    }

    public final tc1<Wallet, te4> c() {
        return this.c;
    }

    public final tc1<Wallet, te4> d() {
        return this.e;
    }

    public final tc1<Wallet, te4> e() {
        return this.d;
    }

    public final Context f() {
        return this.a;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: g */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.d(i, this.f);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: h */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        al1 a2 = al1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_wallet, viewGroup, false));
        fs1.e(a2, "bind(\n                La…ent, false)\n            )");
        return new b(this, a2);
    }
}
