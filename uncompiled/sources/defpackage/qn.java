package defpackage;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.h;
import java.util.Objects;
import net.safemoon.androidwallet.R;

/* compiled from: BaseNavigationFragment.kt */
/* renamed from: qn  reason: default package */
/* loaded from: classes2.dex */
public abstract class qn extends Fragment {
    public NavController a;

    public final NavController e() {
        return this.a;
    }

    public final void f() {
        pg4.e(requireActivity());
        NavController navController = this.a;
        if (navController != null) {
            navController.w();
        }
        if (this.a == null) {
            if (requireActivity().getSupportFragmentManager().o0() == 0) {
                requireActivity().onBackPressed();
            } else {
                requireActivity().getSupportFragmentManager().Y0();
            }
        }
    }

    public final void g(ce2 ce2Var) {
        fs1.f(ce2Var, "directions");
        h(ce2Var, true);
    }

    public final void h(ce2 ce2Var, boolean z) {
        fs1.f(ce2Var, "directions");
        try {
            pg4.e(requireActivity());
            if (z) {
                NavController navController = this.a;
                if (navController != null) {
                    navController.u(ce2Var, new h.a().b(R.anim.screen_slide_in).c(R.anim.screen_fade_out).e(R.anim.screen_fade_in).f(R.anim.screen_slide_out).a());
                }
            } else {
                NavController navController2 = this.a;
                if (navController2 != null) {
                    navController2.t(ce2Var);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        Fragment j0 = requireActivity().getSupportFragmentManager().j0(R.id.nav_host_fragment);
        Objects.requireNonNull(j0, "null cannot be cast to non-null type androidx.navigation.fragment.NavHostFragment");
        this.a = ((NavHostFragment) j0).h();
    }
}
