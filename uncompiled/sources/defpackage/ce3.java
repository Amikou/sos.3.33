package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: ce3  reason: default package */
/* loaded from: classes2.dex */
public class ce3 extends ct0.b {
    public static final BigInteger g = ae3.j;
    public int[] f;

    public ce3() {
        this.f = ad2.e();
    }

    public ce3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP128R1FieldElement");
        }
        this.f = be3.d(bigInteger);
    }

    public ce3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] e = ad2.e();
        be3.a(this.f, ((ce3) ct0Var).f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] e = ad2.e();
        be3.b(this.f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] e = ad2.e();
        g92.d(be3.a, ((ce3) ct0Var).f, e);
        be3.e(e, this.f, e);
        return new ce3(e);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ce3) {
            return ad2.i(this.f, ((ce3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] e = ad2.e();
        g92.d(be3.a, this.f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ad2.o(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 4);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ad2.q(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] e = ad2.e();
        be3.e(this.f, ((ce3) ct0Var).f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] e = ad2.e();
        be3.g(this.f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (ad2.q(iArr) || ad2.o(iArr)) {
            return this;
        }
        int[] e = ad2.e();
        be3.j(iArr, e);
        be3.e(e, iArr, e);
        int[] e2 = ad2.e();
        be3.k(e, 2, e2);
        be3.e(e2, e, e2);
        int[] e3 = ad2.e();
        be3.k(e2, 4, e3);
        be3.e(e3, e2, e3);
        be3.k(e3, 2, e2);
        be3.e(e2, e, e2);
        be3.k(e2, 10, e);
        be3.e(e, e2, e);
        be3.k(e, 10, e3);
        be3.e(e3, e2, e3);
        be3.j(e3, e2);
        be3.e(e2, iArr, e2);
        be3.k(e2, 95, e2);
        be3.j(e2, e3);
        if (ad2.i(iArr, e3)) {
            return new ce3(e2);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] e = ad2.e();
        be3.j(this.f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] e = ad2.e();
        be3.m(this.f, ((ce3) ct0Var).f, e);
        return new ce3(e);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return ad2.m(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ad2.x(this.f);
    }
}
