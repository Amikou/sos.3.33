package defpackage;

import android.view.View;

/* compiled from: ViewOffsetHelper.java */
/* renamed from: jj4  reason: default package */
/* loaded from: classes2.dex */
public class jj4 {
    public final View a;
    public int b;
    public int c;
    public int d;
    public int e;
    public boolean f = true;
    public boolean g = true;

    public jj4(View view) {
        this.a = view;
    }

    public void a() {
        View view = this.a;
        ei4.d0(view, this.d - (view.getTop() - this.b));
        View view2 = this.a;
        ei4.c0(view2, this.e - (view2.getLeft() - this.c));
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.e;
    }

    public int d() {
        return this.d;
    }

    public boolean e() {
        return this.g;
    }

    public boolean f() {
        return this.f;
    }

    public void g() {
        this.b = this.a.getTop();
        this.c = this.a.getLeft();
    }

    public void h(boolean z) {
        this.g = z;
    }

    public boolean i(int i) {
        if (!this.g || this.e == i) {
            return false;
        }
        this.e = i;
        a();
        return true;
    }

    public boolean j(int i) {
        if (!this.f || this.d == i) {
            return false;
        }
        this.d = i;
        a();
        return true;
    }

    public void k(boolean z) {
        this.f = z;
    }
}
