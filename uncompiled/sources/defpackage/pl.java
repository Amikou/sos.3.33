package defpackage;

import com.google.android.datatransport.Priority;
import defpackage.ob4;
import java.util.Arrays;
import java.util.Objects;

/* compiled from: AutoValue_TransportContext.java */
/* renamed from: pl  reason: default package */
/* loaded from: classes.dex */
public final class pl extends ob4 {
    public final String a;
    public final byte[] b;
    public final Priority c;

    /* compiled from: AutoValue_TransportContext.java */
    /* renamed from: pl$b */
    /* loaded from: classes.dex */
    public static final class b extends ob4.a {
        public String a;
        public byte[] b;
        public Priority c;

        @Override // defpackage.ob4.a
        public ob4 a() {
            String str = "";
            if (this.a == null) {
                str = " backendName";
            }
            if (this.c == null) {
                str = str + " priority";
            }
            if (str.isEmpty()) {
                return new pl(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.ob4.a
        public ob4.a b(String str) {
            Objects.requireNonNull(str, "Null backendName");
            this.a = str;
            return this;
        }

        @Override // defpackage.ob4.a
        public ob4.a c(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @Override // defpackage.ob4.a
        public ob4.a d(Priority priority) {
            Objects.requireNonNull(priority, "Null priority");
            this.c = priority;
            return this;
        }
    }

    @Override // defpackage.ob4
    public String b() {
        return this.a;
    }

    @Override // defpackage.ob4
    public byte[] c() {
        return this.b;
    }

    @Override // defpackage.ob4
    public Priority d() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ob4) {
            ob4 ob4Var = (ob4) obj;
            if (this.a.equals(ob4Var.b())) {
                if (Arrays.equals(this.b, ob4Var instanceof pl ? ((pl) ob4Var).b : ob4Var.c()) && this.c.equals(ob4Var.d())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b)) * 1000003) ^ this.c.hashCode();
    }

    public pl(String str, byte[] bArr, Priority priority) {
        this.a = str;
        this.b = bArr;
        this.c = priority;
    }
}
