package defpackage;

import androidx.paging.LoadType;

/* renamed from: hp2  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class hp2 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[LoadType.values().length];
        a = iArr;
        iArr[LoadType.REFRESH.ordinal()] = 1;
        iArr[LoadType.PREPEND.ordinal()] = 2;
        iArr[LoadType.APPEND.ordinal()] = 3;
    }
}
