package defpackage;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

/* compiled from: ResourceCacheKey.java */
/* renamed from: t73  reason: default package */
/* loaded from: classes.dex */
public final class t73 implements fx1 {
    public static final s22<Class<?>, byte[]> j = new s22<>(50);
    public final sh b;
    public final fx1 c;
    public final fx1 d;
    public final int e;
    public final int f;
    public final Class<?> g;
    public final vn2 h;
    public final za4<?> i;

    public t73(sh shVar, fx1 fx1Var, fx1 fx1Var2, int i, int i2, za4<?> za4Var, Class<?> cls, vn2 vn2Var) {
        this.b = shVar;
        this.c = fx1Var;
        this.d = fx1Var2;
        this.e = i;
        this.f = i2;
        this.i = za4Var;
        this.g = cls;
        this.h = vn2Var;
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.d(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.b(messageDigest);
        this.c.b(messageDigest);
        messageDigest.update(bArr);
        za4<?> za4Var = this.i;
        if (za4Var != null) {
            za4Var.b(messageDigest);
        }
        this.h.b(messageDigest);
        messageDigest.update(c());
        this.b.c(bArr);
    }

    public final byte[] c() {
        s22<Class<?>, byte[]> s22Var = j;
        byte[] g = s22Var.g(this.g);
        if (g == null) {
            byte[] bytes = this.g.getName().getBytes(fx1.a);
            s22Var.k(this.g, bytes);
            return bytes;
        }
        return g;
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof t73) {
            t73 t73Var = (t73) obj;
            return this.f == t73Var.f && this.e == t73Var.e && mg4.d(this.i, t73Var.i) && this.g.equals(t73Var.g) && this.c.equals(t73Var.c) && this.d.equals(t73Var.d) && this.h.equals(t73Var.h);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        za4<?> za4Var = this.i;
        if (za4Var != null) {
            hashCode = (hashCode * 31) + za4Var.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + "', options=" + this.h + '}';
    }
}
