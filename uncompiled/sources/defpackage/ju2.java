package defpackage;

import android.database.Cursor;
import androidx.room.RoomDatabase;

/* compiled from: PreferenceDao_Impl.java */
/* renamed from: ju2  reason: default package */
/* loaded from: classes.dex */
public final class ju2 implements iu2 {
    public final RoomDatabase a;
    public final zv0<hu2> b;

    /* compiled from: PreferenceDao_Impl.java */
    /* renamed from: ju2$a */
    /* loaded from: classes.dex */
    public class a extends zv0<hu2> {
        public a(ju2 ju2Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, hu2 hu2Var) {
            String str = hu2Var.a;
            if (str == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, str);
            }
            Long l = hu2Var.b;
            if (l == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.q0(2, l.longValue());
            }
        }
    }

    public ju2(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @Override // defpackage.iu2
    public Long a(String str) {
        k93 c = k93.c("SELECT long_value FROM Preference where `key`=?", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        this.a.d();
        Long l = null;
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            if (c2.moveToFirst() && !c2.isNull(0)) {
                l = Long.valueOf(c2.getLong(0));
            }
            return l;
        } finally {
            c2.close();
            c.f();
        }
    }

    @Override // defpackage.iu2
    public void b(hu2 hu2Var) {
        this.a.d();
        this.a.e();
        try {
            this.b.h(hu2Var);
            this.a.E();
        } finally {
            this.a.j();
        }
    }
}
