package defpackage;

import androidx.media3.common.util.b;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.wi3;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: ChunkReader.java */
/* renamed from: qy  reason: default package */
/* loaded from: classes.dex */
public final class qy {
    public final f84 a;
    public final int b;
    public final int c;
    public final long d;
    public final int e;
    public int f;
    public int g;
    public int h;
    public int i;
    public int j;
    public long[] k;
    public int[] l;

    public qy(int i, int i2, long j, int i3, f84 f84Var) {
        boolean z = true;
        if (i2 != 1 && i2 != 2) {
            z = false;
        }
        ii.a(z);
        this.d = j;
        this.e = i3;
        this.a = f84Var;
        this.b = d(i, i2 == 2 ? 1667497984 : 1651965952);
        this.c = i2 == 2 ? d(i, 1650720768) : -1;
        this.k = new long[RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN];
        this.l = new int[RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN];
    }

    public static int d(int i, int i2) {
        return (((i % 10) + 48) << 8) | ((i / 10) + 48) | i2;
    }

    public void a() {
        this.h++;
    }

    public void b(long j) {
        if (this.j == this.l.length) {
            long[] jArr = this.k;
            this.k = Arrays.copyOf(jArr, (jArr.length * 3) / 2);
            int[] iArr = this.l;
            this.l = Arrays.copyOf(iArr, (iArr.length * 3) / 2);
        }
        long[] jArr2 = this.k;
        int i = this.j;
        jArr2[i] = j;
        this.l[i] = this.i;
        this.j = i + 1;
    }

    public void c() {
        this.k = Arrays.copyOf(this.k, this.j);
        this.l = Arrays.copyOf(this.l, this.j);
    }

    public final long e(int i) {
        return (this.d * i) / this.e;
    }

    public long f() {
        return e(this.h);
    }

    public long g() {
        return e(1);
    }

    public final yi3 h(int i) {
        return new yi3(this.l[i] * g(), this.k[i]);
    }

    public wi3.a i(long j) {
        int g = (int) (j / g());
        int h = b.h(this.l, g, true, true);
        if (this.l[h] == g) {
            return new wi3.a(h(h));
        }
        yi3 h2 = h(h);
        int i = h + 1;
        if (i < this.k.length) {
            return new wi3.a(h2, h(i));
        }
        return new wi3.a(h2);
    }

    public boolean j(int i) {
        return this.b == i || this.c == i;
    }

    public void k() {
        this.i++;
    }

    public boolean l() {
        return Arrays.binarySearch(this.l, this.h) >= 0;
    }

    public boolean m(q11 q11Var) throws IOException {
        int i = this.g;
        int d = i - this.a.d(q11Var, i, false);
        this.g = d;
        boolean z = d == 0;
        if (z) {
            if (this.f > 0) {
                this.a.b(f(), l() ? 1 : 0, this.f, 0, null);
            }
            a();
        }
        return z;
    }

    public void n(int i) {
        this.f = i;
        this.g = i;
    }

    public void o(long j) {
        if (this.j == 0) {
            this.h = 0;
            return;
        }
        this.h = this.l[b.i(this.k, j, true, true)];
    }
}
