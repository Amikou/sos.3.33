package defpackage;

import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: v45  reason: default package */
/* loaded from: classes.dex */
public final class v45 implements z55 {
    public final z55 a;
    public final String f0;

    public v45() {
        throw null;
    }

    public v45(String str) {
        this.a = z55.X;
        this.f0 = str;
    }

    public v45(String str, z55 z55Var) {
        this.a = z55Var;
        this.f0 = str;
    }

    public final z55 a() {
        return this.a;
    }

    @Override // defpackage.z55
    public final Double b() {
        throw new IllegalStateException("Control is not a double");
    }

    @Override // defpackage.z55
    public final Boolean c() {
        throw new IllegalStateException("Control is not a boolean");
    }

    public final String d() {
        return this.f0;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof v45) {
            v45 v45Var = (v45) obj;
            return this.f0.equals(v45Var.f0) && this.a.equals(v45Var.a);
        }
        return false;
    }

    public final int hashCode() {
        return (this.f0.hashCode() * 31) + this.a.hashCode();
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return null;
    }

    @Override // defpackage.z55
    public final z55 m() {
        return new v45(this.f0, this.a.m());
    }

    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        throw new IllegalStateException("Control does not have functions");
    }

    @Override // defpackage.z55
    public final String zzc() {
        throw new IllegalStateException("Control is not a String");
    }
}
