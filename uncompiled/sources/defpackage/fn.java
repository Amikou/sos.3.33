package defpackage;

import androidx.media3.datasource.b;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: BaseDataSource.java */
/* renamed from: fn  reason: default package */
/* loaded from: classes.dex */
public abstract class fn implements b {
    public final boolean a;
    public final ArrayList<fa4> b = new ArrayList<>(1);
    public int c;
    public je0 d;

    public fn(boolean z) {
        this.a = z;
    }

    @Override // androidx.media3.datasource.b
    public final void c(fa4 fa4Var) {
        ii.e(fa4Var);
        if (this.b.contains(fa4Var)) {
            return;
        }
        this.b.add(fa4Var);
        this.c++;
    }

    @Override // androidx.media3.datasource.b
    public /* synthetic */ Map i() {
        return ee0.a(this);
    }

    public final void o(int i) {
        je0 je0Var = (je0) androidx.media3.common.util.b.j(this.d);
        for (int i2 = 0; i2 < this.c; i2++) {
            this.b.get(i2).a(this, je0Var, this.a, i);
        }
    }

    public final void p() {
        je0 je0Var = (je0) androidx.media3.common.util.b.j(this.d);
        for (int i = 0; i < this.c; i++) {
            this.b.get(i).d(this, je0Var, this.a);
        }
        this.d = null;
    }

    public final void q(je0 je0Var) {
        for (int i = 0; i < this.c; i++) {
            this.b.get(i).i(this, je0Var, this.a);
        }
    }

    public final void r(je0 je0Var) {
        this.d = je0Var;
        for (int i = 0; i < this.c; i++) {
            this.b.get(i).e(this, je0Var, this.a);
        }
    }
}
