package defpackage;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ux5  reason: default package */
/* loaded from: classes.dex */
public class ux5<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public final int a;
    public List<by5> f0;
    public Map<K, V> g0;
    public boolean h0;
    public volatile ey5 i0;
    public Map<K, V> j0;
    public volatile vx5 k0;

    public ux5(int i) {
        this.a = i;
        this.f0 = Collections.emptyList();
        this.g0 = Collections.emptyMap();
        this.j0 = Collections.emptyMap();
    }

    public static <FieldDescriptorType extends pr5<FieldDescriptorType>> ux5<FieldDescriptorType, Object> d(int i) {
        return new sx5(i);
    }

    public final int a(K k) {
        int size = this.f0.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo((Comparable) this.f0.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo((Comparable) this.f0.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    @Override // java.util.AbstractMap, java.util.Map
    /* renamed from: b */
    public final V put(K k, V v) {
        p();
        int a = a(k);
        if (a >= 0) {
            return (V) this.f0.get(a).setValue(v);
        }
        p();
        if (this.f0.isEmpty() && !(this.f0 instanceof ArrayList)) {
            this.f0 = new ArrayList(this.a);
        }
        int i = -(a + 1);
        if (i >= this.a) {
            return q().put(k, v);
        }
        int size = this.f0.size();
        int i2 = this.a;
        if (size == i2) {
            by5 remove = this.f0.remove(i2 - 1);
            q().put((K) remove.getKey(), (V) remove.getValue());
        }
        this.f0.add(i, new by5(this, k, v));
        return null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        p();
        if (!this.f0.isEmpty()) {
            this.f0.clear();
        }
        if (this.g0.isEmpty()) {
            return;
        }
        this.g0.clear();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.g0.containsKey(comparable);
    }

    public void e() {
        Map<K, V> unmodifiableMap;
        Map<K, V> unmodifiableMap2;
        if (this.h0) {
            return;
        }
        if (this.g0.isEmpty()) {
            unmodifiableMap = Collections.emptyMap();
        } else {
            unmodifiableMap = Collections.unmodifiableMap(this.g0);
        }
        this.g0 = unmodifiableMap;
        if (this.j0.isEmpty()) {
            unmodifiableMap2 = Collections.emptyMap();
        } else {
            unmodifiableMap2 = Collections.unmodifiableMap(this.j0);
        }
        this.j0 = unmodifiableMap2;
        this.h0 = true;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.i0 == null) {
            this.i0 = new ey5(this, null);
        }
        return this.i0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ux5)) {
            return super.equals(obj);
        }
        ux5 ux5Var = (ux5) obj;
        int size = size();
        if (size != ux5Var.size()) {
            return false;
        }
        int j = j();
        if (j != ux5Var.j()) {
            return entrySet().equals(ux5Var.entrySet());
        }
        for (int i = 0; i < j; i++) {
            if (!h(i).equals(ux5Var.h(i))) {
                return false;
            }
        }
        if (j != size) {
            return this.g0.equals(ux5Var.g0);
        }
        return true;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return (V) this.f0.get(a).getValue();
        }
        return this.g0.get(comparable);
    }

    public final Map.Entry<K, V> h(int i) {
        return this.f0.get(i);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int hashCode() {
        int j = j();
        int i = 0;
        for (int i2 = 0; i2 < j; i2++) {
            i += this.f0.get(i2).hashCode();
        }
        return this.g0.size() > 0 ? i + this.g0.hashCode() : i;
    }

    public final boolean i() {
        return this.h0;
    }

    public final int j() {
        return this.f0.size();
    }

    public final V k(int i) {
        p();
        V v = (V) this.f0.remove(i).getValue();
        if (!this.g0.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = q().entrySet().iterator();
            this.f0.add(new by5(this, it.next()));
            it.remove();
        }
        return v;
    }

    public final Iterable<Map.Entry<K, V>> m() {
        if (this.g0.isEmpty()) {
            return ay5.a();
        }
        return this.g0.entrySet();
    }

    public final Set<Map.Entry<K, V>> o() {
        if (this.k0 == null) {
            this.k0 = new vx5(this, null);
        }
        return this.k0;
    }

    public final void p() {
        if (this.h0) {
            throw new UnsupportedOperationException();
        }
    }

    public final SortedMap<K, V> q() {
        p();
        if (this.g0.isEmpty() && !(this.g0 instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.g0 = treeMap;
            this.j0 = treeMap.descendingMap();
        }
        return (SortedMap) this.g0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        p();
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return (V) k(a);
        }
        if (this.g0.isEmpty()) {
            return null;
        }
        return this.g0.remove(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.f0.size() + this.g0.size();
    }

    public /* synthetic */ ux5(int i, sx5 sx5Var) {
        this(i);
    }
}
