package defpackage;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: qf5 */
/* loaded from: classes.dex */
public final class qf5 {
    public static final we5<Long> A;
    public static final we5<Boolean> A0;
    public static final we5<Long> B;
    public static final we5<Boolean> B0;
    public static final we5<Integer> C;
    public static final we5<Long> D;
    public static final we5<Integer> E;
    public static final we5<Integer> F;
    public static final we5<Integer> G;
    public static final we5<Integer> H;
    public static final we5<Integer> I;
    public static final we5<Long> J;
    public static final we5<Boolean> K;
    public static final we5<String> L;
    public static final we5<Long> M;
    public static final we5<Integer> N;
    public static final we5<Double> O;
    public static final we5<Integer> P;
    public static final we5<Integer> Q;
    public static final we5<Long> R;
    public static final we5<Boolean> S;
    public static final we5<Boolean> T;
    public static final we5<Boolean> U;
    public static final we5<Boolean> V;
    public static final we5<Boolean> W;
    public static final we5<Boolean> X;
    public static final we5<Boolean> Y;
    public static final we5<Boolean> Z;
    public static final List<we5<?>> a = Collections.synchronizedList(new ArrayList());
    public static final we5<Boolean> a0;
    public static final we5<Long> b;
    public static final we5<Boolean> b0;
    public static final we5<Long> c;
    public static final we5<Boolean> c0;
    public static final we5<Long> d;
    public static final we5<Boolean> d0;
    public static final we5<String> e;
    public static final we5<Boolean> e0;
    public static final we5<String> f;
    public static final we5<Boolean> f0;
    public static final we5<Integer> g;
    public static final we5<Boolean> g0;
    public static final we5<Integer> h;
    public static final we5<Boolean> h0;
    public static final we5<Integer> i;
    public static final we5<Boolean> i0;
    public static final we5<Integer> j;
    public static final we5<Boolean> j0;
    public static final we5<Integer> k;
    public static final we5<Boolean> k0;
    public static final we5<Integer> l;
    public static final we5<Boolean> l0;
    public static final we5<Integer> m;
    public static final we5<Boolean> m0;
    public static final we5<Integer> n;
    public static final we5<Boolean> n0;
    public static final we5<Integer> o;
    public static final we5<Boolean> o0;
    public static final we5<Integer> p;
    public static final we5<Boolean> p0;
    public static final we5<String> q;
    public static final we5<Boolean> q0;
    public static final we5<Long> r;
    public static final we5<Boolean> r0;
    public static final we5<Long> s;
    public static final we5<Boolean> s0;
    public static final we5<Long> t;
    public static final we5<Boolean> t0;
    public static final we5<Long> u;
    public static final we5<Integer> u0;
    public static final we5<Long> v;
    public static final we5<Boolean> v0;
    public static final we5<Long> w;
    public static final we5<Boolean> w0;
    public static final we5<Long> x;
    public static final we5<Boolean> x0;
    public static final we5<Long> y;
    public static final we5<Boolean> y0;
    public static final we5<Long> z;
    public static final we5<Boolean> z0;

    static {
        Collections.synchronizedSet(new HashSet());
        b = b("measurement.ad_id_cache_time", 10000L, 10000L, j65.a);
        c = b("measurement.monitoring.sample_period_millis", 86400000L, 86400000L, i85.a);
        d = b("measurement.config.cache_time", 86400000L, 3600000L, f95.a);
        e = b("measurement.config.url_scheme", "https", "https", ua5.a);
        f = b("measurement.config.url_authority", "app-measurement.com", "app-measurement.com", ob5.a);
        g = b("measurement.upload.max_bundles", 100, 100, ic5.a);
        h = b("measurement.upload.max_batch_size", 65536, 65536, td5.a);
        i = b("measurement.upload.max_bundle_size", 65536, 65536, me5.a);
        j = b("measurement.upload.max_events_per_bundle", 1000, 1000, ne5.a);
        k = b("measurement.upload.max_events_per_day", 100000, 100000, pe5.a);
        l = b("measurement.upload.max_error_events_per_day", 1000, 1000, m65.a);
        m = b("measurement.upload.max_public_events_per_day", 50000, 50000, p65.a);
        n = b("measurement.upload.max_conversions_per_day", 10000, 10000, r65.a);
        o = b("measurement.upload.max_realtime_events_per_day", 10, 10, u65.a);
        p = b("measurement.store.max_stored_events_per_app", 100000, 100000, x65.a);
        q = b("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a", v75.a);
        r = b("measurement.upload.backoff_period", 43200000L, 43200000L, x75.a);
        s = b("measurement.upload.window_interval", 3600000L, 3600000L, a85.a);
        t = b("measurement.upload.interval", 3600000L, 3600000L, d85.a);
        u = b("measurement.upload.realtime_upload_interval", 10000L, 10000L, f85.a);
        v = b("measurement.upload.debug_upload_interval", 1000L, 1000L, l85.a);
        w = b("measurement.upload.minimum_delay", 500L, 500L, n85.a);
        x = b("measurement.alarm_manager.minimum_interval", 60000L, 60000L, p85.a);
        y = b("measurement.upload.stale_data_deletion_interval", 86400000L, 86400000L, s85.a);
        z = b("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L, u85.a);
        Long valueOf = Long.valueOf((long) u84.DEFAULT_POLLING_FREQUENCY);
        A = b("measurement.upload.initial_upload_delay_time", valueOf, valueOf, w85.a);
        B = b("measurement.upload.retry_time", 1800000L, 1800000L, y85.a);
        C = b("measurement.upload.retry_count", 6, 6, z85.a);
        D = b("measurement.upload.max_queue_time", 2419200000L, 2419200000L, b95.a);
        E = b("measurement.lifetimevalue.max_currency_tracked", 4, 4, d95.a);
        F = b("measurement.audience.filter_result_max_count", 200, 200, h95.a);
        G = b("measurement.upload.max_public_user_properties", 25, 25, null);
        H = b("measurement.upload.max_event_name_cardinality", 500, 500, null);
        I = b("measurement.upload.max_public_event_params", 25, 25, null);
        J = b("measurement.service_client.idle_disconnect_millis", 5000L, 5000L, j95.a);
        Boolean bool = Boolean.FALSE;
        K = b("measurement.test.boolean_flag", bool, bool, m95.a);
        L = b("measurement.test.string_flag", "---", "---", o95.a);
        M = b("measurement.test.long_flag", -1L, -1L, q95.a);
        N = b("measurement.test.int_flag", -2, -2, t95.a);
        Double valueOf2 = Double.valueOf(-3.0d);
        O = b("measurement.test.double_flag", valueOf2, valueOf2, w95.a);
        P = b("measurement.experiment.max_ids", 50, 50, y95.a);
        Q = b("measurement.max_bundles_per_iteration", 100, 100, ba5.a);
        R = b("measurement.sdk.attribution.cache.ttl", 604800000L, 604800000L, sa5.a);
        S = b("measurement.validation.internal_limits_internal_event_params", bool, bool, va5.a);
        Boolean bool2 = Boolean.TRUE;
        T = b("measurement.collection.firebase_global_collection_flag_enabled", bool2, bool2, xa5.a);
        U = b("measurement.collection.redundant_engagement_removal_enabled", bool, bool, ya5.a);
        V = b("measurement.collection.log_event_and_bundle_v2", bool2, bool2, ab5.a);
        W = b("measurement.quality.checksum", bool, bool, null);
        X = b("measurement.sdk.collection.validate_param_names_alphabetical", bool2, bool2, cb5.a);
        Y = b("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", bool, bool, db5.a);
        Z = b("measurement.audience.refresh_event_count_filters_timestamp", bool, bool, fb5.a);
        a0 = b("measurement.audience.use_bundle_timestamp_for_event_count_filters", bool, bool, ib5.a);
        b0 = b("measurement.sdk.collection.retrieve_deeplink_from_bow_2", bool2, bool2, lb5.a);
        c0 = b("measurement.sdk.collection.last_deep_link_referrer2", bool2, bool2, nb5.a);
        d0 = b("measurement.sdk.collection.last_deep_link_referrer_campaign2", bool, bool, qb5.a);
        e0 = b("measurement.sdk.collection.last_gclid_from_referrer2", bool, bool, sb5.a);
        f0 = b("measurement.sdk.collection.enable_extend_user_property_size", bool2, bool2, tb5.a);
        g0 = b("measurement.upload.file_lock_state_check", bool2, bool2, ub5.a);
        h0 = b("measurement.ga.ga_app_id", bool, bool, vb5.a);
        i0 = b("measurement.lifecycle.app_in_background_parameter", bool, bool, xb5.a);
        j0 = b("measurement.integration.disable_firebase_instance_id", bool, bool, zb5.a);
        k0 = b("measurement.lifecycle.app_backgrounded_engagement", bool, bool, bc5.a);
        l0 = b("measurement.collection.service.update_with_analytics_fix", bool, bool, dc5.a);
        m0 = b("measurement.service.use_appinfo_modified", bool2, bool2, fc5.a);
        n0 = b("measurement.client.firebase_feature_rollout.v1.enable", bool2, bool2, kc5.a);
        o0 = b("measurement.client.sessions.check_on_reset_and_enable2", bool2, bool2, mc5.a);
        p0 = b("measurement.scheduler.task_thread.cleanup_on_exit", bool, bool, ed5.a);
        q0 = b("measurement.upload.file_truncate_fix", bool, bool, gd5.a);
        r0 = b("measurement.sdk.screen.disabling_automatic_reporting", bool2, bool2, id5.a);
        s0 = b("measurement.sdk.screen.manual_screen_view_logging", bool2, bool2, kd5.a);
        b("measurement.collection.synthetic_data_mitigation", bool, bool, md5.a);
        t0 = b("measurement.androidId.delete_feature", bool2, bool2, od5.a);
        u0 = b("measurement.service.storage_consent_support_version", 203600, 203600, qd5.a);
        v0 = b("measurement.upload.directly_maybe_log_error_events", bool2, bool2, rd5.a);
        w0 = b("measurement.frontend.directly_maybe_log_error_events", bool, bool, vd5.a);
        x0 = b("measurement.client.properties.non_null_origin", bool2, bool2, xd5.a);
        b("measurement.client.click_identifier_control.dev", bool, bool, zd5.a);
        b("measurement.service.click_identifier_control", bool, bool, be5.a);
        y0 = b("measurement.client.reject_blank_user_id", bool2, bool2, de5.a);
        z0 = b("measurement.config.persist_last_modified", bool2, bool2, fe5.a);
        A0 = b("measurement.client.consent.suppress_1p_in_ga4f_install", bool2, bool2, ge5.a);
        B0 = b("measurement.module.pixie.ees", bool, bool, ie5.a);
        b("measurement.euid.client.dev", bool, bool, ke5.a);
        b("measurement.euid.service", bool, bool, le5.a);
    }

    public static Map<String, String> a(Context context) {
        bn5 b2 = bn5.b(context.getContentResolver(), bo5.a("com.google.android.gms.measurement"));
        return b2 == null ? Collections.emptyMap() : b2.c();
    }

    public static <V> we5<V> b(String str, V v2, V v3, te5<V> te5Var) {
        we5<V> we5Var = new we5<>(str, v2, v3, te5Var, null);
        a.add(we5Var);
        return we5Var;
    }
}
