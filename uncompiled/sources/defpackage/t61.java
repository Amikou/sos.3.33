package defpackage;

@Deprecated
/* renamed from: t61  reason: default package */
/* loaded from: classes.dex */
public abstract class t61<T> {
    public final T a;

    @Deprecated
    /* renamed from: t61$a */
    /* loaded from: classes.dex */
    public static class a extends t61<Boolean> {
        public a(int i, String str, Boolean bool) {
            super(i, str, bool);
        }
    }

    public t61(int i, String str, T t) {
        this.a = t;
        aq3.a().a(this);
    }

    @Deprecated
    public static a a(int i, String str, Boolean bool) {
        return new a(i, str, bool);
    }
}
