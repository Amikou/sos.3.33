package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: DvbSubtitleReader.java */
/* renamed from: qs0  reason: default package */
/* loaded from: classes.dex */
public final class qs0 implements ku0 {
    public final List<gc4.a> a;
    public final f84[] b;
    public boolean c;
    public int d;
    public int e;
    public long f = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    public qs0(List<gc4.a> list) {
        this.a = list;
        this.b = new f84[list.size()];
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        f84[] f84VarArr;
        if (this.c) {
            if (this.d != 2 || b(op2Var, 32)) {
                if (this.d != 1 || b(op2Var, 0)) {
                    int e = op2Var.e();
                    int a = op2Var.a();
                    for (f84 f84Var : this.b) {
                        op2Var.P(e);
                        f84Var.a(op2Var, a);
                    }
                    this.e += a;
                }
            }
        }
    }

    public final boolean b(op2 op2Var, int i) {
        if (op2Var.a() == 0) {
            return false;
        }
        if (op2Var.D() != i) {
            this.c = false;
        }
        this.d--;
        return this.c;
    }

    @Override // defpackage.ku0
    public void c() {
        this.c = false;
        this.f = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
        if (this.c) {
            if (this.f != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                for (f84 f84Var : this.b) {
                    f84Var.b(this.f, 1, this.e, 0, null);
                }
            }
            this.c = false;
        }
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if ((i & 4) == 0) {
            return;
        }
        this.c = true;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.f = j;
        }
        this.e = 0;
        this.d = 2;
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        for (int i = 0; i < this.b.length; i++) {
            gc4.a aVar = this.a.get(i);
            dVar.a();
            f84 f = r11Var.f(dVar.c(), 3);
            f.f(new j.b().S(dVar.b()).e0("application/dvbsubs").T(Collections.singletonList(aVar.b)).V(aVar.a).E());
            this.b[i] = f;
        }
    }
}
