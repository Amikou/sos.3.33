package defpackage;

import defpackage.ct0;
import defpackage.pt0;
import java.math.BigInteger;
import java.util.Hashtable;
import java.util.Random;

/* renamed from: xs0  reason: default package */
/* loaded from: classes2.dex */
public abstract class xs0 {
    public z41 a;
    public ct0 b;
    public ct0 c;
    public BigInteger d;
    public BigInteger e;
    public int f = 0;
    public bt0 g = null;
    public it0 h = null;

    /* renamed from: xs0$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ int b;
        public final /* synthetic */ byte[] c;

        public a(int i, int i2, byte[] bArr) {
            this.a = i;
            this.b = i2;
            this.c = bArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            int i2;
            int i3 = this.b;
            byte[] bArr = new byte[i3];
            byte[] bArr2 = new byte[i3];
            int i4 = 0;
            for (int i5 = 0; i5 < this.a; i5++) {
                int i6 = ((i5 ^ i) - 1) >> 31;
                int i7 = 0;
                while (true) {
                    i2 = this.b;
                    if (i7 < i2) {
                        byte b = bArr[i7];
                        byte[] bArr3 = this.c;
                        bArr[i7] = (byte) (b ^ (bArr3[i4 + i7] & i6));
                        bArr2[i7] = (byte) ((bArr3[(i2 + i4) + i7] & i6) ^ bArr2[i7]);
                        i7++;
                    }
                }
                i4 += i2 * 2;
            }
            xs0 xs0Var = xs0.this;
            return xs0Var.i(xs0Var.n(new BigInteger(1, bArr)), xs0.this.n(new BigInteger(1, bArr2)), false);
        }
    }

    /* renamed from: xs0$b */
    /* loaded from: classes2.dex */
    public static abstract class b extends xs0 {
        public BigInteger[] i;

        public b(int i, int i2, int i3, int i4) {
            super(F(i, i2, i3, i4));
            this.i = null;
        }

        public static z41 F(int i, int i2, int i3, int i4) {
            if (i2 != 0) {
                if (i3 == 0) {
                    if (i4 == 0) {
                        return a51.a(new int[]{0, i2, i});
                    }
                    throw new IllegalArgumentException("k3 must be 0 if k2 == 0");
                } else if (i3 > i2) {
                    if (i4 > i3) {
                        return a51.a(new int[]{0, i2, i3, i4, i});
                    }
                    throw new IllegalArgumentException("k3 must be > k2");
                } else {
                    throw new IllegalArgumentException("k2 must be > k1");
                }
            }
            throw new IllegalArgumentException("k1 must be > 0");
        }

        public synchronized BigInteger[] G() {
            if (this.i == null) {
                this.i = q64.f(this);
            }
            return this.i;
        }

        public boolean H() {
            return this.d != null && this.e != null && this.c.h() && (this.b.i() || this.b.h());
        }

        public ct0 I(ct0 ct0Var) {
            ct0 ct0Var2;
            if (ct0Var.i()) {
                return ct0Var;
            }
            ct0 n = n(ws0.a);
            int u = u();
            Random random = new Random();
            do {
                ct0 n2 = n(new BigInteger(u, random));
                ct0 ct0Var3 = ct0Var;
                ct0Var2 = n;
                for (int i = 1; i < u; i++) {
                    ct0 o = ct0Var3.o();
                    ct0Var2 = ct0Var2.o().a(o.j(n2));
                    ct0Var3 = o.a(ct0Var);
                }
                if (!ct0Var3.i()) {
                    return null;
                }
            } while (ct0Var2.o().a(ct0Var2).i());
            return ct0Var2;
        }

        @Override // defpackage.xs0
        public pt0 h(BigInteger bigInteger, BigInteger bigInteger2, boolean z) {
            ct0 n = n(bigInteger);
            ct0 n2 = n(bigInteger2);
            int r = r();
            if (r == 5 || r == 6) {
                if (!n.i()) {
                    n2 = n2.d(n).a(n);
                } else if (!n2.o().equals(p())) {
                    throw new IllegalArgumentException();
                }
            }
            return i(n, n2, z);
        }

        @Override // defpackage.xs0
        public pt0 l(int i, BigInteger bigInteger) {
            ct0 ct0Var;
            ct0 n = n(bigInteger);
            if (n.i()) {
                ct0Var = p().n();
            } else {
                ct0 I = I(n.o().g().j(p()).a(o()).a(n));
                if (I != null) {
                    if (I.s() != (i == 1)) {
                        I = I.b();
                    }
                    int r = r();
                    ct0Var = (r == 5 || r == 6) ? I.a(n) : I.j(n);
                } else {
                    ct0Var = null;
                }
            }
            if (ct0Var != null) {
                return i(n, ct0Var, true);
            }
            throw new IllegalArgumentException("Invalid point compression");
        }
    }

    /* renamed from: xs0$c */
    /* loaded from: classes2.dex */
    public static abstract class c extends xs0 {
        public c(BigInteger bigInteger) {
            super(a51.b(bigInteger));
        }

        @Override // defpackage.xs0
        public pt0 l(int i, BigInteger bigInteger) {
            ct0 n = n(bigInteger);
            ct0 n2 = n.o().a(this.b).j(n).a(this.c).n();
            if (n2 != null) {
                if (n2.s() != (i == 1)) {
                    n2 = n2.m();
                }
                return i(n, n2, true);
            }
            throw new IllegalArgumentException("Invalid point compression");
        }
    }

    /* renamed from: xs0$d */
    /* loaded from: classes2.dex */
    public class d {
        public int a;
        public bt0 b;
        public it0 c;

        public d(int i, bt0 bt0Var, it0 it0Var) {
            this.a = i;
            this.b = bt0Var;
            this.c = it0Var;
        }

        public xs0 a() {
            if (xs0.this.D(this.a)) {
                xs0 c = xs0.this.c();
                if (c != xs0.this) {
                    synchronized (c) {
                        c.f = this.a;
                        c.g = this.b;
                        c.h = this.c;
                    }
                    return c;
                }
                throw new IllegalStateException("implementation returned current curve");
            }
            throw new IllegalStateException("unsupported coordinate system");
        }

        public d b(bt0 bt0Var) {
            this.b = bt0Var;
            return this;
        }
    }

    /* renamed from: xs0$e */
    /* loaded from: classes2.dex */
    public static class e extends b {
        public int j;
        public int k;
        public int l;
        public int m;
        public pt0.d n;

        /* renamed from: xs0$e$a */
        /* loaded from: classes2.dex */
        public class a implements ht0 {
            public final /* synthetic */ int a;
            public final /* synthetic */ int b;
            public final /* synthetic */ long[] c;
            public final /* synthetic */ int[] d;

            public a(int i, int i2, long[] jArr, int[] iArr) {
                this.a = i;
                this.b = i2;
                this.c = jArr;
                this.d = iArr;
            }

            @Override // defpackage.ht0
            public int a() {
                return this.a;
            }

            @Override // defpackage.ht0
            public pt0 b(int i) {
                int i2;
                long[] k = kd2.k(this.b);
                long[] k2 = kd2.k(this.b);
                int i3 = 0;
                for (int i4 = 0; i4 < this.a; i4++) {
                    long j = ((i4 ^ i) - 1) >> 31;
                    int i5 = 0;
                    while (true) {
                        i2 = this.b;
                        if (i5 < i2) {
                            long j2 = k[i5];
                            long[] jArr = this.c;
                            k[i5] = j2 ^ (jArr[i3 + i5] & j);
                            k2[i5] = k2[i5] ^ (jArr[(i2 + i3) + i5] & j);
                            i5++;
                        }
                    }
                    i3 += i2 * 2;
                }
                e eVar = e.this;
                return eVar.i(new ct0.c(eVar.j, this.d, new d22(k)), new ct0.c(e.this.j, this.d, new d22(k2)), false);
            }
        }

        public e(int i, int i2, int i3, int i4, ct0 ct0Var, ct0 ct0Var2, BigInteger bigInteger, BigInteger bigInteger2) {
            super(i, i2, i3, i4);
            this.j = i;
            this.k = i2;
            this.l = i3;
            this.m = i4;
            this.d = bigInteger;
            this.e = bigInteger2;
            this.n = new pt0.d(this, null, null, false);
            this.b = ct0Var;
            this.c = ct0Var2;
            this.f = 6;
        }

        public e(int i, int i2, int i3, int i4, BigInteger bigInteger, BigInteger bigInteger2) {
            this(i, i2, i3, i4, bigInteger, bigInteger2, (BigInteger) null, (BigInteger) null);
        }

        public e(int i, int i2, int i3, int i4, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) {
            super(i, i2, i3, i4);
            this.j = i;
            this.k = i2;
            this.l = i3;
            this.m = i4;
            this.d = bigInteger3;
            this.e = bigInteger4;
            this.n = new pt0.d(this, null, null, false);
            this.b = n(bigInteger);
            this.c = n(bigInteger2);
            this.f = 6;
        }

        public e(int i, int i2, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) {
            this(i, i2, 0, 0, bigInteger, bigInteger2, bigInteger3, bigInteger4);
        }

        @Override // defpackage.xs0
        public boolean D(int i) {
            return i == 0 || i == 1 || i == 6;
        }

        public boolean K() {
            return this.l == 0 && this.m == 0;
        }

        @Override // defpackage.xs0
        public xs0 c() {
            return new e(this.j, this.k, this.l, this.m, this.b, this.c, this.d, this.e);
        }

        @Override // defpackage.xs0
        public ht0 e(pt0[] pt0VarArr, int i, int i2) {
            int i3 = (this.j + 63) >>> 6;
            int[] iArr = K() ? new int[]{this.k} : new int[]{this.k, this.l, this.m};
            long[] jArr = new long[i2 * i3 * 2];
            int i4 = 0;
            for (int i5 = 0; i5 < i2; i5++) {
                pt0 pt0Var = pt0VarArr[i + i5];
                ((ct0.c) pt0Var.n()).i.l(jArr, i4);
                int i6 = i4 + i3;
                ((ct0.c) pt0Var.o()).i.l(jArr, i6);
                i4 = i6 + i3;
            }
            return new a(i2, i3, jArr, iArr);
        }

        @Override // defpackage.xs0
        public it0 f() {
            return H() ? new ll4() : super.f();
        }

        @Override // defpackage.xs0
        public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
            return new pt0.d(this, ct0Var, ct0Var2, z);
        }

        @Override // defpackage.xs0
        public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
            return new pt0.d(this, ct0Var, ct0Var2, ct0VarArr, z);
        }

        @Override // defpackage.xs0
        public ct0 n(BigInteger bigInteger) {
            return new ct0.c(this.j, this.k, this.l, this.m, bigInteger);
        }

        @Override // defpackage.xs0
        public int u() {
            return this.j;
        }

        @Override // defpackage.xs0
        public pt0 v() {
            return this.n;
        }
    }

    /* renamed from: xs0$f */
    /* loaded from: classes2.dex */
    public static class f extends c {
        public BigInteger i;
        public BigInteger j;
        public pt0.e k;

        public f(BigInteger bigInteger, BigInteger bigInteger2, ct0 ct0Var, ct0 ct0Var2, BigInteger bigInteger3, BigInteger bigInteger4) {
            super(bigInteger);
            this.i = bigInteger;
            this.j = bigInteger2;
            this.k = new pt0.e(this, null, null, false);
            this.b = ct0Var;
            this.c = ct0Var2;
            this.d = bigInteger3;
            this.e = bigInteger4;
            this.f = 4;
        }

        public f(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
            this(bigInteger, bigInteger2, bigInteger3, null, null);
        }

        public f(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, BigInteger bigInteger5) {
            super(bigInteger);
            this.i = bigInteger;
            this.j = ct0.d.u(bigInteger);
            this.k = new pt0.e(this, null, null, false);
            this.b = n(bigInteger2);
            this.c = n(bigInteger3);
            this.d = bigInteger4;
            this.e = bigInteger5;
            this.f = 4;
        }

        @Override // defpackage.xs0
        public boolean D(int i) {
            return i == 0 || i == 1 || i == 2 || i == 4;
        }

        @Override // defpackage.xs0
        public xs0 c() {
            return new f(this.i, this.j, this.b, this.c, this.d, this.e);
        }

        @Override // defpackage.xs0
        public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
            return new pt0.e(this, ct0Var, ct0Var2, z);
        }

        @Override // defpackage.xs0
        public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
            return new pt0.e(this, ct0Var, ct0Var2, ct0VarArr, z);
        }

        @Override // defpackage.xs0
        public ct0 n(BigInteger bigInteger) {
            return new ct0.d(this.i, this.j, bigInteger);
        }

        @Override // defpackage.xs0
        public int u() {
            return this.i.bitLength();
        }

        @Override // defpackage.xs0
        public pt0 v() {
            return this.k;
        }

        @Override // defpackage.xs0
        public pt0 z(pt0 pt0Var) {
            int r;
            return (this == pt0Var.i() || r() != 2 || pt0Var.u() || !((r = pt0Var.i().r()) == 2 || r == 3 || r == 4)) ? super.z(pt0Var) : new pt0.e(this, n(pt0Var.b.t()), n(pt0Var.c.t()), new ct0[]{n(pt0Var.d[0].t())}, pt0Var.e);
        }
    }

    public xs0(z41 z41Var) {
        this.a = z41Var;
    }

    public void A(pt0[] pt0VarArr) {
        B(pt0VarArr, 0, pt0VarArr.length, null);
    }

    public void B(pt0[] pt0VarArr, int i, int i2, ct0 ct0Var) {
        b(pt0VarArr, i, i2);
        int r = r();
        if (r == 0 || r == 5) {
            if (ct0Var != null) {
                throw new IllegalArgumentException("'iso' not valid for affine coordinates");
            }
            return;
        }
        ct0[] ct0VarArr = new ct0[i2];
        int[] iArr = new int[i2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            int i5 = i + i4;
            pt0 pt0Var = pt0VarArr[i5];
            if (pt0Var != null && (ct0Var != null || !pt0Var.v())) {
                ct0VarArr[i3] = pt0Var.s(0);
                iArr[i3] = i5;
                i3++;
            }
        }
        if (i3 == 0) {
            return;
        }
        vs0.n(ct0VarArr, 0, i3, ct0Var);
        for (int i6 = 0; i6 < i3; i6++) {
            int i7 = iArr[i6];
            pt0VarArr[i7] = pt0VarArr[i7].B(ct0VarArr[i6]);
        }
    }

    public ut2 C(pt0 pt0Var, String str, tt2 tt2Var) {
        Hashtable hashtable;
        ut2 a2;
        a(pt0Var);
        synchronized (pt0Var) {
            hashtable = pt0Var.f;
            if (hashtable == null) {
                hashtable = new Hashtable(4);
                pt0Var.f = hashtable;
            }
        }
        synchronized (hashtable) {
            ut2 ut2Var = (ut2) hashtable.get(str);
            a2 = tt2Var.a(ut2Var);
            if (a2 != ut2Var) {
                hashtable.put(str, a2);
            }
        }
        return a2;
    }

    public boolean D(int i) {
        return i == 0;
    }

    public pt0 E(BigInteger bigInteger, BigInteger bigInteger2) {
        pt0 g = g(bigInteger, bigInteger2);
        if (g.w()) {
            return g;
        }
        throw new IllegalArgumentException("Invalid point coordinates");
    }

    public void a(pt0 pt0Var) {
        if (pt0Var == null || this != pt0Var.i()) {
            throw new IllegalArgumentException("'point' must be non-null and on this curve");
        }
    }

    public void b(pt0[] pt0VarArr, int i, int i2) {
        if (pt0VarArr == null) {
            throw new IllegalArgumentException("'points' cannot be null");
        }
        if (i < 0 || i2 < 0 || i > pt0VarArr.length - i2) {
            throw new IllegalArgumentException("invalid range specified for 'points'");
        }
        for (int i3 = 0; i3 < i2; i3++) {
            pt0 pt0Var = pt0VarArr[i + i3];
            if (pt0Var != null && this != pt0Var.i()) {
                throw new IllegalArgumentException("'points' entries must be null or on this curve");
            }
        }
    }

    public abstract xs0 c();

    public synchronized d d() {
        return new d(this.f, this.g, this.h);
    }

    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        int u = (u() + 7) >>> 3;
        byte[] bArr = new byte[i2 * u * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            byte[] byteArray = pt0Var.n().t().toByteArray();
            byte[] byteArray2 = pt0Var.o().t().toByteArray();
            int i5 = 1;
            int i6 = byteArray.length > u ? 1 : 0;
            int length = byteArray.length - i6;
            if (byteArray2.length <= u) {
                i5 = 0;
            }
            int length2 = byteArray2.length - i5;
            int i7 = i3 + u;
            System.arraycopy(byteArray, i6, bArr, i7 - length, length);
            i3 = i7 + u;
            System.arraycopy(byteArray2, i5, bArr, i3 - length2, length2);
        }
        return new a(i2, u, bArr);
    }

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof xs0) && m((xs0) obj));
    }

    public it0 f() {
        bt0 bt0Var = this.g;
        return bt0Var instanceof ke1 ? new le1(this, (ke1) bt0Var) : new fl4();
    }

    public pt0 g(BigInteger bigInteger, BigInteger bigInteger2) {
        return h(bigInteger, bigInteger2, false);
    }

    public pt0 h(BigInteger bigInteger, BigInteger bigInteger2, boolean z) {
        return i(n(bigInteger), n(bigInteger2), z);
    }

    public int hashCode() {
        return (t().hashCode() ^ yr1.a(o().t().hashCode(), 8)) ^ yr1.a(p().t().hashCode(), 16);
    }

    public abstract pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z);

    public abstract pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z);

    public pt0 k(byte[] bArr) {
        pt0 v;
        int u = (u() + 7) / 8;
        byte b2 = bArr[0];
        if (b2 != 0) {
            if (b2 == 2 || b2 == 3) {
                if (bArr.length != u + 1) {
                    throw new IllegalArgumentException("Incorrect length for compressed encoding");
                }
                v = l(b2 & 1, ip.c(bArr, 1, u));
                if (!v.w()) {
                    throw new IllegalArgumentException("Invalid point");
                }
            } else if (b2 != 4) {
                if (b2 != 6 && b2 != 7) {
                    throw new IllegalArgumentException("Invalid point encoding 0x" + Integer.toString(b2, 16));
                } else if (bArr.length != (u * 2) + 1) {
                    throw new IllegalArgumentException("Incorrect length for hybrid encoding");
                } else {
                    BigInteger c2 = ip.c(bArr, 1, u);
                    BigInteger c3 = ip.c(bArr, u + 1, u);
                    if (c3.testBit(0) != (b2 == 7)) {
                        throw new IllegalArgumentException("Inconsistent Y coordinate in hybrid encoding");
                    }
                    v = E(c2, c3);
                }
            } else if (bArr.length != (u * 2) + 1) {
                throw new IllegalArgumentException("Incorrect length for uncompressed encoding");
            } else {
                v = E(ip.c(bArr, 1, u), ip.c(bArr, u + 1, u));
            }
        } else if (bArr.length != 1) {
            throw new IllegalArgumentException("Incorrect length for infinity encoding");
        } else {
            v = v();
        }
        if (b2 == 0 || !v.u()) {
            return v;
        }
        throw new IllegalArgumentException("Invalid infinity encoding");
    }

    public abstract pt0 l(int i, BigInteger bigInteger);

    public boolean m(xs0 xs0Var) {
        return this == xs0Var || (xs0Var != null && t().equals(xs0Var.t()) && o().t().equals(xs0Var.o().t()) && p().t().equals(xs0Var.p().t()));
    }

    public abstract ct0 n(BigInteger bigInteger);

    public ct0 o() {
        return this.b;
    }

    public ct0 p() {
        return this.c;
    }

    public BigInteger q() {
        return this.e;
    }

    public int r() {
        return this.f;
    }

    public bt0 s() {
        return this.g;
    }

    public z41 t() {
        return this.a;
    }

    public abstract int u();

    public abstract pt0 v();

    public synchronized it0 w() {
        if (this.h == null) {
            this.h = f();
        }
        return this.h;
    }

    public BigInteger x() {
        return this.d;
    }

    public ut2 y(pt0 pt0Var, String str) {
        Hashtable hashtable;
        ut2 ut2Var;
        a(pt0Var);
        synchronized (pt0Var) {
            hashtable = pt0Var.f;
        }
        if (hashtable == null) {
            return null;
        }
        synchronized (hashtable) {
            ut2Var = (ut2) hashtable.get(str);
        }
        return ut2Var;
    }

    public pt0 z(pt0 pt0Var) {
        if (this == pt0Var.i()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return v();
        }
        pt0 A = pt0Var.A();
        return h(A.q().t(), A.r().t(), A.e);
    }
}
