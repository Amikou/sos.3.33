package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: o52  reason: default package */
/* loaded from: classes2.dex */
public class o52 extends h {
    public final int a;
    public final int f0;
    public final he1 g0;

    public o52(int i, int i2, he1 he1Var) {
        this.a = i;
        this.f0 = i2;
        this.g0 = new he1(he1Var);
    }

    public o52(h4 h4Var) {
        this.a = ((g) h4Var.D(0)).B().intValue();
        this.f0 = ((g) h4Var.D(1)).B().intValue();
        this.g0 = new he1(((f4) h4Var.D(2)).D());
    }

    public static o52 p(Object obj) {
        if (obj instanceof o52) {
            return (o52) obj;
        }
        if (obj != null) {
            return new o52(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(this.a));
        d4Var.a(new g(this.f0));
        d4Var.a(new j0(this.g0.c()));
        return new n0(d4Var);
    }

    public he1 o() {
        return new he1(this.g0);
    }

    public int q() {
        return this.a;
    }

    public int s() {
        return this.f0;
    }
}
