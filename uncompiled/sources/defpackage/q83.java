package defpackage;

import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.api.Status;
import defpackage.l83;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: q83  reason: default package */
/* loaded from: classes.dex */
public abstract class q83<R extends l83, S extends l83> {
    public Status a(@RecentlyNonNull Status status) {
        return status;
    }

    @RecentlyNullable
    public abstract gq2<S> b(@RecentlyNonNull R r);
}
