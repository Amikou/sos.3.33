package defpackage;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

/* compiled from: WorkManagerTaskExecutor.java */
/* renamed from: iq4  reason: default package */
/* loaded from: classes.dex */
public class iq4 implements q34 {
    public final wl3 a;
    public final Handler b = new Handler(Looper.getMainLooper());
    public final Executor c = new a();

    /* compiled from: WorkManagerTaskExecutor.java */
    /* renamed from: iq4$a */
    /* loaded from: classes.dex */
    public class a implements Executor {
        public a() {
        }

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            iq4.this.d(runnable);
        }
    }

    public iq4(Executor executor) {
        this.a = new wl3(executor);
    }

    @Override // defpackage.q34
    public Executor a() {
        return this.c;
    }

    @Override // defpackage.q34
    public void b(Runnable runnable) {
        this.a.execute(runnable);
    }

    @Override // defpackage.q34
    public wl3 c() {
        return this.a;
    }

    public void d(Runnable runnable) {
        this.b.post(runnable);
    }
}
