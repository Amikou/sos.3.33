package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Log.java */
/* renamed from: el  reason: default package */
/* loaded from: classes2.dex */
public final class el extends r90.e.d.AbstractC0276d {
    public final String a;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Log.java */
    /* renamed from: el$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.AbstractC0276d.a {
        public String a;

        @Override // defpackage.r90.e.d.AbstractC0276d.a
        public r90.e.d.AbstractC0276d a() {
            String str = "";
            if (this.a == null) {
                str = " content";
            }
            if (str.isEmpty()) {
                return new el(this.a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.AbstractC0276d.a
        public r90.e.d.AbstractC0276d.a b(String str) {
            Objects.requireNonNull(str, "Null content");
            this.a = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.AbstractC0276d
    public String b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.AbstractC0276d) {
            return this.a.equals(((r90.e.d.AbstractC0276d) obj).b());
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    public String toString() {
        return "Log{content=" + this.a + "}";
    }

    public el(String str) {
        this.a = str;
    }
}
