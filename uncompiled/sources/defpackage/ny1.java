package defpackage;

/* renamed from: ny1  reason: default package */
/* loaded from: classes2.dex */
public class ny1 {
    public int a;
    public int b;
    public int c;
    public short[][][] d;
    public short[][][] e;
    public short[][] f;
    public short[] g;

    public ny1(byte b, byte b2, short[][][] sArr, short[][][] sArr2, short[][] sArr3, short[] sArr4) {
        int i = b & 255;
        this.a = i;
        int i2 = b2 & 255;
        this.b = i2;
        this.c = i2 - i;
        this.d = sArr;
        this.e = sArr2;
        this.f = sArr3;
        this.g = sArr4;
    }

    public short[][][] a() {
        return this.d;
    }

    public short[][][] b() {
        return this.e;
    }

    public short[] c() {
        return this.g;
    }

    public short[][] d() {
        return this.f;
    }

    public int e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ny1)) {
            return false;
        }
        ny1 ny1Var = (ny1) obj;
        return this.a == ny1Var.f() && this.b == ny1Var.g() && this.c == ny1Var.e() && o33.k(this.d, ny1Var.a()) && o33.k(this.e, ny1Var.b()) && o33.j(this.f, ny1Var.d()) && o33.i(this.g, ny1Var.c());
    }

    public int f() {
        return this.a;
    }

    public int g() {
        return this.b;
    }

    public int hashCode() {
        return (((((((((((this.a * 37) + this.b) * 37) + this.c) * 37) + wh.y(this.d)) * 37) + wh.y(this.e)) * 37) + wh.x(this.f)) * 37) + wh.w(this.g);
    }
}
