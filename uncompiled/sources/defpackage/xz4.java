package defpackage;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.d;
import java.util.ArrayList;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: xz4  reason: default package */
/* loaded from: classes.dex */
public final class xz4 extends b05 {
    public final ArrayList<a.f> f0;
    public final /* synthetic */ rz4 g0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public xz4(rz4 rz4Var, ArrayList<a.f> arrayList) {
        super(rz4Var, null);
        this.g0 = rz4Var;
        this.f0 = arrayList;
    }

    @Override // defpackage.b05
    public final void a() {
        i05 i05Var;
        Set<Scope> H;
        d dVar;
        i05 i05Var2;
        i05Var = this.g0.a;
        d05 d05Var = i05Var.m;
        H = this.g0.H();
        d05Var.p = H;
        ArrayList<a.f> arrayList = this.f0;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            a.f fVar = arrayList.get(i);
            i++;
            dVar = this.g0.o;
            i05Var2 = this.g0.a;
            fVar.h(dVar, i05Var2.m.p);
        }
    }
}
