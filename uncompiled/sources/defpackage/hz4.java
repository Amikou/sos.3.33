package defpackage;

import android.os.RemoteException;
import com.google.android.play.core.internal.p;
import java.util.Map;

/* renamed from: hz4  reason: default package */
/* loaded from: classes2.dex */
public final class hz4 extends jt4 {
    public final /* synthetic */ Map f0;
    public final /* synthetic */ tx4 g0;
    public final /* synthetic */ st4 h0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public hz4(st4 st4Var, tx4 tx4Var, Map map, tx4 tx4Var2) {
        super(tx4Var);
        this.h0 = st4Var;
        this.f0 = map;
        this.g0 = tx4Var2;
    }

    @Override // defpackage.jt4
    public final void a() {
        it4 it4Var;
        zt4 zt4Var;
        String str;
        try {
            zt4Var = this.h0.c;
            str = this.h0.a;
            ((p) zt4Var.c()).h1(str, st4.m(this.f0), new kt4(this.h0, this.g0));
        } catch (RemoteException e) {
            it4Var = st4.f;
            it4Var.c(e, "syncPacks", new Object[0]);
            this.g0.d(new RuntimeException(e));
        }
    }
}
