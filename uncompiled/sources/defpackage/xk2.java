package defpackage;

import com.onesignal.OSSubscriptionState;
import org.json.JSONObject;

/* compiled from: OSSubscriptionStateChanges.java */
/* renamed from: xk2  reason: default package */
/* loaded from: classes2.dex */
public class xk2 {
    public OSSubscriptionState a;
    public OSSubscriptionState b;

    public xk2(OSSubscriptionState oSSubscriptionState, OSSubscriptionState oSSubscriptionState2) {
        this.a = oSSubscriptionState;
        this.b = oSSubscriptionState2;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("from", this.a.j());
            jSONObject.put("to", this.b.j());
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return a().toString();
    }
}
