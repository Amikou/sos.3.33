package defpackage;

import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/* compiled from: ReadWrite.kt */
/* renamed from: q44  reason: default package */
/* loaded from: classes2.dex */
public final class q44 {
    public static final long a(Reader reader, Writer writer, int i) {
        fs1.f(reader, "$this$copyTo");
        fs1.f(writer, "out");
        char[] cArr = new char[i];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += read;
            read = reader.read(cArr);
        }
        return j;
    }

    public static /* synthetic */ long b(Reader reader, Writer writer, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return a(reader, writer, i);
    }

    public static final String c(Reader reader) {
        fs1.f(reader, "$this$readText");
        StringWriter stringWriter = new StringWriter();
        b(reader, stringWriter, 0, 2, null);
        String stringWriter2 = stringWriter.toString();
        fs1.e(stringWriter2, "buffer.toString()");
        return stringWriter2;
    }
}
