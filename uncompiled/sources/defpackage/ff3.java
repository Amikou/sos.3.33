package defpackage;

import java.math.BigInteger;

/* renamed from: ff3  reason: default package */
/* loaded from: classes2.dex */
public class ff3 {
    public static final int[] a = {-977, -2, -1, -1, -1, -1, -1, -1};
    public static final int[] b = {954529, 1954, 1, 0, 0, 0, 0, 0, -1954, -3, -1, -1, -1, -1, -1, -1};
    public static final int[] c = {-954529, -1955, -2, -1, -1, -1, -1, -1, 1953, 2};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (ed2.a(iArr, iArr2, iArr3) != 0 || (iArr3[7] == -1 && ed2.s(iArr3, a))) {
            kd2.b(8, 977, iArr3);
        }
    }

    public static void b(int[] iArr, int[] iArr2) {
        if (kd2.s(8, iArr, iArr2) != 0 || (iArr2[7] == -1 && ed2.s(iArr2, a))) {
            kd2.b(8, 977, iArr2);
        }
    }

    public static int[] c(BigInteger bigInteger) {
        int[] o = ed2.o(bigInteger);
        if (o[7] == -1) {
            int[] iArr = a;
            if (ed2.s(o, iArr)) {
                ed2.I(iArr, o);
            }
        }
        return o;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] j = ed2.j();
        ed2.y(iArr, iArr2, j);
        g(j, iArr3);
    }

    public static void e(int[] iArr, int[] iArr2, int[] iArr3) {
        if (ed2.C(iArr, iArr2, iArr3) != 0 || (iArr3[15] == -1 && kd2.q(16, iArr3, b))) {
            int[] iArr4 = c;
            if (kd2.e(iArr4.length, iArr4, iArr3) != 0) {
                kd2.t(16, iArr3, iArr4.length);
            }
        }
    }

    public static void f(int[] iArr, int[] iArr2) {
        if (ed2.v(iArr)) {
            ed2.L(iArr2);
        } else {
            ed2.H(a, iArr, iArr2);
        }
    }

    public static void g(int[] iArr, int[] iArr2) {
        if (ed2.A(977, ed2.z(977, iArr, 8, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[7] == -1 && ed2.s(iArr2, a))) {
            kd2.b(8, 977, iArr2);
        }
    }

    public static void h(int i, int[] iArr) {
        if ((i == 0 || ed2.B(977, i, iArr, 0) == 0) && !(iArr[7] == -1 && ed2.s(iArr, a))) {
            return;
        }
        kd2.b(8, 977, iArr);
    }

    public static void i(int[] iArr, int[] iArr2) {
        int[] j = ed2.j();
        ed2.F(iArr, j);
        g(j, iArr2);
    }

    public static void j(int[] iArr, int i, int[] iArr2) {
        int[] j = ed2.j();
        ed2.F(iArr, j);
        while (true) {
            g(j, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            ed2.F(iArr2, j);
        }
    }

    public static void k(int[] iArr, int[] iArr2, int[] iArr3) {
        if (ed2.H(iArr, iArr2, iArr3) != 0) {
            kd2.L(8, 977, iArr3);
        }
    }

    public static void l(int[] iArr, int[] iArr2) {
        if (kd2.E(8, iArr, 0, iArr2) != 0 || (iArr2[7] == -1 && ed2.s(iArr2, a))) {
            kd2.b(8, 977, iArr2);
        }
    }
}
