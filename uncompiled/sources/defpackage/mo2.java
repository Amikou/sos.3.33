package defpackage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.introspect.a;
import com.fasterxml.jackson.databind.util.c;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/* compiled from: POJOPropertiesCollector.java */
/* renamed from: mo2  reason: default package */
/* loaded from: classes.dex */
public class mo2 {
    public final MapperConfig<?> a;
    public final boolean b;
    public final boolean c;
    public final JavaType d;
    public final a e;
    public final VisibilityChecker<?> f;
    public final AnnotationIntrospector g;
    public final String h;
    public boolean i;
    public LinkedHashMap<String, no2> j;
    public LinkedList<no2> k;
    public LinkedList<AnnotatedMember> l;
    public LinkedList<AnnotatedMethod> m;
    public LinkedList<AnnotatedMember> n;
    public LinkedList<AnnotatedMethod> o;
    public HashSet<String> p;
    public LinkedHashMap<Object, AnnotatedMember> q;

    public mo2(MapperConfig<?> mapperConfig, boolean z, JavaType javaType, a aVar, String str) {
        this.a = mapperConfig;
        this.c = mapperConfig.isEnabled(MapperFeature.USE_STD_BEAN_NAMING);
        this.b = z;
        this.d = javaType;
        this.e = aVar;
        this.h = str == null ? "set" : str;
        AnnotationIntrospector annotationIntrospector = mapperConfig.isAnnotationProcessingEnabled() ? mapperConfig.getAnnotationIntrospector() : null;
        this.g = annotationIntrospector;
        if (annotationIntrospector == null) {
            this.f = mapperConfig.getDefaultVisibilityChecker();
        } else {
            this.f = annotationIntrospector.findAutoDetectVisibility(aVar, mapperConfig.getDefaultVisibilityChecker());
        }
    }

    public Set<String> A() {
        return this.p;
    }

    public Map<Object, AnnotatedMember> B() {
        if (!this.i) {
            u();
        }
        return this.q;
    }

    public AnnotatedMethod C() {
        if (!this.i) {
            u();
        }
        LinkedList<AnnotatedMethod> linkedList = this.o;
        if (linkedList != null) {
            if (linkedList.size() > 1) {
                H("Multiple value properties defined (" + this.o.get(0) + " vs " + this.o.get(1) + ")");
            }
            return this.o.get(0);
        }
        return null;
    }

    public jl2 D() {
        AnnotationIntrospector annotationIntrospector = this.g;
        if (annotationIntrospector == null) {
            return null;
        }
        jl2 findObjectIdInfo = annotationIntrospector.findObjectIdInfo(this.e);
        return findObjectIdInfo != null ? this.g.findObjectReferenceInfo(this.e, findObjectIdInfo) : findObjectIdInfo;
    }

    public List<vo> E() {
        return new ArrayList(F().values());
    }

    public Map<String, no2> F() {
        if (!this.i) {
            u();
        }
        return this.j;
    }

    public JavaType G() {
        return this.d;
    }

    public void H(String str) {
        throw new IllegalArgumentException("Problem with definition of " + this.e + ": " + str);
    }

    public void a(Map<String, no2> map, AnnotatedParameter annotatedParameter) {
        String findImplicitPropertyName = this.g.findImplicitPropertyName(annotatedParameter);
        if (findImplicitPropertyName == null) {
            findImplicitPropertyName = "";
        }
        PropertyName findNameForDeserialization = this.g.findNameForDeserialization(annotatedParameter);
        boolean z = (findNameForDeserialization == null || findNameForDeserialization.isEmpty()) ? false : true;
        if (!z) {
            if (findImplicitPropertyName.isEmpty() || !this.g.hasCreatorAnnotation(annotatedParameter.getOwner())) {
                return;
            }
            findNameForDeserialization = PropertyName.construct(findImplicitPropertyName);
        }
        PropertyName propertyName = findNameForDeserialization;
        no2 l = (z && findImplicitPropertyName.isEmpty()) ? l(map, propertyName) : m(map, findImplicitPropertyName);
        l.c0(annotatedParameter, propertyName, z, true, false);
        this.k.add(l);
    }

    public void b(Map<String, no2> map) {
        if (this.g == null) {
            return;
        }
        Iterator<AnnotatedConstructor> it = this.e.P().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AnnotatedConstructor next = it.next();
            if (this.k == null) {
                this.k = new LinkedList<>();
            }
            int parameterCount = next.getParameterCount();
            for (int i = 0; i < parameterCount; i++) {
                a(map, next.getParameter(i));
            }
        }
        for (AnnotatedMethod annotatedMethod : this.e.R()) {
            if (this.k == null) {
                this.k = new LinkedList<>();
            }
            int parameterCount2 = annotatedMethod.getParameterCount();
            for (int i2 = 0; i2 < parameterCount2; i2++) {
                a(map, annotatedMethod.getParameter(i2));
            }
        }
    }

    public void c(Map<String, no2> map) {
        boolean z;
        AnnotationIntrospector annotationIntrospector = this.g;
        boolean z2 = (this.b || this.a.isEnabled(MapperFeature.ALLOW_FINAL_FIELDS_AS_MUTATORS)) ? false : true;
        boolean isEnabled = this.a.isEnabled(MapperFeature.PROPAGATE_TRANSIENT_MARKER);
        for (AnnotatedField annotatedField : this.e.L()) {
            PropertyName propertyName = null;
            String findImplicitPropertyName = annotationIntrospector == null ? null : annotationIntrospector.findImplicitPropertyName(annotatedField);
            if (findImplicitPropertyName == null) {
                findImplicitPropertyName = annotatedField.getName();
            }
            if (annotationIntrospector != null) {
                if (this.b) {
                    propertyName = annotationIntrospector.findNameForSerialization(annotatedField);
                } else {
                    propertyName = annotationIntrospector.findNameForDeserialization(annotatedField);
                }
            }
            boolean z3 = propertyName != null;
            if (z3 && propertyName.isEmpty()) {
                propertyName = k(findImplicitPropertyName);
                z = false;
            } else {
                z = z3;
            }
            boolean z4 = propertyName != null;
            if (!z4) {
                z4 = this.f.isFieldVisible(annotatedField);
            }
            boolean z5 = annotationIntrospector != null && annotationIntrospector.hasIgnoreMarker(annotatedField);
            if (annotatedField.isTransient() && !z3) {
                if (isEnabled) {
                    z5 = true;
                }
                z4 = false;
            }
            if (!z2 || propertyName != null || z5 || !Modifier.isFinal(annotatedField.getModifiers())) {
                if (annotatedField.hasAnnotation(ru1.class)) {
                    if (this.n == null) {
                        this.n = new LinkedList<>();
                    }
                    this.n.add(annotatedField);
                }
                m(map, findImplicitPropertyName).d0(annotatedField, propertyName, z, z4, z5);
            }
        }
    }

    public void d(Map<String, no2> map, AnnotatedMethod annotatedMethod, AnnotationIntrospector annotationIntrospector) {
        String findImplicitPropertyName;
        if (annotatedMethod.hasReturnType()) {
            if (annotationIntrospector != null) {
                if (annotationIntrospector.hasAnyGetterAnnotation(annotatedMethod)) {
                    if (this.l == null) {
                        this.l = new LinkedList<>();
                    }
                    this.l.add(annotatedMethod);
                    return;
                } else if (annotationIntrospector.hasAsValueAnnotation(annotatedMethod)) {
                    if (this.o == null) {
                        this.o = new LinkedList<>();
                    }
                    this.o.add(annotatedMethod);
                    return;
                }
            }
            PropertyName findNameForSerialization = annotationIntrospector == null ? null : annotationIntrospector.findNameForSerialization(annotatedMethod);
            boolean z = true;
            boolean z2 = findNameForSerialization != null;
            if (!z2) {
                findImplicitPropertyName = annotationIntrospector != null ? annotationIntrospector.findImplicitPropertyName(annotatedMethod) : null;
                if (findImplicitPropertyName == null) {
                    findImplicitPropertyName = yo.g(annotatedMethod, annotatedMethod.getName(), this.c);
                }
                if (findImplicitPropertyName == null) {
                    findImplicitPropertyName = yo.e(annotatedMethod, annotatedMethod.getName(), this.c);
                    if (findImplicitPropertyName == null) {
                        return;
                    }
                    z = this.f.isIsGetterVisible(annotatedMethod);
                } else {
                    z = this.f.isGetterVisible(annotatedMethod);
                }
            } else {
                findImplicitPropertyName = annotationIntrospector != null ? annotationIntrospector.findImplicitPropertyName(annotatedMethod) : null;
                if (findImplicitPropertyName == null) {
                    findImplicitPropertyName = yo.d(annotatedMethod, this.c);
                }
                if (findImplicitPropertyName == null) {
                    findImplicitPropertyName = annotatedMethod.getName();
                }
                if (findNameForSerialization.isEmpty()) {
                    findNameForSerialization = k(findImplicitPropertyName);
                    z2 = false;
                }
            }
            m(map, findImplicitPropertyName).e0(annotatedMethod, findNameForSerialization, z2, z, annotationIntrospector != null ? annotationIntrospector.hasIgnoreMarker(annotatedMethod) : false);
        }
    }

    public void e(Map<String, no2> map) {
        AnnotationIntrospector annotationIntrospector = this.g;
        if (annotationIntrospector == null) {
            return;
        }
        for (AnnotatedMember annotatedMember : this.e.L()) {
            i(annotationIntrospector.findInjectableValueId(annotatedMember), annotatedMember);
        }
        for (AnnotatedMethod annotatedMethod : this.e.U()) {
            if (annotatedMethod.getParameterCount() == 1) {
                i(annotationIntrospector.findInjectableValueId(annotatedMethod), annotatedMethod);
            }
        }
    }

    public void f(Map<String, no2> map) {
        AnnotationIntrospector annotationIntrospector = this.g;
        for (AnnotatedMethod annotatedMethod : this.e.U()) {
            int parameterCount = annotatedMethod.getParameterCount();
            if (parameterCount == 0) {
                d(map, annotatedMethod, annotationIntrospector);
            } else if (parameterCount == 1) {
                g(map, annotatedMethod, annotationIntrospector);
            } else if (parameterCount == 2 && annotationIntrospector != null && annotationIntrospector.hasAnySetterAnnotation(annotatedMethod)) {
                if (this.m == null) {
                    this.m = new LinkedList<>();
                }
                this.m.add(annotatedMethod);
            }
        }
    }

    public void g(Map<String, no2> map, AnnotatedMethod annotatedMethod, AnnotationIntrospector annotationIntrospector) {
        String findImplicitPropertyName;
        PropertyName findNameForDeserialization = annotationIntrospector == null ? null : annotationIntrospector.findNameForDeserialization(annotatedMethod);
        boolean z = true;
        boolean z2 = findNameForDeserialization != null;
        if (!z2) {
            findImplicitPropertyName = annotationIntrospector != null ? annotationIntrospector.findImplicitPropertyName(annotatedMethod) : null;
            if (findImplicitPropertyName == null) {
                findImplicitPropertyName = yo.f(annotatedMethod, this.h, this.c);
            }
            if (findImplicitPropertyName == null) {
                return;
            }
            z = this.f.isSetterVisible(annotatedMethod);
        } else {
            findImplicitPropertyName = annotationIntrospector != null ? annotationIntrospector.findImplicitPropertyName(annotatedMethod) : null;
            if (findImplicitPropertyName == null) {
                findImplicitPropertyName = yo.f(annotatedMethod, this.h, this.c);
            }
            if (findImplicitPropertyName == null) {
                findImplicitPropertyName = annotatedMethod.getName();
            }
            if (findNameForDeserialization.isEmpty()) {
                findNameForDeserialization = k(findImplicitPropertyName);
                z2 = false;
            }
        }
        m(map, findImplicitPropertyName).f0(annotatedMethod, findNameForDeserialization, z2, z, annotationIntrospector != null ? annotationIntrospector.hasIgnoreMarker(annotatedMethod) : false);
    }

    public final void h(String str) {
        if (this.b) {
            return;
        }
        if (this.p == null) {
            this.p = new HashSet<>();
        }
        this.p.add(str);
    }

    public void i(Object obj, AnnotatedMember annotatedMember) {
        if (obj == null) {
            return;
        }
        if (this.q == null) {
            this.q = new LinkedHashMap<>();
        }
        if (this.q.put(obj, annotatedMember) == null) {
            return;
        }
        String name = obj.getClass().getName();
        throw new IllegalArgumentException("Duplicate injectable value with id '" + String.valueOf(obj) + "' (of type " + name + ")");
    }

    public final PropertyNamingStrategy j() {
        AnnotationIntrospector annotationIntrospector = this.g;
        Object findNamingStrategy = annotationIntrospector == null ? null : annotationIntrospector.findNamingStrategy(this.e);
        if (findNamingStrategy == null) {
            return this.a.getPropertyNamingStrategy();
        }
        if (findNamingStrategy instanceof PropertyNamingStrategy) {
            return (PropertyNamingStrategy) findNamingStrategy;
        }
        if (findNamingStrategy instanceof Class) {
            Class cls = (Class) findNamingStrategy;
            if (cls == PropertyNamingStrategy.class) {
                return null;
            }
            if (PropertyNamingStrategy.class.isAssignableFrom(cls)) {
                this.a.getHandlerInstantiator();
                return (PropertyNamingStrategy) c.i(cls, this.a.canOverrideAccessModifiers());
            }
            throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<PropertyNamingStrategy>");
        }
        throw new IllegalStateException("AnnotationIntrospector returned PropertyNamingStrategy definition of type " + findNamingStrategy.getClass().getName() + "; expected type PropertyNamingStrategy or Class<PropertyNamingStrategy> instead");
    }

    public final PropertyName k(String str) {
        return PropertyName.construct(str, null);
    }

    public no2 l(Map<String, no2> map, PropertyName propertyName) {
        return m(map, propertyName.getSimpleName());
    }

    public no2 m(Map<String, no2> map, String str) {
        no2 no2Var = map.get(str);
        if (no2Var == null) {
            no2 no2Var2 = new no2(this.a, this.g, this.b, PropertyName.construct(str));
            map.put(str, no2Var2);
            return no2Var2;
        }
        return no2Var;
    }

    public void n(Map<String, no2> map) {
        boolean isEnabled = this.a.isEnabled(MapperFeature.INFER_PROPERTY_MUTATORS);
        for (no2 no2Var : map.values()) {
            JsonProperty.Access v0 = no2Var.v0(isEnabled);
            if (!this.b && v0 == JsonProperty.Access.READ_ONLY) {
                h(no2Var.t());
            }
        }
    }

    public void o(Map<String, no2> map) {
        Iterator<no2> it = map.values().iterator();
        while (it.hasNext()) {
            no2 next = it.next();
            if (!next.h0()) {
                it.remove();
            } else if (next.g0()) {
                if (!next.G()) {
                    it.remove();
                    h(next.t());
                } else {
                    next.u0();
                    if (!this.b && !next.a()) {
                        h(next.t());
                    }
                }
            }
        }
    }

    public void p(Map<String, no2> map) {
        Iterator<Map.Entry<String, no2>> it = map.entrySet().iterator();
        LinkedList linkedList = null;
        while (it.hasNext()) {
            no2 value = it.next().getValue();
            Set<PropertyName> m0 = value.m0();
            if (!m0.isEmpty()) {
                it.remove();
                if (linkedList == null) {
                    linkedList = new LinkedList();
                }
                if (m0.size() == 1) {
                    linkedList.add(value.x0(m0.iterator().next()));
                } else {
                    linkedList.addAll(value.k0(m0));
                }
            }
        }
        if (linkedList != null) {
            Iterator it2 = linkedList.iterator();
            while (it2.hasNext()) {
                no2 no2Var = (no2) it2.next();
                String t = no2Var.t();
                no2 no2Var2 = map.get(t);
                if (no2Var2 == null) {
                    map.put(t, no2Var);
                } else {
                    no2Var2.b0(no2Var);
                }
                t(no2Var, this.k);
            }
        }
    }

    public void q(Map<String, no2> map, PropertyNamingStrategy propertyNamingStrategy) {
        no2[] no2VarArr = (no2[]) map.values().toArray(new no2[map.size()]);
        map.clear();
        for (no2 no2Var : no2VarArr) {
            PropertyName o = no2Var.o();
            String str = null;
            if (!no2Var.H() || this.a.isEnabled(MapperFeature.ALLOW_EXPLICIT_PROPERTY_RENAMING)) {
                if (this.b) {
                    if (no2Var.C()) {
                        str = propertyNamingStrategy.nameForGetterMethod(this.a, no2Var.p(), o.getSimpleName());
                    } else if (no2Var.B()) {
                        str = propertyNamingStrategy.nameForField(this.a, no2Var.l(), o.getSimpleName());
                    }
                } else if (no2Var.E()) {
                    str = propertyNamingStrategy.nameForSetterMethod(this.a, no2Var.x(), o.getSimpleName());
                } else if (no2Var.A()) {
                    str = propertyNamingStrategy.nameForConstructorParameter(this.a, no2Var.p0(), o.getSimpleName());
                } else if (no2Var.B()) {
                    str = propertyNamingStrategy.nameForField(this.a, no2Var.l(), o.getSimpleName());
                } else if (no2Var.C()) {
                    str = propertyNamingStrategy.nameForGetterMethod(this.a, no2Var.p(), o.getSimpleName());
                }
            }
            if (str != null && !o.hasSimpleName(str)) {
                no2Var = no2Var.z0(str);
            } else {
                str = o.getSimpleName();
            }
            no2 no2Var2 = map.get(str);
            if (no2Var2 == null) {
                map.put(str, no2Var);
            } else {
                no2Var2.b0(no2Var);
            }
            t(no2Var, this.k);
        }
    }

    public void r(Map<String, no2> map) {
        PropertyName findWrapperName;
        Iterator<Map.Entry<String, no2>> it = map.entrySet().iterator();
        LinkedList linkedList = null;
        while (it.hasNext()) {
            no2 value = it.next().getValue();
            AnnotatedMember v = value.v();
            if (v != null && (findWrapperName = this.g.findWrapperName(v)) != null && findWrapperName.hasSimpleName() && !findWrapperName.equals(value.o())) {
                if (linkedList == null) {
                    linkedList = new LinkedList();
                }
                linkedList.add(value.x0(findWrapperName));
                it.remove();
            }
        }
        if (linkedList != null) {
            Iterator it2 = linkedList.iterator();
            while (it2.hasNext()) {
                no2 no2Var = (no2) it2.next();
                String t = no2Var.t();
                no2 no2Var2 = map.get(t);
                if (no2Var2 == null) {
                    map.put(t, no2Var);
                } else {
                    no2Var2.b0(no2Var);
                }
            }
        }
    }

    public void s(Map<String, no2> map) {
        boolean booleanValue;
        Map<? extends Object, ? extends Object> linkedHashMap;
        AnnotationIntrospector annotationIntrospector = this.g;
        Boolean findSerializationSortAlphabetically = annotationIntrospector == null ? null : annotationIntrospector.findSerializationSortAlphabetically(this.e);
        if (findSerializationSortAlphabetically == null) {
            booleanValue = this.a.shouldSortPropertiesAlphabetically();
        } else {
            booleanValue = findSerializationSortAlphabetically.booleanValue();
        }
        String[] findSerializationPropertyOrder = annotationIntrospector != null ? annotationIntrospector.findSerializationPropertyOrder(this.e) : null;
        if (!booleanValue && this.k == null && findSerializationPropertyOrder == null) {
            return;
        }
        int size = map.size();
        if (booleanValue) {
            linkedHashMap = new TreeMap<>();
        } else {
            linkedHashMap = new LinkedHashMap<>(size + size);
        }
        for (no2 no2Var : map.values()) {
            linkedHashMap.put(no2Var.t(), no2Var);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(size + size);
        if (findSerializationPropertyOrder != null) {
            for (String str : findSerializationPropertyOrder) {
                no2 no2Var2 = (no2) linkedHashMap.get(str);
                if (no2Var2 == null) {
                    Iterator<no2> it = map.values().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        no2 next = it.next();
                        if (str.equals(next.q0())) {
                            str = next.t();
                            no2Var2 = next;
                            break;
                        }
                    }
                }
                if (no2Var2 != null) {
                    linkedHashMap2.put(str, no2Var2);
                }
            }
        }
        Collection<no2> collection = this.k;
        if (collection != null) {
            if (booleanValue) {
                TreeMap treeMap = new TreeMap();
                Iterator<no2> it2 = this.k.iterator();
                while (it2.hasNext()) {
                    no2 next2 = it2.next();
                    treeMap.put(next2.t(), next2);
                }
                collection = treeMap.values();
            }
            for (no2 no2Var3 : collection) {
                String t = no2Var3.t();
                if (linkedHashMap.containsKey(t)) {
                    linkedHashMap2.put(t, no2Var3);
                }
            }
        }
        linkedHashMap2.putAll(linkedHashMap);
        map.clear();
        map.putAll(linkedHashMap2);
    }

    public void t(no2 no2Var, List<no2> list) {
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).q0().equals(no2Var.q0())) {
                    list.set(i, no2Var);
                    return;
                }
            }
        }
    }

    public void u() {
        LinkedHashMap<String, no2> linkedHashMap = new LinkedHashMap<>();
        c(linkedHashMap);
        f(linkedHashMap);
        if (!this.e.T()) {
            b(linkedHashMap);
        }
        e(linkedHashMap);
        o(linkedHashMap);
        for (no2 no2Var : linkedHashMap.values()) {
            no2Var.s0(this.b);
        }
        n(linkedHashMap);
        p(linkedHashMap);
        PropertyNamingStrategy j = j();
        if (j != null) {
            q(linkedHashMap, j);
        }
        for (no2 no2Var2 : linkedHashMap.values()) {
            no2Var2.w0();
        }
        if (this.a.isEnabled(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME)) {
            r(linkedHashMap);
        }
        s(linkedHashMap);
        this.j = linkedHashMap;
        this.i = true;
    }

    public AnnotatedMember v() {
        if (!this.i) {
            u();
        }
        LinkedList<AnnotatedMember> linkedList = this.l;
        if (linkedList != null) {
            if (linkedList.size() > 1) {
                H("Multiple 'any-getters' defined (" + this.l.get(0) + " vs " + this.l.get(1) + ")");
            }
            return this.l.getFirst();
        }
        return null;
    }

    public AnnotatedMember w() {
        if (!this.i) {
            u();
        }
        LinkedList<AnnotatedMember> linkedList = this.n;
        if (linkedList != null) {
            if (linkedList.size() > 1) {
                H("Multiple 'any-Setters' defined (" + this.m.get(0) + " vs " + this.n.get(1) + ")");
            }
            return this.n.getFirst();
        }
        return null;
    }

    public AnnotatedMethod x() {
        if (!this.i) {
            u();
        }
        LinkedList<AnnotatedMethod> linkedList = this.m;
        if (linkedList != null) {
            if (linkedList.size() > 1) {
                H("Multiple 'any-setters' defined (" + this.m.get(0) + " vs " + this.m.get(1) + ")");
            }
            return this.m.getFirst();
        }
        return null;
    }

    public a y() {
        return this.e;
    }

    public MapperConfig<?> z() {
        return this.a;
    }
}
