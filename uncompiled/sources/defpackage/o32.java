package defpackage;

import java.io.IOException;
import java.math.BigInteger;
import org.web3j.ens.a;
import org.web3j.protocol.exceptions.TransactionException;

/* compiled from: ManagedTransaction.java */
/* renamed from: o32  reason: default package */
/* loaded from: classes3.dex */
public abstract class o32 {
    public static final BigInteger GAS_PRICE = BigInteger.valueOf(22000000000L);
    public a ensResolver;
    public u84 transactionManager;
    public ko4 web3j;

    public o32(ko4 ko4Var, u84 u84Var) {
        this(new a(ko4Var), ko4Var, u84Var);
    }

    public String call(String str, String str2, gi0 gi0Var) throws IOException {
        return this.transactionManager.sendCall(str, str2, gi0Var);
    }

    public long getSyncThreshold() {
        return this.ensResolver.getSyncThreshold();
    }

    public BigInteger requestCurrentGasPrice() throws IOException {
        return this.web3j.ethGasPrice().send().getGasPrice();
    }

    public w84 send(String str, String str2, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) throws IOException, TransactionException {
        return this.transactionManager.executeTransaction(bigInteger2, bigInteger3, str, str2, bigInteger);
    }

    public w84 sendEIP1559(String str, String str2, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) throws IOException, TransactionException {
        return this.transactionManager.executeTransactionEIP1559(bigInteger3, bigInteger4, bigInteger2, str, str2, bigInteger);
    }

    public void setSyncThreshold(long j) {
        this.ensResolver.setSyncThreshold(j);
    }

    public o32(a aVar, ko4 ko4Var, u84 u84Var) {
        this.transactionManager = u84Var;
        this.ensResolver = aVar;
        this.web3j = ko4Var;
    }

    public w84 send(String str, String str2, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, boolean z) throws IOException, TransactionException {
        return this.transactionManager.executeTransaction(bigInteger2, bigInteger3, str, str2, bigInteger, z);
    }
}
