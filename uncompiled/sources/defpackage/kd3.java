package defpackage;

import android.content.Context;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;

/* compiled from: SchedulingModule_WorkSchedulerFactory.java */
/* renamed from: kd3  reason: default package */
/* loaded from: classes.dex */
public final class kd3 implements z11<rq4> {
    public final ew2<Context> a;
    public final ew2<dy0> b;
    public final ew2<SchedulerConfig> c;
    public final ew2<qz> d;

    public kd3(ew2<Context> ew2Var, ew2<dy0> ew2Var2, ew2<SchedulerConfig> ew2Var3, ew2<qz> ew2Var4) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
        this.d = ew2Var4;
    }

    public static kd3 a(ew2<Context> ew2Var, ew2<dy0> ew2Var2, ew2<SchedulerConfig> ew2Var3, ew2<qz> ew2Var4) {
        return new kd3(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static rq4 c(Context context, dy0 dy0Var, SchedulerConfig schedulerConfig, qz qzVar) {
        return (rq4) yt2.c(jd3.a(context, dy0Var, schedulerConfig, qzVar), "Cannot return null from a non-@Nullable @Provides method");
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public rq4 get() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
