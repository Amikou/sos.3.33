package defpackage;

/* compiled from: ForwardingObject.java */
/* renamed from: i91  reason: default package */
/* loaded from: classes2.dex */
public abstract class i91 {
    public abstract Object delegate();

    public String toString() {
        return delegate().toString();
    }
}
