package defpackage;

import java.util.Iterator;

/* compiled from: Sequences.kt */
/* renamed from: gb4  reason: default package */
/* loaded from: classes2.dex */
public final class gb4<T, R> implements ol3<R> {
    public final ol3<T> a;
    public final tc1<T, R> b;

    /* compiled from: Sequences.kt */
    /* renamed from: gb4$a */
    /* loaded from: classes2.dex */
    public static final class a implements Iterator<R>, tw1 {
        public final Iterator<T> a;

        public a() {
            this.a = gb4.this.a.iterator();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @Override // java.util.Iterator
        public R next() {
            return (R) gb4.this.b.invoke(this.a.next());
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public gb4(ol3<? extends T> ol3Var, tc1<? super T, ? extends R> tc1Var) {
        fs1.f(ol3Var, "sequence");
        fs1.f(tc1Var, "transformer");
        this.a = ol3Var;
        this.b = tc1Var;
    }

    @Override // defpackage.ol3
    public Iterator<R> iterator() {
        return new a();
    }
}
