package defpackage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import net.safemoon.androidwallet.R;

/* compiled from: CustomPop.kt */
/* renamed from: jc0  reason: default package */
/* loaded from: classes2.dex */
public final class jc0 {
    public static final jc0 a = new jc0();

    public static final void c(Activity activity, Integer num, int i, boolean z, DialogInterface.OnDismissListener onDismissListener) {
        fs1.f(activity, "activity");
        String string = num == null ? null : activity.getString(num.intValue());
        String string2 = activity.getString(i);
        fs1.e(string2, "activity.getString(desc)");
        d(activity, string, string2, z, onDismissListener);
    }

    public static final void d(Activity activity, String str, String str2, boolean z, DialogInterface.OnDismissListener onDismissListener) {
        fs1.f(activity, "activity");
        fs1.f(str2, "desc");
        final Dialog b = a.b(activity, z);
        ym0 a2 = ym0.a(activity.getLayoutInflater().inflate(R.layout.dialog_alert, (ViewGroup) null));
        fs1.e(a2, "bind(activity.layoutInfl…yout.dialog_alert, null))");
        if (str == null) {
            a2.d.setVisibility(8);
        } else {
            a2.d.setText(str);
        }
        a2.c.setText(str2);
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: ic0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                jc0.f(b, view);
            }
        });
        b.setContentView(a2.b());
        b.setOnDismissListener(onDismissListener);
        b.show();
    }

    public static /* synthetic */ void e(Activity activity, Integer num, int i, boolean z, DialogInterface.OnDismissListener onDismissListener, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            z = true;
        }
        c(activity, num, i, z, onDismissListener);
    }

    public static final void f(Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        dialog.dismiss();
    }

    public final Dialog b(Context context, boolean z) {
        Dialog dialog = new Dialog(context, 2132017235);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(z);
        dialog.setCanceledOnTouchOutside(z);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.getAttributes().gravity = 17;
            window.getAttributes().width = -1;
        }
        return dialog;
    }
}
