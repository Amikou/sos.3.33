package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.a;
import java.security.MessageDigest;

/* compiled from: DrawableTransformation.java */
/* renamed from: ar0  reason: default package */
/* loaded from: classes.dex */
public class ar0 implements za4<Drawable> {
    public final za4<Bitmap> b;
    public final boolean c;

    public ar0(za4<Bitmap> za4Var, boolean z) {
        this.b = za4Var;
        this.c = z;
    }

    @Override // defpackage.za4
    public s73<Drawable> a(Context context, s73<Drawable> s73Var, int i, int i2) {
        jq f = a.c(context).f();
        Drawable drawable = s73Var.get();
        s73<Bitmap> a = zq0.a(f, drawable, i, i2);
        if (a == null) {
            if (this.c) {
                throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
            }
            return s73Var;
        }
        s73<Bitmap> a2 = this.b.a(context, a, i, i2);
        if (a2.equals(a)) {
            a2.b();
            return s73Var;
        }
        return d(context, a2);
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        this.b.b(messageDigest);
    }

    public za4<BitmapDrawable> c() {
        return this;
    }

    public final s73<Drawable> d(Context context, s73<Bitmap> s73Var) {
        return uy1.f(context.getResources(), s73Var);
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof ar0) {
            return this.b.equals(((ar0) obj).b);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return this.b.hashCode();
    }
}
