package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.ThreadContextElement;

/* compiled from: ThreadContext.kt */
/* renamed from: m54  reason: default package */
/* loaded from: classes2.dex */
public final class m54 {
    public final CoroutineContext a;
    public final Object[] b;
    public final ThreadContextElement<Object>[] c;
    public int d;

    public m54(CoroutineContext coroutineContext, int i) {
        this.a = coroutineContext;
        this.b = new Object[i];
        this.c = new g54[i];
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void a(g54<?> g54Var, Object obj) {
        Object[] objArr = this.b;
        int i = this.d;
        objArr[i] = obj;
        ThreadContextElement<Object>[] threadContextElementArr = this.c;
        this.d = i + 1;
        threadContextElementArr[i] = g54Var;
    }

    public final void b(CoroutineContext coroutineContext) {
        int length = this.c.length - 1;
        if (length < 0) {
            return;
        }
        while (true) {
            int i = length - 1;
            g54 g54Var = this.c[length];
            fs1.d(g54Var);
            g54Var.q(coroutineContext, this.b[length]);
            if (i < 0) {
                return;
            }
            length = i;
        }
    }
}
