package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Status;
import defpackage.l83;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: n83  reason: default package */
/* loaded from: classes.dex */
public abstract class n83<R extends l83> implements m83<R> {
    @Override // defpackage.m83
    public final void a(@RecentlyNonNull R r) {
        Status e = r.e();
        if (e.L1()) {
            c(r);
            return;
        }
        b(e);
        if (r instanceof k63) {
            try {
                ((k63) r).a();
            } catch (RuntimeException unused) {
                String valueOf = String.valueOf(r);
                StringBuilder sb = new StringBuilder(valueOf.length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
            }
        }
    }

    public abstract void b(@RecentlyNonNull Status status);

    public abstract void c(@RecentlyNonNull R r);
}
