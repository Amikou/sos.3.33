package defpackage;

import android.os.Bundle;
import defpackage.kb;
import java.util.Locale;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: CrashlyticsAnalyticsListener.java */
/* renamed from: k90  reason: default package */
/* loaded from: classes2.dex */
public class k90 implements kb.b {
    public rb a;
    public rb b;

    public static void b(rb rbVar, String str, Bundle bundle) {
        if (rbVar == null) {
            return;
        }
        rbVar.d(str, bundle);
    }

    @Override // defpackage.kb.b
    public void a(int i, Bundle bundle) {
        String string;
        w12.f().i(String.format(Locale.US, "Analytics listener received message. ID: %d, Extras: %s", Integer.valueOf(i), bundle));
        if (bundle == null || (string = bundle.getString(PublicResolver.FUNC_NAME)) == null) {
            return;
        }
        Bundle bundle2 = bundle.getBundle("params");
        if (bundle2 == null) {
            bundle2 = new Bundle();
        }
        c(string, bundle2);
    }

    public final void c(String str, Bundle bundle) {
        rb rbVar;
        if ("clx".equals(bundle.getString("_o"))) {
            rbVar = this.a;
        } else {
            rbVar = this.b;
        }
        b(rbVar, str, bundle);
    }

    public void d(rb rbVar) {
        this.b = rbVar;
    }

    public void e(rb rbVar) {
        this.a = rbVar;
    }
}
