package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.CustomKeyBoard;

/* compiled from: ActivityHomeBinding.java */
/* renamed from: f7  reason: default package */
/* loaded from: classes2.dex */
public final class f7 {
    public final CoordinatorLayout a;
    public final ConstraintLayout b;
    public final ConstraintLayout c;
    public final CardView d;
    public final BottomNavigationView e;

    public f7(CoordinatorLayout coordinatorLayout, Barrier barrier, TextView textView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, AppCompatImageView appCompatImageView, CustomKeyBoard customKeyBoard, CardView cardView, BottomNavigationView bottomNavigationView, CoordinatorLayout coordinatorLayout2, AppCompatTextView appCompatTextView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7) {
        this.a = coordinatorLayout;
        this.b = constraintLayout;
        this.c = constraintLayout2;
        this.d = cardView;
        this.e = bottomNavigationView;
    }

    public static f7 a(View view) {
        int i = R.id.barrierBottom;
        Barrier barrier = (Barrier) ai4.a(view, R.id.barrierBottom);
        if (barrier != null) {
            i = R.id.btnConfirm;
            TextView textView = (TextView) ai4.a(view, R.id.btnConfirm);
            if (textView != null) {
                i = R.id.cl_top;
                ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.cl_top);
                if (constraintLayout != null) {
                    i = R.id.container;
                    ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.container);
                    if (constraintLayout2 != null) {
                        i = R.id.imageView5;
                        ImageView imageView = (ImageView) ai4.a(view, R.id.imageView5);
                        if (imageView != null) {
                            i = R.id.img_logo;
                            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                            if (appCompatImageView != null) {
                                i = R.id.keyWrapper;
                                CustomKeyBoard customKeyBoard = (CustomKeyBoard) ai4.a(view, R.id.keyWrapper);
                                if (customKeyBoard != null) {
                                    i = R.id.loaderWrapper;
                                    CardView cardView = (CardView) ai4.a(view, R.id.loaderWrapper);
                                    if (cardView != null) {
                                        i = R.id.nav_view;
                                        BottomNavigationView bottomNavigationView = (BottomNavigationView) ai4.a(view, R.id.nav_view);
                                        if (bottomNavigationView != null) {
                                            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) view;
                                            i = R.id.text_version;
                                            AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.text_version);
                                            if (appCompatTextView != null) {
                                                i = R.id.textView28;
                                                TextView textView2 = (TextView) ai4.a(view, R.id.textView28);
                                                if (textView2 != null) {
                                                    i = R.id.textView29;
                                                    TextView textView3 = (TextView) ai4.a(view, R.id.textView29);
                                                    if (textView3 != null) {
                                                        i = R.id.textView30;
                                                        TextView textView4 = (TextView) ai4.a(view, R.id.textView30);
                                                        if (textView4 != null) {
                                                            i = R.id.textView31;
                                                            TextView textView5 = (TextView) ai4.a(view, R.id.textView31);
                                                            if (textView5 != null) {
                                                                i = R.id.textView32;
                                                                TextView textView6 = (TextView) ai4.a(view, R.id.textView32);
                                                                if (textView6 != null) {
                                                                    i = R.id.textView33;
                                                                    TextView textView7 = (TextView) ai4.a(view, R.id.textView33);
                                                                    if (textView7 != null) {
                                                                        return new f7(coordinatorLayout, barrier, textView, constraintLayout, constraintLayout2, imageView, appCompatImageView, customKeyBoard, cardView, bottomNavigationView, coordinatorLayout, appCompatTextView, textView2, textView3, textView4, textView5, textView6, textView7);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static f7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static f7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_home, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public CoordinatorLayout b() {
        return this.a;
    }
}
