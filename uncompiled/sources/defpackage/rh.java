package defpackage;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* compiled from: ArrayMap.java */
/* renamed from: rh  reason: default package */
/* loaded from: classes.dex */
public class rh<K, V> extends vo3<K, V> implements Map<K, V> {
    public t32<K, V> l0;

    /* compiled from: ArrayMap.java */
    /* renamed from: rh$a */
    /* loaded from: classes.dex */
    public class a extends t32<K, V> {
        public a() {
        }

        @Override // defpackage.t32
        public void a() {
            rh.this.clear();
        }

        @Override // defpackage.t32
        public Object b(int i, int i2) {
            return rh.this.f0[(i << 1) + i2];
        }

        @Override // defpackage.t32
        public Map<K, V> c() {
            return rh.this;
        }

        @Override // defpackage.t32
        public int d() {
            return rh.this.g0;
        }

        @Override // defpackage.t32
        public int e(Object obj) {
            return rh.this.f(obj);
        }

        @Override // defpackage.t32
        public int f(Object obj) {
            return rh.this.h(obj);
        }

        @Override // defpackage.t32
        public void g(K k, V v) {
            rh.this.put(k, v);
        }

        @Override // defpackage.t32
        public void h(int i) {
            rh.this.k(i);
        }

        @Override // defpackage.t32
        public V i(int i, V v) {
            return rh.this.l(i, v);
        }
    }

    public rh() {
    }

    @Override // java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        return n().l();
    }

    @Override // java.util.Map
    public Set<K> keySet() {
        return n().m();
    }

    public final t32<K, V> n() {
        if (this.l0 == null) {
            this.l0 = new a();
        }
        return this.l0;
    }

    public boolean o(Collection<?> collection) {
        return t32.p(this, collection);
    }

    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        c(this.g0 + map.size());
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override // java.util.Map
    public Collection<V> values() {
        return n().n();
    }

    public rh(int i) {
        super(i);
    }

    public rh(vo3 vo3Var) {
        super(vo3Var);
    }
}
