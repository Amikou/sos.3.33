package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.repository.dataSource.token.user.LocalUserTokenDataSource;

/* compiled from: UserTokenListRepositoryProvider.kt */
/* renamed from: gg4  reason: default package */
/* loaded from: classes2.dex */
public final class gg4 implements rm1 {
    public static final gg4 a = new gg4();
    public static bn1 b;

    @Override // defpackage.rm1
    public void a() {
        b = null;
    }

    public final bn1 b(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new fg4(new LocalUserTokenDataSource(ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, context, null, 2, null).Z()));
                }
                te4 te4Var = te4.a;
            }
        }
        bn1 bn1Var = b;
        fs1.d(bn1Var);
        return bn1Var;
    }
}
