package defpackage;

import androidx.recyclerview.widget.g;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: CollectionsAdapter.kt */
/* renamed from: s10  reason: default package */
/* loaded from: classes2.dex */
public final class s10 {
    public static final a a = new a();

    /* compiled from: CollectionsAdapter.kt */
    /* renamed from: s10$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<RoomCollectionAndNft> {
        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(RoomCollectionAndNft roomCollectionAndNft, RoomCollectionAndNft roomCollectionAndNft2) {
            fs1.f(roomCollectionAndNft, "oldItem");
            fs1.f(roomCollectionAndNft2, "newItem");
            if (fs1.b(roomCollectionAndNft.getCollection().getName(), roomCollectionAndNft2.getCollection().getName()) && fs1.b(roomCollectionAndNft.getCollection().getImageUrl(), roomCollectionAndNft2.getCollection().getImageUrl()) && roomCollectionAndNft.getCollection().getOrder() == roomCollectionAndNft2.getCollection().getOrder() && roomCollectionAndNft.getShowHideIcon() == roomCollectionAndNft2.getShowHideIcon()) {
                RoomNFT roomNFT = (RoomNFT) j20.M(roomCollectionAndNft.getNfts());
                String image_preview_url = roomNFT == null ? null : roomNFT.getImage_preview_url();
                RoomNFT roomNFT2 = (RoomNFT) j20.M(roomCollectionAndNft2.getNfts());
                if (fs1.b(image_preview_url, roomNFT2 != null ? roomNFT2.getImage_preview_url() : null)) {
                    return true;
                }
            }
            return false;
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(RoomCollectionAndNft roomCollectionAndNft, RoomCollectionAndNft roomCollectionAndNft2) {
            fs1.f(roomCollectionAndNft, "oldItem");
            fs1.f(roomCollectionAndNft2, "newItem");
            return fs1.b(roomCollectionAndNft.getCollection().getId(), roomCollectionAndNft2.getCollection().getId());
        }
    }
}
