package defpackage;

/* compiled from: RotationOptions.java */
/* renamed from: p93  reason: default package */
/* loaded from: classes.dex */
public class p93 {
    public static final p93 c = new p93(-1, false);
    public static final p93 d;
    public final int a;
    public final boolean b;

    static {
        new p93(-2, false);
        d = new p93(-1, true);
    }

    public p93(int i, boolean z) {
        this.a = i;
        this.b = z;
    }

    public static p93 a() {
        return c;
    }

    public static p93 b() {
        return d;
    }

    public boolean c() {
        return this.b;
    }

    public int d() {
        if (!f()) {
            return this.a;
        }
        throw new IllegalStateException("Rotation is set to use EXIF");
    }

    public boolean e() {
        return this.a != -2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof p93) {
            p93 p93Var = (p93) obj;
            return this.a == p93Var.a && this.b == p93Var.b;
        }
        return false;
    }

    public boolean f() {
        return this.a == -1;
    }

    public int hashCode() {
        return ck1.c(Integer.valueOf(this.a), Boolean.valueOf(this.b));
    }

    public String toString() {
        return String.format(null, "%d defer:%b", Integer.valueOf(this.a), Boolean.valueOf(this.b));
    }
}
