package defpackage;

import java.util.Iterator;

/* compiled from: Sequences.kt */
/* renamed from: tl3  reason: default package */
/* loaded from: classes2.dex */
public class tl3 extends sl3 {

    /* compiled from: Sequences.kt */
    /* renamed from: tl3$a */
    /* loaded from: classes2.dex */
    public static final class a implements ol3<T> {
        public final /* synthetic */ Iterator a;

        public a(Iterator it) {
            this.a = it;
        }

        @Override // defpackage.ol3
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    public static final <T> ol3<T> c(Iterator<? extends T> it) {
        fs1.f(it, "$this$asSequence");
        return d(new a(it));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> ol3<T> d(ol3<? extends T> ol3Var) {
        fs1.f(ol3Var, "$this$constrainOnce");
        return ol3Var instanceof c60 ? ol3Var : new c60(ol3Var);
    }

    public static final <T> ol3<T> e(rc1<? extends T> rc1Var, tc1<? super T, ? extends T> tc1Var) {
        fs1.f(rc1Var, "seedFunction");
        fs1.f(tc1Var, "nextFunction");
        return new xe1(rc1Var, tc1Var);
    }
}
