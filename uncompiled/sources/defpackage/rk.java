package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session.java */
/* renamed from: rk  reason: default package */
/* loaded from: classes2.dex */
public final class rk extends r90.e {
    public final String a;
    public final String b;
    public final long c;
    public final Long d;
    public final boolean e;
    public final r90.e.a f;
    public final r90.e.f g;
    public final r90.e.AbstractC0277e h;
    public final r90.e.c i;
    public final ip1<r90.e.d> j;
    public final int k;

    /* compiled from: AutoValue_CrashlyticsReport_Session.java */
    /* renamed from: rk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.b {
        public String a;
        public String b;
        public Long c;
        public Long d;
        public Boolean e;
        public r90.e.a f;
        public r90.e.f g;
        public r90.e.AbstractC0277e h;
        public r90.e.c i;
        public ip1<r90.e.d> j;
        public Integer k;

        @Override // defpackage.r90.e.b
        public r90.e a() {
            String str = "";
            if (this.a == null) {
                str = " generator";
            }
            if (this.b == null) {
                str = str + " identifier";
            }
            if (this.c == null) {
                str = str + " startedAt";
            }
            if (this.e == null) {
                str = str + " crashed";
            }
            if (this.f == null) {
                str = str + " app";
            }
            if (this.k == null) {
                str = str + " generatorType";
            }
            if (str.isEmpty()) {
                return new rk(this.a, this.b, this.c.longValue(), this.d, this.e.booleanValue(), this.f, this.g, this.h, this.i, this.j, this.k.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.b
        public r90.e.b b(r90.e.a aVar) {
            Objects.requireNonNull(aVar, "Null app");
            this.f = aVar;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b c(boolean z) {
            this.e = Boolean.valueOf(z);
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b d(r90.e.c cVar) {
            this.i = cVar;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b e(Long l) {
            this.d = l;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b f(ip1<r90.e.d> ip1Var) {
            this.j = ip1Var;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b g(String str) {
            Objects.requireNonNull(str, "Null generator");
            this.a = str;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b h(int i) {
            this.k = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b i(String str) {
            Objects.requireNonNull(str, "Null identifier");
            this.b = str;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b k(r90.e.AbstractC0277e abstractC0277e) {
            this.h = abstractC0277e;
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b l(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.b
        public r90.e.b m(r90.e.f fVar) {
            this.g = fVar;
            return this;
        }

        public b() {
        }

        public b(r90.e eVar) {
            this.a = eVar.f();
            this.b = eVar.h();
            this.c = Long.valueOf(eVar.k());
            this.d = eVar.d();
            this.e = Boolean.valueOf(eVar.m());
            this.f = eVar.b();
            this.g = eVar.l();
            this.h = eVar.j();
            this.i = eVar.c();
            this.j = eVar.e();
            this.k = Integer.valueOf(eVar.g());
        }
    }

    @Override // defpackage.r90.e
    public r90.e.a b() {
        return this.f;
    }

    @Override // defpackage.r90.e
    public r90.e.c c() {
        return this.i;
    }

    @Override // defpackage.r90.e
    public Long d() {
        return this.d;
    }

    @Override // defpackage.r90.e
    public ip1<r90.e.d> e() {
        return this.j;
    }

    public boolean equals(Object obj) {
        Long l;
        r90.e.f fVar;
        r90.e.AbstractC0277e abstractC0277e;
        r90.e.c cVar;
        ip1<r90.e.d> ip1Var;
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e) {
            r90.e eVar = (r90.e) obj;
            return this.a.equals(eVar.f()) && this.b.equals(eVar.h()) && this.c == eVar.k() && ((l = this.d) != null ? l.equals(eVar.d()) : eVar.d() == null) && this.e == eVar.m() && this.f.equals(eVar.b()) && ((fVar = this.g) != null ? fVar.equals(eVar.l()) : eVar.l() == null) && ((abstractC0277e = this.h) != null ? abstractC0277e.equals(eVar.j()) : eVar.j() == null) && ((cVar = this.i) != null ? cVar.equals(eVar.c()) : eVar.c() == null) && ((ip1Var = this.j) != null ? ip1Var.equals(eVar.e()) : eVar.e() == null) && this.k == eVar.g();
        }
        return false;
    }

    @Override // defpackage.r90.e
    public String f() {
        return this.a;
    }

    @Override // defpackage.r90.e
    public int g() {
        return this.k;
    }

    @Override // defpackage.r90.e
    public String h() {
        return this.b;
    }

    public int hashCode() {
        long j = this.c;
        int hashCode = (((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003;
        Long l = this.d;
        int hashCode2 = (((((hashCode ^ (l == null ? 0 : l.hashCode())) * 1000003) ^ (this.e ? 1231 : 1237)) * 1000003) ^ this.f.hashCode()) * 1000003;
        r90.e.f fVar = this.g;
        int hashCode3 = (hashCode2 ^ (fVar == null ? 0 : fVar.hashCode())) * 1000003;
        r90.e.AbstractC0277e abstractC0277e = this.h;
        int hashCode4 = (hashCode3 ^ (abstractC0277e == null ? 0 : abstractC0277e.hashCode())) * 1000003;
        r90.e.c cVar = this.i;
        int hashCode5 = (hashCode4 ^ (cVar == null ? 0 : cVar.hashCode())) * 1000003;
        ip1<r90.e.d> ip1Var = this.j;
        return ((hashCode5 ^ (ip1Var != null ? ip1Var.hashCode() : 0)) * 1000003) ^ this.k;
    }

    @Override // defpackage.r90.e
    public r90.e.AbstractC0277e j() {
        return this.h;
    }

    @Override // defpackage.r90.e
    public long k() {
        return this.c;
    }

    @Override // defpackage.r90.e
    public r90.e.f l() {
        return this.g;
    }

    @Override // defpackage.r90.e
    public boolean m() {
        return this.e;
    }

    @Override // defpackage.r90.e
    public r90.e.b n() {
        return new b(this);
    }

    public String toString() {
        return "Session{generator=" + this.a + ", identifier=" + this.b + ", startedAt=" + this.c + ", endedAt=" + this.d + ", crashed=" + this.e + ", app=" + this.f + ", user=" + this.g + ", os=" + this.h + ", device=" + this.i + ", events=" + this.j + ", generatorType=" + this.k + "}";
    }

    public rk(String str, String str2, long j, Long l, boolean z, r90.e.a aVar, r90.e.f fVar, r90.e.AbstractC0277e abstractC0277e, r90.e.c cVar, ip1<r90.e.d> ip1Var, int i) {
        this.a = str;
        this.b = str2;
        this.c = j;
        this.d = l;
        this.e = z;
        this.f = aVar;
        this.g = fVar;
        this.h = abstractC0277e;
        this.i = cVar;
        this.j = ip1Var;
        this.k = i;
    }
}
