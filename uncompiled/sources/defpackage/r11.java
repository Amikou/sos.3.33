package defpackage;

/* compiled from: ExtractorOutput.java */
/* renamed from: r11  reason: default package */
/* loaded from: classes.dex */
public interface r11 {
    public static final r11 e = new a();

    /* compiled from: ExtractorOutput.java */
    /* renamed from: r11$a */
    /* loaded from: classes.dex */
    public class a implements r11 {
        @Override // defpackage.r11
        public f84 f(int i, int i2) {
            throw new UnsupportedOperationException();
        }

        @Override // defpackage.r11
        public void m() {
            throw new UnsupportedOperationException();
        }

        @Override // defpackage.r11
        public void p(wi3 wi3Var) {
            throw new UnsupportedOperationException();
        }
    }

    f84 f(int i, int i2);

    void m();

    void p(wi3 wi3Var);
}
