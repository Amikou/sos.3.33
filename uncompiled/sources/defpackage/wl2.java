package defpackage;

import androidx.media3.common.ParserException;
import java.io.IOException;

/* compiled from: OggExtractor.java */
/* renamed from: wl2  reason: default package */
/* loaded from: classes.dex */
public class wl2 implements p11 {
    public r11 a;
    public fu3 b;
    public boolean c;

    static {
        vl2 vl2Var = vl2.b;
    }

    public static /* synthetic */ p11[] d() {
        return new p11[]{new wl2()};
    }

    public static op2 e(op2 op2Var) {
        op2Var.P(0);
        return op2Var;
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        fu3 fu3Var = this.b;
        if (fu3Var != null) {
            fu3Var.m(j, j2);
        }
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        ii.i(this.a);
        if (this.b == null) {
            if (h(q11Var)) {
                q11Var.j();
            } else {
                throw ParserException.createForMalformedContainer("Failed to determine bitstream type", null);
            }
        }
        if (!this.c) {
            f84 f = this.a.f(0, 1);
            this.a.m();
            this.b.d(this.a, f);
            this.c = true;
        }
        return this.b.g(q11Var, ot2Var);
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        try {
            return h(q11Var);
        } catch (ParserException unused) {
            return false;
        }
    }

    public final boolean h(q11 q11Var) throws IOException {
        yl2 yl2Var = new yl2();
        if (yl2Var.a(q11Var, true) && (yl2Var.b & 2) == 2) {
            int min = Math.min(yl2Var.f, 8);
            op2 op2Var = new op2(min);
            q11Var.n(op2Var.d(), 0, min);
            if (q61.p(e(op2Var))) {
                this.b = new q61();
            } else if (cl4.r(e(op2Var))) {
                this.b = new cl4();
            } else if (wn2.p(e(op2Var))) {
                this.b = new wn2();
            }
            return true;
        }
        return false;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.a = r11Var;
    }
}
