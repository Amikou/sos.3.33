package defpackage;

import android.os.RemoteException;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ar5  reason: default package */
/* loaded from: classes.dex */
public final class ar5 implements Runnable {
    public final /* synthetic */ AtomicReference a;
    public final /* synthetic */ zzp f0;
    public final /* synthetic */ p g0;

    public ar5(p pVar, AtomicReference atomicReference, zzp zzpVar) {
        this.g0 = pVar;
        this.a = atomicReference;
        this.f0 = zzpVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AtomicReference atomicReference;
        d dVar;
        synchronized (this.a) {
            try {
            } catch (RemoteException e) {
                this.g0.a.w().l().b("Failed to get app instance id", e);
                atomicReference = this.a;
            }
            if (!this.g0.a.A().s().h()) {
                this.g0.a.w().s().a("Analytics storage consent denied; will not get app instance id");
                this.g0.a.F().p(null);
                this.g0.a.A().g.b(null);
                this.a.set(null);
                this.a.notify();
                return;
            }
            dVar = this.g0.d;
            if (dVar == null) {
                this.g0.a.w().l().a("Failed to get app instance id");
                this.a.notify();
                return;
            }
            zt2.j(this.f0);
            this.a.set(dVar.u(this.f0));
            String str = (String) this.a.get();
            if (str != null) {
                this.g0.a.F().p(str);
                this.g0.a.A().g.b(str);
            }
            this.g0.D();
            atomicReference = this.a;
            atomicReference.notify();
        }
    }
}
