package defpackage;

import defpackage.k5;
import java.util.Arrays;
import kotlin.Result;

/* compiled from: AbstractSharedFlow.kt */
/* renamed from: i5  reason: default package */
/* loaded from: classes2.dex */
public abstract class i5<S extends k5<?>> {
    public S[] a;
    public int f0;
    public int g0;
    public jb2<Integer> h0;

    public final S g() {
        S s;
        jb2<Integer> jb2Var;
        synchronized (this) {
            S[] l = l();
            if (l == null) {
                l = i(2);
                this.a = l;
            } else if (k() >= l.length) {
                Object[] copyOf = Arrays.copyOf(l, l.length * 2);
                fs1.e(copyOf, "java.util.Arrays.copyOf(this, newSize)");
                this.a = (S[]) ((k5[]) copyOf);
                l = (S[]) ((k5[]) copyOf);
            }
            int i = this.g0;
            do {
                s = l[i];
                if (s == null) {
                    s = h();
                    l[i] = s;
                }
                i++;
                if (i >= l.length) {
                    i = 0;
                }
            } while (!s.a(this));
            this.g0 = i;
            this.f0 = k() + 1;
            jb2Var = this.h0;
        }
        if (jb2Var != null) {
            xs3.e(jb2Var, 1);
        }
        return s;
    }

    public abstract S h();

    public abstract S[] i(int i);

    public final void j(S s) {
        jb2<Integer> jb2Var;
        int i;
        q70[] b;
        synchronized (this) {
            this.f0 = k() - 1;
            jb2Var = this.h0;
            i = 0;
            if (k() == 0) {
                this.g0 = 0;
            }
            b = s.b(this);
        }
        int length = b.length;
        while (i < length) {
            q70 q70Var = b[i];
            i++;
            if (q70Var != null) {
                te4 te4Var = te4.a;
                Result.a aVar = Result.Companion;
                q70Var.resumeWith(Result.m52constructorimpl(te4Var));
            }
        }
        if (jb2Var == null) {
            return;
        }
        xs3.e(jb2Var, -1);
    }

    public final int k() {
        return this.f0;
    }

    public final S[] l() {
        return this.a;
    }
}
