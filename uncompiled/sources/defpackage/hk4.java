package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: ViewTokenDetailsBinding.java */
/* renamed from: hk4  reason: default package */
/* loaded from: classes2.dex */
public final class hk4 {
    public final AppCompatTextView a;
    public final MaterialTextView b;
    public final AppCompatTextView c;
    public final AppCompatTextView d;
    public final AppCompatTextView e;

    public hk4(MaterialCardView materialCardView, AppCompatTextView appCompatTextView, MaterialTextView materialTextView, AppCompatTextView appCompatTextView2, AppCompatTextView appCompatTextView3, AppCompatTextView appCompatTextView4) {
        this.a = appCompatTextView;
        this.b = materialTextView;
        this.c = appCompatTextView2;
        this.d = appCompatTextView3;
        this.e = appCompatTextView4;
    }

    public static hk4 a(View view) {
        int i = R.id.txtCirculatingSupply;
        AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtCirculatingSupply);
        if (appCompatTextView != null) {
            i = R.id.txtContractAddress;
            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtContractAddress);
            if (materialTextView != null) {
                i = R.id.txtMarketCap;
                AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txtMarketCap);
                if (appCompatTextView2 != null) {
                    i = R.id.txtTotalSupply;
                    AppCompatTextView appCompatTextView3 = (AppCompatTextView) ai4.a(view, R.id.txtTotalSupply);
                    if (appCompatTextView3 != null) {
                        i = R.id.txtVolume24h;
                        AppCompatTextView appCompatTextView4 = (AppCompatTextView) ai4.a(view, R.id.txtVolume24h);
                        if (appCompatTextView4 != null) {
                            return new hk4((MaterialCardView) view, appCompatTextView, materialTextView, appCompatTextView2, appCompatTextView3, appCompatTextView4);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
