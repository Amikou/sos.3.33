package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapResource.java */
/* renamed from: oq  reason: default package */
/* loaded from: classes.dex */
public class oq implements s73<Bitmap>, oq1 {
    public final Bitmap a;
    public final jq f0;

    public oq(Bitmap bitmap, jq jqVar) {
        this.a = (Bitmap) wt2.e(bitmap, "Bitmap must not be null");
        this.f0 = (jq) wt2.e(jqVar, "BitmapPool must not be null");
    }

    public static oq f(Bitmap bitmap, jq jqVar) {
        if (bitmap == null) {
            return null;
        }
        return new oq(bitmap, jqVar);
    }

    @Override // defpackage.s73
    public int a() {
        return mg4.h(this.a);
    }

    @Override // defpackage.s73
    public void b() {
        this.f0.c(this.a);
    }

    @Override // defpackage.oq1
    public void c() {
        this.a.prepareToDraw();
    }

    @Override // defpackage.s73
    public Class<Bitmap> d() {
        return Bitmap.class;
    }

    @Override // defpackage.s73
    /* renamed from: e */
    public Bitmap get() {
        return this.a;
    }
}
