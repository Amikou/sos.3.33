package defpackage;

import android.os.Bundle;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: pc5  reason: default package */
/* loaded from: classes.dex */
public final class pc5 extends bf5 {
    public final Map<String, Long> b;
    public final Map<String, Integer> c;
    public long d;

    public pc5(ck5 ck5Var) {
        super(ck5Var);
        this.c = new rh();
        this.b = new rh();
    }

    public static /* synthetic */ void i(pc5 pc5Var, String str, long j) {
        pc5Var.e();
        zt2.f(str);
        if (pc5Var.c.isEmpty()) {
            pc5Var.d = j;
        }
        Integer num = pc5Var.c.get(str);
        if (num != null) {
            pc5Var.c.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (pc5Var.c.size() >= 100) {
            pc5Var.a.w().p().a("Too many ads visible");
        } else {
            pc5Var.c.put(str, 1);
            pc5Var.b.put(str, Long.valueOf(j));
        }
    }

    public static /* synthetic */ void j(pc5 pc5Var, String str, long j) {
        pc5Var.e();
        zt2.f(str);
        Integer num = pc5Var.c.get(str);
        if (num != null) {
            bq5 r = pc5Var.a.Q().r(false);
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                pc5Var.c.remove(str);
                Long l = pc5Var.b.get(str);
                if (l == null) {
                    pc5Var.a.w().l().a("First ad unit exposure time was never set");
                } else {
                    long longValue = l.longValue();
                    pc5Var.b.remove(str);
                    pc5Var.n(str, j - longValue, r);
                }
                if (pc5Var.c.isEmpty()) {
                    long j2 = pc5Var.d;
                    if (j2 == 0) {
                        pc5Var.a.w().l().a("First ad exposure time was never set");
                        return;
                    }
                    pc5Var.l(j - j2, r);
                    pc5Var.d = 0L;
                    return;
                }
                return;
            }
            pc5Var.c.put(str, Integer.valueOf(intValue));
            return;
        }
        pc5Var.a.w().l().b("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    public final void f(String str, long j) {
        if (str != null && str.length() != 0) {
            this.a.q().p(new n35(this, str, j));
        } else {
            this.a.w().l().a("Ad unit id must be a non-empty string");
        }
    }

    public final void g(String str, long j) {
        if (str != null && str.length() != 0) {
            this.a.q().p(new f75(this, str, j));
        } else {
            this.a.w().l().a("Ad unit id must be a non-empty string");
        }
    }

    public final void h(long j) {
        bq5 r = this.a.Q().r(false);
        for (String str : this.b.keySet()) {
            n(str, j - this.b.get(str).longValue(), r);
        }
        if (!this.b.isEmpty()) {
            l(j - this.d, r);
        }
        o(j);
    }

    public final void l(long j, bq5 bq5Var) {
        if (bq5Var == null) {
            this.a.w().v().a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            this.a.w().v().b("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            qq5.x(bq5Var, bundle, true);
            this.a.F().X("am", "_xa", bundle);
        }
    }

    public final void n(String str, long j, bq5 bq5Var) {
        if (bq5Var == null) {
            this.a.w().v().a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            this.a.w().v().b("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            qq5.x(bq5Var, bundle, true);
            this.a.F().X("am", "_xu", bundle);
        }
    }

    public final void o(long j) {
        for (String str : this.b.keySet()) {
            this.b.put(str, Long.valueOf(j));
        }
        if (this.b.isEmpty()) {
            return;
        }
        this.d = j;
    }
}
