package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: s16  reason: default package */
/* loaded from: classes.dex */
public final class s16 implements r16 {
    public static final wo5<Boolean> a;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.client.sessions.check_on_reset_and_enable2", true);
        ro5Var.b("measurement.client.sessions.check_on_startup", true);
        ro5Var.b("measurement.client.sessions.start_session_before_view_screen", true);
    }

    @Override // defpackage.r16
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.r16
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
