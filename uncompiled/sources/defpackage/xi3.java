package defpackage;

import androidx.media3.common.util.b;

/* compiled from: SeekParameters.java */
/* renamed from: xi3  reason: default package */
/* loaded from: classes.dex */
public final class xi3 {
    public static final xi3 c;
    public static final xi3 d;
    public final long a;
    public final long b;

    static {
        xi3 xi3Var = new xi3(0L, 0L);
        c = xi3Var;
        new xi3(Long.MAX_VALUE, Long.MAX_VALUE);
        new xi3(Long.MAX_VALUE, 0L);
        new xi3(0L, Long.MAX_VALUE);
        d = xi3Var;
    }

    public xi3(long j, long j2) {
        ii.a(j >= 0);
        ii.a(j2 >= 0);
        this.a = j;
        this.b = j2;
    }

    public long a(long j, long j2, long j3) {
        long j4 = this.a;
        if (j4 == 0 && this.b == 0) {
            return j;
        }
        long P0 = b.P0(j, j4, Long.MIN_VALUE);
        long b = b.b(j, this.b, Long.MAX_VALUE);
        boolean z = true;
        boolean z2 = P0 <= j2 && j2 <= b;
        if (P0 > j3 || j3 > b) {
            z = false;
        }
        return (z2 && z) ? Math.abs(j2 - j) <= Math.abs(j3 - j) ? j2 : j3 : z2 ? j2 : z ? j3 : P0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || xi3.class != obj.getClass()) {
            return false;
        }
        xi3 xi3Var = (xi3) obj;
        return this.a == xi3Var.a && this.b == xi3Var.b;
    }

    public int hashCode() {
        return (((int) this.a) * 31) + ((int) this.b);
    }
}
