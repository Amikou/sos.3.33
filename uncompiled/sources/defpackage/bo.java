package defpackage;

/* compiled from: BaseUrl.java */
/* renamed from: bo  reason: default package */
/* loaded from: classes.dex */
public final class bo {
    public final String a;
    public final String b;
    public final int c;
    public final int d;

    public bo(String str, String str2, int i, int i2) {
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof bo) {
            bo boVar = (bo) obj;
            return this.c == boVar.c && this.d == boVar.d && ql2.a(this.a, boVar.a) && ql2.a(this.b, boVar.b);
        }
        return false;
    }

    public int hashCode() {
        return ql2.b(this.a, this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
    }
}
