package defpackage;

import androidx.media3.exoplayer.source.s;

/* compiled from: CompositeSequenceableLoader.java */
/* renamed from: p40  reason: default package */
/* loaded from: classes.dex */
public class p40 implements s {
    public final s[] a;

    public p40(s[] sVarArr) {
        this.a = sVarArr;
    }

    @Override // androidx.media3.exoplayer.source.s
    public final long a() {
        long j = Long.MAX_VALUE;
        for (s sVar : this.a) {
            long a = sVar.a();
            if (a != Long.MIN_VALUE) {
                j = Math.min(j, a);
            }
        }
        if (j == Long.MAX_VALUE) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.source.s
    public boolean d(long j) {
        s[] sVarArr;
        boolean z;
        boolean z2 = false;
        do {
            long a = a();
            if (a == Long.MIN_VALUE) {
                break;
            }
            z = false;
            for (s sVar : this.a) {
                long a2 = sVar.a();
                boolean z3 = a2 != Long.MIN_VALUE && a2 <= j;
                if (a2 == a || z3) {
                    z |= sVar.d(j);
                }
            }
            z2 |= z;
        } while (z);
        return z2;
    }

    @Override // androidx.media3.exoplayer.source.s
    public boolean e() {
        for (s sVar : this.a) {
            if (sVar.e()) {
                return true;
            }
        }
        return false;
    }

    @Override // androidx.media3.exoplayer.source.s
    public final long g() {
        long j = Long.MAX_VALUE;
        for (s sVar : this.a) {
            long g = sVar.g();
            if (g != Long.MIN_VALUE) {
                j = Math.min(j, g);
            }
        }
        if (j == Long.MAX_VALUE) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    @Override // androidx.media3.exoplayer.source.s
    public final void h(long j) {
        for (s sVar : this.a) {
            sVar.h(j);
        }
    }
}
