package defpackage;

import android.os.Handler;
import android.os.Looper;
import defpackage.tl0;
import java.util.ArrayList;

/* compiled from: DeferredReleaserConcurrentImpl.java */
/* renamed from: ul0  reason: default package */
/* loaded from: classes.dex */
public class ul0 extends tl0 {
    public final Object b = new Object();
    public final Runnable f = new a();
    public ArrayList<tl0.a> d = new ArrayList<>();
    public ArrayList<tl0.a> e = new ArrayList<>();
    public final Handler c = new Handler(Looper.getMainLooper());

    /* compiled from: DeferredReleaserConcurrentImpl.java */
    /* renamed from: ul0$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (ul0.this.b) {
                ArrayList arrayList = ul0.this.e;
                ul0 ul0Var = ul0.this;
                ul0Var.e = ul0Var.d;
                ul0.this.d = arrayList;
            }
            int size = ul0.this.e.size();
            for (int i = 0; i < size; i++) {
                ((tl0.a) ul0.this.e.get(i)).a();
            }
            ul0.this.e.clear();
        }
    }

    @Override // defpackage.tl0
    public void a(tl0.a aVar) {
        synchronized (this.b) {
            this.d.remove(aVar);
        }
    }

    @Override // defpackage.tl0
    public void d(tl0.a aVar) {
        if (!tl0.c()) {
            aVar.a();
            return;
        }
        synchronized (this.b) {
            if (this.d.contains(aVar)) {
                return;
            }
            this.d.add(aVar);
            boolean z = true;
            if (this.d.size() != 1) {
                z = false;
            }
            if (z) {
                this.c.post(this.f);
            }
        }
    }
}
