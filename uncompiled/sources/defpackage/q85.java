package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: q85  reason: default package */
/* loaded from: classes.dex */
public final class q85 extends o65 {
    public q85() {
        this.a.add(zzbl.ADD);
        this.a.add(zzbl.DIVIDE);
        this.a.add(zzbl.MODULUS);
        this.a.add(zzbl.MULTIPLY);
        this.a.add(zzbl.NEGATE);
        this.a.add(zzbl.POST_DECREMENT);
        this.a.add(zzbl.POST_INCREMENT);
        this.a.add(zzbl.PRE_DECREMENT);
        this.a.add(zzbl.PRE_INCREMENT);
        this.a.add(zzbl.SUBTRACT);
    }

    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        zzbl zzblVar = zzbl.ADD;
        int ordinal = vm5.e(str).ordinal();
        if (ordinal == 0) {
            vm5.a(zzblVar.name(), 2, list);
            z55 a = wk5Var.a(list.get(0));
            z55 a2 = wk5Var.a(list.get(1));
            if (!(a instanceof m55) && !(a instanceof f65) && !(a2 instanceof m55) && !(a2 instanceof f65)) {
                return new z45(Double.valueOf(a.b().doubleValue() + a2.b().doubleValue()));
            }
            String valueOf = String.valueOf(a.zzc());
            String valueOf2 = String.valueOf(a2.zzc());
            return new f65(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        } else if (ordinal == 21) {
            vm5.a(zzbl.DIVIDE.name(), 2, list);
            return new z45(Double.valueOf(wk5Var.a(list.get(0)).b().doubleValue() / wk5Var.a(list.get(1)).b().doubleValue()));
        } else if (ordinal == 59) {
            vm5.a(zzbl.SUBTRACT.name(), 2, list);
            return new z45(Double.valueOf(wk5Var.a(list.get(0)).b().doubleValue() + new z45(Double.valueOf(-wk5Var.a(list.get(1)).b().doubleValue())).b().doubleValue()));
        } else if (ordinal == 52 || ordinal == 53) {
            vm5.a(str, 2, list);
            z55 a3 = wk5Var.a(list.get(0));
            wk5Var.a(list.get(1));
            return a3;
        } else if (ordinal != 55 && ordinal != 56) {
            switch (ordinal) {
                case 44:
                    vm5.a(zzbl.MODULUS.name(), 2, list);
                    return new z45(Double.valueOf(wk5Var.a(list.get(0)).b().doubleValue() % wk5Var.a(list.get(1)).b().doubleValue()));
                case 45:
                    vm5.a(zzbl.MULTIPLY.name(), 2, list);
                    return new z45(Double.valueOf(wk5Var.a(list.get(0)).b().doubleValue() * wk5Var.a(list.get(1)).b().doubleValue()));
                case 46:
                    vm5.a(zzbl.NEGATE.name(), 1, list);
                    return new z45(Double.valueOf(-wk5Var.a(list.get(0)).b().doubleValue()));
                default:
                    return super.b(str);
            }
        } else {
            vm5.a(str, 1, list);
            return wk5Var.a(list.get(0));
        }
    }
}
