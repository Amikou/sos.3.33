package defpackage;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/* compiled from: ImageViewTargetFactory.java */
/* renamed from: fp1  reason: default package */
/* loaded from: classes.dex */
public class fp1 {
    public <Z> ek4<ImageView, Z> a(ImageView imageView, Class<Z> cls) {
        if (Bitmap.class.equals(cls)) {
            return new bq(imageView);
        }
        if (Drawable.class.isAssignableFrom(cls)) {
            return new vq0(imageView);
        }
        throw new IllegalArgumentException("Unhandled class: " + cls + ", try .as*(Class).transcode(ResourceTranscoder)");
    }
}
