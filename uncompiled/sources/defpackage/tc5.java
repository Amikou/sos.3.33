package defpackage;

import android.os.Bundle;
import defpackage.kb;
import defpackage.tf;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: tc5  reason: default package */
/* loaded from: classes2.dex */
public final class tc5 implements tf.a {
    public final /* synthetic */ ff5 a;

    public tc5(ff5 ff5Var) {
        this.a = ff5Var;
    }

    @Override // defpackage.em5
    public final void a(String str, String str2, Bundle bundle, long j) {
        kb.b bVar;
        if (this.a.a.contains(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("events", ha5.f(str2));
            bVar = this.a.b;
            bVar.a(2, bundle2);
        }
    }
}
