package defpackage;

import kotlin.io.FileWalkDirection;

/* renamed from: x31  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class x31 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[FileWalkDirection.values().length];
        a = iArr;
        iArr[FileWalkDirection.TOP_DOWN.ordinal()] = 1;
        iArr[FileWalkDirection.BOTTOM_UP.ordinal()] = 2;
    }
}
