package defpackage;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.facebook.imagepipeline.image.a;

/* compiled from: DefaultDrawableFactory.java */
/* renamed from: ti0  reason: default package */
/* loaded from: classes.dex */
public class ti0 implements uq0 {
    public final Resources a;
    public final uq0 b;

    public ti0(Resources resources, uq0 uq0Var) {
        this.a = resources;
        this.b = uq0Var;
    }

    public static boolean c(c00 c00Var) {
        return (c00Var.l() == 1 || c00Var.l() == 0) ? false : true;
    }

    public static boolean d(c00 c00Var) {
        return (c00Var.m() == 0 || c00Var.m() == -1) ? false : true;
    }

    @Override // defpackage.uq0
    public boolean a(a aVar) {
        return true;
    }

    @Override // defpackage.uq0
    public Drawable b(a aVar) {
        try {
            if (nc1.d()) {
                nc1.a("DefaultDrawableFactory#createDrawable");
            }
            if (aVar instanceof c00) {
                c00 c00Var = (c00) aVar;
                BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a, c00Var.f());
                if (d(c00Var) || c(c00Var)) {
                    zn2 zn2Var = new zn2(bitmapDrawable, c00Var.m(), c00Var.l());
                    if (nc1.d()) {
                        nc1.b();
                    }
                    return zn2Var;
                }
                return bitmapDrawable;
            }
            uq0 uq0Var = this.b;
            if (uq0Var != null && uq0Var.a(aVar)) {
                Drawable b = this.b.b(aVar);
                if (nc1.d()) {
                    nc1.b();
                }
                return b;
            }
            if (nc1.d()) {
                nc1.b();
            }
            return null;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }
}
