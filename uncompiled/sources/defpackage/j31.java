package defpackage;

import defpackage.r90;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;

/* compiled from: FileBackedNativeSessionFile.java */
/* renamed from: j31  reason: default package */
/* loaded from: classes2.dex */
public class j31 implements td2 {
    public final File a;
    public final String b;
    public final String c;

    public j31(String str, String str2, File file) {
        this.b = str;
        this.c = str2;
        this.a = file;
    }

    public final byte[] a() {
        byte[] bArr = new byte[8192];
        try {
            InputStream c = c();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            if (c == null) {
                gZIPOutputStream.close();
                byteArrayOutputStream.close();
                if (c != null) {
                    c.close();
                }
                return null;
            }
            while (true) {
                try {
                    int read = c.read(bArr);
                    if (read > 0) {
                        gZIPOutputStream.write(bArr, 0, read);
                    } else {
                        gZIPOutputStream.finish();
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        gZIPOutputStream.close();
                        byteArrayOutputStream.close();
                        c.close();
                        return byteArray;
                    }
                } catch (Throwable th) {
                    try {
                        gZIPOutputStream.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // defpackage.td2
    public InputStream c() {
        if (this.a.exists() && this.a.isFile()) {
            try {
                return new FileInputStream(this.a);
            } catch (FileNotFoundException unused) {
            }
        }
        return null;
    }

    @Override // defpackage.td2
    public String d() {
        return this.c;
    }

    @Override // defpackage.td2
    public r90.d.b e() {
        byte[] a = a();
        if (a != null) {
            return r90.d.b.a().b(a).c(this.b).a();
        }
        return null;
    }
}
