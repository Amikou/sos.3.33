package defpackage;

import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: EventLoop.common.kt */
/* renamed from: xx0  reason: default package */
/* loaded from: classes2.dex */
public abstract class xx0 extends CoroutineDispatcher {
    public long f0;
    public boolean g0;
    public th<qp0<?>> h0;

    public static /* synthetic */ void S(xx0 xx0Var, boolean z, int i, Object obj) {
        if (obj != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: incrementUseCount");
        }
        if ((i & 1) != 0) {
            z = false;
        }
        xx0Var.R(z);
    }

    public static /* synthetic */ void m(xx0 xx0Var, boolean z, int i, Object obj) {
        if (obj != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: decrementUseCount");
        }
        if ((i & 1) != 0) {
            z = false;
        }
        xx0Var.l(z);
    }

    public final long M(boolean z) {
        return z ? 4294967296L : 1L;
    }

    public final void N(qp0<?> qp0Var) {
        th<qp0<?>> thVar = this.h0;
        if (thVar == null) {
            thVar = new th<>();
            this.h0 = thVar;
        }
        thVar.a(qp0Var);
    }

    public long Q() {
        th<qp0<?>> thVar = this.h0;
        return (thVar == null || thVar.c()) ? Long.MAX_VALUE : 0L;
    }

    public final void R(boolean z) {
        this.f0 += M(z);
        if (z) {
            return;
        }
        this.g0 = true;
    }

    public final boolean W() {
        return this.f0 >= M(true);
    }

    public final boolean X() {
        th<qp0<?>> thVar = this.h0;
        if (thVar == null) {
            return true;
        }
        return thVar.c();
    }

    public long a0() {
        return !b0() ? Long.MAX_VALUE : 0L;
    }

    public final boolean b0() {
        qp0<?> d;
        th<qp0<?>> thVar = this.h0;
        if (thVar == null || (d = thVar.d()) == null) {
            return false;
        }
        d.run();
        return true;
    }

    public boolean e0() {
        return false;
    }

    public final void l(boolean z) {
        long M = this.f0 - M(z);
        this.f0 = M;
        if (M > 0) {
            return;
        }
        if (ze0.a()) {
            if (!(this.f0 == 0)) {
                throw new AssertionError();
            }
        }
        if (this.g0) {
            shutdown();
        }
    }

    public void shutdown() {
    }
}
