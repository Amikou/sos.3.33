package defpackage;

import com.google.android.play.core.internal.d;

/* renamed from: ew4  reason: default package */
/* loaded from: classes2.dex */
public final class ew4<T> implements jw4<T> {
    public jw4<T> a;

    public static <T> void b(jw4<T> jw4Var, jw4<T> jw4Var2) {
        d.j(jw4Var2);
        ew4 ew4Var = (ew4) jw4Var;
        if (ew4Var.a != null) {
            throw new IllegalStateException();
        }
        ew4Var.a = jw4Var2;
    }

    @Override // defpackage.jw4
    public final T a() {
        jw4<T> jw4Var = this.a;
        if (jw4Var != null) {
            return jw4Var.a();
        }
        throw new IllegalStateException();
    }
}
