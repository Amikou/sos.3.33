package defpackage;

import com.google.android.play.core.assetpacks.n;
import java.io.File;
import java.io.FilenameFilter;

/* renamed from: dx4  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class dx4 implements FilenameFilter {
    public static final FilenameFilter a = new dx4();

    @Override // java.io.FilenameFilter
    public final boolean accept(File file, String str) {
        boolean matches;
        matches = n.a.matcher(str).matches();
        return matches;
    }
}
