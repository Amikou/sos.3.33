package defpackage;

import java.util.Arrays;

/* compiled from: Objects.java */
/* renamed from: ol2  reason: default package */
/* loaded from: classes.dex */
public final class ol2 {

    /* compiled from: Objects.java */
    /* renamed from: ol2$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final String a;
        public final a b;
        public a c;
        public boolean d;

        /* compiled from: Objects.java */
        /* renamed from: ol2$b$a */
        /* loaded from: classes.dex */
        public static final class a {
            public String a;
            public Object b;
            public a c;

            public a() {
            }
        }

        public b a(String str, int i) {
            return e(str, String.valueOf(i));
        }

        public b b(String str, Object obj) {
            return e(str, obj);
        }

        public b c(String str, boolean z) {
            return e(str, String.valueOf(z));
        }

        public final a d() {
            a aVar = new a();
            this.c.c = aVar;
            this.c = aVar;
            return aVar;
        }

        public final b e(String str, Object obj) {
            a d = d();
            d.b = obj;
            d.a = (String) xt2.g(str);
            return this;
        }

        public String toString() {
            boolean z = this.d;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.a);
            sb.append('{');
            String str = "";
            for (a aVar = this.b.c; aVar != null; aVar = aVar.c) {
                Object obj = aVar.b;
                if (!z || obj != null) {
                    sb.append(str);
                    String str2 = aVar.a;
                    if (str2 != null) {
                        sb.append(str2);
                        sb.append('=');
                    }
                    if (obj != null && obj.getClass().isArray()) {
                        String deepToString = Arrays.deepToString(new Object[]{obj});
                        sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
                    } else {
                        sb.append(obj);
                    }
                    str = ", ";
                }
            }
            sb.append('}');
            return sb.toString();
        }

        public b(String str) {
            a aVar = new a();
            this.b = aVar;
            this.c = aVar;
            this.d = false;
            this.a = (String) xt2.g(str);
        }
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int b(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static b c(Object obj) {
        return new b(obj.getClass().getSimpleName());
    }
}
