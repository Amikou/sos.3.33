package defpackage;

import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.c;
import java.io.File;
import java.io.IOException;

/* renamed from: pw4  reason: default package */
/* loaded from: classes2.dex */
public final class pw4 {
    public static final it4 b = new it4("MergeSliceTaskHandler");
    public final c a;

    public pw4(c cVar) {
        this.a = cVar;
    }

    public static void b(File file, File file2) {
        File[] listFiles;
        if (!file.isDirectory()) {
            if (file2.exists()) {
                String valueOf = String.valueOf(file2);
                StringBuilder sb = new StringBuilder(valueOf.length() + 51);
                sb.append("File clashing with existing file from other slice: ");
                sb.append(valueOf);
                throw new bj(sb.toString());
            } else if (file.renameTo(file2)) {
                return;
            } else {
                String valueOf2 = String.valueOf(file);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 21);
                sb2.append("Unable to move file: ");
                sb2.append(valueOf2);
                throw new bj(sb2.toString());
            }
        }
        file2.mkdirs();
        for (File file3 : file.listFiles()) {
            b(file3, new File(file2, file3.getName()));
        }
        if (file.delete()) {
            return;
        }
        String valueOf3 = String.valueOf(file);
        StringBuilder sb3 = new StringBuilder(valueOf3.length() + 28);
        sb3.append("Unable to delete directory: ");
        sb3.append(valueOf3);
        throw new bj(sb3.toString());
    }

    public final void a(ow4 ow4Var) {
        File w = this.a.w(ow4Var.b, ow4Var.c, ow4Var.d, ow4Var.e);
        if (!w.exists()) {
            throw new bj(String.format("Cannot find verified files for slice %s.", ow4Var.e), ow4Var.a);
        }
        File x = this.a.x(ow4Var.b, ow4Var.c, ow4Var.d);
        if (!x.exists()) {
            x.mkdirs();
        }
        b(w, x);
        try {
            this.a.z(ow4Var.b, ow4Var.c, ow4Var.d, this.a.y(ow4Var.b, ow4Var.c, ow4Var.d) + 1);
        } catch (IOException e) {
            b.b("Writing merge checkpoint failed with %s.", e.getMessage());
            throw new bj("Writing merge checkpoint failed.", e, ow4Var.a);
        }
    }
}
