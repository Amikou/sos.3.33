package defpackage;

import android.content.Context;

/* compiled from: DelayedConsentInitializationParameters.java */
/* renamed from: zl0  reason: default package */
/* loaded from: classes2.dex */
public class zl0 {
    public final Context a;
    public final String b;

    public zl0(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    public String a() {
        return this.b;
    }

    public Context b() {
        return this.a;
    }
}
