package defpackage;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

/* compiled from: LazyBitmapDrawableResource.java */
/* renamed from: uy1  reason: default package */
/* loaded from: classes.dex */
public final class uy1 implements s73<BitmapDrawable>, oq1 {
    public final Resources a;
    public final s73<Bitmap> f0;

    public uy1(Resources resources, s73<Bitmap> s73Var) {
        this.a = (Resources) wt2.d(resources);
        this.f0 = (s73) wt2.d(s73Var);
    }

    public static s73<BitmapDrawable> f(Resources resources, s73<Bitmap> s73Var) {
        if (s73Var == null) {
            return null;
        }
        return new uy1(resources, s73Var);
    }

    @Override // defpackage.s73
    public int a() {
        return this.f0.a();
    }

    @Override // defpackage.s73
    public void b() {
        this.f0.b();
    }

    @Override // defpackage.oq1
    public void c() {
        s73<Bitmap> s73Var = this.f0;
        if (s73Var instanceof oq1) {
            ((oq1) s73Var).c();
        }
    }

    @Override // defpackage.s73
    public Class<BitmapDrawable> d() {
        return BitmapDrawable.class;
    }

    @Override // defpackage.s73
    /* renamed from: e */
    public BitmapDrawable get() {
        return new BitmapDrawable(this.a, this.f0.get());
    }
}
