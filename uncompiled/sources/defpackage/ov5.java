package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ov5  reason: default package */
/* loaded from: classes.dex */
public final class ov5 implements Runnable {
    public final /* synthetic */ hw5 a;
    public final /* synthetic */ fw5 f0;

    public ov5(fw5 fw5Var, hw5 hw5Var) {
        this.f0 = fw5Var;
        this.a = hw5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5.B(this.f0, this.a);
        this.f0.R();
    }
}
