package defpackage;

import android.os.Looper;
import android.os.Message;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: k05  reason: default package */
/* loaded from: classes.dex */
public final class k05 extends q25 {
    public final /* synthetic */ i05 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k05(i05 i05Var, Looper looper) {
        super(looper);
        this.a = i05Var;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((q05) message.obj).b(this.a);
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
