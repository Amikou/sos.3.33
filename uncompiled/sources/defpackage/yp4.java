package defpackage;

import android.text.TextUtils;
import androidx.work.ExistingWorkPolicy;
import androidx.work.d;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: WorkContinuationImpl.java */
/* renamed from: yp4  reason: default package */
/* loaded from: classes.dex */
public class yp4 extends xp4 {
    public static final String j = v12.f("WorkContinuationImpl");
    public final hq4 a;
    public final String b;
    public final ExistingWorkPolicy c;
    public final List<? extends d> d;
    public final List<String> e;
    public final List<String> f;
    public final List<yp4> g;
    public boolean h;
    public kn2 i;

    public yp4(hq4 hq4Var, List<? extends d> list) {
        this(hq4Var, null, ExistingWorkPolicy.KEEP, list, null);
    }

    public static boolean i(yp4 yp4Var, Set<String> set) {
        set.addAll(yp4Var.c());
        Set<String> l = l(yp4Var);
        for (String str : set) {
            if (l.contains(str)) {
                return true;
            }
        }
        List<yp4> e = yp4Var.e();
        if (e != null && !e.isEmpty()) {
            for (yp4 yp4Var2 : e) {
                if (i(yp4Var2, set)) {
                    return true;
                }
            }
        }
        set.removeAll(yp4Var.c());
        return false;
    }

    public static Set<String> l(yp4 yp4Var) {
        HashSet hashSet = new HashSet();
        List<yp4> e = yp4Var.e();
        if (e != null && !e.isEmpty()) {
            for (yp4 yp4Var2 : e) {
                hashSet.addAll(yp4Var2.c());
            }
        }
        return hashSet;
    }

    public kn2 a() {
        if (!this.h) {
            tv0 tv0Var = new tv0(this);
            this.a.r().b(tv0Var);
            this.i = tv0Var.d();
        } else {
            v12.c().h(j, String.format("Already enqueued work ids (%s)", TextUtils.join(", ", this.e)), new Throwable[0]);
        }
        return this.i;
    }

    public ExistingWorkPolicy b() {
        return this.c;
    }

    public List<String> c() {
        return this.e;
    }

    public String d() {
        return this.b;
    }

    public List<yp4> e() {
        return this.g;
    }

    public List<? extends d> f() {
        return this.d;
    }

    public hq4 g() {
        return this.a;
    }

    public boolean h() {
        return i(this, new HashSet());
    }

    public boolean j() {
        return this.h;
    }

    public void k() {
        this.h = true;
    }

    public yp4(hq4 hq4Var, String str, ExistingWorkPolicy existingWorkPolicy, List<? extends d> list) {
        this(hq4Var, str, existingWorkPolicy, list, null);
    }

    public yp4(hq4 hq4Var, String str, ExistingWorkPolicy existingWorkPolicy, List<? extends d> list, List<yp4> list2) {
        this.a = hq4Var;
        this.b = str;
        this.c = existingWorkPolicy;
        this.d = list;
        this.g = list2;
        this.e = new ArrayList(list.size());
        this.f = new ArrayList();
        if (list2 != null) {
            for (yp4 yp4Var : list2) {
                this.f.addAll(yp4Var.f);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            String a = list.get(i).a();
            this.e.add(a);
            this.f.add(a);
        }
    }
}
