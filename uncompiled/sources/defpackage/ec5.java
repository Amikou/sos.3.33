package defpackage;

import com.google.android.gms.internal.clearcut.o;
import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzco;

/* renamed from: ec5  reason: default package */
/* loaded from: classes.dex */
public class ec5 {
    public volatile o a;
    public volatile zzbb b;

    static {
        n95.b();
    }

    public final int a() {
        if (this.b != null) {
            return this.b.size();
        }
        if (this.a != null) {
            return this.a.j();
        }
        return 0;
    }

    public final o b(o oVar) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    try {
                        this.a = oVar;
                        this.b = zzbb.zzfi;
                    } catch (zzco unused) {
                        this.a = oVar;
                        this.b = zzbb.zzfi;
                    }
                }
            }
        }
        return this.a;
    }

    public final o c(o oVar) {
        o oVar2 = this.a;
        this.b = null;
        this.a = oVar;
        return oVar2;
    }

    public final zzbb d() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                return this.b;
            }
            this.b = this.a == null ? zzbb.zzfi : this.a.e();
            return this.b;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ec5) {
            ec5 ec5Var = (ec5) obj;
            o oVar = this.a;
            o oVar2 = ec5Var.a;
            return (oVar == null && oVar2 == null) ? d().equals(ec5Var.d()) : (oVar == null || oVar2 == null) ? oVar != null ? oVar.equals(ec5Var.b(oVar.a())) : b(oVar2.a()).equals(oVar2) : oVar.equals(oVar2);
        }
        return false;
    }

    public int hashCode() {
        return 1;
    }
}
