package defpackage;

import defpackage.xk4;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/* compiled from: TransformablePage.kt */
/* renamed from: xa4  reason: default package */
/* loaded from: classes.dex */
public final class xa4<T> {
    public final int[] a;
    public final List<T> b;
    public final int c;
    public final List<Integer> d;
    public static final a f = new a(null);
    public static final xa4<Object> e = new xa4<>(0, b20.g());

    /* compiled from: TransformablePage.kt */
    /* renamed from: xa4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public final xa4<Object> a() {
            return xa4.e;
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public xa4(int[] iArr, List<? extends T> list, int i, List<Integer> list2) {
        fs1.f(iArr, "originalPageOffsets");
        fs1.f(list, "data");
        this.a = iArr;
        this.b = list;
        this.c = i;
        this.d = list2;
        boolean z = false;
        if (!(iArr.length == 0)) {
            if ((list2 == null || list2.size() == list.size()) ? true : z) {
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("If originalIndices (size = ");
            fs1.d(list2);
            sb.append(list2.size());
            sb.append(") is provided,");
            sb.append(" it must be same length as data (size = ");
            sb.append(list.size());
            sb.append(')');
            throw new IllegalArgumentException(sb.toString().toString());
        }
        throw new IllegalArgumentException("originalPageOffsets cannot be empty when constructing TransformablePage".toString());
    }

    public final List<T> b() {
        return this.b;
    }

    public final int[] c() {
        return this.a;
    }

    public final xk4.a d(int i, int i2, int i3, int i4, int i5) {
        sr1 h;
        int i6 = this.c;
        List<Integer> list = this.d;
        if (list != null && (h = b20.h(list)) != null && h.s(i)) {
            i = this.d.get(i).intValue();
        }
        return new xk4.a(i6, i, i2, i3, i4, i5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!fs1.b(xa4.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type androidx.paging.TransformablePage<*>");
        xa4 xa4Var = (xa4) obj;
        return Arrays.equals(this.a, xa4Var.a) && !(fs1.b(this.b, xa4Var.b) ^ true) && this.c == xa4Var.c && !(fs1.b(this.d, xa4Var.d) ^ true);
    }

    public int hashCode() {
        int hashCode = ((((Arrays.hashCode(this.a) * 31) + this.b.hashCode()) * 31) + this.c) * 31;
        List<Integer> list = this.d;
        return hashCode + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        return "TransformablePage(originalPageOffsets=" + Arrays.toString(this.a) + ", data=" + this.b + ", hintOriginalPageOffset=" + this.c + ", hintOriginalIndices=" + this.d + ")";
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public xa4(int i, List<? extends T> list) {
        this(new int[]{i}, list, i, null);
        fs1.f(list, "data");
    }
}
