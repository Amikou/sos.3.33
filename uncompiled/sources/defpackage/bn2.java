package defpackage;

import android.content.ContentValues;
import android.database.Cursor;

/* compiled from: OneSignalDb.java */
/* renamed from: bn2  reason: default package */
/* loaded from: classes2.dex */
public interface bn2 {
    int a(String str, ContentValues contentValues, String str2, String[] strArr);

    Cursor b(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6);

    Cursor c(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5);

    void d(String str, String str2, String[] strArr);

    void e(String str, String str2, ContentValues contentValues);
}
