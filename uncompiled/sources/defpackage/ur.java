package defpackage;

import okio.b;

/* compiled from: Buffer.kt */
/* renamed from: ur */
/* loaded from: classes2.dex */
public final class ur {
    public static final byte[] a = b.a("0123456789abcdef");

    public static final byte[] a() {
        return a;
    }

    public static final boolean b(bj3 bj3Var, int i, byte[] bArr, int i2, int i3) {
        fs1.f(bj3Var, "segment");
        fs1.f(bArr, "bytes");
        int i4 = bj3Var.c;
        byte[] bArr2 = bj3Var.a;
        while (i2 < i3) {
            if (i == i4) {
                bj3Var = bj3Var.f;
                fs1.d(bj3Var);
                byte[] bArr3 = bj3Var.a;
                bArr2 = bArr3;
                i = bj3Var.b;
                i4 = bj3Var.c;
            }
            if (bArr2[i] != bArr[i2]) {
                return false;
            }
            i++;
            i2++;
        }
        return true;
    }

    public static final String c(b bVar, long j) {
        fs1.f(bVar, "$this$readUtf8Line");
        if (j > 0) {
            long j2 = j - 1;
            if (bVar.j(j2) == ((byte) 13)) {
                String S = bVar.S(j2);
                bVar.skip(2L);
                return S;
            }
        }
        String S2 = bVar.S(j);
        bVar.skip(1L);
        return S2;
    }

    public static final int d(b bVar, un2 un2Var, boolean z) {
        int i;
        int i2;
        int i3;
        int i4;
        bj3 bj3Var;
        fs1.f(bVar, "$this$selectPrefix");
        fs1.f(un2Var, "options");
        bj3 bj3Var2 = bVar.a;
        if (bj3Var2 == null) {
            return z ? -2 : -1;
        }
        byte[] bArr = bj3Var2.a;
        int i5 = bj3Var2.b;
        int i6 = bj3Var2.c;
        int[] n = un2Var.n();
        bj3 bj3Var3 = bj3Var2;
        int i7 = -1;
        int i8 = 0;
        loop0: while (true) {
            int i9 = i8 + 1;
            int i10 = n[i8];
            int i11 = i9 + 1;
            int i12 = n[i9];
            if (i12 != -1) {
                i7 = i12;
            }
            if (bj3Var3 == null) {
                break;
            }
            if (i10 >= 0) {
                i = i5 + 1;
                int i13 = bArr[i5] & 255;
                int i14 = i11 + i10;
                while (i11 != i14) {
                    if (i13 == n[i11]) {
                        i2 = n[i11 + i10];
                        if (i == i6) {
                            bj3Var3 = bj3Var3.f;
                            fs1.d(bj3Var3);
                            i = bj3Var3.b;
                            bArr = bj3Var3.a;
                            i6 = bj3Var3.c;
                            if (bj3Var3 == bj3Var2) {
                                bj3Var3 = null;
                            }
                        }
                    } else {
                        i11++;
                    }
                }
                return i7;
            }
            int i15 = i11 + (i10 * (-1));
            while (true) {
                int i16 = i5 + 1;
                int i17 = i11 + 1;
                if ((bArr[i5] & 255) != n[i11]) {
                    return i7;
                }
                boolean z2 = i17 == i15;
                if (i16 == i6) {
                    fs1.d(bj3Var3);
                    bj3 bj3Var4 = bj3Var3.f;
                    fs1.d(bj3Var4);
                    i4 = bj3Var4.b;
                    byte[] bArr2 = bj3Var4.a;
                    i3 = bj3Var4.c;
                    if (bj3Var4 != bj3Var2) {
                        bj3Var = bj3Var4;
                        bArr = bArr2;
                    } else if (!z2) {
                        break loop0;
                    } else {
                        bArr = bArr2;
                        bj3Var = null;
                    }
                } else {
                    bj3 bj3Var5 = bj3Var3;
                    i3 = i6;
                    i4 = i16;
                    bj3Var = bj3Var5;
                }
                if (z2) {
                    i2 = n[i17];
                    i = i4;
                    i6 = i3;
                    bj3Var3 = bj3Var;
                    break;
                }
                i5 = i4;
                i6 = i3;
                i11 = i17;
                bj3Var3 = bj3Var;
            }
            if (i2 >= 0) {
                return i2;
            }
            i8 = -i2;
            i5 = i;
        }
        if (z) {
            return -2;
        }
        return i7;
    }

    public static /* synthetic */ int e(b bVar, un2 un2Var, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return d(bVar, un2Var, z);
    }
}
