package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.f;
import java.util.ArrayList;

/* compiled from: Grouping.java */
/* renamed from: vi1  reason: default package */
/* loaded from: classes.dex */
public class vi1 {
    public static dp4 a(ConstraintWidget constraintWidget, int i, ArrayList<dp4> arrayList, dp4 dp4Var) {
        int i2;
        int p1;
        if (i == 0) {
            i2 = constraintWidget.M0;
        } else {
            i2 = constraintWidget.N0;
        }
        if (i2 != -1 && (dp4Var == null || i2 != dp4Var.b)) {
            int i3 = 0;
            while (true) {
                if (i3 >= arrayList.size()) {
                    break;
                }
                dp4 dp4Var2 = arrayList.get(i3);
                if (dp4Var2.c() == i2) {
                    if (dp4Var != null) {
                        dp4Var.g(i, dp4Var2);
                        arrayList.remove(dp4Var);
                    }
                    dp4Var = dp4Var2;
                } else {
                    i3++;
                }
            }
        } else if (i2 != -1) {
            return dp4Var;
        }
        if (dp4Var == null) {
            if ((constraintWidget instanceof mk1) && (p1 = ((mk1) constraintWidget).p1(i)) != -1) {
                int i4 = 0;
                while (true) {
                    if (i4 >= arrayList.size()) {
                        break;
                    }
                    dp4 dp4Var3 = arrayList.get(i4);
                    if (dp4Var3.c() == p1) {
                        dp4Var = dp4Var3;
                        break;
                    }
                    i4++;
                }
            }
            if (dp4Var == null) {
                dp4Var = new dp4(i);
            }
            arrayList.add(dp4Var);
        }
        if (dp4Var.a(constraintWidget)) {
            if (constraintWidget instanceof f) {
                f fVar = (f) constraintWidget;
                fVar.o1().c(fVar.p1() == 0 ? 1 : 0, arrayList, dp4Var);
            }
            if (i == 0) {
                constraintWidget.M0 = dp4Var.c();
                constraintWidget.M.c(i, arrayList, dp4Var);
                constraintWidget.O.c(i, arrayList, dp4Var);
            } else {
                constraintWidget.N0 = dp4Var.c();
                constraintWidget.N.c(i, arrayList, dp4Var);
                constraintWidget.Q.c(i, arrayList, dp4Var);
                constraintWidget.P.c(i, arrayList, dp4Var);
            }
            constraintWidget.T.c(i, arrayList, dp4Var);
        }
        return dp4Var;
    }

    public static dp4 b(ArrayList<dp4> arrayList, int i) {
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            dp4 dp4Var = arrayList.get(i2);
            if (i == dp4Var.b) {
                return dp4Var;
            }
        }
        return null;
    }

    /* JADX WARN: Removed duplicated region for block: B:179:0x0354  */
    /* JADX WARN: Removed duplicated region for block: B:191:0x0390  */
    /* JADX WARN: Removed duplicated region for block: B:194:0x0395 A[ADDED_TO_REGION] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean c(androidx.constraintlayout.core.widgets.d r16, defpackage.jo.b r17) {
        /*
            Method dump skipped, instructions count: 924
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.vi1.c(androidx.constraintlayout.core.widgets.d, jo$b):boolean");
    }

    public static boolean d(ConstraintWidget.DimensionBehaviour dimensionBehaviour, ConstraintWidget.DimensionBehaviour dimensionBehaviour2, ConstraintWidget.DimensionBehaviour dimensionBehaviour3, ConstraintWidget.DimensionBehaviour dimensionBehaviour4) {
        ConstraintWidget.DimensionBehaviour dimensionBehaviour5;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour6;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour7 = ConstraintWidget.DimensionBehaviour.FIXED;
        return (dimensionBehaviour3 == dimensionBehaviour7 || dimensionBehaviour3 == (dimensionBehaviour6 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) || (dimensionBehaviour3 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT && dimensionBehaviour != dimensionBehaviour6)) || (dimensionBehaviour4 == dimensionBehaviour7 || dimensionBehaviour4 == (dimensionBehaviour5 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) || (dimensionBehaviour4 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT && dimensionBehaviour2 != dimensionBehaviour5));
    }
}
