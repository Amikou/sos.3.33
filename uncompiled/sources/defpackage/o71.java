package defpackage;

import defpackage.be1;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: Context.kt */
/* renamed from: o71 */
/* loaded from: classes2.dex */
public final /* synthetic */ class o71 {
    public static final <T> j71<T> a(j71<? extends T> j71Var, int i, BufferOverflow bufferOverflow) {
        int i2;
        BufferOverflow bufferOverflow2;
        boolean z = true;
        if (i >= 0 || i == -2 || i == -1) {
            if (i == -1 && bufferOverflow != BufferOverflow.SUSPEND) {
                z = false;
            }
            if (z) {
                if (i == -1) {
                    bufferOverflow2 = BufferOverflow.DROP_OLDEST;
                    i2 = 0;
                } else {
                    i2 = i;
                    bufferOverflow2 = bufferOverflow;
                }
                return j71Var instanceof be1 ? be1.a.a((be1) j71Var, null, i2, bufferOverflow2, 1, null) : new px(j71Var, null, i2, bufferOverflow2, 2, null);
            }
            throw new IllegalArgumentException("CONFLATED capacity cannot be used with non-default onBufferOverflow".toString());
        }
        throw new IllegalArgumentException(fs1.l("Buffer size should be non-negative, BUFFERED, or CONFLATED, but was ", Integer.valueOf(i)).toString());
    }

    public static /* synthetic */ j71 b(j71 j71Var, int i, BufferOverflow bufferOverflow, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = -2;
        }
        if ((i2 & 2) != 0) {
            bufferOverflow = BufferOverflow.SUSPEND;
        }
        return n71.a(j71Var, i, bufferOverflow);
    }

    public static final <T> j71<T> c(j71<? extends T> j71Var) {
        j71<T> b;
        b = b(j71Var, -1, null, 2, null);
        return b;
    }
}
