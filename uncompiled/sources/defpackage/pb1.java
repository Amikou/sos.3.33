package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentWalletBinding.java */
/* renamed from: pb1  reason: default package */
/* loaded from: classes2.dex */
public final class pb1 {
    public final wf a;
    public final wn4 b;
    public final RecyclerView c;
    public final RecyclerView d;
    public final SwipeRefreshLayout e;
    public final TextView f;
    public final TextView g;

    public pb1(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FrameLayout frameLayout, wf wfVar, wn4 wn4Var, LinearLayout linearLayout, LinearLayout linearLayout2, RecyclerView recyclerView, RecyclerView recyclerView2, SwipeRefreshLayout swipeRefreshLayout, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5) {
        this.a = wfVar;
        this.b = wn4Var;
        this.c = recyclerView;
        this.d = recyclerView2;
        this.e = swipeRefreshLayout;
        this.f = textView3;
        this.g = textView4;
    }

    public static pb1 a(View view) {
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        int i = R.id.headerContainer;
        FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.headerContainer);
        if (frameLayout != null) {
            i = R.id.lHeader;
            View a = ai4.a(view, R.id.lHeader);
            if (a != null) {
                wf a2 = wf.a(a);
                i = R.id.lMain;
                View a3 = ai4.a(view, R.id.lMain);
                if (a3 != null) {
                    wn4 a4 = wn4.a(a3);
                    i = R.id.llGainersHeader;
                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.llGainersHeader);
                    if (linearLayout != null) {
                        i = R.id.llMyTokensHeader;
                        LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.llMyTokensHeader);
                        if (linearLayout2 != null) {
                            i = R.id.rvLatest;
                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvLatest);
                            if (recyclerView != null) {
                                i = R.id.rvYourTokens;
                                RecyclerView recyclerView2 = (RecyclerView) ai4.a(view, R.id.rvYourTokens);
                                if (recyclerView2 != null) {
                                    i = R.id.swipeRefreshLayout;
                                    SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swipeRefreshLayout);
                                    if (swipeRefreshLayout != null) {
                                        i = R.id.tvBased;
                                        TextView textView = (TextView) ai4.a(view, R.id.tvBased);
                                        if (textView != null) {
                                            i = R.id.tvGainer;
                                            TextView textView2 = (TextView) ai4.a(view, R.id.tvGainer);
                                            if (textView2 != null) {
                                                i = R.id.tvSeeAll;
                                                TextView textView3 = (TextView) ai4.a(view, R.id.tvSeeAll);
                                                if (textView3 != null) {
                                                    i = R.id.tvSeeAllTokens;
                                                    TextView textView4 = (TextView) ai4.a(view, R.id.tvSeeAllTokens);
                                                    if (textView4 != null) {
                                                        i = R.id.tvYourTokens;
                                                        TextView textView5 = (TextView) ai4.a(view, R.id.tvYourTokens);
                                                        if (textView5 != null) {
                                                            return new pb1(constraintLayout, constraintLayout, frameLayout, a2, a4, linearLayout, linearLayout2, recyclerView, recyclerView2, swipeRefreshLayout, textView, textView2, textView3, textView4, textView5);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
