package defpackage;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import okio.ByteString;
import okio.b;
import okio.d;
import okio.k;
import okio.m;
import okio.n;
import okio.o;

/* compiled from: RealBufferedSource.kt */
/* renamed from: d43  reason: default package */
/* loaded from: classes2.dex */
public final class d43 implements d {
    public final b a;
    public boolean f0;
    public final n g0;

    public d43(n nVar) {
        fs1.f(nVar, "source");
        this.g0 = nVar;
        this.a = new b();
    }

    @Override // okio.d
    public void A1(long j) {
        if (!V0(j)) {
            throw new EOFException();
        }
    }

    @Override // okio.d
    public long E1() {
        byte j;
        A1(1L);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!V0(i2)) {
                break;
            }
            j = this.a.j(i);
            if ((j < ((byte) 48) || j > ((byte) 57)) && ((j < ((byte) 97) || j > ((byte) 102)) && (j < ((byte) 65) || j > ((byte) 70)))) {
                break;
            }
            i = i2;
        }
        if (i == 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Expected leading [0-9a-fA-F] character but was 0x");
            String num = Integer.toString(j, xx.a(xx.a(16)));
            fs1.e(num, "java.lang.Integer.toStri…(this, checkRadix(radix))");
            sb.append(num);
            throw new NumberFormatException(sb.toString());
        }
        return this.a.E1();
    }

    @Override // okio.d
    public boolean G0(long j, ByteString byteString) {
        fs1.f(byteString, "bytes");
        return c(j, byteString, 0, byteString.size());
    }

    @Override // okio.d
    public InputStream G1() {
        return new a();
    }

    @Override // okio.d
    public b I() {
        return this.a;
    }

    @Override // okio.d
    public String I0(Charset charset) {
        fs1.f(charset, "charset");
        this.a.P0(this.g0);
        return this.a.I0(charset);
    }

    @Override // okio.d
    public ByteString J(long j) {
        A1(j);
        return this.a.J(j);
    }

    @Override // okio.d
    public ByteString R0() {
        this.a.P0(this.g0);
        return this.a.R0();
    }

    @Override // okio.d
    public boolean V0(long j) {
        if (j >= 0) {
            if (!this.f0) {
                while (this.a.a0() < j) {
                    if (this.g0.read(this.a, 8192) == -1) {
                        return false;
                    }
                }
                return true;
            }
            throw new IllegalStateException("closed".toString());
        }
        throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
    }

    @Override // okio.d
    public byte[] Z() {
        this.a.P0(this.g0);
        return this.a.Z();
    }

    public long a(byte b) {
        return b(b, 0L, Long.MAX_VALUE);
    }

    @Override // okio.d
    public String a1() {
        return p0(Long.MAX_VALUE);
    }

    public long b(byte b, long j, long j2) {
        boolean z = true;
        if (!this.f0) {
            if (!((0 > j || j2 < j) ? false : false)) {
                throw new IllegalArgumentException(("fromIndex=" + j + " toIndex=" + j2).toString());
            }
            while (j < j2) {
                long l = this.a.l(b, j, j2);
                if (l != -1) {
                    return l;
                }
                long a0 = this.a.a0();
                if (a0 >= j2 || this.g0.read(this.a, 8192) == -1) {
                    return -1L;
                }
                j = Math.max(j, a0);
            }
            return -1L;
        }
        throw new IllegalStateException("closed".toString());
    }

    public boolean c(long j, ByteString byteString, int i, int i2) {
        int i3;
        fs1.f(byteString, "bytes");
        if (!this.f0) {
            if (j >= 0 && i >= 0 && i2 >= 0 && byteString.size() - i >= i2) {
                for (i3 = 0; i3 < i2; i3 = i3 + 1) {
                    long j2 = i3 + j;
                    i3 = (V0(1 + j2) && this.a.j(j2) == byteString.getByte(i + i3)) ? i3 + 1 : 0;
                }
                return true;
            }
            return false;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.d
    public boolean c0() {
        if (!this.f0) {
            return this.a.c0() && this.g0.read(this.a, (long) 8192) == -1;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (this.f0) {
            return;
        }
        this.f0 = true;
        this.g0.close();
        this.a.a();
    }

    public int d() {
        A1(4L);
        return this.a.N();
    }

    public short e() {
        A1(2L);
        return this.a.Q();
    }

    @Override // okio.d
    public byte[] g1(long j) {
        A1(j);
        return this.a.g1(j);
    }

    @Override // okio.d
    public int h0(un2 un2Var) {
        fs1.f(un2Var, "options");
        if (!this.f0) {
            while (true) {
                int d = ur.d(this.a, un2Var, true);
                if (d == -2) {
                    if (this.g0.read(this.a, 8192) == -1) {
                        break;
                    }
                } else if (d != -1) {
                    this.a.skip(un2Var.m()[d].size());
                    return d;
                }
            }
            return -1;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return !this.f0;
    }

    @Override // okio.d
    public String j1() {
        this.a.P0(this.g0);
        return this.a.j1();
    }

    @Override // okio.d
    public void k0(b bVar, long j) {
        fs1.f(bVar, "sink");
        try {
            A1(j);
            this.a.k0(bVar, j);
        } catch (EOFException e) {
            bVar.P0(this.a);
            throw e;
        }
    }

    @Override // okio.d
    public b o() {
        return this.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x002c, code lost:
        if (r4 == 0) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x002f, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append("Expected leading [0-9] or '-' character but was 0x");
        r2 = java.lang.Integer.toString(r8, defpackage.xx.a(defpackage.xx.a(16)));
        defpackage.fs1.e(r2, "java.lang.Integer.toStri…(this, checkRadix(radix))");
        r1.append(r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0058, code lost:
        throw new java.lang.NumberFormatException(r1.toString());
     */
    @Override // okio.d
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long o0() {
        /*
            r10 = this;
            r0 = 1
            r10.A1(r0)
            r2 = 0
            r4 = r2
        L8:
            long r6 = r4 + r0
            boolean r8 = r10.V0(r6)
            if (r8 == 0) goto L59
            okio.b r8 = r10.a
            byte r8 = r8.j(r4)
            r9 = 48
            byte r9 = (byte) r9
            if (r8 < r9) goto L20
            r9 = 57
            byte r9 = (byte) r9
            if (r8 <= r9) goto L2a
        L20:
            int r4 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r4 != 0) goto L2c
            r5 = 45
            byte r5 = (byte) r5
            if (r8 == r5) goto L2a
            goto L2c
        L2a:
            r4 = r6
            goto L8
        L2c:
            if (r4 == 0) goto L2f
            goto L59
        L2f:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected leading [0-9] or '-' character but was 0x"
            r1.append(r2)
            r2 = 16
            int r2 = defpackage.xx.a(r2)
            int r2 = defpackage.xx.a(r2)
            java.lang.String r2 = java.lang.Integer.toString(r8, r2)
            java.lang.String r3 = "java.lang.Integer.toStri…(this, checkRadix(radix))"
            defpackage.fs1.e(r2, r3)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L59:
            okio.b r0 = r10.a
            long r0 = r0.o0()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.d43.o0():long");
    }

    @Override // okio.d
    public String p0(long j) {
        if (j >= 0) {
            long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
            byte b = (byte) 10;
            long b2 = b(b, 0L, j2);
            if (b2 != -1) {
                return ur.c(this.a, b2);
            }
            if (j2 < Long.MAX_VALUE && V0(j2) && this.a.j(j2 - 1) == ((byte) 13) && V0(1 + j2) && this.a.j(j2) == b) {
                return ur.c(this.a, j2);
            }
            b bVar = new b();
            b bVar2 = this.a;
            bVar2.f(bVar, 0L, Math.min(32, bVar2.a0()));
            throw new EOFException("\\n not found: limit=" + Math.min(this.a.a0(), j) + " content=" + bVar.R0().hex() + "…");
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    @Override // okio.d
    public d peek() {
        return k.d(new fq2(this));
    }

    @Override // okio.n
    public long read(b bVar, long j) {
        fs1.f(bVar, "sink");
        if (j >= 0) {
            if (!this.f0) {
                if (this.a.a0() == 0 && this.g0.read(this.a, 8192) == -1) {
                    return -1L;
                }
                return this.a.read(bVar, Math.min(j, this.a.a0()));
            }
            throw new IllegalStateException("closed".toString());
        }
        throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
    }

    @Override // okio.d
    public byte readByte() {
        A1(1L);
        return this.a.readByte();
    }

    @Override // okio.d
    public void readFully(byte[] bArr) {
        fs1.f(bArr, "sink");
        try {
            A1(bArr.length);
            this.a.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (this.a.a0() > 0) {
                b bVar = this.a;
                int read = bVar.read(bArr, i, (int) bVar.a0());
                if (read == -1) {
                    throw new AssertionError();
                }
                i += read;
            }
            throw e;
        }
    }

    @Override // okio.d
    public int readInt() {
        A1(4L);
        return this.a.readInt();
    }

    @Override // okio.d
    public long readLong() {
        A1(8L);
        return this.a.readLong();
    }

    @Override // okio.d
    public short readShort() {
        A1(2L);
        return this.a.readShort();
    }

    @Override // okio.d
    public void skip(long j) {
        if (!(!this.f0)) {
            throw new IllegalStateException("closed".toString());
        }
        while (j > 0) {
            if (this.a.a0() == 0 && this.g0.read(this.a, 8192) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.a.a0());
            this.a.skip(min);
            j -= min;
        }
    }

    @Override // okio.d
    public long t1(m mVar) {
        fs1.f(mVar, "sink");
        long j = 0;
        while (this.g0.read(this.a, 8192) != -1) {
            long d = this.a.d();
            if (d > 0) {
                j += d;
                mVar.write(this.a, d);
            }
        }
        if (this.a.a0() > 0) {
            long a0 = j + this.a.a0();
            b bVar = this.a;
            mVar.write(bVar, bVar.a0());
            return a0;
        }
        return j;
    }

    @Override // okio.n
    public o timeout() {
        return this.g0.timeout();
    }

    public String toString() {
        return "buffer(" + this.g0 + ')';
    }

    /* compiled from: RealBufferedSource.kt */
    /* renamed from: d43$a */
    /* loaded from: classes2.dex */
    public static final class a extends InputStream {
        public a() {
        }

        @Override // java.io.InputStream
        public int available() {
            d43 d43Var = d43.this;
            if (!d43Var.f0) {
                return (int) Math.min(d43Var.a.a0(), Integer.MAX_VALUE);
            }
            throw new IOException("closed");
        }

        @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            d43.this.close();
        }

        @Override // java.io.InputStream
        public int read() {
            d43 d43Var = d43.this;
            if (!d43Var.f0) {
                if (d43Var.a.a0() == 0) {
                    d43 d43Var2 = d43.this;
                    if (d43Var2.g0.read(d43Var2.a, 8192) == -1) {
                        return -1;
                    }
                }
                return d43.this.a.readByte() & 255;
            }
            throw new IOException("closed");
        }

        public String toString() {
            return d43.this + ".inputStream()";
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            fs1.f(bArr, "data");
            if (!d43.this.f0) {
                c.b(bArr.length, i, i2);
                if (d43.this.a.a0() == 0) {
                    d43 d43Var = d43.this;
                    if (d43Var.g0.read(d43Var.a, 8192) == -1) {
                        return -1;
                    }
                }
                return d43.this.a.read(bArr, i, i2);
            }
            throw new IOException("closed");
        }
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) {
        fs1.f(byteBuffer, "sink");
        if (this.a.a0() == 0 && this.g0.read(this.a, 8192) == -1) {
            return -1;
        }
        return this.a.read(byteBuffer);
    }
}
