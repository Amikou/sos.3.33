package defpackage;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/* compiled from: StrictLineReader.java */
/* renamed from: ku3  reason: default package */
/* loaded from: classes.dex */
public class ku3 implements Closeable {
    public final InputStream a;
    public final Charset f0;
    public byte[] g0;
    public int h0;
    public int i0;

    /* compiled from: StrictLineReader.java */
    /* renamed from: ku3$a */
    /* loaded from: classes.dex */
    public class a extends ByteArrayOutputStream {
        public a(int i) {
            super(i);
        }

        @Override // java.io.ByteArrayOutputStream
        public String toString() {
            int i = ((ByteArrayOutputStream) this).count;
            if (i > 0 && ((ByteArrayOutputStream) this).buf[i - 1] == 13) {
                i--;
            }
            try {
                return new String(((ByteArrayOutputStream) this).buf, 0, i, ku3.this.f0.name());
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
    }

    public ku3(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    public final void b() throws IOException {
        InputStream inputStream = this.a;
        byte[] bArr = this.g0;
        int read = inputStream.read(bArr, 0, bArr.length);
        if (read != -1) {
            this.h0 = 0;
            this.i0 = read;
            return;
        }
        throw new EOFException();
    }

    public boolean c() {
        return this.i0 == -1;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this.a) {
            if (this.g0 != null) {
                this.g0 = null;
                this.a.close();
            }
        }
    }

    public String d() throws IOException {
        int i;
        byte[] bArr;
        int i2;
        synchronized (this.a) {
            if (this.g0 != null) {
                if (this.h0 >= this.i0) {
                    b();
                }
                for (int i3 = this.h0; i3 != this.i0; i3++) {
                    byte[] bArr2 = this.g0;
                    if (bArr2[i3] == 10) {
                        int i4 = this.h0;
                        if (i3 != i4) {
                            i2 = i3 - 1;
                            if (bArr2[i2] == 13) {
                                String str = new String(bArr2, i4, i2 - i4, this.f0.name());
                                this.h0 = i3 + 1;
                                return str;
                            }
                        }
                        i2 = i3;
                        String str2 = new String(bArr2, i4, i2 - i4, this.f0.name());
                        this.h0 = i3 + 1;
                        return str2;
                    }
                }
                a aVar = new a((this.i0 - this.h0) + 80);
                loop1: while (true) {
                    byte[] bArr3 = this.g0;
                    int i5 = this.h0;
                    aVar.write(bArr3, i5, this.i0 - i5);
                    this.i0 = -1;
                    b();
                    i = this.h0;
                    while (i != this.i0) {
                        bArr = this.g0;
                        if (bArr[i] == 10) {
                            break loop1;
                        }
                        i++;
                    }
                }
                int i6 = this.h0;
                if (i != i6) {
                    aVar.write(bArr, i6, i - i6);
                }
                this.h0 = i + 1;
                return aVar.toString();
            }
            throw new IOException("LineReader is closed");
        }
    }

    public ku3(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw null;
        }
        if (i >= 0) {
            if (charset.equals(og4.a)) {
                this.a = inputStream;
                this.f0 = charset;
                this.g0 = new byte[i];
                return;
            }
            throw new IllegalArgumentException("Unsupported encoding");
        }
        throw new IllegalArgumentException("capacity <= 0");
    }
}
