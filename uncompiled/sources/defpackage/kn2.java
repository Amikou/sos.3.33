package defpackage;

import android.annotation.SuppressLint;

/* compiled from: Operation.java */
/* renamed from: kn2  reason: default package */
/* loaded from: classes.dex */
public interface kn2 {
    @SuppressLint({"SyntheticAccessor"})
    public static final b.c a = new b.c();
    @SuppressLint({"SyntheticAccessor"})
    public static final b.C0195b b = new b.C0195b();

    /* compiled from: Operation.java */
    /* renamed from: kn2$b */
    /* loaded from: classes.dex */
    public static abstract class b {

        /* compiled from: Operation.java */
        /* renamed from: kn2$b$a */
        /* loaded from: classes.dex */
        public static final class a extends b {
            public final Throwable a;

            public a(Throwable th) {
                this.a = th;
            }

            public Throwable a() {
                return this.a;
            }

            public String toString() {
                return String.format("FAILURE (%s)", this.a.getMessage());
            }
        }

        /* compiled from: Operation.java */
        /* renamed from: kn2$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0195b extends b {
            public String toString() {
                return "IN_PROGRESS";
            }

            public C0195b() {
            }
        }

        /* compiled from: Operation.java */
        /* renamed from: kn2$b$c */
        /* loaded from: classes.dex */
        public static final class c extends b {
            public String toString() {
                return "SUCCESS";
            }

            public c() {
            }
        }
    }
}
