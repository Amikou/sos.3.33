package defpackage;

import android.annotation.SuppressLint;
import java.util.ArrayDeque;
import java.util.Queue;
import kotlin.coroutines.CoroutineContext;

/* compiled from: DispatchQueue.kt */
/* renamed from: mp0  reason: default package */
/* loaded from: classes.dex */
public final class mp0 {
    public boolean b;
    public boolean c;
    public boolean a = true;
    public final Queue<Runnable> d = new ArrayDeque();

    /* compiled from: DispatchQueue.kt */
    /* renamed from: mp0$a */
    /* loaded from: classes.dex */
    public static final class a implements Runnable {
        public final /* synthetic */ CoroutineContext f0;
        public final /* synthetic */ Runnable g0;

        public a(CoroutineContext coroutineContext, Runnable runnable) {
            this.f0 = coroutineContext;
            this.g0 = runnable;
        }

        @Override // java.lang.Runnable
        public final void run() {
            mp0.this.e(this.g0);
        }
    }

    public final boolean b() {
        return this.b || !this.a;
    }

    @SuppressLint({"WrongThread"})
    public final void c(CoroutineContext coroutineContext, Runnable runnable) {
        fs1.f(coroutineContext, "context");
        fs1.f(runnable, "runnable");
        e32 l = tp0.c().l();
        if (!l.j(coroutineContext) && !b()) {
            e(runnable);
        } else {
            l.h(coroutineContext, new a(coroutineContext, runnable));
        }
    }

    public final void d() {
        if (this.c) {
            return;
        }
        try {
            this.c = true;
            while ((!this.d.isEmpty()) && b()) {
                Runnable poll = this.d.poll();
                if (poll != null) {
                    poll.run();
                }
            }
        } finally {
            this.c = false;
        }
    }

    public final void e(Runnable runnable) {
        if (this.d.offer(runnable)) {
            d();
            return;
        }
        throw new IllegalStateException("cannot enqueue any more runnables".toString());
    }

    public final void f() {
        this.b = true;
        d();
    }

    public final void g() {
        this.a = true;
    }

    public final void h() {
        if (this.a) {
            if (!this.b) {
                this.a = false;
                d();
                return;
            }
            throw new IllegalStateException("Cannot resume a finished dispatcher".toString());
        }
    }
}
