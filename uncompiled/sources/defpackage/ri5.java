package defpackage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.internal.measurement.f;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ri5  reason: default package */
/* loaded from: classes.dex */
public final class ri5 implements ServiceConnection {
    public final String a;
    public final /* synthetic */ ui5 f0;

    public ri5(ui5 ui5Var, String str) {
        this.f0 = ui5Var;
        this.a = str;
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder != null) {
            try {
                f F1 = e95.F1(iBinder);
                if (F1 == null) {
                    this.f0.a.w().p().a("Install Referrer Service implementation was not found");
                    return;
                }
                this.f0.a.w().v().a("Install Referrer Service connected");
                this.f0.a.q().p(new oi5(this, F1, this));
                return;
            } catch (RuntimeException e) {
                this.f0.a.w().p().b("Exception occurred while calling Install Referrer API", e);
                return;
            }
        }
        this.f0.a.w().p().a("Install Referrer connection returned with null binder");
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        this.f0.a.w().v().a("Install Referrer Service disconnected");
    }
}
