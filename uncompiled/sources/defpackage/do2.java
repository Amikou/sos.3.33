package defpackage;

import java.io.OutputStream;
import okio.b;
import okio.m;
import okio.o;

/* compiled from: JvmOkio.kt */
/* renamed from: do2  reason: default package */
/* loaded from: classes2.dex */
public final class do2 implements m {
    public final OutputStream a;
    public final o f0;

    public do2(OutputStream outputStream, o oVar) {
        fs1.f(outputStream, "out");
        fs1.f(oVar, "timeout");
        this.a = outputStream;
        this.f0 = oVar;
    }

    @Override // okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        this.a.close();
    }

    @Override // okio.m, java.io.Flushable
    public void flush() {
        this.a.flush();
    }

    @Override // okio.m
    public o timeout() {
        return this.f0;
    }

    public String toString() {
        return "sink(" + this.a + ')';
    }

    @Override // okio.m
    public void write(b bVar, long j) {
        fs1.f(bVar, "source");
        c.b(bVar.a0(), 0L, j);
        while (j > 0) {
            this.f0.throwIfReached();
            bj3 bj3Var = bVar.a;
            fs1.d(bj3Var);
            int min = (int) Math.min(j, bj3Var.c - bj3Var.b);
            this.a.write(bj3Var.a, bj3Var.b, min);
            bj3Var.b += min;
            long j2 = min;
            j -= j2;
            bVar.X(bVar.a0() - j2);
            if (bj3Var.b == bj3Var.c) {
                bVar.a = bj3Var.b();
                dj3.b(bj3Var);
            }
        }
    }
}
