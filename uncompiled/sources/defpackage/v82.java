package defpackage;

import android.graphics.PointF;
import android.graphics.Rect;
import android.net.Uri;
import defpackage.l80;
import java.util.Map;

/* compiled from: MiddlewareUtils.java */
/* renamed from: v82  reason: default package */
/* loaded from: classes.dex */
public class v82 {
    public static l80.a a(Map<String, Object> map, Map<String, Object> map2, Map<String, Object> map3, Rect rect, String str, PointF pointF, Map<String, Object> map4, Object obj, Uri uri) {
        l80.a aVar = new l80.a();
        if (rect != null) {
            rect.width();
            rect.height();
        }
        aVar.a = obj;
        return aVar;
    }
}
