package defpackage;

/* renamed from: ey2  reason: default package */
/* loaded from: classes.dex */
public final class ey2 {
    public static final int alpha = 2130968628;
    public static final int drawerLayoutStyle = 2130968931;
    public static final int elevation = 2130968947;
    public static final int font = 2130969021;
    public static final int fontProviderAuthority = 2130969023;
    public static final int fontProviderCerts = 2130969024;
    public static final int fontProviderFetchStrategy = 2130969025;
    public static final int fontProviderFetchTimeout = 2130969026;
    public static final int fontProviderPackage = 2130969027;
    public static final int fontProviderQuery = 2130969028;
    public static final int fontStyle = 2130969030;
    public static final int fontVariationSettings = 2130969031;
    public static final int fontWeight = 2130969032;
    public static final int ttcIndex = 2130969779;
}
