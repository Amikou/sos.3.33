package defpackage;

import com.google.crypto.tink.a;
import com.google.crypto.tink.proto.x;
import com.google.crypto.tink.q;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

/* compiled from: KmsEnvelopeAead.java */
/* renamed from: ux1  reason: default package */
/* loaded from: classes2.dex */
public final class ux1 implements a {
    public static final byte[] c = new byte[0];
    public final x a;
    public final a b;

    public ux1(x xVar, a aVar) {
        this.a = xVar;
        this.b = aVar;
    }

    @Override // com.google.crypto.tink.a
    public byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        byte[] byteArray = q.o(this.a).toByteArray();
        return c(this.b.a(byteArray, c), ((a) q.i(this.a.J(), byteArray, a.class)).a(bArr, bArr2));
    }

    @Override // com.google.crypto.tink.a
    public byte[] b(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        try {
            ByteBuffer wrap = ByteBuffer.wrap(bArr);
            int i = wrap.getInt();
            if (i > 0 && i <= bArr.length - 4) {
                byte[] bArr3 = new byte[i];
                wrap.get(bArr3, 0, i);
                byte[] bArr4 = new byte[wrap.remaining()];
                wrap.get(bArr4, 0, wrap.remaining());
                return ((a) q.i(this.a.J(), this.b.b(bArr3, c), a.class)).b(bArr4, bArr2);
            }
            throw new GeneralSecurityException("invalid ciphertext");
        } catch (IndexOutOfBoundsException | NegativeArraySizeException | BufferUnderflowException e) {
            throw new GeneralSecurityException("invalid ciphertext", e);
        }
    }

    public final byte[] c(byte[] bArr, byte[] bArr2) {
        return ByteBuffer.allocate(bArr.length + 4 + bArr2.length).putInt(bArr.length).put(bArr).put(bArr2).array();
    }
}
