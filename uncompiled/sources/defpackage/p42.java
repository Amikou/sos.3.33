package defpackage;

import android.graphics.drawable.Drawable;
import android.view.View;

/* compiled from: MaterialShapeUtils.java */
/* renamed from: p42  reason: default package */
/* loaded from: classes2.dex */
public class p42 {
    public static w80 a(int i) {
        if (i != 0) {
            if (i != 1) {
                return b();
            }
            return new ad0();
        }
        return new v93();
    }

    public static w80 b() {
        return new v93();
    }

    public static cu0 c() {
        return new cu0();
    }

    public static void d(View view, float f) {
        Drawable background = view.getBackground();
        if (background instanceof o42) {
            ((o42) background).Z(f);
        }
    }

    public static void e(View view) {
        Drawable background = view.getBackground();
        if (background instanceof o42) {
            f(view, (o42) background);
        }
    }

    public static void f(View view, o42 o42Var) {
        if (o42Var.R()) {
            o42Var.e0(mk4.g(view));
        }
    }
}
