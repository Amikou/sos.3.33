package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.play.core.splitinstall.l;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: yy4  reason: default package */
/* loaded from: classes2.dex */
public final class yy4 extends mu4<tr3> {
    public static yy4 j;
    public final Handler g;
    public final ix4 h;
    public final Set<ur3> i;

    public yy4(Context context, ix4 ix4Var) {
        super(new it4("SplitInstallListenerRegistry"), new IntentFilter("com.google.android.play.core.splitinstall.receiver.SplitInstallUpdateIntentService"), context);
        this.g = new Handler(Looper.getMainLooper());
        this.i = new LinkedHashSet();
        this.h = ix4Var;
    }

    public static synchronized yy4 f(Context context) {
        yy4 yy4Var;
        synchronized (yy4.class) {
            if (j == null) {
                j = new yy4(context, l.a);
            }
            yy4Var = j;
        }
        return yy4Var;
    }

    @Override // defpackage.mu4
    public final void a(Context context, Intent intent) {
        Bundle bundleExtra = intent.getBundleExtra("session_state");
        if (bundleExtra == null) {
            return;
        }
        tr3 e = tr3.e(bundleExtra);
        this.a.a("ListenerRegistryBroadcastReceiver.onReceive: %s", e);
        lx4 a = this.h.a();
        if (e.i() != 3 || a == null) {
            g(e);
        } else {
            a.a(e.d(), new ty4(this, e, intent, context));
        }
    }

    public final synchronized void g(tr3 tr3Var) {
        Iterator it = new LinkedHashSet(this.i).iterator();
        while (it.hasNext()) {
            ((ur3) it.next()).a(tr3Var);
        }
        super.d(tr3Var);
    }
}
