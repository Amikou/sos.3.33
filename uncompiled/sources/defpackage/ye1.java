package defpackage;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.generic.a;
import defpackage.qc3;

/* compiled from: GenericDraweeHierarchy.java */
/* renamed from: ye1  reason: default package */
/* loaded from: classes.dex */
public class ye1 implements vm3 {
    public final Drawable a;
    public final Resources b;
    public RoundingParams c;
    public final l93 d;
    public final b21 e;
    public final d91 f;

    public ye1(ze1 ze1Var) {
        ColorDrawable colorDrawable = new ColorDrawable(0);
        this.a = colorDrawable;
        if (nc1.d()) {
            nc1.a("GenericDraweeHierarchy()");
        }
        this.b = ze1Var.p();
        this.c = ze1Var.s();
        d91 d91Var = new d91(colorDrawable);
        this.f = d91Var;
        int i = 1;
        int size = ze1Var.j() != null ? ze1Var.j().size() : 1;
        int i2 = (size == 0 ? 1 : size) + (ze1Var.m() != null ? 1 : 0);
        Drawable[] drawableArr = new Drawable[i2 + 6];
        drawableArr[0] = i(ze1Var.e(), null);
        drawableArr[1] = i(ze1Var.k(), ze1Var.l());
        drawableArr[2] = h(d91Var, ze1Var.d(), ze1Var.c(), ze1Var.b());
        drawableArr[3] = i(ze1Var.n(), ze1Var.o());
        drawableArr[4] = i(ze1Var.q(), ze1Var.r());
        drawableArr[5] = i(ze1Var.h(), ze1Var.i());
        if (i2 > 0) {
            if (ze1Var.j() != null) {
                i = 0;
                for (Drawable drawable : ze1Var.j()) {
                    drawableArr[i + 6] = i(drawable, null);
                    i++;
                }
            }
            if (ze1Var.m() != null) {
                drawableArr[i + 6] = i(ze1Var.m(), null);
            }
        }
        b21 b21Var = new b21(drawableArr, false, 2);
        this.e = b21Var;
        b21Var.v(ze1Var.g());
        l93 l93Var = new l93(a.e(b21Var, this.c));
        this.d = l93Var;
        l93Var.mutate();
        s();
        if (nc1.d()) {
            nc1.b();
        }
    }

    @Override // defpackage.jr0
    public Rect a() {
        return this.d.getBounds();
    }

    @Override // defpackage.vm3
    public void b(Drawable drawable) {
        this.d.p(drawable);
    }

    @Override // defpackage.vm3
    public void c(Throwable th) {
        this.e.h();
        k();
        if (this.e.b(4) != null) {
            j(4);
        } else {
            j(1);
        }
        this.e.k();
    }

    @Override // defpackage.vm3
    public void d(Throwable th) {
        this.e.h();
        k();
        if (this.e.b(5) != null) {
            j(5);
        } else {
            j(1);
        }
        this.e.k();
    }

    @Override // defpackage.vm3
    public void e(float f, boolean z) {
        if (this.e.b(3) == null) {
            return;
        }
        this.e.h();
        u(f);
        if (z) {
            this.e.o();
        }
        this.e.k();
    }

    @Override // defpackage.jr0
    public Drawable f() {
        return this.d;
    }

    @Override // defpackage.vm3
    public void g(Drawable drawable, float f, boolean z) {
        Drawable d = a.d(drawable, this.c, this.b);
        d.mutate();
        this.f.f(d);
        this.e.h();
        k();
        j(2);
        u(f);
        if (z) {
            this.e.o();
        }
        this.e.k();
    }

    public final Drawable h(Drawable drawable, qc3.b bVar, PointF pointF, ColorFilter colorFilter) {
        drawable.setColorFilter(colorFilter);
        return a.g(drawable, bVar, pointF);
    }

    public final Drawable i(Drawable drawable, qc3.b bVar) {
        return a.f(a.d(drawable, this.c, this.b), bVar);
    }

    public final void j(int i) {
        if (i >= 0) {
            this.e.m(i);
        }
    }

    public final void k() {
        l(1);
        l(2);
        l(3);
        l(4);
        l(5);
    }

    public final void l(int i) {
        if (i >= 0) {
            this.e.n(i);
        }
    }

    public PointF m() {
        if (q(2)) {
            return p(2).r();
        }
        return null;
    }

    public qc3.b n() {
        if (q(2)) {
            return p(2).s();
        }
        return null;
    }

    public final wq0 o(int i) {
        wq0 d = this.e.d(i);
        if (d.i() instanceof z42) {
            d = (z42) d.i();
        }
        return d.i() instanceof oc3 ? (oc3) d.i() : d;
    }

    public final oc3 p(int i) {
        wq0 o = o(i);
        if (o instanceof oc3) {
            return (oc3) o;
        }
        return a.h(o, qc3.b.a);
    }

    public final boolean q(int i) {
        return o(i) instanceof oc3;
    }

    public final void r() {
        this.f.f(this.a);
    }

    @Override // defpackage.vm3
    public void reset() {
        r();
        s();
    }

    public final void s() {
        b21 b21Var = this.e;
        if (b21Var != null) {
            b21Var.h();
            this.e.l();
            k();
            j(1);
            this.e.o();
            this.e.k();
        }
    }

    public void t(lm2 lm2Var) {
        this.e.u(lm2Var);
    }

    public final void u(float f) {
        Drawable b = this.e.b(3);
        if (b == null) {
            return;
        }
        if (f >= 0.999f) {
            if (b instanceof Animatable) {
                ((Animatable) b).stop();
            }
            l(3);
        } else {
            if (b instanceof Animatable) {
                ((Animatable) b).start();
            }
            j(3);
        }
        b.setLevel(Math.round(f * 10000.0f));
    }
}
