package defpackage;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.d;

/* renamed from: fs4  reason: default package */
/* loaded from: classes2.dex */
public final class fs4 {
    public final d a;
    public final int b;
    public final int c;

    public fs4(int i, qo0 qo0Var) {
        if (i < 2) {
            throw new IllegalArgumentException("height must be >= 2");
        }
        Objects.requireNonNull(qo0Var, "digest == null");
        d dVar = new d(new jl4(qo0Var));
        this.a = dVar;
        this.b = i;
        this.c = a();
        ql0.b(b().g(), c(), g(), dVar.d().c(), i);
    }

    public final int a() {
        int i = 2;
        while (true) {
            int i2 = this.b;
            if (i > i2) {
                throw new IllegalStateException("should never happen...");
            }
            if ((i2 - i) % 2 == 0) {
                return i;
            }
            i++;
        }
    }

    public qo0 b() {
        return this.a.d().a();
    }

    public int c() {
        return this.a.d().b();
    }

    public int d() {
        return this.b;
    }

    public int e() {
        return this.c;
    }

    public d f() {
        return this.a;
    }

    public int g() {
        return this.a.d().d();
    }
}
