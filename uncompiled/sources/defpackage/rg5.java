package defpackage;

import android.os.Bundle;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzas;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: rg5  reason: default package */
/* loaded from: classes.dex */
public final class rg5 {
    public final String a;
    public final String b;
    public final long c;
    public final Bundle d;

    public rg5(String str, String str2, Bundle bundle, long j) {
        this.a = str;
        this.b = str2;
        this.d = bundle;
        this.c = j;
    }

    public static rg5 a(zzas zzasVar) {
        return new rg5(zzasVar.a, zzasVar.g0, zzasVar.f0.N1(), zzasVar.h0);
    }

    public final zzas b() {
        return new zzas(this.a, new zzaq(new Bundle(this.d)), this.b, this.c);
    }

    public final String toString() {
        String str = this.b;
        String str2 = this.a;
        String valueOf = String.valueOf(this.d);
        int length = String.valueOf(str).length();
        StringBuilder sb = new StringBuilder(length + 21 + String.valueOf(str2).length() + valueOf.length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }
}
