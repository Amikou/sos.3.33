package defpackage;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: nj5  reason: default package */
/* loaded from: classes.dex */
public final class nj5 implements a16 {
    public final /* synthetic */ String a;
    public final /* synthetic */ qj5 b;

    public nj5(qj5 qj5Var, String str) {
        this.b = qj5Var;
        this.a = str;
    }

    @Override // defpackage.a16
    public final String a(String str) {
        Map map;
        map = this.b.d;
        Map map2 = (Map) map.get(this.a);
        if (map2 == null || !map2.containsKey(str)) {
            return null;
        }
        return (String) map2.get(str);
    }
}
