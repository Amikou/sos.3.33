package defpackage;

import io.reactivex.internal.schedulers.RxThreadFactory;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: ComputationScheduler.java */
/* renamed from: s40  reason: default package */
/* loaded from: classes2.dex */
public final class s40 extends bd3 {
    public static final a c;
    public static final RxThreadFactory d;
    public static final int e = a(Runtime.getRuntime().availableProcessors(), Integer.getInteger("rx2.computation-threads", 0).intValue());
    public static final b f;
    public final ThreadFactory a;
    public final AtomicReference<a> b;

    /* compiled from: ComputationScheduler.java */
    /* renamed from: s40$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public final b[] a;

        public a(int i, ThreadFactory threadFactory) {
            this.a = new b[i];
            for (int i2 = 0; i2 < i; i2++) {
                this.a[i2] = new b(threadFactory);
            }
        }

        public void a() {
            for (b bVar : this.a) {
                bVar.a();
            }
        }
    }

    /* compiled from: ComputationScheduler.java */
    /* renamed from: s40$b */
    /* loaded from: classes2.dex */
    public static final class b extends nf2 {
        public b(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        b bVar = new b(new RxThreadFactory("RxComputationShutdown"));
        f = bVar;
        bVar.a();
        RxThreadFactory rxThreadFactory = new RxThreadFactory("RxComputationThreadPool", Math.max(1, Math.min(10, Integer.getInteger("rx2.computation-priority", 5).intValue())), true);
        d = rxThreadFactory;
        a aVar = new a(0, rxThreadFactory);
        c = aVar;
        aVar.a();
    }

    public s40() {
        this(d);
    }

    public static int a(int i, int i2) {
        return (i2 <= 0 || i2 > i) ? i : i2;
    }

    public void b() {
        a aVar = new a(e, this.a);
        if (this.b.compareAndSet(c, aVar)) {
            return;
        }
        aVar.a();
    }

    public s40(ThreadFactory threadFactory) {
        this.a = threadFactory;
        this.b = new AtomicReference<>(c);
        b();
    }
}
