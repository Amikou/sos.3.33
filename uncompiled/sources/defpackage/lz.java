package defpackage;

import java.io.IOException;
import java.math.BigInteger;

/* compiled from: ClientTransactionManager.java */
/* renamed from: lz  reason: default package */
/* loaded from: classes3.dex */
public class lz extends u84 {
    private final ko4 web3j;

    public lz(ko4 ko4Var, String str) {
        super(ko4Var, str);
        this.web3j = ko4Var;
    }

    @Override // defpackage.u84
    public vw0 getCode(String str, gi0 gi0Var) throws IOException {
        return this.web3j.ethGetCode(str, gi0Var).send();
    }

    @Override // defpackage.u84
    public String sendCall(String str, String str2, gi0 gi0Var) throws IOException {
        iw0 send = this.web3j.ethCall(p84.createEthCallTransaction(getFromAddress(), str, str2), gi0Var).send();
        u84.assertCallNotReverted(send);
        return send.getValue();
    }

    @Override // defpackage.u84
    public hx0 sendTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3, boolean z) throws IOException {
        return this.web3j.ethSendTransaction(new p84(getFromAddress(), null, bigInteger, bigInteger2, str, bigInteger3, str2)).send();
    }

    @Override // defpackage.u84
    public hx0 sendTransactionEIP1559(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2, BigInteger bigInteger4, boolean z) throws IOException {
        return this.web3j.ethSendTransaction(new p84(getFromAddress(), null, null, bigInteger3, str, bigInteger4, str2, bigInteger, bigInteger2)).send();
    }

    public lz(ko4 ko4Var, String str, int i, int i2) {
        super(ko4Var, i, i2, str);
        this.web3j = ko4Var;
    }

    public lz(ko4 ko4Var, String str, x84 x84Var) {
        super(x84Var, str);
        this.web3j = ko4Var;
    }
}
