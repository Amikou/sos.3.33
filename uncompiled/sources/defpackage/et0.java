package defpackage;

import java.math.BigInteger;
import java.security.KeyPair;
import java.util.Arrays;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.web3j.crypto.c;

/* compiled from: ECKeyPair.java */
/* renamed from: et0  reason: default package */
/* loaded from: classes3.dex */
public class et0 {
    private final BigInteger privateKey;
    private final BigInteger publicKey;

    public et0(BigInteger bigInteger, BigInteger bigInteger2) {
        this.privateKey = bigInteger;
        this.publicKey = bigInteger2;
    }

    public static et0 create(KeyPair keyPair) {
        BigInteger d = ((BCECPrivateKey) keyPair.getPrivate()).getD();
        byte[] l = ((BCECPublicKey) keyPair.getPublic()).getQ().l(false);
        return new et0(d, new BigInteger(1, Arrays.copyOfRange(l, 1, l.length)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || et0.class != obj.getClass()) {
            return false;
        }
        et0 et0Var = (et0) obj;
        BigInteger bigInteger = this.privateKey;
        if (bigInteger == null ? et0Var.privateKey == null : bigInteger.equals(et0Var.privateKey)) {
            BigInteger bigInteger2 = this.publicKey;
            if (bigInteger2 != null) {
                return bigInteger2.equals(et0Var.publicKey);
            }
            return et0Var.publicKey == null;
        }
        return false;
    }

    public BigInteger getPrivateKey() {
        return this.privateKey;
    }

    public BigInteger getPublicKey() {
        return this.publicKey;
    }

    public int hashCode() {
        BigInteger bigInteger = this.privateKey;
        int hashCode = (bigInteger != null ? bigInteger.hashCode() : 0) * 31;
        BigInteger bigInteger2 = this.publicKey;
        return hashCode + (bigInteger2 != null ? bigInteger2.hashCode() : 0);
    }

    public ys0 sign(byte[] bArr) {
        zs0 zs0Var = new zs0(new ij1(new ka3()));
        zs0Var.d(true, new st0(this.privateKey, c.CURVE));
        BigInteger[] c = zs0Var.c(bArr);
        return new ys0(c[0], c[1]).toCanonicalised();
    }

    public static et0 create(BigInteger bigInteger) {
        return new et0(bigInteger, c.publicKeyFromPrivate(bigInteger));
    }

    public static et0 create(byte[] bArr) {
        return create(ej2.toBigInt(bArr));
    }
}
