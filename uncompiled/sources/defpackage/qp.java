package defpackage;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;

/* compiled from: BitmapBytesTranscoder.java */
/* renamed from: qp  reason: default package */
/* loaded from: classes.dex */
public class qp implements e83<Bitmap, byte[]> {
    public final Bitmap.CompressFormat a;
    public final int b;

    public qp() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @Override // defpackage.e83
    public s73<byte[]> a(s73<Bitmap> s73Var, vn2 vn2Var) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        s73Var.get().compress(this.a, this.b, byteArrayOutputStream);
        s73Var.b();
        return new dt(byteArrayOutputStream.toByteArray());
    }

    public qp(Bitmap.CompressFormat compressFormat, int i) {
        this.a = compressFormat;
        this.b = i;
    }
}
