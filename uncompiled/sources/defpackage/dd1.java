package defpackage;

import java.lang.Throwable;

/* compiled from: Function.java */
/* renamed from: dd1  reason: default package */
/* loaded from: classes.dex */
public interface dd1<TInput, TResult, TException extends Throwable> {
    TResult apply(TInput tinput) throws Throwable;
}
