package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.util.b;
import androidx.media3.extractor.metadata.id3.CommentFrame;
import androidx.media3.extractor.metadata.id3.InternalFrame;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: GaplessInfoHolder.java */
/* renamed from: qe1  reason: default package */
/* loaded from: classes.dex */
public final class qe1 {
    public static final Pattern c = Pattern.compile("^ [0-9a-fA-F]{8} ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8})");
    public int a = -1;
    public int b = -1;

    public boolean a() {
        return (this.a == -1 || this.b == -1) ? false : true;
    }

    public final boolean b(String str) {
        Matcher matcher = c.matcher(str);
        if (matcher.find()) {
            try {
                int parseInt = Integer.parseInt((String) b.j(matcher.group(1)), 16);
                int parseInt2 = Integer.parseInt((String) b.j(matcher.group(2)), 16);
                if (parseInt > 0 || parseInt2 > 0) {
                    this.a = parseInt;
                    this.b = parseInt2;
                    return true;
                }
                return false;
            } catch (NumberFormatException unused) {
                return false;
            }
        }
        return false;
    }

    public boolean c(Metadata metadata) {
        for (int i = 0; i < metadata.d(); i++) {
            Metadata.Entry c2 = metadata.c(i);
            if (c2 instanceof CommentFrame) {
                CommentFrame commentFrame = (CommentFrame) c2;
                if ("iTunSMPB".equals(commentFrame.g0) && b(commentFrame.h0)) {
                    return true;
                }
            } else if (c2 instanceof InternalFrame) {
                InternalFrame internalFrame = (InternalFrame) c2;
                if ("com.apple.iTunes".equals(internalFrame.f0) && "iTunSMPB".equals(internalFrame.g0) && b(internalFrame.h0)) {
                    return true;
                }
            } else {
                continue;
            }
        }
        return false;
    }

    public boolean d(int i) {
        int i2 = i >> 12;
        int i3 = i & 4095;
        if (i2 > 0 || i3 > 0) {
            this.a = i2;
            this.b = i3;
            return true;
        }
        return false;
    }
}
