package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import defpackage.wi3;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import zendesk.support.request.CellBase;

/* compiled from: AmrExtractor.java */
/* renamed from: ib  reason: default package */
/* loaded from: classes.dex */
public final class ib implements p11 {
    public static final int[] p;
    public static final int[] q;
    public static final byte[] r;
    public static final byte[] s;
    public static final int t;
    public final byte[] a;
    public final int b;
    public boolean c;
    public long d;
    public int e;
    public int f;
    public boolean g;
    public long h;
    public int i;
    public int j;
    public long k;
    public r11 l;
    public f84 m;
    public wi3 n;
    public boolean o;

    static {
        hb hbVar = hb.b;
        p = new int[]{13, 14, 16, 18, 20, 21, 27, 32, 6, 7, 6, 6, 1, 1, 1, 1};
        int[] iArr = {18, 24, 33, 37, 41, 47, 51, 59, 61, 6, 1, 1, 1, 1, 1, 1};
        q = iArr;
        r = b.j0("#!AMR\n");
        s = b.j0("#!AMR-WB\n");
        t = iArr[8];
    }

    public ib() {
        this(0);
    }

    public static int e(int i, long j) {
        return (int) (((i * 8) * 1000000) / j);
    }

    public static /* synthetic */ p11[] n() {
        return new p11[]{new ib()};
    }

    public static boolean q(q11 q11Var, byte[] bArr) throws IOException {
        q11Var.j();
        byte[] bArr2 = new byte[bArr.length];
        q11Var.n(bArr2, 0, bArr.length);
        return Arrays.equals(bArr2, bArr);
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.d = 0L;
        this.e = 0;
        this.f = 0;
        if (j != 0) {
            wi3 wi3Var = this.n;
            if (wi3Var instanceof z50) {
                this.k = ((z50) wi3Var).c(j);
                return;
            }
        }
        this.k = 0L;
    }

    public final void d() {
        ii.i(this.m);
        b.j(this.l);
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        d();
        if (q11Var.getPosition() == 0 && !s(q11Var)) {
            throw ParserException.createForMalformedContainer("Could not find AMR header.", null);
        }
        o();
        int t2 = t(q11Var);
        p(q11Var.getLength(), t2);
        return t2;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        return s(q11Var);
    }

    public final wi3 h(long j, boolean z) {
        return new z50(j, this.h, e(this.i, 20000L), this.i, z);
    }

    public final int i(int i) throws ParserException {
        if (l(i)) {
            return this.c ? q[i] : p[i];
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Illegal AMR ");
        sb.append(this.c ? "WB" : "NB");
        sb.append(" frame type ");
        sb.append(i);
        throw ParserException.createForMalformedContainer(sb.toString(), null);
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.l = r11Var;
        this.m = r11Var.f(0, 1);
        r11Var.m();
    }

    public final boolean k(int i) {
        return !this.c && (i < 12 || i > 14);
    }

    public final boolean l(int i) {
        return i >= 0 && i <= 15 && (m(i) || k(i));
    }

    public final boolean m(int i) {
        return this.c && (i < 10 || i > 13);
    }

    public final void o() {
        if (this.o) {
            return;
        }
        this.o = true;
        boolean z = this.c;
        this.m.f(new j.b().e0(z ? "audio/amr-wb" : "audio/3gpp").W(t).H(1).f0(z ? 16000 : 8000).E());
    }

    public final void p(long j, int i) {
        int i2;
        if (this.g) {
            return;
        }
        int i3 = this.b;
        if ((i3 & 1) != 0 && j != -1 && ((i2 = this.i) == -1 || i2 == this.e)) {
            if (this.j >= 20 || i == -1) {
                wi3 h = h(j, (i3 & 2) != 0);
                this.n = h;
                this.l.p(h);
                this.g = true;
                return;
            }
            return;
        }
        wi3.b bVar = new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        this.n = bVar;
        this.l.p(bVar);
        this.g = true;
    }

    public final int r(q11 q11Var) throws IOException {
        q11Var.j();
        q11Var.n(this.a, 0, 1);
        byte b = this.a[0];
        if ((b & 131) <= 0) {
            return i((b >> 3) & 15);
        }
        throw ParserException.createForMalformedContainer("Invalid padding bits for frame header " + ((int) b), null);
    }

    public final boolean s(q11 q11Var) throws IOException {
        byte[] bArr = r;
        if (q(q11Var, bArr)) {
            this.c = false;
            q11Var.k(bArr.length);
            return true;
        }
        byte[] bArr2 = s;
        if (q(q11Var, bArr2)) {
            this.c = true;
            q11Var.k(bArr2.length);
            return true;
        }
        return false;
    }

    public final int t(q11 q11Var) throws IOException {
        if (this.f == 0) {
            try {
                int r2 = r(q11Var);
                this.e = r2;
                this.f = r2;
                if (this.i == -1) {
                    this.h = q11Var.getPosition();
                    this.i = this.e;
                }
                if (this.i == this.e) {
                    this.j++;
                }
            } catch (EOFException unused) {
                return -1;
            }
        }
        int d = this.m.d(q11Var, this.f, true);
        if (d == -1) {
            return -1;
        }
        int i = this.f - d;
        this.f = i;
        if (i > 0) {
            return 0;
        }
        this.m.b(this.k + this.d, 1, this.e, 0, null);
        this.d += 20000;
        return 0;
    }

    public ib(int i) {
        this.b = (i & 2) != 0 ? i | 1 : i;
        this.a = new byte[1];
        this.i = -1;
    }
}
