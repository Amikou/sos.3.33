package defpackage;

import com.google.android.gms.internal.measurement.b1;
import com.google.android.gms.internal.measurement.f1;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: dw5  reason: default package */
/* loaded from: classes.dex */
public final class dw5 {
    public f1 a;
    public List<Long> b;
    public List<b1> c;
    public long d;
    public final /* synthetic */ fw5 e;

    public /* synthetic */ dw5(fw5 fw5Var, ov5 ov5Var) {
        this.e = fw5Var;
    }

    public static final long b(b1 b1Var) {
        return ((b1Var.C() / 1000) / 60) / 60;
    }

    public final boolean a(long j, b1 b1Var) {
        zt2.j(b1Var);
        if (this.c == null) {
            this.c = new ArrayList();
        }
        if (this.b == null) {
            this.b = new ArrayList();
        }
        if (this.c.size() <= 0 || b(this.c.get(0)) == b(b1Var)) {
            long e = this.d + b1Var.e();
            this.e.S();
            if (e >= Math.max(0, qf5.i.b(null).intValue())) {
                return false;
            }
            this.d = e;
            this.c.add(b1Var);
            this.b.add(Long.valueOf(j));
            int size = this.c.size();
            this.e.S();
            return size < Math.max(1, qf5.j.b(null).intValue());
        }
        return false;
    }
}
