package defpackage;

import android.graphics.drawable.Drawable;

/* compiled from: Target.java */
/* renamed from: i34  reason: default package */
/* loaded from: classes.dex */
public interface i34<R> extends pz1 {
    void a(dq3 dq3Var);

    void c(d73 d73Var);

    void f(dq3 dq3Var);

    void g(Drawable drawable);

    void j(R r, hb4<? super R> hb4Var);

    void k(Drawable drawable);

    d73 l();

    void m(Drawable drawable);
}
