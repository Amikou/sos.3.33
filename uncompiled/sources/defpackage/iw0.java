package defpackage;

import java.util.Collections;
import java.util.List;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: EthCall.java */
/* renamed from: iw0  reason: default package */
/* loaded from: classes3.dex */
public class iw0 extends i83<String> {
    private static final String errorMethodId = "0x08c379a0";
    private static final List<TypeReference<zc4>> revertReasonType = Collections.singletonList(TypeReference.create(m4.getType(Utf8String.TYPE_NAME)));

    private boolean isErrorInResult() {
        return getValue() != null && getValue().startsWith(errorMethodId);
    }

    public String getRevertReason() {
        if (isErrorInResult()) {
            return ((Utf8String) yd1.decode(getValue().substring(10), revertReasonType).get(0)).getValue();
        }
        if (hasError()) {
            return getError().getMessage();
        }
        return null;
    }

    public String getValue() {
        return getResult();
    }

    public boolean isReverted() {
        return hasError() || isErrorInResult();
    }

    @Deprecated
    public boolean reverts() {
        return isReverted();
    }
}
