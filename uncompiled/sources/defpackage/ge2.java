package defpackage;

import android.content.Context;
import android.content.Intent;

/* compiled from: NavigateToAndroidSettingsForNotifications.kt */
/* renamed from: ge2  reason: default package */
/* loaded from: classes2.dex */
public final class ge2 {
    public static final ge2 a = new ge2();

    public final void a(Context context) {
        fs1.f(context, "context");
        Intent intent = new Intent();
        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
        intent.addFlags(268435456);
        intent.putExtra("app_package", context.getPackageName());
        intent.putExtra("app_uid", context.getApplicationInfo().uid);
        intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
        context.startActivity(intent);
    }
}
