package defpackage;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: wu5  reason: default package */
/* loaded from: classes.dex */
public final class wu5 implements Runnable {
    public final /* synthetic */ vz5 a;
    public final /* synthetic */ AppMeasurementDynamiteService f0;

    public wu5(AppMeasurementDynamiteService appMeasurementDynamiteService, vz5 vz5Var) {
        this.f0 = appMeasurementDynamiteService;
        this.a = vz5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.a.F().u(this.a);
    }
}
