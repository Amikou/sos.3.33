package defpackage;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

/* compiled from: BitmapDrawableTranscoder.java */
/* renamed from: vp  reason: default package */
/* loaded from: classes.dex */
public class vp implements e83<Bitmap, BitmapDrawable> {
    public final Resources a;

    public vp(Resources resources) {
        this.a = (Resources) wt2.d(resources);
    }

    @Override // defpackage.e83
    public s73<BitmapDrawable> a(s73<Bitmap> s73Var, vn2 vn2Var) {
        return uy1.f(this.a, s73Var);
    }
}
