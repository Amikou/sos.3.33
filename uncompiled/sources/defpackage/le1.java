package defpackage;

import java.math.BigInteger;

/* renamed from: le1  reason: default package */
/* loaded from: classes2.dex */
public class le1 extends x4 {
    public final xs0 a;
    public final ke1 b;

    public le1(xs0 xs0Var, ke1 ke1Var) {
        if (xs0Var == null || xs0Var.x() == null) {
            throw new IllegalArgumentException("Need curve with known group order");
        }
        this.a = xs0Var;
        this.b = ke1Var;
    }

    @Override // defpackage.x4
    public pt0 c(pt0 pt0Var, BigInteger bigInteger) {
        if (this.a.m(pt0Var.i())) {
            BigInteger[] c = this.b.c(bigInteger.mod(pt0Var.i().x()));
            BigInteger bigInteger2 = c[0];
            BigInteger bigInteger3 = c[1];
            qt0 a = this.b.a();
            return this.b.b() ? vs0.c(pt0Var, bigInteger2, a, bigInteger3) : vs0.b(pt0Var, bigInteger2, a.a(pt0Var), bigInteger3);
        }
        throw new IllegalStateException();
    }
}
