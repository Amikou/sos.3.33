package defpackage;

import android.util.Pair;
import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import java.io.IOException;
import okhttp3.internal.http.StatusLine;

/* compiled from: WavExtractor.java */
/* renamed from: co4  reason: default package */
/* loaded from: classes.dex */
public final class co4 implements p11 {
    public r11 a;
    public f84 b;
    public b e;
    public int c = 0;
    public long d = -1;
    public int f = -1;
    public long g = -1;

    /* compiled from: WavExtractor.java */
    /* renamed from: co4$a */
    /* loaded from: classes.dex */
    public static final class a implements b {
        public static final int[] m = {-1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8};
        public static final int[] n = {7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66, 73, 80, 88, 97, 107, 118, 130, 143, 157, 173, 190, 209, 230, 253, 279, StatusLine.HTTP_TEMP_REDIRECT, 337, 371, 408, 449, 494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899, 15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767};
        public final r11 a;
        public final f84 b;
        public final do4 c;
        public final int d;
        public final byte[] e;
        public final op2 f;
        public final int g;
        public final j h;
        public int i;
        public long j;
        public int k;
        public long l;

        public a(r11 r11Var, f84 f84Var, do4 do4Var) throws ParserException {
            this.a = r11Var;
            this.b = f84Var;
            this.c = do4Var;
            int max = Math.max(1, do4Var.c / 10);
            this.g = max;
            op2 op2Var = new op2(do4Var.f);
            op2Var.v();
            int v = op2Var.v();
            this.d = v;
            int i = do4Var.b;
            int i2 = (((do4Var.d - (i * 4)) * 8) / (do4Var.e * i)) + 1;
            if (v == i2) {
                int l = androidx.media3.common.util.b.l(max, v);
                this.e = new byte[do4Var.d * l];
                this.f = new op2(l * h(v, i));
                int i3 = ((do4Var.c * do4Var.d) * 8) / v;
                this.h = new j.b().e0("audio/raw").G(i3).Z(i3).W(h(max, i)).H(do4Var.b).f0(do4Var.c).Y(2).E();
                return;
            }
            throw ParserException.createForMalformedContainer("Expected frames per block: " + i2 + "; got: " + v, null);
        }

        public static int h(int i, int i2) {
            return i * 2 * i2;
        }

        @Override // defpackage.co4.b
        public void a(long j) {
            this.i = 0;
            this.j = j;
            this.k = 0;
            this.l = 0L;
        }

        /* JADX WARN: Removed duplicated region for block: B:15:0x0047  */
        /* JADX WARN: Removed duplicated region for block: B:7:0x0020  */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:10:0x0035 -> B:4:0x001b). Please submit an issue!!! */
        @Override // defpackage.co4.b
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public boolean b(defpackage.q11 r7, long r8) throws java.io.IOException {
            /*
                r6 = this;
                int r0 = r6.g
                int r1 = r6.k
                int r1 = r6.f(r1)
                int r0 = r0 - r1
                int r1 = r6.d
                int r0 = androidx.media3.common.util.b.l(r0, r1)
                do4 r1 = r6.c
                int r1 = r1.d
                int r0 = r0 * r1
                r1 = 0
                int r1 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
                r2 = 1
                if (r1 != 0) goto L1d
            L1b:
                r1 = r2
                goto L1e
            L1d:
                r1 = 0
            L1e:
                if (r1 != 0) goto L3e
                int r3 = r6.i
                if (r3 >= r0) goto L3e
                int r3 = r0 - r3
                long r3 = (long) r3
                long r3 = java.lang.Math.min(r3, r8)
                int r3 = (int) r3
                byte[] r4 = r6.e
                int r5 = r6.i
                int r3 = r7.read(r4, r5, r3)
                r4 = -1
                if (r3 != r4) goto L38
                goto L1b
            L38:
                int r4 = r6.i
                int r4 = r4 + r3
                r6.i = r4
                goto L1e
            L3e:
                int r7 = r6.i
                do4 r8 = r6.c
                int r8 = r8.d
                int r7 = r7 / r8
                if (r7 <= 0) goto L75
                byte[] r8 = r6.e
                op2 r9 = r6.f
                r6.d(r8, r7, r9)
                int r8 = r6.i
                do4 r9 = r6.c
                int r9 = r9.d
                int r7 = r7 * r9
                int r8 = r8 - r7
                r6.i = r8
                op2 r7 = r6.f
                int r7 = r7.f()
                f84 r8 = r6.b
                op2 r9 = r6.f
                r8.a(r9, r7)
                int r8 = r6.k
                int r8 = r8 + r7
                r6.k = r8
                int r7 = r6.f(r8)
                int r8 = r6.g
                if (r7 < r8) goto L75
                r6.i(r8)
            L75:
                if (r1 == 0) goto L82
                int r7 = r6.k
                int r7 = r6.f(r7)
                if (r7 <= 0) goto L82
                r6.i(r7)
            L82:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.co4.a.b(q11, long):boolean");
        }

        @Override // defpackage.co4.b
        public void c(int i, long j) {
            this.a.p(new fo4(this.c, this.d, i, j));
            this.b.f(this.h);
        }

        public final void d(byte[] bArr, int i, op2 op2Var) {
            for (int i2 = 0; i2 < i; i2++) {
                for (int i3 = 0; i3 < this.c.b; i3++) {
                    e(bArr, i2, i3, op2Var.d());
                }
            }
            int g = g(this.d * i);
            op2Var.P(0);
            op2Var.O(g);
        }

        public final void e(byte[] bArr, int i, int i2, byte[] bArr2) {
            do4 do4Var = this.c;
            int i3 = do4Var.d;
            int i4 = do4Var.b;
            int i5 = (i * i3) + (i2 * 4);
            int i6 = (i4 * 4) + i5;
            int i7 = (i3 / i4) - 4;
            int i8 = (short) (((bArr[i5 + 1] & 255) << 8) | (bArr[i5] & 255));
            int min = Math.min(bArr[i5 + 2] & 255, 88);
            int i9 = n[min];
            int i10 = ((i * this.d * i4) + i2) * 2;
            bArr2[i10] = (byte) (i8 & 255);
            bArr2[i10 + 1] = (byte) (i8 >> 8);
            for (int i11 = 0; i11 < i7 * 2; i11++) {
                int i12 = bArr[((i11 / 8) * i4 * 4) + i6 + ((i11 / 2) % 4)] & 255;
                int i13 = i11 % 2 == 0 ? i12 & 15 : i12 >> 4;
                int i14 = ((((i13 & 7) * 2) + 1) * i9) >> 3;
                if ((i13 & 8) != 0) {
                    i14 = -i14;
                }
                i8 = androidx.media3.common.util.b.q(i8 + i14, -32768, 32767);
                i10 += i4 * 2;
                bArr2[i10] = (byte) (i8 & 255);
                bArr2[i10 + 1] = (byte) (i8 >> 8);
                int i15 = min + m[i13];
                int[] iArr = n;
                min = androidx.media3.common.util.b.q(i15, 0, iArr.length - 1);
                i9 = iArr[min];
            }
        }

        public final int f(int i) {
            return i / (this.c.b * 2);
        }

        public final int g(int i) {
            return h(i, this.c.b);
        }

        public final void i(int i) {
            long J0 = this.j + androidx.media3.common.util.b.J0(this.l, 1000000L, this.c.c);
            int g = g(i);
            this.b.b(J0, 1, g, this.k - g, null);
            this.l += i;
            this.k -= g;
        }
    }

    /* compiled from: WavExtractor.java */
    /* renamed from: co4$b */
    /* loaded from: classes.dex */
    public interface b {
        void a(long j);

        boolean b(q11 q11Var, long j) throws IOException;

        void c(int i, long j) throws ParserException;
    }

    /* compiled from: WavExtractor.java */
    /* renamed from: co4$c */
    /* loaded from: classes.dex */
    public static final class c implements b {
        public final r11 a;
        public final f84 b;
        public final do4 c;
        public final j d;
        public final int e;
        public long f;
        public int g;
        public long h;

        public c(r11 r11Var, f84 f84Var, do4 do4Var, String str, int i) throws ParserException {
            this.a = r11Var;
            this.b = f84Var;
            this.c = do4Var;
            int i2 = (do4Var.b * do4Var.e) / 8;
            if (do4Var.d == i2) {
                int i3 = do4Var.c;
                int i4 = i3 * i2 * 8;
                int max = Math.max(i2, (i3 * i2) / 10);
                this.e = max;
                this.d = new j.b().e0(str).G(i4).Z(i4).W(max).H(do4Var.b).f0(do4Var.c).Y(i).E();
                return;
            }
            throw ParserException.createForMalformedContainer("Expected block size: " + i2 + "; got: " + do4Var.d, null);
        }

        @Override // defpackage.co4.b
        public void a(long j) {
            this.f = j;
            this.g = 0;
            this.h = 0L;
        }

        @Override // defpackage.co4.b
        public boolean b(q11 q11Var, long j) throws IOException {
            int i;
            do4 do4Var;
            int i2;
            int i3;
            long j2 = j;
            while (true) {
                i = (j2 > 0L ? 1 : (j2 == 0L ? 0 : -1));
                if (i <= 0 || (i2 = this.g) >= (i3 = this.e)) {
                    break;
                }
                int d = this.b.d(q11Var, (int) Math.min(i3 - i2, j2), true);
                if (d == -1) {
                    j2 = 0;
                } else {
                    this.g += d;
                    j2 -= d;
                }
            }
            int i4 = this.c.d;
            int i5 = this.g / i4;
            if (i5 > 0) {
                int i6 = i5 * i4;
                int i7 = this.g - i6;
                this.b.b(this.f + androidx.media3.common.util.b.J0(this.h, 1000000L, do4Var.c), 1, i6, i7, null);
                this.h += i5;
                this.g = i7;
            }
            return i <= 0;
        }

        @Override // defpackage.co4.b
        public void c(int i, long j) {
            this.a.p(new fo4(this.c, 1, i, j));
            this.b.f(this.d);
        }
    }

    static {
        bo4 bo4Var = bo4.b;
    }

    public static /* synthetic */ p11[] e() {
        return new p11[]{new co4()};
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.c = j == 0 ? 0 : 4;
        b bVar = this.e;
        if (bVar != null) {
            bVar.a(j2);
        }
    }

    public final void d() {
        ii.i(this.b);
        androidx.media3.common.util.b.j(this.a);
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        d();
        int i = this.c;
        if (i == 0) {
            h(q11Var);
            return 0;
        } else if (i == 1) {
            k(q11Var);
            return 0;
        } else if (i == 2) {
            i(q11Var);
            return 0;
        } else if (i == 3) {
            m(q11Var);
            return 0;
        } else if (i == 4) {
            return l(q11Var);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        return eo4.a(q11Var);
    }

    public final void h(q11 q11Var) throws IOException {
        ii.g(q11Var.getPosition() == 0);
        int i = this.f;
        if (i != -1) {
            q11Var.k(i);
            this.c = 4;
        } else if (eo4.a(q11Var)) {
            q11Var.k((int) (q11Var.e() - q11Var.getPosition()));
            this.c = 1;
        } else {
            throw ParserException.createForMalformedContainer("Unsupported or unrecognized wav file type.", null);
        }
    }

    public final void i(q11 q11Var) throws IOException {
        do4 b2 = eo4.b(q11Var);
        int i = b2.a;
        if (i == 17) {
            this.e = new a(this.a, this.b, b2);
        } else if (i == 6) {
            this.e = new c(this.a, this.b, b2, "audio/g711-alaw", -1);
        } else if (i == 7) {
            this.e = new c(this.a, this.b, b2, "audio/g711-mlaw", -1);
        } else {
            int a2 = go4.a(i, b2.e);
            if (a2 != 0) {
                this.e = new c(this.a, this.b, b2, "audio/raw", a2);
            } else {
                throw ParserException.createForUnsupportedContainerFeature("Unsupported WAV format type: " + b2.a);
            }
        }
        this.c = 3;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.a = r11Var;
        this.b = r11Var.f(0, 1);
        r11Var.m();
    }

    public final void k(q11 q11Var) throws IOException {
        this.d = eo4.c(q11Var);
        this.c = 2;
    }

    public final int l(q11 q11Var) throws IOException {
        ii.g(this.g != -1);
        return ((b) ii.e(this.e)).b(q11Var, this.g - q11Var.getPosition()) ? -1 : 0;
    }

    public final void m(q11 q11Var) throws IOException {
        Pair<Long, Long> e = eo4.e(q11Var);
        this.f = ((Long) e.first).intValue();
        long longValue = ((Long) e.second).longValue();
        long j = this.d;
        if (j != -1 && longValue == 4294967295L) {
            longValue = j;
        }
        this.g = this.f + longValue;
        long length = q11Var.getLength();
        if (length != -1 && this.g > length) {
            p12.i("WavExtractor", "Data exceeds input length: " + this.g + ", " + length);
            this.g = length;
        }
        ((b) ii.e(this.e)).c(this.f, this.g);
        this.c = 4;
    }
}
