package defpackage;

import java.util.Iterator;

/* compiled from: Sequences.kt */
/* renamed from: es0  reason: default package */
/* loaded from: classes2.dex */
public final class es0<T> implements ol3<T>, fs0<T> {
    public final ol3<T> a;
    public final int b;

    /* compiled from: Sequences.kt */
    /* renamed from: es0$a */
    /* loaded from: classes2.dex */
    public static final class a implements Iterator<T>, tw1 {
        public final Iterator<T> a;
        public int f0;

        public a(es0 es0Var) {
            this.a = es0Var.a.iterator();
            this.f0 = es0Var.b;
        }

        public final void a() {
            while (this.f0 > 0 && this.a.hasNext()) {
                this.a.next();
                this.f0--;
            }
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            a();
            return this.a.hasNext();
        }

        @Override // java.util.Iterator
        public T next() {
            a();
            return this.a.next();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public es0(ol3<? extends T> ol3Var, int i) {
        fs1.f(ol3Var, "sequence");
        this.a = ol3Var;
        this.b = i;
        if (i >= 0) {
            return;
        }
        throw new IllegalArgumentException(("count must be non-negative, but was " + i + '.').toString());
    }

    @Override // defpackage.fs0
    public ol3<T> a(int i) {
        int i2 = this.b + i;
        return i2 < 0 ? new es0(this, i) : new es0(this.a, i2);
    }

    @Override // defpackage.ol3
    public Iterator<T> iterator() {
        return new a(this);
    }
}
