package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: ContinuationImpl.kt */
/* renamed from: s30  reason: default package */
/* loaded from: classes2.dex */
public final class s30 implements q70<Object> {
    public static final s30 a = new s30();

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    public String toString() {
        return "This continuation is already complete";
    }
}
