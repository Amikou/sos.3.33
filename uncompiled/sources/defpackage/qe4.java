package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: CoroutineContext.kt */
/* renamed from: qe4  reason: default package */
/* loaded from: classes2.dex */
public final class qe4<T> extends vd3<T> {
    public CoroutineContext h0;
    public Object i0;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public qe4(kotlin.coroutines.CoroutineContext r3, defpackage.q70<? super T> r4) {
        /*
            r2 = this;
            se4 r0 = defpackage.se4.a
            kotlin.coroutines.CoroutineContext$a r1 = r3.get(r0)
            if (r1 != 0) goto Lc
            kotlin.coroutines.CoroutineContext r3 = r3.plus(r0)
        Lc:
            r2.<init>(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.qe4.<init>(kotlin.coroutines.CoroutineContext, q70):void");
    }

    @Override // defpackage.vd3, defpackage.t4
    public void H0(Object obj) {
        CoroutineContext coroutineContext = this.h0;
        if (coroutineContext != null) {
            ThreadContextKt.a(coroutineContext, this.i0);
            this.h0 = null;
            this.i0 = null;
        }
        Object a = w30.a(obj, this.g0);
        q70<T> q70Var = this.g0;
        CoroutineContext context = q70Var.getContext();
        Object c = ThreadContextKt.c(context, null);
        qe4<?> e = c != ThreadContextKt.a ? x80.e(q70Var, context, c) : null;
        try {
            this.g0.resumeWith(a);
            te4 te4Var = te4.a;
        } finally {
            if (e == null || e.M0()) {
                ThreadContextKt.a(context, c);
            }
        }
    }

    public final boolean M0() {
        if (this.h0 == null) {
            return false;
        }
        this.h0 = null;
        this.i0 = null;
        return true;
    }

    public final void N0(CoroutineContext coroutineContext, Object obj) {
        this.h0 = coroutineContext;
        this.i0 = obj;
    }
}
