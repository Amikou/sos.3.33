package defpackage;

import android.graphics.Bitmap;
import com.bumptech.glide.load.EncodeStrategy;

/* compiled from: BitmapEncoder.java */
/* renamed from: wp  reason: default package */
/* loaded from: classes.dex */
public class wp implements y73<Bitmap> {
    public static final mn2<Integer> b = mn2.f("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
    public static final mn2<Bitmap.CompressFormat> c = mn2.e("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
    public final sh a;

    public wp(sh shVar) {
        this.a = shVar;
    }

    @Override // defpackage.y73
    public EncodeStrategy b(vn2 vn2Var) {
        return EncodeStrategy.TRANSFORMED;
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x005d, code lost:
        if (r6 == null) goto L18;
     */
    @Override // defpackage.ev0
    /* renamed from: c */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean a(defpackage.s73<android.graphics.Bitmap> r9, java.io.File r10, defpackage.vn2 r11) {
        /*
            r8 = this;
            java.lang.String r0 = "BitmapEncoder"
            java.lang.Object r9 = r9.get()
            android.graphics.Bitmap r9 = (android.graphics.Bitmap) r9
            android.graphics.Bitmap$CompressFormat r1 = r8.d(r9, r11)
            int r2 = r9.getWidth()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r3 = r9.getHeight()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            java.lang.String r4 = "encode: [%dx%d] %s"
            defpackage.mg1.d(r4, r2, r3, r1)
            long r2 = defpackage.t12.b()     // Catch: java.lang.Throwable -> Lb0
            mn2<java.lang.Integer> r4 = defpackage.wp.b     // Catch: java.lang.Throwable -> Lb0
            java.lang.Object r4 = r11.c(r4)     // Catch: java.lang.Throwable -> Lb0
            java.lang.Integer r4 = (java.lang.Integer) r4     // Catch: java.lang.Throwable -> Lb0
            int r4 = r4.intValue()     // Catch: java.lang.Throwable -> Lb0
            r5 = 0
            r6 = 0
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch: java.lang.Throwable -> L56 java.io.IOException -> L58
            r7.<init>(r10)     // Catch: java.lang.Throwable -> L56 java.io.IOException -> L58
            sh r10 = r8.a     // Catch: java.lang.Throwable -> L51 java.io.IOException -> L54
            if (r10 == 0) goto L45
            com.bumptech.glide.load.data.c r10 = new com.bumptech.glide.load.data.c     // Catch: java.lang.Throwable -> L51 java.io.IOException -> L54
            sh r6 = r8.a     // Catch: java.lang.Throwable -> L51 java.io.IOException -> L54
            r10.<init>(r7, r6)     // Catch: java.lang.Throwable -> L51 java.io.IOException -> L54
            r6 = r10
            goto L46
        L45:
            r6 = r7
        L46:
            r9.compress(r1, r4, r6)     // Catch: java.lang.Throwable -> L56 java.io.IOException -> L58
            r6.close()     // Catch: java.lang.Throwable -> L56 java.io.IOException -> L58
            r5 = 1
        L4d:
            r6.close()     // Catch: java.io.IOException -> L60 java.lang.Throwable -> Lb0
            goto L60
        L51:
            r9 = move-exception
            r6 = r7
            goto Laa
        L54:
            r6 = r7
            goto L58
        L56:
            r9 = move-exception
            goto Laa
        L58:
            r10 = 3
            boolean r10 = android.util.Log.isLoggable(r0, r10)     // Catch: java.lang.Throwable -> L56
            if (r6 == 0) goto L60
            goto L4d
        L60:
            r10 = 2
            boolean r10 = android.util.Log.isLoggable(r0, r10)     // Catch: java.lang.Throwable -> Lb0
            if (r10 == 0) goto La6
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> Lb0
            r10.<init>()     // Catch: java.lang.Throwable -> Lb0
            java.lang.String r0 = "Compressed with type: "
            r10.append(r0)     // Catch: java.lang.Throwable -> Lb0
            r10.append(r1)     // Catch: java.lang.Throwable -> Lb0
            java.lang.String r0 = " of size "
            r10.append(r0)     // Catch: java.lang.Throwable -> Lb0
            int r0 = defpackage.mg4.h(r9)     // Catch: java.lang.Throwable -> Lb0
            r10.append(r0)     // Catch: java.lang.Throwable -> Lb0
            java.lang.String r0 = " in "
            r10.append(r0)     // Catch: java.lang.Throwable -> Lb0
            double r0 = defpackage.t12.a(r2)     // Catch: java.lang.Throwable -> Lb0
            r10.append(r0)     // Catch: java.lang.Throwable -> Lb0
            java.lang.String r0 = ", options format: "
            r10.append(r0)     // Catch: java.lang.Throwable -> Lb0
            mn2<android.graphics.Bitmap$CompressFormat> r0 = defpackage.wp.c     // Catch: java.lang.Throwable -> Lb0
            java.lang.Object r11 = r11.c(r0)     // Catch: java.lang.Throwable -> Lb0
            r10.append(r11)     // Catch: java.lang.Throwable -> Lb0
            java.lang.String r11 = ", hasAlpha: "
            r10.append(r11)     // Catch: java.lang.Throwable -> Lb0
            boolean r9 = r9.hasAlpha()     // Catch: java.lang.Throwable -> Lb0
            r10.append(r9)     // Catch: java.lang.Throwable -> Lb0
        La6:
            defpackage.mg1.e()
            return r5
        Laa:
            if (r6 == 0) goto Laf
            r6.close()     // Catch: java.io.IOException -> Laf java.lang.Throwable -> Lb0
        Laf:
            throw r9     // Catch: java.lang.Throwable -> Lb0
        Lb0:
            r9 = move-exception
            defpackage.mg1.e()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.wp.a(s73, java.io.File, vn2):boolean");
    }

    public final Bitmap.CompressFormat d(Bitmap bitmap, vn2 vn2Var) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) vn2Var.c(c);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }
}
