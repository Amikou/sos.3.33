package defpackage;

/* renamed from: ie1  reason: default package */
/* loaded from: classes2.dex */
public class ie1 implements ps2 {
    public final int[] a;

    public ie1(int[] iArr) {
        this.a = wh.f(iArr);
    }

    @Override // defpackage.ps2
    public int[] a() {
        return wh.f(this.a);
    }

    @Override // defpackage.ps2
    public int b() {
        int[] iArr = this.a;
        return iArr[iArr.length - 1];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ie1) {
            return wh.c(this.a, ((ie1) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return wh.t(this.a);
    }
}
