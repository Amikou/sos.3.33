package defpackage;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.a;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: BeanDescription.java */
/* renamed from: so  reason: default package */
/* loaded from: classes.dex */
public abstract class so {
    public final JavaType a;

    public so(JavaType javaType) {
        this.a = javaType;
    }

    public abstract Object A(boolean z);

    public boolean B() {
        return t().T();
    }

    public abstract AnnotatedMember a();

    public abstract AnnotatedMethod b();

    public abstract AnnotatedMember c();

    public abstract Map<String, AnnotatedMember> d();

    public abstract AnnotatedConstructor e();

    public abstract o80<Object, Object> f();

    public abstract JsonFormat.Value g(JsonFormat.Value value);

    public abstract Method h(Class<?>... clsArr);

    public abstract Map<Object, AnnotatedMember> i();

    public abstract AnnotatedMethod j();

    public abstract AnnotatedMethod k(String str, Class<?>[] clsArr);

    public abstract Class<?> l();

    public abstract d.a m();

    public abstract List<vo> n();

    public abstract JsonInclude.Value o(JsonInclude.Value value);

    public abstract o80<Object, Object> p();

    public abstract Constructor<?> q(Class<?>... clsArr);

    public Class<?> r() {
        return this.a.getRawClass();
    }

    public abstract xe s();

    public abstract a t();

    public abstract List<AnnotatedConstructor> u();

    public abstract List<AnnotatedMethod> v();

    public abstract Set<String> w();

    public abstract jl2 x();

    public JavaType y() {
        return this.a;
    }

    public abstract boolean z();
}
