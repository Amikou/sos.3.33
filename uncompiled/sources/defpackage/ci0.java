package defpackage;

import android.graphics.Bitmap;
import android.util.SparseArray;
import java.util.concurrent.ExecutorService;

/* compiled from: DefaultBitmapFramePreparer.java */
/* renamed from: ci0  reason: default package */
/* loaded from: classes.dex */
public class ci0 implements zp {
    public static final Class<?> f = ci0.class;
    public final br2 a;
    public final aq b;
    public final Bitmap.Config c;
    public final ExecutorService d;
    public final SparseArray<Runnable> e = new SparseArray<>();

    /* compiled from: DefaultBitmapFramePreparer.java */
    /* renamed from: ci0$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final xp a;
        public final de f0;
        public final int g0;
        public final int h0;

        public a(de deVar, xp xpVar, int i, int i2) {
            this.f0 = deVar;
            this.a = xpVar;
            this.g0 = i;
            this.h0 = i2;
        }

        public final boolean a(int i, int i2) {
            com.facebook.common.references.a<Bitmap> a;
            int i3 = 2;
            try {
                if (i2 == 1) {
                    a = this.a.a(i, this.f0.e(), this.f0.c());
                } else if (i2 != 2) {
                    return false;
                } else {
                    a = ci0.this.a.a(this.f0.e(), this.f0.c(), ci0.this.c);
                    i3 = -1;
                }
                boolean b = b(i, a, i2);
                com.facebook.common.references.a.g(a);
                return (b || i3 == -1) ? b : a(i, i3);
            } catch (RuntimeException e) {
                v11.u(ci0.f, "Failed to create frame bitmap", e);
                return false;
            } finally {
                com.facebook.common.references.a.g(null);
            }
        }

        public final boolean b(int i, com.facebook.common.references.a<Bitmap> aVar, int i2) {
            if (com.facebook.common.references.a.u(aVar) && ci0.this.b.a(i, aVar.j())) {
                v11.o(ci0.f, "Frame %d ready.", Integer.valueOf(this.g0));
                synchronized (ci0.this.e) {
                    this.a.b(this.g0, aVar, i2);
                }
                return true;
            }
            return false;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                if (this.a.c(this.g0)) {
                    v11.o(ci0.f, "Frame %d is cached already.", Integer.valueOf(this.g0));
                    synchronized (ci0.this.e) {
                        ci0.this.e.remove(this.h0);
                    }
                    return;
                }
                if (a(this.g0, 1)) {
                    v11.o(ci0.f, "Prepared frame frame %d.", Integer.valueOf(this.g0));
                } else {
                    v11.f(ci0.f, "Could not prepare frame %d.", Integer.valueOf(this.g0));
                }
                synchronized (ci0.this.e) {
                    ci0.this.e.remove(this.h0);
                }
            } catch (Throwable th) {
                synchronized (ci0.this.e) {
                    ci0.this.e.remove(this.h0);
                    throw th;
                }
            }
        }
    }

    public ci0(br2 br2Var, aq aqVar, Bitmap.Config config, ExecutorService executorService) {
        this.a = br2Var;
        this.b = aqVar;
        this.c = config;
        this.d = executorService;
    }

    public static int g(de deVar, int i) {
        return (deVar.hashCode() * 31) + i;
    }

    @Override // defpackage.zp
    public boolean a(xp xpVar, de deVar, int i) {
        int g = g(deVar, i);
        synchronized (this.e) {
            if (this.e.get(g) != null) {
                v11.o(f, "Already scheduled decode job for frame %d", Integer.valueOf(i));
                return true;
            } else if (xpVar.c(i)) {
                v11.o(f, "Frame %d is cached already.", Integer.valueOf(i));
                return true;
            } else {
                a aVar = new a(deVar, xpVar, i, g);
                this.e.put(g, aVar);
                this.d.execute(aVar);
                return true;
            }
        }
    }
}
