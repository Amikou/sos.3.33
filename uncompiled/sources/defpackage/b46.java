package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: b46  reason: default package */
/* loaded from: classes.dex */
public final class b46 implements a46 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true);

    @Override // defpackage.a46
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
