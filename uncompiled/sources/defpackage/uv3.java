package defpackage;

import java.util.List;

/* compiled from: SubtitleOutputBuffer.java */
/* renamed from: uv3  reason: default package */
/* loaded from: classes.dex */
public abstract class uv3 extends if0 implements qv3 {
    public qv3 g0;
    public long h0;

    @Override // defpackage.qv3
    public int a(long j) {
        return ((qv3) ii.e(this.g0)).a(j - this.h0);
    }

    @Override // defpackage.qv3
    public long d(int i) {
        return ((qv3) ii.e(this.g0)).d(i) + this.h0;
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return ((qv3) ii.e(this.g0)).e(j - this.h0);
    }

    @Override // defpackage.qv3
    public int f() {
        return ((qv3) ii.e(this.g0)).f();
    }

    @Override // defpackage.sr
    public void h() {
        super.h();
        this.g0 = null;
    }

    public void v(long j, qv3 qv3Var, long j2) {
        this.f0 = j;
        this.g0 = qv3Var;
        if (j2 != Long.MAX_VALUE) {
            j = j2;
        }
        this.h0 = j;
    }
}
