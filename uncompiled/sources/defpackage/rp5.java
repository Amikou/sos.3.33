package defpackage;

import com.google.android.gms.internal.measurement.n1;
import com.google.android.gms.internal.measurement.zzgs;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: rp5  reason: default package */
/* loaded from: classes.dex */
public final class rp5 {
    public static z55 a(Object obj) {
        if (obj == null) {
            return z55.Y;
        }
        if (obj instanceof String) {
            return new f65((String) obj);
        }
        if (obj instanceof Double) {
            return new z45((Double) obj);
        }
        if (obj instanceof Long) {
            return new z45(Double.valueOf(((Long) obj).doubleValue()));
        }
        if (obj instanceof Integer) {
            return new z45(Double.valueOf(((Integer) obj).doubleValue()));
        }
        if (obj instanceof Boolean) {
            return new s45((Boolean) obj);
        }
        throw new IllegalArgumentException("Invalid value type");
    }

    public static z55 b(n1 n1Var) {
        if (n1Var == null) {
            return z55.X;
        }
        zzgs zzgsVar = zzgs.UNKNOWN;
        int ordinal = n1Var.x().ordinal();
        if (ordinal != 0) {
            if (ordinal == 1) {
                if (n1Var.A()) {
                    return new f65(n1Var.B());
                }
                return z55.e0;
            } else if (ordinal == 2) {
                if (n1Var.E()) {
                    return new z45(Double.valueOf(n1Var.F()));
                }
                return new z45(null);
            } else if (ordinal == 3) {
                if (n1Var.C()) {
                    return new s45(Boolean.valueOf(n1Var.D()));
                }
                return new s45(null);
            } else if (ordinal == 4) {
                List<n1> y = n1Var.y();
                ArrayList arrayList = new ArrayList();
                for (n1 n1Var2 : y) {
                    arrayList.add(b(n1Var2));
                }
                return new a65(n1Var.z(), arrayList);
            } else {
                String valueOf = String.valueOf(n1Var);
                StringBuilder sb = new StringBuilder(valueOf.length() + 16);
                sb.append("Invalid entity: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            }
        }
        throw new IllegalArgumentException("Unknown type found. Cannot convert entity");
    }
}
