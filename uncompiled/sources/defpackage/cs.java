package defpackage;

/* compiled from: BundleCompat.java */
/* renamed from: cs  reason: default package */
/* loaded from: classes2.dex */
public interface cs<T> {
    void a(String str, Long l);

    Long b(String str);

    T c();

    Integer d(String str);

    String e(String str);

    boolean f(String str);

    boolean getBoolean(String str, boolean z);

    void putString(String str, String str2);
}
