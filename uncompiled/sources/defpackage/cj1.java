package defpackage;

import defpackage.gc4;
import java.util.Arrays;
import zendesk.support.request.CellBase;

/* compiled from: H262Reader.java */
/* renamed from: cj1  reason: default package */
/* loaded from: classes.dex */
public final class cj1 implements ku0 {
    public static final double[] q = {23.976023976023978d, 24.0d, 25.0d, 29.97002997002997d, 30.0d, 50.0d, 59.94005994005994d, 60.0d};
    public String a;
    public f84 b;
    public final xf4 c;
    public final op2 d;
    public final vc2 e;
    public final boolean[] f;
    public final a g;
    public long h;
    public boolean i;
    public boolean j;
    public long k;
    public long l;
    public long m;
    public long n;
    public boolean o;
    public boolean p;

    /* compiled from: H262Reader.java */
    /* renamed from: cj1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final byte[] e = {0, 0, 1};
        public boolean a;
        public int b;
        public int c;
        public byte[] d;

        public a(int i) {
            this.d = new byte[i];
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.a) {
                int i3 = i2 - i;
                byte[] bArr2 = this.d;
                int length = bArr2.length;
                int i4 = this.b;
                if (length < i4 + i3) {
                    this.d = Arrays.copyOf(bArr2, (i4 + i3) * 2);
                }
                System.arraycopy(bArr, i, this.d, this.b, i3);
                this.b += i3;
            }
        }

        public boolean b(int i, int i2) {
            if (this.a) {
                int i3 = this.b - i2;
                this.b = i3;
                if (this.c == 0 && i == 181) {
                    this.c = i3;
                } else {
                    this.a = false;
                    return true;
                }
            } else if (i == 179) {
                this.a = true;
            }
            byte[] bArr = e;
            a(bArr, 0, bArr.length);
            return false;
        }

        public void c() {
            this.a = false;
            this.b = 0;
            this.c = 0;
        }
    }

    public cj1() {
        this(null);
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x0075  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static android.util.Pair<androidx.media3.common.j, java.lang.Long> b(defpackage.cj1.a r8, java.lang.String r9) {
        /*
            byte[] r0 = r8.d
            int r1 = r8.b
            byte[] r0 = java.util.Arrays.copyOf(r0, r1)
            r1 = 4
            r2 = r0[r1]
            r2 = r2 & 255(0xff, float:3.57E-43)
            r3 = 5
            r4 = r0[r3]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 6
            r5 = r0[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r2 = r2 << r1
            int r6 = r4 >> 4
            r2 = r2 | r6
            r4 = r4 & 15
            int r4 = r4 << 8
            r4 = r4 | r5
            r5 = 7
            r6 = r0[r5]
            r6 = r6 & 240(0xf0, float:3.36E-43)
            int r6 = r6 >> r1
            r7 = 2
            if (r6 == r7) goto L3d
            r7 = 3
            if (r6 == r7) goto L37
            if (r6 == r1) goto L31
            r1 = 1065353216(0x3f800000, float:1.0)
            goto L44
        L31:
            int r1 = r4 * 121
            float r1 = (float) r1
            int r6 = r2 * 100
            goto L42
        L37:
            int r1 = r4 * 16
            float r1 = (float) r1
            int r6 = r2 * 9
            goto L42
        L3d:
            int r1 = r4 * 4
            float r1 = (float) r1
            int r6 = r2 * 3
        L42:
            float r6 = (float) r6
            float r1 = r1 / r6
        L44:
            androidx.media3.common.j$b r6 = new androidx.media3.common.j$b
            r6.<init>()
            androidx.media3.common.j$b r9 = r6.S(r9)
            java.lang.String r6 = "video/mpeg2"
            androidx.media3.common.j$b r9 = r9.e0(r6)
            androidx.media3.common.j$b r9 = r9.j0(r2)
            androidx.media3.common.j$b r9 = r9.Q(r4)
            androidx.media3.common.j$b r9 = r9.a0(r1)
            java.util.List r1 = java.util.Collections.singletonList(r0)
            androidx.media3.common.j$b r9 = r9.T(r1)
            androidx.media3.common.j r9 = r9.E()
            r1 = 0
            r4 = r0[r5]
            r4 = r4 & 15
            int r4 = r4 + (-1)
            if (r4 < 0) goto L9c
            double[] r5 = defpackage.cj1.q
            int r6 = r5.length
            if (r4 >= r6) goto L9c
            r1 = r5[r4]
            int r8 = r8.c
            int r8 = r8 + 9
            r4 = r0[r8]
            r4 = r4 & 96
            int r3 = r4 >> 5
            r8 = r0[r8]
            r8 = r8 & 31
            if (r3 == r8) goto L95
            double r3 = (double) r3
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r3 = r3 + r5
            int r8 = r8 + 1
            double r5 = (double) r8
            double r3 = r3 / r5
            double r1 = r1 * r3
        L95:
            r3 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r3 = r3 / r1
            long r1 = (long) r3
        L9c:
            java.lang.Long r8 = java.lang.Long.valueOf(r1)
            android.util.Pair r8 = android.util.Pair.create(r9, r8)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cj1.b(cj1$a, java.lang.String):android.util.Pair");
    }

    /* JADX WARN: Removed duplicated region for block: B:59:0x012c  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x0142  */
    @Override // defpackage.ku0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void a(defpackage.op2 r21) {
        /*
            Method dump skipped, instructions count: 328
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cj1.a(op2):void");
    }

    @Override // defpackage.ku0
    public void c() {
        wc2.a(this.f);
        this.g.c();
        vc2 vc2Var = this.e;
        if (vc2Var != null) {
            vc2Var.d();
        }
        this.h = 0L;
        this.i = false;
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.n = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        this.l = j;
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.a = dVar.b();
        this.b = r11Var.f(dVar.c(), 2);
        xf4 xf4Var = this.c;
        if (xf4Var != null) {
            xf4Var.b(r11Var, dVar);
        }
    }

    public cj1(xf4 xf4Var) {
        this.c = xf4Var;
        this.f = new boolean[4];
        this.g = new a(128);
        if (xf4Var != null) {
            this.e = new vc2(178, 128);
            this.d = new op2();
        } else {
            this.e = null;
            this.d = null;
        }
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.n = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
