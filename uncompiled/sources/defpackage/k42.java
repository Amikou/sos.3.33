package defpackage;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.AttributeSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.card.MaterialCardView;
import defpackage.pn3;

/* compiled from: MaterialCardViewHelper.java */
/* renamed from: k42  reason: default package */
/* loaded from: classes2.dex */
public class k42 {
    public static final int[] t = {16842912};
    public static final double u = Math.cos(Math.toRadians(45.0d));
    public final MaterialCardView a;
    public final o42 c;
    public final o42 d;
    public int e;
    public int f;
    public int g;
    public Drawable h;
    public Drawable i;
    public ColorStateList j;
    public ColorStateList k;
    public pn3 l;
    public ColorStateList m;
    public Drawable n;
    public LayerDrawable o;
    public o42 p;
    public o42 q;
    public boolean s;
    public final Rect b = new Rect();
    public boolean r = false;

    /* compiled from: MaterialCardViewHelper.java */
    /* renamed from: k42$a */
    /* loaded from: classes2.dex */
    public class a extends InsetDrawable {
        public a(k42 k42Var, Drawable drawable, int i, int i2, int i3, int i4) {
            super(drawable, i, i2, i3, i4);
        }

        @Override // android.graphics.drawable.Drawable
        public int getMinimumHeight() {
            return -1;
        }

        @Override // android.graphics.drawable.Drawable
        public int getMinimumWidth() {
            return -1;
        }

        @Override // android.graphics.drawable.InsetDrawable, android.graphics.drawable.DrawableWrapper, android.graphics.drawable.Drawable
        public boolean getPadding(Rect rect) {
            return false;
        }
    }

    public k42(MaterialCardView materialCardView, AttributeSet attributeSet, int i, int i2) {
        this.a = materialCardView;
        o42 o42Var = new o42(materialCardView.getContext(), attributeSet, i, i2);
        this.c = o42Var;
        o42Var.P(materialCardView.getContext());
        o42Var.g0(-12303292);
        pn3.b v = o42Var.D().v();
        TypedArray obtainStyledAttributes = materialCardView.getContext().obtainStyledAttributes(attributeSet, o23.CardView, i, y13.CardView);
        int i3 = o23.CardView_cardCornerRadius;
        if (obtainStyledAttributes.hasValue(i3)) {
            v.o(obtainStyledAttributes.getDimension(i3, Utils.FLOAT_EPSILON));
        }
        this.d = new o42();
        R(v.m());
        obtainStyledAttributes.recycle();
    }

    public Rect A() {
        return this.b;
    }

    public final Drawable B(Drawable drawable) {
        int ceil;
        int i;
        if ((Build.VERSION.SDK_INT < 21) || this.a.getUseCompatPadding()) {
            int ceil2 = (int) Math.ceil(d());
            ceil = (int) Math.ceil(c());
            i = ceil2;
        } else {
            ceil = 0;
            i = 0;
        }
        return new a(this, drawable, ceil, i, ceil, i);
    }

    public boolean C() {
        return this.r;
    }

    public boolean D() {
        return this.s;
    }

    public void E(TypedArray typedArray) {
        ColorStateList b = n42.b(this.a.getContext(), typedArray, o23.MaterialCardView_strokeColor);
        this.m = b;
        if (b == null) {
            this.m = ColorStateList.valueOf(-1);
        }
        this.g = typedArray.getDimensionPixelSize(o23.MaterialCardView_strokeWidth, 0);
        boolean z = typedArray.getBoolean(o23.MaterialCardView_android_checkable, false);
        this.s = z;
        this.a.setLongClickable(z);
        this.k = n42.b(this.a.getContext(), typedArray, o23.MaterialCardView_checkedIconTint);
        K(n42.d(this.a.getContext(), typedArray, o23.MaterialCardView_checkedIcon));
        M(typedArray.getDimensionPixelSize(o23.MaterialCardView_checkedIconSize, 0));
        L(typedArray.getDimensionPixelSize(o23.MaterialCardView_checkedIconMargin, 0));
        ColorStateList b2 = n42.b(this.a.getContext(), typedArray, o23.MaterialCardView_rippleColor);
        this.j = b2;
        if (b2 == null) {
            this.j = ColorStateList.valueOf(l42.d(this.a, gy2.colorControlHighlight));
        }
        I(n42.b(this.a.getContext(), typedArray, o23.MaterialCardView_cardForegroundColor));
        c0();
        Z();
        d0();
        this.a.setBackgroundInternal(B(this.c));
        Drawable r = this.a.isClickable() ? r() : this.d;
        this.h = r;
        this.a.setForeground(B(r));
    }

    public void F(int i, int i2) {
        int i3;
        int i4;
        if (this.o != null) {
            int i5 = this.e;
            int i6 = this.f;
            int i7 = (i - i5) - i6;
            int i8 = (i2 - i5) - i6;
            if ((Build.VERSION.SDK_INT < 21) || this.a.getUseCompatPadding()) {
                i8 -= (int) Math.ceil(d() * 2.0f);
                i7 -= (int) Math.ceil(c() * 2.0f);
            }
            int i9 = i8;
            int i10 = this.e;
            if (ei4.E(this.a) == 1) {
                i4 = i7;
                i3 = i10;
            } else {
                i3 = i7;
                i4 = i10;
            }
            this.o.setLayerInset(2, i3, this.e, i4, i9);
        }
    }

    public void G(boolean z) {
        this.r = z;
    }

    public void H(ColorStateList colorStateList) {
        this.c.a0(colorStateList);
    }

    public void I(ColorStateList colorStateList) {
        o42 o42Var = this.d;
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        o42Var.a0(colorStateList);
    }

    public void J(boolean z) {
        this.s = z;
    }

    public void K(Drawable drawable) {
        this.i = drawable;
        if (drawable != null) {
            Drawable r = androidx.core.graphics.drawable.a.r(drawable.mutate());
            this.i = r;
            androidx.core.graphics.drawable.a.o(r, this.k);
        }
        if (this.o != null) {
            this.o.setDrawableByLayerId(b03.mtrl_card_checked_layer_id, f());
        }
    }

    public void L(int i) {
        this.e = i;
    }

    public void M(int i) {
        this.f = i;
    }

    public void N(ColorStateList colorStateList) {
        this.k = colorStateList;
        Drawable drawable = this.i;
        if (drawable != null) {
            androidx.core.graphics.drawable.a.o(drawable, colorStateList);
        }
    }

    public void O(float f) {
        R(this.l.w(f));
        this.h.invalidateSelf();
        if (W() || V()) {
            Y();
        }
        if (W()) {
            b0();
        }
    }

    public void P(float f) {
        this.c.b0(f);
        o42 o42Var = this.d;
        if (o42Var != null) {
            o42Var.b0(f);
        }
        o42 o42Var2 = this.q;
        if (o42Var2 != null) {
            o42Var2.b0(f);
        }
    }

    public void Q(ColorStateList colorStateList) {
        this.j = colorStateList;
        c0();
    }

    public void R(pn3 pn3Var) {
        this.l = pn3Var;
        this.c.setShapeAppearanceModel(pn3Var);
        o42 o42Var = this.c;
        o42Var.f0(!o42Var.S());
        o42 o42Var2 = this.d;
        if (o42Var2 != null) {
            o42Var2.setShapeAppearanceModel(pn3Var);
        }
        o42 o42Var3 = this.q;
        if (o42Var3 != null) {
            o42Var3.setShapeAppearanceModel(pn3Var);
        }
        o42 o42Var4 = this.p;
        if (o42Var4 != null) {
            o42Var4.setShapeAppearanceModel(pn3Var);
        }
    }

    public void S(ColorStateList colorStateList) {
        if (this.m == colorStateList) {
            return;
        }
        this.m = colorStateList;
        d0();
    }

    public void T(int i) {
        if (i == this.g) {
            return;
        }
        this.g = i;
        d0();
    }

    public void U(int i, int i2, int i3, int i4) {
        this.b.set(i, i2, i3, i4);
        Y();
    }

    public final boolean V() {
        return this.a.getPreventCornerOverlap() && !e();
    }

    public final boolean W() {
        return this.a.getPreventCornerOverlap() && e() && this.a.getUseCompatPadding();
    }

    public void X() {
        Drawable drawable = this.h;
        Drawable r = this.a.isClickable() ? r() : this.d;
        this.h = r;
        if (drawable != r) {
            a0(r);
        }
    }

    public void Y() {
        int a2 = (int) ((V() || W() ? a() : Utils.FLOAT_EPSILON) - t());
        MaterialCardView materialCardView = this.a;
        Rect rect = this.b;
        materialCardView.l(rect.left + a2, rect.top + a2, rect.right + a2, rect.bottom + a2);
    }

    public void Z() {
        this.c.Z(this.a.getCardElevation());
    }

    public final float a() {
        return Math.max(Math.max(b(this.l.q(), this.c.I()), b(this.l.s(), this.c.J())), Math.max(b(this.l.k(), this.c.t()), b(this.l.i(), this.c.s())));
    }

    public final void a0(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 23 && (this.a.getForeground() instanceof InsetDrawable)) {
            ((InsetDrawable) this.a.getForeground()).setDrawable(drawable);
        } else {
            this.a.setForeground(B(drawable));
        }
    }

    public final float b(w80 w80Var, float f) {
        if (w80Var instanceof v93) {
            return (float) ((1.0d - u) * f);
        }
        return w80Var instanceof ad0 ? f / 2.0f : Utils.FLOAT_EPSILON;
    }

    public void b0() {
        if (!C()) {
            this.a.setBackgroundInternal(B(this.c));
        }
        this.a.setForeground(B(this.h));
    }

    public final float c() {
        return this.a.getMaxCardElevation() + (W() ? a() : Utils.FLOAT_EPSILON);
    }

    public final void c0() {
        Drawable drawable;
        if (b93.a && (drawable = this.n) != null) {
            ((RippleDrawable) drawable).setColor(this.j);
            return;
        }
        o42 o42Var = this.p;
        if (o42Var != null) {
            o42Var.a0(this.j);
        }
    }

    public final float d() {
        return (this.a.getMaxCardElevation() * 1.5f) + (W() ? a() : Utils.FLOAT_EPSILON);
    }

    public void d0() {
        this.d.k0(this.g, this.m);
    }

    public final boolean e() {
        return Build.VERSION.SDK_INT >= 21 && this.c.S();
    }

    public final Drawable f() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = this.i;
        if (drawable != null) {
            stateListDrawable.addState(t, drawable);
        }
        return stateListDrawable;
    }

    public final Drawable g() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        o42 i = i();
        this.p = i;
        i.a0(this.j);
        stateListDrawable.addState(new int[]{16842919}, this.p);
        return stateListDrawable;
    }

    public final Drawable h() {
        if (b93.a) {
            this.q = i();
            return new RippleDrawable(this.j, null, this.q);
        }
        return g();
    }

    public final o42 i() {
        return new o42(this.l);
    }

    public void j() {
        Drawable drawable = this.n;
        if (drawable != null) {
            Rect bounds = drawable.getBounds();
            int i = bounds.bottom;
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i - 1);
            this.n.setBounds(bounds.left, bounds.top, bounds.right, i);
        }
    }

    public o42 k() {
        return this.c;
    }

    public ColorStateList l() {
        return this.c.x();
    }

    public ColorStateList m() {
        return this.d.x();
    }

    public Drawable n() {
        return this.i;
    }

    public int o() {
        return this.e;
    }

    public int p() {
        return this.f;
    }

    public ColorStateList q() {
        return this.k;
    }

    public final Drawable r() {
        if (this.n == null) {
            this.n = h();
        }
        if (this.o == null) {
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{this.n, this.d, f()});
            this.o = layerDrawable;
            layerDrawable.setId(2, b03.mtrl_card_checked_layer_id);
        }
        return this.o;
    }

    public float s() {
        return this.c.I();
    }

    public final float t() {
        return this.a.getPreventCornerOverlap() ? (Build.VERSION.SDK_INT < 21 || this.a.getUseCompatPadding()) ? (float) ((1.0d - u) * this.a.getCardViewRadius()) : Utils.FLOAT_EPSILON : Utils.FLOAT_EPSILON;
    }

    public float u() {
        return this.c.y();
    }

    public ColorStateList v() {
        return this.j;
    }

    public pn3 w() {
        return this.l;
    }

    public int x() {
        ColorStateList colorStateList = this.m;
        if (colorStateList == null) {
            return -1;
        }
        return colorStateList.getDefaultColor();
    }

    public ColorStateList y() {
        return this.m;
    }

    public int z() {
        return this.g;
    }
}
