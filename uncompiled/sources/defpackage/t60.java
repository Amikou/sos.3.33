package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.contact.room.RoomContact;

/* compiled from: ContactDao.kt */
/* renamed from: t60  reason: default package */
/* loaded from: classes2.dex */
public interface t60 {
    LiveData<List<RoomContact>> a();

    Object b(String str, int i, q70<? super List<RoomContact>> q70Var);

    void c(String str);

    void d(int i, long j);

    void e(RoomContact roomContact);

    Object f(RoomContact roomContact, q70<? super te4> q70Var);
}
