package defpackage;

/* compiled from: ImagePerfImageOriginListener.java */
/* renamed from: lo1  reason: default package */
/* loaded from: classes.dex */
public class lo1 implements fo1 {
    public final po1 a;
    public final mo1 b;

    public lo1(po1 po1Var, mo1 mo1Var) {
        this.a = po1Var;
        this.b = mo1Var;
    }

    @Override // defpackage.fo1
    public void a(String str, int i, boolean z, String str2) {
        this.a.p(i);
        this.a.y(str2);
        this.b.b(this.a, 1);
    }
}
