package defpackage;

import java.util.Iterator;

/* renamed from: pi5  reason: default package */
/* loaded from: classes.dex */
public final class pi5 implements Iterator<String> {
    public Iterator<String> a;
    public final /* synthetic */ ki5 f0;

    public pi5(ki5 ki5Var) {
        jc5 jc5Var;
        this.f0 = ki5Var;
        jc5Var = ki5Var.a;
        this.a = jc5Var.iterator();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.Iterator
    public final /* synthetic */ String next() {
        return this.a.next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
