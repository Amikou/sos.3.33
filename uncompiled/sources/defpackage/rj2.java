package defpackage;

import android.os.Build;
import com.onesignal.OSInAppMessageController;
import com.onesignal.a1;
import com.onesignal.w;
import com.onesignal.w0;

/* compiled from: OSInAppMessageControllerFactory.java */
/* renamed from: rj2  reason: default package */
/* loaded from: classes2.dex */
public class rj2 {
    public static final Object b = new Object();
    public OSInAppMessageController a;

    public OSInAppMessageController a(a1 a1Var, w0 w0Var, yj2 yj2Var, vk2 vk2Var, cy1 cy1Var) {
        if (this.a == null) {
            synchronized (b) {
                if (this.a == null) {
                    if (Build.VERSION.SDK_INT <= 18) {
                        this.a = new w(null, w0Var, yj2Var, vk2Var, cy1Var);
                    } else {
                        this.a = new OSInAppMessageController(a1Var, w0Var, yj2Var, vk2Var, cy1Var);
                    }
                }
            }
        }
        return this.a;
    }
}
