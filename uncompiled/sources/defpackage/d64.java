package defpackage;

import java.util.Calendar;
import java.util.TimeZone;

/* compiled from: TimeSource.java */
/* renamed from: d64  reason: default package */
/* loaded from: classes2.dex */
public class d64 {
    public static final d64 c = new d64(null, null);
    public final Long a;
    public final TimeZone b;

    public d64(Long l, TimeZone timeZone) {
        this.a = l;
        this.b = timeZone;
    }

    public static d64 c() {
        return c;
    }

    public Calendar a() {
        return b(this.b);
    }

    public Calendar b(TimeZone timeZone) {
        Calendar calendar = timeZone == null ? Calendar.getInstance() : Calendar.getInstance(timeZone);
        Long l = this.a;
        if (l != null) {
            calendar.setTimeInMillis(l.longValue());
        }
        return calendar;
    }
}
