package defpackage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import defpackage.ag1;
import java.nio.ByteBuffer;
import java.util.List;

/* compiled from: GifDrawable.java */
/* renamed from: uf1  reason: default package */
/* loaded from: classes.dex */
public class uf1 extends Drawable implements ag1.b, Animatable {
    public final a a;
    public boolean f0;
    public boolean g0;
    public boolean h0;
    public boolean i0;
    public int j0;
    public int k0;
    public boolean l0;
    public Paint m0;
    public Rect n0;
    public List<hd> o0;

    /* compiled from: GifDrawable.java */
    /* renamed from: uf1$a */
    /* loaded from: classes.dex */
    public static final class a extends Drawable.ConstantState {
        public final ag1 a;

        public a(ag1 ag1Var) {
            this.a = ag1Var;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public int getChangingConfigurations() {
            return 0;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public Drawable newDrawable() {
            return new uf1(this);
        }
    }

    public uf1(Context context, tf1 tf1Var, za4<Bitmap> za4Var, int i, int i2, Bitmap bitmap) {
        this(new a(new ag1(com.bumptech.glide.a.c(context), tf1Var, i, i2, za4Var, bitmap)));
    }

    @Override // defpackage.ag1.b
    public void a() {
        if (b() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (g() == f() - 1) {
            this.j0++;
        }
        int i = this.k0;
        if (i == -1 || this.j0 < i) {
            return;
        }
        j();
        stop();
    }

    public final Drawable.Callback b() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    public ByteBuffer c() {
        return this.a.a.b();
    }

    public final Rect d() {
        if (this.n0 == null) {
            this.n0 = new Rect();
        }
        return this.n0;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (this.h0) {
            return;
        }
        if (this.l0) {
            Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), d());
            this.l0 = false;
        }
        canvas.drawBitmap(this.a.a.c(), (Rect) null, d(), h());
    }

    public Bitmap e() {
        return this.a.a.e();
    }

    public int f() {
        return this.a.a.f();
    }

    public int g() {
        return this.a.a.d();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.a.a.h();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.a.a.k();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -2;
    }

    public final Paint h() {
        if (this.m0 == null) {
            this.m0 = new Paint(2);
        }
        return this.m0;
    }

    public int i() {
        return this.a.a.j();
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.f0;
    }

    public final void j() {
        List<hd> list = this.o0;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                this.o0.get(i).onAnimationEnd(this);
            }
        }
    }

    public void k() {
        this.h0 = true;
        this.a.a.a();
    }

    public final void l() {
        this.j0 = 0;
    }

    public void m(za4<Bitmap> za4Var, Bitmap bitmap) {
        this.a.a.o(za4Var, bitmap);
    }

    public final void n() {
        wt2.a(!this.h0, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.a.a.f() == 1) {
            invalidateSelf();
        } else if (this.f0) {
        } else {
            this.f0 = true;
            this.a.a.r(this);
            invalidateSelf();
        }
    }

    public final void o() {
        this.f0 = false;
        this.a.a.s(this);
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.l0 = true;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        h().setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        h().setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        wt2.a(!this.h0, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.i0 = z;
        if (!z) {
            o();
        } else if (this.g0) {
            n();
        }
        return super.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        this.g0 = true;
        l();
        if (this.i0) {
            n();
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        this.g0 = false;
        o();
    }

    public uf1(a aVar) {
        this.i0 = true;
        this.k0 = -1;
        this.a = (a) wt2.d(aVar);
    }
}
