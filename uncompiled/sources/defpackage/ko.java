package defpackage;

import androidx.media3.decoder.DecoderInputBuffer;
import java.nio.ByteBuffer;

/* compiled from: BatchBuffer.java */
/* renamed from: ko  reason: default package */
/* loaded from: classes.dex */
public final class ko extends DecoderInputBuffer {
    public long m0;
    public int n0;
    public int o0;

    public ko() {
        super(2);
        this.o0 = 32;
    }

    public boolean C(DecoderInputBuffer decoderInputBuffer) {
        ii.a(!decoderInputBuffer.y());
        ii.a(!decoderInputBuffer.l());
        ii.a(!decoderInputBuffer.p());
        if (D(decoderInputBuffer)) {
            int i = this.n0;
            this.n0 = i + 1;
            if (i == 0) {
                this.i0 = decoderInputBuffer.i0;
                if (decoderInputBuffer.s()) {
                    t(1);
                }
            }
            if (decoderInputBuffer.o()) {
                t(Integer.MIN_VALUE);
            }
            ByteBuffer byteBuffer = decoderInputBuffer.g0;
            if (byteBuffer != null) {
                v(byteBuffer.remaining());
                this.g0.put(byteBuffer);
            }
            this.m0 = decoderInputBuffer.i0;
            return true;
        }
        return false;
    }

    public final boolean D(DecoderInputBuffer decoderInputBuffer) {
        ByteBuffer byteBuffer;
        if (I()) {
            if (this.n0 < this.o0 && decoderInputBuffer.o() == o()) {
                ByteBuffer byteBuffer2 = decoderInputBuffer.g0;
                return byteBuffer2 == null || (byteBuffer = this.g0) == null || byteBuffer.position() + byteBuffer2.remaining() <= 3072000;
            }
            return false;
        }
        return true;
    }

    public long E() {
        return this.i0;
    }

    public long G() {
        return this.m0;
    }

    public int H() {
        return this.n0;
    }

    public boolean I() {
        return this.n0 > 0;
    }

    public void J(int i) {
        ii.a(i > 0);
        this.o0 = i;
    }

    @Override // androidx.media3.decoder.DecoderInputBuffer, defpackage.sr
    public void h() {
        super.h();
        this.n0 = 0;
    }
}
