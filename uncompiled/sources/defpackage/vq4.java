package defpackage;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import androidx.work.WorkInfo;
import defpackage.tq4;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WorkSpecDao_Impl.java */
/* renamed from: vq4  reason: default package */
/* loaded from: classes.dex */
public final class vq4 implements uq4 {
    public final RoomDatabase a;
    public final zv0<tq4> b;
    public final co3 c;
    public final co3 d;
    public final co3 e;
    public final co3 f;
    public final co3 g;
    public final co3 h;
    public final co3 i;

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$a */
    /* loaded from: classes.dex */
    public class a extends zv0<tq4> {
        public a(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`out_of_quota_policy`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, tq4 tq4Var) {
            String str = tq4Var.a;
            if (str == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, str);
            }
            ww3Var.q0(2, ar4.j(tq4Var.b));
            String str2 = tq4Var.c;
            if (str2 == null) {
                ww3Var.Y0(3);
            } else {
                ww3Var.L(3, str2);
            }
            String str3 = tq4Var.d;
            if (str3 == null) {
                ww3Var.Y0(4);
            } else {
                ww3Var.L(4, str3);
            }
            byte[] n = androidx.work.b.n(tq4Var.e);
            if (n == null) {
                ww3Var.Y0(5);
            } else {
                ww3Var.A0(5, n);
            }
            byte[] n2 = androidx.work.b.n(tq4Var.f);
            if (n2 == null) {
                ww3Var.Y0(6);
            } else {
                ww3Var.A0(6, n2);
            }
            ww3Var.q0(7, tq4Var.g);
            ww3Var.q0(8, tq4Var.h);
            ww3Var.q0(9, tq4Var.i);
            ww3Var.q0(10, tq4Var.k);
            ww3Var.q0(11, ar4.a(tq4Var.l));
            ww3Var.q0(12, tq4Var.m);
            ww3Var.q0(13, tq4Var.n);
            ww3Var.q0(14, tq4Var.o);
            ww3Var.q0(15, tq4Var.p);
            ww3Var.q0(16, tq4Var.q ? 1L : 0L);
            ww3Var.q0(17, ar4.i(tq4Var.r));
            h60 h60Var = tq4Var.j;
            if (h60Var != null) {
                ww3Var.q0(18, ar4.h(h60Var.b()));
                ww3Var.q0(19, h60Var.g() ? 1L : 0L);
                ww3Var.q0(20, h60Var.h() ? 1L : 0L);
                ww3Var.q0(21, h60Var.f() ? 1L : 0L);
                ww3Var.q0(22, h60Var.i() ? 1L : 0L);
                ww3Var.q0(23, h60Var.c());
                ww3Var.q0(24, h60Var.d());
                byte[] c = ar4.c(h60Var.a());
                if (c == null) {
                    ww3Var.Y0(25);
                    return;
                } else {
                    ww3Var.A0(25, c);
                    return;
                }
            }
            ww3Var.Y0(18);
            ww3Var.Y0(19);
            ww3Var.Y0(20);
            ww3Var.Y0(21);
            ww3Var.Y0(22);
            ww3Var.Y0(23);
            ww3Var.Y0(24);
            ww3Var.Y0(25);
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$b */
    /* loaded from: classes.dex */
    public class b extends co3 {
        public b(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$c */
    /* loaded from: classes.dex */
    public class c extends co3 {
        public c(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$d */
    /* loaded from: classes.dex */
    public class d extends co3 {
        public d(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$e */
    /* loaded from: classes.dex */
    public class e extends co3 {
        public e(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$f */
    /* loaded from: classes.dex */
    public class f extends co3 {
        public f(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$g */
    /* loaded from: classes.dex */
    public class g extends co3 {
        public g(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$h */
    /* loaded from: classes.dex */
    public class h extends co3 {
        public h(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    /* compiled from: WorkSpecDao_Impl.java */
    /* renamed from: vq4$i */
    /* loaded from: classes.dex */
    public class i extends co3 {
        public i(vq4 vq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    public vq4(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
        this.d = new c(this, roomDatabase);
        this.e = new d(this, roomDatabase);
        this.f = new e(this, roomDatabase);
        this.g = new f(this, roomDatabase);
        this.h = new g(this, roomDatabase);
        this.i = new h(this, roomDatabase);
        new i(this, roomDatabase);
    }

    @Override // defpackage.uq4
    public int a(WorkInfo.State state, String... strArr) {
        this.a.d();
        StringBuilder b2 = qu3.b();
        b2.append("UPDATE workspec SET state=");
        b2.append("?");
        b2.append(" WHERE id IN (");
        qu3.a(b2, strArr.length);
        b2.append(")");
        ww3 g2 = this.a.g(b2.toString());
        g2.q0(1, ar4.j(state));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                g2.Y0(i2);
            } else {
                g2.L(i2, str);
            }
            i2++;
        }
        this.a.e();
        try {
            int T = g2.T();
            this.a.E();
            return T;
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.uq4
    public int b(String str, long j) {
        this.a.d();
        ww3 a2 = this.h.a();
        a2.q0(1, j);
        if (str == null) {
            a2.Y0(2);
        } else {
            a2.L(2, str);
        }
        this.a.e();
        try {
            int T = a2.T();
            this.a.E();
            return T;
        } finally {
            this.a.j();
            this.h.f(a2);
        }
    }

    @Override // defpackage.uq4
    public List<tq4.b> c(String str) {
        k93 c2 = k93.c("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            int e2 = wb0.e(c3, "id");
            int e3 = wb0.e(c3, "state");
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                tq4.b bVar = new tq4.b();
                bVar.a = c3.getString(e2);
                bVar.b = ar4.g(c3.getInt(e3));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.uq4
    public List<tq4> d(long j) {
        k93 k93Var;
        int e2;
        int e3;
        int e4;
        int e5;
        int e6;
        int e7;
        int e8;
        int e9;
        int e10;
        int e11;
        int e12;
        int e13;
        int e14;
        int e15;
        k93 c2 = k93.c("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE period_start_time >= ? AND state IN (2, 3, 5) ORDER BY period_start_time DESC", 1);
        c2.q0(1, j);
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            e2 = wb0.e(c3, "required_network_type");
            e3 = wb0.e(c3, "requires_charging");
            e4 = wb0.e(c3, "requires_device_idle");
            e5 = wb0.e(c3, "requires_battery_not_low");
            e6 = wb0.e(c3, "requires_storage_not_low");
            e7 = wb0.e(c3, "trigger_content_update_delay");
            e8 = wb0.e(c3, "trigger_max_content_delay");
            e9 = wb0.e(c3, "content_uri_triggers");
            e10 = wb0.e(c3, "id");
            e11 = wb0.e(c3, "state");
            e12 = wb0.e(c3, "worker_class_name");
            e13 = wb0.e(c3, "input_merger_class_name");
            e14 = wb0.e(c3, "input");
            e15 = wb0.e(c3, "output");
            k93Var = c2;
        } catch (Throwable th) {
            th = th;
            k93Var = c2;
        }
        try {
            int e16 = wb0.e(c3, "initial_delay");
            int e17 = wb0.e(c3, "interval_duration");
            int e18 = wb0.e(c3, "flex_duration");
            int e19 = wb0.e(c3, "run_attempt_count");
            int e20 = wb0.e(c3, "backoff_policy");
            int e21 = wb0.e(c3, "backoff_delay_duration");
            int e22 = wb0.e(c3, "period_start_time");
            int e23 = wb0.e(c3, "minimum_retention_duration");
            int e24 = wb0.e(c3, "schedule_requested_at");
            int e25 = wb0.e(c3, "run_in_foreground");
            int e26 = wb0.e(c3, "out_of_quota_policy");
            int i2 = e15;
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                String string = c3.getString(e10);
                int i3 = e10;
                String string2 = c3.getString(e12);
                int i4 = e12;
                h60 h60Var = new h60();
                int i5 = e2;
                h60Var.k(ar4.e(c3.getInt(e2)));
                h60Var.m(c3.getInt(e3) != 0);
                h60Var.n(c3.getInt(e4) != 0);
                h60Var.l(c3.getInt(e5) != 0);
                h60Var.o(c3.getInt(e6) != 0);
                int i6 = e3;
                int i7 = e4;
                h60Var.p(c3.getLong(e7));
                h60Var.q(c3.getLong(e8));
                h60Var.j(ar4.b(c3.getBlob(e9)));
                tq4 tq4Var = new tq4(string, string2);
                tq4Var.b = ar4.g(c3.getInt(e11));
                tq4Var.d = c3.getString(e13);
                tq4Var.e = androidx.work.b.g(c3.getBlob(e14));
                int i8 = i2;
                tq4Var.f = androidx.work.b.g(c3.getBlob(i8));
                int i9 = e16;
                i2 = i8;
                tq4Var.g = c3.getLong(i9);
                int i10 = e13;
                int i11 = e17;
                tq4Var.h = c3.getLong(i11);
                int i12 = e5;
                int i13 = e18;
                tq4Var.i = c3.getLong(i13);
                int i14 = e19;
                tq4Var.k = c3.getInt(i14);
                int i15 = e20;
                tq4Var.l = ar4.d(c3.getInt(i15));
                e18 = i13;
                int i16 = e21;
                tq4Var.m = c3.getLong(i16);
                int i17 = e22;
                tq4Var.n = c3.getLong(i17);
                e22 = i17;
                int i18 = e23;
                tq4Var.o = c3.getLong(i18);
                int i19 = e24;
                tq4Var.p = c3.getLong(i19);
                int i20 = e25;
                tq4Var.q = c3.getInt(i20) != 0;
                int i21 = e26;
                tq4Var.r = ar4.f(c3.getInt(i21));
                tq4Var.j = h60Var;
                arrayList.add(tq4Var);
                e3 = i6;
                e26 = i21;
                e13 = i10;
                e16 = i9;
                e17 = i11;
                e19 = i14;
                e24 = i19;
                e10 = i3;
                e12 = i4;
                e2 = i5;
                e25 = i20;
                e23 = i18;
                e4 = i7;
                e21 = i16;
                e5 = i12;
                e20 = i15;
            }
            c3.close();
            k93Var.f();
            return arrayList;
        } catch (Throwable th2) {
            th = th2;
            c3.close();
            k93Var.f();
            throw th;
        }
    }

    @Override // defpackage.uq4
    public void delete(String str) {
        this.a.d();
        ww3 a2 = this.c.a();
        if (str == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.c.f(a2);
        }
    }

    @Override // defpackage.uq4
    public List<tq4> e(int i2) {
        k93 k93Var;
        int e2;
        int e3;
        int e4;
        int e5;
        int e6;
        int e7;
        int e8;
        int e9;
        int e10;
        int e11;
        int e12;
        int e13;
        int e14;
        int e15;
        k93 c2 = k93.c("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        c2.q0(1, i2);
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            e2 = wb0.e(c3, "required_network_type");
            e3 = wb0.e(c3, "requires_charging");
            e4 = wb0.e(c3, "requires_device_idle");
            e5 = wb0.e(c3, "requires_battery_not_low");
            e6 = wb0.e(c3, "requires_storage_not_low");
            e7 = wb0.e(c3, "trigger_content_update_delay");
            e8 = wb0.e(c3, "trigger_max_content_delay");
            e9 = wb0.e(c3, "content_uri_triggers");
            e10 = wb0.e(c3, "id");
            e11 = wb0.e(c3, "state");
            e12 = wb0.e(c3, "worker_class_name");
            e13 = wb0.e(c3, "input_merger_class_name");
            e14 = wb0.e(c3, "input");
            e15 = wb0.e(c3, "output");
            k93Var = c2;
        } catch (Throwable th) {
            th = th;
            k93Var = c2;
        }
        try {
            int e16 = wb0.e(c3, "initial_delay");
            int e17 = wb0.e(c3, "interval_duration");
            int e18 = wb0.e(c3, "flex_duration");
            int e19 = wb0.e(c3, "run_attempt_count");
            int e20 = wb0.e(c3, "backoff_policy");
            int e21 = wb0.e(c3, "backoff_delay_duration");
            int e22 = wb0.e(c3, "period_start_time");
            int e23 = wb0.e(c3, "minimum_retention_duration");
            int e24 = wb0.e(c3, "schedule_requested_at");
            int e25 = wb0.e(c3, "run_in_foreground");
            int e26 = wb0.e(c3, "out_of_quota_policy");
            int i3 = e15;
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                String string = c3.getString(e10);
                int i4 = e10;
                String string2 = c3.getString(e12);
                int i5 = e12;
                h60 h60Var = new h60();
                int i6 = e2;
                h60Var.k(ar4.e(c3.getInt(e2)));
                h60Var.m(c3.getInt(e3) != 0);
                h60Var.n(c3.getInt(e4) != 0);
                h60Var.l(c3.getInt(e5) != 0);
                h60Var.o(c3.getInt(e6) != 0);
                int i7 = e3;
                int i8 = e4;
                h60Var.p(c3.getLong(e7));
                h60Var.q(c3.getLong(e8));
                h60Var.j(ar4.b(c3.getBlob(e9)));
                tq4 tq4Var = new tq4(string, string2);
                tq4Var.b = ar4.g(c3.getInt(e11));
                tq4Var.d = c3.getString(e13);
                tq4Var.e = androidx.work.b.g(c3.getBlob(e14));
                int i9 = i3;
                tq4Var.f = androidx.work.b.g(c3.getBlob(i9));
                i3 = i9;
                int i10 = e16;
                tq4Var.g = c3.getLong(i10);
                int i11 = e13;
                int i12 = e17;
                tq4Var.h = c3.getLong(i12);
                int i13 = e5;
                int i14 = e18;
                tq4Var.i = c3.getLong(i14);
                int i15 = e19;
                tq4Var.k = c3.getInt(i15);
                int i16 = e20;
                tq4Var.l = ar4.d(c3.getInt(i16));
                e18 = i14;
                int i17 = e21;
                tq4Var.m = c3.getLong(i17);
                int i18 = e22;
                tq4Var.n = c3.getLong(i18);
                e22 = i18;
                int i19 = e23;
                tq4Var.o = c3.getLong(i19);
                int i20 = e24;
                tq4Var.p = c3.getLong(i20);
                int i21 = e25;
                tq4Var.q = c3.getInt(i21) != 0;
                int i22 = e26;
                tq4Var.r = ar4.f(c3.getInt(i22));
                tq4Var.j = h60Var;
                arrayList.add(tq4Var);
                e26 = i22;
                e3 = i7;
                e13 = i11;
                e16 = i10;
                e17 = i12;
                e19 = i15;
                e24 = i20;
                e10 = i4;
                e12 = i5;
                e2 = i6;
                e25 = i21;
                e23 = i19;
                e4 = i8;
                e21 = i17;
                e5 = i13;
                e20 = i16;
            }
            c3.close();
            k93Var.f();
            return arrayList;
        } catch (Throwable th2) {
            th = th2;
            c3.close();
            k93Var.f();
            throw th;
        }
    }

    @Override // defpackage.uq4
    public List<tq4> f() {
        k93 k93Var;
        int e2;
        int e3;
        int e4;
        int e5;
        int e6;
        int e7;
        int e8;
        int e9;
        int e10;
        int e11;
        int e12;
        int e13;
        int e14;
        int e15;
        k93 c2 = k93.c("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            e2 = wb0.e(c3, "required_network_type");
            e3 = wb0.e(c3, "requires_charging");
            e4 = wb0.e(c3, "requires_device_idle");
            e5 = wb0.e(c3, "requires_battery_not_low");
            e6 = wb0.e(c3, "requires_storage_not_low");
            e7 = wb0.e(c3, "trigger_content_update_delay");
            e8 = wb0.e(c3, "trigger_max_content_delay");
            e9 = wb0.e(c3, "content_uri_triggers");
            e10 = wb0.e(c3, "id");
            e11 = wb0.e(c3, "state");
            e12 = wb0.e(c3, "worker_class_name");
            e13 = wb0.e(c3, "input_merger_class_name");
            e14 = wb0.e(c3, "input");
            e15 = wb0.e(c3, "output");
            k93Var = c2;
        } catch (Throwable th) {
            th = th;
            k93Var = c2;
        }
        try {
            int e16 = wb0.e(c3, "initial_delay");
            int e17 = wb0.e(c3, "interval_duration");
            int e18 = wb0.e(c3, "flex_duration");
            int e19 = wb0.e(c3, "run_attempt_count");
            int e20 = wb0.e(c3, "backoff_policy");
            int e21 = wb0.e(c3, "backoff_delay_duration");
            int e22 = wb0.e(c3, "period_start_time");
            int e23 = wb0.e(c3, "minimum_retention_duration");
            int e24 = wb0.e(c3, "schedule_requested_at");
            int e25 = wb0.e(c3, "run_in_foreground");
            int e26 = wb0.e(c3, "out_of_quota_policy");
            int i2 = e15;
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                String string = c3.getString(e10);
                int i3 = e10;
                String string2 = c3.getString(e12);
                int i4 = e12;
                h60 h60Var = new h60();
                int i5 = e2;
                h60Var.k(ar4.e(c3.getInt(e2)));
                h60Var.m(c3.getInt(e3) != 0);
                h60Var.n(c3.getInt(e4) != 0);
                h60Var.l(c3.getInt(e5) != 0);
                h60Var.o(c3.getInt(e6) != 0);
                int i6 = e3;
                int i7 = e4;
                h60Var.p(c3.getLong(e7));
                h60Var.q(c3.getLong(e8));
                h60Var.j(ar4.b(c3.getBlob(e9)));
                tq4 tq4Var = new tq4(string, string2);
                tq4Var.b = ar4.g(c3.getInt(e11));
                tq4Var.d = c3.getString(e13);
                tq4Var.e = androidx.work.b.g(c3.getBlob(e14));
                int i8 = i2;
                tq4Var.f = androidx.work.b.g(c3.getBlob(i8));
                i2 = i8;
                int i9 = e16;
                tq4Var.g = c3.getLong(i9);
                int i10 = e14;
                int i11 = e17;
                tq4Var.h = c3.getLong(i11);
                int i12 = e5;
                int i13 = e18;
                tq4Var.i = c3.getLong(i13);
                int i14 = e19;
                tq4Var.k = c3.getInt(i14);
                int i15 = e20;
                tq4Var.l = ar4.d(c3.getInt(i15));
                e18 = i13;
                int i16 = e21;
                tq4Var.m = c3.getLong(i16);
                int i17 = e22;
                tq4Var.n = c3.getLong(i17);
                e22 = i17;
                int i18 = e23;
                tq4Var.o = c3.getLong(i18);
                int i19 = e24;
                tq4Var.p = c3.getLong(i19);
                int i20 = e25;
                tq4Var.q = c3.getInt(i20) != 0;
                int i21 = e26;
                tq4Var.r = ar4.f(c3.getInt(i21));
                tq4Var.j = h60Var;
                arrayList.add(tq4Var);
                e26 = i21;
                e3 = i6;
                e14 = i10;
                e16 = i9;
                e17 = i11;
                e19 = i14;
                e24 = i19;
                e10 = i3;
                e12 = i4;
                e2 = i5;
                e25 = i20;
                e23 = i18;
                e4 = i7;
                e21 = i16;
                e5 = i12;
                e20 = i15;
            }
            c3.close();
            k93Var.f();
            return arrayList;
        } catch (Throwable th2) {
            th = th2;
            c3.close();
            k93Var.f();
            throw th;
        }
    }

    @Override // defpackage.uq4
    public void g(String str, androidx.work.b bVar) {
        this.a.d();
        ww3 a2 = this.d.a();
        byte[] n = androidx.work.b.n(bVar);
        if (n == null) {
            a2.Y0(1);
        } else {
            a2.A0(1, n);
        }
        if (str == null) {
            a2.Y0(2);
        } else {
            a2.L(2, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.d.f(a2);
        }
    }

    @Override // defpackage.uq4
    public List<tq4> h() {
        k93 k93Var;
        int e2;
        int e3;
        int e4;
        int e5;
        int e6;
        int e7;
        int e8;
        int e9;
        int e10;
        int e11;
        int e12;
        int e13;
        int e14;
        int e15;
        k93 c2 = k93.c("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=1", 0);
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            e2 = wb0.e(c3, "required_network_type");
            e3 = wb0.e(c3, "requires_charging");
            e4 = wb0.e(c3, "requires_device_idle");
            e5 = wb0.e(c3, "requires_battery_not_low");
            e6 = wb0.e(c3, "requires_storage_not_low");
            e7 = wb0.e(c3, "trigger_content_update_delay");
            e8 = wb0.e(c3, "trigger_max_content_delay");
            e9 = wb0.e(c3, "content_uri_triggers");
            e10 = wb0.e(c3, "id");
            e11 = wb0.e(c3, "state");
            e12 = wb0.e(c3, "worker_class_name");
            e13 = wb0.e(c3, "input_merger_class_name");
            e14 = wb0.e(c3, "input");
            e15 = wb0.e(c3, "output");
            k93Var = c2;
        } catch (Throwable th) {
            th = th;
            k93Var = c2;
        }
        try {
            int e16 = wb0.e(c3, "initial_delay");
            int e17 = wb0.e(c3, "interval_duration");
            int e18 = wb0.e(c3, "flex_duration");
            int e19 = wb0.e(c3, "run_attempt_count");
            int e20 = wb0.e(c3, "backoff_policy");
            int e21 = wb0.e(c3, "backoff_delay_duration");
            int e22 = wb0.e(c3, "period_start_time");
            int e23 = wb0.e(c3, "minimum_retention_duration");
            int e24 = wb0.e(c3, "schedule_requested_at");
            int e25 = wb0.e(c3, "run_in_foreground");
            int e26 = wb0.e(c3, "out_of_quota_policy");
            int i2 = e15;
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                String string = c3.getString(e10);
                int i3 = e10;
                String string2 = c3.getString(e12);
                int i4 = e12;
                h60 h60Var = new h60();
                int i5 = e2;
                h60Var.k(ar4.e(c3.getInt(e2)));
                h60Var.m(c3.getInt(e3) != 0);
                h60Var.n(c3.getInt(e4) != 0);
                h60Var.l(c3.getInt(e5) != 0);
                h60Var.o(c3.getInt(e6) != 0);
                int i6 = e3;
                int i7 = e4;
                h60Var.p(c3.getLong(e7));
                h60Var.q(c3.getLong(e8));
                h60Var.j(ar4.b(c3.getBlob(e9)));
                tq4 tq4Var = new tq4(string, string2);
                tq4Var.b = ar4.g(c3.getInt(e11));
                tq4Var.d = c3.getString(e13);
                tq4Var.e = androidx.work.b.g(c3.getBlob(e14));
                int i8 = i2;
                tq4Var.f = androidx.work.b.g(c3.getBlob(i8));
                i2 = i8;
                int i9 = e16;
                tq4Var.g = c3.getLong(i9);
                int i10 = e14;
                int i11 = e17;
                tq4Var.h = c3.getLong(i11);
                int i12 = e5;
                int i13 = e18;
                tq4Var.i = c3.getLong(i13);
                int i14 = e19;
                tq4Var.k = c3.getInt(i14);
                int i15 = e20;
                tq4Var.l = ar4.d(c3.getInt(i15));
                e18 = i13;
                int i16 = e21;
                tq4Var.m = c3.getLong(i16);
                int i17 = e22;
                tq4Var.n = c3.getLong(i17);
                e22 = i17;
                int i18 = e23;
                tq4Var.o = c3.getLong(i18);
                int i19 = e24;
                tq4Var.p = c3.getLong(i19);
                int i20 = e25;
                tq4Var.q = c3.getInt(i20) != 0;
                int i21 = e26;
                tq4Var.r = ar4.f(c3.getInt(i21));
                tq4Var.j = h60Var;
                arrayList.add(tq4Var);
                e26 = i21;
                e3 = i6;
                e14 = i10;
                e16 = i9;
                e17 = i11;
                e19 = i14;
                e24 = i19;
                e10 = i3;
                e12 = i4;
                e2 = i5;
                e25 = i20;
                e23 = i18;
                e4 = i7;
                e21 = i16;
                e5 = i12;
                e20 = i15;
            }
            c3.close();
            k93Var.f();
            return arrayList;
        } catch (Throwable th2) {
            th = th2;
            c3.close();
            k93Var.f();
            throw th;
        }
    }

    @Override // defpackage.uq4
    public boolean i() {
        boolean z = false;
        k93 c2 = k93.c("SELECT COUNT(*) > 0 FROM workspec WHERE state NOT IN (2, 3, 5) LIMIT 1", 0);
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            if (c3.moveToFirst()) {
                if (c3.getInt(0) != 0) {
                    z = true;
                }
            }
            return z;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.uq4
    public List<String> j(String str) {
        k93 c2 = k93.c("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                arrayList.add(c3.getString(0));
            }
            return arrayList;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.uq4
    public WorkInfo.State k(String str) {
        k93 c2 = k93.c("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            return c3.moveToFirst() ? ar4.g(c3.getInt(0)) : null;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.uq4
    public tq4 l(String str) {
        k93 k93Var;
        int e2;
        int e3;
        int e4;
        int e5;
        int e6;
        int e7;
        int e8;
        int e9;
        int e10;
        int e11;
        int e12;
        int e13;
        int e14;
        int e15;
        tq4 tq4Var;
        k93 c2 = k93.c("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE id=?", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            e2 = wb0.e(c3, "required_network_type");
            e3 = wb0.e(c3, "requires_charging");
            e4 = wb0.e(c3, "requires_device_idle");
            e5 = wb0.e(c3, "requires_battery_not_low");
            e6 = wb0.e(c3, "requires_storage_not_low");
            e7 = wb0.e(c3, "trigger_content_update_delay");
            e8 = wb0.e(c3, "trigger_max_content_delay");
            e9 = wb0.e(c3, "content_uri_triggers");
            e10 = wb0.e(c3, "id");
            e11 = wb0.e(c3, "state");
            e12 = wb0.e(c3, "worker_class_name");
            e13 = wb0.e(c3, "input_merger_class_name");
            e14 = wb0.e(c3, "input");
            e15 = wb0.e(c3, "output");
            k93Var = c2;
        } catch (Throwable th) {
            th = th;
            k93Var = c2;
        }
        try {
            int e16 = wb0.e(c3, "initial_delay");
            int e17 = wb0.e(c3, "interval_duration");
            int e18 = wb0.e(c3, "flex_duration");
            int e19 = wb0.e(c3, "run_attempt_count");
            int e20 = wb0.e(c3, "backoff_policy");
            int e21 = wb0.e(c3, "backoff_delay_duration");
            int e22 = wb0.e(c3, "period_start_time");
            int e23 = wb0.e(c3, "minimum_retention_duration");
            int e24 = wb0.e(c3, "schedule_requested_at");
            int e25 = wb0.e(c3, "run_in_foreground");
            int e26 = wb0.e(c3, "out_of_quota_policy");
            if (c3.moveToFirst()) {
                String string = c3.getString(e10);
                String string2 = c3.getString(e12);
                h60 h60Var = new h60();
                h60Var.k(ar4.e(c3.getInt(e2)));
                h60Var.m(c3.getInt(e3) != 0);
                h60Var.n(c3.getInt(e4) != 0);
                h60Var.l(c3.getInt(e5) != 0);
                h60Var.o(c3.getInt(e6) != 0);
                h60Var.p(c3.getLong(e7));
                h60Var.q(c3.getLong(e8));
                h60Var.j(ar4.b(c3.getBlob(e9)));
                tq4 tq4Var2 = new tq4(string, string2);
                tq4Var2.b = ar4.g(c3.getInt(e11));
                tq4Var2.d = c3.getString(e13);
                tq4Var2.e = androidx.work.b.g(c3.getBlob(e14));
                tq4Var2.f = androidx.work.b.g(c3.getBlob(e15));
                tq4Var2.g = c3.getLong(e16);
                tq4Var2.h = c3.getLong(e17);
                tq4Var2.i = c3.getLong(e18);
                tq4Var2.k = c3.getInt(e19);
                tq4Var2.l = ar4.d(c3.getInt(e20));
                tq4Var2.m = c3.getLong(e21);
                tq4Var2.n = c3.getLong(e22);
                tq4Var2.o = c3.getLong(e23);
                tq4Var2.p = c3.getLong(e24);
                tq4Var2.q = c3.getInt(e25) != 0;
                tq4Var2.r = ar4.f(c3.getInt(e26));
                tq4Var2.j = h60Var;
                tq4Var = tq4Var2;
            } else {
                tq4Var = null;
            }
            c3.close();
            k93Var.f();
            return tq4Var;
        } catch (Throwable th2) {
            th = th2;
            c3.close();
            k93Var.f();
            throw th;
        }
    }

    @Override // defpackage.uq4
    public int m(String str) {
        this.a.d();
        ww3 a2 = this.g.a();
        if (str == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str);
        }
        this.a.e();
        try {
            int T = a2.T();
            this.a.E();
            return T;
        } finally {
            this.a.j();
            this.g.f(a2);
        }
    }

    @Override // defpackage.uq4
    public void n(tq4 tq4Var) {
        this.a.d();
        this.a.e();
        try {
            this.b.h(tq4Var);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.uq4
    public List<String> o(String str) {
        k93 c2 = k93.c("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                arrayList.add(c3.getString(0));
            }
            return arrayList;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.uq4
    public List<androidx.work.b> p(String str) {
        k93 c2 = k93.c("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            c2.Y0(1);
        } else {
            c2.L(1, str);
        }
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                arrayList.add(androidx.work.b.g(c3.getBlob(0)));
            }
            return arrayList;
        } finally {
            c3.close();
            c2.f();
        }
    }

    @Override // defpackage.uq4
    public int q(String str) {
        this.a.d();
        ww3 a2 = this.f.a();
        if (str == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str);
        }
        this.a.e();
        try {
            int T = a2.T();
            this.a.E();
            return T;
        } finally {
            this.a.j();
            this.f.f(a2);
        }
    }

    @Override // defpackage.uq4
    public void r(String str, long j) {
        this.a.d();
        ww3 a2 = this.e.a();
        a2.q0(1, j);
        if (str == null) {
            a2.Y0(2);
        } else {
            a2.L(2, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.e.f(a2);
        }
    }

    @Override // defpackage.uq4
    public List<tq4> s(int i2) {
        k93 k93Var;
        int e2;
        int e3;
        int e4;
        int e5;
        int e6;
        int e7;
        int e8;
        int e9;
        int e10;
        int e11;
        int e12;
        int e13;
        int e14;
        int e15;
        k93 c2 = k93.c("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 ORDER BY period_start_time LIMIT ?", 1);
        c2.q0(1, i2);
        this.a.d();
        Cursor c3 = id0.c(this.a, c2, false, null);
        try {
            e2 = wb0.e(c3, "required_network_type");
            e3 = wb0.e(c3, "requires_charging");
            e4 = wb0.e(c3, "requires_device_idle");
            e5 = wb0.e(c3, "requires_battery_not_low");
            e6 = wb0.e(c3, "requires_storage_not_low");
            e7 = wb0.e(c3, "trigger_content_update_delay");
            e8 = wb0.e(c3, "trigger_max_content_delay");
            e9 = wb0.e(c3, "content_uri_triggers");
            e10 = wb0.e(c3, "id");
            e11 = wb0.e(c3, "state");
            e12 = wb0.e(c3, "worker_class_name");
            e13 = wb0.e(c3, "input_merger_class_name");
            e14 = wb0.e(c3, "input");
            e15 = wb0.e(c3, "output");
            k93Var = c2;
        } catch (Throwable th) {
            th = th;
            k93Var = c2;
        }
        try {
            int e16 = wb0.e(c3, "initial_delay");
            int e17 = wb0.e(c3, "interval_duration");
            int e18 = wb0.e(c3, "flex_duration");
            int e19 = wb0.e(c3, "run_attempt_count");
            int e20 = wb0.e(c3, "backoff_policy");
            int e21 = wb0.e(c3, "backoff_delay_duration");
            int e22 = wb0.e(c3, "period_start_time");
            int e23 = wb0.e(c3, "minimum_retention_duration");
            int e24 = wb0.e(c3, "schedule_requested_at");
            int e25 = wb0.e(c3, "run_in_foreground");
            int e26 = wb0.e(c3, "out_of_quota_policy");
            int i3 = e15;
            ArrayList arrayList = new ArrayList(c3.getCount());
            while (c3.moveToNext()) {
                String string = c3.getString(e10);
                int i4 = e10;
                String string2 = c3.getString(e12);
                int i5 = e12;
                h60 h60Var = new h60();
                int i6 = e2;
                h60Var.k(ar4.e(c3.getInt(e2)));
                h60Var.m(c3.getInt(e3) != 0);
                h60Var.n(c3.getInt(e4) != 0);
                h60Var.l(c3.getInt(e5) != 0);
                h60Var.o(c3.getInt(e6) != 0);
                int i7 = e3;
                int i8 = e4;
                h60Var.p(c3.getLong(e7));
                h60Var.q(c3.getLong(e8));
                h60Var.j(ar4.b(c3.getBlob(e9)));
                tq4 tq4Var = new tq4(string, string2);
                tq4Var.b = ar4.g(c3.getInt(e11));
                tq4Var.d = c3.getString(e13);
                tq4Var.e = androidx.work.b.g(c3.getBlob(e14));
                int i9 = i3;
                tq4Var.f = androidx.work.b.g(c3.getBlob(i9));
                i3 = i9;
                int i10 = e16;
                tq4Var.g = c3.getLong(i10);
                int i11 = e13;
                int i12 = e17;
                tq4Var.h = c3.getLong(i12);
                int i13 = e5;
                int i14 = e18;
                tq4Var.i = c3.getLong(i14);
                int i15 = e19;
                tq4Var.k = c3.getInt(i15);
                int i16 = e20;
                tq4Var.l = ar4.d(c3.getInt(i16));
                e18 = i14;
                int i17 = e21;
                tq4Var.m = c3.getLong(i17);
                int i18 = e22;
                tq4Var.n = c3.getLong(i18);
                e22 = i18;
                int i19 = e23;
                tq4Var.o = c3.getLong(i19);
                int i20 = e24;
                tq4Var.p = c3.getLong(i20);
                int i21 = e25;
                tq4Var.q = c3.getInt(i21) != 0;
                int i22 = e26;
                tq4Var.r = ar4.f(c3.getInt(i22));
                tq4Var.j = h60Var;
                arrayList.add(tq4Var);
                e26 = i22;
                e3 = i7;
                e13 = i11;
                e16 = i10;
                e17 = i12;
                e19 = i15;
                e24 = i20;
                e10 = i4;
                e12 = i5;
                e2 = i6;
                e25 = i21;
                e23 = i19;
                e4 = i8;
                e21 = i17;
                e5 = i13;
                e20 = i16;
            }
            c3.close();
            k93Var.f();
            return arrayList;
        } catch (Throwable th2) {
            th = th2;
            c3.close();
            k93Var.f();
            throw th;
        }
    }

    @Override // defpackage.uq4
    public int t() {
        this.a.d();
        ww3 a2 = this.i.a();
        this.a.e();
        try {
            int T = a2.T();
            this.a.E();
            return T;
        } finally {
            this.a.j();
            this.i.f(a2);
        }
    }
}
