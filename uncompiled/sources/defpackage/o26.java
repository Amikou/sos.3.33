package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: o26  reason: default package */
/* loaded from: classes.dex */
public final class o26 implements yp5<p26> {
    public static final o26 f0 = new o26();
    public final yp5<p26> a = gq5.a(gq5.b(new q26()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final p26 zza() {
        return this.a.zza();
    }
}
