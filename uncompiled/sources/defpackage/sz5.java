package defpackage;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: sz5  reason: default package */
/* loaded from: classes.dex */
public final class sz5 extends dx5 {
    public final Callable<String> e;

    public sz5(Callable<String> callable) {
        super(false, null, null);
        this.e = callable;
    }

    @Override // defpackage.dx5
    public final String f() {
        try {
            return this.e.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
