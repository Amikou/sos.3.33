package defpackage;

import com.google.auto.value.AutoValue;
import defpackage.ol;

/* compiled from: SendRequest.java */
@AutoValue
/* renamed from: ck3  reason: default package */
/* loaded from: classes.dex */
public abstract class ck3 {

    /* compiled from: SendRequest.java */
    @AutoValue.Builder
    /* renamed from: ck3$a */
    /* loaded from: classes.dex */
    public static abstract class a {
        public abstract ck3 a();

        public abstract a b(hv0 hv0Var);

        public abstract a c(com.google.android.datatransport.a<?> aVar);

        public abstract a d(fb4<?, byte[]> fb4Var);

        public abstract a e(ob4 ob4Var);

        public abstract a f(String str);
    }

    public static a a() {
        return new ol.b();
    }

    public abstract hv0 b();

    public abstract com.google.android.datatransport.a<?> c();

    public byte[] d() {
        return e().apply(c().b());
    }

    public abstract fb4<?, byte[]> e();

    public abstract ob4 f();

    public abstract String g();
}
