package defpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: FilterTokenListByPhraseUseCase.kt */
/* renamed from: k41  reason: default package */
/* loaded from: classes2.dex */
public final class k41 implements zl1 {
    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.zl1
    public List<IToken> a(List<? extends IToken> list, String str) {
        fs1.f(list, "origin");
        fs1.f(str, "phrase");
        if (dv3.w(str)) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        for (IToken iToken : list) {
            String name = iToken.getName();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = name.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            Objects.requireNonNull(lowerCase, "null cannot be cast to non-null type kotlin.CharSequence");
            String obj = StringsKt__StringsKt.K0(lowerCase).toString();
            String lowerCase2 = str.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            Objects.requireNonNull(lowerCase2, "null cannot be cast to non-null type kotlin.CharSequence");
            if (!StringsKt__StringsKt.M(obj, StringsKt__StringsKt.K0(lowerCase2).toString(), false, 2, null)) {
                String symbol = iToken.getSymbol();
                Objects.requireNonNull(symbol, "null cannot be cast to non-null type java.lang.String");
                String lowerCase3 = symbol.toLowerCase(locale);
                fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                Objects.requireNonNull(lowerCase3, "null cannot be cast to non-null type kotlin.CharSequence");
                String obj2 = StringsKt__StringsKt.K0(lowerCase3).toString();
                String lowerCase4 = str.toLowerCase(locale);
                fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                Objects.requireNonNull(lowerCase4, "null cannot be cast to non-null type kotlin.CharSequence");
                if (StringsKt__StringsKt.M(obj2, StringsKt__StringsKt.K0(lowerCase4).toString(), false, 2, null)) {
                }
            }
            arrayList.add(iToken);
        }
        return arrayList;
    }
}
