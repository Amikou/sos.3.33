package defpackage;

/* compiled from: EventLoop.common.kt */
/* renamed from: j54  reason: default package */
/* loaded from: classes2.dex */
public final class j54 {
    public static final j54 a = new j54();
    public static final ThreadLocal<xx0> b = new ThreadLocal<>();

    public final xx0 a() {
        return b.get();
    }

    public final xx0 b() {
        ThreadLocal<xx0> threadLocal = b;
        xx0 xx0Var = threadLocal.get();
        if (xx0Var == null) {
            xx0 a2 = zx0.a();
            threadLocal.set(a2);
            return a2;
        }
        return xx0Var;
    }

    public final void c() {
        b.set(null);
    }

    public final void d(xx0 xx0Var) {
        b.set(xx0Var);
    }
}
