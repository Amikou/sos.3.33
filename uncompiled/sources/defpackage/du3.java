package defpackage;

import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.a;
import com.bumptech.glide.load.b;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import okhttp3.internal.http2.Http2;

/* compiled from: StreamGifDecoder.java */
/* renamed from: du3  reason: default package */
/* loaded from: classes.dex */
public class du3 implements b<InputStream, uf1> {
    public final List<ImageHeaderParser> a;
    public final b<ByteBuffer, uf1> b;
    public final sh c;

    public du3(List<ImageHeaderParser> list, b<ByteBuffer, uf1> bVar, sh shVar) {
        this.a = list;
        this.b = bVar;
        this.c = shVar;
    }

    public static byte[] e(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Http2.INITIAL_MAX_FRAME_SIZE);
        try {
            byte[] bArr = new byte[Http2.INITIAL_MAX_FRAME_SIZE];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<uf1> b(InputStream inputStream, int i, int i2, vn2 vn2Var) throws IOException {
        byte[] e = e(inputStream);
        if (e == null) {
            return null;
        }
        return this.b.b(ByteBuffer.wrap(e), i, i2, vn2Var);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(InputStream inputStream, vn2 vn2Var) throws IOException {
        return !((Boolean) vn2Var.c(eg1.b)).booleanValue() && a.f(this.a, inputStream, this.c) == ImageHeaderParser.ImageType.GIF;
    }
}
