package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.a;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.model.Coin;

/* compiled from: CMCListCheckable.kt */
/* renamed from: f11  reason: default package */
/* loaded from: classes2.dex */
public final class f11 extends RecyclerView.a0 {
    public final vs1 a;
    public final rl1 b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f11(vs1 vs1Var, rl1 rl1Var) {
        super(vs1Var.b());
        fs1.f(vs1Var, "binding");
        this.a = vs1Var;
        this.b = rl1Var;
    }

    public static final void c(d11 d11Var, f11 f11Var, Coin coin, View view) {
        fs1.f(d11Var, "$extendCoin");
        fs1.f(f11Var, "this$0");
        fs1.f(coin, "$item");
        if (d11Var.d()) {
            rl1 rl1Var = f11Var.b;
            if (rl1Var == null) {
                return;
            }
            rl1Var.a(coin);
            return;
        }
        rl1 rl1Var2 = f11Var.b;
        if (rl1Var2 == null) {
            return;
        }
        rl1Var2.b(coin);
    }

    public final void b(final d11 d11Var) {
        fs1.f(d11Var, "extendCoin");
        vs1 vs1Var = this.a;
        final Coin c = d11Var.c();
        Integer id = c.getId();
        fs1.e(id, "item.id");
        Object f = a4.f(id.intValue(), c.getSymbol());
        TextView textView = vs1Var.g;
        textView.setText(((Object) c.getName()) + " (" + ((Object) c.getSymbol()) + ')');
        a.u(vs1Var.c).x(f).I0(vs1Var.c);
        MaterialCheckBox materialCheckBox = vs1Var.b;
        fs1.e(materialCheckBox, "checkableIcon");
        materialCheckBox.setVisibility(0);
        vs1Var.b.setChecked(d11Var.d());
        vs1Var.d.setVisibility(d11Var.d() ? 0 : 8);
        vs1Var.e.setOnClickListener(new View.OnClickListener() { // from class: e11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                f11.c(d11.this, this, c, view);
            }
        });
    }
}
