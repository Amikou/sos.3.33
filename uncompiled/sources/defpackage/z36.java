package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: z36  reason: default package */
/* loaded from: classes.dex */
public final class z36 implements yp5<a46> {
    public static final z36 f0 = new z36();
    public final yp5<a46> a = gq5.a(gq5.b(new b46()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final a46 zza() {
        return this.a.zza();
    }
}
