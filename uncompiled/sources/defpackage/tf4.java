package defpackage;

import defpackage.j92;
import java.io.InputStream;
import java.net.URL;

/* compiled from: UrlLoader.java */
/* renamed from: tf4  reason: default package */
/* loaded from: classes.dex */
public class tf4 implements j92<URL, InputStream> {
    public final j92<ng1, InputStream> a;

    /* compiled from: UrlLoader.java */
    /* renamed from: tf4$a */
    /* loaded from: classes.dex */
    public static class a implements k92<URL, InputStream> {
        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<URL, InputStream> c(qa2 qa2Var) {
            return new tf4(qa2Var.d(ng1.class, InputStream.class));
        }
    }

    public tf4(j92<ng1, InputStream> j92Var) {
        this.a = j92Var;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<InputStream> b(URL url, int i, int i2, vn2 vn2Var) {
        return this.a.b(new ng1(url), i, i2, vn2Var);
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(URL url) {
        return true;
    }
}
