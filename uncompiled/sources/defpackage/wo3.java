package defpackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.util.c;
import java.util.Collections;
import java.util.Iterator;

/* compiled from: SimpleBeanPropertyDefinition.java */
/* renamed from: wo3  reason: default package */
/* loaded from: classes.dex */
public class wo3 extends vo {
    public final AnnotationIntrospector f0;
    public final AnnotatedMember g0;
    public final PropertyMetadata h0;
    public final PropertyName i0;
    public final JsonInclude.Value j0;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public wo3(com.fasterxml.jackson.databind.introspect.AnnotatedMember r7, com.fasterxml.jackson.databind.PropertyName r8, com.fasterxml.jackson.databind.AnnotationIntrospector r9, com.fasterxml.jackson.databind.PropertyMetadata r10, com.fasterxml.jackson.annotation.JsonInclude.Include r11) {
        /*
            r6 = this;
            if (r11 == 0) goto Ld
            com.fasterxml.jackson.annotation.JsonInclude$Include r0 = com.fasterxml.jackson.annotation.JsonInclude.Include.USE_DEFAULTS
            if (r11 != r0) goto L7
            goto Ld
        L7:
            r0 = 0
            com.fasterxml.jackson.annotation.JsonInclude$Value r11 = com.fasterxml.jackson.annotation.JsonInclude.Value.construct(r11, r0)
            goto Lf
        Ld:
            com.fasterxml.jackson.annotation.JsonInclude$Value r11 = defpackage.vo.a
        Lf:
            r5 = r11
            r0 = r6
            r1 = r7
            r2 = r8
            r3 = r9
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.wo3.<init>(com.fasterxml.jackson.databind.introspect.AnnotatedMember, com.fasterxml.jackson.databind.PropertyName, com.fasterxml.jackson.databind.AnnotationIntrospector, com.fasterxml.jackson.databind.PropertyMetadata, com.fasterxml.jackson.annotation.JsonInclude$Include):void");
    }

    public static wo3 J(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, PropertyName propertyName) {
        return L(mapperConfig, annotatedMember, propertyName, null, vo.a);
    }

    public static wo3 K(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, PropertyName propertyName, PropertyMetadata propertyMetadata, JsonInclude.Include include) {
        return new wo3(annotatedMember, propertyName, mapperConfig == null ? null : mapperConfig.getAnnotationIntrospector(), propertyMetadata, include);
    }

    public static wo3 L(MapperConfig<?> mapperConfig, AnnotatedMember annotatedMember, PropertyName propertyName, PropertyMetadata propertyMetadata, JsonInclude.Value value) {
        return new wo3(annotatedMember, propertyName, mapperConfig == null ? null : mapperConfig.getAnnotationIntrospector(), propertyMetadata, value);
    }

    @Override // defpackage.vo
    public boolean A() {
        return this.g0 instanceof AnnotatedParameter;
    }

    @Override // defpackage.vo
    public boolean B() {
        return this.g0 instanceof AnnotatedField;
    }

    @Override // defpackage.vo
    public boolean C() {
        return p() != null;
    }

    @Override // defpackage.vo
    public boolean D(PropertyName propertyName) {
        return this.i0.equals(propertyName);
    }

    @Override // defpackage.vo
    public boolean E() {
        return x() != null;
    }

    @Override // defpackage.vo
    public boolean G() {
        return false;
    }

    @Override // defpackage.vo
    public boolean H() {
        return false;
    }

    public AnnotatedParameter M() {
        AnnotatedMember annotatedMember = this.g0;
        if (annotatedMember instanceof AnnotatedParameter) {
            return (AnnotatedParameter) annotatedMember;
        }
        return null;
    }

    @Override // defpackage.vo
    public JsonInclude.Value e() {
        return this.j0;
    }

    @Override // defpackage.vo
    public AnnotatedMember j() {
        AnnotatedMethod p = p();
        return p == null ? l() : p;
    }

    @Override // defpackage.vo
    public Iterator<AnnotatedParameter> k() {
        AnnotatedParameter M = M();
        if (M == null) {
            return c.k();
        }
        return Collections.singleton(M).iterator();
    }

    @Override // defpackage.vo
    public AnnotatedField l() {
        AnnotatedMember annotatedMember = this.g0;
        if (annotatedMember instanceof AnnotatedField) {
            return (AnnotatedField) annotatedMember;
        }
        return null;
    }

    @Override // defpackage.vo
    public PropertyName o() {
        return this.i0;
    }

    @Override // defpackage.vo
    public AnnotatedMethod p() {
        AnnotatedMember annotatedMember = this.g0;
        if ((annotatedMember instanceof AnnotatedMethod) && ((AnnotatedMethod) annotatedMember).getParameterCount() == 0) {
            return (AnnotatedMethod) this.g0;
        }
        return null;
    }

    @Override // defpackage.vo
    public PropertyMetadata r() {
        return this.h0;
    }

    @Override // defpackage.vo
    public AnnotatedMember s() {
        AnnotatedParameter M = M();
        if (M == null) {
            AnnotatedMethod x = x();
            return x == null ? l() : x;
        }
        return M;
    }

    @Override // defpackage.vo
    public String t() {
        return this.i0.getSimpleName();
    }

    @Override // defpackage.vo
    public AnnotatedMember u() {
        AnnotatedMethod x = x();
        return x == null ? l() : x;
    }

    @Override // defpackage.vo
    public AnnotatedMember v() {
        return this.g0;
    }

    @Override // defpackage.vo
    public AnnotatedMethod x() {
        AnnotatedMember annotatedMember = this.g0;
        if ((annotatedMember instanceof AnnotatedMethod) && ((AnnotatedMethod) annotatedMember).getParameterCount() == 1) {
            return (AnnotatedMethod) this.g0;
        }
        return null;
    }

    @Override // defpackage.vo
    public PropertyName y() {
        AnnotationIntrospector annotationIntrospector = this.f0;
        if (annotationIntrospector != null || this.g0 == null) {
            return annotationIntrospector.findWrapperName(this.g0);
        }
        return null;
    }

    public wo3(AnnotatedMember annotatedMember, PropertyName propertyName, AnnotationIntrospector annotationIntrospector, PropertyMetadata propertyMetadata, JsonInclude.Value value) {
        this.f0 = annotationIntrospector;
        this.g0 = annotatedMember;
        this.i0 = propertyName;
        propertyName.getSimpleName();
        this.h0 = propertyMetadata == null ? PropertyMetadata.STD_OPTIONAL : propertyMetadata;
        this.j0 = value;
    }
}
