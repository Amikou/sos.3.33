package defpackage;

import android.util.Pair;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: EncodedCacheKeyMultiplexProducer.java */
/* renamed from: wu0  reason: default package */
/* loaded from: classes.dex */
public class wu0 extends eb2<Pair<wt, ImageRequest.RequestLevel>, zu0> {
    public final xt f;

    public wu0(xt xtVar, boolean z, dv2 dv2Var) {
        super(dv2Var, "EncodedCacheKeyMultiplexProducer", "multiplex_enc_cnt", z);
        this.f = xtVar;
    }

    @Override // defpackage.eb2
    /* renamed from: k */
    public zu0 f(zu0 zu0Var) {
        return zu0.b(zu0Var);
    }

    @Override // defpackage.eb2
    /* renamed from: l */
    public Pair<wt, ImageRequest.RequestLevel> i(ev2 ev2Var) {
        return Pair.create(this.f.d(ev2Var.c(), ev2Var.a()), ev2Var.n());
    }
}
