package defpackage;

import kotlin.KotlinNothingValueException;
import kotlin.coroutines.CoroutineContext;

/* compiled from: MainDispatchers.kt */
/* renamed from: a92  reason: default package */
/* loaded from: classes2.dex */
public final class a92 extends e32 implements wl0 {
    public final Throwable f0;
    public final String g0;

    public a92(Throwable th, String str) {
        this.f0 = th;
        this.g0 = str;
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    /* renamed from: M */
    public Void h(CoroutineContext coroutineContext, Runnable runnable) {
        N();
        throw new KotlinNothingValueException();
    }

    public final Void N() {
        String l;
        if (this.f0 != null) {
            String str = this.g0;
            String str2 = "";
            if (str != null && (l = fs1.l(". ", str)) != null) {
                str2 = l;
            }
            throw new IllegalStateException(fs1.l("Module with the Main dispatcher had failed to initialize", str2), this.f0);
        }
        g32.c();
        throw new KotlinNothingValueException();
    }

    @Override // defpackage.wl0
    /* renamed from: Q */
    public Void f(long j, ov<? super te4> ovVar) {
        N();
        throw new KotlinNothingValueException();
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public boolean j(CoroutineContext coroutineContext) {
        N();
        throw new KotlinNothingValueException();
    }

    @Override // defpackage.e32
    public e32 l() {
        return this;
    }

    @Override // defpackage.e32, kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Dispatchers.Main[missing");
        Throwable th = this.f0;
        sb.append(th != null ? fs1.l(", cause=", th) : "");
        sb.append(']');
        return sb.toString();
    }
}
