package defpackage;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.vision.face.internal.client.zza;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: fa5  reason: default package */
/* loaded from: classes.dex */
public final class fa5 implements Parcelable.Creator<zza> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zza createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        PointF[] pointFArr = null;
        int i = 0;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 2) {
                pointFArr = (PointF[]) SafeParcelReader.s(parcel, C, PointF.CREATOR);
            } else if (v != 3) {
                SafeParcelReader.I(parcel, C);
            } else {
                i = SafeParcelReader.E(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zza(pointFArr, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zza[] newArray(int i) {
        return new zza[i];
    }
}
