package defpackage;

import com.rd.animation.type.AnimationType;
import com.rd.draw.data.Orientation;
import com.rd.draw.data.RtlMode;

/* compiled from: Indicator.java */
/* renamed from: mq1  reason: default package */
/* loaded from: classes2.dex */
public class mq1 {
    public int a;
    public int b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;
    public float j;
    public int k;
    public int l;
    public boolean m;
    public boolean n;
    public boolean o;
    public boolean p;
    public long q;
    public long r;
    public int t;
    public int u;
    public int v;
    public Orientation x;
    public AnimationType y;
    public RtlMode z;
    public int s = 3;
    public int w = -1;

    public void A(long j) {
        this.r = j;
    }

    public void B(AnimationType animationType) {
        this.y = animationType;
    }

    public void C(boolean z) {
        this.n = z;
    }

    public void D(int i) {
        this.s = i;
    }

    public void E(boolean z) {
        this.o = z;
    }

    public void F(boolean z) {
        this.p = z;
    }

    public void G(int i) {
        this.a = i;
    }

    public void H(boolean z) {
    }

    public void I(long j) {
        this.q = j;
    }

    public void J(boolean z) {
        this.m = z;
    }

    public void K(int i) {
        this.v = i;
    }

    public void L(Orientation orientation) {
        this.x = orientation;
    }

    public void M(int i) {
        this.d = i;
    }

    public void N(int i) {
        this.h = i;
    }

    public void O(int i) {
        this.e = i;
    }

    public void P(int i) {
        this.g = i;
    }

    public void Q(int i) {
        this.f = i;
    }

    public void R(int i) {
        this.c = i;
    }

    public void S(RtlMode rtlMode) {
        this.z = rtlMode;
    }

    public void T(float f) {
        this.j = f;
    }

    public void U(int i) {
        this.l = i;
    }

    public void V(int i) {
        this.t = i;
    }

    public void W(int i) {
        this.u = i;
    }

    public void X(int i) {
        this.i = i;
    }

    public void Y(int i) {
        this.k = i;
    }

    public void Z(int i) {
        this.w = i;
    }

    public long a() {
        return this.r;
    }

    public void a0(int i) {
        this.b = i;
    }

    public AnimationType b() {
        if (this.y == null) {
            this.y = AnimationType.NONE;
        }
        return this.y;
    }

    public int c() {
        return this.s;
    }

    public int d() {
        return this.a;
    }

    public long e() {
        return this.q;
    }

    public int f() {
        return this.v;
    }

    public Orientation g() {
        if (this.x == null) {
            this.x = Orientation.HORIZONTAL;
        }
        return this.x;
    }

    public int h() {
        return this.d;
    }

    public int i() {
        return this.h;
    }

    public int j() {
        return this.e;
    }

    public int k() {
        return this.g;
    }

    public int l() {
        return this.f;
    }

    public int m() {
        return this.c;
    }

    public RtlMode n() {
        if (this.z == null) {
            this.z = RtlMode.Off;
        }
        return this.z;
    }

    public float o() {
        return this.j;
    }

    public int p() {
        return this.l;
    }

    public int q() {
        return this.t;
    }

    public int r() {
        return this.u;
    }

    public int s() {
        return this.i;
    }

    public int t() {
        return this.k;
    }

    public int u() {
        return this.w;
    }

    public int v() {
        return this.b;
    }

    public boolean w() {
        return this.n;
    }

    public boolean x() {
        return this.o;
    }

    public boolean y() {
        return this.p;
    }

    public boolean z() {
        return this.m;
    }
}
