package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: q16  reason: default package */
/* loaded from: classes.dex */
public final class q16 implements yp5<r16> {
    public static final q16 f0 = new q16();
    public final yp5<r16> a = gq5.a(gq5.b(new s16()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final r16 zza() {
        return this.a.zza();
    }
}
