package defpackage;

import androidx.media3.common.util.b;

/* compiled from: ProgramInformation.java */
/* renamed from: mv2  reason: default package */
/* loaded from: classes.dex */
public final class mv2 {
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;

    public mv2(String str, String str2, String str3, String str4, String str5) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof mv2) {
            mv2 mv2Var = (mv2) obj;
            return b.c(this.a, mv2Var.a) && b.c(this.b, mv2Var.b) && b.c(this.c, mv2Var.c) && b.c(this.d, mv2Var.d) && b.c(this.e, mv2Var.e);
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = (527 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        return hashCode4 + (str5 != null ? str5.hashCode() : 0);
    }
}
