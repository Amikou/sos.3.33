package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentDefaultDateFormatBinding.java */
/* renamed from: aa1  reason: default package */
/* loaded from: classes2.dex */
public final class aa1 {
    public final ConstraintLayout a;
    public final rp3 b;
    public final LinearLayout c;

    public aa1(ConstraintLayout constraintLayout, MaterialCardView materialCardView, rp3 rp3Var, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = rp3Var;
        this.c = linearLayout;
    }

    public static aa1 a(View view) {
        int i = R.id.ccWrapperDateFormat;
        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ccWrapperDateFormat);
        if (materialCardView != null) {
            i = R.id.toolbar;
            View a = ai4.a(view, R.id.toolbar);
            if (a != null) {
                rp3 a2 = rp3.a(a);
                LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.wrapperDateFormat);
                if (linearLayout != null) {
                    return new aa1((ConstraintLayout) view, materialCardView, a2, linearLayout);
                }
                i = R.id.wrapperDateFormat;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
