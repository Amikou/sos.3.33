package defpackage;

import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.y0;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: si5  reason: default package */
/* loaded from: classes.dex */
public final class si5 extends w1<y0, si5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ si5(defpackage.qh5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.y0 r1 = com.google.android.gms.internal.measurement.y0.z()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.si5.<init>(qh5):void");
    }
}
