package defpackage;

import android.view.MotionEvent;

/* compiled from: MotionEventCompat.java */
/* renamed from: v92  reason: default package */
/* loaded from: classes.dex */
public final class v92 {
    public static boolean a(MotionEvent motionEvent, int i) {
        return (motionEvent.getSource() & i) == i;
    }
}
