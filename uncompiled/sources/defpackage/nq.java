package defpackage;

import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: BitmapProbeProducer.java */
/* renamed from: nq  reason: default package */
/* loaded from: classes.dex */
public class nq implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final l72<wt, PooledByteBuffer> a;
    public final xr b;
    public final xr c;
    public final xt d;
    public final dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> e;
    public final gr<wt> f;
    public final gr<wt> g;

    /* compiled from: BitmapProbeProducer.java */
    /* renamed from: nq$a */
    /* loaded from: classes.dex */
    public static class a extends bm0<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>, com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
        public final ev2 c;
        public final l72<wt, PooledByteBuffer> d;
        public final xr e;
        public final xr f;
        public final xt g;
        public final gr<wt> h;
        public final gr<wt> i;

        public a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var, l72<wt, PooledByteBuffer> l72Var, xr xrVar, xr xrVar2, xt xtVar, gr<wt> grVar, gr<wt> grVar2) {
            super(l60Var);
            this.c = ev2Var;
            this.d = l72Var;
            this.e = xrVar;
            this.f = xrVar2;
            this.g = xtVar;
            this.h = grVar;
            this.i = grVar2;
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar, int i) {
            boolean d;
            try {
                if (nc1.d()) {
                    nc1.a("BitmapProbeProducer#onNewResultImpl");
                }
                if (!qm.f(i) && aVar != null && !qm.m(i, 8)) {
                    ImageRequest c = this.c.c();
                    wt d2 = this.g.d(c, this.c.a());
                    String str = (String) this.c.i("origin");
                    if (str != null && str.equals("memory_bitmap")) {
                        if (this.c.d().C().s() && !this.h.b(d2)) {
                            this.d.b(d2);
                            this.h.a(d2);
                        }
                        if (this.c.d().C().q() && !this.i.b(d2)) {
                            (c.d() == ImageRequest.CacheChoice.SMALL ? this.f : this.e).h(d2);
                            this.i.a(d2);
                        }
                    }
                    p().d(aVar, i);
                    if (d) {
                        return;
                    }
                    return;
                }
                p().d(aVar, i);
                if (nc1.d()) {
                    nc1.b();
                }
            } finally {
                if (nc1.d()) {
                    nc1.b();
                }
            }
        }
    }

    public nq(l72<wt, PooledByteBuffer> l72Var, xr xrVar, xr xrVar2, xt xtVar, gr<wt> grVar, gr<wt> grVar2, dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dv2Var) {
        this.a = l72Var;
        this.b = xrVar;
        this.c = xrVar2;
        this.d = xtVar;
        this.f = grVar;
        this.g = grVar2;
        this.e = dv2Var;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        try {
            if (nc1.d()) {
                nc1.a("BitmapProbeProducer#produceResults");
            }
            iv2 l = ev2Var.l();
            l.k(ev2Var, b());
            a aVar = new a(l60Var, ev2Var, this.a, this.b, this.c, this.d, this.f, this.g);
            l.a(ev2Var, "BitmapProbeProducer", null);
            if (nc1.d()) {
                nc1.a("mInputProducer.produceResult");
            }
            this.e.a(aVar, ev2Var);
            if (nc1.d()) {
                nc1.b();
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public String b() {
        return "BitmapProbeProducer";
    }
}
