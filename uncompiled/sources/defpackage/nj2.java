package defpackage;

import com.onesignal.OneSignal;
import com.onesignal.OneSignalStateSynchronizer;
import com.onesignal.b1;
import com.onesignal.n0;
import org.json.JSONObject;

/* compiled from: OSEmailSubscriptionState.java */
/* renamed from: nj2  reason: default package */
/* loaded from: classes2.dex */
public class nj2 implements Cloneable {
    public n0<Object, nj2> a = new n0<>("changed", false);
    public String f0;
    public String g0;

    public nj2(boolean z) {
        if (z) {
            String str = b1.a;
            this.f0 = b1.f(str, "PREFS_ONESIGNAL_EMAIL_ID_LAST", null);
            this.g0 = b1.f(str, "PREFS_ONESIGNAL_EMAIL_ADDRESS_LAST", null);
            return;
        }
        this.f0 = OneSignal.Y();
        this.g0 = OneSignalStateSynchronizer.b().E();
    }

    public n0<Object, nj2> a() {
        return this.a;
    }

    public boolean b() {
        return (this.f0 == null || this.g0 == null) ? false : true;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (Throwable unused) {
            return null;
        }
    }

    public void d() {
        String str = b1.a;
        b1.m(str, "PREFS_ONESIGNAL_EMAIL_ID_LAST", this.f0);
        b1.m(str, "PREFS_ONESIGNAL_EMAIL_ADDRESS_LAST", this.g0);
    }

    public void e(String str) {
        boolean z = true;
        if (str != null ? str.equals(this.f0) : this.f0 == null) {
            z = false;
        }
        this.f0 = str;
        if (z) {
            this.a.c(this);
        }
    }

    public JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        try {
            String str = this.f0;
            if (str != null) {
                jSONObject.put("emailUserId", str);
            } else {
                jSONObject.put("emailUserId", JSONObject.NULL);
            }
            String str2 = this.g0;
            if (str2 != null) {
                jSONObject.put("emailAddress", str2);
            } else {
                jSONObject.put("emailAddress", JSONObject.NULL);
            }
            jSONObject.put("isSubscribed", b());
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject;
    }

    public String toString() {
        return f().toString();
    }
}
