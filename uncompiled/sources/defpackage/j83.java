package defpackage;

import com.google.firebase.components.DependencyException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: RestrictedComponentContainer.java */
/* renamed from: j83  reason: default package */
/* loaded from: classes2.dex */
public final class j83 extends r4 {
    public final Set<Class<?>> a;
    public final Set<Class<?>> b;
    public final Set<Class<?>> c;
    public final Set<Class<?>> d;
    public final Set<Class<?>> e;
    public final Set<Class<?>> f;
    public final b40 g;

    /* compiled from: RestrictedComponentContainer.java */
    /* renamed from: j83$a */
    /* loaded from: classes2.dex */
    public static class a implements ow2 {
        public final ow2 a;

        public a(Set<Class<?>> set, ow2 ow2Var) {
            this.a = ow2Var;
        }
    }

    public j83(a40<?> a40Var, b40 b40Var) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        HashSet hashSet5 = new HashSet();
        for (hm0 hm0Var : a40Var.e()) {
            if (hm0Var.e()) {
                if (hm0Var.g()) {
                    hashSet4.add(hm0Var.c());
                } else {
                    hashSet.add(hm0Var.c());
                }
            } else if (hm0Var.d()) {
                hashSet3.add(hm0Var.c());
            } else if (hm0Var.g()) {
                hashSet5.add(hm0Var.c());
            } else {
                hashSet2.add(hm0Var.c());
            }
        }
        if (!a40Var.h().isEmpty()) {
            hashSet.add(ow2.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = Collections.unmodifiableSet(hashSet3);
        this.d = Collections.unmodifiableSet(hashSet4);
        this.e = Collections.unmodifiableSet(hashSet5);
        this.f = a40Var.h();
        this.g = b40Var;
    }

    @Override // defpackage.r4, defpackage.b40
    public <T> T a(Class<T> cls) {
        if (this.a.contains(cls)) {
            T t = (T) this.g.a(cls);
            return !cls.equals(ow2.class) ? t : (T) new a(this.f, (ow2) t);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }

    @Override // defpackage.b40
    public <T> fw2<T> b(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.g.b(cls);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    @Override // defpackage.b40
    public <T> fw2<Set<T>> c(Class<T> cls) {
        if (this.e.contains(cls)) {
            return this.g.c(cls);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }

    @Override // defpackage.r4, defpackage.b40
    public <T> Set<T> d(Class<T> cls) {
        if (this.d.contains(cls)) {
            return this.g.d(cls);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }

    @Override // defpackage.b40
    public <T> rl0<T> e(Class<T> cls) {
        if (this.c.contains(cls)) {
            return this.g.e(cls);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Deferred<%s>.", cls));
    }
}
