package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: e16  reason: default package */
/* loaded from: classes.dex */
public final class e16 implements yp5<f16> {
    public static final e16 f0 = new e16();
    public final yp5<f16> a = gq5.a(gq5.b(new g16()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final f16 zza() {
        return this.a.zza();
    }
}
