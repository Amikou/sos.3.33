package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: RoomDatabase.kt */
/* renamed from: s84  reason: default package */
/* loaded from: classes.dex */
public final class s84 implements CoroutineContext.a {
    public static final a f0 = new a(null);
    public final r70 a;

    /* compiled from: RoomDatabase.kt */
    /* renamed from: s84$a */
    /* loaded from: classes.dex */
    public static final class a implements CoroutineContext.b<s84> {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public final r70 c() {
        return this.a;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public <R> R fold(R r, hd1<? super R, ? super CoroutineContext.a, ? extends R> hd1Var) {
        return (R) CoroutineContext.a.C0196a.a(this, r, hd1Var);
    }

    @Override // kotlin.coroutines.CoroutineContext.a, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        return (E) CoroutineContext.a.C0196a.b(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext.a
    public CoroutineContext.b<s84> getKey() {
        return f0;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        return CoroutineContext.a.C0196a.c(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        return CoroutineContext.a.C0196a.d(this, coroutineContext);
    }
}
