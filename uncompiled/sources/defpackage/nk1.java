package defpackage;

import androidx.media3.common.ParserException;
import defpackage.wc2;
import java.util.Collections;
import java.util.List;

/* compiled from: HevcConfig.java */
/* renamed from: nk1  reason: default package */
/* loaded from: classes.dex */
public final class nk1 {
    public final List<byte[]> a;
    public final int b;
    public final float c;
    public final String d;

    public nk1(List<byte[]> list, int i, int i2, int i3, float f, String str) {
        this.a = list;
        this.b = i;
        this.c = f;
        this.d = str;
    }

    public static nk1 a(op2 op2Var) throws ParserException {
        int i;
        int i2;
        try {
            op2Var.Q(21);
            int D = op2Var.D() & 3;
            int D2 = op2Var.D();
            int e = op2Var.e();
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < D2; i5++) {
                op2Var.Q(1);
                int J = op2Var.J();
                for (int i6 = 0; i6 < J; i6++) {
                    int J2 = op2Var.J();
                    i4 += J2 + 4;
                    op2Var.Q(J2);
                }
            }
            op2Var.P(e);
            byte[] bArr = new byte[i4];
            float f = 1.0f;
            String str = null;
            int i7 = -1;
            int i8 = -1;
            int i9 = 0;
            int i10 = 0;
            while (i9 < D2) {
                int D3 = op2Var.D() & 127;
                int J3 = op2Var.J();
                int i11 = i3;
                while (i11 < J3) {
                    int J4 = op2Var.J();
                    byte[] bArr2 = wc2.a;
                    int i12 = D2;
                    System.arraycopy(bArr2, i3, bArr, i10, bArr2.length);
                    int length = i10 + bArr2.length;
                    System.arraycopy(op2Var.d(), op2Var.e(), bArr, length, J4);
                    if (D3 == 33 && i11 == 0) {
                        wc2.a h = wc2.h(bArr, length, length + J4);
                        int i13 = h.g;
                        i8 = h.h;
                        f = h.i;
                        i = D3;
                        i2 = J3;
                        i7 = i13;
                        str = h00.c(h.a, h.b, h.c, h.d, h.e, h.f);
                    } else {
                        i = D3;
                        i2 = J3;
                    }
                    i10 = length + J4;
                    op2Var.Q(J4);
                    i11++;
                    D2 = i12;
                    D3 = i;
                    J3 = i2;
                    i3 = 0;
                }
                i9++;
                i3 = 0;
            }
            return new nk1(i4 == 0 ? Collections.emptyList() : Collections.singletonList(bArr), D + 1, i7, i8, f, str);
        } catch (ArrayIndexOutOfBoundsException e2) {
            throw ParserException.createForMalformedContainer("Error parsing HEVC config", e2);
        }
    }
}
