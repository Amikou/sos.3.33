package defpackage;

import android.graphics.Bitmap;
import android.graphics.Rect;

/* compiled from: BitmapFrameRenderer.java */
/* renamed from: aq  reason: default package */
/* loaded from: classes.dex */
public interface aq {
    boolean a(int i, Bitmap bitmap);

    int c();

    void d(Rect rect);

    int e();
}
