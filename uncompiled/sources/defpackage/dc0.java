package defpackage;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.R;

/* compiled from: CustomConfirmation.kt */
/* renamed from: dc0  reason: default package */
/* loaded from: classes2.dex */
public final class dc0 {
    public final Context a;
    public final List<gt> b;
    public final hd1<gt, Dialog, te4> c;
    public nn0 d;
    public Dialog e;

    /* JADX WARN: Multi-variable type inference failed */
    public dc0(Context context, List<gt> list, hd1<? super gt, ? super Dialog, te4> hd1Var) {
        fs1.f(context, "context");
        fs1.f(list, "buttons");
        fs1.f(hd1Var, "callBack");
        this.a = context;
        this.b = list;
        this.c = hd1Var;
    }

    public static /* synthetic */ dc0 c(dc0 dc0Var, boolean z, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z = true;
        }
        if ((i & 2) != 0) {
            z2 = true;
        }
        return dc0Var.b(z, z2);
    }

    public static final void d(dc0 dc0Var, gt gtVar, View view) {
        fs1.f(dc0Var, "this$0");
        fs1.f(gtVar, "$it");
        hd1<gt, Dialog, te4> f = dc0Var.f();
        Dialog dialog = dc0Var.e;
        if (dialog == null) {
            fs1.r("alertDialog");
            dialog = null;
        }
        f.invoke(gtVar, dialog);
    }

    public final dc0 b(boolean z, boolean z2) {
        View inflate = LayoutInflater.from(this.a).inflate(R.layout.dialog_custom_confirmation, (ViewGroup) null);
        nn0 a = nn0.a(inflate);
        this.d = a;
        fs1.d(a);
        for (final gt gtVar : e()) {
            View inflate2 = LayoutInflater.from(g()).inflate(R.layout.item_simple_text_button, (ViewGroup) null);
            Objects.requireNonNull(inflate2, "null cannot be cast to non-null type android.widget.Button");
            Button button = (Button) inflate2;
            String c = gtVar.c();
            if (c != null) {
                button.setText(c);
            }
            Integer b = gtVar.b();
            if (b != null) {
                button.setText(b.intValue());
            }
            int a2 = gtVar.a();
            if (a2 == -3 || a2 == -2 || a2 == -1) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(button.getText());
                spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 0);
                button.setText(spannableStringBuilder);
            }
            button.setTextColor(m70.d(g(), R.color.t5));
            if (gtVar.d()) {
                button.setOnClickListener(new View.OnClickListener() { // from class: cc0
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        dc0.d(dc0.this, gtVar, view);
                    }
                });
            }
            a.a.addView(button);
        }
        Dialog dialog = new Dialog(this.a, 2132017235);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(z);
        dialog.setCanceledOnTouchOutside(z2);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.getAttributes().gravity = 17;
            window.getAttributes().width = -1;
        }
        dialog.setContentView(inflate);
        te4 te4Var = te4.a;
        this.e = dialog;
        return this;
    }

    public final List<gt> e() {
        return this.b;
    }

    public final hd1<gt, Dialog, te4> f() {
        return this.c;
    }

    public final Context g() {
        return this.a;
    }

    public final boolean h() {
        Dialog dialog = this.e;
        if (dialog != null) {
            if (dialog == null) {
                fs1.r("alertDialog");
                dialog = null;
            }
            if (dialog.isShowing()) {
                return true;
            }
        }
        return false;
    }

    public final void i() {
        Dialog dialog = this.e;
        if (dialog != null) {
            if (dialog == null) {
                fs1.r("alertDialog");
                dialog = null;
            }
            dialog.show();
        }
    }
}
