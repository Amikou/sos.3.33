package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: l65  reason: default package */
/* loaded from: classes.dex */
public final class l65 extends o65 {
    public l65() {
        this.a.add(zzbl.BITWISE_AND);
        this.a.add(zzbl.BITWISE_LEFT_SHIFT);
        this.a.add(zzbl.BITWISE_NOT);
        this.a.add(zzbl.BITWISE_OR);
        this.a.add(zzbl.BITWISE_RIGHT_SHIFT);
        this.a.add(zzbl.BITWISE_UNSIGNED_RIGHT_SHIFT);
        this.a.add(zzbl.BITWISE_XOR);
    }

    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        zzbl zzblVar = zzbl.ADD;
        switch (vm5.e(str).ordinal()) {
            case 4:
                vm5.a(zzbl.BITWISE_AND.name(), 2, list);
                return new z45(Double.valueOf(vm5.g(wk5Var.a(list.get(0)).b().doubleValue()) & vm5.g(wk5Var.a(list.get(1)).b().doubleValue())));
            case 5:
                vm5.a(zzbl.BITWISE_LEFT_SHIFT.name(), 2, list);
                return new z45(Double.valueOf(vm5.g(wk5Var.a(list.get(0)).b().doubleValue()) << ((int) (vm5.h(wk5Var.a(list.get(1)).b().doubleValue()) & 31))));
            case 6:
                vm5.a(zzbl.BITWISE_NOT.name(), 1, list);
                return new z45(Double.valueOf(~vm5.g(wk5Var.a(list.get(0)).b().doubleValue())));
            case 7:
                vm5.a(zzbl.BITWISE_OR.name(), 2, list);
                return new z45(Double.valueOf(vm5.g(wk5Var.a(list.get(0)).b().doubleValue()) | vm5.g(wk5Var.a(list.get(1)).b().doubleValue())));
            case 8:
                vm5.a(zzbl.BITWISE_RIGHT_SHIFT.name(), 2, list);
                return new z45(Double.valueOf(vm5.g(wk5Var.a(list.get(0)).b().doubleValue()) >> ((int) (vm5.h(wk5Var.a(list.get(1)).b().doubleValue()) & 31))));
            case 9:
                vm5.a(zzbl.BITWISE_UNSIGNED_RIGHT_SHIFT.name(), 2, list);
                return new z45(Double.valueOf(vm5.h(wk5Var.a(list.get(0)).b().doubleValue()) >>> ((int) (vm5.h(wk5Var.a(list.get(1)).b().doubleValue()) & 31))));
            case 10:
                vm5.a(zzbl.BITWISE_XOR.name(), 2, list);
                return new z45(Double.valueOf(vm5.g(wk5Var.a(list.get(0)).b().doubleValue()) ^ vm5.g(wk5Var.a(list.get(1)).b().doubleValue())));
            default:
                return super.b(str);
        }
    }
}
