package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: f65  reason: default package */
/* loaded from: classes.dex */
public final class f65 implements Iterable<z55>, z55 {
    public final String a;

    public f65(String str) {
        if (str != null) {
            this.a = str;
            return;
        }
        throw new IllegalArgumentException("StringValue cannot be null.");
    }

    @Override // defpackage.z55
    public final Double b() {
        if (this.a.isEmpty()) {
            return Double.valueOf((double) Utils.DOUBLE_EPSILON);
        }
        try {
            return Double.valueOf(this.a);
        } catch (NumberFormatException unused) {
            return Double.valueOf(Double.NaN);
        }
    }

    @Override // defpackage.z55
    public final Boolean c() {
        return Boolean.valueOf(!this.a.isEmpty());
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof f65) {
            return this.a.equals(((f65) obj).a);
        }
        return false;
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return new d65(this);
    }

    @Override // java.lang.Iterable
    public final Iterator<z55> iterator() {
        return new e65(this);
    }

    @Override // defpackage.z55
    public final z55 m() {
        return new f65(this.a);
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        String str2;
        Object obj;
        String str3;
        String str4;
        char c;
        f65 f65Var;
        int i;
        z55 z45Var;
        double doubleValue;
        String str5;
        Matcher matcher;
        double min;
        double min2;
        f65 f65Var2;
        int i2;
        int i3;
        wk5 wk5Var2;
        int i4;
        int length;
        if ("charAt".equals(str) || "concat".equals(str) || "hasOwnProperty".equals(str) || "indexOf".equals(str) || "lastIndexOf".equals(str) || "match".equals(str) || "replace".equals(str) || "search".equals(str) || "slice".equals(str) || "split".equals(str) || "substring".equals(str) || "toLowerCase".equals(str) || "toLocaleLowerCase".equals(str) || "toString".equals(str) || "toUpperCase".equals(str)) {
            str2 = "toLocaleUpperCase";
        } else {
            str2 = "toLocaleUpperCase";
            if (!str2.equals(str) && !"trim".equals(str)) {
                throw new IllegalArgumentException(String.format("%s is not a String function", str));
            }
        }
        switch (str.hashCode()) {
            case -1789698943:
                obj = "charAt";
                str3 = "hasOwnProperty";
                if (str.equals(str3)) {
                    str4 = "toString";
                    c = 2;
                    break;
                }
                c = 65535;
                str4 = "toString";
                break;
            case -1776922004:
                obj = "charAt";
                if (str.equals("toString")) {
                    c = 14;
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -1464939364:
                obj = "charAt";
                if (str.equals("toLocaleLowerCase")) {
                    c = '\f';
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -1361633751:
                obj = "charAt";
                if (str.equals(obj)) {
                    str3 = "hasOwnProperty";
                    c = 0;
                    str4 = "toString";
                    break;
                }
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -1354795244:
                if (str.equals("concat")) {
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    c = 1;
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -1137582698:
                if (str.equals("toLowerCase")) {
                    c = '\r';
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -906336856:
                if (str.equals("search")) {
                    c = 7;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -726908483:
                if (str.equals(str2)) {
                    c = 11;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -467511597:
                if (str.equals("lastIndexOf")) {
                    c = 4;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case -399551817:
                if (str.equals("toUpperCase")) {
                    c = 15;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 3568674:
                if (str.equals("trim")) {
                    c = 16;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 103668165:
                if (str.equals("match")) {
                    c = 5;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 109526418:
                if (str.equals("slice")) {
                    c = '\b';
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 109648666:
                if (str.equals("split")) {
                    c = '\t';
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 530542161:
                if (str.equals("substring")) {
                    c = '\n';
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 1094496948:
                if (str.equals("replace")) {
                    c = 6;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            case 1943291465:
                if (str.equals("indexOf")) {
                    c = 3;
                    obj = "charAt";
                    str3 = "hasOwnProperty";
                    str4 = "toString";
                    break;
                }
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
            default:
                obj = "charAt";
                str3 = "hasOwnProperty";
                c = 65535;
                str4 = "toString";
                break;
        }
        String str6 = obj;
        switch (c) {
            case 0:
                vm5.c(str6, 1, list);
                int i5 = list.size() > 0 ? (int) vm5.i(wk5Var.a(list.get(0)).b().doubleValue()) : 0;
                String str7 = this.a;
                if (i5 >= 0 && i5 < str7.length()) {
                    return new f65(String.valueOf(str7.charAt(i5)));
                }
                return z55.e0;
            case 1:
                f65Var = this;
                if (list.size() != 0) {
                    StringBuilder sb = new StringBuilder(f65Var.a);
                    for (int i6 = 0; i6 < list.size(); i6++) {
                        sb.append(wk5Var.a(list.get(i6)).zzc());
                    }
                    return new f65(sb.toString());
                }
                return f65Var;
            case 2:
                vm5.a(str3, 1, list);
                String str8 = this.a;
                z55 a = wk5Var.a(list.get(0));
                if ("length".equals(a.zzc())) {
                    return z55.c0;
                }
                double doubleValue2 = a.b().doubleValue();
                return (doubleValue2 != Math.floor(doubleValue2) || (i = (int) doubleValue2) < 0 || i >= str8.length()) ? z55.d0 : z55.c0;
            case 3:
                vm5.c("indexOf", 2, list);
                z45Var = new z45(Double.valueOf(this.a.indexOf(list.size() > 0 ? wk5Var.a(list.get(0)).zzc() : "undefined", (int) vm5.i(list.size() < 2 ? 0.0d : wk5Var.a(list.get(1)).b().doubleValue()))));
                return z45Var;
            case 4:
                vm5.c("lastIndexOf", 2, list);
                String str9 = this.a;
                String zzc = list.size() > 0 ? wk5Var.a(list.get(0)).zzc() : "undefined";
                return new z45(Double.valueOf(str9.lastIndexOf(zzc, (int) (Double.isNaN(list.size() < 2 ? Double.NaN : wk5Var.a(list.get(1)).b().doubleValue()) ? Double.POSITIVE_INFINITY : vm5.i(doubleValue)))));
            case 5:
                vm5.c("match", 1, list);
                Matcher matcher2 = Pattern.compile(list.size() > 0 ? wk5Var.a(list.get(0)).zzc() : "").matcher(this.a);
                return matcher2.find() ? new p45(Arrays.asList(new f65(matcher2.group()))) : z55.Y;
            case 6:
                f65Var = this;
                vm5.c("replace", 2, list);
                z55 z55Var = z55.X;
                if (list.size() > 0) {
                    str5 = wk5Var.a(list.get(0)).zzc();
                    if (list.size() > 1) {
                        z55Var = wk5Var.a(list.get(1));
                    }
                }
                String str10 = str5;
                String str11 = f65Var.a;
                int indexOf = str11.indexOf(str10);
                if (indexOf >= 0) {
                    if (z55Var instanceof c55) {
                        z55Var = ((c55) z55Var).a(wk5Var, Arrays.asList(new f65(str10), new z45(Double.valueOf(indexOf)), f65Var));
                    }
                    String substring = str11.substring(0, indexOf);
                    String zzc2 = z55Var.zzc();
                    String substring2 = str11.substring(indexOf + str10.length());
                    StringBuilder sb2 = new StringBuilder(String.valueOf(substring).length() + String.valueOf(zzc2).length() + String.valueOf(substring2).length());
                    sb2.append(substring);
                    sb2.append(zzc2);
                    sb2.append(substring2);
                    z45Var = new f65(sb2.toString());
                    return z45Var;
                }
                return f65Var;
            case 7:
                vm5.c("search", 1, list);
                if (Pattern.compile(list.size() > 0 ? wk5Var.a(list.get(0)).zzc() : "undefined").matcher(this.a).find()) {
                    return new z45(Double.valueOf(matcher.start()));
                }
                return new z45(Double.valueOf(-1.0d));
            case '\b':
                vm5.c("slice", 2, list);
                String str12 = this.a;
                double i7 = vm5.i(list.size() > 0 ? wk5Var.a(list.get(0)).b().doubleValue() : Utils.DOUBLE_EPSILON);
                if (i7 < Utils.DOUBLE_EPSILON) {
                    min = Math.max(str12.length() + i7, (double) Utils.DOUBLE_EPSILON);
                } else {
                    min = Math.min(i7, str12.length());
                }
                int i8 = (int) min;
                double i9 = vm5.i(list.size() > 1 ? wk5Var.a(list.get(1)).b().doubleValue() : str12.length());
                if (i9 < Utils.DOUBLE_EPSILON) {
                    min2 = Math.max(str12.length() + i9, (double) Utils.DOUBLE_EPSILON);
                } else {
                    min2 = Math.min(i9, str12.length());
                }
                f65Var2 = new f65(str12.substring(i8, Math.max(0, ((int) min2) - i8) + i8));
                return f65Var2;
            case '\t':
                vm5.c("split", 2, list);
                String str13 = this.a;
                if (str13.length() == 0) {
                    return new p45(Arrays.asList(this));
                }
                ArrayList arrayList = new ArrayList();
                if (list.size() == 0) {
                    arrayList.add(this);
                } else {
                    String zzc3 = wk5Var.a(list.get(0)).zzc();
                    long h = list.size() > 1 ? vm5.h(wk5Var.a(list.get(1)).b().doubleValue()) : 2147483647L;
                    if (h == 0) {
                        return new p45();
                    }
                    String[] split = str13.split(Pattern.quote(zzc3), ((int) h) + 1);
                    int length2 = split.length;
                    if (!zzc3.equals("") || length2 <= 0) {
                        i2 = length2;
                        i3 = 0;
                    } else {
                        boolean equals = split[0].equals("");
                        i2 = length2 - 1;
                        if (!split[i2].equals("")) {
                            i2 = length2;
                        }
                        i3 = equals;
                    }
                    if (length2 > h) {
                        i2--;
                    }
                    for (int i10 = i3; i10 < i2; i10++) {
                        arrayList.add(new f65(split[i10]));
                    }
                }
                return new p45(arrayList);
            case '\n':
                vm5.c("substring", 2, list);
                String str14 = this.a;
                if (list.size() > 0) {
                    wk5Var2 = wk5Var;
                    i4 = (int) vm5.i(wk5Var2.a(list.get(0)).b().doubleValue());
                } else {
                    wk5Var2 = wk5Var;
                    i4 = 0;
                }
                if (list.size() > 1) {
                    length = (int) vm5.i(wk5Var2.a(list.get(1)).b().doubleValue());
                } else {
                    length = str14.length();
                }
                int min3 = Math.min(Math.max(i4, 0), str14.length());
                int min4 = Math.min(Math.max(length, 0), str14.length());
                f65Var2 = new f65(str14.substring(Math.min(min3, min4), Math.max(min3, min4)));
                return f65Var2;
            case 11:
                vm5.a(str2, 0, list);
                return new f65(this.a.toUpperCase());
            case '\f':
                vm5.a("toLocaleLowerCase", 0, list);
                return new f65(this.a.toLowerCase());
            case '\r':
                vm5.a("toLowerCase", 0, list);
                return new f65(this.a.toLowerCase(Locale.ENGLISH));
            case 14:
                f65Var = this;
                vm5.a(str4, 0, list);
                return f65Var;
            case 15:
                vm5.a("toUpperCase", 0, list);
                return new f65(this.a.toUpperCase(Locale.ENGLISH));
            case 16:
                vm5.a("toUpperCase", 0, list);
                return new f65(this.a.trim());
            default:
                throw new IllegalArgumentException("Command not supported");
        }
    }

    public final String toString() {
        String str = this.a;
        StringBuilder sb = new StringBuilder(str.length() + 2);
        sb.append('\"');
        sb.append(str);
        sb.append('\"');
        return sb.toString();
    }

    @Override // defpackage.z55
    public final String zzc() {
        return this.a;
    }
}
