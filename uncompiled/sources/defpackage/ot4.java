package defpackage;

import android.os.Bundle;
import com.google.android.play.core.assetpacks.a;

/* renamed from: ot4  reason: default package */
/* loaded from: classes2.dex */
public final class ot4 extends a<Void> {
    public final int c;
    public final String d;
    public final int e;
    public final /* synthetic */ st4 f;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ot4(st4 st4Var, tx4<Void> tx4Var, int i, String str, int i2) {
        super(st4Var, tx4Var);
        this.f = st4Var;
        this.c = i;
        this.d = str;
        this.e = i2;
    }

    @Override // com.google.android.play.core.assetpacks.a, com.google.android.play.core.internal.r
    public final void Z0(Bundle bundle) {
        zt4 zt4Var;
        it4 it4Var;
        zt4Var = this.f.c;
        zt4Var.b();
        int i = bundle.getInt("error_code");
        it4Var = st4.f;
        it4Var.b("onError(%d), retrying notifyModuleCompleted...", Integer.valueOf(i));
        int i2 = this.e;
        if (i2 > 0) {
            this.f.x(this.c, this.d, i2 - 1);
        }
    }
}
