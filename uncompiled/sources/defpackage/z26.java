package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: z26  reason: default package */
/* loaded from: classes.dex */
public final class z26 implements y26 {
    public static final wo5<Boolean> a;
    public static final wo5<Double> b;
    public static final wo5<Long> c;
    public static final wo5<Long> d;
    public static final wo5<String> e;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.test.boolean_flag", false);
        b = ro5Var.c("measurement.test.double_flag", -3.0d);
        c = ro5Var.a("measurement.test.int_flag", -2L);
        d = ro5Var.a("measurement.test.long_flag", -1L);
        e = ro5Var.d("measurement.test.string_flag", "---");
    }

    @Override // defpackage.y26
    public final long b() {
        return d.e().longValue();
    }

    @Override // defpackage.y26
    public final String c() {
        return e.e();
    }

    @Override // defpackage.y26
    public final boolean zza() {
        return a.e().booleanValue();
    }

    @Override // defpackage.y26
    public final double zzb() {
        return b.e().doubleValue();
    }

    @Override // defpackage.y26
    public final long zzc() {
        return c.e().longValue();
    }
}
