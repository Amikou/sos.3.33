package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapCounter.java */
/* renamed from: rp  reason: default package */
/* loaded from: classes.dex */
public class rp {
    public int a;
    public long b;
    public final int c;
    public final int d;
    public final d83<Bitmap> e;

    /* compiled from: BitmapCounter.java */
    /* renamed from: rp$a */
    /* loaded from: classes.dex */
    public class a implements d83<Bitmap> {
        public a() {
        }

        @Override // defpackage.d83
        /* renamed from: b */
        public void a(Bitmap bitmap) {
            try {
                rp.this.a(bitmap);
            } finally {
                bitmap.recycle();
            }
        }
    }

    public rp(int i, int i2) {
        xt2.b(Boolean.valueOf(i > 0));
        xt2.b(Boolean.valueOf(i2 > 0));
        this.c = i;
        this.d = i2;
        this.e = new a();
    }

    public synchronized void a(Bitmap bitmap) {
        int e = rq.e(bitmap);
        xt2.c(this.a > 0, "No bitmaps registered.");
        long j = e;
        xt2.d(j <= this.b, "Bitmap size bigger than the total registered size: %d, %d", Integer.valueOf(e), Long.valueOf(this.b));
        this.b -= j;
        this.a--;
    }

    public synchronized int b() {
        return this.a;
    }

    public synchronized int c() {
        return this.c;
    }

    public synchronized int d() {
        return this.d;
    }

    public d83<Bitmap> e() {
        return this.e;
    }

    public synchronized long f() {
        return this.b;
    }

    public synchronized boolean g(Bitmap bitmap) {
        int e = rq.e(bitmap);
        int i = this.a;
        if (i < this.c) {
            long j = this.b;
            long j2 = e;
            if (j + j2 <= this.d) {
                this.a = i + 1;
                this.b = j + j2;
                return true;
            }
        }
        return false;
    }
}
