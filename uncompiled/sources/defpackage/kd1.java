package defpackage;

/* compiled from: Functions.kt */
/* renamed from: kd1  reason: default package */
/* loaded from: classes2.dex */
public interface kd1<P1, P2, P3, R> extends pd1<R> {
    R invoke(P1 p1, P2 p2, P3 p3);
}
