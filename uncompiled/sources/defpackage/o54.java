package defpackage;

/* compiled from: Emitters.kt */
/* renamed from: o54  reason: default package */
/* loaded from: classes2.dex */
public final class o54 implements k71<Object> {
    public final Throwable a;

    public o54(Throwable th) {
        this.a = th;
    }

    @Override // defpackage.k71
    public Object emit(Object obj, q70<? super te4> q70Var) {
        throw this.a;
    }
}
