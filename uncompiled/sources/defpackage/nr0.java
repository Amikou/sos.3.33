package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import defpackage.wn;

/* compiled from: DrawingDelegate.java */
/* renamed from: nr0  reason: default package */
/* loaded from: classes2.dex */
public abstract class nr0<S extends wn> {
    public S a;
    public er0 b;

    public nr0(S s) {
        this.a = s;
    }

    public abstract void a(Canvas canvas, float f);

    public abstract void b(Canvas canvas, Paint paint, float f, float f2, int i);

    public abstract void c(Canvas canvas, Paint paint);

    public abstract int d();

    public abstract int e();

    public void f(er0 er0Var) {
        this.b = er0Var;
    }

    public void g(Canvas canvas, float f) {
        this.a.e();
        a(canvas, f);
    }
}
