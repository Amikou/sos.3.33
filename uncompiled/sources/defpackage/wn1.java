package defpackage;

/* compiled from: ImageFormat.java */
/* renamed from: wn1  reason: default package */
/* loaded from: classes.dex */
public class wn1 {
    public static final wn1 b = new wn1("UNKNOWN", null);
    public final String a;

    /* compiled from: ImageFormat.java */
    /* renamed from: wn1$a */
    /* loaded from: classes.dex */
    public interface a {
        int a();

        wn1 b(byte[] bArr, int i);
    }

    public wn1(String str, String str2) {
        this.a = str;
    }

    public String a() {
        return this.a;
    }

    public String toString() {
        return a();
    }
}
