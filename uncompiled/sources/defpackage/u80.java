package defpackage;

import defpackage.u80;
import java.lang.Throwable;

/* compiled from: Debug.common.kt */
/* renamed from: u80  reason: default package */
/* loaded from: classes2.dex */
public interface u80<T extends Throwable & u80<T>> {
    T createCopy();
}
