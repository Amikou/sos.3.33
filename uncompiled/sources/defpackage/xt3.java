package defpackage;

import android.content.Context;

/* compiled from: StorageNotLowController.java */
/* renamed from: xt3  reason: default package */
/* loaded from: classes.dex */
public class xt3 extends d60<Boolean> {
    public xt3(Context context, q34 q34Var) {
        super(j84.c(context, q34Var).e());
    }

    @Override // defpackage.d60
    public boolean b(tq4 tq4Var) {
        return tq4Var.j.i();
    }

    @Override // defpackage.d60
    /* renamed from: i */
    public boolean c(Boolean bool) {
        return !bool.booleanValue();
    }
}
