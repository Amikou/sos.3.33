package defpackage;

import java.util.concurrent.locks.LockSupport;
import kotlinx.coroutines.d;
import kotlinx.coroutines.e;

/* compiled from: EventLoop.kt */
/* renamed from: yx0  reason: default package */
/* loaded from: classes2.dex */
public abstract class yx0 extends xx0 {
    public abstract Thread f0();

    public final void g0(long j, e.b bVar) {
        if (ze0.a()) {
            if (!(this != d.k0)) {
                throw new AssertionError();
            }
        }
        d.k0.J0(j, bVar);
    }

    public final void i0() {
        Thread f0 = f0();
        if (Thread.currentThread() != f0) {
            n5.a();
            LockSupport.unpark(f0);
        }
    }
}
