package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: ConstraintController.java */
/* renamed from: d60  reason: default package */
/* loaded from: classes.dex */
public abstract class d60<T> implements f60<T> {
    public final List<String> a = new ArrayList();
    public T b;
    public g60<T> c;
    public a d;

    /* compiled from: ConstraintController.java */
    /* renamed from: d60$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(List<String> list);

        void b(List<String> list);
    }

    public d60(g60<T> g60Var) {
        this.c = g60Var;
    }

    @Override // defpackage.f60
    public void a(T t) {
        this.b = t;
        h(this.d, t);
    }

    public abstract boolean b(tq4 tq4Var);

    public abstract boolean c(T t);

    public boolean d(String str) {
        T t = this.b;
        return t != null && c(t) && this.a.contains(str);
    }

    public void e(Iterable<tq4> iterable) {
        this.a.clear();
        for (tq4 tq4Var : iterable) {
            if (b(tq4Var)) {
                this.a.add(tq4Var.a);
            }
        }
        if (this.a.isEmpty()) {
            this.c.c(this);
        } else {
            this.c.a(this);
        }
        h(this.d, this.b);
    }

    public void f() {
        if (this.a.isEmpty()) {
            return;
        }
        this.a.clear();
        this.c.c(this);
    }

    public void g(a aVar) {
        if (this.d != aVar) {
            this.d = aVar;
            h(aVar, this.b);
        }
    }

    public final void h(a aVar, T t) {
        if (this.a.isEmpty() || aVar == null) {
            return;
        }
        if (t != null && !c(t)) {
            aVar.a(this.a);
        } else {
            aVar.b(this.a);
        }
    }
}
