package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import defpackage.o81;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

/* compiled from: FontRequestWorker.java */
/* renamed from: m81  reason: default package */
/* loaded from: classes.dex */
public class m81 {
    public static final t22<String, Typeface> a = new t22<>(16);
    public static final ExecutorService b = f73.a("fonts-androidx", 10, 10000);
    public static final Object c = new Object();
    public static final vo3<String, ArrayList<n60<e>>> d = new vo3<>();

    /* compiled from: FontRequestWorker.java */
    /* renamed from: m81$a */
    /* loaded from: classes.dex */
    public class a implements Callable<e> {
        public final /* synthetic */ String a;
        public final /* synthetic */ Context b;
        public final /* synthetic */ l81 c;
        public final /* synthetic */ int d;

        public a(String str, Context context, l81 l81Var, int i) {
            this.a = str;
            this.b = context;
            this.c = l81Var;
            this.d = i;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public e call() {
            return m81.c(this.a, this.b, this.c, this.d);
        }
    }

    /* compiled from: FontRequestWorker.java */
    /* renamed from: m81$b */
    /* loaded from: classes.dex */
    public class b implements n60<e> {
        public final /* synthetic */ yu a;

        public b(yu yuVar) {
            this.a = yuVar;
        }

        @Override // defpackage.n60
        /* renamed from: a */
        public void accept(e eVar) {
            if (eVar == null) {
                eVar = new e(-3);
            }
            this.a.b(eVar);
        }
    }

    /* compiled from: FontRequestWorker.java */
    /* renamed from: m81$c */
    /* loaded from: classes.dex */
    public class c implements Callable<e> {
        public final /* synthetic */ String a;
        public final /* synthetic */ Context b;
        public final /* synthetic */ l81 c;
        public final /* synthetic */ int d;

        public c(String str, Context context, l81 l81Var, int i) {
            this.a = str;
            this.b = context;
            this.c = l81Var;
            this.d = i;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public e call() {
            try {
                return m81.c(this.a, this.b, this.c, this.d);
            } catch (Throwable unused) {
                return new e(-3);
            }
        }
    }

    /* compiled from: FontRequestWorker.java */
    /* renamed from: m81$d */
    /* loaded from: classes.dex */
    public class d implements n60<e> {
        public final /* synthetic */ String a;

        public d(String str) {
            this.a = str;
        }

        @Override // defpackage.n60
        /* renamed from: a */
        public void accept(e eVar) {
            synchronized (m81.c) {
                vo3<String, ArrayList<n60<e>>> vo3Var = m81.d;
                ArrayList<n60<e>> arrayList = vo3Var.get(this.a);
                if (arrayList == null) {
                    return;
                }
                vo3Var.remove(this.a);
                for (int i = 0; i < arrayList.size(); i++) {
                    arrayList.get(i).accept(eVar);
                }
            }
        }
    }

    public static String a(l81 l81Var, int i) {
        return l81Var.d() + "-" + i;
    }

    @SuppressLint({"WrongConstant"})
    public static int b(o81.a aVar) {
        int i = 1;
        if (aVar.c() != 0) {
            return aVar.c() != 1 ? -3 : -2;
        }
        o81.b[] b2 = aVar.b();
        if (b2 != null && b2.length != 0) {
            i = 0;
            for (o81.b bVar : b2) {
                int b3 = bVar.b();
                if (b3 != 0) {
                    if (b3 < 0) {
                        return -3;
                    }
                    return b3;
                }
            }
        }
        return i;
    }

    public static e c(String str, Context context, l81 l81Var, int i) {
        t22<String, Typeface> t22Var = a;
        Typeface c2 = t22Var.c(str);
        if (c2 != null) {
            return new e(c2);
        }
        try {
            o81.a d2 = k81.d(context, l81Var, null);
            int b2 = b(d2);
            if (b2 != 0) {
                return new e(b2);
            }
            Typeface b3 = yd4.b(context, null, d2.b(), i);
            if (b3 != null) {
                t22Var.d(str, b3);
                return new e(b3);
            }
            return new e(-3);
        } catch (PackageManager.NameNotFoundException unused) {
            return new e(-1);
        }
    }

    public static Typeface d(Context context, l81 l81Var, int i, Executor executor, yu yuVar) {
        String a2 = a(l81Var, i);
        Typeface c2 = a.c(a2);
        if (c2 != null) {
            yuVar.b(new e(c2));
            return c2;
        }
        b bVar = new b(yuVar);
        synchronized (c) {
            vo3<String, ArrayList<n60<e>>> vo3Var = d;
            ArrayList<n60<e>> arrayList = vo3Var.get(a2);
            if (arrayList != null) {
                arrayList.add(bVar);
                return null;
            }
            ArrayList<n60<e>> arrayList2 = new ArrayList<>();
            arrayList2.add(bVar);
            vo3Var.put(a2, arrayList2);
            c cVar = new c(a2, context, l81Var, i);
            if (executor == null) {
                executor = b;
            }
            f73.b(executor, cVar, new d(a2));
            return null;
        }
    }

    public static Typeface e(Context context, l81 l81Var, yu yuVar, int i, int i2) {
        String a2 = a(l81Var, i);
        Typeface c2 = a.c(a2);
        if (c2 != null) {
            yuVar.b(new e(c2));
            return c2;
        } else if (i2 == -1) {
            e c3 = c(a2, context, l81Var, i);
            yuVar.b(c3);
            return c3.a;
        } else {
            try {
                e eVar = (e) f73.c(b, new a(a2, context, l81Var, i), i2);
                yuVar.b(eVar);
                return eVar.a;
            } catch (InterruptedException unused) {
                yuVar.b(new e(-3));
                return null;
            }
        }
    }

    /* compiled from: FontRequestWorker.java */
    /* renamed from: m81$e */
    /* loaded from: classes.dex */
    public static final class e {
        public final Typeface a;
        public final int b;

        public e(int i) {
            this.a = null;
            this.b = i;
        }

        @SuppressLint({"WrongConstant"})
        public boolean a() {
            return this.b == 0;
        }

        @SuppressLint({"WrongConstant"})
        public e(Typeface typeface) {
            this.a = typeface;
            this.b = 0;
        }
    }
}
