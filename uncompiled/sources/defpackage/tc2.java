package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: tc2  reason: default package */
/* loaded from: classes2.dex */
public interface tc2 {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;
    public static final i g;
    public static final i h;
    public static final i i;
    public static final i j;

    static {
        i iVar = new i("2.16.840.1.101.3.4");
        a = iVar;
        i z = iVar.z("2");
        b = z;
        c = z.z("1");
        d = z.z("2");
        e = z.z("3");
        f = z.z("4");
        z.z("5");
        z.z("6");
        z.z("7");
        z.z("8");
        z.z("9");
        z.z("10");
        g = z.z("11");
        h = z.z("12");
        z.z("13");
        z.z("14");
        z.z("15");
        z.z("16");
        i z2 = iVar.z("1");
        i = z2;
        z2.z("1");
        z2.z("2");
        z2.z("3");
        z2.z("4");
        z2.z("5");
        z2.z("6");
        z2.z("7");
        z2.z("8");
        z2.z("21");
        z2.z("22");
        z2.z("23");
        z2.z("24");
        z2.z("25");
        z2.z("26");
        z2.z("27");
        z2.z("28");
        z2.z("41");
        z2.z("42");
        z2.z("43");
        z2.z("44");
        z2.z("45");
        z2.z("46");
        z2.z("47");
        z2.z("48");
        i z3 = iVar.z("3");
        j = z3;
        z3.z("1");
        z3.z("2");
        z3.z("3");
        z3.z("4");
        z3.z("5");
        z3.z("6");
        z3.z("7");
        z3.z("8");
        z3.z("9");
        z3.z("10");
        z3.z("11");
        z3.z("12");
        z3.z("13");
        z3.z("14");
        z3.z("15");
        z3.z("16");
    }
}
