package defpackage;

import android.net.Uri;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: bo5  reason: default package */
/* loaded from: classes.dex */
public final class bo5 {
    public static final rh<String, Uri> a = new rh<>();

    public static synchronized Uri a(String str) {
        Uri uri;
        synchronized (bo5.class) {
            rh<String, Uri> rhVar = a;
            uri = rhVar.get("com.google.android.gms.measurement");
            if (uri == null) {
                String valueOf = String.valueOf(Uri.encode("com.google.android.gms.measurement"));
                uri = Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
                rhVar.put("com.google.android.gms.measurement", uri);
            }
        }
        return uri;
    }
}
