package defpackage;

import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.a;
import java.util.HashMap;
import java.util.Map;

/* compiled from: StagingArea.java */
/* renamed from: js3  reason: default package */
/* loaded from: classes.dex */
public class js3 {
    public static final Class<?> b = js3.class;
    public Map<wt, zu0> a = new HashMap();

    public static js3 b() {
        return new js3();
    }

    public synchronized zu0 a(wt wtVar) {
        xt2.g(wtVar);
        zu0 zu0Var = this.a.get(wtVar);
        if (zu0Var != null) {
            synchronized (zu0Var) {
                if (!zu0.F(zu0Var)) {
                    this.a.remove(wtVar);
                    v11.v(b, "Found closed reference %d for key %s (%d)", Integer.valueOf(System.identityHashCode(zu0Var)), wtVar.b(), Integer.valueOf(System.identityHashCode(wtVar)));
                    return null;
                }
                zu0Var = zu0.b(zu0Var);
            }
        }
        return zu0Var;
    }

    public final synchronized void c() {
        v11.o(b, "Count = %d", Integer.valueOf(this.a.size()));
    }

    public synchronized void d(wt wtVar, zu0 zu0Var) {
        xt2.g(wtVar);
        xt2.b(Boolean.valueOf(zu0.F(zu0Var)));
        zu0.c(this.a.put(wtVar, zu0.b(zu0Var)));
        c();
    }

    public boolean e(wt wtVar) {
        zu0 remove;
        xt2.g(wtVar);
        synchronized (this) {
            remove = this.a.remove(wtVar);
        }
        if (remove == null) {
            return false;
        }
        try {
            return remove.C();
        } finally {
            remove.close();
        }
    }

    public synchronized boolean f(wt wtVar, zu0 zu0Var) {
        xt2.g(wtVar);
        xt2.g(zu0Var);
        xt2.b(Boolean.valueOf(zu0.F(zu0Var)));
        zu0 zu0Var2 = this.a.get(wtVar);
        if (zu0Var2 == null) {
            return false;
        }
        a<PooledByteBuffer> e = zu0Var2.e();
        a<PooledByteBuffer> e2 = zu0Var.e();
        if (e != null && e2 != null && e.j() == e2.j()) {
            this.a.remove(wtVar);
            a.g(e2);
            a.g(e);
            zu0.c(zu0Var2);
            c();
            return true;
        }
        a.g(e2);
        a.g(e);
        zu0.c(zu0Var2);
        return false;
    }
}
