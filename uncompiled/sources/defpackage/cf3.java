package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: cf3  reason: default package */
/* loaded from: classes2.dex */
public class cf3 extends ct0.b {
    public static final BigInteger g = af3.j;
    public int[] f;

    public cf3() {
        this.f = dd2.e();
    }

    public cf3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP224R1FieldElement");
        }
        this.f = bf3.d(bigInteger);
    }

    public cf3(int[] iArr) {
        this.f = iArr;
    }

    public static void u(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4, int[] iArr5, int[] iArr6, int[] iArr7) {
        bf3.e(iArr5, iArr3, iArr7);
        bf3.e(iArr7, iArr, iArr7);
        bf3.e(iArr4, iArr2, iArr6);
        bf3.a(iArr6, iArr7, iArr6);
        bf3.e(iArr4, iArr3, iArr7);
        dd2.d(iArr6, iArr4);
        bf3.e(iArr5, iArr2, iArr5);
        bf3.a(iArr5, iArr7, iArr5);
        bf3.j(iArr5, iArr6);
        bf3.e(iArr6, iArr, iArr6);
    }

    public static void v(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4, int[] iArr5) {
        dd2.d(iArr, iArr4);
        int[] e = dd2.e();
        int[] e2 = dd2.e();
        for (int i = 0; i < 7; i++) {
            dd2.d(iArr2, e);
            dd2.d(iArr3, e2);
            int i2 = 1 << i;
            while (true) {
                i2--;
                if (i2 >= 0) {
                    w(iArr2, iArr3, iArr4, iArr5);
                }
            }
            u(iArr, e, e2, iArr2, iArr3, iArr4, iArr5);
        }
    }

    public static void w(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4) {
        bf3.e(iArr2, iArr, iArr2);
        bf3.n(iArr2, iArr2);
        bf3.j(iArr, iArr4);
        bf3.a(iArr3, iArr4, iArr);
        bf3.e(iArr3, iArr4, iArr3);
        bf3.i(kd2.G(7, iArr3, 2, 0), iArr3);
    }

    public static boolean x(int[] iArr) {
        int[] e = dd2.e();
        int[] e2 = dd2.e();
        dd2.d(iArr, e);
        for (int i = 0; i < 7; i++) {
            dd2.d(e, e2);
            bf3.k(e, 1 << i, e);
            bf3.e(e, e2, e);
        }
        bf3.k(e, 95, e);
        return dd2.k(e);
    }

    public static boolean y(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] e = dd2.e();
        dd2.d(iArr2, e);
        int[] e2 = dd2.e();
        e2[0] = 1;
        int[] e3 = dd2.e();
        v(iArr, e, e2, e3, iArr3);
        int[] e4 = dd2.e();
        int[] e5 = dd2.e();
        for (int i = 1; i < 96; i++) {
            dd2.d(e, e4);
            dd2.d(e2, e5);
            w(e, e2, e3, iArr3);
            if (dd2.l(e)) {
                g92.d(bf3.a, e5, iArr3);
                bf3.e(iArr3, e4, iArr3);
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] e = dd2.e();
        bf3.a(this.f, ((cf3) ct0Var).f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] e = dd2.e();
        bf3.b(this.f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] e = dd2.e();
        g92.d(bf3.a, ((cf3) ct0Var).f, e);
        bf3.e(e, this.f, e);
        return new cf3(e);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof cf3) {
            return dd2.g(this.f, ((cf3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] e = dd2.e();
        g92.d(bf3.a, this.f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return dd2.k(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 7);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return dd2.l(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] e = dd2.e();
        bf3.e(this.f, ((cf3) ct0Var).f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] e = dd2.e();
        bf3.g(this.f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (dd2.l(iArr) || dd2.k(iArr)) {
            return this;
        }
        int[] e = dd2.e();
        bf3.g(iArr, e);
        int[] e2 = g92.e(bf3.a);
        int[] e3 = dd2.e();
        if (x(iArr)) {
            while (!y(e, e2, e3)) {
                bf3.b(e2, e2);
            }
            bf3.j(e3, e2);
            if (dd2.g(iArr, e2)) {
                return new cf3(e3);
            }
            return null;
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] e = dd2.e();
        bf3.j(this.f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] e = dd2.e();
        bf3.m(this.f, ((cf3) ct0Var).f, e);
        return new cf3(e);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return dd2.i(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return dd2.u(this.f);
    }
}
