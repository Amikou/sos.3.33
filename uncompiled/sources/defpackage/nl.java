package defpackage;

import java.util.Objects;

/* compiled from: AutoValue_PersistedEvent.java */
/* renamed from: nl  reason: default package */
/* loaded from: classes.dex */
public final class nl extends nq2 {
    public final long a;
    public final ob4 b;
    public final wx0 c;

    public nl(long j, ob4 ob4Var, wx0 wx0Var) {
        this.a = j;
        Objects.requireNonNull(ob4Var, "Null transportContext");
        this.b = ob4Var;
        Objects.requireNonNull(wx0Var, "Null event");
        this.c = wx0Var;
    }

    @Override // defpackage.nq2
    public wx0 b() {
        return this.c;
    }

    @Override // defpackage.nq2
    public long c() {
        return this.a;
    }

    @Override // defpackage.nq2
    public ob4 d() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof nq2) {
            nq2 nq2Var = (nq2) obj;
            return this.a == nq2Var.c() && this.b.equals(nq2Var.d()) && this.c.equals(nq2Var.b());
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        return ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    public String toString() {
        return "PersistedEvent{id=" + this.a + ", transportContext=" + this.b + ", event=" + this.c + "}";
    }
}
