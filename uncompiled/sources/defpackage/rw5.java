package defpackage;

import com.google.android.gms.internal.vision.r0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: rw5  reason: default package */
/* loaded from: classes.dex */
public final class rw5 {
    public static final lw5 a = c();
    public static final lw5 b = new r0();

    public static lw5 a() {
        return a;
    }

    public static lw5 b() {
        return b;
    }

    public static lw5 c() {
        try {
            return (lw5) com.google.protobuf.r0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
