package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import java.util.List;

/* compiled from: UserDataReader.java */
/* renamed from: xf4  reason: default package */
/* loaded from: classes.dex */
public final class xf4 {
    public final List<j> a;
    public final f84[] b;

    public xf4(List<j> list) {
        this.a = list;
        this.b = new f84[list.size()];
    }

    public void a(long j, op2 op2Var) {
        if (op2Var.a() < 9) {
            return;
        }
        int n = op2Var.n();
        int n2 = op2Var.n();
        int D = op2Var.D();
        if (n == 434 && n2 == 1195456820 && D == 3) {
            uw.b(j, op2Var, this.b);
        }
    }

    public void b(r11 r11Var, gc4.d dVar) {
        for (int i = 0; i < this.b.length; i++) {
            dVar.a();
            f84 f = r11Var.f(dVar.c(), 3);
            j jVar = this.a.get(i);
            String str = jVar.p0;
            ii.b("application/cea-608".equals(str) || "application/cea-708".equals(str), "Invalid closed caption mime type provided: " + str);
            f.f(new j.b().S(dVar.b()).e0(str).g0(jVar.h0).V(jVar.g0).F(jVar.H0).T(jVar.r0).E());
            this.b[i] = f;
        }
    }
}
