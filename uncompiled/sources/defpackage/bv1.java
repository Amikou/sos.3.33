package defpackage;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.a;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import java.io.IOException;

/* compiled from: JsonGeneratorImpl.java */
/* renamed from: bv1  reason: default package */
/* loaded from: classes.dex */
public abstract class bv1 extends we1 {
    public static final int[] q0 = a.e();
    public final jm1 k0;
    public int[] l0;
    public int m0;
    public CharacterEscapes n0;
    public yl3 o0;
    public boolean p0;

    public bv1(jm1 jm1Var, int i, c cVar) {
        super(i, cVar);
        this.l0 = q0;
        this.o0 = DefaultPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR;
        this.k0 = jm1Var;
        if (JsonGenerator.Feature.ESCAPE_NON_ASCII.enabledIn(i)) {
            this.m0 = 127;
        }
        this.p0 = !JsonGenerator.Feature.QUOTE_FIELD_NAMES.enabledIn(i);
    }

    public void H1(String str) throws IOException {
        a(String.format("Can not %s, expecting field name (context: %s)", str, this.i0.i()));
    }

    public void I1(String str, int i) throws IOException {
        if (i == 0) {
            if (this.i0.e()) {
                this.a.beforeArrayValues(this);
            } else if (this.i0.f()) {
                this.a.beforeObjectEntries(this);
            }
        } else if (i == 1) {
            this.a.writeArrayValueSeparator(this);
        } else if (i == 2) {
            this.a.writeObjectFieldValueSeparator(this);
        } else if (i == 3) {
            this.a.writeRootValueSeparator(this);
        } else if (i != 5) {
            b();
        } else {
            H1(str);
        }
    }

    @Override // defpackage.we1, com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator i(JsonGenerator.Feature feature) {
        super.i(feature);
        if (feature == JsonGenerator.Feature.QUOTE_FIELD_NAMES) {
            this.p0 = true;
        }
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator r(CharacterEscapes characterEscapes) {
        this.n0 = characterEscapes;
        if (characterEscapes == null) {
            this.l0 = q0;
        } else {
            this.l0 = characterEscapes.getEscapeCodesForAscii();
        }
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void r1(String str, String str2) throws IOException {
        i0(str);
        o1(str2);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator w(int i) {
        if (i < 0) {
            i = 0;
        }
        this.m0 = i;
        return this;
    }

    @Override // defpackage.we1
    public void w1(int i, int i2) {
        super.w1(i, i2);
        this.p0 = !JsonGenerator.Feature.QUOTE_FIELD_NAMES.enabledIn(i);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public JsonGenerator z(yl3 yl3Var) {
        this.o0 = yl3Var;
        return this;
    }
}
