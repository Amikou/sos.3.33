package defpackage;

import com.google.android.gms.measurement.internal.zzas;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: hl5  reason: default package */
/* loaded from: classes.dex */
public final class hl5 implements Runnable {
    public final /* synthetic */ zzas a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ pl5 g0;

    public hl5(pl5 pl5Var, zzas zzasVar, String str) {
        this.g0 = pl5Var;
        this.a = zzasVar;
        this.f0 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        fw5 fw5Var2;
        fw5Var = this.g0.a;
        fw5Var.i();
        fw5Var2 = this.g0.a;
        fw5Var2.h0(this.a, this.f0);
    }
}
