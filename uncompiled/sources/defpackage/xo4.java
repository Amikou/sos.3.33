package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.util.b;
import java.util.regex.Pattern;

/* compiled from: WebvttParserUtil.java */
/* renamed from: xo4  reason: default package */
/* loaded from: classes.dex */
public final class xo4 {
    static {
        Pattern.compile("^NOTE([ \t].*)?$");
    }

    public static boolean a(op2 op2Var) {
        String p = op2Var.p();
        return p != null && p.startsWith("WEBVTT");
    }

    public static float b(String str) throws NumberFormatException {
        if (str.endsWith("%")) {
            return Float.parseFloat(str.substring(0, str.length() - 1)) / 100.0f;
        }
        throw new NumberFormatException("Percentages must end with %");
    }

    public static long c(String str) throws NumberFormatException {
        String[] M0 = b.M0(str, "\\.");
        long j = 0;
        for (String str2 : b.L0(M0[0], ":")) {
            j = (j * 60) + Long.parseLong(str2);
        }
        long j2 = j * 1000;
        if (M0.length == 2) {
            j2 += Long.parseLong(M0[1]);
        }
        return j2 * 1000;
    }

    public static void d(op2 op2Var) throws ParserException {
        int e = op2Var.e();
        if (a(op2Var)) {
            return;
        }
        op2Var.P(e);
        throw ParserException.createForMalformedContainer("Expected WEBVTT. Got " + op2Var.p(), null);
    }
}
