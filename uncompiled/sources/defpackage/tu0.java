package defpackage;

import androidx.media3.decoder.DecoderInputBuffer;
import androidx.media3.exoplayer.source.r;

/* compiled from: EmptySampleStream.java */
/* renamed from: tu0  reason: default package */
/* loaded from: classes.dex */
public final class tu0 implements r {
    @Override // androidx.media3.exoplayer.source.r
    public void b() {
    }

    @Override // androidx.media3.exoplayer.source.r
    public boolean f() {
        return true;
    }

    @Override // androidx.media3.exoplayer.source.r
    public int m(long j) {
        return 0;
    }

    @Override // androidx.media3.exoplayer.source.r
    public int p(y81 y81Var, DecoderInputBuffer decoderInputBuffer, int i) {
        decoderInputBuffer.t(4);
        return -4;
    }
}
