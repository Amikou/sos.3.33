package defpackage;

import com.google.crypto.tink.shaded.protobuf.ByteString;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: UnmodifiableLazyStringList.java */
/* renamed from: cf4  reason: default package */
/* loaded from: classes2.dex */
public class cf4 extends AbstractList<String> implements dz1, RandomAccess {
    public final dz1 a;

    /* compiled from: UnmodifiableLazyStringList.java */
    /* renamed from: cf4$a */
    /* loaded from: classes2.dex */
    public class a implements ListIterator<String> {
        public ListIterator<String> a;
        public final /* synthetic */ int f0;

        public a(int i) {
            this.f0 = i;
            this.a = cf4.this.a.listIterator(i);
        }

        @Override // java.util.ListIterator
        /* renamed from: a */
        public void add(String str) {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.ListIterator, java.util.Iterator
        /* renamed from: b */
        public String next() {
            return this.a.next();
        }

        @Override // java.util.ListIterator
        /* renamed from: c */
        public String previous() {
            return this.a.previous();
        }

        @Override // java.util.ListIterator
        /* renamed from: d */
        public void set(String str) {
            throw new UnsupportedOperationException();
        }

        @Override // java.util.ListIterator, java.util.Iterator
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @Override // java.util.ListIterator
        public boolean hasPrevious() {
            return this.a.hasPrevious();
        }

        @Override // java.util.ListIterator
        public int nextIndex() {
            return this.a.nextIndex();
        }

        @Override // java.util.ListIterator
        public int previousIndex() {
            return this.a.previousIndex();
        }

        @Override // java.util.ListIterator, java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /* compiled from: UnmodifiableLazyStringList.java */
    /* renamed from: cf4$b */
    /* loaded from: classes2.dex */
    public class b implements Iterator<String> {
        public Iterator<String> a;

        public b() {
            this.a = cf4.this.a.iterator();
        }

        @Override // java.util.Iterator
        /* renamed from: a */
        public String next() {
            return this.a.next();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public cf4(dz1 dz1Var) {
        this.a = dz1Var;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public String get(int i) {
        return (String) this.a.get(i);
    }

    @Override // defpackage.dz1
    public void i1(ByteString byteString) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
    public Iterator<String> iterator() {
        return new b();
    }

    @Override // defpackage.dz1
    public Object j(int i) {
        return this.a.j(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public ListIterator<String> listIterator(int i) {
        return new a(i);
    }

    @Override // defpackage.dz1
    public List<?> r() {
        return this.a.r();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.a.size();
    }

    @Override // defpackage.dz1
    public dz1 x() {
        return this;
    }
}
