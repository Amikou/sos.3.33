package defpackage;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: qv5  reason: default package */
/* loaded from: classes.dex */
public interface qv5 {
    Map<?, ?> a(Object obj);

    Map<?, ?> b(Object obj);

    Object c(Object obj);

    Object d(Object obj);

    boolean e(Object obj);

    lv5<?, ?> f(Object obj);

    Object g(Object obj, Object obj2);

    int h(int i, Object obj, Object obj2);
}
