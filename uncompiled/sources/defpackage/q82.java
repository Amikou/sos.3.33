package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.extractor.metadata.id3.ApicFrame;
import androidx.media3.extractor.metadata.id3.CommentFrame;
import androidx.media3.extractor.metadata.id3.Id3Frame;
import androidx.media3.extractor.metadata.id3.InternalFrame;
import androidx.media3.extractor.metadata.id3.TextInformationFrame;
import androidx.media3.extractor.metadata.mp4.MdtaMetadataEntry;

/* compiled from: MetadataUtil.java */
/* renamed from: q82  reason: default package */
/* loaded from: classes.dex */
public final class q82 {
    public static final String[] a = {"Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "Indie", "BritPop", "Afro-Punk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Contemporary Christian", "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop", "Abstract", "Art Rock", "Baroque", "Bhangra", "Big beat", "Breakbeat", "Chillout", "Downtempo", "Dub", "EBM", "Eclectic", "Electro", "Electroclash", "Emo", "Experimental", "Garage", "Global", "IDM", "Illbient", "Industro-Goth", "Jam Band", "Krautrock", "Leftfield", "Lounge", "Math Rock", "New Romantic", "Nu-Breakz", "Post-Punk", "Post-Rock", "Psytrance", "Shoegaze", "Space Rock", "Trop Rock", "World Music", "Neoclassical", "Audiobook", "Audio theatre", "Neue Deutsche Welle", "Podcast", "Indie-Rock", "G-Funk", "Dubstep", "Garage Rock", "Psybient"};

    public static CommentFrame a(int i, op2 op2Var) {
        int n = op2Var.n();
        if (op2Var.n() == 1684108385) {
            op2Var.Q(8);
            String y = op2Var.y(n - 16);
            return new CommentFrame("und", y, y);
        }
        p12.i("MetadataUtil", "Failed to parse comment attribute: " + zi.a(i));
        return null;
    }

    public static ApicFrame b(op2 op2Var) {
        int n = op2Var.n();
        if (op2Var.n() == 1684108385) {
            int b = zi.b(op2Var.n());
            String str = b == 13 ? "image/jpeg" : b == 14 ? "image/png" : null;
            if (str == null) {
                p12.i("MetadataUtil", "Unrecognized cover art flags: " + b);
                return null;
            }
            op2Var.Q(4);
            int i = n - 16;
            byte[] bArr = new byte[i];
            op2Var.j(bArr, 0, i);
            return new ApicFrame(str, null, 3, bArr);
        }
        p12.i("MetadataUtil", "Failed to parse cover art attribute");
        return null;
    }

    public static Metadata.Entry c(op2 op2Var) {
        int e = op2Var.e() + op2Var.n();
        int n = op2Var.n();
        int i = (n >> 24) & 255;
        try {
            if (i == 169 || i == 253) {
                int i2 = 16777215 & n;
                if (i2 == 6516084) {
                    return a(n, op2Var);
                }
                if (i2 == 7233901 || i2 == 7631467) {
                    return h(n, "TIT2", op2Var);
                }
                if (i2 == 6516589 || i2 == 7828084) {
                    return h(n, "TCOM", op2Var);
                }
                if (i2 == 6578553) {
                    return h(n, "TDRC", op2Var);
                }
                if (i2 == 4280916) {
                    return h(n, "TPE1", op2Var);
                }
                if (i2 == 7630703) {
                    return h(n, "TSSE", op2Var);
                }
                if (i2 == 6384738) {
                    return h(n, "TALB", op2Var);
                }
                if (i2 == 7108978) {
                    return h(n, "USLT", op2Var);
                }
                if (i2 == 6776174) {
                    return h(n, "TCON", op2Var);
                }
                if (i2 == 6779504) {
                    return h(n, "TIT1", op2Var);
                }
            } else if (n == 1735291493) {
                return g(op2Var);
            } else {
                if (n == 1684632427) {
                    return d(n, "TPOS", op2Var);
                }
                if (n == 1953655662) {
                    return d(n, "TRCK", op2Var);
                }
                if (n == 1953329263) {
                    return i(n, "TBPM", op2Var, true, false);
                }
                if (n == 1668311404) {
                    return i(n, "TCMP", op2Var, true, true);
                }
                if (n == 1668249202) {
                    return b(op2Var);
                }
                if (n == 1631670868) {
                    return h(n, "TPE2", op2Var);
                }
                if (n == 1936682605) {
                    return h(n, "TSOT", op2Var);
                }
                if (n == 1936679276) {
                    return h(n, "TSO2", op2Var);
                }
                if (n == 1936679282) {
                    return h(n, "TSOA", op2Var);
                }
                if (n == 1936679265) {
                    return h(n, "TSOP", op2Var);
                }
                if (n == 1936679791) {
                    return h(n, "TSOC", op2Var);
                }
                if (n == 1920233063) {
                    return i(n, "ITUNESADVISORY", op2Var, false, false);
                }
                if (n == 1885823344) {
                    return i(n, "ITUNESGAPLESS", op2Var, false, true);
                }
                if (n == 1936683886) {
                    return h(n, "TVSHOWSORT", op2Var);
                }
                if (n == 1953919848) {
                    return h(n, "TVSHOW", op2Var);
                }
                if (n == 757935405) {
                    return e(op2Var, e);
                }
            }
            p12.b("MetadataUtil", "Skipped unknown metadata entry: " + zi.a(n));
            return null;
        } finally {
            op2Var.P(e);
        }
    }

    public static TextInformationFrame d(int i, String str, op2 op2Var) {
        int n = op2Var.n();
        if (op2Var.n() == 1684108385 && n >= 22) {
            op2Var.Q(10);
            int J = op2Var.J();
            if (J > 0) {
                String str2 = "" + J;
                int J2 = op2Var.J();
                if (J2 > 0) {
                    str2 = str2 + "/" + J2;
                }
                return new TextInformationFrame(str, null, str2);
            }
        }
        p12.i("MetadataUtil", "Failed to parse index/count attribute: " + zi.a(i));
        return null;
    }

    public static Id3Frame e(op2 op2Var, int i) {
        int i2 = -1;
        int i3 = -1;
        String str = null;
        String str2 = null;
        while (op2Var.e() < i) {
            int e = op2Var.e();
            int n = op2Var.n();
            int n2 = op2Var.n();
            op2Var.Q(4);
            if (n2 == 1835360622) {
                str = op2Var.y(n - 12);
            } else if (n2 == 1851878757) {
                str2 = op2Var.y(n - 12);
            } else {
                if (n2 == 1684108385) {
                    i2 = e;
                    i3 = n;
                }
                op2Var.Q(n - 12);
            }
        }
        if (str == null || str2 == null || i2 == -1) {
            return null;
        }
        op2Var.P(i2);
        op2Var.Q(16);
        return new InternalFrame(str, str2, op2Var.y(i3 - 16));
    }

    public static MdtaMetadataEntry f(op2 op2Var, int i, String str) {
        while (true) {
            int e = op2Var.e();
            if (e >= i) {
                return null;
            }
            int n = op2Var.n();
            if (op2Var.n() == 1684108385) {
                int n2 = op2Var.n();
                int n3 = op2Var.n();
                int i2 = n - 16;
                byte[] bArr = new byte[i2];
                op2Var.j(bArr, 0, i2);
                return new MdtaMetadataEntry(str, bArr, n3, n2);
            }
            op2Var.P(e + n);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x001c  */
    /* JADX WARN: Removed duplicated region for block: B:9:0x0014  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static androidx.media3.extractor.metadata.id3.TextInformationFrame g(defpackage.op2 r3) {
        /*
            int r3 = j(r3)
            r0 = 0
            if (r3 <= 0) goto L11
            java.lang.String[] r1 = defpackage.q82.a
            int r2 = r1.length
            if (r3 > r2) goto L11
            int r3 = r3 + (-1)
            r3 = r1[r3]
            goto L12
        L11:
            r3 = r0
        L12:
            if (r3 == 0) goto L1c
            androidx.media3.extractor.metadata.id3.TextInformationFrame r1 = new androidx.media3.extractor.metadata.id3.TextInformationFrame
            java.lang.String r2 = "TCON"
            r1.<init>(r2, r0, r3)
            return r1
        L1c:
            java.lang.String r3 = "MetadataUtil"
            java.lang.String r1 = "Failed to parse standard genre code"
            defpackage.p12.i(r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.q82.g(op2):androidx.media3.extractor.metadata.id3.TextInformationFrame");
    }

    public static TextInformationFrame h(int i, String str, op2 op2Var) {
        int n = op2Var.n();
        if (op2Var.n() == 1684108385) {
            op2Var.Q(8);
            return new TextInformationFrame(str, null, op2Var.y(n - 16));
        }
        p12.i("MetadataUtil", "Failed to parse text attribute: " + zi.a(i));
        return null;
    }

    public static Id3Frame i(int i, String str, op2 op2Var, boolean z, boolean z2) {
        int j = j(op2Var);
        if (z2) {
            j = Math.min(1, j);
        }
        if (j >= 0) {
            if (z) {
                return new TextInformationFrame(str, null, Integer.toString(j));
            }
            return new CommentFrame("und", str, Integer.toString(j));
        }
        p12.i("MetadataUtil", "Failed to parse uint8 attribute: " + zi.a(i));
        return null;
    }

    public static int j(op2 op2Var) {
        op2Var.Q(4);
        if (op2Var.n() == 1684108385) {
            op2Var.Q(8);
            return op2Var.D();
        }
        p12.i("MetadataUtil", "Failed to parse uint8 attribute value");
        return -1;
    }

    public static void k(int i, qe1 qe1Var, j.b bVar) {
        if (i == 1 && qe1Var.a()) {
            bVar.N(qe1Var.a).O(qe1Var.b);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:4:0x000b, code lost:
        if (r6 != null) goto L5;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void l(int r5, androidx.media3.common.Metadata r6, androidx.media3.common.Metadata r7, androidx.media3.common.j.b r8, androidx.media3.common.Metadata... r9) {
        /*
            androidx.media3.common.Metadata r0 = new androidx.media3.common.Metadata
            r1 = 0
            androidx.media3.common.Metadata$Entry[] r2 = new androidx.media3.common.Metadata.Entry[r1]
            r0.<init>(r2)
            r2 = 1
            if (r5 != r2) goto Le
            if (r6 == 0) goto L3c
            goto L3d
        Le:
            r6 = 2
            if (r5 != r6) goto L3c
            if (r7 == 0) goto L3c
            r5 = r1
        L14:
            int r6 = r7.d()
            if (r5 >= r6) goto L3c
            androidx.media3.common.Metadata$Entry r6 = r7.c(r5)
            boolean r3 = r6 instanceof androidx.media3.extractor.metadata.mp4.MdtaMetadataEntry
            if (r3 == 0) goto L39
            androidx.media3.extractor.metadata.mp4.MdtaMetadataEntry r6 = (androidx.media3.extractor.metadata.mp4.MdtaMetadataEntry) r6
            java.lang.String r3 = r6.a
            java.lang.String r4 = "com.android.capture.fps"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L39
            androidx.media3.common.Metadata r5 = new androidx.media3.common.Metadata
            androidx.media3.common.Metadata$Entry[] r7 = new androidx.media3.common.Metadata.Entry[r2]
            r7[r1] = r6
            r5.<init>(r7)
            r6 = r5
            goto L3d
        L39:
            int r5 = r5 + 1
            goto L14
        L3c:
            r6 = r0
        L3d:
            int r5 = r9.length
        L3e:
            if (r1 >= r5) goto L49
            r7 = r9[r1]
            androidx.media3.common.Metadata r6 = r6.b(r7)
            int r1 = r1 + 1
            goto L3e
        L49:
            int r5 = r6.d()
            if (r5 <= 0) goto L52
            r8.X(r6)
        L52:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.q82.l(int, androidx.media3.common.Metadata, androidx.media3.common.Metadata, androidx.media3.common.j$b, androidx.media3.common.Metadata[]):void");
    }
}
