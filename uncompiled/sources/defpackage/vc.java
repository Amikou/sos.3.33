package defpackage;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import android.util.Log;

/* compiled from: AndroidInjection.java */
/* renamed from: vc  reason: default package */
/* loaded from: classes2.dex */
public final class vc {
    public static xj1 a(Fragment fragment) {
        Fragment fragment2 = fragment;
        do {
            fragment2 = fragment2.getParentFragment();
            if (fragment2 == null) {
                Activity activity = fragment.getActivity();
                if (activity instanceof xj1) {
                    return (xj1) activity;
                }
                if (activity.getApplication() instanceof xj1) {
                    return (xj1) activity.getApplication();
                }
                throw new IllegalArgumentException(String.format("No injector was found for %s", fragment.getClass().getCanonicalName()));
            }
        } while (!(fragment2 instanceof xj1));
        return (xj1) fragment2;
    }

    public static void b(Activity activity) {
        cu2.c(activity, "activity");
        Application application = activity.getApplication();
        if (application instanceof xj1) {
            g(activity, (xj1) application);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), xj1.class.getCanonicalName()));
    }

    public static void c(Fragment fragment) {
        cu2.c(fragment, "fragment");
        xj1 a = a(fragment);
        if (Log.isLoggable("dagger.android", 3)) {
            String.format("An injector for %s was found in %s", fragment.getClass().getCanonicalName(), a.getClass().getCanonicalName());
        }
        g(fragment, a);
    }

    public static void d(Service service) {
        cu2.c(service, "service");
        Application application = service.getApplication();
        if (application instanceof xj1) {
            g(service, (xj1) application);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), xj1.class.getCanonicalName()));
    }

    public static void e(BroadcastReceiver broadcastReceiver, Context context) {
        cu2.c(broadcastReceiver, "broadcastReceiver");
        cu2.c(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof xj1) {
            g(broadcastReceiver, (xj1) application);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), xj1.class.getCanonicalName()));
    }

    public static void f(ContentProvider contentProvider) {
        cu2.c(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof xj1) {
            g(contentProvider, (xj1) application);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), xj1.class.getCanonicalName()));
    }

    public static void g(Object obj, xj1 xj1Var) {
        wc<Object> a = xj1Var.a();
        cu2.d(a, "%s.androidInjector() returned null", xj1Var.getClass());
        a.a(obj);
    }
}
