package defpackage;

import defpackage.bd3;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/* compiled from: NewThreadWorker.java */
/* renamed from: nf2  reason: default package */
/* loaded from: classes2.dex */
public class nf2 extends bd3.a {
    public final ScheduledExecutorService a;
    public volatile boolean b;

    public nf2(ThreadFactory threadFactory) {
        this.a = dd3.a(threadFactory);
    }

    @Override // defpackage.xp0
    public void a() {
        if (this.b) {
            return;
        }
        this.b = true;
        this.a.shutdownNow();
    }
}
