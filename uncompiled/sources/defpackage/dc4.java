package defpackage;

import androidx.media3.common.util.b;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: TsDurationReader.java */
/* renamed from: dc4  reason: default package */
/* loaded from: classes.dex */
public final class dc4 {
    public final int a;
    public boolean d;
    public boolean e;
    public boolean f;
    public final h64 b = new h64(0);
    public long g = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public long h = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public long i = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public final op2 c = new op2();

    public dc4(int i) {
        this.a = i;
    }

    public final int a(q11 q11Var) {
        this.c.M(b.f);
        this.d = true;
        q11Var.j();
        return 0;
    }

    public long b() {
        return this.i;
    }

    public h64 c() {
        return this.b;
    }

    public boolean d() {
        return this.d;
    }

    public int e(q11 q11Var, ot2 ot2Var, int i) throws IOException {
        if (i <= 0) {
            return a(q11Var);
        }
        if (!this.f) {
            return h(q11Var, ot2Var, i);
        }
        if (this.h == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return a(q11Var);
        }
        if (!this.e) {
            return f(q11Var, ot2Var, i);
        }
        long j = this.g;
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return a(q11Var);
        }
        long b = this.b.b(this.h) - this.b.b(j);
        this.i = b;
        if (b < 0) {
            p12.i("TsDurationReader", "Invalid duration: " + this.i + ". Using TIME_UNSET instead.");
            this.i = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return a(q11Var);
    }

    public final int f(q11 q11Var, ot2 ot2Var, int i) throws IOException {
        int min = (int) Math.min(this.a, q11Var.getLength());
        long j = 0;
        if (q11Var.getPosition() != j) {
            ot2Var.a = j;
            return 1;
        }
        this.c.L(min);
        q11Var.j();
        q11Var.n(this.c.d(), 0, min);
        this.g = g(this.c, i);
        this.e = true;
        return 0;
    }

    public final long g(op2 op2Var, int i) {
        int f = op2Var.f();
        for (int e = op2Var.e(); e < f; e++) {
            if (op2Var.d()[e] == 71) {
                long c = hc4.c(op2Var, e, i);
                if (c != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return c;
                }
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public final int h(q11 q11Var, ot2 ot2Var, int i) throws IOException {
        long length = q11Var.getLength();
        int min = (int) Math.min(this.a, length);
        long j = length - min;
        if (q11Var.getPosition() != j) {
            ot2Var.a = j;
            return 1;
        }
        this.c.L(min);
        q11Var.j();
        q11Var.n(this.c.d(), 0, min);
        this.h = i(this.c, i);
        this.f = true;
        return 0;
    }

    public final long i(op2 op2Var, int i) {
        int e = op2Var.e();
        int f = op2Var.f();
        for (int i2 = f - 188; i2 >= e; i2--) {
            if (hc4.b(op2Var.d(), e, f, i2)) {
                long c = hc4.c(op2Var, i2, i);
                if (c != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return c;
                }
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
