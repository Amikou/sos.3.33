package defpackage;

import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: z55  reason: default package */
/* loaded from: classes.dex */
public interface z55 {
    public static final z55 X = new i65();
    public static final z55 Y = new t55();
    public static final z55 Z = new v45("continue");
    public static final z55 a0 = new v45("break");
    public static final z55 b0 = new v45("return");
    public static final z55 c0 = new s45(Boolean.TRUE);
    public static final z55 d0 = new s45(Boolean.FALSE);
    public static final z55 e0 = new f65("");

    Double b();

    Boolean c();

    Iterator<z55> i();

    z55 m();

    z55 n(String str, wk5 wk5Var, List<z55> list);

    String zzc();
}
