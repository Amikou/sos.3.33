package defpackage;

/* compiled from: TimeModule_EventClockFactory.java */
/* renamed from: b64  reason: default package */
/* loaded from: classes.dex */
public final class b64 implements z11<qz> {

    /* compiled from: TimeModule_EventClockFactory.java */
    /* renamed from: b64$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final b64 a = new b64();
    }

    public static b64 a() {
        return a.a;
    }

    public static qz b() {
        return (qz) yt2.c(a64.a(), "Cannot return null from a non-@Nullable @Provides method");
    }

    @Override // defpackage.ew2
    /* renamed from: c */
    public qz get() {
        return b();
    }
}
