package defpackage;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.gson.GsonToken;

/* compiled from: AssetsAllTokenDataSource.kt */
/* renamed from: ni  reason: default package */
/* loaded from: classes2.dex */
public final class ni implements ol1 {
    public final Context a;

    /* compiled from: AssetsAllTokenDataSource.kt */
    /* renamed from: ni$a */
    /* loaded from: classes2.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[TokenType.values().length];
            iArr[TokenType.BEP_20.ordinal()] = 1;
            iArr[TokenType.BEP_20_TEST.ordinal()] = 2;
            iArr[TokenType.ERC_20.ordinal()] = 3;
            iArr[TokenType.ERC_20_TEST.ordinal()] = 4;
            iArr[TokenType.POLYGON.ordinal()] = 5;
            iArr[TokenType.POLYGON_TEST.ordinal()] = 6;
            iArr[TokenType.AVALANCHE_C.ordinal()] = 7;
            iArr[TokenType.AVALANCHE_FUJI_TEST.ordinal()] = 8;
            a = iArr;
        }
    }

    /* compiled from: AssetsAllTokenDataSource.kt */
    /* renamed from: ni$b */
    /* loaded from: classes2.dex */
    public static final class b extends TypeToken<List<? extends GsonToken>> {
    }

    /* compiled from: AssetsAllTokenDataSource.kt */
    /* renamed from: ni$c */
    /* loaded from: classes2.dex */
    public static final class c extends TypeToken<List<? extends GsonToken>> {
    }

    public ni(Context context) {
        fs1.f(context, "context");
        this.a = context;
    }

    @Override // defpackage.ol1
    public List<IToken> a(TokenType tokenType) {
        fs1.f(tokenType, "type");
        Object fromJson = new Gson().fromJson(d(tokenType), new b().getType());
        fs1.e(fromJson, "Gson().fromJson(readJson…st<GsonToken>>() {}.type)");
        return (List) fromJson;
    }

    public final String b(TokenType tokenType) {
        String str;
        switch (a.a[tokenType.ordinal()]) {
            case 1:
            case 2:
                str = "bep_20";
                break;
            case 3:
            case 4:
                str = "erc_20";
                break;
            case 5:
            case 6:
                str = "matic_20";
                break;
            case 7:
            case 8:
                str = "avalanche_c";
                break;
            default:
                throw new NoWhenBranchMatchedException();
        }
        String l = fs1.l(str, "_tokens.json");
        Boolean bool = zr.b;
        fs1.e(bool, "IS_TEST_NET");
        return fs1.l(bool.booleanValue() ? "test/" : "coin/", l);
    }

    public List<GsonToken> c(TokenType tokenType) {
        fs1.f(tokenType, "type");
        Object fromJson = new Gson().fromJson(d(tokenType), new c().getType());
        fs1.e(fromJson, "Gson().fromJson(readJson…st<GsonToken>>() {}.type)");
        return (List) fromJson;
    }

    public final String d(TokenType tokenType) {
        try {
            InputStream open = this.a.getAssets().open(b(tokenType));
            fs1.e(open, "context.assets.open(getFileDirectory(type))");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return new String(bArr, by.a);
        } catch (IOException e) {
            throw e;
        }
    }
}
