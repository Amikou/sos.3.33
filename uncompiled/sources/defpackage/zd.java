package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;
import java.util.List;

/* compiled from: AnimatedImageResultBuilder.java */
/* renamed from: zd  reason: default package */
/* loaded from: classes.dex */
public class zd {
    public final td a;
    public a<Bitmap> b;
    public List<a<Bitmap>> c;
    public int d;
    public pq e;

    public zd(td tdVar) {
        this.a = tdVar;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.util.List<com.facebook.common.references.a<android.graphics.Bitmap>>, com.facebook.common.references.a<android.graphics.Bitmap>] */
    public yd a() {
        try {
            return new yd(this);
        } finally {
            a.g(this.b);
            this.b = null;
            a.h(this.c);
            this.c = null;
        }
    }

    public pq b() {
        return this.e;
    }

    public List<a<Bitmap>> c() {
        return a.f(this.c);
    }

    public int d() {
        return this.d;
    }

    public td e() {
        return this.a;
    }

    public a<Bitmap> f() {
        return a.e(this.b);
    }

    public zd g(pq pqVar) {
        this.e = pqVar;
        return this;
    }

    public zd h(List<a<Bitmap>> list) {
        this.c = a.f(list);
        return this;
    }

    public zd i(int i) {
        this.d = i;
        return this;
    }

    public zd j(a<Bitmap> aVar) {
        this.b = a.e(aVar);
        return this;
    }
}
