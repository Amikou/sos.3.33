package defpackage;

import android.content.Context;
import android.os.Build;
import com.google.crypto.tink.KeyTemplate;
import com.google.crypto.tink.h;
import com.google.crypto.tink.i;
import com.google.crypto.tink.j;
import com.google.crypto.tink.k;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import defpackage.zc;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.ProviderException;

/* compiled from: AndroidKeysetManager.java */
/* renamed from: xc  reason: default package */
/* loaded from: classes2.dex */
public final class xc {
    public static final String c = "xc";
    public final com.google.crypto.tink.a a;
    public i b;

    /* compiled from: AndroidKeysetManager.java */
    /* renamed from: xc$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public j a = null;
        public k b = null;
        public String c = null;
        public com.google.crypto.tink.a d = null;
        public boolean e = true;
        public KeyTemplate f = null;
        public KeyStore g = null;
        public i h;

        public synchronized xc d() throws GeneralSecurityException, IOException {
            if (this.c != null) {
                this.d = g();
            }
            this.h = f();
            return new xc(this);
        }

        public final i e() throws GeneralSecurityException, IOException {
            com.google.crypto.tink.a aVar = this.d;
            if (aVar != null) {
                try {
                    return i.j(h.j(this.a, aVar));
                } catch (InvalidProtocolBufferException | GeneralSecurityException unused) {
                    String unused2 = xc.c;
                }
            }
            return i.j(com.google.crypto.tink.b.a(this.a));
        }

        public final i f() throws GeneralSecurityException, IOException {
            try {
                return e();
            } catch (FileNotFoundException unused) {
                String unused2 = xc.c;
                if (this.f != null) {
                    i a = i.i().a(this.f);
                    i h = a.h(a.c().g().I(0).I());
                    if (this.d != null) {
                        h.c().k(this.b, this.d);
                    } else {
                        com.google.crypto.tink.b.b(h.c(), this.b);
                    }
                    return h;
                }
                throw new GeneralSecurityException("cannot read or generate keyset");
            }
        }

        public final com.google.crypto.tink.a g() throws GeneralSecurityException {
            zc zcVar;
            if (!xc.d()) {
                String unused = xc.c;
                return null;
            }
            if (this.g != null) {
                zcVar = new zc.b().b(this.g).a();
            } else {
                zcVar = new zc();
            }
            boolean e = zcVar.e(this.c);
            if (!e) {
                try {
                    zc.d(this.c);
                } catch (GeneralSecurityException | ProviderException unused2) {
                    String unused3 = xc.c;
                    return null;
                }
            }
            try {
                return zcVar.b(this.c);
            } catch (GeneralSecurityException | ProviderException e2) {
                if (!e) {
                    String unused4 = xc.c;
                    return null;
                }
                throw new KeyStoreException(String.format("the master key %s exists but is unusable", this.c), e2);
            }
        }

        public b h(KeyTemplate keyTemplate) {
            this.f = keyTemplate;
            return this;
        }

        public b i(String str) {
            if (str.startsWith("android-keystore://")) {
                if (this.e) {
                    this.c = str;
                    return this;
                }
                throw new IllegalArgumentException("cannot call withMasterKeyUri() after calling doNotUseKeystore()");
            }
            throw new IllegalArgumentException("key URI must start with android-keystore://");
        }

        public b j(Context context, String str, String str2) throws IOException {
            if (context != null) {
                if (str != null) {
                    this.a = new xn3(context, str, str2);
                    this.b = new yn3(context, str, str2);
                    return this;
                }
                throw new IllegalArgumentException("need a keyset name");
            }
            throw new IllegalArgumentException("need an Android context");
        }
    }

    public static boolean d() {
        return Build.VERSION.SDK_INT >= 23;
    }

    public synchronized h c() throws GeneralSecurityException {
        return this.b.c();
    }

    public xc(b bVar) throws GeneralSecurityException, IOException {
        k unused = bVar.b;
        this.a = bVar.d;
        this.b = bVar.h;
    }
}
