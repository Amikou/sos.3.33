package defpackage;

import android.view.View;
import com.google.android.flexbox.a;
import java.util.List;

/* compiled from: FlexContainer.java */
/* renamed from: z61  reason: default package */
/* loaded from: classes.dex */
public interface z61 {
    void b(View view, int i, int i2, a aVar);

    void c(a aVar);

    View d(int i);

    int e(int i, int i2, int i3);

    void f(int i, View view);

    int getAlignContent();

    int getAlignItems();

    int getFlexDirection();

    int getFlexItemCount();

    List<a> getFlexLinesInternal();

    int getFlexWrap();

    int getLargestMainSize();

    int getMaxLine();

    int getPaddingBottom();

    int getPaddingEnd();

    int getPaddingLeft();

    int getPaddingRight();

    int getPaddingStart();

    int getPaddingTop();

    int getSumOfCrossSize();

    View h(int i);

    int i(View view, int i, int i2);

    int j(int i, int i2, int i3);

    boolean k();

    int l(View view);

    void setFlexLines(List<a> list);
}
