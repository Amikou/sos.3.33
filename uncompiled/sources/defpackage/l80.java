package defpackage;

/* compiled from: ControllerListener2.java */
/* renamed from: l80  reason: default package */
/* loaded from: classes.dex */
public interface l80<INFO> {

    /* compiled from: ControllerListener2.java */
    /* renamed from: l80$a */
    /* loaded from: classes.dex */
    public static class a {
        public Object a;
    }

    void a(String str, INFO info2);

    void b(String str, INFO info2, a aVar);

    void c(String str, a aVar);

    void d(String str, Throwable th, a aVar);

    void e(String str, Object obj, a aVar);

    void f(String str);
}
