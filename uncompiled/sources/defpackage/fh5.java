package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: fh5  reason: default package */
/* loaded from: classes.dex */
public final class fh5 implements Runnable {
    public final /* synthetic */ boolean a;
    public final /* synthetic */ ih5 f0;

    public fh5(ih5 ih5Var, boolean z) {
        this.f0 = ih5Var;
        this.a = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        fw5 fw5Var;
        fw5Var = this.f0.a;
        fw5Var.A(this.a);
    }
}
