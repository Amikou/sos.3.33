package defpackage;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;
import kotlinx.coroutines.d;

/* compiled from: Executors.kt */
/* renamed from: ty0  reason: default package */
/* loaded from: classes2.dex */
public abstract class ty0 extends ExecutorCoroutineDispatcher implements wl0 {
    public boolean f0;

    public final void M() {
        this.f0 = u40.a(l());
    }

    public final ScheduledFuture<?> N(Runnable runnable, CoroutineContext coroutineContext, long j) {
        try {
            Executor l = l();
            ScheduledExecutorService scheduledExecutorService = l instanceof ScheduledExecutorService ? (ScheduledExecutorService) l : null;
            if (scheduledExecutorService == null) {
                return null;
            }
            return scheduledExecutorService.schedule(runnable, j, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            m(coroutineContext, e);
            return null;
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Executor l = l();
        ExecutorService executorService = l instanceof ExecutorService ? (ExecutorService) l : null;
        if (executorService == null) {
            return;
        }
        executorService.shutdown();
    }

    public boolean equals(Object obj) {
        return (obj instanceof ty0) && ((ty0) obj).l() == l();
    }

    @Override // defpackage.wl0
    public void f(long j, ov<? super te4> ovVar) {
        ScheduledFuture<?> N = this.f0 ? N(new t83(this, ovVar), ovVar.getContext(), j) : null;
        if (N != null) {
            xt1.h(ovVar, N);
        } else {
            d.k0.f(j, ovVar);
        }
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        try {
            Executor l = l();
            n5.a();
            l.execute(runnable);
        } catch (RejectedExecutionException e) {
            n5.a();
            m(coroutineContext, e);
            tp0 tp0Var = tp0.a;
            tp0.b().h(coroutineContext, runnable);
        }
    }

    public int hashCode() {
        return System.identityHashCode(l());
    }

    public final void m(CoroutineContext coroutineContext, RejectedExecutionException rejectedExecutionException) {
        xt1.d(coroutineContext, ny0.a("The task was rejected", rejectedExecutionException));
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        return l().toString();
    }
}
