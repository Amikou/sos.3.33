package defpackage;

import android.text.TextUtils;
import android.util.Log;
import java.net.UnknownHostException;

/* compiled from: Log.java */
/* renamed from: p12  reason: default package */
/* loaded from: classes.dex */
public final class p12 {
    public static int b = 0;
    public static boolean c = true;
    public static final Object a = new Object();
    public static a d = a.a;

    /* compiled from: Log.java */
    /* renamed from: p12$a */
    /* loaded from: classes.dex */
    public interface a {
        public static final a a = new C0260a();

        /* compiled from: Log.java */
        /* renamed from: p12$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0260a implements a {
            @Override // defpackage.p12.a
            public void a(String str, String str2) {
            }

            @Override // defpackage.p12.a
            public void b(String str, String str2) {
            }

            @Override // defpackage.p12.a
            public void c(String str, String str2) {
            }

            @Override // defpackage.p12.a
            public void d(String str, String str2) {
            }
        }

        void a(String str, String str2);

        void b(String str, String str2);

        void c(String str, String str2);

        void d(String str, String str2);
    }

    public static String a(String str, Throwable th) {
        String e = e(th);
        if (TextUtils.isEmpty(e)) {
            return str;
        }
        return str + "\n  " + e.replace("\n", "\n  ") + '\n';
    }

    public static void b(String str, String str2) {
        synchronized (a) {
            if (b == 0) {
                d.c(str, str2);
            }
        }
    }

    public static void c(String str, String str2) {
        synchronized (a) {
            if (b <= 3) {
                d.b(str, str2);
            }
        }
    }

    public static void d(String str, String str2, Throwable th) {
        c(str, a(str2, th));
    }

    public static String e(Throwable th) {
        synchronized (a) {
            if (th == null) {
                return null;
            }
            if (h(th)) {
                return "UnknownHostException (no network)";
            }
            if (!c) {
                return th.getMessage();
            }
            return Log.getStackTraceString(th).trim().replace("\t", "    ");
        }
    }

    public static void f(String str, String str2) {
        synchronized (a) {
            if (b <= 1) {
                d.d(str, str2);
            }
        }
    }

    public static void g(String str, String str2, Throwable th) {
        f(str, a(str2, th));
    }

    public static boolean h(Throwable th) {
        while (th != null) {
            if (th instanceof UnknownHostException) {
                return true;
            }
            th = th.getCause();
        }
        return false;
    }

    public static void i(String str, String str2) {
        synchronized (a) {
            if (b <= 2) {
                d.a(str, str2);
            }
        }
    }

    public static void j(String str, String str2, Throwable th) {
        i(str, a(str2, th));
    }
}
