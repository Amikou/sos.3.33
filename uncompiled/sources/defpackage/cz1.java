package defpackage;

import com.google.protobuf.ByteString;
import java.util.List;

/* compiled from: LazyStringList.java */
/* renamed from: cz1  reason: default package */
/* loaded from: classes2.dex */
public interface cz1 extends dw2 {
    void W(ByteString byteString);

    ByteString e1(int i);

    Object j(int i);

    List<?> r();

    cz1 x();
}
