package defpackage;

import androidx.paging.multicast.ChannelManager;
import java.util.Collection;

/* compiled from: ChannelManager.kt */
/* renamed from: rr  reason: default package */
/* loaded from: classes.dex */
public interface rr<T> {

    /* compiled from: ChannelManager.kt */
    /* renamed from: rr$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static <T> boolean a(rr<T> rrVar) {
            return rrVar.b().isEmpty();
        }
    }

    void a(ChannelManager.b.AbstractC0053b.c<T> cVar);

    Collection<ChannelManager.b.AbstractC0053b.c<T>> b();

    boolean isEmpty();
}
