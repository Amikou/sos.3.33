package defpackage;

import java.util.HashSet;

/* compiled from: MediaLibraryInfo.java */
/* renamed from: f62  reason: default package */
/* loaded from: classes.dex */
public final class f62 {
    public static final HashSet<String> a = new HashSet<>();
    public static String b = "media3.common";

    public static synchronized void a(String str) {
        synchronized (f62.class) {
            if (a.add(str)) {
                b += ", " + str;
            }
        }
    }

    public static synchronized String b() {
        String str;
        synchronized (f62.class) {
            str = b;
        }
        return str;
    }
}
