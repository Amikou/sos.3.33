package defpackage;

import android.os.Bundle;
import androidx.lifecycle.l;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* compiled from: LoaderManagerImpl.java */
/* renamed from: b12  reason: default package */
/* loaded from: classes.dex */
public class b12 extends a12 {
    public static boolean c = false;
    public final rz1 a;
    public final b b;

    /* compiled from: LoaderManagerImpl.java */
    /* renamed from: b12$a */
    /* loaded from: classes.dex */
    public static class a<D> extends gb2<D> {
        public final int a;
        public final Bundle b;
        public final z02<D> c;
        public rz1 d;

        public z02<D> a(boolean z) {
            if (b12.c) {
                StringBuilder sb = new StringBuilder();
                sb.append("  Destroying: ");
                sb.append(this);
            }
            throw null;
        }

        public void b(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.a);
            printWriter.print(" mArgs=");
            printWriter.println(this.b);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.c);
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("  ");
            throw null;
        }

        public void c() {
        }

        @Override // androidx.lifecycle.LiveData
        public void onActive() {
            if (b12.c) {
                StringBuilder sb = new StringBuilder();
                sb.append("  Starting: ");
                sb.append(this);
            }
            throw null;
        }

        @Override // androidx.lifecycle.LiveData
        public void onInactive() {
            if (b12.c) {
                StringBuilder sb = new StringBuilder();
                sb.append("  Stopping: ");
                sb.append(this);
            }
            throw null;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // androidx.lifecycle.LiveData
        public void removeObserver(tl2<? super D> tl2Var) {
            super.removeObserver(tl2Var);
            this.d = null;
        }

        @Override // defpackage.gb2, androidx.lifecycle.LiveData
        public void setValue(D d) {
            super.setValue(d);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.a);
            sb.append(" : ");
            gf0.a(this.c, sb);
            sb.append("}}");
            return sb.toString();
        }
    }

    /* compiled from: LoaderManagerImpl.java */
    /* renamed from: b12$b */
    /* loaded from: classes.dex */
    public static class b extends dj4 {
        public static final l.b b = new a();
        public lr3<a> a = new lr3<>();

        /* compiled from: LoaderManagerImpl.java */
        /* renamed from: b12$b$a */
        /* loaded from: classes.dex */
        public static class a implements l.b {
            @Override // androidx.lifecycle.l.b
            public <T extends dj4> T a(Class<T> cls) {
                return new b();
            }
        }

        public static b b(gj4 gj4Var) {
            return (b) new l(gj4Var, b).a(b.class);
        }

        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.a.o() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.a.o(); i++) {
                    a p = this.a.p(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.a.j(i));
                    printWriter.print(": ");
                    printWriter.println(p.toString());
                    p.b(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }

        public void c() {
            int o = this.a.o();
            for (int i = 0; i < o; i++) {
                this.a.p(i).c();
            }
        }

        @Override // defpackage.dj4
        public void onCleared() {
            super.onCleared();
            int o = this.a.o();
            for (int i = 0; i < o; i++) {
                this.a.p(i).a(true);
            }
            this.a.b();
        }
    }

    public b12(rz1 rz1Var, gj4 gj4Var) {
        this.a = rz1Var;
        this.b = b.b(gj4Var);
    }

    @Override // defpackage.a12
    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }

    @Override // defpackage.a12
    public void c() {
        this.b.c();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        gf0.a(this.a, sb);
        sb.append("}}");
        return sb.toString();
    }
}
