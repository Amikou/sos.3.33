package defpackage;

import androidx.media3.common.j;

/* compiled from: DecoderReuseEvaluation.java */
/* renamed from: jf0  reason: default package */
/* loaded from: classes.dex */
public final class jf0 {
    public final String a;
    public final j b;
    public final j c;
    public final int d;
    public final int e;

    public jf0(String str, j jVar, j jVar2, int i, int i2) {
        ii.a(i == 0 || i2 == 0);
        this.a = ii.d(str);
        this.b = (j) ii.e(jVar);
        this.c = (j) ii.e(jVar2);
        this.d = i;
        this.e = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || jf0.class != obj.getClass()) {
            return false;
        }
        jf0 jf0Var = (jf0) obj;
        return this.d == jf0Var.d && this.e == jf0Var.e && this.a.equals(jf0Var.a) && this.b.equals(jf0Var.b) && this.c.equals(jf0Var.c);
    }

    public int hashCode() {
        return ((((((((527 + this.d) * 31) + this.e) * 31) + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }
}
