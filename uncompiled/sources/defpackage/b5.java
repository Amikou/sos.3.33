package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.collections.State;

/* compiled from: AbstractIterator.kt */
/* renamed from: b5  reason: default package */
/* loaded from: classes2.dex */
public abstract class b5<T> implements Iterator<T>, tw1 {
    public State a = State.NotReady;
    public T f0;

    public abstract void a();

    public final void b() {
        this.a = State.Done;
    }

    public final void c(T t) {
        this.f0 = t;
        this.a = State.Ready;
    }

    public final boolean d() {
        this.a = State.Failed;
        a();
        return this.a == State.Ready;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        State state = this.a;
        if (state != State.Failed) {
            int i = a5.a[state.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    return d();
                }
                return true;
            }
            return false;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            this.a = State.NotReady;
            return this.f0;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
