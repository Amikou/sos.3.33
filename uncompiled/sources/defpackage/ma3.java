package defpackage;

/* renamed from: ma3  reason: default package */
/* loaded from: classes2.dex */
public class ma3 extends g22 {
    public ma3() {
    }

    public ma3(ma3 ma3Var) {
        super(ma3Var);
    }

    @Override // defpackage.qo0
    public int a(byte[] bArr, int i) {
        q();
        ro2.k(this.e, bArr, i);
        ro2.k(this.f, bArr, i + 8);
        ro2.k(this.g, bArr, i + 16);
        ro2.k(this.h, bArr, i + 24);
        ro2.k(this.i, bArr, i + 32);
        ro2.k(this.j, bArr, i + 40);
        reset();
        return 48;
    }

    @Override // defpackage.j72
    public j72 copy() {
        return new ma3(this);
    }

    @Override // defpackage.j72
    public void d(j72 j72Var) {
        super.p((ma3) j72Var);
    }

    @Override // defpackage.qo0
    public String g() {
        return "SHA-384";
    }

    @Override // defpackage.qo0
    public int h() {
        return 48;
    }

    @Override // defpackage.g22, defpackage.qo0
    public void reset() {
        super.reset();
        this.e = -3766243637369397544L;
        this.f = 7105036623409894663L;
        this.g = -7973340178411365097L;
        this.h = 1526699215303891257L;
        this.i = 7436329637833083697L;
        this.j = -8163818279084223215L;
        this.k = -2662702644619276377L;
        this.l = 5167115440072839076L;
    }
}
