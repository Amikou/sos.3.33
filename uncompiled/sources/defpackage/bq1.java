package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: IncludeSafemoonBrandBinding.java */
/* renamed from: bq1  reason: default package */
/* loaded from: classes2.dex */
public final class bq1 {
    public bq1(ConstraintLayout constraintLayout, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3) {
    }

    public static bq1 a(View view) {
        int i = R.id.img_logo;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
        if (appCompatImageView != null) {
            i = R.id.img_safemoon;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.img_safemoon);
            if (appCompatImageView2 != null) {
                i = R.id.img_wallet;
                AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.img_wallet);
                if (appCompatImageView3 != null) {
                    return new bq1((ConstraintLayout) view, appCompatImageView, appCompatImageView2, appCompatImageView3);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
