package defpackage;

/* compiled from: CMC.kt */
/* renamed from: i00  reason: default package */
/* loaded from: classes2.dex */
public final class i00 {
    public final String a;
    public final String b;
    public final Integer c;

    public i00(String str, String str2, Integer num) {
        fs1.f(str2, "symbol");
        this.a = str;
        this.b = str2;
        this.c = num;
    }

    public final Integer a() {
        return this.c;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof i00) {
            i00 i00Var = (i00) obj;
            return fs1.b(this.a, i00Var.a) && fs1.b(this.b, i00Var.b) && fs1.b(this.c, i00Var.c);
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = (((str == null ? 0 : str.hashCode()) * 31) + this.b.hashCode()) * 31;
        Integer num = this.c;
        return hashCode + (num != null ? num.hashCode() : 0);
    }

    public String toString() {
        return "CoinIconParams(url=" + ((Object) this.a) + ", symbol=" + this.b + ", imgResId=" + this.c + ')';
    }
}
