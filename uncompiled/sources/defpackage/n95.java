package defpackage;

import com.google.protobuf.Extension;
import java.util.Collections;
import java.util.HashMap;

/* renamed from: n95  reason: default package */
/* loaded from: classes.dex */
public final class n95 {
    public static final n95 a;

    static {
        a();
        a = new n95(true);
    }

    public n95() {
        new HashMap();
    }

    public n95(boolean z) {
        Collections.emptyMap();
    }

    public static Class<?> a() {
        return Extension.class;
    }

    public static n95 b() {
        return k95.b();
    }
}
