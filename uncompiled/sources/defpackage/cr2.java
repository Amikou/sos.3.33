package defpackage;

import android.os.Build;

/* compiled from: PlatformBitmapFactoryProvider.java */
/* renamed from: cr2  reason: default package */
/* loaded from: classes.dex */
public class cr2 {
    public static br2 a(xs2 xs2Var, dr2 dr2Var, a00 a00Var) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            return new ci(xs2Var.b(), a00Var);
        }
        if (i >= 11) {
            return new cl1(new ru0(xs2Var.h()), dr2Var, a00Var);
        }
        return new fg1();
    }
}
