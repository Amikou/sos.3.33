package defpackage;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: NotificationHistoryItemBinding.java */
/* renamed from: pi2  reason: default package */
/* loaded from: classes2.dex */
public final class pi2 {
    public final LinearLayout a;
    public final CheckBox b;
    public final ConstraintLayout c;
    public final TextView d;
    public final TextView e;
    public final TextView f;

    public pi2(LinearLayout linearLayout, CheckBox checkBox, ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, TextView textView, TextView textView2, TextView textView3) {
        this.a = linearLayout;
        this.b = checkBox;
        this.c = constraintLayout;
        this.d = textView;
        this.e = textView2;
        this.f = textView3;
    }

    public static pi2 a(View view) {
        int i = R.id.checkbox;
        CheckBox checkBox = (CheckBox) ai4.a(view, R.id.checkbox);
        if (checkBox != null) {
            i = R.id.itemParent;
            ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.itemParent);
            if (constraintLayout != null) {
                i = R.id.ivArrow;
                ImageView imageView = (ImageView) ai4.a(view, R.id.ivArrow);
                if (imageView != null) {
                    i = R.id.iv_status;
                    ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv_status);
                    if (imageView2 != null) {
                        i = R.id.tvNotificationText;
                        TextView textView = (TextView) ai4.a(view, R.id.tvNotificationText);
                        if (textView != null) {
                            i = R.id.tvNotificationTime;
                            TextView textView2 = (TextView) ai4.a(view, R.id.tvNotificationTime);
                            if (textView2 != null) {
                                i = R.id.tvNotificationTitle;
                                TextView textView3 = (TextView) ai4.a(view, R.id.tvNotificationTitle);
                                if (textView3 != null) {
                                    return new pi2((LinearLayout) view, checkBox, constraintLayout, imageView, imageView2, textView, textView2, textView3);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public LinearLayout b() {
        return this.a;
    }
}
