package defpackage;

import com.google.android.gms.tasks.c;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: az5  reason: default package */
/* loaded from: classes.dex */
public final class az5<TResult> implements l46<TResult> {
    public final Executor a;
    public final Object b = new Object();
    public um2<? super TResult> c;

    public az5(Executor executor, um2<? super TResult> um2Var) {
        this.a = executor;
        this.c = um2Var;
    }

    @Override // defpackage.l46
    public final void c(c<TResult> cVar) {
        if (cVar.p()) {
            synchronized (this.b) {
                if (this.c == null) {
                    return;
                }
                this.a.execute(new xz5(this, cVar));
            }
        }
    }
}
