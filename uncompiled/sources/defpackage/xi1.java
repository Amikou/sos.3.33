package defpackage;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import defpackage.om0;
import defpackage.zl3;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$BooleanRef;

/* compiled from: GsonBuilder.kt */
/* renamed from: xi1  reason: default package */
/* loaded from: classes.dex */
public final class xi1 {

    /* compiled from: GsonBuilder.kt */
    /* renamed from: xi1$a */
    /* loaded from: classes.dex */
    public static final class a<T> implements JsonDeserializer<T> {
        public final /* synthetic */ tc1 a;

        public a(tc1 tc1Var) {
            this.a = tc1Var;
        }

        @Override // com.google.gson.JsonDeserializer
        public final T deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
            tc1 tc1Var = this.a;
            fs1.c(jsonElement, "json");
            fs1.c(type, "type");
            fs1.c(jsonDeserializationContext, "context");
            return (T) tc1Var.invoke(new om0(jsonElement, type, new om0.a(jsonDeserializationContext)));
        }
    }

    /* compiled from: GsonBuilder.kt */
    /* renamed from: xi1$b */
    /* loaded from: classes.dex */
    public static final class b<T> implements JsonSerializer<T> {
        public final /* synthetic */ tc1 a;

        public b(tc1 tc1Var) {
            this.a = tc1Var;
        }

        @Override // com.google.gson.JsonSerializer
        public final JsonElement serialize(T t, Type type, JsonSerializationContext jsonSerializationContext) {
            tc1 tc1Var = this.a;
            fs1.c(t, "src");
            fs1.c(type, "type");
            fs1.c(jsonSerializationContext, "context");
            return (JsonElement) tc1Var.invoke(new zl3(t, type, new zl3.a(jsonSerializationContext)));
        }
    }

    public static final boolean a(ParameterizedType parameterizedType) {
        Type type;
        fs1.g(parameterizedType, "$receiver");
        Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = false;
        Ref$BooleanRef ref$BooleanRef2 = new Ref$BooleanRef();
        ref$BooleanRef2.element = false;
        Ref$BooleanRef ref$BooleanRef3 = new Ref$BooleanRef();
        ref$BooleanRef3.element = false;
        Type rawType = parameterizedType.getRawType();
        if (rawType != null) {
            TypeVariable[] typeParameters = ((Class) rawType).getTypeParameters();
            int i = 0;
            int i2 = 0;
            while (i < typeParameters.length) {
                TypeVariable typeVariable = typeParameters[i];
                int i3 = i2 + 1;
                Type type2 = parameterizedType.getActualTypeArguments()[i2];
                if (type2 instanceof WildcardType) {
                    Type[] bounds = typeVariable.getBounds();
                    int i4 = 0;
                    while (true) {
                        if (i4 >= bounds.length) {
                            type = null;
                            break;
                        }
                        type = bounds[i4];
                        if (ai.p(((WildcardType) type2).getUpperBounds(), type)) {
                            break;
                        }
                        i4++;
                    }
                    if (type != null) {
                        if (fs1.b(type, Object.class)) {
                            ref$BooleanRef.element = true;
                        } else {
                            ref$BooleanRef2.element = true;
                        }
                    } else {
                        ref$BooleanRef3.element = true;
                    }
                } else {
                    ref$BooleanRef3.element = true;
                }
                i++;
                i2 = i3;
            }
            boolean z = ref$BooleanRef.element;
            if (!z || !ref$BooleanRef3.element) {
                return z || (ref$BooleanRef2.element && !ref$BooleanRef3.element);
            }
            throw new IllegalArgumentException("Either none or all type parameters can be wildcard in " + parameterizedType);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Class<*>");
    }

    public static final <T> JsonDeserializer<T> b(tc1<? super om0, ? extends T> tc1Var) {
        fs1.g(tc1Var, "deserializer");
        return new a(tc1Var);
    }

    public static final <T> JsonSerializer<T> c(tc1<? super zl3<T>, ? extends JsonElement> tc1Var) {
        fs1.g(tc1Var, "serializer");
        return new b(tc1Var);
    }

    public static final Type d(Type type) {
        fs1.g(type, "type");
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            ArrayList<Type> arrayList = new ArrayList(actualTypeArguments.length);
            for (Type type2 : actualTypeArguments) {
                if (type2 instanceof WildcardType) {
                    type2 = ((WildcardType) type2).getUpperBounds()[0];
                }
                arrayList.add(type2);
            }
            ArrayList arrayList2 = new ArrayList(c20.q(arrayList, 10));
            for (Type type3 : arrayList) {
                fs1.c(type3, "it");
                arrayList2.add(d(type3));
            }
            Object[] array = arrayList2.toArray(new Type[arrayList2.size()]);
            if (array != null) {
                Type[] typeArr = (Type[]) array;
                Type type4 = TypeToken.getParameterized(parameterizedType.getRawType(), (Type[]) Arrays.copyOf(typeArr, typeArr.length)).getType();
                fs1.c(type4, "TypeToken.getParameteriz…rawType, *arguments).type");
                return type4;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        return type;
    }
}
