package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: lf5  reason: default package */
/* loaded from: classes.dex */
public class lf5 extends Handler {
    public lf5(Looper looper) {
        super(looper);
    }

    public lf5(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
