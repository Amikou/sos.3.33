package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: xl  reason: default package */
/* loaded from: classes2.dex */
public interface xl {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;
    public static final i g;
    public static final i h;
    public static final i i;
    public static final i j;
    public static final i k;
    public static final i l;
    public static final i m;
    public static final i n;
    public static final i o;
    public static final i p;
    public static final i q;
    public static final i r;
    public static final i s;
    public static final i t;
    public static final i u;
    public static final i v;
    public static final i w;

    static {
        i iVar = new i("1.3.6.1.4.1.22554");
        a = iVar;
        i z = iVar.z("1");
        b = z;
        i z2 = z.z("1");
        c = z2;
        i z3 = z.z("2.1");
        d = z3;
        z.z("2.2");
        z.z("2.3");
        z.z("2.4");
        z2.z("1");
        i z4 = z2.z("2");
        e = z4;
        z3.z("1");
        i z5 = z3.z("2");
        f = z5;
        z4.z("1.2");
        z4.z("1.22");
        z4.z("1.42");
        z5.z("1.2");
        z5.z("1.22");
        z5.z("1.42");
        i z6 = iVar.z("2");
        g = z6;
        i z7 = z6.z("1");
        h = z7;
        i = z7.z("1");
        j = z7.z("2");
        k = z7.z("3");
        i z8 = z6.z("2");
        l = z8;
        m = z8.z("1");
        n = z8.z("2");
        o = z8.z("3");
        p = z8.z("4");
        i z9 = z6.z("3");
        q = z9;
        r = z9.z("1");
        s = z9.z("2");
        t = z9.z("3");
        u = z9.z("4");
        i z10 = iVar.z("3");
        v = z10;
        w = z10.z("1");
    }
}
