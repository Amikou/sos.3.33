package defpackage;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.d;
import org.slf4j.a;
import org.web3j.protocol.exceptions.ClientConnectionException;
import org.web3j.utils.b;

/* compiled from: HttpService.java */
/* renamed from: ml1  reason: default package */
/* loaded from: classes3.dex */
public class ml1 extends fm3 {
    private static final List<ConnectionSpec> CONNECTION_SPEC_LIST;
    public static final String DEFAULT_URL = "http://localhost:8545/";
    private static final CipherSuite[] INFURA_CIPHER_SUITES;
    private static final ConnectionSpec INFURA_CIPHER_SUITE_SPEC;
    public static final MediaType JSON_MEDIA_TYPE;
    private static final x12 log;
    private HashMap<String, String> headers;
    private OkHttpClient httpClient;
    private final boolean includeRawResponse;
    private final String url;

    static {
        CipherSuite[] cipherSuiteArr = {CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_RSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384, CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA256, CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA256};
        INFURA_CIPHER_SUITES = cipherSuiteArr;
        ConnectionSpec build = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).cipherSuites(cipherSuiteArr).build();
        INFURA_CIPHER_SUITE_SPEC = build;
        CONNECTION_SPEC_LIST = Arrays.asList(build, ConnectionSpec.CLEARTEXT);
        JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
        log = a.i(ml1.class);
    }

    public ml1(String str, OkHttpClient okHttpClient, boolean z) {
        super(z);
        this.headers = new HashMap<>();
        this.url = str;
        this.httpClient = okHttpClient;
        this.includeRawResponse = z;
    }

    private Headers buildHeaders() {
        return Headers.of(this.headers);
    }

    private InputStream buildInputStream(ResponseBody responseBody) throws IOException {
        InputStream byteStream = responseBody.byteStream();
        if (this.includeRawResponse) {
            d source = responseBody.source();
            source.V0(Long.MAX_VALUE);
            long a0 = source.I().a0();
            if (a0 <= 2147483647L) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(byteStream, (int) a0);
                bufferedInputStream.mark(byteStream.available());
                return bufferedInputStream;
            }
            throw new UnsupportedOperationException("Non-integer input buffer size specified: " + a0);
        }
        return byteStream;
    }

    private static void configureLogging(OkHttpClient.Builder builder) {
        if (log.isDebugEnabled()) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(ll1.a);
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(httpLoggingInterceptor);
        }
    }

    private static OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder connectionSpecs = new OkHttpClient.Builder().connectionSpecs(CONNECTION_SPEC_LIST);
        configureLogging(connectionSpecs);
        return connectionSpecs.build();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ void lambda$configureLogging$0(String str) {
        log.debug(str);
    }

    public void addHeader(String str, String str2) {
        this.headers.put(str, str2);
    }

    public void addHeaders(Map<String, String> map) {
        this.headers.putAll(map);
    }

    @Override // defpackage.fm3, defpackage.lo4
    public void close() throws IOException {
    }

    public HashMap<String, String> getHeaders() {
        return this.headers;
    }

    public String getUrl() {
        return this.url;
    }

    @Override // defpackage.fm3
    public InputStream performIO(String str) throws IOException {
        Response execute = this.httpClient.newCall(new Request.Builder().url(this.url).headers(buildHeaders()).post(RequestBody.create(str, JSON_MEDIA_TYPE)).build()).execute();
        processHeaders(execute.headers());
        ResponseBody body = execute.body();
        if (execute.isSuccessful()) {
            if (body != null) {
                return buildInputStream(body);
            }
            return null;
        }
        int code = execute.code();
        String string = body == null ? b.MISSING_REASON : body.string();
        throw new ClientConnectionException("Invalid response received: " + code + "; " + string);
    }

    public void processHeaders(Headers headers) {
    }

    public ml1(OkHttpClient okHttpClient, boolean z) {
        this(DEFAULT_URL, okHttpClient, z);
    }

    public ml1(String str, OkHttpClient okHttpClient) {
        this(str, okHttpClient, false);
    }

    public ml1(String str) {
        this(str, createOkHttpClient());
    }

    public ml1(String str, boolean z) {
        this(str, createOkHttpClient(), z);
    }

    public ml1(OkHttpClient okHttpClient) {
        this(DEFAULT_URL, okHttpClient);
    }

    public ml1(boolean z) {
        this(DEFAULT_URL, z);
    }

    public ml1() {
        this(DEFAULT_URL);
    }
}
