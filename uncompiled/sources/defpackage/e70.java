package defpackage;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.a;
import com.fasterxml.jackson.databind.node.b;
import com.fasterxml.jackson.databind.node.e;
import com.fasterxml.jackson.databind.node.l;
import com.fasterxml.jackson.databind.node.m;
import com.fasterxml.jackson.databind.node.p;
import defpackage.e70;

/* compiled from: ContainerNode.java */
/* renamed from: e70  reason: default package */
/* loaded from: classes.dex */
public abstract class e70<T extends e70<T>> extends b {
    public final JsonNodeFactory a;

    public e70(JsonNodeFactory jsonNodeFactory) {
        this.a = jsonNodeFactory;
    }

    public final a K() {
        return this.a.arrayNode();
    }

    public final e L(boolean z) {
        return this.a.m3booleanNode(z);
    }

    public final l N() {
        return this.a.m4nullNode();
    }

    public final m O() {
        return this.a.objectNode();
    }

    public final p P(String str) {
        return this.a.m13textNode(str);
    }

    @Override // com.fasterxml.jackson.databind.d
    public String m() {
        return "";
    }

    public abstract int size();
}
