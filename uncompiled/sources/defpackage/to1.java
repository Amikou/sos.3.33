package defpackage;

import android.content.Context;
import com.facebook.common.memory.PooledByteBuffer;
import defpackage.oo4;
import defpackage.ro1;

/* compiled from: ImagePipelineExperiments.java */
/* renamed from: to1  reason: default package */
/* loaded from: classes.dex */
public class to1 {
    public final int A;
    public final boolean B;
    public final boolean C;
    public final boolean D;
    public final boolean E;
    public final boolean a;
    public final oo4.a b;
    public final boolean c;
    public final oo4 d;
    public final boolean e;
    public final boolean f;
    public final int g;
    public final int h;
    public boolean i;
    public final int j;
    public final boolean k;
    public final boolean l;
    public final d m;
    public final fw3<Boolean> n;
    public final boolean o;
    public final boolean p;
    public final int q;
    public final fw3<Boolean> r;
    public final boolean s;
    public final long t;
    public boolean u;
    public boolean v;
    public boolean w;
    public final boolean x;
    public final boolean y;
    public final boolean z;

    /* compiled from: ImagePipelineExperiments.java */
    /* renamed from: to1$b */
    /* loaded from: classes.dex */
    public static class b {
        public oo4.a b;
        public oo4 d;
        public d m;
        public fw3<Boolean> n;
        public boolean o;
        public boolean p;
        public int q;
        public boolean s;
        public boolean u;
        public boolean v;
        public boolean a = false;
        public boolean c = false;
        public boolean e = false;
        public boolean f = false;
        public int g = 0;
        public int h = 0;
        public boolean i = false;
        public int j = 2048;
        public boolean k = false;
        public boolean l = false;
        public fw3<Boolean> r = gw3.a(Boolean.FALSE);
        public long t = 0;
        public boolean w = true;
        public boolean x = true;
        public boolean y = false;
        public boolean z = false;
        public int A = 20;
        public boolean B = false;
        public boolean C = false;
        public boolean D = false;
        public boolean E = false;

        public b(ro1.b bVar) {
        }

        public to1 t() {
            return new to1(this);
        }
    }

    /* compiled from: ImagePipelineExperiments.java */
    /* renamed from: to1$c */
    /* loaded from: classes.dex */
    public static class c implements d {
        @Override // defpackage.to1.d
        public hv2 a(Context context, os osVar, tn1 tn1Var, qv2 qv2Var, boolean z, boolean z2, boolean z3, wy0 wy0Var, com.facebook.common.memory.b bVar, com.facebook.common.memory.c cVar, l72<wt, com.facebook.imagepipeline.image.a> l72Var, l72<wt, PooledByteBuffer> l72Var2, xr xrVar, xr xrVar2, xt xtVar, br2 br2Var, int i, int i2, boolean z4, int i3, a00 a00Var, boolean z5, int i4) {
            return new hv2(context, osVar, tn1Var, qv2Var, z, z2, z3, wy0Var, bVar, l72Var, l72Var2, xrVar, xrVar2, xtVar, br2Var, i, i2, z4, i3, a00Var, z5, i4);
        }
    }

    /* compiled from: ImagePipelineExperiments.java */
    /* renamed from: to1$d */
    /* loaded from: classes.dex */
    public interface d {
        hv2 a(Context context, os osVar, tn1 tn1Var, qv2 qv2Var, boolean z, boolean z2, boolean z3, wy0 wy0Var, com.facebook.common.memory.b bVar, com.facebook.common.memory.c cVar, l72<wt, com.facebook.imagepipeline.image.a> l72Var, l72<wt, PooledByteBuffer> l72Var2, xr xrVar, xr xrVar2, xt xtVar, br2 br2Var, int i, int i2, boolean z4, int i3, a00 a00Var, boolean z5, int i4);
    }

    public boolean A() {
        return this.v;
    }

    public boolean B() {
        return this.p;
    }

    public boolean C() {
        return this.E;
    }

    public boolean D() {
        return this.u;
    }

    public boolean E() {
        return this.D;
    }

    public boolean a() {
        return this.B;
    }

    public int b() {
        return this.q;
    }

    public boolean c() {
        return this.i;
    }

    public int d() {
        return this.h;
    }

    public int e() {
        return this.g;
    }

    public int f() {
        return this.j;
    }

    public long g() {
        return this.t;
    }

    public d h() {
        return this.m;
    }

    public fw3<Boolean> i() {
        return this.r;
    }

    public int j() {
        return this.A;
    }

    public boolean k() {
        return this.f;
    }

    public boolean l() {
        return this.e;
    }

    public oo4 m() {
        return this.d;
    }

    public oo4.a n() {
        return this.b;
    }

    public boolean o() {
        return this.C;
    }

    public boolean p() {
        return this.c;
    }

    public boolean q() {
        return this.z;
    }

    public boolean r() {
        return this.w;
    }

    public boolean s() {
        return this.y;
    }

    public boolean t() {
        return this.x;
    }

    public boolean u() {
        return this.s;
    }

    public boolean v() {
        return this.o;
    }

    public fw3<Boolean> w() {
        return this.n;
    }

    public boolean x() {
        return this.k;
    }

    public boolean y() {
        return this.l;
    }

    public boolean z() {
        return this.a;
    }

    public to1(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
        if (bVar.m != null) {
            this.m = bVar.m;
        } else {
            this.m = new c();
        }
        this.n = bVar.n;
        this.o = bVar.o;
        this.p = bVar.p;
        this.q = bVar.q;
        this.r = bVar.r;
        this.s = bVar.s;
        this.t = bVar.t;
        this.u = bVar.u;
        this.v = bVar.v;
        this.w = bVar.w;
        this.x = bVar.x;
        this.y = bVar.y;
        this.z = bVar.z;
        this.A = bVar.A;
        this.B = bVar.B;
        this.C = bVar.C;
        this.D = bVar.D;
        this.E = bVar.E;
    }
}
