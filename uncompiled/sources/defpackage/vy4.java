package defpackage;

/* renamed from: vy4  reason: default package */
/* loaded from: classes2.dex */
public final class vy4 implements Runnable {
    public final /* synthetic */ tr3 a;
    public final /* synthetic */ int f0;
    public final /* synthetic */ int g0;
    public final /* synthetic */ yy4 h0;

    public vy4(yy4 yy4Var, tr3 tr3Var, int i, int i2) {
        this.h0 = yy4Var;
        this.a = tr3Var;
        this.f0 = i;
        this.g0 = i2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        yy4 yy4Var = this.h0;
        tr3 tr3Var = this.a;
        yy4Var.g(new bt4(tr3Var.h(), this.f0, this.g0, tr3Var.c(), tr3Var.j(), tr3Var.a(), tr3Var.b(), tr3Var.g(), tr3Var.d()));
    }
}
