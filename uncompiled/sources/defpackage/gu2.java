package defpackage;

/* compiled from: Predicate.java */
/* renamed from: gu2  reason: default package */
/* loaded from: classes2.dex */
public interface gu2<T> {
    boolean apply(T t);
}
