package defpackage;

/* compiled from: Type.java */
/* renamed from: zc4  reason: default package */
/* loaded from: classes3.dex */
public interface zc4<T> {
    public static final int MAX_BIT_LENGTH = 256;
    public static final int MAX_BYTE_LENGTH = 32;

    int bytes32PaddedLength();

    String getTypeAsString();

    T getValue();
}
