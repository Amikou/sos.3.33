package defpackage;

/* compiled from: Suppliers.java */
/* renamed from: gw3  reason: default package */
/* loaded from: classes.dex */
public class gw3 {
    public static final fw3<Boolean> a;

    /* compiled from: Suppliers.java */
    /* renamed from: gw3$a */
    /* loaded from: classes.dex */
    public static class a implements fw3<T> {
        public final /* synthetic */ Object a;

        public a(Object obj) {
            this.a = obj;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T, java.lang.Object] */
        @Override // defpackage.fw3
        public T get() {
            return this.a;
        }
    }

    /* compiled from: Suppliers.java */
    /* renamed from: gw3$b */
    /* loaded from: classes.dex */
    public static class b implements fw3<Boolean> {
        @Override // defpackage.fw3
        /* renamed from: a */
        public Boolean get() {
            return Boolean.TRUE;
        }
    }

    /* compiled from: Suppliers.java */
    /* renamed from: gw3$c */
    /* loaded from: classes.dex */
    public static class c implements fw3<Boolean> {
        @Override // defpackage.fw3
        /* renamed from: a */
        public Boolean get() {
            return Boolean.FALSE;
        }
    }

    static {
        new b();
        a = new c();
    }

    public static <T> fw3<T> a(T t) {
        return new a(t);
    }
}
