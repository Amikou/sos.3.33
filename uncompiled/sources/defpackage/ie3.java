package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: ie3  reason: default package */
/* loaded from: classes2.dex */
public class ie3 extends ct0.b {
    public static final BigInteger g = ge3.j;
    public int[] f;

    public ie3() {
        this.f = bd2.d();
    }

    public ie3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP160R1FieldElement");
        }
        this.f = he3.c(bigInteger);
    }

    public ie3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] d = bd2.d();
        he3.a(this.f, ((ie3) ct0Var).f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] d = bd2.d();
        he3.b(this.f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] d = bd2.d();
        g92.d(he3.a, ((ie3) ct0Var).f, d);
        he3.d(d, this.f, d);
        return new ie3(d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ie3) {
            return bd2.f(this.f, ((ie3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] d = bd2.d();
        g92.d(he3.a, this.f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return bd2.j(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 5);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return bd2.k(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] d = bd2.d();
        he3.d(this.f, ((ie3) ct0Var).f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] d = bd2.d();
        he3.f(this.f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (bd2.k(iArr) || bd2.j(iArr)) {
            return this;
        }
        int[] d = bd2.d();
        he3.i(iArr, d);
        he3.d(d, iArr, d);
        int[] d2 = bd2.d();
        he3.j(d, 2, d2);
        he3.d(d2, d, d2);
        he3.j(d2, 4, d);
        he3.d(d, d2, d);
        he3.j(d, 8, d2);
        he3.d(d2, d, d2);
        he3.j(d2, 16, d);
        he3.d(d, d2, d);
        he3.j(d, 32, d2);
        he3.d(d2, d, d2);
        he3.j(d2, 64, d);
        he3.d(d, d2, d);
        he3.i(d, d2);
        he3.d(d2, iArr, d2);
        he3.j(d2, 29, d2);
        he3.i(d2, d);
        if (bd2.f(iArr, d)) {
            return new ie3(d2);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] d = bd2.d();
        he3.i(this.f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] d = bd2.d();
        he3.k(this.f, ((ie3) ct0Var).f, d);
        return new ie3(d);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return bd2.h(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return bd2.u(this.f);
    }
}
