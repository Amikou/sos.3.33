package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: SimpleChannelFlow.kt */
/* renamed from: mp3  reason: default package */
/* loaded from: classes.dex */
public final class mp3<T> implements lp3<T>, c90, uj3<T> {
    public final uj3<T> a;
    public final /* synthetic */ c90 f0;

    /* JADX WARN: Multi-variable type inference failed */
    public mp3(c90 c90Var, uj3<? super T> uj3Var) {
        fs1.f(c90Var, "scope");
        fs1.f(uj3Var, "channel");
        this.f0 = c90Var;
        this.a = uj3Var;
    }

    @Override // defpackage.uj3
    public Object h(T t, q70<? super te4> q70Var) {
        return this.a.h(t, q70Var);
    }

    @Override // defpackage.uj3
    public boolean l(Throwable th) {
        return this.a.l(th);
    }

    @Override // defpackage.c90
    public CoroutineContext m() {
        return this.f0.m();
    }
}
