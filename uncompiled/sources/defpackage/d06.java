package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: d06  reason: default package */
/* loaded from: classes.dex */
public final class d06 implements c06 {
    public static final wo5<Boolean> a;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.androidId.delete_feature", true);
        ro5Var.b("measurement.log_androidId_enabled", false);
    }

    @Override // defpackage.c06
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
