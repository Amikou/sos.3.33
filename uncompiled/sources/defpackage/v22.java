package defpackage;

import android.graphics.Bitmap;

/* compiled from: LruPoolStrategy.java */
/* renamed from: v22  reason: default package */
/* loaded from: classes.dex */
public interface v22 {
    String a(int i, int i2, Bitmap.Config config);

    int b(Bitmap bitmap);

    void c(Bitmap bitmap);

    Bitmap d(int i, int i2, Bitmap.Config config);

    String e(Bitmap bitmap);

    Bitmap removeLast();
}
