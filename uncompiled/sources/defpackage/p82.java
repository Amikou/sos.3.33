package defpackage;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.exoplayer.c;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: MetadataRenderer.java */
/* renamed from: p82  reason: default package */
/* loaded from: classes.dex */
public final class p82 extends c implements Handler.Callback {
    public final m82 q0;
    public final o82 r0;
    public final Handler s0;
    public final n82 t0;
    public l82 u0;
    public boolean v0;
    public boolean w0;
    public long x0;
    public long y0;
    public Metadata z0;

    public p82(o82 o82Var, Looper looper) {
        this(o82Var, looper, m82.a);
    }

    @Override // androidx.media3.exoplayer.c
    public void G() {
        this.z0 = null;
        this.y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.u0 = null;
    }

    @Override // androidx.media3.exoplayer.c
    public void I(long j, boolean z) {
        this.z0 = null;
        this.y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.v0 = false;
        this.w0 = false;
    }

    @Override // androidx.media3.exoplayer.c
    public void M(j[] jVarArr, long j, long j2) {
        this.u0 = this.q0.b(jVarArr[0]);
    }

    public final void Q(Metadata metadata, List<Metadata.Entry> list) {
        for (int i = 0; i < metadata.d(); i++) {
            j f0 = metadata.c(i).f0();
            if (f0 != null && this.q0.a(f0)) {
                l82 b = this.q0.b(f0);
                byte[] bArr = (byte[]) ii.e(metadata.c(i).y1());
                this.t0.h();
                this.t0.v(bArr.length);
                ((ByteBuffer) b.j(this.t0.g0)).put(bArr);
                this.t0.x();
                Metadata a = b.a(this.t0);
                if (a != null) {
                    Q(a, list);
                }
            } else {
                list.add(metadata.c(i));
            }
        }
    }

    public final void R(Metadata metadata) {
        Handler handler = this.s0;
        if (handler != null) {
            handler.obtainMessage(0, metadata).sendToTarget();
        } else {
            S(metadata);
        }
    }

    public final void S(Metadata metadata) {
        this.r0.t(metadata);
    }

    public final boolean T(long j) {
        boolean z;
        Metadata metadata = this.z0;
        if (metadata == null || this.y0 > j) {
            z = false;
        } else {
            R(metadata);
            this.z0 = null;
            this.y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            z = true;
        }
        if (this.v0 && this.z0 == null) {
            this.w0 = true;
        }
        return z;
    }

    public final void U() {
        if (this.v0 || this.z0 != null) {
            return;
        }
        this.t0.h();
        y81 B = B();
        int N = N(B, this.t0, 0);
        if (N != -4) {
            if (N == -5) {
                this.x0 = ((j) ii.e(B.b)).t0;
            }
        } else if (this.t0.p()) {
            this.v0 = true;
        } else {
            n82 n82Var = this.t0;
            n82Var.m0 = this.x0;
            n82Var.x();
            Metadata a = ((l82) b.j(this.u0)).a(this.t0);
            if (a != null) {
                ArrayList arrayList = new ArrayList(a.d());
                Q(a, arrayList);
                if (arrayList.isEmpty()) {
                    return;
                }
                this.z0 = new Metadata(arrayList);
                this.y0 = this.t0.i0;
            }
        }
    }

    @Override // androidx.media3.exoplayer.n
    public int a(j jVar) {
        if (this.q0.a(jVar)) {
            return u63.a(jVar.I0 == 0 ? 4 : 2);
        }
        return u63.a(0);
    }

    @Override // androidx.media3.exoplayer.m
    public boolean d() {
        return this.w0;
    }

    @Override // androidx.media3.exoplayer.m
    public boolean f() {
        return true;
    }

    @Override // androidx.media3.exoplayer.m, androidx.media3.exoplayer.n
    public String getName() {
        return "MetadataRenderer";
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            S((Metadata) message.obj);
            return true;
        }
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.m
    public void r(long j, long j2) {
        boolean z = true;
        while (z) {
            U();
            z = T(j);
        }
    }

    public p82(o82 o82Var, Looper looper, m82 m82Var) {
        super(5);
        this.r0 = (o82) ii.e(o82Var);
        this.s0 = looper == null ? null : b.u(looper, this);
        this.q0 = (m82) ii.e(m82Var);
        this.t0 = new n82();
        this.y0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
