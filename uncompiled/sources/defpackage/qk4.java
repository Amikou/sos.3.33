package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.view.View;

/* compiled from: ViewUtilsApi21.java */
/* renamed from: qk4  reason: default package */
/* loaded from: classes.dex */
public class qk4 extends pk4 {
    public static boolean g = true;
    public static boolean h = true;
    public static boolean i = true;

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public void e(View view, Matrix matrix) {
        if (g) {
            try {
                view.setAnimationMatrix(matrix);
            } catch (NoSuchMethodError unused) {
                g = false;
            }
        }
    }

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public void i(View view, Matrix matrix) {
        if (h) {
            try {
                view.transformMatrixToGlobal(matrix);
            } catch (NoSuchMethodError unused) {
                h = false;
            }
        }
    }

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public void j(View view, Matrix matrix) {
        if (i) {
            try {
                view.transformMatrixToLocal(matrix);
            } catch (NoSuchMethodError unused) {
                i = false;
            }
        }
    }
}
