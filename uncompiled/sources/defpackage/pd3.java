package defpackage;

import android.database.sqlite.SQLiteDatabase;
import defpackage.sd3;

/* renamed from: pd3  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class pd3 implements sd3.a {
    public static final /* synthetic */ pd3 a = new pd3();

    @Override // defpackage.sd3.a
    public final void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE events ADD COLUMN payload_encoding TEXT");
    }
}
