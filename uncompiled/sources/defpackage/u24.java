package defpackage;

/* compiled from: SystemIdInfo.java */
/* renamed from: u24  reason: default package */
/* loaded from: classes.dex */
public class u24 {
    public final String a;
    public final int b;

    public u24(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof u24) {
            u24 u24Var = (u24) obj;
            if (this.b != u24Var.b) {
                return false;
            }
            return this.a.equals(u24Var.a);
        }
        return false;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }
}
