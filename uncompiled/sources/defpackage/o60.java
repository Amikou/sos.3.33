package defpackage;

/* renamed from: o60  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class o60 implements m60 {
    public final m60 a;
    public final m60 b;

    public o60(m60 m60Var, m60 m60Var2) {
        this.a = m60Var;
        this.b = m60Var2;
    }

    public static m60 a(m60 m60Var, m60 m60Var2) {
        return new o60(m60Var, m60Var2);
    }

    @Override // defpackage.m60
    public void accept(Object obj) {
        p60.b(this.a, this.b, obj);
    }
}
