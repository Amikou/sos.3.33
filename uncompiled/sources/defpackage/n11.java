package defpackage;

import android.content.res.Resources;

/* compiled from: Extentions.kt */
/* renamed from: n11  reason: default package */
/* loaded from: classes.dex */
public final class n11 {
    public static final float a(float f) {
        return f * Resources.getSystem().getDisplayMetrics().density;
    }
}
