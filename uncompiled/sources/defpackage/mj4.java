package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.ArrayList;

/* compiled from: ViewOverlayApi14.java */
/* renamed from: mj4  reason: default package */
/* loaded from: classes2.dex */
public class mj4 implements pj4 {
    public a a;

    /* compiled from: ViewOverlayApi14.java */
    @SuppressLint({"ViewConstructor", "PrivateApi"})
    /* renamed from: mj4$a */
    /* loaded from: classes2.dex */
    public static class a extends ViewGroup {
        public ViewGroup a;
        public View f0;
        public ArrayList<Drawable> g0;
        public mj4 h0;
        public boolean i0;

        static {
            try {
                Class cls = Integer.TYPE;
                ViewGroup.class.getDeclaredMethod("invalidateChildInParentFast", cls, cls, Rect.class);
            } catch (NoSuchMethodException unused) {
            }
        }

        public a(Context context, ViewGroup viewGroup, View view, mj4 mj4Var) {
            super(context);
            this.g0 = null;
            this.a = viewGroup;
            this.f0 = view;
            setRight(viewGroup.getWidth());
            setBottom(viewGroup.getHeight());
            viewGroup.addView(this);
            this.h0 = mj4Var;
        }

        public void a(Drawable drawable) {
            b();
            if (this.g0 == null) {
                this.g0 = new ArrayList<>();
            }
            if (this.g0.contains(drawable)) {
                return;
            }
            this.g0.add(drawable);
            invalidate(drawable.getBounds());
            drawable.setCallback(this);
        }

        public final void b() {
            if (this.i0) {
                throw new IllegalStateException("This overlay was disposed already. Please use a new one via ViewGroupUtils.getOverlay()");
            }
        }

        public final void c() {
            if (getChildCount() == 0) {
                ArrayList<Drawable> arrayList = this.g0;
                if (arrayList == null || arrayList.size() == 0) {
                    this.i0 = true;
                    this.a.removeView(this);
                }
            }
        }

        public final void d(int[] iArr) {
            int[] iArr2 = new int[2];
            int[] iArr3 = new int[2];
            this.a.getLocationOnScreen(iArr2);
            this.f0.getLocationOnScreen(iArr3);
            iArr[0] = iArr3[0] - iArr2[0];
            iArr[1] = iArr3[1] - iArr2[1];
        }

        @Override // android.view.ViewGroup, android.view.View
        public void dispatchDraw(Canvas canvas) {
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            this.a.getLocationOnScreen(iArr);
            this.f0.getLocationOnScreen(iArr2);
            canvas.translate(iArr2[0] - iArr[0], iArr2[1] - iArr[1]);
            canvas.clipRect(new Rect(0, 0, this.f0.getWidth(), this.f0.getHeight()));
            super.dispatchDraw(canvas);
            ArrayList<Drawable> arrayList = this.g0;
            int size = arrayList == null ? 0 : arrayList.size();
            for (int i = 0; i < size; i++) {
                this.g0.get(i).draw(canvas);
            }
        }

        @Override // android.view.ViewGroup, android.view.View
        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            return false;
        }

        public void e(Drawable drawable) {
            ArrayList<Drawable> arrayList = this.g0;
            if (arrayList != null) {
                arrayList.remove(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(null);
                c();
            }
        }

        @Override // android.view.ViewGroup, android.view.ViewParent
        public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
            if (this.a != null) {
                rect.offset(iArr[0], iArr[1]);
                if (this.a != null) {
                    iArr[0] = 0;
                    iArr[1] = 0;
                    int[] iArr2 = new int[2];
                    d(iArr2);
                    rect.offset(iArr2[0], iArr2[1]);
                    return super.invalidateChildInParent(iArr, rect);
                }
                invalidate(rect);
                return null;
            }
            return null;
        }

        @Override // android.view.View, android.graphics.drawable.Drawable.Callback
        public void invalidateDrawable(Drawable drawable) {
            invalidate(drawable.getBounds());
        }

        @Override // android.view.ViewGroup, android.view.View
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        }

        @Override // android.view.View
        public boolean verifyDrawable(Drawable drawable) {
            ArrayList<Drawable> arrayList;
            return super.verifyDrawable(drawable) || ((arrayList = this.g0) != null && arrayList.contains(drawable));
        }
    }

    public mj4(Context context, ViewGroup viewGroup, View view) {
        this.a = new a(context, viewGroup, view, this);
    }

    public static mj4 c(View view) {
        ViewGroup d = mk4.d(view);
        if (d != null) {
            int childCount = d.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = d.getChildAt(i);
                if (childAt instanceof a) {
                    return ((a) childAt).h0;
                }
            }
            return new mi4(d.getContext(), d, view);
        }
        return null;
    }

    @Override // defpackage.pj4
    public void a(Drawable drawable) {
        this.a.a(drawable);
    }

    @Override // defpackage.pj4
    public void b(Drawable drawable) {
        this.a.e(drawable);
    }
}
