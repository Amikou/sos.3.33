package defpackage;

import java.io.File;
import kotlin.io.FileWalkDirection;

/* compiled from: FileTreeWalk.kt */
/* renamed from: d41  reason: default package */
/* loaded from: classes2.dex */
public class d41 extends c41 {
    public static final w31 c(File file, FileWalkDirection fileWalkDirection) {
        fs1.f(file, "$this$walk");
        fs1.f(fileWalkDirection, "direction");
        return new w31(file, fileWalkDirection);
    }

    public static final w31 d(File file) {
        fs1.f(file, "$this$walkBottomUp");
        return c(file, FileWalkDirection.BOTTOM_UP);
    }

    public static final w31 e(File file) {
        fs1.f(file, "$this$walkTopDown");
        return c(file, FileWalkDirection.TOP_DOWN);
    }
}
