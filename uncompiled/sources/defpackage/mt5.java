package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: mt5  reason: default package */
/* loaded from: classes.dex */
public final class mt5 implements Runnable {
    public final /* synthetic */ fw5 a;
    public final /* synthetic */ Runnable f0;

    public mt5(st5 st5Var, fw5 fw5Var, Runnable runnable) {
        this.a = fw5Var;
        this.f0 = runnable;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.a.i();
        this.a.h(this.f0);
        this.a.d();
    }
}
