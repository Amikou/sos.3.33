package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Status;
import defpackage.l83;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: gq2  reason: default package */
/* loaded from: classes.dex */
public abstract class gq2<R extends l83> {

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: gq2$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(@RecentlyNonNull Status status);
    }

    public abstract void a(@RecentlyNonNull a aVar);

    public abstract void b();

    @RecentlyNonNull
    public abstract boolean c();
}
