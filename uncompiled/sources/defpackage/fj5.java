package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: fj5  reason: default package */
/* loaded from: classes.dex */
public final class fj5 extends qi5 {
    public final bj5 a = new bj5();

    @Override // defpackage.qi5
    public final void a(Throwable th) {
        th.printStackTrace();
        List<Throwable> a = this.a.a(th, false);
        if (a == null) {
            return;
        }
        synchronized (a) {
            for (Throwable th2 : a) {
                System.err.print("Suppressed: ");
                th2.printStackTrace();
            }
        }
    }
}
