package defpackage;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.a;
import com.fasterxml.jackson.databind.jsontype.b;
import com.fasterxml.jackson.databind.jsontype.c;
import defpackage.vd4;
import java.util.Collection;

/* compiled from: TypeResolverBuilder.java */
/* renamed from: vd4  reason: default package */
/* loaded from: classes.dex */
public interface vd4<T extends vd4<T>> {
    a buildTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, Collection<NamedType> collection);

    c buildTypeSerializer(SerializationConfig serializationConfig, JavaType javaType, Collection<NamedType> collection);

    T defaultImpl(Class<?> cls);

    Class<?> getDefaultImpl();

    T inclusion(JsonTypeInfo.As as);

    T init(JsonTypeInfo.Id id, b bVar);

    T typeIdVisibility(boolean z);

    T typeProperty(String str);
}
