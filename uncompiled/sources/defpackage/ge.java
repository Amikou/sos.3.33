package defpackage;

import com.github.mikephil.charting.utils.Utils;
import com.rd.animation.type.AnimationType;
import com.rd.animation.type.DropAnimation;
import com.rd.draw.data.Orientation;
import defpackage.xg4;

/* compiled from: AnimationController.java */
/* renamed from: ge  reason: default package */
/* loaded from: classes2.dex */
public class ge {
    public xg4 a;
    public xg4.a b;
    public nm c;
    public mq1 d;
    public float e;
    public boolean f;

    /* compiled from: AnimationController.java */
    /* renamed from: ge$a */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[AnimationType.values().length];
            a = iArr;
            try {
                iArr[AnimationType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[AnimationType.COLOR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[AnimationType.SCALE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[AnimationType.WORM.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                a[AnimationType.FILL.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                a[AnimationType.SLIDE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                a[AnimationType.THIN_WORM.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                a[AnimationType.DROP.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                a[AnimationType.SWAP.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                a[AnimationType.SCALE_DOWN.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    public ge(mq1 mq1Var, xg4.a aVar) {
        this.a = new xg4(aVar);
        this.b = aVar;
        this.d = mq1Var;
    }

    public final void a() {
        switch (a.a[this.d.b().ordinal()]) {
            case 1:
                this.b.a(null);
                return;
            case 2:
                c();
                return;
            case 3:
                h();
                return;
            case 4:
                m();
                return;
            case 5:
                f();
                return;
            case 6:
                j();
                return;
            case 7:
                l();
                return;
            case 8:
                d();
                return;
            case 9:
                k();
                return;
            case 10:
                i();
                return;
            default:
                return;
        }
    }

    public void b() {
        this.f = false;
        this.e = Utils.FLOAT_EPSILON;
        a();
    }

    public final void c() {
        int p = this.d.p();
        int t = this.d.t();
        nm b = this.a.a().l(t, p).b(this.d.a());
        if (this.f) {
            b.d(this.e);
        } else {
            b.e();
        }
        this.c = b;
    }

    public final void d() {
        int q = this.d.z() ? this.d.q() : this.d.f();
        int r = this.d.z() ? this.d.r() : this.d.q();
        int a2 = r80.a(this.d, q);
        int a3 = r80.a(this.d, r);
        int l = this.d.l();
        int j = this.d.j();
        if (this.d.g() != Orientation.HORIZONTAL) {
            l = j;
        }
        int m = this.d.m();
        DropAnimation m2 = this.a.b().i(this.d.a()).m(a2, a3, (m * 3) + l, m + l, m);
        if (this.f) {
            m2.d(this.e);
        } else {
            m2.e();
        }
        this.c = m2;
    }

    public void e() {
        nm nmVar = this.c;
        if (nmVar != null) {
            nmVar.c();
        }
    }

    public final void f() {
        int p = this.d.p();
        int t = this.d.t();
        int m = this.d.m();
        int s = this.d.s();
        nm b = this.a.c().q(t, p, m, s).b(this.d.a());
        if (this.f) {
            b.d(this.e);
        } else {
            b.e();
        }
        this.c = b;
    }

    public void g(float f) {
        this.f = true;
        this.e = f;
        a();
    }

    public final void h() {
        int p = this.d.p();
        int t = this.d.t();
        int m = this.d.m();
        float o = this.d.o();
        nm b = this.a.d().p(t, p, m, o).b(this.d.a());
        if (this.f) {
            b.d(this.e);
        } else {
            b.e();
        }
        this.c = b;
    }

    public final void i() {
        int p = this.d.p();
        int t = this.d.t();
        int m = this.d.m();
        float o = this.d.o();
        nm b = this.a.e().p(t, p, m, o).b(this.d.a());
        if (this.f) {
            b.d(this.e);
        } else {
            b.e();
        }
        this.c = b;
    }

    public final void j() {
        int q = this.d.z() ? this.d.q() : this.d.f();
        int r = this.d.z() ? this.d.r() : this.d.q();
        nm b = this.a.f().l(r80.a(this.d, q), r80.a(this.d, r)).b(this.d.a());
        if (this.f) {
            b.d(this.e);
        } else {
            b.e();
        }
        this.c = b;
    }

    public final void k() {
        int q = this.d.z() ? this.d.q() : this.d.f();
        int r = this.d.z() ? this.d.r() : this.d.q();
        nm b = this.a.g().l(r80.a(this.d, q), r80.a(this.d, r)).b(this.d.a());
        if (this.f) {
            b.d(this.e);
        } else {
            b.e();
        }
        this.c = b;
    }

    public final void l() {
        int q = this.d.z() ? this.d.q() : this.d.f();
        int r = this.d.z() ? this.d.r() : this.d.q();
        int a2 = r80.a(this.d, q);
        int a3 = r80.a(this.d, r);
        boolean z = r > q;
        dr4 j = this.a.h().n(a2, a3, this.d.m(), z).j(this.d.a());
        if (this.f) {
            j.d(this.e);
        } else {
            j.e();
        }
        this.c = j;
    }

    public final void m() {
        int q = this.d.z() ? this.d.q() : this.d.f();
        int r = this.d.z() ? this.d.r() : this.d.q();
        int a2 = r80.a(this.d, q);
        int a3 = r80.a(this.d, r);
        boolean z = r > q;
        dr4 j = this.a.i().n(a2, a3, this.d.m(), z).j(this.d.a());
        if (this.f) {
            j.d(this.e);
        } else {
            j.e();
        }
        this.c = j;
    }
}
