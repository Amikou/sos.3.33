package defpackage;

import java.util.ListIterator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: sy5  reason: default package */
/* loaded from: classes.dex */
public final class sy5 implements ListIterator<String> {
    public ListIterator<String> a;
    public final /* synthetic */ int f0;
    public final /* synthetic */ uy5 g0;

    public sy5(uy5 uy5Var, int i) {
        gu5 gu5Var;
        this.g0 = uy5Var;
        this.f0 = i;
        gu5Var = uy5Var.a;
        this.a = gu5Var.listIterator(i);
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.ListIterator
    public final boolean hasPrevious() {
        return this.a.hasPrevious();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final /* synthetic */ Object next() {
        return this.a.next();
    }

    @Override // java.util.ListIterator
    public final int nextIndex() {
        return this.a.nextIndex();
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ String previous() {
        return this.a.previous();
    }

    @Override // java.util.ListIterator
    public final int previousIndex() {
        return this.a.previousIndex();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }
}
