package defpackage;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ay5  reason: default package */
/* loaded from: classes.dex */
public final class ay5 {
    public static final Iterator<Object> a = new yx5();
    public static final Iterable<Object> b = new cy5();

    public static <T> Iterable<T> a() {
        return (Iterable<T>) b;
    }
}
