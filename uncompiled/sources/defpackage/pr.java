package defpackage;

import android.util.SparseArray;
import java.util.LinkedList;

/* compiled from: BucketMap.java */
/* renamed from: pr  reason: default package */
/* loaded from: classes.dex */
public class pr<T> {
    public final SparseArray<b<T>> a = new SparseArray<>();
    public b<T> b;
    public b<T> c;

    /* compiled from: BucketMap.java */
    /* renamed from: pr$b */
    /* loaded from: classes.dex */
    public static class b<I> {
        public b<I> a;
        public int b;
        public LinkedList<I> c;
        public b<I> d;

        public String toString() {
            return "LinkedEntry(key: " + this.b + ")";
        }

        public b(b<I> bVar, int i, LinkedList<I> linkedList, b<I> bVar2) {
            this.a = bVar;
            this.b = i;
            this.c = linkedList;
            this.d = bVar2;
        }
    }

    public synchronized T a(int i) {
        b<T> bVar = this.a.get(i);
        if (bVar == null) {
            return null;
        }
        T pollFirst = bVar.c.pollFirst();
        c(bVar);
        return pollFirst;
    }

    public final void b(b<T> bVar) {
        if (bVar == null || !bVar.c.isEmpty()) {
            return;
        }
        d(bVar);
        this.a.remove(bVar.b);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void c(b<T> bVar) {
        if (this.b == bVar) {
            return;
        }
        d(bVar);
        b bVar2 = (b<T>) this.b;
        if (bVar2 == null) {
            this.b = bVar;
            this.c = bVar;
            return;
        }
        bVar.d = bVar2;
        bVar2.a = bVar;
        this.b = bVar;
    }

    public final synchronized void d(b<T> bVar) {
        b bVar2 = (b<T>) bVar.a;
        b bVar3 = (b<T>) bVar.d;
        if (bVar2 != null) {
            bVar2.d = bVar3;
        }
        if (bVar3 != null) {
            bVar3.a = bVar2;
        }
        bVar.a = null;
        bVar.d = null;
        if (bVar == this.b) {
            this.b = bVar3;
        }
        if (bVar == this.c) {
            this.c = bVar2;
        }
    }

    public synchronized void e(int i, T t) {
        b<T> bVar = this.a.get(i);
        if (bVar == null) {
            bVar = new b<>(null, i, new LinkedList(), null);
            this.a.put(i, bVar);
        }
        bVar.c.addLast(t);
        c(bVar);
    }

    public synchronized T f() {
        b<T> bVar = this.c;
        if (bVar == null) {
            return null;
        }
        T pollLast = bVar.c.pollLast();
        b(bVar);
        return pollLast;
    }
}
