package defpackage;

import android.content.res.Resources;
import android.widget.SpinnerAdapter;

/* compiled from: ThemedSpinnerAdapter.java */
/* renamed from: c54  reason: default package */
/* loaded from: classes.dex */
public interface c54 extends SpinnerAdapter {
    Resources.Theme getDropDownViewTheme();

    void setDropDownViewTheme(Resources.Theme theme);
}
