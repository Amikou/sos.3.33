package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ContactAddressPopUp.kt */
/* renamed from: q60  reason: default package */
/* loaded from: classes2.dex */
public final class q60 {
    public static final q60 a = new q60();
    public static int b;
    public static PopupWindow c;

    /* compiled from: Comparisons.kt */
    /* renamed from: q60$a */
    /* loaded from: classes2.dex */
    public static final class a<T> implements Comparator {
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            String name = ((IContact) t).getName();
            Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
            Locale locale = Locale.ROOT;
            String lowerCase = name.toLowerCase(locale);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            String name2 = ((IContact) t2).getName();
            Objects.requireNonNull(name2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase2 = name2.toLowerCase(locale);
            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            return l30.a(lowerCase, lowerCase2);
        }
    }

    /* compiled from: ContactAddressPopUp.kt */
    /* renamed from: q60$b */
    /* loaded from: classes2.dex */
    public static final class b extends RecyclerView.i {
        public final /* synthetic */ z60 a;
        public final /* synthetic */ View b;
        public final /* synthetic */ EditText c;
        public final /* synthetic */ PopupWindow.OnDismissListener d;

        public b(z60 z60Var, View view, EditText editText, PopupWindow.OnDismissListener onDismissListener) {
            this.a = z60Var;
            this.b = view;
            this.c = editText;
            this.d = onDismissListener;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.i
        public void onChanged() {
            super.onChanged();
            if (q60.c != null) {
                PopupWindow popupWindow = q60.c;
                boolean z = false;
                if (popupWindow != null && popupWindow.isShowing()) {
                    z = true;
                }
                if (!z || this.a.getItemCount() == q60.b) {
                    return;
                }
                q60 q60Var = q60.a;
                q60.b = this.a.getItemCount();
                PopupWindow popupWindow2 = q60.c;
                fs1.d(popupWindow2);
                popupWindow2.setOnDismissListener(null);
                PopupWindow popupWindow3 = q60.c;
                fs1.d(popupWindow3);
                popupWindow3.dismiss();
                View view = this.b;
                fs1.e(view, "view");
                q60.c = q60Var.f(view, q60.b);
                PopupWindow popupWindow4 = q60.c;
                if (popupWindow4 != null) {
                    popupWindow4.showAsDropDown(this.c);
                }
                this.c.requestFocus();
                PopupWindow popupWindow5 = q60.c;
                fs1.d(popupWindow5);
                popupWindow5.setOnDismissListener(this.d);
            }
        }
    }

    public final PopupWindow f(View view, int i) {
        PopupWindow popupWindow = new PopupWindow(view, -1, i <= 3 ? -2 : (int) t40.a(200));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void g() {
        PopupWindow popupWindow = c;
        if (popupWindow == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final boolean h() {
        PopupWindow popupWindow = c;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final q60 i(Context context, List<? extends IContact> list, EditText editText, sl1 sl1Var, PopupWindow.OnDismissListener onDismissListener) {
        fs1.f(context, "context");
        fs1.f(list, "itemData");
        fs1.f(editText, "editText");
        fs1.f(sl1Var, "onIContactItemClickListener");
        fs1.f(onDismissListener, "dismissListener");
        b = list.size();
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        View inflate = ((LayoutInflater) systemService).inflate(R.layout.dialog_contact_search, (ViewGroup) null);
        z60 z60Var = new z60(j20.e0(list, new a()), editText, sl1Var);
        ((RecyclerView) inflate.findViewById(R.id.recyclerView)).setAdapter(z60Var);
        fs1.e(inflate, "view");
        c = f(inflate, list.size());
        z60Var.registerAdapterDataObserver(new b(z60Var, inflate, editText, onDismissListener));
        PopupWindow popupWindow = c;
        if (popupWindow != null) {
            popupWindow.setOnDismissListener(onDismissListener);
        }
        PopupWindow popupWindow2 = c;
        if (popupWindow2 != null) {
            popupWindow2.showAsDropDown(editText);
        }
        editText.requestFocus();
        return this;
    }
}
