package defpackage;

import java.math.BigInteger;
import java.security.spec.AlgorithmParameterSpec;

/* renamed from: ot0  reason: default package */
/* loaded from: classes2.dex */
public class ot0 implements AlgorithmParameterSpec {
    public xs0 a;
    public byte[] b;
    public pt0 c;
    public BigInteger d;
    public BigInteger e;

    public ot0(xs0 xs0Var, pt0 pt0Var, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        this.a = xs0Var;
        this.c = pt0Var.A();
        this.d = bigInteger;
        this.e = bigInteger2;
        this.b = bArr;
    }

    public xs0 a() {
        return this.a;
    }

    public pt0 b() {
        return this.c;
    }

    public BigInteger c() {
        return this.e;
    }

    public BigInteger d() {
        return this.d;
    }

    public byte[] e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ot0) {
            ot0 ot0Var = (ot0) obj;
            return a().m(ot0Var.a()) && b().e(ot0Var.b());
        }
        return false;
    }

    public int hashCode() {
        return a().hashCode() ^ b().hashCode();
    }
}
