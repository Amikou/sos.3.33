package defpackage;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* compiled from: LockFreeLinkedList.kt */
/* renamed from: l12  reason: default package */
/* loaded from: classes2.dex */
public class l12 {
    public static final /* synthetic */ AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(l12.class, Object.class, "_next");
    public static final /* synthetic */ AtomicReferenceFieldUpdater f0 = AtomicReferenceFieldUpdater.newUpdater(l12.class, Object.class, "_prev");
    public static final /* synthetic */ AtomicReferenceFieldUpdater g0 = AtomicReferenceFieldUpdater.newUpdater(l12.class, Object.class, "_removedRef");
    public volatile /* synthetic */ Object _next = this;
    public volatile /* synthetic */ Object _prev = this;
    private volatile /* synthetic */ Object _removedRef = null;

    /* compiled from: LockFreeLinkedList.kt */
    /* renamed from: l12$a */
    /* loaded from: classes2.dex */
    public static abstract class a extends cj<l12> {
        public final l12 b;
        public l12 c;

        public a(l12 l12Var) {
            this.b = l12Var;
        }

        @Override // defpackage.cj
        /* renamed from: h */
        public void d(l12 l12Var, Object obj) {
            boolean z = obj == null;
            l12 l12Var2 = z ? this.b : this.c;
            if (l12Var2 != null && l12.a.compareAndSet(l12Var, this, l12Var2) && z) {
                l12 l12Var3 = this.b;
                l12 l12Var4 = this.c;
                fs1.d(l12Var4);
                l12Var3.m(l12Var4);
            }
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* renamed from: l12$b */
    /* loaded from: classes2.dex */
    public static final class b extends fn2 {
    }

    public final boolean i(l12 l12Var, l12 l12Var2) {
        f0.lazySet(l12Var, this);
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
        atomicReferenceFieldUpdater.lazySet(l12Var, l12Var2);
        if (atomicReferenceFieldUpdater.compareAndSet(this, l12Var2, l12Var)) {
            l12Var.m(l12Var2);
            return true;
        }
        return false;
    }

    public final boolean j(l12 l12Var) {
        f0.lazySet(l12Var, this);
        a.lazySet(l12Var, this);
        while (n() == this) {
            if (a.compareAndSet(this, this, l12Var)) {
                l12Var.m(this);
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Code restructure failed: missing block: B:28:0x0048, code lost:
        if (defpackage.l12.a.compareAndSet(r3, r2, ((defpackage.t63) r4).a) != false) goto L23;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.l12 k(defpackage.fn2 r8) {
        /*
            r7 = this;
        L0:
            java.lang.Object r0 = r7._prev
            l12 r0 = (defpackage.l12) r0
            r1 = 0
            r2 = r0
        L6:
            r3 = r1
        L7:
            java.lang.Object r4 = r2._next
            if (r4 != r7) goto L18
            if (r0 != r2) goto Le
            return r2
        Le:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = defpackage.l12.f0
            boolean r0 = r1.compareAndSet(r7, r0, r2)
            if (r0 != 0) goto L17
            goto L0
        L17:
            return r2
        L18:
            boolean r5 = r7.s()
            if (r5 == 0) goto L1f
            return r1
        L1f:
            if (r4 != r8) goto L22
            return r2
        L22:
            boolean r5 = r4 instanceof defpackage.fn2
            if (r5 == 0) goto L38
            if (r8 == 0) goto L32
            r0 = r4
            fn2 r0 = (defpackage.fn2) r0
            boolean r0 = r8.b(r0)
            if (r0 == 0) goto L32
            return r1
        L32:
            fn2 r4 = (defpackage.fn2) r4
            r4.c(r2)
            goto L0
        L38:
            boolean r5 = r4 instanceof defpackage.t63
            if (r5 == 0) goto L52
            if (r3 == 0) goto L4d
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r5 = defpackage.l12.a
            t63 r4 = (defpackage.t63) r4
            l12 r4 = r4.a
            boolean r2 = r5.compareAndSet(r3, r2, r4)
            if (r2 != 0) goto L4b
            goto L0
        L4b:
            r2 = r3
            goto L6
        L4d:
            java.lang.Object r2 = r2._prev
            l12 r2 = (defpackage.l12) r2
            goto L7
        L52:
            r3 = r4
            l12 r3 = (defpackage.l12) r3
            r6 = r3
            r3 = r2
            r2 = r6
            goto L7
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.l12.k(fn2):l12");
    }

    public final l12 l(l12 l12Var) {
        while (l12Var.s()) {
            l12Var = (l12) l12Var._prev;
        }
        return l12Var;
    }

    public final void m(l12 l12Var) {
        l12 l12Var2;
        do {
            l12Var2 = (l12) l12Var._prev;
            if (n() != l12Var) {
                return;
            }
        } while (!f0.compareAndSet(l12Var, l12Var2, this));
        if (s()) {
            l12Var.k(null);
        }
    }

    public final Object n() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof fn2)) {
                return obj;
            }
            ((fn2) obj).c(this);
        }
    }

    public final l12 o() {
        return k12.b(n());
    }

    public final l12 p() {
        l12 k = k(null);
        return k == null ? l((l12) this._prev) : k;
    }

    public final void q() {
        ((t63) n()).a.k(null);
    }

    public final void r() {
        l12 l12Var = this;
        while (true) {
            Object n = l12Var.n();
            if (!(n instanceof t63)) {
                l12Var.k(null);
                return;
            }
            l12Var = ((t63) n).a;
        }
    }

    public boolean s() {
        return n() instanceof t63;
    }

    public boolean t() {
        return v() == null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append((Object) getClass().getSimpleName());
        sb.append('@');
        sb.append((Object) Integer.toHexString(System.identityHashCode(this)));
        return sb.toString();
    }

    public final l12 u() {
        while (true) {
            l12 l12Var = (l12) n();
            if (l12Var == this) {
                return null;
            }
            if (l12Var.t()) {
                return l12Var;
            }
            l12Var.q();
        }
    }

    public final l12 v() {
        Object n;
        l12 l12Var;
        do {
            n = n();
            if (n instanceof t63) {
                return ((t63) n).a;
            }
            if (n == this) {
                return (l12) n;
            }
            l12Var = (l12) n;
        } while (!a.compareAndSet(this, n, l12Var.w()));
        l12Var.k(null);
        return null;
    }

    public final t63 w() {
        t63 t63Var = (t63) this._removedRef;
        if (t63Var == null) {
            t63 t63Var2 = new t63(this);
            g0.lazySet(this, t63Var2);
            return t63Var2;
        }
        return t63Var;
    }

    public final int x(l12 l12Var, l12 l12Var2, a aVar) {
        f0.lazySet(l12Var, this);
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
        atomicReferenceFieldUpdater.lazySet(l12Var, l12Var2);
        aVar.c = l12Var2;
        if (atomicReferenceFieldUpdater.compareAndSet(this, l12Var2, aVar)) {
            return aVar.c(this) == null ? 1 : 2;
        }
        return 0;
    }
}
