package defpackage;

import android.app.Dialog;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: l25  reason: default package */
/* loaded from: classes.dex */
public final class l25 extends t05 {
    public final /* synthetic */ Dialog a;
    public final /* synthetic */ g25 b;

    public l25(g25 g25Var, Dialog dialog) {
        this.b = g25Var;
        this.a = dialog;
    }

    @Override // defpackage.t05
    public final void a() {
        this.b.f0.n();
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
    }
}
