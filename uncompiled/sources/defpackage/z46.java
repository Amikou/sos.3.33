package defpackage;

import com.google.android.gms.internal.firebase_messaging.i;
import com.google.android.gms.internal.firebase_messaging.zzy;
import java.lang.annotation.Annotation;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: z46  reason: default package */
/* loaded from: classes.dex */
public final class z46 implements i {
    public final int a;
    public final zzy b;

    public z46(int i, zzy zzyVar) {
        this.a = i;
        this.b = zzyVar;
    }

    @Override // java.lang.annotation.Annotation
    public final Class<? extends Annotation> annotationType() {
        return i.class;
    }

    @Override // java.lang.annotation.Annotation
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof i) {
            i iVar = (i) obj;
            return this.a == iVar.zza() && this.b.equals(iVar.zzb());
        }
        return false;
    }

    @Override // java.lang.annotation.Annotation
    public final int hashCode() {
        return (this.a ^ 14552422) + (this.b.hashCode() ^ 2041407134);
    }

    @Override // java.lang.annotation.Annotation
    public final String toString() {
        return "@com.google.firebase.encoders.proto.Protobuf(tag=" + this.a + "intEncoding=" + this.b + ')';
    }

    @Override // com.google.android.gms.internal.firebase_messaging.i
    public final int zza() {
        return this.a;
    }

    @Override // com.google.android.gms.internal.firebase_messaging.i
    public final zzy zzb() {
        return this.b;
    }
}
