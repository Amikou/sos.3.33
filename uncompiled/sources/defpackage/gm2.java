package defpackage;

import androidx.activity.OnBackPressedDispatcher;

/* compiled from: OnBackPressedDispatcherOwner.java */
/* renamed from: gm2  reason: default package */
/* loaded from: classes.dex */
public interface gm2 extends rz1 {
    OnBackPressedDispatcher getOnBackPressedDispatcher();
}
