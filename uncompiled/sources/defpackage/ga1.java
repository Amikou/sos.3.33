package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.Group;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.chip.Chip;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentGoogleAuthPairBinding.java */
/* renamed from: ga1  reason: default package */
/* loaded from: classes2.dex */
public final class ga1 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final Group c;
    public final Group d;
    public final ShapeableImageView e;
    public final AppCompatImageView f;
    public final AppCompatImageView g;
    public final Chip h;
    public final MaterialTextView i;
    public final MaterialButton j;

    public ga1(MaterialCardView materialCardView, Barrier barrier, MaterialButton materialButton, MaterialButton materialButton2, View view, Group group, Group group2, ShapeableImageView shapeableImageView, ShapeableImageView shapeableImageView2, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, MaterialTextView materialTextView, Chip chip, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialButton materialButton3) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = group;
        this.d = group2;
        this.e = shapeableImageView2;
        this.f = appCompatImageView;
        this.g = appCompatImageView2;
        this.h = chip;
        this.i = materialTextView3;
        this.j = materialButton3;
    }

    public static ga1 a(View view) {
        int i = R.id.barrierBottom;
        Barrier barrier = (Barrier) ai4.a(view, R.id.barrierBottom);
        if (barrier != null) {
            i = R.id.btnContinue;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
            if (materialButton != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton2 != null) {
                    i = R.id.dividerBottom;
                    View a = ai4.a(view, R.id.dividerBottom);
                    if (a != null) {
                        i = R.id.groupKey;
                        Group group = (Group) ai4.a(view, R.id.groupKey);
                        if (group != null) {
                            i = R.id.groupQR;
                            Group group2 = (Group) ai4.a(view, R.id.groupQR);
                            if (group2 != null) {
                                i = R.id.imgAuthIcon;
                                ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgAuthIcon);
                                if (shapeableImageView != null) {
                                    i = R.id.imgQr;
                                    ShapeableImageView shapeableImageView2 = (ShapeableImageView) ai4.a(view, R.id.imgQr);
                                    if (shapeableImageView2 != null) {
                                        i = R.id.imgSwitchKey;
                                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgSwitchKey);
                                        if (appCompatImageView != null) {
                                            i = R.id.imgSwitchQR;
                                            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgSwitchQR);
                                            if (appCompatImageView2 != null) {
                                                i = R.id.txtAuthCopyTitle;
                                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAuthCopyTitle);
                                                if (materialTextView != null) {
                                                    i = R.id.txtAuthKey;
                                                    Chip chip = (Chip) ai4.a(view, R.id.txtAuthKey);
                                                    if (chip != null) {
                                                        i = R.id.txtAuthScanTitle;
                                                        MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtAuthScanTitle);
                                                        if (materialTextView2 != null) {
                                                            i = R.id.txtSwitchKey;
                                                            MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtSwitchKey);
                                                            if (materialTextView3 != null) {
                                                                i = R.id.txtSwitchQR;
                                                                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.txtSwitchQR);
                                                                if (materialButton3 != null) {
                                                                    return new ga1((MaterialCardView) view, barrier, materialButton, materialButton2, a, group, group2, shapeableImageView, shapeableImageView2, appCompatImageView, appCompatImageView2, materialTextView, chip, materialTextView2, materialTextView3, materialButton3);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
