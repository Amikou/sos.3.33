package defpackage;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ViewSwitcher;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.CurrencyConverterLayout;

/* compiled from: DialogAnchorCurrencyConverterBinding.java */
/* renamed from: dn0  reason: default package */
/* loaded from: classes2.dex */
public final class dn0 {
    public final ViewSwitcher a;
    public final CurrencyConverterLayout b;
    public final CurrencyConverterLayout c;
    public final AppCompatTextView d;
    public final ImageButton e;
    public final RecyclerView f;
    public final zd3 g;

    public dn0(ConstraintLayout constraintLayout, ViewSwitcher viewSwitcher, CurrencyConverterLayout currencyConverterLayout, CurrencyConverterLayout currencyConverterLayout2, AppCompatTextView appCompatTextView, ImageButton imageButton, RecyclerView recyclerView, zd3 zd3Var, AppCompatTextView appCompatTextView2) {
        this.a = viewSwitcher;
        this.b = currencyConverterLayout;
        this.c = currencyConverterLayout2;
        this.d = appCompatTextView;
        this.e = imageButton;
        this.f = recyclerView;
        this.g = zd3Var;
    }

    public static dn0 a(View view) {
        int i = R.id.ccWrapper;
        ViewSwitcher viewSwitcher = (ViewSwitcher) ai4.a(view, R.id.ccWrapper);
        if (viewSwitcher != null) {
            i = R.id.convertFrom;
            CurrencyConverterLayout currencyConverterLayout = (CurrencyConverterLayout) ai4.a(view, R.id.convertFrom);
            if (currencyConverterLayout != null) {
                i = R.id.convertTo;
                CurrencyConverterLayout currencyConverterLayout2 = (CurrencyConverterLayout) ai4.a(view, R.id.convertTo);
                if (currencyConverterLayout2 != null) {
                    i = R.id.defaultCurrencyTitle;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.defaultCurrencyTitle);
                    if (appCompatTextView != null) {
                        i = R.id.imgArrow;
                        ImageButton imageButton = (ImageButton) ai4.a(view, R.id.imgArrow);
                        if (imageButton != null) {
                            i = R.id.recyclerView;
                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.recyclerView);
                            if (recyclerView != null) {
                                i = R.id.searchBar;
                                View a = ai4.a(view, R.id.searchBar);
                                if (a != null) {
                                    zd3 a2 = zd3.a(a);
                                    i = R.id.txtTitle;
                                    AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txtTitle);
                                    if (appCompatTextView2 != null) {
                                        return new dn0((ConstraintLayout) view, viewSwitcher, currencyConverterLayout, currencyConverterLayout2, appCompatTextView, imageButton, recyclerView, a2, appCompatTextView2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
