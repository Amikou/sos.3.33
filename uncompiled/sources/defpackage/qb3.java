package defpackage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import android.util.Base64;
import com.google.android.datatransport.runtime.synchronization.SynchronizationException;
import defpackage.l24;
import defpackage.wx0;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: SQLiteEventStore.java */
/* renamed from: qb3  reason: default package */
/* loaded from: classes.dex */
public class qb3 implements dy0, l24 {
    public static final hv0 i0 = hv0.b("proto");
    public final sd3 a;
    public final qz f0;
    public final qz g0;
    public final ey0 h0;

    /* compiled from: SQLiteEventStore.java */
    /* renamed from: qb3$b */
    /* loaded from: classes.dex */
    public interface b<T, U> {
        U apply(T t);
    }

    /* compiled from: SQLiteEventStore.java */
    /* renamed from: qb3$c */
    /* loaded from: classes.dex */
    public static class c {
        public final String a;
        public final String b;

        public c(String str, String str2) {
            this.a = str;
            this.b = str2;
        }
    }

    /* compiled from: SQLiteEventStore.java */
    /* renamed from: qb3$d */
    /* loaded from: classes.dex */
    public interface d<T> {
        T a();
    }

    public qb3(qz qzVar, qz qzVar2, ey0 ey0Var, sd3 sd3Var) {
        this.a = sd3Var;
        this.f0 = qzVar;
        this.g0 = qzVar2;
        this.h0 = ey0Var;
    }

    public static byte[] H0(String str) {
        if (str == null) {
            return null;
        }
        return Base64.decode(str, 0);
    }

    public static hv0 L0(String str) {
        if (str == null) {
            return i0;
        }
        return hv0.b(str);
    }

    public static String O0(Iterable<nq2> iterable) {
        StringBuilder sb = new StringBuilder("(");
        Iterator<nq2> it = iterable.iterator();
        while (it.hasNext()) {
            sb.append(it.next().c());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public static /* synthetic */ Integer R(long j, SQLiteDatabase sQLiteDatabase) {
        return Integer.valueOf(sQLiteDatabase.delete("events", "timestamp_ms < ?", new String[]{String.valueOf(j)}));
    }

    public static <T> T S0(Cursor cursor, b<Cursor, T> bVar) {
        try {
            return bVar.apply(cursor);
        } finally {
            cursor.close();
        }
    }

    public static /* synthetic */ Object W(Throwable th) {
        throw new SynchronizationException("Timed out while trying to acquire the lock.", th);
    }

    public static /* synthetic */ SQLiteDatabase X(Throwable th) {
        throw new SynchronizationException("Timed out while trying to open db.", th);
    }

    public static /* synthetic */ Long a0(Cursor cursor) {
        if (cursor.moveToNext()) {
            return Long.valueOf(cursor.getLong(0));
        }
        return 0L;
    }

    public static /* synthetic */ Long b0(Cursor cursor) {
        if (cursor.moveToNext()) {
            return Long.valueOf(cursor.getLong(0));
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Boolean e0(ob4 ob4Var, SQLiteDatabase sQLiteDatabase) {
        Long F = F(sQLiteDatabase, ob4Var);
        if (F == null) {
            return Boolean.FALSE;
        }
        return (Boolean) S0(z().rawQuery("SELECT 1 FROM events WHERE context_id = ? LIMIT 1", new String[]{F.toString()}), bb3.a);
    }

    public static /* synthetic */ List f0(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            arrayList.add(ob4.a().b(cursor.getString(1)).d(wu2.b(cursor.getInt(2))).c(H0(cursor.getString(3))).a());
        }
        return arrayList;
    }

    public static /* synthetic */ List g0(SQLiteDatabase sQLiteDatabase) {
        return (List) S0(sQLiteDatabase.rawQuery("SELECT distinct t._id, t.backend_name, t.priority, t.extras FROM transport_contexts AS t, events AS e WHERE e.context_id = t._id", new String[0]), pb3.a);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ List i0(ob4 ob4Var, SQLiteDatabase sQLiteDatabase) {
        List<nq2> B0 = B0(sQLiteDatabase, ob4Var);
        return Q(B0, F0(sQLiteDatabase, B0));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object m0(List list, ob4 ob4Var, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            boolean z = cursor.getInt(7) != 0;
            wx0.a k = wx0.a().j(cursor.getString(1)).i(cursor.getLong(2)).k(cursor.getLong(3));
            if (z) {
                k.h(new cv0(L0(cursor.getString(4)), cursor.getBlob(5)));
            } else {
                k.h(new cv0(L0(cursor.getString(4)), J0(j)));
            }
            if (!cursor.isNull(6)) {
                k.g(Integer.valueOf(cursor.getInt(6)));
            }
            list.add(nq2.a(j, ob4Var, k.d()));
        }
        return null;
    }

    public static /* synthetic */ Object r0(Map map, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            Set set = (Set) map.get(Long.valueOf(j));
            if (set == null) {
                set = new HashSet();
                map.put(Long.valueOf(j), set);
            }
            set.add(new c(cursor.getString(1), cursor.getString(2)));
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Long w0(ob4 ob4Var, wx0 wx0Var, SQLiteDatabase sQLiteDatabase) {
        if (N()) {
            return -1L;
        }
        long x = x(sQLiteDatabase, ob4Var);
        int e = this.h0.e();
        byte[] a2 = wx0Var.e().a();
        boolean z = a2.length <= e;
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", Long.valueOf(x));
        contentValues.put("transport_name", wx0Var.j());
        contentValues.put("timestamp_ms", Long.valueOf(wx0Var.f()));
        contentValues.put("uptime_ms", Long.valueOf(wx0Var.k()));
        contentValues.put("payload_encoding", wx0Var.e().b().a());
        contentValues.put("code", wx0Var.d());
        contentValues.put("num_attempts", (Integer) 0);
        contentValues.put("inline", Boolean.valueOf(z));
        contentValues.put("payload", z ? a2 : new byte[0]);
        long insert = sQLiteDatabase.insert("events", null, contentValues);
        if (!z) {
            int ceil = (int) Math.ceil(a2.length / e);
            for (int i = 1; i <= ceil; i++) {
                byte[] copyOfRange = Arrays.copyOfRange(a2, (i - 1) * e, Math.min(i * e, a2.length));
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("event_id", Long.valueOf(insert));
                contentValues2.put("sequence_num", Integer.valueOf(i));
                contentValues2.put("bytes", copyOfRange);
                sQLiteDatabase.insert("event_payloads", null, contentValues2);
            }
        }
        for (Map.Entry<String, String> entry : wx0Var.i().entrySet()) {
            ContentValues contentValues3 = new ContentValues();
            contentValues3.put("event_id", Long.valueOf(insert));
            contentValues3.put(PublicResolver.FUNC_NAME, entry.getKey());
            contentValues3.put("value", entry.getValue());
            sQLiteDatabase.insert("event_metadata", null, contentValues3);
        }
        return Long.valueOf(insert);
    }

    public static /* synthetic */ byte[] x0(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (cursor.moveToNext()) {
            byte[] blob = cursor.getBlob(0);
            arrayList.add(blob);
            i += blob.length;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            byte[] bArr2 = (byte[]) arrayList.get(i3);
            System.arraycopy(bArr2, 0, bArr, i2, bArr2.length);
            i2 += bArr2.length;
        }
        return bArr;
    }

    public static /* synthetic */ Object y0(String str, SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.compileStatement(str).execute();
        sQLiteDatabase.compileStatement("DELETE FROM events WHERE num_attempts >= 16").execute();
        return null;
    }

    public static /* synthetic */ Object z0(long j, ob4 ob4Var, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("next_request_ms", Long.valueOf(j));
        if (sQLiteDatabase.update("transport_contexts", contentValues, "backend_name = ? and priority = ?", new String[]{ob4Var.b(), String.valueOf(wu2.a(ob4Var.d()))}) < 1) {
            contentValues.put("backend_name", ob4Var.b());
            contentValues.put("priority", Integer.valueOf(wu2.a(ob4Var.d())));
            sQLiteDatabase.insert("transport_contexts", null, contentValues);
        }
        return null;
    }

    public final long A() {
        return z().compileStatement("PRAGMA page_count").simpleQueryForLong();
    }

    public final List<nq2> B0(SQLiteDatabase sQLiteDatabase, final ob4 ob4Var) {
        final ArrayList arrayList = new ArrayList();
        Long F = F(sQLiteDatabase, ob4Var);
        if (F == null) {
            return arrayList;
        }
        S0(sQLiteDatabase.query("events", new String[]{"_id", "transport_name", "timestamp_ms", "uptime_ms", "payload_encoding", "payload", "code", "inline"}, "context_id = ?", new String[]{F.toString()}, null, null, null, String.valueOf(this.h0.d())), new b() { // from class: lb3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                Object m0;
                m0 = qb3.this.m0(arrayList, ob4Var, (Cursor) obj);
                return m0;
            }
        });
        return arrayList;
    }

    public final long C() {
        return z().compileStatement("PRAGMA page_size").simpleQueryForLong();
    }

    @Override // defpackage.dy0
    public void E(Iterable<nq2> iterable) {
        if (iterable.iterator().hasNext()) {
            z().compileStatement("DELETE FROM events WHERE _id in " + O0(iterable)).execute();
        }
    }

    public final Long F(SQLiteDatabase sQLiteDatabase, ob4 ob4Var) {
        StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        ArrayList arrayList = new ArrayList(Arrays.asList(ob4Var.b(), String.valueOf(wu2.a(ob4Var.d()))));
        if (ob4Var.c() != null) {
            sb.append(" and extras = ?");
            arrayList.add(Base64.encodeToString(ob4Var.c(), 0));
        } else {
            sb.append(" and extras is null");
        }
        return (Long) S0(sQLiteDatabase.query("transport_contexts", new String[]{"_id"}, sb.toString(), (String[]) arrayList.toArray(new String[0]), null, null, null), za3.a);
    }

    public final Map<Long, Set<c>> F0(SQLiteDatabase sQLiteDatabase, List<nq2> list) {
        final HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder("event_id IN (");
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).c());
            if (i < list.size() - 1) {
                sb.append(',');
            }
        }
        sb.append(')');
        S0(sQLiteDatabase.query("event_metadata", new String[]{"event_id", PublicResolver.FUNC_NAME, "value"}, sb.toString(), null, null, null, null), new b() { // from class: nb3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                Object r0;
                r0 = qb3.r0(hashMap, (Cursor) obj);
                return r0;
            }
        });
        return hashMap;
    }

    @Override // defpackage.dy0
    public Iterable<nq2> H(final ob4 ob4Var) {
        return (Iterable) M(new b() { // from class: jb3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                List i02;
                i02 = qb3.this.i0(ob4Var, (SQLiteDatabase) obj);
                return i02;
            }
        });
    }

    public final byte[] J0(long j) {
        return (byte[]) S0(z().query("event_payloads", new String[]{"bytes"}, "event_id = ?", new String[]{String.valueOf(j)}, null, null, "sequence_num"), ob3.a);
    }

    public final <T> T K0(d<T> dVar, b<Throwable, T> bVar) {
        long a2 = this.g0.a();
        while (true) {
            try {
                return dVar.a();
            } catch (SQLiteDatabaseLockedException e) {
                if (this.g0.a() >= this.h0.b() + a2) {
                    return bVar.apply(e);
                }
                SystemClock.sleep(50L);
            }
        }
    }

    public <T> T M(b<SQLiteDatabase, T> bVar) {
        SQLiteDatabase z = z();
        z.beginTransaction();
        try {
            T apply = bVar.apply(z);
            z.setTransactionSuccessful();
            return apply;
        } finally {
            z.endTransaction();
        }
    }

    public final boolean N() {
        return A() * C() >= this.h0.f();
    }

    public final List<nq2> Q(List<nq2> list, Map<Long, Set<c>> map) {
        ListIterator<nq2> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            nq2 next = listIterator.next();
            if (map.containsKey(Long.valueOf(next.c()))) {
                wx0.a l = next.b().l();
                for (c cVar : map.get(Long.valueOf(next.c()))) {
                    l.c(cVar.a, cVar.b);
                }
                listIterator.set(nq2.a(next.c(), next.d(), l.d()));
            }
        }
        return list;
    }

    @Override // defpackage.l24
    public <T> T a(l24.a<T> aVar) {
        SQLiteDatabase z = z();
        w(z);
        try {
            T execute = aVar.execute();
            z.setTransactionSuccessful();
            return execute;
        } finally {
            z.endTransaction();
        }
    }

    @Override // defpackage.dy0
    public nq2 b1(final ob4 ob4Var, final wx0 wx0Var) {
        z12.b("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", ob4Var.d(), wx0Var.j(), ob4Var.b());
        long longValue = ((Long) M(new b() { // from class: kb3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                Long w0;
                w0 = qb3.this.w0(ob4Var, wx0Var, (SQLiteDatabase) obj);
                return w0;
            }
        })).longValue();
        if (longValue < 1) {
            return null;
        }
        return nq2.a(longValue, ob4Var, wx0Var);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @Override // defpackage.dy0
    public boolean d1(final ob4 ob4Var) {
        return ((Boolean) M(new b() { // from class: ib3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                Boolean e0;
                e0 = qb3.this.e0(ob4Var, (SQLiteDatabase) obj);
                return e0;
            }
        })).booleanValue();
    }

    @Override // defpackage.dy0
    public Iterable<ob4> j0() {
        return (Iterable) M(cb3.a);
    }

    @Override // defpackage.dy0
    public int k() {
        final long a2 = this.f0.a() - this.h0.c();
        return ((Integer) M(new b() { // from class: ya3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                Integer R;
                R = qb3.R(a2, (SQLiteDatabase) obj);
                return R;
            }
        })).intValue();
    }

    @Override // defpackage.dy0
    public long l0(ob4 ob4Var) {
        return ((Long) S0(z().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[]{ob4Var.b(), String.valueOf(wu2.a(ob4Var.d()))}), ab3.a)).longValue();
    }

    @Override // defpackage.dy0
    public void u1(final ob4 ob4Var, final long j) {
        M(new b() { // from class: hb3
            @Override // defpackage.qb3.b
            public final Object apply(Object obj) {
                Object z0;
                z0 = qb3.z0(j, ob4Var, (SQLiteDatabase) obj);
                return z0;
            }
        });
    }

    public final void w(final SQLiteDatabase sQLiteDatabase) {
        K0(new d() { // from class: gb3
            @Override // defpackage.qb3.d
            public final Object a() {
                Object beginTransaction;
                beginTransaction = sQLiteDatabase.beginTransaction();
                return beginTransaction;
            }
        }, eb3.a);
    }

    public final long x(SQLiteDatabase sQLiteDatabase, ob4 ob4Var) {
        Long F = F(sQLiteDatabase, ob4Var);
        if (F != null) {
            return F.longValue();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("backend_name", ob4Var.b());
        contentValues.put("priority", Integer.valueOf(wu2.a(ob4Var.d())));
        contentValues.put("next_request_ms", (Integer) 0);
        if (ob4Var.c() != null) {
            contentValues.put("extras", Base64.encodeToString(ob4Var.c(), 0));
        }
        return sQLiteDatabase.insert("transport_contexts", null, contentValues);
    }

    @Override // defpackage.dy0
    public void x1(Iterable<nq2> iterable) {
        if (iterable.iterator().hasNext()) {
            final String str = "UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in " + O0(iterable);
            M(new b() { // from class: mb3
                @Override // defpackage.qb3.b
                public final Object apply(Object obj) {
                    Object y0;
                    y0 = qb3.y0(str, (SQLiteDatabase) obj);
                    return y0;
                }
            });
        }
    }

    public SQLiteDatabase z() {
        final sd3 sd3Var = this.a;
        Objects.requireNonNull(sd3Var);
        return (SQLiteDatabase) K0(new d() { // from class: fb3
            @Override // defpackage.qb3.d
            public final Object a() {
                return sd3.this.getWritableDatabase();
            }
        }, db3.a);
    }
}
