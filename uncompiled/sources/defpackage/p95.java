package defpackage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: p95  reason: default package */
/* loaded from: classes.dex */
public interface p95 {
    ExecutorService a(ThreadFactory threadFactory, int i);
}
