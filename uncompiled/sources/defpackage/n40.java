package defpackage;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.ArrayList;

/* compiled from: CompositeDisposable.java */
/* renamed from: n40  reason: default package */
/* loaded from: classes2.dex */
public final class n40 implements xp0 {
    public in2<xp0> a;
    public volatile boolean b;

    @Override // defpackage.xp0
    public void a() {
        if (this.b) {
            return;
        }
        synchronized (this) {
            if (this.b) {
                return;
            }
            this.b = true;
            in2<xp0> in2Var = this.a;
            this.a = null;
            c(in2Var);
        }
    }

    public boolean b(xp0 xp0Var) {
        il2.a(xp0Var, "Disposable item is null");
        if (this.b) {
            return false;
        }
        synchronized (this) {
            if (this.b) {
                return false;
            }
            in2<xp0> in2Var = this.a;
            if (in2Var != null && in2Var.b(xp0Var)) {
                return true;
            }
            return false;
        }
    }

    public void c(in2<xp0> in2Var) {
        Object[] a;
        if (in2Var == null) {
            return;
        }
        ArrayList arrayList = null;
        for (Object obj : in2Var.a()) {
            if (obj instanceof xp0) {
                try {
                    ((xp0) obj).a();
                } catch (Throwable th) {
                    my0.a(th);
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(th);
                }
            }
        }
        if (arrayList != null) {
            if (arrayList.size() == 1) {
                throw ExceptionHelper.a((Throwable) arrayList.get(0));
            }
            throw new CompositeException(arrayList);
        }
    }

    public boolean d(xp0 xp0Var) {
        if (b(xp0Var)) {
            xp0Var.a();
            return true;
        }
        return false;
    }
}
