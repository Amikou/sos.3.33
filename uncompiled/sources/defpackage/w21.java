package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.w21;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.common.FooterHistoryItem;
import net.safemoon.androidwallet.model.common.HeaderItemHistory;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.fiat.ResultItemFiat;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: FiatListAdapter.kt */
/* renamed from: w21  reason: default package */
/* loaded from: classes2.dex */
public abstract class w21 extends RecyclerView.Adapter<RecyclerView.a0> {
    public final a a;
    public final List<HistoryListItem> b;

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: w21$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(Fiat fiat);
    }

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: w21$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final ys1 a;
        public final /* synthetic */ w21 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(w21 w21Var, ys1 ys1Var) {
            super(ys1Var.b());
            fs1.f(w21Var, "this$0");
            fs1.f(ys1Var, "binding");
            this.b = w21Var;
            this.a = ys1Var;
        }

        public static final void c(w21 w21Var, RoomFiat roomFiat, View view) {
            fs1.f(w21Var, "this$0");
            w21Var.a.a(new Fiat(roomFiat.getSymbol(), roomFiat.getName(), roomFiat.getRate()));
        }

        public final void b(HistoryListItem historyListItem, boolean z) {
            fs1.f(historyListItem, "item");
            final RoomFiat result = ((ResultItemFiat) historyListItem).getResult();
            ys1 ys1Var = this.a;
            final w21 w21Var = this.b;
            if (result != null) {
                TextView textView = ys1Var.c;
                textView.setText(result.getSymbol() + " - " + ((Object) result.getName()));
                ys1Var.b.setChecked(fs1.b(w21Var.c().getSymbol(), result.getSymbol()));
            }
            ys1Var.d.setVisibility(e30.l0(z));
            if (result != null) {
                this.itemView.setOnClickListener(new View.OnClickListener() { // from class: x21
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        w21.b.c(w21.this, result, view);
                    }
                });
            }
        }
    }

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: w21$c */
    /* loaded from: classes2.dex */
    public static final class c extends RecyclerView.a0 {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(View view) {
            super(view);
            fs1.f(view, "itemView");
        }
    }

    /* compiled from: FiatListAdapter.kt */
    /* renamed from: w21$d */
    /* loaded from: classes2.dex */
    public static final class d extends RecyclerView.a0 {
        public final zs1 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(zs1 zs1Var) {
            super(zs1Var.b());
            fs1.f(zs1Var, "binding");
            this.a = zs1Var;
        }

        public final void a(HistoryListItem historyListItem) {
            fs1.f(historyListItem, "item");
            this.a.b.setText(((HeaderItemHistory) historyListItem).getTitle());
        }
    }

    public w21(a aVar) {
        fs1.f(aVar, "onDefaultCurrencySelectedListener");
        this.a = aVar;
        this.b = new ArrayList();
    }

    public final void b() {
        notifyDataSetChanged();
    }

    public abstract Fiat c();

    public final void d(List<HistoryListItem> list) {
        fs1.f(list, "items");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.b.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.b.get(i).getType();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        fs1.f(a0Var, "holder");
        HistoryListItem historyListItem = this.b.get(i);
        int i2 = i + 1;
        boolean z = i2 < this.b.size() ? this.b.get(i2) instanceof FooterHistoryItem : false;
        if (getItemViewType(i) == 0) {
            if (a0Var instanceof d) {
                ((d) a0Var).a(historyListItem);
            }
        } else if (getItemViewType(i) != 1 && (a0Var instanceof b)) {
            ((b) a0Var).b(historyListItem, z);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        if (i == 0) {
            zs1 a2 = zs1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fiat_list_header, viewGroup, false));
            fs1.e(a2, "bind(\n                  … false)\n                )");
            return new d(a2);
        } else if (i != 1) {
            ys1 a3 = ys1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fiat, viewGroup, false));
            fs1.e(a3, "bind(\n                  … false)\n                )");
            return new b(this, a3);
        } else {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_fiat_list_footer, viewGroup, false);
            fs1.e(inflate, "from(parent.context)\n   …st_footer, parent, false)");
            return new c(inflate);
        }
    }
}
