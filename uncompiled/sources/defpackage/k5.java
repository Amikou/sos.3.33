package defpackage;

import kotlin.Unit;
import kotlin.coroutines.Continuation;

/* compiled from: AbstractSharedFlow.kt */
/* renamed from: k5  reason: default package */
/* loaded from: classes2.dex */
public abstract class k5<F> {
    public abstract boolean a(F f);

    public abstract Continuation<Unit>[] b(F f);
}
