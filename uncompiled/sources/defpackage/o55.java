package defpackage;

import android.content.SharedPreferences;
import android.util.Base64;
import com.google.android.gms.internal.clearcut.b;
import java.io.IOException;

/* renamed from: o55  reason: default package */
/* loaded from: classes.dex */
public final class o55 extends r45<T> {
    public final Object k;
    public String l;
    public T m;
    public final /* synthetic */ b n;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o55(w55 w55Var, String str, Object obj, b bVar) {
        super(w55Var, str, obj, null);
        this.n = bVar;
        this.k = new Object();
    }

    @Override // defpackage.r45
    public final T c(SharedPreferences sharedPreferences) {
        try {
            return j(sharedPreferences.getString(this.b, ""));
        } catch (ClassCastException unused) {
            String valueOf = String.valueOf(this.b);
            if (valueOf.length() != 0) {
                "Invalid byte[] value in SharedPreferences for ".concat(valueOf);
                return null;
            }
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r1v9, types: [T, java.lang.Object] */
    @Override // defpackage.r45
    public final T j(String str) {
        T t;
        try {
            synchronized (this.k) {
                if (!str.equals(this.l)) {
                    ?? a = this.n.a(Base64.decode(str, 3));
                    this.l = str;
                    this.m = a;
                }
                t = this.m;
            }
            return t;
        } catch (IOException | IllegalArgumentException unused) {
            String str2 = this.b;
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 27 + String.valueOf(str).length());
            sb.append("Invalid byte[] value for ");
            sb.append(str2);
            sb.append(": ");
            sb.append(str);
            return null;
        }
    }
}
