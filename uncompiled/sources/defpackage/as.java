package defpackage;

import defpackage.r70;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.a;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: Builders.common.kt */
/* renamed from: as */
/* loaded from: classes2.dex */
public final /* synthetic */ class as {
    public static final st1 a(c90 c90Var, CoroutineContext coroutineContext, CoroutineStart coroutineStart, hd1<? super c90, ? super q70<? super te4>, ? extends Object> hd1Var) {
        t4 ls3Var;
        CoroutineContext c = x80.c(c90Var, coroutineContext);
        if (coroutineStart.isLazy()) {
            ls3Var = new bz1(c, hd1Var);
        } else {
            ls3Var = new ls3(c, true);
        }
        ls3Var.K0(coroutineStart, ls3Var, hd1Var);
        return ls3Var;
    }

    public static /* synthetic */ st1 b(c90 c90Var, CoroutineContext coroutineContext, CoroutineStart coroutineStart, hd1 hd1Var, int i, Object obj) {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        if ((i & 2) != 0) {
            coroutineStart = CoroutineStart.DEFAULT;
        }
        return a.a(c90Var, coroutineContext, coroutineStart, hd1Var);
    }

    public static final <T> Object c(CoroutineContext coroutineContext, hd1<? super c90, ? super q70<? super T>, ? extends Object> hd1Var, q70<? super T> q70Var) {
        Object M0;
        CoroutineContext context = q70Var.getContext();
        CoroutineContext plus = context.plus(coroutineContext);
        xt1.j(plus);
        if (plus == context) {
            vd3 vd3Var = new vd3(plus, q70Var);
            M0 = re4.c(vd3Var, vd3Var, hd1Var);
        } else {
            r70.b bVar = r70.d;
            if (fs1.b(plus.get(bVar), context.get(bVar))) {
                qe4 qe4Var = new qe4(plus, q70Var);
                Object c = ThreadContextKt.c(plus, null);
                try {
                    Object c2 = re4.c(qe4Var, qe4Var, hd1Var);
                    ThreadContextKt.a(plus, c);
                    M0 = c2;
                } catch (Throwable th) {
                    ThreadContextKt.a(plus, c);
                    throw th;
                }
            } else {
                pp0 pp0Var = new pp0(plus, q70Var);
                sv.d(hd1Var, pp0Var, pp0Var, null, 4, null);
                M0 = pp0Var.M0();
            }
        }
        if (M0 == gs1.d()) {
            ef0.c(q70Var);
        }
        return M0;
    }
}
