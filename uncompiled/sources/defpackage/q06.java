package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: q06  reason: default package */
/* loaded from: classes.dex */
public final class q06 implements yp5<r06> {
    public static final q06 f0 = new q06();
    public final yp5<r06> a = gq5.a(gq5.b(new s06()));

    public static long a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final r06 zza() {
        return this.a.zza();
    }
}
