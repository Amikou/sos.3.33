package defpackage;

import androidx.room.RoomDatabase;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: SharedSQLiteStatement.java */
/* renamed from: co3  reason: default package */
/* loaded from: classes.dex */
public abstract class co3 {
    public final AtomicBoolean a = new AtomicBoolean(false);
    public final RoomDatabase b;
    public volatile ww3 c;

    public co3(RoomDatabase roomDatabase) {
        this.b = roomDatabase;
    }

    public ww3 a() {
        b();
        return e(this.a.compareAndSet(false, true));
    }

    public void b() {
        this.b.c();
    }

    public final ww3 c() {
        return this.b.g(d());
    }

    public abstract String d();

    public final ww3 e(boolean z) {
        if (z) {
            if (this.c == null) {
                this.c = c();
            }
            return this.c;
        }
        return c();
    }

    public void f(ww3 ww3Var) {
        if (ww3Var == this.c) {
            this.a.set(false);
        }
    }
}
