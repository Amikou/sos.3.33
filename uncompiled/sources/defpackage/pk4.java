package defpackage;

import android.annotation.SuppressLint;
import android.view.View;

/* compiled from: ViewUtilsApi19.java */
/* renamed from: pk4  reason: default package */
/* loaded from: classes.dex */
public class pk4 extends uk4 {
    public static boolean f = true;

    @Override // defpackage.uk4
    public void a(View view) {
    }

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public float c(View view) {
        if (f) {
            try {
                return view.getTransitionAlpha();
            } catch (NoSuchMethodError unused) {
                f = false;
            }
        }
        return view.getAlpha();
    }

    @Override // defpackage.uk4
    public void d(View view) {
    }

    @Override // defpackage.uk4
    @SuppressLint({"NewApi"})
    public void g(View view, float f2) {
        if (f) {
            try {
                view.setTransitionAlpha(f2);
                return;
            } catch (NoSuchMethodError unused) {
                f = false;
            }
        }
        view.setAlpha(f2);
    }
}
