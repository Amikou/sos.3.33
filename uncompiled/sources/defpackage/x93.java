package defpackage;

import android.graphics.Canvas;
import android.graphics.drawable.NinePatchDrawable;

/* compiled from: RoundedNinePatchDrawable.java */
/* renamed from: x93  reason: default package */
/* loaded from: classes.dex */
public class x93 extends w93 {
    public x93(NinePatchDrawable ninePatchDrawable) {
        super(ninePatchDrawable);
    }

    @Override // defpackage.w93, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (nc1.d()) {
            nc1.a("RoundedNinePatchDrawable#draw");
        }
        if (!f()) {
            super.draw(canvas);
            if (nc1.d()) {
                nc1.b();
                return;
            }
            return;
        }
        i();
        g();
        canvas.clipPath(this.i0);
        super.draw(canvas);
        if (nc1.d()) {
            nc1.b();
        }
    }
}
