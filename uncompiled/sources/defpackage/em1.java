package defpackage;

import java.util.Map;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: IGetTokenTypeMapUseCase.kt */
/* renamed from: em1  reason: default package */
/* loaded from: classes2.dex */
public interface em1 {
    Map<String, TokenType> get();
}
