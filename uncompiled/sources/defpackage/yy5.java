package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: yy5  reason: default package */
/* loaded from: classes.dex */
public final class yy5 extends c55 {
    public final /* synthetic */ a16 g0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public yy5(uz5 uz5Var, String str, a16 a16Var) {
        super("getValue");
        this.g0 = a16Var;
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        vm5.a("getValue", 2, list);
        z55 a = wk5Var.a(list.get(0));
        z55 a2 = wk5Var.a(list.get(1));
        String a3 = this.g0.a(a.zzc());
        return a3 != null ? new f65(a3) : a2;
    }
}
