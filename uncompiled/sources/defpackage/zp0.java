package defpackage;

/* compiled from: CancellableContinuation.kt */
/* renamed from: zp0  reason: default package */
/* loaded from: classes2.dex */
public final class zp0 extends iv {
    public final yp0 a;

    public zp0(yp0 yp0Var) {
        this.a = yp0Var;
    }

    @Override // defpackage.jv
    public void a(Throwable th) {
        this.a.a();
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        a(th);
        return te4.a;
    }

    public String toString() {
        return "DisposeOnCancel[" + this.a + ']';
    }
}
