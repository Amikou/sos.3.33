package defpackage;

import android.content.ComponentName;
import android.net.Uri;
import android.os.Build;
import com.onesignal.OneSignal;
import defpackage.rc0;

/* compiled from: OneSignalChromeTab.java */
/* renamed from: an2  reason: default package */
/* loaded from: classes2.dex */
public class an2 {

    /* compiled from: OneSignalChromeTab.java */
    /* renamed from: an2$a */
    /* loaded from: classes2.dex */
    public static class a extends tc0 {
        public String f0;
        public boolean g0;

        public a(String str, boolean z) {
            this.f0 = str;
            this.g0 = z;
        }

        @Override // defpackage.tc0
        public void a(ComponentName componentName, androidx.browser.customtabs.a aVar) {
            aVar.e(0L);
            uc0 c = aVar.c(null);
            if (c == null) {
                return;
            }
            Uri parse = Uri.parse(this.f0);
            c.f(parse, null, null);
            if (this.g0) {
                rc0 a = new rc0.a(c).a();
                a.a.setData(parse);
                a.a.addFlags(268435456);
                if (Build.VERSION.SDK_INT >= 16) {
                    OneSignal.e.startActivity(a.a, a.b);
                } else {
                    OneSignal.e.startActivity(a.a);
                }
            }
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
        }
    }

    public static boolean a() {
        return true;
    }

    public static boolean b(String str, boolean z) {
        if (a()) {
            return androidx.browser.customtabs.a.a(OneSignal.e, "com.android.chrome", new a(str, z));
        }
        return false;
    }
}
