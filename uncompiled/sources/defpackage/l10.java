package defpackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: CollectionUtils.java */
/* renamed from: l10  reason: default package */
/* loaded from: classes2.dex */
public class l10 {

    /* compiled from: CollectionUtils.java */
    /* renamed from: l10$a */
    /* loaded from: classes2.dex */
    public static class a implements os4 {
        @Override // defpackage.os4
        public Object apply(Object obj) {
            return obj;
        }
    }

    static {
        new a();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static <TypeT> List<TypeT> a(Collection<TypeT> collection, TypeT typet, ps4<TypeT, TypeT, Boolean> ps4Var) {
        if (collection == null) {
            return new ArrayList(0);
        }
        ArrayList arrayList = new ArrayList(collection);
        boolean z = false;
        for (int i = 0; i < arrayList.size(); i++) {
            if (((Boolean) ps4Var.apply(arrayList.get(i), typet)).booleanValue()) {
                arrayList.set(i, typet);
                z = true;
            }
        }
        if (!z) {
            arrayList.add(typet);
        }
        return arrayList;
    }

    public static <TypeT> List<TypeT> b(List<TypeT>... listArr) {
        if (listArr != null && listArr.length != 0) {
            ArrayList arrayList = new ArrayList();
            Iterator it = new CopyOnWriteArrayList(listArr).iterator();
            while (it.hasNext()) {
                List list = (List) it.next();
                if (i(list)) {
                    Iterator it2 = new CopyOnWriteArrayList(list).iterator();
                    while (it2.hasNext()) {
                        arrayList.add(it2.next());
                    }
                }
            }
            return arrayList;
        }
        return new ArrayList();
    }

    public static <TypeT> List<TypeT> c(List<TypeT> list) {
        if (list == null) {
            return new ArrayList();
        }
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList(list);
        ArrayList arrayList = new ArrayList(copyOnWriteArrayList.size());
        Iterator it = copyOnWriteArrayList.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList;
    }

    public static <KeyT, ValueT> Map<KeyT, ValueT> d(Map<KeyT, ValueT> map) {
        if (map == null) {
            return new HashMap();
        }
        Map synchronizedMap = Collections.synchronizedMap(map);
        HashMap hashMap = new HashMap();
        hashMap.putAll(synchronizedMap);
        return hashMap;
    }

    public static <TypeT> List<TypeT> e(List<TypeT> list) {
        return g(list) ? new ArrayList() : list;
    }

    public static <TypeT> List<TypeT> f(Collection<TypeT> collection, os4<TypeT, Boolean> os4Var) {
        if (collection == null) {
            return new ArrayList(0);
        }
        ArrayList arrayList = new ArrayList(collection.size());
        for (TypeT typet : collection) {
            if (os4Var.apply(typet).booleanValue()) {
                arrayList.add(typet);
            }
        }
        return arrayList;
    }

    public static <TypeT> boolean g(Collection<TypeT> collection) {
        return collection == null || collection.isEmpty();
    }

    public static <TypeT> boolean h(TypeT[] typetArr) {
        return typetArr == null || typetArr.length == 0;
    }

    public static <TypeT> boolean i(Collection<TypeT> collection) {
        return !g(collection);
    }

    public static <TypeT> boolean j(TypeT[] typetArr) {
        return !h(typetArr);
    }

    public static <TypeT, ReturnT> List<ReturnT> k(Collection<TypeT> collection, os4<TypeT, ReturnT> os4Var) {
        if (collection == null) {
            return new ArrayList(0);
        }
        ArrayList arrayList = new ArrayList(collection.size());
        for (TypeT typet : collection) {
            arrayList.add(os4Var.apply(typet));
        }
        return arrayList;
    }

    public static <TypeT> List<TypeT> l(List<TypeT> list) {
        return Collections.unmodifiableList(e(list));
    }
}
