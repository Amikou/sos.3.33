package defpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import androidx.core.content.FileProvider;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.web3j.ens.contracts.generated.PublicResolver;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.i;

/* compiled from: Storage.java */
/* renamed from: vt3  reason: default package */
/* loaded from: classes3.dex */
public class vt3 {
    public static String c(Context context, Uri uri, boolean z) {
        String substring;
        String lastPathSegment;
        int lastIndexOf;
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        String scheme = uri.getScheme();
        if (PublicResolver.FUNC_CONTENT.equals(scheme)) {
            substring = singleton.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            substring = (!"file".equals(scheme) || (lastIndexOf = (lastPathSegment = uri.getLastPathSegment()).lastIndexOf(".")) == -1) ? "tmp" : lastPathSegment.substring(lastIndexOf + 1, lastPathSegment.length());
        }
        return z ? String.format(Locale.US, ".%s", substring) : substring;
    }

    public static String g(Context context, Uri uri) {
        String scheme = uri.getScheme();
        if (!PublicResolver.FUNC_CONTENT.equals(scheme)) {
            return "file".equals(scheme) ? uri.getLastPathSegment() : "";
        }
        Cursor query = context.getContentResolver().query(uri, new String[]{"_display_name"}, null, null, null);
        if (query != null) {
            try {
                return query.moveToFirst() ? query.getString(0) : "";
            } finally {
                query.close();
            }
        }
        return "";
    }

    public static MediaResult j(Context context, Uri uri) {
        String str;
        String str2;
        long j;
        String str3 = "";
        long j2 = -1;
        if (PublicResolver.FUNC_CONTENT.equals(uri.getScheme())) {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor query = contentResolver.query(uri, new String[]{"_size", "_display_name"}, null, null, null);
            String type = contentResolver.getType(uri);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        long j3 = query.getLong(query.getColumnIndex("_size"));
                        str3 = query.getString(query.getColumnIndex("_display_name"));
                        j2 = j3;
                    }
                } finally {
                    query.close();
                }
            }
            str = str3;
            j = j2;
            str2 = type;
        } else {
            str = "";
            str2 = str;
            j = -1;
        }
        return new MediaResult(null, uri, uri, str, str2, j, -1L, -1L);
    }

    public final File a(File file, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        if (TextUtils.isEmpty(str2)) {
            str2 = "";
        }
        sb.append(str2);
        return new File(file, sb.toString());
    }

    public final File b(Context context, String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "";
        } else {
            str2 = str + File.separator;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(k(context));
        String str3 = File.separator;
        sb.append(str3);
        sb.append("belvedere-data-v2");
        sb.append(str3);
        sb.append(str2);
        File file = new File(sb.toString());
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        if (file.isDirectory()) {
            return file;
        }
        return null;
    }

    public File d(Context context, String str, String str2) {
        String str3 = "user";
        if (!TextUtils.isEmpty(str)) {
            str3 = "user" + File.separator + str;
        }
        File b = b(context, str3);
        if (b == null) {
            i.e("Belvedere", "Error creating cache directory");
            return null;
        }
        return a(b, str2, null);
    }

    public File e(Context context) {
        File b = b(context, "media");
        if (b == null) {
            i.e("Belvedere", "Error creating cache directory");
            return null;
        }
        Locale locale = Locale.US;
        return a(b, String.format(locale, "camera_image_%s", new SimpleDateFormat("yyyyMMddHHmmssSSS", locale).format(new Date(System.currentTimeMillis()))), ".jpg");
    }

    public File f(Context context, Uri uri, String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "media";
        } else {
            str2 = "user" + File.separator + str;
        }
        File b = b(context, str2);
        String str3 = null;
        if (b == null) {
            i.e("Belvedere", "Error creating cache directory");
            return null;
        }
        String g = g(context, uri);
        if (TextUtils.isEmpty(g)) {
            Locale locale = Locale.US;
            g = String.format(locale, "attachment_%s", new SimpleDateFormat("yyyyMMddHHmmssSSS", locale).format(new Date(System.currentTimeMillis())));
            str3 = c(context, uri, true);
        }
        return a(b, g, str3);
    }

    public String h(Context context) {
        return String.format(Locale.US, "%s%s", context.getPackageName(), context.getString(j13.belvedere_sdk_fpa_suffix_v2));
    }

    public Uri i(Context context, File file) {
        String h = h(context);
        try {
            return FileProvider.e(context, h, file);
        } catch (IllegalArgumentException unused) {
            i.b("Belvedere", String.format(Locale.US, "The selected file can't be shared %s", file.toString()));
            return null;
        } catch (NullPointerException e) {
            i.c("Belvedere", String.format(Locale.US, "=====================\nFileProvider failed to retrieve file uri. There might be an issue with the FileProvider \nPlease make sure that manifest-merger is working, and that you have defined the applicationId (package name) in the build.gradle\nManifest merger: http://tools.android.com/tech-docs/new-build-system/user-guide/manifest-merger\nIf your are not able to use gradle or the manifest merger, please add the following to your AndroidManifest.xml:\n        <provider\n            android:name=\"com.zendesk.belvedere.BelvedereFileProvider\"\n            android:authorities=\"${applicationId}%s\"\n            android:exported=\"false\"\n            android:grantUriPermissions=\"true\">\n            <meta-data\n                android:name=\"android.support.FILE_PROVIDER_PATHS\"\n                android:resource=\"@xml/belvedere_attachment_storage_v2\" />\n        </provider>\n=====================", h), e);
            throw new RuntimeException("Please specify your application id");
        }
    }

    public final String k(Context context) {
        return context.getCacheDir().getAbsolutePath();
    }

    public void l(Context context, Intent intent, Uri uri, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            intent.addFlags(i);
            return;
        }
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 65536)) {
            context.grantUriPermission(resolveInfo.activityInfo.packageName, uri, i);
        }
    }

    public void m(Context context, Uri uri, int i) {
        context.revokeUriPermission(uri, i);
    }
}
