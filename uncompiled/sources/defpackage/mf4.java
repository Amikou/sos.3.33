package defpackage;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.datatransport.runtime.backends.BackendResponse;
import com.google.android.datatransport.runtime.backends.c;
import com.google.android.datatransport.runtime.synchronization.SynchronizationException;
import defpackage.l24;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Executor;

/* compiled from: Uploader.java */
/* renamed from: mf4  reason: default package */
/* loaded from: classes.dex */
public class mf4 {
    public final Context a;
    public final bm b;
    public final dy0 c;
    public final rq4 d;
    public final Executor e;
    public final l24 f;
    public final qz g;

    public mf4(Context context, bm bmVar, dy0 dy0Var, rq4 rq4Var, Executor executor, l24 l24Var, qz qzVar) {
        this.a = context;
        this.b = bmVar;
        this.c = dy0Var;
        this.d = rq4Var;
        this.e = executor;
        this.f = l24Var;
        this.g = qzVar;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Iterable f(ob4 ob4Var) {
        return this.c.H(ob4Var);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object g(BackendResponse backendResponse, Iterable iterable, ob4 ob4Var, int i) {
        if (backendResponse.c() == BackendResponse.Status.TRANSIENT_ERROR) {
            this.c.x1(iterable);
            this.d.a(ob4Var, i + 1);
            return null;
        }
        this.c.E(iterable);
        if (backendResponse.c() == BackendResponse.Status.OK) {
            this.c.u1(ob4Var, this.g.a() + backendResponse.b());
        }
        if (this.c.d1(ob4Var)) {
            this.d.b(ob4Var, 1, true);
            return null;
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object h(ob4 ob4Var, int i) {
        this.d.a(ob4Var, i + 1);
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void i(final ob4 ob4Var, final int i, Runnable runnable) {
        try {
            try {
                l24 l24Var = this.f;
                final dy0 dy0Var = this.c;
                Objects.requireNonNull(dy0Var);
                l24Var.a(new l24.a() { // from class: hf4
                    @Override // defpackage.l24.a
                    public final Object execute() {
                        return Integer.valueOf(dy0.this.k());
                    }
                });
                if (!e()) {
                    this.f.a(new l24.a() { // from class: jf4
                        @Override // defpackage.l24.a
                        public final Object execute() {
                            Object h;
                            h = mf4.this.h(ob4Var, i);
                            return h;
                        }
                    });
                } else {
                    j(ob4Var, i);
                }
            } catch (SynchronizationException unused) {
                this.d.a(ob4Var, i + 1);
            }
        } finally {
            runnable.run();
        }
    }

    public boolean e() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void j(final ob4 ob4Var, final int i) {
        BackendResponse b;
        nb4 nb4Var = this.b.get(ob4Var.b());
        final Iterable<nq2> iterable = (Iterable) this.f.a(new l24.a() { // from class: if4
            @Override // defpackage.l24.a
            public final Object execute() {
                Iterable f;
                f = mf4.this.f(ob4Var);
                return f;
            }
        });
        if (iterable.iterator().hasNext()) {
            if (nb4Var == null) {
                z12.a("Uploader", "Unknown backend for %s, deleting event batch for it...", ob4Var);
                b = BackendResponse.a();
            } else {
                ArrayList arrayList = new ArrayList();
                for (nq2 nq2Var : iterable) {
                    arrayList.add(nq2Var.b());
                }
                b = nb4Var.b(c.a().b(arrayList).c(ob4Var.c()).a());
            }
            final BackendResponse backendResponse = b;
            this.f.a(new l24.a() { // from class: kf4
                @Override // defpackage.l24.a
                public final Object execute() {
                    Object g;
                    g = mf4.this.g(backendResponse, iterable, ob4Var, i);
                    return g;
                }
            });
        }
    }

    public void k(final ob4 ob4Var, final int i, final Runnable runnable) {
        this.e.execute(new Runnable() { // from class: lf4
            @Override // java.lang.Runnable
            public final void run() {
                mf4.this.i(ob4Var, i, runnable);
            }
        });
    }
}
