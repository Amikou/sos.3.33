package defpackage;

import com.google.android.gms.measurement.internal.o;
import java.net.URL;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: kp5  reason: default package */
/* loaded from: classes.dex */
public final class kp5 implements Runnable {
    public final URL a;
    public final String f0;
    public final /* synthetic */ o g0;
    public final zj5 h0;

    public kp5(o oVar, String str, URL url, byte[] bArr, Map map, zj5 zj5Var, byte[] bArr2) {
        this.g0 = oVar;
        zt2.f(str);
        zt2.j(url);
        zt2.j(zj5Var);
        this.a = url;
        this.h0 = zj5Var;
        this.f0 = str;
    }

    public final /* synthetic */ void a(int i, Exception exc, byte[] bArr, Map map) {
        this.h0.a(this.f0, i, exc, bArr, map);
    }

    public final void b(final int i, final Exception exc, final byte[] bArr, final Map<String, List<String>> map) {
        this.g0.a.q().p(new Runnable(this, i, exc, bArr, map) { // from class: ip5
            public final kp5 a;
            public final int f0;
            public final Exception g0;
            public final byte[] h0;
            public final Map i0;

            {
                this.a = this;
                this.f0 = i;
                this.g0 = exc;
                this.h0 = bArr;
                this.i0 = map;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.a.a(this.f0, this.g0, this.h0, this.i0);
            }
        });
    }

    /* JADX WARN: Removed duplicated region for block: B:38:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0071  */
    @Override // java.lang.Runnable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void run() {
        /*
            r10 = this;
            com.google.android.gms.measurement.internal.o r0 = r10.g0
            r0.d()
            r0 = 0
            r1 = 0
            com.google.android.gms.measurement.internal.o r2 = r10.g0     // Catch: java.lang.Throwable -> L5c java.io.IOException -> L6a
            java.net.URL r3 = r10.a     // Catch: java.lang.Throwable -> L5c java.io.IOException -> L6a
            java.net.HttpURLConnection r2 = r2.l(r3)     // Catch: java.lang.Throwable -> L5c java.io.IOException -> L6a
            int r3 = r2.getResponseCode()     // Catch: java.lang.Throwable -> L50 java.io.IOException -> L56
            java.util.Map r4 = r2.getHeaderFields()     // Catch: java.lang.Throwable -> L4a java.io.IOException -> L4d
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch: java.lang.Throwable -> L3e
            r5.<init>()     // Catch: java.lang.Throwable -> L3e
            java.io.InputStream r6 = r2.getInputStream()     // Catch: java.lang.Throwable -> L3e
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch: java.lang.Throwable -> L3c
        L24:
            int r8 = r6.read(r7)     // Catch: java.lang.Throwable -> L3c
            if (r8 <= 0) goto L2e
            r5.write(r7, r0, r8)     // Catch: java.lang.Throwable -> L3c
            goto L24
        L2e:
            byte[] r0 = r5.toByteArray()     // Catch: java.lang.Throwable -> L3c
            r6.close()     // Catch: java.lang.Throwable -> L46 java.io.IOException -> L48
            r2.disconnect()
            r10.b(r3, r1, r0, r4)
            return
        L3c:
            r0 = move-exception
            goto L40
        L3e:
            r0 = move-exception
            r6 = r1
        L40:
            if (r6 == 0) goto L45
            r6.close()     // Catch: java.lang.Throwable -> L46 java.io.IOException -> L48
        L45:
            throw r0     // Catch: java.lang.Throwable -> L46 java.io.IOException -> L48
        L46:
            r0 = move-exception
            goto L61
        L48:
            r0 = move-exception
            goto L6f
        L4a:
            r0 = move-exception
            r4 = r1
            goto L61
        L4d:
            r0 = move-exception
            r4 = r1
            goto L6f
        L50:
            r3 = move-exception
            r4 = r1
            r9 = r3
            r3 = r0
            r0 = r9
            goto L61
        L56:
            r3 = move-exception
            r4 = r1
            r9 = r3
            r3 = r0
            r0 = r9
            goto L6f
        L5c:
            r2 = move-exception
            r3 = r0
            r4 = r1
            r0 = r2
            r2 = r4
        L61:
            if (r2 == 0) goto L66
            r2.disconnect()
        L66:
            r10.b(r3, r1, r1, r4)
            throw r0
        L6a:
            r2 = move-exception
            r3 = r0
            r4 = r1
            r0 = r2
            r2 = r4
        L6f:
            if (r2 == 0) goto L74
            r2.disconnect()
        L74:
            r10.b(r3, r0, r1, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.kp5.run():void");
    }
}
