package defpackage;

/* compiled from: SwapAnimationValue.java */
/* renamed from: ax3  reason: default package */
/* loaded from: classes2.dex */
public class ax3 implements wg4 {
    public int a;
    public int b;

    public int a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public void c(int i) {
        this.a = i;
    }

    public void d(int i) {
        this.b = i;
    }
}
