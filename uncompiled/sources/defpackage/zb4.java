package defpackage;

/* compiled from: TrimmedThrowableData.java */
/* renamed from: zb4  reason: default package */
/* loaded from: classes2.dex */
public class zb4 {
    public final String a;
    public final String b;
    public final StackTraceElement[] c;
    public final zb4 d;

    public zb4(Throwable th, is3 is3Var) {
        this.a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = is3Var.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new zb4(cause, is3Var) : null;
    }
}
