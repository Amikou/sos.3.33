package defpackage;

import android.os.SystemClock;

/* compiled from: UptimeClock.java */
/* renamed from: of4  reason: default package */
/* loaded from: classes.dex */
public class of4 implements qz {
    @Override // defpackage.qz
    public long a() {
        return SystemClock.elapsedRealtime();
    }
}
