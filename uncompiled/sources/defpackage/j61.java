package defpackage;

import androidx.media3.common.v;
import androidx.media3.exoplayer.source.chunk.MediaChunkIterator;
import java.util.List;

/* compiled from: FixedTrackSelection.java */
/* renamed from: j61  reason: default package */
/* loaded from: classes.dex */
public final class j61 extends ao {
    public final int g;
    public final Object h;

    public j61(v vVar, int i, int i2) {
        this(vVar, i, i2, 0, null);
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public void a(long j, long j2, long j3, List<? extends s52> list, MediaChunkIterator[] mediaChunkIteratorArr) {
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public int d() {
        return 0;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public int o() {
        return this.g;
    }

    @Override // androidx.media3.exoplayer.trackselection.c
    public Object q() {
        return this.h;
    }

    public j61(v vVar, int i, int i2, int i3, Object obj) {
        super(vVar, new int[]{i}, i2);
        this.g = i3;
        this.h = obj;
    }
}
