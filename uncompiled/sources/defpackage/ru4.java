package defpackage;

import com.google.android.play.core.internal.d;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: ru4  reason: default package */
/* loaded from: classes2.dex */
public final class ru4 implements ku4 {
    public final /* synthetic */ int a = 0;

    public ru4() {
    }

    public ru4(byte[] bArr) {
    }

    @Override // defpackage.ku4
    public final Object[] a(Object obj, ArrayList arrayList, File file, ArrayList arrayList2) {
        return (Object[]) (this.a != 0 ? d.b(obj, "makeDexElements", Object[].class, ArrayList.class, arrayList, File.class, file, ArrayList.class, arrayList2) : d.b(obj, "makePathElements", Object[].class, List.class, arrayList, File.class, file, List.class, arrayList2));
    }
}
