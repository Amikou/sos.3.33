package defpackage;

import java.security.GeneralSecurityException;

/* compiled from: MacConfig.java */
/* renamed from: x22  reason: default package */
/* loaded from: classes2.dex */
public final class x22 {
    @Deprecated
    public static final i63 a;

    static {
        new rk1().c();
        a = i63.D();
        try {
            a();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    @Deprecated
    public static void a() throws GeneralSecurityException {
        b();
    }

    public static void b() throws GeneralSecurityException {
        rk1.m(true);
        ea.n(true);
        y22.e();
    }
}
