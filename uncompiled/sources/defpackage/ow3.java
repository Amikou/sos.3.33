package defpackage;

import android.app.ActivityManager;
import android.database.Cursor;
import android.net.Uri;

/* compiled from: SupportSQLiteCompat.java */
/* renamed from: ow3  reason: default package */
/* loaded from: classes.dex */
public final class ow3 {
    public static Uri a(Cursor cursor) {
        return cursor.getNotificationUri();
    }

    public static boolean b(ActivityManager activityManager) {
        return activityManager.isLowRamDevice();
    }
}
