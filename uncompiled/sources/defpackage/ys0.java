package defpackage;

import java.math.BigInteger;
import org.web3j.crypto.c;

/* compiled from: ECDSASignature.java */
/* renamed from: ys0  reason: default package */
/* loaded from: classes3.dex */
public class ys0 {
    public final BigInteger r;
    public final BigInteger s;

    public ys0(BigInteger bigInteger, BigInteger bigInteger2) {
        this.r = bigInteger;
        this.s = bigInteger2;
    }

    public boolean isCanonical() {
        return this.s.compareTo(c.HALF_CURVE_ORDER) <= 0;
    }

    public ys0 toCanonicalised() {
        return !isCanonical() ? new ys0(this.r, c.CURVE.d().subtract(this.s)) : this;
    }
}
