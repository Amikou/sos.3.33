package defpackage;

import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.k;
import com.google.android.play.core.assetpacks.m;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: hw4  reason: default package */
/* loaded from: classes2.dex */
public final class hw4 {
    public static final it4 d = new it4("ExtractorTaskFinder");
    public final zv4 a;
    public final c b;
    public final su4 c;

    public hw4(zv4 zv4Var, c cVar, su4 su4Var) {
        this.a = zv4Var;
        this.b = cVar;
        this.c = su4Var;
    }

    public static boolean c(vv4 vv4Var) {
        int i = vv4Var.f;
        return i == 1 || i == 2;
    }

    public final fw4 a() {
        fw4 fw4Var;
        bv4 bv4Var;
        xw4 xw4Var;
        int i;
        try {
            this.a.a();
            ArrayList arrayList = new ArrayList();
            for (tv4 tv4Var : this.a.c().values()) {
                if (k.f(tv4Var.c.c)) {
                    arrayList.add(tv4Var);
                }
            }
            if (arrayList.isEmpty()) {
                this.a.b();
                return null;
            }
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    fw4Var = null;
                    break;
                }
                tv4 tv4Var2 = (tv4) it.next();
                try {
                    c cVar = this.b;
                    rv4 rv4Var = tv4Var2.c;
                    if (cVar.y(rv4Var.a, tv4Var2.b, rv4Var.b) == tv4Var2.c.e.size()) {
                        d.a("Found final move task for session %s with pack %s.", Integer.valueOf(tv4Var2.a), tv4Var2.c.a);
                        int i2 = tv4Var2.a;
                        rv4 rv4Var2 = tv4Var2.c;
                        fw4Var = new rw4(i2, rv4Var2.a, tv4Var2.b, rv4Var2.b);
                        break;
                    }
                } catch (IOException e) {
                    throw new bj(String.format("Failed to check number of completed merges for session %s, pack %s", Integer.valueOf(tv4Var2.a), tv4Var2.c.a), e, tv4Var2.a);
                }
            }
            if (fw4Var == null) {
                Iterator it2 = arrayList.iterator();
                loop2: while (true) {
                    if (!it2.hasNext()) {
                        fw4Var = null;
                        break;
                    }
                    tv4 tv4Var3 = (tv4) it2.next();
                    if (k.f(tv4Var3.c.c)) {
                        for (vv4 vv4Var : tv4Var3.c.e) {
                            c cVar2 = this.b;
                            rv4 rv4Var3 = tv4Var3.c;
                            if (cVar2.w(rv4Var3.a, tv4Var3.b, rv4Var3.b, vv4Var.a).exists()) {
                                d.a("Found merge task for session %s with pack %s and slice %s.", Integer.valueOf(tv4Var3.a), tv4Var3.c.a, vv4Var.a);
                                int i3 = tv4Var3.a;
                                rv4 rv4Var4 = tv4Var3.c;
                                fw4Var = new ow4(i3, rv4Var4.a, tv4Var3.b, rv4Var4.b, vv4Var.a);
                                break loop2;
                            }
                        }
                        continue;
                    }
                }
                if (fw4Var == null) {
                    Iterator it3 = arrayList.iterator();
                    loop4: while (true) {
                        if (!it3.hasNext()) {
                            fw4Var = null;
                            break;
                        }
                        tv4 tv4Var4 = (tv4) it3.next();
                        if (k.f(tv4Var4.c.c)) {
                            for (vv4 vv4Var2 : tv4Var4.c.e) {
                                if (b(tv4Var4, vv4Var2)) {
                                    c cVar3 = this.b;
                                    rv4 rv4Var5 = tv4Var4.c;
                                    if (cVar3.v(rv4Var5.a, tv4Var4.b, rv4Var5.b, vv4Var2.a).exists()) {
                                        d.a("Found verify task for session %s with pack %s and slice %s.", Integer.valueOf(tv4Var4.a), tv4Var4.c.a, vv4Var2.a);
                                        int i4 = tv4Var4.a;
                                        rv4 rv4Var6 = tv4Var4.c;
                                        fw4Var = new ex4(i4, rv4Var6.a, tv4Var4.b, rv4Var6.b, vv4Var2.a, vv4Var2.b);
                                        break loop4;
                                    }
                                }
                            }
                            continue;
                        }
                    }
                    if (fw4Var == null) {
                        Iterator it4 = arrayList.iterator();
                        loop6: while (true) {
                            if (!it4.hasNext()) {
                                bv4Var = null;
                                break;
                            }
                            tv4 tv4Var5 = (tv4) it4.next();
                            if (k.f(tv4Var5.c.c)) {
                                for (vv4 vv4Var3 : tv4Var5.c.e) {
                                    if (!c(vv4Var3)) {
                                        c cVar4 = this.b;
                                        rv4 rv4Var7 = tv4Var5.c;
                                        Iterator it5 = it4;
                                        try {
                                            i = new m(cVar4, rv4Var7.a, tv4Var5.b, rv4Var7.b, vv4Var3.a).k();
                                        } catch (IOException e2) {
                                            d.b("Slice checkpoint corrupt, restarting extraction. %s", e2);
                                            i = 0;
                                        }
                                        if (i != -1 && vv4Var3.d.get(i).a) {
                                            d.a("Found extraction task using compression format %s for session %s, pack %s, slice %s, chunk %s.", Integer.valueOf(vv4Var3.e), Integer.valueOf(tv4Var5.a), tv4Var5.c.a, vv4Var3.a, Integer.valueOf(i));
                                            InputStream a = this.c.a(tv4Var5.a, tv4Var5.c.a, vv4Var3.a, i);
                                            int i5 = tv4Var5.a;
                                            rv4 rv4Var8 = tv4Var5.c;
                                            String str = rv4Var8.a;
                                            int i6 = tv4Var5.b;
                                            long j = rv4Var8.b;
                                            String str2 = vv4Var3.a;
                                            int i7 = vv4Var3.e;
                                            int size = vv4Var3.d.size();
                                            rv4 rv4Var9 = tv4Var5.c;
                                            bv4Var = new bv4(i5, str, i6, j, str2, i7, i, size, rv4Var9.d, rv4Var9.c, a);
                                            break loop6;
                                        }
                                        it4 = it5;
                                    }
                                }
                                continue;
                            }
                        }
                        if (bv4Var == null) {
                            Iterator it6 = arrayList.iterator();
                            loop8: while (true) {
                                if (!it6.hasNext()) {
                                    xw4Var = null;
                                    break;
                                }
                                tv4 tv4Var6 = (tv4) it6.next();
                                if (k.f(tv4Var6.c.c)) {
                                    for (vv4 vv4Var4 : tv4Var6.c.e) {
                                        if (c(vv4Var4) && vv4Var4.d.get(0).a && !b(tv4Var6, vv4Var4)) {
                                            d.a("Found patch slice task using patch format %s for session %s, pack %s, slice %s.", Integer.valueOf(vv4Var4.f), Integer.valueOf(tv4Var6.a), tv4Var6.c.a, vv4Var4.a);
                                            InputStream a2 = this.c.a(tv4Var6.a, tv4Var6.c.a, vv4Var4.a, 0);
                                            int i8 = tv4Var6.a;
                                            String str3 = tv4Var6.c.a;
                                            xw4Var = new xw4(i8, str3, this.b.G(str3), this.b.H(tv4Var6.c.a), tv4Var6.b, tv4Var6.c.b, vv4Var4.f, vv4Var4.a, vv4Var4.c, a2);
                                            break loop8;
                                        }
                                    }
                                    continue;
                                }
                            }
                            if (xw4Var != null) {
                                return xw4Var;
                            }
                            return null;
                        }
                        return bv4Var;
                    }
                }
            }
            return fw4Var;
        } finally {
            this.a.b();
        }
    }

    public final boolean b(tv4 tv4Var, vv4 vv4Var) {
        c cVar = this.b;
        rv4 rv4Var = tv4Var.c;
        return new m(cVar, rv4Var.a, tv4Var.b, rv4Var.b, vv4Var.a).l();
    }
}
