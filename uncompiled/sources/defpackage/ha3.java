package defpackage;

/* compiled from: SHA1.java */
/* renamed from: ha3  reason: default package */
/* loaded from: classes.dex */
public final class ha3 extends y72 {
    public int[] c;
    public int h;
    public byte[] e = new byte[20];
    public boolean f = false;
    public int[] g = new int[16];
    public int[] i = new int[5];
    public long d = 0;

    public ha3() {
        this.c = new int[5];
        this.c = new int[5];
        int i = 0;
        while (true) {
            byte[] bArr = this.e;
            if (i >= bArr.length) {
                return;
            }
            bArr[i] = 0;
            i++;
        }
    }

    @Override // defpackage.y72
    public synchronized void b(byte b) {
        int i = this.h;
        int i2 = (i & 3) * 8;
        this.d += 8;
        int[] iArr = this.g;
        int i3 = i >> 2;
        iArr[i3] = iArr[i3] & (~(255 << i2));
        int i4 = i >> 2;
        iArr[i4] = ((b & 255) << i2) | iArr[i4];
        int i5 = i + 1;
        this.h = i5;
        if (i5 == 64) {
            p();
            this.h = 0;
        }
    }

    public final void e(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        iArr[i5] = iArr[i5] + (((iArr[i3] ^ iArr[i4]) & iArr[i2]) ^ iArr[i4]) + l(i6) + 1518500249 + o(iArr[i], 5);
        iArr[i2] = o(iArr[i2], 30);
    }

    public final void f(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        iArr[i5] = iArr[i5] + (((iArr[i3] ^ iArr[i4]) & iArr[i2]) ^ iArr[i4]) + k(i6) + 1518500249 + o(iArr[i], 5);
        iArr[i2] = o(iArr[i2], 30);
    }

    public final void g(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        iArr[i5] = iArr[i5] + ((iArr[i3] ^ iArr[i2]) ^ iArr[i4]) + k(i6) + 1859775393 + o(iArr[i], 5);
        iArr[i2] = o(iArr[i2], 30);
    }

    public final void h(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        iArr[i5] = iArr[i5] + ((((iArr[i3] & iArr[i2]) | (iArr[i4] & (iArr[i2] | iArr[i3]))) + k(i6)) - 1894007588) + o(iArr[i], 5);
        iArr[i2] = o(iArr[i2], 30);
    }

    public final void i(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        iArr[i5] = iArr[i5] + ((((iArr[i3] ^ iArr[i2]) ^ iArr[i4]) + k(i6)) - 899497514) + o(iArr[i], 5);
        iArr[i2] = o(iArr[i2], 30);
    }

    public void j() {
        int i = 0;
        while (true) {
            byte[] bArr = this.e;
            if (i >= bArr.length) {
                return;
            }
            bArr[i] = 0;
            i++;
        }
    }

    public final int k(int i) {
        int i2 = i & 15;
        int[] iArr = this.g;
        int i3 = iArr[(i + 13) & 15];
        int i4 = iArr[(i + 8) & 15];
        int o = o((iArr[(i + 2) & 15] ^ (i3 ^ i4)) ^ iArr[i2], 1) & (-1);
        this.g[i2] = o;
        return o;
    }

    public final int l(int i) {
        int[] iArr = this.g;
        iArr[i] = (o(iArr[i], 24) & (-16711936)) | (o(this.g[i], 8) & 16711935);
        return this.g[i];
    }

    public void m() {
        long j;
        byte[] bArr = new byte[8];
        byte[] bArr2 = new byte[8];
        int[] iArr = new int[8];
        int[] iArr2 = new int[8];
        int[] iArr3 = new int[8];
        long[] jArr = new long[8];
        for (int i = 0; i < 8; i++) {
            int i2 = (7 - i) * 8;
            iArr[i] = i2;
            if (iArr[i] > 31) {
                iArr2[i] = ((int) this.d) >>> 31;
            } else {
                iArr2[i] = ((int) this.d) >>> iArr[i];
            }
            iArr3[i] = iArr2[i] & 255;
            jArr[i] = this.d >>> i2;
            bArr[i] = (byte) iArr3[i];
            bArr2[i] = (byte) ((j >>> i2) & 255);
        }
        b(Byte.MIN_VALUE);
        while (this.h != 56) {
            b((byte) 0);
        }
        c(bArr2);
        for (int i3 = 0; i3 < 20; i3++) {
            this.e[i3] = (byte) ((this.c[i3 >> 2] >> ((3 - (i3 & 3)) * 8)) & 255);
        }
        this.f = true;
    }

    public void n() {
        int[] iArr = this.c;
        iArr[0] = 1732584193;
        iArr[1] = -271733879;
        iArr[2] = -1732584194;
        iArr[3] = 271733878;
        iArr[4] = -1009589776;
        this.d = 0L;
        this.e = new byte[20];
        this.f = false;
        this.h = 0;
    }

    public final int o(int i, int i2) {
        return (i >>> (32 - i2)) | (i << i2);
    }

    public void p() {
        int[] iArr = this.i;
        int[] iArr2 = this.c;
        iArr[0] = iArr2[0];
        iArr[1] = iArr2[1];
        iArr[2] = iArr2[2];
        iArr[3] = iArr2[3];
        iArr[4] = iArr2[4];
        e(iArr, 0, 1, 2, 3, 4, 0);
        e(this.i, 4, 0, 1, 2, 3, 1);
        e(this.i, 3, 4, 0, 1, 2, 2);
        e(this.i, 2, 3, 4, 0, 1, 3);
        e(this.i, 1, 2, 3, 4, 0, 4);
        e(this.i, 0, 1, 2, 3, 4, 5);
        e(this.i, 4, 0, 1, 2, 3, 6);
        e(this.i, 3, 4, 0, 1, 2, 7);
        e(this.i, 2, 3, 4, 0, 1, 8);
        e(this.i, 1, 2, 3, 4, 0, 9);
        e(this.i, 0, 1, 2, 3, 4, 10);
        e(this.i, 4, 0, 1, 2, 3, 11);
        e(this.i, 3, 4, 0, 1, 2, 12);
        e(this.i, 2, 3, 4, 0, 1, 13);
        e(this.i, 1, 2, 3, 4, 0, 14);
        e(this.i, 0, 1, 2, 3, 4, 15);
        f(this.i, 4, 0, 1, 2, 3, 16);
        f(this.i, 3, 4, 0, 1, 2, 17);
        f(this.i, 2, 3, 4, 0, 1, 18);
        f(this.i, 1, 2, 3, 4, 0, 19);
        g(this.i, 0, 1, 2, 3, 4, 20);
        g(this.i, 4, 0, 1, 2, 3, 21);
        g(this.i, 3, 4, 0, 1, 2, 22);
        g(this.i, 2, 3, 4, 0, 1, 23);
        g(this.i, 1, 2, 3, 4, 0, 24);
        g(this.i, 0, 1, 2, 3, 4, 25);
        g(this.i, 4, 0, 1, 2, 3, 26);
        g(this.i, 3, 4, 0, 1, 2, 27);
        g(this.i, 2, 3, 4, 0, 1, 28);
        g(this.i, 1, 2, 3, 4, 0, 29);
        g(this.i, 0, 1, 2, 3, 4, 30);
        g(this.i, 4, 0, 1, 2, 3, 31);
        g(this.i, 3, 4, 0, 1, 2, 32);
        g(this.i, 2, 3, 4, 0, 1, 33);
        g(this.i, 1, 2, 3, 4, 0, 34);
        g(this.i, 0, 1, 2, 3, 4, 35);
        g(this.i, 4, 0, 1, 2, 3, 36);
        g(this.i, 3, 4, 0, 1, 2, 37);
        g(this.i, 2, 3, 4, 0, 1, 38);
        g(this.i, 1, 2, 3, 4, 0, 39);
        h(this.i, 0, 1, 2, 3, 4, 40);
        h(this.i, 4, 0, 1, 2, 3, 41);
        h(this.i, 3, 4, 0, 1, 2, 42);
        h(this.i, 2, 3, 4, 0, 1, 43);
        h(this.i, 1, 2, 3, 4, 0, 44);
        h(this.i, 0, 1, 2, 3, 4, 45);
        h(this.i, 4, 0, 1, 2, 3, 46);
        h(this.i, 3, 4, 0, 1, 2, 47);
        h(this.i, 2, 3, 4, 0, 1, 48);
        h(this.i, 1, 2, 3, 4, 0, 49);
        h(this.i, 0, 1, 2, 3, 4, 50);
        h(this.i, 4, 0, 1, 2, 3, 51);
        h(this.i, 3, 4, 0, 1, 2, 52);
        h(this.i, 2, 3, 4, 0, 1, 53);
        h(this.i, 1, 2, 3, 4, 0, 54);
        h(this.i, 0, 1, 2, 3, 4, 55);
        h(this.i, 4, 0, 1, 2, 3, 56);
        h(this.i, 3, 4, 0, 1, 2, 57);
        h(this.i, 2, 3, 4, 0, 1, 58);
        h(this.i, 1, 2, 3, 4, 0, 59);
        i(this.i, 0, 1, 2, 3, 4, 60);
        i(this.i, 4, 0, 1, 2, 3, 61);
        i(this.i, 3, 4, 0, 1, 2, 62);
        i(this.i, 2, 3, 4, 0, 1, 63);
        i(this.i, 1, 2, 3, 4, 0, 64);
        i(this.i, 0, 1, 2, 3, 4, 65);
        i(this.i, 4, 0, 1, 2, 3, 66);
        i(this.i, 3, 4, 0, 1, 2, 67);
        i(this.i, 2, 3, 4, 0, 1, 68);
        i(this.i, 1, 2, 3, 4, 0, 69);
        i(this.i, 0, 1, 2, 3, 4, 70);
        i(this.i, 4, 0, 1, 2, 3, 71);
        i(this.i, 3, 4, 0, 1, 2, 72);
        i(this.i, 2, 3, 4, 0, 1, 73);
        i(this.i, 1, 2, 3, 4, 0, 74);
        i(this.i, 0, 1, 2, 3, 4, 75);
        i(this.i, 4, 0, 1, 2, 3, 76);
        i(this.i, 3, 4, 0, 1, 2, 77);
        i(this.i, 2, 3, 4, 0, 1, 78);
        i(this.i, 1, 2, 3, 4, 0, 79);
        int[] iArr3 = this.c;
        int i = iArr3[0];
        int[] iArr4 = this.i;
        iArr3[0] = i + iArr4[0];
        iArr3[1] = iArr3[1] + iArr4[1];
        iArr3[2] = iArr3[2] + iArr4[2];
        iArr3[3] = iArr3[3] + iArr4[3];
        iArr3[4] = iArr3[4] + iArr4[4];
    }
}
