package defpackage;

import com.google.android.gms.internal.measurement.b1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: b56  reason: default package */
/* loaded from: classes.dex */
public final class b56 {
    public b1 a;
    public Long b;
    public long c;
    public final /* synthetic */ x56 d;

    public /* synthetic */ b56(x56 x56Var, q46 q46Var) {
        this.d = x56Var;
    }

    /* JADX WARN: Code restructure failed: missing block: B:36:0x00fa, code lost:
        if (r14 == null) goto L64;
     */
    /* JADX WARN: Not initialized variable reg: 14, insn: 0x01df: MOVE  (r5 I:??[OBJECT, ARRAY]) = (r14 I:??[OBJECT, ARRAY]), block:B:66:0x01df */
    /* JADX WARN: Removed duplicated region for block: B:68:0x01e2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.google.android.gms.internal.measurement.b1 a(java.lang.String r18, com.google.android.gms.internal.measurement.b1 r19) {
        /*
            Method dump skipped, instructions count: 585
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b56.a(java.lang.String, com.google.android.gms.internal.measurement.b1):com.google.android.gms.internal.measurement.b1");
    }
}
