package defpackage;

import net.safemoon.androidwallet.model.BalanceByBlock;
import net.safemoon.androidwallet.model.EthBasGasPrice;
import net.safemoon.androidwallet.model.EthGasPrice;
import net.safemoon.androidwallet.model.ReceiptStatus;
import net.safemoon.androidwallet.model.transaction.history.TransactionHistoryModel;
import retrofit2.b;

/* compiled from: BlockChainInterface.kt */
/* renamed from: wq  reason: default package */
/* loaded from: classes2.dex */
public interface wq {

    /* compiled from: BlockChainInterface.kt */
    /* renamed from: wq$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ b a(wq wqVar, String str, Integer num, Integer num2, String str2, String str3, int i, int i2, int i3, Object obj) {
            if (obj == null) {
                return wqVar.d(str, (i3 & 2) != 0 ? 1 : num, (i3 & 4) != 0 ? 100 : num2, str2, (i3 & 16) != 0 ? "desc" : str3, (i3 & 32) != 0 ? 0 : i, (i3 & 64) != 0 ? 999999999 : i2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getNativeTransactionData");
        }

        public static /* synthetic */ b b(wq wqVar, String str, Integer num, Integer num2, String str2, String str3, String str4, int i, int i2, int i3, Object obj) {
            if (obj == null) {
                return wqVar.e(str, (i3 & 2) != 0 ? 1 : num, (i3 & 4) != 0 ? 100 : num2, str2, str3, (i3 & 32) != 0 ? "desc" : str4, (i3 & 64) != 0 ? 0 : i, (i3 & 128) != 0 ? 999999999 : i2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getTransactionData");
        }
    }

    @ge1("/api?module=transaction&action=gettxreceiptstatus")
    b<ReceiptStatus> a(@yw2("txhash") String str, @yw2("apikey") String str2);

    @hk1({"Cache-Control: no-cache"})
    @ge1("/api?module=gastracker&action=gasoracle")
    b<EthGasPrice> b(@yw2("apikey") String str);

    @ge1("/api?module=account&action=tokenbalancehistory")
    b<BalanceByBlock> c(@yw2("contractaddress") String str, @yw2("address") String str2, @yw2("blockno") String str3, @yw2("apikey") String str4);

    @ge1("/api?module=account&action=txlist&startblock=0&endblock=99999999")
    b<TransactionHistoryModel> d(@yw2("address") String str, @yw2("page") Integer num, @yw2("offset") Integer num2, @yw2("apikey") String str2, @yw2("sort") String str3, @yw2("startblock") int i, @yw2("endblock") int i2);

    @ge1("/api?module=account&action=tokentx")
    b<TransactionHistoryModel> e(@yw2("address") String str, @yw2("page") Integer num, @yw2("offset") Integer num2, @yw2("apikey") String str2, @yw2("contractaddress") String str3, @yw2("sort") String str4, @yw2("startblock") int i, @yw2("endblock") int i2);

    @ge1("/api?module=account&action=balancehistory")
    b<BalanceByBlock> f(@yw2("address") String str, @yw2("blockno") String str2, @yw2("apikey") String str3);

    @ge1("/api?module=account&action=tokentx&page=1&offset=1")
    b<TransactionHistoryModel> g(@yw2("contractaddress") String str, @yw2("apikey") String str2);

    @hk1({"Cache-Control: no-cache"})
    @ge1("/api?module=proxy&action=eth_gasPrice")
    b<EthBasGasPrice> getGasPrice(@yw2("apikey") String str);
}
