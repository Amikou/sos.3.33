package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.ClipRecyclerView;
import net.safemoon.androidwallet.views.ClipRelativeLayout;

/* compiled from: FragmentReceiveBinding.java */
/* renamed from: ra1  reason: default package */
/* loaded from: classes2.dex */
public final class ra1 {
    public final AppBarLayout a;
    public final ImageView b;
    public final ConstraintLayout c;
    public final ClipRecyclerView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;

    public ra1(ClipRelativeLayout clipRelativeLayout, AppBarLayout appBarLayout, CollapsingToolbarLayout collapsingToolbarLayout, ImageView imageView, ConstraintLayout constraintLayout, ClipRecyclerView clipRecyclerView, ConstraintLayout constraintLayout2, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
        this.a = appBarLayout;
        this.b = imageView;
        this.c = constraintLayout;
        this.d = clipRecyclerView;
        this.e = textView;
        this.f = textView3;
        this.g = textView4;
    }

    public static ra1 a(View view) {
        int i = R.id.appBar;
        AppBarLayout appBarLayout = (AppBarLayout) ai4.a(view, R.id.appBar);
        if (appBarLayout != null) {
            i = R.id.ccToolBar;
            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) ai4.a(view, R.id.ccToolBar);
            if (collapsingToolbarLayout != null) {
                i = R.id.iv_back;
                ImageView imageView = (ImageView) ai4.a(view, R.id.iv_back);
                if (imageView != null) {
                    i = R.id.lWalletBalance;
                    ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.lWalletBalance);
                    if (constraintLayout != null) {
                        i = R.id.rvMyTokenList;
                        ClipRecyclerView clipRecyclerView = (ClipRecyclerView) ai4.a(view, R.id.rvMyTokenList);
                        if (clipRecyclerView != null) {
                            i = R.id.topBar;
                            ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.topBar);
                            if (constraintLayout2 != null) {
                                i = R.id.tvMainWallet;
                                TextView textView = (TextView) ai4.a(view, R.id.tvMainWallet);
                                if (textView != null) {
                                    i = R.id.tvSend;
                                    TextView textView2 = (TextView) ai4.a(view, R.id.tvSend);
                                    if (textView2 != null) {
                                        i = R.id.tvWalletBlnc;
                                        TextView textView3 = (TextView) ai4.a(view, R.id.tvWalletBlnc);
                                        if (textView3 != null) {
                                            i = R.id.txtSymbol;
                                            TextView textView4 = (TextView) ai4.a(view, R.id.txtSymbol);
                                            if (textView4 != null) {
                                                return new ra1((ClipRelativeLayout) view, appBarLayout, collapsingToolbarLayout, imageView, constraintLayout, clipRecyclerView, constraintLayout2, textView, textView2, textView3, textView4);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
