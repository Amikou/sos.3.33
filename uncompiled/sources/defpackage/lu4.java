package defpackage;

import com.google.android.play.core.assetpacks.AssetPackState;
import java.util.Map;

/* renamed from: lu4  reason: default package */
/* loaded from: classes2.dex */
public final class lu4 extends li {
    public final long a;
    public final Map<String, AssetPackState> b;

    public lu4(long j, Map<String, AssetPackState> map) {
        this.a = j;
        this.b = map;
    }

    @Override // defpackage.li
    public final Map<String, AssetPackState> e() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof li) {
            li liVar = (li) obj;
            if (this.a == liVar.f() && this.b.equals(liVar.e())) {
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.li
    public final long f() {
        return this.a;
    }

    public final int hashCode() {
        long j = this.a;
        return ((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    public final String toString() {
        long j = this.a;
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(valueOf.length() + 61);
        sb.append("AssetPackStates{totalBytes=");
        sb.append(j);
        sb.append(", packStates=");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }
}
