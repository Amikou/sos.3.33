package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: bv5  reason: default package */
/* loaded from: classes.dex */
public final class bv5 {
    public final rz a;
    public long b;

    public bv5(rz rzVar) {
        zt2.j(rzVar);
        this.a = rzVar;
    }

    public final void a() {
        this.b = this.a.b();
    }

    public final void b() {
        this.b = 0L;
    }

    public final boolean c(long j) {
        return this.b == 0 || this.a.b() - this.b >= 3600000;
    }
}
