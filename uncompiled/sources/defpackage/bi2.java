package defpackage;

import net.safemoon.androidwallet.fragments.NotificationHistoryFragment;

/* renamed from: bi2  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class bi2 implements rc1 {
    public final /* synthetic */ NotificationHistoryFragment a;

    public /* synthetic */ bi2(NotificationHistoryFragment notificationHistoryFragment) {
        this.a = notificationHistoryFragment;
    }

    @Override // defpackage.rc1
    public final Object invoke() {
        return NotificationHistoryFragment.x(this.a);
    }
}
