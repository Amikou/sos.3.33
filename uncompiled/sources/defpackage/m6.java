package defpackage;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* compiled from: ActionProvider.java */
/* renamed from: m6  reason: default package */
/* loaded from: classes.dex */
public abstract class m6 {
    public a a;
    public b b;

    /* compiled from: ActionProvider.java */
    /* renamed from: m6$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(boolean z);
    }

    /* compiled from: ActionProvider.java */
    /* renamed from: m6$b */
    /* loaded from: classes.dex */
    public interface b {
        void onActionProviderVisibilityChanged(boolean z);
    }

    public m6(Context context) {
    }

    public boolean a() {
        return false;
    }

    public boolean b() {
        return true;
    }

    public abstract View c();

    public View d(MenuItem menuItem) {
        return c();
    }

    public boolean e() {
        return false;
    }

    public void f(SubMenu subMenu) {
    }

    public boolean g() {
        return false;
    }

    public void h() {
        this.b = null;
        this.a = null;
    }

    public void i(a aVar) {
        this.a = aVar;
    }

    public void j(b bVar) {
        if (this.b != null && bVar != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this ");
            sb.append(getClass().getSimpleName());
            sb.append(" instance while it is still in use somewhere else?");
        }
        this.b = bVar;
    }

    public void k(boolean z) {
        a aVar = this.a;
        if (aVar != null) {
            aVar.a(z);
        }
    }
}
