package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ReflectionDataSourceProvider.kt */
/* renamed from: e53  reason: default package */
/* loaded from: classes2.dex */
public final class e53 {
    public static final e53 a = new e53();
    public static ReflectionDataSource b;

    public final ReflectionDataSource a(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new ReflectionDataSource(ApplicationRoomDatabase.k.c(ApplicationRoomDatabase.n, context, null, 2, null).X());
                }
                te4 te4Var = te4.a;
            }
        }
        ReflectionDataSource reflectionDataSource = b;
        fs1.d(reflectionDataSource);
        return reflectionDataSource;
    }

    public final void b() {
        b = null;
    }
}
