package defpackage;

import com.google.android.play.core.install.InstallState;
import java.util.Objects;

/* renamed from: xs4  reason: default package */
/* loaded from: classes2.dex */
public final class xs4 extends InstallState {
    public final int a;
    public final long b;
    public final long c;
    public final int d;
    public final String e;

    public xs4(int i, long j, long j2, int i2, String str) {
        this.a = i;
        this.b = j;
        this.c = j2;
        this.d = i2;
        Objects.requireNonNull(str, "Null packageName");
        this.e = str;
    }

    @Override // com.google.android.play.core.install.InstallState
    public final long b() {
        return this.b;
    }

    @Override // com.google.android.play.core.install.InstallState
    public final int c() {
        return this.d;
    }

    @Override // com.google.android.play.core.install.InstallState
    public final int d() {
        return this.a;
    }

    @Override // com.google.android.play.core.install.InstallState
    public final String e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof InstallState) {
            InstallState installState = (InstallState) obj;
            if (this.a == installState.d() && this.b == installState.b() && this.c == installState.f() && this.d == installState.c() && this.e.equals(installState.e())) {
                return true;
            }
        }
        return false;
    }

    @Override // com.google.android.play.core.install.InstallState
    public final long f() {
        return this.c;
    }

    public final int hashCode() {
        int i = this.a;
        long j = this.b;
        long j2 = this.c;
        return ((((((((i ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.d) * 1000003) ^ this.e.hashCode();
    }

    public final String toString() {
        int i = this.a;
        long j = this.b;
        long j2 = this.c;
        int i2 = this.d;
        String str = this.e;
        StringBuilder sb = new StringBuilder(str.length() + 164);
        sb.append("InstallState{installStatus=");
        sb.append(i);
        sb.append(", bytesDownloaded=");
        sb.append(j);
        sb.append(", totalBytesToDownload=");
        sb.append(j2);
        sb.append(", installErrorCode=");
        sb.append(i2);
        sb.append(", packageName=");
        sb.append(str);
        sb.append("}");
        return sb.toString();
    }
}
