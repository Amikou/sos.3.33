package defpackage;

import java.math.BigInteger;

/* renamed from: xe3  reason: default package */
/* loaded from: classes2.dex */
public class xe3 {
    public static final int[] a = {-6803, -2, -1, -1, -1, -1, -1};
    public static final int[] b = {46280809, 13606, 1, 0, 0, 0, 0, -13606, -3, -1, -1, -1, -1, -1};
    public static final int[] c = {-46280809, -13607, -2, -1, -1, -1, -1, 13605, 2};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (dd2.a(iArr, iArr2, iArr3) != 0 || (iArr3[6] == -1 && dd2.j(iArr3, a))) {
            kd2.b(7, 6803, iArr3);
        }
    }

    public static void b(int[] iArr, int[] iArr2) {
        if (kd2.s(7, iArr, iArr2) != 0 || (iArr2[6] == -1 && dd2.j(iArr2, a))) {
            kd2.b(7, 6803, iArr2);
        }
    }

    public static int[] c(BigInteger bigInteger) {
        int[] h = dd2.h(bigInteger);
        if (h[6] == -1 && dd2.j(h, a)) {
            kd2.b(7, 6803, h);
        }
        return h;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] f = dd2.f();
        dd2.m(iArr, iArr2, f);
        g(f, iArr3);
    }

    public static void e(int[] iArr, int[] iArr2, int[] iArr3) {
        if (dd2.q(iArr, iArr2, iArr3) != 0 || (iArr3[13] == -1 && kd2.q(14, iArr3, b))) {
            int[] iArr4 = c;
            if (kd2.e(iArr4.length, iArr4, iArr3) != 0) {
                kd2.t(14, iArr3, iArr4.length);
            }
        }
    }

    public static void f(int[] iArr, int[] iArr2) {
        if (dd2.l(iArr)) {
            dd2.v(iArr2);
        } else {
            dd2.s(a, iArr, iArr2);
        }
    }

    public static void g(int[] iArr, int[] iArr2) {
        if (dd2.o(6803, dd2.n(6803, iArr, 7, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[6] == -1 && dd2.j(iArr2, a))) {
            kd2.b(7, 6803, iArr2);
        }
    }

    public static void h(int i, int[] iArr) {
        if ((i == 0 || dd2.p(6803, i, iArr, 0) == 0) && !(iArr[6] == -1 && dd2.j(iArr, a))) {
            return;
        }
        kd2.b(7, 6803, iArr);
    }

    public static void i(int[] iArr, int[] iArr2) {
        int[] f = dd2.f();
        dd2.r(iArr, f);
        g(f, iArr2);
    }

    public static void j(int[] iArr, int i, int[] iArr2) {
        int[] f = dd2.f();
        dd2.r(iArr, f);
        while (true) {
            g(f, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            dd2.r(iArr2, f);
        }
    }

    public static void k(int[] iArr, int[] iArr2, int[] iArr3) {
        if (dd2.s(iArr, iArr2, iArr3) != 0) {
            kd2.L(7, 6803, iArr3);
        }
    }

    public static void l(int[] iArr, int[] iArr2) {
        if (kd2.E(7, iArr, 0, iArr2) != 0 || (iArr2[6] == -1 && dd2.j(iArr2, a))) {
            kd2.b(7, 6803, iArr2);
        }
    }
}
