package defpackage;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: yg5  reason: default package */
/* loaded from: classes.dex */
public final class yg5 implements Runnable {
    public final wg5 a;
    public final int f0;
    public final Throwable g0;
    public final byte[] h0;
    public final String i0;
    public final Map<String, List<String>> j0;

    public /* synthetic */ yg5(String str, wg5 wg5Var, int i, Throwable th, byte[] bArr, Map map, ug5 ug5Var) {
        zt2.j(wg5Var);
        this.a = wg5Var;
        this.f0 = i;
        this.g0 = th;
        this.h0 = bArr;
        this.i0 = str;
        this.j0 = map;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.a.a(this.i0, this.f0, this.g0, this.h0, this.j0);
    }
}
