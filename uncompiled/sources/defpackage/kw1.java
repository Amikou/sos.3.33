package defpackage;

import com.fasterxml.jackson.databind.deser.i;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/* compiled from: JsonValueInstantiator.java */
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: kw1  reason: default package */
/* loaded from: classes.dex */
public @interface kw1 {
    Class<? extends i> value();
}
