package defpackage;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Objects;
import kotlin.Pair;
import kotlin.Result;
import kotlinx.coroutines.internal.ExceptionsConstuctorKt;

/* compiled from: StackTraceRecovery.kt */
/* renamed from: hs3  reason: default package */
/* loaded from: classes2.dex */
public final class hs3 {
    public static final String a;
    public static final String b;

    static {
        Object m52constructorimpl;
        Object m52constructorimpl2;
        try {
            Result.a aVar = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(Class.forName("kotlin.coroutines.jvm.internal.BaseContinuationImpl").getCanonicalName());
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(o83.a(th));
        }
        if (Result.m55exceptionOrNullimpl(m52constructorimpl) != null) {
            m52constructorimpl = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        a = (String) m52constructorimpl;
        try {
            Result.a aVar3 = Result.Companion;
            m52constructorimpl2 = Result.m52constructorimpl(hs3.class.getCanonicalName());
        } catch (Throwable th2) {
            Result.a aVar4 = Result.Companion;
            m52constructorimpl2 = Result.m52constructorimpl(o83.a(th2));
        }
        if (Result.m55exceptionOrNullimpl(m52constructorimpl2) != null) {
            m52constructorimpl2 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        b = (String) m52constructorimpl2;
    }

    public static final StackTraceElement b(String str) {
        return new StackTraceElement(fs1.l("\b\b\b(", str), "\b", "\b", -1);
    }

    public static final <E extends Throwable> Pair<E, StackTraceElement[]> c(E e) {
        boolean z;
        Throwable cause = e.getCause();
        if (cause != null && fs1.b(cause.getClass(), e.getClass())) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            int length = stackTrace.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = false;
                    break;
                } else if (h(stackTrace[i])) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (z) {
                return qc4.a(cause, stackTrace);
            }
            return qc4.a(e, new StackTraceElement[0]);
        }
        return qc4.a(e, new StackTraceElement[0]);
    }

    public static final <E extends Throwable> E d(E e, E e2, ArrayDeque<StackTraceElement> arrayDeque) {
        arrayDeque.addFirst(b("Coroutine boundary"));
        StackTraceElement[] stackTrace = e.getStackTrace();
        int g = g(stackTrace, a);
        int i = 0;
        if (g == -1) {
            Object[] array = arrayDeque.toArray(new StackTraceElement[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            e2.setStackTrace((StackTraceElement[]) array);
            return e2;
        }
        StackTraceElement[] stackTraceElementArr = new StackTraceElement[arrayDeque.size() + g];
        if (g > 0) {
            int i2 = 0;
            while (true) {
                int i3 = i2 + 1;
                stackTraceElementArr[i2] = stackTrace[i2];
                if (i3 >= g) {
                    break;
                }
                i2 = i3;
            }
        }
        Iterator<StackTraceElement> it = arrayDeque.iterator();
        while (it.hasNext()) {
            stackTraceElementArr[i + g] = it.next();
            i++;
        }
        e2.setStackTrace(stackTraceElementArr);
        return e2;
    }

    public static final ArrayDeque<StackTraceElement> e(e90 e90Var) {
        ArrayDeque<StackTraceElement> arrayDeque = new ArrayDeque<>();
        StackTraceElement stackTraceElement = e90Var.getStackTraceElement();
        if (stackTraceElement != null) {
            arrayDeque.add(stackTraceElement);
        }
        while (true) {
            e90Var = e90Var.getCallerFrame();
            if (e90Var == null) {
                return arrayDeque;
            }
            StackTraceElement stackTraceElement2 = e90Var.getStackTraceElement();
            if (stackTraceElement2 != null) {
                arrayDeque.add(stackTraceElement2);
            }
        }
    }

    public static final boolean f(StackTraceElement stackTraceElement, StackTraceElement stackTraceElement2) {
        return stackTraceElement.getLineNumber() == stackTraceElement2.getLineNumber() && fs1.b(stackTraceElement.getMethodName(), stackTraceElement2.getMethodName()) && fs1.b(stackTraceElement.getFileName(), stackTraceElement2.getFileName()) && fs1.b(stackTraceElement.getClassName(), stackTraceElement2.getClassName());
    }

    public static final int g(StackTraceElement[] stackTraceElementArr, String str) {
        int length = stackTraceElementArr.length;
        for (int i = 0; i < length; i++) {
            if (fs1.b(str, stackTraceElementArr[i].getClassName())) {
                return i;
            }
        }
        return -1;
    }

    public static final boolean h(StackTraceElement stackTraceElement) {
        return dv3.H(stackTraceElement.getClassName(), "\b\b\b", false, 2, null);
    }

    public static final void i(StackTraceElement[] stackTraceElementArr, ArrayDeque<StackTraceElement> arrayDeque) {
        int length = stackTraceElementArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (h(stackTraceElementArr[i])) {
                break;
            } else {
                i++;
            }
        }
        int i2 = i + 1;
        int length2 = stackTraceElementArr.length - 1;
        if (i2 > length2) {
            return;
        }
        while (true) {
            int i3 = length2 - 1;
            if (f(stackTraceElementArr[length2], arrayDeque.getLast())) {
                arrayDeque.removeLast();
            }
            arrayDeque.addFirst(stackTraceElementArr[length2]);
            if (length2 == i2) {
                return;
            }
            length2 = i3;
        }
    }

    public static final <E extends Throwable> E j(E e, e90 e90Var) {
        Pair c = c(e);
        Throwable th = (Throwable) c.component1();
        StackTraceElement[] stackTraceElementArr = (StackTraceElement[]) c.component2();
        Throwable e2 = ExceptionsConstuctorKt.e(th);
        if (e2 != null && fs1.b(e2.getMessage(), th.getMessage())) {
            ArrayDeque<StackTraceElement> e3 = e(e90Var);
            if (e3.isEmpty()) {
                return e;
            }
            if (th != e) {
                i(stackTraceElementArr, e3);
            }
            return (E) d(th, e2, e3);
        }
        return e;
    }

    public static final <E extends Throwable> E k(E e) {
        Throwable e2;
        return (ze0.d() && (e2 = ExceptionsConstuctorKt.e(e)) != null) ? (E) l(e2) : e;
    }

    public static final <E extends Throwable> E l(E e) {
        StackTraceElement stackTraceElement;
        StackTraceElement[] stackTrace = e.getStackTrace();
        int length = stackTrace.length;
        int g = g(stackTrace, b);
        int i = g + 1;
        int g2 = g(stackTrace, a);
        int i2 = (length - g) - (g2 == -1 ? 0 : length - g2);
        StackTraceElement[] stackTraceElementArr = new StackTraceElement[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            if (i3 == 0) {
                stackTraceElement = b("Coroutine boundary");
            } else {
                stackTraceElement = stackTrace[(i + i3) - 1];
            }
            stackTraceElementArr[i3] = stackTraceElement;
        }
        e.setStackTrace(stackTraceElementArr);
        return e;
    }

    public static final <E extends Throwable> E m(E e) {
        E e2 = (E) e.getCause();
        if (e2 != null && fs1.b(e2.getClass(), e.getClass())) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            int length = stackTrace.length;
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (h(stackTrace[i])) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (z) {
                return e2;
            }
        }
        return e;
    }
}
