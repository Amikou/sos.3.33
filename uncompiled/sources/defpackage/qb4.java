package defpackage;

import java.util.Set;

/* compiled from: TransportFactoryImpl.java */
/* renamed from: qb4  reason: default package */
/* loaded from: classes.dex */
public final class qb4 implements pb4 {
    public final Set<hv0> a;
    public final ob4 b;
    public final tb4 c;

    public qb4(Set<hv0> set, ob4 ob4Var, tb4 tb4Var) {
        this.a = set;
        this.b = ob4Var;
        this.c = tb4Var;
    }

    @Override // defpackage.pb4
    public <T> mb4<T> a(String str, Class<T> cls, hv0 hv0Var, fb4<T, byte[]> fb4Var) {
        if (this.a.contains(hv0Var)) {
            return new sb4(this.b, str, hv0Var, fb4Var, this.c);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", hv0Var, this.a));
    }
}
