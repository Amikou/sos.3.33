package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: Produce.kt */
/* renamed from: cv2 */
/* loaded from: classes2.dex */
public final class cv2 {
    public static final <E> f43<E> a(c90 c90Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow, CoroutineStart coroutineStart, tc1<? super Throwable, te4> tc1Var, hd1<? super kv2<? super E>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        gv2 gv2Var = new gv2(x80.c(c90Var, coroutineContext), rx.b(i, bufferOverflow, null, 4, null));
        if (tc1Var != null) {
            gv2Var.z(tc1Var);
        }
        gv2Var.K0(coroutineStart, gv2Var, hd1Var);
        return gv2Var;
    }

    public static /* synthetic */ f43 b(c90 c90Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow, CoroutineStart coroutineStart, tc1 tc1Var, hd1 hd1Var, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        CoroutineContext coroutineContext2 = coroutineContext;
        if ((i2 & 2) != 0) {
            i = 0;
        }
        int i3 = i;
        if ((i2 & 4) != 0) {
            bufferOverflow = BufferOverflow.SUSPEND;
        }
        BufferOverflow bufferOverflow2 = bufferOverflow;
        if ((i2 & 8) != 0) {
            coroutineStart = CoroutineStart.DEFAULT;
        }
        CoroutineStart coroutineStart2 = coroutineStart;
        if ((i2 & 16) != 0) {
            tc1Var = null;
        }
        return a(c90Var, coroutineContext2, i3, bufferOverflow2, coroutineStart2, tc1Var, hd1Var);
    }
}
