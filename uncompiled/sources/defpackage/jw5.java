package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.measurement.internal.zzkq;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jw5  reason: default package */
/* loaded from: classes.dex */
public final class jw5 implements Parcelable.Creator<zzkq> {
    public static void a(zzkq zzkqVar, Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.m(parcel, 1, zzkqVar.a);
        yb3.s(parcel, 2, zzkqVar.f0, false);
        yb3.o(parcel, 3, zzkqVar.g0);
        yb3.p(parcel, 4, zzkqVar.h0, false);
        yb3.k(parcel, 5, null, false);
        yb3.s(parcel, 6, zzkqVar.i0, false);
        yb3.s(parcel, 7, zzkqVar.j0, false);
        yb3.i(parcel, 8, zzkqVar.k0, false);
        yb3.b(parcel, a);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzkq createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        int i = 0;
        long j = 0;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 1:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 2:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 3:
                    j = SafeParcelReader.F(parcel, C);
                    break;
                case 4:
                    l = SafeParcelReader.G(parcel, C);
                    break;
                case 5:
                    f = SafeParcelReader.B(parcel, C);
                    break;
                case 6:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 7:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 8:
                    d = SafeParcelReader.z(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzkq(i, str, j, l, f, str2, str3, d);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzkq[] newArray(int i) {
        return new zzkq[i];
    }
}
