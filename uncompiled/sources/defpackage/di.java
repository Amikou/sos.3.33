package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.facebook.imagepipeline.platform.a;

/* compiled from: ArtDecoder.java */
@TargetApi(21)
/* renamed from: di  reason: default package */
/* loaded from: classes.dex */
public class di extends a {
    public di(iq iqVar, int i, it2 it2Var) {
        super(iqVar, i, it2Var);
    }

    @Override // com.facebook.imagepipeline.platform.a
    public int e(int i, int i2, BitmapFactory.Options options) {
        return rq.d(i, i2, (Bitmap.Config) du2.e(options.inPreferredConfig));
    }
}
