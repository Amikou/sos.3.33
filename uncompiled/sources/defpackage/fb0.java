package defpackage;

import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;

/* compiled from: CryptoPriceAlertFragment.kt */
/* renamed from: fb0  reason: default package */
/* loaded from: classes2.dex */
public final class fb0 {
    public static final List<gt> a;
    public static final List<gt> b;
    public static final List<gt> c;

    static {
        ArrayList arrayList = new ArrayList();
        Integer valueOf = Integer.valueOf((int) R.string.common_asking_title);
        arrayList.add(new gt(valueOf, null, -3, false, 2, null));
        arrayList.add(new gt(Integer.valueOf((int) R.string.ca_alert_delete), null, 100, true, 2, null));
        arrayList.add(new gt(Integer.valueOf((int) R.string.ca_alert_delete_all), null, 101, true, 2, null));
        Integer valueOf2 = Integer.valueOf((int) R.string.cancel);
        arrayList.add(new gt(valueOf2, null, -1, true, 2, null));
        a = j20.k0(arrayList);
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new gt(Integer.valueOf((int) R.string.ca_alert_unsaved_changes_title), null, -3, false, 2, null));
        arrayList2.add(new gt(Integer.valueOf((int) R.string.ca_alert_unsaved_changes_continue_editing), null, 102, true, 2, null));
        arrayList2.add(new gt(Integer.valueOf((int) R.string.ca_alert_unsaved_changes_continue_proceed), null, 103, true, 2, null));
        b = j20.k0(arrayList2);
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new gt(valueOf, null, -3, false, 2, null));
        arrayList3.add(new gt(Integer.valueOf((int) R.string.ca_alert_save_settings), null, 104, true, 2, null));
        arrayList3.add(new gt(Integer.valueOf((int) R.string.ca_alert_save_continue_editing), null, 105, true, 2, null));
        arrayList3.add(new gt(valueOf2, null, -1, true, 2, null));
        c = j20.k0(arrayList3);
    }

    public static final List<gt> a() {
        return a;
    }

    public static final List<gt> b() {
        return c;
    }

    public static final List<gt> c() {
        return b;
    }
}
