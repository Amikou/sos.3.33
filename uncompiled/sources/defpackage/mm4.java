package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletDao.kt */
/* renamed from: mm4  reason: default package */
/* loaded from: classes2.dex */
public interface mm4 {
    Object a(long j, int i, q70<? super te4> q70Var);

    LiveData<List<Wallet>> b();

    Object c(Wallet wallet2, q70<? super te4> q70Var);

    Object d(long j, String str, String str2, String str3, String str4, int i, String str5, q70<? super te4> q70Var);

    Object e(long j, int i, q70<? super te4> q70Var);

    Object f(q70<? super List<Wallet>> q70Var);

    Object g(long j, String str, int i, q70<? super te4> q70Var);

    Object h(long j, q70<? super Wallet> q70Var);

    Object i(Wallet wallet2, q70<? super Long> q70Var);

    Object j(String str, q70<? super Boolean> q70Var);

    Object k(long j, String str, q70<? super te4> q70Var);

    LiveData<Integer> l();
}
