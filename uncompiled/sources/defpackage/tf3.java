package defpackage;

import defpackage.pt0;

/* renamed from: tf3  reason: default package */
/* loaded from: classes2.dex */
public class tf3 extends pt0.c {
    public tf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public tf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public tf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        sf3 sf3Var = (sf3) this.c;
        if (sf3Var.i()) {
            return i.v();
        }
        sf3 sf3Var2 = (sf3) this.b;
        sf3 sf3Var3 = (sf3) this.d[0];
        int[] j = kd2.j(17);
        int[] j2 = kd2.j(17);
        int[] j3 = kd2.j(17);
        rf3.j(sf3Var.f, j3);
        int[] j4 = kd2.j(17);
        rf3.j(j3, j4);
        boolean h = sf3Var3.h();
        int[] iArr = sf3Var3.f;
        if (!h) {
            rf3.j(iArr, j2);
            iArr = j2;
        }
        rf3.l(sf3Var2.f, iArr, j);
        rf3.a(sf3Var2.f, iArr, j2);
        rf3.f(j2, j, j2);
        kd2.c(17, j2, j2, j2);
        rf3.i(j2);
        rf3.f(j3, sf3Var2.f, j3);
        kd2.G(17, j3, 2, 0);
        rf3.i(j3);
        kd2.H(17, j4, 3, 0, j);
        rf3.i(j);
        sf3 sf3Var4 = new sf3(j4);
        rf3.j(j2, sf3Var4.f);
        int[] iArr2 = sf3Var4.f;
        rf3.l(iArr2, j3, iArr2);
        int[] iArr3 = sf3Var4.f;
        rf3.l(iArr3, j3, iArr3);
        sf3 sf3Var5 = new sf3(j3);
        rf3.l(j3, sf3Var4.f, sf3Var5.f);
        int[] iArr4 = sf3Var5.f;
        rf3.f(iArr4, j2, iArr4);
        int[] iArr5 = sf3Var5.f;
        rf3.l(iArr5, j, iArr5);
        sf3 sf3Var6 = new sf3(j2);
        rf3.m(sf3Var.f, sf3Var6.f);
        if (!h) {
            int[] iArr6 = sf3Var6.f;
            rf3.f(iArr6, sf3Var3.f, iArr6);
        }
        return new tf3(i, sf3Var4, sf3Var5, new ct0[]{sf3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        sf3 sf3Var = (sf3) this.b;
        sf3 sf3Var2 = (sf3) this.c;
        sf3 sf3Var3 = (sf3) pt0Var.q();
        sf3 sf3Var4 = (sf3) pt0Var.r();
        sf3 sf3Var5 = (sf3) this.d[0];
        sf3 sf3Var6 = (sf3) pt0Var.s(0);
        int[] j = kd2.j(17);
        int[] j2 = kd2.j(17);
        int[] j3 = kd2.j(17);
        int[] j4 = kd2.j(17);
        boolean h = sf3Var5.h();
        if (h) {
            iArr = sf3Var3.f;
            iArr2 = sf3Var4.f;
        } else {
            rf3.j(sf3Var5.f, j3);
            rf3.f(j3, sf3Var3.f, j2);
            rf3.f(j3, sf3Var5.f, j3);
            rf3.f(j3, sf3Var4.f, j3);
            iArr = j2;
            iArr2 = j3;
        }
        boolean h2 = sf3Var6.h();
        if (h2) {
            iArr3 = sf3Var.f;
            iArr4 = sf3Var2.f;
        } else {
            rf3.j(sf3Var6.f, j4);
            rf3.f(j4, sf3Var.f, j);
            rf3.f(j4, sf3Var6.f, j4);
            rf3.f(j4, sf3Var2.f, j4);
            iArr3 = j;
            iArr4 = j4;
        }
        int[] j5 = kd2.j(17);
        rf3.l(iArr3, iArr, j5);
        rf3.l(iArr4, iArr2, j2);
        if (kd2.w(17, j5)) {
            return kd2.w(17, j2) ? J() : i.v();
        }
        rf3.j(j5, j3);
        int[] j6 = kd2.j(17);
        rf3.f(j3, j5, j6);
        rf3.f(j3, iArr3, j3);
        rf3.f(iArr4, j6, j);
        sf3 sf3Var7 = new sf3(j4);
        rf3.j(j2, sf3Var7.f);
        int[] iArr5 = sf3Var7.f;
        rf3.a(iArr5, j6, iArr5);
        int[] iArr6 = sf3Var7.f;
        rf3.l(iArr6, j3, iArr6);
        int[] iArr7 = sf3Var7.f;
        rf3.l(iArr7, j3, iArr7);
        sf3 sf3Var8 = new sf3(j6);
        rf3.l(j3, sf3Var7.f, sf3Var8.f);
        rf3.f(sf3Var8.f, j2, j2);
        rf3.l(j2, j, sf3Var8.f);
        sf3 sf3Var9 = new sf3(j5);
        if (!h) {
            int[] iArr8 = sf3Var9.f;
            rf3.f(iArr8, sf3Var5.f, iArr8);
        }
        if (!h2) {
            int[] iArr9 = sf3Var9.f;
            rf3.f(iArr9, sf3Var6.f, iArr9);
        }
        return new tf3(i, sf3Var7, sf3Var8, new ct0[]{sf3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new tf3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new tf3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
