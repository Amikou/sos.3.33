package defpackage;

import kotlinx.coroutines.sync.MutexImpl;

/* compiled from: Mutex.kt */
/* renamed from: lb2  reason: default package */
/* loaded from: classes2.dex */
public final class lb2 {
    public static final k24 a;
    public static final k24 b;
    public static final k24 c;
    public static final nu0 d;
    public static final nu0 e;

    static {
        new k24("LOCK_FAIL");
        a = new k24("UNLOCK_FAIL");
        new k24("SELECT_SUCCESS");
        k24 k24Var = new k24("LOCKED");
        b = k24Var;
        k24 k24Var2 = new k24("UNLOCKED");
        c = k24Var2;
        d = new nu0(k24Var);
        e = new nu0(k24Var2);
    }

    public static final kb2 a(boolean z) {
        return new MutexImpl(z);
    }

    public static /* synthetic */ kb2 b(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }
}
