package defpackage;

/* compiled from: SeekPoint.java */
/* renamed from: yi3  reason: default package */
/* loaded from: classes.dex */
public final class yi3 {
    public static final yi3 c = new yi3(0, 0);
    public final long a;
    public final long b;

    public yi3(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || yi3.class != obj.getClass()) {
            return false;
        }
        yi3 yi3Var = (yi3) obj;
        return this.a == yi3Var.a && this.b == yi3Var.b;
    }

    public int hashCode() {
        return (((int) this.a) * 31) + ((int) this.b);
    }

    public String toString() {
        return "[timeUs=" + this.a + ", position=" + this.b + "]";
    }
}
