package defpackage;

import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: d15  reason: default package */
/* loaded from: classes.dex */
public final class d15 {
    public static final ExecutorService a = e25.a().b(new dj2("GAC_Transform"), s25.a);

    public static ExecutorService a() {
        return a;
    }
}
