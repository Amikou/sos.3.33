package defpackage;

import com.google.crypto.tink.o;
import com.google.crypto.tink.p;
import com.google.crypto.tink.q;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.logging.Logger;

/* compiled from: AeadWrapper.java */
/* renamed from: da  reason: default package */
/* loaded from: classes2.dex */
public class da implements p<com.google.crypto.tink.a, com.google.crypto.tink.a> {
    public static final Logger a = Logger.getLogger(da.class.getName());

    /* compiled from: AeadWrapper.java */
    /* renamed from: da$b */
    /* loaded from: classes2.dex */
    public static class b implements com.google.crypto.tink.a {
        public final o<com.google.crypto.tink.a> a;

        @Override // com.google.crypto.tink.a
        public byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            return at.a(this.a.b().a(), this.a.b().c().a(bArr, bArr2));
        }

        @Override // com.google.crypto.tink.a
        public byte[] b(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            if (bArr.length > 5) {
                byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, 5);
                byte[] copyOfRange2 = Arrays.copyOfRange(bArr, 5, bArr.length);
                for (o.b<com.google.crypto.tink.a> bVar : this.a.c(copyOfRange)) {
                    try {
                        return bVar.c().b(copyOfRange2, bArr2);
                    } catch (GeneralSecurityException e) {
                        Logger logger = da.a;
                        logger.info("ciphertext prefix matches a key, but cannot decrypt: " + e.toString());
                    }
                }
            }
            for (o.b<com.google.crypto.tink.a> bVar2 : this.a.e()) {
                try {
                    return bVar2.c().b(bArr, bArr2);
                } catch (GeneralSecurityException unused) {
                }
            }
            throw new GeneralSecurityException("decryption failed");
        }

        public b(o<com.google.crypto.tink.a> oVar) {
            this.a = oVar;
        }
    }

    public static void e() throws GeneralSecurityException {
        q.r(new da());
    }

    @Override // com.google.crypto.tink.p
    public Class<com.google.crypto.tink.a> a() {
        return com.google.crypto.tink.a.class;
    }

    @Override // com.google.crypto.tink.p
    public Class<com.google.crypto.tink.a> b() {
        return com.google.crypto.tink.a.class;
    }

    @Override // com.google.crypto.tink.p
    /* renamed from: f */
    public com.google.crypto.tink.a c(o<com.google.crypto.tink.a> oVar) throws GeneralSecurityException {
        return new b(oVar);
    }
}
