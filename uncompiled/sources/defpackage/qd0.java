package defpackage;

/* renamed from: qd0  reason: default package */
/* loaded from: classes2.dex */
public class qd0 {
    public byte[] a;
    public int b;

    public qd0(byte[] bArr, int i) {
        this(bArr, i, -1);
    }

    public qd0(byte[] bArr, int i, int i2) {
        this.a = bArr;
        this.b = i;
    }

    public int a() {
        return this.b;
    }

    public byte[] b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj instanceof qd0) {
            qd0 qd0Var = (qd0) obj;
            if (qd0Var.b != this.b) {
                return false;
            }
            return wh.a(this.a, qd0Var.a);
        }
        return false;
    }

    public int hashCode() {
        return this.b ^ wh.r(this.a);
    }
}
