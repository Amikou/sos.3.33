package defpackage;

/* compiled from: BaseConsumer.java */
/* renamed from: qm  reason: default package */
/* loaded from: classes.dex */
public abstract class qm<T> implements l60<T> {
    public boolean a = false;

    public static boolean e(int i) {
        return (i & 1) == 1;
    }

    public static boolean f(int i) {
        return !e(i);
    }

    public static int l(boolean z) {
        return z ? 1 : 0;
    }

    public static boolean m(int i, int i2) {
        return (i & i2) != 0;
    }

    public static boolean n(int i, int i2) {
        return (i & i2) == i2;
    }

    public static int o(int i, int i2) {
        return i & (~i2);
    }

    @Override // defpackage.l60
    public synchronized void a(Throwable th) {
        if (this.a) {
            return;
        }
        this.a = true;
        try {
            h(th);
        } catch (Exception e) {
            k(e);
        }
    }

    @Override // defpackage.l60
    public synchronized void b() {
        if (this.a) {
            return;
        }
        this.a = true;
        try {
            g();
        } catch (Exception e) {
            k(e);
        }
    }

    @Override // defpackage.l60
    public synchronized void c(float f) {
        if (this.a) {
            return;
        }
        try {
            j(f);
        } catch (Exception e) {
            k(e);
        }
    }

    @Override // defpackage.l60
    public synchronized void d(T t, int i) {
        if (this.a) {
            return;
        }
        this.a = e(i);
        try {
            i(t, i);
        } catch (Exception e) {
            k(e);
        }
    }

    public abstract void g();

    public abstract void h(Throwable th);

    public abstract void i(T t, int i);

    public abstract void j(float f);

    public void k(Exception exc) {
        v11.y(getClass(), "unhandled exception", exc);
    }
}
