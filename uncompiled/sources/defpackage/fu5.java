package defpackage;

import android.os.Handler;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: fu5  reason: default package */
/* loaded from: classes.dex */
public final class fu5 {
    public du5 a;
    public final /* synthetic */ qu5 b;

    public fu5(qu5 qu5Var) {
        this.b = qu5Var;
    }

    public final void a() {
        Handler handler;
        this.b.e();
        if (this.a != null) {
            handler = this.b.c;
            handler.removeCallbacks(this.a);
        }
        if (this.b.a.z().v(null, qf5.s0)) {
            this.b.a.A().q.b(false);
        }
    }

    public final void b(long j) {
        Handler handler;
        this.a = new du5(this, this.b.a.a().a(), j);
        handler = this.b.c;
        handler.postDelayed(this.a, 2000L);
    }
}
