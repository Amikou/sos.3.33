package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: BottomAppBarTopEdgeTreatment.java */
/* renamed from: er  reason: default package */
/* loaded from: classes2.dex */
public class er extends cu0 implements Cloneable {
    public float a;
    public float f0;
    public float g0;
    public float h0;
    public float i0;
    public float j0 = -1.0f;

    public er(float f, float f2, float f3) {
        this.f0 = f;
        this.a = f2;
        k(f3);
        this.i0 = Utils.FLOAT_EPSILON;
    }

    @Override // defpackage.cu0
    public void b(float f, float f2, float f3, rn3 rn3Var) {
        float f4;
        float f5;
        float f6 = this.g0;
        if (f6 == Utils.FLOAT_EPSILON) {
            rn3Var.m(f, Utils.FLOAT_EPSILON);
            return;
        }
        float f7 = ((this.f0 * 2.0f) + f6) / 2.0f;
        float f8 = f3 * this.a;
        float f9 = f2 + this.i0;
        float f10 = (this.h0 * f3) + ((1.0f - f3) * f7);
        if (f10 / f7 >= 1.0f) {
            rn3Var.m(f, Utils.FLOAT_EPSILON);
            return;
        }
        float f11 = this.j0;
        float f12 = f11 * f3;
        boolean z = f11 == -1.0f || Math.abs((f11 * 2.0f) - f6) < 0.1f;
        if (z) {
            f4 = f10;
            f5 = 0.0f;
        } else {
            f5 = 1.75f;
            f4 = 0.0f;
        }
        float f13 = f7 + f8;
        float f14 = f4 + f8;
        float sqrt = (float) Math.sqrt((f13 * f13) - (f14 * f14));
        float f15 = f9 - sqrt;
        float f16 = f9 + sqrt;
        float degrees = (float) Math.toDegrees(Math.atan(sqrt / f14));
        float f17 = (90.0f - degrees) + f5;
        rn3Var.m(f15, Utils.FLOAT_EPSILON);
        float f18 = f8 * 2.0f;
        rn3Var.a(f15 - f8, Utils.FLOAT_EPSILON, f15 + f8, f18, 270.0f, degrees);
        if (z) {
            rn3Var.a(f9 - f7, (-f7) - f4, f9 + f7, f7 - f4, 180.0f - f17, (f17 * 2.0f) - 180.0f);
        } else {
            float f19 = this.f0;
            float f20 = f12 * 2.0f;
            float f21 = f9 - f7;
            rn3Var.a(f21, -(f12 + f19), f21 + f19 + f20, f19 + f12, 180.0f - f17, ((f17 * 2.0f) - 180.0f) / 2.0f);
            float f22 = f9 + f7;
            float f23 = this.f0;
            rn3Var.m(f22 - ((f23 / 2.0f) + f12), f23 + f12);
            float f24 = this.f0;
            rn3Var.a(f22 - (f20 + f24), -(f12 + f24), f22, f24 + f12, 90.0f, f17 - 90.0f);
        }
        rn3Var.a(f16 - f8, Utils.FLOAT_EPSILON, f16 + f8, f18, 270.0f - degrees, degrees);
        rn3Var.m(f, Utils.FLOAT_EPSILON);
    }

    public float d() {
        return this.h0;
    }

    public float e() {
        return this.j0;
    }

    public float f() {
        return this.f0;
    }

    public float g() {
        return this.a;
    }

    public float h() {
        return this.g0;
    }

    public float j() {
        return this.i0;
    }

    public void k(float f) {
        if (f >= Utils.FLOAT_EPSILON) {
            this.h0 = f;
            return;
        }
        throw new IllegalArgumentException("cradleVerticalOffset must be positive.");
    }

    public void l(float f) {
        this.j0 = f;
    }

    public void o(float f) {
        this.f0 = f;
    }

    public void p(float f) {
        this.a = f;
    }

    public void r(float f) {
        this.g0 = f;
    }

    public void s(float f) {
        this.i0 = f;
    }
}
