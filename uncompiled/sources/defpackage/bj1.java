package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import androidx.constraintlayout.core.widgets.f;

/* compiled from: GuidelineReference.java */
/* renamed from: bj1  reason: default package */
/* loaded from: classes.dex */
public class bj1 extends WidgetRun {
    public bj1(ConstraintWidget constraintWidget) {
        super(constraintWidget);
        constraintWidget.d.f();
        constraintWidget.e.f();
        this.f = ((f) constraintWidget).p1();
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun, defpackage.im0
    public void a(im0 im0Var) {
        DependencyNode dependencyNode = this.h;
        if (dependencyNode.c && !dependencyNode.j) {
            this.h.d((int) ((dependencyNode.l.get(0).g * ((f) this.b).s1()) + 0.5f));
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void d() {
        f fVar = (f) this.b;
        int q1 = fVar.q1();
        int r1 = fVar.r1();
        fVar.s1();
        if (fVar.p1() == 1) {
            if (q1 != -1) {
                this.h.l.add(this.b.Y.d.h);
                this.b.Y.d.h.k.add(this.h);
                this.h.f = q1;
            } else if (r1 != -1) {
                this.h.l.add(this.b.Y.d.i);
                this.b.Y.d.i.k.add(this.h);
                this.h.f = -r1;
            } else {
                DependencyNode dependencyNode = this.h;
                dependencyNode.b = true;
                dependencyNode.l.add(this.b.Y.d.i);
                this.b.Y.d.i.k.add(this.h);
            }
            q(this.b.d.h);
            q(this.b.d.i);
            return;
        }
        if (q1 != -1) {
            this.h.l.add(this.b.Y.e.h);
            this.b.Y.e.h.k.add(this.h);
            this.h.f = q1;
        } else if (r1 != -1) {
            this.h.l.add(this.b.Y.e.i);
            this.b.Y.e.i.k.add(this.h);
            this.h.f = -r1;
        } else {
            DependencyNode dependencyNode2 = this.h;
            dependencyNode2.b = true;
            dependencyNode2.l.add(this.b.Y.e.i);
            this.b.Y.e.i.k.add(this.h);
        }
        q(this.b.e.h);
        q(this.b.e.i);
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void e() {
        if (((f) this.b).p1() == 1) {
            this.b.j1(this.h.g);
        } else {
            this.b.k1(this.h.g);
        }
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public void f() {
        this.h.c();
    }

    @Override // androidx.constraintlayout.core.widgets.analyzer.WidgetRun
    public boolean m() {
        return false;
    }

    public final void q(DependencyNode dependencyNode) {
        this.h.k.add(dependencyNode);
        dependencyNode.l.add(this.h);
    }
}
