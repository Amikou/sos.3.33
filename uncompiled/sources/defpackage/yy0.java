package defpackage;

import java.util.concurrent.Executor;

/* compiled from: Executors.java */
/* renamed from: yy0  reason: default package */
/* loaded from: classes.dex */
public final class yy0 {
    public static final Executor a = new a();
    public static final Executor b = new b();

    /* compiled from: Executors.java */
    /* renamed from: yy0$a */
    /* loaded from: classes.dex */
    public class a implements Executor {
        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            mg4.u(runnable);
        }
    }

    /* compiled from: Executors.java */
    /* renamed from: yy0$b */
    /* loaded from: classes.dex */
    public class b implements Executor {
        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            runnable.run();
        }
    }

    public static Executor a() {
        return b;
    }

    public static Executor b() {
        return a;
    }
}
