package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: SendtoFragmentDirections.java */
/* renamed from: nl3  reason: default package */
/* loaded from: classes2.dex */
public class nl3 {

    /* compiled from: SendtoFragmentDirections.java */
    /* renamed from: nl3$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("etAddress")) {
                bundle.putString("etAddress", (String) this.a.get("etAddress"));
            }
            if (this.a.containsKey("balance")) {
                bundle.putString("balance", (String) this.a.get("balance"));
            }
            if (this.a.containsKey("userTokenData")) {
                UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) this.a.get("userTokenData");
                if (!Parcelable.class.isAssignableFrom(UserTokenItemDisplayModel.class) && userTokenItemDisplayModel != null) {
                    if (Serializable.class.isAssignableFrom(UserTokenItemDisplayModel.class)) {
                        bundle.putSerializable("userTokenData", (Serializable) Serializable.class.cast(userTokenItemDisplayModel));
                    } else {
                        throw new UnsupportedOperationException(UserTokenItemDisplayModel.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("userTokenData", (Parcelable) Parcelable.class.cast(userTokenItemDisplayModel));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_sendtoFragment_to_sendingFragment;
        }

        public String c() {
            return (String) this.a.get("balance");
        }

        public String d() {
            return (String) this.a.get("etAddress");
        }

        public UserTokenItemDisplayModel e() {
            return (UserTokenItemDisplayModel) this.a.get("userTokenData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("etAddress") != bVar.a.containsKey("etAddress")) {
                return false;
            }
            if (d() == null ? bVar.d() == null : d().equals(bVar.d())) {
                if (this.a.containsKey("balance") != bVar.a.containsKey("balance")) {
                    return false;
                }
                if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                    if (this.a.containsKey("userTokenData") != bVar.a.containsKey("userTokenData")) {
                        return false;
                    }
                    if (e() == null ? bVar.e() == null : e().equals(bVar.e())) {
                        return b() == bVar.b();
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return (((((((d() != null ? d().hashCode() : 0) + 31) * 31) + (c() != null ? c().hashCode() : 0)) * 31) + (e() != null ? e().hashCode() : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionSendtoFragmentToSendingFragment(actionId=" + b() + "){etAddress=" + d() + ", balance=" + c() + ", userTokenData=" + e() + "}";
        }

        public b(String str, String str2, UserTokenItemDisplayModel userTokenItemDisplayModel) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (str != null) {
                hashMap.put("etAddress", str);
                if (str2 != null) {
                    hashMap.put("balance", str2);
                    if (userTokenItemDisplayModel != null) {
                        hashMap.put("userTokenData", userTokenItemDisplayModel);
                        return;
                    }
                    throw new IllegalArgumentException("Argument \"userTokenData\" is marked as non-null but was passed a null value.");
                }
                throw new IllegalArgumentException("Argument \"balance\" is marked as non-null but was passed a null value.");
            }
            throw new IllegalArgumentException("Argument \"etAddress\" is marked as non-null but was passed a null value.");
        }
    }

    public static ce2 a() {
        return new l6(R.id.action_sendtoFragment_to_manageContactsFragment);
    }

    public static b b(String str, String str2, UserTokenItemDisplayModel userTokenItemDisplayModel) {
        return new b(str, str2, userTokenItemDisplayModel);
    }
}
