package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: f36  reason: default package */
/* loaded from: classes.dex */
public final class f36 implements e36 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.collection.log_event_and_bundle_v2", true);

    @Override // defpackage.e36
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
