package defpackage;

/* compiled from: Progressions.kt */
/* renamed from: qr1  reason: default package */
/* loaded from: classes2.dex */
public class qr1 implements Iterable<Integer>, tw1 {
    public static final a h0 = new a(null);
    public final int a;
    public final int f0;
    public final int g0;

    /* compiled from: Progressions.kt */
    /* renamed from: qr1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public final qr1 a(int i, int i2, int i3) {
            return new qr1(i, i2, i3);
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public qr1(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        }
        if (i3 != Integer.MIN_VALUE) {
            this.a = i;
            this.f0 = pv2.b(i, i2, i3);
            this.g0 = i3;
            return;
        }
        throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
    }

    public boolean equals(Object obj) {
        if (obj instanceof qr1) {
            if (!isEmpty() || !((qr1) obj).isEmpty()) {
                qr1 qr1Var = (qr1) obj;
                if (this.a != qr1Var.a || this.f0 != qr1Var.f0 || this.g0 != qr1Var.g0) {
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.a * 31) + this.f0) * 31) + this.g0;
    }

    public boolean isEmpty() {
        if (this.g0 > 0) {
            if (this.a > this.f0) {
                return true;
            }
        } else if (this.a < this.f0) {
            return true;
        }
        return false;
    }

    public final int m() {
        return this.a;
    }

    public final int n() {
        return this.f0;
    }

    public final int o() {
        return this.g0;
    }

    @Override // java.lang.Iterable
    /* renamed from: p */
    public or1 iterator() {
        return new rr1(this.a, this.f0, this.g0);
    }

    public String toString() {
        StringBuilder sb;
        int i;
        if (this.g0 > 0) {
            sb = new StringBuilder();
            sb.append(this.a);
            sb.append("..");
            sb.append(this.f0);
            sb.append(" step ");
            i = this.g0;
        } else {
            sb = new StringBuilder();
            sb.append(this.a);
            sb.append(" downTo ");
            sb.append(this.f0);
            sb.append(" step ");
            i = -this.g0;
        }
        sb.append(i);
        return sb.toString();
    }
}
