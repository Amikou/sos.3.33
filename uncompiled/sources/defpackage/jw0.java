package defpackage;

import java.math.BigInteger;

/* compiled from: EthChainId.java */
/* renamed from: jw0  reason: default package */
/* loaded from: classes3.dex */
public class jw0 extends i83<String> {
    public BigInteger getChainId() {
        return ej2.decodeQuantity(getResult());
    }
}
