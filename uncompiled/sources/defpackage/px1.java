package defpackage;

import java.util.Map;

/* compiled from: KeyValueHolder.java */
/* renamed from: px1  reason: default package */
/* loaded from: classes2.dex */
public final class px1<K, V> implements Map.Entry<K, V> {
    public final K a;
    public final V f0;

    public px1(K k, V v) {
        this.a = (K) rl2.f(k);
        this.f0 = (V) rl2.f(v);
    }

    @Override // java.util.Map.Entry
    public boolean equals(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            return this.a.equals(entry.getKey()) && this.f0.equals(entry.getValue());
        }
        return false;
    }

    @Override // java.util.Map.Entry
    public K getKey() {
        return this.a;
    }

    @Override // java.util.Map.Entry
    public V getValue() {
        return this.f0;
    }

    @Override // java.util.Map.Entry
    public int hashCode() {
        return this.a.hashCode() ^ this.f0.hashCode();
    }

    @Override // java.util.Map.Entry
    public V setValue(V v) {
        throw new UnsupportedOperationException("not supported");
    }

    public String toString() {
        return this.a + "=" + this.f0;
    }
}
