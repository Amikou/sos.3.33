package defpackage;

/* compiled from: NestedScrollingChild.java */
/* renamed from: pe2  reason: default package */
/* loaded from: classes.dex */
public interface pe2 {
    boolean isNestedScrollingEnabled();

    void stopNestedScroll();
}
