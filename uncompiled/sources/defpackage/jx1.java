package defpackage;

import android.content.Context;
import android.util.Xml;
import androidx.constraintlayout.motion.widget.a;
import androidx.constraintlayout.motion.widget.b;
import androidx.constraintlayout.motion.widget.c;
import androidx.constraintlayout.motion.widget.d;
import androidx.constraintlayout.motion.widget.e;
import androidx.constraintlayout.motion.widget.f;
import androidx.constraintlayout.widget.ConstraintAttribute;
import androidx.constraintlayout.widget.ConstraintLayout;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: KeyFrames.java */
/* renamed from: jx1  reason: default package */
/* loaded from: classes.dex */
public class jx1 {
    public static HashMap<String, Constructor<? extends a>> b;
    public HashMap<Integer, ArrayList<a>> a = new HashMap<>();

    static {
        HashMap<String, Constructor<? extends a>> hashMap = new HashMap<>();
        b = hashMap;
        try {
            hashMap.put("KeyAttribute", b.class.getConstructor(new Class[0]));
            b.put("KeyPosition", d.class.getConstructor(new Class[0]));
            b.put("KeyCycle", c.class.getConstructor(new Class[0]));
            b.put("KeyTimeCycle", e.class.getConstructor(new Class[0]));
            b.put("KeyTrigger", f.class.getConstructor(new Class[0]));
        } catch (NoSuchMethodException unused) {
        }
    }

    public jx1() {
    }

    public void a(u92 u92Var) {
        ArrayList<a> arrayList = this.a.get(-1);
        if (arrayList != null) {
            u92Var.b(arrayList);
        }
    }

    public void b(u92 u92Var) {
        ArrayList<a> arrayList = this.a.get(Integer.valueOf(u92Var.c));
        if (arrayList != null) {
            u92Var.b(arrayList);
        }
        ArrayList<a> arrayList2 = this.a.get(-1);
        if (arrayList2 != null) {
            Iterator<a> it = arrayList2.iterator();
            while (it.hasNext()) {
                a next = it.next();
                if (next.f(((ConstraintLayout.LayoutParams) u92Var.b.getLayoutParams()).X)) {
                    u92Var.a(next);
                }
            }
        }
    }

    public void c(a aVar) {
        if (!this.a.containsKey(Integer.valueOf(aVar.b))) {
            this.a.put(Integer.valueOf(aVar.b), new ArrayList<>());
        }
        ArrayList<a> arrayList = this.a.get(Integer.valueOf(aVar.b));
        if (arrayList != null) {
            arrayList.add(aVar);
        }
    }

    public ArrayList<a> d(int i) {
        return this.a.get(Integer.valueOf(i));
    }

    public jx1(Context context, XmlPullParser xmlPullParser) {
        HashMap<String, ConstraintAttribute> hashMap;
        HashMap<String, ConstraintAttribute> hashMap2;
        a aVar = null;
        try {
            int eventType = xmlPullParser.getEventType();
            while (eventType != 1) {
                if (eventType != 2) {
                    if (eventType == 3 && "KeyFrameSet".equals(xmlPullParser.getName())) {
                        return;
                    }
                } else {
                    String name = xmlPullParser.getName();
                    if (b.containsKey(name)) {
                        try {
                            Constructor<? extends a> constructor = b.get(name);
                            if (constructor != null) {
                                a newInstance = constructor.newInstance(new Object[0]);
                                try {
                                    newInstance.e(context, Xml.asAttributeSet(xmlPullParser));
                                    c(newInstance);
                                } catch (Exception unused) {
                                }
                                aVar = newInstance;
                                continue;
                            } else {
                                throw new NullPointerException("Keymaker for " + name + " not found");
                                break;
                            }
                        } catch (Exception unused2) {
                            continue;
                        }
                    } else if (name.equalsIgnoreCase("CustomAttribute")) {
                        if (aVar != null && (hashMap2 = aVar.e) != null) {
                            ConstraintAttribute.i(context, xmlPullParser, hashMap2);
                            continue;
                        }
                    } else if (name.equalsIgnoreCase("CustomMethod") && aVar != null && (hashMap = aVar.e) != null) {
                        ConstraintAttribute.i(context, xmlPullParser, hashMap);
                        continue;
                    }
                }
                eventType = xmlPullParser.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
    }
}
