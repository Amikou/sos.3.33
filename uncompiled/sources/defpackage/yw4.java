package defpackage;

import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.e;
import com.google.android.play.core.assetpacks.i;
import com.google.android.play.core.assetpacks.m;
import com.google.android.play.core.internal.d;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/* renamed from: yw4  reason: default package */
/* loaded from: classes2.dex */
public final class yw4 {
    public static final it4 c = new it4("PatchSliceTaskHandler");
    public final c a;
    public final cw4<zy4> b;

    public yw4(c cVar, cw4<zy4> cw4Var) {
        this.a = cVar;
        this.b = cw4Var;
    }

    public final void a(xw4 xw4Var) {
        File t = this.a.t(xw4Var.b, xw4Var.c, xw4Var.d);
        File file = new File(this.a.u(xw4Var.b, xw4Var.c, xw4Var.d), xw4Var.h);
        try {
            InputStream inputStream = xw4Var.j;
            if (xw4Var.g == 2) {
                inputStream = new GZIPInputStream(inputStream, 8192);
            }
            e eVar = new e(t, file);
            File v = this.a.v(xw4Var.b, xw4Var.e, xw4Var.f, xw4Var.h);
            if (!v.exists()) {
                v.mkdirs();
            }
            m mVar = new m(this.a, xw4Var.b, xw4Var.e, xw4Var.f, xw4Var.h);
            d.l(eVar, inputStream, new i(v, mVar), xw4Var.i);
            mVar.d(0);
            inputStream.close();
            c.d("Patching and extraction finished for slice %s of pack %s.", xw4Var.h, xw4Var.b);
            this.b.a().c(xw4Var.a, xw4Var.b, xw4Var.h, 0);
            try {
                xw4Var.j.close();
            } catch (IOException unused) {
                c.e("Could not close file for slice %s of pack %s.", xw4Var.h, xw4Var.b);
            }
        } catch (IOException e) {
            c.b("IOException during patching %s.", e.getMessage());
            throw new bj(String.format("Error patching slice %s of pack %s.", xw4Var.h, xw4Var.b), e, xw4Var.a);
        }
    }
}
