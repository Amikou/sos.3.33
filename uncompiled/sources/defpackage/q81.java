package defpackage;

import net.safemoon.androidwallet.domain.useCase.startup.forceIcon.ForceTokenLogoUpdateUseCase;
import net.safemoon.androidwallet.repository.ReflectionDataSource;

/* compiled from: ForceTokenListLogoUpdateUseCase.kt */
/* renamed from: q81  reason: default package */
/* loaded from: classes2.dex */
public final class q81 implements am1 {
    public final bn1 a;
    public final ReflectionDataSource b;
    public final c90 c;

    public q81(bn1 bn1Var, ReflectionDataSource reflectionDataSource, c90 c90Var) {
        fs1.f(bn1Var, "userTokenListRepository");
        fs1.f(reflectionDataSource, "reflectionDataSource");
        fs1.f(c90Var, "coroutineScope");
        this.a = bn1Var;
        this.b = reflectionDataSource;
        this.c = c90Var;
    }

    @Override // defpackage.am1
    public void a() {
        b("ERC_PSAFEMOON", "p_safemoon").a();
    }

    public final am1 b(String str, String str2) {
        return new ForceTokenLogoUpdateUseCase(this.a, this.b, this.c, str, str2);
    }
}
