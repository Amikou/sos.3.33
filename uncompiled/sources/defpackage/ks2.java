package defpackage;

import android.media.metrics.LogSessionId;
import androidx.media3.common.util.b;

/* compiled from: PlayerId.java */
/* renamed from: ks2  reason: default package */
/* loaded from: classes.dex */
public final class ks2 {
    public final a a;

    /* compiled from: PlayerId.java */
    /* renamed from: ks2$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final a b = new a(LogSessionId.LOG_SESSION_ID_NONE);
        public final LogSessionId a;

        public a(LogSessionId logSessionId) {
            this.a = logSessionId;
        }
    }

    static {
        if (b.a < 31) {
            new ks2();
        } else {
            new ks2(a.b);
        }
    }

    public ks2() {
        this((a) null);
        ii.g(b.a < 31);
    }

    public LogSessionId a() {
        return ((a) ii.e(this.a)).a;
    }

    public ks2(LogSessionId logSessionId) {
        this(new a(logSessionId));
    }

    public ks2(a aVar) {
        this.a = aVar;
    }
}
