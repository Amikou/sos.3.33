package defpackage;

import java.util.NoSuchElementException;

/* compiled from: ProgressionIterators.kt */
/* renamed from: rr1  reason: default package */
/* loaded from: classes2.dex */
public final class rr1 extends or1 {
    public final int a;
    public boolean f0;
    public int g0;
    public final int h0;

    public rr1(int i, int i2, int i3) {
        this.h0 = i3;
        this.a = i2;
        boolean z = true;
        if (i3 <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.f0 = z;
        this.g0 = z ? i : i2;
    }

    @Override // defpackage.or1
    public int b() {
        int i = this.g0;
        if (i == this.a) {
            if (this.f0) {
                this.f0 = false;
            } else {
                throw new NoSuchElementException();
            }
        } else {
            this.g0 = this.h0 + i;
        }
        return i;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.f0;
    }
}
