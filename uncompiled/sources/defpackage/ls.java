package defpackage;

/* compiled from: ByteArrayAdapter.java */
/* renamed from: ls  reason: default package */
/* loaded from: classes.dex */
public final class ls implements jh<byte[]> {
    @Override // defpackage.jh
    public String a() {
        return "ByteArrayPool";
    }

    @Override // defpackage.jh
    public int b() {
        return 1;
    }

    @Override // defpackage.jh
    /* renamed from: d */
    public int c(byte[] bArr) {
        return bArr.length;
    }

    @Override // defpackage.jh
    /* renamed from: e */
    public byte[] newArray(int i) {
        return new byte[i];
    }
}
