package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: j16  reason: default package */
/* loaded from: classes.dex */
public final class j16 implements i16 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.client.consent.suppress_1p_in_ga4f_install", true);

    @Override // defpackage.i16
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.i16
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
