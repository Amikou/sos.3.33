package defpackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.collections.EmptyList;

/* compiled from: Collections.kt */
/* renamed from: b20  reason: default package */
/* loaded from: classes2.dex */
public class b20 extends a20 {
    public static final <T> ArrayList<T> c(T... tArr) {
        fs1.f(tArr, "elements");
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new kh(tArr, true));
    }

    public static final <T> Collection<T> d(T[] tArr) {
        fs1.f(tArr, "$this$asCollection");
        return new kh(tArr, false);
    }

    public static final <T extends Comparable<? super T>> int e(List<? extends T> list, T t, int i, int i2) {
        fs1.f(list, "$this$binarySearch");
        n(list.size(), i, i2);
        int i3 = i2 - 1;
        while (i <= i3) {
            int i4 = (i + i3) >>> 1;
            int a = l30.a(list.get(i4), t);
            if (a < 0) {
                i = i4 + 1;
            } else if (a <= 0) {
                return i4;
            } else {
                i3 = i4 - 1;
            }
        }
        return -(i + 1);
    }

    public static /* synthetic */ int f(List list, Comparable comparable, int i, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = list.size();
        }
        return e(list, comparable, i, i2);
    }

    public static final <T> List<T> g() {
        return EmptyList.INSTANCE;
    }

    public static final sr1 h(Collection<?> collection) {
        fs1.f(collection, "$this$indices");
        return new sr1(0, collection.size() - 1);
    }

    public static final <T> int i(List<? extends T> list) {
        fs1.f(list, "$this$lastIndex");
        return list.size() - 1;
    }

    public static final <T> List<T> j(T... tArr) {
        fs1.f(tArr, "elements");
        return tArr.length > 0 ? zh.c(tArr) : g();
    }

    public static final <T> List<T> k(T... tArr) {
        fs1.f(tArr, "elements");
        return ai.r(tArr);
    }

    public static final <T> List<T> l(T... tArr) {
        fs1.f(tArr, "elements");
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new kh(tArr, true));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> m(List<? extends T> list) {
        fs1.f(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size != 0) {
            return size != 1 ? list : a20.b(list.get(0));
        }
        return g();
    }

    public static final void n(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("fromIndex (" + i2 + ") is greater than toIndex (" + i3 + ").");
        } else if (i2 < 0) {
            throw new IndexOutOfBoundsException("fromIndex (" + i2 + ") is less than zero.");
        } else if (i3 <= i) {
        } else {
            throw new IndexOutOfBoundsException("toIndex (" + i3 + ") is greater than size (" + i + ").");
        }
    }

    public static final void o() {
        throw new ArithmeticException("Count overflow has happened.");
    }

    public static final void p() {
        throw new ArithmeticException("Index overflow has happened.");
    }
}
