package defpackage;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.b;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: yz4  reason: default package */
/* loaded from: classes.dex */
public final class yz4 extends q05 {
    public final /* synthetic */ b.c b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public yz4(wz4 wz4Var, j05 j05Var, b.c cVar) {
        super(j05Var);
        this.b = cVar;
    }

    @Override // defpackage.q05
    public final void a() {
        this.b.b(new ConnectionResult(16, null));
    }
}
