package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: y34  reason: default package */
/* loaded from: classes2.dex */
public interface y34 {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;
    public static final i f;
    public static final i g;
    public static final i h;
    public static final i i;
    public static final i j;
    public static final i k;
    public static final i l;
    public static final i m;
    public static final i n;
    public static final i o;
    public static final i p;
    public static final i q;
    public static final i r;
    public static final i s;
    public static final i t;

    static {
        i iVar = new i("1.3.36.3");
        a = iVar;
        iVar.z("2.1");
        iVar.z("2.2");
        iVar.z("2.3");
        i z = iVar.z("3.1");
        b = z;
        z.z("2");
        z.z("3");
        z.z("4");
        i z2 = iVar.z("3.2");
        c = z2;
        z2.z("1");
        z2.z("2");
        i z3 = iVar.z("3.2.8");
        d = z3;
        i z4 = z3.z("1");
        e = z4;
        i z5 = z4.z("1");
        f = z5;
        g = z5.z("1");
        h = z5.z("2");
        i = z5.z("3");
        j = z5.z("4");
        k = z5.z("5");
        l = z5.z("6");
        m = z5.z("7");
        n = z5.z("8");
        o = z5.z("9");
        p = z5.z("10");
        q = z5.z("11");
        r = z5.z("12");
        s = z5.z("13");
        t = z5.z("14");
    }
}
