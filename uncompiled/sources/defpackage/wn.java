package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

/* compiled from: BaseProgressIndicatorSpec.java */
/* renamed from: wn  reason: default package */
/* loaded from: classes2.dex */
public abstract class wn {
    public int a;
    public int b;
    public int[] c = new int[0];
    public int d;
    public int e;
    public int f;

    public wn(Context context, AttributeSet attributeSet, int i, int i2) {
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(jz2.mtrl_progress_track_thickness);
        TypedArray h = a54.h(context, attributeSet, o23.BaseProgressIndicator, i, i2, new int[0]);
        this.a = n42.c(context, h, o23.BaseProgressIndicator_trackThickness, dimensionPixelSize);
        this.b = Math.min(n42.c(context, h, o23.BaseProgressIndicator_trackCornerRadius, 0), this.a / 2);
        this.e = h.getInt(o23.BaseProgressIndicator_showAnimationBehavior, 0);
        this.f = h.getInt(o23.BaseProgressIndicator_hideAnimationBehavior, 0);
        c(context, h);
        d(context, h);
        h.recycle();
    }

    public boolean a() {
        return this.f != 0;
    }

    public boolean b() {
        return this.e != 0;
    }

    public final void c(Context context, TypedArray typedArray) {
        int i = o23.BaseProgressIndicator_indicatorColor;
        if (!typedArray.hasValue(i)) {
            this.c = new int[]{l42.b(context, gy2.colorPrimary, -1)};
        } else if (typedArray.peekValue(i).type != 1) {
            this.c = new int[]{typedArray.getColor(i, -1)};
        } else {
            int[] intArray = context.getResources().getIntArray(typedArray.getResourceId(i, -1));
            this.c = intArray;
            if (intArray.length == 0) {
                throw new IllegalArgumentException("indicatorColors cannot be empty when indicatorColor is not used.");
            }
        }
    }

    public final void d(Context context, TypedArray typedArray) {
        int i = o23.BaseProgressIndicator_trackColor;
        if (typedArray.hasValue(i)) {
            this.d = typedArray.getColor(i, -1);
            return;
        }
        this.d = this.c[0];
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{16842803});
        float f = obtainStyledAttributes.getFloat(0, 0.2f);
        obtainStyledAttributes.recycle();
        this.d = l42.a(this.d, (int) (f * 255.0f));
    }

    public abstract void e();
}
