package defpackage;

import defpackage.l80;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ForwardingControllerListener2.java */
/* renamed from: b91  reason: default package */
/* loaded from: classes.dex */
public class b91<I> extends dn<I> {
    public final List<l80<I>> a = new ArrayList(2);

    @Override // defpackage.dn, defpackage.l80
    public void b(String str, I i, l80.a aVar) {
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            try {
                l80<I> l80Var = this.a.get(i2);
                if (l80Var != null) {
                    l80Var.b(str, i, aVar);
                }
            } catch (Exception e) {
                h("ForwardingControllerListener2 exception in onFinalImageSet", e);
            }
        }
    }

    @Override // defpackage.dn, defpackage.l80
    public void c(String str, l80.a aVar) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                l80<I> l80Var = this.a.get(i);
                if (l80Var != null) {
                    l80Var.c(str, aVar);
                }
            } catch (Exception e) {
                h("ForwardingControllerListener2 exception in onRelease", e);
            }
        }
    }

    @Override // defpackage.dn, defpackage.l80
    public void d(String str, Throwable th, l80.a aVar) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                l80<I> l80Var = this.a.get(i);
                if (l80Var != null) {
                    l80Var.d(str, th, aVar);
                }
            } catch (Exception e) {
                h("ForwardingControllerListener2 exception in onFailure", e);
            }
        }
    }

    @Override // defpackage.dn, defpackage.l80
    public void e(String str, Object obj, l80.a aVar) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                l80<I> l80Var = this.a.get(i);
                if (l80Var != null) {
                    l80Var.e(str, obj, aVar);
                }
            } catch (Exception e) {
                h("ForwardingControllerListener2 exception in onSubmit", e);
            }
        }
    }

    public synchronized void g(l80<I> l80Var) {
        this.a.add(l80Var);
    }

    public final synchronized void h(String str, Throwable th) {
    }

    public synchronized void i(l80<I> l80Var) {
        int indexOf = this.a.indexOf(l80Var);
        if (indexOf != -1) {
            this.a.remove(indexOf);
        }
    }
}
