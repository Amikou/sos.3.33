package defpackage;

import java.util.Arrays;

/* renamed from: e85  reason: default package */
/* loaded from: classes.dex */
public final class e85 implements j85 {
    public e85() {
    }

    public /* synthetic */ e85(b85 b85Var) {
        this();
    }

    @Override // defpackage.j85
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
