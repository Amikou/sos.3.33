package defpackage;

import android.util.Pair;
import androidx.media3.common.ParserException;
import androidx.media3.common.util.b;
import java.io.IOException;

/* compiled from: WavHeaderReader.java */
/* renamed from: eo4  reason: default package */
/* loaded from: classes.dex */
public final class eo4 {

    /* compiled from: WavHeaderReader.java */
    /* renamed from: eo4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final long b;

        public a(int i, long j) {
            this.a = i;
            this.b = j;
        }

        public static a a(q11 q11Var, op2 op2Var) throws IOException {
            q11Var.n(op2Var.d(), 0, 8);
            op2Var.P(0);
            return new a(op2Var.n(), op2Var.t());
        }
    }

    public static boolean a(q11 q11Var) throws IOException {
        op2 op2Var = new op2(8);
        int i = a.a(q11Var, op2Var).a;
        if (i == 1380533830 || i == 1380333108) {
            q11Var.n(op2Var.d(), 0, 4);
            op2Var.P(0);
            int n = op2Var.n();
            if (n != 1463899717) {
                p12.c("WavHeaderReader", "Unsupported form type: " + n);
                return false;
            }
            return true;
        }
        return false;
    }

    public static do4 b(q11 q11Var) throws IOException {
        byte[] bArr;
        op2 op2Var = new op2(16);
        a d = d(1718449184, q11Var, op2Var);
        ii.g(d.b >= 16);
        q11Var.n(op2Var.d(), 0, 16);
        op2Var.P(0);
        int v = op2Var.v();
        int v2 = op2Var.v();
        int u = op2Var.u();
        int u2 = op2Var.u();
        int v3 = op2Var.v();
        int v4 = op2Var.v();
        int i = ((int) d.b) - 16;
        if (i > 0) {
            byte[] bArr2 = new byte[i];
            q11Var.n(bArr2, 0, i);
            bArr = bArr2;
        } else {
            bArr = b.f;
        }
        q11Var.k((int) (q11Var.e() - q11Var.getPosition()));
        return new do4(v, v2, u, u2, v3, v4, bArr);
    }

    public static long c(q11 q11Var) throws IOException {
        op2 op2Var = new op2(8);
        a a2 = a.a(q11Var, op2Var);
        if (a2.a != 1685272116) {
            q11Var.j();
            return -1L;
        }
        q11Var.f(8);
        op2Var.P(0);
        q11Var.n(op2Var.d(), 0, 8);
        long r = op2Var.r();
        q11Var.k(((int) a2.b) + 8);
        return r;
    }

    public static a d(int i, q11 q11Var, op2 op2Var) throws IOException {
        a a2 = a.a(q11Var, op2Var);
        while (a2.a != i) {
            p12.i("WavHeaderReader", "Ignoring unknown WAV chunk: " + a2.a);
            long j = a2.b + 8;
            if (j <= 2147483647L) {
                q11Var.k((int) j);
                a2 = a.a(q11Var, op2Var);
            } else {
                throw ParserException.createForUnsupportedContainerFeature("Chunk is too large (~2GB+) to skip; id: " + a2.a);
            }
        }
        return a2;
    }

    public static Pair<Long, Long> e(q11 q11Var) throws IOException {
        q11Var.j();
        a d = d(1684108385, q11Var, new op2(8));
        q11Var.k(8);
        return Pair.create(Long.valueOf(q11Var.getPosition()), Long.valueOf(d.b));
    }
}
