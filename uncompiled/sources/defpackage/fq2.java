package defpackage;

import okio.b;
import okio.d;
import okio.n;
import okio.o;

/* compiled from: PeekSource.kt */
/* renamed from: fq2  reason: default package */
/* loaded from: classes2.dex */
public final class fq2 implements n {
    public final b a;
    public bj3 f0;
    public int g0;
    public boolean h0;
    public long i0;
    public final d j0;

    public fq2(d dVar) {
        fs1.f(dVar, "upstream");
        this.j0 = dVar;
        b o = dVar.o();
        this.a = o;
        bj3 bj3Var = o.a;
        this.f0 = bj3Var;
        this.g0 = bj3Var != null ? bj3Var.b : -1;
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.h0 = true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r5 == r6.b) goto L29;
     */
    /* JADX WARN: Removed duplicated region for block: B:17:0x002d  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x006f  */
    @Override // okio.n
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long read(okio.b r9, long r10) {
        /*
            r8 = this;
            java.lang.String r0 = "sink"
            defpackage.fs1.f(r9, r0)
            r0 = 0
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            r3 = 0
            r4 = 1
            if (r2 < 0) goto Lf
            r5 = r4
            goto L10
        Lf:
            r5 = r3
        L10:
            if (r5 == 0) goto L87
            boolean r5 = r8.h0
            r5 = r5 ^ r4
            if (r5 == 0) goto L7b
            bj3 r5 = r8.f0
            if (r5 == 0) goto L2a
            okio.b r6 = r8.a
            bj3 r6 = r6.a
            if (r5 != r6) goto L2b
            int r5 = r8.g0
            defpackage.fs1.d(r6)
            int r6 = r6.b
            if (r5 != r6) goto L2b
        L2a:
            r3 = r4
        L2b:
            if (r3 == 0) goto L6f
            if (r2 != 0) goto L30
            return r0
        L30:
            okio.d r0 = r8.j0
            long r1 = r8.i0
            r3 = 1
            long r1 = r1 + r3
            boolean r0 = r0.V0(r1)
            if (r0 != 0) goto L40
            r9 = -1
            return r9
        L40:
            bj3 r0 = r8.f0
            if (r0 != 0) goto L53
            okio.b r0 = r8.a
            bj3 r0 = r0.a
            if (r0 == 0) goto L53
            r8.f0 = r0
            defpackage.fs1.d(r0)
            int r0 = r0.b
            r8.g0 = r0
        L53:
            okio.b r0 = r8.a
            long r0 = r0.a0()
            long r2 = r8.i0
            long r0 = r0 - r2
            long r10 = java.lang.Math.min(r10, r0)
            okio.b r2 = r8.a
            long r4 = r8.i0
            r3 = r9
            r6 = r10
            r2.f(r3, r4, r6)
            long r0 = r8.i0
            long r0 = r0 + r10
            r8.i0 = r0
            return r10
        L6f:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "Peek source is invalid because upstream source was used"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L7b:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "closed"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L87:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r0 = "byteCount < 0: "
            r9.append(r0)
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.String r9 = r9.toString()
            r10.<init>(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.fq2.read(okio.b, long):long");
    }

    @Override // okio.n
    public o timeout() {
        return this.j0.timeout();
    }
}
