package defpackage;

import java.util.Objects;

/* compiled from: ObjectHelper.java */
/* renamed from: il2  reason: default package */
/* loaded from: classes2.dex */
public final class il2 {

    /* compiled from: ObjectHelper.java */
    /* renamed from: il2$a */
    /* loaded from: classes2.dex */
    public static final class a {
    }

    static {
        new a();
    }

    public static <T> T a(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }

    public static int b(int i, String str) {
        if (i > 0) {
            return i;
        }
        throw new IllegalArgumentException(str + " > 0 required but it was " + i);
    }
}
