package defpackage;

import com.google.crypto.tink.shaded.protobuf.i0;

/* compiled from: NewInstanceSchemas.java */
/* renamed from: lf2  reason: default package */
/* loaded from: classes2.dex */
public final class lf2 {
    public static final jf2 a = c();
    public static final jf2 b = new i0();

    public static jf2 a() {
        return a;
    }

    public static jf2 b() {
        return b;
    }

    public static jf2 c() {
        try {
            return (jf2) Class.forName("com.google.crypto.tink.shaded.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
