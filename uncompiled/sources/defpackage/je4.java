package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.d;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: UTF8JsonGenerator.java */
/* renamed from: je4  reason: default package */
/* loaded from: classes.dex */
public class je4 extends bv1 {
    public static final byte[] A0 = a.c();
    public static final byte[] B0 = {110, 117, 108, 108};
    public static final byte[] C0 = {116, 114, 117, 101};
    public static final byte[] D0 = {102, 97, 108, 115, 101};
    public final OutputStream r0;
    public byte s0;
    public byte[] t0;
    public int u0;
    public final int v0;
    public final int w0;
    public char[] x0;
    public final int y0;
    public boolean z0;

    public je4(jm1 jm1Var, int i, c cVar, OutputStream outputStream) {
        super(jm1Var, i, cVar);
        this.s0 = (byte) 34;
        this.r0 = outputStream;
        this.z0 = true;
        byte[] j = jm1Var.j();
        this.t0 = j;
        int length = j.length;
        this.v0 = length;
        this.w0 = length >> 3;
        char[] e = jm1Var.e();
        this.x0 = e;
        this.y0 = e.length;
        if (F1(JsonGenerator.Feature.ESCAPE_NON_ASCII)) {
            w(127);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void B0(BigDecimal bigDecimal) throws IOException {
        z1("write a number");
        if (bigDecimal == null) {
            Y1();
        } else if (this.h0) {
            d2(v1(bigDecimal));
        } else {
            W0(v1(bigDecimal));
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void F0(BigInteger bigInteger) throws IOException {
        z1("write a number");
        if (bigInteger == null) {
            Y1();
        } else if (this.h0) {
            d2(bigInteger.toString());
        } else {
            W0(bigInteger.toString());
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void H0(short s) throws IOException {
        z1("write a number");
        if (this.u0 + 6 >= this.v0) {
            J1();
        }
        if (this.h0) {
            e2(s);
        } else {
            this.u0 = aj2.o(s, this.t0, this.u0);
        }
    }

    public final void J1() throws IOException {
        int i = this.u0;
        if (i > 0) {
            this.u0 = 0;
            this.r0.write(this.t0, 0, i);
        }
    }

    public final int K1(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws IOException, JsonGenerationException {
        int length = bArr2.length;
        if (i + length > i2) {
            this.u0 = i;
            J1();
            int i4 = this.u0;
            if (length > bArr.length) {
                this.r0.write(bArr2, 0, length);
                return i4;
            }
            System.arraycopy(bArr2, 0, bArr, i4, length);
            i = i4 + length;
        }
        if ((i3 * 6) + i > i2) {
            J1();
            return this.u0;
        }
        return i;
    }

    public final int L1(int i, int i2) throws IOException {
        byte[] bArr = this.t0;
        if (i >= 55296 && i <= 57343) {
            int i3 = i2 + 1;
            bArr[i2] = 92;
            int i4 = i3 + 1;
            bArr[i3] = 117;
            int i5 = i4 + 1;
            byte[] bArr2 = A0;
            bArr[i4] = bArr2[(i >> 12) & 15];
            int i6 = i5 + 1;
            bArr[i5] = bArr2[(i >> 8) & 15];
            int i7 = i6 + 1;
            bArr[i6] = bArr2[(i >> 4) & 15];
            int i8 = i7 + 1;
            bArr[i7] = bArr2[i & 15];
            return i8;
        }
        int i9 = i2 + 1;
        bArr[i2] = (byte) ((i >> 12) | 224);
        int i10 = i9 + 1;
        bArr[i9] = (byte) (((i >> 6) & 63) | 128);
        int i11 = i10 + 1;
        bArr[i10] = (byte) ((i & 63) | 128);
        return i11;
    }

    public final int M1(int i, char[] cArr, int i2, int i3) throws IOException {
        if (i >= 55296 && i <= 57343) {
            if (i2 >= i3 || cArr == null) {
                a(String.format("Split surrogate on writeRaw() input (last character): first character 0x%4x", Integer.valueOf(i)));
            }
            N1(i, cArr[i2]);
            return i2 + 1;
        }
        byte[] bArr = this.t0;
        int i4 = this.u0;
        int i5 = i4 + 1;
        this.u0 = i5;
        bArr[i4] = (byte) ((i >> 12) | 224);
        int i6 = i5 + 1;
        this.u0 = i6;
        bArr[i5] = (byte) (((i >> 6) & 63) | 128);
        this.u0 = i6 + 1;
        bArr[i6] = (byte) ((i & 63) | 128);
        return i2;
    }

    public final void N1(int i, int i2) throws IOException {
        int y1 = y1(i, i2);
        if (this.u0 + 4 > this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i3 = this.u0;
        int i4 = i3 + 1;
        this.u0 = i4;
        bArr[i3] = (byte) ((y1 >> 18) | 240);
        int i5 = i4 + 1;
        this.u0 = i5;
        bArr[i4] = (byte) (((y1 >> 12) & 63) | 128);
        int i6 = i5 + 1;
        this.u0 = i6;
        bArr[i5] = (byte) (((y1 >> 6) & 63) | 128);
        this.u0 = i6 + 1;
        bArr[i6] = (byte) ((y1 & 63) | 128);
    }

    public final int O1(InputStream inputStream, byte[] bArr, int i, int i2, int i3) throws IOException {
        int i4 = 0;
        while (i < i2) {
            bArr[i4] = bArr[i];
            i4++;
            i++;
        }
        int min = Math.min(i3, bArr.length);
        do {
            int i5 = min - i4;
            if (i5 == 0) {
                break;
            }
            int read = inputStream.read(bArr, i4, i5);
            if (read < 0) {
                return i4;
            }
            i4 += read;
        } while (i4 < 3);
        return i4;
    }

    public void P1() {
        byte[] bArr = this.t0;
        if (bArr != null && this.z0) {
            this.t0 = null;
            this.k0.t(bArr);
        }
        char[] cArr = this.x0;
        if (cArr != null) {
            this.x0 = null;
            this.k0.p(cArr);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public int Q(Base64Variant base64Variant, InputStream inputStream, int i) throws IOException, JsonGenerationException {
        z1("write a binary value");
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i2 = this.u0;
        this.u0 = i2 + 1;
        bArr[i2] = this.s0;
        byte[] d = this.k0.d();
        try {
            if (i < 0) {
                i = Q1(base64Variant, inputStream, d);
            } else {
                int R1 = R1(base64Variant, inputStream, d, i);
                if (R1 > 0) {
                    a("Too few bytes available: missing " + R1 + " bytes (out of " + i + ")");
                }
            }
            this.k0.o(d);
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr2 = this.t0;
            int i3 = this.u0;
            this.u0 = i3 + 1;
            bArr2[i3] = this.s0;
            return i;
        } catch (Throwable th) {
            this.k0.o(d);
            throw th;
        }
    }

    public final int Q1(Base64Variant base64Variant, InputStream inputStream, byte[] bArr) throws IOException, JsonGenerationException {
        int i = this.v0 - 6;
        int i2 = 2;
        int maxLineLength = base64Variant.getMaxLineLength() >> 2;
        int i3 = -3;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i4 > i3) {
                i5 = O1(inputStream, bArr, i4, i5, bArr.length);
                if (i5 < 3) {
                    break;
                }
                i3 = i5 - 3;
                i4 = 0;
            }
            if (this.u0 > i) {
                J1();
            }
            int i7 = i4 + 1;
            int i8 = i7 + 1;
            i4 = i8 + 1;
            i6 += 3;
            int encodeBase64Chunk = base64Variant.encodeBase64Chunk((((bArr[i7] & 255) | (bArr[i4] << 8)) << 8) | (bArr[i8] & 255), this.t0, this.u0);
            this.u0 = encodeBase64Chunk;
            maxLineLength--;
            if (maxLineLength <= 0) {
                byte[] bArr2 = this.t0;
                int i9 = encodeBase64Chunk + 1;
                this.u0 = i9;
                bArr2[encodeBase64Chunk] = 92;
                this.u0 = i9 + 1;
                bArr2[i9] = 110;
                maxLineLength = base64Variant.getMaxLineLength() >> 2;
            }
        }
        if (i5 > 0) {
            if (this.u0 > i) {
                J1();
            }
            int i10 = bArr[0] << 16;
            if (1 < i5) {
                i10 |= (bArr[1] & 255) << 8;
            } else {
                i2 = 1;
            }
            int i11 = i6 + i2;
            this.u0 = base64Variant.encodeBase64Partial(i10, i2, this.t0, this.u0);
            return i11;
        }
        return i6;
    }

    public final int R1(Base64Variant base64Variant, InputStream inputStream, byte[] bArr, int i) throws IOException, JsonGenerationException {
        int O1;
        int i2 = this.v0 - 6;
        int i3 = 2;
        int maxLineLength = base64Variant.getMaxLineLength() >> 2;
        int i4 = -3;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i <= 2) {
                break;
            }
            if (i5 > i4) {
                i6 = O1(inputStream, bArr, i5, i6, i);
                if (i6 < 3) {
                    i5 = 0;
                    break;
                }
                i4 = i6 - 3;
                i5 = 0;
            }
            if (this.u0 > i2) {
                J1();
            }
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            i5 = i8 + 1;
            i -= 3;
            int encodeBase64Chunk = base64Variant.encodeBase64Chunk((((bArr[i7] & 255) | (bArr[i5] << 8)) << 8) | (bArr[i8] & 255), this.t0, this.u0);
            this.u0 = encodeBase64Chunk;
            maxLineLength--;
            if (maxLineLength <= 0) {
                byte[] bArr2 = this.t0;
                int i9 = encodeBase64Chunk + 1;
                this.u0 = i9;
                bArr2[encodeBase64Chunk] = 92;
                this.u0 = i9 + 1;
                bArr2[i9] = 110;
                maxLineLength = base64Variant.getMaxLineLength() >> 2;
            }
        }
        if (i <= 0 || (O1 = O1(inputStream, bArr, i5, i6, i)) <= 0) {
            return i;
        }
        if (this.u0 > i2) {
            J1();
        }
        int i10 = bArr[0] << 16;
        if (1 < O1) {
            i10 |= (bArr[1] & 255) << 8;
        } else {
            i3 = 1;
        }
        this.u0 = base64Variant.encodeBase64Partial(i10, i3, this.t0, this.u0);
        return i - i3;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void S(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        z1("write a binary value");
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i3 = this.u0;
        this.u0 = i3 + 1;
        bArr2[i3] = this.s0;
        S1(base64Variant, bArr, i, i2 + i);
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr3 = this.t0;
        int i4 = this.u0;
        this.u0 = i4 + 1;
        bArr3[i4] = this.s0;
    }

    public final void S1(Base64Variant base64Variant, byte[] bArr, int i, int i2) throws IOException, JsonGenerationException {
        int i3 = i2 - 3;
        int i4 = this.v0 - 6;
        int maxLineLength = base64Variant.getMaxLineLength() >> 2;
        while (i <= i3) {
            if (this.u0 > i4) {
                J1();
            }
            int i5 = i + 1;
            int i6 = i5 + 1;
            int i7 = i6 + 1;
            int encodeBase64Chunk = base64Variant.encodeBase64Chunk((((bArr[i] << 8) | (bArr[i5] & 255)) << 8) | (bArr[i6] & 255), this.t0, this.u0);
            this.u0 = encodeBase64Chunk;
            maxLineLength--;
            if (maxLineLength <= 0) {
                byte[] bArr2 = this.t0;
                int i8 = encodeBase64Chunk + 1;
                this.u0 = i8;
                bArr2[encodeBase64Chunk] = 92;
                this.u0 = i8 + 1;
                bArr2[i8] = 110;
                maxLineLength = base64Variant.getMaxLineLength() >> 2;
            }
            i = i7;
        }
        int i9 = i2 - i;
        if (i9 > 0) {
            if (this.u0 > i4) {
                J1();
            }
            int i10 = i + 1;
            int i11 = bArr[i] << 16;
            if (i9 == 2) {
                i11 |= (bArr[i10] & 255) << 8;
            }
            this.u0 = base64Variant.encodeBase64Partial(i11, i9, this.t0, this.u0);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void T0(char c) throws IOException {
        if (this.u0 + 3 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        if (c <= 127) {
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = (byte) c;
        } else if (c < 2048) {
            int i2 = this.u0;
            int i3 = i2 + 1;
            this.u0 = i3;
            bArr[i2] = (byte) ((c >> 6) | 192);
            this.u0 = i3 + 1;
            bArr[i3] = (byte) ((c & '?') | 128);
        } else {
            M1(c, null, 0, 0);
        }
    }

    public final void T1(byte[] bArr) throws IOException {
        int length = bArr.length;
        if (this.u0 + length > this.v0) {
            J1();
            if (length > 512) {
                this.r0.write(bArr, 0, length);
                return;
            }
        }
        System.arraycopy(bArr, 0, this.t0, this.u0, length);
        this.u0 += length;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void U0(yl3 yl3Var) throws IOException {
        byte[] asUnquotedUTF8 = yl3Var.asUnquotedUTF8();
        if (asUnquotedUTF8.length > 0) {
            T1(asUnquotedUTF8);
        }
    }

    public final int U1(byte[] bArr, int i, yl3 yl3Var, int i2) throws IOException, JsonGenerationException {
        byte[] asUnquotedUTF8 = yl3Var.asUnquotedUTF8();
        int length = asUnquotedUTF8.length;
        if (length > 6) {
            return K1(bArr, i, this.v0, asUnquotedUTF8, i2);
        }
        System.arraycopy(asUnquotedUTF8, 0, bArr, i, length);
        return i + length;
    }

    public final void V1(String str, int i, int i2) throws IOException {
        if (this.u0 + ((i2 - i) * 6) > this.v0) {
            J1();
        }
        int i3 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        int i4 = this.m0;
        if (i4 <= 0) {
            i4 = 65535;
        }
        CharacterEscapes characterEscapes = this.n0;
        while (i < i2) {
            int i5 = i + 1;
            char charAt = str.charAt(i);
            if (charAt <= 127) {
                if (iArr[charAt] == 0) {
                    bArr[i3] = (byte) charAt;
                    i = i5;
                    i3++;
                } else {
                    int i6 = iArr[charAt];
                    if (i6 > 0) {
                        int i7 = i3 + 1;
                        bArr[i3] = 92;
                        i3 = i7 + 1;
                        bArr[i7] = (byte) i6;
                    } else if (i6 == -2) {
                        yl3 escapeSequence = characterEscapes.getEscapeSequence(charAt);
                        if (escapeSequence == null) {
                            a("Invalid custom escape definitions; custom escape not found for character code 0x" + Integer.toHexString(charAt) + ", although was supposed to have one");
                        }
                        i3 = U1(bArr, i3, escapeSequence, i2 - i5);
                    } else {
                        i3 = X1(charAt, i3);
                    }
                }
            } else if (charAt > i4) {
                i3 = X1(charAt, i3);
            } else {
                yl3 escapeSequence2 = characterEscapes.getEscapeSequence(charAt);
                if (escapeSequence2 != null) {
                    i3 = U1(bArr, i3, escapeSequence2, i2 - i5);
                } else if (charAt <= 2047) {
                    int i8 = i3 + 1;
                    bArr[i3] = (byte) ((charAt >> 6) | 192);
                    i3 = i8 + 1;
                    bArr[i8] = (byte) ((charAt & '?') | 128);
                } else {
                    i3 = L1(charAt, i3);
                }
            }
            i = i5;
        }
        this.u0 = i3;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void W0(String str) throws IOException {
        int length = str.length();
        char[] cArr = this.x0;
        if (length <= cArr.length) {
            str.getChars(0, length, cArr, 0);
            X0(cArr, 0, length);
            return;
        }
        r2(str, 0, length);
    }

    public final void W1(char[] cArr, int i, int i2) throws IOException {
        if (this.u0 + ((i2 - i) * 6) > this.v0) {
            J1();
        }
        int i3 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        int i4 = this.m0;
        if (i4 <= 0) {
            i4 = 65535;
        }
        CharacterEscapes characterEscapes = this.n0;
        while (i < i2) {
            int i5 = i + 1;
            char c = cArr[i];
            if (c <= 127) {
                if (iArr[c] == 0) {
                    bArr[i3] = (byte) c;
                    i = i5;
                    i3++;
                } else {
                    int i6 = iArr[c];
                    if (i6 > 0) {
                        int i7 = i3 + 1;
                        bArr[i3] = 92;
                        i3 = i7 + 1;
                        bArr[i7] = (byte) i6;
                    } else if (i6 == -2) {
                        yl3 escapeSequence = characterEscapes.getEscapeSequence(c);
                        if (escapeSequence == null) {
                            a("Invalid custom escape definitions; custom escape not found for character code 0x" + Integer.toHexString(c) + ", although was supposed to have one");
                        }
                        i3 = U1(bArr, i3, escapeSequence, i2 - i5);
                    } else {
                        i3 = X1(c, i3);
                    }
                }
            } else if (c > i4) {
                i3 = X1(c, i3);
            } else {
                yl3 escapeSequence2 = characterEscapes.getEscapeSequence(c);
                if (escapeSequence2 != null) {
                    i3 = U1(bArr, i3, escapeSequence2, i2 - i5);
                } else if (c <= 2047) {
                    int i8 = i3 + 1;
                    bArr[i3] = (byte) ((c >> 6) | 192);
                    i3 = i8 + 1;
                    bArr[i8] = (byte) ((c & '?') | 128);
                } else {
                    i3 = L1(c, i3);
                }
            }
            i = i5;
        }
        this.u0 = i3;
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x001c, code lost:
        r0 = r7 + 1;
        r7 = r6[r7];
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0022, code lost:
        if (r7 >= 2048) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x0024, code lost:
        r1 = r5.t0;
        r2 = r5.u0;
        r3 = r2 + 1;
        r5.u0 = r3;
        r1[r2] = (byte) ((r7 >> 6) | 192);
        r5.u0 = r3 + 1;
        r1[r3] = (byte) ((r7 & '?') | 128);
        r7 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x0040, code lost:
        r7 = M1(r7, r6, r0, r8);
     */
    @Override // com.fasterxml.jackson.core.JsonGenerator
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void X0(char[] r6, int r7, int r8) throws java.io.IOException {
        /*
            r5 = this;
            int r0 = r8 + r8
            int r0 = r0 + r8
            int r1 = r5.u0
            int r1 = r1 + r0
            int r2 = r5.v0
            if (r1 <= r2) goto L13
            if (r2 >= r0) goto L10
            r5.g2(r6, r7, r8)
            return
        L10:
            r5.J1()
        L13:
            int r8 = r8 + r7
        L14:
            if (r7 >= r8) goto L54
        L16:
            char r0 = r6[r7]
            r1 = 127(0x7f, float:1.78E-43)
            if (r0 <= r1) goto L45
            int r0 = r7 + 1
            char r7 = r6[r7]
            r1 = 2048(0x800, float:2.87E-42)
            if (r7 >= r1) goto L40
            byte[] r1 = r5.t0
            int r2 = r5.u0
            int r3 = r2 + 1
            r5.u0 = r3
            int r4 = r7 >> 6
            r4 = r4 | 192(0xc0, float:2.69E-43)
            byte r4 = (byte) r4
            r1[r2] = r4
            int r2 = r3 + 1
            r5.u0 = r2
            r7 = r7 & 63
            r7 = r7 | 128(0x80, float:1.8E-43)
            byte r7 = (byte) r7
            r1[r3] = r7
            r7 = r0
            goto L14
        L40:
            int r7 = r5.M1(r7, r6, r0, r8)
            goto L14
        L45:
            byte[] r1 = r5.t0
            int r2 = r5.u0
            int r3 = r2 + 1
            r5.u0 = r3
            byte r0 = (byte) r0
            r1[r2] = r0
            int r7 = r7 + 1
            if (r7 < r8) goto L16
        L54:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.je4.X0(char[], int, int):void");
    }

    public final int X1(int i, int i2) throws IOException {
        int i3;
        byte[] bArr = this.t0;
        int i4 = i2 + 1;
        bArr[i2] = 92;
        int i5 = i4 + 1;
        bArr[i4] = 117;
        if (i > 255) {
            int i6 = 255 & (i >> 8);
            int i7 = i5 + 1;
            byte[] bArr2 = A0;
            bArr[i5] = bArr2[i6 >> 4];
            i3 = i7 + 1;
            bArr[i7] = bArr2[i6 & 15];
            i &= 255;
        } else {
            int i8 = i5 + 1;
            bArr[i5] = 48;
            i3 = i8 + 1;
            bArr[i8] = 48;
        }
        int i9 = i3 + 1;
        byte[] bArr3 = A0;
        bArr[i3] = bArr3[i >> 4];
        int i10 = i9 + 1;
        bArr[i9] = bArr3[i & 15];
        return i10;
    }

    public final void Y1() throws IOException {
        if (this.u0 + 4 >= this.v0) {
            J1();
        }
        System.arraycopy(B0, 0, this.t0, this.u0, 4);
        this.u0 += 4;
    }

    @Override // defpackage.we1, com.fasterxml.jackson.core.JsonGenerator
    public void Z0(yl3 yl3Var) throws IOException {
        z1("write a raw (unencoded) value");
        byte[] asUnquotedUTF8 = yl3Var.asUnquotedUTF8();
        if (asUnquotedUTF8.length > 0) {
            T1(asUnquotedUTF8);
        }
    }

    public final void Z1(yl3 yl3Var) throws IOException {
        int t = this.i0.t(yl3Var.getValue());
        if (t == 4) {
            a("Can not write a field name, expecting a value");
        }
        if (t == 1) {
            this.a.writeObjectEntrySeparator(this);
        } else {
            this.a.beforeObjectEntries(this);
        }
        boolean z = !this.p0;
        if (z) {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr = this.t0;
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = this.s0;
        }
        T1(yl3Var.asQuotedUTF8());
        if (z) {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr2 = this.t0;
            int i2 = this.u0;
            this.u0 = i2 + 1;
            bArr2[i2] = this.s0;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void a0(boolean z) throws IOException {
        z1("write a boolean value");
        if (this.u0 + 5 >= this.v0) {
            J1();
        }
        byte[] bArr = z ? C0 : D0;
        int length = bArr.length;
        System.arraycopy(bArr, 0, this.t0, this.u0, length);
        this.u0 += length;
    }

    public final void a2(String str) throws IOException {
        int t = this.i0.t(str);
        if (t == 4) {
            a("Can not write a field name, expecting a value");
        }
        if (t == 1) {
            this.a.writeObjectEntrySeparator(this);
        } else {
            this.a.beforeObjectEntries(this);
        }
        if (this.p0) {
            o2(str, false);
            return;
        }
        int length = str.length();
        if (length > this.y0) {
            o2(str, true);
            return;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = this.s0;
        str.getChars(0, length, this.x0, 0);
        if (length <= this.w0) {
            if (this.u0 + length > this.v0) {
                J1();
            }
            i2(this.x0, 0, length);
        } else {
            p2(this.x0, 0, length);
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i2 = this.u0;
        this.u0 = i2 + 1;
        bArr2[i2] = this.s0;
    }

    public final void b2(int i) throws IOException {
        if (this.u0 + 13 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i2 = this.u0;
        int i3 = i2 + 1;
        this.u0 = i3;
        bArr[i2] = this.s0;
        int o = aj2.o(i, bArr, i3);
        this.u0 = o;
        byte[] bArr2 = this.t0;
        this.u0 = o + 1;
        bArr2[o] = this.s0;
    }

    public final void c2(long j) throws IOException {
        if (this.u0 + 23 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        int i2 = i + 1;
        this.u0 = i2;
        bArr[i] = this.s0;
        int q = aj2.q(j, bArr, i2);
        this.u0 = q;
        byte[] bArr2 = this.t0;
        this.u0 = q + 1;
        bArr2[q] = this.s0;
    }

    @Override // defpackage.we1, com.fasterxml.jackson.core.JsonGenerator, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        super.close();
        if (this.t0 != null && F1(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT)) {
            while (true) {
                cw1 l = l();
                if (l.e()) {
                    e0();
                } else if (!l.f()) {
                    break;
                } else {
                    f0();
                }
            }
        }
        J1();
        this.u0 = 0;
        if (this.r0 != null) {
            if (!this.k0.n() && !F1(JsonGenerator.Feature.AUTO_CLOSE_TARGET)) {
                if (F1(JsonGenerator.Feature.FLUSH_PASSED_TO_STREAM)) {
                    this.r0.flush();
                }
            } else {
                this.r0.close();
            }
        }
        P1();
    }

    public final void d2(String str) throws IOException {
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = this.s0;
        W0(str);
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i2 = this.u0;
        this.u0 = i2 + 1;
        bArr2[i2] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void e0() throws IOException {
        if (!this.i0.e()) {
            a("Current context not Array but " + this.i0.i());
        }
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeEndArray(this, this.i0.c());
        } else {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr = this.t0;
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = 93;
        }
        this.i0 = this.i0.l();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void e1() throws IOException {
        z1("start an array");
        this.i0 = this.i0.m();
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeStartArray(this);
            return;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = 91;
    }

    public final void e2(short s) throws IOException {
        if (this.u0 + 8 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        int i2 = i + 1;
        this.u0 = i2;
        bArr[i] = this.s0;
        int o = aj2.o(s, bArr, i2);
        this.u0 = o;
        byte[] bArr2 = this.t0;
        this.u0 = o + 1;
        bArr2[o] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void f0() throws IOException {
        if (!this.i0.f()) {
            a("Current context not Object but " + this.i0.i());
        }
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeEndObject(this, this.i0.c());
        } else {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr = this.t0;
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = 125;
        }
        this.i0 = this.i0.l();
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0008, code lost:
        r0 = r7 + 1;
        r7 = r6[r7];
     */
    /* JADX WARN: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r7 >= 2048) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0010, code lost:
        r1 = r5.t0;
        r2 = r5.u0;
        r3 = r2 + 1;
        r5.u0 = r3;
        r1[r2] = (byte) ((r7 >> 6) | 192);
        r5.u0 = r3 + 1;
        r1[r3] = (byte) ((r7 & '?') | 128);
        r7 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x002c, code lost:
        r7 = M1(r7, r6, r0, r8);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void f2(char[] r6, int r7, int r8) throws java.io.IOException {
        /*
            r5 = this;
        L0:
            if (r7 >= r8) goto L40
        L2:
            char r0 = r6[r7]
            r1 = 127(0x7f, float:1.78E-43)
            if (r0 <= r1) goto L31
            int r0 = r7 + 1
            char r7 = r6[r7]
            r1 = 2048(0x800, float:2.87E-42)
            if (r7 >= r1) goto L2c
            byte[] r1 = r5.t0
            int r2 = r5.u0
            int r3 = r2 + 1
            r5.u0 = r3
            int r4 = r7 >> 6
            r4 = r4 | 192(0xc0, float:2.69E-43)
            byte r4 = (byte) r4
            r1[r2] = r4
            int r2 = r3 + 1
            r5.u0 = r2
            r7 = r7 & 63
            r7 = r7 | 128(0x80, float:1.8E-43)
            byte r7 = (byte) r7
            r1[r3] = r7
            r7 = r0
            goto L0
        L2c:
            int r7 = r5.M1(r7, r6, r0, r8)
            goto L0
        L31:
            byte[] r1 = r5.t0
            int r2 = r5.u0
            int r3 = r2 + 1
            r5.u0 = r3
            byte r0 = (byte) r0
            r1[r2] = r0
            int r7 = r7 + 1
            if (r7 < r8) goto L2
        L40:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.je4.f2(char[], int, int):void");
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator, java.io.Flushable
    public void flush() throws IOException {
        J1();
        if (this.r0 == null || !F1(JsonGenerator.Feature.FLUSH_PASSED_TO_STREAM)) {
            return;
        }
        this.r0.flush();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void g0(yl3 yl3Var) throws IOException {
        if (this.a != null) {
            Z1(yl3Var);
            return;
        }
        int t = this.i0.t(yl3Var.getValue());
        if (t == 4) {
            a("Can not write a field name, expecting a value");
        }
        if (t == 1) {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr = this.t0;
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = 44;
        }
        if (this.p0) {
            q2(yl3Var);
            return;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i2 = this.u0;
        int i3 = i2 + 1;
        this.u0 = i3;
        bArr2[i2] = this.s0;
        int appendQuotedUTF8 = yl3Var.appendQuotedUTF8(bArr2, i3);
        if (appendQuotedUTF8 < 0) {
            T1(yl3Var.asQuotedUTF8());
        } else {
            this.u0 += appendQuotedUTF8;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr3 = this.t0;
        int i4 = this.u0;
        this.u0 = i4 + 1;
        bArr3[i4] = this.s0;
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x001e, code lost:
        if (r9 >= 2048) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0020, code lost:
        r4 = r7.u0;
        r5 = r4 + 1;
        r7.u0 = r5;
        r1[r4] = (byte) ((r9 >> 6) | 192);
        r7.u0 = r5 + 1;
        r1[r5] = (byte) ((r9 & '?') | 128);
        r9 = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x0039, code lost:
        r9 = M1(r9, r8, r2, r10);
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0013, code lost:
        if ((r7.u0 + 3) < r7.v0) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x0015, code lost:
        J1();
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0018, code lost:
        r2 = r9 + 1;
        r9 = r8[r9];
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void g2(char[] r8, int r9, int r10) throws java.io.IOException {
        /*
            r7 = this;
            int r0 = r7.v0
            byte[] r1 = r7.t0
            int r10 = r10 + r9
        L5:
            if (r9 >= r10) goto L52
        L7:
            char r2 = r8[r9]
            r3 = 128(0x80, float:1.8E-43)
            if (r2 < r3) goto L3e
            int r2 = r7.u0
            int r2 = r2 + 3
            int r4 = r7.v0
            if (r2 < r4) goto L18
            r7.J1()
        L18:
            int r2 = r9 + 1
            char r9 = r8[r9]
            r4 = 2048(0x800, float:2.87E-42)
            if (r9 >= r4) goto L39
            int r4 = r7.u0
            int r5 = r4 + 1
            r7.u0 = r5
            int r6 = r9 >> 6
            r6 = r6 | 192(0xc0, float:2.69E-43)
            byte r6 = (byte) r6
            r1[r4] = r6
            int r4 = r5 + 1
            r7.u0 = r4
            r9 = r9 & 63
            r9 = r9 | r3
            byte r9 = (byte) r9
            r1[r5] = r9
            r9 = r2
            goto L5
        L39:
            int r9 = r7.M1(r9, r8, r2, r10)
            goto L5
        L3e:
            int r3 = r7.u0
            if (r3 < r0) goto L45
            r7.J1()
        L45:
            int r3 = r7.u0
            int r4 = r3 + 1
            r7.u0 = r4
            byte r2 = (byte) r2
            r1[r3] = r2
            int r9 = r9 + 1
            if (r9 < r10) goto L7
        L52:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.je4.g2(char[], int, int):void");
    }

    public final void h2(String str, int i, int i2) throws IOException {
        int i3 = i2 + i;
        int i4 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        while (i < i3) {
            char charAt = str.charAt(i);
            if (charAt > 127 || iArr[charAt] != 0) {
                break;
            }
            bArr[i4] = (byte) charAt;
            i++;
            i4++;
        }
        this.u0 = i4;
        if (i < i3) {
            if (this.n0 != null) {
                V1(str, i, i3);
            } else if (this.m0 == 0) {
                j2(str, i, i3);
            } else {
                l2(str, i, i3);
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void i0(String str) throws IOException {
        if (this.a != null) {
            a2(str);
            return;
        }
        int t = this.i0.t(str);
        if (t == 4) {
            a("Can not write a field name, expecting a value");
        }
        if (t == 1) {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr = this.t0;
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = 44;
        }
        if (this.p0) {
            o2(str, false);
            return;
        }
        int length = str.length();
        if (length > this.y0) {
            o2(str, true);
            return;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i2 = this.u0;
        int i3 = i2 + 1;
        this.u0 = i3;
        bArr2[i2] = this.s0;
        if (length <= this.w0) {
            if (i3 + length > this.v0) {
                J1();
            }
            h2(str, 0, length);
        } else {
            n2(str, 0, length);
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr3 = this.t0;
        int i4 = this.u0;
        this.u0 = i4 + 1;
        bArr3[i4] = this.s0;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void i1() throws IOException {
        z1("start an object");
        this.i0 = this.i0.n();
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeStartObject(this);
            return;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = 123;
    }

    public final void i2(char[] cArr, int i, int i2) throws IOException {
        int i3 = i2 + i;
        int i4 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        while (i < i3) {
            char c = cArr[i];
            if (c > 127 || iArr[c] != 0) {
                break;
            }
            bArr[i4] = (byte) c;
            i++;
            i4++;
        }
        this.u0 = i4;
        if (i < i3) {
            if (this.n0 != null) {
                W1(cArr, i, i3);
            } else if (this.m0 == 0) {
                k2(cArr, i, i3);
            } else {
                m2(cArr, i, i3);
            }
        }
    }

    public final void j2(String str, int i, int i2) throws IOException {
        if (this.u0 + ((i2 - i) * 6) > this.v0) {
            J1();
        }
        int i3 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        while (i < i2) {
            int i4 = i + 1;
            char charAt = str.charAt(i);
            if (charAt <= 127) {
                if (iArr[charAt] == 0) {
                    bArr[i3] = (byte) charAt;
                    i = i4;
                    i3++;
                } else {
                    int i5 = iArr[charAt];
                    if (i5 > 0) {
                        int i6 = i3 + 1;
                        bArr[i3] = 92;
                        i3 = i6 + 1;
                        bArr[i6] = (byte) i5;
                    } else {
                        i3 = X1(charAt, i3);
                    }
                }
            } else if (charAt <= 2047) {
                int i7 = i3 + 1;
                bArr[i3] = (byte) ((charAt >> 6) | 192);
                i3 = i7 + 1;
                bArr[i7] = (byte) ((charAt & '?') | 128);
            } else {
                i3 = L1(charAt, i3);
            }
            i = i4;
        }
        this.u0 = i3;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void k1(Object obj) throws IOException {
        z1("start an object");
        mw1 n = this.i0.n();
        this.i0 = n;
        if (obj != null) {
            n.h(obj);
        }
        d dVar = this.a;
        if (dVar != null) {
            dVar.writeStartObject(this);
            return;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = 123;
    }

    public final void k2(char[] cArr, int i, int i2) throws IOException {
        if (this.u0 + ((i2 - i) * 6) > this.v0) {
            J1();
        }
        int i3 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        while (i < i2) {
            int i4 = i + 1;
            char c = cArr[i];
            if (c <= 127) {
                if (iArr[c] == 0) {
                    bArr[i3] = (byte) c;
                    i = i4;
                    i3++;
                } else {
                    int i5 = iArr[c];
                    if (i5 > 0) {
                        int i6 = i3 + 1;
                        bArr[i3] = 92;
                        i3 = i6 + 1;
                        bArr[i6] = (byte) i5;
                    } else {
                        i3 = X1(c, i3);
                    }
                }
            } else if (c <= 2047) {
                int i7 = i3 + 1;
                bArr[i3] = (byte) ((c >> 6) | 192);
                i3 = i7 + 1;
                bArr[i7] = (byte) ((c & '?') | 128);
            } else {
                i3 = L1(c, i3);
            }
            i = i4;
        }
        this.u0 = i3;
    }

    public final void l2(String str, int i, int i2) throws IOException {
        if (this.u0 + ((i2 - i) * 6) > this.v0) {
            J1();
        }
        int i3 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        int i4 = this.m0;
        while (i < i2) {
            int i5 = i + 1;
            char charAt = str.charAt(i);
            if (charAt <= 127) {
                if (iArr[charAt] == 0) {
                    bArr[i3] = (byte) charAt;
                    i = i5;
                    i3++;
                } else {
                    int i6 = iArr[charAt];
                    if (i6 > 0) {
                        int i7 = i3 + 1;
                        bArr[i3] = 92;
                        i3 = i7 + 1;
                        bArr[i7] = (byte) i6;
                    } else {
                        i3 = X1(charAt, i3);
                    }
                }
            } else if (charAt > i4) {
                i3 = X1(charAt, i3);
            } else if (charAt <= 2047) {
                int i8 = i3 + 1;
                bArr[i3] = (byte) ((charAt >> 6) | 192);
                i3 = i8 + 1;
                bArr[i8] = (byte) ((charAt & '?') | 128);
            } else {
                i3 = L1(charAt, i3);
            }
            i = i5;
        }
        this.u0 = i3;
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void m0() throws IOException {
        z1("write a null");
        Y1();
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public final void m1(yl3 yl3Var) throws IOException {
        z1("write a string");
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        int i2 = i + 1;
        this.u0 = i2;
        bArr[i] = this.s0;
        int appendQuotedUTF8 = yl3Var.appendQuotedUTF8(bArr, i2);
        if (appendQuotedUTF8 < 0) {
            T1(yl3Var.asQuotedUTF8());
        } else {
            this.u0 += appendQuotedUTF8;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i3 = this.u0;
        this.u0 = i3 + 1;
        bArr2[i3] = this.s0;
    }

    public final void m2(char[] cArr, int i, int i2) throws IOException {
        if (this.u0 + ((i2 - i) * 6) > this.v0) {
            J1();
        }
        int i3 = this.u0;
        byte[] bArr = this.t0;
        int[] iArr = this.l0;
        int i4 = this.m0;
        while (i < i2) {
            int i5 = i + 1;
            char c = cArr[i];
            if (c <= 127) {
                if (iArr[c] == 0) {
                    bArr[i3] = (byte) c;
                    i = i5;
                    i3++;
                } else {
                    int i6 = iArr[c];
                    if (i6 > 0) {
                        int i7 = i3 + 1;
                        bArr[i3] = 92;
                        i3 = i7 + 1;
                        bArr[i7] = (byte) i6;
                    } else {
                        i3 = X1(c, i3);
                    }
                }
            } else if (c > i4) {
                i3 = X1(c, i3);
            } else if (c <= 2047) {
                int i8 = i3 + 1;
                bArr[i3] = (byte) ((c >> 6) | 192);
                i3 = i8 + 1;
                bArr[i8] = (byte) ((c & '?') | 128);
            } else {
                i3 = L1(c, i3);
            }
            i = i5;
        }
        this.u0 = i3;
    }

    public final void n2(String str, int i, int i2) throws IOException {
        do {
            int min = Math.min(this.w0, i2);
            if (this.u0 + min > this.v0) {
                J1();
            }
            h2(str, i, min);
            i += min;
            i2 -= min;
        } while (i2 > 0);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void o1(String str) throws IOException {
        z1("write a string");
        if (str == null) {
            Y1();
            return;
        }
        int length = str.length();
        if (length > this.w0) {
            o2(str, true);
            return;
        }
        if (this.u0 + length >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = this.s0;
        h2(str, 0, length);
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i2 = this.u0;
        this.u0 = i2 + 1;
        bArr2[i2] = this.s0;
    }

    public final void o2(String str, boolean z) throws IOException {
        if (z) {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr = this.t0;
            int i = this.u0;
            this.u0 = i + 1;
            bArr[i] = this.s0;
        }
        int length = str.length();
        int i2 = 0;
        while (length > 0) {
            int min = Math.min(this.w0, length);
            if (this.u0 + min > this.v0) {
                J1();
            }
            h2(str, i2, min);
            i2 += min;
            length -= min;
        }
        if (z) {
            if (this.u0 >= this.v0) {
                J1();
            }
            byte[] bArr2 = this.t0;
            int i3 = this.u0;
            this.u0 = i3 + 1;
            bArr2[i3] = this.s0;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void p1(char[] cArr, int i, int i2) throws IOException {
        z1("write a string");
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i3 = this.u0;
        int i4 = i3 + 1;
        this.u0 = i4;
        bArr[i3] = this.s0;
        if (i2 <= this.w0) {
            if (i4 + i2 > this.v0) {
                J1();
            }
            i2(cArr, i, i2);
        } else {
            p2(cArr, i, i2);
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr2 = this.t0;
        int i5 = this.u0;
        this.u0 = i5 + 1;
        bArr2[i5] = this.s0;
    }

    public final void p2(char[] cArr, int i, int i2) throws IOException {
        do {
            int min = Math.min(this.w0, i2);
            if (this.u0 + min > this.v0) {
                J1();
            }
            i2(cArr, i, min);
            i += min;
            i2 -= min;
        } while (i2 > 0);
    }

    public final void q2(yl3 yl3Var) throws IOException {
        int appendQuotedUTF8 = yl3Var.appendQuotedUTF8(this.t0, this.u0);
        if (appendQuotedUTF8 < 0) {
            T1(yl3Var.asQuotedUTF8());
        } else {
            this.u0 += appendQuotedUTF8;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void r0(double d) throws IOException {
        if (!this.h0 && ((!Double.isNaN(d) && !Double.isInfinite(d)) || !JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS.enabledIn(this.g0))) {
            z1("write a number");
            W0(String.valueOf(d));
            return;
        }
        o1(String.valueOf(d));
    }

    public void r2(String str, int i, int i2) throws IOException {
        char c;
        char[] cArr = this.x0;
        int length = cArr.length;
        if (i2 <= length) {
            str.getChars(i, i + i2, cArr, 0);
            X0(cArr, 0, i2);
            return;
        }
        int i3 = this.v0;
        int min = Math.min(length, (i3 >> 2) + (i3 >> 4));
        int i4 = min * 3;
        while (i2 > 0) {
            int min2 = Math.min(min, i2);
            str.getChars(i, i + min2, cArr, 0);
            if (this.u0 + i4 > this.v0) {
                J1();
            }
            if (min2 > 1 && (c = cArr[min2 - 1]) >= 55296 && c <= 56319) {
                min2--;
            }
            f2(cArr, 0, min2);
            i += min2;
            i2 -= min2;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void w0(float f) throws IOException {
        if (!this.h0 && ((!Float.isNaN(f) && !Float.isInfinite(f)) || !JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS.enabledIn(this.g0))) {
            z1("write a number");
            W0(String.valueOf(f));
            return;
        }
        o1(String.valueOf(f));
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void x0(int i) throws IOException {
        z1("write a number");
        if (this.u0 + 11 >= this.v0) {
            J1();
        }
        if (this.h0) {
            b2(i);
        } else {
            this.u0 = aj2.o(i, this.t0, this.u0);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void y0(long j) throws IOException {
        z1("write a number");
        if (this.h0) {
            c2(j);
            return;
        }
        if (this.u0 + 21 >= this.v0) {
            J1();
        }
        this.u0 = aj2.q(j, this.t0, this.u0);
    }

    @Override // com.fasterxml.jackson.core.JsonGenerator
    public void z0(String str) throws IOException {
        z1("write a number");
        if (this.h0) {
            d2(str);
        } else {
            W0(str);
        }
    }

    @Override // defpackage.we1
    public final void z1(String str) throws IOException {
        byte b;
        int u = this.i0.u();
        if (this.a != null) {
            I1(str, u);
            return;
        }
        if (u == 1) {
            b = 44;
        } else if (u != 2) {
            if (u != 3) {
                if (u != 5) {
                    return;
                }
                H1(str);
                return;
            }
            yl3 yl3Var = this.o0;
            if (yl3Var != null) {
                byte[] asUnquotedUTF8 = yl3Var.asUnquotedUTF8();
                if (asUnquotedUTF8.length > 0) {
                    T1(asUnquotedUTF8);
                    return;
                }
                return;
            }
            return;
        } else {
            b = 58;
        }
        if (this.u0 >= this.v0) {
            J1();
        }
        byte[] bArr = this.t0;
        int i = this.u0;
        this.u0 = i + 1;
        bArr[i] = b;
    }
}
