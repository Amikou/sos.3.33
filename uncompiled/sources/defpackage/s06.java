package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: s06  reason: default package */
/* loaded from: classes.dex */
public final class s06 implements r06 {
    public static final wo5<Long> a;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        ro5Var.b("measurement.client.consent_state_v1", true);
        ro5Var.b("measurement.client.3p_consent_state_v1", true);
        ro5Var.b("measurement.service.consent_state_v1_W36", true);
        ro5Var.a("measurement.id.service.consent_state_v1_W36", 0L);
        a = ro5Var.a("measurement.service.storage_consent_support_version", 203590L);
    }

    @Override // defpackage.r06
    public final long zza() {
        return a.e().longValue();
    }
}
