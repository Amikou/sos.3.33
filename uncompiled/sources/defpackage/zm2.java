package defpackage;

import android.view.animation.Interpolator;

/* compiled from: OneSignalBounceInterpolator.java */
/* renamed from: zm2  reason: default package */
/* loaded from: classes2.dex */
public class zm2 implements Interpolator {
    public double a;
    public double b;

    public zm2(double d, double d2) {
        this.a = 1.0d;
        this.b = 10.0d;
        this.a = d;
        this.b = d2;
    }

    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        return (float) ((Math.pow(2.718281828459045d, (-f) / this.a) * (-1.0d) * Math.cos(this.b * f)) + 1.0d);
    }
}
