package defpackage;

import com.facebook.common.internal.ImmutableMap;
import java.util.Locale;
import java.util.Map;

/* compiled from: MediaUtils.java */
/* renamed from: f72  reason: default package */
/* loaded from: classes.dex */
public class f72 {
    public static final Map<String, String> a = ImmutableMap.of("mkv", "video/x-matroska", "glb", "model/gltf-binary");

    public static String a(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf < 0 || lastIndexOf == str.length() - 1) {
            return null;
        }
        return str.substring(lastIndexOf + 1);
    }

    public static String b(String str) {
        String a2 = a(str);
        if (a2 == null) {
            return null;
        }
        String lowerCase = a2.toLowerCase(Locale.US);
        String a3 = x82.a(lowerCase);
        return a3 == null ? a.get(lowerCase) : a3;
    }

    public static boolean c(String str) {
        return str != null && str.startsWith("video/");
    }
}
