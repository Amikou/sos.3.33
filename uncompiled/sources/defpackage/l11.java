package defpackage;

import com.google.crypto.tink.shaded.protobuf.o;
import com.google.crypto.tink.shaded.protobuf.p;

/* compiled from: ExtensionSchemas.java */
/* renamed from: l11  reason: default package */
/* loaded from: classes2.dex */
public final class l11 {
    public static final o<?> a = new p();
    public static final o<?> b = c();

    public static o<?> a() {
        o<?> oVar = b;
        if (oVar != null) {
            return oVar;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    public static o<?> b() {
        return a;
    }

    public static o<?> c() {
        try {
            return (o) Class.forName("com.google.crypto.tink.shaded.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
