package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.priceAlert.PAToken;
import net.safemoon.androidwallet.model.transaction.history.Result;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;
import org.web3j.abi.datatypes.Address;

/* compiled from: TransferHistoryFragmentDirections.java */
/* renamed from: ea4  reason: default package */
/* loaded from: classes2.dex */
public class ea4 {

    /* compiled from: TransferHistoryFragmentDirections.java */
    /* renamed from: ea4$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("token")) {
                PAToken pAToken = (PAToken) this.a.get("token");
                if (!Parcelable.class.isAssignableFrom(PAToken.class) && pAToken != null) {
                    if (Serializable.class.isAssignableFrom(PAToken.class)) {
                        bundle.putSerializable("token", (Serializable) Serializable.class.cast(pAToken));
                    } else {
                        throw new UnsupportedOperationException(PAToken.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("token", (Parcelable) Parcelable.class.cast(pAToken));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_transferHistoryFragment_to_cryptoPriceAlertFragment;
        }

        public PAToken c() {
            return (PAToken) this.a.get("token");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("token") != bVar.a.containsKey("token")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionTransferHistoryFragmentToCryptoPriceAlertFragment(actionId=" + b() + "){token=" + c() + "}";
        }

        public b(PAToken pAToken) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (pAToken != null) {
                hashMap.put("token", pAToken);
                return;
            }
            throw new IllegalArgumentException("Argument \"token\" is marked as non-null but was passed a null value.");
        }
    }

    /* compiled from: TransferHistoryFragmentDirections.java */
    /* renamed from: ea4$c */
    /* loaded from: classes2.dex */
    public static class c implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("userTokenData")) {
                UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) this.a.get("userTokenData");
                if (!Parcelable.class.isAssignableFrom(UserTokenItemDisplayModel.class) && userTokenItemDisplayModel != null) {
                    if (Serializable.class.isAssignableFrom(UserTokenItemDisplayModel.class)) {
                        bundle.putSerializable("userTokenData", (Serializable) Serializable.class.cast(userTokenItemDisplayModel));
                    } else {
                        throw new UnsupportedOperationException(UserTokenItemDisplayModel.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("userTokenData", (Parcelable) Parcelable.class.cast(userTokenItemDisplayModel));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_transferHistoryFragment_to_navigation_swap;
        }

        public UserTokenItemDisplayModel c() {
            return (UserTokenItemDisplayModel) this.a.get("userTokenData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.a.containsKey("userTokenData") != cVar.a.containsKey("userTokenData")) {
                return false;
            }
            if (c() == null ? cVar.c() == null : c().equals(cVar.c())) {
                return b() == cVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionTransferHistoryFragmentToNavigationSwap(actionId=" + b() + "){userTokenData=" + c() + "}";
        }

        public c(UserTokenItemDisplayModel userTokenItemDisplayModel) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (userTokenItemDisplayModel != null) {
                hashMap.put("userTokenData", userTokenItemDisplayModel);
                return;
            }
            throw new IllegalArgumentException("Argument \"userTokenData\" is marked as non-null but was passed a null value.");
        }
    }

    /* compiled from: TransferHistoryFragmentDirections.java */
    /* renamed from: ea4$d */
    /* loaded from: classes2.dex */
    public static class d implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("tokenChainId")) {
                bundle.putInt("tokenChainId", ((Integer) this.a.get("tokenChainId")).intValue());
            }
            if (this.a.containsKey(Address.TYPE_NAME)) {
                bundle.putString(Address.TYPE_NAME, (String) this.a.get(Address.TYPE_NAME));
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_transferHistoryFragment_to_qrFragment;
        }

        public String c() {
            return (String) this.a.get(Address.TYPE_NAME);
        }

        public int d() {
            return ((Integer) this.a.get("tokenChainId")).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.a.containsKey("tokenChainId") == dVar.a.containsKey("tokenChainId") && d() == dVar.d() && this.a.containsKey(Address.TYPE_NAME) == dVar.a.containsKey(Address.TYPE_NAME)) {
                if (c() == null ? dVar.c() == null : c().equals(dVar.c())) {
                    return b() == dVar.b();
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return ((((d() + 31) * 31) + (c() != null ? c().hashCode() : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionTransferHistoryFragmentToQrFragment(actionId=" + b() + "){tokenChainId=" + d() + ", address=" + c() + "}";
        }

        public d(int i, String str) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            hashMap.put("tokenChainId", Integer.valueOf(i));
            if (str != null) {
                hashMap.put(Address.TYPE_NAME, str);
                return;
            }
            throw new IllegalArgumentException("Argument \"address\" is marked as non-null but was passed a null value.");
        }
    }

    /* compiled from: TransferHistoryFragmentDirections.java */
    /* renamed from: ea4$e */
    /* loaded from: classes2.dex */
    public static class e implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("userTokenData")) {
                UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) this.a.get("userTokenData");
                if (!Parcelable.class.isAssignableFrom(UserTokenItemDisplayModel.class) && userTokenItemDisplayModel != null) {
                    if (Serializable.class.isAssignableFrom(UserTokenItemDisplayModel.class)) {
                        bundle.putSerializable("userTokenData", (Serializable) Serializable.class.cast(userTokenItemDisplayModel));
                    } else {
                        throw new UnsupportedOperationException(UserTokenItemDisplayModel.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("userTokenData", (Parcelable) Parcelable.class.cast(userTokenItemDisplayModel));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_transferHistoryFragment_to_sendtoFragment;
        }

        public UserTokenItemDisplayModel c() {
            return (UserTokenItemDisplayModel) this.a.get("userTokenData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || e.class != obj.getClass()) {
                return false;
            }
            e eVar = (e) obj;
            if (this.a.containsKey("userTokenData") != eVar.a.containsKey("userTokenData")) {
                return false;
            }
            if (c() == null ? eVar.c() == null : c().equals(eVar.c())) {
                return b() == eVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionTransferHistoryFragmentToSendtoFragment(actionId=" + b() + "){userTokenData=" + c() + "}";
        }

        public e(UserTokenItemDisplayModel userTokenItemDisplayModel) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (userTokenItemDisplayModel != null) {
                hashMap.put("userTokenData", userTokenItemDisplayModel);
                return;
            }
            throw new IllegalArgumentException("Argument \"userTokenData\" is marked as non-null but was passed a null value.");
        }
    }

    /* compiled from: TransferHistoryFragmentDirections.java */
    /* renamed from: ea4$f */
    /* loaded from: classes2.dex */
    public static class f implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("result")) {
                Result result = (Result) this.a.get("result");
                if (!Parcelable.class.isAssignableFrom(Result.class) && result != null) {
                    if (Serializable.class.isAssignableFrom(Result.class)) {
                        bundle.putSerializable("result", (Serializable) Serializable.class.cast(result));
                    } else {
                        throw new UnsupportedOperationException(Result.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("result", (Parcelable) Parcelable.class.cast(result));
                }
            }
            if (this.a.containsKey("tokenChainId")) {
                bundle.putInt("tokenChainId", ((Integer) this.a.get("tokenChainId")).intValue());
            }
            if (this.a.containsKey("isNewTransaction")) {
                bundle.putBoolean("isNewTransaction", ((Boolean) this.a.get("isNewTransaction")).booleanValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_transferHistoryFragment_to_transferDetailsFragmentStatus;
        }

        public boolean c() {
            return ((Boolean) this.a.get("isNewTransaction")).booleanValue();
        }

        public Result d() {
            return (Result) this.a.get("result");
        }

        public int e() {
            return ((Integer) this.a.get("tokenChainId")).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || f.class != obj.getClass()) {
                return false;
            }
            f fVar = (f) obj;
            if (this.a.containsKey("result") != fVar.a.containsKey("result")) {
                return false;
            }
            if (d() == null ? fVar.d() == null : d().equals(fVar.d())) {
                return this.a.containsKey("tokenChainId") == fVar.a.containsKey("tokenChainId") && e() == fVar.e() && this.a.containsKey("isNewTransaction") == fVar.a.containsKey("isNewTransaction") && c() == fVar.c() && b() == fVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((((((d() != null ? d().hashCode() : 0) + 31) * 31) + e()) * 31) + (c() ? 1 : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionTransferHistoryFragmentToTransferDetailsFragmentStatus(actionId=" + b() + "){result=" + d() + ", tokenChainId=" + e() + ", isNewTransaction=" + c() + "}";
        }

        public f(Result result, int i, boolean z) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (result != null) {
                hashMap.put("result", result);
                hashMap.put("tokenChainId", Integer.valueOf(i));
                hashMap.put("isNewTransaction", Boolean.valueOf(z));
                return;
            }
            throw new IllegalArgumentException("Argument \"result\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(PAToken pAToken) {
        return new b(pAToken);
    }

    public static c b(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        return new c(userTokenItemDisplayModel);
    }

    public static d c(int i, String str) {
        return new d(i, str);
    }

    public static e d(UserTokenItemDisplayModel userTokenItemDisplayModel) {
        return new e(userTokenItemDisplayModel);
    }

    public static f e(Result result, int i, boolean z) {
        return new f(result, i, z);
    }
}
