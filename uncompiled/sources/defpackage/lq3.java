package defpackage;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.core.graphics.drawable.a;
import java.util.Objects;
import kotlin.jvm.internal.Ref$BooleanRef;
import net.safemoon.androidwallet.views.slidetoact.SlideToActView;

/* compiled from: SlideToActIconUtil.kt */
/* renamed from: lq3  reason: default package */
/* loaded from: classes2.dex */
public final class lq3 {
    public static final lq3 a = new lq3();

    public static final void d(Drawable drawable, SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(drawable, "$icon");
        fs1.f(slideToActView, "$view");
        Object animatedValue = valueAnimator.getAnimatedValue();
        Objects.requireNonNull(animatedValue, "null cannot be cast to non-null type kotlin.Int");
        drawable.setAlpha(((Integer) animatedValue).intValue());
        slideToActView.invalidate();
    }

    public static final void e(Ref$BooleanRef ref$BooleanRef, Drawable drawable, SlideToActView slideToActView, ValueAnimator valueAnimator) {
        fs1.f(ref$BooleanRef, "$startedOnce");
        fs1.f(drawable, "$icon");
        fs1.f(slideToActView, "$view");
        if (ref$BooleanRef.element) {
            return;
        }
        a.h(drawable);
        slideToActView.invalidate();
        ref$BooleanRef.element = true;
    }

    public final ValueAnimator c(final SlideToActView slideToActView, final Drawable drawable, ValueAnimator.AnimatorUpdateListener animatorUpdateListener) {
        fs1.f(slideToActView, "view");
        fs1.f(drawable, "icon");
        fs1.f(animatorUpdateListener, "listener");
        if (f(drawable)) {
            ValueAnimator ofInt = ValueAnimator.ofInt(0, 255);
            ofInt.addUpdateListener(animatorUpdateListener);
            ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: jq3
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    lq3.d(drawable, slideToActView, valueAnimator);
                }
            });
            fs1.e(ofInt, "tickAnimator");
            return ofInt;
        }
        ValueAnimator ofInt2 = ValueAnimator.ofInt(0);
        final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ofInt2.addUpdateListener(animatorUpdateListener);
        ofInt2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: kq3
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                lq3.e(Ref$BooleanRef.this, drawable, slideToActView, valueAnimator);
            }
        });
        fs1.e(ofInt2, "tickAnimator");
        return ofInt2;
    }

    public final boolean f(Drawable drawable) {
        int i = Build.VERSION.SDK_INT;
        if (i <= 24) {
            return true;
        }
        if (i < 21 || (drawable instanceof AnimatedVectorDrawable)) {
            return i < 21 && !(drawable instanceof be);
        }
        return true;
    }

    public final Drawable g(Context context, int i) {
        fs1.f(context, "context");
        if (Build.VERSION.SDK_INT >= 21) {
            Drawable drawable = context.getResources().getDrawable(i, context.getTheme());
            fs1.e(drawable, "{\n            context.re… context.theme)\n        }");
            return drawable;
        }
        be a2 = be.a(context, i);
        if (a2 == null) {
            Drawable f = m70.f(context, i);
            fs1.d(f);
            fs1.e(f, "getDrawable(context, value)!!");
            return f;
        }
        return a2;
    }

    public final void h(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 21 && (drawable instanceof AnimatedVectorDrawable)) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (drawable instanceof be) {
            ((be) drawable).start();
        }
    }

    public final void i(Drawable drawable, int i) {
        fs1.f(drawable, "icon");
        if (Build.VERSION.SDK_INT >= 21) {
            drawable.setTint(i);
        } else if (drawable instanceof be) {
            ((be) drawable).setTint(i);
        } else {
            a.n(drawable, i);
        }
    }
}
