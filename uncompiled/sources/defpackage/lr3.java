package defpackage;

/* compiled from: SparseArrayCompat.java */
/* renamed from: lr3  reason: default package */
/* loaded from: classes.dex */
public class lr3<E> implements Cloneable {
    public static final Object i0 = new Object();
    public boolean a;
    public int[] f0;
    public Object[] g0;
    public int h0;

    public lr3() {
        this(10);
    }

    public void a(int i, E e) {
        int i2 = this.h0;
        if (i2 != 0 && i <= this.f0[i2 - 1]) {
            k(i, e);
            return;
        }
        if (this.a && i2 >= this.f0.length) {
            e();
        }
        int i3 = this.h0;
        if (i3 >= this.f0.length) {
            int e2 = c70.e(i3 + 1);
            int[] iArr = new int[e2];
            Object[] objArr = new Object[e2];
            int[] iArr2 = this.f0;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            Object[] objArr2 = this.g0;
            System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
            this.f0 = iArr;
            this.g0 = objArr;
        }
        this.f0[i3] = i;
        this.g0[i3] = e;
        this.h0 = i3 + 1;
    }

    public void b() {
        int i = this.h0;
        Object[] objArr = this.g0;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.h0 = 0;
        this.a = false;
    }

    /* renamed from: d */
    public lr3<E> clone() {
        try {
            lr3<E> lr3Var = (lr3) super.clone();
            lr3Var.f0 = (int[]) this.f0.clone();
            lr3Var.g0 = (Object[]) this.g0.clone();
            return lr3Var;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final void e() {
        int i = this.h0;
        int[] iArr = this.f0;
        Object[] objArr = this.g0;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != i0) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.a = false;
        this.h0 = i2;
    }

    public E f(int i) {
        return g(i, null);
    }

    public E g(int i, E e) {
        int a = c70.a(this.f0, this.h0, i);
        if (a >= 0) {
            Object[] objArr = this.g0;
            if (objArr[a] != i0) {
                return (E) objArr[a];
            }
        }
        return e;
    }

    public int h(E e) {
        if (this.a) {
            e();
        }
        for (int i = 0; i < this.h0; i++) {
            if (this.g0[i] == e) {
                return i;
            }
        }
        return -1;
    }

    public int j(int i) {
        if (this.a) {
            e();
        }
        return this.f0[i];
    }

    public void k(int i, E e) {
        int a = c70.a(this.f0, this.h0, i);
        if (a >= 0) {
            this.g0[a] = e;
            return;
        }
        int i2 = ~a;
        int i3 = this.h0;
        if (i2 < i3) {
            Object[] objArr = this.g0;
            if (objArr[i2] == i0) {
                this.f0[i2] = i;
                objArr[i2] = e;
                return;
            }
        }
        if (this.a && i3 >= this.f0.length) {
            e();
            i2 = ~c70.a(this.f0, this.h0, i);
        }
        int i4 = this.h0;
        if (i4 >= this.f0.length) {
            int e2 = c70.e(i4 + 1);
            int[] iArr = new int[e2];
            Object[] objArr2 = new Object[e2];
            int[] iArr2 = this.f0;
            System.arraycopy(iArr2, 0, iArr, 0, iArr2.length);
            Object[] objArr3 = this.g0;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f0 = iArr;
            this.g0 = objArr2;
        }
        int i5 = this.h0;
        if (i5 - i2 != 0) {
            int[] iArr3 = this.f0;
            int i6 = i2 + 1;
            System.arraycopy(iArr3, i2, iArr3, i6, i5 - i2);
            Object[] objArr4 = this.g0;
            System.arraycopy(objArr4, i2, objArr4, i6, this.h0 - i2);
        }
        this.f0[i2] = i;
        this.g0[i2] = e;
        this.h0++;
    }

    public void l(int i) {
        Object[] objArr = this.g0;
        Object obj = objArr[i];
        Object obj2 = i0;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.a = true;
        }
    }

    public int o() {
        if (this.a) {
            e();
        }
        return this.h0;
    }

    public E p(int i) {
        if (this.a) {
            e();
        }
        return (E) this.g0[i];
    }

    public String toString() {
        if (o() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.h0 * 28);
        sb.append('{');
        for (int i = 0; i < this.h0; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(j(i));
            sb.append('=');
            E p = p(i);
            if (p != this) {
                sb.append(p);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public lr3(int i) {
        this.a = false;
        if (i == 0) {
            this.f0 = c70.a;
            this.g0 = c70.c;
            return;
        }
        int e = c70.e(i);
        this.f0 = new int[e];
        this.g0 = new Object[e];
    }
}
