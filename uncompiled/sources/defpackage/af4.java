package defpackage;

import java.util.Iterator;

/* compiled from: UnmodifiableIterator.java */
/* renamed from: af4  reason: default package */
/* loaded from: classes2.dex */
public abstract class af4<E> implements Iterator<E> {
    @Override // java.util.Iterator
    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
