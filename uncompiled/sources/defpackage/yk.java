package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_BinaryImage.java */
/* renamed from: yk  reason: default package */
/* loaded from: classes2.dex */
public final class yk extends r90.e.d.a.b.AbstractC0266a {
    public final long a;
    public final long b;
    public final String c;
    public final String d;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_BinaryImage.java */
    /* renamed from: yk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.b.AbstractC0266a.AbstractC0267a {
        public Long a;
        public Long b;
        public String c;
        public String d;

        @Override // defpackage.r90.e.d.a.b.AbstractC0266a.AbstractC0267a
        public r90.e.d.a.b.AbstractC0266a a() {
            String str = "";
            if (this.a == null) {
                str = " baseAddress";
            }
            if (this.b == null) {
                str = str + " size";
            }
            if (this.c == null) {
                str = str + " name";
            }
            if (str.isEmpty()) {
                return new yk(this.a.longValue(), this.b.longValue(), this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0266a.AbstractC0267a
        public r90.e.d.a.b.AbstractC0266a.AbstractC0267a b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0266a.AbstractC0267a
        public r90.e.d.a.b.AbstractC0266a.AbstractC0267a c(String str) {
            Objects.requireNonNull(str, "Null name");
            this.c = str;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0266a.AbstractC0267a
        public r90.e.d.a.b.AbstractC0266a.AbstractC0267a d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0266a.AbstractC0267a
        public r90.e.d.a.b.AbstractC0266a.AbstractC0267a e(String str) {
            this.d = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0266a
    public long b() {
        return this.a;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0266a
    public String c() {
        return this.c;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0266a
    public long d() {
        return this.b;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0266a
    public String e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a.b.AbstractC0266a) {
            r90.e.d.a.b.AbstractC0266a abstractC0266a = (r90.e.d.a.b.AbstractC0266a) obj;
            if (this.a == abstractC0266a.b() && this.b == abstractC0266a.d() && this.c.equals(abstractC0266a.c())) {
                String str = this.d;
                if (str == null) {
                    if (abstractC0266a.e() == null) {
                        return true;
                    }
                } else if (str.equals(abstractC0266a.e())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        int hashCode = (((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003) ^ this.c.hashCode()) * 1000003;
        String str = this.d;
        return hashCode ^ (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return "BinaryImage{baseAddress=" + this.a + ", size=" + this.b + ", name=" + this.c + ", uuid=" + this.d + "}";
    }

    public yk(long j, long j2, String str, String str2) {
        this.a = j;
        this.b = j2;
        this.c = str;
        this.d = str2;
    }
}
