package defpackage;

import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import net.safemoon.androidwallet.R;

/* compiled from: ItemChainNetworkVerticalBinding.java */
/* renamed from: us1  reason: default package */
/* loaded from: classes2.dex */
public final class us1 {
    public final LinearLayoutCompat a;
    public final ImageView b;

    public us1(LinearLayoutCompat linearLayoutCompat, ImageView imageView, LinearLayoutCompat linearLayoutCompat2) {
        this.a = linearLayoutCompat;
        this.b = imageView;
    }

    public static us1 a(View view) {
        ImageView imageView = (ImageView) ai4.a(view, R.id.imgNativeNetwork);
        if (imageView != null) {
            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) view;
            return new us1(linearLayoutCompat, imageView, linearLayoutCompat);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.imgNativeNetwork)));
    }

    public LinearLayoutCompat b() {
        return this.a;
    }
}
