package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: LayoutPasswordRequirementsBinding.java */
/* renamed from: qy1  reason: default package */
/* loaded from: classes2.dex */
public final class qy1 {
    public final ImageView a;
    public final ImageView b;
    public final ImageView c;
    public final TextView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;
    public final TextView h;

    public qy1(ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, ImageView imageView3, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5) {
        this.a = imageView;
        this.b = imageView2;
        this.c = imageView3;
        this.d = textView;
        this.e = textView2;
        this.f = textView3;
        this.g = textView4;
        this.h = textView5;
    }

    public static qy1 a(View view) {
        int i = R.id.iv_1;
        ImageView imageView = (ImageView) ai4.a(view, R.id.iv_1);
        if (imageView != null) {
            i = R.id.iv_2;
            ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv_2);
            if (imageView2 != null) {
                i = R.id.iv_4;
                ImageView imageView3 = (ImageView) ai4.a(view, R.id.iv_4);
                if (imageView3 != null) {
                    i = R.id.min_char;
                    TextView textView = (TextView) ai4.a(view, R.id.min_char);
                    if (textView != null) {
                        i = R.id.tvMinPassReq;
                        TextView textView2 = (TextView) ai4.a(view, R.id.tvMinPassReq);
                        if (textView2 != null) {
                            i = R.id.tv_number;
                            TextView textView3 = (TextView) ai4.a(view, R.id.tv_number);
                            if (textView3 != null) {
                                i = R.id.tvPassNotMatch;
                                TextView textView4 = (TextView) ai4.a(view, R.id.tvPassNotMatch);
                                if (textView4 != null) {
                                    i = R.id.tv_special;
                                    TextView textView5 = (TextView) ai4.a(view, R.id.tv_special);
                                    if (textView5 != null) {
                                        return new qy1((ConstraintLayout) view, imageView, imageView2, imageView3, textView, textView2, textView3, textView4, textView5);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
