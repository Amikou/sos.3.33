package defpackage;

import java.util.Set;

/* compiled from: AbstractComponentContainer.java */
/* renamed from: r4  reason: default package */
/* loaded from: classes2.dex */
public abstract class r4 implements b40 {
    @Override // defpackage.b40
    public <T> T a(Class<T> cls) {
        fw2<T> b = b(cls);
        if (b == null) {
            return null;
        }
        return b.get();
    }

    @Override // defpackage.b40
    public <T> Set<T> d(Class<T> cls) {
        return c(cls).get();
    }
}
