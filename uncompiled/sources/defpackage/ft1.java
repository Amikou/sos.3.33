package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import net.safemoon.androidwallet.R;

/* compiled from: ItemSelectSecurityQuestionBinding.java */
/* renamed from: ft1  reason: default package */
/* loaded from: classes2.dex */
public final class ft1 {
    public final LinearLayoutCompat a;
    public final TextView b;

    public ft1(LinearLayoutCompat linearLayoutCompat, LinearLayoutCompat linearLayoutCompat2, TextView textView) {
        this.a = linearLayoutCompat;
        this.b = textView;
    }

    public static ft1 a(View view) {
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) view;
        TextView textView = (TextView) ai4.a(view, R.id.tvQuestion);
        if (textView != null) {
            return new ft1(linearLayoutCompat, linearLayoutCompat, textView);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.tvQuestion)));
    }

    public static ft1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_select_security_question, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public LinearLayoutCompat b() {
        return this.a;
    }
}
