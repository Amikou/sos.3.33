package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.ConfirmPassphraseActivity;

/* renamed from: f50  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class f50 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ f50 a = new f50();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        ConfirmPassphraseActivity.F(dialogInterface);
    }
}
