package defpackage;

import androidx.media3.common.g;
import java.io.IOException;

/* compiled from: ExtractorInput.java */
/* renamed from: q11  reason: default package */
/* loaded from: classes.dex */
public interface q11 extends g {
    boolean a(byte[] bArr, int i, int i2, boolean z) throws IOException;

    boolean d(byte[] bArr, int i, int i2, boolean z) throws IOException;

    long e();

    void f(int i) throws IOException;

    int g(int i) throws IOException;

    long getLength();

    long getPosition();

    int h(byte[] bArr, int i, int i2) throws IOException;

    void j();

    void k(int i) throws IOException;

    boolean l(int i, boolean z) throws IOException;

    void n(byte[] bArr, int i, int i2) throws IOException;

    @Override // androidx.media3.common.g
    int read(byte[] bArr, int i, int i2) throws IOException;

    void readFully(byte[] bArr, int i, int i2) throws IOException;
}
