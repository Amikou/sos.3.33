package defpackage;

import com.google.protobuf.s;
import com.google.protobuf.t;
import com.google.protobuf.u;

/* compiled from: ExtensionSchemas.java */
/* renamed from: k11  reason: default package */
/* loaded from: classes2.dex */
public final class k11 {
    public static final s<?> a = new u();
    public static final s<?> b = c();

    public static s<?> a() {
        s<?> sVar = b;
        if (sVar != null) {
            return sVar;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    public static s<?> b() {
        return a;
    }

    public static s<?> c() {
        try {
            int i = t.b;
            return (s) t.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
