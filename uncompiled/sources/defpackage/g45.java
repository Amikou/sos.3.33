package defpackage;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: g45  reason: default package */
/* loaded from: classes.dex */
public final class g45 {
    public static final ConcurrentHashMap<Uri, g45> h = new ConcurrentHashMap<>();
    public static final String[] i = {"key", "value"};
    public final ContentResolver a;
    public final Uri b;
    public volatile Map<String, String> e;
    public final Object d = new Object();
    public final Object f = new Object();
    public final List<o45> g = new ArrayList();
    public final ContentObserver c = new k45(this, null);

    public g45(ContentResolver contentResolver, Uri uri) {
        this.a = contentResolver;
        this.b = uri;
    }

    public static g45 a(ContentResolver contentResolver, Uri uri) {
        ConcurrentHashMap<Uri, g45> concurrentHashMap = h;
        g45 g45Var = concurrentHashMap.get(uri);
        if (g45Var == null) {
            g45 g45Var2 = new g45(contentResolver, uri);
            g45 putIfAbsent = concurrentHashMap.putIfAbsent(uri, g45Var2);
            if (putIfAbsent == null) {
                g45Var2.a.registerContentObserver(g45Var2.b, false, g45Var2.c);
                return g45Var2;
            }
            return putIfAbsent;
        }
        return g45Var;
    }

    public final Map<String, String> c() {
        Map<String, String> e = r45.h("gms:phenotype:phenotype_flag:debug_disable_caching", false) ? e() : this.e;
        if (e == null) {
            synchronized (this.d) {
                e = this.e;
                if (e == null) {
                    e = e();
                    this.e = e;
                }
            }
        }
        return e != null ? e : Collections.emptyMap();
    }

    public final void d() {
        synchronized (this.d) {
            this.e = null;
        }
    }

    public final Map<String, String> e() {
        try {
            HashMap hashMap = new HashMap();
            Cursor query = this.a.query(this.b, i, null, null, null);
            if (query != null) {
                while (query.moveToNext()) {
                    hashMap.put(query.getString(0), query.getString(1));
                }
                query.close();
            }
            return hashMap;
        } catch (SQLiteException | SecurityException unused) {
            return null;
        }
    }

    public final void f() {
        synchronized (this.f) {
            for (o45 o45Var : this.g) {
                o45Var.a();
            }
        }
    }
}
