package defpackage;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.carousel.ContactCarouselView;

/* compiled from: FragmentSendtoBinding.java */
/* renamed from: bb1  reason: default package */
/* loaded from: classes2.dex */
public final class bb1 {
    public final ScrollView a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final ImageView e;
    public final MaterialCheckBox f;
    public final ConstraintLayout g;
    public final ContactCarouselView h;
    public final EditText i;
    public final TextInputEditText j;
    public final AppCompatEditText k;
    public final AppCompatImageView l;
    public final AppCompatImageView m;
    public final AppCompatImageView n;
    public final ImageView o;
    public final ScrollView p;
    public final TextView q;
    public final TextView r;
    public final TextView s;
    public final TextView t;
    public final TextView u;
    public final TextView v;
    public final TextView w;
    public final TextView x;

    public bb1(ScrollView scrollView, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, AppCompatImageView appCompatImageView, ImageView imageView, MaterialCheckBox materialCheckBox, ConstraintLayout constraintLayout, ContactCarouselView contactCarouselView, EditText editText, TextInputEditText textInputEditText, AppCompatEditText appCompatEditText, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, AppCompatImageView appCompatImageView4, ImageView imageView2, LinearLayout linearLayout, View view, LinearLayoutCompat linearLayoutCompat, ScrollView scrollView2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, TextView textView9) {
        this.a = scrollView;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = imageView;
        this.f = materialCheckBox;
        this.g = constraintLayout;
        this.h = contactCarouselView;
        this.i = editText;
        this.j = textInputEditText;
        this.k = appCompatEditText;
        this.l = appCompatImageView2;
        this.m = appCompatImageView3;
        this.n = appCompatImageView4;
        this.o = imageView2;
        this.p = scrollView2;
        this.q = textView;
        this.r = textView2;
        this.s = textView3;
        this.t = textView4;
        this.u = textView5;
        this.v = textView6;
        this.w = textView7;
        this.x = textView9;
    }

    public static bb1 a(View view) {
        int i = R.id.btnAddContact;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAddContact);
        if (materialButton != null) {
            i = R.id.btnClearText;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnClearText);
            if (materialButton2 != null) {
                i = R.id.btnSend;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnSend);
                if (materialButton3 != null) {
                    i = R.id.bubble;
                    AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.bubble);
                    if (appCompatImageView != null) {
                        i = R.id.camera;
                        ImageView imageView = (ImageView) ai4.a(view, R.id.camera);
                        if (imageView != null) {
                            i = R.id.chkFullAmountRecipient;
                            MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chkFullAmountRecipient);
                            if (materialCheckBox != null) {
                                i = R.id.container;
                                ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.container);
                                if (constraintLayout != null) {
                                    i = R.id.cvRecentContact;
                                    ContactCarouselView contactCarouselView = (ContactCarouselView) ai4.a(view, R.id.cvRecentContact);
                                    if (contactCarouselView != null) {
                                        i = R.id.etAddress;
                                        EditText editText = (EditText) ai4.a(view, R.id.etAddress);
                                        if (editText != null) {
                                            i = R.id.etAmount;
                                            TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.etAmount);
                                            if (textInputEditText != null) {
                                                i = R.id.etContactSearch;
                                                AppCompatEditText appCompatEditText = (AppCompatEditText) ai4.a(view, R.id.etContactSearch);
                                                if (appCompatEditText != null) {
                                                    i = R.id.imgContacts;
                                                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgContacts);
                                                    if (appCompatImageView2 != null) {
                                                        i = R.id.imgSymbol;
                                                        AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.imgSymbol);
                                                        if (appCompatImageView3 != null) {
                                                            i = R.id.imgWallets;
                                                            AppCompatImageView appCompatImageView4 = (AppCompatImageView) ai4.a(view, R.id.imgWallets);
                                                            if (appCompatImageView4 != null) {
                                                                i = R.id.iv_back;
                                                                ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv_back);
                                                                if (imageView2 != null) {
                                                                    i = R.id.lAddressContainer;
                                                                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.lAddressContainer);
                                                                    if (linearLayout != null) {
                                                                        i = R.id.line;
                                                                        View a = ai4.a(view, R.id.line);
                                                                        if (a != null) {
                                                                            i = R.id.lnDetails;
                                                                            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.lnDetails);
                                                                            if (linearLayoutCompat != null) {
                                                                                ScrollView scrollView = (ScrollView) view;
                                                                                i = R.id.tvBalance;
                                                                                TextView textView = (TextView) ai4.a(view, R.id.tvBalance);
                                                                                if (textView != null) {
                                                                                    i = R.id.tvFiat;
                                                                                    TextView textView2 = (TextView) ai4.a(view, R.id.tvFiat);
                                                                                    if (textView2 != null) {
                                                                                        i = R.id.tvHalf;
                                                                                        TextView textView3 = (TextView) ai4.a(view, R.id.tvHalf);
                                                                                        if (textView3 != null) {
                                                                                            i = R.id.tvMax;
                                                                                            TextView textView4 = (TextView) ai4.a(view, R.id.tvMax);
                                                                                            if (textView4 != null) {
                                                                                                i = R.id.tv_paste;
                                                                                                TextView textView5 = (TextView) ai4.a(view, R.id.tv_paste);
                                                                                                if (textView5 != null) {
                                                                                                    i = R.id.tvPreviewToken;
                                                                                                    TextView textView6 = (TextView) ai4.a(view, R.id.tvPreviewToken);
                                                                                                    if (textView6 != null) {
                                                                                                        i = R.id.tvQuarter;
                                                                                                        TextView textView7 = (TextView) ai4.a(view, R.id.tvQuarter);
                                                                                                        if (textView7 != null) {
                                                                                                            i = R.id.tvSend2;
                                                                                                            TextView textView8 = (TextView) ai4.a(view, R.id.tvSend2);
                                                                                                            if (textView8 != null) {
                                                                                                                i = R.id.tvTokenName;
                                                                                                                TextView textView9 = (TextView) ai4.a(view, R.id.tvTokenName);
                                                                                                                if (textView9 != null) {
                                                                                                                    return new bb1(scrollView, materialButton, materialButton2, materialButton3, appCompatImageView, imageView, materialCheckBox, constraintLayout, contactCarouselView, editText, textInputEditText, appCompatEditText, appCompatImageView2, appCompatImageView3, appCompatImageView4, imageView2, linearLayout, a, linearLayoutCompat, scrollView, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ScrollView b() {
        return this.a;
    }
}
