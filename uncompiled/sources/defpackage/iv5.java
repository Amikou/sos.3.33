package defpackage;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: iv5  reason: default package */
/* loaded from: classes.dex */
public final class iv5 implements aw5 {
    public aw5[] a;

    public iv5(aw5... aw5VarArr) {
        this.a = aw5VarArr;
    }

    @Override // defpackage.aw5
    public final boolean a(Class<?> cls) {
        for (aw5 aw5Var : this.a) {
            if (aw5Var.a(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.aw5
    public final tv5 b(Class<?> cls) {
        aw5[] aw5VarArr;
        for (aw5 aw5Var : this.a) {
            if (aw5Var.a(cls)) {
                return aw5Var.b(cls);
            }
        }
        String name = cls.getName();
        throw new UnsupportedOperationException(name.length() != 0 ? "No factory is available for message type: ".concat(name) : new String("No factory is available for message type: "));
    }
}
