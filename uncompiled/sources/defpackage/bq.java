package defpackage;

import android.graphics.Bitmap;
import android.widget.ImageView;

/* compiled from: BitmapImageViewTarget.java */
/* renamed from: bq  reason: default package */
/* loaded from: classes.dex */
public class bq extends ep1<Bitmap> {
    public bq(ImageView imageView) {
        super(imageView);
    }

    @Override // defpackage.ep1
    /* renamed from: t */
    public void r(Bitmap bitmap) {
        ((ImageView) this.a).setImageBitmap(bitmap);
    }
}
