package defpackage;

/* compiled from: StateFlow.kt */
/* renamed from: jb2  reason: default package */
/* loaded from: classes2.dex */
public interface jb2<T> extends ws3<T>, ib2<T> {
    boolean b(T t, T t2);

    @Override // defpackage.ws3
    T getValue();

    void setValue(T t);
}
