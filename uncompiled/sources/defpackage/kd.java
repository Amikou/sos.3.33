package defpackage;

import android.graphics.Canvas;
import android.graphics.Rect;
import com.facebook.imagepipeline.animated.base.AnimatedDrawableFrameInfo;

/* compiled from: AnimatedDrawableBackend.java */
/* renamed from: kd  reason: default package */
/* loaded from: classes.dex */
public interface kd {
    int a();

    int b();

    AnimatedDrawableFrameInfo c(int i);

    void d(int i, Canvas canvas);

    int e(int i);

    kd f(Rect rect);

    int g();

    int getHeight();

    int getWidth();

    int h();

    yd i();
}
