package defpackage;

import android.os.Build;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: jr2  reason: default package */
/* loaded from: classes.dex */
public final class jr2 {
    public static Boolean a;

    @RecentlyNonNull
    public static boolean a() {
        return true;
    }

    @RecentlyNonNull
    public static boolean b() {
        return Build.VERSION.SDK_INT >= 16;
    }

    @RecentlyNonNull
    public static boolean c() {
        return Build.VERSION.SDK_INT >= 18;
    }

    @RecentlyNonNull
    public static boolean d() {
        return Build.VERSION.SDK_INT >= 19;
    }

    @RecentlyNonNull
    public static boolean e() {
        return Build.VERSION.SDK_INT >= 20;
    }

    @RecentlyNonNull
    public static boolean f() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @RecentlyNonNull
    public static boolean g() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @RecentlyNonNull
    public static boolean h() {
        return Build.VERSION.SDK_INT >= 26;
    }

    @RecentlyNonNull
    public static boolean i() {
        return Build.VERSION.SDK_INT >= 28;
    }

    @RecentlyNonNull
    public static boolean j() {
        return Build.VERSION.SDK_INT >= 29;
    }

    @RecentlyNonNull
    public static boolean k() {
        boolean z = false;
        if (j()) {
            if (Build.VERSION.SDK_INT < 30 || !Build.VERSION.CODENAME.equals("REL")) {
                String str = Build.VERSION.CODENAME;
                if (str.length() == 1 && str.charAt(0) >= 'R' && str.charAt(0) <= 'Z') {
                    Boolean bool = a;
                    if (bool != null) {
                        return bool.booleanValue();
                    }
                    try {
                        if ("google".equals(Build.BRAND) && !Build.ID.startsWith("RPP1") && !Build.ID.startsWith("RPP2") && Integer.parseInt(Build.VERSION.INCREMENTAL) >= 6301457) {
                            z = true;
                        }
                        a = Boolean.valueOf(z);
                    } catch (NumberFormatException unused) {
                        a = Boolean.TRUE;
                    }
                    a.booleanValue();
                    return a.booleanValue();
                }
                return false;
            }
            return true;
        }
        return false;
    }
}
