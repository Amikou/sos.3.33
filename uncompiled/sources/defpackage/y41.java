package defpackage;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* compiled from: FingerprintManagerCompat.java */
@Deprecated
/* renamed from: y41  reason: default package */
/* loaded from: classes.dex */
public class y41 {
    public final Context a;

    /* compiled from: FingerprintManagerCompat.java */
    /* renamed from: y41$a */
    /* loaded from: classes.dex */
    public class a extends FingerprintManager.AuthenticationCallback {
        public final /* synthetic */ b a;

        public a(b bVar) {
            this.a = bVar;
        }

        @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
        public void onAuthenticationError(int i, CharSequence charSequence) {
            this.a.a(i, charSequence);
        }

        @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
        public void onAuthenticationFailed() {
            this.a.b();
        }

        @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
        public void onAuthenticationHelp(int i, CharSequence charSequence) {
            this.a.c(i, charSequence);
        }

        @Override // android.hardware.fingerprint.FingerprintManager.AuthenticationCallback
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult authenticationResult) {
            this.a.d(new c(y41.f(authenticationResult.getCryptoObject())));
        }
    }

    /* compiled from: FingerprintManagerCompat.java */
    /* renamed from: y41$b */
    /* loaded from: classes.dex */
    public static abstract class b {
        public abstract void a(int i, CharSequence charSequence);

        public abstract void b();

        public abstract void c(int i, CharSequence charSequence);

        public abstract void d(c cVar);
    }

    /* compiled from: FingerprintManagerCompat.java */
    /* renamed from: y41$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final d a;

        public c(d dVar) {
            this.a = dVar;
        }

        public d a() {
            return this.a;
        }
    }

    public y41(Context context) {
        this.a = context;
    }

    public static y41 b(Context context) {
        return new y41(context);
    }

    public static FingerprintManager c(Context context) {
        int i = Build.VERSION.SDK_INT;
        if (i == 23) {
            return (FingerprintManager) context.getSystemService(FingerprintManager.class);
        }
        if (i <= 23 || !context.getPackageManager().hasSystemFeature("android.hardware.fingerprint")) {
            return null;
        }
        return (FingerprintManager) context.getSystemService(FingerprintManager.class);
    }

    public static d f(FingerprintManager.CryptoObject cryptoObject) {
        if (cryptoObject == null) {
            return null;
        }
        if (cryptoObject.getCipher() != null) {
            return new d(cryptoObject.getCipher());
        }
        if (cryptoObject.getSignature() != null) {
            return new d(cryptoObject.getSignature());
        }
        if (cryptoObject.getMac() != null) {
            return new d(cryptoObject.getMac());
        }
        return null;
    }

    public static FingerprintManager.AuthenticationCallback g(b bVar) {
        return new a(bVar);
    }

    public static FingerprintManager.CryptoObject h(d dVar) {
        if (dVar == null) {
            return null;
        }
        if (dVar.a() != null) {
            return new FingerprintManager.CryptoObject(dVar.a());
        }
        if (dVar.c() != null) {
            return new FingerprintManager.CryptoObject(dVar.c());
        }
        if (dVar.b() != null) {
            return new FingerprintManager.CryptoObject(dVar.b());
        }
        return null;
    }

    public void a(d dVar, int i, tv tvVar, b bVar, Handler handler) {
        FingerprintManager c2;
        if (Build.VERSION.SDK_INT < 23 || (c2 = c(this.a)) == null) {
            return;
        }
        c2.authenticate(h(dVar), tvVar != null ? (CancellationSignal) tvVar.b() : null, i, g(bVar), handler);
    }

    public boolean d() {
        FingerprintManager c2;
        return Build.VERSION.SDK_INT >= 23 && (c2 = c(this.a)) != null && c2.hasEnrolledFingerprints();
    }

    public boolean e() {
        FingerprintManager c2;
        return Build.VERSION.SDK_INT >= 23 && (c2 = c(this.a)) != null && c2.isHardwareDetected();
    }

    /* compiled from: FingerprintManagerCompat.java */
    /* renamed from: y41$d */
    /* loaded from: classes.dex */
    public static class d {
        public final Signature a;
        public final Cipher b;
        public final Mac c;

        public d(Signature signature) {
            this.a = signature;
            this.b = null;
            this.c = null;
        }

        public Cipher a() {
            return this.b;
        }

        public Mac b() {
            return this.c;
        }

        public Signature c() {
            return this.a;
        }

        public d(Cipher cipher) {
            this.b = cipher;
            this.a = null;
            this.c = null;
        }

        public d(Mac mac) {
            this.c = mac;
            this.b = null;
            this.a = null;
        }
    }
}
