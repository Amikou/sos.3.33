package defpackage;

import com.google.zxing.qrcode.encoder.Encoder;
import java.nio.charset.Charset;

/* compiled from: Charsets.java */
/* renamed from: cy  reason: default package */
/* loaded from: classes2.dex */
public final class cy {
    public static final Charset a = Charset.forName("US-ASCII");
    public static final Charset b = Charset.forName(Encoder.DEFAULT_BYTE_MODE_ENCODING);
    public static final Charset c = Charset.forName("UTF-8");
    public static final Charset d;
    public static final Charset e;

    static {
        Charset.forName("UTF-16BE");
        d = Charset.forName("UTF-16LE");
        e = Charset.forName("UTF-16");
    }
}
