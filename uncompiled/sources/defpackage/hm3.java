package defpackage;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import retrofit2.g;
import retrofit2.m;
import retrofit2.o;
import retrofit2.p;

/* compiled from: ServiceMethod.java */
/* renamed from: hm3  reason: default package */
/* loaded from: classes3.dex */
public abstract class hm3<T> {
    public static <T> hm3<T> b(o oVar, Method method) {
        m b = m.b(oVar, method);
        Type genericReturnType = method.getGenericReturnType();
        if (!p.j(genericReturnType)) {
            if (genericReturnType != Void.TYPE) {
                return g.f(oVar, method, b);
            }
            throw p.m(method, "Service methods cannot return void.", new Object[0]);
        }
        throw p.m(method, "Method return type must not include a type variable or wildcard: %s", genericReturnType);
    }

    public abstract T a(Object[] objArr);
}
