package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.math.BigInteger;
import java.sql.Timestamp;

/* compiled from: AKTSecpUtils.java */
/* renamed from: i2  reason: default package */
/* loaded from: classes.dex */
public class i2 {
    public static qy4 a = new qy4();
    public static BigInteger b;

    public static long A() {
        return (new Timestamp(System.currentTimeMillis()).getTime() / 1000) / 60;
    }

    public static String a(qy4 qy4Var, String str) {
        BigInteger bigInteger = qy4Var.a()[0];
        b = qy4Var.b();
        String k = qy4Var.k();
        ay1.g(qy4Var.h());
        return t(new String[]{"AKTRequestID", "PING", "AKTAPIID"}, new String[]{"GR8PPP01", k, str}, qy4Var.g("SERVER"), qy4Var.g("SOCKET"));
    }

    public static String b(qy4 qy4Var, String str) {
        String g = qy4Var.g("PONG");
        lm.a(g);
        qy4Var.f(b);
        ay1.g(qy4Var.h());
        String j = qy4Var.j(g);
        String str2 = String.valueOf(lm.d(ay1.f(la3.a(ay1.a(qy4Var.g("EMAIL").toLowerCase()))))) + "@AKT.com";
        qy4Var.c(str2, "EMAILB64");
        String l = qy4Var.l(str2);
        qy4Var.l("2505551212");
        return t(new String[]{"AKTRequestID", "PING", "EMAIL", "TODECRYPT", "AKTAPIID"}, new String[]{"GR8PPP03", j, l, qy4Var.l("pppDecrypt"), str}, qy4Var.g("SERVER"), qy4Var.g("SOCKET"));
    }

    public static byte[] c(String str, String str2, String str3) {
        int i;
        bz4 bz4Var = new bz4();
        try {
            dz4 dz4Var = new dz4();
            dz4Var.p(str3);
            dz4Var.q(str2);
            dz4Var.o();
            String a2 = lm.a(dz4Var.e());
            dz4Var.g();
            byte[] a3 = ay1.a(a2);
            bz4Var.i(true);
            if (!bz4Var.h(a3, a3.length)) {
                System.out.println("AKTSU 937 Failed to initialize the encryption class.");
                System.out.println("AKTSU 938 Last error: " + bz4Var.f() + ".");
                return new byte[]{1};
            }
            int indexOf = str.indexOf("=");
            String str4 = null;
            if (indexOf >= 0) {
                int i2 = indexOf + 1;
                str.substring(0, i2);
                str = str.substring(i2);
            }
            try {
                str4 = bz4Var.b(str);
                i = str4.length();
            } catch (Exception unused) {
                i = 0;
            }
            if (i <= 0) {
                byte[] h = ay1.h(ay1.a(q(str2)), q(str3));
                ay1.d(h);
                lm.e(h);
                bz4Var.i(true);
                if (!bz4Var.h(h, h.length)) {
                    System.out.println("AKTSU 988 Failed to initialize the encryption class.");
                    System.out.println("AKTSU 989 Last error: " + bz4Var.f() + ".");
                    return new byte[]{5};
                }
                try {
                    str4 = bz4Var.b(str);
                    str4.length();
                } catch (Exception e) {
                    System.out.println("AKTSU 1001 Error: " + e.toString());
                }
                if (str4.length() <= 0) {
                    System.out.println("AKTSU 1006 non recovery error");
                    return new byte[]{9};
                }
            }
            lm.d(str4);
            byte[] a4 = ay1.a(str4);
            ay1.d(a4);
            return a4;
        } catch (Exception e2) {
            System.out.println("AKTSU 1032: " + e2.toString());
            return new byte[]{2};
        }
    }

    public static void d(qy4 qy4Var, String str, String str2) {
        qy4Var.c(str, str2);
    }

    public static String e(qy4 qy4Var, String str, String str2) {
        dz4 c = q.c(str, str2);
        String e = c.e();
        String g = c.g();
        qy4Var.c("U5", g);
        qy4Var.c("K5", e);
        String str3 = "AKTRequestID=AKTSFMUSER|U5=" + g + "|AKTAPIID=login02|";
        String g2 = qy4Var.g("SERVER");
        return "port=" + qy4Var.g("SOCKET") + ",server=" + g2 + ",00#" + str3.length() + "#|" + str3;
    }

    public static int f(String str) {
        int length = fz4.a.length - 1;
        int i = 0;
        while (true) {
            int i2 = (length + i) / 2;
            int i3 = i2 - 1;
            int compareTo = fz4.a[i3].compareTo(str);
            if (compareTo == 0) {
                return i3;
            }
            if (compareTo > 0) {
                if (length - i < 2) {
                    return i3;
                }
                length = i2;
            } else if (length - i < 2) {
                return i2 + 1;
            } else {
                i = i2;
            }
        }
    }

    public static int g(String str, int i, String[] strArr, int i2) {
        int length = (64 - str.length()) / 2;
        int length2 = (str.length() * 8) / 22;
        if (length > 0) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i3 = 0; i3 < length; i3++) {
                stringBuffer.append("00");
            }
            str = String.valueOf(stringBuffer.toString()) + str;
        }
        zl h = zl.h(r.b(str));
        h.D(8 - i2);
        for (int i4 = 0; i4 < length2; i4++) {
            i--;
            strArr[i] = fz4.a(h.o(11));
            h.D(11);
        }
        return i;
    }

    public static String[] h(String str) {
        int length = (str.length() - 2) / 8;
        if (((str.length() - 2) / 8) * 8 != str.length() - 2) {
            return null;
        }
        int length2 = (((str.length() - 2) * 4) + length) / 11;
        String[] strArr = new String[length2];
        if (length2 > 21) {
            g(str.substring(0, 44), g(str.substring(44), length2, strArr, length), strArr, length);
        } else {
            g(str, length2, strArr, length);
        }
        return strArr;
    }

    public static String i(String str, String str2, String str3) {
        dz4 dz4Var = new dz4();
        dz4Var.t(lm.a(str));
        dz4Var.m(false);
        String b2 = dz4Var.b(str2);
        if (b2.length() != 32) {
            return "error";
        }
        dz4Var.t(b2);
        return ay1.g(dz4Var.b(str3));
    }

    public static String j(String str, String str2, String str3) {
        dz4 dz4Var = new dz4();
        dz4Var.t(lm.a(str));
        dz4Var.m(false);
        String b2 = dz4Var.b(str2);
        if (b2.length() != 32) {
            return "error";
        }
        dz4Var.t(b2);
        return dz4Var.c(ay1.f(r.b(str3)));
    }

    public static zl k(String str) {
        byte[] b2 = r.b(str);
        int length = str.length() / 2;
        byte[] bArr = new byte[32];
        for (int i = 0; i < 32; i++) {
            if (i < length) {
                bArr[31 - i] = b2[(length - i) - 1];
            } else {
                bArr[31 - i] = 0;
            }
        }
        new zl(0);
        zl h = zl.h(bArr);
        byte[] bArr2 = new byte[32];
        h.H(bArr2);
        r.a(bArr2);
        ay1.a(h.toString());
        r.a(la3.a(bArr2));
        return h;
    }

    public static String l(String str) {
        String[] h;
        int length = str.length();
        if (length == 64 || length == 66) {
            String str2 = str;
            int i = 0;
            while (true) {
                if (i >= 5) {
                    break;
                }
                int i2 = 24 - ((4 - i) * 3);
                if (i2 < 24) {
                    int i3 = ((i2 * 8) / 3) + 2;
                    String substring = str.substring(0, i3);
                    str.substring(i3);
                    new zl(0);
                    r.b(substring);
                    zl k = k(str.substring(0, i3 - 2));
                    k.C(256 - ((i2 * 32) / 3));
                    byte[] bArr = new byte[32];
                    k.H(bArr);
                    ay1.g(ay1.f(bArr));
                    byte[] a2 = la3.a(bArr);
                    String g = ay1.g(ay1.f(a2));
                    ay1.f(a2);
                    String substring2 = str.substring(i3);
                    String substring3 = g.substring(0, 64 - i3);
                    boolean z = substring2.compareTo(substring3) == 0;
                    ay1.g(substring2);
                    ay1.g(substring3);
                    if (z) {
                        str2 = str.substring(0, i3);
                        break;
                    }
                } else if (str.length() < 66) {
                    str2 = str + ay1.g(ay1.f(la3.a(r.b(str)))).substring(0, 2);
                }
                i++;
            }
            String str3 = "";
            for (int i4 = 0; i4 < h(str2).length; i4++) {
                str3 = String.valueOf(str3) + h[i4] + "|";
            }
            return str3;
        }
        return null;
    }

    public static String[] m() {
        String g = ay1.g(ay1.b(32));
        return new String[]{g, q.b(q.m(g))};
    }

    public static String n(String str) {
        String[] v = v(str, "|");
        int length = v.length;
        zl zlVar = new zl(0);
        zl zlVar2 = new zl(0);
        byte[] bArr = new byte[32];
        for (int i = 0; i < length; i++) {
            int f = f(v[i]);
            Integer.toHexString(f);
            zl zlVar3 = new zl(f);
            if (i < 23) {
                zlVar = zlVar.z(new zl(zlVar3.o(11)));
            } else {
                zlVar2 = zlVar2.z(new zl(zlVar3.o(11)));
            }
            zlVar.H(bArr);
            if (i < length - 1 && i != 22) {
                zlVar.C(11);
            }
        }
        if (length > 21) {
            byte[] bArr2 = new byte[32];
            byte[] bArr3 = new byte[32];
            zlVar.C(3);
            zlVar.H(bArr2);
            byte[] bArr4 = new byte[32];
            zl zlVar4 = new zl(0);
            zl zlVar5 = new zl(0);
            zlVar5.a(zlVar2);
            zlVar5.D(8);
            zlVar.a(zlVar5);
            zl z = zlVar4.z(new zl(zlVar2.o(8)));
            z.C(248);
            z.H(bArr3);
            String substring = ay1.g(ay1.f(bArr3)).substring(0, 2);
            zlVar.H(bArr2);
            for (int i2 = 0; i2 < 32; i2++) {
                bArr4[i2] = bArr2[i2];
            }
            if ((substring.compareTo(ay1.g(ay1.f(la3.a(bArr4))).substring(0, 2)) == 0 ? 1 : 0) != 0) {
                return r.a(bArr4);
            }
            System.out.println("1312 AKTS invalid mnemonic=" + str);
            return "0000000000000000000000000000000000000000000000000000000000000000";
        }
        int i3 = length / 3;
        zl zlVar6 = new zl(zlVar.o(i3));
        byte[] bArr5 = new byte[32];
        zlVar6.C(248);
        zlVar6.H(bArr5);
        String a2 = r.a(bArr5);
        zl zlVar7 = new zl(0);
        zlVar7.a(zlVar);
        zlVar7.D(i3);
        zlVar7.C(256 - ((length * 32) / 3));
        byte[] bArr6 = new byte[32];
        zlVar7.H(bArr6);
        int i4 = (length * 4) / 3;
        byte[] bArr7 = new byte[i4];
        for (int i5 = 0; i5 < i4; i5++) {
            bArr7[i5] = bArr6[i5];
        }
        r.a(bArr7);
        byte[] a3 = ay1.a(r.a(la3.a(bArr7)));
        byte[] a4 = ay1.a(a2);
        ay1.f(a3);
        ay1.f(a4);
        boolean z2 = length == 12 && a3[0] == a4[1];
        byte[] bArr8 = new byte[32];
        if (z2) {
            zlVar.C(i3);
            zlVar.C(((64 - ((length * 8) / 3)) * 4) - 8);
            byte[] a5 = la3.a(bArr6);
            zlVar.H(bArr6);
            r.a(a5);
            int i6 = (32 - ((8 - i3) * 4)) + 1;
            int i7 = 32 - i6;
            int i8 = 0;
            int i9 = 0;
            while (i8 < i6) {
                bArr8[i9] = bArr6[i8];
                i8++;
                i9++;
            }
            while (r4 < i7) {
                bArr8[i9] = a5[r4];
                r4++;
                i9++;
            }
        } else {
            System.out.println("1362 AKTS invalid mnemonic=" + str);
        }
        return r.a(bArr8);
    }

    public static String o(qy4 qy4Var) {
        return qy4Var.g("questions");
    }

    public static String p(qy4 qy4Var, String str) {
        return qy4Var.g(str);
    }

    public static String q(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            stringBuffer.append(ay1.j(str.charAt(i)));
        }
        int length2 = stringBuffer.toString().length();
        byte[] bArr = new byte[length2];
        for (int i2 = 0; i2 < length2; i2++) {
            bArr[i2] = (byte) stringBuffer.toString().charAt(i2);
        }
        return ay1.f(bArr);
    }

    public static void r(qy4 qy4Var, String str, String str2) {
        a = qy4Var;
        qy4Var.c("SERVER", str);
        a.c("SOCKET", str2);
    }

    public static boolean s(String str, String str2) {
        String a2 = lm.a(str2);
        dz4 dz4Var = new dz4();
        dz4Var.t(a2);
        return dz4Var.b(str).length() == 32;
    }

    public static String t(String[] strArr, String[] strArr2, String str, String str2) {
        String str3 = "";
        for (int i = 0; i < strArr.length; i++) {
            str3 = String.valueOf(str3) + "|" + strArr[i] + "=" + strArr2[i];
        }
        return String.valueOf("port=" + str2 + ",server=" + str) + ",00#" + str3.length() + "#" + str3 + "|";
    }

    public static String u(String str, String str2) {
        dz4 dz4Var = new dz4();
        dz4Var.p(str2);
        dz4Var.q(str);
        dz4Var.o();
        String a2 = lm.a(dz4Var.e());
        byte[] a3 = ay1.a(lm.a(dz4Var.g()));
        byte[] a4 = ay1.a(a2);
        byte[] bArr = new byte[40];
        for (int i = 0; i < 20; i++) {
            bArr[i] = a3[i];
            bArr[i + 20] = a4[i];
        }
        byte[] a5 = la3.a(bArr);
        new zl(0);
        zl h = zl.h(a5);
        byte[] bArr2 = new byte[32];
        h.H(bArr2);
        String str3 = "";
        boolean z = false;
        int i2 = 0;
        while (!z) {
            if (a5[0] == -82) {
                str3 = ay1.g(ay1.f(bArr2));
                z = true;
            } else {
                int i3 = i2 + 1;
                if (i2 > 10000) {
                    z = true;
                } else {
                    h.l(1);
                    h.H(bArr2);
                    a5 = la3.a(bArr2);
                }
                i2 = i3;
            }
        }
        if (i2 >= 10000) {
            return null;
        }
        return str3;
    }

    public static String[] v(String str, String str2) {
        String[] strArr = new String[50];
        int lastIndexOf = str.lastIndexOf(124);
        if (lastIndexOf + 3 > str.length()) {
            str = str.substring(0, lastIndexOf);
        }
        int indexOf = str.indexOf(124);
        if (indexOf < 2) {
            str = str.substring(indexOf + 1);
        }
        int indexOf2 = str.indexOf(124);
        int i = 0;
        do {
            strArr[i] = str.substring(0, indexOf2);
            str = str.substring(indexOf2 + 1);
            i++;
            indexOf2 = str.indexOf(124);
        } while (indexOf2 > 0);
        int i2 = i + 1;
        strArr[i] = str;
        String[] strArr2 = new String[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            strArr2[i3] = strArr[i3];
        }
        return strArr2;
    }

    public static String w(qy4 qy4Var, String[] strArr) {
        for (int i = 1; i < strArr.length; i++) {
            String[] split = strArr[i].split("=");
            String str = split[0];
            switch (str.hashCode()) {
                case 2390:
                    if (str.equals("KA")) {
                        d(qy4Var, "KA", split[1]);
                        continue;
                    }
                    break;
                case 79528:
                    if (str.equals("PSK")) {
                        String str2 = split[1];
                        continue;
                    }
                    break;
                case 84016:
                    if (str.equals("UID")) {
                        String str3 = split[1];
                        continue;
                    }
                    break;
                case 66081660:
                    if (str.equals("EMAIL")) {
                        String p = p(qy4Var, "K5");
                        String p2 = p(qy4Var, "U5");
                        dz4 dz4Var = new dz4();
                        dz4Var.t(lm.a(p));
                        String b2 = dz4Var.b(split[1]);
                        d(qy4Var, "EMAIL", b2);
                        d(qy4Var, "U5", p2);
                        d(qy4Var, "K5", p);
                        d(qy4Var, "TEMPEMAIL", b2);
                        continue;
                    }
                    break;
                case 1543712160:
                    if (str.equals("AKTRequestID")) {
                        continue;
                    }
                    break;
                case 1924154731:
                    if (str.equals("AKTAPIID")) {
                        String str4 = split[1];
                        continue;
                    }
                    break;
            }
            System.out.println("AKTSU 1390 should not be here=" + split[1]);
        }
        return a(qy4Var, "login");
    }

    public static void x(qy4 qy4Var, String[] strArr) {
        for (int i = 1; i < strArr.length; i++) {
            String[] split = strArr[i].split("=");
            String str = split[0];
            int hashCode = str.hashCode();
            if (hashCode == 2461688) {
                if (str.equals("PONG")) {
                    String str2 = split[1];
                }
                System.out.println("AKTSU 1331 should not be here=" + split[1]);
            } else if (hashCode == 1543712160) {
                if (str.equals("AKTRequestID")) {
                }
                System.out.println("AKTSU 1331 should not be here=" + split[1]);
            } else {
                if (hashCode == 1924154731 && str.equals("AKTAPIID")) {
                    String str3 = split[1];
                }
                System.out.println("AKTSU 1331 should not be here=" + split[1]);
            }
        }
    }

    public static String y(qy4 qy4Var, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        qy4 qy4Var2 = qy4Var;
        String str9 = str;
        String str10 = str3;
        dz4 dz4Var = new dz4();
        dz4Var.p(str10);
        dz4Var.q(str2);
        dz4Var.o();
        String e = dz4Var.e();
        String a2 = lm.a(e);
        String g = dz4Var.g();
        String f = ay1.f(r.b(str8));
        dz4Var.t(a2);
        dz4Var.m(true);
        String c = dz4Var.c(f);
        String replace = str5.toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        String replace2 = str7.toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        byte[] bArr = new byte[20];
        int i = 0;
        while (i < 20) {
            bArr[i] = -96;
            i++;
            str10 = str10;
            str9 = str;
            replace = replace;
            qy4Var2 = qy4Var;
        }
        int i2 = 0;
        while (i2 < replace.length()) {
            String str11 = replace;
            bArr[i2] = (byte) str11.charAt(i2);
            i2++;
            str10 = str10;
            str9 = str;
            replace = str11;
            qy4Var2 = qy4Var;
        }
        int i3 = 0;
        while (i3 < replace2.length()) {
            String str12 = replace2;
            bArr[(20 - i3) - 1] = (byte) str12.charAt(i3);
            i3++;
            replace2 = str12;
            str10 = str10;
            str9 = str;
            replace = replace;
            qy4Var2 = qy4Var;
        }
        byte[] a3 = la3.a(bArr);
        dz4 dz4Var2 = new dz4();
        dz4Var2.t(ay1.f(a3));
        dz4Var2.m(true);
        String c2 = dz4Var2.c(f);
        dz4Var2.s();
        dz4Var2.t(f);
        dz4Var2.m(true);
        String c3 = dz4Var2.c(String.valueOf(str2) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + str10);
        String d = lm.d(ay1.f(la3.a(ay1.a(f))));
        byte[] a4 = ay1.a(f);
        int i4 = 32;
        byte[] bArr2 = new byte[32];
        String str13 = replace2;
        int i5 = 0;
        while (i5 < i4) {
            String str14 = c3;
            byte[] bArr3 = bArr2;
            bArr3[31 - i5] = a4[i5];
            i5++;
            replace = replace;
            bArr2 = bArr3;
            i4 = i4;
            qy4Var2 = qy4Var;
            str9 = str;
            c3 = str14;
        }
        String d2 = lm.d(ay1.f(la3.a(bArr2)));
        byte[] a5 = ay1.a(a2);
        ay1.d(a5);
        ay1.g(a2);
        ay1.g(a2);
        r.a(a5);
        qy4Var2.c("U5", g);
        qy4Var2.c("K5", e);
        qy4Var2.c("KA", c);
        qy4Var2.c("PBU5", d);
        qy4Var2.c("PB5K", d2);
        qy4Var2.c("EMAIL", str9);
        long A = A();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(A);
        qy4Var2.c("TIMENOW", stringBuffer.toString());
        String[] strArr = new String[21];
        ay1.g(f);
        StringBuffer stringBuffer2 = new StringBuffer("");
        String[] strArr2 = {"AKTRequestID", "EMAIL", "KA", "SECURITYLEVEL", "KS", "U5", "PBU5", "PB5K", "UPKEY", "UPKEYH", "AKTAPIID", "FN", "QN1", "QN2", "KPPP", "PSK", "K5", "AN1", "AN2", "U", "P"};
        int i6 = 0;
        while (i6 < f.length()) {
            stringBuffer2.append(f.charAt((f.length() - i6) - 1));
            i6++;
            c3 = c3;
            replace = replace;
            strArr2 = strArr2;
            qy4Var2 = qy4Var;
            str9 = str;
        }
        String stringBuffer3 = stringBuffer2.toString();
        String a6 = lm.a(g);
        lm.d(f);
        ay1.g(a6);
        ay1.g(a2);
        ay1.g(stringBuffer3);
        String b2 = ez4.b(f);
        lm.d(b2);
        String f2 = ay1.f(la3.a(ay1.a("" + stringBuffer3)));
        lm.d(f2);
        ay1.g(f2);
        String f3 = ay1.f(la3.a(ay1.a(c3)));
        String b3 = ay1.b(32);
        lm.d(ay1.f(la3.a(ay1.a(str.toLowerCase()))));
        qy4Var2.c("EMAIL", str9);
        String l = qy4Var.l(str);
        qy4Var2.l(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        String l2 = qy4Var2.l(a6);
        String l3 = qy4Var2.l(a2);
        String l4 = qy4Var2.l(b2);
        String l5 = qy4Var2.l(f2);
        String l6 = qy4Var2.l(c3);
        String l7 = qy4Var2.l(f3);
        String l8 = qy4Var2.l(c);
        String l9 = qy4Var2.l(c2);
        String l10 = qy4Var2.l("ZecureIT.uky");
        String l11 = qy4Var2.l(str4);
        String l12 = qy4Var2.l(str6);
        String l13 = qy4Var2.l("1");
        String l14 = qy4Var2.l(b3);
        String l15 = qy4Var2.l(replace);
        String l16 = qy4Var2.l(str13);
        String l17 = qy4Var2.l(str2);
        String l18 = qy4Var2.l(str3);
        String g2 = qy4Var2.g("KPPP");
        strArr[0] = "GR8Reg01";
        strArr[1] = l;
        strArr[2] = l8;
        strArr[3] = l13;
        strArr[4] = l9;
        strArr[5] = l2;
        strArr[6] = l4;
        strArr[7] = l5;
        strArr[8] = l6;
        strArr[9] = l7;
        strArr[10] = "GR8Reg01";
        strArr[11] = l10;
        strArr[12] = l11;
        strArr[13] = l12;
        strArr[14] = g2;
        strArr[15] = l14;
        strArr[16] = l3;
        strArr[17] = l15;
        strArr[18] = l16;
        strArr[19] = l17;
        strArr[20] = l18;
        return t(strArr2, strArr, qy4Var2.g("SERVER"), qy4Var2.g("SOCKET"));
    }

    public static byte[] z(byte[] bArr) {
        byte[] bArr2 = new byte[32];
        for (int i = 0; i < 32; i++) {
            bArr2[31 - i] = bArr[i];
        }
        return bArr2;
    }
}
