package defpackage;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import defpackage.j92;
import java.io.File;
import java.io.InputStream;

/* compiled from: StringLoader.java */
/* renamed from: nu3  reason: default package */
/* loaded from: classes.dex */
public class nu3<Data> implements j92<String, Data> {
    public final j92<Uri, Data> a;

    /* compiled from: StringLoader.java */
    /* renamed from: nu3$a */
    /* loaded from: classes.dex */
    public static final class a implements k92<String, AssetFileDescriptor> {
        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<String, AssetFileDescriptor> c(qa2 qa2Var) {
            return new nu3(qa2Var.d(Uri.class, AssetFileDescriptor.class));
        }
    }

    /* compiled from: StringLoader.java */
    /* renamed from: nu3$b */
    /* loaded from: classes.dex */
    public static class b implements k92<String, ParcelFileDescriptor> {
        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<String, ParcelFileDescriptor> c(qa2 qa2Var) {
            return new nu3(qa2Var.d(Uri.class, ParcelFileDescriptor.class));
        }
    }

    /* compiled from: StringLoader.java */
    /* renamed from: nu3$c */
    /* loaded from: classes.dex */
    public static class c implements k92<String, InputStream> {
        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<String, InputStream> c(qa2 qa2Var) {
            return new nu3(qa2Var.d(Uri.class, InputStream.class));
        }
    }

    public nu3(j92<Uri, Data> j92Var) {
        this.a = j92Var;
    }

    public static Uri e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return f(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? f(str) : parse;
    }

    public static Uri f(String str) {
        return Uri.fromFile(new File(str));
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(String str, int i, int i2, vn2 vn2Var) {
        Uri e = e(str);
        if (e == null || !this.a.a(e)) {
            return null;
        }
        return this.a.b(e, i, i2, vn2Var);
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(String str) {
        return true;
    }
}
