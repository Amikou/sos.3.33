package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application.java */
/* renamed from: wk  reason: default package */
/* loaded from: classes2.dex */
public final class wk extends r90.e.d.a {
    public final r90.e.d.a.b a;
    public final ip1<r90.c> b;
    public final ip1<r90.c> c;
    public final Boolean d;
    public final int e;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application.java */
    /* renamed from: wk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.AbstractC0265a {
        public r90.e.d.a.b a;
        public ip1<r90.c> b;
        public ip1<r90.c> c;
        public Boolean d;
        public Integer e;

        @Override // defpackage.r90.e.d.a.AbstractC0265a
        public r90.e.d.a a() {
            String str = "";
            if (this.a == null) {
                str = " execution";
            }
            if (this.e == null) {
                str = str + " uiOrientation";
            }
            if (str.isEmpty()) {
                return new wk(this.a, this.b, this.c, this.d, this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.AbstractC0265a
        public r90.e.d.a.AbstractC0265a b(Boolean bool) {
            this.d = bool;
            return this;
        }

        @Override // defpackage.r90.e.d.a.AbstractC0265a
        public r90.e.d.a.AbstractC0265a c(ip1<r90.c> ip1Var) {
            this.b = ip1Var;
            return this;
        }

        @Override // defpackage.r90.e.d.a.AbstractC0265a
        public r90.e.d.a.AbstractC0265a d(r90.e.d.a.b bVar) {
            Objects.requireNonNull(bVar, "Null execution");
            this.a = bVar;
            return this;
        }

        @Override // defpackage.r90.e.d.a.AbstractC0265a
        public r90.e.d.a.AbstractC0265a e(ip1<r90.c> ip1Var) {
            this.c = ip1Var;
            return this;
        }

        @Override // defpackage.r90.e.d.a.AbstractC0265a
        public r90.e.d.a.AbstractC0265a f(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        public b() {
        }

        public b(r90.e.d.a aVar) {
            this.a = aVar.d();
            this.b = aVar.c();
            this.c = aVar.e();
            this.d = aVar.b();
            this.e = Integer.valueOf(aVar.f());
        }
    }

    @Override // defpackage.r90.e.d.a
    public Boolean b() {
        return this.d;
    }

    @Override // defpackage.r90.e.d.a
    public ip1<r90.c> c() {
        return this.b;
    }

    @Override // defpackage.r90.e.d.a
    public r90.e.d.a.b d() {
        return this.a;
    }

    @Override // defpackage.r90.e.d.a
    public ip1<r90.c> e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        ip1<r90.c> ip1Var;
        ip1<r90.c> ip1Var2;
        Boolean bool;
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a) {
            r90.e.d.a aVar = (r90.e.d.a) obj;
            return this.a.equals(aVar.d()) && ((ip1Var = this.b) != null ? ip1Var.equals(aVar.c()) : aVar.c() == null) && ((ip1Var2 = this.c) != null ? ip1Var2.equals(aVar.e()) : aVar.e() == null) && ((bool = this.d) != null ? bool.equals(aVar.b()) : aVar.b() == null) && this.e == aVar.f();
        }
        return false;
    }

    @Override // defpackage.r90.e.d.a
    public int f() {
        return this.e;
    }

    @Override // defpackage.r90.e.d.a
    public r90.e.d.a.AbstractC0265a g() {
        return new b(this);
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        ip1<r90.c> ip1Var = this.b;
        int hashCode2 = (hashCode ^ (ip1Var == null ? 0 : ip1Var.hashCode())) * 1000003;
        ip1<r90.c> ip1Var2 = this.c;
        int hashCode3 = (hashCode2 ^ (ip1Var2 == null ? 0 : ip1Var2.hashCode())) * 1000003;
        Boolean bool = this.d;
        return ((hashCode3 ^ (bool != null ? bool.hashCode() : 0)) * 1000003) ^ this.e;
    }

    public String toString() {
        return "Application{execution=" + this.a + ", customAttributes=" + this.b + ", internalKeys=" + this.c + ", background=" + this.d + ", uiOrientation=" + this.e + "}";
    }

    public wk(r90.e.d.a.b bVar, ip1<r90.c> ip1Var, ip1<r90.c> ip1Var2, Boolean bool, int i) {
        this.a = bVar;
        this.b = ip1Var;
        this.c = ip1Var2;
        this.d = bool;
        this.e = i;
    }
}
