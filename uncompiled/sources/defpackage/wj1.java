package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* compiled from: HardwareKeyWatcher.java */
/* renamed from: wj1  reason: default package */
/* loaded from: classes.dex */
public class wj1 {
    public Context a;
    public IntentFilter b;
    public b c;
    public a d;

    /* compiled from: HardwareKeyWatcher.java */
    /* renamed from: wj1$a */
    /* loaded from: classes.dex */
    public class a extends BroadcastReceiver {
        public a() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            String stringExtra;
            String action = intent.getAction();
            if (!"android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(action) || (stringExtra = intent.getStringExtra("reason")) == null) {
                return;
            }
            wj1 wj1Var = wj1.this;
            wj1Var.f("action:" + action + ", reason:" + stringExtra);
            if (wj1.this.c != null) {
                char c = 65535;
                switch (stringExtra.hashCode()) {
                    case 350448461:
                        if (stringExtra.equals("recentapps")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 1092716832:
                        if (stringExtra.equals("homekey")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 2014770135:
                        if (stringExtra.equals("fs_gesture")) {
                            c = 2;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        wj1.this.e("onRecentAppsPressed (recentapps)");
                        wj1.this.c.b();
                        return;
                    case 1:
                        wj1.this.e("onHomePressed (homekey)");
                        wj1.this.c.a();
                        return;
                    case 2:
                        wj1.this.e("onRecentAppsPressed (fs_gesture)");
                        wj1.this.c.b();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* compiled from: HardwareKeyWatcher.java */
    /* renamed from: wj1$b */
    /* loaded from: classes.dex */
    public interface b {
        void a();

        void b();
    }

    public wj1(Context context) {
        this.a = context;
        IntentFilter intentFilter = new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        this.b = intentFilter;
        intentFilter.setPriority(1000);
    }

    public Context d() {
        return this.a;
    }

    public final void e(String str) {
    }

    public final void f(String str) {
    }

    public void g(b bVar) {
        this.c = bVar;
        this.d = new a();
    }

    public void h() {
        if (this.d != null) {
            e("startWatch on " + this.a);
            this.a.registerReceiver(this.d, this.b);
        }
    }

    public void i() {
        if (this.d != null) {
            e("stopWatch on " + this.a);
            this.a.unregisterReceiver(this.d);
        }
    }
}
