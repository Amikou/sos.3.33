package defpackage;

import androidx.media3.exoplayer.mediacodec.MediaCodecUtil;
import androidx.media3.exoplayer.mediacodec.f;
import java.util.List;

/* renamed from: v52  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class v52 implements f {
    public static final /* synthetic */ v52 b = new v52();

    @Override // androidx.media3.exoplayer.mediacodec.f
    public final List a(String str, boolean z, boolean z2) {
        return MediaCodecUtil.s(str, z, z2);
    }
}
