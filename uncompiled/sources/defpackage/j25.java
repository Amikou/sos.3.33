package defpackage;

import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: j25  reason: default package */
/* loaded from: classes.dex */
public final class j25 {
    public final int a;
    public final ConnectionResult b;

    public j25(ConnectionResult connectionResult, int i) {
        zt2.j(connectionResult);
        this.b = connectionResult;
        this.a = i;
    }

    public final int a() {
        return this.a;
    }

    public final ConnectionResult b() {
        return this.b;
    }
}
