package defpackage;

import java.security.MessageDigest;

/* compiled from: Options.java */
/* renamed from: vn2  reason: default package */
/* loaded from: classes.dex */
public final class vn2 implements fx1 {
    public final rh<mn2<?>, Object> b = new yt();

    /* JADX WARN: Multi-variable type inference failed */
    public static <T> void f(mn2<T> mn2Var, Object obj, MessageDigest messageDigest) {
        mn2Var.g(obj, messageDigest);
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            f(this.b.i(i), this.b.m(i), messageDigest);
        }
    }

    public <T> T c(mn2<T> mn2Var) {
        return this.b.containsKey(mn2Var) ? (T) this.b.get(mn2Var) : mn2Var.c();
    }

    public void d(vn2 vn2Var) {
        this.b.j(vn2Var.b);
    }

    public <T> vn2 e(mn2<T> mn2Var, T t) {
        this.b.put(mn2Var, t);
        return this;
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof vn2) {
            return this.b.equals(((vn2) obj).b);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return "Options{values=" + this.b + '}';
    }
}
