package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: HolderDappBinding.java */
/* renamed from: uk1  reason: default package */
/* loaded from: classes2.dex */
public final class uk1 {
    public final ConstraintLayout a;
    public final AppCompatImageView b;
    public final AppCompatImageView c;
    public final AppCompatImageView d;
    public final AppCompatTextView e;
    public final AppCompatTextView f;

    public uk1(ConstraintLayout constraintLayout, MaterialButton materialButton, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, MaterialCardView materialCardView, MaterialCardView materialCardView2, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2) {
        this.a = constraintLayout;
        this.b = appCompatImageView;
        this.c = appCompatImageView2;
        this.d = appCompatImageView3;
        this.e = appCompatTextView;
        this.f = appCompatTextView2;
    }

    public static uk1 a(View view) {
        int i = R.id.btnDelete;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDelete);
        if (materialButton != null) {
            i = R.id.imgDesktop;
            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgDesktop);
            if (appCompatImageView != null) {
                i = R.id.imgRightIndicator;
                AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgRightIndicator);
                if (appCompatImageView2 != null) {
                    i = R.id.imgThumbDapp;
                    AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.imgThumbDapp);
                    if (appCompatImageView3 != null) {
                        i = R.id.rowBG;
                        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.rowBG);
                        if (materialCardView != null) {
                            i = R.id.rowFG;
                            MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.rowFG);
                            if (materialCardView2 != null) {
                                i = R.id.txtDAppLink;
                                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtDAppLink);
                                if (appCompatTextView != null) {
                                    i = R.id.txtDAppName;
                                    AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txtDAppName);
                                    if (appCompatTextView2 != null) {
                                        return new uk1((ConstraintLayout) view, materialButton, appCompatImageView, appCompatImageView2, appCompatImageView3, materialCardView, materialCardView2, appCompatTextView, appCompatTextView2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static uk1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.holder_dapp, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
