package defpackage;

import android.app.Dialog;
import android.os.Bundle;

/* compiled from: AppCompatDialogFragment.java */
/* renamed from: ff  reason: default package */
/* loaded from: classes.dex */
public class ff extends sn0 {
    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        return new ef(getContext(), l());
    }

    @Override // defpackage.sn0
    public void t(Dialog dialog, int i) {
        if (dialog instanceof ef) {
            ef efVar = (ef) dialog;
            if (i != 1 && i != 2) {
                if (i != 3) {
                    return;
                }
                dialog.getWindow().addFlags(24);
            }
            efVar.supportRequestWindowFeature(1);
            return;
        }
        super.t(dialog, i);
    }
}
