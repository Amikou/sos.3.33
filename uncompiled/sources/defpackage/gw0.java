package defpackage;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: EthBlock.java */
/* renamed from: gw0  reason: default package */
/* loaded from: classes3.dex */
public class gw0 extends i83<a> {

    /* compiled from: EthBlock.java */
    /* renamed from: gw0$a */
    /* loaded from: classes3.dex */
    public static class a {
        private String author;
        private String difficulty;
        private String extraData;
        private String gasLimit;
        private String gasUsed;
        private String hash;
        private String logsBloom;
        private String miner;
        private String mixHash;
        private String nonce;
        private String number;
        private String parentHash;
        private String receiptsRoot;
        private List<String> sealFields;
        private String sha3Uncles;
        private String size;
        private String stateRoot;
        private String timestamp;
        private String totalDifficulty;
        private List<f> transactions;
        private String transactionsRoot;
        private List<String> uncles;

        public a() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (getNumberRaw() == null ? aVar.getNumberRaw() == null : getNumberRaw().equals(aVar.getNumberRaw())) {
                    if (getHash() == null ? aVar.getHash() == null : getHash().equals(aVar.getHash())) {
                        if (getParentHash() == null ? aVar.getParentHash() == null : getParentHash().equals(aVar.getParentHash())) {
                            if (getNonceRaw() == null ? aVar.getNonceRaw() == null : getNonceRaw().equals(aVar.getNonceRaw())) {
                                if (getSha3Uncles() == null ? aVar.getSha3Uncles() == null : getSha3Uncles().equals(aVar.getSha3Uncles())) {
                                    if (getLogsBloom() == null ? aVar.getLogsBloom() == null : getLogsBloom().equals(aVar.getLogsBloom())) {
                                        if (getTransactionsRoot() == null ? aVar.getTransactionsRoot() == null : getTransactionsRoot().equals(aVar.getTransactionsRoot())) {
                                            if (getStateRoot() == null ? aVar.getStateRoot() == null : getStateRoot().equals(aVar.getStateRoot())) {
                                                if (getReceiptsRoot() == null ? aVar.getReceiptsRoot() == null : getReceiptsRoot().equals(aVar.getReceiptsRoot())) {
                                                    if (getAuthor() == null ? aVar.getAuthor() == null : getAuthor().equals(aVar.getAuthor())) {
                                                        if (getMiner() == null ? aVar.getMiner() == null : getMiner().equals(aVar.getMiner())) {
                                                            if (getMixHash() == null ? aVar.getMixHash() == null : getMixHash().equals(aVar.getMixHash())) {
                                                                if (getDifficultyRaw() == null ? aVar.getDifficultyRaw() == null : getDifficultyRaw().equals(aVar.getDifficultyRaw())) {
                                                                    if (getTotalDifficultyRaw() == null ? aVar.getTotalDifficultyRaw() == null : getTotalDifficultyRaw().equals(aVar.getTotalDifficultyRaw())) {
                                                                        if (getExtraData() == null ? aVar.getExtraData() == null : getExtraData().equals(aVar.getExtraData())) {
                                                                            if (getSizeRaw() == null ? aVar.getSizeRaw() == null : getSizeRaw().equals(aVar.getSizeRaw())) {
                                                                                if (getGasLimitRaw() == null ? aVar.getGasLimitRaw() == null : getGasLimitRaw().equals(aVar.getGasLimitRaw())) {
                                                                                    if (getGasUsedRaw() == null ? aVar.getGasUsedRaw() == null : getGasUsedRaw().equals(aVar.getGasUsedRaw())) {
                                                                                        if (getTimestampRaw() == null ? aVar.getTimestampRaw() == null : getTimestampRaw().equals(aVar.getTimestampRaw())) {
                                                                                            if (getTransactions() == null ? aVar.getTransactions() == null : getTransactions().equals(aVar.getTransactions())) {
                                                                                                if (getUncles() == null ? aVar.getUncles() == null : getUncles().equals(aVar.getUncles())) {
                                                                                                    if (getSealFields() != null) {
                                                                                                        return getSealFields().equals(aVar.getSealFields());
                                                                                                    }
                                                                                                    return aVar.getSealFields() == null;
                                                                                                }
                                                                                                return false;
                                                                                            }
                                                                                            return false;
                                                                                        }
                                                                                        return false;
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                                return false;
                                                                            }
                                                                            return false;
                                                                        }
                                                                        return false;
                                                                    }
                                                                    return false;
                                                                }
                                                                return false;
                                                            }
                                                            return false;
                                                        }
                                                        return false;
                                                    }
                                                    return false;
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public String getAuthor() {
            return this.author;
        }

        public BigInteger getDifficulty() {
            return ej2.decodeQuantity(this.difficulty);
        }

        public String getDifficultyRaw() {
            return this.difficulty;
        }

        public String getExtraData() {
            return this.extraData;
        }

        public BigInteger getGasLimit() {
            return ej2.decodeQuantity(this.gasLimit);
        }

        public String getGasLimitRaw() {
            return this.gasLimit;
        }

        public BigInteger getGasUsed() {
            return ej2.decodeQuantity(this.gasUsed);
        }

        public String getGasUsedRaw() {
            return this.gasUsed;
        }

        public String getHash() {
            return this.hash;
        }

        public String getLogsBloom() {
            return this.logsBloom;
        }

        public String getMiner() {
            return this.miner;
        }

        public String getMixHash() {
            return this.mixHash;
        }

        public BigInteger getNonce() {
            return ej2.decodeQuantity(this.nonce);
        }

        public String getNonceRaw() {
            return this.nonce;
        }

        public BigInteger getNumber() {
            return ej2.decodeQuantity(this.number);
        }

        public String getNumberRaw() {
            return this.number;
        }

        public String getParentHash() {
            return this.parentHash;
        }

        public String getReceiptsRoot() {
            return this.receiptsRoot;
        }

        public List<String> getSealFields() {
            return this.sealFields;
        }

        public String getSha3Uncles() {
            return this.sha3Uncles;
        }

        public BigInteger getSize() {
            String str = this.size;
            return str != null ? ej2.decodeQuantity(str) : BigInteger.ZERO;
        }

        public String getSizeRaw() {
            return this.size;
        }

        public String getStateRoot() {
            return this.stateRoot;
        }

        public BigInteger getTimestamp() {
            return ej2.decodeQuantity(this.timestamp);
        }

        public String getTimestampRaw() {
            return this.timestamp;
        }

        public BigInteger getTotalDifficulty() {
            return ej2.decodeQuantity(this.totalDifficulty);
        }

        public String getTotalDifficultyRaw() {
            return this.totalDifficulty;
        }

        public List<f> getTransactions() {
            return this.transactions;
        }

        public String getTransactionsRoot() {
            return this.transactionsRoot;
        }

        public List<String> getUncles() {
            return this.uncles;
        }

        public int hashCode() {
            return ((((((((((((((((((((((((((((((((((((((((((getNumberRaw() != null ? getNumberRaw().hashCode() : 0) * 31) + (getHash() != null ? getHash().hashCode() : 0)) * 31) + (getParentHash() != null ? getParentHash().hashCode() : 0)) * 31) + (getNonceRaw() != null ? getNonceRaw().hashCode() : 0)) * 31) + (getSha3Uncles() != null ? getSha3Uncles().hashCode() : 0)) * 31) + (getLogsBloom() != null ? getLogsBloom().hashCode() : 0)) * 31) + (getTransactionsRoot() != null ? getTransactionsRoot().hashCode() : 0)) * 31) + (getStateRoot() != null ? getStateRoot().hashCode() : 0)) * 31) + (getReceiptsRoot() != null ? getReceiptsRoot().hashCode() : 0)) * 31) + (getAuthor() != null ? getAuthor().hashCode() : 0)) * 31) + (getMiner() != null ? getMiner().hashCode() : 0)) * 31) + (getMixHash() != null ? getMixHash().hashCode() : 0)) * 31) + (getDifficultyRaw() != null ? getDifficultyRaw().hashCode() : 0)) * 31) + (getTotalDifficultyRaw() != null ? getTotalDifficultyRaw().hashCode() : 0)) * 31) + (getExtraData() != null ? getExtraData().hashCode() : 0)) * 31) + (getSizeRaw() != null ? getSizeRaw().hashCode() : 0)) * 31) + (getGasLimitRaw() != null ? getGasLimitRaw().hashCode() : 0)) * 31) + (getGasUsedRaw() != null ? getGasUsedRaw().hashCode() : 0)) * 31) + (getTimestampRaw() != null ? getTimestampRaw().hashCode() : 0)) * 31) + (getTransactions() != null ? getTransactions().hashCode() : 0)) * 31) + (getUncles() != null ? getUncles().hashCode() : 0)) * 31) + (getSealFields() != null ? getSealFields().hashCode() : 0);
        }

        public void setAuthor(String str) {
            this.author = str;
        }

        public void setDifficulty(String str) {
            this.difficulty = str;
        }

        public void setExtraData(String str) {
            this.extraData = str;
        }

        public void setGasLimit(String str) {
            this.gasLimit = str;
        }

        public void setGasUsed(String str) {
            this.gasUsed = str;
        }

        public void setHash(String str) {
            this.hash = str;
        }

        public void setLogsBloom(String str) {
            this.logsBloom = str;
        }

        public void setMiner(String str) {
            this.miner = str;
        }

        public void setMixHash(String str) {
            this.mixHash = str;
        }

        public void setNonce(String str) {
            this.nonce = str;
        }

        public void setNumber(String str) {
            this.number = str;
        }

        public void setParentHash(String str) {
            this.parentHash = str;
        }

        public void setReceiptsRoot(String str) {
            this.receiptsRoot = str;
        }

        public void setSealFields(List<String> list) {
            this.sealFields = list;
        }

        public void setSha3Uncles(String str) {
            this.sha3Uncles = str;
        }

        public void setSize(String str) {
            this.size = str;
        }

        public void setStateRoot(String str) {
            this.stateRoot = str;
        }

        public void setTimestamp(String str) {
            this.timestamp = str;
        }

        public void setTotalDifficulty(String str) {
            this.totalDifficulty = str;
        }

        @com.fasterxml.jackson.databind.annotation.b(using = c.class)
        public void setTransactions(List<f> list) {
            this.transactions = list;
        }

        public void setTransactionsRoot(String str) {
            this.transactionsRoot = str;
        }

        public void setUncles(List<String> list) {
            this.uncles = list;
        }

        public a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, String str18, String str19, List<f> list, List<String> list2, List<String> list3) {
            this.number = str;
            this.hash = str2;
            this.parentHash = str3;
            this.nonce = str4;
            this.sha3Uncles = str5;
            this.logsBloom = str6;
            this.transactionsRoot = str7;
            this.stateRoot = str8;
            this.receiptsRoot = str9;
            this.author = str10;
            this.miner = str11;
            this.mixHash = str12;
            this.difficulty = str13;
            this.totalDifficulty = str14;
            this.extraData = str15;
            this.size = str16;
            this.gasLimit = str17;
            this.gasUsed = str18;
            this.timestamp = str19;
            this.transactions = list;
            this.uncles = list2;
            this.sealFields = list3;
        }
    }

    /* compiled from: EthBlock.java */
    /* renamed from: gw0$b */
    /* loaded from: classes3.dex */
    public static class b extends com.fasterxml.jackson.databind.c<a> {
        private ObjectReader objectReader = ml2.getObjectReader();

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // com.fasterxml.jackson.databind.c
        public a deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            if (jsonParser.u() != JsonToken.VALUE_NULL) {
                return (a) this.objectReader.readValue(jsonParser, a.class);
            }
            return null;
        }
    }

    /* compiled from: EthBlock.java */
    /* renamed from: gw0$c */
    /* loaded from: classes3.dex */
    public static class c extends com.fasterxml.jackson.databind.c<List<f>> {
        private ObjectReader objectReader = ml2.getObjectReader();

        @Override // com.fasterxml.jackson.databind.c
        public List<f> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            ArrayList arrayList = new ArrayList();
            JsonToken T0 = jsonParser.T0();
            if (T0 == JsonToken.START_OBJECT) {
                Iterator readValues = this.objectReader.readValues(jsonParser, e.class);
                while (readValues.hasNext()) {
                    arrayList.add(readValues.next());
                }
            } else if (T0 == JsonToken.VALUE_STRING) {
                jsonParser.x0();
                Iterator readValues2 = this.objectReader.readValues(jsonParser, d.class);
                while (readValues2.hasNext()) {
                    arrayList.add(readValues2.next());
                }
            }
            return arrayList;
        }
    }

    /* compiled from: EthBlock.java */
    /* renamed from: gw0$d */
    /* loaded from: classes3.dex */
    public static class d implements f<String> {
        private String value;

        public d() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof d) {
                String str = this.value;
                String str2 = ((d) obj).value;
                return str != null ? str.equals(str2) : str2 == null;
            }
            return false;
        }

        public int hashCode() {
            String str = this.value;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public void setValue(String str) {
            this.value = str;
        }

        public d(String str) {
            this.value = str;
        }

        @Override // defpackage.gw0.f
        public String get() {
            return this.value;
        }
    }

    /* compiled from: EthBlock.java */
    /* renamed from: gw0$e */
    /* loaded from: classes3.dex */
    public static class e extends o84 implements f<o84> {
        public e() {
        }

        @Override // defpackage.gw0.f
        public o84 get() {
            return this;
        }

        public e(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, int i) {
            super(str, str2, str3, str4, str5, str6, str7, str8, str10, str9, str11, str12, str13, str14, str15, str16, i);
        }
    }

    /* compiled from: EthBlock.java */
    /* renamed from: gw0$f */
    /* loaded from: classes3.dex */
    public interface f<T> {
        T get();
    }

    public a getBlock() {
        return getResult();
    }

    @Override // defpackage.i83
    @com.fasterxml.jackson.databind.annotation.b(using = b.class)
    public void setResult(a aVar) {
        super.setResult((gw0) aVar);
    }
}
