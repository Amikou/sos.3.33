package defpackage;

/* renamed from: pd5  reason: default package */
/* loaded from: classes.dex */
public final class pd5 implements ee5 {
    public ee5[] a;

    public pd5(ee5... ee5VarArr) {
        this.a = ee5VarArr;
    }

    @Override // defpackage.ee5
    public final boolean a(Class<?> cls) {
        for (ee5 ee5Var : this.a) {
            if (ee5Var.a(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.ee5
    public final ce5 b(Class<?> cls) {
        ee5[] ee5VarArr;
        for (ee5 ee5Var : this.a) {
            if (ee5Var.a(cls)) {
                return ee5Var.b(cls);
            }
        }
        String name = cls.getName();
        throw new UnsupportedOperationException(name.length() != 0 ? "No factory is available for message type: ".concat(name) : new String("No factory is available for message type: "));
    }
}
