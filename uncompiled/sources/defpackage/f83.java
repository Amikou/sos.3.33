package defpackage;

import android.content.Context;
import com.google.firebase.crashlytics.internal.common.CommonUtils;

/* compiled from: ResourceUnityVersionProvider.java */
/* renamed from: f83  reason: default package */
/* loaded from: classes2.dex */
public class f83 implements ze4 {
    public final Context a;
    public boolean b = false;
    public String c;

    public f83(Context context) {
        this.a = context;
    }

    @Override // defpackage.ze4
    public String a() {
        if (!this.b) {
            this.c = CommonUtils.z(this.a);
            this.b = true;
        }
        String str = this.c;
        if (str != null) {
            return str;
        }
        return null;
    }
}
