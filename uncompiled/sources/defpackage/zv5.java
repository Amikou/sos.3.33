package defpackage;

import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: zv5  reason: default package */
/* loaded from: classes.dex */
public interface zv5<E> extends List<E>, RandomAccess {
    zv5<E> Q(int i);

    boolean zza();

    void zzb();
}
