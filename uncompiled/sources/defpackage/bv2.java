package defpackage;

import android.content.Context;
import android.os.PowerManager;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import defpackage.cr4;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/* compiled from: Processor.java */
/* renamed from: bv2  reason: default package */
/* loaded from: classes.dex */
public class bv2 implements qy0, t81 {
    public static final String p0 = v12.f("Processor");
    public Context f0;
    public androidx.work.a g0;
    public q34 h0;
    public WorkDatabase i0;
    public List<cd3> l0;
    public Map<String, cr4> k0 = new HashMap();
    public Map<String, cr4> j0 = new HashMap();
    public Set<String> m0 = new HashSet();
    public final List<qy0> n0 = new ArrayList();
    public PowerManager.WakeLock a = null;
    public final Object o0 = new Object();

    /* compiled from: Processor.java */
    /* renamed from: bv2$a */
    /* loaded from: classes.dex */
    public static class a implements Runnable {
        public qy0 a;
        public String f0;
        public l02<Boolean> g0;

        public a(qy0 qy0Var, String str, l02<Boolean> l02Var) {
            this.a = qy0Var;
            this.f0 = str;
            this.g0 = l02Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            boolean z;
            try {
                z = this.g0.get().booleanValue();
            } catch (InterruptedException | ExecutionException unused) {
                z = true;
            }
            this.a.c(this.f0, z);
        }
    }

    public bv2(Context context, androidx.work.a aVar, q34 q34Var, WorkDatabase workDatabase, List<cd3> list) {
        this.f0 = context;
        this.g0 = aVar;
        this.h0 = q34Var;
        this.i0 = workDatabase;
        this.l0 = list;
    }

    public static boolean e(String str, cr4 cr4Var) {
        if (cr4Var != null) {
            cr4Var.d();
            v12.c().a(p0, String.format("WorkerWrapper interrupted for %s", str), new Throwable[0]);
            return true;
        }
        v12.c().a(p0, String.format("WorkerWrapper could not be found for %s", str), new Throwable[0]);
        return false;
    }

    @Override // defpackage.t81
    public void a(String str, s81 s81Var) {
        synchronized (this.o0) {
            v12.c().d(p0, String.format("Moving WorkSpec (%s) to the foreground", str), new Throwable[0]);
            cr4 remove = this.k0.remove(str);
            if (remove != null) {
                if (this.a == null) {
                    PowerManager.WakeLock b = rl4.b(this.f0, "ProcessorForegroundLck");
                    this.a = b;
                    b.acquire();
                }
                this.j0.put(str, remove);
                m70.l(this.f0, androidx.work.impl.foreground.a.d(this.f0, str, s81Var));
            }
        }
    }

    @Override // defpackage.t81
    public void b(String str) {
        synchronized (this.o0) {
            this.j0.remove(str);
            m();
        }
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        synchronized (this.o0) {
            this.k0.remove(str);
            v12.c().a(p0, String.format("%s %s executed; reschedule = %s", bv2.class.getSimpleName(), str, Boolean.valueOf(z)), new Throwable[0]);
            for (qy0 qy0Var : this.n0) {
                qy0Var.c(str, z);
            }
        }
    }

    public void d(qy0 qy0Var) {
        synchronized (this.o0) {
            this.n0.add(qy0Var);
        }
    }

    public boolean f(String str) {
        boolean contains;
        synchronized (this.o0) {
            contains = this.m0.contains(str);
        }
        return contains;
    }

    public boolean g(String str) {
        boolean z;
        synchronized (this.o0) {
            z = this.k0.containsKey(str) || this.j0.containsKey(str);
        }
        return z;
    }

    public boolean h(String str) {
        boolean containsKey;
        synchronized (this.o0) {
            containsKey = this.j0.containsKey(str);
        }
        return containsKey;
    }

    public void i(qy0 qy0Var) {
        synchronized (this.o0) {
            this.n0.remove(qy0Var);
        }
    }

    public boolean j(String str) {
        return k(str, null);
    }

    public boolean k(String str, WorkerParameters.a aVar) {
        synchronized (this.o0) {
            if (g(str)) {
                v12.c().a(p0, String.format("Work %s is already enqueued for processing", str), new Throwable[0]);
                return false;
            }
            cr4 a2 = new cr4.c(this.f0, this.g0, this.h0, this, this.i0, str).c(this.l0).b(aVar).a();
            l02<Boolean> b = a2.b();
            b.d(new a(this, str, b), this.h0.a());
            this.k0.put(str, a2);
            this.h0.c().execute(a2);
            v12.c().a(p0, String.format("%s: processing %s", bv2.class.getSimpleName(), str), new Throwable[0]);
            return true;
        }
    }

    public boolean l(String str) {
        boolean e;
        synchronized (this.o0) {
            boolean z = true;
            v12.c().a(p0, String.format("Processor cancelling %s", str), new Throwable[0]);
            this.m0.add(str);
            cr4 remove = this.j0.remove(str);
            if (remove == null) {
                z = false;
            }
            if (remove == null) {
                remove = this.k0.remove(str);
            }
            e = e(str, remove);
            if (z) {
                m();
            }
        }
        return e;
    }

    public final void m() {
        synchronized (this.o0) {
            if (!(!this.j0.isEmpty())) {
                this.f0.startService(androidx.work.impl.foreground.a.e(this.f0));
                PowerManager.WakeLock wakeLock = this.a;
                if (wakeLock != null) {
                    wakeLock.release();
                    this.a = null;
                }
            }
        }
    }

    public boolean n(String str) {
        boolean e;
        synchronized (this.o0) {
            v12.c().a(p0, String.format("Processor stopping foreground work %s", str), new Throwable[0]);
            e = e(str, this.j0.remove(str));
        }
        return e;
    }

    public boolean o(String str) {
        boolean e;
        synchronized (this.o0) {
            v12.c().a(p0, String.format("Processor stopping background work %s", str), new Throwable[0]);
            e = e(str, this.k0.remove(str));
        }
        return e;
    }
}
