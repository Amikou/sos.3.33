package defpackage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: RequestTracker.java */
/* renamed from: o73  reason: default package */
/* loaded from: classes.dex */
public class o73 {
    public final Set<d73> a = Collections.newSetFromMap(new WeakHashMap());
    public final Set<d73> b = new HashSet();
    public boolean c;

    public boolean a(d73 d73Var) {
        boolean z = true;
        if (d73Var == null) {
            return true;
        }
        boolean remove = this.a.remove(d73Var);
        if (!this.b.remove(d73Var) && !remove) {
            z = false;
        }
        if (z) {
            d73Var.clear();
        }
        return z;
    }

    public void b() {
        for (d73 d73Var : mg4.j(this.a)) {
            a(d73Var);
        }
        this.b.clear();
    }

    public void c() {
        this.c = true;
        for (d73 d73Var : mg4.j(this.a)) {
            if (d73Var.isRunning() || d73Var.l()) {
                d73Var.clear();
                this.b.add(d73Var);
            }
        }
    }

    public void d() {
        this.c = true;
        for (d73 d73Var : mg4.j(this.a)) {
            if (d73Var.isRunning()) {
                d73Var.c();
                this.b.add(d73Var);
            }
        }
    }

    public void e() {
        for (d73 d73Var : mg4.j(this.a)) {
            if (!d73Var.l() && !d73Var.j()) {
                d73Var.clear();
                if (!this.c) {
                    d73Var.k();
                } else {
                    this.b.add(d73Var);
                }
            }
        }
    }

    public void f() {
        this.c = false;
        for (d73 d73Var : mg4.j(this.a)) {
            if (!d73Var.l() && !d73Var.isRunning()) {
                d73Var.k();
            }
        }
        this.b.clear();
    }

    public void g(d73 d73Var) {
        this.a.add(d73Var);
        if (!this.c) {
            d73Var.k();
            return;
        }
        d73Var.clear();
        this.b.add(d73Var);
    }

    public String toString() {
        return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
    }
}
