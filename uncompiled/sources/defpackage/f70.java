package defpackage;

import android.content.ClipData;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.ContentInfo;

/* compiled from: ContentInfoCompat.java */
/* renamed from: f70  reason: default package */
/* loaded from: classes.dex */
public final class f70 {
    public final f a;

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final c a;

        public a(ClipData clipData, int i) {
            if (Build.VERSION.SDK_INT >= 31) {
                this.a = new b(clipData, i);
            } else {
                this.a = new d(clipData, i);
            }
        }

        public f70 a() {
            return this.a.build();
        }

        public a b(Bundle bundle) {
            this.a.setExtras(bundle);
            return this;
        }

        public a c(int i) {
            this.a.b(i);
            return this;
        }

        public a d(Uri uri) {
            this.a.a(uri);
            return this;
        }
    }

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$b */
    /* loaded from: classes.dex */
    public static final class b implements c {
        public final ContentInfo.Builder a;

        public b(ClipData clipData, int i) {
            this.a = new ContentInfo.Builder(clipData, i);
        }

        @Override // defpackage.f70.c
        public void a(Uri uri) {
            this.a.setLinkUri(uri);
        }

        @Override // defpackage.f70.c
        public void b(int i) {
            this.a.setFlags(i);
        }

        @Override // defpackage.f70.c
        public f70 build() {
            return new f70(new e(this.a.build()));
        }

        @Override // defpackage.f70.c
        public void setExtras(Bundle bundle) {
            this.a.setExtras(bundle);
        }
    }

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$c */
    /* loaded from: classes.dex */
    public interface c {
        void a(Uri uri);

        void b(int i);

        f70 build();

        void setExtras(Bundle bundle);
    }

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$d */
    /* loaded from: classes.dex */
    public static final class d implements c {
        public ClipData a;
        public int b;
        public int c;
        public Uri d;
        public Bundle e;

        public d(ClipData clipData, int i) {
            this.a = clipData;
            this.b = i;
        }

        @Override // defpackage.f70.c
        public void a(Uri uri) {
            this.d = uri;
        }

        @Override // defpackage.f70.c
        public void b(int i) {
            this.c = i;
        }

        @Override // defpackage.f70.c
        public f70 build() {
            return new f70(new g(this));
        }

        @Override // defpackage.f70.c
        public void setExtras(Bundle bundle) {
            this.e = bundle;
        }
    }

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$e */
    /* loaded from: classes.dex */
    public static final class e implements f {
        public final ContentInfo a;

        public e(ContentInfo contentInfo) {
            this.a = (ContentInfo) du2.e(contentInfo);
        }

        @Override // defpackage.f70.f
        public ClipData a() {
            return this.a.getClip();
        }

        @Override // defpackage.f70.f
        public ContentInfo b() {
            return this.a;
        }

        @Override // defpackage.f70.f
        public int getFlags() {
            return this.a.getFlags();
        }

        @Override // defpackage.f70.f
        public int getSource() {
            return this.a.getSource();
        }

        public String toString() {
            return "ContentInfoCompat{" + this.a + "}";
        }
    }

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$f */
    /* loaded from: classes.dex */
    public interface f {
        ClipData a();

        ContentInfo b();

        int getFlags();

        int getSource();
    }

    /* compiled from: ContentInfoCompat.java */
    /* renamed from: f70$g */
    /* loaded from: classes.dex */
    public static final class g implements f {
        public final ClipData a;
        public final int b;
        public final int c;
        public final Uri d;
        public final Bundle e;

        public g(d dVar) {
            this.a = (ClipData) du2.e(dVar.a);
            this.b = du2.b(dVar.b, 0, 5, "source");
            this.c = du2.d(dVar.c, 1);
            this.d = dVar.d;
            this.e = dVar.e;
        }

        @Override // defpackage.f70.f
        public ClipData a() {
            return this.a;
        }

        @Override // defpackage.f70.f
        public ContentInfo b() {
            return null;
        }

        @Override // defpackage.f70.f
        public int getFlags() {
            return this.c;
        }

        @Override // defpackage.f70.f
        public int getSource() {
            return this.b;
        }

        public String toString() {
            String str;
            StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{clip=");
            sb.append(this.a.getDescription());
            sb.append(", source=");
            sb.append(f70.e(this.b));
            sb.append(", flags=");
            sb.append(f70.a(this.c));
            if (this.d == null) {
                str = "";
            } else {
                str = ", hasLinkUri(" + this.d.toString().length() + ")";
            }
            sb.append(str);
            sb.append(this.e != null ? ", hasExtras" : "");
            sb.append("}");
            return sb.toString();
        }
    }

    public f70(f fVar) {
        this.a = fVar;
    }

    public static String a(int i) {
        return (i & 1) != 0 ? "FLAG_CONVERT_TO_PLAIN_TEXT" : String.valueOf(i);
    }

    public static String e(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? String.valueOf(i) : "SOURCE_PROCESS_TEXT" : "SOURCE_AUTOFILL" : "SOURCE_DRAG_AND_DROP" : "SOURCE_INPUT_METHOD" : "SOURCE_CLIPBOARD" : "SOURCE_APP";
    }

    public static f70 g(ContentInfo contentInfo) {
        return new f70(new e(contentInfo));
    }

    public ClipData b() {
        return this.a.a();
    }

    public int c() {
        return this.a.getFlags();
    }

    public int d() {
        return this.a.getSource();
    }

    public ContentInfo f() {
        return this.a.b();
    }

    public String toString() {
        return this.a.toString();
    }
}
