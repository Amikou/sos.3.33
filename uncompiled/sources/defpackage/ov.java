package defpackage;

import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CancellableContinuation.kt */
/* renamed from: ov  reason: default package */
/* loaded from: classes2.dex */
public interface ov<T> extends q70<T> {

    /* compiled from: CancellableContinuation.kt */
    /* renamed from: ov$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ Object a(ov ovVar, Object obj, Object obj2, int i, Object obj3) {
            if (obj3 == null) {
                if ((i & 2) != 0) {
                    obj2 = null;
                }
                return ovVar.c(obj, obj2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryResume");
        }
    }

    Object c(T t, Object obj);

    void f(tc1<? super Throwable, te4> tc1Var);

    Object i(Throwable th);

    void j(T t, tc1<? super Throwable, te4> tc1Var);

    void k(CoroutineDispatcher coroutineDispatcher, T t);

    Object o(T t, Object obj, tc1<? super Throwable, te4> tc1Var);

    void s(Object obj);
}
