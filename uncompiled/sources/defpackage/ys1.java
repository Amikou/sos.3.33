package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: ItemFiatBinding.java */
/* renamed from: ys1  reason: default package */
/* loaded from: classes2.dex */
public final class ys1 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final TextView c;
    public final View d;

    public ys1(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, TextView textView, View view) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = textView;
        this.d = view;
    }

    public static ys1 a(View view) {
        int i = R.id.cbSelectFiat;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbSelectFiat);
        if (materialCheckBox != null) {
            i = R.id.tvCurrencyName;
            TextView textView = (TextView) ai4.a(view, R.id.tvCurrencyName);
            if (textView != null) {
                i = R.id.vDivider;
                View a = ai4.a(view, R.id.vDivider);
                if (a != null) {
                    return new ys1((ConstraintLayout) view, materialCheckBox, textView, a);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
