package defpackage;

import com.google.crypto.tink.shaded.protobuf.c0;
import com.google.crypto.tink.shaded.protobuf.d0;

/* compiled from: MapFieldSchemas.java */
/* renamed from: v32  reason: default package */
/* loaded from: classes2.dex */
public final class v32 {
    public static final c0 a = c();
    public static final c0 b = new d0();

    public static c0 a() {
        return a;
    }

    public static c0 b() {
        return b;
    }

    public static c0 c() {
        try {
            return (c0) Class.forName("com.google.crypto.tink.shaded.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
