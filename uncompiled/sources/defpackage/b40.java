package defpackage;

import java.util.Set;

/* compiled from: ComponentContainer.java */
/* renamed from: b40  reason: default package */
/* loaded from: classes2.dex */
public interface b40 {
    <T> T a(Class<T> cls);

    <T> fw2<T> b(Class<T> cls);

    <T> fw2<Set<T>> c(Class<T> cls);

    <T> Set<T> d(Class<T> cls);

    <T> rl0<T> e(Class<T> cls);
}
