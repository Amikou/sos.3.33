package defpackage;

import com.bumptech.glide.load.data.d;
import java.util.Collections;
import java.util.List;

/* compiled from: ModelLoader.java */
/* renamed from: j92  reason: default package */
/* loaded from: classes.dex */
public interface j92<Model, Data> {

    /* compiled from: ModelLoader.java */
    /* renamed from: j92$a */
    /* loaded from: classes.dex */
    public static class a<Data> {
        public final fx1 a;
        public final List<fx1> b;
        public final d<Data> c;

        public a(fx1 fx1Var, d<Data> dVar) {
            this(fx1Var, Collections.emptyList(), dVar);
        }

        public a(fx1 fx1Var, List<fx1> list, d<Data> dVar) {
            this.a = (fx1) wt2.d(fx1Var);
            this.b = (List) wt2.d(list);
            this.c = (d) wt2.d(dVar);
        }
    }

    boolean a(Model model);

    a<Data> b(Model model, int i, int i2, vn2 vn2Var);
}
