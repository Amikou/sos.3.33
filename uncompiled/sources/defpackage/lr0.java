package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;

/* compiled from: Drawer.java */
/* renamed from: lr0  reason: default package */
/* loaded from: classes2.dex */
public class lr0 {
    public io a;
    public v20 b;
    public lc3 c;
    public fr4 d;
    public iq3 e;
    public g41 f;
    public f54 g;
    public cs0 h;
    public fx3 i;
    public kc3 j;
    public int k;
    public int l;
    public int m;

    public lr0(mq1 mq1Var) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        this.a = new io(paint, mq1Var);
        this.b = new v20(paint, mq1Var);
        this.c = new lc3(paint, mq1Var);
        this.d = new fr4(paint, mq1Var);
        this.e = new iq3(paint, mq1Var);
        this.f = new g41(paint, mq1Var);
        this.g = new f54(paint, mq1Var);
        this.h = new cs0(paint, mq1Var);
        this.i = new fx3(paint, mq1Var);
        this.j = new kc3(paint, mq1Var);
    }

    public void a(Canvas canvas, boolean z) {
        if (this.b != null) {
            this.a.a(canvas, this.k, z, this.l, this.m);
        }
    }

    public void b(Canvas canvas, wg4 wg4Var) {
        v20 v20Var = this.b;
        if (v20Var != null) {
            v20Var.a(canvas, wg4Var, this.k, this.l, this.m);
        }
    }

    public void c(Canvas canvas, wg4 wg4Var) {
        cs0 cs0Var = this.h;
        if (cs0Var != null) {
            cs0Var.a(canvas, wg4Var, this.l, this.m);
        }
    }

    public void d(Canvas canvas, wg4 wg4Var) {
        g41 g41Var = this.f;
        if (g41Var != null) {
            g41Var.a(canvas, wg4Var, this.k, this.l, this.m);
        }
    }

    public void e(Canvas canvas, wg4 wg4Var) {
        lc3 lc3Var = this.c;
        if (lc3Var != null) {
            lc3Var.a(canvas, wg4Var, this.k, this.l, this.m);
        }
    }

    public void f(Canvas canvas, wg4 wg4Var) {
        kc3 kc3Var = this.j;
        if (kc3Var != null) {
            kc3Var.a(canvas, wg4Var, this.k, this.l, this.m);
        }
    }

    public void g(Canvas canvas, wg4 wg4Var) {
        iq3 iq3Var = this.e;
        if (iq3Var != null) {
            iq3Var.a(canvas, wg4Var, this.l, this.m);
        }
    }

    public void h(Canvas canvas, wg4 wg4Var) {
        fx3 fx3Var = this.i;
        if (fx3Var != null) {
            fx3Var.a(canvas, wg4Var, this.k, this.l, this.m);
        }
    }

    public void i(Canvas canvas, wg4 wg4Var) {
        f54 f54Var = this.g;
        if (f54Var != null) {
            f54Var.a(canvas, wg4Var, this.l, this.m);
        }
    }

    public void j(Canvas canvas, wg4 wg4Var) {
        fr4 fr4Var = this.d;
        if (fr4Var != null) {
            fr4Var.a(canvas, wg4Var, this.l, this.m);
        }
    }

    public void k(int i, int i2, int i3) {
        this.k = i;
        this.l = i2;
        this.m = i3;
    }
}
