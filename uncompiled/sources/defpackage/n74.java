package defpackage;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/* compiled from: TopKSelector.java */
/* renamed from: n74  reason: default package */
/* loaded from: classes2.dex */
public final class n74<T> {
    public final int a;
    public final Comparator<? super T> b;
    public final T[] c;
    public int d;
    public T e;

    public n74(Comparator<? super T> comparator, int i) {
        this.b = (Comparator) au2.l(comparator, "comparator");
        this.a = i;
        au2.f(i >= 0, "k (%s) must be >= 0", i);
        au2.f(i <= 1073741823, "k (%s) must be <= Integer.MAX_VALUE / 2", i);
        this.c = (T[]) new Object[pr1.a(i, 2)];
        this.d = 0;
        this.e = null;
    }

    public static <T> n74<T> a(int i, Comparator<? super T> comparator) {
        return new n74<>(comparator, i);
    }

    public void b(T t) {
        int i = this.a;
        if (i == 0) {
            return;
        }
        int i2 = this.d;
        if (i2 == 0) {
            this.c[0] = t;
            this.e = t;
            this.d = 1;
        } else if (i2 < i) {
            T[] tArr = this.c;
            this.d = i2 + 1;
            tArr[i2] = t;
            if (this.b.compare(t, (Object) yi2.a(this.e)) > 0) {
                this.e = t;
            }
        } else if (this.b.compare(t, (Object) yi2.a(this.e)) < 0) {
            T[] tArr2 = this.c;
            int i3 = this.d;
            int i4 = i3 + 1;
            this.d = i4;
            tArr2[i3] = t;
            if (i4 == this.a * 2) {
                g();
            }
        }
    }

    public void c(Iterator<? extends T> it) {
        while (it.hasNext()) {
            b(it.next());
        }
    }

    public final int d(int i, int i2, int i3) {
        Object a = yi2.a(this.c[i3]);
        T[] tArr = this.c;
        tArr[i3] = tArr[i2];
        int i4 = i;
        while (i < i2) {
            if (this.b.compare((Object) yi2.a(this.c[i]), a) < 0) {
                e(i4, i);
                i4++;
            }
            i++;
        }
        T[] tArr2 = this.c;
        tArr2[i2] = tArr2[i4];
        tArr2[i4] = a;
        return i4;
    }

    public final void e(int i, int i2) {
        T[] tArr = this.c;
        T t = tArr[i];
        tArr[i] = tArr[i2];
        tArr[i2] = t;
    }

    public List<T> f() {
        Arrays.sort(this.c, 0, this.d, this.b);
        int i = this.d;
        int i2 = this.a;
        if (i > i2) {
            T[] tArr = this.c;
            Arrays.fill(tArr, i2, tArr.length, (Object) null);
            int i3 = this.a;
            this.d = i3;
            this.e = this.c[i3 - 1];
        }
        return Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(this.c, this.d)));
    }

    public final void g() {
        int i = (this.a * 2) - 1;
        int d = pr1.d(i + 0, RoundingMode.CEILING) * 3;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i2 < i) {
                int d2 = d(i2, i, ((i2 + i) + 1) >>> 1);
                int i5 = this.a;
                if (d2 <= i5) {
                    if (d2 >= i5) {
                        break;
                    }
                    i2 = Math.max(d2, i2 + 1);
                    i4 = d2;
                } else {
                    i = d2 - 1;
                }
                i3++;
                if (i3 >= d) {
                    Arrays.sort(this.c, i2, i + 1, this.b);
                    break;
                }
            } else {
                break;
            }
        }
        this.d = this.a;
        this.e = (T) yi2.a(this.c[i4]);
        while (true) {
            i4++;
            if (i4 >= this.a) {
                return;
            }
            if (this.b.compare((Object) yi2.a(this.c[i4]), (Object) yi2.a(this.e)) > 0) {
                this.e = this.c[i4];
            }
        }
    }
}
