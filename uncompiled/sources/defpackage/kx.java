package defpackage;

/* compiled from: Channel.kt */
/* renamed from: kx  reason: default package */
/* loaded from: classes2.dex */
public interface kx<E> extends uj3<E>, f43<E> {
    public static final a c = a.a;

    /* compiled from: Channel.kt */
    /* renamed from: kx$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static final /* synthetic */ a a = new a();
        public static final int b = a34.b("kotlinx.coroutines.channels.defaultBuffer", 64, 1, 2147483646);

        public final int a() {
            return b;
        }
    }
}
