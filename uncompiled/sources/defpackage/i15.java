package defpackage;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: i15  reason: default package */
/* loaded from: classes.dex */
public final class i15 implements Runnable {
    public final /* synthetic */ l83 a;
    public final /* synthetic */ j15 f0;

    public i15(j15 j15Var, l83 l83Var) {
        this.f0 = j15Var;
        this.a = l83Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        WeakReference weakReference;
        q83 q83Var;
        l15 unused;
        l15 unused2;
        l15 unused3;
        l15 unused4;
        try {
            try {
                BasePendingResult.o.set(Boolean.TRUE);
                q83Var = this.f0.a;
                ((q83) zt2.j(q83Var)).b(this.a);
                unused = this.f0.g;
                unused2 = this.f0.g;
                throw null;
            } catch (RuntimeException unused5) {
                unused3 = this.f0.g;
                unused4 = this.f0.g;
                throw null;
            }
        } catch (Throwable th) {
            BasePendingResult.o.set(Boolean.FALSE);
            j15 j15Var = this.f0;
            j15.d(this.a);
            weakReference = this.f0.f;
            GoogleApiClient googleApiClient = (GoogleApiClient) weakReference.get();
            if (googleApiClient != null) {
                googleApiClient.l(this.f0);
            }
            throw th;
        }
    }
}
