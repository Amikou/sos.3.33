package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Process;
import android.os.WorkSource;
import android.util.Log;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: sq4  reason: default package */
/* loaded from: classes.dex */
public class sq4 {
    public static final Method a;
    public static final Method b;
    public static final Method c;
    public static final Method d;

    static {
        Process.myUid();
        a = g();
        b = i();
        c = j();
        k();
        d = l();
        m();
        n();
    }

    @RecentlyNullable
    public static WorkSource a(@RecentlyNonNull Context context, String str) {
        if (context != null && context.getPackageManager() != null && str != null) {
            try {
                ApplicationInfo c2 = kr4.a(context).c(str, 0);
                if (c2 == null) {
                    if (str.length() != 0) {
                        "Could not get applicationInfo from package: ".concat(str);
                    }
                    return null;
                }
                return e(c2.uid, str);
            } catch (PackageManager.NameNotFoundException unused) {
                if (str.length() != 0) {
                    "Could not find package: ".concat(str);
                }
            }
        }
        return null;
    }

    @RecentlyNonNull
    public static List<String> b(WorkSource workSource) {
        ArrayList arrayList = new ArrayList();
        int d2 = workSource == null ? 0 : d(workSource);
        if (d2 == 0) {
            return arrayList;
        }
        for (int i = 0; i < d2; i++) {
            String f = f(workSource, i);
            if (!vu3.a(f)) {
                arrayList.add((String) zt2.j(f));
            }
        }
        return arrayList;
    }

    @RecentlyNonNull
    public static boolean c(@RecentlyNonNull Context context) {
        return (context == null || context.getPackageManager() == null || kr4.a(context).b("android.permission.UPDATE_DEVICE_STATS", context.getPackageName()) != 0) ? false : true;
    }

    public static int d(WorkSource workSource) {
        Method method = c;
        if (method != null) {
            try {
                return ((Integer) zt2.j(method.invoke(workSource, new Object[0]))).intValue();
            } catch (Exception e) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
            }
        }
        return 0;
    }

    public static WorkSource e(int i, String str) {
        WorkSource workSource = new WorkSource();
        h(workSource, i, str);
        return workSource;
    }

    public static String f(WorkSource workSource, int i) {
        Method method = d;
        if (method != null) {
            try {
                return (String) method.invoke(workSource, Integer.valueOf(i));
            } catch (Exception e) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
                return null;
            }
        }
        return null;
    }

    public static Method g() {
        try {
            return WorkSource.class.getMethod("add", Integer.TYPE);
        } catch (Exception unused) {
            return null;
        }
    }

    public static void h(WorkSource workSource, int i, String str) {
        Method method = b;
        if (method != null) {
            if (str == null) {
                str = "";
            }
            try {
                method.invoke(workSource, Integer.valueOf(i), str);
                return;
            } catch (Exception e) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
                return;
            }
        }
        Method method2 = a;
        if (method2 != null) {
            try {
                method2.invoke(workSource, Integer.valueOf(i));
            } catch (Exception e2) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e2);
            }
        }
    }

    public static Method i() {
        if (jr2.c()) {
            try {
                return WorkSource.class.getMethod("add", Integer.TYPE, String.class);
            } catch (Exception unused) {
            }
        }
        return null;
    }

    public static Method j() {
        try {
            return WorkSource.class.getMethod("size", new Class[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    public static Method k() {
        try {
            return WorkSource.class.getMethod("get", Integer.TYPE);
        } catch (Exception unused) {
            return null;
        }
    }

    public static Method l() {
        if (jr2.c()) {
            try {
                return WorkSource.class.getMethod("getName", Integer.TYPE);
            } catch (Exception unused) {
            }
        }
        return null;
    }

    public static final Method m() {
        if (jr2.i()) {
            try {
                return WorkSource.class.getMethod("createWorkChain", new Class[0]);
            } catch (Exception unused) {
            }
        }
        return null;
    }

    @SuppressLint({"PrivateApi"})
    public static final Method n() {
        if (jr2.i()) {
            try {
                return Class.forName("android.os.WorkSource$WorkChain").getMethod("addNode", Integer.TYPE, String.class);
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
