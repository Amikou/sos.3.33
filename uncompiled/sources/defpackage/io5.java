package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: io5  reason: default package */
/* loaded from: classes.dex */
public final class io5 extends wo5<Long> {
    public io5(ro5 ro5Var, String str, Long l, boolean z) {
        super(ro5Var, str, l, true, null);
    }

    @Override // defpackage.wo5
    public final /* bridge */ /* synthetic */ Long a(Object obj) {
        try {
            return Long.valueOf(Long.parseLong((String) obj));
        } catch (NumberFormatException unused) {
            String d = super.d();
            String str = (String) obj;
            StringBuilder sb = new StringBuilder(String.valueOf(d).length() + 25 + str.length());
            sb.append("Invalid long value for ");
            sb.append(d);
            sb.append(": ");
            sb.append(str);
            return null;
        }
    }
}
