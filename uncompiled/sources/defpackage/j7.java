package defpackage;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Insets;
import android.os.Build;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowMetrics;
import androidx.core.content.FileProvider;
import defpackage.jp4;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import kotlin.io.FilesKt__UtilsKt;

/* compiled from: Activity.kt */
/* renamed from: j7  reason: default package */
/* loaded from: classes2.dex */
public final class j7 {
    public static final Object a(Activity activity, String str, q70<? super URLConnection> q70Var) {
        if (str != null) {
            try {
            } catch (Exception unused) {
                return null;
            }
        }
        return new URL(str).openConnection();
    }

    public static final int b(Activity activity) {
        int i;
        int i2;
        fs1.f(activity, "<this>");
        if (Build.VERSION.SDK_INT >= 30) {
            WindowMetrics currentWindowMetrics = activity.getWindowManager().getCurrentWindowMetrics();
            fs1.e(currentWindowMetrics, "windowManager.currentWindowMetrics");
            Insets insets = currentWindowMetrics.getWindowInsets().getInsets(WindowInsets.Type.systemBars());
            fs1.e(insets, "metrics.windowInsets.get…Insets.Type.systemBars())");
            i = currentWindowMetrics.getBounds().height() - insets.bottom;
            i2 = insets.top;
        } else {
            View decorView = activity.getWindow().getDecorView();
            fs1.e(decorView, "window.decorView");
            cr1 f = jp4.y(decorView.getRootWindowInsets(), decorView).f(jp4.m.b());
            fs1.e(f, "toWindowInsetsCompat(vie…Compat.Type.systemBars())");
            i = activity.getResources().getDisplayMetrics().heightPixels - f.d;
            i2 = f.b;
        }
        return i - i2;
    }

    public static final int c(Activity activity) {
        int i;
        int i2;
        fs1.f(activity, "<this>");
        if (Build.VERSION.SDK_INT >= 30) {
            WindowMetrics currentWindowMetrics = activity.getWindowManager().getCurrentWindowMetrics();
            fs1.e(currentWindowMetrics, "windowManager.currentWindowMetrics");
            Insets insets = currentWindowMetrics.getWindowInsets().getInsets(WindowInsets.Type.systemBars());
            fs1.e(insets, "metrics.windowInsets.get…Insets.Type.systemBars())");
            i = currentWindowMetrics.getBounds().width() - insets.left;
            i2 = insets.right;
        } else {
            View decorView = activity.getWindow().getDecorView();
            fs1.e(decorView, "window.decorView");
            cr1 f = jp4.y(decorView.getRootWindowInsets(), decorView).f(jp4.m.b());
            fs1.e(f, "toWindowInsetsCompat(vie…Compat.Type.systemBars())");
            i = activity.getResources().getDisplayMetrics().widthPixels - f.a;
            i2 = f.c;
        }
        return i - i2;
    }

    public static final Object d(Activity activity, File file, String str, q70<? super File> q70Var) {
        File file2 = new File(activity.getCacheDir(), str);
        if (file2.exists() || FilesKt__UtilsKt.g(file, file2, true, null, 4, null)) {
            return file2;
        }
        return null;
    }

    public static final void e(Activity activity, File file, String str, String str2) {
        fs1.f(activity, "<this>");
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setFlags(1);
        if (str2 != null) {
            intent.setType(str2);
        }
        if (str != null) {
            if (file == null && str2 == null) {
                intent.setType("text/plain");
            }
            intent.putExtra("android.intent.extra.TEXT", str);
        }
        if (file != null) {
            intent.putExtra("android.intent.extra.STREAM", FileProvider.e(activity, fs1.l(activity.getPackageName(), ".provider"), file));
        }
        activity.startActivity(intent);
    }
}
