package defpackage;

import java.io.File;
import java.util.List;

/* compiled from: FilePathComponents.kt */
/* renamed from: p31  reason: default package */
/* loaded from: classes2.dex */
public final class p31 {
    public final File a;
    public final List<File> b;

    /* JADX WARN: Multi-variable type inference failed */
    public p31(File file, List<? extends File> list) {
        fs1.f(file, "root");
        fs1.f(list, "segments");
        this.a = file;
        this.b = list;
    }

    public final File a() {
        return this.a;
    }

    public final List<File> b() {
        return this.b;
    }

    public final int c() {
        return this.b.size();
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof p31) {
                p31 p31Var = (p31) obj;
                return fs1.b(this.a, p31Var.a) && fs1.b(this.b, p31Var.b);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        File file = this.a;
        int hashCode = (file != null ? file.hashCode() : 0) * 31;
        List<File> list = this.b;
        return hashCode + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        return "FilePathComponents(root=" + this.a + ", segments=" + this.b + ")";
    }
}
