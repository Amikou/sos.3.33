package defpackage;

import android.graphics.Bitmap;
import com.bumptech.glide.load.resource.bitmap.l;
import java.security.MessageDigest;

/* compiled from: CircleCrop.java */
/* renamed from: uy  reason: default package */
/* loaded from: classes.dex */
public class uy extends qq {
    public static final byte[] b = "com.bumptech.glide.load.resource.bitmap.CircleCrop.1".getBytes(fx1.a);

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        messageDigest.update(b);
    }

    @Override // defpackage.qq
    public Bitmap c(jq jqVar, Bitmap bitmap, int i, int i2) {
        return l.d(jqVar, bitmap, i, i2);
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        return obj instanceof uy;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return 1101716364;
    }
}
