package defpackage;

/* compiled from: ITokenExtraParams.kt */
/* renamed from: tm1  reason: default package */
/* loaded from: classes2.dex */
public final class tm1 {
    public boolean a;
    public String b = "";
    public boolean c = true;
    public boolean d = true;

    public final boolean a() {
        return this.d;
    }

    public final boolean b() {
        return this.c;
    }

    public final String c() {
        return this.b;
    }

    public final boolean d() {
        return this.a;
    }

    public final void e(boolean z) {
        this.a = z;
    }

    public final void f(boolean z) {
        this.d = z;
    }

    public final void g(boolean z) {
        this.c = z;
    }

    public final void h(String str) {
        fs1.f(str, "<set-?>");
        this.b = str;
    }
}
