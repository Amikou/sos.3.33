package defpackage;

/* compiled from: Doubles.java */
/* renamed from: kq0  reason: default package */
/* loaded from: classes2.dex */
public final class kq0 {
    public static int a(double d) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
    }
}
