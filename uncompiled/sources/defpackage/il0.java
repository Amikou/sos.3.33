package defpackage;

import androidx.media3.exoplayer.trackselection.b;
import java.util.Comparator;
import java.util.List;

/* renamed from: il0  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class il0 implements Comparator {
    public static final /* synthetic */ il0 a = new il0();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return b.i.j((List) obj, (List) obj2);
    }
}
