package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.collectible.RoomCollection;
import net.safemoon.androidwallet.model.collectible.RoomCollectionAndNft;

/* compiled from: CollectionDao.kt */
/* renamed from: j10  reason: default package */
/* loaded from: classes2.dex */
public interface j10 {
    Object a(long j, int i, q70<? super te4> q70Var);

    Object b(int i, q70<? super List<RoomCollection>> q70Var);

    Object c(RoomCollection roomCollection, q70<? super te4> q70Var);

    Object d(long j, int i, q70<? super te4> q70Var);

    Object e(RoomCollection roomCollection, q70<? super Long> q70Var);

    LiveData<List<RoomCollectionAndNft>> f(int i);
}
