package defpackage;

import com.facebook.common.internal.ImmutableMap;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: EncodedMemoryCacheProducer.java */
/* renamed from: bv0  reason: default package */
/* loaded from: classes.dex */
public class bv0 implements dv2<zu0> {
    public final l72<wt, PooledByteBuffer> a;
    public final xt b;
    public final dv2<zu0> c;

    /* compiled from: EncodedMemoryCacheProducer.java */
    /* renamed from: bv0$a */
    /* loaded from: classes.dex */
    public static class a extends bm0<zu0, zu0> {
        public final l72<wt, PooledByteBuffer> c;
        public final wt d;
        public final boolean e;
        public final boolean f;

        public a(l60<zu0> l60Var, l72<wt, PooledByteBuffer> l72Var, wt wtVar, boolean z, boolean z2) {
            super(l60Var);
            this.c = l72Var;
            this.d = wtVar;
            this.e = z;
            this.f = z2;
        }

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(zu0 zu0Var, int i) {
            boolean d;
            try {
                if (nc1.d()) {
                    nc1.a("EncodedMemoryCacheProducer#onNewResultImpl");
                }
                if (!qm.f(i) && zu0Var != null && !qm.m(i, 10) && zu0Var.l() != wn1.b) {
                    com.facebook.common.references.a<PooledByteBuffer> e = zu0Var.e();
                    if (e != null) {
                        com.facebook.common.references.a<PooledByteBuffer> aVar = null;
                        if (this.f && this.e) {
                            aVar = this.c.c(this.d, e);
                        }
                        com.facebook.common.references.a.g(e);
                        if (aVar != null) {
                            zu0 zu0Var2 = new zu0(aVar);
                            zu0Var2.d(zu0Var);
                            com.facebook.common.references.a.g(aVar);
                            p().c(1.0f);
                            p().d(zu0Var2, i);
                            zu0.c(zu0Var2);
                            if (d) {
                                return;
                            }
                            return;
                        }
                    }
                    p().d(zu0Var, i);
                    if (nc1.d()) {
                        nc1.b();
                        return;
                    }
                    return;
                }
                p().d(zu0Var, i);
                if (nc1.d()) {
                    nc1.b();
                }
            } finally {
                if (nc1.d()) {
                    nc1.b();
                }
            }
        }
    }

    public bv0(l72<wt, PooledByteBuffer> l72Var, xt xtVar, dv2<zu0> dv2Var) {
        this.a = l72Var;
        this.b = xtVar;
        this.c = dv2Var;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        try {
            if (nc1.d()) {
                nc1.a("EncodedMemoryCacheProducer#produceResults");
            }
            iv2 l = ev2Var.l();
            l.k(ev2Var, "EncodedMemoryCacheProducer");
            wt d = this.b.d(ev2Var.c(), ev2Var.a());
            com.facebook.common.references.a<PooledByteBuffer> aVar = ev2Var.c().x(4) ? this.a.get(d) : null;
            if (aVar != null) {
                zu0 zu0Var = new zu0(aVar);
                try {
                    l.a(ev2Var, "EncodedMemoryCacheProducer", l.j(ev2Var, "EncodedMemoryCacheProducer") ? ImmutableMap.of("cached_value_found", "true") : null);
                    l.e(ev2Var, "EncodedMemoryCacheProducer", true);
                    ev2Var.k("memory_encoded");
                    l60Var.c(1.0f);
                    l60Var.d(zu0Var, 1);
                    com.facebook.common.references.a.g(aVar);
                } finally {
                    zu0.c(zu0Var);
                }
            } else if (ev2Var.n().getValue() >= ImageRequest.RequestLevel.ENCODED_MEMORY_CACHE.getValue()) {
                l.a(ev2Var, "EncodedMemoryCacheProducer", l.j(ev2Var, "EncodedMemoryCacheProducer") ? ImmutableMap.of("cached_value_found", "false") : null);
                l.e(ev2Var, "EncodedMemoryCacheProducer", false);
                ev2Var.f("memory_encoded", "nil-result");
                l60Var.d(null, 1);
                com.facebook.common.references.a.g(aVar);
                if (nc1.d()) {
                    nc1.b();
                }
            } else {
                a aVar2 = new a(l60Var, this.a, d, ev2Var.c().x(8), ev2Var.d().C().r());
                l.a(ev2Var, "EncodedMemoryCacheProducer", l.j(ev2Var, "EncodedMemoryCacheProducer") ? ImmutableMap.of("cached_value_found", "false") : null);
                this.c.a(aVar2, ev2Var);
                com.facebook.common.references.a.g(aVar);
                if (nc1.d()) {
                    nc1.b();
                }
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }
}
