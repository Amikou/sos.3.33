package defpackage;

import java.util.List;
import java.util.RandomAccess;
import okio.ByteString;
import okio.b;

/* compiled from: Options.kt */
/* renamed from: un2  reason: default package */
/* loaded from: classes2.dex */
public final class un2 extends c5<ByteString> implements RandomAccess {
    public static final a h0 = new a(null);
    public final ByteString[] f0;
    public final int[] g0;

    /* compiled from: Options.kt */
    /* renamed from: un2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public static /* synthetic */ void b(a aVar, long j, b bVar, int i, List list, int i2, int i3, List list2, int i4, Object obj) {
            aVar.a((i4 & 1) != 0 ? 0L : j, bVar, (i4 & 4) != 0 ? 0 : i, list, (i4 & 16) != 0 ? 0 : i2, (i4 & 32) != 0 ? list.size() : i3, list2);
        }

        public final void a(long j, b bVar, int i, List<? extends ByteString> list, int i2, int i3, List<Integer> list2) {
            int i4;
            int i5;
            int i6;
            int i7;
            b bVar2;
            int i8 = i;
            if (i2 < i3) {
                for (int i9 = i2; i9 < i3; i9++) {
                    if (!(list.get(i9).size() >= i8)) {
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                }
                ByteString byteString = list.get(i2);
                ByteString byteString2 = list.get(i3 - 1);
                int i10 = -1;
                if (i8 == byteString.size()) {
                    int i11 = i2 + 1;
                    i4 = i11;
                    i5 = list2.get(i2).intValue();
                    byteString = list.get(i11);
                } else {
                    i4 = i2;
                    i5 = -1;
                }
                if (byteString.getByte(i8) != byteString2.getByte(i8)) {
                    int i12 = 1;
                    for (int i13 = i4 + 1; i13 < i3; i13++) {
                        if (list.get(i13 - 1).getByte(i8) != list.get(i13).getByte(i8)) {
                            i12++;
                        }
                    }
                    long c = j + c(bVar) + 2 + (i12 * 2);
                    bVar.V(i12);
                    bVar.V(i5);
                    for (int i14 = i4; i14 < i3; i14++) {
                        byte b = list.get(i14).getByte(i8);
                        if (i14 == i4 || b != list.get(i14 - 1).getByte(i8)) {
                            bVar.V(b & 255);
                        }
                    }
                    b bVar3 = new b();
                    while (i4 < i3) {
                        byte b2 = list.get(i4).getByte(i8);
                        int i15 = i4 + 1;
                        int i16 = i15;
                        while (true) {
                            if (i16 >= i3) {
                                i6 = i3;
                                break;
                            } else if (b2 != list.get(i16).getByte(i8)) {
                                i6 = i16;
                                break;
                            } else {
                                i16++;
                            }
                        }
                        if (i15 == i6 && i8 + 1 == list.get(i4).size()) {
                            bVar.V(list2.get(i4).intValue());
                            i7 = i6;
                            bVar2 = bVar3;
                        } else {
                            bVar.V(((int) (c + c(bVar3))) * i10);
                            i7 = i6;
                            bVar2 = bVar3;
                            a(c, bVar3, i8 + 1, list, i4, i6, list2);
                        }
                        bVar3 = bVar2;
                        i4 = i7;
                        i10 = -1;
                    }
                    bVar.P0(bVar3);
                    return;
                }
                int min = Math.min(byteString.size(), byteString2.size());
                int i17 = 0;
                for (int i18 = i8; i18 < min && byteString.getByte(i18) == byteString2.getByte(i18); i18++) {
                    i17++;
                }
                long c2 = j + c(bVar) + 2 + i17 + 1;
                bVar.V(-i17);
                bVar.V(i5);
                int i19 = i8 + i17;
                while (i8 < i19) {
                    bVar.V(byteString.getByte(i8) & 255);
                    i8++;
                }
                if (i4 + 1 == i3) {
                    if (i19 == list.get(i4).size()) {
                        bVar.V(list2.get(i4).intValue());
                        return;
                    }
                    throw new IllegalStateException("Check failed.".toString());
                }
                b bVar4 = new b();
                bVar.V(((int) (c(bVar4) + c2)) * (-1));
                a(c2, bVar4, i19, list, i4, i3, list2);
                bVar.P0(bVar4);
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        public final long c(b bVar) {
            return bVar.a0() / 4;
        }

        /* JADX WARN: Code restructure failed: missing block: B:55:0x00f1, code lost:
            continue;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final defpackage.un2 d(okio.ByteString... r17) {
            /*
                Method dump skipped, instructions count: 328
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.un2.a.d(okio.ByteString[]):un2");
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public /* synthetic */ un2(ByteString[] byteStringArr, int[] iArr, qi0 qi0Var) {
        this(byteStringArr, iArr);
    }

    @Override // kotlin.collections.AbstractCollection, java.util.Collection
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof ByteString) {
            return i((ByteString) obj);
        }
        return false;
    }

    @Override // kotlin.collections.AbstractCollection
    public int e() {
        return this.f0.length;
    }

    public /* bridge */ boolean i(ByteString byteString) {
        return super.contains(byteString);
    }

    @Override // defpackage.c5, java.util.List
    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof ByteString) {
            return o((ByteString) obj);
        }
        return -1;
    }

    @Override // defpackage.c5, java.util.List
    /* renamed from: k */
    public ByteString get(int i) {
        return this.f0[i];
    }

    @Override // defpackage.c5, java.util.List
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof ByteString) {
            return p((ByteString) obj);
        }
        return -1;
    }

    public final ByteString[] m() {
        return this.f0;
    }

    public final int[] n() {
        return this.g0;
    }

    public /* bridge */ int o(ByteString byteString) {
        return super.indexOf(byteString);
    }

    public /* bridge */ int p(ByteString byteString) {
        return super.lastIndexOf(byteString);
    }

    public un2(ByteString[] byteStringArr, int[] iArr) {
        this.f0 = byteStringArr;
        this.g0 = iArr;
    }
}
