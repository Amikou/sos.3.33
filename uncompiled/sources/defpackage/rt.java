package defpackage;

import org.web3j.abi.datatypes.Address;

/* compiled from: PreFetchData.kt */
/* renamed from: rt  reason: default package */
/* loaded from: classes2.dex */
public final class rt {
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final int e;
    public final int f;
    public double g;
    public final boolean h;

    public rt(String str, String str2, String str3, String str4, int i, int i2, double d, boolean z) {
        fs1.f(str, Address.TYPE_NAME);
        fs1.f(str2, "cmcId");
        fs1.f(str3, "cmcSlug");
        fs1.f(str4, "symbol");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = i;
        this.f = i2;
        this.g = d;
        this.h = z;
    }

    public final String a() {
        return this.a;
    }

    public final int b() {
        return this.f;
    }

    public final String c() {
        return this.b;
    }

    public final String d() {
        return this.c;
    }

    public final int e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof rt) {
            rt rtVar = (rt) obj;
            return fs1.b(this.a, rtVar.a) && fs1.b(this.b, rtVar.b) && fs1.b(this.c, rtVar.c) && fs1.b(this.d, rtVar.d) && this.e == rtVar.e && this.f == rtVar.f && fs1.b(Double.valueOf(this.g), Double.valueOf(rtVar.g)) && this.h == rtVar.h;
        }
        return false;
    }

    public final boolean f() {
        return this.h;
    }

    public final double g() {
        return this.g;
    }

    public final String h() {
        return this.d;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = ((((((((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f) * 31) + Double.doubleToLongBits(this.g)) * 31;
        boolean z = this.h;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return hashCode + i;
    }

    public final void i(double d) {
        this.g = d;
    }

    public String toString() {
        return "CMCToken(address=" + this.a + ", cmcId=" + this.b + ", cmcSlug=" + this.c + ", symbol=" + this.d + ", decimal=" + this.e + ", chainId=" + this.f + ", nativeBalance=" + this.g + ", hasSwap=" + this.h + ')';
    }

    public /* synthetic */ rt(String str, String str2, String str3, String str4, int i, int i2, double d, boolean z, int i3, qi0 qi0Var) {
        this(str, str2, str3, str4, i, i2, (i3 & 64) != 0 ? 0.0d : d, z);
    }
}
