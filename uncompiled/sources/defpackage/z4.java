package defpackage;

import java.util.NoSuchElementException;

/* compiled from: AbstractIndexedListIterator.java */
/* renamed from: z4  reason: default package */
/* loaded from: classes2.dex */
public abstract class z4<E> extends df4<E> {
    public final int a;
    public int f0;

    public z4(int i, int i2) {
        au2.m(i2, i);
        this.a = i;
        this.f0 = i2;
    }

    public abstract E a(int i);

    @Override // java.util.Iterator, java.util.ListIterator
    public final boolean hasNext() {
        return this.f0 < this.a;
    }

    @Override // java.util.ListIterator
    public final boolean hasPrevious() {
        return this.f0 > 0;
    }

    @Override // java.util.Iterator, java.util.ListIterator
    public final E next() {
        if (hasNext()) {
            int i = this.f0;
            this.f0 = i + 1;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.ListIterator
    public final int nextIndex() {
        return this.f0;
    }

    @Override // java.util.ListIterator
    public final E previous() {
        if (hasPrevious()) {
            int i = this.f0 - 1;
            this.f0 = i;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.ListIterator
    public final int previousIndex() {
        return this.f0 - 1;
    }
}
