package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.Barrier;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: ViewSwapCardBinding.java */
/* renamed from: ck4  reason: default package */
/* loaded from: classes2.dex */
public final class ck4 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final MaterialButton f;
    public final AppCompatTextView g;
    public final AppCompatTextView h;
    public final ShapeableImageView i;
    public final AutofitEdittext j;
    public final RelativeLayout k;
    public final MaterialTextView l;
    public final AppCompatTextView m;
    public final MaterialTextView n;

    public ck4(MaterialCardView materialCardView, Barrier barrier, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, MaterialButton materialButton5, MaterialButton materialButton6, AppCompatTextView appCompatTextView, FrameLayout frameLayout, AppCompatTextView appCompatTextView2, ShapeableImageView shapeableImageView, AutofitEdittext autofitEdittext, RelativeLayout relativeLayout, MaterialCardView materialCardView2, MaterialTextView materialTextView, AppCompatTextView appCompatTextView3, MaterialTextView materialTextView2) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = materialButton3;
        this.d = materialButton4;
        this.e = materialButton5;
        this.f = materialButton6;
        this.g = appCompatTextView;
        this.h = appCompatTextView2;
        this.i = shapeableImageView;
        this.j = autofitEdittext;
        this.k = relativeLayout;
        this.l = materialTextView;
        this.m = appCompatTextView3;
        this.n = materialTextView2;
    }

    public static ck4 a(View view) {
        int i = R.id.barrierForNewValue;
        Barrier barrier = (Barrier) ai4.a(view, R.id.barrierForNewValue);
        if (barrier != null) {
            i = R.id.btnCC25;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnCC25);
            if (materialButton != null) {
                i = R.id.btnCC50;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnCC50);
                if (materialButton2 != null) {
                    i = R.id.btnCC75;
                    MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnCC75);
                    if (materialButton3 != null) {
                        i = R.id.btnCCMax;
                        MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.btnCCMax);
                        if (materialButton4 != null) {
                            i = R.id.btnClearText;
                            MaterialButton materialButton5 = (MaterialButton) ai4.a(view, R.id.btnClearText);
                            if (materialButton5 != null) {
                                i = R.id.btnSelectCurrency;
                                MaterialButton materialButton6 = (MaterialButton) ai4.a(view, R.id.btnSelectCurrency);
                                if (materialButton6 != null) {
                                    i = R.id.ccBalance;
                                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.ccBalance);
                                    if (appCompatTextView != null) {
                                        i = R.id.ccBalanceParent;
                                        FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.ccBalanceParent);
                                        if (frameLayout != null) {
                                            i = R.id.hintNewValue;
                                            AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.hintNewValue);
                                            if (appCompatTextView2 != null) {
                                                i = R.id.imgCoin;
                                                ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgCoin);
                                                if (shapeableImageView != null) {
                                                    i = R.id.newValue;
                                                    AutofitEdittext autofitEdittext = (AutofitEdittext) ai4.a(view, R.id.newValue);
                                                    if (autofitEdittext != null) {
                                                        i = R.id.newValueParent;
                                                        RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.newValueParent);
                                                        if (relativeLayout != null) {
                                                            MaterialCardView materialCardView = (MaterialCardView) view;
                                                            i = R.id.txtCurrentPrice;
                                                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtCurrentPrice);
                                                            if (materialTextView != null) {
                                                                i = R.id.txtLoader;
                                                                AppCompatTextView appCompatTextView3 = (AppCompatTextView) ai4.a(view, R.id.txtLoader);
                                                                if (appCompatTextView3 != null) {
                                                                    i = R.id.txtName;
                                                                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtName);
                                                                    if (materialTextView2 != null) {
                                                                        return new ck4(materialCardView, barrier, materialButton, materialButton2, materialButton3, materialButton4, materialButton5, materialButton6, appCompatTextView, frameLayout, appCompatTextView2, shapeableImageView, autofitEdittext, relativeLayout, materialCardView, materialTextView, appCompatTextView3, materialTextView2);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
