package defpackage;

import com.google.protobuf.r0;
import com.google.protobuf.s0;

/* compiled from: NewInstanceSchemas.java */
/* renamed from: kf2  reason: default package */
/* loaded from: classes2.dex */
public final class kf2 {
    public static final if2 a = c();
    public static final if2 b = new s0();

    public static if2 a() {
        return a;
    }

    public static if2 b() {
        return b;
    }

    public static if2 c() {
        try {
            return (if2) r0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
