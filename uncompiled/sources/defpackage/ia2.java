package defpackage;

import androidx.media3.common.util.b;
import androidx.media3.extractor.text.SubtitleDecoderException;
import androidx.media3.extractor.text.a;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.Collections;

/* compiled from: Mp4WebvttDecoder.java */
/* renamed from: ia2  reason: default package */
/* loaded from: classes.dex */
public final class ia2 extends a {
    public final op2 n;

    public ia2() {
        super("Mp4WebvttDecoder");
        this.n = new op2();
    }

    public static kb0 C(op2 op2Var, int i) throws SubtitleDecoderException {
        CharSequence charSequence = null;
        kb0.b bVar = null;
        while (i > 0) {
            if (i >= 8) {
                int n = op2Var.n();
                int n2 = op2Var.n();
                int i2 = n - 8;
                String C = b.C(op2Var.d(), op2Var.e(), i2);
                op2Var.Q(i2);
                i = (i - 8) - i2;
                if (n2 == 1937011815) {
                    bVar = uo4.o(C);
                } else if (n2 == 1885436268) {
                    charSequence = uo4.q(null, C.trim(), Collections.emptyList());
                }
            } else {
                throw new SubtitleDecoderException("Incomplete vtt cue box header found.");
            }
        }
        if (charSequence == null) {
            charSequence = "";
        }
        if (bVar != null) {
            return bVar.o(charSequence).a();
        }
        return uo4.l(charSequence);
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.n.N(bArr, i);
        ArrayList arrayList = new ArrayList();
        while (this.n.a() > 0) {
            if (this.n.a() >= 8) {
                int n = this.n.n();
                if (this.n.n() == 1987343459) {
                    arrayList.add(C(this.n, n - 8));
                } else {
                    this.n.Q(n - 8);
                }
            } else {
                throw new SubtitleDecoderException("Incomplete Mp4Webvtt Top Level box header found.");
            }
        }
        return new ja2(arrayList);
    }
}
