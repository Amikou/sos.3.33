package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: RoundedCornerTreatment.java */
/* renamed from: v93  reason: default package */
/* loaded from: classes2.dex */
public class v93 extends w80 {
    public float a = -1.0f;

    @Override // defpackage.w80
    public void a(rn3 rn3Var, float f, float f2, float f3) {
        rn3Var.o(Utils.FLOAT_EPSILON, f3 * f2, 180.0f, 180.0f - f);
        float f4 = f3 * 2.0f * f2;
        rn3Var.a(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, f4, f4, 180.0f, f);
    }
}
