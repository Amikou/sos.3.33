package defpackage;

import java.util.concurrent.ThreadFactory;

/* renamed from: zx4  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class zx4 implements ThreadFactory {
    public final /* synthetic */ int a = 0;
    public static final ThreadFactory g0 = new zx4(null);
    public static final ThreadFactory f0 = new zx4();

    public zx4() {
    }

    public zx4(byte[] bArr) {
    }

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        return this.a != 0 ? new Thread(runnable, "AssetPackBackgroundExecutor") : new Thread(runnable, "UpdateListenerExecutor");
    }
}
