package defpackage;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.widget.ScrollingTabContainerView;

/* compiled from: DecorToolbar.java */
/* renamed from: lf0  reason: default package */
/* loaded from: classes.dex */
public interface lf0 {
    boolean a();

    boolean b();

    boolean c();

    void collapseActionView();

    boolean d();

    boolean e();

    void f();

    void g(ScrollingTabContainerView scrollingTabContainerView);

    Context getContext();

    CharSequence getTitle();

    boolean h();

    void i(int i);

    Menu j();

    void k(int i);

    int l();

    vj4 m(int i, long j);

    void n(i.a aVar, e.a aVar2);

    ViewGroup o();

    void p(boolean z);

    int q();

    void r(int i);

    void s();

    void setIcon(int i);

    void setIcon(Drawable drawable);

    void setMenu(Menu menu, i.a aVar);

    void setMenuPrepared();

    void setTitle(CharSequence charSequence);

    void setVisibility(int i);

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);

    void t();

    void u(Drawable drawable);

    void v(boolean z);
}
