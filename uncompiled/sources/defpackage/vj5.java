package defpackage;

import com.google.android.gms.measurement.internal.h;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: vj5  reason: default package */
/* loaded from: classes.dex */
public final class vj5<V> extends FutureTask<V> implements Comparable<vj5<V>> {
    public final long a;
    public final boolean f0;
    public final String g0;
    public final /* synthetic */ h h0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public vj5(h hVar, Runnable runnable, boolean z, String str) {
        super(runnable, null);
        AtomicLong atomicLong;
        this.h0 = hVar;
        zt2.j(str);
        atomicLong = h.l;
        long andIncrement = atomicLong.getAndIncrement();
        this.a = andIncrement;
        this.g0 = str;
        this.f0 = z;
        if (andIncrement == Long.MAX_VALUE) {
            hVar.a.w().l().a("Tasks index overflow");
        }
    }

    @Override // java.lang.Comparable
    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        vj5 vj5Var = (vj5) obj;
        boolean z = this.f0;
        if (z != vj5Var.f0) {
            return !z ? 1 : -1;
        }
        int i = (this.a > vj5Var.a ? 1 : (this.a == vj5Var.a ? 0 : -1));
        if (i < 0) {
            return -1;
        }
        if (i > 0) {
            return 1;
        }
        this.h0.a.w().n().b("Two tasks share the same index. index", Long.valueOf(this.a));
        return 0;
    }

    @Override // java.util.concurrent.FutureTask
    public final void setException(Throwable th) {
        this.h0.a.w().l().b(this.g0, th);
        super.setException(th);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public vj5(h hVar, Callable<V> callable, boolean z, String str) {
        super(callable);
        AtomicLong atomicLong;
        this.h0 = hVar;
        zt2.j("Task exception on worker thread");
        atomicLong = h.l;
        long andIncrement = atomicLong.getAndIncrement();
        this.a = andIncrement;
        this.g0 = "Task exception on worker thread";
        this.f0 = z;
        if (andIncrement == Long.MAX_VALUE) {
            hVar.a.w().l().a("Tasks index overflow");
        }
    }
}
