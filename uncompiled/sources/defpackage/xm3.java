package defpackage;

import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: SettableProducerContext.java */
/* renamed from: xm3  reason: default package */
/* loaded from: classes.dex */
public class xm3 extends un {
    public xm3(ImageRequest imageRequest, ev2 ev2Var) {
        this(imageRequest, ev2Var.getId(), ev2Var.j(), ev2Var.l(), ev2Var.a(), ev2Var.n(), ev2Var.h(), ev2Var.m(), ev2Var.getPriority(), ev2Var.d());
    }

    public xm3(ImageRequest imageRequest, String str, String str2, iv2 iv2Var, Object obj, ImageRequest.RequestLevel requestLevel, boolean z, boolean z2, Priority priority, so1 so1Var) {
        super(imageRequest, str, str2, iv2Var, obj, requestLevel, z, z2, priority, so1Var);
    }
}
