package defpackage;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.JobCancellationException;

/* compiled from: ChannelCoroutine.kt */
/* renamed from: mx  reason: default package */
/* loaded from: classes2.dex */
public class mx<E> extends t4<te4> implements kx<E> {
    public final kx<E> g0;

    public mx(CoroutineContext coroutineContext, kx<E> kxVar, boolean z, boolean z2) {
        super(coroutineContext, z, z2);
        this.g0 = kxVar;
    }

    @Override // defpackage.bu1
    public void K(Throwable th) {
        CancellationException A0 = bu1.A0(this, th, null, 1, null);
        this.g0.a(A0);
        I(A0);
    }

    public final kx<E> L0() {
        return this.g0;
    }

    @Override // defpackage.bu1, defpackage.st1
    public final void a(CancellationException cancellationException) {
        if (isCancelled()) {
            return;
        }
        if (cancellationException == null) {
            cancellationException = new JobCancellationException(N(), null, this);
        }
        K(cancellationException);
    }

    @Override // defpackage.f43
    public Object e(q70<? super tx<? extends E>> q70Var) {
        Object e = this.g0.e(q70Var);
        gs1.d();
        return e;
    }

    @Override // defpackage.uj3
    public Object h(E e, q70<? super te4> q70Var) {
        return this.g0.h(e, q70Var);
    }

    @Override // defpackage.f43
    public qx<E> iterator() {
        return this.g0.iterator();
    }

    @Override // defpackage.uj3
    public boolean l(Throwable th) {
        return this.g0.l(th);
    }

    @Override // defpackage.uj3
    public boolean offer(E e) {
        return this.g0.offer(e);
    }

    @Override // defpackage.uj3
    public Object p(E e) {
        return this.g0.p(e);
    }
}
