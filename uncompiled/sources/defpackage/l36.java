package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.measurement.internal.zzp;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: l36  reason: default package */
/* loaded from: classes.dex */
public final class l36 implements Parcelable.Creator<zzp> {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzp createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        boolean z = true;
        boolean z2 = true;
        boolean z3 = false;
        int i = 0;
        boolean z4 = false;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        Boolean bool = null;
        ArrayList<String> arrayList = null;
        String str8 = null;
        String str9 = "";
        long j6 = -2147483648L;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 3:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 4:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 5:
                    str4 = SafeParcelReader.p(parcel, C);
                    break;
                case 6:
                    j = SafeParcelReader.F(parcel, C);
                    break;
                case 7:
                    j2 = SafeParcelReader.F(parcel, C);
                    break;
                case 8:
                    str5 = SafeParcelReader.p(parcel, C);
                    break;
                case 9:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 10:
                    z3 = SafeParcelReader.w(parcel, C);
                    break;
                case 11:
                    j6 = SafeParcelReader.F(parcel, C);
                    break;
                case 12:
                    str6 = SafeParcelReader.p(parcel, C);
                    break;
                case 13:
                    j3 = SafeParcelReader.F(parcel, C);
                    break;
                case 14:
                    j4 = SafeParcelReader.F(parcel, C);
                    break;
                case 15:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 16:
                    z2 = SafeParcelReader.w(parcel, C);
                    break;
                case 17:
                case 20:
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
                case 18:
                    z4 = SafeParcelReader.w(parcel, C);
                    break;
                case 19:
                    str7 = SafeParcelReader.p(parcel, C);
                    break;
                case 21:
                    bool = SafeParcelReader.x(parcel, C);
                    break;
                case 22:
                    j5 = SafeParcelReader.F(parcel, C);
                    break;
                case 23:
                    arrayList = SafeParcelReader.r(parcel, C);
                    break;
                case 24:
                    str8 = SafeParcelReader.p(parcel, C);
                    break;
                case 25:
                    str9 = SafeParcelReader.p(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzp(str, str2, str3, str4, j, j2, str5, z, z3, j6, str6, j3, j4, i, z2, z4, str7, bool, j5, arrayList, str8, str9);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzp[] newArray(int i) {
        return new zzp[i];
    }
}
