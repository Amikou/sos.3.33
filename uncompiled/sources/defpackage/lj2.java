package defpackage;

import com.onesignal.influence.domain.OSInfluenceChannel;

/* compiled from: OSCachedUniqueOutcome.kt */
/* renamed from: lj2  reason: default package */
/* loaded from: classes2.dex */
public class lj2 {
    public final String a;
    public final OSInfluenceChannel b;

    public lj2(String str, OSInfluenceChannel oSInfluenceChannel) {
        fs1.f(str, "influenceId");
        fs1.f(oSInfluenceChannel, "channel");
        this.a = str;
        this.b = oSInfluenceChannel;
    }

    public OSInfluenceChannel a() {
        return this.b;
    }

    public String b() {
        return this.a;
    }
}
