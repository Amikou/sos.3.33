package defpackage;

import com.google.android.datatransport.Priority;
import com.google.android.datatransport.a;
import java.util.Objects;

/* compiled from: AutoValue_Event.java */
/* renamed from: il  reason: default package */
/* loaded from: classes.dex */
public final class il<T> extends a<T> {
    public final Integer a;
    public final T b;
    public final Priority c;

    public il(Integer num, T t, Priority priority) {
        this.a = num;
        Objects.requireNonNull(t, "Null payload");
        this.b = t;
        Objects.requireNonNull(priority, "Null priority");
        this.c = priority;
    }

    @Override // com.google.android.datatransport.a
    public Integer a() {
        return this.a;
    }

    @Override // com.google.android.datatransport.a
    public T b() {
        return this.b;
    }

    @Override // com.google.android.datatransport.a
    public Priority c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof a) {
            a aVar = (a) obj;
            Integer num = this.a;
            if (num != null ? num.equals(aVar.a()) : aVar.a() == null) {
                if (this.b.equals(aVar.b()) && this.c.equals(aVar.c())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        Integer num = this.a;
        return (((((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    public String toString() {
        return "Event{code=" + this.a + ", payload=" + this.b + ", priority=" + this.c + "}";
    }
}
