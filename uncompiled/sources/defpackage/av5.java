package defpackage;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: av5  reason: default package */
/* loaded from: classes.dex */
public final class av5 implements aw5 {
    @Override // defpackage.aw5
    public final boolean a(Class<?> cls) {
        return false;
    }

    @Override // defpackage.aw5
    public final tv5 b(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
