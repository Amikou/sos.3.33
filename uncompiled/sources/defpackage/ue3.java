package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: ue3  reason: default package */
/* loaded from: classes2.dex */
public class ue3 extends ct0.b {
    public static final BigInteger g = se3.j;
    public int[] f;

    public ue3() {
        this.f = cd2.g();
    }

    public ue3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP192R1FieldElement");
        }
        this.f = te3.d(bigInteger);
    }

    public ue3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] g2 = cd2.g();
        te3.a(this.f, ((ue3) ct0Var).f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] g2 = cd2.g();
        te3.b(this.f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] g2 = cd2.g();
        g92.d(te3.a, ((ue3) ct0Var).f, g2);
        te3.e(g2, this.f, g2);
        return new ue3(g2);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ue3) {
            return cd2.l(this.f, ((ue3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] g2 = cd2.g();
        g92.d(te3.a, this.f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return cd2.s(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 6);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return cd2.u(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] g2 = cd2.g();
        te3.e(this.f, ((ue3) ct0Var).f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] g2 = cd2.g();
        te3.g(this.f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (cd2.u(iArr) || cd2.s(iArr)) {
            return this;
        }
        int[] g2 = cd2.g();
        int[] g3 = cd2.g();
        te3.j(iArr, g2);
        te3.e(g2, iArr, g2);
        te3.k(g2, 2, g3);
        te3.e(g3, g2, g3);
        te3.k(g3, 4, g2);
        te3.e(g2, g3, g2);
        te3.k(g2, 8, g3);
        te3.e(g3, g2, g3);
        te3.k(g3, 16, g2);
        te3.e(g2, g3, g2);
        te3.k(g2, 32, g3);
        te3.e(g3, g2, g3);
        te3.k(g3, 64, g2);
        te3.e(g2, g3, g2);
        te3.k(g2, 62, g2);
        te3.j(g2, g3);
        if (cd2.l(iArr, g3)) {
            return new ue3(g2);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] g2 = cd2.g();
        te3.j(this.f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] g2 = cd2.g();
        te3.m(this.f, ((ue3) ct0Var).f, g2);
        return new ue3(g2);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return cd2.p(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return cd2.H(this.f);
    }
}
