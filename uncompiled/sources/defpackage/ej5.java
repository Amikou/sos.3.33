package defpackage;

import com.google.android.gms.internal.measurement.zzfi;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ej5  reason: default package */
/* loaded from: classes.dex */
public final class ej5 implements sv5 {
    public static final sv5 a = new ej5();

    @Override // defpackage.sv5
    public final boolean d(int i) {
        return zzfi.zza(i) != null;
    }
}
