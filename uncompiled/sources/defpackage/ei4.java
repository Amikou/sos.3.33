package defpackage;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.ContentInfo;
import android.view.Display;
import android.view.KeyEvent;
import android.view.OnReceiveContentListener;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeProvider;
import com.github.mikephil.charting.utils.Utils;
import defpackage.b6;
import defpackage.ei4;
import defpackage.jp4;
import defpackage.z5;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ViewCompat.java */
@SuppressLint({"PrivateConstructorForUtilityClass"})
/* renamed from: ei4  reason: default package */
/* loaded from: classes.dex */
public class ei4 {
    public static Field b;
    public static boolean c;
    public static Field d;
    public static boolean e;
    public static WeakHashMap<View, String> f;
    public static Field h;
    public static ThreadLocal<Rect> j;
    public static final AtomicInteger a = new AtomicInteger(1);
    public static WeakHashMap<View, vj4> g = null;
    public static boolean i = false;
    public static final int[] k = {j03.accessibility_custom_action_0, j03.accessibility_custom_action_1, j03.accessibility_custom_action_2, j03.accessibility_custom_action_3, j03.accessibility_custom_action_4, j03.accessibility_custom_action_5, j03.accessibility_custom_action_6, j03.accessibility_custom_action_7, j03.accessibility_custom_action_8, j03.accessibility_custom_action_9, j03.accessibility_custom_action_10, j03.accessibility_custom_action_11, j03.accessibility_custom_action_12, j03.accessibility_custom_action_13, j03.accessibility_custom_action_14, j03.accessibility_custom_action_15, j03.accessibility_custom_action_16, j03.accessibility_custom_action_17, j03.accessibility_custom_action_18, j03.accessibility_custom_action_19, j03.accessibility_custom_action_20, j03.accessibility_custom_action_21, j03.accessibility_custom_action_22, j03.accessibility_custom_action_23, j03.accessibility_custom_action_24, j03.accessibility_custom_action_25, j03.accessibility_custom_action_26, j03.accessibility_custom_action_27, j03.accessibility_custom_action_28, j03.accessibility_custom_action_29, j03.accessibility_custom_action_30, j03.accessibility_custom_action_31};
    public static final rm2 l = di4.a;

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$a */
    /* loaded from: classes.dex */
    public class a extends f<Boolean> {
        public a(int i, Class cls, int i2) {
            super(i, cls, i2);
        }

        @Override // defpackage.ei4.f
        /* renamed from: i */
        public Boolean d(View view) {
            return Boolean.valueOf(q.d(view));
        }

        @Override // defpackage.ei4.f
        /* renamed from: j */
        public void e(View view, Boolean bool) {
            q.i(view, bool.booleanValue());
        }

        @Override // defpackage.ei4.f
        /* renamed from: k */
        public boolean h(Boolean bool, Boolean bool2) {
            return !a(bool, bool2);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$b */
    /* loaded from: classes.dex */
    public class b extends f<CharSequence> {
        public b(int i, Class cls, int i2, int i3) {
            super(i, cls, i2, i3);
        }

        @Override // defpackage.ei4.f
        /* renamed from: i */
        public CharSequence d(View view) {
            return q.b(view);
        }

        @Override // defpackage.ei4.f
        /* renamed from: j */
        public void e(View view, CharSequence charSequence) {
            q.h(view, charSequence);
        }

        @Override // defpackage.ei4.f
        /* renamed from: k */
        public boolean h(CharSequence charSequence, CharSequence charSequence2) {
            return !TextUtils.equals(charSequence, charSequence2);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$c */
    /* loaded from: classes.dex */
    public class c extends f<CharSequence> {
        public c(int i, Class cls, int i2, int i3) {
            super(i, cls, i2, i3);
        }

        @Override // defpackage.ei4.f
        /* renamed from: i */
        public CharSequence d(View view) {
            return s.a(view);
        }

        @Override // defpackage.ei4.f
        /* renamed from: j */
        public void e(View view, CharSequence charSequence) {
            s.b(view, charSequence);
        }

        @Override // defpackage.ei4.f
        /* renamed from: k */
        public boolean h(CharSequence charSequence, CharSequence charSequence2) {
            return !TextUtils.equals(charSequence, charSequence2);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$d */
    /* loaded from: classes.dex */
    public class d extends f<Boolean> {
        public d(int i, Class cls, int i2) {
            super(i, cls, i2);
        }

        @Override // defpackage.ei4.f
        /* renamed from: i */
        public Boolean d(View view) {
            return Boolean.valueOf(q.c(view));
        }

        @Override // defpackage.ei4.f
        /* renamed from: j */
        public void e(View view, Boolean bool) {
            q.g(view, bool.booleanValue());
        }

        @Override // defpackage.ei4.f
        /* renamed from: k */
        public boolean h(Boolean bool, Boolean bool2) {
            return !a(bool, bool2);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$e */
    /* loaded from: classes.dex */
    public static class e implements ViewTreeObserver.OnGlobalLayoutListener, View.OnAttachStateChangeListener {
        public final WeakHashMap<View, Boolean> a = new WeakHashMap<>();

        public final void a(View view, boolean z) {
            boolean z2 = view.getVisibility() == 0;
            if (z != z2) {
                ei4.b0(view, z2 ? 16 : 32);
                this.a.put(view, Boolean.valueOf(z2));
            }
        }

        public final void b(View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
        public void onGlobalLayout() {
            if (Build.VERSION.SDK_INT < 28) {
                for (Map.Entry<View, Boolean> entry : this.a.entrySet()) {
                    a(entry.getKey(), entry.getValue().booleanValue());
                }
            }
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
            b(view);
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$f */
    /* loaded from: classes.dex */
    public static abstract class f<T> {
        public final int a;
        public final Class<T> b;
        public final int c;
        public final int d;

        public f(int i, Class<T> cls, int i2) {
            this(i, cls, 0, i2);
        }

        public boolean a(Boolean bool, Boolean bool2) {
            return (bool != null && bool.booleanValue()) == (bool2 != null && bool2.booleanValue());
        }

        public final boolean b() {
            return Build.VERSION.SDK_INT >= 19;
        }

        public final boolean c() {
            return Build.VERSION.SDK_INT >= this.c;
        }

        public abstract T d(View view);

        public abstract void e(View view, T t);

        public T f(View view) {
            if (c()) {
                return d(view);
            }
            if (b()) {
                T t = (T) view.getTag(this.a);
                if (this.b.isInstance(t)) {
                    return t;
                }
                return null;
            }
            return null;
        }

        public void g(View view, T t) {
            if (c()) {
                e(view, t);
            } else if (b() && h(f(view), t)) {
                ei4.l(view);
                view.setTag(this.a, t);
                ei4.b0(view, this.d);
            }
        }

        public abstract boolean h(T t, T t2);

        public f(int i, Class<T> cls, int i2, int i3) {
            this.a = i;
            this.b = cls;
            this.d = i2;
            this.c = i3;
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$g */
    /* loaded from: classes.dex */
    public static class g {
        public static boolean a(View view) {
            return view.hasOnClickListeners();
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$h */
    /* loaded from: classes.dex */
    public static class h {
        public static AccessibilityNodeProvider a(View view) {
            return view.getAccessibilityNodeProvider();
        }

        public static boolean b(View view) {
            return view.getFitsSystemWindows();
        }

        public static int c(View view) {
            return view.getImportantForAccessibility();
        }

        public static int d(View view) {
            return view.getMinimumHeight();
        }

        public static int e(View view) {
            return view.getMinimumWidth();
        }

        public static ViewParent f(View view) {
            return view.getParentForAccessibility();
        }

        public static int g(View view) {
            return view.getWindowSystemUiVisibility();
        }

        public static boolean h(View view) {
            return view.hasOverlappingRendering();
        }

        public static boolean i(View view) {
            return view.hasTransientState();
        }

        public static boolean j(View view, int i, Bundle bundle) {
            return view.performAccessibilityAction(i, bundle);
        }

        public static void k(View view) {
            view.postInvalidateOnAnimation();
        }

        public static void l(View view, int i, int i2, int i3, int i4) {
            view.postInvalidateOnAnimation(i, i2, i3, i4);
        }

        public static void m(View view, Runnable runnable) {
            view.postOnAnimation(runnable);
        }

        public static void n(View view, Runnable runnable, long j) {
            view.postOnAnimationDelayed(runnable, j);
        }

        public static void o(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener);
        }

        public static void p(View view) {
            view.requestFitSystemWindows();
        }

        public static void q(View view, Drawable drawable) {
            view.setBackground(drawable);
        }

        public static void r(View view, boolean z) {
            view.setHasTransientState(z);
        }

        public static void s(View view, int i) {
            view.setImportantForAccessibility(i);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$i */
    /* loaded from: classes.dex */
    public static class i {
        public static int a() {
            return View.generateViewId();
        }

        public static Display b(View view) {
            return view.getDisplay();
        }

        public static int c(View view) {
            return view.getLabelFor();
        }

        public static int d(View view) {
            return view.getLayoutDirection();
        }

        public static int e(View view) {
            return view.getPaddingEnd();
        }

        public static int f(View view) {
            return view.getPaddingStart();
        }

        public static boolean g(View view) {
            return view.isPaddingRelative();
        }

        public static void h(View view, int i) {
            view.setLabelFor(i);
        }

        public static void i(View view, Paint paint) {
            view.setLayerPaint(paint);
        }

        public static void j(View view, int i) {
            view.setLayoutDirection(i);
        }

        public static void k(View view, int i, int i2, int i3, int i4) {
            view.setPaddingRelative(i, i2, i3, i4);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$j */
    /* loaded from: classes.dex */
    public static class j {
        public static Rect a(View view) {
            return view.getClipBounds();
        }

        public static boolean b(View view) {
            return view.isInLayout();
        }

        public static void c(View view, Rect rect) {
            view.setClipBounds(rect);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$k */
    /* loaded from: classes.dex */
    public static class k {
        public static int a(View view) {
            return view.getAccessibilityLiveRegion();
        }

        public static boolean b(View view) {
            return view.isAttachedToWindow();
        }

        public static boolean c(View view) {
            return view.isLaidOut();
        }

        public static boolean d(View view) {
            return view.isLayoutDirectionResolved();
        }

        public static void e(ViewParent viewParent, View view, View view2, int i) {
            viewParent.notifySubtreeAccessibilityStateChanged(view, view2, i);
        }

        public static void f(View view, int i) {
            view.setAccessibilityLiveRegion(i);
        }

        public static void g(AccessibilityEvent accessibilityEvent, int i) {
            accessibilityEvent.setContentChangeTypes(i);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$l */
    /* loaded from: classes.dex */
    public static class l {
        public static WindowInsets a(View view, WindowInsets windowInsets) {
            return view.dispatchApplyWindowInsets(windowInsets);
        }

        public static WindowInsets b(View view, WindowInsets windowInsets) {
            return view.onApplyWindowInsets(windowInsets);
        }

        public static void c(View view) {
            view.requestApplyInsets();
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$m */
    /* loaded from: classes.dex */
    public static class m {

        /* compiled from: ViewCompat.java */
        /* renamed from: ei4$m$a */
        /* loaded from: classes.dex */
        public class a implements View.OnApplyWindowInsetsListener {
            public jp4 a = null;
            public final /* synthetic */ View b;
            public final /* synthetic */ em2 c;

            public a(View view, em2 em2Var) {
                this.b = view;
                this.c = em2Var;
            }

            @Override // android.view.View.OnApplyWindowInsetsListener
            public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
                jp4 y = jp4.y(windowInsets, view);
                int i = Build.VERSION.SDK_INT;
                if (i < 30) {
                    m.a(windowInsets, this.b);
                    if (y.equals(this.a)) {
                        return this.c.a(view, y).w();
                    }
                }
                this.a = y;
                jp4 a = this.c.a(view, y);
                if (i >= 30) {
                    return a.w();
                }
                ei4.q0(view);
                return a.w();
            }
        }

        public static void a(WindowInsets windowInsets, View view) {
            View.OnApplyWindowInsetsListener onApplyWindowInsetsListener = (View.OnApplyWindowInsetsListener) view.getTag(j03.tag_window_insets_animation_callback);
            if (onApplyWindowInsetsListener != null) {
                onApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
            }
        }

        public static jp4 b(View view, jp4 jp4Var, Rect rect) {
            WindowInsets w = jp4Var.w();
            if (w != null) {
                return jp4.y(view.computeSystemWindowInsets(w, rect), view);
            }
            rect.setEmpty();
            return jp4Var;
        }

        public static boolean c(View view, float f, float f2, boolean z) {
            return view.dispatchNestedFling(f, f2, z);
        }

        public static boolean d(View view, float f, float f2) {
            return view.dispatchNestedPreFling(f, f2);
        }

        public static boolean e(View view, int i, int i2, int[] iArr, int[] iArr2) {
            return view.dispatchNestedPreScroll(i, i2, iArr, iArr2);
        }

        public static boolean f(View view, int i, int i2, int i3, int i4, int[] iArr) {
            return view.dispatchNestedScroll(i, i2, i3, i4, iArr);
        }

        public static ColorStateList g(View view) {
            return view.getBackgroundTintList();
        }

        public static PorterDuff.Mode h(View view) {
            return view.getBackgroundTintMode();
        }

        public static float i(View view) {
            return view.getElevation();
        }

        public static jp4 j(View view) {
            return jp4.a.a(view);
        }

        public static String k(View view) {
            return view.getTransitionName();
        }

        public static float l(View view) {
            return view.getTranslationZ();
        }

        public static float m(View view) {
            return view.getZ();
        }

        public static boolean n(View view) {
            return view.hasNestedScrollingParent();
        }

        public static boolean o(View view) {
            return view.isImportantForAccessibility();
        }

        public static boolean p(View view) {
            return view.isNestedScrollingEnabled();
        }

        public static void q(View view, ColorStateList colorStateList) {
            view.setBackgroundTintList(colorStateList);
        }

        public static void r(View view, PorterDuff.Mode mode) {
            view.setBackgroundTintMode(mode);
        }

        public static void s(View view, float f) {
            view.setElevation(f);
        }

        public static void t(View view, boolean z) {
            view.setNestedScrollingEnabled(z);
        }

        public static void u(View view, em2 em2Var) {
            if (Build.VERSION.SDK_INT < 30) {
                view.setTag(j03.tag_on_apply_window_listener, em2Var);
            }
            if (em2Var == null) {
                view.setOnApplyWindowInsetsListener((View.OnApplyWindowInsetsListener) view.getTag(j03.tag_window_insets_animation_callback));
            } else {
                view.setOnApplyWindowInsetsListener(new a(view, em2Var));
            }
        }

        public static void v(View view, String str) {
            view.setTransitionName(str);
        }

        public static void w(View view, float f) {
            view.setTranslationZ(f);
        }

        public static void x(View view, float f) {
            view.setZ(f);
        }

        public static boolean y(View view, int i) {
            return view.startNestedScroll(i);
        }

        public static void z(View view) {
            view.stopNestedScroll();
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$n */
    /* loaded from: classes.dex */
    public static class n {
        public static jp4 a(View view) {
            WindowInsets rootWindowInsets = view.getRootWindowInsets();
            if (rootWindowInsets == null) {
                return null;
            }
            jp4 x = jp4.x(rootWindowInsets);
            x.u(x);
            x.d(view.getRootView());
            return x;
        }

        public static int b(View view) {
            return view.getScrollIndicators();
        }

        public static void c(View view, int i) {
            view.setScrollIndicators(i);
        }

        public static void d(View view, int i, int i2) {
            view.setScrollIndicators(i, i2);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$o */
    /* loaded from: classes.dex */
    public static class o {
        public static void a(View view) {
            view.cancelDragAndDrop();
        }

        public static void b(View view) {
            view.dispatchFinishTemporaryDetach();
        }

        public static void c(View view) {
            view.dispatchStartTemporaryDetach();
        }

        public static void d(View view, PointerIcon pointerIcon) {
            view.setPointerIcon(pointerIcon);
        }

        public static boolean e(View view, ClipData clipData, View.DragShadowBuilder dragShadowBuilder, Object obj, int i) {
            return view.startDragAndDrop(clipData, dragShadowBuilder, obj, i);
        }

        public static void f(View view, View.DragShadowBuilder dragShadowBuilder) {
            view.updateDragShadow(dragShadowBuilder);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$p */
    /* loaded from: classes.dex */
    public static class p {
        public static void a(View view, Collection<View> collection, int i) {
            view.addKeyboardNavigationClusters(collection, i);
        }

        public static int b(View view) {
            return view.getImportantForAutofill();
        }

        public static int c(View view) {
            return view.getNextClusterForwardId();
        }

        public static boolean d(View view) {
            return view.hasExplicitFocusable();
        }

        public static boolean e(View view) {
            return view.isFocusedByDefault();
        }

        public static boolean f(View view) {
            return view.isImportantForAutofill();
        }

        public static boolean g(View view) {
            return view.isKeyboardNavigationCluster();
        }

        public static View h(View view, View view2, int i) {
            return view.keyboardNavigationClusterSearch(view2, i);
        }

        public static boolean i(View view) {
            return view.restoreDefaultFocus();
        }

        public static void j(View view, String... strArr) {
            view.setAutofillHints(strArr);
        }

        public static void k(View view, boolean z) {
            view.setFocusedByDefault(z);
        }

        public static void l(View view, int i) {
            view.setImportantForAutofill(i);
        }

        public static void m(View view, boolean z) {
            view.setKeyboardNavigationCluster(z);
        }

        public static void n(View view, int i) {
            view.setNextClusterForwardId(i);
        }

        public static void o(View view, CharSequence charSequence) {
            view.setTooltipText(charSequence);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$q */
    /* loaded from: classes.dex */
    public static class q {
        public static void a(View view, final v vVar) {
            int i = j03.tag_unhandled_key_listeners;
            vo3 vo3Var = (vo3) view.getTag(i);
            if (vo3Var == null) {
                vo3Var = new vo3();
                view.setTag(i, vo3Var);
            }
            Objects.requireNonNull(vVar);
            View.OnUnhandledKeyEventListener onUnhandledKeyEventListener = new View.OnUnhandledKeyEventListener() { // from class: fi4
                @Override // android.view.View.OnUnhandledKeyEventListener
                public final boolean onUnhandledKeyEvent(View view2, KeyEvent keyEvent) {
                    return ei4.v.this.onUnhandledKeyEvent(view2, keyEvent);
                }
            };
            vo3Var.put(vVar, onUnhandledKeyEventListener);
            view.addOnUnhandledKeyEventListener(onUnhandledKeyEventListener);
        }

        public static CharSequence b(View view) {
            return view.getAccessibilityPaneTitle();
        }

        public static boolean c(View view) {
            return view.isAccessibilityHeading();
        }

        public static boolean d(View view) {
            return view.isScreenReaderFocusable();
        }

        public static void e(View view, v vVar) {
            View.OnUnhandledKeyEventListener onUnhandledKeyEventListener;
            vo3 vo3Var = (vo3) view.getTag(j03.tag_unhandled_key_listeners);
            if (vo3Var == null || (onUnhandledKeyEventListener = (View.OnUnhandledKeyEventListener) vo3Var.get(vVar)) == null) {
                return;
            }
            view.removeOnUnhandledKeyEventListener(onUnhandledKeyEventListener);
        }

        public static <T> T f(View view, int i) {
            return (T) view.requireViewById(i);
        }

        public static void g(View view, boolean z) {
            view.setAccessibilityHeading(z);
        }

        public static void h(View view, CharSequence charSequence) {
            view.setAccessibilityPaneTitle(charSequence);
        }

        public static void i(View view, boolean z) {
            view.setScreenReaderFocusable(z);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$r */
    /* loaded from: classes.dex */
    public static class r {
        public static View.AccessibilityDelegate a(View view) {
            return view.getAccessibilityDelegate();
        }

        public static List<Rect> b(View view) {
            return view.getSystemGestureExclusionRects();
        }

        public static void c(View view, Context context, int[] iArr, AttributeSet attributeSet, TypedArray typedArray, int i, int i2) {
            view.saveAttributeDataForStyleable(context, iArr, attributeSet, typedArray, i, i2);
        }

        public static void d(View view, List<Rect> list) {
            view.setSystemGestureExclusionRects(list);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$s */
    /* loaded from: classes.dex */
    public static class s {
        public static CharSequence a(View view) {
            return view.getStateDescription();
        }

        public static void b(View view, CharSequence charSequence) {
            view.setStateDescription(charSequence);
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$t */
    /* loaded from: classes.dex */
    public static final class t {
        public static String[] a(View view) {
            return view.getReceiveContentMimeTypes();
        }

        public static f70 b(View view, f70 f70Var) {
            ContentInfo f = f70Var.f();
            ContentInfo performReceiveContent = view.performReceiveContent(f);
            if (performReceiveContent == null) {
                return null;
            }
            return performReceiveContent == f ? f70Var : f70.g(performReceiveContent);
        }

        public static void c(View view, String[] strArr, qm2 qm2Var) {
            if (qm2Var == null) {
                view.setOnReceiveContentListener(strArr, null);
            } else {
                view.setOnReceiveContentListener(strArr, new u(qm2Var));
            }
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$u */
    /* loaded from: classes.dex */
    public static final class u implements OnReceiveContentListener {
        public final qm2 a;

        public u(qm2 qm2Var) {
            this.a = qm2Var;
        }

        @Override // android.view.OnReceiveContentListener
        public ContentInfo onReceiveContent(View view, ContentInfo contentInfo) {
            f70 g = f70.g(contentInfo);
            f70 a = this.a.a(view, g);
            if (a == null) {
                return null;
            }
            return a == g ? contentInfo : a.f();
        }
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$v */
    /* loaded from: classes.dex */
    public interface v {
        boolean onUnhandledKeyEvent(View view, KeyEvent keyEvent);
    }

    /* compiled from: ViewCompat.java */
    /* renamed from: ei4$w */
    /* loaded from: classes.dex */
    public static class w {
        public static final ArrayList<WeakReference<View>> d = new ArrayList<>();
        public WeakHashMap<View, Boolean> a = null;
        public SparseArray<WeakReference<View>> b = null;
        public WeakReference<KeyEvent> c = null;

        public static w a(View view) {
            int i = j03.tag_unhandled_key_event_manager;
            w wVar = (w) view.getTag(i);
            if (wVar == null) {
                w wVar2 = new w();
                view.setTag(i, wVar2);
                return wVar2;
            }
            return wVar;
        }

        public boolean b(View view, KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                g();
            }
            View c = c(view, keyEvent);
            if (keyEvent.getAction() == 0) {
                int keyCode = keyEvent.getKeyCode();
                if (c != null && !KeyEvent.isModifierKey(keyCode)) {
                    d().put(keyCode, new WeakReference<>(c));
                }
            }
            return c != null;
        }

        public final View c(View view, KeyEvent keyEvent) {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null && weakHashMap.containsKey(view)) {
                if (view instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                        View c = c(viewGroup.getChildAt(childCount), keyEvent);
                        if (c != null) {
                            return c;
                        }
                    }
                }
                if (e(view, keyEvent)) {
                    return view;
                }
            }
            return null;
        }

        public final SparseArray<WeakReference<View>> d() {
            if (this.b == null) {
                this.b = new SparseArray<>();
            }
            return this.b;
        }

        public final boolean e(View view, KeyEvent keyEvent) {
            ArrayList arrayList = (ArrayList) view.getTag(j03.tag_unhandled_key_listeners);
            if (arrayList != null) {
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    if (((v) arrayList.get(size)).onUnhandledKeyEvent(view, keyEvent)) {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        public boolean f(KeyEvent keyEvent) {
            int indexOfKey;
            WeakReference<KeyEvent> weakReference = this.c;
            if (weakReference == null || weakReference.get() != keyEvent) {
                this.c = new WeakReference<>(keyEvent);
                WeakReference<View> weakReference2 = null;
                SparseArray<WeakReference<View>> d2 = d();
                if (keyEvent.getAction() == 1 && (indexOfKey = d2.indexOfKey(keyEvent.getKeyCode())) >= 0) {
                    weakReference2 = d2.valueAt(indexOfKey);
                    d2.removeAt(indexOfKey);
                }
                if (weakReference2 == null) {
                    weakReference2 = d2.get(keyEvent.getKeyCode());
                }
                if (weakReference2 != null) {
                    View view = weakReference2.get();
                    if (view != null && ei4.V(view)) {
                        e(view, keyEvent);
                    }
                    return true;
                }
                return false;
            }
            return false;
        }

        public final void g() {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            ArrayList<WeakReference<View>> arrayList = d;
            if (arrayList.isEmpty()) {
                return;
            }
            synchronized (arrayList) {
                if (this.a == null) {
                    this.a = new WeakHashMap<>();
                }
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    ArrayList<WeakReference<View>> arrayList2 = d;
                    View view = arrayList2.get(size).get();
                    if (view == null) {
                        arrayList2.remove(size);
                    } else {
                        this.a.put(view, Boolean.TRUE);
                        for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
                            this.a.put((View) parent, Boolean.TRUE);
                        }
                    }
                }
            }
        }
    }

    static {
        new e();
    }

    public static rm2 A(View view) {
        if (view instanceof rm2) {
            return (rm2) view;
        }
        return l;
    }

    public static void A0(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            m.s(view, f2);
        }
    }

    public static boolean B(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.b(view);
        }
        return false;
    }

    @Deprecated
    public static void B0(View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    public static int C(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.c(view);
        }
        return 0;
    }

    public static void C0(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            h.r(view, z);
        }
    }

    @SuppressLint({"InlinedApi"})
    public static int D(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return p.b(view);
        }
        return 0;
    }

    public static void D0(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 19) {
            h.s(view, i2);
        } else if (i3 >= 16) {
            if (i2 == 4) {
                i2 = 2;
            }
            h.s(view, i2);
        }
    }

    public static int E(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return i.d(view);
        }
        return 0;
    }

    public static void E0(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 26) {
            p.l(view, i2);
        }
    }

    public static int F(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.d(view);
        }
        if (!e) {
            try {
                Field declaredField = View.class.getDeclaredField("mMinHeight");
                d = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            e = true;
        }
        Field field = d;
        if (field != null) {
            try {
                return ((Integer) field.get(view)).intValue();
            } catch (Exception unused2) {
                return 0;
            }
        }
        return 0;
    }

    public static void F0(View view, Paint paint) {
        if (Build.VERSION.SDK_INT >= 17) {
            i.i(view, paint);
            return;
        }
        view.setLayerType(view.getLayerType(), paint);
        view.invalidate();
    }

    public static int G(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.e(view);
        }
        if (!c) {
            try {
                Field declaredField = View.class.getDeclaredField("mMinWidth");
                b = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            c = true;
        }
        Field field = b;
        if (field != null) {
            try {
                return ((Integer) field.get(view)).intValue();
            } catch (Exception unused2) {
                return 0;
            }
        }
        return 0;
    }

    public static void G0(View view, em2 em2Var) {
        if (Build.VERSION.SDK_INT >= 21) {
            m.u(view, em2Var);
        }
    }

    public static String[] H(View view) {
        if (Build.VERSION.SDK_INT >= 31) {
            return t.a(view);
        }
        return (String[]) view.getTag(j03.tag_on_receive_content_mime_types);
    }

    public static void H0(View view, int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 17) {
            i.k(view, i2, i3, i4, i5);
        } else {
            view.setPadding(i2, i3, i4, i5);
        }
    }

    public static int I(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return i.e(view);
        }
        return view.getPaddingRight();
    }

    public static void I0(View view, ms2 ms2Var) {
        if (Build.VERSION.SDK_INT >= 24) {
            o.d(view, (PointerIcon) (ms2Var != null ? ms2Var.a() : null));
        }
    }

    public static int J(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return i.f(view);
        }
        return view.getPaddingLeft();
    }

    public static void J0(View view, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 23) {
            n.d(view, i2, i3);
        }
    }

    public static ViewParent K(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.f(view);
        }
        return view.getParent();
    }

    public static void K0(View view, CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 19) {
            P0().g(view, charSequence);
        }
    }

    public static jp4 L(View view) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            return n.a(view);
        }
        if (i2 >= 21) {
            return m.j(view);
        }
        return null;
    }

    public static void L0(View view, String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            m.v(view, str);
            return;
        }
        if (f == null) {
            f = new WeakHashMap<>();
        }
        f.put(view, str);
    }

    public static CharSequence M(View view) {
        return P0().f(view);
    }

    public static void M0(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            m.w(view, f2);
        }
    }

    public static String N(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return m.k(view);
        }
        WeakHashMap<View, String> weakHashMap = f;
        if (weakHashMap == null) {
            return null;
        }
        return weakHashMap.get(view);
    }

    public static void N0(View view) {
        if (C(view) == 0) {
            D0(view, 1);
        }
        for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
            if (C((View) parent) == 4) {
                D0(view, 2);
                return;
            }
        }
    }

    public static float O(View view) {
        return Build.VERSION.SDK_INT >= 21 ? m.l(view) : Utils.FLOAT_EPSILON;
    }

    public static void O0(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            m.x(view, f2);
        }
    }

    @Deprecated
    public static int P(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.g(view);
        }
        return 0;
    }

    public static f<CharSequence> P0() {
        return new c(j03.tag_state_description, CharSequence.class, 64, 30);
    }

    public static float Q(View view) {
        return Build.VERSION.SDK_INT >= 21 ? m.m(view) : Utils.FLOAT_EPSILON;
    }

    public static void Q0(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            m.z(view);
        } else if (view instanceof pe2) {
            ((pe2) view).stopNestedScroll();
        }
    }

    public static boolean R(View view) {
        if (Build.VERSION.SDK_INT >= 15) {
            return g.a(view);
        }
        return false;
    }

    public static void R0(View view) {
        float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }

    public static boolean S(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.h(view);
        }
        return true;
    }

    public static boolean T(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.i(view);
        }
        return false;
    }

    public static boolean U(View view) {
        Boolean f2 = b().f(view);
        return f2 != null && f2.booleanValue();
    }

    public static boolean V(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return k.b(view);
        }
        return view.getWindowToken() != null;
    }

    public static boolean W(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return k.c(view);
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    public static boolean X(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return m.p(view);
        }
        if (view instanceof pe2) {
            return ((pe2) view).isNestedScrollingEnabled();
        }
        return false;
    }

    public static boolean Y(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return i.g(view);
        }
        return false;
    }

    public static boolean Z(View view) {
        Boolean f2 = s0().f(view);
        return f2 != null && f2.booleanValue();
    }

    public static /* synthetic */ f70 a0(f70 f70Var) {
        return f70Var;
    }

    public static f<Boolean> b() {
        return new d(j03.tag_accessibility_heading, Boolean.class, 28);
    }

    public static void b0(View view, int i2) {
        AccessibilityManager accessibilityManager = (AccessibilityManager) view.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled()) {
            boolean z = r(view) != null && view.getVisibility() == 0;
            if (q(view) != 0 || z) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(z ? 32 : 2048);
                k.g(obtain, i2);
                if (z) {
                    obtain.getText().add(r(view));
                    N0(view);
                }
                view.sendAccessibilityEventUnchecked(obtain);
            } else if (i2 == 32) {
                AccessibilityEvent obtain2 = AccessibilityEvent.obtain();
                view.onInitializeAccessibilityEvent(obtain2);
                obtain2.setEventType(32);
                k.g(obtain2, i2);
                obtain2.setSource(view);
                view.onPopulateAccessibilityEvent(obtain2);
                obtain2.getText().add(r(view));
                accessibilityManager.sendAccessibilityEvent(obtain2);
            } else if (view.getParent() != null) {
                try {
                    k.e(view.getParent(), view, view, i2);
                } catch (AbstractMethodError unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(view.getParent().getClass().getSimpleName());
                    sb.append(" does not fully implement ViewParent");
                }
            }
        }
    }

    public static int c(View view, CharSequence charSequence, e6 e6Var) {
        int t2 = t(view, charSequence);
        if (t2 != -1) {
            d(view, new b6.a(t2, charSequence, e6Var));
        }
        return t2;
    }

    public static void c0(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 23) {
            view.offsetLeftAndRight(i2);
        } else if (i3 >= 21) {
            Rect z = z();
            boolean z2 = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                z.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z2 = !z.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            f(view, i2);
            if (z2 && z.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(z);
            }
        } else {
            f(view, i2);
        }
    }

    public static void d(View view, b6.a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            l(view);
            o0(aVar.b(), view);
            s(view).add(aVar);
            b0(view, 0);
        }
    }

    public static void d0(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 23) {
            view.offsetTopAndBottom(i2);
        } else if (i3 >= 21) {
            Rect z = z();
            boolean z2 = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                z.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z2 = !z.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            g(view, i2);
            if (z2 && z.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(z);
            }
        } else {
            g(view, i2);
        }
    }

    public static vj4 e(View view) {
        if (g == null) {
            g = new WeakHashMap<>();
        }
        vj4 vj4Var = g.get(view);
        if (vj4Var == null) {
            vj4 vj4Var2 = new vj4(view);
            g.put(view, vj4Var2);
            return vj4Var2;
        }
        return vj4Var;
    }

    public static jp4 e0(View view, jp4 jp4Var) {
        WindowInsets w2;
        if (Build.VERSION.SDK_INT >= 21 && (w2 = jp4Var.w()) != null) {
            WindowInsets b2 = l.b(view, w2);
            if (!b2.equals(w2)) {
                return jp4.y(b2, view);
            }
        }
        return jp4Var;
    }

    public static void f(View view, int i2) {
        view.offsetLeftAndRight(i2);
        if (view.getVisibility() == 0) {
            R0(view);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                R0((View) parent);
            }
        }
    }

    public static void f0(View view, b6 b6Var) {
        view.onInitializeAccessibilityNodeInfo(b6Var.H0());
    }

    public static void g(View view, int i2) {
        view.offsetTopAndBottom(i2);
        if (view.getVisibility() == 0) {
            R0(view);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                R0((View) parent);
            }
        }
    }

    public static f<CharSequence> g0() {
        return new b(j03.tag_accessibility_pane_title, CharSequence.class, 8, 28);
    }

    public static jp4 h(View view, jp4 jp4Var, Rect rect) {
        return Build.VERSION.SDK_INT >= 21 ? m.b(view, jp4Var, rect) : jp4Var;
    }

    public static boolean h0(View view, int i2, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return h.j(view, i2, bundle);
        }
        return false;
    }

    public static jp4 i(View view, jp4 jp4Var) {
        WindowInsets w2;
        if (Build.VERSION.SDK_INT >= 21 && (w2 = jp4Var.w()) != null) {
            WindowInsets a2 = l.a(view, w2);
            if (!a2.equals(w2)) {
                return jp4.y(a2, view);
            }
        }
        return jp4Var;
    }

    public static f70 i0(View view, f70 f70Var) {
        if (Log.isLoggable("ViewCompat", 3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("performReceiveContent: ");
            sb.append(f70Var);
            sb.append(", view=");
            sb.append(view.getClass().getSimpleName());
            sb.append("[");
            sb.append(view.getId());
            sb.append("]");
        }
        if (Build.VERSION.SDK_INT >= 31) {
            return t.b(view, f70Var);
        }
        qm2 qm2Var = (qm2) view.getTag(j03.tag_on_receive_content_listener);
        if (qm2Var != null) {
            f70 a2 = qm2Var.a(view, f70Var);
            if (a2 == null) {
                return null;
            }
            return A(view).a(a2);
        }
        return A(view).a(f70Var);
    }

    public static boolean j(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return w.a(view).b(view, keyEvent);
    }

    public static void j0(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            h.k(view);
        } else {
            view.postInvalidate();
        }
    }

    public static boolean k(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return w.a(view).f(keyEvent);
    }

    public static void k0(View view, int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 16) {
            h.l(view, i2, i3, i4, i5);
        } else {
            view.postInvalidate(i2, i3, i4, i5);
        }
    }

    public static void l(View view) {
        z5 n2 = n(view);
        if (n2 == null) {
            n2 = new z5();
        }
        t0(view, n2);
    }

    public static void l0(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            h.m(view, runnable);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay());
        }
    }

    public static int m() {
        AtomicInteger atomicInteger;
        int i2;
        int i3;
        if (Build.VERSION.SDK_INT >= 17) {
            return i.a();
        }
        do {
            atomicInteger = a;
            i2 = atomicInteger.get();
            i3 = i2 + 1;
            if (i3 > 16777215) {
                i3 = 1;
            }
        } while (!atomicInteger.compareAndSet(i2, i3));
        return i2;
    }

    @SuppressLint({"LambdaLast"})
    public static void m0(View view, Runnable runnable, long j2) {
        if (Build.VERSION.SDK_INT >= 16) {
            h.n(view, runnable, j2);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay() + j2);
        }
    }

    public static z5 n(View view) {
        View.AccessibilityDelegate o2 = o(view);
        if (o2 == null) {
            return null;
        }
        if (o2 instanceof z5.a) {
            return ((z5.a) o2).a;
        }
        return new z5(o2);
    }

    public static void n0(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 21) {
            o0(i2, view);
            b0(view, 0);
        }
    }

    public static View.AccessibilityDelegate o(View view) {
        if (Build.VERSION.SDK_INT >= 29) {
            return r.a(view);
        }
        return p(view);
    }

    public static void o0(int i2, View view) {
        List<b6.a> s2 = s(view);
        for (int i3 = 0; i3 < s2.size(); i3++) {
            if (s2.get(i3).b() == i2) {
                s2.remove(i3);
                return;
            }
        }
    }

    public static View.AccessibilityDelegate p(View view) {
        if (i) {
            return null;
        }
        if (h == null) {
            try {
                Field declaredField = View.class.getDeclaredField("mAccessibilityDelegate");
                h = declaredField;
                declaredField.setAccessible(true);
            } catch (Throwable unused) {
                i = true;
                return null;
            }
        }
        try {
            Object obj = h.get(view);
            if (obj instanceof View.AccessibilityDelegate) {
                return (View.AccessibilityDelegate) obj;
            }
            return null;
        } catch (Throwable unused2) {
            i = true;
            return null;
        }
    }

    public static void p0(View view, b6.a aVar, CharSequence charSequence, e6 e6Var) {
        if (e6Var == null && charSequence == null) {
            n0(view, aVar.b());
        } else {
            d(view, aVar.a(charSequence, e6Var));
        }
    }

    public static int q(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return k.a(view);
        }
        return 0;
    }

    public static void q0(View view) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 20) {
            l.c(view);
        } else if (i2 >= 16) {
            h.p(view);
        }
    }

    public static CharSequence r(View view) {
        return g0().f(view);
    }

    public static void r0(View view, @SuppressLint({"ContextFirst"}) Context context, int[] iArr, AttributeSet attributeSet, TypedArray typedArray, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 29) {
            r.c(view, context, iArr, attributeSet, typedArray, i2, i3);
        }
    }

    public static List<b6.a> s(View view) {
        int i2 = j03.tag_accessibility_actions;
        ArrayList arrayList = (ArrayList) view.getTag(i2);
        if (arrayList == null) {
            ArrayList arrayList2 = new ArrayList();
            view.setTag(i2, arrayList2);
            return arrayList2;
        }
        return arrayList;
    }

    public static f<Boolean> s0() {
        return new a(j03.tag_screen_reader_focusable, Boolean.class, 28);
    }

    public static int t(View view, CharSequence charSequence) {
        List<b6.a> s2 = s(view);
        for (int i2 = 0; i2 < s2.size(); i2++) {
            if (TextUtils.equals(charSequence, s2.get(i2).c())) {
                return s2.get(i2).b();
            }
        }
        int i3 = -1;
        int i4 = 0;
        while (true) {
            int[] iArr = k;
            if (i4 >= iArr.length || i3 != -1) {
                break;
            }
            int i5 = iArr[i4];
            boolean z = true;
            for (int i6 = 0; i6 < s2.size(); i6++) {
                z &= s2.get(i6).b() != i5;
            }
            if (z) {
                i3 = i5;
            }
            i4++;
        }
        return i3;
    }

    public static void t0(View view, z5 z5Var) {
        if (z5Var == null && (o(view) instanceof z5.a)) {
            z5Var = new z5();
        }
        view.setAccessibilityDelegate(z5Var == null ? null : z5Var.d());
    }

    public static ColorStateList u(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return m.g(view);
        }
        if (view instanceof m64) {
            return ((m64) view).getSupportBackgroundTintList();
        }
        return null;
    }

    public static void u0(View view, boolean z) {
        b().g(view, Boolean.valueOf(z));
    }

    public static PorterDuff.Mode v(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return m.h(view);
        }
        if (view instanceof m64) {
            return ((m64) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    public static void v0(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 19) {
            k.f(view, i2);
        }
    }

    public static Rect w(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return j.a(view);
        }
        return null;
    }

    public static void w0(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            h.q(view, drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static Display x(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return i.b(view);
        }
        if (V(view)) {
            return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }

    public static void x0(View view, ColorStateList colorStateList) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            m.q(view, colorStateList);
            if (i2 == 21) {
                Drawable background = view.getBackground();
                boolean z = (m.g(view) == null && m.h(view) == null) ? false : true;
                if (background == null || !z) {
                    return;
                }
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                h.q(view, background);
            }
        } else if (view instanceof m64) {
            ((m64) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    public static float y(View view) {
        return Build.VERSION.SDK_INT >= 21 ? m.i(view) : Utils.FLOAT_EPSILON;
    }

    public static void y0(View view, PorterDuff.Mode mode) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            m.r(view, mode);
            if (i2 == 21) {
                Drawable background = view.getBackground();
                boolean z = (m.g(view) == null && m.h(view) == null) ? false : true;
                if (background == null || !z) {
                    return;
                }
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                h.q(view, background);
            }
        } else if (view instanceof m64) {
            ((m64) view).setSupportBackgroundTintMode(mode);
        }
    }

    public static Rect z() {
        if (j == null) {
            j = new ThreadLocal<>();
        }
        Rect rect = j.get();
        if (rect == null) {
            rect = new Rect();
            j.set(rect);
        }
        rect.setEmpty();
        return rect;
    }

    public static void z0(View view, Rect rect) {
        if (Build.VERSION.SDK_INT >= 18) {
            j.c(view, rect);
        }
    }
}
