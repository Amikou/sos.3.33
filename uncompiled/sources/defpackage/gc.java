package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.Objects;

/* compiled from: AnchorItemDialog.kt */
/* renamed from: gc  reason: default package */
/* loaded from: classes2.dex */
public class gc {
    public final rc1<te4> a;
    public final rc1<te4> b;
    public PopupWindow c;

    public gc(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        fs1.f(rc1Var, "onShowListener");
        fs1.f(rc1Var2, "onDismissListener");
        this.a = rc1Var;
        this.b = rc1Var2;
    }

    public static final void h(gc gcVar) {
        fs1.f(gcVar, "this$0");
        gcVar.e().invoke();
    }

    public final View b(Context context, int i, int i2) {
        fs1.f(context, "context");
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        View inflate = ((LayoutInflater) systemService).inflate(i2, (ViewGroup) null);
        fs1.e(inflate, "view");
        this.c = c(inflate, i);
        return inflate;
    }

    public final PopupWindow c(View view, int i) {
        PopupWindow popupWindow = new PopupWindow(view, i, -2);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void d() {
        PopupWindow popupWindow;
        if (!f() || (popupWindow = this.c) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final rc1<te4> e() {
        return this.b;
    }

    public final boolean f() {
        PopupWindow popupWindow = this.c;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final void g(View view) {
        fs1.f(view, "anchorView");
        PopupWindow popupWindow = this.c;
        if (popupWindow != null) {
            popupWindow.showAsDropDown(view);
        }
        this.a.invoke();
        PopupWindow popupWindow2 = this.c;
        if (popupWindow2 == null) {
            return;
        }
        popupWindow2.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: fc
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                gc.h(gc.this);
            }
        });
    }
}
