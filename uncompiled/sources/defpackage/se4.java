package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: CoroutineContext.kt */
/* renamed from: se4  reason: default package */
/* loaded from: classes2.dex */
public final class se4 implements CoroutineContext.a, CoroutineContext.b<se4> {
    public static final se4 a = new se4();

    @Override // kotlin.coroutines.CoroutineContext
    public <R> R fold(R r, hd1<? super R, ? super CoroutineContext.a, ? extends R> hd1Var) {
        return (R) CoroutineContext.a.C0196a.a(this, r, hd1Var);
    }

    @Override // kotlin.coroutines.CoroutineContext.a, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        return (E) CoroutineContext.a.C0196a.b(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext.a
    public CoroutineContext.b<?> getKey() {
        return this;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        return CoroutineContext.a.C0196a.c(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        return CoroutineContext.a.C0196a.d(this, coroutineContext);
    }
}
