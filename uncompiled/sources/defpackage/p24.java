package defpackage;

/* compiled from: SystemClock.java */
/* renamed from: p24  reason: default package */
/* loaded from: classes2.dex */
public class p24 implements sz {
    public static p24 a;

    public static p24 b() {
        if (a == null) {
            a = new p24();
        }
        return a;
    }

    @Override // defpackage.sz
    public long a() {
        return System.currentTimeMillis();
    }
}
