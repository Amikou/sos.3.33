package defpackage;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import defpackage.j92;
import java.io.InputStream;

/* compiled from: ResourceLoader.java */
/* renamed from: a83  reason: default package */
/* loaded from: classes.dex */
public class a83<Data> implements j92<Integer, Data> {
    public final j92<Uri, Data> a;
    public final Resources b;

    /* compiled from: ResourceLoader.java */
    /* renamed from: a83$a */
    /* loaded from: classes.dex */
    public static final class a implements k92<Integer, AssetFileDescriptor> {
        public final Resources a;

        public a(Resources resources) {
            this.a = resources;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Integer, AssetFileDescriptor> c(qa2 qa2Var) {
            return new a83(this.a, qa2Var.d(Uri.class, AssetFileDescriptor.class));
        }
    }

    /* compiled from: ResourceLoader.java */
    /* renamed from: a83$b */
    /* loaded from: classes.dex */
    public static class b implements k92<Integer, ParcelFileDescriptor> {
        public final Resources a;

        public b(Resources resources) {
            this.a = resources;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Integer, ParcelFileDescriptor> c(qa2 qa2Var) {
            return new a83(this.a, qa2Var.d(Uri.class, ParcelFileDescriptor.class));
        }
    }

    /* compiled from: ResourceLoader.java */
    /* renamed from: a83$c */
    /* loaded from: classes.dex */
    public static class c implements k92<Integer, InputStream> {
        public final Resources a;

        public c(Resources resources) {
            this.a = resources;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Integer, InputStream> c(qa2 qa2Var) {
            return new a83(this.a, qa2Var.d(Uri.class, InputStream.class));
        }
    }

    /* compiled from: ResourceLoader.java */
    /* renamed from: a83$d */
    /* loaded from: classes.dex */
    public static class d implements k92<Integer, Uri> {
        public final Resources a;

        public d(Resources resources) {
            this.a = resources;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Integer, Uri> c(qa2 qa2Var) {
            return new a83(this.a, ve4.c());
        }
    }

    public a83(Resources resources, j92<Uri, Data> j92Var) {
        this.b = resources;
        this.a = j92Var;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(Integer num, int i, int i2, vn2 vn2Var) {
        Uri d2 = d(num);
        if (d2 == null) {
            return null;
        }
        return this.a.b(d2, i, i2, vn2Var);
    }

    public final Uri d(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException unused) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Received invalid resource id: ");
                sb.append(num);
                return null;
            }
            return null;
        }
    }

    @Override // defpackage.j92
    /* renamed from: e */
    public boolean a(Integer num) {
        return true;
    }
}
