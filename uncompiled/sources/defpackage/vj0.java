package defpackage;

import defpackage.wn1;

/* compiled from: DefaultImageFormatChecker.java */
/* renamed from: vj0  reason: default package */
/* loaded from: classes.dex */
public class vj0 implements wn1.a {
    public static final byte[] c;
    public static final int d;
    public static final byte[] e;
    public static final int f;
    public static final byte[] g = yn1.a("GIF87a");
    public static final byte[] h = yn1.a("GIF89a");
    public static final byte[] i;
    public static final int j;
    public static final byte[] k;
    public static final int l;
    public static final byte[] m;
    public static final byte[][] n;
    public static final byte[] o;
    public static final byte[] p;
    public static final int q;
    public final int a = hs1.a(21, 20, d, f, 6, j, l, 12);
    public boolean b = false;

    static {
        byte[] bArr = {-1, -40, -1};
        c = bArr;
        d = bArr.length;
        byte[] bArr2 = {-119, 80, 78, 71, 13, 10, 26, 10};
        e = bArr2;
        f = bArr2.length;
        byte[] a = yn1.a("BM");
        i = a;
        j = a.length;
        byte[] bArr3 = {0, 0, 1, 0};
        k = bArr3;
        l = bArr3.length;
        m = yn1.a("ftyp");
        n = new byte[][]{yn1.a("heic"), yn1.a("heix"), yn1.a("hevc"), yn1.a("hevx"), yn1.a("mif1"), yn1.a("msf1")};
        byte[] bArr4 = {73, 73, 42, 0};
        o = bArr4;
        p = new byte[]{77, 77, 0, 42};
        q = bArr4.length;
    }

    public static wn1 c(byte[] bArr, int i2) {
        xt2.b(Boolean.valueOf(po4.h(bArr, 0, i2)));
        if (po4.g(bArr, 0)) {
            return wj0.f;
        }
        if (po4.f(bArr, 0)) {
            return wj0.g;
        }
        if (po4.c(bArr, 0, i2)) {
            if (po4.b(bArr, 0)) {
                return wj0.j;
            }
            if (po4.d(bArr, 0)) {
                return wj0.i;
            }
            return wj0.h;
        }
        return wn1.b;
    }

    public static boolean d(byte[] bArr, int i2) {
        byte[] bArr2 = i;
        if (i2 < bArr2.length) {
            return false;
        }
        return yn1.c(bArr, bArr2);
    }

    public static boolean e(byte[] bArr, int i2) {
        return i2 >= q && (yn1.c(bArr, o) || yn1.c(bArr, p));
    }

    public static boolean f(byte[] bArr, int i2) {
        if (i2 < 6) {
            return false;
        }
        return yn1.c(bArr, g) || yn1.c(bArr, h);
    }

    public static boolean g(byte[] bArr, int i2) {
        if (i2 >= 12 && bArr[3] >= 8 && yn1.b(bArr, m, 4)) {
            for (byte[] bArr2 : n) {
                if (yn1.b(bArr, bArr2, 8)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public static boolean h(byte[] bArr, int i2) {
        byte[] bArr2 = k;
        if (i2 < bArr2.length) {
            return false;
        }
        return yn1.c(bArr, bArr2);
    }

    public static boolean i(byte[] bArr, int i2) {
        byte[] bArr2 = c;
        return i2 >= bArr2.length && yn1.c(bArr, bArr2);
    }

    public static boolean j(byte[] bArr, int i2) {
        byte[] bArr2 = e;
        return i2 >= bArr2.length && yn1.c(bArr, bArr2);
    }

    @Override // defpackage.wn1.a
    public int a() {
        return this.a;
    }

    @Override // defpackage.wn1.a
    public final wn1 b(byte[] bArr, int i2) {
        xt2.g(bArr);
        if (!this.b && po4.h(bArr, 0, i2)) {
            return c(bArr, i2);
        }
        if (i(bArr, i2)) {
            return wj0.a;
        }
        if (j(bArr, i2)) {
            return wj0.b;
        }
        if (this.b && po4.h(bArr, 0, i2)) {
            return c(bArr, i2);
        }
        if (f(bArr, i2)) {
            return wj0.c;
        }
        if (d(bArr, i2)) {
            return wj0.d;
        }
        if (h(bArr, i2)) {
            return wj0.e;
        }
        if (g(bArr, i2)) {
            return wj0.k;
        }
        if (e(bArr, i2)) {
            return wj0.l;
        }
        return wn1.b;
    }
}
