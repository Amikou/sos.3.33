package defpackage;

import androidx.media3.common.util.b;
import defpackage.wi3;

/* compiled from: IndexSeeker.java */
/* renamed from: kq1  reason: default package */
/* loaded from: classes.dex */
public final class kq1 implements zi3 {
    public final long a;
    public final e22 b;
    public final e22 c;
    public long d;

    public kq1(long j, long j2, long j3) {
        this.d = j;
        this.a = j3;
        e22 e22Var = new e22();
        this.b = e22Var;
        e22 e22Var2 = new e22();
        this.c = e22Var2;
        e22Var.a(0L);
        e22Var2.a(j2);
    }

    public boolean a(long j) {
        e22 e22Var = this.b;
        return j - e22Var.b(e22Var.c() - 1) < 100000;
    }

    @Override // defpackage.zi3
    public long b(long j) {
        return this.b.b(b.f(this.c, j, true, true));
    }

    public void c(long j, long j2) {
        if (a(j)) {
            return;
        }
        this.b.a(j);
        this.c.a(j2);
    }

    @Override // defpackage.zi3
    public long d() {
        return this.a;
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    public void f(long j) {
        this.d = j;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        int f = b.f(this.b, j, true, true);
        yi3 yi3Var = new yi3(this.b.b(f), this.c.b(f));
        if (yi3Var.a != j && f != this.b.c() - 1) {
            int i = f + 1;
            return new wi3.a(yi3Var, new yi3(this.b.b(i), this.c.b(i)));
        }
        return new wi3.a(yi3Var);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.d;
    }
}
