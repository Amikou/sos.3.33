package defpackage;

import android.util.Pair;
import android.view.View;
import com.rd.animation.type.AnimationType;
import com.rd.draw.data.Orientation;

/* compiled from: MeasureController.java */
/* renamed from: q52  reason: default package */
/* loaded from: classes2.dex */
public class q52 {
    public Pair<Integer, Integer> a(mq1 mq1Var, int i, int i2) {
        int i3;
        int i4;
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        int c = mq1Var.c();
        int m = mq1Var.m();
        int s = mq1Var.s();
        int h = mq1Var.h();
        int j = mq1Var.j();
        int l = mq1Var.l();
        int k = mq1Var.k();
        int i5 = mq1Var.i();
        int i6 = m * 2;
        Orientation g = mq1Var.g();
        if (c != 0) {
            i4 = (i6 * c) + (s * 2 * c) + (h * (c - 1));
            i3 = i6 + s;
            if (g != Orientation.HORIZONTAL) {
                i4 = i3;
                i3 = i4;
            }
        } else {
            i3 = 0;
            i4 = 0;
        }
        if (mq1Var.b() == AnimationType.DROP) {
            if (g == Orientation.HORIZONTAL) {
                i3 *= 2;
            } else {
                i4 *= 2;
            }
        }
        Orientation orientation = Orientation.HORIZONTAL;
        int i7 = i4 + j + k;
        int i8 = i3 + l + i5;
        if (mode != 1073741824) {
            size = mode == Integer.MIN_VALUE ? Math.min(i7, size) : i7;
        }
        if (mode2 != 1073741824) {
            size2 = mode2 == Integer.MIN_VALUE ? Math.min(i8, size2) : i8;
        }
        if (size < 0) {
            size = 0;
        }
        int i9 = size2 >= 0 ? size2 : 0;
        mq1Var.a0(size);
        mq1Var.G(i9);
        return new Pair<>(Integer.valueOf(size), Integer.valueOf(i9));
    }
}
