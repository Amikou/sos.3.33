package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* compiled from: IdenticonDrawable.kt */
/* renamed from: on1  reason: default package */
/* loaded from: classes2.dex */
public abstract class on1 extends Drawable {
    public final Rect a;
    public final Rect b;
    public final Bitmap c;
    public final Canvas d;
    public final Paint e = new Paint(1);
    public int f;

    public on1(int i, int i2, int i3, int i4) {
        this.f = i3;
        while (i % i4 != 0) {
            i++;
        }
        while (i2 % i4 != 0) {
            i2++;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        fs1.e(createBitmap, "createBitmap(actualWidth… Bitmap.Config.ARGB_8888)");
        this.c = createBitmap;
        this.a = new Rect(0, 0, i, i2);
        this.b = new Rect(0, 0, i, i2);
        this.d = new Canvas(createBitmap);
    }

    public abstract void a(Canvas canvas);

    public final int b() {
        return this.f;
    }

    public final void c() {
        a(this.d);
        invalidateSelf();
    }

    public void d(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        fs1.f(canvas, "canvas");
        this.b.set(0, 0, canvas.getWidth(), canvas.getHeight());
        canvas.drawBitmap(this.c, this.a, this.b, this.e);
    }

    public final void e(int i) {
        this.f = i;
        d(i);
        c();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.e.getAlpha();
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.e.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.e.setColorFilter(colorFilter);
    }
}
