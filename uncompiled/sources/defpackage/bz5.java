package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.vision.face.internal.client.LandmarkParcel;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: bz5  reason: default package */
/* loaded from: classes.dex */
public final class bz5 implements Parcelable.Creator<LandmarkParcel> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LandmarkParcel createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        float f = Utils.FLOAT_EPSILON;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 1) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 2) {
                f = SafeParcelReader.A(parcel, C);
            } else if (v == 3) {
                f2 = SafeParcelReader.A(parcel, C);
            } else if (v != 4) {
                SafeParcelReader.I(parcel, C);
            } else {
                i2 = SafeParcelReader.E(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new LandmarkParcel(i, f, f2, i2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LandmarkParcel[] newArray(int i) {
        return new LandmarkParcel[i];
    }
}
