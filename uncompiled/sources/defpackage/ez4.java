package defpackage;

import java.io.PrintStream;

/* compiled from: uishash.java */
/* renamed from: ez4  reason: default package */
/* loaded from: classes.dex */
public class ez4 {
    public static int a;

    public static byte[] a(String str) {
        if (a == 1) {
            PrintStream printStream = System.out;
            printStream.println("GetHashBytes 137 stringin=" + str);
        }
        return ay1.a(b(str));
    }

    public static String b(String str) {
        if (a == 1) {
            PrintStream printStream = System.out;
            printStream.println("GetHashString 101 stringin=" + str);
        }
        if (str == null) {
            return new String();
        }
        ha3 ha3Var = new ha3();
        byte[] a2 = ay1.a(str);
        ha3Var.n();
        ha3Var.d(a2, 0, str.length());
        ha3Var.m();
        if (ha3Var.f) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < 20; i++) {
                stringBuffer.append((char) ha3Var.e[i]);
            }
            return stringBuffer.toString();
        }
        return new String("");
    }

    public static byte[] c(byte[] bArr) {
        return d(bArr, bArr.length);
    }

    public static byte[] d(byte[] bArr, int i) {
        ha3 ha3Var = new ha3();
        ha3Var.n();
        ha3Var.d(bArr, 0, i);
        ha3Var.m();
        if (ha3Var.f) {
            return ha3Var.e;
        }
        return null;
    }
}
