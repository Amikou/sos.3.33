package defpackage;

import android.util.Pair;
import com.facebook.common.util.TriState;
import com.facebook.imagepipeline.common.Priority;
import com.github.mikephil.charting.utils.Utils;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

/* compiled from: MultiplexProducer.java */
/* renamed from: eb2  reason: default package */
/* loaded from: classes.dex */
public abstract class eb2<K, T extends Closeable> implements dv2<T> {
    public final Map<K, eb2<K, T>.b> a;
    public final dv2<T> b;
    public final boolean c;
    public final String d;
    public final String e;

    /* compiled from: MultiplexProducer.java */
    /* renamed from: eb2$b */
    /* loaded from: classes.dex */
    public class b {
        public final K a;
        public final CopyOnWriteArraySet<Pair<l60<T>, ev2>> b = rm3.a();
        public T c;
        public float d;
        public int e;
        public un f;
        public eb2<K, T>.b.C0167b g;

        /* compiled from: MultiplexProducer.java */
        /* renamed from: eb2$b$a */
        /* loaded from: classes.dex */
        public class a extends vn {
            public final /* synthetic */ Pair a;

            public a(Pair pair) {
                this.a = pair;
            }

            @Override // defpackage.fv2
            public void a() {
                boolean remove;
                List list;
                un unVar;
                List list2;
                List list3;
                synchronized (b.this) {
                    remove = b.this.b.remove(this.a);
                    list = null;
                    if (!remove) {
                        unVar = null;
                        list2 = null;
                    } else if (b.this.b.isEmpty()) {
                        unVar = b.this.f;
                        list2 = null;
                    } else {
                        List s = b.this.s();
                        list2 = b.this.t();
                        list3 = b.this.r();
                        unVar = null;
                        list = s;
                    }
                    list3 = list2;
                }
                un.r(list);
                un.s(list2);
                un.q(list3);
                if (unVar != null) {
                    if (eb2.this.c && !unVar.h()) {
                        un.s(unVar.x(Priority.LOW));
                    } else {
                        unVar.t();
                    }
                }
                if (remove) {
                    ((l60) this.a.first).b();
                }
            }

            @Override // defpackage.vn, defpackage.fv2
            public void b() {
                un.q(b.this.r());
            }

            @Override // defpackage.vn, defpackage.fv2
            public void c() {
                un.s(b.this.t());
            }

            @Override // defpackage.vn, defpackage.fv2
            public void d() {
                un.r(b.this.s());
            }
        }

        /* compiled from: MultiplexProducer.java */
        /* renamed from: eb2$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0167b extends qm<T> {
            public C0167b() {
            }

            @Override // defpackage.qm
            public void g() {
                try {
                    if (nc1.d()) {
                        nc1.a("MultiplexProducer#onCancellation");
                    }
                    b.this.m(this);
                } finally {
                    if (nc1.d()) {
                        nc1.b();
                    }
                }
            }

            @Override // defpackage.qm
            public void h(Throwable th) {
                try {
                    if (nc1.d()) {
                        nc1.a("MultiplexProducer#onFailure");
                    }
                    b.this.n(this, th);
                } finally {
                    if (nc1.d()) {
                        nc1.b();
                    }
                }
            }

            @Override // defpackage.qm
            public void j(float f) {
                try {
                    if (nc1.d()) {
                        nc1.a("MultiplexProducer#onProgressUpdate");
                    }
                    b.this.p(this, f);
                } finally {
                    if (nc1.d()) {
                        nc1.b();
                    }
                }
            }

            @Override // defpackage.qm
            /* renamed from: p */
            public void i(T t, int i) {
                try {
                    if (nc1.d()) {
                        nc1.a("MultiplexProducer#onNewResult");
                    }
                    b.this.o(this, t, i);
                } finally {
                    if (nc1.d()) {
                        nc1.b();
                    }
                }
            }
        }

        public b(K k) {
            this.a = k;
        }

        public final void g(Pair<l60<T>, ev2> pair, ev2 ev2Var) {
            ev2Var.o(new a(pair));
        }

        /* JADX WARN: Multi-variable type inference failed */
        public boolean h(l60<T> l60Var, ev2 ev2Var) {
            Pair<l60<T>, ev2> create = Pair.create(l60Var, ev2Var);
            synchronized (this) {
                if (eb2.this.h(this.a) != this) {
                    return false;
                }
                this.b.add(create);
                List<fv2> s = s();
                List<fv2> t = t();
                List<fv2> r = r();
                Closeable closeable = this.c;
                float f = this.d;
                int i = this.e;
                un.r(s);
                un.s(t);
                un.q(r);
                synchronized (create) {
                    synchronized (this) {
                        if (closeable != this.c) {
                            closeable = null;
                        } else if (closeable != null) {
                            closeable = eb2.this.f(closeable);
                        }
                    }
                    if (closeable != null) {
                        if (f > Utils.FLOAT_EPSILON) {
                            l60Var.c(f);
                        }
                        l60Var.d(closeable, i);
                        i(closeable);
                    }
                }
                g(create, ev2Var);
                return true;
            }
        }

        public final void i(Closeable closeable) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        public final synchronized boolean j() {
            Iterator<Pair<l60<T>, ev2>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((ev2) it.next().second).m()) {
                    return true;
                }
            }
            return false;
        }

        public final synchronized boolean k() {
            Iterator<Pair<l60<T>, ev2>> it = this.b.iterator();
            while (it.hasNext()) {
                if (!((ev2) it.next().second).h()) {
                    return false;
                }
            }
            return true;
        }

        public final synchronized Priority l() {
            Priority priority;
            priority = Priority.LOW;
            Iterator<Pair<l60<T>, ev2>> it = this.b.iterator();
            while (it.hasNext()) {
                priority = Priority.getHigherPriority(priority, ((ev2) it.next().second).getPriority());
            }
            return priority;
        }

        public void m(eb2<K, T>.b.C0167b c0167b) {
            synchronized (this) {
                if (this.g != c0167b) {
                    return;
                }
                this.g = null;
                this.f = null;
                i(this.c);
                this.c = null;
                q(TriState.UNSET);
            }
        }

        public void n(eb2<K, T>.b.C0167b c0167b, Throwable th) {
            synchronized (this) {
                if (this.g != c0167b) {
                    return;
                }
                Iterator<Pair<l60<T>, ev2>> it = this.b.iterator();
                this.b.clear();
                eb2.this.j(this.a, this);
                i(this.c);
                this.c = null;
                while (it.hasNext()) {
                    Pair<l60<T>, ev2> next = it.next();
                    synchronized (next) {
                        ((ev2) next.second).l().c((ev2) next.second, eb2.this.d, th, null);
                        ((l60) next.first).a(th);
                    }
                }
            }
        }

        public void o(eb2<K, T>.b.C0167b c0167b, T t, int i) {
            synchronized (this) {
                if (this.g != c0167b) {
                    return;
                }
                i(this.c);
                this.c = null;
                Iterator<Pair<l60<T>, ev2>> it = this.b.iterator();
                int size = this.b.size();
                if (qm.f(i)) {
                    this.c = (T) eb2.this.f(t);
                    this.e = i;
                } else {
                    this.b.clear();
                    eb2.this.j(this.a, this);
                }
                while (it.hasNext()) {
                    Pair<l60<T>, ev2> next = it.next();
                    synchronized (next) {
                        if (qm.e(i)) {
                            ((ev2) next.second).l().a((ev2) next.second, eb2.this.d, null);
                            un unVar = this.f;
                            if (unVar != null) {
                                ((ev2) next.second).g(unVar.getExtras());
                            }
                            ((ev2) next.second).b(eb2.this.e, Integer.valueOf(size));
                        }
                        ((l60) next.first).d(t, i);
                    }
                }
            }
        }

        public void p(eb2<K, T>.b.C0167b c0167b, float f) {
            synchronized (this) {
                if (this.g != c0167b) {
                    return;
                }
                this.d = f;
                Iterator<Pair<l60<T>, ev2>> it = this.b.iterator();
                while (it.hasNext()) {
                    Pair<l60<T>, ev2> next = it.next();
                    synchronized (next) {
                        ((l60) next.first).c(f);
                    }
                }
            }
        }

        public final void q(TriState triState) {
            synchronized (this) {
                boolean z = true;
                xt2.b(Boolean.valueOf(this.f == null));
                if (this.g != null) {
                    z = false;
                }
                xt2.b(Boolean.valueOf(z));
                if (this.b.isEmpty()) {
                    eb2.this.j(this.a, this);
                    return;
                }
                ev2 ev2Var = (ev2) this.b.iterator().next().second;
                un unVar = new un(ev2Var.c(), ev2Var.getId(), ev2Var.l(), ev2Var.a(), ev2Var.n(), k(), j(), l(), ev2Var.d());
                this.f = unVar;
                unVar.g(ev2Var.getExtras());
                if (triState.isSet()) {
                    this.f.b("started_as_prefetch", Boolean.valueOf(triState.asBoolean()));
                }
                eb2<K, T>.b.C0167b c0167b = new C0167b();
                this.g = c0167b;
                eb2.this.b.a(c0167b, this.f);
            }
        }

        public final synchronized List<fv2> r() {
            un unVar = this.f;
            if (unVar == null) {
                return null;
            }
            return unVar.v(j());
        }

        public final synchronized List<fv2> s() {
            un unVar = this.f;
            if (unVar == null) {
                return null;
            }
            return unVar.w(k());
        }

        public final synchronized List<fv2> t() {
            un unVar = this.f;
            if (unVar == null) {
                return null;
            }
            return unVar.x(l());
        }
    }

    public eb2(dv2<T> dv2Var, String str, String str2) {
        this(dv2Var, str, str2, false);
    }

    @Override // defpackage.dv2
    public void a(l60<T> l60Var, ev2 ev2Var) {
        boolean z;
        eb2<K, T>.b h;
        try {
            if (nc1.d()) {
                nc1.a("MultiplexProducer#produceResults");
            }
            ev2Var.l().k(ev2Var, this.d);
            K i = i(ev2Var);
            do {
                z = false;
                synchronized (this) {
                    h = h(i);
                    if (h == null) {
                        h = g(i);
                        z = true;
                    }
                }
            } while (!h.h(l60Var, ev2Var));
            if (z) {
                h.q(TriState.valueOf(ev2Var.h()));
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public abstract T f(T t);

    public final synchronized eb2<K, T>.b g(K k) {
        eb2<K, T>.b bVar;
        bVar = new b(k);
        this.a.put(k, bVar);
        return bVar;
    }

    public synchronized eb2<K, T>.b h(K k) {
        return this.a.get(k);
    }

    public abstract K i(ev2 ev2Var);

    public synchronized void j(K k, eb2<K, T>.b bVar) {
        if (this.a.get(k) == bVar) {
            this.a.remove(k);
        }
    }

    public eb2(dv2<T> dv2Var, String str, String str2, boolean z) {
        this.b = dv2Var;
        this.a = new HashMap();
        this.c = z;
        this.d = str;
        this.e = str2;
    }
}
