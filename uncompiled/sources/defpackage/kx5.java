package defpackage;

import com.google.android.gms.internal.vision.n0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: kx5  reason: default package */
/* loaded from: classes.dex */
public final class kx5 implements tv5 {
    public final n0 a;
    public final String b;
    public final Object[] c;
    public final int d;

    public kx5(n0 n0Var, String str, Object[] objArr) {
        this.a = n0Var;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.d = charAt;
            return;
        }
        int i = charAt & 8191;
        int i2 = 13;
        int i3 = 1;
        while (true) {
            int i4 = i3 + 1;
            char charAt2 = str.charAt(i3);
            if (charAt2 < 55296) {
                this.d = i | (charAt2 << i2);
                return;
            }
            i |= (charAt2 & 8191) << i2;
            i2 += 13;
            i3 = i4;
        }
    }

    public final String a() {
        return this.b;
    }

    public final Object[] b() {
        return this.c;
    }

    @Override // defpackage.tv5
    public final int zza() {
        return (this.d & 1) == 1 ? bx5.a : bx5.b;
    }

    @Override // defpackage.tv5
    public final boolean zzb() {
        return (this.d & 2) == 2;
    }

    @Override // defpackage.tv5
    public final n0 zzc() {
        return this.a;
    }
}
