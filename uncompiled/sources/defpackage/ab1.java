package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSendingBinding.java */
/* renamed from: ab1  reason: default package */
/* loaded from: classes2.dex */
public final class ab1 {
    public final MaterialButton a;
    public final FrameLayout b;
    public final ImageView c;
    public final ImageView d;
    public final ConstraintLayout e;
    public final ProgressBar f;
    public final TextView g;
    public final TextView h;
    public final TextView i;
    public final TextView j;
    public final TextView k;

    public ab1(FrameLayout frameLayout, MaterialButton materialButton, FrameLayout frameLayout2, FrameLayout frameLayout3, ImageView imageView, ImageView imageView2, MaterialCardView materialCardView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ProgressBar progressBar, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view) {
        this.a = materialButton;
        this.b = frameLayout2;
        this.c = imageView;
        this.d = imageView2;
        this.e = constraintLayout2;
        this.f = progressBar;
        this.g = textView;
        this.h = textView2;
        this.i = textView3;
        this.j = textView4;
        this.k = textView5;
    }

    public static ab1 a(View view) {
        int i = R.id.btnSendNew;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnSendNew);
        if (materialButton != null) {
            i = R.id.btnSendParent;
            FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.btnSendParent);
            if (frameLayout != null) {
                FrameLayout frameLayout2 = (FrameLayout) view;
                i = R.id.iv_back;
                ImageView imageView = (ImageView) ai4.a(view, R.id.iv_back);
                if (imageView != null) {
                    i = R.id.ivContactIcon;
                    ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivContactIcon);
                    if (imageView2 != null) {
                        i = R.id.ivContactIconContainer;
                        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ivContactIconContainer);
                        if (materialCardView != null) {
                            i = R.id.lWalletBalance;
                            ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.lWalletBalance);
                            if (constraintLayout != null) {
                                i = R.id.llItemContactCarouselContainer;
                                ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.llItemContactCarouselContainer);
                                if (constraintLayout2 != null) {
                                    i = R.id.progressBar;
                                    ProgressBar progressBar = (ProgressBar) ai4.a(view, R.id.progressBar);
                                    if (progressBar != null) {
                                        i = R.id.tvAddress;
                                        TextView textView = (TextView) ai4.a(view, R.id.tvAddress);
                                        if (textView != null) {
                                            i = R.id.tvBalance;
                                            TextView textView2 = (TextView) ai4.a(view, R.id.tvBalance);
                                            if (textView2 != null) {
                                                i = R.id.tvBalanceInFiat;
                                                TextView textView3 = (TextView) ai4.a(view, R.id.tvBalanceInFiat);
                                                if (textView3 != null) {
                                                    i = R.id.tvContactText;
                                                    TextView textView4 = (TextView) ai4.a(view, R.id.tvContactText);
                                                    if (textView4 != null) {
                                                        i = R.id.tvGasPrice;
                                                        TextView textView5 = (TextView) ai4.a(view, R.id.tvGasPrice);
                                                        if (textView5 != null) {
                                                            i = R.id.viewFakeEnd;
                                                            View a = ai4.a(view, R.id.viewFakeEnd);
                                                            if (a != null) {
                                                                return new ab1(frameLayout2, materialButton, frameLayout, frameLayout2, imageView, imageView2, materialCardView, constraintLayout, constraintLayout2, progressBar, textView, textView2, textView3, textView4, textView5, a);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
