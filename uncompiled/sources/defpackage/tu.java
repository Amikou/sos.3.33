package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import com.google.android.material.datepicker.MaterialCalendar;

/* compiled from: CalendarStyle.java */
/* renamed from: tu  reason: default package */
/* loaded from: classes2.dex */
public final class tu {
    public final su a;
    public final su b;
    public final su c;
    public final su d;
    public final su e;
    public final su f;
    public final su g;
    public final Paint h;

    public tu(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i42.c(context, gy2.materialCalendarStyle, MaterialCalendar.class.getCanonicalName()), o23.MaterialCalendar);
        this.a = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_dayStyle, 0));
        this.g = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_dayInvalidStyle, 0));
        this.b = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_daySelectedStyle, 0));
        this.c = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_dayTodayStyle, 0));
        ColorStateList b = n42.b(context, obtainStyledAttributes, o23.MaterialCalendar_rangeFillColor);
        this.d = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_yearStyle, 0));
        this.e = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_yearSelectedStyle, 0));
        this.f = su.a(context, obtainStyledAttributes.getResourceId(o23.MaterialCalendar_yearTodayStyle, 0));
        Paint paint = new Paint();
        this.h = paint;
        paint.setColor(b.getDefaultColor());
        obtainStyledAttributes.recycle();
    }
}
