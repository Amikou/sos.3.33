package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.RecoverWalletActivity;

/* renamed from: v43  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class v43 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ v43 a = new v43();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        RecoverWalletActivity.a0(dialogInterface);
    }
}
