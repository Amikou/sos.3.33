package defpackage;

import androidx.media3.common.util.b;
import defpackage.la2;
import defpackage.wi3;

/* compiled from: VbriSeeker.java */
/* renamed from: ch4  reason: default package */
/* loaded from: classes.dex */
public final class ch4 implements zi3 {
    public final long[] a;
    public final long[] b;
    public final long c;
    public final long d;

    public ch4(long[] jArr, long[] jArr2, long j, long j2) {
        this.a = jArr;
        this.b = jArr2;
        this.c = j;
        this.d = j2;
    }

    public static ch4 a(long j, long j2, la2.a aVar, op2 op2Var) {
        int D;
        op2Var.Q(10);
        int n = op2Var.n();
        if (n <= 0) {
            return null;
        }
        int i = aVar.d;
        long J0 = b.J0(n, 1000000 * (i >= 32000 ? 1152 : 576), i);
        int J = op2Var.J();
        int J2 = op2Var.J();
        int J3 = op2Var.J();
        op2Var.Q(2);
        long j3 = j2 + aVar.c;
        long[] jArr = new long[J];
        long[] jArr2 = new long[J];
        int i2 = 0;
        long j4 = j2;
        while (i2 < J) {
            int i3 = J2;
            long j5 = j3;
            jArr[i2] = (i2 * J0) / J;
            jArr2[i2] = Math.max(j4, j5);
            if (J3 == 1) {
                D = op2Var.D();
            } else if (J3 == 2) {
                D = op2Var.J();
            } else if (J3 == 3) {
                D = op2Var.G();
            } else if (J3 != 4) {
                return null;
            } else {
                D = op2Var.H();
            }
            j4 += D * i3;
            i2++;
            jArr = jArr;
            J2 = i3;
            j3 = j5;
        }
        long[] jArr3 = jArr;
        if (j != -1 && j != j4) {
            p12.i("VbriSeeker", "VBRI data size mismatch: " + j + ", " + j4);
        }
        return new ch4(jArr3, jArr2, J0, j4);
    }

    @Override // defpackage.zi3
    public long b(long j) {
        return this.a[b.i(this.b, j, true, true)];
    }

    @Override // defpackage.zi3
    public long d() {
        return this.d;
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        int i = b.i(this.a, j, true, true);
        yi3 yi3Var = new yi3(this.a[i], this.b[i]);
        if (yi3Var.a < j && i != this.a.length - 1) {
            int i2 = i + 1;
            return new wi3.a(yi3Var, new yi3(this.a[i2], this.b[i2]));
        }
        return new wi3.a(yi3Var);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.c;
    }
}
