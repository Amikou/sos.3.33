package defpackage;

import android.graphics.drawable.Drawable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

/* compiled from: ActionBarOnDestinationChangedListener.java */
/* renamed from: h6  reason: default package */
/* loaded from: classes.dex */
public class h6 extends p4 {
    public final AppCompatActivity f;

    public h6(AppCompatActivity appCompatActivity, af afVar) {
        super(appCompatActivity.getDrawerToggleDelegate().a(), afVar);
        this.f = appCompatActivity;
    }

    @Override // defpackage.p4
    public void c(Drawable drawable, int i) {
        ActionBar supportActionBar = this.f.getSupportActionBar();
        if (drawable == null) {
            supportActionBar.t(false);
            return;
        }
        supportActionBar.t(true);
        this.f.getDrawerToggleDelegate().b(drawable, i);
    }

    @Override // defpackage.p4
    public void d(CharSequence charSequence) {
        this.f.getSupportActionBar().x(charSequence);
    }
}
