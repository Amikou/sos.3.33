package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/* compiled from: KeyCycleOscillator.java */
/* renamed from: hx1  reason: default package */
/* loaded from: classes.dex */
public abstract class hx1 {
    public b a;
    public String b;
    public int c = 0;
    public String d = null;
    public int e = 0;
    public ArrayList<c> f = new ArrayList<>();

    /* compiled from: KeyCycleOscillator.java */
    /* renamed from: hx1$a */
    /* loaded from: classes.dex */
    public class a implements Comparator<c> {
        public a(hx1 hx1Var) {
        }

        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(c cVar, c cVar2) {
            return Integer.compare(cVar.a, cVar2.a);
        }
    }

    /* compiled from: KeyCycleOscillator.java */
    /* renamed from: hx1$b */
    /* loaded from: classes.dex */
    public static class b {
        public ao2 a;
        public float[] b;
        public double[] c;
        public float[] d;
        public float[] e;
        public float[] f;
        public bc0 g;
        public double[] h;
        public double[] i;

        public b(int i, String str, int i2, int i3) {
            ao2 ao2Var = new ao2();
            this.a = ao2Var;
            ao2Var.g(i, str);
            this.b = new float[i3];
            this.c = new double[i3];
            this.d = new float[i3];
            this.e = new float[i3];
            this.f = new float[i3];
            float[] fArr = new float[i3];
        }

        public double a(float f) {
            bc0 bc0Var = this.g;
            if (bc0Var != null) {
                double d = f;
                bc0Var.g(d, this.i);
                this.g.d(d, this.h);
            } else {
                double[] dArr = this.i;
                dArr[0] = 0.0d;
                dArr[1] = 0.0d;
                dArr[2] = 0.0d;
            }
            double d2 = f;
            double e = this.a.e(d2, this.h[1]);
            double d3 = this.a.d(d2, this.h[1], this.i[1]);
            double[] dArr2 = this.i;
            return dArr2[0] + (e * dArr2[2]) + (d3 * this.h[2]);
        }

        public double b(float f) {
            bc0 bc0Var = this.g;
            if (bc0Var != null) {
                bc0Var.d(f, this.h);
            } else {
                double[] dArr = this.h;
                dArr[0] = this.e[0];
                dArr[1] = this.f[0];
                dArr[2] = this.b[0];
            }
            double[] dArr2 = this.h;
            return dArr2[0] + (this.a.e(f, dArr2[1]) * this.h[2]);
        }

        public void c(int i, int i2, float f, float f2, float f3, float f4) {
            this.c[i] = i2 / 100.0d;
            this.d[i] = f;
            this.e[i] = f2;
            this.f[i] = f3;
            this.b[i] = f4;
        }

        public void d(float f) {
            double[][] dArr = (double[][]) Array.newInstance(double.class, this.c.length, 3);
            float[] fArr = this.b;
            this.h = new double[fArr.length + 2];
            this.i = new double[fArr.length + 2];
            if (this.c[0] > Utils.DOUBLE_EPSILON) {
                this.a.a(Utils.DOUBLE_EPSILON, this.d[0]);
            }
            double[] dArr2 = this.c;
            int length = dArr2.length - 1;
            if (dArr2[length] < 1.0d) {
                this.a.a(1.0d, this.d[length]);
            }
            for (int i = 0; i < dArr.length; i++) {
                dArr[i][0] = this.e[i];
                dArr[i][1] = this.f[i];
                dArr[i][2] = this.b[i];
                this.a.a(this.c[i], this.d[i]);
            }
            this.a.f();
            double[] dArr3 = this.c;
            if (dArr3.length > 1) {
                this.g = bc0.a(0, dArr3, dArr);
            } else {
                this.g = null;
            }
        }
    }

    /* compiled from: KeyCycleOscillator.java */
    /* renamed from: hx1$c */
    /* loaded from: classes.dex */
    public static class c {
        public int a;
        public float b;
        public float c;
        public float d;
        public float e;

        public c(int i, float f, float f2, float f3, float f4) {
            this.a = i;
            this.b = f4;
            this.c = f2;
            this.d = f;
            this.e = f3;
        }
    }

    public float a(float f) {
        return (float) this.a.b(f);
    }

    public float b(float f) {
        return (float) this.a.a(f);
    }

    public void c(Object obj) {
    }

    public void d(int i, int i2, String str, int i3, float f, float f2, float f3, float f4) {
        this.f.add(new c(i, f, f2, f3, f4));
        if (i3 != -1) {
            this.e = i3;
        }
        this.c = i2;
        this.d = str;
    }

    public void e(int i, int i2, String str, int i3, float f, float f2, float f3, float f4, Object obj) {
        this.f.add(new c(i, f, f2, f3, f4));
        if (i3 != -1) {
            this.e = i3;
        }
        this.c = i2;
        c(obj);
        this.d = str;
    }

    public void f(String str) {
        this.b = str;
    }

    public void g(float f) {
        int size = this.f.size();
        if (size == 0) {
            return;
        }
        Collections.sort(this.f, new a(this));
        double[] dArr = new double[size];
        char c2 = 0;
        double[][] dArr2 = (double[][]) Array.newInstance(double.class, size, 3);
        this.a = new b(this.c, this.d, this.e, size);
        Iterator<c> it = this.f.iterator();
        int i = 0;
        while (it.hasNext()) {
            c next = it.next();
            float f2 = next.d;
            dArr[i] = f2 * 0.01d;
            double[] dArr3 = dArr2[i];
            float f3 = next.b;
            dArr3[c2] = f3;
            double[] dArr4 = dArr2[i];
            float f4 = next.c;
            dArr4[1] = f4;
            double[] dArr5 = dArr2[i];
            float f5 = next.e;
            dArr5[2] = f5;
            this.a.c(i, next.a, f2, f4, f5, f3);
            i++;
            c2 = 0;
        }
        this.a.d(f);
        bc0.a(0, dArr, dArr2);
    }

    public boolean h() {
        return this.e == 1;
    }

    public String toString() {
        String str = this.b;
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        Iterator<c> it = this.f.iterator();
        while (it.hasNext()) {
            c next = it.next();
            str = str + "[" + next.a + " , " + decimalFormat.format(next.b) + "] ";
        }
        return str;
    }
}
