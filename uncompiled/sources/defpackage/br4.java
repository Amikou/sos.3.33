package defpackage;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

/* compiled from: WorkerFactory.java */
/* renamed from: br4  reason: default package */
/* loaded from: classes.dex */
public abstract class br4 {
    public static final String a = v12.f("WorkerFactory");

    /* compiled from: WorkerFactory.java */
    /* renamed from: br4$a */
    /* loaded from: classes.dex */
    public class a extends br4 {
        @Override // defpackage.br4
        public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
            return null;
        }
    }

    public static br4 c() {
        return new a();
    }

    public abstract ListenableWorker a(Context context, String str, WorkerParameters workerParameters);

    public final ListenableWorker b(Context context, String str, WorkerParameters workerParameters) {
        ListenableWorker a2 = a(context, str, workerParameters);
        if (a2 == null) {
            Class cls = null;
            try {
                cls = Class.forName(str).asSubclass(ListenableWorker.class);
            } catch (Throwable th) {
                v12 c = v12.c();
                String str2 = a;
                c.b(str2, "Invalid class: " + str, th);
            }
            if (cls != null) {
                try {
                    a2 = (ListenableWorker) cls.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                } catch (Throwable th2) {
                    v12 c2 = v12.c();
                    String str3 = a;
                    c2.b(str3, "Could not instantiate " + str, th2);
                }
            }
        }
        if (a2 == null || !a2.l()) {
            return a2;
        }
        throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", getClass().getName(), str));
    }
}
