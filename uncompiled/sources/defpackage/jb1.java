package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentTransferHistoryBinding.java */
/* renamed from: jb1  reason: default package */
/* loaded from: classes2.dex */
public final class jb1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final AppCompatImageView f;
    public final AppCompatImageView g;
    public final AppCompatImageView h;
    public final AppCompatImageView i;
    public final AppCompatImageView j;
    public final RecyclerView k;
    public final SwipeRefreshLayout l;
    public final g74 m;
    public final TextView n;
    public final TextView o;
    public final TextView p;
    public final AppCompatTextView q;

    public jb1(ConstraintLayout constraintLayout, AppBarLayout appBarLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, CollapsingToolbarLayout collapsingToolbarLayout, ConstraintLayout constraintLayout2, CoordinatorLayout coordinatorLayout, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, AppCompatImageView appCompatImageView4, AppCompatImageView appCompatImageView5, LinearLayoutCompat linearLayoutCompat, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, g74 g74Var, TextView textView, TextView textView2, TextView textView3, AppCompatTextView appCompatTextView) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = materialButton4;
        this.f = appCompatImageView;
        this.g = appCompatImageView2;
        this.h = appCompatImageView3;
        this.i = appCompatImageView4;
        this.j = appCompatImageView5;
        this.k = recyclerView;
        this.l = swipeRefreshLayout;
        this.m = g74Var;
        this.n = textView;
        this.o = textView2;
        this.p = textView3;
        this.q = appCompatTextView;
    }

    public static jb1 a(View view) {
        int i = R.id.appBar;
        AppBarLayout appBarLayout = (AppBarLayout) ai4.a(view, R.id.appBar);
        if (appBarLayout != null) {
            i = R.id.btnBuy;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBuy);
            if (materialButton != null) {
                i = R.id.btnReceive;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnReceive);
                if (materialButton2 != null) {
                    i = R.id.btnSend;
                    MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnSend);
                    if (materialButton3 != null) {
                        i = R.id.btnSwap;
                        MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.btnSwap);
                        if (materialButton4 != null) {
                            i = R.id.ccToolBar;
                            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) ai4.a(view, R.id.ccToolBar);
                            if (collapsingToolbarLayout != null) {
                                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                                i = R.id.coordinator;
                                CoordinatorLayout coordinatorLayout = (CoordinatorLayout) ai4.a(view, R.id.coordinator);
                                if (coordinatorLayout != null) {
                                    i = R.id.imgCMC;
                                    AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgCMC);
                                    if (appCompatImageView != null) {
                                        i = R.id.imgDexscreener;
                                        AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgDexscreener);
                                        if (appCompatImageView2 != null) {
                                            i = R.id.imgPriceAlert;
                                            AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.imgPriceAlert);
                                            if (appCompatImageView3 != null) {
                                                i = R.id.imgSymbol;
                                                AppCompatImageView appCompatImageView4 = (AppCompatImageView) ai4.a(view, R.id.imgSymbol);
                                                if (appCompatImageView4 != null) {
                                                    i = R.id.imgTransactionHistoryInfo;
                                                    AppCompatImageView appCompatImageView5 = (AppCompatImageView) ai4.a(view, R.id.imgTransactionHistoryInfo);
                                                    if (appCompatImageView5 != null) {
                                                        i = R.id.lButtonsContainer;
                                                        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.lButtonsContainer);
                                                        if (linearLayoutCompat != null) {
                                                            i = R.id.rv_trans;
                                                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rv_trans);
                                                            if (recyclerView != null) {
                                                                i = R.id.rvTransGroup;
                                                                SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.rvTransGroup);
                                                                if (swipeRefreshLayout != null) {
                                                                    i = R.id.toolbar;
                                                                    View a = ai4.a(view, R.id.toolbar);
                                                                    if (a != null) {
                                                                        g74 a2 = g74.a(a);
                                                                        i = R.id.tvChainWallet;
                                                                        TextView textView = (TextView) ai4.a(view, R.id.tvChainWallet);
                                                                        if (textView != null) {
                                                                            i = R.id.tvFiatBalance;
                                                                            TextView textView2 = (TextView) ai4.a(view, R.id.tvFiatBalance);
                                                                            if (textView2 != null) {
                                                                                i = R.id.tv_not;
                                                                                TextView textView3 = (TextView) ai4.a(view, R.id.tv_not);
                                                                                if (textView3 != null) {
                                                                                    i = R.id.tvWalletBlnc;
                                                                                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.tvWalletBlnc);
                                                                                    if (appCompatTextView != null) {
                                                                                        return new jb1(constraintLayout, appBarLayout, materialButton, materialButton2, materialButton3, materialButton4, collapsingToolbarLayout, constraintLayout, coordinatorLayout, appCompatImageView, appCompatImageView2, appCompatImageView3, appCompatImageView4, appCompatImageView5, linearLayoutCompat, recyclerView, swipeRefreshLayout, a2, textView, textView2, textView3, appCompatTextView);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
