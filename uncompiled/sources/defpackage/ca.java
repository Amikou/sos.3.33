package defpackage;

import java.security.GeneralSecurityException;

/* compiled from: AeadConfig.java */
/* renamed from: ca  reason: default package */
/* loaded from: classes2.dex */
public final class ca {
    @Deprecated
    public static final i63 a;

    static {
        new fa().c();
        new la().c();
        new na().c();
        new ja().c();
        new tx1().c();
        new vx1().c();
        new bx().c();
        new yr4().c();
        a = i63.D();
        try {
            a();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    @Deprecated
    public static void a() throws GeneralSecurityException {
        b();
    }

    public static void b() throws GeneralSecurityException {
        x22.b();
        fa.l(true);
        ja.l(true);
        la.n(true);
        na.m(true);
        bx.l(true);
        tx1.l(true);
        vx1.l(true);
        yr4.l(true);
        da.e();
    }
}
