package defpackage;

/* compiled from: FP.java */
/* renamed from: x11  reason: default package */
/* loaded from: classes2.dex */
public final class x11 {
    public final zl a;
    public int b;

    public x11(int i) {
        if (i < 0) {
            zl zlVar = new zl(f33.a);
            zlVar.l(i);
            zlVar.v();
            this.a = new zl(zlVar);
        } else {
            this.a = new zl(i);
        }
        n();
    }

    public static int i(int i) {
        int i2 = i | (i >>> 1);
        int i3 = i2 | (i2 >>> 2);
        int i4 = i3 | (i3 >>> 4);
        int i5 = i4 | (i4 >>> 8);
        int i6 = i5 | (i5 >>> 16);
        int i7 = i6 - ((i6 >>> 1) & 1431655765);
        int i8 = (i7 & 858993459) + ((i7 >>> 2) & 858993459);
        return ((252645135 & (i8 + (i8 >>> 4))) * 16843009) >>> 24;
    }

    public static zl j(hd0 hd0Var) {
        return zl.q(new zl(f33.a), 15772622380152113L, hd0Var);
    }

    public static int s(zl zlVar, zl zlVar2) {
        return (int) (zlVar.a[4] / (zlVar2.a[4] + 1));
    }

    public void A() {
        this.a.J();
        this.b = 1;
    }

    public void a(x11 x11Var) {
        this.a.a(x11Var.a);
        int i = this.b + x11Var.b;
        this.b = i;
        if (i > 16777215) {
            u();
        }
    }

    public void b(x11 x11Var, int i) {
        this.a.c(x11Var.a, i);
        int i2 = this.b;
        this.b = ((x11Var.b ^ i2) & (-i)) ^ i2;
    }

    public void c(x11 x11Var) {
        this.a.e(x11Var.a);
        this.b = x11Var.b;
    }

    public boolean d(x11 x11Var) {
        x11 x11Var2 = new x11(this);
        x11 x11Var3 = new x11(x11Var);
        x11Var2.u();
        x11Var3.u();
        return zl.d(x11Var2.a, x11Var3.a) == 0;
    }

    public void e(int i) {
        boolean z;
        if (i < 0) {
            i = -i;
            z = true;
        } else {
            z = false;
        }
        if (this.b * i <= 16777215) {
            this.a.A(i);
            this.b *= i;
        } else {
            k(new x11(i));
        }
        if (z) {
            l();
            m();
        }
    }

    public void f(x11 x11Var) {
        m();
        x11 x11Var2 = new x11(this);
        if (x11Var == null) {
            q();
        } else {
            c(x11Var);
        }
        for (int i = 0; i <= 1; i++) {
            x();
        }
        k(x11Var2);
        u();
    }

    public boolean g() {
        x11 x11Var = new x11(this);
        x11Var.u();
        return x11Var.t().m();
    }

    public boolean h() {
        x11 x11Var = new x11(this);
        x11Var.u();
        return x11Var.a.n();
    }

    public void k(x11 x11Var) {
        if (this.b * x11Var.b > 16777215) {
            u();
        }
        this.a.e(j(zl.r(this.a, x11Var.a)));
        this.b = 2;
    }

    public void l() {
        zl zlVar = new zl(f33.a);
        int i = i(this.b - 1);
        zlVar.j(i);
        this.a.B(zlVar);
        int i2 = (1 << i) + 1;
        this.b = i2;
        if (i2 > 16777215) {
            u();
        }
    }

    public void m() {
        this.a.v();
    }

    public void n() {
        this.a.e(j(zl.r(this.a, new zl(f33.c))));
        this.b = 2;
    }

    public void o() {
        this.a.w();
        n();
    }

    public final x11 p(zl zlVar) {
        byte[] bArr = new byte[71];
        x11[] x11VarArr = new x11[16];
        m();
        zl zlVar2 = new zl(zlVar);
        zlVar2.v();
        int u = ((zlVar2.u() + 3) / 4) + 1;
        for (int i = 0; i < u; i++) {
            int o = zlVar2.o(4);
            zlVar2.g(o);
            zlVar2.v();
            bArr[i] = (byte) o;
            zlVar2.k(4);
        }
        x11VarArr[0] = new x11(1);
        x11VarArr[1] = new x11(this);
        for (int i2 = 2; i2 < 16; i2++) {
            x11VarArr[i2] = new x11(x11VarArr[i2 - 1]);
            x11VarArr[i2].k(this);
        }
        x11 x11Var = new x11(x11VarArr[bArr[u - 1]]);
        for (int i3 = u - 2; i3 >= 0; i3--) {
            x11Var.x();
            x11Var.x();
            x11Var.x();
            x11Var.x();
            x11Var.k(x11VarArr[bArr[i3]]);
        }
        x11Var.u();
        return x11Var;
    }

    public final void q() {
        zl zlVar = new zl(f33.a);
        zlVar.g(1);
        zlVar.D(1);
        zlVar.g(1);
        zlVar.k(1);
        c(p(zlVar));
    }

    public int r(x11 x11Var) {
        x11 x11Var2 = new x11(this);
        x11Var2.q();
        if (x11Var != null) {
            x11Var.c(x11Var2);
        }
        x11Var2.x();
        x11Var2.k(this);
        return x11Var2.g() ? 1 : 0;
    }

    public zl t() {
        return j(new hd0(this.a));
    }

    public String toString() {
        return t().toString();
    }

    public void u() {
        int i;
        long[] jArr = f33.a;
        zl zlVar = new zl(jArr);
        zl zlVar2 = new zl(jArr);
        this.a.v();
        int i2 = this.b;
        if (i2 > 16) {
            long A = zlVar2.A(s(this.a, zlVar));
            long[] jArr2 = zlVar2.a;
            jArr2[4] = jArr2[4] + (A << 56);
            this.a.G(zlVar2);
            this.a.v();
            i = 2;
        } else {
            i = i(i2 - 1);
        }
        zlVar.j(i);
        while (i > 0) {
            this.a.c(zlVar2, 1 - zl.F(zlVar2, this.a, zlVar));
            i--;
        }
        this.b = 1;
    }

    public void v(x11 x11Var) {
        x11 x11Var2 = new x11(this);
        x11Var2.l();
        c(x11Var);
        a(x11Var2);
    }

    public int w() {
        x11 x11Var = new x11(this);
        x11Var.u();
        return x11Var.t().y();
    }

    public void x() {
        int i = this.b;
        if (i * i > 16777215) {
            u();
        }
        this.a.e(j(zl.E(this.a)));
        this.b = 2;
    }

    public x11 y(x11 x11Var) {
        x11 x11Var2 = new x11(this);
        if (x11Var == null) {
            x11Var2.q();
        } else {
            x11Var2.c(x11Var);
        }
        new x11(new zl(f33.b));
        x11 x11Var3 = new x11(x11Var2);
        x11Var3.x();
        x11Var3.k(this);
        x11 x11Var4 = new x11(this);
        x11Var4.k(x11Var2);
        new x11(x11Var3);
        int w = x11Var4.w();
        x11 x11Var5 = new x11(x11Var4);
        x11Var5.l();
        x11Var5.m();
        x11Var4.b(x11Var5, w);
        return x11Var4;
    }

    public void z(x11 x11Var) {
        x11 x11Var2 = new x11(x11Var);
        x11Var2.l();
        a(x11Var2);
    }

    public x11() {
        this.a = new zl(0);
        this.b = 1;
    }

    public x11(zl zlVar) {
        this.a = new zl(zlVar);
        n();
    }

    public x11(x11 x11Var) {
        this.a = new zl(x11Var.a);
        this.b = x11Var.b;
    }
}
