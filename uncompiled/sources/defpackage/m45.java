package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: m45  reason: default package */
/* loaded from: classes.dex */
public final class m45 implements Iterator<z55> {
    public int a = 0;
    public final /* synthetic */ p45 f0;

    public m45(p45 p45Var) {
        this.f0 = p45Var;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a < this.f0.s();
    }

    @Override // java.util.Iterator
    public final /* bridge */ /* synthetic */ z55 next() {
        if (this.a < this.f0.s()) {
            p45 p45Var = this.f0;
            int i = this.a;
            this.a = i + 1;
            return p45Var.w(i);
        }
        int i2 = this.a;
        StringBuilder sb = new StringBuilder(32);
        sb.append("Out of bounds index: ");
        sb.append(i2);
        throw new NoSuchElementException(sb.toString());
    }
}
