package defpackage;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.i;

/* renamed from: qx1  reason: default package */
/* loaded from: classes2.dex */
public final class qx1 {
    public final qo0 a;
    public final int b;

    public qx1(qo0 qo0Var, int i) {
        Objects.requireNonNull(qo0Var, "digest == null");
        this.a = qo0Var;
        this.b = i;
    }

    public byte[] a(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        int i = this.b;
        if (length == i) {
            if (bArr2.length == i) {
                return d(0, bArr, bArr2);
            }
            throw new IllegalArgumentException("wrong in length");
        }
        throw new IllegalArgumentException("wrong key length");
    }

    public byte[] b(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        int i = this.b;
        if (length == i) {
            if (bArr2.length == i * 2) {
                return d(1, bArr, bArr2);
            }
            throw new IllegalArgumentException("wrong in length");
        }
        throw new IllegalArgumentException("wrong key length");
    }

    public byte[] c(byte[] bArr, byte[] bArr2) {
        if (bArr.length == this.b) {
            if (bArr2.length == 32) {
                return d(3, bArr, bArr2);
            }
            throw new IllegalArgumentException("wrong address length");
        }
        throw new IllegalArgumentException("wrong key length");
    }

    public final byte[] d(int i, byte[] bArr, byte[] bArr2) {
        byte[] p = i.p(i, this.b);
        this.a.b(p, 0, p.length);
        this.a.b(bArr, 0, bArr.length);
        this.a.b(bArr2, 0, bArr2.length);
        int i2 = this.b;
        byte[] bArr3 = new byte[i2];
        qo0 qo0Var = this.a;
        if (qo0Var instanceof ls4) {
            ((ls4) qo0Var).e(bArr3, 0, i2);
        } else {
            qo0Var.a(bArr3, 0);
        }
        return bArr3;
    }
}
