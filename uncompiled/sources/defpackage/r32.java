package defpackage;

import com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite;
import com.google.crypto.tink.shaded.protobuf.ProtoSyntax;
import com.google.crypto.tink.shaded.protobuf.g0;
import com.google.crypto.tink.shaded.protobuf.h0;
import com.google.crypto.tink.shaded.protobuf.n0;
import com.google.crypto.tink.shaded.protobuf.o0;
import com.google.crypto.tink.shaded.protobuf.t;
import com.google.crypto.tink.shaded.protobuf.v;
import com.google.crypto.tink.shaded.protobuf.z;

/* compiled from: ManifestSchemaFactory.java */
/* renamed from: r32  reason: default package */
/* loaded from: classes2.dex */
public final class r32 implements nd3 {
    public static final c82 b = new a();
    public final c82 a;

    /* compiled from: ManifestSchemaFactory.java */
    /* renamed from: r32$a */
    /* loaded from: classes2.dex */
    public class a implements c82 {
        @Override // defpackage.c82
        public a82 a(Class<?> cls) {
            throw new IllegalStateException("This should never be called.");
        }

        @Override // defpackage.c82
        public boolean b(Class<?> cls) {
            return false;
        }
    }

    /* compiled from: ManifestSchemaFactory.java */
    /* renamed from: r32$b */
    /* loaded from: classes2.dex */
    public static class b implements c82 {
        public c82[] a;

        public b(c82... c82VarArr) {
            this.a = c82VarArr;
        }

        @Override // defpackage.c82
        public a82 a(Class<?> cls) {
            c82[] c82VarArr;
            for (c82 c82Var : this.a) {
                if (c82Var.b(cls)) {
                    return c82Var.a(cls);
                }
            }
            throw new UnsupportedOperationException("No factory is available for message type: " + cls.getName());
        }

        @Override // defpackage.c82
        public boolean b(Class<?> cls) {
            for (c82 c82Var : this.a) {
                if (c82Var.b(cls)) {
                    return true;
                }
            }
            return false;
        }
    }

    public r32() {
        this(b());
    }

    public static c82 b() {
        return new b(t.c(), c());
    }

    public static c82 c() {
        try {
            return (c82) Class.forName("com.google.crypto.tink.shaded.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    public static boolean d(a82 a82Var) {
        return a82Var.c() == ProtoSyntax.PROTO2;
    }

    public static <T> n0<T> e(Class<T> cls, a82 a82Var) {
        if (GeneratedMessageLite.class.isAssignableFrom(cls)) {
            if (d(a82Var)) {
                return g0.Q(cls, a82Var, lf2.b(), z.b(), o0.M(), l11.b(), v32.b());
            }
            return g0.Q(cls, a82Var, lf2.b(), z.b(), o0.M(), null, v32.b());
        } else if (d(a82Var)) {
            return g0.Q(cls, a82Var, lf2.a(), z.a(), o0.H(), l11.a(), v32.a());
        } else {
            return g0.Q(cls, a82Var, lf2.a(), z.a(), o0.I(), null, v32.a());
        }
    }

    @Override // defpackage.nd3
    public <T> n0<T> a(Class<T> cls) {
        o0.J(cls);
        a82 a2 = this.a.a(cls);
        if (a2.a()) {
            if (GeneratedMessageLite.class.isAssignableFrom(cls)) {
                return h0.m(o0.M(), l11.b(), a2.b());
            }
            return h0.m(o0.H(), l11.a(), a2.b());
        }
        return e(cls, a2);
    }

    public r32(c82 c82Var) {
        this.a = (c82) v.b(c82Var, "messageInfoFactory");
    }
}
