package defpackage;

/* compiled from: SQLiteEventStore_Factory.java */
/* renamed from: rb3  reason: default package */
/* loaded from: classes.dex */
public final class rb3 implements z11<qb3> {
    public final ew2<qz> a;
    public final ew2<qz> b;
    public final ew2<ey0> c;
    public final ew2<sd3> d;

    public rb3(ew2<qz> ew2Var, ew2<qz> ew2Var2, ew2<ey0> ew2Var3, ew2<sd3> ew2Var4) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
        this.d = ew2Var4;
    }

    public static rb3 a(ew2<qz> ew2Var, ew2<qz> ew2Var2, ew2<ey0> ew2Var3, ew2<sd3> ew2Var4) {
        return new rb3(ew2Var, ew2Var2, ew2Var3, ew2Var4);
    }

    public static qb3 c(qz qzVar, qz qzVar2, Object obj, Object obj2) {
        return new qb3(qzVar, qzVar2, (ey0) obj, (sd3) obj2);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public qb3 get() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
