package defpackage;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.TypedValue;
import android.widget.ProgressBar;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.zabj;
import defpackage.dh2;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: dh1  reason: default package */
/* loaded from: classes.dex */
public class dh1 extends eh1 {
    public static final Object d = new Object();
    public static final dh1 e = new dh1();
    public String c;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    @SuppressLint({"HandlerLeak"})
    /* renamed from: dh1$a */
    /* loaded from: classes.dex */
    public class a extends q25 {
        public final Context a;

        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.a = context.getApplicationContext();
        }

        @Override // android.os.Handler
        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                return;
            }
            int i2 = dh1.this.i(this.a);
            if (dh1.this.m(i2)) {
                dh1.this.s(this.a, i2);
            }
        }
    }

    public static dh1 q() {
        return e;
    }

    @RecentlyNonNull
    public static Dialog t(@RecentlyNonNull Activity activity, @RecentlyNonNull DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(a15.g(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        x(activity, create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    public static Dialog u(Context context, int i, m05 m05Var, DialogInterface.OnCancelListener onCancelListener) {
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        AlertDialog.Builder builder = "Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId)) ? new AlertDialog.Builder(context, 5) : null;
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(a15.g(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String i2 = a15.i(context, i);
        if (i2 != null) {
            builder.setPositiveButton(i2, m05Var);
        }
        String b = a15.b(context, i);
        if (b != null) {
            builder.setTitle(b);
        }
        String.format("Creating dialog for Google Play services availability issue. ConnectionResult=%s", Integer.valueOf(i));
        new IllegalArgumentException();
        return builder.create();
    }

    public static void x(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            iw3.v(dialog, onCancelListener).u(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        ErrorDialogFragment.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    @RecentlyNonNull
    public final boolean A(@RecentlyNonNull Activity activity, @RecentlyNonNull nz1 nz1Var, @RecentlyNonNull int i, @RecentlyNonNull int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog u = u(activity, i, m05.a(nz1Var, d(activity, i, "d"), 2), onCancelListener);
        if (u == null) {
            return false;
        }
        x(activity, u, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    @RecentlyNonNull
    public final boolean B(@RecentlyNonNull Context context, @RecentlyNonNull ConnectionResult connectionResult, @RecentlyNonNull int i) {
        PendingIntent p = p(context, connectionResult);
        if (p != null) {
            z(context, connectionResult.I1(), null, GoogleApiActivity.a(context, p, i));
            return true;
        }
        return false;
    }

    @Override // defpackage.eh1
    @RecentlyNullable
    public Intent d(Context context, @RecentlyNonNull int i, String str) {
        return super.d(context, i, str);
    }

    @Override // defpackage.eh1
    @RecentlyNullable
    public PendingIntent e(@RecentlyNonNull Context context, @RecentlyNonNull int i, @RecentlyNonNull int i2) {
        return super.e(context, i, i2);
    }

    @Override // defpackage.eh1
    public final String g(@RecentlyNonNull int i) {
        return super.g(i);
    }

    @Override // defpackage.eh1
    @RecentlyNonNull
    public int i(@RecentlyNonNull Context context) {
        return super.i(context);
    }

    @Override // defpackage.eh1
    @RecentlyNonNull
    public int j(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        return super.j(context, i);
    }

    @Override // defpackage.eh1
    @RecentlyNonNull
    public final boolean m(@RecentlyNonNull int i) {
        return super.m(i);
    }

    @RecentlyNullable
    public Dialog o(@RecentlyNonNull Activity activity, @RecentlyNonNull int i, @RecentlyNonNull int i2, DialogInterface.OnCancelListener onCancelListener) {
        return u(activity, i, m05.b(activity, d(activity, i, "d"), i2), onCancelListener);
    }

    @RecentlyNullable
    public PendingIntent p(@RecentlyNonNull Context context, @RecentlyNonNull ConnectionResult connectionResult) {
        if (connectionResult.L1()) {
            return connectionResult.K1();
        }
        return e(context, connectionResult.I1(), 0);
    }

    @RecentlyNonNull
    public boolean r(@RecentlyNonNull Activity activity, @RecentlyNonNull int i, @RecentlyNonNull int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog o = o(activity, i, i2, onCancelListener);
        if (o == null) {
            return false;
        }
        x(activity, o, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    public void s(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        z(context, i, null, f(context, i, 0, "n"));
    }

    public final zabj v(Context context, t05 t05Var) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        zabj zabjVar = new zabj(t05Var);
        context.registerReceiver(zabjVar, intentFilter);
        zabjVar.b(context);
        if (l(context, "com.google.android.gms")) {
            return zabjVar;
        }
        t05Var.a();
        zabjVar.a();
        return null;
    }

    public final String w() {
        String str;
        synchronized (d) {
            str = this.c;
        }
        return str;
    }

    public final void y(Context context) {
        new a(context).sendEmptyMessageDelayed(1, 120000L);
    }

    @TargetApi(20)
    public final void z(Context context, int i, String str, PendingIntent pendingIntent) {
        int i2;
        String.format("GMS core API Availability. ConnectionResult=%s, tag=%s", Integer.valueOf(i), null);
        new IllegalArgumentException();
        if (i == 18) {
            y(context);
        } else if (pendingIntent == null) {
        } else {
            String f = a15.f(context, i);
            String h = a15.h(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) zt2.j(context.getSystemService("notification"));
            dh2.e C = new dh2.e(context).v(true).g(true).m(f).C(new dh2.c().h(h));
            if (vm0.d(context)) {
                zt2.m(jr2.e());
                C.A(context.getApplicationInfo().icon).y(2);
                if (vm0.f(context)) {
                    C.a(pz2.common_full_open_on_phone, resources.getString(p13.common_open_on_phone), pendingIntent);
                } else {
                    C.k(pendingIntent);
                }
            } else {
                C.A(17301642).D(resources.getString(p13.common_google_play_services_notification_ticker)).G(System.currentTimeMillis()).k(pendingIntent).l(h);
            }
            if (jr2.h()) {
                zt2.m(jr2.h());
                String w = w();
                if (w == null) {
                    w = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel("com.google.android.gms.availability");
                    String a2 = a15.a(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel("com.google.android.gms.availability", a2, 4));
                    } else if (!a2.contentEquals(notificationChannel.getName())) {
                        notificationChannel.setName(a2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                C.h(w);
            }
            Notification b = C.b();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                qh1.d.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, b);
        }
    }
}
