package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.b;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/* compiled from: TypeNameIdResolver.java */
/* renamed from: td4  reason: default package */
/* loaded from: classes.dex */
public class td4 extends pd4 {
    public final MapperConfig<?> c;
    public final Map<String, String> d;
    public final Map<String, JavaType> e;

    public td4(MapperConfig<?> mapperConfig, JavaType javaType, Map<String, String> map, Map<String, JavaType> map2) {
        super(javaType, mapperConfig.getTypeFactory());
        this.c = mapperConfig;
        this.d = map;
        this.e = map2;
    }

    public static String g(Class<?> cls) {
        String name = cls.getName();
        int lastIndexOf = name.lastIndexOf(46);
        return lastIndexOf < 0 ? name : name.substring(lastIndexOf + 1);
    }

    public static td4 i(MapperConfig<?> mapperConfig, JavaType javaType, Collection<NamedType> collection, boolean z, boolean z2) {
        JavaType javaType2;
        if (z != z2) {
            HashMap hashMap = null;
            Map hashMap2 = z ? new HashMap() : null;
            if (z2) {
                hashMap = new HashMap();
                hashMap2 = new TreeMap();
            }
            if (collection != null) {
                for (NamedType namedType : collection) {
                    Class<?> type = namedType.getType();
                    String name = namedType.hasName() ? namedType.getName() : g(type);
                    if (z) {
                        hashMap2.put(type.getName(), name);
                    }
                    if (z2 && ((javaType2 = (JavaType) hashMap.get(name)) == null || !type.isAssignableFrom(javaType2.getRawClass()))) {
                        hashMap.put(name, mapperConfig.constructType(type));
                    }
                }
            }
            return new td4(mapperConfig, javaType, hashMap2, hashMap);
        }
        throw new IllegalArgumentException();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String a(Object obj) {
        return j(obj.getClass());
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String b() {
        return new TreeSet(this.e.keySet()).toString();
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public JavaType d(b bVar, String str) {
        return h(str);
    }

    @Override // com.fasterxml.jackson.databind.jsontype.b
    public String e(Object obj, Class<?> cls) {
        if (obj == null) {
            return j(cls);
        }
        return a(obj);
    }

    public JavaType h(String str) {
        return this.e.get(str);
    }

    public String j(Class<?> cls) {
        String str;
        if (cls == null) {
            return null;
        }
        Class<?> rawClass = this.a.constructType(cls).getRawClass();
        String name = rawClass.getName();
        synchronized (this.d) {
            str = this.d.get(name);
            if (str == null) {
                if (this.c.isAnnotationProcessingEnabled()) {
                    str = this.c.getAnnotationIntrospector().findTypeName(this.c.introspectClassAnnotations(rawClass).t());
                }
                if (str == null) {
                    str = g(rawClass);
                }
                this.d.put(name, str);
            }
        }
        return str;
    }

    public String toString() {
        return String.format("[%s; id-to-type=%s]", td4.class.getName(), this.e);
    }
}
