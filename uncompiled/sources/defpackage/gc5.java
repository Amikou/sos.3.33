package defpackage;

import com.google.android.gms.internal.clearcut.zzbb;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: gc5  reason: default package */
/* loaded from: classes.dex */
public final class gc5 extends n65<String> implements jc5, RandomAccess {
    public static final gc5 g0;
    public final List<Object> f0;

    static {
        gc5 gc5Var = new gc5();
        g0 = gc5Var;
        gc5Var.v();
    }

    public gc5() {
        this(10);
    }

    public gc5(int i) {
        this(new ArrayList(i));
    }

    public gc5(ArrayList<Object> arrayList) {
        this.f0 = arrayList;
    }

    public static String i(Object obj) {
        return obj instanceof String ? (String) obj : obj instanceof zzbb ? ((zzbb) obj).zzz() : gb5.h((byte[]) obj);
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5 T0(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f0);
            return new gc5(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        e();
        this.f0.add(i, (String) obj);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.List
    public final boolean addAll(int i, Collection<? extends String> collection) {
        e();
        if (collection instanceof jc5) {
            collection = ((jc5) collection).b0();
        }
        boolean addAll = this.f0.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @Override // defpackage.jc5
    public final List<?> b0() {
        return Collections.unmodifiableList(this.f0);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final void clear() {
        e();
        this.f0.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        Object obj = this.f0.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzbb) {
            zzbb zzbbVar = (zzbb) obj;
            String zzz = zzbbVar.zzz();
            if (zzbbVar.zzaa()) {
                this.f0.set(i, zzz);
            }
            return zzz;
        }
        byte[] bArr = (byte[]) obj;
        String h = gb5.h(bArr);
        if (gb5.g(bArr)) {
            this.f0.set(i, h);
        }
        return h;
    }

    @Override // defpackage.jc5
    public final Object j(int i) {
        return this.f0.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        Object remove = this.f0.remove(i);
        ((AbstractList) this).modCount++;
        return i(remove);
    }

    @Override // defpackage.jc5
    public final jc5 s1() {
        return u() ? new ki5(this) : this;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        e();
        return i(this.f0.set(i, (String) obj));
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.f0.size();
    }
}
