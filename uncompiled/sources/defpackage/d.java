package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;

/* compiled from: AKTAES.java */
/* renamed from: d  reason: default package */
/* loaded from: classes.dex */
public class d {
    public int[] a = new int[64];
    public short[] b = {99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22};
    public short[] c = {82, 9, 106, 213, 48, 54, 165, 56, 191, 64, 163, 158, 129, 243, 215, 251, 124, 227, 57, 130, 155, 47, 255, 135, 52, 142, 67, 68, 196, 222, 233, 203, 84, 123, 148, 50, 166, 194, 35, 61, 238, 76, 149, 11, 66, 250, 195, 78, 8, 46, 161, 102, 40, 217, 36, 178, 118, 91, 162, 73, 109, 139, 209, 37, 114, 248, 246, 100, 134, 104, 152, 22, 212, 164, 92, 204, 93, 101, 182, 146, 108, 112, 72, 80, 253, 237, 185, 218, 94, 21, 70, 87, 167, 141, 157, 132, 144, 216, 171, 0, 140, 188, 211, 10, 247, 228, 88, 5, 184, 179, 69, 6, 208, 44, 30, 143, 202, 63, 15, 2, 193, 175, 189, 3, 1, 19, 138, 107, 58, 145, 17, 65, 79, 103, 220, 234, 151, 242, 207, 206, 240, 180, 230, 115, 150, 172, 116, 34, 231, 173, 53, 133, 226, 249, 55, 232, 28, 117, 223, 110, 71, 241, 26, 113, 29, 41, 197, 137, 111, 183, 98, 14, 170, 24, 190, 27, 252, 86, 62, 75, 198, 210, 121, 32, 154, 219, 192, 254, 120, 205, 90, 244, 31, 221, 168, 51, 136, 7, 199, 49, 177, 18, 16, 89, 39, 128, 236, 95, 96, 81, 127, 169, 25, 181, 74, 13, 45, 229, 122, 159, 147, 201, 156, 239, 160, 224, 59, 77, 174, 42, 245, 176, 200, 235, 187, 60, 131, 83, 153, 97, 23, 43, 4, 126, 186, 119, 214, 38, 225, 105, 20, 99, 85, 33, 12, 125};
    public int[] d = {1, 2, 4, 8, 16, 32, 64, 128, 27, 54, 108, 216, 171, 77, 154, 47, 94, 188, 99, 198, 151, 53, 106, 212, 179, 125, 250, 239};

    public final int a(int i) {
        int i2 = (-2139062144) & i;
        return ((i & 2139062143) << 1) ^ ((i2 - ((i2 >> 7) & 33554431)) & 454761243);
    }

    public boolean b(byte[] bArr) {
        int length = bArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = (char) bArr[i];
        }
        return c(cArr);
    }

    public boolean c(char[] cArr) {
        int i;
        int i2;
        int i3;
        char[] cArr2 = new char[33];
        int length = cArr.length;
        d();
        int i4 = 0;
        int i5 = 0;
        while (i4 < 32) {
            if (i5 >= length) {
                i5 = 0;
            }
            int i6 = i5 + 1;
            cArr2[i4] = cArr[i5];
            if (i6 >= length) {
                if (i4 < 32) {
                    i4++;
                    cArr2[i4] = 202;
                }
                i5 = 0;
            } else {
                i5 = i6;
            }
            i4++;
        }
        System.out.println(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        byte[] bArr = new byte[32];
        for (int i7 = 0; i7 < 32; i7++) {
            bArr[i7] = (byte) cArr2[i7];
        }
        ay1.d(bArr);
        for (int i8 = 0; i8 < 8; i8++) {
            this.a[i8] = ay1.p(cArr2, i8 * 4);
        }
        byte[] bArr2 = new byte[4];
        int i9 = 0;
        int i10 = 0;
        while (i10 < 56) {
            int i11 = i10 + 8;
            int q = ay1.q(3, this.a[i11 - 1]);
            int i12 = 0;
            while (i12 < 8) {
                if (i12 == 0) {
                    i2 = i9 + 1;
                    i3 = this.d[i9] ^ i(q);
                } else {
                    if (i12 == 4) {
                        i = i(this.a[i10 + 11]);
                    } else {
                        i = this.a[((i10 + i12) + 8) - 1];
                    }
                    i2 = i9;
                    i3 = i;
                }
                int[] iArr = this.a;
                int i13 = i10 + i12;
                int i14 = i13 + 8;
                iArr[i14] = i3 ^ iArr[i13];
                ay1.k(bArr2, 0, iArr[i14]);
                i12++;
                i9 = i2;
            }
            i10 = i11;
        }
        return true;
    }

    public void d() {
        for (int i = 0; i < 64; i++) {
            this.a[i] = 0;
        }
    }

    public void e(byte[] bArr, byte[] bArr2, int i) {
        int[] iArr = new int[4];
        int[] iArr2 = new int[4];
        int i2 = (i + 7) * 4;
        for (int i3 = 3; i3 >= 0; i3--) {
            i2--;
            iArr[i3] = ay1.o(bArr, i3 * 4) ^ this.a[i2];
        }
        for (int i4 = 0; i4 < i + 5; i4++) {
            for (int i5 = 3; i5 >= 0; i5--) {
                i2--;
                iArr2[i5] = h(i5, iArr) ^ this.a[i2];
            }
            for (int i6 = 3; i6 >= 0; i6--) {
                iArr[i6] = g(iArr2[i6]);
            }
        }
        for (int i7 = 3; i7 >= 0; i7--) {
            i2--;
            ay1.k(bArr2, i7 * 4, h(i7, iArr) ^ this.a[i2]);
        }
    }

    public void f(byte[] bArr, byte[] bArr2, int i) {
        int[] iArr = new int[4];
        int[] iArr2 = new int[4];
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i3 < 4) {
            iArr[i3] = this.a[i4] ^ ay1.o(bArr, i3 * 4);
            i3++;
            i4++;
        }
        int i5 = i4;
        for (int i6 = 0; i6 < i + 5; i6++) {
            for (int i7 = 0; i7 < 4; i7++) {
                iArr2[i7] = k(i7, iArr);
            }
            int i8 = 0;
            while (i8 < 4) {
                iArr[i8] = j(iArr2[i8]) ^ this.a[i5];
                i8++;
                i5++;
            }
        }
        while (i2 < 4) {
            ay1.k(bArr2, i2 * 4, k(i2, iArr) ^ this.a[i5]);
            i2++;
            i5++;
        }
    }

    public final int g(int i) {
        int a = a(i);
        int a2 = a(a);
        int a3 = a(a2);
        int i2 = i ^ a3;
        int q = ay1.q(3, a ^ i2);
        return ay1.q(1, i2) ^ ((q ^ (a3 ^ (a ^ a2))) ^ ay1.q(2, a2 ^ i2));
    }

    public final int h(int i, int[] iArr) {
        return ay1.i(this.c[ay1.l(0, iArr[i])], this.c[ay1.l(1, iArr[(i + 3) % 4])], this.c[ay1.l(2, iArr[(i + 2) % 4])], this.c[ay1.l(3, iArr[(i + 1) % 4])]);
    }

    public final int i(int i) {
        return ay1.i(this.b[ay1.l(0, i)], this.b[ay1.l(1, i)], this.b[ay1.l(2, i)], this.b[ay1.l(3, i)]);
    }

    public final int j(int i) {
        int a = a(i);
        return ay1.q(1, i) ^ ((a ^ ay1.q(3, i ^ a)) ^ ay1.q(2, i));
    }

    public final int k(int i, int[] iArr) {
        return ay1.i(this.b[ay1.l(0, iArr[i])], this.b[ay1.l(1, iArr[(i + 1) % 4])], this.b[ay1.l(2, iArr[(i + 2) % 4])], this.b[ay1.l(3, iArr[(i + 3) % 4])]);
    }
}
