package defpackage;

import android.content.SharedPreferences;
import android.util.Pair;
import defpackage.ba;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: mi5  reason: default package */
/* loaded from: classes.dex */
public final class mi5 extends sl5 {
    public static final Pair<String, Long> x = new Pair<>("", 0L);
    public SharedPreferences c;
    public rh5 d;
    public final ph5 e;
    public final ph5 f;
    public final ji5 g;
    public String h;
    public boolean i;
    public long j;
    public final ph5 k;
    public final nh5 l;
    public final ji5 m;
    public final nh5 n;
    public final ph5 o;
    public boolean p;
    public final nh5 q;
    public final nh5 r;
    public final ph5 s;
    public final ji5 t;
    public final ji5 u;
    public final ph5 v;
    public final oh5 w;

    public mi5(ck5 ck5Var) {
        super(ck5Var);
        this.k = new ph5(this, "session_timeout", 1800000L);
        this.l = new nh5(this, "start_new_session", true);
        this.o = new ph5(this, "last_pause_time", 0L);
        this.m = new ji5(this, "non_personalized_ads", null);
        this.n = new nh5(this, "allow_remote_dynamite", false);
        this.e = new ph5(this, "first_open_time", 0L);
        this.f = new ph5(this, "app_install_time", 0L);
        this.g = new ji5(this, "app_instance_id", null);
        this.q = new nh5(this, "app_backgrounded", false);
        this.r = new nh5(this, "deep_link_retrieval_complete", false);
        this.s = new ph5(this, "deep_link_retrieval_attempts", 0L);
        this.t = new ji5(this, "firebase_feature_rollouts", null);
        this.u = new ji5(this, "deferred_attribution_cache", null);
        this.v = new ph5(this, "deferred_attribution_cache_timestamp", 0L);
        this.w = new oh5(this, "default_event_parameters", null);
    }

    @Override // defpackage.sl5
    public final boolean f() {
        return true;
    }

    @Override // defpackage.sl5
    public final void g() {
        SharedPreferences sharedPreferences = this.a.m().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.c = sharedPreferences;
        boolean z = sharedPreferences.getBoolean("has_been_opened", false);
        this.p = z;
        if (!z) {
            SharedPreferences.Editor edit = this.c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.a.z();
        this.d = new rh5(this, "health_monitor", Math.max(0L, qf5.c.b(null).longValue()), null);
    }

    public final Pair<String, Boolean> l(String str) {
        e();
        long b = this.a.a().b();
        String str2 = this.h;
        if (str2 != null && b < this.j) {
            return new Pair<>(str2, Boolean.valueOf(this.i));
        }
        this.j = b + this.a.z().r(str, qf5.b);
        ba.d(true);
        try {
            ba.a b2 = ba.b(this.a.m());
            this.h = "";
            String a = b2.a();
            if (a != null) {
                this.h = a;
            }
            this.i = b2.b();
        } catch (Exception e) {
            this.a.w().u().b("Unable to get advertising id", e);
            this.h = "";
        }
        ba.d(false);
        return new Pair<>(this.h, Boolean.valueOf(this.i));
    }

    public final SharedPreferences n() {
        e();
        i();
        zt2.j(this.c);
        return this.c;
    }

    public final void o(Boolean bool) {
        e();
        SharedPreferences.Editor edit = n().edit();
        if (bool != null) {
            edit.putBoolean("measurement_enabled", bool.booleanValue());
        } else {
            edit.remove("measurement_enabled");
        }
        edit.apply();
    }

    public final Boolean p() {
        e();
        if (n().contains("measurement_enabled")) {
            return Boolean.valueOf(n().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    public final boolean r(int i) {
        return t45.m(i, n().getInt("consent_source", 100));
    }

    public final t45 s() {
        e();
        return t45.c(n().getString("consent_settings", "G1"));
    }

    public final void t(boolean z) {
        e();
        this.a.w().v().b("App measurement setting deferred collection", Boolean.valueOf(z));
        SharedPreferences.Editor edit = n().edit();
        edit.putBoolean("deferred_analytics_collection", z);
        edit.apply();
    }

    public final boolean u() {
        SharedPreferences sharedPreferences = this.c;
        if (sharedPreferences == null) {
            return false;
        }
        return sharedPreferences.contains("deferred_analytics_collection");
    }

    public final boolean v(long j) {
        return j - this.k.a() > this.o.a();
    }
}
