package defpackage;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import kotlin.coroutines.CoroutineContext;

/* renamed from: xt1  reason: default package */
/* loaded from: classes2.dex */
public final class xt1 {
    public static final q30 a(st1 st1Var) {
        return zt1.a(st1Var);
    }

    public static final void c(st1 st1Var, String str, Throwable th) {
        zt1.c(st1Var, str, th);
    }

    public static final void d(CoroutineContext coroutineContext, CancellationException cancellationException) {
        zt1.d(coroutineContext, cancellationException);
    }

    public static final Object g(st1 st1Var, q70<? super te4> q70Var) {
        return zt1.g(st1Var, q70Var);
    }

    public static final void h(ov<?> ovVar, Future<?> future) {
        yt1.a(ovVar, future);
    }

    public static final void i(st1 st1Var) {
        zt1.h(st1Var);
    }

    public static final void j(CoroutineContext coroutineContext) {
        zt1.i(coroutineContext);
    }
}
