package defpackage;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: mn5  reason: default package */
/* loaded from: classes.dex */
public final class mn5 implements Runnable {
    public final /* synthetic */ long a;
    public final /* synthetic */ dp5 f0;

    public mn5(dp5 dp5Var, long j) {
        this.f0 = dp5Var;
        this.a = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.s(this.a, true);
        this.f0.a.R().T(new AtomicReference<>());
    }
}
