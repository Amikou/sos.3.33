package defpackage;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.l;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: aa5  reason: default package */
/* loaded from: classes.dex */
public final class aa5 extends l {
    public final AtomicReference<Bundle> a = new AtomicReference<>();
    public boolean b;

    public static final <T> T H1(Bundle bundle, Class<T> cls) {
        Object obj;
        if (bundle == null || (obj = bundle.get("r")) == null) {
            return null;
        }
        try {
            return cls.cast(obj);
        } catch (ClassCastException e) {
            String.format("Unexpected object type. Expected, Received: %s, %s", cls.getCanonicalName(), obj.getClass().getCanonicalName());
            throw e;
        }
    }

    public final String F1(long j) {
        return (String) H1(G1(j), String.class);
    }

    public final Bundle G1(long j) {
        Bundle bundle;
        synchronized (this.a) {
            if (!this.b) {
                try {
                    this.a.wait(j);
                } catch (InterruptedException unused) {
                    return null;
                }
            }
            bundle = this.a.get();
        }
        return bundle;
    }

    @Override // com.google.android.gms.internal.measurement.m
    public final void K0(Bundle bundle) {
        synchronized (this.a) {
            this.a.set(bundle);
            this.b = true;
            this.a.notify();
        }
    }
}
