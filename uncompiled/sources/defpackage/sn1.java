package defpackage;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import defpackage.sn1;

/* compiled from: ImageDecodeOptionsBuilder.java */
/* renamed from: sn1  reason: default package */
/* loaded from: classes.dex */
public class sn1<T extends sn1> {
    public int a = 100;
    public int b = Integer.MAX_VALUE;
    public boolean c;
    public boolean d;
    public boolean e;
    public boolean f;
    public Bitmap.Config g;
    public Bitmap.Config h;
    public tn1 i;
    public pq j;
    public ColorSpace k;
    public boolean l;

    public sn1() {
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        this.g = config;
        this.h = config;
    }

    public rn1 a() {
        return new rn1(this);
    }

    public Bitmap.Config b() {
        return this.h;
    }

    public Bitmap.Config c() {
        return this.g;
    }

    public pq d() {
        return this.j;
    }

    public ColorSpace e() {
        return this.k;
    }

    public tn1 f() {
        return this.i;
    }

    public boolean g() {
        return this.e;
    }

    public boolean h() {
        return this.c;
    }

    public boolean i() {
        return this.l;
    }

    public boolean j() {
        return this.f;
    }

    public int k() {
        return this.b;
    }

    public int l() {
        return this.a;
    }

    public boolean m() {
        return this.d;
    }
}
