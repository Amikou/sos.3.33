package defpackage;

/* compiled from: FrescoSystrace.java */
/* renamed from: nc1  reason: default package */
/* loaded from: classes.dex */
public class nc1 {
    public static volatile c a;

    /* compiled from: FrescoSystrace.java */
    /* renamed from: nc1$b */
    /* loaded from: classes.dex */
    public static final class b {
        public b() {
        }
    }

    /* compiled from: FrescoSystrace.java */
    /* renamed from: nc1$c */
    /* loaded from: classes.dex */
    public interface c {
        void a(String str);

        boolean b();

        void c();
    }

    static {
        new b();
        a = null;
    }

    public static void a(String str) {
        c().a(str);
    }

    public static void b() {
        c().c();
    }

    public static c c() {
        if (a == null) {
            synchronized (nc1.class) {
                if (a == null) {
                    a = new mj0();
                }
            }
        }
        return a;
    }

    public static boolean d() {
        return c().b();
    }
}
