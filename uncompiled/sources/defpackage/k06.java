package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: k06  reason: default package */
/* loaded from: classes.dex */
public final class k06 implements yp5<l06> {
    public static final k06 f0 = new k06();
    public final yp5<l06> a = gq5.a(gq5.b(new m06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final l06 zza() {
        return this.a.zza();
    }
}
