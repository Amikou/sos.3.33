package defpackage;

import java.util.ArrayList;

/* compiled from: RlpDecoder.java */
/* renamed from: c93  reason: default package */
/* loaded from: classes3.dex */
public class c93 {
    public static int OFFSET_LONG_LIST = 247;
    public static int OFFSET_LONG_STRING = 183;
    public static int OFFSET_SHORT_LIST = 192;
    public static int OFFSET_SHORT_STRING = 128;

    private static int calcLength(int i, byte[] bArr, int i2) {
        byte b = (byte) (i - 1);
        int i3 = 0;
        for (int i4 = 1; i4 <= i; i4++) {
            i3 += (bArr[i2 + i4] & 255) << (b * 8);
            b = (byte) (b - 1);
        }
        return i3;
    }

    public static e93 decode(byte[] bArr) {
        e93 e93Var = new e93(new ArrayList());
        traverse(bArr, 0, bArr.length, e93Var);
        return e93Var;
    }

    private static void traverse(byte[] bArr, int i, int i2, e93 e93Var) {
        byte b;
        int calcLength;
        int i3;
        int i4;
        if (bArr != null) {
            try {
                if (bArr.length == 0) {
                    return;
                }
                while (i < i2) {
                    int i5 = bArr[i] & 255;
                    int i6 = OFFSET_SHORT_STRING;
                    if (i5 < i6) {
                        e93Var.getValues().add(f93.create(new byte[]{(byte) i5}));
                    } else if (i5 == i6) {
                        e93Var.getValues().add(f93.create(new byte[0]));
                    } else {
                        if (i5 > i6 && i5 <= OFFSET_LONG_STRING) {
                            i3 = (byte) (i5 - i6);
                            byte[] bArr2 = new byte[i3];
                            System.arraycopy(bArr, i + 1, bArr2, 0, i3);
                            e93Var.getValues().add(f93.create(bArr2));
                        } else {
                            int i7 = OFFSET_LONG_STRING;
                            if (i5 > i7 && i5 < OFFSET_SHORT_LIST) {
                                b = (byte) (i5 - i7);
                                calcLength = calcLength(b, bArr, i);
                                byte[] bArr3 = new byte[calcLength];
                                System.arraycopy(bArr, i + b + 1, bArr3, 0, calcLength);
                                e93Var.getValues().add(f93.create(bArr3));
                            } else {
                                int i8 = OFFSET_SHORT_LIST;
                                if (i5 >= i8 && i5 <= OFFSET_LONG_LIST) {
                                    i3 = (byte) (i5 - i8);
                                    e93 e93Var2 = new e93(new ArrayList());
                                    traverse(bArr, i + 1, i + i3 + 1, e93Var2);
                                    e93Var.getValues().add(e93Var2);
                                } else {
                                    int i9 = OFFSET_LONG_LIST;
                                    if (i5 > i9) {
                                        b = (byte) (i5 - i9);
                                        calcLength = calcLength(b, bArr, i);
                                        e93 e93Var3 = new e93(new ArrayList());
                                        int i10 = i + b;
                                        traverse(bArr, i10 + 1, i10 + calcLength + 1, e93Var3);
                                        e93Var.getValues().add(e93Var3);
                                    }
                                }
                            }
                            i4 = b + calcLength + 1;
                            i += i4;
                        }
                        i4 = i3 + 1;
                        i += i4;
                    }
                    i++;
                }
            } catch (Exception e) {
                throw new RuntimeException("RLP wrong encoding", e);
            }
        }
    }
}
