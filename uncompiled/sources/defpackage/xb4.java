package defpackage;

/* compiled from: TransportRuntime_Factory.java */
/* renamed from: xb4  reason: default package */
/* loaded from: classes.dex */
public final class xb4 implements z11<vb4> {
    public final ew2<qz> a;
    public final ew2<qz> b;
    public final ew2<ad3> c;
    public final ew2<mf4> d;
    public final ew2<eq4> e;

    public xb4(ew2<qz> ew2Var, ew2<qz> ew2Var2, ew2<ad3> ew2Var3, ew2<mf4> ew2Var4, ew2<eq4> ew2Var5) {
        this.a = ew2Var;
        this.b = ew2Var2;
        this.c = ew2Var3;
        this.d = ew2Var4;
        this.e = ew2Var5;
    }

    public static xb4 a(ew2<qz> ew2Var, ew2<qz> ew2Var2, ew2<ad3> ew2Var3, ew2<mf4> ew2Var4, ew2<eq4> ew2Var5) {
        return new xb4(ew2Var, ew2Var2, ew2Var3, ew2Var4, ew2Var5);
    }

    public static vb4 c(qz qzVar, qz qzVar2, ad3 ad3Var, mf4 mf4Var, eq4 eq4Var) {
        return new vb4(qzVar, qzVar2, ad3Var, mf4Var, eq4Var);
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public vb4 get() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
