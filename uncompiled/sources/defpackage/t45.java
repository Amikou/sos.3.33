package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: t45  reason: default package */
/* loaded from: classes.dex */
public final class t45 {
    public static final t45 c = new t45(null, null);
    public final Boolean a;
    public final Boolean b;

    public t45(Boolean bool, Boolean bool2) {
        this.a = bool;
        this.b = bool2;
    }

    public static String a(Bundle bundle) {
        String string = bundle.getString("ad_storage");
        if (string == null || o(string) != null) {
            String string2 = bundle.getString("analytics_storage");
            if (string2 == null || o(string2) != null) {
                return null;
            }
            return string2;
        }
        return string;
    }

    public static t45 b(Bundle bundle) {
        return bundle == null ? c : new t45(o(bundle.getString("ad_storage")), o(bundle.getString("analytics_storage")));
    }

    public static t45 c(String str) {
        Boolean bool;
        if (str != null) {
            Boolean p = str.length() >= 3 ? p(str.charAt(2)) : null;
            bool = str.length() >= 4 ? p(str.charAt(3)) : null;
            r0 = p;
        } else {
            bool = null;
        }
        return new t45(r0, bool);
    }

    public static Boolean j(Boolean bool, Boolean bool2) {
        if (bool == null) {
            return bool2;
        }
        if (bool2 == null) {
            return bool;
        }
        boolean z = false;
        if (bool.booleanValue() && bool2.booleanValue()) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    public static boolean m(int i, int i2) {
        return i <= i2;
    }

    public static final int n(Boolean bool) {
        if (bool == null) {
            return 0;
        }
        return bool.booleanValue() ? 1 : 2;
    }

    public static Boolean o(String str) {
        if (str == null) {
            return null;
        }
        if (str.equals("granted")) {
            return Boolean.TRUE;
        }
        if (str.equals("denied")) {
            return Boolean.FALSE;
        }
        return null;
    }

    public static Boolean p(char c2) {
        if (c2 != '0') {
            if (c2 != '1') {
                return null;
            }
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static final char q(Boolean bool) {
        if (bool == null) {
            return '-';
        }
        return bool.booleanValue() ? '1' : '0';
    }

    public final String d() {
        return "G1" + q(this.a) + q(this.b);
    }

    public final Boolean e() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof t45) {
            t45 t45Var = (t45) obj;
            return n(this.a) == n(t45Var.a) && n(this.b) == n(t45Var.b);
        }
        return false;
    }

    public final boolean f() {
        Boolean bool = this.a;
        return bool == null || bool.booleanValue();
    }

    public final Boolean g() {
        return this.b;
    }

    public final boolean h() {
        Boolean bool = this.b;
        return bool == null || bool.booleanValue();
    }

    public final int hashCode() {
        return ((n(this.a) + 527) * 31) + n(this.b);
    }

    public final boolean i(t45 t45Var) {
        Boolean bool = this.a;
        Boolean bool2 = Boolean.FALSE;
        if (bool != bool2 || t45Var.a == bool2) {
            return this.b == bool2 && t45Var.b != bool2;
        }
        return true;
    }

    public final t45 k(t45 t45Var) {
        return new t45(j(this.a, t45Var.a), j(this.b, t45Var.b));
    }

    public final t45 l(t45 t45Var) {
        Boolean bool = this.a;
        if (bool == null) {
            bool = t45Var.a;
        }
        Boolean bool2 = this.b;
        if (bool2 == null) {
            bool2 = t45Var.b;
        }
        return new t45(bool, bool2);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ConsentSettings: ");
        sb.append("adStorage=");
        Boolean bool = this.a;
        if (bool == null) {
            sb.append("uninitialized");
        } else {
            sb.append(true != bool.booleanValue() ? "denied" : "granted");
        }
        sb.append(", analyticsStorage=");
        Boolean bool2 = this.b;
        if (bool2 == null) {
            sb.append("uninitialized");
        } else {
            sb.append(true == bool2.booleanValue() ? "granted" : "denied");
        }
        return sb.toString();
    }
}
