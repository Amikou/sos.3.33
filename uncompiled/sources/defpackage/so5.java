package defpackage;

import com.google.android.gms.internal.vision.zzht;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: so5  reason: default package */
/* loaded from: classes.dex */
public final class so5 extends xo5 {
    public int a = 0;
    public final int f0;
    public final /* synthetic */ zzht g0;

    public so5(zzht zzhtVar) {
        this.g0 = zzhtVar;
        this.f0 = zzhtVar.zza();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a < this.f0;
    }

    @Override // defpackage.hp5
    public final byte zza() {
        int i = this.a;
        if (i < this.f0) {
            this.a = i + 1;
            return this.g0.zzb(i);
        }
        throw new NoSuchElementException();
    }
}
