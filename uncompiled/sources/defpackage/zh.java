package defpackage;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/* compiled from: _ArraysJvm.kt */
/* renamed from: zh  reason: default package */
/* loaded from: classes2.dex */
public class zh extends yh {
    public static final <T> List<T> c(T[] tArr) {
        fs1.f(tArr, "$this$asList");
        List<T> a = bi.a(tArr);
        fs1.e(a, "ArraysUtilJVM.asList(this)");
        return a;
    }

    public static final byte[] d(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        fs1.f(bArr, "$this$copyInto");
        fs1.f(bArr2, "destination");
        System.arraycopy(bArr, i2, bArr2, i, i3 - i2);
        return bArr2;
    }

    public static final <T> T[] e(T[] tArr, T[] tArr2, int i, int i2, int i3) {
        fs1.f(tArr, "$this$copyInto");
        fs1.f(tArr2, "destination");
        System.arraycopy(tArr, i2, tArr2, i, i3 - i2);
        return tArr2;
    }

    public static /* synthetic */ byte[] f(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = bArr.length;
        }
        return d(bArr, bArr2, i, i2, i3);
    }

    public static /* synthetic */ Object[] g(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = objArr.length;
        }
        return e(objArr, objArr2, i, i2, i3);
    }

    public static final byte[] h(byte[] bArr, int i, int i2) {
        fs1.f(bArr, "$this$copyOfRangeImpl");
        xh.b(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        fs1.e(copyOfRange, "java.util.Arrays.copyOfR…this, fromIndex, toIndex)");
        return copyOfRange;
    }

    public static final <T> T[] i(T[] tArr, int i, int i2) {
        fs1.f(tArr, "$this$copyOfRangeImpl");
        xh.b(i2, tArr.length);
        T[] tArr2 = (T[]) Arrays.copyOfRange(tArr, i, i2);
        fs1.e(tArr2, "java.util.Arrays.copyOfR…this, fromIndex, toIndex)");
        return tArr2;
    }

    public static final void j(int[] iArr, int i, int i2, int i3) {
        fs1.f(iArr, "$this$fill");
        Arrays.fill(iArr, i2, i3, i);
    }

    public static final <T> void k(T[] tArr, T t, int i, int i2) {
        fs1.f(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    public static /* synthetic */ void l(int[] iArr, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = iArr.length;
        }
        j(iArr, i, i2, i3);
    }

    public static /* synthetic */ void m(Object[] objArr, Object obj, int i, int i2, int i3, Object obj2) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = objArr.length;
        }
        k(objArr, obj, i, i2);
    }

    public static final byte[] n(byte[] bArr, byte[] bArr2) {
        fs1.f(bArr, "$this$plus");
        fs1.f(bArr2, "elements");
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + length2);
        System.arraycopy(bArr2, 0, copyOf, length, length2);
        fs1.e(copyOf, "result");
        return copyOf;
    }

    public static final <T> void o(T[] tArr, Comparator<? super T> comparator) {
        fs1.f(tArr, "$this$sortWith");
        fs1.f(comparator, "comparator");
        if (tArr.length > 1) {
            Arrays.sort(tArr, comparator);
        }
    }
}
