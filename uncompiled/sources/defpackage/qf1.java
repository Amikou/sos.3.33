package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

/* compiled from: GhostViewPort.java */
@SuppressLint({"ViewConstructor"})
/* renamed from: qf1  reason: default package */
/* loaded from: classes.dex */
public class qf1 extends ViewGroup implements nf1 {
    public ViewGroup a;
    public View f0;
    public final View g0;
    public int h0;
    public Matrix i0;
    public final ViewTreeObserver.OnPreDrawListener j0;

    /* compiled from: GhostViewPort.java */
    /* renamed from: qf1$a */
    /* loaded from: classes.dex */
    public class a implements ViewTreeObserver.OnPreDrawListener {
        public a() {
        }

        @Override // android.view.ViewTreeObserver.OnPreDrawListener
        public boolean onPreDraw() {
            View view;
            ei4.j0(qf1.this);
            qf1 qf1Var = qf1.this;
            ViewGroup viewGroup = qf1Var.a;
            if (viewGroup == null || (view = qf1Var.f0) == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            ei4.j0(qf1.this.a);
            qf1 qf1Var2 = qf1.this;
            qf1Var2.a = null;
            qf1Var2.f0 = null;
            return true;
        }
    }

    public qf1(View view) {
        super(view.getContext());
        this.j0 = new a();
        this.g0 = view;
        setWillNotDraw(false);
        setLayerType(2, null);
    }

    public static qf1 b(View view, ViewGroup viewGroup, Matrix matrix) {
        of1 of1Var;
        if (view.getParent() instanceof ViewGroup) {
            of1 b = of1.b(viewGroup);
            qf1 e = e(view);
            int i = 0;
            if (e != null && (of1Var = (of1) e.getParent()) != b) {
                i = e.h0;
                of1Var.removeView(e);
                e = null;
            }
            if (e == null) {
                if (matrix == null) {
                    matrix = new Matrix();
                    c(view, viewGroup, matrix);
                }
                e = new qf1(view);
                e.h(matrix);
                if (b == null) {
                    b = new of1(viewGroup);
                } else {
                    b.g();
                }
                d(viewGroup, b);
                d(viewGroup, e);
                b.a(e);
                e.h0 = i;
            } else if (matrix != null) {
                e.h(matrix);
            }
            e.h0++;
            return e;
        }
        throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
    }

    public static void c(View view, ViewGroup viewGroup, Matrix matrix) {
        ViewGroup viewGroup2 = (ViewGroup) view.getParent();
        matrix.reset();
        nk4.j(viewGroup2, matrix);
        matrix.preTranslate(-viewGroup2.getScrollX(), -viewGroup2.getScrollY());
        nk4.k(viewGroup, matrix);
    }

    public static void d(View view, View view2) {
        nk4.g(view2, view2.getLeft(), view2.getTop(), view2.getLeft() + view.getWidth(), view2.getTop() + view.getHeight());
    }

    public static qf1 e(View view) {
        return (qf1) view.getTag(zz2.ghost_view);
    }

    public static void f(View view) {
        qf1 e = e(view);
        if (e != null) {
            int i = e.h0 - 1;
            e.h0 = i;
            if (i <= 0) {
                ((of1) e.getParent()).removeView(e);
            }
        }
    }

    public static void g(View view, qf1 qf1Var) {
        view.setTag(zz2.ghost_view, qf1Var);
    }

    @Override // defpackage.nf1
    public void a(ViewGroup viewGroup, View view) {
        this.a = viewGroup;
        this.f0 = view;
    }

    public void h(Matrix matrix) {
        this.i0 = matrix;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        g(this.g0, this);
        this.g0.getViewTreeObserver().addOnPreDrawListener(this.j0);
        nk4.i(this.g0, 4);
        if (this.g0.getParent() != null) {
            ((View) this.g0.getParent()).invalidate();
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        this.g0.getViewTreeObserver().removeOnPreDrawListener(this.j0);
        nk4.i(this.g0, 0);
        g(this.g0, null);
        if (this.g0.getParent() != null) {
            ((View) this.g0.getParent()).invalidate();
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        yv.a(canvas, true);
        canvas.setMatrix(this.i0);
        nk4.i(this.g0, 0);
        this.g0.invalidate();
        nk4.i(this.g0, 4);
        drawChild(canvas, this.g0, getDrawingTime());
        yv.a(canvas, false);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @Override // android.view.View, defpackage.nf1
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (e(this.g0) == this) {
            nk4.i(this.g0, i == 0 ? 4 : 0);
        }
    }
}
