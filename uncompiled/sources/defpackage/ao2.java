package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* compiled from: Oscillator.java */
/* renamed from: ao2  reason: default package */
/* loaded from: classes.dex */
public class ao2 {
    public double[] c;
    public String d;
    public p92 e;
    public int f;
    public float[] a = new float[0];
    public double[] b = new double[0];
    public double g = 6.283185307179586d;

    public void a(double d, float f) {
        int length = this.a.length + 1;
        int binarySearch = Arrays.binarySearch(this.b, d);
        if (binarySearch < 0) {
            binarySearch = (-binarySearch) - 1;
        }
        this.b = Arrays.copyOf(this.b, length);
        this.a = Arrays.copyOf(this.a, length);
        this.c = new double[length];
        double[] dArr = this.b;
        System.arraycopy(dArr, binarySearch, dArr, binarySearch + 1, (length - binarySearch) - 1);
        this.b[binarySearch] = d;
        this.a[binarySearch] = f;
    }

    public double b(double d) {
        if (d <= Utils.DOUBLE_EPSILON) {
            d = 1.0E-5d;
        } else if (d >= 1.0d) {
            d = 0.999999d;
        }
        int binarySearch = Arrays.binarySearch(this.b, d);
        if (binarySearch <= 0 && binarySearch != 0) {
            int i = (-binarySearch) - 1;
            float[] fArr = this.a;
            int i2 = i - 1;
            double d2 = fArr[i] - fArr[i2];
            double[] dArr = this.b;
            double d3 = d2 / (dArr[i] - dArr[i2]);
            return (fArr[i2] - (d3 * dArr[i2])) + (d * d3);
        }
        return Utils.DOUBLE_EPSILON;
    }

    public double c(double d) {
        if (d < Utils.DOUBLE_EPSILON) {
            d = 0.0d;
        } else if (d > 1.0d) {
            d = 1.0d;
        }
        int binarySearch = Arrays.binarySearch(this.b, d);
        if (binarySearch > 0) {
            return 1.0d;
        }
        if (binarySearch != 0) {
            int i = (-binarySearch) - 1;
            float[] fArr = this.a;
            int i2 = i - 1;
            double d2 = fArr[i] - fArr[i2];
            double[] dArr = this.b;
            double d3 = d2 / (dArr[i] - dArr[i2]);
            return this.c[i2] + ((fArr[i2] - (dArr[i2] * d3)) * (d - dArr[i2])) + ((d3 * ((d * d) - (dArr[i2] * dArr[i2]))) / 2.0d);
        }
        return Utils.DOUBLE_EPSILON;
    }

    public double d(double d, double d2, double d3) {
        double c = d2 + c(d);
        double b = b(d) + d3;
        switch (this.f) {
            case 1:
                return Utils.DOUBLE_EPSILON;
            case 2:
                return b * 4.0d * Math.signum((((c * 4.0d) + 3.0d) % 4.0d) - 2.0d);
            case 3:
                return b * 2.0d;
            case 4:
                return (-b) * 2.0d;
            case 5:
                double d4 = this.g;
                return (-d4) * b * Math.sin(d4 * c);
            case 6:
                return b * 4.0d * ((((c * 4.0d) + 2.0d) % 4.0d) - 2.0d);
            case 7:
                return this.e.f(c % 1.0d, 0);
            default:
                double d5 = this.g;
                return b * d5 * Math.cos(d5 * c);
        }
    }

    public double e(double d, double d2) {
        double abs;
        double c = c(d) + d2;
        switch (this.f) {
            case 1:
                return Math.signum(0.5d - (c % 1.0d));
            case 2:
                abs = Math.abs((((c * 4.0d) + 1.0d) % 4.0d) - 2.0d);
                break;
            case 3:
                return (((c * 2.0d) + 1.0d) % 2.0d) - 1.0d;
            case 4:
                abs = ((c * 2.0d) + 1.0d) % 2.0d;
                break;
            case 5:
                return Math.cos(this.g * (d2 + c));
            case 6:
                double abs2 = 1.0d - Math.abs(((c * 4.0d) % 4.0d) - 2.0d);
                abs = abs2 * abs2;
                break;
            case 7:
                return this.e.c(c % 1.0d, 0);
            default:
                return Math.sin(this.g * c);
        }
        return 1.0d - abs;
    }

    public void f() {
        float[] fArr;
        float[] fArr2;
        float[] fArr3;
        int i;
        int i2 = 0;
        double d = 0.0d;
        while (true) {
            if (i2 >= this.a.length) {
                break;
            }
            d += fArr[i2];
            i2++;
        }
        int i3 = 1;
        double d2 = 0.0d;
        int i4 = 1;
        while (true) {
            if (i4 >= this.a.length) {
                break;
            }
            double[] dArr = this.b;
            d2 += (dArr[i4] - dArr[i4 - 1]) * ((fArr2[i] + fArr2[i4]) / 2.0f);
            i4++;
        }
        int i5 = 0;
        while (true) {
            float[] fArr4 = this.a;
            if (i5 >= fArr4.length) {
                break;
            }
            fArr4[i5] = (float) (fArr4[i5] * (d / d2));
            i5++;
        }
        this.c[0] = 0.0d;
        while (true) {
            if (i3 >= this.a.length) {
                return;
            }
            int i6 = i3 - 1;
            double[] dArr2 = this.b;
            double d3 = dArr2[i3] - dArr2[i6];
            double[] dArr3 = this.c;
            dArr3[i3] = dArr3[i6] + (d3 * ((fArr3[i6] + fArr3[i3]) / 2.0f));
            i3++;
        }
    }

    public void g(int i, String str) {
        this.f = i;
        this.d = str;
        if (str != null) {
            this.e = p92.i(str);
        }
    }

    public String toString() {
        return "pos =" + Arrays.toString(this.b) + " period=" + Arrays.toString(this.a);
    }
}
