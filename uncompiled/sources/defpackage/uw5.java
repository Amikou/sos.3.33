package defpackage;

import com.google.android.gms.internal.measurement.l2;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: uw5  reason: default package */
/* loaded from: classes.dex */
public final class uw5 extends ww5 {
    public /* synthetic */ uw5(qw5 qw5Var) {
        super(null);
    }

    @Override // defpackage.ww5
    public final void a(Object obj, long j) {
        ((zv5) l2.s(obj, j)).zzb();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.util.List] */
    @Override // defpackage.ww5
    public final <E> void b(Object obj, Object obj2, long j) {
        zv5<E> zv5Var = (zv5) l2.s(obj, j);
        zv5<E> zv5Var2 = (zv5) l2.s(obj2, j);
        int size = zv5Var.size();
        int size2 = zv5Var2.size();
        zv5<E> zv5Var3 = zv5Var;
        zv5Var3 = zv5Var;
        if (size > 0 && size2 > 0) {
            boolean zza = zv5Var.zza();
            zv5<E> zv5Var4 = zv5Var;
            if (!zza) {
                zv5Var4 = zv5Var.Q(size2 + size);
            }
            zv5Var4.addAll(zv5Var2);
            zv5Var3 = zv5Var4;
        }
        if (size > 0) {
            zv5Var2 = zv5Var3;
        }
        l2.t(obj, j, zv5Var2);
    }
}
