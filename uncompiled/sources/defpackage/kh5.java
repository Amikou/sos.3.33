package defpackage;

import com.google.android.gms.internal.measurement.zzew;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: kh5  reason: default package */
/* loaded from: classes.dex */
public final class kh5 implements sv5 {
    public static final sv5 a = new kh5();

    @Override // defpackage.sv5
    public final boolean d(int i) {
        return zzew.zza(i) != null;
    }
}
