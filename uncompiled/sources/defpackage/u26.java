package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: u26  reason: default package */
/* loaded from: classes.dex */
public final class u26 implements yp5<v26> {
    public static final u26 f0 = new u26();
    public final yp5<v26> a = gq5.a(gq5.b(new w26()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final v26 zza() {
        return this.a.zza();
    }
}
