package defpackage;

import android.graphics.Matrix;
import android.view.View;

/* compiled from: ViewUtilsApi29.java */
/* renamed from: tk4  reason: default package */
/* loaded from: classes.dex */
public class tk4 extends sk4 {
    @Override // defpackage.pk4, defpackage.uk4
    public float c(View view) {
        return view.getTransitionAlpha();
    }

    @Override // defpackage.qk4, defpackage.uk4
    public void e(View view, Matrix matrix) {
        view.setAnimationMatrix(matrix);
    }

    @Override // defpackage.rk4, defpackage.uk4
    public void f(View view, int i, int i2, int i3, int i4) {
        view.setLeftTopRightBottom(i, i2, i3, i4);
    }

    @Override // defpackage.pk4, defpackage.uk4
    public void g(View view, float f) {
        view.setTransitionAlpha(f);
    }

    @Override // defpackage.sk4, defpackage.uk4
    public void h(View view, int i) {
        view.setTransitionVisibility(i);
    }

    @Override // defpackage.qk4, defpackage.uk4
    public void i(View view, Matrix matrix) {
        view.transformMatrixToGlobal(matrix);
    }

    @Override // defpackage.qk4, defpackage.uk4
    public void j(View view, Matrix matrix) {
        view.transformMatrixToLocal(matrix);
    }
}
