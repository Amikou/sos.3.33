package defpackage;

import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: bq3  reason: default package */
/* loaded from: classes.dex */
public final class bq3 {
    public final int a;
    public final int b;

    public bq3(@RecentlyNonNull int i, @RecentlyNonNull int i2) {
        this.a = i;
        this.b = i2;
    }

    @RecentlyNonNull
    public final int a() {
        return this.b;
    }

    @RecentlyNonNull
    public final int b() {
        return this.a;
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof bq3) {
            bq3 bq3Var = (bq3) obj;
            if (this.a == bq3Var.a && this.b == bq3Var.b) {
                return true;
            }
        }
        return false;
    }

    @RecentlyNonNull
    public final int hashCode() {
        int i = this.b;
        int i2 = this.a;
        return i ^ ((i2 >>> 16) | (i2 << 16));
    }

    @RecentlyNonNull
    public final String toString() {
        int i = this.a;
        int i2 = this.b;
        StringBuilder sb = new StringBuilder(23);
        sb.append(i);
        sb.append("x");
        sb.append(i2);
        return sb.toString();
    }
}
