package defpackage;

import android.os.Build;

/* compiled from: StaticWebpNativeLoader.java */
/* renamed from: jt3  reason: default package */
/* loaded from: classes.dex */
public class jt3 {
    public static boolean a;

    public static synchronized void a() {
        synchronized (jt3.class) {
            if (!a) {
                if (Build.VERSION.SDK_INT <= 16) {
                    try {
                        pd2.d("fb_jpegturbo");
                    } catch (UnsatisfiedLinkError unused) {
                    }
                }
                pd2.d("static-webp");
                a = true;
            }
        }
    }
}
