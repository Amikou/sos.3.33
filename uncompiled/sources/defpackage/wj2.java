package defpackage;

import java.util.Objects;

/* compiled from: OSInfluenceConstants.kt */
/* renamed from: wj2  reason: default package */
/* loaded from: classes2.dex */
public final class wj2 {
    public static final String a;
    public static final String b;
    public static final wj2 c = new wj2();

    static {
        String canonicalName = vj2.class.getCanonicalName();
        Objects.requireNonNull(canonicalName, "null cannot be cast to non-null type kotlin.String");
        a = canonicalName;
        String canonicalName2 = ck2.class.getCanonicalName();
        Objects.requireNonNull(canonicalName2, "null cannot be cast to non-null type kotlin.String");
        b = canonicalName2;
    }

    public final String a() {
        return a;
    }

    public final String b() {
        return b;
    }
}
