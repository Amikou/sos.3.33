package defpackage;

import android.content.Context;
import net.safemoon.androidwallet.domain.useCase.wipe.AsyncWipeDataUseCase;
import net.safemoon.androidwallet.domain.useCase.wipe.WipeAppDataUseCase;

/* compiled from: WipeAppDataUseCaseProvider.kt */
/* renamed from: lp4  reason: default package */
/* loaded from: classes2.dex */
public final class lp4 {
    static {
        new lp4();
    }

    public static final en1 a(Context context) {
        fs1.f(context, "context");
        return new AsyncWipeDataUseCase(new WipeAppDataUseCase(context, b20.j(za.a, e31.a, mu2.a, gg4.a)));
    }
}
