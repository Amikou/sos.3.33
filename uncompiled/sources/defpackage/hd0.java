package defpackage;

/* compiled from: DBIG.java */
/* renamed from: hd0  reason: default package */
/* loaded from: classes2.dex */
public class hd0 {
    public long[] a;

    public hd0(int i) {
        long[] jArr = new long[10];
        this.a = jArr;
        jArr[0] = i;
        for (int i2 = 1; i2 < 10; i2++) {
            this.a[i2] = 0;
        }
    }

    public int a() {
        hd0 hd0Var = new hd0(this);
        hd0Var.b();
        int i = 9;
        while (i >= 0 && hd0Var.a[i] == 0) {
            i--;
        }
        if (i < 0) {
            return 0;
        }
        int i2 = i * 56;
        long j = hd0Var.a[i];
        while (j != 0) {
            j /= 2;
            i2++;
        }
        return i2;
    }

    public void b() {
        long j = 0;
        for (int i = 0; i < 9; i++) {
            long[] jArr = this.a;
            long j2 = jArr[i] + j;
            j = j2 >> 56;
            jArr[i] = j2 & 72057594037927935L;
        }
        long[] jArr2 = this.a;
        jArr2[9] = jArr2[9] + j;
    }

    public void c(int i) {
        int i2;
        int i3;
        int i4 = i % 56;
        int i5 = i / 56;
        int i6 = 0;
        while (true) {
            i2 = 10 - i5;
            i3 = i2 - 1;
            if (i6 >= i3) {
                break;
            }
            long[] jArr = this.a;
            int i7 = i5 + i6;
            jArr[i6] = (jArr[i7] >> i4) | ((jArr[i7 + 1] << (56 - i4)) & 72057594037927935L);
            i6++;
        }
        long[] jArr2 = this.a;
        jArr2[i3] = jArr2[9] >> i4;
        while (i2 < 10) {
            this.a[i2] = 0;
            i2++;
        }
    }

    public String toString() {
        hd0 hd0Var;
        int a = a();
        String str = "";
        for (int i = (a % 4 == 0 ? a >> 2 : (a >> 2) + 1) - 1; i >= 0; i--) {
            new hd0(this).c(i * 4);
            str = String.valueOf(str) + Integer.toHexString((int) (15 & hd0Var.a[0]));
        }
        return str;
    }

    public hd0(hd0 hd0Var) {
        this.a = new long[10];
        for (int i = 0; i < 10; i++) {
            this.a[i] = hd0Var.a[i];
        }
    }

    public hd0(zl zlVar) {
        this.a = new long[10];
        for (int i = 0; i < 4; i++) {
            this.a[i] = zlVar.a[i];
        }
        long[] jArr = this.a;
        long[] jArr2 = zlVar.a;
        jArr[4] = jArr2[4] & 72057594037927935L;
        jArr[5] = jArr2[4] >> 56;
        for (int i2 = 6; i2 < 10; i2++) {
            this.a[i2] = 0;
        }
    }
}
