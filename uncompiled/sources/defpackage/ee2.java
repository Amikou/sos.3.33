package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import okhttp3.HttpUrl;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: NavType.java */
/* renamed from: ee2  reason: default package */
/* loaded from: classes.dex */
public abstract class ee2<T> {
    public static final ee2<Integer> b = new c(false);
    public static final ee2<Integer> c = new d(false);
    public static final ee2<int[]> d = new e(true);
    public static final ee2<Long> e = new f(false);
    public static final ee2<long[]> f = new g(true);
    public static final ee2<Float> g = new h(false);
    public static final ee2<float[]> h = new i(true);
    public static final ee2<Boolean> i = new j(false);
    public static final ee2<boolean[]> j = new k(true);
    public static final ee2<String> k = new a(true);
    public static final ee2<String[]> l = new b(true);
    public final boolean a;

    /* compiled from: NavType.java */
    /* renamed from: ee2$a */
    /* loaded from: classes.dex */
    public class a extends ee2<String> {
        public a(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return Utf8String.TYPE_NAME;
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public String b(Bundle bundle, String str) {
            return (String) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public String h(String str) {
            return str;
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, String str2) {
            bundle.putString(str, str2);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$b */
    /* loaded from: classes.dex */
    public class b extends ee2<String[]> {
        public b(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "string[]";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public String[] b(Bundle bundle, String str) {
            return (String[]) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public String[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, String[] strArr) {
            bundle.putStringArray(str, strArr);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$c */
    /* loaded from: classes.dex */
    public class c extends ee2<Integer> {
        public c(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "integer";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public Integer b(Bundle bundle, String str) {
            return (Integer) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public Integer h(String str) {
            if (str.startsWith("0x")) {
                return Integer.valueOf(Integer.parseInt(str.substring(2), 16));
            }
            return Integer.valueOf(Integer.parseInt(str));
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, Integer num) {
            bundle.putInt(str, num.intValue());
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$d */
    /* loaded from: classes.dex */
    public class d extends ee2<Integer> {
        public d(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "reference";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public Integer b(Bundle bundle, String str) {
            return (Integer) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public Integer h(String str) {
            if (str.startsWith("0x")) {
                return Integer.valueOf(Integer.parseInt(str.substring(2), 16));
            }
            return Integer.valueOf(Integer.parseInt(str));
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, Integer num) {
            bundle.putInt(str, num.intValue());
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$e */
    /* loaded from: classes.dex */
    public class e extends ee2<int[]> {
        public e(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "integer[]";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public int[] b(Bundle bundle, String str) {
            return (int[]) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public int[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, int[] iArr) {
            bundle.putIntArray(str, iArr);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$f */
    /* loaded from: classes.dex */
    public class f extends ee2<Long> {
        public f(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "long";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public Long b(Bundle bundle, String str) {
            return (Long) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public Long h(String str) {
            if (str.endsWith("L")) {
                str = str.substring(0, str.length() - 1);
            }
            if (str.startsWith("0x")) {
                return Long.valueOf(Long.parseLong(str.substring(2), 16));
            }
            return Long.valueOf(Long.parseLong(str));
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, Long l) {
            bundle.putLong(str, l.longValue());
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$g */
    /* loaded from: classes.dex */
    public class g extends ee2<long[]> {
        public g(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "long[]";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public long[] b(Bundle bundle, String str) {
            return (long[]) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public long[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, long[] jArr) {
            bundle.putLongArray(str, jArr);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$h */
    /* loaded from: classes.dex */
    public class h extends ee2<Float> {
        public h(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "float";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public Float b(Bundle bundle, String str) {
            return (Float) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public Float h(String str) {
            return Float.valueOf(Float.parseFloat(str));
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, Float f) {
            bundle.putFloat(str, f.floatValue());
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$i */
    /* loaded from: classes.dex */
    public class i extends ee2<float[]> {
        public i(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "float[]";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public float[] b(Bundle bundle, String str) {
            return (float[]) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public float[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, float[] fArr) {
            bundle.putFloatArray(str, fArr);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$j */
    /* loaded from: classes.dex */
    public class j extends ee2<Boolean> {
        public j(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "boolean";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public Boolean b(Bundle bundle, String str) {
            return (Boolean) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public Boolean h(String str) {
            if ("true".equals(str)) {
                return Boolean.TRUE;
            }
            if ("false".equals(str)) {
                return Boolean.FALSE;
            }
            throw new IllegalArgumentException("A boolean NavType only accepts \"true\" or \"false\" values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, Boolean bool) {
            bundle.putBoolean(str, bool.booleanValue());
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$k */
    /* loaded from: classes.dex */
    public class k extends ee2<boolean[]> {
        public k(boolean z) {
            super(z);
        }

        @Override // defpackage.ee2
        public String c() {
            return "boolean[]";
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public boolean[] b(Bundle bundle, String str) {
            return (boolean[]) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public boolean[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, boolean[] zArr) {
            bundle.putBooleanArray(str, zArr);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$l */
    /* loaded from: classes.dex */
    public static final class l<D extends Enum> extends p<D> {
        public final Class<D> n;

        public l(Class<D> cls) {
            super(false, cls);
            if (cls.isEnum()) {
                this.n = cls;
                return;
            }
            throw new IllegalArgumentException(cls + " is not an Enum type.");
        }

        @Override // defpackage.ee2.p, defpackage.ee2
        public String c() {
            return this.n.getName();
        }

        @Override // defpackage.ee2.p
        /* renamed from: m */
        public D k(String str) {
            D[] enumConstants;
            for (D d : this.n.getEnumConstants()) {
                if (d.name().equals(str)) {
                    return d;
                }
            }
            throw new IllegalArgumentException("Enum value " + str + " not found for type " + this.n.getName() + ".");
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$m */
    /* loaded from: classes.dex */
    public static final class m<D extends Parcelable> extends ee2<D[]> {
        public final Class<D[]> m;

        public m(Class<D> cls) {
            super(true);
            if (Parcelable.class.isAssignableFrom(cls)) {
                try {
                    this.m = (Class<D[]>) Class.forName("[L" + cls.getName() + ";");
                    return;
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
            throw new IllegalArgumentException(cls + " does not implement Parcelable.");
        }

        @Override // defpackage.ee2
        public String c() {
            return this.m.getName();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || m.class != obj.getClass()) {
                return false;
            }
            return this.m.equals(((m) obj).m);
        }

        public int hashCode() {
            return this.m.hashCode();
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public D[] b(Bundle bundle, String str) {
            return (D[]) ((Parcelable[]) bundle.get(str));
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public D[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, D[] dArr) {
            this.m.cast(dArr);
            bundle.putParcelableArray(str, dArr);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$n */
    /* loaded from: classes.dex */
    public static final class n<D> extends ee2<D> {
        public final Class<D> m;

        public n(Class<D> cls) {
            super(true);
            if (!Parcelable.class.isAssignableFrom(cls) && !Serializable.class.isAssignableFrom(cls)) {
                throw new IllegalArgumentException(cls + " does not implement Parcelable or Serializable.");
            }
            this.m = cls;
        }

        @Override // defpackage.ee2
        public D b(Bundle bundle, String str) {
            return (D) bundle.get(str);
        }

        @Override // defpackage.ee2
        public String c() {
            return this.m.getName();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || n.class != obj.getClass()) {
                return false;
            }
            return this.m.equals(((n) obj).m);
        }

        @Override // defpackage.ee2
        public D h(String str) {
            throw new UnsupportedOperationException("Parcelables don't support default values.");
        }

        public int hashCode() {
            return this.m.hashCode();
        }

        @Override // defpackage.ee2
        public void i(Bundle bundle, String str, D d) {
            this.m.cast(d);
            if (d != null && !(d instanceof Parcelable)) {
                if (d instanceof Serializable) {
                    bundle.putSerializable(str, (Serializable) d);
                    return;
                }
                return;
            }
            bundle.putParcelable(str, (Parcelable) d);
        }
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$o */
    /* loaded from: classes.dex */
    public static final class o<D extends Serializable> extends ee2<D[]> {
        public final Class<D[]> m;

        public o(Class<D> cls) {
            super(true);
            if (Serializable.class.isAssignableFrom(cls)) {
                try {
                    this.m = (Class<D[]>) Class.forName("[L" + cls.getName() + ";");
                    return;
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
            throw new IllegalArgumentException(cls + " does not implement Serializable.");
        }

        @Override // defpackage.ee2
        public String c() {
            return this.m.getName();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || o.class != obj.getClass()) {
                return false;
            }
            return this.m.equals(((o) obj).m);
        }

        public int hashCode() {
            return this.m.hashCode();
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public D[] b(Bundle bundle, String str) {
            return (D[]) ((Serializable[]) bundle.get(str));
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public D[] h(String str) {
            throw new UnsupportedOperationException("Arrays don't support default values.");
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, D[] dArr) {
            this.m.cast(dArr);
            bundle.putSerializable(str, dArr);
        }
    }

    public ee2(boolean z) {
        this.a = z;
    }

    public static ee2<?> a(String str, String str2) {
        String str3;
        ee2<Integer> ee2Var = b;
        if (ee2Var.c().equals(str)) {
            return ee2Var;
        }
        ee2 ee2Var2 = d;
        if (ee2Var2.c().equals(str)) {
            return ee2Var2;
        }
        ee2<Long> ee2Var3 = e;
        if (ee2Var3.c().equals(str)) {
            return ee2Var3;
        }
        ee2 ee2Var4 = f;
        if (ee2Var4.c().equals(str)) {
            return ee2Var4;
        }
        ee2<Boolean> ee2Var5 = i;
        if (ee2Var5.c().equals(str)) {
            return ee2Var5;
        }
        ee2 ee2Var6 = j;
        if (ee2Var6.c().equals(str)) {
            return ee2Var6;
        }
        ee2<String> ee2Var7 = k;
        if (ee2Var7.c().equals(str)) {
            return ee2Var7;
        }
        ee2 ee2Var8 = l;
        if (ee2Var8.c().equals(str)) {
            return ee2Var8;
        }
        ee2<Float> ee2Var9 = g;
        if (ee2Var9.c().equals(str)) {
            return ee2Var9;
        }
        ee2 ee2Var10 = h;
        if (ee2Var10.c().equals(str)) {
            return ee2Var10;
        }
        ee2<Integer> ee2Var11 = c;
        if (ee2Var11.c().equals(str)) {
            return ee2Var11;
        }
        if (str == null || str.isEmpty()) {
            return ee2Var7;
        }
        try {
            if (!str.startsWith(".") || str2 == null) {
                str3 = str;
            } else {
                str3 = str2 + str;
            }
            if (str.endsWith(HttpUrl.PATH_SEGMENT_ENCODE_SET_URI)) {
                str3 = str3.substring(0, str3.length() - 2);
                Class<?> cls = Class.forName(str3);
                if (Parcelable.class.isAssignableFrom(cls)) {
                    return new m(cls);
                }
                if (Serializable.class.isAssignableFrom(cls)) {
                    return new o(cls);
                }
            } else {
                Class<?> cls2 = Class.forName(str3);
                if (Parcelable.class.isAssignableFrom(cls2)) {
                    return new n(cls2);
                }
                if (Enum.class.isAssignableFrom(cls2)) {
                    return new l(cls2);
                }
                if (Serializable.class.isAssignableFrom(cls2)) {
                    return new p(cls2);
                }
            }
            throw new IllegalArgumentException(str3 + " is not Serializable or Parcelable.");
        } catch (ClassNotFoundException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static ee2 d(String str) {
        try {
            try {
                try {
                    try {
                        ee2<Integer> ee2Var = b;
                        ee2Var.h(str);
                        return ee2Var;
                    } catch (IllegalArgumentException unused) {
                        return k;
                    }
                } catch (IllegalArgumentException unused2) {
                    ee2<Long> ee2Var2 = e;
                    ee2Var2.h(str);
                    return ee2Var2;
                }
            } catch (IllegalArgumentException unused3) {
                ee2<Float> ee2Var3 = g;
                ee2Var3.h(str);
                return ee2Var3;
            }
        } catch (IllegalArgumentException unused4) {
            ee2<Boolean> ee2Var4 = i;
            ee2Var4.h(str);
            return ee2Var4;
        }
    }

    public static ee2 e(Object obj) {
        if (obj instanceof Integer) {
            return b;
        }
        if (obj instanceof int[]) {
            return d;
        }
        if (obj instanceof Long) {
            return e;
        }
        if (obj instanceof long[]) {
            return f;
        }
        if (obj instanceof Float) {
            return g;
        }
        if (obj instanceof float[]) {
            return h;
        }
        if (obj instanceof Boolean) {
            return i;
        }
        if (obj instanceof boolean[]) {
            return j;
        }
        if (!(obj instanceof String) && obj != null) {
            if (obj instanceof String[]) {
                return l;
            }
            if (obj.getClass().isArray() && Parcelable.class.isAssignableFrom(obj.getClass().getComponentType())) {
                return new m(obj.getClass().getComponentType());
            }
            if (obj.getClass().isArray() && Serializable.class.isAssignableFrom(obj.getClass().getComponentType())) {
                return new o(obj.getClass().getComponentType());
            }
            if (obj instanceof Parcelable) {
                return new n(obj.getClass());
            }
            if (obj instanceof Enum) {
                return new l(obj.getClass());
            }
            if (obj instanceof Serializable) {
                return new p(obj.getClass());
            }
            throw new IllegalArgumentException("Object of type " + obj.getClass().getName() + " is not supported for navigation arguments.");
        }
        return k;
    }

    public abstract T b(Bundle bundle, String str);

    public abstract String c();

    public boolean f() {
        return this.a;
    }

    public T g(Bundle bundle, String str, String str2) {
        T h2 = h(str2);
        i(bundle, str, h2);
        return h2;
    }

    public abstract T h(String str);

    public abstract void i(Bundle bundle, String str, T t);

    public String toString() {
        return c();
    }

    /* compiled from: NavType.java */
    /* renamed from: ee2$p */
    /* loaded from: classes.dex */
    public static class p<D extends Serializable> extends ee2<D> {
        public final Class<D> m;

        public p(Class<D> cls) {
            super(true);
            if (Serializable.class.isAssignableFrom(cls)) {
                if (!cls.isEnum()) {
                    this.m = cls;
                    return;
                }
                throw new IllegalArgumentException(cls + " is an Enum. You should use EnumType instead.");
            }
            throw new IllegalArgumentException(cls + " does not implement Serializable.");
        }

        @Override // defpackage.ee2
        public String c() {
            return this.m.getName();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof p) {
                return this.m.equals(((p) obj).m);
            }
            return false;
        }

        public int hashCode() {
            return this.m.hashCode();
        }

        @Override // defpackage.ee2
        /* renamed from: j */
        public D b(Bundle bundle, String str) {
            return (D) bundle.get(str);
        }

        @Override // defpackage.ee2
        /* renamed from: k */
        public D h(String str) {
            throw new UnsupportedOperationException("Serializables don't support default values.");
        }

        @Override // defpackage.ee2
        /* renamed from: l */
        public void i(Bundle bundle, String str, D d) {
            this.m.cast(d);
            bundle.putSerializable(str, d);
        }

        public p(boolean z, Class<D> cls) {
            super(z);
            if (Serializable.class.isAssignableFrom(cls)) {
                this.m = cls;
                return;
            }
            throw new IllegalArgumentException(cls + " does not implement Serializable.");
        }
    }
}
