package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: ITokenDisplayModelMapper.kt */
/* renamed from: sm1  reason: default package */
/* loaded from: classes2.dex */
public interface sm1 {
    Object a(LiveData<List<IToken>> liveData, TokenType tokenType, q70<? super LiveData<List<q9>>> q70Var);
}
