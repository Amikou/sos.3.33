package defpackage;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.b;

/* compiled from: UnitDrawableDecoder.java */
/* renamed from: ue4  reason: default package */
/* loaded from: classes.dex */
public class ue4 implements b<Drawable, Drawable> {
    @Override // com.bumptech.glide.load.b
    /* renamed from: c */
    public s73<Drawable> b(Drawable drawable, int i, int i2, vn2 vn2Var) {
        return wg2.f(drawable);
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public boolean a(Drawable drawable, vn2 vn2Var) {
        return true;
    }
}
