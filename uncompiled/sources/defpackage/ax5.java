package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: ax5  reason: default package */
/* loaded from: classes.dex */
public final class ax5 implements tx5 {
    @Override // defpackage.tx5
    public final rx5 a(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }

    @Override // defpackage.tx5
    public final boolean b(Class<?> cls) {
        return false;
    }
}
