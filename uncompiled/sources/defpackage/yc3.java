package defpackage;

import android.graphics.SurfaceTexture;
import android.media.MediaFormat;
import android.opengl.GLES20;
import android.opengl.Matrix;
import androidx.media3.common.j;
import androidx.media3.common.util.GlUtil;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.http2.Http2;

/* compiled from: SceneRenderer.java */
/* renamed from: yc3  reason: default package */
/* loaded from: classes.dex */
public final class yc3 implements lh4, ev {
    public int m0;
    public SurfaceTexture n0;
    public byte[] q0;
    public final AtomicBoolean a = new AtomicBoolean();
    public final AtomicBoolean f0 = new AtomicBoolean(true);
    public final zv2 g0 = new zv2();
    public final zb1 h0 = new zb1();
    public final e64<Long> i0 = new e64<>();
    public final e64<xv2> j0 = new e64<>();
    public final float[] k0 = new float[16];
    public final float[] l0 = new float[16];
    public volatile int o0 = 0;
    public int p0 = -1;

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void g(SurfaceTexture surfaceTexture) {
        this.a.set(true);
    }

    @Override // defpackage.ev
    public void a(long j, float[] fArr) {
        this.h0.e(j, fArr);
    }

    @Override // defpackage.ev
    public void b() {
        this.i0.c();
        this.h0.d();
        this.f0.set(true);
    }

    public void d(float[] fArr, boolean z) {
        GLES20.glClear(Http2.INITIAL_MAX_FRAME_SIZE);
        GlUtil.c();
        if (this.a.compareAndSet(true, false)) {
            ((SurfaceTexture) ii.e(this.n0)).updateTexImage();
            GlUtil.c();
            if (this.f0.compareAndSet(true, false)) {
                Matrix.setIdentityM(this.k0, 0);
            }
            long timestamp = this.n0.getTimestamp();
            Long g = this.i0.g(timestamp);
            if (g != null) {
                this.h0.c(this.k0, g.longValue());
            }
            xv2 j = this.j0.j(timestamp);
            if (j != null) {
                this.g0.d(j);
            }
        }
        Matrix.multiplyMM(this.l0, 0, fArr, 0, this.k0, 0);
        this.g0.a(this.m0, this.l0, z);
    }

    @Override // defpackage.lh4
    public void e(long j, long j2, j jVar, MediaFormat mediaFormat) {
        this.i0.a(j2, Long.valueOf(j));
        i(jVar.z0, jVar.A0, j2);
    }

    public SurfaceTexture f() {
        GLES20.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        GlUtil.c();
        this.g0.b();
        GlUtil.c();
        this.m0 = GlUtil.f();
        SurfaceTexture surfaceTexture = new SurfaceTexture(this.m0);
        this.n0 = surfaceTexture;
        surfaceTexture.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() { // from class: xc3
            @Override // android.graphics.SurfaceTexture.OnFrameAvailableListener
            public final void onFrameAvailable(SurfaceTexture surfaceTexture2) {
                yc3.this.g(surfaceTexture2);
            }
        });
        return this.n0;
    }

    public void h(int i) {
        this.o0 = i;
    }

    public final void i(byte[] bArr, int i, long j) {
        byte[] bArr2 = this.q0;
        int i2 = this.p0;
        this.q0 = bArr;
        if (i == -1) {
            i = this.o0;
        }
        this.p0 = i;
        if (i2 == i && Arrays.equals(bArr2, this.q0)) {
            return;
        }
        byte[] bArr3 = this.q0;
        xv2 a = bArr3 != null ? yv2.a(bArr3, this.p0) : null;
        if (a == null || !zv2.c(a)) {
            a = xv2.b(this.p0);
        }
        this.j0.a(j, a);
    }
}
