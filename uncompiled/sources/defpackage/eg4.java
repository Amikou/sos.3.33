package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.token.room.RoomToken;

/* compiled from: UserTokenListDao.kt */
/* renamed from: eg4  reason: default package */
/* loaded from: classes2.dex */
public interface eg4 {
    LiveData<List<RoomToken>> a();

    Object b(String str, String str2, q70<? super te4> q70Var);

    void c(String str, double d);

    LiveData<RoomToken> d(String str);

    Object e(String str, q70<? super RoomToken> q70Var);

    void f(String str, double d, double d2);

    Object g(int i, q70<? super List<RoomToken>> q70Var);

    Object h(int i, String str, q70<? super RoomToken> q70Var);

    Object i(String str, int i, q70<? super te4> q70Var);

    boolean j(String str, int i);

    void k(RoomToken roomToken);

    Object l(q70<? super List<RoomToken>> q70Var);

    void m(RoomToken roomToken);

    LiveData<List<RoomToken>> n(int i);
}
