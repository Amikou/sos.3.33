package defpackage;

import androidx.room.RoomDatabase;

/* compiled from: WorkProgressDao_Impl.java */
/* renamed from: oq4  reason: default package */
/* loaded from: classes.dex */
public final class oq4 implements nq4 {
    public final RoomDatabase a;
    public final co3 b;
    public final co3 c;

    /* compiled from: WorkProgressDao_Impl.java */
    /* renamed from: oq4$a */
    /* loaded from: classes.dex */
    public class a extends zv0<mq4> {
        public a(oq4 oq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, mq4 mq4Var) {
            String str = mq4Var.a;
            if (str == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, str);
            }
            byte[] n = androidx.work.b.n(mq4Var.b);
            if (n == null) {
                ww3Var.Y0(2);
            } else {
                ww3Var.A0(2, n);
            }
        }
    }

    /* compiled from: WorkProgressDao_Impl.java */
    /* renamed from: oq4$b */
    /* loaded from: classes.dex */
    public class b extends co3 {
        public b(oq4 oq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE from WorkProgress where work_spec_id=?";
        }
    }

    /* compiled from: WorkProgressDao_Impl.java */
    /* renamed from: oq4$c */
    /* loaded from: classes.dex */
    public class c extends co3 {
        public c(oq4 oq4Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM WorkProgress";
        }
    }

    public oq4(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        new a(this, roomDatabase);
        this.b = new b(this, roomDatabase);
        this.c = new c(this, roomDatabase);
    }

    @Override // defpackage.nq4
    public void a() {
        this.a.d();
        ww3 a2 = this.c.a();
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.c.f(a2);
        }
    }

    @Override // defpackage.nq4
    public void delete(String str) {
        this.a.d();
        ww3 a2 = this.b.a();
        if (str == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.b.f(a2);
        }
    }
}
