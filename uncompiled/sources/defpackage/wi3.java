package defpackage;

/* compiled from: SeekMap.java */
/* renamed from: wi3  reason: default package */
/* loaded from: classes.dex */
public interface wi3 {

    /* compiled from: SeekMap.java */
    /* renamed from: wi3$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final yi3 a;
        public final yi3 b;

        public a(yi3 yi3Var) {
            this(yi3Var, yi3Var);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.a.equals(aVar.a) && this.b.equals(aVar.b);
        }

        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }

        public String toString() {
            String str;
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(this.a);
            if (this.a.equals(this.b)) {
                str = "";
            } else {
                str = ", " + this.b;
            }
            sb.append(str);
            sb.append("]");
            return sb.toString();
        }

        public a(yi3 yi3Var, yi3 yi3Var2) {
            this.a = (yi3) ii.e(yi3Var);
            this.b = (yi3) ii.e(yi3Var2);
        }
    }

    /* compiled from: SeekMap.java */
    /* renamed from: wi3$b */
    /* loaded from: classes.dex */
    public static class b implements wi3 {
        public final long a;
        public final a b;

        public b(long j) {
            this(j, 0L);
        }

        @Override // defpackage.wi3
        public boolean e() {
            return false;
        }

        @Override // defpackage.wi3
        public a h(long j) {
            return this.b;
        }

        @Override // defpackage.wi3
        public long i() {
            return this.a;
        }

        public b(long j, long j2) {
            this.a = j;
            this.b = new a(j2 == 0 ? yi3.c : new yi3(0L, j2));
        }
    }

    boolean e();

    a h(long j);

    long i();
}
