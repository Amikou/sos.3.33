package defpackage;

import java.security.MessageDigest;

/* compiled from: ObjectKey.java */
/* renamed from: ll2  reason: default package */
/* loaded from: classes.dex */
public final class ll2 implements fx1 {
    public final Object b;

    public ll2(Object obj) {
        this.b = wt2.d(obj);
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(fx1.a));
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof ll2) {
            return this.b.equals(((ll2) obj).b);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
