package defpackage;

import android.os.RemoteException;
import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: cr5  reason: default package */
/* loaded from: classes.dex */
public final class cr5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ m f0;
    public final /* synthetic */ p g0;

    public cr5(p pVar, zzp zzpVar, m mVar) {
        this.g0 = pVar;
        this.a = zzpVar;
        this.f0 = mVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ck5 ck5Var;
        d dVar;
        String str = null;
        try {
            try {
                if (this.g0.a.A().s().h()) {
                    dVar = this.g0.d;
                    if (dVar == null) {
                        this.g0.a.w().l().a("Failed to get app instance id");
                        ck5Var = this.g0.a;
                    } else {
                        zt2.j(this.a);
                        str = dVar.u(this.a);
                        if (str != null) {
                            this.g0.a.F().p(str);
                            this.g0.a.A().g.b(str);
                        }
                        this.g0.D();
                        ck5Var = this.g0.a;
                    }
                } else {
                    this.g0.a.w().s().a("Analytics storage consent denied; will not get app instance id");
                    this.g0.a.F().p(null);
                    this.g0.a.A().g.b(null);
                    ck5Var = this.g0.a;
                }
            } catch (RemoteException e) {
                this.g0.a.w().l().b("Failed to get app instance id", e);
                ck5Var = this.g0.a;
            }
            ck5Var.G().R(this.f0, str);
        } catch (Throwable th) {
            this.g0.a.G().R(this.f0, null);
            throw th;
        }
    }
}
