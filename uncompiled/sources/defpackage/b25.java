package defpackage;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: b25  reason: default package */
/* loaded from: classes.dex */
public final class b25 {
    public final rh<ze<?>, ConnectionResult> a;
    public final rh<ze<?>, String> b;
    public final n34<Map<ze<?>, String>> c;
    public int d;
    public boolean e;

    public final Set<ze<?>> a() {
        return this.a.keySet();
    }

    public final void b(ze<?> zeVar, ConnectionResult connectionResult, String str) {
        this.a.put(zeVar, connectionResult);
        this.b.put(zeVar, str);
        this.d--;
        if (!connectionResult.M1()) {
            this.e = true;
        }
        if (this.d == 0) {
            if (this.e) {
                this.c.b(new AvailabilityException(this.a));
                return;
            }
            this.c.c(this.b);
        }
    }
}
