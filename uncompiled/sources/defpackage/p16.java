package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: p16  reason: default package */
/* loaded from: classes.dex */
public final class p16 implements o16 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.service.use_appinfo_modified", true);

    @Override // defpackage.o16
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.o16
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
