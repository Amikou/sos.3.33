package defpackage;

import android.util.Pair;
import androidx.media3.common.util.b;
import androidx.media3.extractor.metadata.id3.MlltFrame;
import com.github.mikephil.charting.utils.Utils;
import defpackage.wi3;
import zendesk.support.request.CellBase;

/* compiled from: MlltSeeker.java */
/* renamed from: d92  reason: default package */
/* loaded from: classes.dex */
public final class d92 implements zi3 {
    public final long[] a;
    public final long[] b;
    public final long c;

    public d92(long[] jArr, long[] jArr2, long j) {
        this.a = jArr;
        this.b = jArr2;
        this.c = j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? b.y0(jArr2[jArr2.length - 1]) : j;
    }

    public static d92 a(long j, MlltFrame mlltFrame, long j2) {
        int length = mlltFrame.i0.length;
        int i = length + 1;
        long[] jArr = new long[i];
        long[] jArr2 = new long[i];
        jArr[0] = j;
        long j3 = 0;
        jArr2[0] = 0;
        for (int i2 = 1; i2 <= length; i2++) {
            int i3 = i2 - 1;
            j += mlltFrame.g0 + mlltFrame.i0[i3];
            j3 += mlltFrame.h0 + mlltFrame.j0[i3];
            jArr[i2] = j;
            jArr2[i2] = j3;
        }
        return new d92(jArr, jArr2, j2);
    }

    public static Pair<Long, Long> c(long j, long[] jArr, long[] jArr2) {
        int i = b.i(jArr, j, true, true);
        long j2 = jArr[i];
        long j3 = jArr2[i];
        int i2 = i + 1;
        if (i2 == jArr.length) {
            return Pair.create(Long.valueOf(j2), Long.valueOf(j3));
        }
        long j4 = jArr[i2];
        return Pair.create(Long.valueOf(j), Long.valueOf(((long) ((j4 == j2 ? Utils.DOUBLE_EPSILON : (j - j2) / (j4 - j2)) * (jArr2[i2] - j3))) + j3));
    }

    @Override // defpackage.zi3
    public long b(long j) {
        return b.y0(((Long) c(j, this.a, this.b).second).longValue());
    }

    @Override // defpackage.zi3
    public long d() {
        return -1L;
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        Pair<Long, Long> c = c(b.U0(b.r(j, 0L, this.c)), this.b, this.a);
        return new wi3.a(new yi3(b.y0(((Long) c.first).longValue()), ((Long) c.second).longValue()));
    }

    @Override // defpackage.wi3
    public long i() {
        return this.c;
    }
}
