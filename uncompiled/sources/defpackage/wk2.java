package defpackage;

import com.onesignal.b1;
import java.util.Set;

/* compiled from: OSSharedPreferencesWrapper.java */
/* renamed from: wk2  reason: default package */
/* loaded from: classes2.dex */
public class wk2 implements vk2 {
    @Override // defpackage.vk2
    public void a(String str, String str2, int i) {
        b1.k(str, str2, i);
    }

    @Override // defpackage.vk2
    public void b(String str, String str2, boolean z) {
        b1.j(str, str2, z);
    }

    @Override // defpackage.vk2
    public Set<String> c(String str, String str2, Set<String> set) {
        return b1.g(str, str2, set);
    }

    @Override // defpackage.vk2
    public int d(String str, String str2, int i) {
        return b1.c(str, str2, i);
    }

    @Override // defpackage.vk2
    public String e(String str, String str2, String str3) {
        return b1.f(str, str2, str3);
    }

    @Override // defpackage.vk2
    public String f() {
        return b1.a;
    }

    @Override // defpackage.vk2
    public void g(String str, String str2, Set<String> set) {
        b1.n(str, str2, set);
    }

    @Override // defpackage.vk2
    public String h() {
        return "PREFS_OS_OUTCOMES_V2";
    }

    @Override // defpackage.vk2
    public void i(String str, String str2, String str3) {
        b1.m(str, str2, str3);
    }

    @Override // defpackage.vk2
    public boolean j(String str, String str2, boolean z) {
        return b1.b(str, str2, z);
    }
}
