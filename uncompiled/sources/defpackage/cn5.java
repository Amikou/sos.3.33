package defpackage;

import com.google.android.gms.internal.vision.zzgz;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: cn5  reason: default package */
/* loaded from: classes.dex */
public final class cn5 implements ws5 {
    public static final ws5 a = new cn5();

    @Override // defpackage.ws5
    public final boolean d(int i) {
        return zzgz.zza(i) != null;
    }
}
