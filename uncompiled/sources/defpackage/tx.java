package defpackage;

/* compiled from: Channel.kt */
/* renamed from: tx  reason: default package */
/* loaded from: classes2.dex */
public final class tx<T> {
    public static final b b = new b(null);
    public static final c c = new c();
    public final Object a;

    /* compiled from: Channel.kt */
    /* renamed from: tx$a */
    /* loaded from: classes2.dex */
    public static final class a extends c {
        public final Throwable a;

        public a(Throwable th) {
            this.a = th;
        }

        public boolean equals(Object obj) {
            return (obj instanceof a) && fs1.b(this.a, ((a) obj).a);
        }

        public int hashCode() {
            Throwable th = this.a;
            if (th != null) {
                return th.hashCode();
            }
            return 0;
        }

        @Override // defpackage.tx.c
        public String toString() {
            return "Closed(" + this.a + ')';
        }
    }

    /* compiled from: Channel.kt */
    /* renamed from: tx$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }

        public final <E> Object a(Throwable th) {
            return tx.c(new a(th));
        }

        public final <E> Object b() {
            return tx.c(tx.c);
        }

        public final <E> Object c(E e) {
            return tx.c(e);
        }
    }

    /* compiled from: Channel.kt */
    /* renamed from: tx$c */
    /* loaded from: classes2.dex */
    public static class c {
        public String toString() {
            return "Failed";
        }
    }

    public /* synthetic */ tx(Object obj) {
        this.a = obj;
    }

    public static final /* synthetic */ tx b(Object obj) {
        return new tx(obj);
    }

    public static <T> Object c(Object obj) {
        return obj;
    }

    public static boolean d(Object obj, Object obj2) {
        return (obj2 instanceof tx) && fs1.b(obj, ((tx) obj2).k());
    }

    public static final Throwable e(Object obj) {
        a aVar = obj instanceof a ? (a) obj : null;
        if (aVar == null) {
            return null;
        }
        return aVar.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final T f(Object obj) {
        Throwable th;
        if (obj instanceof c) {
            if (!(obj instanceof a) || (th = ((a) obj).a) == null) {
                throw new IllegalStateException(fs1.l("Trying to call 'getOrThrow' on a failed channel result: ", obj).toString());
            }
            throw th;
        }
        return obj;
    }

    public static int g(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public static final boolean h(Object obj) {
        return obj instanceof a;
    }

    public static final boolean i(Object obj) {
        return !(obj instanceof c);
    }

    public static String j(Object obj) {
        if (obj instanceof a) {
            return obj.toString();
        }
        return "Value(" + obj + ')';
    }

    public boolean equals(Object obj) {
        return d(k(), obj);
    }

    public int hashCode() {
        return g(k());
    }

    public final /* synthetic */ Object k() {
        return this.a;
    }

    public String toString() {
        return j(k());
    }
}
