package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Signal.java */
/* renamed from: al  reason: default package */
/* loaded from: classes2.dex */
public final class al extends r90.e.d.a.b.AbstractC0270d {
    public final String a;
    public final String b;
    public final long c;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Signal.java */
    /* renamed from: al$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.b.AbstractC0270d.AbstractC0271a {
        public String a;
        public String b;
        public Long c;

        @Override // defpackage.r90.e.d.a.b.AbstractC0270d.AbstractC0271a
        public r90.e.d.a.b.AbstractC0270d a() {
            String str = "";
            if (this.a == null) {
                str = " name";
            }
            if (this.b == null) {
                str = str + " code";
            }
            if (this.c == null) {
                str = str + " address";
            }
            if (str.isEmpty()) {
                return new al(this.a, this.b, this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0270d.AbstractC0271a
        public r90.e.d.a.b.AbstractC0270d.AbstractC0271a b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0270d.AbstractC0271a
        public r90.e.d.a.b.AbstractC0270d.AbstractC0271a c(String str) {
            Objects.requireNonNull(str, "Null code");
            this.b = str;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0270d.AbstractC0271a
        public r90.e.d.a.b.AbstractC0270d.AbstractC0271a d(String str) {
            Objects.requireNonNull(str, "Null name");
            this.a = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0270d
    public long b() {
        return this.c;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0270d
    public String c() {
        return this.b;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0270d
    public String d() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a.b.AbstractC0270d) {
            r90.e.d.a.b.AbstractC0270d abstractC0270d = (r90.e.d.a.b.AbstractC0270d) obj;
            return this.a.equals(abstractC0270d.d()) && this.b.equals(abstractC0270d.c()) && this.c == abstractC0270d.b();
        }
        return false;
    }

    public int hashCode() {
        long j = this.c;
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return "Signal{name=" + this.a + ", code=" + this.b + ", address=" + this.c + "}";
    }

    public al(String str, String str2, long j) {
        this.a = str;
        this.b = str2;
        this.c = j;
    }
}
