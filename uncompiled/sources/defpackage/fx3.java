package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.rd.draw.data.Orientation;

/* compiled from: SwapDrawer.java */
/* renamed from: fx3  reason: default package */
/* loaded from: classes2.dex */
public class fx3 extends hn {
    public fx3(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2, int i3) {
        if (wg4Var instanceof ax3) {
            ax3 ax3Var = (ax3) wg4Var;
            int p = this.b.p();
            int t = this.b.t();
            int m = this.b.m();
            int q = this.b.q();
            int r = this.b.r();
            int f = this.b.f();
            int a = ax3Var.a();
            if (this.b.z()) {
                if (i == r) {
                    a = ax3Var.a();
                } else {
                    if (i == q) {
                        a = ax3Var.b();
                    }
                    p = t;
                }
            } else if (i == f) {
                a = ax3Var.a();
            } else {
                if (i == q) {
                    a = ax3Var.b();
                }
                p = t;
            }
            this.a.setColor(p);
            if (this.b.g() == Orientation.HORIZONTAL) {
                canvas.drawCircle(a, i3, m, this.a);
            } else {
                canvas.drawCircle(i2, a, m, this.a);
            }
        }
    }
}
