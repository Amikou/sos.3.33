package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: ViewSwapStatsInfoBinding.java */
/* renamed from: dk4  reason: default package */
/* loaded from: classes2.dex */
public final class dk4 {
    public final AppCompatImageView a;
    public final AppCompatImageView b;
    public final AppCompatTextView c;
    public final AppCompatTextView d;
    public final AppCompatTextView e;
    public final AppCompatTextView f;
    public final AppCompatTextView g;
    public final AppCompatTextView h;
    public final AppCompatTextView i;
    public final AppCompatTextView j;
    public final AppCompatTextView k;
    public final AppCompatTextView l;
    public final AppCompatTextView m;
    public final AppCompatTextView n;
    public final AppCompatTextView o;
    public final AppCompatTextView p;
    public final AppCompatTextView q;
    public final AppCompatTextView r;
    public final AppCompatTextView s;
    public final AppCompatTextView t;
    public final AppCompatTextView u;
    public final AppCompatTextView v;
    public final AppCompatTextView w;
    public final AppCompatTextView x;

    public dk4(MaterialCardView materialCardView, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, AppCompatTextView appCompatTextView3, AppCompatTextView appCompatTextView4, AppCompatTextView appCompatTextView5, AppCompatTextView appCompatTextView6, AppCompatTextView appCompatTextView7, AppCompatTextView appCompatTextView8, AppCompatTextView appCompatTextView9, AppCompatTextView appCompatTextView10, AppCompatTextView appCompatTextView11, AppCompatTextView appCompatTextView12, AppCompatTextView appCompatTextView13, AppCompatTextView appCompatTextView14, AppCompatTextView appCompatTextView15, AppCompatTextView appCompatTextView16, AppCompatTextView appCompatTextView17, AppCompatTextView appCompatTextView18, AppCompatTextView appCompatTextView19, AppCompatTextView appCompatTextView20, AppCompatTextView appCompatTextView21, AppCompatTextView appCompatTextView22, AppCompatTextView appCompatTextView23, AppCompatTextView appCompatTextView24, AppCompatTextView appCompatTextView25, AppCompatTextView appCompatTextView26, AppCompatTextView appCompatTextView27, AppCompatTextView appCompatTextView28, AppCompatTextView appCompatTextView29, AppCompatTextView appCompatTextView30, AppCompatTextView appCompatTextView31, AppCompatTextView appCompatTextView32, AppCompatTextView appCompatTextView33, AppCompatTextView appCompatTextView34, AppCompatTextView appCompatTextView35) {
        this.a = appCompatImageView;
        this.b = appCompatImageView3;
        this.c = appCompatTextView;
        this.d = appCompatTextView3;
        this.e = appCompatTextView4;
        this.f = appCompatTextView5;
        this.g = appCompatTextView6;
        this.h = appCompatTextView8;
        this.i = appCompatTextView9;
        this.j = appCompatTextView10;
        this.k = appCompatTextView11;
        this.l = appCompatTextView13;
        this.m = appCompatTextView15;
        this.n = appCompatTextView16;
        this.o = appCompatTextView18;
        this.p = appCompatTextView20;
        this.q = appCompatTextView22;
        this.r = appCompatTextView25;
        this.s = appCompatTextView26;
        this.t = appCompatTextView28;
        this.u = appCompatTextView30;
        this.v = appCompatTextView31;
        this.w = appCompatTextView33;
        this.x = appCompatTextView35;
    }

    public static dk4 a(View view) {
        int i = R.id.from_pair_token_icon;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.from_pair_token_icon);
        if (appCompatImageView != null) {
            i = R.id.pair_token_icon;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.pair_token_icon);
            if (appCompatImageView2 != null) {
                i = R.id.to_pair_token_icon;
                AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.to_pair_token_icon);
                if (appCompatImageView3 != null) {
                    i = R.id.txt1hourHeader;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txt1hourHeader);
                    if (appCompatTextView != null) {
                        i = R.id.txt24HourTitle;
                        AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txt24HourTitle);
                        if (appCompatTextView2 != null) {
                            i = R.id.txt24HourValue;
                            AppCompatTextView appCompatTextView3 = (AppCompatTextView) ai4.a(view, R.id.txt24HourValue);
                            if (appCompatTextView3 != null) {
                                i = R.id.txt24hourHeader;
                                AppCompatTextView appCompatTextView4 = (AppCompatTextView) ai4.a(view, R.id.txt24hourHeader);
                                if (appCompatTextView4 != null) {
                                    i = R.id.txt5minuteHeader;
                                    AppCompatTextView appCompatTextView5 = (AppCompatTextView) ai4.a(view, R.id.txt5minuteHeader);
                                    if (appCompatTextView5 != null) {
                                        i = R.id.txt6hourHeader;
                                        AppCompatTextView appCompatTextView6 = (AppCompatTextView) ai4.a(view, R.id.txt6hourHeader);
                                        if (appCompatTextView6 != null) {
                                            i = R.id.txtBuysTitle;
                                            AppCompatTextView appCompatTextView7 = (AppCompatTextView) ai4.a(view, R.id.txtBuysTitle);
                                            if (appCompatTextView7 != null) {
                                                i = R.id.txtBuysValue;
                                                AppCompatTextView appCompatTextView8 = (AppCompatTextView) ai4.a(view, R.id.txtBuysValue);
                                                if (appCompatTextView8 != null) {
                                                    i = R.id.txt_current_token_1;
                                                    AppCompatTextView appCompatTextView9 = (AppCompatTextView) ai4.a(view, R.id.txt_current_token_1);
                                                    if (appCompatTextView9 != null) {
                                                        i = R.id.txt_current_token_2;
                                                        AppCompatTextView appCompatTextView10 = (AppCompatTextView) ai4.a(view, R.id.txt_current_token_2);
                                                        if (appCompatTextView10 != null) {
                                                            i = R.id.txt_current_token_3;
                                                            AppCompatTextView appCompatTextView11 = (AppCompatTextView) ai4.a(view, R.id.txt_current_token_3);
                                                            if (appCompatTextView11 != null) {
                                                                i = R.id.txtFdvTitle;
                                                                AppCompatTextView appCompatTextView12 = (AppCompatTextView) ai4.a(view, R.id.txtFdvTitle);
                                                                if (appCompatTextView12 != null) {
                                                                    i = R.id.txtFdvValue;
                                                                    AppCompatTextView appCompatTextView13 = (AppCompatTextView) ai4.a(view, R.id.txtFdvValue);
                                                                    if (appCompatTextView13 != null) {
                                                                        i = R.id.txtFiveMinuteTitle;
                                                                        AppCompatTextView appCompatTextView14 = (AppCompatTextView) ai4.a(view, R.id.txtFiveMinuteTitle);
                                                                        if (appCompatTextView14 != null) {
                                                                            i = R.id.txtFiveMinuteValue;
                                                                            AppCompatTextView appCompatTextView15 = (AppCompatTextView) ai4.a(view, R.id.txtFiveMinuteValue);
                                                                            if (appCompatTextView15 != null) {
                                                                                i = R.id.txt_from_pair_token;
                                                                                AppCompatTextView appCompatTextView16 = (AppCompatTextView) ai4.a(view, R.id.txt_from_pair_token);
                                                                                if (appCompatTextView16 != null) {
                                                                                    i = R.id.txtLiquidityTitle;
                                                                                    AppCompatTextView appCompatTextView17 = (AppCompatTextView) ai4.a(view, R.id.txtLiquidityTitle);
                                                                                    if (appCompatTextView17 != null) {
                                                                                        i = R.id.txtLiquidityValue;
                                                                                        AppCompatTextView appCompatTextView18 = (AppCompatTextView) ai4.a(view, R.id.txtLiquidityValue);
                                                                                        if (appCompatTextView18 != null) {
                                                                                            i = R.id.txtMktCapTitle;
                                                                                            AppCompatTextView appCompatTextView19 = (AppCompatTextView) ai4.a(view, R.id.txtMktCapTitle);
                                                                                            if (appCompatTextView19 != null) {
                                                                                                i = R.id.txtMktCapValue;
                                                                                                AppCompatTextView appCompatTextView20 = (AppCompatTextView) ai4.a(view, R.id.txtMktCapValue);
                                                                                                if (appCompatTextView20 != null) {
                                                                                                    i = R.id.txtOneHourTitle;
                                                                                                    AppCompatTextView appCompatTextView21 = (AppCompatTextView) ai4.a(view, R.id.txtOneHourTitle);
                                                                                                    if (appCompatTextView21 != null) {
                                                                                                        i = R.id.txtOneHourValue;
                                                                                                        AppCompatTextView appCompatTextView22 = (AppCompatTextView) ai4.a(view, R.id.txtOneHourValue);
                                                                                                        if (appCompatTextView22 != null) {
                                                                                                            i = R.id.txtPriceTitle;
                                                                                                            AppCompatTextView appCompatTextView23 = (AppCompatTextView) ai4.a(view, R.id.txtPriceTitle);
                                                                                                            if (appCompatTextView23 != null) {
                                                                                                                i = R.id.txtPriceUsdTitle;
                                                                                                                AppCompatTextView appCompatTextView24 = (AppCompatTextView) ai4.a(view, R.id.txtPriceUsdTitle);
                                                                                                                if (appCompatTextView24 != null) {
                                                                                                                    i = R.id.txtPriceUsdValue;
                                                                                                                    AppCompatTextView appCompatTextView25 = (AppCompatTextView) ai4.a(view, R.id.txtPriceUsdValue);
                                                                                                                    if (appCompatTextView25 != null) {
                                                                                                                        i = R.id.txtPriceValue;
                                                                                                                        AppCompatTextView appCompatTextView26 = (AppCompatTextView) ai4.a(view, R.id.txtPriceValue);
                                                                                                                        if (appCompatTextView26 != null) {
                                                                                                                            i = R.id.txtSellsTitle;
                                                                                                                            AppCompatTextView appCompatTextView27 = (AppCompatTextView) ai4.a(view, R.id.txtSellsTitle);
                                                                                                                            if (appCompatTextView27 != null) {
                                                                                                                                i = R.id.txtSellsValue;
                                                                                                                                AppCompatTextView appCompatTextView28 = (AppCompatTextView) ai4.a(view, R.id.txtSellsValue);
                                                                                                                                if (appCompatTextView28 != null) {
                                                                                                                                    i = R.id.txtSixHourTitle;
                                                                                                                                    AppCompatTextView appCompatTextView29 = (AppCompatTextView) ai4.a(view, R.id.txtSixHourTitle);
                                                                                                                                    if (appCompatTextView29 != null) {
                                                                                                                                        i = R.id.txtSixHourValue;
                                                                                                                                        AppCompatTextView appCompatTextView30 = (AppCompatTextView) ai4.a(view, R.id.txtSixHourValue);
                                                                                                                                        if (appCompatTextView30 != null) {
                                                                                                                                            i = R.id.txt_to_pair_token;
                                                                                                                                            AppCompatTextView appCompatTextView31 = (AppCompatTextView) ai4.a(view, R.id.txt_to_pair_token);
                                                                                                                                            if (appCompatTextView31 != null) {
                                                                                                                                                i = R.id.txtTxnsTitle;
                                                                                                                                                AppCompatTextView appCompatTextView32 = (AppCompatTextView) ai4.a(view, R.id.txtTxnsTitle);
                                                                                                                                                if (appCompatTextView32 != null) {
                                                                                                                                                    i = R.id.txtTxnsValue;
                                                                                                                                                    AppCompatTextView appCompatTextView33 = (AppCompatTextView) ai4.a(view, R.id.txtTxnsValue);
                                                                                                                                                    if (appCompatTextView33 != null) {
                                                                                                                                                        i = R.id.txtVolumeTitle;
                                                                                                                                                        AppCompatTextView appCompatTextView34 = (AppCompatTextView) ai4.a(view, R.id.txtVolumeTitle);
                                                                                                                                                        if (appCompatTextView34 != null) {
                                                                                                                                                            i = R.id.txtVolumeValue;
                                                                                                                                                            AppCompatTextView appCompatTextView35 = (AppCompatTextView) ai4.a(view, R.id.txtVolumeValue);
                                                                                                                                                            if (appCompatTextView35 != null) {
                                                                                                                                                                return new dk4((MaterialCardView) view, appCompatImageView, appCompatImageView2, appCompatImageView3, appCompatTextView, appCompatTextView2, appCompatTextView3, appCompatTextView4, appCompatTextView5, appCompatTextView6, appCompatTextView7, appCompatTextView8, appCompatTextView9, appCompatTextView10, appCompatTextView11, appCompatTextView12, appCompatTextView13, appCompatTextView14, appCompatTextView15, appCompatTextView16, appCompatTextView17, appCompatTextView18, appCompatTextView19, appCompatTextView20, appCompatTextView21, appCompatTextView22, appCompatTextView23, appCompatTextView24, appCompatTextView25, appCompatTextView26, appCompatTextView27, appCompatTextView28, appCompatTextView29, appCompatTextView30, appCompatTextView31, appCompatTextView32, appCompatTextView33, appCompatTextView34, appCompatTextView35);
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
