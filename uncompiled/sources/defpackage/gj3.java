package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import androidx.recyclerview.widget.o;
import com.google.android.material.checkbox.MaterialCheckBox;
import defpackage.gj3;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.MyTokenType;

/* compiled from: SelectChainAdapter.kt */
/* renamed from: gj3  reason: default package */
/* loaded from: classes2.dex */
public final class gj3 extends o<MyTokenType, b> {
    public final rc1<te4> a;

    /* compiled from: SelectChainAdapter.kt */
    /* renamed from: gj3$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<MyTokenType> {
        public static final a a = new a();

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(MyTokenType myTokenType, MyTokenType myTokenType2) {
            fs1.f(myTokenType, "oldItem");
            fs1.f(myTokenType2, "newItem");
            return myTokenType.getTokenType() == myTokenType2.getTokenType() && myTokenType.isSelect() == myTokenType2.isSelect();
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(MyTokenType myTokenType, MyTokenType myTokenType2) {
            fs1.f(myTokenType, "oldItem");
            fs1.f(myTokenType2, "newItem");
            return fs1.b(myTokenType, myTokenType2);
        }
    }

    /* compiled from: SelectChainAdapter.kt */
    /* renamed from: gj3$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final /* synthetic */ gj3 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(gj3 gj3Var, View view) {
            super(view);
            fs1.f(gj3Var, "this$0");
            fs1.f(view, "itemView");
            this.a = gj3Var;
        }

        public static final void c(MyTokenType myTokenType, gj3 gj3Var, b bVar, View view) {
            fs1.f(myTokenType, "$myTokenType");
            fs1.f(gj3Var, "this$0");
            fs1.f(bVar, "this$1");
            myTokenType.setSelect(!myTokenType.isSelect());
            gj3Var.notifyItemChanged(bVar.getAbsoluteAdapterPosition());
            rc1<te4> a = gj3Var.a();
            if (a == null) {
                return;
            }
            a.invoke();
        }

        public final void b(final MyTokenType myTokenType) {
            fs1.f(myTokenType, "myTokenType");
            vs1 a = vs1.a(this.itemView);
            final gj3 gj3Var = this.a;
            TokenType tokenType = myTokenType.getTokenType();
            TextView textView = a.g;
            textView.setText(tokenType.getChainTitle() + " (" + tokenType.getDisplayName() + ')');
            com.bumptech.glide.a.u(a.c).w(Integer.valueOf(tokenType.getIcon())).I0(a.c);
            MaterialCheckBox materialCheckBox = a.b;
            fs1.e(materialCheckBox, "checkableIcon");
            materialCheckBox.setVisibility(0);
            a.b.setChecked(myTokenType.isSelect());
            a.e.setOnClickListener(new View.OnClickListener() { // from class: hj3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    gj3.b.c(MyTokenType.this, gj3Var, this, view);
                }
            });
        }
    }

    public gj3(rc1<te4> rc1Var) {
        super(a.a);
        this.a = rc1Var;
    }

    public final rc1<te4> a() {
        return this.a;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        MyTokenType item = getItem(i);
        fs1.e(item, "getItem(position)");
        bVar.b(item);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_check_token, viewGroup, false);
        fs1.e(inflate, "from(parent.context).inf…heck_token, parent,false)");
        return new b(this, inflate);
    }
}
