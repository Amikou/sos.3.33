package defpackage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.play.core.assetpacks.AssetPackException;
import com.google.android.play.core.assetpacks.AssetPackState;
import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.k;
import com.google.android.play.core.internal.p;
import com.google.android.play.core.tasks.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: st4  reason: default package */
/* loaded from: classes2.dex */
public final class st4 implements zy4 {
    public static final it4 f = new it4("AssetPackServiceImpl");
    public static final Intent g = new Intent("com.google.android.play.core.assetmoduleservice.BIND_ASSET_MODULE_SERVICE").setPackage("com.android.vending");
    public final String a;
    public final fv4 b;
    public zt4<p> c;
    public zt4<p> d;
    public final AtomicBoolean e = new AtomicBoolean();

    public st4(Context context, fv4 fv4Var) {
        this.a = context.getPackageName();
        this.b = fv4Var;
        if (hv4.a(context)) {
            Context c = ny4.c(context);
            it4 it4Var = f;
            Intent intent = g;
            this.c = new zt4<>(c, it4Var, "AssetPackService", intent, az4.c);
            this.d = new zt4<>(ny4.c(context), it4Var, "AssetPackService-keepAlive", intent, az4.b);
        }
        f.a("AssetPackService initiated.", new Object[0]);
    }

    public static Bundle h(int i, String str) {
        Bundle i2 = i(i);
        i2.putString("module_name", str);
        return i2;
    }

    public static Bundle i(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("session_id", i);
        return bundle;
    }

    public static Bundle j() {
        Bundle bundle = new Bundle();
        bundle.putInt("playcore_version_code", 10901);
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(0);
        arrayList.add(1);
        bundle.putIntegerArrayList("supported_compression_formats", arrayList);
        bundle.putIntegerArrayList("supported_patch_formats", new ArrayList<>());
        return bundle;
    }

    public static /* synthetic */ ArrayList k(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Bundle bundle = new Bundle();
            bundle.putString("module_name", (String) it.next());
            arrayList.add(bundle);
        }
        return arrayList;
    }

    public static /* synthetic */ Bundle m(Map map) {
        Bundle j = j();
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
        for (Map.Entry entry : map.entrySet()) {
            Bundle bundle = new Bundle();
            bundle.putString("installed_asset_module_name", (String) entry.getKey());
            bundle.putLong("installed_asset_module_version", ((Long) entry.getValue()).longValue());
            arrayList.add(bundle);
        }
        j.putParcelableArrayList("installed_asset_module", arrayList);
        return j;
    }

    public static /* synthetic */ Bundle q(int i, String str, String str2, int i2) {
        Bundle h = h(i, str);
        h.putString("slice_id", str2);
        h.putInt("chunk_number", i2);
        return h;
    }

    public static /* synthetic */ List u(st4 st4Var, List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AssetPackState next = li.b((Bundle) it.next(), st4Var.b).e().values().iterator().next();
            if (next == null) {
                f.b("onGetSessionStates: Bundle contained no pack.", new Object[0]);
            }
            if (k.d(next.g())) {
                arrayList.add(next.f());
            }
        }
        return arrayList;
    }

    public static <T> l34<T> y() {
        f.b("onError(%d)", -11);
        return a.c(new AssetPackException(-11));
    }

    @Override // defpackage.zy4
    public final synchronized void a() {
        if (this.d == null) {
            f.e("Keep alive connection manager is not initialized.", new Object[0]);
            return;
        }
        it4 it4Var = f;
        it4Var.d("keepAlive", new Object[0]);
        if (!this.e.compareAndSet(false, true)) {
            it4Var.d("Service is already kept alive.", new Object[0]);
            return;
        }
        tx4 tx4Var = new tx4();
        this.d.a(new ht4(this, tx4Var, tx4Var));
    }

    @Override // defpackage.zy4
    public final void b(int i) {
        if (this.c == null) {
            throw new bj("The Play Store app is not installed or is an unofficial version.", i);
        }
        f.d("notifySessionFailed", new Object[0]);
        tx4 tx4Var = new tx4();
        this.c.a(new ft4(this, tx4Var, i, tx4Var));
    }

    @Override // defpackage.zy4
    public final void c(int i, String str, String str2, int i2) {
        if (this.c == null) {
            throw new bj("The Play Store app is not installed or is an unofficial version.", i);
        }
        f.d("notifyChunkTransferred", new Object[0]);
        tx4 tx4Var = new tx4();
        this.c.a(new dt4(this, tx4Var, i, str, str2, i2, tx4Var));
    }

    @Override // defpackage.zy4
    public final void d(int i, String str) {
        x(i, str, 10);
    }

    @Override // defpackage.zy4
    public final l34<ParcelFileDescriptor> e(int i, String str, String str2, int i2) {
        if (this.c == null) {
            return y();
        }
        f.d("getChunkFileDescriptor(%s, %s, %d, session=%d)", str, str2, Integer.valueOf(i2), Integer.valueOf(i));
        tx4 tx4Var = new tx4();
        this.c.a(new gt4(this, tx4Var, i, str, str2, i2, tx4Var));
        return tx4Var.c();
    }

    @Override // defpackage.zy4
    public final l34<List<String>> f(Map<String, Long> map) {
        if (this.c == null) {
            return y();
        }
        f.d("syncPacks", new Object[0]);
        tx4 tx4Var = new tx4();
        this.c.a(new hz4(this, tx4Var, map, tx4Var));
        return tx4Var.c();
    }

    @Override // defpackage.zy4
    public final void g(List<String> list) {
        if (this.c == null) {
            return;
        }
        f.d("cancelDownloads(%s)", list);
        tx4 tx4Var = new tx4();
        this.c.a(new gz4(this, tx4Var, list, tx4Var));
    }

    public final void x(int i, String str, int i2) {
        if (this.c == null) {
            throw new bj("The Play Store app is not installed or is an unofficial version.", i);
        }
        f.d("notifyModuleCompleted", new Object[0]);
        tx4 tx4Var = new tx4();
        this.c.a(new et4(this, tx4Var, i, str, tx4Var, i2));
    }
}
