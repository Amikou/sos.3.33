package defpackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import defpackage.i41;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Filter.java */
@JsonInclude(JsonInclude.Include.NON_NULL)
/* renamed from: i41  reason: default package */
/* loaded from: classes3.dex */
public abstract class i41<T extends i41> {
    private T thisObj = getThis();
    private List<Object> topics = new ArrayList();

    /* compiled from: Filter.java */
    /* renamed from: i41$a */
    /* loaded from: classes3.dex */
    public static class a {
        private List<b> topics = new ArrayList();

        public a(String... strArr) {
            for (String str : strArr) {
                if (str != null) {
                    this.topics.add(new b(str));
                } else {
                    this.topics.add(new b());
                }
            }
        }

        public List<b> getValue() {
            return this.topics;
        }
    }

    /* compiled from: Filter.java */
    /* renamed from: i41$b */
    /* loaded from: classes3.dex */
    public static class b {
        private String topic;

        public b() {
            this.topic = null;
        }

        public String getValue() {
            return this.topic;
        }

        public b(String str) {
            this.topic = str;
        }
    }

    public T addNullTopic() {
        this.topics.add(new b());
        return getThis();
    }

    public T addOptionalTopics(String... strArr) {
        this.topics.add(new a(strArr));
        return getThis();
    }

    public T addSingleTopic(String str) {
        this.topics.add(new b(str));
        return getThis();
    }

    public abstract T getThis();

    public List<Object> getTopics() {
        return this.topics;
    }
}
