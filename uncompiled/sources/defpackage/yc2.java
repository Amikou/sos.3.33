package defpackage;

import androidx.annotation.RecentlyNonNull;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: yc2  reason: default package */
/* loaded from: classes.dex */
public class yc2 implements ThreadFactory {
    public final String a;
    public final ThreadFactory f0;

    public yc2(@RecentlyNonNull String str) {
        this(str, 0);
    }

    @Override // java.util.concurrent.ThreadFactory
    @RecentlyNonNull
    public Thread newThread(@RecentlyNonNull Runnable runnable) {
        Thread newThread = this.f0.newThread(new a45(runnable, 0));
        newThread.setName(this.a);
        return newThread;
    }

    public yc2(String str, int i) {
        this.f0 = Executors.defaultThreadFactory();
        this.a = (String) zt2.k(str, "Name must not be null");
    }
}
