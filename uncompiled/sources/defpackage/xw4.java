package defpackage;

import java.io.InputStream;

/* renamed from: xw4  reason: default package */
/* loaded from: classes2.dex */
public final class xw4 extends fw4 {
    public final int c;
    public final long d;
    public final int e;
    public final long f;
    public final int g;
    public final String h;
    public final long i;
    public final InputStream j;

    public xw4(int i, String str, int i2, long j, int i3, long j2, int i4, String str2, long j3, InputStream inputStream) {
        super(i, str);
        this.c = i2;
        this.d = j;
        this.e = i3;
        this.f = j2;
        this.g = i4;
        this.h = str2;
        this.i = j3;
        this.j = inputStream;
    }
}
