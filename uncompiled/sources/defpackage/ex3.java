package defpackage;

import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.Pair;
import net.safemoon.androidwallet.SafeswapPair;
import net.safemoon.androidwallet.SafeswapRouter;
import net.safemoon.androidwallet.model.swap.PairsData;
import net.safemoon.androidwallet.model.swap.Swap;
import net.safemoon.androidwallet.model.swap.Token;

/* compiled from: SwapBestTrade.kt */
/* renamed from: ex3  reason: default package */
/* loaded from: classes2.dex */
public abstract class ex3 {
    public final Swap a;
    public final List<PairsData> b;
    public final int c;
    public final int d;
    public final int e;

    public ex3(SafeswapRouter safeswapRouter, Swap swap) {
        fs1.f(safeswapRouter, "mainRouter");
        fs1.f(swap, "swap");
        this.a = swap;
        this.b = new ArrayList();
        this.c = 10000;
        this.d = 25;
        this.e = 10000 - 25;
    }

    public static /* synthetic */ ArrayList b(ex3 ex3Var, List list, BigInteger bigInteger, String str, String str2, int i, int i2, ArrayList arrayList, BigInteger bigInteger2, String str3, ArrayList arrayList2, int i3, Object obj) {
        BigInteger bigInteger3;
        if (obj == null) {
            ArrayList arrayList3 = (i3 & 1) != 0 ? new ArrayList() : list;
            if ((i3 & 2) != 0) {
                BigInteger bigInteger4 = BigInteger.ZERO;
                fs1.e(bigInteger4, "ZERO");
                bigInteger3 = bigInteger4;
            } else {
                bigInteger3 = bigInteger;
            }
            return ex3Var.a(arrayList3, bigInteger3, str, str2, i, i2, (i3 & 64) != 0 ? new ArrayList() : arrayList, (i3 & 128) != 0 ? bigInteger3 : bigInteger2, (i3 & 256) != 0 ? str : str3, (i3 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? new ArrayList() : arrayList2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: bestTradeExactIn");
    }

    public static /* synthetic */ ArrayList d(ex3 ex3Var, List list, BigInteger bigInteger, String str, String str2, int i, int i2, ArrayList arrayList, BigInteger bigInteger2, String str3, ArrayList arrayList2, int i3, Object obj) {
        BigInteger bigInteger3;
        if (obj == null) {
            ArrayList arrayList3 = (i3 & 1) != 0 ? new ArrayList() : list;
            if ((i3 & 2) != 0) {
                BigInteger bigInteger4 = BigInteger.ZERO;
                fs1.e(bigInteger4, "ZERO");
                bigInteger3 = bigInteger4;
            } else {
                bigInteger3 = bigInteger;
            }
            return ex3Var.c(arrayList3, bigInteger3, str, str2, i, i2, (i3 & 64) != 0 ? new ArrayList() : arrayList, (i3 & 128) != 0 ? bigInteger3 : bigInteger2, (i3 & 256) != 0 ? str2 : str3, (i3 & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? new ArrayList() : arrayList2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: bestTradeExactOut");
    }

    public final ArrayList<l84> a(List<PairsData> list, BigInteger bigInteger, String str, String str2, int i, int i2, ArrayList<PairsData> arrayList, BigInteger bigInteger2, String str3, ArrayList<l84> arrayList2) {
        String contractAddress;
        String lowerCase;
        String contractAddress2;
        String lowerCase2;
        String str4;
        Object obj;
        Pair pair;
        String lowerCase3;
        String lowerCase4;
        BigInteger bigInteger3;
        ArrayList<l84> arrayList3;
        String str5;
        String str6;
        String lowerCase5;
        String lowerCase6;
        Object obj2;
        String reserve0;
        String reserve1;
        String lowerCase7;
        String lowerCase8;
        BigInteger bigInteger4 = bigInteger;
        String str7 = str;
        ArrayList<PairsData> arrayList4 = arrayList;
        ArrayList<l84> arrayList5 = arrayList2;
        if (list.isEmpty() || i == 0) {
            return arrayList5;
        }
        if (fs1.b(bigInteger2, bigInteger4) || arrayList.size() > 0) {
            StringBuilder sb = new StringBuilder();
            String str8 = "MaxHops=";
            sb.append("MaxHops=");
            sb.append(i);
            String str9 = ", PairSize=";
            sb.append(", PairSize=");
            sb.append(list.size());
            sb.append(", tokenIn=");
            sb.append(str7);
            sb.append(", tokenOut=");
            sb.append(str2);
            e30.c0(sb.toString(), "BestTrade-BeforeLoopStart");
            for (PairsData pairsData : list) {
                Token token0 = pairsData.getToken0();
                if (token0 == null || (contractAddress = token0.getContractAddress()) == null) {
                    lowerCase = null;
                } else {
                    lowerCase = contractAddress.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                Token token1 = pairsData.getToken1();
                if (token1 == null || (contractAddress2 = token1.getContractAddress()) == null) {
                    lowerCase2 = null;
                } else {
                    lowerCase2 = contractAddress2.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                if (fs1.b(str7, lowerCase) || fs1.b(str7, lowerCase2)) {
                    if (pairsData.getReserve0() != null && pairsData.getReserve1() != null) {
                        Iterator it = this.b.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                str4 = lowerCase2;
                                obj2 = null;
                                break;
                            }
                            obj2 = it.next();
                            String pairAddress = ((PairsData) obj2).getPairAddress();
                            str4 = lowerCase2;
                            if (pairAddress == null) {
                                lowerCase7 = null;
                            } else {
                                lowerCase7 = pairAddress.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase7, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            String pairAddress2 = pairsData.getPairAddress();
                            Iterator it2 = it;
                            if (pairAddress2 == null) {
                                lowerCase8 = null;
                            } else {
                                lowerCase8 = pairAddress2.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase8, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            if (fs1.b(lowerCase7, lowerCase8)) {
                                break;
                            }
                            lowerCase2 = str4;
                            it = it2;
                        }
                        PairsData pairsData2 = (PairsData) obj2;
                        String str10 = "0";
                        if (pairsData2 == null || (reserve0 = pairsData2.getReserve0()) == null) {
                            reserve0 = "0";
                        }
                        BigInteger bigInteger5 = new BigInteger(reserve0);
                        if (pairsData2 != null && (reserve1 = pairsData2.getReserve1()) != null) {
                            str10 = reserve1;
                        }
                        pair = new Pair(bigInteger5, new BigInteger(str10));
                    } else {
                        str4 = lowerCase2;
                        String pairAddress3 = pairsData.getPairAddress();
                        fs1.d(pairAddress3);
                        pc4<BigInteger, BigInteger, BigInteger> send = n(pairAddress3).o().send();
                        BigInteger component1 = send.component1();
                        BigInteger component2 = send.component2();
                        Iterator it3 = this.b.iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                obj = null;
                                break;
                            }
                            obj = it3.next();
                            Iterator it4 = it3;
                            String pairAddress4 = ((PairsData) obj).getPairAddress();
                            if (pairAddress4 == null) {
                                lowerCase3 = null;
                            } else {
                                lowerCase3 = pairAddress4.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            String pairAddress5 = pairsData.getPairAddress();
                            if (pairAddress5 == null) {
                                lowerCase4 = null;
                            } else {
                                lowerCase4 = pairAddress5.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            if (fs1.b(lowerCase3, lowerCase4)) {
                                break;
                            }
                            it3 = it4;
                        }
                        PairsData pairsData3 = (PairsData) obj;
                        if (pairsData3 != null) {
                            pairsData3.setReserve0(component1 == null ? null : component1.toString());
                            pairsData3.setReserve1(component2 == null ? null : component2.toString());
                            te4 te4Var = te4.a;
                        }
                        pair = new Pair(component1, component2);
                    }
                    BigInteger bigInteger6 = (BigInteger) pair.component1();
                    BigInteger bigInteger7 = (BigInteger) pair.component2();
                    BigInteger bigInteger8 = BigInteger.ZERO;
                    if (fs1.b(bigInteger6, bigInteger8)) {
                        bigInteger4 = bigInteger;
                        str7 = str;
                        arrayList4 = arrayList;
                    } else if (!fs1.b(bigInteger7, bigInteger8)) {
                        String str11 = fs1.b(lowerCase, str7) ? str4 : lowerCase;
                        try {
                            if (fs1.b(lowerCase, str7)) {
                                fs1.e(bigInteger6, "reserve0");
                                fs1.e(bigInteger7, "reserve1");
                                bigInteger3 = k(bigInteger4, bigInteger6, bigInteger7);
                            } else {
                                fs1.e(bigInteger7, "reserve1");
                                fs1.e(bigInteger6, "reserve0");
                                bigInteger3 = k(bigInteger4, bigInteger7, bigInteger6);
                            }
                        } catch (Exception unused) {
                            bigInteger3 = BigInteger.ZERO;
                        }
                        BigInteger bigInteger9 = bigInteger3;
                        if (!fs1.b(bigInteger9, BigInteger.ZERO)) {
                            if (fs1.b(str11, str2)) {
                                e30.c0(str8 + i + str9 + list.size() + ", bestTrades=" + arrayList2.size(), "BestTrade-BeforeSort");
                                ArrayList arrayList6 = new ArrayList();
                                arrayList6.addAll(arrayList4);
                                arrayList6.add(pairsData);
                                te4 te4Var2 = te4.a;
                                fs1.e(bigInteger9, "amountOut");
                                String str12 = str9;
                                arrayList3 = arrayList2;
                                s(arrayList3, new l84(arrayList6, str3, str2, bigInteger2, bigInteger9, Utils.DOUBLE_EPSILON, 32, null), i2);
                                StringBuilder sb2 = new StringBuilder();
                                str5 = str8;
                                sb2.append(str5);
                                sb2.append(i);
                                str6 = str12;
                                sb2.append(str6);
                                sb2.append(list.size());
                                sb2.append(", bestTrades=");
                                sb2.append(arrayList2.size());
                                e30.c0(sb2.toString(), "BestTrade-AfterSort");
                                str7 = str;
                            } else {
                                arrayList3 = arrayList2;
                                String str13 = str9;
                                str5 = str8;
                                str6 = str13;
                                if (i <= 1 || list.size() <= 1) {
                                    str7 = str;
                                    arrayList4 = arrayList;
                                } else {
                                    List<PairsData> arrayList7 = new ArrayList<>();
                                    for (Object obj3 : list) {
                                        String str14 = str6;
                                        String pairAddress6 = ((PairsData) obj3).getPairAddress();
                                        String str15 = str5;
                                        if (pairAddress6 == null) {
                                            lowerCase5 = null;
                                        } else {
                                            lowerCase5 = pairAddress6.toLowerCase(Locale.ROOT);
                                            fs1.e(lowerCase5, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                                        }
                                        String pairAddress7 = pairsData.getPairAddress();
                                        if (pairAddress7 == null) {
                                            lowerCase6 = null;
                                        } else {
                                            lowerCase6 = pairAddress7.toLowerCase(Locale.ROOT);
                                            fs1.e(lowerCase6, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                                        }
                                        if (!dv3.t(lowerCase5, lowerCase6, true)) {
                                            arrayList7.add(obj3);
                                        }
                                        str5 = str15;
                                        str6 = str14;
                                    }
                                    String str16 = str5;
                                    fs1.e(bigInteger9, "amountOut");
                                    fs1.d(str11);
                                    ArrayList<PairsData> arrayList8 = new ArrayList<>();
                                    arrayList8.addAll(arrayList4);
                                    arrayList8.add(pairsData);
                                    te4 te4Var3 = te4.a;
                                    a(arrayList7, bigInteger9, str11, str2, i - 1, i2, arrayList8, bigInteger2, str3, arrayList2);
                                    arrayList4 = arrayList;
                                    arrayList5 = arrayList3;
                                    str9 = str6;
                                    str8 = str16;
                                    bigInteger4 = bigInteger;
                                    str7 = str;
                                }
                            }
                            arrayList5 = arrayList3;
                            bigInteger4 = bigInteger;
                            String str17 = str5;
                            str9 = str6;
                            str8 = str17;
                        }
                    }
                    arrayList5 = arrayList2;
                }
            }
            return arrayList5;
        }
        return arrayList5;
    }

    public final ArrayList<l84> c(List<PairsData> list, BigInteger bigInteger, String str, String str2, int i, int i2, ArrayList<PairsData> arrayList, BigInteger bigInteger2, String str3, ArrayList<l84> arrayList2) {
        String contractAddress;
        String lowerCase;
        String contractAddress2;
        String lowerCase2;
        String str4;
        Object obj;
        Pair pair;
        String lowerCase3;
        String lowerCase4;
        BigInteger bigInteger3;
        String lowerCase5;
        String lowerCase6;
        Object obj2;
        String reserve0;
        String reserve1;
        String lowerCase7;
        String lowerCase8;
        ArrayList<l84> arrayList3 = arrayList2;
        if (list.isEmpty() || i == 0) {
            return arrayList3;
        }
        if (fs1.b(bigInteger2, bigInteger) || arrayList.size() > 0) {
            for (PairsData pairsData : list) {
                Token token0 = pairsData.getToken0();
                if (token0 == null || (contractAddress = token0.getContractAddress()) == null) {
                    lowerCase = null;
                } else {
                    lowerCase = contractAddress.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                Token token1 = pairsData.getToken1();
                if (token1 == null || (contractAddress2 = token1.getContractAddress()) == null) {
                    lowerCase2 = null;
                } else {
                    lowerCase2 = contractAddress2.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                if (fs1.b(str2, lowerCase) || fs1.b(str2, lowerCase2)) {
                    if (pairsData.getReserve0() != null && pairsData.getReserve1() != null) {
                        Iterator<T> it = this.b.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj2 = null;
                                break;
                            }
                            obj2 = it.next();
                            String pairAddress = ((PairsData) obj2).getPairAddress();
                            if (pairAddress == null) {
                                lowerCase7 = null;
                            } else {
                                lowerCase7 = pairAddress.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase7, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            String pairAddress2 = pairsData.getPairAddress();
                            if (pairAddress2 == null) {
                                lowerCase8 = null;
                            } else {
                                lowerCase8 = pairAddress2.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase8, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            if (fs1.b(lowerCase7, lowerCase8)) {
                                break;
                            }
                        }
                        PairsData pairsData2 = (PairsData) obj2;
                        String str5 = "0";
                        if (pairsData2 == null || (reserve0 = pairsData2.getReserve0()) == null) {
                            reserve0 = "0";
                        }
                        BigInteger bigInteger4 = new BigInteger(reserve0);
                        if (pairsData2 != null && (reserve1 = pairsData2.getReserve1()) != null) {
                            str5 = reserve1;
                        }
                        pair = new Pair(bigInteger4, new BigInteger(str5));
                        str4 = lowerCase2;
                    } else {
                        String pairAddress3 = pairsData.getPairAddress();
                        fs1.d(pairAddress3);
                        pc4<BigInteger, BigInteger, BigInteger> send = n(pairAddress3).o().send();
                        BigInteger component1 = send.component1();
                        BigInteger component2 = send.component2();
                        Iterator it2 = this.b.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                str4 = lowerCase2;
                                obj = null;
                                break;
                            }
                            obj = it2.next();
                            String pairAddress4 = ((PairsData) obj).getPairAddress();
                            str4 = lowerCase2;
                            if (pairAddress4 == null) {
                                lowerCase3 = null;
                            } else {
                                lowerCase3 = pairAddress4.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            String pairAddress5 = pairsData.getPairAddress();
                            Iterator it3 = it2;
                            if (pairAddress5 == null) {
                                lowerCase4 = null;
                            } else {
                                lowerCase4 = pairAddress5.toLowerCase(Locale.ROOT);
                                fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            }
                            if (fs1.b(lowerCase3, lowerCase4)) {
                                break;
                            }
                            lowerCase2 = str4;
                            it2 = it3;
                        }
                        PairsData pairsData3 = (PairsData) obj;
                        if (pairsData3 != null) {
                            pairsData3.setReserve0(component1 == null ? null : component1.toString());
                            pairsData3.setReserve1(component2 == null ? null : component2.toString());
                            te4 te4Var = te4.a;
                        }
                        pair = new Pair(component1, component2);
                    }
                    BigInteger bigInteger5 = (BigInteger) pair.component1();
                    BigInteger bigInteger6 = (BigInteger) pair.component2();
                    BigInteger bigInteger7 = BigInteger.ZERO;
                    if (!fs1.b(bigInteger5, bigInteger7)) {
                        if (!fs1.b(bigInteger6, bigInteger7)) {
                            String str6 = fs1.b(lowerCase, str2) ? str4 : lowerCase;
                            try {
                                if (fs1.b(lowerCase, str2)) {
                                    fs1.e(bigInteger5, "reserve0");
                                    fs1.e(bigInteger6, "reserve1");
                                    bigInteger3 = j(bigInteger, bigInteger5, bigInteger6);
                                } else {
                                    fs1.e(bigInteger6, "reserve1");
                                    fs1.e(bigInteger5, "reserve0");
                                    bigInteger3 = j(bigInteger, bigInteger6, bigInteger5);
                                }
                            } catch (Exception unused) {
                                bigInteger3 = BigInteger.ZERO;
                            }
                            BigInteger bigInteger8 = bigInteger3;
                            if (!fs1.b(bigInteger8, BigInteger.ZERO)) {
                                if (fs1.b(str6, str)) {
                                    ArrayList arrayList4 = new ArrayList();
                                    arrayList4.add(pairsData);
                                    arrayList4.addAll(arrayList);
                                    te4 te4Var2 = te4.a;
                                    fs1.e(bigInteger8, "amountIn");
                                    s(arrayList3, new l84(arrayList4, str, str3, bigInteger8, bigInteger2, Utils.DOUBLE_EPSILON, 32, null), i2);
                                } else if (i > 1 && list.size() > 1) {
                                    List<PairsData> arrayList5 = new ArrayList<>();
                                    for (Object obj3 : list) {
                                        String pairAddress6 = ((PairsData) obj3).getPairAddress();
                                        if (pairAddress6 == null) {
                                            lowerCase5 = null;
                                        } else {
                                            lowerCase5 = pairAddress6.toLowerCase(Locale.ROOT);
                                            fs1.e(lowerCase5, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                                        }
                                        String pairAddress7 = pairsData.getPairAddress();
                                        if (pairAddress7 == null) {
                                            lowerCase6 = null;
                                        } else {
                                            lowerCase6 = pairAddress7.toLowerCase(Locale.ROOT);
                                            fs1.e(lowerCase6, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                                        }
                                        if (!dv3.t(lowerCase5, lowerCase6, true)) {
                                            arrayList5.add(obj3);
                                        }
                                    }
                                    fs1.e(bigInteger8, "amountIn");
                                    fs1.d(str6);
                                    ArrayList<PairsData> arrayList6 = new ArrayList<>();
                                    arrayList6.add(pairsData);
                                    arrayList6.addAll(arrayList);
                                    te4 te4Var3 = te4.a;
                                    arrayList3 = arrayList2;
                                    c(arrayList5, bigInteger8, str, str6, i - 1, i2, arrayList6, bigInteger2, str3, arrayList3);
                                }
                            }
                        }
                    }
                    arrayList3 = arrayList2;
                }
            }
            return arrayList2;
        }
        return arrayList3;
    }

    public final BigDecimal e(l84 l84Var) {
        BigDecimal multiply = o(this.a, p(l84Var)).multiply(new BigDecimal(l84Var.a()));
        BigDecimal subtract = multiply.subtract(new BigDecimal(l84Var.b()));
        fs1.e(subtract, "quote.subtract(a.amountOut.toBigDecimal())");
        fs1.e(multiply, "quote");
        BigDecimal divide = subtract.divide(multiply, RoundingMode.HALF_EVEN);
        fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
        return divide;
    }

    public final BigDecimal f(BigDecimal bigDecimal, l84 l84Var) {
        Number valueOf;
        fs1.f(bigDecimal, "amountIn");
        fs1.f(l84Var, "a");
        int size = l84Var.d().size();
        if (size == 0) {
            valueOf = 0;
        } else {
            valueOf = Double.valueOf(1 - ((this.c * Math.pow(this.e, size)) / Math.pow(this.c, size + 1)));
        }
        BigDecimal multiply = bigDecimal.multiply(new BigDecimal(String.valueOf(valueOf.doubleValue())));
        fs1.e(multiply, "this.multiply(other)");
        return multiply;
    }

    public final List<String> g(String str, ArrayList<String> arrayList) {
        String str2;
        Object obj;
        Token token0;
        String contractAddress;
        String lowerCase;
        Token token1;
        String contractAddress2;
        String lowerCase2;
        fs1.f(str, "inputToken");
        fs1.f(arrayList, "pairs");
        ArrayList arrayList2 = new ArrayList();
        String lowerCase3 = str.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            String next = it.next();
            Iterator<T> it2 = this.b.iterator();
            while (true) {
                str2 = null;
                if (!it2.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it2.next();
                String pairAddress = ((PairsData) obj).getPairAddress();
                if (pairAddress == null) {
                    lowerCase2 = null;
                } else {
                    lowerCase2 = pairAddress.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                fs1.e(next, "pair");
                String lowerCase4 = next.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (fs1.b(lowerCase2, lowerCase4)) {
                    break;
                }
            }
            PairsData pairsData = (PairsData) obj;
            if (pairsData == null || (token0 = pairsData.getToken0()) == null || (contractAddress = token0.getContractAddress()) == null) {
                lowerCase = null;
            } else {
                lowerCase = contractAddress.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            if (pairsData != null && (token1 = pairsData.getToken1()) != null && (contractAddress2 = token1.getContractAddress()) != null) {
                String lowerCase5 = contractAddress2.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase5, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                str2 = lowerCase5;
            }
            if (dv3.t(lowerCase, lowerCase3, true)) {
                if (lowerCase != null) {
                    arrayList2.add(lowerCase);
                }
                if (str2 != null) {
                    lowerCase3 = str2;
                }
            } else {
                if (str2 != null) {
                    arrayList2.add(str2);
                }
                if (lowerCase != null) {
                    lowerCase3 = lowerCase;
                }
            }
        }
        if (arrayList2.size() > 0 && !dv3.t(lowerCase3, (String) arrayList2.get(0), true)) {
            arrayList2.add(lowerCase3);
        }
        return arrayList2;
    }

    public final List<Pair<Token, Token>> h(List<Token> list, Pair<String, String> pair) {
        fs1.f(list, "basePairs");
        fs1.f(pair, "pair");
        ArrayList arrayList = new ArrayList();
        Token token = new Token();
        token.setContractAddress(pair.getFirst());
        te4 te4Var = te4.a;
        Token token2 = new Token();
        token2.setContractAddress(pair.getSecond());
        arrayList.add(qc4.a(token, token2));
        for (Token token3 : list) {
            Token token4 = new Token();
            token4.setContractAddress(pair.getFirst());
            te4 te4Var2 = te4.a;
            arrayList.add(qc4.a(token3, token4));
        }
        for (Token token5 : list) {
            Token token6 = new Token();
            token6.setContractAddress(pair.getSecond());
            te4 te4Var3 = te4.a;
            arrayList.add(qc4.a(token5, token6));
        }
        Iterator<Integer> it = b20.h(list).iterator();
        while (it.hasNext()) {
            int b = ((or1) it).b();
            for (Number number : j20.X(b20.h(list), new sr1(0, b))) {
                arrayList.add(qc4.a(list.get(b), list.get(number.intValue())));
            }
        }
        return arrayList;
    }

    public final List<PairsData> i() {
        return this.b;
    }

    public final BigInteger j(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
        BigInteger bigInteger4 = BigInteger.ZERO;
        if (!fs1.b(bigInteger3, bigInteger4) && !fs1.b(bigInteger2, bigInteger4) && bigInteger.compareTo(bigInteger2) < 0) {
            BigInteger multiply = bigInteger3.multiply(bigInteger);
            fs1.e(multiply, "this.multiply(other)");
            BigInteger valueOf = BigInteger.valueOf(10000);
            fs1.e(valueOf, "BigInteger.valueOf(this.toLong())");
            BigInteger multiply2 = multiply.multiply(valueOf);
            fs1.e(multiply2, "this.multiply(other)");
            BigInteger subtract = bigInteger2.subtract(bigInteger);
            fs1.e(subtract, "this.subtract(other)");
            BigInteger valueOf2 = BigInteger.valueOf(9975);
            fs1.e(valueOf2, "BigInteger.valueOf(this.toLong())");
            BigInteger multiply3 = subtract.multiply(valueOf2);
            fs1.e(multiply3, "this.multiply(other)");
            BigInteger divide = multiply2.divide(multiply3);
            fs1.e(divide, "this.divide(other)");
            BigInteger valueOf3 = BigInteger.valueOf(1);
            fs1.e(valueOf3, "BigInteger.valueOf(this.toLong())");
            BigInteger add = divide.add(valueOf3);
            fs1.e(add, "this.add(other)");
            return add;
        }
        throw new Exception("insufficient liquidity error");
    }

    public final BigInteger k(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
        BigInteger bigInteger4 = BigInteger.ZERO;
        if (!fs1.b(bigInteger2, bigInteger4) && !fs1.b(bigInteger3, bigInteger4)) {
            BigInteger valueOf = BigInteger.valueOf(9975);
            fs1.e(valueOf, "BigInteger.valueOf(this.toLong())");
            BigInteger multiply = bigInteger.multiply(valueOf);
            fs1.e(multiply, "this.multiply(other)");
            BigInteger multiply2 = multiply.multiply(bigInteger3);
            fs1.e(multiply2, "this.multiply(other)");
            BigInteger valueOf2 = BigInteger.valueOf(10000);
            fs1.e(valueOf2, "BigInteger.valueOf(this.toLong())");
            BigInteger multiply3 = bigInteger2.multiply(valueOf2);
            fs1.e(multiply3, "this.multiply(other)");
            BigInteger add = multiply3.add(multiply);
            fs1.e(add, "this.add(other)");
            BigInteger divide = multiply2.divide(add);
            fs1.e(divide, "this.divide(other)");
            if (fs1.b(divide, bigInteger4)) {
                throw new Exception("insufficient liquidity error");
            }
            return divide;
        }
        throw new Exception("insufficient liquidity error");
    }

    public final l84 l(BigInteger bigInteger, String str, String str2) {
        double d;
        fs1.f(bigInteger, "amountIn");
        fs1.f(str, "tokenIn");
        fs1.f(str2, "tokenOut");
        ArrayList<l84> b = b(this, this.b, bigInteger, str, str2, 3, 50, null, null, null, null, 960, null);
        for (l84 l84Var : b) {
            try {
                d = q(l84Var).doubleValue();
            } catch (Exception unused) {
                d = 100.0d;
            }
            l84Var.e(d);
        }
        return (l84) b.get(0);
    }

    public final l84 m(BigInteger bigInteger, String str, String str2) {
        double d;
        fs1.f(bigInteger, "amountOut");
        fs1.f(str, "tokenIn");
        fs1.f(str2, "tokenOut");
        ArrayList<l84> d2 = d(this, this.b, bigInteger, str, str2, 3, 50, null, null, null, null, 960, null);
        for (l84 l84Var : d2) {
            try {
                d = q(l84Var).doubleValue();
            } catch (Exception unused) {
                d = 100.0d;
            }
            l84Var.e(d);
        }
        return (l84) d2.get(0);
    }

    public abstract SafeswapPair n(String str);

    public final BigDecimal o(Swap swap, List<String> list) {
        String str;
        Object obj;
        Token token0;
        String contractAddress;
        String lowerCase;
        String reserve0;
        BigDecimal divide;
        String reserve1;
        Token token1;
        String contractAddress2;
        String lowerCase2;
        fs1.f(swap, "sourceSwap");
        fs1.f(list, "arrayList");
        BigDecimal bigDecimal = BigDecimal.ONE;
        String D = e30.D(swap);
        Objects.requireNonNull(D, "null cannot be cast to non-null type java.lang.String");
        String lowerCase3 = D.toLowerCase(Locale.ROOT);
        fs1.e(lowerCase3, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
        for (String str2 : list) {
            Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase4 = str2.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase4, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            Iterator<T> it = i().iterator();
            while (true) {
                str = null;
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                String pairAddress = ((PairsData) obj).getPairAddress();
                if (pairAddress == null) {
                    lowerCase2 = null;
                } else {
                    lowerCase2 = pairAddress.toLowerCase(Locale.ROOT);
                    fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                }
                if (fs1.b(lowerCase2, lowerCase4)) {
                    break;
                }
            }
            PairsData pairsData = (PairsData) obj;
            if (pairsData == null || (token0 = pairsData.getToken0()) == null || (contractAddress = token0.getContractAddress()) == null) {
                lowerCase = null;
            } else {
                lowerCase = contractAddress.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            if (pairsData != null && (token1 = pairsData.getToken1()) != null && (contractAddress2 = token1.getContractAddress()) != null) {
                str = contractAddress2.toLowerCase(Locale.ROOT);
                fs1.e(str, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            }
            String str3 = "0";
            if (pairsData == null || (reserve0 = pairsData.getReserve0()) == null) {
                reserve0 = "0";
            }
            BigInteger bigInteger = new BigInteger(reserve0);
            if (pairsData != null && (reserve1 = pairsData.getReserve1()) != null) {
                str3 = reserve1;
            }
            BigInteger bigInteger2 = new BigInteger(str3);
            if (dv3.t(lowerCase3, lowerCase, true)) {
                if (str != null) {
                    lowerCase3 = str;
                }
                BigDecimal scale = new BigDecimal(bigInteger2).setScale(50);
                fs1.e(scale, "reserve1.toBigDecimal().setScale(50)");
                BigDecimal scale2 = new BigDecimal(bigInteger).setScale(50);
                fs1.e(scale2, "reserve0.toBigDecimal().setScale(50)");
                divide = scale.divide(scale2, RoundingMode.HALF_EVEN);
                fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
            } else {
                if (lowerCase != null) {
                    lowerCase3 = lowerCase;
                }
                BigDecimal scale3 = new BigDecimal(bigInteger).setScale(50);
                fs1.e(scale3, "reserve0.toBigDecimal().setScale(50)");
                BigDecimal scale4 = new BigDecimal(bigInteger2).setScale(50);
                fs1.e(scale4, "reserve1.toBigDecimal().setScale(50)");
                divide = scale3.divide(scale4, RoundingMode.HALF_EVEN);
                fs1.e(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
            }
            bigDecimal = bigDecimal.multiply(divide);
        }
        fs1.e(bigDecimal, "midPrice");
        return bigDecimal;
    }

    public final ArrayList<String> p(l84 l84Var) {
        fs1.f(l84Var, "a");
        ArrayList<String> arrayList = new ArrayList<>();
        for (PairsData pairsData : l84Var.d()) {
            String pairAddress = pairsData.getPairAddress();
            if (pairAddress != null) {
                arrayList.add(pairAddress);
            }
        }
        return arrayList;
    }

    public final BigDecimal q(l84 l84Var) {
        fs1.f(l84Var, "a");
        BigDecimal abs = e(l84Var).abs();
        fs1.e(abs, "priceImpact.abs()");
        BigDecimal subtract = abs.subtract(new BigDecimal(String.valueOf(1.0d - Math.pow(0.9975d, p(l84Var).size()))));
        fs1.e(subtract, "this.subtract(other)");
        BigDecimal multiply = subtract.multiply(new BigDecimal(String.valueOf(100.0d)));
        fs1.e(multiply, "this.multiply(other)");
        return multiply;
    }

    public final int r(l84 l84Var, l84 l84Var2) {
        if (!fs1.b(l84Var.b(), l84Var2.b())) {
            return l84Var.b().compareTo(l84Var2.b()) < 0 ? 1 : -1;
        } else if (fs1.b(l84Var.a(), l84Var2.a())) {
            return 0;
        } else {
            return l84Var.a().compareTo(l84Var2.a()) < 0 ? -1 : 1;
        }
    }

    public final ArrayList<l84> s(ArrayList<l84> arrayList, l84 l84Var, int i) {
        e30.c0(fs1.l("maxResults = ", Integer.valueOf(i)), "SortedInsert-Start");
        if (arrayList.size() == 0) {
            arrayList.add(l84Var);
            e30.c0("newTrade Added", "SortedInsert-bestTrades.size == 0");
            return arrayList;
        }
        boolean z = arrayList.size() == i;
        l84 l84Var2 = arrayList.get(arrayList.size() - 1);
        fs1.e(l84Var2, "bestTrades[bestTrades.size - 1]");
        l84 l84Var3 = l84Var2;
        if (!z || t(l84Var3, l84Var) > 0) {
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                int i3 = (i2 + size) >> 1;
                l84 l84Var4 = arrayList.get(i3);
                fs1.e(l84Var4, "bestTrades[mid]");
                if (t(l84Var4, l84Var) <= 0) {
                    i2 = i3 + 1;
                } else {
                    size = i3;
                }
            }
            e30.c0("Lo = " + i2 + " & Hi= " + size, "SortedInsert-After While");
            ArrayList arrayList2 = new ArrayList();
            Object[] a = rt1.a(arrayList.toArray(), i2, 0, l84Var);
            fs1.e(a, "splice(bestTrades.toArray(), lo, 0, newTrade)");
            for (Object obj : a) {
                Objects.requireNonNull(obj, "null cannot be cast to non-null type net.safemoon.androidwallet.viewmodels.swap.TradeStructure");
                arrayList2.add((l84) obj);
            }
            if (z) {
                g20.A(arrayList2);
            }
            arrayList.clear();
            arrayList.addAll(arrayList2);
            e30.c0(fs1.l("bestTrades = ", new Gson().toJson(arrayList)), "SortedInsert-AfterSplice");
            return arrayList;
        }
        return arrayList;
    }

    public final int t(l84 l84Var, l84 l84Var2) {
        int r = r(l84Var, l84Var2);
        if (r != 0) {
            return r;
        }
        BigDecimal e = e(l84Var);
        BigDecimal e2 = e(l84Var2);
        if (e.compareTo(e2) < 0) {
            return -1;
        }
        if (e.compareTo(e2) > 0) {
            return 1;
        }
        return l84Var.d().size() - l84Var2.d().size();
    }
}
