package defpackage;

import androidx.media3.common.util.b;
import androidx.media3.exoplayer.source.j;

/* compiled from: MediaPeriodInfo.java */
/* renamed from: k62  reason: default package */
/* loaded from: classes.dex */
public final class k62 {
    public final j.b a;
    public final long b;
    public final long c;
    public final long d;
    public final long e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;

    public k62(j.b bVar, long j, long j2, long j3, long j4, boolean z, boolean z2, boolean z3, boolean z4) {
        boolean z5 = false;
        ii.a(!z4 || z2);
        ii.a(!z3 || z2);
        if (!z || (!z2 && !z3 && !z4)) {
            z5 = true;
        }
        ii.a(z5);
        this.a = bVar;
        this.b = j;
        this.c = j2;
        this.d = j3;
        this.e = j4;
        this.f = z;
        this.g = z2;
        this.h = z3;
        this.i = z4;
    }

    public k62 a(long j) {
        return j == this.c ? this : new k62(this.a, this.b, j, this.d, this.e, this.f, this.g, this.h, this.i);
    }

    public k62 b(long j) {
        return j == this.b ? this : new k62(this.a, j, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || k62.class != obj.getClass()) {
            return false;
        }
        k62 k62Var = (k62) obj;
        return this.b == k62Var.b && this.c == k62Var.c && this.d == k62Var.d && this.e == k62Var.e && this.f == k62Var.f && this.g == k62Var.g && this.h == k62Var.h && this.i == k62Var.i && b.c(this.a, k62Var.a);
    }

    public int hashCode() {
        return ((((((((((((((((527 + this.a.hashCode()) * 31) + ((int) this.b)) * 31) + ((int) this.c)) * 31) + ((int) this.d)) * 31) + ((int) this.e)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0);
    }
}
