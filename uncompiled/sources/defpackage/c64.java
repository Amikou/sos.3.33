package defpackage;

/* compiled from: TimeModule_UptimeClockFactory.java */
/* renamed from: c64  reason: default package */
/* loaded from: classes.dex */
public final class c64 implements z11<qz> {

    /* compiled from: TimeModule_UptimeClockFactory.java */
    /* renamed from: c64$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final c64 a = new c64();
    }

    public static c64 a() {
        return a.a;
    }

    public static qz c() {
        return (qz) yt2.c(a64.b(), "Cannot return null from a non-@Nullable @Provides method");
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public qz get() {
        return c();
    }
}
