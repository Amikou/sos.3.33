package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktRegisterGuideBinding.java */
/* renamed from: u6  reason: default package */
/* loaded from: classes2.dex */
public final class u6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final sp3 c;
    public final TextView d;

    public u6(ConstraintLayout constraintLayout, MaterialButton materialButton, sp3 sp3Var, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = sp3Var;
        this.d = textView3;
    }

    public static u6 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            i = R.id.toolbar;
            View a = ai4.a(view, R.id.toolbar);
            if (a != null) {
                sp3 a2 = sp3.a(a);
                i = R.id.tvFirstContent;
                TextView textView = (TextView) ai4.a(view, R.id.tvFirstContent);
                if (textView != null) {
                    i = R.id.tvHeader;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvHeader);
                    if (textView2 != null) {
                        i = R.id.tvLogin;
                        TextView textView3 = (TextView) ai4.a(view, R.id.tvLogin);
                        if (textView3 != null) {
                            i = R.id.tvLoginNotice;
                            TextView textView4 = (TextView) ai4.a(view, R.id.tvLoginNotice);
                            if (textView4 != null) {
                                i = R.id.tvSecondContent;
                                TextView textView5 = (TextView) ai4.a(view, R.id.tvSecondContent);
                                if (textView5 != null) {
                                    i = R.id.tvThirdContent;
                                    TextView textView6 = (TextView) ai4.a(view, R.id.tvThirdContent);
                                    if (textView6 != null) {
                                        i = R.id.tvTitle;
                                        TextView textView7 = (TextView) ai4.a(view, R.id.tvTitle);
                                        if (textView7 != null) {
                                            return new u6((ConstraintLayout) view, materialButton, a2, textView, textView2, textView3, textView4, textView5, textView6, textView7);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static u6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static u6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_register_guide, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
