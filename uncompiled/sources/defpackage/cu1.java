package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: cu1  reason: default package */
/* loaded from: classes2.dex */
public final class cu1 {
    public static final k24 a = new k24("COMPLETING_ALREADY");
    public static final k24 b = new k24("COMPLETING_WAITING_CHILDREN");
    public static final k24 c = new k24("COMPLETING_RETRY");
    public static final k24 d = new k24("TOO_LATE_TO_CANCEL");
    public static final k24 e = new k24("SEALED");
    public static final ou0 f = new ou0(false);
    public static final ou0 g = new ou0(true);

    public static final Object g(Object obj) {
        return obj instanceof dq1 ? new eq1((dq1) obj) : obj;
    }

    public static final Object h(Object obj) {
        dq1 dq1Var;
        eq1 eq1Var = obj instanceof eq1 ? (eq1) obj : null;
        return (eq1Var == null || (dq1Var = eq1Var.a) == null) ? obj : dq1Var;
    }
}
