package defpackage;

import android.os.Handler;
import androidx.media3.common.u;
import androidx.media3.exoplayer.i;
import androidx.media3.exoplayer.n;
import androidx.media3.exoplayer.source.j;
import androidx.media3.exoplayer.trackselection.f;
import androidx.media3.exoplayer.trackselection.g;
import com.google.common.collect.ImmutableList;
import zendesk.support.request.CellBase;

/* compiled from: MediaPeriodQueue.java */
/* renamed from: m62  reason: default package */
/* loaded from: classes.dex */
public final class m62 {
    public final u.b a = new u.b();
    public final u.c b = new u.c();
    public final jb c;
    public final Handler d;
    public long e;
    public int f;
    public boolean g;
    public i h;
    public i i;
    public i j;
    public int k;
    public Object l;
    public long m;

    public m62(jb jbVar, Handler handler) {
        this.c = jbVar;
        this.d = handler;
    }

    public static j.b A(u uVar, Object obj, long j, long j2, u.c cVar, u.b bVar) {
        uVar.h(obj, bVar);
        uVar.n(bVar.g0, cVar);
        int b = uVar.b(obj);
        Object obj2 = obj;
        while (bVar.h0 == 0 && bVar.e() > 0 && bVar.s(bVar.q()) && bVar.g(0L) == -1) {
            int i = b + 1;
            if (b >= cVar.t0) {
                break;
            }
            uVar.g(i, bVar, true);
            obj2 = ii.e(bVar.f0);
            b = i;
        }
        uVar.h(obj2, bVar);
        int g = bVar.g(j);
        if (g == -1) {
            return new j.b(obj2, j2, bVar.f(j));
        }
        return new j.b(obj2, g, bVar.m(g), j2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void w(ImmutableList.a aVar, j.b bVar) {
        this.c.G(aVar.l(), bVar);
    }

    public j.b B(u uVar, Object obj, long j) {
        long C = C(uVar, obj);
        uVar.h(obj, this.a);
        uVar.n(this.a.g0, this.b);
        boolean z = false;
        for (int b = uVar.b(obj); b >= this.b.s0; b--) {
            uVar.g(b, this.a, true);
            boolean z2 = this.a.e() > 0;
            z |= z2;
            u.b bVar = this.a;
            if (bVar.g(bVar.h0) != -1) {
                obj = ii.e(this.a.f0);
            }
            if (z && (!z2 || this.a.h0 != 0)) {
                break;
            }
        }
        return A(uVar, obj, j, C, this.b, this.a);
    }

    public final long C(u uVar, Object obj) {
        int b;
        int i = uVar.h(obj, this.a).g0;
        Object obj2 = this.l;
        if (obj2 != null && (b = uVar.b(obj2)) != -1 && uVar.f(b, this.a).g0 == i) {
            return this.m;
        }
        for (i iVar = this.h; iVar != null; iVar = iVar.j()) {
            if (iVar.b.equals(obj)) {
                return iVar.f.a.d;
            }
        }
        for (i iVar2 = this.h; iVar2 != null; iVar2 = iVar2.j()) {
            int b2 = uVar.b(iVar2.b);
            if (b2 != -1 && uVar.f(b2, this.a).g0 == i) {
                return iVar2.f.a.d;
            }
        }
        long j = this.e;
        this.e = 1 + j;
        if (this.h == null) {
            this.l = obj;
            this.m = j;
        }
        return j;
    }

    public boolean D() {
        i iVar = this.j;
        return iVar == null || (!iVar.f.i && iVar.q() && this.j.f.e != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && this.k < 100);
    }

    public final boolean E(u uVar) {
        i iVar = this.h;
        if (iVar == null) {
            return true;
        }
        int b = uVar.b(iVar.b);
        while (true) {
            b = uVar.d(b, this.a, this.b, this.f, this.g);
            while (iVar.j() != null && !iVar.f.g) {
                iVar = iVar.j();
            }
            i j = iVar.j();
            if (b == -1 || j == null || uVar.b(j.b) != b) {
                break;
            }
            iVar = j;
        }
        boolean z = z(iVar);
        iVar.f = r(uVar, iVar.f);
        return !z;
    }

    public boolean F(u uVar, long j, long j2) {
        k62 k62Var;
        i iVar = this.h;
        i iVar2 = null;
        while (iVar != null) {
            k62 k62Var2 = iVar.f;
            if (iVar2 == null) {
                k62Var = r(uVar, k62Var2);
            } else {
                k62 i = i(uVar, iVar2, j);
                if (i == null) {
                    return !z(iVar2);
                }
                if (!e(k62Var2, i)) {
                    return !z(iVar2);
                }
                k62Var = i;
            }
            iVar.f = k62Var.a(k62Var2.c);
            if (!d(k62Var2.e, k62Var.e)) {
                iVar.A();
                long j3 = k62Var.e;
                return (z(iVar) || (iVar == this.i && !iVar.f.f && ((j2 > Long.MIN_VALUE ? 1 : (j2 == Long.MIN_VALUE ? 0 : -1)) == 0 || (j2 > ((j3 > CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 1 : (j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 0 : -1)) == 0 ? Long.MAX_VALUE : iVar.z(j3)) ? 1 : (j2 == ((j3 > CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 1 : (j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? 0 : -1)) == 0 ? Long.MAX_VALUE : iVar.z(j3)) ? 0 : -1)) >= 0))) ? false : true;
            }
            iVar2 = iVar;
            iVar = iVar.j();
        }
        return true;
    }

    public boolean G(u uVar, int i) {
        this.f = i;
        return E(uVar);
    }

    public boolean H(u uVar, boolean z) {
        this.g = z;
        return E(uVar);
    }

    public i b() {
        i iVar = this.h;
        if (iVar == null) {
            return null;
        }
        if (iVar == this.i) {
            this.i = iVar.j();
        }
        this.h.t();
        int i = this.k - 1;
        this.k = i;
        if (i == 0) {
            this.j = null;
            i iVar2 = this.h;
            this.l = iVar2.b;
            this.m = iVar2.f.a.d;
        }
        this.h = this.h.j();
        x();
        return this.h;
    }

    public i c() {
        i iVar = this.i;
        ii.g((iVar == null || iVar.j() == null) ? false : true);
        this.i = this.i.j();
        x();
        return this.i;
    }

    public final boolean d(long j, long j2) {
        return j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j == j2;
    }

    public final boolean e(k62 k62Var, k62 k62Var2) {
        return k62Var.b == k62Var2.b && k62Var.a.equals(k62Var2.a);
    }

    public void f() {
        if (this.k == 0) {
            return;
        }
        i iVar = (i) ii.i(this.h);
        this.l = iVar.b;
        this.m = iVar.f.a.d;
        while (iVar != null) {
            iVar.t();
            iVar = iVar.j();
        }
        this.h = null;
        this.j = null;
        this.i = null;
        this.k = 0;
        x();
    }

    public i g(n[] nVarArr, f fVar, gb gbVar, androidx.media3.exoplayer.j jVar, k62 k62Var, g gVar) {
        i iVar = this.j;
        i iVar2 = new i(nVarArr, iVar == null ? 1000000000000L : (iVar.l() + this.j.f.e) - k62Var.b, fVar, gbVar, jVar, k62Var, gVar);
        i iVar3 = this.j;
        if (iVar3 != null) {
            iVar3.w(iVar2);
        } else {
            this.h = iVar2;
            this.i = iVar2;
        }
        this.l = null;
        this.j = iVar2;
        this.k++;
        x();
        return iVar2;
    }

    public final k62 h(lr2 lr2Var) {
        return k(lr2Var.a, lr2Var.b, lr2Var.c, lr2Var.r);
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x00cc, code lost:
        if (r0.s(r0.q()) != false) goto L26;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.k62 i(androidx.media3.common.u r20, androidx.media3.exoplayer.i r21, long r22) {
        /*
            Method dump skipped, instructions count: 450
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.m62.i(androidx.media3.common.u, androidx.media3.exoplayer.i, long):k62");
    }

    public i j() {
        return this.j;
    }

    public final k62 k(u uVar, j.b bVar, long j, long j2) {
        uVar.h(bVar.a, this.a);
        if (bVar.b()) {
            return l(uVar, bVar.a, bVar.b, bVar.c, j, bVar.d);
        }
        return m(uVar, bVar.a, j2, j, bVar.d);
    }

    public final k62 l(u uVar, Object obj, int i, int i2, long j, long j2) {
        j.b bVar = new j.b(obj, i, i2, j2);
        long d = uVar.h(bVar.a, this.a).d(bVar.b, bVar.c);
        long i3 = i2 == this.a.m(i) ? this.a.i() : 0L;
        return new k62(bVar, (d == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || i3 < d) ? i3 : Math.max(0L, d - 1), j, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, d, this.a.s(bVar.b), false, false, false);
    }

    public final k62 m(u uVar, Object obj, long j, long j2, long j3) {
        boolean z;
        long j4;
        long j5;
        long j6;
        long j7 = j;
        uVar.h(obj, this.a);
        int f = this.a.f(j7);
        int i = 1;
        if (f == -1) {
            if (this.a.e() > 0) {
                u.b bVar = this.a;
                if (bVar.s(bVar.q())) {
                    z = true;
                }
            }
            z = false;
        } else {
            if (this.a.s(f)) {
                long h = this.a.h(f);
                u.b bVar2 = this.a;
                if (h == bVar2.h0 && bVar2.r(f)) {
                    z = true;
                    f = -1;
                }
            }
            z = false;
        }
        j.b bVar3 = new j.b(obj, j3, f);
        boolean s = s(bVar3);
        boolean u = u(uVar, bVar3);
        boolean t = t(uVar, bVar3, s);
        boolean z2 = f != -1 && this.a.s(f);
        if (f != -1) {
            j5 = this.a.h(f);
        } else if (z) {
            j5 = this.a.h0;
        } else {
            j4 = -9223372036854775807L;
            j6 = (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || j4 == Long.MIN_VALUE) ? this.a.h0 : j4;
            if (j6 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j7 >= j6) {
                if (!t && z) {
                    i = 0;
                }
                j7 = Math.max(0L, j6 - i);
            }
            return new k62(bVar3, j7, j2, j4, j6, z2, s, u, t);
        }
        j4 = j5;
        if (j4 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
        }
        if (j6 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            if (!t) {
                i = 0;
            }
            j7 = Math.max(0L, j6 - i);
        }
        return new k62(bVar3, j7, j2, j4, j6, z2, s, u, t);
    }

    public final long n(u uVar, Object obj, int i) {
        uVar.h(obj, this.a);
        long h = this.a.h(i);
        if (h == Long.MIN_VALUE) {
            return this.a.h0;
        }
        return h + this.a.k(i);
    }

    public k62 o(long j, lr2 lr2Var) {
        i iVar = this.j;
        if (iVar == null) {
            return h(lr2Var);
        }
        return i(lr2Var.a, iVar, j);
    }

    public i p() {
        return this.h;
    }

    public i q() {
        return this.i;
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x0062  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x006c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public defpackage.k62 r(androidx.media3.common.u r19, defpackage.k62 r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            androidx.media3.exoplayer.source.j$b r3 = r2.a
            boolean r12 = r0.s(r3)
            boolean r13 = r0.u(r1, r3)
            boolean r14 = r0.t(r1, r3, r12)
            androidx.media3.exoplayer.source.j$b r4 = r2.a
            java.lang.Object r4 = r4.a
            androidx.media3.common.u$b r5 = r0.a
            r1.h(r4, r5)
            boolean r1 = r3.b()
            r4 = -1
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r1 != 0) goto L35
            int r1 = r3.e
            if (r1 != r4) goto L2e
            goto L35
        L2e:
            androidx.media3.common.u$b r7 = r0.a
            long r7 = r7.h(r1)
            goto L36
        L35:
            r7 = r5
        L36:
            boolean r1 = r3.b()
            if (r1 == 0) goto L48
            androidx.media3.common.u$b r1 = r0.a
            int r5 = r3.b
            int r6 = r3.c
            long r5 = r1.d(r5, r6)
        L46:
            r9 = r5
            goto L5c
        L48:
            int r1 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r1 == 0) goto L55
            r5 = -9223372036854775808
            int r1 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r1 != 0) goto L53
            goto L55
        L53:
            r9 = r7
            goto L5c
        L55:
            androidx.media3.common.u$b r1 = r0.a
            long r5 = r1.l()
            goto L46
        L5c:
            boolean r1 = r3.b()
            if (r1 == 0) goto L6c
            androidx.media3.common.u$b r1 = r0.a
            int r4 = r3.b
            boolean r1 = r1.s(r4)
        L6a:
            r11 = r1
            goto L7c
        L6c:
            int r1 = r3.e
            if (r1 == r4) goto L7a
            androidx.media3.common.u$b r4 = r0.a
            boolean r1 = r4.s(r1)
            if (r1 == 0) goto L7a
            r1 = 1
            goto L6a
        L7a:
            r1 = 0
            goto L6a
        L7c:
            k62 r15 = new k62
            long r4 = r2.b
            long r1 = r2.c
            r16 = r1
            r1 = r15
            r2 = r3
            r3 = r4
            r5 = r16
            r1.<init>(r2, r3, r5, r7, r9, r11, r12, r13, r14)
            return r15
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.m62.r(androidx.media3.common.u, k62):k62");
    }

    public final boolean s(j.b bVar) {
        return !bVar.b() && bVar.e == -1;
    }

    public final boolean t(u uVar, j.b bVar, boolean z) {
        int b = uVar.b(bVar.a);
        return !uVar.n(uVar.f(b, this.a).g0, this.b).m0 && uVar.r(b, this.a, this.b, this.f, this.g) && z;
    }

    public final boolean u(u uVar, j.b bVar) {
        if (s(bVar)) {
            return uVar.n(uVar.h(bVar.a, this.a).g0, this.b).t0 == uVar.b(bVar.a);
        }
        return false;
    }

    public boolean v(androidx.media3.exoplayer.source.i iVar) {
        i iVar2 = this.j;
        return iVar2 != null && iVar2.a == iVar;
    }

    public final void x() {
        final ImmutableList.a builder = ImmutableList.builder();
        for (i iVar = this.h; iVar != null; iVar = iVar.j()) {
            builder.a(iVar.f.a);
        }
        i iVar2 = this.i;
        final j.b bVar = iVar2 == null ? null : iVar2.f.a;
        this.d.post(new Runnable() { // from class: l62
            @Override // java.lang.Runnable
            public final void run() {
                m62.this.w(builder, bVar);
            }
        });
    }

    public void y(long j) {
        i iVar = this.j;
        if (iVar != null) {
            iVar.s(j);
        }
    }

    public boolean z(i iVar) {
        boolean z = false;
        ii.g(iVar != null);
        if (iVar.equals(this.j)) {
            return false;
        }
        this.j = iVar;
        while (iVar.j() != null) {
            iVar = iVar.j();
            if (iVar == this.i) {
                this.i = this.h;
                z = true;
            }
            iVar.t();
            this.k--;
        }
        this.j.w(null);
        x();
        return z;
    }
}
