package defpackage;

import java.util.Objects;

/* compiled from: Encoding.java */
/* renamed from: hv0  reason: default package */
/* loaded from: classes.dex */
public final class hv0 {
    public final String a;

    public hv0(String str) {
        Objects.requireNonNull(str, "name is null");
        this.a = str;
    }

    public static hv0 b(String str) {
        return new hv0(str);
    }

    public String a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof hv0) {
            return this.a.equals(((hv0) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    public String toString() {
        return "Encoding{name=\"" + this.a + "\"}";
    }
}
