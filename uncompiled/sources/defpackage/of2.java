package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.collectible.RoomNFT;

/* compiled from: NftDao.kt */
/* renamed from: of2  reason: default package */
/* loaded from: classes2.dex */
public interface of2 {
    Object a(long j, int i, q70<? super te4> q70Var);

    Object b(String str, long j, q70<? super RoomNFT> q70Var);

    Object c(String str, long j, q70<? super Boolean> q70Var);

    Object d(long j, q70<? super RoomNFT> q70Var);

    Object e(long j, q70<? super List<RoomNFT>> q70Var);

    Object f(long j, q70<? super te4> q70Var);

    Object g(RoomNFT roomNFT, q70<? super Long> q70Var);

    Object h(int i, q70<? super List<RoomNFT>> q70Var);

    LiveData<List<RoomNFT>> i(long j);

    Object j(RoomNFT roomNFT, q70<? super te4> q70Var);
}
