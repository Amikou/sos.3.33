package defpackage;

import android.graphics.Bitmap;
import androidx.media3.common.util.b;
import androidx.media3.extractor.text.SubtitleDecoderException;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.zip.Inflater;

/* compiled from: PgsDecoder.java */
/* renamed from: qq2  reason: default package */
/* loaded from: classes.dex */
public final class qq2 extends androidx.media3.extractor.text.a {
    public final op2 n;
    public final op2 o;
    public final a p;
    public Inflater q;

    /* compiled from: PgsDecoder.java */
    /* renamed from: qq2$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final op2 a = new op2();
        public final int[] b = new int[256];
        public boolean c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;

        public kb0 d() {
            int i;
            if (this.d == 0 || this.e == 0 || this.h == 0 || this.i == 0 || this.a.f() == 0 || this.a.e() != this.a.f() || !this.c) {
                return null;
            }
            this.a.P(0);
            int i2 = this.h * this.i;
            int[] iArr = new int[i2];
            int i3 = 0;
            while (i3 < i2) {
                int D = this.a.D();
                if (D != 0) {
                    i = i3 + 1;
                    iArr[i3] = this.b[D];
                } else {
                    int D2 = this.a.D();
                    if (D2 != 0) {
                        i = ((D2 & 64) == 0 ? D2 & 63 : ((D2 & 63) << 8) | this.a.D()) + i3;
                        Arrays.fill(iArr, i3, i, (D2 & 128) == 0 ? 0 : this.b[this.a.D()]);
                    }
                }
                i3 = i;
            }
            return new kb0.b().f(Bitmap.createBitmap(iArr, this.h, this.i, Bitmap.Config.ARGB_8888)).k(this.f / this.d).l(0).h(this.g / this.e, 0).i(0).n(this.h / this.d).g(this.i / this.e).a();
        }

        public final void e(op2 op2Var, int i) {
            int G;
            if (i < 4) {
                return;
            }
            op2Var.Q(3);
            int i2 = i - 4;
            if ((op2Var.D() & 128) != 0) {
                if (i2 < 7 || (G = op2Var.G()) < 4) {
                    return;
                }
                this.h = op2Var.J();
                this.i = op2Var.J();
                this.a.L(G - 4);
                i2 -= 7;
            }
            int e = this.a.e();
            int f = this.a.f();
            if (e >= f || i2 <= 0) {
                return;
            }
            int min = Math.min(i2, f - e);
            op2Var.j(this.a.d(), e, min);
            this.a.P(e + min);
        }

        public final void f(op2 op2Var, int i) {
            if (i < 19) {
                return;
            }
            this.d = op2Var.J();
            this.e = op2Var.J();
            op2Var.Q(11);
            this.f = op2Var.J();
            this.g = op2Var.J();
        }

        public final void g(op2 op2Var, int i) {
            if (i % 5 != 2) {
                return;
            }
            op2Var.Q(2);
            Arrays.fill(this.b, 0);
            int i2 = i / 5;
            int i3 = 0;
            while (i3 < i2) {
                int D = op2Var.D();
                int D2 = op2Var.D();
                int D3 = op2Var.D();
                int D4 = op2Var.D();
                int D5 = op2Var.D();
                double d = D2;
                double d2 = D3 - 128;
                int i4 = i3;
                double d3 = D4 - 128;
                this.b[D] = b.q((int) (d + (d3 * 1.772d)), 0, 255) | (b.q((int) ((d - (0.34414d * d3)) - (d2 * 0.71414d)), 0, 255) << 8) | (D5 << 24) | (b.q((int) ((1.402d * d2) + d), 0, 255) << 16);
                i3 = i4 + 1;
            }
            this.c = true;
        }

        public void h() {
            this.d = 0;
            this.e = 0;
            this.f = 0;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.a.L(0);
            this.c = false;
        }
    }

    public qq2() {
        super("PgsDecoder");
        this.n = new op2();
        this.o = new op2();
        this.p = new a();
    }

    public static kb0 D(op2 op2Var, a aVar) {
        int f = op2Var.f();
        int D = op2Var.D();
        int J = op2Var.J();
        int e = op2Var.e() + J;
        kb0 kb0Var = null;
        if (e > f) {
            op2Var.P(f);
            return null;
        }
        if (D != 128) {
            switch (D) {
                case 20:
                    aVar.g(op2Var, J);
                    break;
                case 21:
                    aVar.e(op2Var, J);
                    break;
                case 22:
                    aVar.f(op2Var, J);
                    break;
            }
        } else {
            kb0Var = aVar.d();
            aVar.h();
        }
        op2Var.P(e);
        return kb0Var;
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.n.N(bArr, i);
        C(this.n);
        this.p.h();
        ArrayList arrayList = new ArrayList();
        while (this.n.a() >= 3) {
            kb0 D = D(this.n, this.p);
            if (D != null) {
                arrayList.add(D);
            }
        }
        return new rq2(Collections.unmodifiableList(arrayList));
    }

    public final void C(op2 op2Var) {
        if (op2Var.a() <= 0 || op2Var.h() != 120) {
            return;
        }
        if (this.q == null) {
            this.q = new Inflater();
        }
        if (b.n0(op2Var, this.o, this.q)) {
            op2Var.N(this.o.d(), this.o.f());
        }
    }
}
