package defpackage;

import android.os.Bundle;
import defpackage.kb;
import defpackage.rl0;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: AnalyticsDeferredProxy.java */
/* renamed from: pb  reason: default package */
/* loaded from: classes2.dex */
public class pb {
    public final rl0<kb> a;
    public volatile qb b;
    public volatile lr c;
    public final List<kr> d;

    public pb(rl0<kb> rl0Var) {
        this(rl0Var, new xo0(), new pe4());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void g(String str, Bundle bundle) {
        this.b.a(str, bundle);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void h(kr krVar) {
        synchronized (this) {
            if (this.c instanceof xo0) {
                this.d.add(krVar);
            }
            this.c.a(krVar);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void i(fw2 fw2Var) {
        kb kbVar = (kb) fw2Var.get();
        p90 p90Var = new p90(kbVar);
        k90 k90Var = new k90();
        if (j(kbVar, k90Var) != null) {
            w12.f().b("Registered Firebase Analytics listener.");
            jr jrVar = new jr();
            xq xqVar = new xq(p90Var, 500, TimeUnit.MILLISECONDS);
            synchronized (this) {
                for (kr krVar : this.d) {
                    jrVar.a(krVar);
                }
                k90Var.d(jrVar);
                k90Var.e(xqVar);
                this.c = jrVar;
                this.b = xqVar;
            }
            return;
        }
        w12.f().k("Could not register Firebase Analytics listener; a listener is already registered.");
    }

    public static kb.a j(kb kbVar, k90 k90Var) {
        kb.a a = kbVar.a("clx", k90Var);
        if (a == null) {
            w12.f().b("Could not register AnalyticsConnectorListener with Crashlytics origin.");
            a = kbVar.a("crash", k90Var);
            if (a != null) {
                w12.f().k("A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version.");
            }
        }
        return a;
    }

    public qb d() {
        return new qb() { // from class: mb
            @Override // defpackage.qb
            public final void a(String str, Bundle bundle) {
                pb.this.g(str, bundle);
            }
        };
    }

    public lr e() {
        return new lr() { // from class: nb
            @Override // defpackage.lr
            public final void a(kr krVar) {
                pb.this.h(krVar);
            }
        };
    }

    public final void f() {
        this.a.a(new rl0.a() { // from class: ob
            @Override // defpackage.rl0.a
            public final void a(fw2 fw2Var) {
                pb.this.i(fw2Var);
            }
        });
    }

    public pb(rl0<kb> rl0Var, lr lrVar, qb qbVar) {
        this.a = rl0Var;
        this.c = lrVar;
        this.d = new ArrayList();
        this.b = qbVar;
        f();
    }
}
