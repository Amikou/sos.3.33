package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: cx5  reason: default package */
/* loaded from: classes.dex */
public final class cx5 implements tx5 {
    public final tx5[] a;

    public cx5(tx5... tx5VarArr) {
        this.a = tx5VarArr;
    }

    @Override // defpackage.tx5
    public final rx5 a(Class<?> cls) {
        tx5[] tx5VarArr = this.a;
        for (int i = 0; i < 2; i++) {
            tx5 tx5Var = tx5VarArr[i];
            if (tx5Var.b(cls)) {
                return tx5Var.a(cls);
            }
        }
        String name = cls.getName();
        throw new UnsupportedOperationException(name.length() != 0 ? "No factory is available for message type: ".concat(name) : new String("No factory is available for message type: "));
    }

    @Override // defpackage.tx5
    public final boolean b(Class<?> cls) {
        tx5[] tx5VarArr = this.a;
        for (int i = 0; i < 2; i++) {
            if (tx5VarArr[i].b(cls)) {
                return true;
            }
        }
        return false;
    }
}
