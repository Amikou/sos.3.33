package defpackage;

import org.bouncycastle.crypto.a;

/* renamed from: lo2  reason: default package */
/* loaded from: classes2.dex */
public class lo2 extends io2 {
    public a d;
    public byte[] e;

    public lo2(qo0 qo0Var) {
        hj1 hj1Var = new hj1(qo0Var);
        this.d = hj1Var;
        this.e = new byte[hj1Var.c()];
    }

    @Override // defpackage.io2
    public ty a(int i) {
        return e(i);
    }

    public final void c(byte[] bArr, int i, byte[] bArr2, byte[] bArr3, int i2) {
        if (i == 0) {
            throw new IllegalArgumentException("iteration count must be at least 1.");
        }
        if (bArr != null) {
            this.d.b(bArr, 0, bArr.length);
        }
        this.d.b(bArr2, 0, bArr2.length);
        this.d.a(this.e, 0);
        byte[] bArr4 = this.e;
        System.arraycopy(bArr4, 0, bArr3, i2, bArr4.length);
        for (int i3 = 1; i3 < i; i3++) {
            a aVar = this.d;
            byte[] bArr5 = this.e;
            aVar.b(bArr5, 0, bArr5.length);
            this.d.a(this.e, 0);
            int i4 = 0;
            while (true) {
                byte[] bArr6 = this.e;
                if (i4 != bArr6.length) {
                    int i5 = i2 + i4;
                    bArr3[i5] = (byte) (bArr6[i4] ^ bArr3[i5]);
                    i4++;
                }
            }
        }
    }

    public final byte[] d(int i) {
        int i2;
        int c = this.d.c();
        int i3 = ((i + c) - 1) / c;
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[i3 * c];
        this.d.d(new kx1(this.a));
        int i4 = 0;
        for (int i5 = 1; i5 <= i3; i5++) {
            while (true) {
                byte b = (byte) (bArr[i2] + 1);
                bArr[i2] = b;
                i2 = b == 0 ? i2 - 1 : 3;
            }
            c(this.b, this.c, bArr, bArr2, i4);
            i4 += c;
        }
        return bArr2;
    }

    public ty e(int i) {
        int i2 = i / 8;
        return new kx1(wh.l(d(i2), 0, i2), 0, i2);
    }
}
