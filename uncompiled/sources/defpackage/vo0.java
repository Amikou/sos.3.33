package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.a;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.f;
import com.github.mikephil.charting.utils.Utils;
import defpackage.jo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: Direct.java */
/* renamed from: vo0  reason: default package */
/* loaded from: classes.dex */
public class vo0 {
    public static jo.a a = new jo.a();
    public static int b = 0;
    public static int c = 0;

    public static boolean a(int i, ConstraintWidget constraintWidget) {
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2;
        ConstraintWidget.DimensionBehaviour C = constraintWidget.C();
        ConstraintWidget.DimensionBehaviour S = constraintWidget.S();
        d dVar = constraintWidget.M() != null ? (d) constraintWidget.M() : null;
        if (dVar != null) {
            dVar.C();
            ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (dVar != null) {
            dVar.S();
            ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        ConstraintWidget.DimensionBehaviour dimensionBehaviour5 = ConstraintWidget.DimensionBehaviour.FIXED;
        boolean z = C == dimensionBehaviour5 || constraintWidget.m0() || C == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || (C == (dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) && constraintWidget.s == 0 && constraintWidget.b0 == Utils.FLOAT_EPSILON && constraintWidget.Z(0)) || (C == dimensionBehaviour2 && constraintWidget.s == 1 && constraintWidget.c0(0, constraintWidget.V()));
        boolean z2 = S == dimensionBehaviour5 || constraintWidget.n0() || S == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || (S == (dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) && constraintWidget.t == 0 && constraintWidget.b0 == Utils.FLOAT_EPSILON && constraintWidget.Z(1)) || (C == dimensionBehaviour && constraintWidget.t == 1 && constraintWidget.c0(1, constraintWidget.z()));
        if (constraintWidget.b0 <= Utils.FLOAT_EPSILON || !(z || z2)) {
            return z && z2;
        }
        return true;
    }

    public static void b(int i, ConstraintWidget constraintWidget, jo.b bVar, boolean z) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        ConstraintAnchor constraintAnchor4;
        ConstraintAnchor constraintAnchor5;
        if (constraintWidget.f0()) {
            return;
        }
        b++;
        if (!(constraintWidget instanceof d) && constraintWidget.l0()) {
            int i2 = i + 1;
            if (a(i2, constraintWidget)) {
                d.P1(i2, constraintWidget, bVar, new jo.a(), jo.a.k);
            }
        }
        ConstraintAnchor q = constraintWidget.q(ConstraintAnchor.Type.LEFT);
        ConstraintAnchor q2 = constraintWidget.q(ConstraintAnchor.Type.RIGHT);
        int e = q.e();
        int e2 = q2.e();
        if (q.d() != null && q.n()) {
            Iterator<ConstraintAnchor> it = q.d().iterator();
            while (it.hasNext()) {
                ConstraintAnchor next = it.next();
                ConstraintWidget constraintWidget2 = next.d;
                int i3 = i + 1;
                boolean a2 = a(i3, constraintWidget2);
                if (constraintWidget2.l0() && a2) {
                    d.P1(i3, constraintWidget2, bVar, new jo.a(), jo.a.k);
                }
                ConstraintWidget.DimensionBehaviour C = constraintWidget2.C();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (C == dimensionBehaviour && !a2) {
                    if (constraintWidget2.C() == dimensionBehaviour && constraintWidget2.w >= 0 && constraintWidget2.v >= 0 && (constraintWidget2.U() == 8 || (constraintWidget2.s == 0 && constraintWidget2.x() == Utils.FLOAT_EPSILON))) {
                        if (!constraintWidget2.h0() && !constraintWidget2.k0()) {
                            if (((next == constraintWidget2.M && (constraintAnchor5 = constraintWidget2.O.f) != null && constraintAnchor5.n()) || (next == constraintWidget2.O && (constraintAnchor4 = constraintWidget2.M.f) != null && constraintAnchor4.n())) && !constraintWidget2.h0()) {
                                e(i3, constraintWidget, bVar, constraintWidget2, z);
                            }
                        }
                    }
                } else if (!constraintWidget2.l0()) {
                    ConstraintAnchor constraintAnchor6 = constraintWidget2.M;
                    if (next == constraintAnchor6 && constraintWidget2.O.f == null) {
                        int f = constraintAnchor6.f() + e;
                        constraintWidget2.C0(f, constraintWidget2.V() + f);
                        b(i3, constraintWidget2, bVar, z);
                    } else {
                        ConstraintAnchor constraintAnchor7 = constraintWidget2.O;
                        if (next == constraintAnchor7 && constraintAnchor6.f == null) {
                            int f2 = e - constraintAnchor7.f();
                            constraintWidget2.C0(f2 - constraintWidget2.V(), f2);
                            b(i3, constraintWidget2, bVar, z);
                        } else if (next == constraintAnchor6 && (constraintAnchor3 = constraintAnchor7.f) != null && constraintAnchor3.n() && !constraintWidget2.h0()) {
                            d(i3, bVar, constraintWidget2, z);
                        }
                    }
                }
            }
        }
        if (constraintWidget instanceof f) {
            return;
        }
        if (q2.d() != null && q2.n()) {
            Iterator<ConstraintAnchor> it2 = q2.d().iterator();
            while (it2.hasNext()) {
                ConstraintAnchor next2 = it2.next();
                ConstraintWidget constraintWidget3 = next2.d;
                int i4 = i + 1;
                boolean a3 = a(i4, constraintWidget3);
                if (constraintWidget3.l0() && a3) {
                    d.P1(i4, constraintWidget3, bVar, new jo.a(), jo.a.k);
                }
                boolean z2 = (next2 == constraintWidget3.M && (constraintAnchor2 = constraintWidget3.O.f) != null && constraintAnchor2.n()) || (next2 == constraintWidget3.O && (constraintAnchor = constraintWidget3.M.f) != null && constraintAnchor.n());
                ConstraintWidget.DimensionBehaviour C2 = constraintWidget3.C();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (C2 == dimensionBehaviour2 && !a3) {
                    if (constraintWidget3.C() == dimensionBehaviour2 && constraintWidget3.w >= 0 && constraintWidget3.v >= 0 && (constraintWidget3.U() == 8 || (constraintWidget3.s == 0 && constraintWidget3.x() == Utils.FLOAT_EPSILON))) {
                        if (!constraintWidget3.h0() && !constraintWidget3.k0() && z2 && !constraintWidget3.h0()) {
                            e(i4, constraintWidget, bVar, constraintWidget3, z);
                        }
                    }
                } else if (!constraintWidget3.l0()) {
                    ConstraintAnchor constraintAnchor8 = constraintWidget3.M;
                    if (next2 == constraintAnchor8 && constraintWidget3.O.f == null) {
                        int f3 = constraintAnchor8.f() + e2;
                        constraintWidget3.C0(f3, constraintWidget3.V() + f3);
                        b(i4, constraintWidget3, bVar, z);
                    } else {
                        ConstraintAnchor constraintAnchor9 = constraintWidget3.O;
                        if (next2 == constraintAnchor9 && constraintAnchor8.f == null) {
                            int f4 = e2 - constraintAnchor9.f();
                            constraintWidget3.C0(f4 - constraintWidget3.V(), f4);
                            b(i4, constraintWidget3, bVar, z);
                        } else if (z2 && !constraintWidget3.h0()) {
                            d(i4, bVar, constraintWidget3, z);
                        }
                    }
                }
            }
        }
        constraintWidget.p0();
    }

    public static void c(int i, a aVar, jo.b bVar, int i2, boolean z) {
        if (aVar.q1()) {
            if (i2 == 0) {
                b(i + 1, aVar, bVar, z);
            } else {
                i(i + 1, aVar, bVar);
            }
        }
    }

    public static void d(int i, jo.b bVar, ConstraintWidget constraintWidget, boolean z) {
        float A = constraintWidget.A();
        int e = constraintWidget.M.f.e();
        int e2 = constraintWidget.O.f.e();
        int f = constraintWidget.M.f() + e;
        int f2 = e2 - constraintWidget.O.f();
        if (e == e2) {
            A = 0.5f;
        } else {
            e = f;
            e2 = f2;
        }
        int V = constraintWidget.V();
        int i2 = (e2 - e) - V;
        if (e > e2) {
            i2 = (e - e2) - V;
        }
        int i3 = ((int) (i2 > 0 ? (A * i2) + 0.5f : A * i2)) + e;
        int i4 = i3 + V;
        if (e > e2) {
            i4 = i3 - V;
        }
        constraintWidget.C0(i3, i4);
        b(i + 1, constraintWidget, bVar, z);
    }

    public static void e(int i, ConstraintWidget constraintWidget, jo.b bVar, ConstraintWidget constraintWidget2, boolean z) {
        int V;
        float A = constraintWidget2.A();
        int e = constraintWidget2.M.f.e() + constraintWidget2.M.f();
        int e2 = constraintWidget2.O.f.e() - constraintWidget2.O.f();
        if (e2 >= e) {
            int V2 = constraintWidget2.V();
            if (constraintWidget2.U() != 8) {
                int i2 = constraintWidget2.s;
                if (i2 == 2) {
                    if (constraintWidget instanceof d) {
                        V = constraintWidget.V();
                    } else {
                        V = constraintWidget.M().V();
                    }
                    V2 = (int) (constraintWidget2.A() * 0.5f * V);
                } else if (i2 == 0) {
                    V2 = e2 - e;
                }
                V2 = Math.max(constraintWidget2.v, V2);
                int i3 = constraintWidget2.w;
                if (i3 > 0) {
                    V2 = Math.min(i3, V2);
                }
            }
            int i4 = e + ((int) ((A * ((e2 - e) - V2)) + 0.5f));
            constraintWidget2.C0(i4, V2 + i4);
            b(i + 1, constraintWidget2, bVar, z);
        }
    }

    public static void f(int i, jo.b bVar, ConstraintWidget constraintWidget) {
        float Q = constraintWidget.Q();
        int e = constraintWidget.N.f.e();
        int e2 = constraintWidget.P.f.e();
        int f = constraintWidget.N.f() + e;
        int f2 = e2 - constraintWidget.P.f();
        if (e == e2) {
            Q = 0.5f;
        } else {
            e = f;
            e2 = f2;
        }
        int z = constraintWidget.z();
        int i2 = (e2 - e) - z;
        if (e > e2) {
            i2 = (e - e2) - z;
        }
        int i3 = (int) (i2 > 0 ? (Q * i2) + 0.5f : Q * i2);
        int i4 = e + i3;
        int i5 = i4 + z;
        if (e > e2) {
            i4 = e - i3;
            i5 = i4 - z;
        }
        constraintWidget.F0(i4, i5);
        i(i + 1, constraintWidget, bVar);
    }

    public static void g(int i, ConstraintWidget constraintWidget, jo.b bVar, ConstraintWidget constraintWidget2) {
        int z;
        float Q = constraintWidget2.Q();
        int e = constraintWidget2.N.f.e() + constraintWidget2.N.f();
        int e2 = constraintWidget2.P.f.e() - constraintWidget2.P.f();
        if (e2 >= e) {
            int z2 = constraintWidget2.z();
            if (constraintWidget2.U() != 8) {
                int i2 = constraintWidget2.t;
                if (i2 == 2) {
                    if (constraintWidget instanceof d) {
                        z = constraintWidget.z();
                    } else {
                        z = constraintWidget.M().z();
                    }
                    z2 = (int) (Q * 0.5f * z);
                } else if (i2 == 0) {
                    z2 = e2 - e;
                }
                z2 = Math.max(constraintWidget2.y, z2);
                int i3 = constraintWidget2.z;
                if (i3 > 0) {
                    z2 = Math.min(i3, z2);
                }
            }
            int i4 = e + ((int) ((Q * ((e2 - e) - z2)) + 0.5f));
            constraintWidget2.F0(i4, z2 + i4);
            i(i + 1, constraintWidget2, bVar);
        }
    }

    public static void h(d dVar, jo.b bVar) {
        ConstraintWidget.DimensionBehaviour C = dVar.C();
        ConstraintWidget.DimensionBehaviour S = dVar.S();
        b = 0;
        c = 0;
        dVar.v0();
        ArrayList<ConstraintWidget> o1 = dVar.o1();
        int size = o1.size();
        for (int i = 0; i < size; i++) {
            o1.get(i).v0();
        }
        boolean M1 = dVar.M1();
        if (C == ConstraintWidget.DimensionBehaviour.FIXED) {
            dVar.C0(0, dVar.V());
        } else {
            dVar.D0(0);
        }
        boolean z = false;
        boolean z2 = false;
        for (int i2 = 0; i2 < size; i2++) {
            ConstraintWidget constraintWidget = o1.get(i2);
            if (constraintWidget instanceof f) {
                f fVar = (f) constraintWidget;
                if (fVar.p1() == 1) {
                    if (fVar.q1() != -1) {
                        fVar.t1(fVar.q1());
                    } else if (fVar.r1() != -1 && dVar.m0()) {
                        fVar.t1(dVar.V() - fVar.r1());
                    } else if (dVar.m0()) {
                        fVar.t1((int) ((fVar.s1() * dVar.V()) + 0.5f));
                    }
                    z = true;
                }
            } else if ((constraintWidget instanceof a) && ((a) constraintWidget).u1() == 0) {
                z2 = true;
            }
        }
        if (z) {
            for (int i3 = 0; i3 < size; i3++) {
                ConstraintWidget constraintWidget2 = o1.get(i3);
                if (constraintWidget2 instanceof f) {
                    f fVar2 = (f) constraintWidget2;
                    if (fVar2.p1() == 1) {
                        b(0, fVar2, bVar, M1);
                    }
                }
            }
        }
        b(0, dVar, bVar, M1);
        if (z2) {
            for (int i4 = 0; i4 < size; i4++) {
                ConstraintWidget constraintWidget3 = o1.get(i4);
                if (constraintWidget3 instanceof a) {
                    a aVar = (a) constraintWidget3;
                    if (aVar.u1() == 0) {
                        c(0, aVar, bVar, 0, M1);
                    }
                }
            }
        }
        if (S == ConstraintWidget.DimensionBehaviour.FIXED) {
            dVar.F0(0, dVar.z());
        } else {
            dVar.E0(0);
        }
        boolean z3 = false;
        boolean z4 = false;
        for (int i5 = 0; i5 < size; i5++) {
            ConstraintWidget constraintWidget4 = o1.get(i5);
            if (constraintWidget4 instanceof f) {
                f fVar3 = (f) constraintWidget4;
                if (fVar3.p1() == 0) {
                    if (fVar3.q1() != -1) {
                        fVar3.t1(fVar3.q1());
                    } else if (fVar3.r1() != -1 && dVar.n0()) {
                        fVar3.t1(dVar.z() - fVar3.r1());
                    } else if (dVar.n0()) {
                        fVar3.t1((int) ((fVar3.s1() * dVar.z()) + 0.5f));
                    }
                    z3 = true;
                }
            } else if ((constraintWidget4 instanceof a) && ((a) constraintWidget4).u1() == 1) {
                z4 = true;
            }
        }
        if (z3) {
            for (int i6 = 0; i6 < size; i6++) {
                ConstraintWidget constraintWidget5 = o1.get(i6);
                if (constraintWidget5 instanceof f) {
                    f fVar4 = (f) constraintWidget5;
                    if (fVar4.p1() == 0) {
                        i(1, fVar4, bVar);
                    }
                }
            }
        }
        i(0, dVar, bVar);
        if (z4) {
            for (int i7 = 0; i7 < size; i7++) {
                ConstraintWidget constraintWidget6 = o1.get(i7);
                if (constraintWidget6 instanceof a) {
                    a aVar2 = (a) constraintWidget6;
                    if (aVar2.u1() == 1) {
                        c(0, aVar2, bVar, 1, M1);
                    }
                }
            }
        }
        for (int i8 = 0; i8 < size; i8++) {
            ConstraintWidget constraintWidget7 = o1.get(i8);
            if (constraintWidget7.l0() && a(0, constraintWidget7)) {
                d.P1(0, constraintWidget7, bVar, a, jo.a.k);
                if (constraintWidget7 instanceof f) {
                    if (((f) constraintWidget7).p1() == 0) {
                        i(0, constraintWidget7, bVar);
                    } else {
                        b(0, constraintWidget7, bVar, M1);
                    }
                } else {
                    b(0, constraintWidget7, bVar, M1);
                    i(0, constraintWidget7, bVar);
                }
            }
        }
    }

    public static void i(int i, ConstraintWidget constraintWidget, jo.b bVar) {
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        ConstraintAnchor constraintAnchor4;
        ConstraintAnchor constraintAnchor5;
        if (constraintWidget.o0()) {
            return;
        }
        c++;
        if (!(constraintWidget instanceof d) && constraintWidget.l0()) {
            int i2 = i + 1;
            if (a(i2, constraintWidget)) {
                d.P1(i2, constraintWidget, bVar, new jo.a(), jo.a.k);
            }
        }
        ConstraintAnchor q = constraintWidget.q(ConstraintAnchor.Type.TOP);
        ConstraintAnchor q2 = constraintWidget.q(ConstraintAnchor.Type.BOTTOM);
        int e = q.e();
        int e2 = q2.e();
        if (q.d() != null && q.n()) {
            Iterator<ConstraintAnchor> it = q.d().iterator();
            while (it.hasNext()) {
                ConstraintAnchor next = it.next();
                ConstraintWidget constraintWidget2 = next.d;
                int i3 = i + 1;
                boolean a2 = a(i3, constraintWidget2);
                if (constraintWidget2.l0() && a2) {
                    d.P1(i3, constraintWidget2, bVar, new jo.a(), jo.a.k);
                }
                ConstraintWidget.DimensionBehaviour S = constraintWidget2.S();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (S == dimensionBehaviour && !a2) {
                    if (constraintWidget2.S() == dimensionBehaviour && constraintWidget2.z >= 0 && constraintWidget2.y >= 0 && (constraintWidget2.U() == 8 || (constraintWidget2.t == 0 && constraintWidget2.x() == Utils.FLOAT_EPSILON))) {
                        if (!constraintWidget2.j0() && !constraintWidget2.k0()) {
                            if (((next == constraintWidget2.N && (constraintAnchor5 = constraintWidget2.P.f) != null && constraintAnchor5.n()) || (next == constraintWidget2.P && (constraintAnchor4 = constraintWidget2.N.f) != null && constraintAnchor4.n())) && !constraintWidget2.j0()) {
                                g(i3, constraintWidget, bVar, constraintWidget2);
                            }
                        }
                    }
                } else if (!constraintWidget2.l0()) {
                    ConstraintAnchor constraintAnchor6 = constraintWidget2.N;
                    if (next == constraintAnchor6 && constraintWidget2.P.f == null) {
                        int f = constraintAnchor6.f() + e;
                        constraintWidget2.F0(f, constraintWidget2.z() + f);
                        i(i3, constraintWidget2, bVar);
                    } else {
                        ConstraintAnchor constraintAnchor7 = constraintWidget2.P;
                        if (next == constraintAnchor7 && constraintAnchor7.f == null) {
                            int f2 = e - constraintAnchor7.f();
                            constraintWidget2.F0(f2 - constraintWidget2.z(), f2);
                            i(i3, constraintWidget2, bVar);
                        } else if (next == constraintAnchor6 && (constraintAnchor3 = constraintAnchor7.f) != null && constraintAnchor3.n()) {
                            f(i3, bVar, constraintWidget2);
                        }
                    }
                }
            }
        }
        if (constraintWidget instanceof f) {
            return;
        }
        if (q2.d() != null && q2.n()) {
            Iterator<ConstraintAnchor> it2 = q2.d().iterator();
            while (it2.hasNext()) {
                ConstraintAnchor next2 = it2.next();
                ConstraintWidget constraintWidget3 = next2.d;
                int i4 = i + 1;
                boolean a3 = a(i4, constraintWidget3);
                if (constraintWidget3.l0() && a3) {
                    d.P1(i4, constraintWidget3, bVar, new jo.a(), jo.a.k);
                }
                boolean z = (next2 == constraintWidget3.N && (constraintAnchor2 = constraintWidget3.P.f) != null && constraintAnchor2.n()) || (next2 == constraintWidget3.P && (constraintAnchor = constraintWidget3.N.f) != null && constraintAnchor.n());
                ConstraintWidget.DimensionBehaviour S2 = constraintWidget3.S();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (S2 == dimensionBehaviour2 && !a3) {
                    if (constraintWidget3.S() == dimensionBehaviour2 && constraintWidget3.z >= 0 && constraintWidget3.y >= 0 && (constraintWidget3.U() == 8 || (constraintWidget3.t == 0 && constraintWidget3.x() == Utils.FLOAT_EPSILON))) {
                        if (!constraintWidget3.j0() && !constraintWidget3.k0() && z && !constraintWidget3.j0()) {
                            g(i4, constraintWidget, bVar, constraintWidget3);
                        }
                    }
                } else if (!constraintWidget3.l0()) {
                    ConstraintAnchor constraintAnchor8 = constraintWidget3.N;
                    if (next2 == constraintAnchor8 && constraintWidget3.P.f == null) {
                        int f3 = constraintAnchor8.f() + e2;
                        constraintWidget3.F0(f3, constraintWidget3.z() + f3);
                        i(i4, constraintWidget3, bVar);
                    } else {
                        ConstraintAnchor constraintAnchor9 = constraintWidget3.P;
                        if (next2 == constraintAnchor9 && constraintAnchor8.f == null) {
                            int f4 = e2 - constraintAnchor9.f();
                            constraintWidget3.F0(f4 - constraintWidget3.z(), f4);
                            i(i4, constraintWidget3, bVar);
                        } else if (z && !constraintWidget3.j0()) {
                            f(i4, bVar, constraintWidget3);
                        }
                    }
                }
            }
        }
        ConstraintAnchor q3 = constraintWidget.q(ConstraintAnchor.Type.BASELINE);
        if (q3.d() != null && q3.n()) {
            int e3 = q3.e();
            Iterator<ConstraintAnchor> it3 = q3.d().iterator();
            while (it3.hasNext()) {
                ConstraintAnchor next3 = it3.next();
                ConstraintWidget constraintWidget4 = next3.d;
                int i5 = i + 1;
                boolean a4 = a(i5, constraintWidget4);
                if (constraintWidget4.l0() && a4) {
                    d.P1(i5, constraintWidget4, bVar, new jo.a(), jo.a.k);
                }
                if (constraintWidget4.S() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || a4) {
                    if (!constraintWidget4.l0() && next3 == constraintWidget4.Q) {
                        constraintWidget4.B0(next3.f() + e3);
                        i(i5, constraintWidget4, bVar);
                    }
                }
            }
        }
        constraintWidget.q0();
    }
}
