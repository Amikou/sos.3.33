package defpackage;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.data.i;
import com.bumptech.glide.load.data.n;
import defpackage.j92;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: UriLoader.java */
/* renamed from: pf4  reason: default package */
/* loaded from: classes.dex */
public class pf4<Data> implements j92<Uri, Data> {
    public static final Set<String> b = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", PublicResolver.FUNC_CONTENT)));
    public final c<Data> a;

    /* compiled from: UriLoader.java */
    /* renamed from: pf4$a */
    /* loaded from: classes.dex */
    public static final class a implements k92<Uri, AssetFileDescriptor>, c<AssetFileDescriptor> {
        public final ContentResolver a;

        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.pf4.c
        public com.bumptech.glide.load.data.d<AssetFileDescriptor> b(Uri uri) {
            return new com.bumptech.glide.load.data.a(this.a, uri);
        }

        @Override // defpackage.k92
        public j92<Uri, AssetFileDescriptor> c(qa2 qa2Var) {
            return new pf4(this);
        }
    }

    /* compiled from: UriLoader.java */
    /* renamed from: pf4$b */
    /* loaded from: classes.dex */
    public static class b implements k92<Uri, ParcelFileDescriptor>, c<ParcelFileDescriptor> {
        public final ContentResolver a;

        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.pf4.c
        public com.bumptech.glide.load.data.d<ParcelFileDescriptor> b(Uri uri) {
            return new i(this.a, uri);
        }

        @Override // defpackage.k92
        public j92<Uri, ParcelFileDescriptor> c(qa2 qa2Var) {
            return new pf4(this);
        }
    }

    /* compiled from: UriLoader.java */
    /* renamed from: pf4$c */
    /* loaded from: classes.dex */
    public interface c<Data> {
        com.bumptech.glide.load.data.d<Data> b(Uri uri);
    }

    /* compiled from: UriLoader.java */
    /* renamed from: pf4$d */
    /* loaded from: classes.dex */
    public static class d implements k92<Uri, InputStream>, c<InputStream> {
        public final ContentResolver a;

        public d(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.pf4.c
        public com.bumptech.glide.load.data.d<InputStream> b(Uri uri) {
            return new n(this.a, uri);
        }

        @Override // defpackage.k92
        public j92<Uri, InputStream> c(qa2 qa2Var) {
            return new pf4(this);
        }
    }

    public pf4(c<Data> cVar) {
        this.a = cVar;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(Uri uri, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(uri), this.a.b(uri));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
