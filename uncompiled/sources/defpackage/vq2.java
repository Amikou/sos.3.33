package defpackage;

/* compiled from: PingStore.java */
/* renamed from: vq2  reason: default package */
/* loaded from: classes.dex */
public class vq2 {
    public String[] b = new String[100];
    public String[] c = new String[100];
    public int a = 0;

    public int a(String str, String str2) {
        Boolean bool = Boolean.FALSE;
        int i = 0;
        while (true) {
            if (i >= this.a) {
                break;
            } else if (this.b[i].equals(str)) {
                this.c[i] = str2;
                bool = Boolean.TRUE;
                break;
            } else {
                i++;
            }
        }
        if (!bool.booleanValue()) {
            String[] strArr = this.b;
            int i2 = this.a;
            strArr[i2] = str;
            String[] strArr2 = this.c;
            this.a = i2 + 1;
            strArr2[i2] = str2;
        }
        return this.a + 1;
    }

    public String b(String str) {
        for (int i = 0; i <= this.a; i++) {
            if (this.b[i].equals(str)) {
                return this.c[i];
            }
        }
        return "null";
    }
}
