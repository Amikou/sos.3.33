package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: dn5  reason: default package */
/* loaded from: classes.dex */
public final class dn5 implements Runnable {
    public final /* synthetic */ long a;
    public final /* synthetic */ dp5 f0;

    public dn5(dp5 dp5Var, long j) {
        this.f0 = dp5Var;
        this.a = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f0.a.A().k.b(this.a);
        this.f0.a.w().u().b("Session timeout duration set", Long.valueOf(this.a));
    }
}
