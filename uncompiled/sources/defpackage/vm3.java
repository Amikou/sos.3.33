package defpackage;

import android.graphics.drawable.Drawable;

/* compiled from: SettableDraweeHierarchy.java */
/* renamed from: vm3  reason: default package */
/* loaded from: classes.dex */
public interface vm3 extends jr0 {
    void b(Drawable drawable);

    void c(Throwable th);

    void d(Throwable th);

    void e(float f, boolean z);

    void g(Drawable drawable, float f, boolean z);

    void reset();
}
