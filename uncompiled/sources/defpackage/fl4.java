package defpackage;

import java.math.BigInteger;

/* renamed from: fl4  reason: default package */
/* loaded from: classes2.dex */
public class fl4 extends x4 {
    @Override // defpackage.x4
    public pt0 c(pt0 pt0Var, BigInteger bigInteger) {
        pt0 pt0Var2;
        int max = Math.max(2, Math.min(16, d(bigInteger.bitLength())));
        gl4 l = hl4.l(pt0Var, max, true);
        pt0[] a = l.a();
        pt0[] b = l.b();
        int[] d = hl4.d(max, bigInteger);
        pt0 v = pt0Var.i().v();
        int length = d.length;
        if (length > 1) {
            length--;
            int i = d[length];
            int i2 = i >> 16;
            int i3 = i & 65535;
            int abs = Math.abs(i2);
            pt0[] pt0VarArr = i2 < 0 ? b : a;
            if ((abs << 2) < (1 << max)) {
                byte b2 = d22.g0[abs];
                int i4 = max - b2;
                pt0Var2 = pt0VarArr[((1 << (max - 1)) - 1) >>> 1].a(pt0VarArr[(((abs ^ (1 << (b2 - 1))) << i4) + 1) >>> 1]);
                i3 -= i4;
            } else {
                pt0Var2 = pt0VarArr[abs >>> 1];
            }
            v = pt0Var2.I(i3);
        }
        while (length > 0) {
            length--;
            int i5 = d[length];
            int i6 = i5 >> 16;
            v = v.K((i6 < 0 ? b : a)[Math.abs(i6) >>> 1]).I(i5 & 65535);
        }
        return v;
    }

    public int d(int i) {
        return hl4.i(i);
    }
}
