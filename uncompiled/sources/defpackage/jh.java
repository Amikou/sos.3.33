package defpackage;

/* compiled from: ArrayAdapterInterface.java */
/* renamed from: jh  reason: default package */
/* loaded from: classes.dex */
public interface jh<T> {
    String a();

    int b();

    int c(T t);

    T newArray(int i);
}
