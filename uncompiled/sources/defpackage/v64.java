package defpackage;

import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;

/* compiled from: TokenFilter.java */
/* renamed from: v64  reason: default package */
/* loaded from: classes.dex */
public class v64 {
    public static final v64 a = new v64();

    public boolean a() {
        return true;
    }

    public void b() {
    }

    public v64 c() {
        return this;
    }

    public v64 d() {
        return this;
    }

    public v64 e(int i) {
        return this;
    }

    public v64 f(String str) {
        return this;
    }

    public v64 g(int i) {
        return this;
    }

    public boolean h(JsonParser jsonParser) throws IOException {
        return a();
    }

    public String toString() {
        return this == a ? "TokenFilter.INCLUDE_ALL" : super.toString();
    }
}
