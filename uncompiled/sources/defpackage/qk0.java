package defpackage;

import defpackage.l24;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

/* compiled from: DefaultScheduler.java */
/* renamed from: qk0  reason: default package */
/* loaded from: classes.dex */
public class qk0 implements ad3 {
    public static final Logger f = Logger.getLogger(vb4.class.getName());
    public final rq4 a;
    public final Executor b;
    public final bm c;
    public final dy0 d;
    public final l24 e;

    public qk0(Executor executor, bm bmVar, rq4 rq4Var, dy0 dy0Var, l24 l24Var) {
        this.b = executor;
        this.c = bmVar;
        this.a = rq4Var;
        this.d = dy0Var;
        this.e = l24Var;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object d(ob4 ob4Var, wx0 wx0Var) {
        this.d.b1(ob4Var, wx0Var);
        this.a.a(ob4Var, 1);
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void e(final ob4 ob4Var, yb4 yb4Var, wx0 wx0Var) {
        try {
            nb4 nb4Var = this.c.get(ob4Var.b());
            if (nb4Var == null) {
                String format = String.format("Transport backend '%s' is not registered", ob4Var.b());
                f.warning(format);
                yb4Var.a(new IllegalArgumentException(format));
                return;
            }
            final wx0 a = nb4Var.a(wx0Var);
            this.e.a(new l24.a() { // from class: nk0
                @Override // defpackage.l24.a
                public final Object execute() {
                    Object d;
                    d = qk0.this.d(ob4Var, a);
                    return d;
                }
            });
            yb4Var.a(null);
        } catch (Exception e) {
            Logger logger = f;
            logger.warning("Error scheduling event " + e.getMessage());
            yb4Var.a(e);
        }
    }

    @Override // defpackage.ad3
    public void a(final ob4 ob4Var, final wx0 wx0Var, final yb4 yb4Var) {
        this.b.execute(new Runnable() { // from class: ok0
            @Override // java.lang.Runnable
            public final void run() {
                qk0.this.e(ob4Var, yb4Var, wx0Var);
            }
        });
    }
}
