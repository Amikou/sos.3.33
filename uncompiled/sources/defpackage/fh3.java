package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: fh3  reason: default package */
/* loaded from: classes2.dex */
public class fh3 extends ct0.a {
    public long[] f;

    public fh3() {
        this.f = fd2.b();
    }

    public fh3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 283) {
            throw new IllegalArgumentException("x value invalid for SecT283FieldElement");
        }
        this.f = eh3.d(bigInteger);
    }

    public fh3(long[] jArr) {
        this.f = jArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        long[] b = fd2.b();
        eh3.a(this.f, ((fh3) ct0Var).f, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        long[] b = fd2.b();
        eh3.c(this.f, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        return j(ct0Var.g());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof fh3) {
            return fd2.d(this.f, ((fh3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return 283;
    }

    @Override // defpackage.ct0
    public ct0 g() {
        long[] b = fd2.b();
        eh3.j(this.f, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return fd2.f(this.f);
    }

    public int hashCode() {
        return wh.v(this.f, 0, 5) ^ 2831275;
    }

    @Override // defpackage.ct0
    public boolean i() {
        return fd2.g(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        long[] b = fd2.b();
        eh3.k(this.f, ((fh3) ct0Var).f, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return l(ct0Var, ct0Var2, ct0Var3);
    }

    @Override // defpackage.ct0
    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        long[] jArr = this.f;
        long[] jArr2 = ((fh3) ct0Var).f;
        long[] jArr3 = ((fh3) ct0Var2).f;
        long[] jArr4 = ((fh3) ct0Var3).f;
        long[] k = kd2.k(9);
        eh3.l(jArr, jArr2, k);
        eh3.l(jArr3, jArr4, k);
        long[] b = fd2.b();
        eh3.m(k, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        return this;
    }

    @Override // defpackage.ct0
    public ct0 n() {
        long[] b = fd2.b();
        eh3.o(this.f, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 o() {
        long[] b = fd2.b();
        eh3.p(this.f, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        long[] jArr = this.f;
        long[] jArr2 = ((fh3) ct0Var).f;
        long[] jArr3 = ((fh3) ct0Var2).f;
        long[] k = kd2.k(9);
        eh3.q(jArr, k);
        eh3.l(jArr2, jArr3, k);
        long[] b = fd2.b();
        eh3.m(k, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 q(int i) {
        if (i < 1) {
            return this;
        }
        long[] b = fd2.b();
        eh3.r(this.f, i, b);
        return new fh3(b);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        return a(ct0Var);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return (this.f[0] & 1) != 0;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return fd2.h(this.f);
    }

    @Override // defpackage.ct0.a
    public int u() {
        return eh3.s(this.f);
    }
}
