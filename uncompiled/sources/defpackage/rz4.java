package defpackage;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.zas;
import com.google.android.gms.signin.internal.zam;
import defpackage.kz;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: rz4  reason: default package */
/* loaded from: classes.dex */
public final class rz4 implements j05 {
    public final i05 a;
    public final Lock b;
    public final Context c;
    public final eh1 d;
    public ConnectionResult e;
    public int f;
    public int h;
    public p15 k;
    public boolean l;
    public boolean m;
    public boolean n;
    public d o;
    public boolean p;
    public boolean q;
    public final kz r;
    public final Map<a<?>, Boolean> s;
    public final a.AbstractC0106a<? extends p15, uo3> t;
    public int g = 0;
    public final Bundle i = new Bundle();
    public final Set<a.c> j = new HashSet();
    public ArrayList<Future<?>> u = new ArrayList<>();

    public rz4(i05 i05Var, kz kzVar, Map<a<?>, Boolean> map, eh1 eh1Var, a.AbstractC0106a<? extends p15, uo3> abstractC0106a, Lock lock, Context context) {
        this.a = i05Var;
        this.r = kzVar;
        this.s = map;
        this.d = eh1Var;
        this.t = abstractC0106a;
        this.b = lock;
        this.c = context;
    }

    public static String u(int i) {
        return i != 0 ? i != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    public final void B() {
        this.a.n();
        r05.a().execute(new uz4(this));
        p15 p15Var = this.k;
        if (p15Var != null) {
            if (this.p) {
                p15Var.d((d) zt2.j(this.o), this.q);
            }
            m(false);
        }
        for (a.c<?> cVar : this.a.g.keySet()) {
            ((a.f) zt2.j(this.a.f.get(cVar))).b();
        }
        this.a.n.d(this.i.isEmpty() ? null : this.i);
    }

    public final void D() {
        this.m = false;
        this.a.m.p = Collections.emptySet();
        for (a.c<?> cVar : this.j) {
            if (!this.a.g.containsKey(cVar)) {
                this.a.g.put(cVar, new ConnectionResult(17, null));
            }
        }
    }

    public final void F() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Future<?> future = arrayList.get(i);
            i++;
            future.cancel(true);
        }
        this.u.clear();
    }

    public final Set<Scope> H() {
        if (this.r == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.r.e());
        Map<a<?>, kz.b> f = this.r.f();
        for (a<?> aVar : f.keySet()) {
            if (!this.a.g.containsKey(aVar.c())) {
                hashSet.addAll(f.get(aVar).a);
            }
        }
        return hashSet;
    }

    @Override // defpackage.j05
    public final void a() {
        this.a.g.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (a<?> aVar : this.s.keySet()) {
            a.f fVar = (a.f) zt2.j(this.a.f.get(aVar.c()));
            z |= aVar.a().b() == 1;
            boolean booleanValue = this.s.get(aVar).booleanValue();
            if (fVar.u()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(aVar.c());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(fVar, new tz4(this, aVar, booleanValue));
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            zt2.j(this.r);
            zt2.j(this.t);
            this.r.g(Integer.valueOf(System.identityHashCode(this.a.m)));
            c05 c05Var = new c05(this, null);
            a.AbstractC0106a<? extends p15, uo3> abstractC0106a = this.t;
            Context context = this.c;
            Looper g = this.a.m.g();
            kz kzVar = this.r;
            this.k = abstractC0106a.d(context, g, kzVar, kzVar.i(), c05Var, c05Var);
        }
        this.h = this.a.f.size();
        this.u.add(r05.a().submit(new wz4(this, hashMap)));
    }

    @Override // defpackage.j05
    public final void b(ConnectionResult connectionResult, a<?> aVar, boolean z) {
        if (s(1)) {
            r(connectionResult, aVar, z);
            if (x()) {
                B();
            }
        }
    }

    @Override // defpackage.j05
    public final void c() {
    }

    @Override // defpackage.j05
    public final void d(Bundle bundle) {
        if (s(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (x()) {
                B();
            }
        }
    }

    @Override // defpackage.j05
    public final void e(int i) {
        q(new ConnectionResult(8, null));
    }

    @Override // defpackage.j05
    public final boolean f() {
        F();
        m(true);
        this.a.i(null);
        return true;
    }

    @Override // defpackage.j05
    public final <A extends a.b, T extends b<? extends l83, A>> T g(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    public final void i(zam zamVar) {
        if (s(0)) {
            ConnectionResult I1 = zamVar.I1();
            if (I1.M1()) {
                zas zasVar = (zas) zt2.j(zamVar.J1());
                ConnectionResult J1 = zasVar.J1();
                if (!J1.M1()) {
                    String valueOf = String.valueOf(J1);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GACConnecting", sb.toString(), new Exception());
                    q(J1);
                    return;
                }
                this.n = true;
                this.o = (d) zt2.j(zasVar.I1());
                this.p = zasVar.K1();
                this.q = zasVar.L1();
                y();
            } else if (n(I1)) {
                D();
                y();
            } else {
                q(I1);
            }
        }
    }

    public final void m(boolean z) {
        p15 p15Var = this.k;
        if (p15Var != null) {
            if (p15Var.c() && z) {
                p15Var.a();
            }
            p15Var.b();
            kz kzVar = (kz) zt2.j(this.r);
            this.o = null;
        }
    }

    public final boolean n(ConnectionResult connectionResult) {
        return this.l && !connectionResult.L1();
    }

    public final void q(ConnectionResult connectionResult) {
        F();
        m(!connectionResult.L1());
        this.a.i(connectionResult);
        this.a.n.a(connectionResult);
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0022, code lost:
        if ((r5.L1() || r4.d.c(r5.I1()) != null) != false) goto L15;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void r(com.google.android.gms.common.ConnectionResult r5, com.google.android.gms.common.api.a<?> r6, boolean r7) {
        /*
            r4 = this;
            com.google.android.gms.common.api.a$e r0 = r6.a()
            int r0 = r0.b()
            r1 = 0
            r2 = 1
            if (r7 == 0) goto L24
            boolean r7 = r5.L1()
            if (r7 == 0) goto L14
        L12:
            r7 = r2
            goto L22
        L14:
            eh1 r7 = r4.d
            int r3 = r5.I1()
            android.content.Intent r7 = r7.c(r3)
            if (r7 == 0) goto L21
            goto L12
        L21:
            r7 = r1
        L22:
            if (r7 == 0) goto L2d
        L24:
            com.google.android.gms.common.ConnectionResult r7 = r4.e
            if (r7 == 0) goto L2c
            int r7 = r4.f
            if (r0 >= r7) goto L2d
        L2c:
            r1 = r2
        L2d:
            if (r1 == 0) goto L33
            r4.e = r5
            r4.f = r0
        L33:
            i05 r7 = r4.a
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.ConnectionResult> r7 = r7.g
            com.google.android.gms.common.api.a$c r6 = r6.c()
            r7.put(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.rz4.r(com.google.android.gms.common.ConnectionResult, com.google.android.gms.common.api.a, boolean):void");
    }

    public final boolean s(int i) {
        if (this.g != i) {
            this.a.m.s();
            String valueOf = String.valueOf(this);
            StringBuilder sb = new StringBuilder(valueOf.length() + 23);
            sb.append("Unexpected callback in ");
            sb.append(valueOf);
            int i2 = this.h;
            StringBuilder sb2 = new StringBuilder(33);
            sb2.append("mRemainingConnections=");
            sb2.append(i2);
            String u = u(this.g);
            String u2 = u(i);
            StringBuilder sb3 = new StringBuilder(String.valueOf(u).length() + 70 + String.valueOf(u2).length());
            sb3.append("GoogleApiClient connecting is in step ");
            sb3.append(u);
            sb3.append(" but received callback for step ");
            sb3.append(u2);
            new Exception();
            q(new ConnectionResult(8, null));
            return false;
        }
        return true;
    }

    public final boolean x() {
        int i = this.h - 1;
        this.h = i;
        if (i > 0) {
            return false;
        }
        if (i < 0) {
            this.a.m.s();
            Log.wtf("GACConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            q(new ConnectionResult(8, null));
            return false;
        }
        ConnectionResult connectionResult = this.e;
        if (connectionResult != null) {
            this.a.l = this.f;
            q(connectionResult);
            return false;
        }
        return true;
    }

    public final void y() {
        if (this.h != 0) {
            return;
        }
        if (!this.m || this.n) {
            ArrayList arrayList = new ArrayList();
            this.g = 1;
            this.h = this.a.f.size();
            for (a.c<?> cVar : this.a.f.keySet()) {
                if (this.a.g.containsKey(cVar)) {
                    if (x()) {
                        B();
                    }
                } else {
                    arrayList.add(this.a.f.get(cVar));
                }
            }
            if (arrayList.isEmpty()) {
                return;
            }
            this.u.add(r05.a().submit(new xz4(this, arrayList)));
        }
    }
}
