package defpackage;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;

/* compiled from: GsonBuilder.kt */
/* renamed from: om0  reason: default package */
/* loaded from: classes.dex */
public final class om0 {
    public final JsonElement a;
    public final Type b;
    public final a c;

    /* compiled from: GsonBuilder.kt */
    /* renamed from: om0$a */
    /* loaded from: classes.dex */
    public static final class a implements JsonDeserializationContext {
        public final JsonDeserializationContext a;

        public a(JsonDeserializationContext jsonDeserializationContext) {
            fs1.g(jsonDeserializationContext, "gsonContext");
            this.a = jsonDeserializationContext;
        }

        public final JsonDeserializationContext a() {
            return this.a;
        }

        @Override // com.google.gson.JsonDeserializationContext
        public <T> T deserialize(JsonElement jsonElement, Type type) {
            return (T) this.a.deserialize(jsonElement, type);
        }
    }

    public om0(JsonElement jsonElement, Type type, a aVar) {
        fs1.g(jsonElement, "json");
        fs1.g(type, "type");
        fs1.g(aVar, "context");
        this.a = jsonElement;
        this.b = type;
        this.c = aVar;
    }

    public final a a() {
        return this.c;
    }

    public final JsonElement b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof om0) {
                om0 om0Var = (om0) obj;
                return fs1.b(this.a, om0Var.a) && fs1.b(this.b, om0Var.b) && fs1.b(this.c, om0Var.c);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        JsonElement jsonElement = this.a;
        int hashCode = (jsonElement != null ? jsonElement.hashCode() : 0) * 31;
        Type type = this.b;
        int hashCode2 = (hashCode + (type != null ? type.hashCode() : 0)) * 31;
        a aVar = this.c;
        return hashCode2 + (aVar != null ? aVar.hashCode() : 0);
    }

    public String toString() {
        return "DeserializerArg(json=" + this.a + ", type=" + this.b + ", context=" + this.c + ")";
    }
}
