package defpackage;

import kotlinx.coroutines.e;

/* compiled from: EventLoop.kt */
/* renamed from: zq  reason: default package */
/* loaded from: classes2.dex */
public final class zq extends e {
    public final Thread k0;

    public zq(Thread thread) {
        this.k0 = thread;
    }

    @Override // defpackage.yx0
    public Thread f0() {
        return this.k0;
    }
}
