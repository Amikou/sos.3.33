package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;
import android.util.SparseArray;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.internal.vision.y0;
import com.google.android.gms.internal.vision.zzk;
import com.google.android.gms.internal.vision.zzs;
import com.google.android.gms.vision.barcode.Barcode;
import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: hm  reason: default package */
/* loaded from: classes.dex */
public final class hm extends rm0<Barcode> {
    public final y0 c;

    /* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
    /* renamed from: hm$a */
    /* loaded from: classes.dex */
    public static class a {
        public Context a;
        public zzk b = new zzk();

        public a(@RecentlyNonNull Context context) {
            this.a = context;
        }

        @RecentlyNonNull
        public hm a() {
            return new hm(new y0(this.a, this.b));
        }

        @RecentlyNonNull
        public a b(int i) {
            this.b.a = i;
            return this;
        }
    }

    public hm(y0 y0Var) {
        this.c = y0Var;
    }

    @Override // defpackage.rm0
    @RecentlyNonNull
    public final SparseArray<Barcode> a(@RecentlyNonNull yb1 yb1Var) {
        Barcode[] g;
        if (yb1Var != null) {
            zzs I1 = zzs.I1(yb1Var);
            if (yb1Var.a() != null) {
                g = this.c.f((Bitmap) zt2.j(yb1Var.a()), I1);
                if (g == null) {
                    throw new IllegalArgumentException("Internal barcode detector error; check logcat output.");
                }
            } else if (Build.VERSION.SDK_INT >= 19 && yb1Var.d() != null) {
                g = this.c.g((ByteBuffer) zt2.j(((Image.Plane[]) zt2.j(yb1Var.d()))[0].getBuffer()), new zzs(((Image.Plane[]) zt2.j(yb1Var.d()))[0].getRowStride(), I1.f0, I1.g0, I1.h0, I1.i0));
            } else {
                g = this.c.g((ByteBuffer) zt2.j(yb1Var.b()), I1);
            }
            SparseArray<Barcode> sparseArray = new SparseArray<>(g.length);
            for (Barcode barcode : g) {
                sparseArray.append(barcode.f0.hashCode(), barcode);
            }
            return sparseArray;
        }
        throw new IllegalArgumentException("No frame supplied.");
    }

    @Override // defpackage.rm0
    public final boolean b() {
        return this.c.c();
    }

    @Override // defpackage.rm0
    public final void d() {
        super.d();
        this.c.d();
    }
}
