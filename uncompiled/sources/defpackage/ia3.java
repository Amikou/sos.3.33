package defpackage;

/* renamed from: ia3  reason: default package */
/* loaded from: classes2.dex */
public class ia3 extends re1 {
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int[] i;
    public int j;

    public ia3() {
        this.i = new int[80];
        reset();
    }

    public ia3(ia3 ia3Var) {
        super(ia3Var);
        this.i = new int[80];
        n(ia3Var);
    }

    @Override // defpackage.qo0
    public int a(byte[] bArr, int i) {
        j();
        ro2.c(this.d, bArr, i);
        ro2.c(this.e, bArr, i + 4);
        ro2.c(this.f, bArr, i + 8);
        ro2.c(this.g, bArr, i + 12);
        ro2.c(this.h, bArr, i + 16);
        reset();
        return 20;
    }

    @Override // defpackage.j72
    public j72 copy() {
        return new ia3(this);
    }

    @Override // defpackage.j72
    public void d(j72 j72Var) {
        ia3 ia3Var = (ia3) j72Var;
        super.i(ia3Var);
        n(ia3Var);
    }

    @Override // defpackage.qo0
    public String g() {
        return "SHA-1";
    }

    @Override // defpackage.qo0
    public int h() {
        return 20;
    }

    @Override // defpackage.re1
    public void k() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        for (int i13 = 16; i13 < 80; i13++) {
            int[] iArr = this.i;
            int i14 = ((iArr[i13 - 3] ^ iArr[i13 - 8]) ^ iArr[i13 - 14]) ^ iArr[i13 - 16];
            iArr[i13] = (i14 >>> 31) | (i14 << 1);
        }
        int i15 = this.d;
        int i16 = this.e;
        int i17 = this.f;
        int i18 = this.g;
        int i19 = this.h;
        int i20 = 0;
        int i21 = 0;
        while (i20 < 4) {
            int i22 = i21 + 1;
            int o = i19 + ((i15 << 5) | (i15 >>> 27)) + o(i16, i17, i18) + this.i[i21] + 1518500249;
            int i23 = (i16 >>> 2) | (i16 << 30);
            int i24 = i22 + 1;
            int o2 = i18 + ((o << 5) | (o >>> 27)) + o(i15, i23, i17) + this.i[i22] + 1518500249;
            int i25 = (i15 >>> 2) | (i15 << 30);
            int i26 = i24 + 1;
            int o3 = i17 + ((o2 << 5) | (o2 >>> 27)) + o(o, i25, i23) + this.i[i24] + 1518500249;
            i19 = (o >>> 2) | (o << 30);
            int i27 = i26 + 1;
            i16 = i23 + ((o3 << 5) | (o3 >>> 27)) + o(o2, i19, i25) + this.i[i26] + 1518500249;
            i18 = (o2 >>> 2) | (o2 << 30);
            i15 = i25 + ((i16 << 5) | (i16 >>> 27)) + o(o3, i18, i19) + this.i[i27] + 1518500249;
            i17 = (o3 >>> 2) | (o3 << 30);
            i20++;
            i21 = i27 + 1;
        }
        int i28 = 0;
        while (i28 < 4) {
            int i29 = i21 + 1;
            int q = i19 + ((i15 << 5) | (i15 >>> 27)) + q(i16, i17, i18) + this.i[i21] + 1859775393;
            int i30 = (i16 >>> 2) | (i16 << 30);
            int i31 = i29 + 1;
            int q2 = i18 + ((q << 5) | (q >>> 27)) + q(i15, i30, i17) + this.i[i29] + 1859775393;
            int i32 = (i15 >>> 2) | (i15 << 30);
            int i33 = i31 + 1;
            int q3 = i17 + ((q2 << 5) | (q2 >>> 27)) + q(q, i32, i30) + this.i[i31] + 1859775393;
            i19 = (q >>> 2) | (q << 30);
            int i34 = i33 + 1;
            i16 = i30 + ((q3 << 5) | (q3 >>> 27)) + q(q2, i19, i32) + this.i[i33] + 1859775393;
            i18 = (q2 >>> 2) | (q2 << 30);
            i15 = i32 + ((i16 << 5) | (i16 >>> 27)) + q(q3, i18, i19) + this.i[i34] + 1859775393;
            i17 = (q3 >>> 2) | (q3 << 30);
            i28++;
            i21 = i34 + 1;
        }
        int i35 = 0;
        while (i35 < 4) {
            int p = i19 + (((((i15 << 5) | (i15 >>> 27)) + p(i16, i17, i18)) + this.i[i21]) - 1894007588);
            int p2 = i18 + (((((p << 5) | (p >>> 27)) + p(i15, i8, i17)) + this.i[i7]) - 1894007588);
            int p3 = i17 + (((((p2 << 5) | (p2 >>> 27)) + p(p, i10, i8)) + this.i[i9]) - 1894007588);
            i19 = (p >>> 2) | (p << 30);
            i16 = ((i16 >>> 2) | (i16 << 30)) + (((((p3 << 5) | (p3 >>> 27)) + p(p2, i19, i10)) + this.i[i11]) - 1894007588);
            i18 = (p2 >>> 2) | (p2 << 30);
            i15 = ((i15 >>> 2) | (i15 << 30)) + (((((i16 << 5) | (i16 >>> 27)) + p(p3, i18, i19)) + this.i[i12]) - 1894007588);
            i17 = (p3 >>> 2) | (p3 << 30);
            i35++;
            i21 = i21 + 1 + 1 + 1 + 1 + 1;
        }
        int i36 = 0;
        while (i36 <= 3) {
            int q4 = i19 + (((((i15 << 5) | (i15 >>> 27)) + q(i16, i17, i18)) + this.i[i21]) - 899497514);
            int q5 = i18 + (((((q4 << 5) | (q4 >>> 27)) + q(i15, i2, i17)) + this.i[i]) - 899497514);
            int q6 = i17 + (((((q5 << 5) | (q5 >>> 27)) + q(q4, i4, i2)) + this.i[i3]) - 899497514);
            i19 = (q4 >>> 2) | (q4 << 30);
            i16 = ((i16 >>> 2) | (i16 << 30)) + (((((q6 << 5) | (q6 >>> 27)) + q(q5, i19, i4)) + this.i[i5]) - 899497514);
            i18 = (q5 >>> 2) | (q5 << 30);
            i15 = ((i15 >>> 2) | (i15 << 30)) + (((((i16 << 5) | (i16 >>> 27)) + q(q6, i18, i19)) + this.i[i6]) - 899497514);
            i17 = (q6 >>> 2) | (q6 << 30);
            i36++;
            i21 = i21 + 1 + 1 + 1 + 1 + 1;
        }
        this.d += i15;
        this.e += i16;
        this.f += i17;
        this.g += i18;
        this.h += i19;
        this.j = 0;
        for (int i37 = 0; i37 < 16; i37++) {
            this.i[i37] = 0;
        }
    }

    @Override // defpackage.re1
    public void l(long j) {
        if (this.j > 14) {
            k();
        }
        int[] iArr = this.i;
        iArr[14] = (int) (j >>> 32);
        iArr[15] = (int) j;
    }

    @Override // defpackage.re1
    public void m(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        int i4 = (bArr[i3 + 1] & 255) | (bArr[i] << 24) | ((bArr[i2] & 255) << 16) | ((bArr[i3] & 255) << 8);
        int[] iArr = this.i;
        int i5 = this.j;
        iArr[i5] = i4;
        int i6 = i5 + 1;
        this.j = i6;
        if (i6 == 16) {
            k();
        }
    }

    public final void n(ia3 ia3Var) {
        this.d = ia3Var.d;
        this.e = ia3Var.e;
        this.f = ia3Var.f;
        this.g = ia3Var.g;
        this.h = ia3Var.h;
        int[] iArr = ia3Var.i;
        System.arraycopy(iArr, 0, this.i, 0, iArr.length);
        this.j = ia3Var.j;
    }

    public final int o(int i, int i2, int i3) {
        return ((~i) & i3) | (i2 & i);
    }

    public final int p(int i, int i2, int i3) {
        return (i & i3) | (i & i2) | (i2 & i3);
    }

    public final int q(int i, int i2, int i3) {
        return (i ^ i2) ^ i3;
    }

    @Override // defpackage.re1, defpackage.qo0
    public void reset() {
        super.reset();
        this.d = 1732584193;
        this.e = -271733879;
        this.f = -1732584194;
        this.g = 271733878;
        this.h = -1009589776;
        this.j = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.i;
            if (i == iArr.length) {
                return;
            }
            iArr[i] = 0;
            i++;
        }
    }
}
