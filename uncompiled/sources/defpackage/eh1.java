package defpackage;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: eh1  reason: default package */
/* loaded from: classes.dex */
public class eh1 {
    @RecentlyNonNull
    public static final int a = qh1.a;
    public static final eh1 b = new eh1();

    @RecentlyNonNull
    public static eh1 h() {
        return b;
    }

    public static String n(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(a);
        sb.append("-");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(kr4.a(context).e(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return sb.toString();
    }

    public void a(@RecentlyNonNull Context context) {
        qh1.a(context);
    }

    @RecentlyNonNull
    public int b(@RecentlyNonNull Context context) {
        return qh1.b(context);
    }

    @RecentlyNullable
    @Deprecated
    public Intent c(@RecentlyNonNull int i) {
        return d(null, i, null);
    }

    @RecentlyNullable
    public Intent d(Context context, @RecentlyNonNull int i, String str) {
        if (i != 1 && i != 2) {
            if (i != 3) {
                return null;
            }
            return zr5.b("com.google.android.gms");
        } else if (context != null && vm0.f(context)) {
            return zr5.a();
        } else {
            return zr5.c("com.google.android.gms", n(context, str));
        }
    }

    @RecentlyNullable
    public PendingIntent e(@RecentlyNonNull Context context, @RecentlyNonNull int i, @RecentlyNonNull int i2) {
        return f(context, i, i2, null);
    }

    @RecentlyNullable
    public PendingIntent f(@RecentlyNonNull Context context, @RecentlyNonNull int i, @RecentlyNonNull int i2, String str) {
        Intent d = d(context, i, str);
        if (d == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, d, 134217728);
    }

    public String g(@RecentlyNonNull int i) {
        return qh1.c(i);
    }

    @RecentlyNonNull
    public int i(@RecentlyNonNull Context context) {
        return j(context, a);
    }

    @RecentlyNonNull
    public int j(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        int g = qh1.g(context, i);
        if (qh1.h(context, g)) {
            return 18;
        }
        return g;
    }

    @RecentlyNonNull
    public boolean k(@RecentlyNonNull Context context, @RecentlyNonNull int i) {
        return qh1.h(context, i);
    }

    @RecentlyNonNull
    public boolean l(@RecentlyNonNull Context context, @RecentlyNonNull String str) {
        return qh1.m(context, str);
    }

    @RecentlyNonNull
    public boolean m(@RecentlyNonNull int i) {
        return qh1.j(i);
    }
}
