package defpackage;

import java.io.File;

/* compiled from: RootToolsInternalMethods.java */
/* renamed from: m93  reason: default package */
/* loaded from: classes2.dex */
public class m93 {
    public static boolean a() {
        String[] strArr = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
        for (int i = 0; i < 8; i++) {
            try {
                if (new File(strArr[i] + "su").exists()) {
                    return true;
                }
            } catch (Throwable unused) {
            }
        }
        return false;
    }
}
