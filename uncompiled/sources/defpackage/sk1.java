package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: HolderChainNetworkBinding.java */
/* renamed from: sk1  reason: default package */
/* loaded from: classes2.dex */
public final class sk1 {
    public final ConstraintLayout a;
    public final ImageView b;
    public final TextView c;

    public sk1(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, AppCompatImageView appCompatImageView, ImageView imageView, TextView textView) {
        this.a = constraintLayout;
        this.b = imageView;
        this.c = textView;
    }

    public static sk1 a(View view) {
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        int i = R.id.imgIndicator;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgIndicator);
        if (appCompatImageView != null) {
            i = R.id.ivChainIcon;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivChainIcon);
            if (imageView != null) {
                i = R.id.tvChainName;
                TextView textView = (TextView) ai4.a(view, R.id.tvChainName);
                if (textView != null) {
                    return new sk1(constraintLayout, constraintLayout, appCompatImageView, imageView, textView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
