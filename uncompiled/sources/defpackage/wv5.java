package defpackage;

import com.google.protobuf.j0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: wv5  reason: default package */
/* loaded from: classes.dex */
public final class wv5 {
    public static final qv5 a = c();
    public static final qv5 b = new pv5();

    public static qv5 a() {
        return a;
    }

    public static qv5 b() {
        return b;
    }

    public static qv5 c() {
        try {
            return (qv5) j0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
