package defpackage;

import androidx.room.o;
import defpackage.tw3;
import java.io.File;
import java.io.InputStream;
import java.util.concurrent.Callable;

/* compiled from: SQLiteCopyOpenHelperFactory.java */
/* renamed from: xa3  reason: default package */
/* loaded from: classes.dex */
public class xa3 implements tw3.c {
    public final String a;
    public final File b;
    public final Callable<InputStream> c;
    public final tw3.c d;

    public xa3(String str, File file, Callable<InputStream> callable, tw3.c cVar) {
        this.a = str;
        this.b = file;
        this.c = callable;
        this.d = cVar;
    }

    @Override // defpackage.tw3.c
    public tw3 a(tw3.b bVar) {
        return new o(bVar.a, this.a, this.b, this.c, bVar.c.a, this.d.a(bVar));
    }
}
