package defpackage;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.core.Constants;

/* compiled from: LazyHeaders.java */
/* renamed from: xy1  reason: default package */
/* loaded from: classes.dex */
public final class xy1 implements gk1 {
    public final Map<String, List<wy1>> b;
    public volatile Map<String, String> c;

    /* compiled from: LazyHeaders.java */
    /* renamed from: xy1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final String b;
        public static final Map<String, List<wy1>> c;
        public Map<String, List<wy1>> a = c;

        static {
            String b2 = b();
            b = b2;
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(b2)) {
                hashMap.put(Constants.USER_AGENT_HEADER_KEY, Collections.singletonList(new b(b2)));
            }
            c = Collections.unmodifiableMap(hashMap);
        }

        public static String b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == '\t') && charAt < 127) {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        public xy1 a() {
            return new xy1(this.a);
        }
    }

    /* compiled from: LazyHeaders.java */
    /* renamed from: xy1$b */
    /* loaded from: classes.dex */
    public static final class b implements wy1 {
        public final String a;

        public b(String str) {
            this.a = str;
        }

        @Override // defpackage.wy1
        public String a() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.a.equals(((b) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return this.a.hashCode();
        }

        public String toString() {
            return "StringHeaderFactory{value='" + this.a + "'}";
        }
    }

    public xy1(Map<String, List<wy1>> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    @Override // defpackage.gk1
    public Map<String, String> a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = Collections.unmodifiableMap(c());
                }
            }
        }
        return this.c;
    }

    public final String b(List<wy1> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String a2 = list.get(i).a();
            if (!TextUtils.isEmpty(a2)) {
                sb.append(a2);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }

    public final Map<String, String> c() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<String, List<wy1>> entry : this.b.entrySet()) {
            String b2 = b(entry.getValue());
            if (!TextUtils.isEmpty(b2)) {
                hashMap.put(entry.getKey(), b2);
            }
        }
        return hashMap;
    }

    public boolean equals(Object obj) {
        if (obj instanceof xy1) {
            return this.b.equals(((xy1) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        return "LazyHeaders{headers=" + this.b + '}';
    }
}
