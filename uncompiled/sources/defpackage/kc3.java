package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;

/* compiled from: ScaleDownDrawer.java */
/* renamed from: kc3  reason: default package */
/* loaded from: classes2.dex */
public class kc3 extends hn {
    public kc3(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2, int i3) {
        if (wg4Var instanceof ic3) {
            ic3 ic3Var = (ic3) wg4Var;
            float m = this.b.m();
            int p = this.b.p();
            int q = this.b.q();
            int r = this.b.r();
            int f = this.b.f();
            if (this.b.z()) {
                if (i == r) {
                    m = ic3Var.e();
                    p = ic3Var.a();
                } else if (i == q) {
                    m = ic3Var.f();
                    p = ic3Var.b();
                }
            } else if (i == q) {
                m = ic3Var.e();
                p = ic3Var.a();
            } else if (i == f) {
                m = ic3Var.f();
                p = ic3Var.b();
            }
            this.a.setColor(p);
            canvas.drawCircle(i2, i3, m, this.a);
        }
    }
}
