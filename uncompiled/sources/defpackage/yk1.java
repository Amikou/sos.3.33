package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: HolderReflectionTokenBinding.java */
/* renamed from: yk1  reason: default package */
/* loaded from: classes2.dex */
public final class yk1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final ImageView c;
    public final TextView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;
    public final TextView h;

    public yk1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, ImageView imageView, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, AppCompatTextView appCompatTextView, View view, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = materialButton2;
        this.c = imageView;
        this.d = textView;
        this.e = textView2;
        this.f = textView3;
        this.g = textView4;
        this.h = textView5;
    }

    public static yk1 a(View view) {
        int i = R.id.btnDelete;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDelete);
        if (materialButton != null) {
            i = R.id.btnEnableAdvancedDataMode;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnEnableAdvancedDataMode);
            if (materialButton2 != null) {
                i = R.id.ivTokenIcon;
                ImageView imageView = (ImageView) ai4.a(view, R.id.ivTokenIcon);
                if (imageView != null) {
                    i = R.id.rowBG;
                    ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.rowBG);
                    if (constraintLayout != null) {
                        i = R.id.rowFG;
                        ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.rowFG);
                        if (constraintLayout2 != null) {
                            ConstraintLayout constraintLayout3 = (ConstraintLayout) view;
                            i = R.id.tvStartDate;
                            TextView textView = (TextView) ai4.a(view, R.id.tvStartDate);
                            if (textView != null) {
                                i = R.id.tvTokenName;
                                TextView textView2 = (TextView) ai4.a(view, R.id.tvTokenName);
                                if (textView2 != null) {
                                    i = R.id.tvTokenNativeBalance;
                                    TextView textView3 = (TextView) ai4.a(view, R.id.tvTokenNativeBalance);
                                    if (textView3 != null) {
                                        i = R.id.tvTokenNativeBalanceInFiat;
                                        TextView textView4 = (TextView) ai4.a(view, R.id.tvTokenNativeBalanceInFiat);
                                        if (textView4 != null) {
                                            i = R.id.tvTokenSymbol;
                                            TextView textView5 = (TextView) ai4.a(view, R.id.tvTokenSymbol);
                                            if (textView5 != null) {
                                                i = R.id.txtNoteAllTime;
                                                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtNoteAllTime);
                                                if (appCompatTextView != null) {
                                                    i = R.id.vDivider;
                                                    View a = ai4.a(view, R.id.vDivider);
                                                    if (a != null) {
                                                        i = R.id.vTokenNameContainer;
                                                        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vTokenNameContainer);
                                                        if (linearLayout != null) {
                                                            return new yk1(constraintLayout3, materialButton, materialButton2, imageView, constraintLayout, constraintLayout2, constraintLayout3, textView, textView2, textView3, textView4, textView5, appCompatTextView, a, linearLayout);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
