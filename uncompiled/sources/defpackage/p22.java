package defpackage;

import android.graphics.Bitmap;

/* compiled from: LruBitmapPool.java */
/* renamed from: p22  reason: default package */
/* loaded from: classes.dex */
public class p22 implements iq {
    public final vs2<Bitmap> a = new lq();
    public final int b;
    public int c;
    public final zs2 d;
    public int e;

    public p22(int i, int i2, zs2 zs2Var, r72 r72Var) {
        this.b = i;
        this.c = i2;
        this.d = zs2Var;
        if (r72Var != null) {
            r72Var.a(this);
        }
    }

    public final Bitmap f(int i) {
        this.d.a(i);
        return Bitmap.createBitmap(1, i, Bitmap.Config.ALPHA_8);
    }

    @Override // defpackage.us2
    /* renamed from: g */
    public synchronized Bitmap get(int i) {
        int i2 = this.e;
        int i3 = this.b;
        if (i2 > i3) {
            i(i3);
        }
        Bitmap bitmap = this.a.get(i);
        if (bitmap != null) {
            int a = this.a.a(bitmap);
            this.e -= a;
            this.d.b(a);
            return bitmap;
        }
        return f(i);
    }

    @Override // defpackage.us2, defpackage.d83
    /* renamed from: h */
    public void a(Bitmap bitmap) {
        int a = this.a.a(bitmap);
        if (a <= this.c) {
            this.d.g(a);
            this.a.c(bitmap);
            synchronized (this) {
                this.e += a;
            }
        }
    }

    public final synchronized void i(int i) {
        Bitmap pop;
        while (this.e > i && (pop = this.a.pop()) != null) {
            int a = this.a.a(pop);
            this.e -= a;
            this.d.e(a);
        }
    }
}
