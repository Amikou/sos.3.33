package defpackage;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: m35  reason: default package */
/* loaded from: classes.dex */
public class m35 implements IInterface {
    public final IBinder a;

    public m35(IBinder iBinder, String str) {
        this.a = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.a;
    }
}
