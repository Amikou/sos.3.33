package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ItemAktFiatListHeaderBinding.java */
/* renamed from: ss1  reason: default package */
/* loaded from: classes2.dex */
public final class ss1 {
    public final ConstraintLayout a;
    public final TextView b;

    public ss1(ConstraintLayout constraintLayout, TextView textView) {
        this.a = constraintLayout;
        this.b = textView;
    }

    public static ss1 a(View view) {
        TextView textView = (TextView) ai4.a(view, R.id.tvText);
        if (textView != null) {
            return new ss1((ConstraintLayout) view, textView);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.tvText)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
