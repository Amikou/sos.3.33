package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: SupportRequestManagerFragment.java */
/* renamed from: mw3  reason: default package */
/* loaded from: classes.dex */
public class mw3 extends Fragment {
    public final d7 a;
    public final m73 f0;
    public final Set<mw3> g0;
    public mw3 h0;
    public k73 i0;
    public Fragment j0;

    /* compiled from: SupportRequestManagerFragment.java */
    /* renamed from: mw3$a */
    /* loaded from: classes.dex */
    public class a implements m73 {
        public a() {
        }

        @Override // defpackage.m73
        public Set<k73> a() {
            Set<mw3> f = mw3.this.f();
            HashSet hashSet = new HashSet(f.size());
            for (mw3 mw3Var : f) {
                if (mw3Var.i() != null) {
                    hashSet.add(mw3Var.i());
                }
            }
            return hashSet;
        }

        public String toString() {
            return super.toString() + "{fragment=" + mw3.this + "}";
        }
    }

    public mw3() {
        this(new d7());
    }

    public static FragmentManager k(Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getFragmentManager();
    }

    public final void e(mw3 mw3Var) {
        this.g0.add(mw3Var);
    }

    public Set<mw3> f() {
        mw3 mw3Var = this.h0;
        if (mw3Var == null) {
            return Collections.emptySet();
        }
        if (equals(mw3Var)) {
            return Collections.unmodifiableSet(this.g0);
        }
        HashSet hashSet = new HashSet();
        for (mw3 mw3Var2 : this.h0.f()) {
            if (l(mw3Var2.h())) {
                hashSet.add(mw3Var2);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public d7 g() {
        return this.a;
    }

    public final Fragment h() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.j0;
    }

    public k73 i() {
        return this.i0;
    }

    public m73 j() {
        return this.f0;
    }

    public final boolean l(Fragment fragment) {
        Fragment h = h();
        while (true) {
            Fragment parentFragment = fragment.getParentFragment();
            if (parentFragment == null) {
                return false;
            }
            if (parentFragment.equals(h)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    public final void m(Context context, FragmentManager fragmentManager) {
        q();
        mw3 s = com.bumptech.glide.a.c(context).k().s(fragmentManager);
        this.h0 = s;
        if (equals(s)) {
            return;
        }
        this.h0.e(this);
    }

    public final void n(mw3 mw3Var) {
        this.g0.remove(mw3Var);
    }

    public void o(Fragment fragment) {
        FragmentManager k;
        this.j0 = fragment;
        if (fragment == null || fragment.getContext() == null || (k = k(fragment)) == null) {
            return;
        }
        m(fragment.getContext(), k);
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        FragmentManager k = k(this);
        if (k == null) {
            return;
        }
        try {
            m(getContext(), k);
        } catch (IllegalStateException unused) {
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.a.c();
        q();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.j0 = null;
        q();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        this.a.d();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        this.a.e();
    }

    public void p(k73 k73Var) {
        this.i0 = k73Var;
    }

    public final void q() {
        mw3 mw3Var = this.h0;
        if (mw3Var != null) {
            mw3Var.n(this);
            this.h0 = null;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public String toString() {
        return super.toString() + "{parent=" + h() + "}";
    }

    @SuppressLint({"ValidFragment"})
    public mw3(d7 d7Var) {
        this.f0 = new a();
        this.g0 = new HashSet();
        this.a = d7Var;
    }
}
