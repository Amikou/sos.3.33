package defpackage;

import androidx.recyclerview.widget.g;

/* compiled from: SimpleITokenAdapter.kt */
/* renamed from: fp3  reason: default package */
/* loaded from: classes2.dex */
public final class fp3 extends g.f<q9> {
    public static final fp3 a = new fp3();

    @Override // androidx.recyclerview.widget.g.f
    /* renamed from: a */
    public boolean areContentsTheSame(q9 q9Var, q9 q9Var2) {
        fs1.f(q9Var, "oldItem");
        fs1.f(q9Var2, "newItem");
        return fs1.b(q9Var, q9Var2);
    }

    @Override // androidx.recyclerview.widget.g.f
    /* renamed from: b */
    public boolean areItemsTheSame(q9 q9Var, q9 q9Var2) {
        fs1.f(q9Var, "oldItem");
        fs1.f(q9Var2, "newItem");
        return fs1.b(q9Var.h(), q9Var2.h());
    }
}
