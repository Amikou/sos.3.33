package defpackage;

import java.util.concurrent.Executor;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: Executors.kt */
/* renamed from: sp0  reason: default package */
/* loaded from: classes2.dex */
public final class sp0 implements Executor {
    public final CoroutineDispatcher a;

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.a.h(EmptyCoroutineContext.INSTANCE, runnable);
    }

    public String toString() {
        return this.a.toString();
    }
}
