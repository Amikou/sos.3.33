package defpackage;

import androidx.room.RoomDatabase;

/* compiled from: EntityInsertionAdapter.java */
/* renamed from: zv0  reason: default package */
/* loaded from: classes.dex */
public abstract class zv0<T> extends co3 {
    public zv0(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    public abstract void g(ww3 ww3Var, T t);

    public final void h(T t) {
        ww3 a = a();
        try {
            g(a, t);
            a.C1();
        } finally {
            f(a);
        }
    }

    public final void i(T[] tArr) {
        ww3 a = a();
        try {
            for (T t : tArr) {
                g(a, t);
                a.C1();
            }
        } finally {
            f(a);
        }
    }

    public final long j(T t) {
        ww3 a = a();
        try {
            g(a, t);
            return a.C1();
        } finally {
            f(a);
        }
    }
}
