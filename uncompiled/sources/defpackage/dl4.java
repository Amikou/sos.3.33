package defpackage;

import android.util.Base64;
import androidx.media3.common.Metadata;
import androidx.media3.common.ParserException;
import androidx.media3.extractor.metadata.flac.PictureFrame;
import androidx.media3.extractor.metadata.vorbis.VorbisComment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: VorbisUtil.java */
/* renamed from: dl4  reason: default package */
/* loaded from: classes.dex */
public final class dl4 {

    /* compiled from: VorbisUtil.java */
    /* renamed from: dl4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public a(int i, int i2, long[] jArr, int i3, boolean z) {
        }
    }

    /* compiled from: VorbisUtil.java */
    /* renamed from: dl4$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final String[] a;

        public b(String str, String[] strArr, int i) {
            this.a = strArr;
        }
    }

    /* compiled from: VorbisUtil.java */
    /* renamed from: dl4$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final boolean a;

        public c(boolean z, int i, int i2, int i3) {
            this.a = z;
        }
    }

    /* compiled from: VorbisUtil.java */
    /* renamed from: dl4$d */
    /* loaded from: classes.dex */
    public static final class d {
        public final int a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final byte[] g;

        public d(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z, byte[] bArr) {
            this.a = i2;
            this.b = i3;
            this.c = i4;
            this.d = i5;
            this.e = i7;
            this.f = i8;
            this.g = bArr;
        }
    }

    public static int a(int i) {
        int i2 = 0;
        while (i > 0) {
            i2++;
            i >>>= 1;
        }
        return i2;
    }

    public static long b(long j, long j2) {
        return (long) Math.floor(Math.pow(j, 1.0d / j2));
    }

    public static Metadata c(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            String str = list.get(i);
            String[] M0 = androidx.media3.common.util.b.M0(str, "=");
            if (M0.length != 2) {
                p12.i("VorbisUtil", "Failed to parse Vorbis comment: " + str);
            } else if (M0[0].equals("METADATA_BLOCK_PICTURE")) {
                try {
                    arrayList.add(PictureFrame.a(new op2(Base64.decode(M0[1], 0))));
                } catch (RuntimeException e) {
                    p12.j("VorbisUtil", "Failed to parse vorbis picture", e);
                }
            } else {
                arrayList.add(new VorbisComment(M0[0], M0[1]));
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata(arrayList);
    }

    public static a d(bl4 bl4Var) throws ParserException {
        if (bl4Var.d(24) == 5653314) {
            int d2 = bl4Var.d(16);
            int d3 = bl4Var.d(24);
            long[] jArr = new long[d3];
            boolean c2 = bl4Var.c();
            long j = 0;
            if (!c2) {
                boolean c3 = bl4Var.c();
                for (int i = 0; i < d3; i++) {
                    if (c3) {
                        if (bl4Var.c()) {
                            jArr[i] = bl4Var.d(5) + 1;
                        } else {
                            jArr[i] = 0;
                        }
                    } else {
                        jArr[i] = bl4Var.d(5) + 1;
                    }
                }
            } else {
                int d4 = bl4Var.d(5) + 1;
                int i2 = 0;
                while (i2 < d3) {
                    int d5 = bl4Var.d(a(d3 - i2));
                    for (int i3 = 0; i3 < d5 && i2 < d3; i3++) {
                        jArr[i2] = d4;
                        i2++;
                    }
                    d4++;
                }
            }
            int d6 = bl4Var.d(4);
            if (d6 <= 2) {
                if (d6 == 1 || d6 == 2) {
                    bl4Var.e(32);
                    bl4Var.e(32);
                    int d7 = bl4Var.d(4) + 1;
                    bl4Var.e(1);
                    if (d6 != 1) {
                        j = d3 * d2;
                    } else if (d2 != 0) {
                        j = b(d3, d2);
                    }
                    bl4Var.e((int) (j * d7));
                }
                return new a(d2, d3, jArr, d6, c2);
            }
            throw ParserException.createForMalformedContainer("lookup type greater than 2 not decodable: " + d6, null);
        }
        throw ParserException.createForMalformedContainer("expected code book to start with [0x56, 0x43, 0x42] at " + bl4Var.b(), null);
    }

    public static void e(bl4 bl4Var) throws ParserException {
        int d2 = bl4Var.d(6) + 1;
        for (int i = 0; i < d2; i++) {
            int d3 = bl4Var.d(16);
            if (d3 == 0) {
                bl4Var.e(8);
                bl4Var.e(16);
                bl4Var.e(16);
                bl4Var.e(6);
                bl4Var.e(8);
                int d4 = bl4Var.d(4) + 1;
                for (int i2 = 0; i2 < d4; i2++) {
                    bl4Var.e(8);
                }
            } else if (d3 != 1) {
                throw ParserException.createForMalformedContainer("floor type greater than 1 not decodable: " + d3, null);
            } else {
                int d5 = bl4Var.d(5);
                int i3 = -1;
                int[] iArr = new int[d5];
                for (int i4 = 0; i4 < d5; i4++) {
                    iArr[i4] = bl4Var.d(4);
                    if (iArr[i4] > i3) {
                        i3 = iArr[i4];
                    }
                }
                int i5 = i3 + 1;
                int[] iArr2 = new int[i5];
                for (int i6 = 0; i6 < i5; i6++) {
                    iArr2[i6] = bl4Var.d(3) + 1;
                    int d6 = bl4Var.d(2);
                    if (d6 > 0) {
                        bl4Var.e(8);
                    }
                    for (int i7 = 0; i7 < (1 << d6); i7++) {
                        bl4Var.e(8);
                    }
                }
                bl4Var.e(2);
                int d7 = bl4Var.d(4);
                int i8 = 0;
                int i9 = 0;
                for (int i10 = 0; i10 < d5; i10++) {
                    i8 += iArr2[iArr[i10]];
                    while (i9 < i8) {
                        bl4Var.e(d7);
                        i9++;
                    }
                }
            }
        }
    }

    public static void f(int i, bl4 bl4Var) throws ParserException {
        int d2 = bl4Var.d(6) + 1;
        for (int i2 = 0; i2 < d2; i2++) {
            int d3 = bl4Var.d(16);
            if (d3 != 0) {
                p12.c("VorbisUtil", "mapping type other than 0 not supported: " + d3);
            } else {
                int d4 = bl4Var.c() ? bl4Var.d(4) + 1 : 1;
                if (bl4Var.c()) {
                    int d5 = bl4Var.d(8) + 1;
                    for (int i3 = 0; i3 < d5; i3++) {
                        int i4 = i - 1;
                        bl4Var.e(a(i4));
                        bl4Var.e(a(i4));
                    }
                }
                if (bl4Var.d(2) != 0) {
                    throw ParserException.createForMalformedContainer("to reserved bits must be zero after mapping coupling steps", null);
                }
                if (d4 > 1) {
                    for (int i5 = 0; i5 < i; i5++) {
                        bl4Var.e(4);
                    }
                }
                for (int i6 = 0; i6 < d4; i6++) {
                    bl4Var.e(8);
                    bl4Var.e(8);
                    bl4Var.e(8);
                }
            }
        }
    }

    public static c[] g(bl4 bl4Var) {
        int d2 = bl4Var.d(6) + 1;
        c[] cVarArr = new c[d2];
        for (int i = 0; i < d2; i++) {
            cVarArr[i] = new c(bl4Var.c(), bl4Var.d(16), bl4Var.d(16), bl4Var.d(8));
        }
        return cVarArr;
    }

    public static void h(bl4 bl4Var) throws ParserException {
        int d2 = bl4Var.d(6) + 1;
        for (int i = 0; i < d2; i++) {
            if (bl4Var.d(16) <= 2) {
                bl4Var.e(24);
                bl4Var.e(24);
                bl4Var.e(24);
                int d3 = bl4Var.d(6) + 1;
                bl4Var.e(8);
                int[] iArr = new int[d3];
                for (int i2 = 0; i2 < d3; i2++) {
                    iArr[i2] = ((bl4Var.c() ? bl4Var.d(5) : 0) * 8) + bl4Var.d(3);
                }
                for (int i3 = 0; i3 < d3; i3++) {
                    for (int i4 = 0; i4 < 8; i4++) {
                        if ((iArr[i3] & (1 << i4)) != 0) {
                            bl4Var.e(8);
                        }
                    }
                }
            } else {
                throw ParserException.createForMalformedContainer("residueType greater than 2 is not decodable", null);
            }
        }
    }

    public static b i(op2 op2Var) throws ParserException {
        return j(op2Var, true, true);
    }

    public static b j(op2 op2Var, boolean z, boolean z2) throws ParserException {
        if (z) {
            m(3, op2Var, false);
        }
        String A = op2Var.A((int) op2Var.t());
        int length = 11 + A.length();
        long t = op2Var.t();
        String[] strArr = new String[(int) t];
        int i = length + 4;
        for (int i2 = 0; i2 < t; i2++) {
            strArr[i2] = op2Var.A((int) op2Var.t());
            i = i + 4 + strArr[i2].length();
        }
        if (z2 && (op2Var.D() & 1) == 0) {
            throw ParserException.createForMalformedContainer("framing bit expected to be set", null);
        }
        return new b(A, strArr, i + 1);
    }

    public static d k(op2 op2Var) throws ParserException {
        m(1, op2Var, false);
        int u = op2Var.u();
        int D = op2Var.D();
        int u2 = op2Var.u();
        int q = op2Var.q();
        if (q <= 0) {
            q = -1;
        }
        int q2 = op2Var.q();
        if (q2 <= 0) {
            q2 = -1;
        }
        int q3 = op2Var.q();
        if (q3 <= 0) {
            q3 = -1;
        }
        int D2 = op2Var.D();
        return new d(u, D, u2, q, q2, q3, (int) Math.pow(2.0d, D2 & 15), (int) Math.pow(2.0d, (D2 & 240) >> 4), (op2Var.D() & 1) > 0, Arrays.copyOf(op2Var.d(), op2Var.f()));
    }

    public static c[] l(op2 op2Var, int i) throws ParserException {
        m(5, op2Var, false);
        int D = op2Var.D() + 1;
        bl4 bl4Var = new bl4(op2Var.d());
        bl4Var.e(op2Var.e() * 8);
        for (int i2 = 0; i2 < D; i2++) {
            d(bl4Var);
        }
        int d2 = bl4Var.d(6) + 1;
        for (int i3 = 0; i3 < d2; i3++) {
            if (bl4Var.d(16) != 0) {
                throw ParserException.createForMalformedContainer("placeholder of time domain transforms not zeroed out", null);
            }
        }
        e(bl4Var);
        h(bl4Var);
        f(i, bl4Var);
        c[] g = g(bl4Var);
        if (bl4Var.c()) {
            return g;
        }
        throw ParserException.createForMalformedContainer("framing bit after modes not set as expected", null);
    }

    public static boolean m(int i, op2 op2Var, boolean z) throws ParserException {
        if (op2Var.a() < 7) {
            if (z) {
                return false;
            }
            throw ParserException.createForMalformedContainer("too short header: " + op2Var.a(), null);
        } else if (op2Var.D() != i) {
            if (z) {
                return false;
            }
            throw ParserException.createForMalformedContainer("expected header type " + Integer.toHexString(i), null);
        } else if (op2Var.D() == 118 && op2Var.D() == 111 && op2Var.D() == 114 && op2Var.D() == 98 && op2Var.D() == 105 && op2Var.D() == 115) {
            return true;
        } else {
            if (z) {
                return false;
            }
            throw ParserException.createForMalformedContainer("expected characters 'vorbis'", null);
        }
    }
}
