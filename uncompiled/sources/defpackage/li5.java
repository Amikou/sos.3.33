package defpackage;

import com.google.android.gms.internal.measurement.q0;
import com.google.android.gms.internal.measurement.w0;
import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.x0;
import com.google.android.gms.internal.measurement.x1;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: li5  reason: default package */
/* loaded from: classes.dex */
public final class li5 extends w1<x0, li5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public li5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.x0 r0 = com.google.android.gms.internal.measurement.x0.K()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.li5.<init>():void");
    }

    public final List<q0> A() {
        return Collections.unmodifiableList(((x0) this.f0).E());
    }

    public final li5 B() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        ((x0) this.f0).zzj = x1.o();
        return this;
    }

    public final int v() {
        return ((x0) this.f0).C();
    }

    public final w0 x(int i) {
        return ((x0) this.f0).D(i);
    }

    public final li5 y(int i, sh5 sh5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        x0.L((x0) this.f0, i, sh5Var.o());
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ li5(defpackage.qh5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.x0 r1 = com.google.android.gms.internal.measurement.x0.K()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.li5.<init>(qh5):void");
    }
}
