package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.card.MaterialCardView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentDefaultScreenBinding.java */
/* renamed from: ca1  reason: default package */
/* loaded from: classes2.dex */
public final class ca1 {
    public final ConstraintLayout a;
    public final rp3 b;
    public final LinearLayout c;

    public ca1(ConstraintLayout constraintLayout, MaterialCardView materialCardView, rp3 rp3Var, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = rp3Var;
        this.c = linearLayout;
    }

    public static ca1 a(View view) {
        int i = R.id.ccWrapperScreen;
        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ccWrapperScreen);
        if (materialCardView != null) {
            i = R.id.toolbar;
            View a = ai4.a(view, R.id.toolbar);
            if (a != null) {
                rp3 a2 = rp3.a(a);
                LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.wrapperScreen);
                if (linearLayout != null) {
                    return new ca1((ConstraintLayout) view, materialCardView, a2, linearLayout);
                }
                i = R.id.wrapperScreen;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
