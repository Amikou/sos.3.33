package defpackage;

import java.util.Comparator;

/* compiled from: Comparisons.kt */
/* renamed from: z83  reason: default package */
/* loaded from: classes2.dex */
public final class z83 implements Comparator<Comparable<? super Object>> {
    public static final z83 a = new z83();

    @Override // java.util.Comparator
    /* renamed from: a */
    public int compare(Comparable<Object> comparable, Comparable<Object> comparable2) {
        fs1.f(comparable, "a");
        fs1.f(comparable2, "b");
        return comparable2.compareTo(comparable);
    }

    @Override // java.util.Comparator
    public final Comparator<Comparable<? super Object>> reversed() {
        return vd2.a;
    }
}
