package defpackage;

import androidx.activity.result.ActivityResultRegistry;

/* compiled from: ActivityResultRegistryOwner.java */
/* renamed from: x7  reason: default package */
/* loaded from: classes.dex */
public interface x7 {
    ActivityResultRegistry getActivityResultRegistry();
}
