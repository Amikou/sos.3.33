package defpackage;

import android.os.Build;

/* compiled from: BundleCompat.java */
/* renamed from: es  reason: default package */
/* loaded from: classes2.dex */
public class es {
    public static cs a() {
        if (Build.VERSION.SDK_INT >= 22) {
            return new fs();
        }
        return new ds();
    }
}
