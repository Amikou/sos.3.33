package defpackage;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: k15  reason: default package */
/* loaded from: classes.dex */
public final class k15 {
    public static final Status c = new Status(8, "The connection to Google Play services was lost");
    public final Set<BasePendingResult<?>> a = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    public final m15 b = new n15(this);

    public final void a() {
        BasePendingResult[] basePendingResultArr;
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.a.toArray(new BasePendingResult[0])) {
            basePendingResult.k(null);
            if (basePendingResult.l()) {
                this.a.remove(basePendingResult);
            }
        }
    }

    public final void b(BasePendingResult<? extends l83> basePendingResult) {
        this.a.add(basePendingResult);
        basePendingResult.k(this.b);
    }
}
