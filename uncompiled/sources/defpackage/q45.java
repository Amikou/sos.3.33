package defpackage;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.media3.common.PlaybackException;
import com.google.android.gms.common.util.b;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: q45  reason: default package */
/* loaded from: classes.dex */
public final class q45 extends ql5 {
    public Boolean b;
    public n45 c;
    public Boolean d;

    public q45(ck5 ck5Var) {
        super(ck5Var);
        this.c = j45.a;
    }

    public static final long I() {
        return qf5.d.b(null).longValue();
    }

    public static final long f() {
        return qf5.D.b(null).longValue();
    }

    public final boolean A() {
        this.a.b();
        Boolean y = y("firebase_analytics_collection_deactivated");
        return y != null && y.booleanValue();
    }

    public final boolean B() {
        Boolean y = y("google_analytics_adid_collection_enabled");
        return y == null || y.booleanValue();
    }

    public final boolean C() {
        Boolean y;
        l26.a();
        return !v(null, qf5.r0) || (y = y("google_analytics_automatic_screen_reporting_enabled")) == null || y.booleanValue();
    }

    public final String D() {
        return g("debug.firebase.analytics.app", "");
    }

    public final String E() {
        return g("debug.deferred.deeplink", "");
    }

    public final boolean F(String str) {
        return "1".equals(this.c.c(str, "gaia_collection_enabled"));
    }

    public final boolean G(String str) {
        return "1".equals(this.c.c(str, "measurement.event_sampling_enabled"));
    }

    public final boolean H() {
        if (this.b == null) {
            Boolean y = y("app_measurement_lite");
            this.b = y;
            if (y == null) {
                this.b = Boolean.FALSE;
            }
        }
        return this.b.booleanValue() || !this.a.O();
    }

    public final String g(String str, String str2) {
        try {
            String str3 = (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, "");
            zt2.j(str3);
            return str3;
        } catch (ClassNotFoundException e) {
            this.a.w().l().b("Could not find SystemProperties class", e);
            return "";
        } catch (IllegalAccessException e2) {
            this.a.w().l().b("Could not access SystemProperties.get()", e2);
            return "";
        } catch (NoSuchMethodException e3) {
            this.a.w().l().b("Could not find SystemProperties.get() method", e3);
            return "";
        } catch (InvocationTargetException e4) {
            this.a.w().l().b("SystemProperties.get() threw an exception", e4);
            return "";
        }
    }

    public final void h(n45 n45Var) {
        this.c = n45Var;
    }

    public final String i() {
        this.a.b();
        return "FA";
    }

    public final int j() {
        sw5 G = this.a.G();
        Boolean o = G.a.R().o();
        if (G.N() < 201500) {
            return (o == null || o.booleanValue()) ? 25 : 100;
        }
        return 100;
    }

    public final int k(String str) {
        return t(str, qf5.I, 25, 100);
    }

    public final int l(String str) {
        return t(str, qf5.H, 500, PlaybackException.ERROR_CODE_IO_UNSPECIFIED);
    }

    public final long n() {
        this.a.b();
        return 42004L;
    }

    public final boolean o() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    ApplicationInfo applicationInfo = this.a.m().getApplicationInfo();
                    String a = b.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        boolean z = false;
                        if (str != null && str.equals(a)) {
                            z = true;
                        }
                        this.d = Boolean.valueOf(z);
                    }
                    if (this.d == null) {
                        this.d = Boolean.TRUE;
                        this.a.w().l().a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.d.booleanValue();
    }

    public final String p(String str, we5<String> we5Var) {
        if (str == null) {
            return we5Var.b(null);
        }
        return we5Var.b(this.c.c(str, we5Var.a()));
    }

    public final long r(String str, we5<Long> we5Var) {
        if (str == null) {
            return we5Var.b(null).longValue();
        }
        String c = this.c.c(str, we5Var.a());
        if (TextUtils.isEmpty(c)) {
            return we5Var.b(null).longValue();
        }
        try {
            return we5Var.b(Long.valueOf(Long.parseLong(c))).longValue();
        } catch (NumberFormatException unused) {
            return we5Var.b(null).longValue();
        }
    }

    public final int s(String str, we5<Integer> we5Var) {
        if (str == null) {
            return we5Var.b(null).intValue();
        }
        String c = this.c.c(str, we5Var.a());
        if (TextUtils.isEmpty(c)) {
            return we5Var.b(null).intValue();
        }
        try {
            return we5Var.b(Integer.valueOf(Integer.parseInt(c))).intValue();
        } catch (NumberFormatException unused) {
            return we5Var.b(null).intValue();
        }
    }

    public final int t(String str, we5<Integer> we5Var, int i, int i2) {
        return Math.max(Math.min(s(str, we5Var), i2), i);
    }

    public final double u(String str, we5<Double> we5Var) {
        if (str == null) {
            return we5Var.b(null).doubleValue();
        }
        String c = this.c.c(str, we5Var.a());
        if (TextUtils.isEmpty(c)) {
            return we5Var.b(null).doubleValue();
        }
        try {
            return we5Var.b(Double.valueOf(Double.parseDouble(c))).doubleValue();
        } catch (NumberFormatException unused) {
            return we5Var.b(null).doubleValue();
        }
    }

    public final boolean v(String str, we5<Boolean> we5Var) {
        if (str == null) {
            return we5Var.b(null).booleanValue();
        }
        String c = this.c.c(str, we5Var.a());
        if (TextUtils.isEmpty(c)) {
            return we5Var.b(null).booleanValue();
        }
        return we5Var.b(Boolean.valueOf(Boolean.parseBoolean(c))).booleanValue();
    }

    public final Bundle x() {
        try {
            if (this.a.m().getPackageManager() == null) {
                this.a.w().l().a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo c = kr4.a(this.a.m()).c(this.a.m().getPackageName(), 128);
            if (c == null) {
                this.a.w().l().a("Failed to load metadata: ApplicationInfo is null");
                return null;
            }
            return c.metaData;
        } catch (PackageManager.NameNotFoundException e) {
            this.a.w().l().b("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    public final Boolean y(String str) {
        zt2.f(str);
        Bundle x = x();
        if (x == null) {
            this.a.w().l().a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (x.containsKey(str)) {
            return Boolean.valueOf(x.getBoolean(str));
        } else {
            return null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x002e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.List<java.lang.String> z(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.String r4 = "analytics.safelisted_events"
            defpackage.zt2.f(r4)
            android.os.Bundle r0 = r3.x()
            r1 = 0
            if (r0 != 0) goto L1d
            ck5 r4 = r3.a
            og5 r4 = r4.w()
            jg5 r4 = r4.l()
            java.lang.String r0 = "Failed to load metadata: Metadata bundle is null"
            r4.a(r0)
        L1b:
            r4 = r1
            goto L2c
        L1d:
            boolean r2 = r0.containsKey(r4)
            if (r2 != 0) goto L24
            goto L1b
        L24:
            int r4 = r0.getInt(r4)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
        L2c:
            if (r4 == 0) goto L58
            ck5 r0 = r3.a     // Catch: android.content.res.Resources.NotFoundException -> L48
            android.content.Context r0 = r0.m()     // Catch: android.content.res.Resources.NotFoundException -> L48
            android.content.res.Resources r0 = r0.getResources()     // Catch: android.content.res.Resources.NotFoundException -> L48
            int r4 = r4.intValue()     // Catch: android.content.res.Resources.NotFoundException -> L48
            java.lang.String[] r4 = r0.getStringArray(r4)     // Catch: android.content.res.Resources.NotFoundException -> L48
            if (r4 != 0) goto L43
            return r1
        L43:
            java.util.List r4 = java.util.Arrays.asList(r4)     // Catch: android.content.res.Resources.NotFoundException -> L48
            return r4
        L48:
            r4 = move-exception
            ck5 r0 = r3.a
            og5 r0 = r0.w()
            jg5 r0 = r0.l()
            java.lang.String r2 = "Failed to load string array from metadata: resource not found"
            r0.b(r2, r4)
        L58:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.q45.z(java.lang.String):java.util.List");
    }
}
