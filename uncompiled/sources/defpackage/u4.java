package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: CoroutineContextImpl.kt */
/* renamed from: u4  reason: default package */
/* loaded from: classes2.dex */
public abstract class u4 implements CoroutineContext.a {
    private final CoroutineContext.b<?> key;

    public u4(CoroutineContext.b<?> bVar) {
        fs1.f(bVar, "key");
        this.key = bVar;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public <R> R fold(R r, hd1<? super R, ? super CoroutineContext.a, ? extends R> hd1Var) {
        fs1.f(hd1Var, "operation");
        return (R) CoroutineContext.a.C0196a.a(this, r, hd1Var);
    }

    @Override // kotlin.coroutines.CoroutineContext.a, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        fs1.f(bVar, "key");
        return (E) CoroutineContext.a.C0196a.b(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext.a
    public CoroutineContext.b<?> getKey() {
        return this.key;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        fs1.f(bVar, "key");
        return CoroutineContext.a.C0196a.c(this, bVar);
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        fs1.f(coroutineContext, "context");
        return CoroutineContext.a.C0196a.d(this, coroutineContext);
    }
}
