package defpackage;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;

/* compiled from: ViewOverlayApi18.java */
/* renamed from: oj4  reason: default package */
/* loaded from: classes.dex */
public class oj4 implements qj4 {
    public final ViewOverlay a;

    public oj4(View view) {
        this.a = view.getOverlay();
    }

    @Override // defpackage.qj4
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @Override // defpackage.qj4
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }
}
