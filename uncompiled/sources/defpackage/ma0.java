package defpackage;

import org.web3j.crypto.a;

/* compiled from: Credentials.java */
/* renamed from: ma0  reason: default package */
/* loaded from: classes3.dex */
public class ma0 {
    private final String address;
    private final et0 ecKeyPair;

    private ma0(et0 et0Var, String str) {
        this.ecKeyPair = et0Var;
        this.address = str;
    }

    public static ma0 create(et0 et0Var) {
        return new ma0(et0Var, ej2.prependHexPrefix(a.getAddress(et0Var)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ma0.class != obj.getClass()) {
            return false;
        }
        ma0 ma0Var = (ma0) obj;
        et0 et0Var = this.ecKeyPair;
        if (et0Var == null ? ma0Var.ecKeyPair == null : et0Var.equals(ma0Var.ecKeyPair)) {
            String str = this.address;
            String str2 = ma0Var.address;
            return str != null ? str.equals(str2) : str2 == null;
        }
        return false;
    }

    public String getAddress() {
        return this.address;
    }

    public et0 getEcKeyPair() {
        return this.ecKeyPair;
    }

    public int hashCode() {
        et0 et0Var = this.ecKeyPair;
        int hashCode = (et0Var != null ? et0Var.hashCode() : 0) * 31;
        String str = this.address;
        return hashCode + (str != null ? str.hashCode() : 0);
    }

    public static ma0 create(String str, String str2) {
        return create(new et0(ej2.toBigInt(str), ej2.toBigInt(str2)));
    }

    public static ma0 create(String str) {
        return create(et0.create(ej2.toBigInt(str)));
    }
}
