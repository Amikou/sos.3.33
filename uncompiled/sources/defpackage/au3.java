package defpackage;

import java8.util.o;
import java8.util.stream.Collector;

/* compiled from: Stream.java */
/* renamed from: au3  reason: default package */
/* loaded from: classes2.dex */
public interface au3<T> {
    long a();

    au3<T> b(fu2<? super T> fu2Var);

    <R> au3<R> c(nd1<? super T, ? extends R> nd1Var);

    o<T> d();

    boolean e(fu2<? super T> fu2Var);

    <R, A> R f(Collector<? super T, A, R> collector);
}
