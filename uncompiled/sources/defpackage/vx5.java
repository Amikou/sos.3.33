package defpackage;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: vx5  reason: default package */
/* loaded from: classes.dex */
public final class vx5 extends ey5 {
    public final /* synthetic */ ux5 f0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public vx5(ux5 ux5Var) {
        super(ux5Var, null);
        this.f0 = ux5Var;
    }

    @Override // defpackage.ey5, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new wx5(this.f0, null);
    }

    public /* synthetic */ vx5(ux5 ux5Var, sx5 sx5Var) {
        this(ux5Var);
    }
}
