package defpackage;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;

/* compiled from: RoundedBitmapDrawable.java */
/* renamed from: t93  reason: default package */
/* loaded from: classes.dex */
public class t93 extends w93 {
    public final Paint H0;
    public final Paint I0;
    public final Bitmap J0;
    public WeakReference<Bitmap> K0;

    public t93(Resources resources, Bitmap bitmap, Paint paint) {
        super(new BitmapDrawable(resources, bitmap));
        Paint paint2 = new Paint();
        this.H0 = paint2;
        Paint paint3 = new Paint(1);
        this.I0 = paint3;
        this.J0 = bitmap;
        if (paint != null) {
            paint2.set(paint);
        }
        paint2.setFlags(1);
        paint3.setStyle(Paint.Style.STROKE);
    }

    @Override // defpackage.w93, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (nc1.d()) {
            nc1.a("RoundedBitmapDrawable#draw");
        }
        if (!f()) {
            super.draw(canvas);
            if (nc1.d()) {
                nc1.b();
                return;
            }
            return;
        }
        i();
        g();
        k();
        int save = canvas.save();
        canvas.concat(this.y0);
        canvas.drawPath(this.i0, this.H0);
        float f = this.h0;
        if (f > Utils.FLOAT_EPSILON) {
            this.I0.setStrokeWidth(f);
            this.I0.setColor(br0.c(this.k0, this.H0.getAlpha()));
            canvas.drawPath(this.l0, this.I0);
        }
        canvas.restoreToCount(save);
        if (nc1.d()) {
            nc1.b();
        }
    }

    @Override // defpackage.w93
    public boolean f() {
        return super.f() && this.J0 != null;
    }

    public final void k() {
        WeakReference<Bitmap> weakReference = this.K0;
        if (weakReference == null || weakReference.get() != this.J0) {
            this.K0 = new WeakReference<>(this.J0);
            Paint paint = this.H0;
            Bitmap bitmap = this.J0;
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            paint.setShader(new BitmapShader(bitmap, tileMode, tileMode));
            this.j0 = true;
        }
        if (this.j0) {
            this.H0.getShader().setLocalMatrix(this.B0);
            this.j0 = false;
        }
        this.H0.setFilterBitmap(c());
    }

    @Override // defpackage.w93, android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        super.setAlpha(i);
        if (i != this.H0.getAlpha()) {
            this.H0.setAlpha(i);
            super.setAlpha(i);
            invalidateSelf();
        }
    }

    @Override // defpackage.w93, android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        super.setColorFilter(colorFilter);
        this.H0.setColorFilter(colorFilter);
    }
}
