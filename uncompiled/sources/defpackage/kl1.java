package defpackage;

/* compiled from: HttpResponse.java */
/* renamed from: kl1  reason: default package */
/* loaded from: classes2.dex */
public class kl1 {
    public final int a;
    public final String b;

    public kl1(int i, String str) {
        this.a = i;
        this.b = str;
    }

    public String a() {
        return this.b;
    }

    public int b() {
        return this.a;
    }
}
