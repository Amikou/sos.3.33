package defpackage;

import android.os.Build;
import java.io.File;

/* renamed from: gu4  reason: default package */
/* loaded from: classes2.dex */
public final class gu4 {
    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static fu4 a() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            switch (i) {
                case 21:
                    return new qu4((byte[]) null);
                case 22:
                    return new qu4();
                case 23:
                    return new qu4((char[]) null);
                case 24:
                    return new qu4((short[]) null);
                case 25:
                    return new qu4((int[]) null);
                case 26:
                    return new qu4((boolean[]) null);
                case 27:
                    if (Build.VERSION.PREVIEW_SDK_INT == 0) {
                        return new qu4((float[]) null);
                    }
                    break;
            }
            return new qu4((byte[][]) null);
        }
        throw new AssertionError("Unsupported Android Version");
    }

    public static String b(File file) {
        if (file.getName().endsWith(".apk")) {
            String str = "";
            String replaceFirst = file.getName().replaceFirst("(_\\d+)?\\.apk", "");
            if (replaceFirst.equals("base-master")) {
                return "";
            }
            String str2 = "base-";
            if (replaceFirst.startsWith("base-")) {
                str = "config.";
            } else {
                replaceFirst = replaceFirst.replace("-", ".config.");
                str2 = ".config.master";
            }
            return replaceFirst.replace(str2, str);
        }
        throw new IllegalArgumentException("Non-apk found in splits directory.");
    }

    public static void c(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static <T> void d(T t, Object obj) {
        if (t == null) {
            throw new NullPointerException((String) obj);
        }
    }
}
