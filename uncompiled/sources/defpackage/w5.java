package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import defpackage.x5;
import zendesk.support.request.CellBase;

/* compiled from: Ac4Reader.java */
/* renamed from: w5  reason: default package */
/* loaded from: classes.dex */
public final class w5 implements ku0 {
    public final np2 a;
    public final op2 b;
    public final String c;
    public String d;
    public f84 e;
    public int f;
    public int g;
    public boolean h;
    public boolean i;
    public long j;
    public j k;
    public int l;
    public long m;

    public w5() {
        this(null);
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        ii.i(this.e);
        while (op2Var.a() > 0) {
            int i = this.f;
            if (i != 0) {
                if (i != 1) {
                    if (i == 2) {
                        int min = Math.min(op2Var.a(), this.l - this.g);
                        this.e.a(op2Var, min);
                        int i2 = this.g + min;
                        this.g = i2;
                        int i3 = this.l;
                        if (i2 == i3) {
                            long j = this.m;
                            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                                this.e.b(j, 1, i3, 0, null);
                                this.m += this.j;
                            }
                            this.f = 0;
                        }
                    }
                } else if (b(op2Var, this.b.d(), 16)) {
                    g();
                    this.b.P(0);
                    this.e.a(this.b, 16);
                    this.f = 2;
                }
            } else if (h(op2Var)) {
                this.f = 1;
                this.b.d()[0] = -84;
                this.b.d()[1] = (byte) (this.i ? 65 : 64);
                this.g = 2;
            }
        }
    }

    public final boolean b(op2 op2Var, byte[] bArr, int i) {
        int min = Math.min(op2Var.a(), i - this.g);
        op2Var.j(bArr, this.g, min);
        int i2 = this.g + min;
        this.g = i2;
        return i2 == i;
    }

    @Override // defpackage.ku0
    public void c() {
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.i = false;
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.m = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.d = dVar.b();
        this.e = r11Var.f(dVar.c(), 1);
    }

    public final void g() {
        this.a.p(0);
        x5.b d = x5.d(this.a);
        j jVar = this.k;
        if (jVar == null || d.b != jVar.C0 || d.a != jVar.D0 || !"audio/ac4".equals(jVar.p0)) {
            j E = new j.b().S(this.d).e0("audio/ac4").H(d.b).f0(d.a).V(this.c).E();
            this.k = E;
            this.e.f(E);
        }
        this.l = d.c;
        this.j = (d.d * 1000000) / this.k.D0;
    }

    public final boolean h(op2 op2Var) {
        int D;
        while (true) {
            if (op2Var.a() <= 0) {
                return false;
            }
            if (!this.h) {
                this.h = op2Var.D() == 172;
            } else {
                D = op2Var.D();
                this.h = D == 172;
                if (D == 64 || D == 65) {
                    break;
                }
            }
        }
        this.i = D == 65;
        return true;
    }

    public w5(String str) {
        np2 np2Var = new np2(new byte[16]);
        this.a = np2Var;
        this.b = new op2(np2Var.a);
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.i = false;
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.c = str;
    }
}
