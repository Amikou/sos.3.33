package defpackage;

/* compiled from: StateVerifier.java */
/* renamed from: et3  reason: default package */
/* loaded from: classes.dex */
public abstract class et3 {

    /* compiled from: StateVerifier.java */
    /* renamed from: et3$b */
    /* loaded from: classes.dex */
    public static class b extends et3 {
        public volatile boolean a;

        public b() {
            super();
        }

        @Override // defpackage.et3
        public void b(boolean z) {
            this.a = z;
        }

        @Override // defpackage.et3
        public void c() {
            if (this.a) {
                throw new IllegalStateException("Already released");
            }
        }
    }

    public static et3 a() {
        return new b();
    }

    public abstract void b(boolean z);

    public abstract void c();

    public et3() {
    }
}
