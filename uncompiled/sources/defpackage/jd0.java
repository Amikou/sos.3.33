package defpackage;

import java.util.ArrayList;
import kotlin.Pair;
import net.safemoon.androidwallet.R;

/* compiled from: SharedUtility.kt */
/* renamed from: jd0  reason: default package */
/* loaded from: classes2.dex */
public final class jd0 {
    public static final jd0 a = new jd0();
    public static final ArrayList<Pair<Integer, Integer>> b = b20.c(new Pair(Integer.valueOf((int) R.id.navigation_wallet), Integer.valueOf((int) R.string.default_screen_main_wallet)), new Pair(Integer.valueOf((int) R.id.myTokensListFragment), Integer.valueOf((int) R.string.default_screen_my_tokens)), new Pair(Integer.valueOf((int) R.id.navigation_swap), Integer.valueOf((int) R.string.default_screen_swap)));

    public final ArrayList<Pair<Integer, Integer>> a() {
        return b;
    }
}
