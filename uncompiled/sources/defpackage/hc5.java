package defpackage;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import com.google.android.gms.internal.measurement.i0;
import com.google.android.gms.internal.measurement.j;
import com.google.android.gms.internal.measurement.zzcl;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: hc5  reason: default package */
/* loaded from: classes.dex */
public final class hc5 extends i0 {
    public final /* synthetic */ String i0;
    public final /* synthetic */ String j0;
    public final /* synthetic */ Context k0;
    public final /* synthetic */ Bundle l0;
    public final /* synthetic */ wf5 m0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public hc5(wf5 wf5Var, String str, String str2, Context context, Bundle bundle) {
        super(wf5Var, true);
        this.m0 = wf5Var;
        this.i0 = str;
        this.j0 = str2;
        this.k0 = context;
        this.l0 = bundle;
    }

    @Override // com.google.android.gms.internal.measurement.i0
    public final void a() {
        boolean q;
        String str;
        String str2;
        String str3;
        j jVar;
        int b;
        j jVar2;
        String str4;
        String unused;
        try {
            wf5 wf5Var = this.m0;
            q = wf5.q(this.i0, this.j0);
            if (q) {
                String str5 = this.j0;
                String str6 = this.i0;
                str4 = this.m0.a;
                str2 = str6;
                str3 = str5;
                str = str4;
            } else {
                str = null;
                str2 = null;
                str3 = null;
            }
            zt2.j(this.k0);
            wf5 wf5Var2 = this.m0;
            wf5Var2.h = wf5Var2.t(this.k0, true);
            jVar = this.m0.h;
            if (jVar == null) {
                unused = this.m0.a;
                return;
            }
            int a = DynamiteModule.a(this.k0, ModuleDescriptor.MODULE_ID);
            zzcl zzclVar = new zzcl(42004L, Math.max(a, b), DynamiteModule.b(this.k0, ModuleDescriptor.MODULE_ID) < a, str, str2, str3, this.l0, sj5.a(this.k0));
            jVar2 = this.m0.h;
            ((j) zt2.j(jVar2)).initialize(nl2.H1(this.k0), zzclVar, this.a);
        } catch (Exception e) {
            this.m0.o(e, true, false);
        }
    }
}
