package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import java.io.FileDescriptor;

/* compiled from: WebpBitmapFactory.java */
/* renamed from: oo4  reason: default package */
/* loaded from: classes.dex */
public interface oo4 {

    /* compiled from: WebpBitmapFactory.java */
    /* renamed from: oo4$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(String str, String str2);
    }

    Bitmap a(FileDescriptor fileDescriptor, Rect rect, BitmapFactory.Options options);

    void b(tp tpVar);

    void c(a aVar);
}
