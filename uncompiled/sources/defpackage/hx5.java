package defpackage;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.a;

/* renamed from: hx5  reason: default package */
/* loaded from: classes.dex */
public final class hx5 extends a.AbstractC0106a<ze5, Object> {
    @Override // com.google.android.gms.common.api.a.AbstractC0106a
    public final /* synthetic */ ze5 d(Context context, Looper looper, kz kzVar, Object obj, GoogleApiClient.b bVar, GoogleApiClient.c cVar) {
        return new ze5(context, looper, kzVar, bVar, cVar);
    }
}
