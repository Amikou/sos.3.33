package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: kr5  reason: default package */
/* loaded from: classes.dex */
public final class kr5 extends br5<Boolean> implements RandomAccess, zv5 {
    public static final kr5 h0;
    public boolean[] f0;
    public int g0;

    static {
        kr5 kr5Var = new kr5(new boolean[0], 0);
        h0 = kr5Var;
        kr5Var.zzb();
    }

    public kr5() {
        this(new boolean[10], 0);
    }

    @Override // defpackage.zv5
    public final /* bridge */ /* synthetic */ zv5 Q(int i) {
        if (i >= this.g0) {
            return new kr5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ void add(int i, Object obj) {
        int i2;
        boolean booleanValue = ((Boolean) obj).booleanValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            boolean[] zArr = this.f0;
            if (i2 < zArr.length) {
                System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
            } else {
                boolean[] zArr2 = new boolean[((i2 * 3) / 2) + 1];
                System.arraycopy(zArr, 0, zArr2, 0, i);
                System.arraycopy(this.f0, i, zArr2, i + 1, this.g0 - i);
                this.f0 = zArr2;
            }
            this.f0[i] = booleanValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(m(i));
    }

    @Override // defpackage.br5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Boolean> collection) {
        e();
        cw5.a(collection);
        if (!(collection instanceof kr5)) {
            return super.addAll(collection);
        }
        kr5 kr5Var = (kr5) collection;
        int i = kr5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.f0;
            if (i3 > zArr.length) {
                this.f0 = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(kr5Var.f0, 0, this.f0, this.g0, kr5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kr5)) {
            return super.equals(obj);
        }
        kr5 kr5Var = (kr5) obj;
        if (this.g0 != kr5Var.g0) {
            return false;
        }
        boolean[] zArr = kr5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object get(int i) {
        k(i);
        return Boolean.valueOf(this.f0[i]);
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + cw5.f(this.f0[i2]);
        }
        return i;
    }

    public final void i(boolean z) {
        e();
        int i = this.g0;
        boolean[] zArr = this.f0;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[((i * 3) / 2) + 1];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.f0 = zArr2;
        }
        boolean[] zArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        zArr3[i2] = z;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Boolean) {
            boolean booleanValue = ((Boolean) obj).booleanValue();
            int i = this.g0;
            for (int i2 = 0; i2 < i; i2++) {
                if (this.f0[i2] == booleanValue) {
                    return i2;
                }
            }
            return -1;
        }
        return -1;
    }

    public final void k(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(m(i));
        }
    }

    public final String m(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object remove(int i) {
        int i2;
        e();
        k(i);
        boolean[] zArr = this.f0;
        boolean z = zArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            boolean[] zArr = this.f0;
            System.arraycopy(zArr, i2, zArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        e();
        k(i);
        boolean[] zArr = this.f0;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    public kr5(boolean[] zArr, int i) {
        this.f0 = zArr;
        this.g0 = i;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* bridge */ /* synthetic */ boolean add(Object obj) {
        i(((Boolean) obj).booleanValue());
        return true;
    }
}
