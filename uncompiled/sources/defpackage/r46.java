package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: r46  reason: default package */
/* loaded from: classes.dex */
public final class r46 extends n36<Bundle> {
    public r46(int i, int i2, Bundle bundle) {
        super(i, 1, bundle);
    }

    @Override // defpackage.n36
    public final void a(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("data");
        if (bundle2 == null) {
            bundle2 = Bundle.EMPTY;
        }
        c(bundle2);
    }

    @Override // defpackage.n36
    public final boolean d() {
        return false;
    }
}
