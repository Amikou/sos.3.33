package defpackage;

/* compiled from: InputMergerFactory.java */
/* renamed from: zq1  reason: default package */
/* loaded from: classes.dex */
public abstract class zq1 {

    /* compiled from: InputMergerFactory.java */
    /* renamed from: zq1$a */
    /* loaded from: classes.dex */
    public class a extends zq1 {
        @Override // defpackage.zq1
        public yq1 a(String str) {
            return null;
        }
    }

    public static zq1 c() {
        return new a();
    }

    public abstract yq1 a(String str);

    public final yq1 b(String str) {
        yq1 a2 = a(str);
        return a2 == null ? yq1.a(str) : a2;
    }
}
