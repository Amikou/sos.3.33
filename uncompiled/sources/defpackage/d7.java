package defpackage;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: ActivityFragmentLifecycle.java */
/* renamed from: d7  reason: default package */
/* loaded from: classes.dex */
public class d7 implements kz1 {
    public final Set<pz1> a = Collections.newSetFromMap(new WeakHashMap());
    public boolean b;
    public boolean c;

    @Override // defpackage.kz1
    public void a(pz1 pz1Var) {
        this.a.remove(pz1Var);
    }

    @Override // defpackage.kz1
    public void b(pz1 pz1Var) {
        this.a.add(pz1Var);
        if (this.c) {
            pz1Var.e();
        } else if (this.b) {
            pz1Var.b();
        } else {
            pz1Var.h();
        }
    }

    public void c() {
        this.c = true;
        for (pz1 pz1Var : mg4.j(this.a)) {
            pz1Var.e();
        }
    }

    public void d() {
        this.b = true;
        for (pz1 pz1Var : mg4.j(this.a)) {
            pz1Var.b();
        }
    }

    public void e() {
        this.b = false;
        for (pz1 pz1Var : mg4.j(this.a)) {
            pz1Var.h();
        }
    }
}
