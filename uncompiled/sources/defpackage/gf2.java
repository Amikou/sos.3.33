package defpackage;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;

/* compiled from: NetworkUnmeteredController.java */
/* renamed from: gf2  reason: default package */
/* loaded from: classes.dex */
public class gf2 extends d60<cf2> {
    public gf2(Context context, q34 q34Var) {
        super(j84.c(context, q34Var).d());
    }

    @Override // defpackage.d60
    public boolean b(tq4 tq4Var) {
        return tq4Var.j.b() == NetworkType.UNMETERED || (Build.VERSION.SDK_INT >= 30 && tq4Var.j.b() == NetworkType.TEMPORARILY_UNMETERED);
    }

    @Override // defpackage.d60
    /* renamed from: i */
    public boolean c(cf2 cf2Var) {
        return !cf2Var.a() || cf2Var.b();
    }
}
