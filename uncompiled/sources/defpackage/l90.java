package defpackage;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: l90  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class l90 implements FilenameFilter {
    public static final /* synthetic */ l90 a = new l90();

    @Override // java.io.FilenameFilter
    public final boolean accept(File file, String str) {
        boolean startsWith;
        startsWith = str.startsWith(".ae");
        return startsWith;
    }
}
