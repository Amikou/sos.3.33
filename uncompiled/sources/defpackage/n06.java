package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: n06  reason: default package */
/* loaded from: classes.dex */
public final class n06 implements yp5<o06> {
    public static final n06 f0 = new n06();
    public final yp5<o06> a = gq5.a(gq5.b(new p06()));

    public static long A() {
        return f0.zza().d();
    }

    public static long B() {
        return f0.zza().j();
    }

    public static long C() {
        return f0.zza().e();
    }

    public static long D() {
        return f0.zza().D();
    }

    public static long E() {
        return f0.zza().m();
    }

    public static long F() {
        return f0.zza().u();
    }

    public static long G() {
        return f0.zza().v();
    }

    public static long H() {
        return f0.zza().s();
    }

    public static long I() {
        return f0.zza().y();
    }

    public static long J() {
        return f0.zza().o();
    }

    public static long a() {
        return f0.zza().p();
    }

    public static long b() {
        return f0.zza().F();
    }

    public static long c() {
        return f0.zza().A();
    }

    public static long d() {
        return f0.zza().B();
    }

    public static long e() {
        return f0.zza().t();
    }

    public static long f() {
        return f0.zza().x();
    }

    public static long g() {
        return f0.zza().n();
    }

    public static long h() {
        return f0.zza().q();
    }

    public static String i() {
        return f0.zza().C();
    }

    public static long j() {
        return f0.zza().E();
    }

    public static long l() {
        return f0.zza().zza();
    }

    public static long m() {
        return f0.zza().zzb();
    }

    public static long n() {
        return f0.zza().zzc();
    }

    public static String o() {
        return f0.zza().b();
    }

    public static String p() {
        return f0.zza().c();
    }

    public static long q() {
        return f0.zza().i();
    }

    public static long r() {
        return f0.zza().h();
    }

    public static long s() {
        return f0.zza().f();
    }

    public static long t() {
        return f0.zza().g();
    }

    public static long u() {
        return f0.zza().l();
    }

    public static long v() {
        return f0.zza().a();
    }

    public static long w() {
        return f0.zza().z();
    }

    public static long x() {
        return f0.zza().k();
    }

    public static long y() {
        return f0.zza().r();
    }

    public static long z() {
        return f0.zza().w();
    }

    @Override // defpackage.yp5
    /* renamed from: k */
    public final o06 zza() {
        return this.a.zza();
    }
}
