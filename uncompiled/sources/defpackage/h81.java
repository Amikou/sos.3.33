package defpackage;

import android.net.Uri;
import androidx.media3.extractor.flv.b;
import java.util.Map;

/* renamed from: h81  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class h81 implements u11 {
    public static final /* synthetic */ h81 b = new h81();

    @Override // defpackage.u11
    public final p11[] a() {
        p11[] h;
        h = b.h();
        return h;
    }

    @Override // defpackage.u11
    public /* synthetic */ p11[] b(Uri uri, Map map) {
        return t11.a(this, uri, map);
    }
}
