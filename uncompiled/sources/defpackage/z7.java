package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivitySplashBinding.java */
/* renamed from: z7  reason: default package */
/* loaded from: classes2.dex */
public final class z7 {
    public final ConstraintLayout a;
    public final AppCompatImageView b;
    public final AppCompatImageView c;
    public final AppCompatTextView d;
    public final AppCompatTextView e;
    public final AppCompatTextView f;
    public final AppCompatTextView g;

    public z7(ConstraintLayout constraintLayout, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, AppCompatTextView appCompatTextView3, AppCompatTextView appCompatTextView4) {
        this.a = constraintLayout;
        this.b = appCompatImageView;
        this.c = appCompatImageView2;
        this.d = appCompatTextView;
        this.e = appCompatTextView2;
        this.f = appCompatTextView3;
        this.g = appCompatTextView4;
    }

    public static z7 a(View view) {
        int i = R.id.img_logo;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
        if (appCompatImageView != null) {
            i = R.id.img_shield_logo;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.img_shield_logo);
            if (appCompatImageView2 != null) {
                i = R.id.tv_protect_content;
                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.tv_protect_content);
                if (appCompatTextView != null) {
                    i = R.id.tv_shied_content;
                    AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.tv_shied_content);
                    if (appCompatTextView2 != null) {
                        i = R.id.tv_shied_tm_content;
                        AppCompatTextView appCompatTextView3 = (AppCompatTextView) ai4.a(view, R.id.tv_shied_tm_content);
                        if (appCompatTextView3 != null) {
                            i = R.id.tv_version;
                            AppCompatTextView appCompatTextView4 = (AppCompatTextView) ai4.a(view, R.id.tv_version);
                            if (appCompatTextView4 != null) {
                                return new z7((ConstraintLayout) view, appCompatImageView, appCompatImageView2, appCompatTextView, appCompatTextView2, appCompatTextView3, appCompatTextView4);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static z7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static z7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_splash, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
