package defpackage;

import androidx.media3.common.util.b;
import defpackage.lp;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: TsBinarySearchSeeker.java */
/* renamed from: cc4  reason: default package */
/* loaded from: classes.dex */
public final class cc4 extends lp {

    /* compiled from: TsBinarySearchSeeker.java */
    /* renamed from: cc4$a */
    /* loaded from: classes.dex */
    public static final class a implements lp.f {
        public final h64 a;
        public final op2 b = new op2();
        public final int c;
        public final int d;

        public a(int i, h64 h64Var, int i2) {
            this.c = i;
            this.a = h64Var;
            this.d = i2;
        }

        @Override // defpackage.lp.f
        public lp.e a(q11 q11Var, long j) throws IOException {
            long position = q11Var.getPosition();
            int min = (int) Math.min(this.d, q11Var.getLength() - position);
            this.b.L(min);
            q11Var.n(this.b.d(), 0, min);
            return c(this.b, j, position);
        }

        @Override // defpackage.lp.f
        public void b() {
            this.b.M(b.f);
        }

        public final lp.e c(op2 op2Var, long j, long j2) {
            int a;
            int a2;
            int f = op2Var.f();
            long j3 = -1;
            long j4 = -1;
            long j5 = -9223372036854775807L;
            while (op2Var.a() >= 188 && (a2 = (a = hc4.a(op2Var.d(), op2Var.e(), f)) + 188) <= f) {
                long c = hc4.c(op2Var, a, this.c);
                if (c != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    long b = this.a.b(c);
                    if (b > j) {
                        if (j5 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                            return lp.e.d(b, j2);
                        }
                        return lp.e.e(j2 + j4);
                    } else if (100000 + b > j) {
                        return lp.e.e(j2 + a);
                    } else {
                        j4 = a;
                        j5 = b;
                    }
                }
                op2Var.P(a2);
                j3 = a2;
            }
            if (j5 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return lp.e.f(j5, j2 + j3);
            }
            return lp.e.d;
        }
    }

    public cc4(h64 h64Var, long j, long j2, int i, int i2) {
        super(new lp.b(), new a(i, h64Var, i2), j, 0L, j + 1, 0L, j2, 188L, 940);
    }
}
