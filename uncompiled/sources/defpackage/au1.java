package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: au1  reason: default package */
/* loaded from: classes2.dex */
public abstract class au1 extends v30 implements yp0, dq1 {
    public bu1 h0;

    public final void A(bu1 bu1Var) {
        this.h0 = bu1Var;
    }

    @Override // defpackage.yp0
    public void a() {
        z().v0(this);
    }

    @Override // defpackage.dq1
    public boolean b() {
        return true;
    }

    @Override // defpackage.dq1
    public tg2 e() {
        return null;
    }

    @Override // defpackage.l12
    public String toString() {
        return ff0.a(this) + '@' + ff0.b(this) + "[job@" + ff0.b(z()) + ']';
    }

    public final bu1 z() {
        bu1 bu1Var = this.h0;
        if (bu1Var != null) {
            return bu1Var;
        }
        fs1.r("job");
        throw null;
    }
}
