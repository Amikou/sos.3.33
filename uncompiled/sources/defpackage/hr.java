package defpackage;

/* compiled from: boxing.kt */
/* renamed from: hr  reason: default package */
/* loaded from: classes2.dex */
public final class hr {
    public static final Boolean a(boolean z) {
        return Boolean.valueOf(z);
    }

    public static final Double b(double d) {
        return new Double(d);
    }

    public static final Float c(float f) {
        return new Float(f);
    }

    public static final Integer d(int i) {
        return new Integer(i);
    }

    public static final Long e(long j) {
        return new Long(j);
    }
}
