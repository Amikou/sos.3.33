package defpackage;

/* renamed from: i13  reason: default package */
/* loaded from: classes2.dex */
public final class i13 {
    public static final int abc_action_bar_home_description = 2131951624;
    public static final int abc_action_bar_up_description = 2131951625;
    public static final int abc_action_menu_overflow_description = 2131951626;
    public static final int abc_action_mode_done = 2131951627;
    public static final int abc_activity_chooser_view_see_all = 2131951628;
    public static final int abc_activitychooserview_choose_application = 2131951629;
    public static final int abc_capital_off = 2131951630;
    public static final int abc_capital_on = 2131951631;
    public static final int abc_menu_alt_shortcut_label = 2131951632;
    public static final int abc_menu_ctrl_shortcut_label = 2131951633;
    public static final int abc_menu_delete_shortcut_label = 2131951634;
    public static final int abc_menu_enter_shortcut_label = 2131951635;
    public static final int abc_menu_function_shortcut_label = 2131951636;
    public static final int abc_menu_meta_shortcut_label = 2131951637;
    public static final int abc_menu_shift_shortcut_label = 2131951638;
    public static final int abc_menu_space_shortcut_label = 2131951639;
    public static final int abc_menu_sym_shortcut_label = 2131951640;
    public static final int abc_prepend_shortcut_label = 2131951641;
    public static final int abc_search_hint = 2131951642;
    public static final int abc_searchview_description_clear = 2131951643;
    public static final int abc_searchview_description_query = 2131951644;
    public static final int abc_searchview_description_search = 2131951645;
    public static final int abc_searchview_description_submit = 2131951646;
    public static final int abc_searchview_description_voice = 2131951647;
    public static final int abc_shareactionprovider_share_with = 2131951648;
    public static final int abc_shareactionprovider_share_with_application = 2131951649;
    public static final int abc_toolbar_collapse_description = 2131951650;
    public static final int app_name = 2131951796;
    public static final int appbar_scrolling_view_behavior = 2131951797;
    public static final int articles_list_fragment_error_message = 2131951803;
    public static final int articles_list_fragment_no_articles_found = 2131951804;
    public static final int ask_request_list_failed_request_message = 2131951805;
    public static final int belvedere_dialog_camera = 2131951843;
    public static final int belvedere_dialog_gallery = 2131951844;
    public static final int belvedere_fam_desc_collapse_fam = 2131951845;
    public static final int belvedere_fam_desc_expand_fam = 2131951846;
    public static final int belvedere_fam_desc_open_gallery = 2131951847;
    public static final int belvedere_fam_desc_open_google_photos = 2131951848;
    public static final int belvedere_image_stream_file_too_large = 2131951849;
    public static final int belvedere_image_stream_title = 2131951850;
    public static final int belvedere_image_stream_unknown_app = 2131951851;
    public static final int belvedere_navigate_to_settings = 2131951852;
    public static final int belvedere_permissions_denied = 2131951853;
    public static final int belvedere_permissions_rationale = 2131951854;
    public static final int belvedere_sdk_fpa_suffix_v2 = 2131951855;
    public static final int belvedere_stream_item_camera_tile_desc = 2131951856;
    public static final int belvedere_stream_item_select_file_desc = 2131951857;
    public static final int belvedere_stream_item_select_image_desc = 2131951858;
    public static final int belvedere_stream_item_unselect_file_desc = 2131951859;
    public static final int belvedere_stream_item_unselect_image_desc = 2131951860;
    public static final int belvedere_toolbar_desc_collapse = 2131951861;
    public static final int bottom_sheet_behavior = 2131951872;
    public static final int bottomsheet_action_expand_halfway = 2131951873;
    public static final int categories_list_fragment_error_message = 2131951898;
    public static final int character_counter_content_description = 2131951899;
    public static final int character_counter_overflowed_content_description = 2131951900;
    public static final int character_counter_pattern = 2131951901;
    public static final int chip_text = 2131951909;
    public static final int clear_text_end_icon_content_description = 2131951911;
    public static final int contact_fragment_title = 2131951973;
    public static final int error_icon_content_description = 2131952056;
    public static final int error_msg_invalid_email = 2131952061;
    public static final int error_msg_invalid_message = 2131952062;
    public static final int error_msg_invalid_name = 2131952063;
    public static final int exposed_dropdown_menu_content_description = 2131952120;
    public static final int fab_transformation_scrim_behavior = 2131952121;
    public static final int fab_transformation_sheet_behavior = 2131952122;
    public static final int help_search_no_results_label = 2131952157;
    public static final int help_search_subtitle_format = 2131952158;
    public static final int help_see_all_articles_label = 2131952159;
    public static final int help_see_all_n_articles_label = 2131952160;
    public static final int hide_bottom_view_on_scroll_behavior = 2131952161;
    public static final int icon_content_description = 2131952169;
    public static final int item_view_role_description = 2131952180;
    public static final int material_clock_display_divider = 2131952240;
    public static final int material_clock_toggle_content_description = 2131952241;
    public static final int material_hour_selection = 2131952242;
    public static final int material_hour_suffix = 2131952243;
    public static final int material_minute_selection = 2131952244;
    public static final int material_minute_suffix = 2131952245;
    public static final int material_motion_easing_accelerated = 2131952246;
    public static final int material_motion_easing_decelerated = 2131952247;
    public static final int material_motion_easing_emphasized = 2131952248;
    public static final int material_motion_easing_linear = 2131952249;
    public static final int material_motion_easing_standard = 2131952250;
    public static final int material_slider_range_end = 2131952251;
    public static final int material_slider_range_start = 2131952252;
    public static final int material_timepicker_am = 2131952253;
    public static final int material_timepicker_clock_mode_description = 2131952254;
    public static final int material_timepicker_hour = 2131952255;
    public static final int material_timepicker_minute = 2131952256;
    public static final int material_timepicker_pm = 2131952257;
    public static final int material_timepicker_select_time = 2131952258;
    public static final int material_timepicker_text_input_mode_description = 2131952259;
    public static final int mtrl_badge_numberless_content_description = 2131952288;
    public static final int mtrl_chip_close_icon_content_description = 2131952289;
    public static final int mtrl_exceed_max_badge_number_content_description = 2131952290;
    public static final int mtrl_exceed_max_badge_number_suffix = 2131952291;
    public static final int mtrl_picker_a11y_next_month = 2131952292;
    public static final int mtrl_picker_a11y_prev_month = 2131952293;
    public static final int mtrl_picker_announce_current_selection = 2131952294;
    public static final int mtrl_picker_cancel = 2131952295;
    public static final int mtrl_picker_confirm = 2131952296;
    public static final int mtrl_picker_date_header_selected = 2131952297;
    public static final int mtrl_picker_date_header_title = 2131952298;
    public static final int mtrl_picker_date_header_unselected = 2131952299;
    public static final int mtrl_picker_day_of_week_column_header = 2131952300;
    public static final int mtrl_picker_invalid_format = 2131952301;
    public static final int mtrl_picker_invalid_format_example = 2131952302;
    public static final int mtrl_picker_invalid_format_use = 2131952303;
    public static final int mtrl_picker_invalid_range = 2131952304;
    public static final int mtrl_picker_navigate_to_year_description = 2131952305;
    public static final int mtrl_picker_out_of_range = 2131952306;
    public static final int mtrl_picker_range_header_only_end_selected = 2131952307;
    public static final int mtrl_picker_range_header_only_start_selected = 2131952308;
    public static final int mtrl_picker_range_header_selected = 2131952309;
    public static final int mtrl_picker_range_header_title = 2131952310;
    public static final int mtrl_picker_range_header_unselected = 2131952311;
    public static final int mtrl_picker_save = 2131952312;
    public static final int mtrl_picker_text_input_date_hint = 2131952313;
    public static final int mtrl_picker_text_input_date_range_end_hint = 2131952314;
    public static final int mtrl_picker_text_input_date_range_start_hint = 2131952315;
    public static final int mtrl_picker_text_input_day_abbr = 2131952316;
    public static final int mtrl_picker_text_input_month_abbr = 2131952317;
    public static final int mtrl_picker_text_input_year_abbr = 2131952318;
    public static final int mtrl_picker_toggle_to_calendar_input_mode = 2131952319;
    public static final int mtrl_picker_toggle_to_day_selection = 2131952320;
    public static final int mtrl_picker_toggle_to_text_input_mode = 2131952321;
    public static final int mtrl_picker_toggle_to_year_selection = 2131952322;
    public static final int network_activity_no_connectivity = 2131952334;
    public static final int password_toggle_content_description = 2131952365;
    public static final int path_password_eye = 2131952367;
    public static final int path_password_eye_mask_strike_through = 2131952368;
    public static final int path_password_eye_mask_visible = 2131952369;
    public static final int path_password_strike_through = 2131952370;
    public static final int request_activity_title = 2131952430;
    public static final int request_attachment_generic_unknown_app = 2131952431;
    public static final int request_dialog_body_unsaved_changes = 2131952432;
    public static final int request_dialog_button_label_cancel = 2131952433;
    public static final int request_dialog_button_label_delete = 2131952434;
    public static final int request_dialog_title_unsaved_changes = 2131952435;
    public static final int request_email_entry_hint = 2131952436;
    public static final int request_error_create_request = 2131952437;
    public static final int request_error_load_comments = 2131952438;
    public static final int request_file_attachment_download_in_progress = 2131952439;
    public static final int request_list_activity_title = 2131952440;
    public static final int request_list_empty_start_conversation = 2131952441;
    public static final int request_list_empty_text = 2131952442;
    public static final int request_list_error_message = 2131952443;
    public static final int request_list_fragment_error_message = 2131952444;
    public static final int request_list_me = 2131952445;
    public static final int request_list_re = 2131952446;
    public static final int request_list_ticket_closed = 2131952447;
    public static final int request_menu_button_label_add_attachments = 2131952448;
    public static final int request_menu_button_label_send = 2131952449;
    public static final int request_message_date_today = 2131952450;
    public static final int request_message_date_yesterday = 2131952451;
    public static final int request_message_entry_hint = 2131952452;
    public static final int request_message_inline_image_title_format = 2131952453;
    public static final int request_message_status_delivered = 2131952454;
    public static final int request_messages_status_error = 2131952455;
    public static final int request_name_entry_hint = 2131952456;
    public static final int request_retry_dialog_delete_message = 2131952457;
    public static final int request_retry_dialog_retry = 2131952458;
    public static final int request_system_message_closed_ticket = 2131952459;
    public static final int request_toolbar_last_reply = 2131952460;
    public static final int request_write_a_message = 2131952461;
    public static final int retry_view_button_label = 2131952466;
    public static final int search_menu_title = 2131952492;
    public static final int sections_list_fragment_error_message = 2131952493;
    public static final int status_bar_notification_info_overflow = 2131952548;
    public static final int support_activity_title = 2131952554;
    public static final int support_articles_list_fragment_error_message = 2131952555;
    public static final int support_articles_list_fragment_no_articles_found = 2131952556;
    public static final int support_categories_list_fragment_error_message = 2131952557;
    public static final int support_conversations_menu = 2131952558;
    public static final int support_help_search_no_results_label = 2131952559;
    public static final int support_help_see_all_articles_label = 2131952560;
    public static final int support_help_see_all_n_articles_label = 2131952561;
    public static final int support_sections_list_fragment_error_message = 2131952562;
    public static final int view_article_attachments_error = 2131952716;
    public static final int view_article_html_body = 2131952717;
    public static final int view_article_seperator = 2131952718;
    public static final int view_article_vote_prompt = 2131952719;
    public static final int zendesk_no_connectivity_error = 2131952773;
    public static final int zendesk_retry_button_label = 2131952774;
    public static final int zg_general_contact_us_button_label_accessibility = 2131952777;
    public static final int zg_general_no_connection_message = 2131952778;
    public static final int zs_engine_greeting_message = 2131952779;
    public static final int zs_engine_message_retry_button = 2131952780;
    public static final int zs_engine_message_send_error_message = 2131952781;
    public static final int zs_engine_request_created_conversations_enabled_message = 2131952782;
    public static final int zs_engine_request_created_conversations_off_message = 2131952783;
    public static final int zs_engine_request_created_request_list_button = 2131952784;
    public static final int zs_engine_request_creation_email_prompt_hint = 2131952785;
    public static final int zs_engine_request_creation_email_prompt_message = 2131952786;
    public static final int zs_engine_request_creation_email_validation_failed_message = 2131952787;
    public static final int zs_general_contact_us_button_label_accessibility = 2131952788;
    public static final int zs_general_referrer_logo_label_accessibility = 2131952789;
    public static final int zs_help_center_content_loaded_accessibility = 2131952790;
    public static final int zs_help_center_search_loaded_accessibility = 2131952791;
    public static final int zs_request_announce_comment_created_accessibility = 2131952792;
    public static final int zs_request_announce_comment_failed_accessibility = 2131952793;
    public static final int zs_request_announce_comments_loaded_accessibility = 2131952794;
    public static final int zs_request_attachment_carousel_attachment_accessibility = 2131952795;
    public static final int zs_request_attachment_carousel_remove_attachment_accessibility = 2131952796;
    public static final int zs_request_attachment_indicator_accessibility = 2131952797;
    public static final int zs_request_attachment_indicator_n_attachments_selected_accessibility = 2131952798;
    public static final int zs_request_attachment_indicator_no_attachments_selected_accessibility = 2131952799;
    public static final int zs_request_attachment_indicator_one_attachments_selected_accessibility = 2131952800;
    public static final int zs_request_contact_option_leave_a_message = 2131952801;
    public static final int zs_request_list_content_load_failed_accessibility = 2131952802;
    public static final int zs_request_list_content_loaded_accessibility = 2131952803;
    public static final int zs_request_list_content_loaded_empty_accessibility = 2131952804;
    public static final int zs_request_list_content_loading_accessibility = 2131952805;
    public static final int zs_request_message_agent_file_accessibility = 2131952806;
    public static final int zs_request_message_agent_image_accessibility = 2131952807;
    public static final int zs_request_message_agent_sent_accessibility = 2131952808;
    public static final int zs_request_message_agent_text_accessibility = 2131952809;
    public static final int zs_request_message_user_error_accessibility = 2131952810;
    public static final int zs_request_message_user_file_accessibility = 2131952811;
    public static final int zs_request_message_user_image_accessibility = 2131952812;
    public static final int zs_request_message_user_sent_accessibility = 2131952813;
    public static final int zs_request_message_user_text_accessibility = 2131952814;
    public static final int zs_request_toolbar_accessibility = 2131952815;
    public static final int zs_view_article_error = 2131952816;
    public static final int zs_view_article_loaded_accessibility = 2131952817;
    public static final int zs_view_article_loading_title = 2131952818;
    public static final int zs_view_article_vote_no_accessibility = 2131952819;
    public static final int zs_view_article_vote_no_remove_accessibility = 2131952820;
    public static final int zs_view_article_vote_yes_accessibility = 2131952821;
    public static final int zs_view_article_vote_yes_remove_accessibility = 2131952822;
    public static final int zs_view_article_voted_failed_accessibility_announce = 2131952823;
    public static final int zs_view_article_voted_no_accessibility_announce = 2131952824;
    public static final int zs_view_article_voted_yes_accessibility_announce = 2131952825;
    public static final int zui_attachment_indicator_accessibility = 2131952826;
    public static final int zui_attachment_indicator_n_attachments_selected_accessibility = 2131952827;
    public static final int zui_attachment_indicator_no_attachments_selected_accessibility = 2131952828;
    public static final int zui_attachment_indicator_one_attachments_selected_accessibility = 2131952829;
    public static final int zui_bot_label = 2131952830;
    public static final int zui_button_label_no = 2131952831;
    public static final int zui_button_label_yes = 2131952832;
    public static final int zui_cell_text_suggested_article_header = 2131952833;
    public static final int zui_cell_text_suggested_articles_header = 2131952834;
    public static final int zui_default_bot_name = 2131952835;
    public static final int zui_dialog_email_error = 2131952836;
    public static final int zui_dialog_email_hint = 2131952837;
    public static final int zui_failed_message_copy = 2131952838;
    public static final int zui_failed_message_delete = 2131952839;
    public static final int zui_failed_message_retry = 2131952840;
    public static final int zui_hint_type_message = 2131952841;
    public static final int zui_label_connecting = 2131952842;
    public static final int zui_label_dialog_delete_btn = 2131952843;
    public static final int zui_label_dialog_retry_btn = 2131952844;
    public static final int zui_label_failed = 2131952845;
    public static final int zui_label_reconnecting = 2131952846;
    public static final int zui_label_reconnecting_failed = 2131952847;
    public static final int zui_label_send = 2131952848;
    public static final int zui_label_sent = 2131952849;
    public static final int zui_label_tap_retry = 2131952850;
    public static final int zui_message_log_article_opened_formatter = 2131952851;
    public static final int zui_message_log_article_suggestion_message = 2131952852;
    public static final int zui_message_log_attachment_sending_failed = 2131952853;
    public static final int zui_message_log_default_visitor_name = 2131952854;
    public static final int zui_message_log_message_attachment_type_not_supported = 2131952855;
    public static final int zui_message_log_message_attachments_not_supported = 2131952856;
    public static final int zui_message_log_message_failed_to_send = 2131952857;
    public static final int zui_message_log_message_file_exceeds_max_size = 2131952858;
    public static final int zui_message_log_transfer_option_selection_formatter = 2131952859;
    public static final int zui_retry_button_label = 2131952860;
    public static final int zui_toolbar_title = 2131952861;
    public static final int zui_unable_open_file = 2131952862;
}
