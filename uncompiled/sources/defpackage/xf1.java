package defpackage;

/* compiled from: GifDrawableResource.java */
/* renamed from: xf1  reason: default package */
/* loaded from: classes.dex */
public class xf1 extends yq0<uf1> {
    public xf1(uf1 uf1Var) {
        super(uf1Var);
    }

    @Override // defpackage.s73
    public int a() {
        return ((uf1) this.a).i();
    }

    @Override // defpackage.s73
    public void b() {
        ((uf1) this.a).stop();
        ((uf1) this.a).k();
    }

    @Override // defpackage.yq0, defpackage.oq1
    public void c() {
        ((uf1) this.a).e().prepareToDraw();
    }

    @Override // defpackage.s73
    public Class<uf1> d() {
        return uf1.class;
    }
}
