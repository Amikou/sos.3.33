package defpackage;

import com.google.android.gms.internal.measurement.zzjd;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: or5  reason: default package */
/* loaded from: classes.dex */
public final class or5 extends tr5 {
    public int a = 0;
    public final int f0;
    public final /* synthetic */ zzjd g0;

    public or5(zzjd zzjdVar) {
        this.g0 = zzjdVar;
        this.f0 = zzjdVar.zzc();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a < this.f0;
    }

    @Override // defpackage.yr5
    public final byte zza() {
        int i = this.a;
        if (i < this.f0) {
            this.a = i + 1;
            return this.g0.zzb(i);
        }
        throw new NoSuchElementException();
    }
}
