package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: r25  reason: default package */
/* loaded from: classes.dex */
public final class r25 implements GoogleApiClient.b, GoogleApiClient.c {
    public final a<?> a;
    public final boolean b;
    public u25 c;

    public r25(a<?> aVar, boolean z) {
        this.a = aVar;
        this.b = z;
    }

    public final u25 a() {
        zt2.k(this.c, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
        return this.c;
    }

    public final void c(u25 u25Var) {
        this.c = u25Var;
    }

    @Override // defpackage.u50
    public final void onConnected(Bundle bundle) {
        a().onConnected(bundle);
    }

    @Override // defpackage.jm2
    public final void onConnectionFailed(ConnectionResult connectionResult) {
        a().b(connectionResult, this.a, this.b);
    }

    @Override // defpackage.u50
    public final void onConnectionSuspended(int i) {
        a().onConnectionSuspended(i);
    }
}
