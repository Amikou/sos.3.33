package defpackage;

import android.text.TextUtils;

/* compiled from: OSUtils.java */
/* renamed from: dl2  reason: default package */
/* loaded from: classes.dex */
public class dl2 {
    public static String a() {
        return c() ? b("ro.build.version.emui", "") : "";
    }

    public static String b(String str, String str2) {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class, String.class).invoke(cls, str, str2);
        } catch (Exception e) {
            e.printStackTrace();
            return str2;
        }
    }

    public static boolean c() {
        return !TextUtils.isEmpty(b("ro.build.version.emui", ""));
    }

    public static boolean d() {
        return a().contains("EmotionUI_3.0");
    }

    public static boolean e() {
        String a = a();
        return "EmotionUI 3".equals(a) || a.contains("EmotionUI_3.1");
    }

    public static boolean f() {
        return d() || e();
    }

    public static boolean g() {
        return !TextUtils.isEmpty(b("ro.miui.ui.version.name", ""));
    }
}
