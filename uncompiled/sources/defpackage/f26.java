package defpackage;

import com.google.android.gms.tasks.b;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.e;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: f26  reason: default package */
/* loaded from: classes.dex */
public final class f26<TResult, TContinuationResult> implements hm2, nm2, um2<TContinuationResult>, l46<TResult> {
    public final Executor a;
    public final b<TResult, TContinuationResult> b;
    public final e<TContinuationResult> c;

    public f26(Executor executor, b<TResult, TContinuationResult> bVar, e<TContinuationResult> eVar) {
        this.a = executor;
        this.b = bVar;
        this.c = eVar;
    }

    @Override // defpackage.um2
    public final void a(TContinuationResult tcontinuationresult) {
        this.c.t(tcontinuationresult);
    }

    @Override // defpackage.nm2
    public final void b(Exception exc) {
        this.c.s(exc);
    }

    @Override // defpackage.l46
    public final void c(c<TResult> cVar) {
        this.a.execute(new c16(this, cVar));
    }

    @Override // defpackage.hm2
    public final void d() {
        this.c.u();
    }
}
