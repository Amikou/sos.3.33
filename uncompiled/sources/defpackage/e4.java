package defpackage;

import org.bouncycastle.asn1.k;

/* renamed from: e4  reason: default package */
/* loaded from: classes2.dex */
public abstract class e4 extends k {
    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        return -1;
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        return kVar instanceof e4;
    }

    public String toString() {
        return "NULL";
    }
}
