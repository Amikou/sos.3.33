package defpackage;

import androidx.paging.LoadType;
import defpackage.w02;
import java.util.Objects;

/* compiled from: CombinedLoadStates.kt */
/* renamed from: a30  reason: default package */
/* loaded from: classes.dex */
public final class a30 {
    public final w02 a;
    public final w02 b;
    public final w02 c;
    public final x02 d;
    public final x02 e;

    /* compiled from: CombinedLoadStates.kt */
    /* renamed from: a30$a */
    /* loaded from: classes.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        w02.c.a aVar = w02.c.d;
        new a30(aVar.b(), aVar.b(), aVar.b(), x02.e.a(), null, 16, null);
    }

    public a30(w02 w02Var, w02 w02Var2, w02 w02Var3, x02 x02Var, x02 x02Var2) {
        fs1.f(w02Var, "refresh");
        fs1.f(w02Var2, "prepend");
        fs1.f(w02Var3, "append");
        fs1.f(x02Var, "source");
        this.a = w02Var;
        this.b = w02Var2;
        this.c = w02Var3;
        this.d = x02Var;
        this.e = x02Var2;
    }

    public final void a(kd1<? super LoadType, ? super Boolean, ? super w02, te4> kd1Var) {
        fs1.f(kd1Var, "op");
        x02 x02Var = this.d;
        LoadType loadType = LoadType.REFRESH;
        w02 g = x02Var.g();
        Boolean bool = Boolean.FALSE;
        kd1Var.invoke(loadType, bool, g);
        LoadType loadType2 = LoadType.PREPEND;
        kd1Var.invoke(loadType2, bool, x02Var.f());
        LoadType loadType3 = LoadType.APPEND;
        kd1Var.invoke(loadType3, bool, x02Var.e());
        x02 x02Var2 = this.e;
        if (x02Var2 != null) {
            w02 g2 = x02Var2.g();
            Boolean bool2 = Boolean.TRUE;
            kd1Var.invoke(loadType, bool2, g2);
            kd1Var.invoke(loadType2, bool2, x02Var2.f());
            kd1Var.invoke(loadType3, bool2, x02Var2.e());
        }
    }

    public final w02 b() {
        return this.c;
    }

    public final x02 c() {
        return this.e;
    }

    public final w02 d() {
        return this.b;
    }

    public final w02 e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!fs1.b(a30.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type androidx.paging.CombinedLoadStates");
        a30 a30Var = (a30) obj;
        return ((fs1.b(this.a, a30Var.a) ^ true) || (fs1.b(this.b, a30Var.b) ^ true) || (fs1.b(this.c, a30Var.c) ^ true) || (fs1.b(this.d, a30Var.d) ^ true) || (fs1.b(this.e, a30Var.e) ^ true)) ? false : true;
    }

    public final x02 f() {
        return this.d;
    }

    public int hashCode() {
        int hashCode = ((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31;
        x02 x02Var = this.e;
        return hashCode + (x02Var != null ? x02Var.hashCode() : 0);
    }

    public String toString() {
        return "CombinedLoadStates(refresh=" + this.a + ", prepend=" + this.b + ", append=" + this.c + ", source=" + this.d + ", mediator=" + this.e + ')';
    }

    public /* synthetic */ a30(w02 w02Var, w02 w02Var2, w02 w02Var3, x02 x02Var, x02 x02Var2, int i, qi0 qi0Var) {
        this(w02Var, w02Var2, w02Var3, x02Var, (i & 16) != 0 ? null : x02Var2);
    }
}
