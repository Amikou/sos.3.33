package defpackage;

import com.google.android.gms.measurement.internal.d;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: xs5  reason: default package */
/* loaded from: classes.dex */
public final class xs5 implements Runnable {
    public final /* synthetic */ d a;
    public final /* synthetic */ dt5 f0;

    public xs5(dt5 dt5Var, d dVar) {
        this.f0 = dt5Var;
        this.a = dVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        synchronized (this.f0) {
            this.f0.a = false;
            if (!this.f0.g0.H()) {
                this.f0.g0.a.w().u().a("Connected to remote service");
                this.f0.g0.r(this.a);
            }
        }
    }
}
