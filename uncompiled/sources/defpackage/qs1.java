package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: ItemAddNewTokenBinding.java */
/* renamed from: qs1  reason: default package */
/* loaded from: classes2.dex */
public final class qs1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialCheckBox c;
    public final ImageView d;
    public final MaterialCardView e;
    public final MaterialCardView f;
    public final TextView g;

    public qs1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialCheckBox materialCheckBox, ImageView imageView, MaterialCardView materialCardView, MaterialCardView materialCardView2, TextView textView) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialCheckBox;
        this.d = imageView;
        this.e = materialCardView;
        this.f = materialCardView2;
        this.g = textView;
    }

    public static qs1 a(View view) {
        int i = R.id.btnDelete;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDelete);
        if (materialButton != null) {
            i = R.id.cbAddToken;
            MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbAddToken);
            if (materialCheckBox != null) {
                i = R.id.ivTokenIcon;
                ImageView imageView = (ImageView) ai4.a(view, R.id.ivTokenIcon);
                if (imageView != null) {
                    i = R.id.rowBG;
                    MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.rowBG);
                    if (materialCardView != null) {
                        i = R.id.rowFG;
                        MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.rowFG);
                        if (materialCardView2 != null) {
                            i = R.id.tvTokenNameAndSymbol;
                            TextView textView = (TextView) ai4.a(view, R.id.tvTokenNameAndSymbol);
                            if (textView != null) {
                                return new qs1((ConstraintLayout) view, materialButton, materialCheckBox, imageView, materialCardView, materialCardView2, textView);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static qs1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_add_new_token, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
