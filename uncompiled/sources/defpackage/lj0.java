package defpackage;

import android.util.SparseIntArray;

/* compiled from: DefaultFlexByteArrayPoolParams.java */
/* renamed from: lj0  reason: default package */
/* loaded from: classes.dex */
public class lj0 {
    public static final int a = Runtime.getRuntime().availableProcessors();

    public static SparseIntArray a(int i, int i2, int i3) {
        SparseIntArray sparseIntArray = new SparseIntArray();
        while (i <= i2) {
            sparseIntArray.put(i, i3);
            i *= 2;
        }
        return sparseIntArray;
    }

    public static ys2 b() {
        int i = a;
        return new ys2(4194304, i * 4194304, a(131072, 4194304, i), 131072, 4194304, i);
    }
}
