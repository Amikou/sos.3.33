package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.AKTImportPrivateKeyActivity;

/* renamed from: z0  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class z0 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ z0 a = new z0();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        AKTImportPrivateKeyActivity.y0(dialogInterface);
    }
}
