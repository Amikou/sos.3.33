package defpackage;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: zp5  reason: default package */
/* loaded from: classes.dex */
public final class zp5 {
    public static String a(Context context, String str, String str2) {
        zt2.j(context);
        Resources resources = context.getResources();
        if (TextUtils.isEmpty(str2)) {
            str2 = sj5.a(context);
        }
        return sj5.b("google_app_id", resources, str2);
    }

    public static String b(String str, String[] strArr, String[] strArr2) {
        zt2.j(strArr);
        zt2.j(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            String str2 = strArr[i];
            if ((str == null && str2 == null) || (str != null && str.equals(str2))) {
                return strArr2[i];
            }
        }
        return null;
    }
}
