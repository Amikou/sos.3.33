package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: Continuation.kt */
/* renamed from: q70  reason: default package */
/* loaded from: classes2.dex */
public interface q70<T> {
    CoroutineContext getContext();

    void resumeWith(Object obj);
}
