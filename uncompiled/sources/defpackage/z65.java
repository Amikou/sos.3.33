package defpackage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: z65  reason: default package */
/* loaded from: classes.dex */
public interface z65 {
    ScheduledExecutorService a(int i, ThreadFactory threadFactory, int i2);

    ExecutorService b(ThreadFactory threadFactory, int i);
}
