package defpackage;

import defpackage.qr1;

/* compiled from: _Ranges.kt */
/* renamed from: u33  reason: default package */
/* loaded from: classes2.dex */
public class u33 extends t33 {
    public static final int b(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    public static final long c(long j, long j2) {
        return j < j2 ? j2 : j;
    }

    public static final int d(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    public static final long e(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    public static final int f(int i, int i2, int i3) {
        if (i2 <= i3) {
            return i < i2 ? i2 : i > i3 ? i3 : i;
        }
        throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
    }

    public static final int g(int i, f00<Integer> f00Var) {
        fs1.f(f00Var, "range");
        if (f00Var instanceof e00) {
            return ((Number) h(Integer.valueOf(i), (e00) f00Var)).intValue();
        }
        if (!f00Var.isEmpty()) {
            return i < f00Var.i().intValue() ? f00Var.i().intValue() : i > f00Var.k().intValue() ? f00Var.k().intValue() : i;
        }
        throw new IllegalArgumentException("Cannot coerce value to an empty range: " + f00Var + '.');
    }

    public static final <T extends Comparable<? super T>> T h(T t, e00<T> e00Var) {
        fs1.f(t, "$this$coerceIn");
        fs1.f(e00Var, "range");
        if (!e00Var.isEmpty()) {
            return (!e00Var.e(t, e00Var.i()) || e00Var.e(e00Var.i(), t)) ? (!e00Var.e(e00Var.k(), t) || e00Var.e(t, e00Var.k())) ? t : e00Var.k() : e00Var.i();
        }
        throw new IllegalArgumentException("Cannot coerce value to an empty range: " + e00Var + '.');
    }

    public static final qr1 i(int i, int i2) {
        return qr1.h0.a(i, i2, -1);
    }

    public static final qr1 j(qr1 qr1Var, int i) {
        fs1.f(qr1Var, "$this$step");
        t33.a(i > 0, Integer.valueOf(i));
        qr1.a aVar = qr1.h0;
        int m = qr1Var.m();
        int n = qr1Var.n();
        if (qr1Var.o() <= 0) {
            i = -i;
        }
        return aVar.a(m, n, i);
    }

    public static final sr1 k(int i, int i2) {
        if (i2 <= Integer.MIN_VALUE) {
            return sr1.j0.a();
        }
        return new sr1(i, i2 - 1);
    }
}
