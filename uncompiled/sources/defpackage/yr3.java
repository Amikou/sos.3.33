package defpackage;

import com.google.common.base.AbstractIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: Splitter.java */
/* renamed from: yr3  reason: default package */
/* loaded from: classes2.dex */
public final class yr3 {
    public final wx a;
    public final boolean b;
    public final c c;
    public final int d;

    /* compiled from: Splitter.java */
    /* renamed from: yr3$a */
    /* loaded from: classes2.dex */
    public class a implements c {
        public final /* synthetic */ wx a;

        /* compiled from: Splitter.java */
        /* renamed from: yr3$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0292a extends b {
            public C0292a(yr3 yr3Var, CharSequence charSequence) {
                super(yr3Var, charSequence);
            }

            @Override // defpackage.yr3.b
            public int f(int i) {
                return i + 1;
            }

            @Override // defpackage.yr3.b
            public int g(int i) {
                return a.this.a.c(this.g0, i);
            }
        }

        public a(wx wxVar) {
            this.a = wxVar;
        }

        @Override // defpackage.yr3.c
        /* renamed from: b */
        public b a(yr3 yr3Var, CharSequence charSequence) {
            return new C0292a(yr3Var, charSequence);
        }
    }

    /* compiled from: Splitter.java */
    /* renamed from: yr3$b */
    /* loaded from: classes2.dex */
    public static abstract class b extends AbstractIterator<String> {
        public final CharSequence g0;
        public final wx h0;
        public final boolean i0;
        public int j0 = 0;
        public int k0;

        public b(yr3 yr3Var, CharSequence charSequence) {
            this.h0 = yr3Var.a;
            this.i0 = yr3Var.b;
            this.k0 = yr3Var.d;
            this.g0 = charSequence;
        }

        @Override // com.google.common.base.AbstractIterator
        /* renamed from: d */
        public String a() {
            int g;
            int i = this.j0;
            while (true) {
                int i2 = this.j0;
                if (i2 != -1) {
                    g = g(i2);
                    if (g == -1) {
                        g = this.g0.length();
                        this.j0 = -1;
                    } else {
                        this.j0 = f(g);
                    }
                    int i3 = this.j0;
                    if (i3 == i) {
                        int i4 = i3 + 1;
                        this.j0 = i4;
                        if (i4 > this.g0.length()) {
                            this.j0 = -1;
                        }
                    } else {
                        while (i < g && this.h0.e(this.g0.charAt(i))) {
                            i++;
                        }
                        while (g > i && this.h0.e(this.g0.charAt(g - 1))) {
                            g--;
                        }
                        if (!this.i0 || i != g) {
                            break;
                        }
                        i = this.j0;
                    }
                } else {
                    return b();
                }
            }
            int i5 = this.k0;
            if (i5 == 1) {
                g = this.g0.length();
                this.j0 = -1;
                while (g > i && this.h0.e(this.g0.charAt(g - 1))) {
                    g--;
                }
            } else {
                this.k0 = i5 - 1;
            }
            return this.g0.subSequence(i, g).toString();
        }

        public abstract int f(int i);

        public abstract int g(int i);
    }

    /* compiled from: Splitter.java */
    /* renamed from: yr3$c */
    /* loaded from: classes2.dex */
    public interface c {
        Iterator<String> a(yr3 yr3Var, CharSequence charSequence);
    }

    public yr3(c cVar) {
        this(cVar, false, wx.f(), Integer.MAX_VALUE);
    }

    public static yr3 d(char c2) {
        return e(wx.d(c2));
    }

    public static yr3 e(wx wxVar) {
        au2.k(wxVar);
        return new yr3(new a(wxVar));
    }

    public List<String> f(CharSequence charSequence) {
        au2.k(charSequence);
        Iterator<String> g = g(charSequence);
        ArrayList arrayList = new ArrayList();
        while (g.hasNext()) {
            arrayList.add(g.next());
        }
        return Collections.unmodifiableList(arrayList);
    }

    public final Iterator<String> g(CharSequence charSequence) {
        return this.c.a(this, charSequence);
    }

    public yr3(c cVar, boolean z, wx wxVar, int i) {
        this.c = cVar;
        this.b = z;
        this.a = wxVar;
        this.d = i;
    }
}
