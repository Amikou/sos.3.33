package defpackage;

import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import defpackage.kn2;
import java.util.LinkedList;
import java.util.UUID;

/* compiled from: CancelWorkRunnable.java */
/* renamed from: kv  reason: default package */
/* loaded from: classes.dex */
public abstract class kv implements Runnable {
    public final ln2 a = new ln2();

    /* compiled from: CancelWorkRunnable.java */
    /* renamed from: kv$a */
    /* loaded from: classes.dex */
    public class a extends kv {
        public final /* synthetic */ hq4 f0;
        public final /* synthetic */ UUID g0;

        public a(hq4 hq4Var, UUID uuid) {
            this.f0 = hq4Var;
            this.g0 = uuid;
        }

        @Override // defpackage.kv
        public void h() {
            WorkDatabase q = this.f0.q();
            q.e();
            try {
                a(this.f0, this.g0.toString());
                q.E();
                q.j();
                g(this.f0);
            } catch (Throwable th) {
                q.j();
                throw th;
            }
        }
    }

    /* compiled from: CancelWorkRunnable.java */
    /* renamed from: kv$b */
    /* loaded from: classes.dex */
    public class b extends kv {
        public final /* synthetic */ hq4 f0;
        public final /* synthetic */ String g0;

        public b(hq4 hq4Var, String str) {
            this.f0 = hq4Var;
            this.g0 = str;
        }

        @Override // defpackage.kv
        public void h() {
            WorkDatabase q = this.f0.q();
            q.e();
            try {
                for (String str : q.P().o(this.g0)) {
                    a(this.f0, str);
                }
                q.E();
                q.j();
                g(this.f0);
            } catch (Throwable th) {
                q.j();
                throw th;
            }
        }
    }

    /* compiled from: CancelWorkRunnable.java */
    /* renamed from: kv$c */
    /* loaded from: classes.dex */
    public class c extends kv {
        public final /* synthetic */ hq4 f0;
        public final /* synthetic */ String g0;
        public final /* synthetic */ boolean h0;

        public c(hq4 hq4Var, String str, boolean z) {
            this.f0 = hq4Var;
            this.g0 = str;
            this.h0 = z;
        }

        @Override // defpackage.kv
        public void h() {
            WorkDatabase q = this.f0.q();
            q.e();
            try {
                for (String str : q.P().j(this.g0)) {
                    a(this.f0, str);
                }
                q.E();
                q.j();
                if (this.h0) {
                    g(this.f0);
                }
            } catch (Throwable th) {
                q.j();
                throw th;
            }
        }
    }

    public static kv b(UUID uuid, hq4 hq4Var) {
        return new a(hq4Var, uuid);
    }

    public static kv c(String str, hq4 hq4Var, boolean z) {
        return new c(hq4Var, str, z);
    }

    public static kv d(String str, hq4 hq4Var) {
        return new b(hq4Var, str);
    }

    public void a(hq4 hq4Var, String str) {
        f(hq4Var.q(), str);
        hq4Var.o().l(str);
        for (cd3 cd3Var : hq4Var.p()) {
            cd3Var.d(str);
        }
    }

    public kn2 e() {
        return this.a;
    }

    public final void f(WorkDatabase workDatabase, String str) {
        uq4 P = workDatabase.P();
        jm0 H = workDatabase.H();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            WorkInfo.State k = P.k(str2);
            if (k != WorkInfo.State.SUCCEEDED && k != WorkInfo.State.FAILED) {
                P.a(WorkInfo.State.CANCELLED, str2);
            }
            linkedList.addAll(H.a(str2));
        }
    }

    public void g(hq4 hq4Var) {
        gd3.b(hq4Var.k(), hq4Var.q(), hq4Var.p());
    }

    public abstract void h();

    @Override // java.lang.Runnable
    public void run() {
        try {
            h();
            this.a.a(kn2.a);
        } catch (Throwable th) {
            this.a.a(new kn2.b.a(th));
        }
    }
}
