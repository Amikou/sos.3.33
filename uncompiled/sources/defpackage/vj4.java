package defpackage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

/* compiled from: ViewPropertyAnimatorCompat.java */
/* renamed from: vj4  reason: default package */
/* loaded from: classes.dex */
public final class vj4 {
    public WeakReference<View> a;
    public Runnable b = null;
    public Runnable c = null;
    public int d = -1;

    /* compiled from: ViewPropertyAnimatorCompat.java */
    /* renamed from: vj4$a */
    /* loaded from: classes.dex */
    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ xj4 a;
        public final /* synthetic */ View f0;

        public a(vj4 vj4Var, xj4 xj4Var, View view) {
            this.a = xj4Var;
            this.f0 = view;
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            this.a.a(this.f0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.a.b(this.f0);
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            this.a.c(this.f0);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat.java */
    /* renamed from: vj4$b */
    /* loaded from: classes.dex */
    public class b implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ zj4 a;
        public final /* synthetic */ View f0;

        public b(vj4 vj4Var, zj4 zj4Var, View view) {
            this.a = zj4Var;
            this.f0 = view;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.a(this.f0);
        }
    }

    /* compiled from: ViewPropertyAnimatorCompat.java */
    /* renamed from: vj4$c */
    /* loaded from: classes.dex */
    public static class c implements xj4 {
        public vj4 a;
        public boolean b;

        public c(vj4 vj4Var) {
            this.a = vj4Var;
        }

        @Override // defpackage.xj4
        public void a(View view) {
            Object tag = view.getTag(2113929216);
            xj4 xj4Var = tag instanceof xj4 ? (xj4) tag : null;
            if (xj4Var != null) {
                xj4Var.a(view);
            }
        }

        @Override // defpackage.xj4
        @SuppressLint({"WrongConstant"})
        public void b(View view) {
            int i = this.a.d;
            if (i > -1) {
                view.setLayerType(i, null);
                this.a.d = -1;
            }
            if (Build.VERSION.SDK_INT >= 16 || !this.b) {
                vj4 vj4Var = this.a;
                Runnable runnable = vj4Var.c;
                if (runnable != null) {
                    vj4Var.c = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                xj4 xj4Var = tag instanceof xj4 ? (xj4) tag : null;
                if (xj4Var != null) {
                    xj4Var.b(view);
                }
                this.b = true;
            }
        }

        @Override // defpackage.xj4
        public void c(View view) {
            this.b = false;
            if (this.a.d > -1) {
                view.setLayerType(2, null);
            }
            vj4 vj4Var = this.a;
            Runnable runnable = vj4Var.b;
            if (runnable != null) {
                vj4Var.b = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            xj4 xj4Var = tag instanceof xj4 ? (xj4) tag : null;
            if (xj4Var != null) {
                xj4Var.c(view);
            }
        }
    }

    public vj4(View view) {
        this.a = new WeakReference<>(view);
    }

    public vj4 a(float f) {
        View view = this.a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    public void b() {
        View view = this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    public long c() {
        View view = this.a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0L;
    }

    public vj4 d(long j) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    public vj4 e(Interpolator interpolator) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    public vj4 f(xj4 xj4Var) {
        View view = this.a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                g(view, xj4Var);
            } else {
                view.setTag(2113929216, xj4Var);
                g(view, new c(this));
            }
        }
        return this;
    }

    public final void g(View view, xj4 xj4Var) {
        if (xj4Var != null) {
            view.animate().setListener(new a(this, xj4Var, view));
        } else {
            view.animate().setListener(null);
        }
    }

    public vj4 h(long j) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    public vj4 i(zj4 zj4Var) {
        View view = this.a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            view.animate().setUpdateListener(zj4Var != null ? new b(this, zj4Var, view) : null);
        }
        return this;
    }

    public void j() {
        View view = this.a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    public vj4 k(float f) {
        View view = this.a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }
}
