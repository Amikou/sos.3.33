package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentCollectionsBinding.java */
/* renamed from: v91  reason: default package */
/* loaded from: classes2.dex */
public final class v91 {
    public final LinearLayout a;
    public final AppCompatImageView b;
    public final AppCompatImageView c;
    public final TextView d;
    public final RecyclerView e;
    public final AppCompatTextView f;

    public v91(LinearLayout linearLayout, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, LinearLayout linearLayout2, TextView textView, TextView textView2, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, AppCompatTextView appCompatTextView) {
        this.a = linearLayout;
        this.b = appCompatImageView;
        this.c = appCompatImageView2;
        this.d = textView2;
        this.e = recyclerView;
        this.f = appCompatTextView;
    }

    public static v91 a(View view) {
        int i = R.id.btnBack;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnBack);
        if (appCompatImageView != null) {
            i = R.id.btnInfo;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.btnInfo);
            if (appCompatImageView2 != null) {
                LinearLayout linearLayout = (LinearLayout) view;
                i = R.id.emptyPlaceholder;
                TextView textView = (TextView) ai4.a(view, R.id.emptyPlaceholder);
                if (textView != null) {
                    i = R.id.learn_more;
                    TextView textView2 = (TextView) ai4.a(view, R.id.learn_more);
                    if (textView2 != null) {
                        i = R.id.nftCollections;
                        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.nftCollections);
                        if (recyclerView != null) {
                            i = R.id.swRefresh;
                            SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swRefresh);
                            if (swipeRefreshLayout != null) {
                                i = R.id.title;
                                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.title);
                                if (appCompatTextView != null) {
                                    return new v91(linearLayout, appCompatImageView, appCompatImageView2, linearLayout, textView, textView2, recyclerView, swipeRefreshLayout, appCompatTextView);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public LinearLayout b() {
        return this.a;
    }
}
