package defpackage;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.firebase.messaging.f;
import defpackage.dh2;
import java.util.concurrent.atomic.AtomicInteger;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: f30  reason: default package */
/* loaded from: classes2.dex */
public final class f30 {
    public static final AtomicInteger a = new AtomicInteger((int) SystemClock.elapsedRealtime());

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* renamed from: f30$a */
    /* loaded from: classes2.dex */
    public static class a {
        public final dh2.e a;
        public final String b;
        public final int c = 0;

        public a(dh2.e eVar, String str, int i) {
            this.a = eVar;
            this.b = str;
        }
    }

    public static PendingIntent a(Context context, f fVar, String str, PackageManager packageManager) {
        Intent f = f(str, fVar, packageManager);
        if (f == null) {
            return null;
        }
        f.addFlags(67108864);
        f.putExtras(fVar.y());
        if (q(fVar)) {
            f.putExtra("gcm.n.analytics_data", fVar.x());
        }
        return PendingIntent.getActivity(context, g(), f, l(1073741824));
    }

    public static PendingIntent b(Context context, f fVar) {
        if (q(fVar)) {
            return c(context, new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS").putExtras(fVar.x()));
        }
        return null;
    }

    public static PendingIntent c(Context context, Intent intent) {
        return PendingIntent.getBroadcast(context, g(), new Intent("com.google.firebase.MESSAGING_EVENT").setComponent(new ComponentName(context, "com.google.firebase.iid.FirebaseInstanceIdReceiver")).putExtra("wrapped_intent", intent), l(1073741824));
    }

    public static a d(Context context, f fVar) {
        Bundle j = j(context.getPackageManager(), context.getPackageName());
        return e(context, context.getPackageName(), fVar, k(context, fVar.k(), j), context.getResources(), context.getPackageManager(), j);
    }

    public static a e(Context context, String str, f fVar, String str2, Resources resources, PackageManager packageManager, Bundle bundle) {
        dh2.e eVar = new dh2.e(context, str2);
        String n = fVar.n(resources, str, "gcm.n.title");
        if (!TextUtils.isEmpty(n)) {
            eVar.m(n);
        }
        String n2 = fVar.n(resources, str, "gcm.n.body");
        if (!TextUtils.isEmpty(n2)) {
            eVar.l(n2);
            eVar.C(new dh2.c().h(n2));
        }
        eVar.A(m(packageManager, resources, str, fVar.p("gcm.n.icon"), bundle));
        Uri n3 = n(str, fVar, resources);
        if (n3 != null) {
            eVar.B(n3);
        }
        eVar.k(a(context, fVar, str, packageManager));
        PendingIntent b = b(context, fVar);
        if (b != null) {
            eVar.o(b);
        }
        Integer h = h(context, fVar.p("gcm.n.color"), bundle);
        if (h != null) {
            eVar.i(h.intValue());
        }
        eVar.g(!fVar.a("gcm.n.sticky"));
        eVar.v(fVar.a("gcm.n.local_only"));
        String p = fVar.p("gcm.n.ticker");
        if (p != null) {
            eVar.D(p);
        }
        Integer m = fVar.m();
        if (m != null) {
            eVar.y(m.intValue());
        }
        Integer r = fVar.r();
        if (r != null) {
            eVar.F(r.intValue());
        }
        Integer l = fVar.l();
        if (l != null) {
            eVar.w(l.intValue());
        }
        Long j = fVar.j("gcm.n.event_time");
        if (j != null) {
            eVar.z(true);
            eVar.G(j.longValue());
        }
        long[] q = fVar.q();
        if (q != null) {
            eVar.E(q);
        }
        int[] e = fVar.e();
        if (e != null) {
            eVar.u(e[0], e[1], e[2]);
        }
        eVar.n(i(fVar));
        return new a(eVar, o(fVar), 0);
    }

    public static Intent f(String str, f fVar, PackageManager packageManager) {
        String p = fVar.p("gcm.n.click_action");
        if (!TextUtils.isEmpty(p)) {
            Intent intent = new Intent(p);
            intent.setPackage(str);
            intent.setFlags(268435456);
            return intent;
        }
        Uri f = fVar.f();
        if (f != null) {
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setPackage(str);
            intent2.setData(f);
            return intent2;
        }
        return packageManager.getLaunchIntentForPackage(str);
    }

    public static int g() {
        return a.incrementAndGet();
    }

    public static Integer h(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException unused) {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 56);
                sb.append("Color is invalid: ");
                sb.append(str);
                sb.append(". Notification will use default color.");
            }
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i != 0) {
            try {
                return Integer.valueOf(m70.d(context, i));
            } catch (Resources.NotFoundException unused2) {
            }
        }
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v2, types: [int] */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6 */
    public static int i(f fVar) {
        boolean a2 = fVar.a("gcm.n.default_sound");
        ?? r0 = a2;
        if (fVar.a("gcm.n.default_vibrate_timings")) {
            r0 = (a2 ? 1 : 0) | true;
        }
        return fVar.a("gcm.n.default_light_settings") ? r0 | 4 : r0;
    }

    public static Bundle j(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            if (applicationInfo != null) {
                Bundle bundle = applicationInfo.metaData;
                if (bundle != null) {
                    return bundle;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(valueOf.length() + 35);
            sb.append("Couldn't get own application info: ");
            sb.append(valueOf);
        }
        return Bundle.EMPTY;
    }

    @TargetApi(26)
    public static String k(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 26) {
            return null;
        }
        try {
            if (context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion >= 26) {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(NotificationManager.class);
                if (!TextUtils.isEmpty(str)) {
                    if (notificationManager.getNotificationChannel(str) != null) {
                        return str;
                    }
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 122);
                    sb.append("Notification Channel requested (");
                    sb.append(str);
                    sb.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                }
                String string = bundle.getString("com.google.firebase.messaging.default_notification_channel_id");
                if (TextUtils.isEmpty(string) || notificationManager.getNotificationChannel(string) == null) {
                    if (notificationManager.getNotificationChannel("fcm_fallback_notification_channel") == null) {
                        int identifier = context.getResources().getIdentifier("fcm_fallback_notification_channel_label", Utf8String.TYPE_NAME, context.getPackageName());
                        notificationManager.createNotificationChannel(new NotificationChannel("fcm_fallback_notification_channel", identifier == 0 ? "Misc" : context.getString(identifier), 3));
                    }
                    return "fcm_fallback_notification_channel";
                }
                return string;
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return null;
    }

    public static int l(int i) {
        return Build.VERSION.SDK_INT >= 23 ? 1140850688 : 1073741824;
    }

    public static int m(PackageManager packageManager, Resources resources, String str, String str2, Bundle bundle) {
        if (!TextUtils.isEmpty(str2)) {
            int identifier = resources.getIdentifier(str2, "drawable", str);
            if (identifier != 0 && p(resources, identifier)) {
                return identifier;
            }
            int identifier2 = resources.getIdentifier(str2, "mipmap", str);
            if (identifier2 != 0 && p(resources, identifier2)) {
                return identifier2;
            }
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 61);
            sb.append("Icon resource ");
            sb.append(str2);
            sb.append(" not found. Notification will use default icon.");
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_icon", 0);
        if (i == 0 || !p(resources, i)) {
            try {
                i = packageManager.getApplicationInfo(str, 0).icon;
            } catch (PackageManager.NameNotFoundException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb2 = new StringBuilder(valueOf.length() + 35);
                sb2.append("Couldn't get own application info: ");
                sb2.append(valueOf);
            }
        }
        if (i == 0 || !p(resources, i)) {
            return 17301651;
        }
        return i;
    }

    public static Uri n(String str, f fVar, Resources resources) {
        String o = fVar.o();
        if (TextUtils.isEmpty(o)) {
            return null;
        }
        if (!"default".equals(o) && resources.getIdentifier(o, "raw", str) != 0) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 24 + String.valueOf(o).length());
            sb.append("android.resource://");
            sb.append(str);
            sb.append("/raw/");
            sb.append(o);
            return Uri.parse(sb.toString());
        }
        return RingtoneManager.getDefaultUri(2);
    }

    public static String o(f fVar) {
        String p = fVar.p("gcm.n.tag");
        if (TextUtils.isEmpty(p)) {
            long uptimeMillis = SystemClock.uptimeMillis();
            StringBuilder sb = new StringBuilder(37);
            sb.append("FCM-Notification:");
            sb.append(uptimeMillis);
            return sb.toString();
        }
        return p;
    }

    @TargetApi(26)
    public static boolean p(Resources resources, int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (resources.getDrawable(i, null) instanceof AdaptiveIconDrawable) {
                StringBuilder sb = new StringBuilder(77);
                sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
                sb.append(i);
                return false;
            }
            return true;
        } catch (Resources.NotFoundException unused) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Couldn't find resource ");
            sb2.append(i);
            sb2.append(", treating it as an invalid icon");
            return false;
        }
    }

    public static boolean q(f fVar) {
        return fVar.a("google.c.a.e");
    }
}
