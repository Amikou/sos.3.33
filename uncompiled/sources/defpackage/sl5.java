package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: sl5  reason: default package */
/* loaded from: classes.dex */
public abstract class sl5 extends ql5 {
    public boolean b;

    public sl5(ck5 ck5Var) {
        super(ck5Var);
        this.a.l();
    }

    public abstract boolean f();

    public void g() {
    }

    public final boolean h() {
        return this.b;
    }

    public final void i() {
        if (!h()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void j() {
        if (!this.b) {
            if (f()) {
                return;
            }
            this.a.n();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    public final void k() {
        if (!this.b) {
            g();
            this.a.n();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
