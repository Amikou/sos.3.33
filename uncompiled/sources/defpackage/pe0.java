package defpackage;

import android.content.Context;
import java.text.SimpleDateFormat;

/* compiled from: DateFormat.kt */
/* renamed from: pe0  reason: default package */
/* loaded from: classes2.dex */
public final class pe0 {
    public static final a a = new a(null);
    public static final String b = "MMM d, yyyy";

    /* compiled from: DateFormat.kt */
    /* renamed from: pe0$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final String a() {
            return pe0.b;
        }

        public final SimpleDateFormat b(Context context) {
            fs1.f(context, "context");
            return new SimpleDateFormat(c(context));
        }

        public final String c(Context context) {
            fs1.f(context, "context");
            String j = bo3.j(context, "DEFAULT_DATE_FORMAT", a());
            fs1.e(j, "getString(context, Share…DATE_FORMAT, DATE_FORMAT)");
            return j;
        }

        public final void d(Context context, String str) {
            fs1.f(context, "context");
            fs1.f(str, "dateFormat");
            bo3.o(context, "DEFAULT_DATE_FORMAT", str);
        }
    }
}
