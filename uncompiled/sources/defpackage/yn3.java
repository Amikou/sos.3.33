package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.crypto.tink.k;
import com.google.crypto.tink.proto.t;
import com.google.crypto.tink.proto.y;
import java.io.IOException;

/* compiled from: SharedPrefKeysetWriter.java */
/* renamed from: yn3  reason: default package */
/* loaded from: classes2.dex */
public final class yn3 implements k {
    public final SharedPreferences.Editor a;
    public final String b;

    public yn3(Context context, String str, String str2) {
        if (str != null) {
            this.b = str;
            Context applicationContext = context.getApplicationContext();
            if (str2 == null) {
                this.a = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit();
                return;
            } else {
                this.a = applicationContext.getSharedPreferences(str2, 0).edit();
                return;
            }
        }
        throw new IllegalArgumentException("keysetName cannot be null");
    }

    @Override // com.google.crypto.tink.k
    public void a(y yVar) throws IOException {
        if (!this.a.putString(this.b, ok1.b(yVar.toByteArray())).commit()) {
            throw new IOException("Failed to write to SharedPreferences");
        }
    }

    @Override // com.google.crypto.tink.k
    public void b(t tVar) throws IOException {
        if (!this.a.putString(this.b, ok1.b(tVar.toByteArray())).commit()) {
            throw new IOException("Failed to write to SharedPreferences");
        }
    }
}
