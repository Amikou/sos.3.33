package defpackage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import defpackage.g83;
import defpackage.n81;
import defpackage.o81;

/* compiled from: TypefaceCompat.java */
/* renamed from: yd4  reason: default package */
/* loaded from: classes.dex */
public class yd4 {
    public static final ee4 a;
    public static final t22<String, Typeface> b;

    /* compiled from: TypefaceCompat.java */
    /* renamed from: yd4$a */
    /* loaded from: classes.dex */
    public static class a extends o81.c {
        public g83.d a;

        public a(g83.d dVar) {
            this.a = dVar;
        }

        @Override // defpackage.o81.c
        public void a(int i) {
            g83.d dVar = this.a;
            if (dVar != null) {
                dVar.d(i);
            }
        }

        @Override // defpackage.o81.c
        public void b(Typeface typeface) {
            g83.d dVar = this.a;
            if (dVar != null) {
                dVar.e(typeface);
            }
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            a = new de4();
        } else if (i >= 28) {
            a = new ce4();
        } else if (i >= 26) {
            a = new be4();
        } else if (i >= 24 && ae4.m()) {
            a = new ae4();
        } else if (i >= 21) {
            a = new zd4();
        } else {
            a = new ee4();
        }
        b = new t22<>(16);
    }

    public static Typeface a(Context context, Typeface typeface, int i) {
        Typeface g;
        if (context != null) {
            return (Build.VERSION.SDK_INT >= 21 || (g = g(context, typeface, i)) == null) ? Typeface.create(typeface, i) : g;
        }
        throw new IllegalArgumentException("Context cannot be null");
    }

    public static Typeface b(Context context, CancellationSignal cancellationSignal, o81.b[] bVarArr, int i) {
        return a.c(context, cancellationSignal, bVarArr, i);
    }

    public static Typeface c(Context context, n81.a aVar, Resources resources, int i, int i2, g83.d dVar, Handler handler, boolean z) {
        Typeface b2;
        if (aVar instanceof n81.d) {
            n81.d dVar2 = (n81.d) aVar;
            Typeface h = h(dVar2.c());
            if (h != null) {
                if (dVar != null) {
                    dVar.b(h, handler);
                }
                return h;
            }
            boolean z2 = !z ? dVar != null : dVar2.a() != 0;
            int d = z ? dVar2.d() : -1;
            b2 = o81.a(context, dVar2.b(), i2, z2, d, g83.d.c(handler), new a(dVar));
        } else {
            b2 = a.b(context, (n81.b) aVar, resources, i2);
            if (dVar != null) {
                if (b2 != null) {
                    dVar.b(b2, handler);
                } else {
                    dVar.a(-3, handler);
                }
            }
        }
        if (b2 != null) {
            b.d(e(resources, i, i2), b2);
        }
        return b2;
    }

    public static Typeface d(Context context, Resources resources, int i, String str, int i2) {
        Typeface e = a.e(context, resources, i, str, i2);
        if (e != null) {
            b.d(e(resources, i, i2), e);
        }
        return e;
    }

    public static String e(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + "-" + i + "-" + i2;
    }

    public static Typeface f(Resources resources, int i, int i2) {
        return b.c(e(resources, i, i2));
    }

    public static Typeface g(Context context, Typeface typeface, int i) {
        ee4 ee4Var = a;
        n81.b i2 = ee4Var.i(typeface);
        if (i2 == null) {
            return null;
        }
        return ee4Var.b(context, i2, context.getResources(), i);
    }

    public static Typeface h(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        Typeface create = Typeface.create(str, 0);
        Typeface create2 = Typeface.create(Typeface.DEFAULT, 0);
        if (create == null || create.equals(create2)) {
            return null;
        }
        return create;
    }
}
