package defpackage;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSendTokenConfirmationBinding.java */
/* renamed from: zn0  reason: default package */
/* loaded from: classes2.dex */
public final class zn0 {
    public final CardView a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final TextView d;
    public final TextView e;

    public zn0(CardView cardView, MaterialButton materialButton, MaterialButton materialButton2, TextView textView, TextView textView2, RelativeLayout relativeLayout) {
        this.a = cardView;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = textView;
        this.e = textView2;
    }

    public static zn0 a(View view) {
        int i = R.id.btnAction;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAction);
        if (materialButton != null) {
            i = R.id.dialog_cross;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
            if (materialButton2 != null) {
                i = R.id.tvDialogContentLine1;
                TextView textView = (TextView) ai4.a(view, R.id.tvDialogContentLine1);
                if (textView != null) {
                    i = R.id.tvDialogTitle;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                    if (textView2 != null) {
                        i = R.id.vDialogContentContainer;
                        RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.vDialogContentContainer);
                        if (relativeLayout != null) {
                            return new zn0((CardView) view, materialButton, materialButton2, textView, textView2, relativeLayout);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
