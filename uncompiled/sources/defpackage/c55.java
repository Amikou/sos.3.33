package defpackage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: c55  reason: default package */
/* loaded from: classes.dex */
public abstract class c55 implements z55, m55 {
    public final String a;
    public final Map<String, z55> f0 = new HashMap();

    public c55(String str) {
        this.a = str;
    }

    public abstract z55 a(wk5 wk5Var, List<z55> list);

    @Override // defpackage.z55
    public final Double b() {
        return Double.valueOf(Double.NaN);
    }

    @Override // defpackage.z55
    public final Boolean c() {
        return Boolean.TRUE;
    }

    public final String d() {
        return this.a;
    }

    @Override // defpackage.m55
    public final z55 e(String str) {
        return this.f0.containsKey(str) ? this.f0.get(str) : z55.X;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof c55) {
            c55 c55Var = (c55) obj;
            String str = this.a;
            if (str != null) {
                return str.equals(c55Var.a);
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        String str = this.a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return g55.b(this.f0);
    }

    @Override // defpackage.m55
    public final void k(String str, z55 z55Var) {
        if (z55Var == null) {
            this.f0.remove(str);
        } else {
            this.f0.put(str, z55Var);
        }
    }

    @Override // defpackage.z55
    public z55 m() {
        return this;
    }

    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        if ("toString".equals(str)) {
            return new f65(this.a);
        }
        return g55.a(this, new f65(str), wk5Var, list);
    }

    @Override // defpackage.m55
    public final boolean o(String str) {
        return this.f0.containsKey(str);
    }

    @Override // defpackage.z55
    public final String zzc() {
        return this.a;
    }
}
