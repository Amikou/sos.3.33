package defpackage;

/* compiled from: LanguageContext.java */
/* renamed from: cy1  reason: default package */
/* loaded from: classes2.dex */
public class cy1 {
    public static cy1 b;
    public iy1 a;

    public cy1(vk2 vk2Var) {
        b = this;
        if (vk2Var.e(vk2Var.f(), "PREFS_OS_LANGUAGE", null) != null) {
            this.a = new jy1(vk2Var);
        } else {
            this.a = new ky1();
        }
    }

    public static cy1 a() {
        return b;
    }

    public String b() {
        return this.a.a();
    }
}
