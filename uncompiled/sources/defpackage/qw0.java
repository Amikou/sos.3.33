package defpackage;

import java.util.Arrays;
import java.util.List;

/* compiled from: EthFilter.java */
/* renamed from: qw0  reason: default package */
/* loaded from: classes3.dex */
public class qw0 extends i41<qw0> {
    private List<String> address;
    private gi0 fromBlock;
    private gi0 toBlock;

    public qw0() {
    }

    public List<String> getAddress() {
        return this.address;
    }

    public gi0 getFromBlock() {
        return this.fromBlock;
    }

    @Override // defpackage.i41
    public qw0 getThis() {
        return this;
    }

    public gi0 getToBlock() {
        return this.toBlock;
    }

    public qw0(gi0 gi0Var, gi0 gi0Var2, List<String> list) {
        this.fromBlock = gi0Var;
        this.toBlock = gi0Var2;
        this.address = list;
    }

    public qw0(gi0 gi0Var, gi0 gi0Var2, String str) {
        this(gi0Var, gi0Var2, Arrays.asList(str));
    }
}
