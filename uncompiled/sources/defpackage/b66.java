package defpackage;

import com.google.android.gms.internal.measurement.a;
import com.google.android.gms.internal.measurement.b;
import java.util.Collections;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: b66  reason: default package */
/* loaded from: classes.dex */
public final class b66 {
    public final TreeMap<Integer, u55> a = new TreeMap<>();
    public final TreeMap<Integer, u55> b = new TreeMap<>();

    public static final int c(wk5 wk5Var, u55 u55Var, z55 z55Var) {
        z55 a = u55Var.a(wk5Var, Collections.singletonList(z55Var));
        if (a instanceof z45) {
            return vm5.g(a.b().doubleValue());
        }
        return -1;
    }

    public final void a(String str, int i, u55 u55Var, String str2) {
        TreeMap<Integer, u55> treeMap;
        if ("create".equals(str2)) {
            treeMap = this.b;
        } else if (!"edit".equals(str2)) {
            String valueOf = String.valueOf(str2);
            throw new IllegalStateException(valueOf.length() != 0 ? "Unknown callback type: ".concat(valueOf) : new String("Unknown callback type: "));
        } else {
            treeMap = this.a;
        }
        if (treeMap.containsKey(Integer.valueOf(i))) {
            i = treeMap.lastKey().intValue() + 1;
        }
        treeMap.put(Integer.valueOf(i), u55Var);
    }

    public final void b(wk5 wk5Var, b bVar) {
        fx5 fx5Var = new fx5(bVar);
        for (Integer num : this.a.keySet()) {
            a clone = bVar.c().clone();
            int c = c(wk5Var, this.a.get(num), fx5Var);
            if (c == 2 || c == -1) {
                bVar.d(clone);
            }
        }
        for (Integer num2 : this.b.keySet()) {
            c(wk5Var, this.b.get(num2), fx5Var);
        }
    }
}
