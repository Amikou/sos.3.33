package defpackage;

import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.flow.SharedFlowImpl;

/* compiled from: SharedFlow.kt */
/* renamed from: wn3  reason: default package */
/* loaded from: classes2.dex */
public final class wn3 extends k5<SharedFlowImpl<?>> {
    public long a = -1;
    public q70<? super te4> b;

    @Override // defpackage.k5
    /* renamed from: c */
    public boolean a(SharedFlowImpl<?> sharedFlowImpl) {
        if (this.a >= 0) {
            return false;
        }
        this.a = sharedFlowImpl.T();
        return true;
    }

    @Override // defpackage.k5
    /* renamed from: d */
    public Continuation<Unit>[] b(SharedFlowImpl<?> sharedFlowImpl) {
        if (ze0.a()) {
            if (!(this.a >= 0)) {
                throw new AssertionError();
            }
        }
        long j = this.a;
        this.a = -1L;
        this.b = null;
        return sharedFlowImpl.S(j);
    }
}
