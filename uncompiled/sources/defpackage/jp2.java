package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;

/* compiled from: Pair.java */
/* renamed from: jp2  reason: default package */
/* loaded from: classes.dex */
public class jp2<F, S> {
    public final F a;
    public final S b;

    public jp2(F f, S s) {
        this.a = f;
        this.b = s;
    }

    public static <A, B> jp2<A, B> a(A a, B b) {
        return new jp2<>(a, b);
    }

    public boolean equals(Object obj) {
        if (obj instanceof jp2) {
            jp2 jp2Var = (jp2) obj;
            return sl2.a(jp2Var.a, this.a) && sl2.a(jp2Var.b, this.b);
        }
        return false;
    }

    public int hashCode() {
        F f = this.a;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        return hashCode ^ (s != null ? s.hashCode() : 0);
    }

    public String toString() {
        return "Pair{" + this.a + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.b + "}";
    }
}
