package defpackage;

import android.content.Context;
import defpackage.x50;

/* compiled from: DefaultConnectivityMonitor.java */
/* renamed from: oi0  reason: default package */
/* loaded from: classes.dex */
public final class oi0 implements x50 {
    public final Context a;
    public final x50.a f0;

    public oi0(Context context, x50.a aVar) {
        this.a = context.getApplicationContext();
        this.f0 = aVar;
    }

    @Override // defpackage.pz1
    public void b() {
        d();
    }

    public final void d() {
        zp3.a(this.a).d(this.f0);
    }

    @Override // defpackage.pz1
    public void e() {
    }

    @Override // defpackage.pz1
    public void h() {
        i();
    }

    public final void i() {
        zp3.a(this.a).e(this.f0);
    }
}
