package defpackage;

import androidx.lifecycle.l;

/* compiled from: TransactionNotificationDetailsViewModelFactory.kt */
/* renamed from: v84  reason: default package */
/* loaded from: classes2.dex */
public final class v84 implements l.b {
    @Override // androidx.lifecycle.l.b
    public <T extends dj4> T a(Class<T> cls) {
        fs1.f(cls, "modelClass");
        ac3 m = a4.m();
        fs1.e(m, "getSafeMoonClient()");
        return new ua4(m);
    }
}
