package defpackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* compiled from: _Sequences.kt */
/* renamed from: vl3  reason: default package */
/* loaded from: classes2.dex */
public class vl3 extends ul3 {

    /* compiled from: Iterables.kt */
    /* renamed from: vl3$a */
    /* loaded from: classes2.dex */
    public static final class a implements Iterable<T>, tw1 {
        public final /* synthetic */ ol3 a;

        public a(ol3 ol3Var) {
            this.a = ol3Var;
        }

        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    public static final <T> Iterable<T> f(ol3<? extends T> ol3Var) {
        fs1.f(ol3Var, "$this$asIterable");
        return new a(ol3Var);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> ol3<T> g(ol3<? extends T> ol3Var, int i) {
        fs1.f(ol3Var, "$this$drop");
        if (i >= 0) {
            return i == 0 ? ol3Var : ol3Var instanceof fs0 ? ((fs0) ol3Var).a(i) : new es0(ol3Var, i);
        }
        throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
    }

    public static final <T, A extends Appendable> A h(ol3<? extends T> ol3Var, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(ol3Var, "$this$joinTo");
        fs1.f(a2, "buffer");
        fs1.f(charSequence, "separator");
        fs1.f(charSequence2, "prefix");
        fs1.f(charSequence3, "postfix");
        fs1.f(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (T t : ol3Var) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            wu3.a(a2, t, tc1Var);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    public static final <T> String i(ol3<? extends T> ol3Var, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(ol3Var, "$this$joinToString");
        fs1.f(charSequence, "separator");
        fs1.f(charSequence2, "prefix");
        fs1.f(charSequence3, "postfix");
        fs1.f(charSequence4, "truncated");
        String sb = ((StringBuilder) h(ol3Var, new StringBuilder(), charSequence, charSequence2, charSequence3, i, charSequence4, tc1Var)).toString();
        fs1.e(sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }

    public static /* synthetic */ String j(ol3 ol3Var, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tc1 tc1Var, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        String str = (i2 & 2) != 0 ? "" : charSequence2;
        String str2 = (i2 & 4) == 0 ? charSequence3 : "";
        if ((i2 & 8) != 0) {
            i = -1;
        }
        int i3 = i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence5 = charSequence4;
        if ((i2 & 32) != 0) {
            tc1Var = null;
        }
        return i(ol3Var, charSequence, str, str2, i3, charSequence5, tc1Var);
    }

    public static final <T, R> ol3<R> k(ol3<? extends T> ol3Var, tc1<? super T, ? extends R> tc1Var) {
        fs1.f(ol3Var, "$this$map");
        fs1.f(tc1Var, "transform");
        return new gb4(ol3Var, tc1Var);
    }

    public static final <T, C extends Collection<? super T>> C l(ol3<? extends T> ol3Var, C c) {
        fs1.f(ol3Var, "$this$toCollection");
        fs1.f(c, "destination");
        for (T t : ol3Var) {
            c.add(t);
        }
        return c;
    }

    public static final <T> List<T> m(ol3<? extends T> ol3Var) {
        fs1.f(ol3Var, "$this$toList");
        return b20.m(n(ol3Var));
    }

    public static final <T> List<T> n(ol3<? extends T> ol3Var) {
        fs1.f(ol3Var, "$this$toMutableList");
        return (List) l(ol3Var, new ArrayList());
    }
}
