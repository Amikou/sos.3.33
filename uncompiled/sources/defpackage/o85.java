package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: o85  reason: default package */
/* loaded from: classes.dex */
public final class o85 extends o65 {
    public o85() {
        this.a.add(zzbl.FOR_IN);
        this.a.add(zzbl.FOR_IN_CONST);
        this.a.add(zzbl.FOR_IN_LET);
        this.a.add(zzbl.FOR_LET);
        this.a.add(zzbl.FOR_OF);
        this.a.add(zzbl.FOR_OF_CONST);
        this.a.add(zzbl.FOR_OF_LET);
        this.a.add(zzbl.WHILE);
    }

    public static z55 c(h85 h85Var, z55 z55Var, z55 z55Var2) {
        return e(h85Var, z55Var.i(), z55Var2);
    }

    public static z55 d(h85 h85Var, z55 z55Var, z55 z55Var2) {
        if (z55Var instanceof Iterable) {
            return e(h85Var, ((Iterable) z55Var).iterator(), z55Var2);
        }
        throw new IllegalArgumentException("Non-iterable type in for...of loop.");
    }

    public static z55 e(h85 h85Var, Iterator<z55> it, z55 z55Var) {
        if (it != null) {
            while (it.hasNext()) {
                z55 b = h85Var.a(it.next()).b((p45) z55Var);
                if (b instanceof v45) {
                    v45 v45Var = (v45) b;
                    if ("break".equals(v45Var.d())) {
                        return z55.X;
                    }
                    if ("return".equals(v45Var.d())) {
                        return v45Var;
                    }
                }
            }
        }
        return z55.X;
    }

    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        zzbl zzblVar = zzbl.ADD;
        int ordinal = vm5.e(str).ordinal();
        if (ordinal != 65) {
            switch (ordinal) {
                case 26:
                    vm5.a(zzbl.FOR_IN.name(), 3, list);
                    if (list.get(0) instanceof f65) {
                        return c(new k85(wk5Var, list.get(0).zzc()), wk5Var.a(list.get(1)), wk5Var.a(list.get(2)));
                    }
                    throw new IllegalArgumentException("Variable name in FOR_IN must be a string");
                case 27:
                    vm5.a(zzbl.FOR_IN_CONST.name(), 3, list);
                    if (list.get(0) instanceof f65) {
                        return c(new c85(wk5Var, list.get(0).zzc()), wk5Var.a(list.get(1)), wk5Var.a(list.get(2)));
                    }
                    throw new IllegalArgumentException("Variable name in FOR_IN_CONST must be a string");
                case 28:
                    vm5.a(zzbl.FOR_IN_LET.name(), 3, list);
                    if (list.get(0) instanceof f65) {
                        return c(new g85(wk5Var, list.get(0).zzc()), wk5Var.a(list.get(1)), wk5Var.a(list.get(2)));
                    }
                    throw new IllegalArgumentException("Variable name in FOR_IN_LET must be a string");
                case 29:
                    vm5.a(zzbl.FOR_LET.name(), 4, list);
                    z55 a = wk5Var.a(list.get(0));
                    if (a instanceof p45) {
                        p45 p45Var = (p45) a;
                        z55 z55Var = list.get(1);
                        z55 z55Var2 = list.get(2);
                        z55 a2 = wk5Var.a(list.get(3));
                        wk5 c = wk5Var.c();
                        for (int i = 0; i < p45Var.s(); i++) {
                            String zzc = p45Var.w(i).zzc();
                            c.e(zzc, wk5Var.h(zzc));
                        }
                        while (wk5Var.a(z55Var).c().booleanValue()) {
                            z55 b = wk5Var.b((p45) a2);
                            if (b instanceof v45) {
                                v45 v45Var = (v45) b;
                                if ("break".equals(v45Var.d())) {
                                    return z55.X;
                                }
                                if ("return".equals(v45Var.d())) {
                                    return v45Var;
                                }
                            }
                            wk5 c2 = wk5Var.c();
                            for (int i2 = 0; i2 < p45Var.s(); i2++) {
                                String zzc2 = p45Var.w(i2).zzc();
                                c2.e(zzc2, c.h(zzc2));
                            }
                            c2.a(z55Var2);
                            c = c2;
                        }
                        return z55.X;
                    }
                    throw new IllegalArgumentException("Initializer variables in FOR_LET must be an ArrayList");
                case 30:
                    vm5.a(zzbl.FOR_OF.name(), 3, list);
                    if (list.get(0) instanceof f65) {
                        return d(new k85(wk5Var, list.get(0).zzc()), wk5Var.a(list.get(1)), wk5Var.a(list.get(2)));
                    }
                    throw new IllegalArgumentException("Variable name in FOR_OF must be a string");
                case 31:
                    vm5.a(zzbl.FOR_OF_CONST.name(), 3, list);
                    if (list.get(0) instanceof f65) {
                        return d(new c85(wk5Var, list.get(0).zzc()), wk5Var.a(list.get(1)), wk5Var.a(list.get(2)));
                    }
                    throw new IllegalArgumentException("Variable name in FOR_OF_CONST must be a string");
                case 32:
                    vm5.a(zzbl.FOR_OF_LET.name(), 3, list);
                    if (list.get(0) instanceof f65) {
                        return d(new g85(wk5Var, list.get(0).zzc()), wk5Var.a(list.get(1)), wk5Var.a(list.get(2)));
                    }
                    throw new IllegalArgumentException("Variable name in FOR_OF_LET must be a string");
                default:
                    return super.b(str);
            }
        }
        vm5.a(zzbl.WHILE.name(), 4, list);
        z55 z55Var3 = list.get(0);
        z55 z55Var4 = list.get(1);
        z55 a3 = wk5Var.a(list.get(3));
        if (wk5Var.a(list.get(2)).c().booleanValue()) {
            z55 b2 = wk5Var.b((p45) a3);
            if (b2 instanceof v45) {
                v45 v45Var2 = (v45) b2;
                if ("break".equals(v45Var2.d())) {
                    return z55.X;
                }
                if ("return".equals(v45Var2.d())) {
                    return v45Var2;
                }
            }
        }
        while (wk5Var.a(z55Var3).c().booleanValue()) {
            z55 b3 = wk5Var.b((p45) a3);
            if (b3 instanceof v45) {
                v45 v45Var3 = (v45) b3;
                if ("break".equals(v45Var3.d())) {
                    return z55.X;
                }
                if ("return".equals(v45Var3.d())) {
                    return v45Var3;
                }
            }
            wk5Var.a(z55Var4);
        }
        return z55.X;
    }
}
