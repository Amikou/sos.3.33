package defpackage;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: AutoRotateDrawable.java */
/* renamed from: hk  reason: default package */
/* loaded from: classes.dex */
public class hk extends d91 implements Runnable {
    public int h0;
    public boolean i0;
    public float j0;
    public boolean k0;

    public hk(Drawable drawable, int i) {
        this(drawable, i, true);
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int save = canvas.save();
        Rect bounds = getBounds();
        int i = bounds.right;
        int i2 = bounds.left;
        int i3 = i - i2;
        int i4 = bounds.bottom;
        int i5 = bounds.top;
        int i6 = i4 - i5;
        float f = this.j0;
        if (!this.i0) {
            f = 360.0f - f;
        }
        canvas.rotate(f, i2 + (i3 / 2), i5 + (i6 / 2));
        super.draw(canvas);
        canvas.restoreToCount(save);
        q();
    }

    public final int p() {
        return (int) ((20.0f / this.h0) * 360.0f);
    }

    public final void q() {
        if (this.k0) {
            return;
        }
        this.k0 = true;
        scheduleSelf(this, SystemClock.uptimeMillis() + 20);
    }

    @Override // java.lang.Runnable
    public void run() {
        this.k0 = false;
        this.j0 += p();
        invalidateSelf();
    }

    public hk(Drawable drawable, int i, boolean z) {
        super((Drawable) xt2.g(drawable));
        this.j0 = Utils.FLOAT_EPSILON;
        this.k0 = false;
        this.h0 = i;
        this.i0 = z;
    }
}
