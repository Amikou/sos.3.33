package defpackage;

import com.google.android.gms.internal.vision.zzha;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: fn5  reason: default package */
/* loaded from: classes.dex */
public final class fn5 implements ws5 {
    public static final ws5 a = new fn5();

    @Override // defpackage.ws5
    public final boolean d(int i) {
        return zzha.zza(i) != null;
    }
}
