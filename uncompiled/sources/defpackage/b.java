package defpackage;

/* compiled from: -Platform.kt */
/* renamed from: b  reason: default package */
/* loaded from: classes2.dex */
public final class b {
    public static final byte[] a(String str) {
        fs1.f(str, "$this$asUtf8ToByteArray");
        byte[] bytes = str.getBytes(by.a);
        fs1.e(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }

    public static final String b(byte[] bArr) {
        fs1.f(bArr, "$this$toUtf8String");
        return new String(bArr, by.a);
    }
}
