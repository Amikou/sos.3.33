package defpackage;

import java.math.BigInteger;

/* compiled from: StringNumberConversionsJVM.kt */
/* renamed from: bv3  reason: default package */
/* loaded from: classes2.dex */
public class bv3 extends av3 {
    public static final BigInteger i(String str) {
        fs1.f(str, "$this$toBigIntegerOrNull");
        return j(str, 10);
    }

    public static final BigInteger j(String str, int i) {
        fs1.f(str, "$this$toBigIntegerOrNull");
        xx.a(i);
        int length = str.length();
        if (length != 0) {
            if (length != 1) {
                for (int i2 = str.charAt(0) == '-' ? 1 : 0; i2 < length; i2++) {
                    if (xx.b(str.charAt(i2), i) < 0) {
                        return null;
                    }
                }
            } else if (xx.b(str.charAt(0), i) < 0) {
                return null;
            }
            return new BigInteger(str, xx.a(i));
        }
        return null;
    }

    public static final Double k(String str) {
        fs1.f(str, "$this$toDoubleOrNull");
        try {
            if (wd3.a.matches(str)) {
                return Double.valueOf(Double.parseDouble(str));
            }
            return null;
        } catch (NumberFormatException unused) {
            return null;
        }
    }
}
