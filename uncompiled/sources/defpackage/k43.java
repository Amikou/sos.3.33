package defpackage;

import android.os.Bundle;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import org.web3j.abi.datatypes.Address;

/* compiled from: ReceiveFragmentDirections.java */
/* renamed from: k43  reason: default package */
/* loaded from: classes2.dex */
public class k43 {

    /* compiled from: ReceiveFragmentDirections.java */
    /* renamed from: k43$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("tokenChainId")) {
                bundle.putInt("tokenChainId", ((Integer) this.a.get("tokenChainId")).intValue());
            }
            if (this.a.containsKey(Address.TYPE_NAME)) {
                bundle.putString(Address.TYPE_NAME, (String) this.a.get(Address.TYPE_NAME));
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_ReceiveFragment_to_qrFragment;
        }

        public String c() {
            return (String) this.a.get(Address.TYPE_NAME);
        }

        public int d() {
            return ((Integer) this.a.get("tokenChainId")).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("tokenChainId") == bVar.a.containsKey("tokenChainId") && d() == bVar.d() && this.a.containsKey(Address.TYPE_NAME) == bVar.a.containsKey(Address.TYPE_NAME)) {
                if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                    return b() == bVar.b();
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return ((((d() + 31) * 31) + (c() != null ? c().hashCode() : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionReceiveFragmentToQrFragment(actionId=" + b() + "){tokenChainId=" + d() + ", address=" + c() + "}";
        }

        public b(int i, String str) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            hashMap.put("tokenChainId", Integer.valueOf(i));
            if (str != null) {
                hashMap.put(Address.TYPE_NAME, str);
                return;
            }
            throw new IllegalArgumentException("Argument \"address\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(int i, String str) {
        return new b(i, str);
    }
}
