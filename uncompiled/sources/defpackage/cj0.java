package defpackage;

import androidx.media3.common.ParserException;
import java.io.IOException;
import java.util.ArrayDeque;

/* compiled from: DefaultEbmlReader.java */
/* renamed from: cj0  reason: default package */
/* loaded from: classes.dex */
public final class cj0 implements au0 {
    public final byte[] a = new byte[8];
    public final ArrayDeque<b> b = new ArrayDeque<>();
    public final bh4 c = new bh4();
    public zt0 d;
    public int e;
    public int f;
    public long g;

    /* compiled from: DefaultEbmlReader.java */
    /* renamed from: cj0$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final int a;
        public final long b;

        public b(int i, long j) {
            this.a = i;
            this.b = j;
        }
    }

    public static String f(q11 q11Var, int i) throws IOException {
        if (i == 0) {
            return "";
        }
        byte[] bArr = new byte[i];
        q11Var.readFully(bArr, 0, i);
        while (i > 0 && bArr[i - 1] == 0) {
            i--;
        }
        return new String(bArr, 0, i);
    }

    @Override // defpackage.au0
    public void a(zt0 zt0Var) {
        this.d = zt0Var;
    }

    @Override // defpackage.au0
    public boolean b(q11 q11Var) throws IOException {
        ii.i(this.d);
        while (true) {
            b peek = this.b.peek();
            if (peek != null && q11Var.getPosition() >= peek.b) {
                this.d.a(this.b.pop().a);
                return true;
            }
            if (this.e == 0) {
                long d = this.c.d(q11Var, true, false, 4);
                if (d == -2) {
                    d = c(q11Var);
                }
                if (d == -1) {
                    return false;
                }
                this.f = (int) d;
                this.e = 1;
            }
            if (this.e == 1) {
                this.g = this.c.d(q11Var, false, true, 8);
                this.e = 2;
            }
            int b2 = this.d.b(this.f);
            if (b2 != 0) {
                if (b2 == 1) {
                    long position = q11Var.getPosition();
                    this.b.push(new b(this.f, this.g + position));
                    this.d.g(this.f, position, this.g);
                    this.e = 0;
                    return true;
                } else if (b2 == 2) {
                    long j = this.g;
                    if (j <= 8) {
                        this.d.h(this.f, e(q11Var, (int) j));
                        this.e = 0;
                        return true;
                    }
                    throw ParserException.createForMalformedContainer("Invalid integer size: " + this.g, null);
                } else if (b2 == 3) {
                    long j2 = this.g;
                    if (j2 <= 2147483647L) {
                        this.d.d(this.f, f(q11Var, (int) j2));
                        this.e = 0;
                        return true;
                    }
                    throw ParserException.createForMalformedContainer("String element size: " + this.g, null);
                } else if (b2 == 4) {
                    this.d.f(this.f, (int) this.g, q11Var);
                    this.e = 0;
                    return true;
                } else if (b2 == 5) {
                    long j3 = this.g;
                    if (j3 != 4 && j3 != 8) {
                        throw ParserException.createForMalformedContainer("Invalid float size: " + this.g, null);
                    }
                    this.d.e(this.f, d(q11Var, (int) j3));
                    this.e = 0;
                    return true;
                } else {
                    throw ParserException.createForMalformedContainer("Invalid element type " + b2, null);
                }
            }
            q11Var.k((int) this.g);
            this.e = 0;
        }
    }

    public final long c(q11 q11Var) throws IOException {
        q11Var.j();
        while (true) {
            q11Var.n(this.a, 0, 4);
            int c = bh4.c(this.a[0]);
            if (c != -1 && c <= 4) {
                int a2 = (int) bh4.a(this.a, c, false);
                if (this.d.c(a2)) {
                    q11Var.k(c);
                    return a2;
                }
            }
            q11Var.k(1);
        }
    }

    public final double d(q11 q11Var, int i) throws IOException {
        long e = e(q11Var, i);
        if (i == 4) {
            return Float.intBitsToFloat((int) e);
        }
        return Double.longBitsToDouble(e);
    }

    public final long e(q11 q11Var, int i) throws IOException {
        q11Var.readFully(this.a, 0, i);
        long j = 0;
        for (int i2 = 0; i2 < i; i2++) {
            j = (j << 8) | (this.a[i2] & 255);
        }
        return j;
    }

    @Override // defpackage.au0
    public void reset() {
        this.e = 0;
        this.b.clear();
        this.c.e();
    }
}
