package defpackage;

/* compiled from: TileMeasures.kt */
/* renamed from: x54  reason: default package */
/* loaded from: classes2.dex */
public final class x54 {
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;
    public final int h;
    public final int i;
    public final int j;
    public final int k;
    public final int l;
    public final int m;
    public final int n;

    public x54(int i, int i2) {
        this.a = i;
        this.b = i2;
        this.c = i / 2;
        this.d = i2 / 2;
        int i3 = i / 3;
        this.e = i3;
        int i4 = i2 / 3;
        this.f = i4;
        int i5 = i / 4;
        this.g = i5;
        int i6 = i2 / 4;
        this.h = i6;
        this.i = i3 * 2;
        this.j = i4 * 2;
        this.k = i5 * 3;
        this.l = i6 * 3;
        this.m = i * 2;
        this.n = i2 * 2;
    }

    public final int a() {
        return this.f;
    }

    public final int b() {
        return this.j;
    }

    public final int c() {
        return this.h;
    }

    public final int d() {
        return this.l;
    }

    public final int e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof x54) {
            x54 x54Var = (x54) obj;
            return this.a == x54Var.a && this.b == x54Var.b;
        }
        return false;
    }

    public final int f() {
        return this.b;
    }

    public final int g() {
        return this.n;
    }

    public final int h() {
        return this.e;
    }

    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    public final int i() {
        return this.i;
    }

    public final int j() {
        return this.g;
    }

    public final int k() {
        return this.k;
    }

    public final int l() {
        return this.c;
    }

    public final int m() {
        return this.a;
    }

    public final int n() {
        return this.m;
    }

    public String toString() {
        return "TileMeasures(width=" + this.a + ", height=" + this.b + ')';
    }
}
