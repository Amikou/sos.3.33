package defpackage;

import android.view.View;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAnchorSelectSecurityQuestionsBinding.java */
/* renamed from: hn0  reason: default package */
/* loaded from: classes2.dex */
public final class hn0 {
    public final LinearLayoutCompat a;

    public hn0(ConstraintLayout constraintLayout, LinearLayoutCompat linearLayoutCompat) {
        this.a = linearLayoutCompat;
    }

    public static hn0 a(View view) {
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.buttonWrapper);
        if (linearLayoutCompat != null) {
            return new hn0((ConstraintLayout) view, linearLayoutCompat);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.buttonWrapper)));
    }
}
