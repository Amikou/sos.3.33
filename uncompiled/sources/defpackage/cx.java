package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.cx;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: ChainNetworkAdapter.kt */
/* renamed from: cx  reason: default package */
/* loaded from: classes2.dex */
public final class cx extends RecyclerView.Adapter<b> implements Filterable {
    public final tc1<TokenType, te4> a;
    public final List<TokenType> f0;
    public final List<TokenType> g0;

    /* compiled from: ChainNetworkAdapter.kt */
    /* renamed from: cx$a */
    /* loaded from: classes2.dex */
    public final class a extends Filter {
        public final /* synthetic */ cx a;

        public a(cx cxVar) {
            fs1.f(cxVar, "this$0");
            this.a = cxVar;
        }

        @Override // android.widget.Filter
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String obj;
            String obj2;
            Filter.FilterResults filterResults = new Filter.FilterResults();
            filterResults.values = this.a.f0;
            if (charSequence != null && (obj = charSequence.toString()) != null && (obj2 = StringsKt__StringsKt.K0(obj).toString()) != null) {
                String lowerCase = obj2.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (lowerCase != null) {
                    List list = this.a.f0;
                    ArrayList arrayList = new ArrayList();
                    for (Object obj3 : list) {
                        TokenType tokenType = (TokenType) obj3;
                        boolean z = true;
                        if (!(lowerCase.length() == 0)) {
                            String planeName = tokenType.getPlaneName();
                            Objects.requireNonNull(planeName, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase2 = planeName.toLowerCase(Locale.ROOT);
                            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            if (!StringsKt__StringsKt.M(lowerCase2, lowerCase, false, 2, null)) {
                                z = false;
                            }
                        }
                        if (z) {
                            arrayList.add(obj3);
                        }
                    }
                    filterResults.values = arrayList;
                }
            }
            return filterResults;
        }

        @Override // android.widget.Filter
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.values == null) {
                return;
            }
            cx cxVar = this.a;
            cxVar.g0.clear();
            List list = cxVar.g0;
            Object obj = filterResults.values;
            Objects.requireNonNull(obj, "null cannot be cast to non-null type java.util.ArrayList<net.safemoon.androidwallet.common.TokenType>{ kotlin.collections.TypeAliasesKt.ArrayList<net.safemoon.androidwallet.common.TokenType> }");
            list.addAll((ArrayList) obj);
            cxVar.notifyDataSetChanged();
        }
    }

    /* compiled from: ChainNetworkAdapter.kt */
    /* renamed from: cx$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final sk1 a;
        public final /* synthetic */ cx b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(cx cxVar, sk1 sk1Var) {
            super(sk1Var.b());
            fs1.f(cxVar, "this$0");
            fs1.f(sk1Var, "binding");
            this.b = cxVar;
            this.a = sk1Var;
        }

        public static final void c(cx cxVar, TokenType tokenType, View view) {
            fs1.f(cxVar, "this$0");
            fs1.f(tokenType, "$tt");
            cxVar.a.invoke(tokenType);
        }

        public final void b(final TokenType tokenType) {
            fs1.f(tokenType, "tokenType");
            final cx cxVar = this.b;
            sk1 sk1Var = this.a;
            sk1Var.c.setText(tokenType.getTitle());
            com.bumptech.glide.a.u(sk1Var.b).w(Integer.valueOf(tokenType.getIcon())).d0(200, 200).a(n73.v0()).I0(sk1Var.b);
            sk1Var.b().setOnClickListener(new View.OnClickListener() { // from class: dx
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    cx.b.c(cx.this, tokenType, view);
                }
            });
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public cx(tc1<? super TokenType, te4> tc1Var) {
        fs1.f(tc1Var, "callBack");
        this.a = tc1Var;
        List<TokenType> a2 = b30.a.a();
        this.f0 = a2;
        ArrayList arrayList = new ArrayList();
        this.g0 = arrayList;
        arrayList.addAll(a2);
    }

    @Override // android.widget.Filterable
    /* renamed from: d */
    public a getFilter() {
        return new a(this);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.b(this.g0.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        sk1 a2 = sk1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_chain_network, viewGroup, false));
        fs1.e(a2, "bind(\n                La…ent, false)\n            )");
        return new b(this, a2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.g0.size();
    }
}
