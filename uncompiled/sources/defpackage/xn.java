package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;

/* compiled from: BaseRequestListener.java */
/* renamed from: xn  reason: default package */
/* loaded from: classes.dex */
public class xn implements h73 {
    @Override // defpackage.h73
    public void a(ImageRequest imageRequest, Object obj, String str, boolean z) {
    }

    @Override // defpackage.jv2
    public void b(String str, String str2) {
    }

    @Override // defpackage.h73
    public void c(ImageRequest imageRequest, String str, boolean z) {
    }

    @Override // defpackage.jv2
    public void d(String str, String str2, Map<String, String> map) {
    }

    @Override // defpackage.jv2
    public void e(String str, String str2, boolean z) {
    }

    @Override // defpackage.jv2
    public boolean f(String str) {
        return false;
    }

    @Override // defpackage.h73
    public void g(ImageRequest imageRequest, String str, Throwable th, boolean z) {
    }

    @Override // defpackage.jv2
    public void h(String str, String str2, String str3) {
    }

    @Override // defpackage.jv2
    public void i(String str, String str2, Map<String, String> map) {
    }

    @Override // defpackage.jv2
    public void j(String str, String str2, Throwable th, Map<String, String> map) {
    }

    @Override // defpackage.h73
    public void k(String str) {
    }
}
