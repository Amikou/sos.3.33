package defpackage;

import java.io.IOException;

/* compiled from: Sniffer.java */
/* renamed from: xq3  reason: default package */
/* loaded from: classes.dex */
public final class xq3 {
    public static final int[] a = {1769172845, 1769172786, 1769172787, 1769172788, 1769172789, 1769172790, 1769172793, 1635148593, 1752589105, 1751479857, 1635135537, 1836069937, 1836069938, 862401121, 862401122, 862417462, 862417718, 862414134, 862414646, 1295275552, 1295270176, 1714714144, 1801741417, 1295275600, 1903435808, 1297305174, 1684175153, 1769172332, 1885955686};

    public static boolean a(int i, boolean z) {
        if ((i >>> 8) == 3368816) {
            return true;
        }
        if (i == 1751476579 && z) {
            return true;
        }
        for (int i2 : a) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean b(q11 q11Var) throws IOException {
        return c(q11Var, true, false);
    }

    public static boolean c(q11 q11Var, boolean z, boolean z2) throws IOException {
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        long length = q11Var.getLength();
        long j = -1;
        int i = (length > (-1L) ? 1 : (length == (-1L) ? 0 : -1));
        long j2 = 4096;
        if (i != 0 && length <= 4096) {
            j2 = length;
        }
        int i2 = (int) j2;
        op2 op2Var = new op2(64);
        boolean z8 = false;
        int i3 = 0;
        boolean z9 = false;
        while (i3 < i2) {
            op2Var.L(8);
            if (!q11Var.d(op2Var.d(), z8 ? 1 : 0, 8, true)) {
                break;
            }
            long F = op2Var.F();
            int n = op2Var.n();
            int i4 = 16;
            if (F == 1) {
                q11Var.n(op2Var.d(), 8, 8);
                op2Var.O(16);
                F = op2Var.w();
            } else {
                if (F == 0) {
                    long length2 = q11Var.getLength();
                    if (length2 != j) {
                        F = (length2 - q11Var.e()) + 8;
                    }
                }
                i4 = 8;
            }
            long j3 = i4;
            if (F < j3) {
                return z8;
            }
            i3 += i4;
            if (n == 1836019574) {
                i2 += (int) F;
                if (i != 0 && i2 > length) {
                    i2 = (int) length;
                }
            } else if (n == 1836019558 || n == 1836475768) {
                z3 = z8 ? 1 : 0;
                z4 = true;
                z5 = true;
                break;
            } else {
                long j4 = length;
                if ((i3 + F) - j3 >= i2) {
                    z3 = false;
                    z4 = true;
                    break;
                }
                int i5 = (int) (F - j3);
                i3 += i5;
                if (n != 1718909296) {
                    z6 = false;
                    z9 = z9;
                    if (i5 != 0) {
                        q11Var.f(i5);
                        z9 = z9;
                    }
                } else if (i5 < 8) {
                    return false;
                } else {
                    op2Var.L(i5);
                    q11Var.n(op2Var.d(), 0, i5);
                    int i6 = i5 / 4;
                    int i7 = 0;
                    while (true) {
                        if (i7 >= i6) {
                            z7 = z9;
                            break;
                        }
                        if (i7 == 1) {
                            op2Var.Q(4);
                        } else if (a(op2Var.n(), z2)) {
                            z7 = true;
                            break;
                        }
                        i7++;
                    }
                    if (!z7) {
                        return false;
                    }
                    z6 = false;
                    z9 = z7;
                }
                z8 = z6;
                length = j4;
            }
            j = -1;
            z9 = z9;
        }
        z3 = z8 ? 1 : 0;
        z4 = true;
        z5 = z3;
        return (z9 && z == z5) ? z4 : z3;
    }

    public static boolean d(q11 q11Var, boolean z) throws IOException {
        return c(q11Var, false, z);
    }
}
