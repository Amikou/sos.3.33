package defpackage;

import androidx.media3.common.j;
import androidx.media3.exoplayer.drm.DrmSession;

/* compiled from: FormatHolder.java */
/* renamed from: y81  reason: default package */
/* loaded from: classes.dex */
public final class y81 {
    public DrmSession a;
    public j b;

    public void a() {
        this.a = null;
        this.b = null;
    }
}
