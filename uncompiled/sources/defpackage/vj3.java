package defpackage;

import defpackage.l12;
import kotlin.Result;

/* compiled from: AbstractChannel.kt */
/* renamed from: vj3  reason: default package */
/* loaded from: classes2.dex */
public class vj3<E> extends tj3 {
    public final E h0;
    public final ov<te4> i0;

    /* JADX WARN: Multi-variable type inference failed */
    public vj3(E e, ov<? super te4> ovVar) {
        this.h0 = e;
        this.i0 = ovVar;
    }

    @Override // defpackage.tj3
    public void A(d00<?> d00Var) {
        ov<te4> ovVar = this.i0;
        Throwable G = d00Var.G();
        Result.a aVar = Result.Companion;
        ovVar.resumeWith(Result.m52constructorimpl(o83.a(G)));
    }

    @Override // defpackage.tj3
    public k24 B(l12.b bVar) {
        Object c = this.i0.c(te4.a, null);
        if (c == null) {
            return null;
        }
        if (ze0.a()) {
            if (!(c == qv.a)) {
                throw new AssertionError();
            }
        }
        return qv.a;
    }

    @Override // defpackage.l12
    public String toString() {
        return ff0.a(this) + '@' + ff0.b(this) + '(' + z() + ')';
    }

    @Override // defpackage.tj3
    public void y() {
        this.i0.s(qv.a);
    }

    @Override // defpackage.tj3
    public E z() {
        return this.h0;
    }
}
