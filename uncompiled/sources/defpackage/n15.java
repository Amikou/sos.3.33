package defpackage;

import com.google.android.gms.common.api.internal.BasePendingResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: n15  reason: default package */
/* loaded from: classes.dex */
public final class n15 implements m15 {
    public final /* synthetic */ k15 a;

    public n15(k15 k15Var) {
        this.a = k15Var;
    }

    @Override // defpackage.m15
    public final void a(BasePendingResult<?> basePendingResult) {
        this.a.a.remove(basePendingResult);
    }
}
