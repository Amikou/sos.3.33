package defpackage;

import defpackage.l24;
import java.util.concurrent.Executor;

/* compiled from: WorkInitializer.java */
/* renamed from: eq4  reason: default package */
/* loaded from: classes.dex */
public class eq4 {
    public final Executor a;
    public final dy0 b;
    public final rq4 c;
    public final l24 d;

    public eq4(Executor executor, dy0 dy0Var, rq4 rq4Var, l24 l24Var) {
        this.a = executor;
        this.b = dy0Var;
        this.c = rq4Var;
        this.d = l24Var;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object d() {
        for (ob4 ob4Var : this.b.j0()) {
            this.c.a(ob4Var, 1);
        }
        return null;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ void e() {
        this.d.a(new l24.a() { // from class: cq4
            @Override // defpackage.l24.a
            public final Object execute() {
                Object d;
                d = eq4.this.d();
                return d;
            }
        });
    }

    public void c() {
        this.a.execute(new Runnable() { // from class: dq4
            @Override // java.lang.Runnable
            public final void run() {
                eq4.this.e();
            }
        });
    }
}
