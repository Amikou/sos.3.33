package defpackage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.appcompat.app.a;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import net.safemoon.androidwallet.R;
import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: ApplicationDialog.kt */
/* renamed from: bh */
/* loaded from: classes2.dex */
public final class bh {
    public static final bh a = new bh();

    public static final void A0(View.OnClickListener onClickListener, Dialog dialog, View view) {
        fs1.f(onClickListener, "$onClickAction");
        fs1.f(dialog, "$dialog");
        onClickListener.onClick(null);
        dialog.dismiss();
    }

    public static final void B0(Activity activity, String str, CompoundButton compoundButton, boolean z) {
        fs1.f(activity, "$activity");
        fs1.f(str, "$preferenceKey");
        bo3.n(activity, str, Boolean.valueOf(z));
    }

    public static final void C0(Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        dialog.dismiss();
    }

    public static final void F(WeakReference<Activity> weakReference, final View.OnClickListener onClickListener, final View.OnClickListener onClickListener2) {
        fs1.f(weakReference, "activityReference");
        fs1.f(onClickListener, "onConfirmAction");
        fs1.f(onClickListener2, "onCancelAction");
        Activity activity = weakReference.get();
        fs1.d(activity);
        fs1.e(activity, "activityReference.get()!!");
        Activity activity2 = activity;
        final Dialog E = a.E(activity2);
        xm0 a2 = xm0.a(activity2.getLayoutInflater().inflate(R.layout.dialog_acknowledgment_warning, (ViewGroup) null));
        fs1.e(a2, "bind(activity.layoutInfl…wledgment_warning, null))");
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: ng
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.G(onClickListener, E, view);
            }
        });
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: rg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.H(onClickListener2, E, view);
            }
        });
        E.setContentView(a2.b());
        E.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: xf
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                bh.I(onClickListener2, dialogInterface);
            }
        });
        E.show();
    }

    public static final void G(View.OnClickListener onClickListener, Dialog dialog, View view) {
        fs1.f(onClickListener, "$onConfirmAction");
        fs1.f(dialog, "$dialog");
        onClickListener.onClick(null);
        dialog.dismiss();
    }

    public static final void H(View.OnClickListener onClickListener, Dialog dialog, View view) {
        fs1.f(onClickListener, "$onCancelAction");
        fs1.f(dialog, "$dialog");
        onClickListener.onClick(null);
        dialog.dismiss();
    }

    public static final void I(View.OnClickListener onClickListener, DialogInterface dialogInterface) {
        fs1.f(onClickListener, "$onCancelAction");
        onClickListener.onClick(null);
    }

    public static final Dialog J(WeakReference<Activity> weakReference, int i, int i2, int i3, int i4, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        xn0 a2 = xn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_refresh_wallets_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        a2.e.setText(i);
        a2.d.setText(i2);
        a2.c.setText(i3);
        a2.b.setText(i4);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: dg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.K(tc1.this, E, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: ag
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.L(tc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void K(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void L(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final Dialog M(WeakReference<Activity> weakReference, Integer num, int i, int i2, int i3, final rc1<te4> rc1Var, final rc1<te4> rc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        on0 a2 = on0.a(activity.getLayoutInflater().inflate(R.layout.dialog_dark_login_confirm, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            num.intValue();
            a2.e.setVisibility(0);
            a2.e.setText(num.intValue());
        }
        a2.d.setText(i);
        a2.c.setText(i2);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: xg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.N(rc1.this, E, view);
            }
        });
        a2.b.setText(i3);
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: ah
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.O(rc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void N(rc1 rc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (rc1Var != null) {
            rc1Var.invoke();
        }
        dialog.cancel();
    }

    public static final void O(rc1 rc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (rc1Var != null) {
            rc1Var.invoke();
        }
        dialog.cancel();
    }

    public static final Dialog P(WeakReference<Activity> weakReference, Integer num, Integer num2, String str, Integer num3, Integer num4, Integer num5, Integer num6, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        mn0 a2 = mn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            MaterialTextView materialTextView = a2.f;
            fs1.e(materialTextView, "tvDialogTitle");
            materialTextView.setVisibility(0);
            a2.f.setText(num.intValue());
        } else {
            MaterialTextView materialTextView2 = a2.f;
            fs1.e(materialTextView2, "tvDialogTitle");
            materialTextView2.setVisibility(8);
        }
        if (num2 != null) {
            a2.e.setText(num2.intValue());
        }
        if (str != null) {
            a2.e.setText(str);
        }
        if (num3 != null) {
            a2.c.setText(num3.intValue());
            MaterialButton materialButton = a2.c;
            fs1.e(materialButton, "btnPositive");
            materialButton.setVisibility(0);
            if (num5 != null) {
                a2.c.setTextColor(num5.intValue());
            }
            a2.c.setOnClickListener(new View.OnClickListener() { // from class: kg
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    bh.R(tc1.this, E, view);
                }
            });
        } else {
            MaterialButton materialButton2 = a2.c;
            fs1.e(materialButton2, "btnPositive");
            materialButton2.setVisibility(8);
        }
        if (num4 != null) {
            a2.b.setText(num4.intValue());
            MaterialButton materialButton3 = a2.b;
            fs1.e(materialButton3, "btnNegative");
            materialButton3.setVisibility(0);
            if (num6 != null) {
                a2.b.setTextColor(num6.intValue());
            }
            a2.b.setOnClickListener(new View.OnClickListener() { // from class: zf
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    bh.S(tc1.this, E, view);
                }
            });
        } else {
            MaterialButton materialButton4 = a2.b;
            fs1.e(materialButton4, "btnNegative");
            materialButton4.setVisibility(8);
        }
        a2.d.setOnClickListener(new View.OnClickListener() { // from class: lg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.T(tc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void R(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void S(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void T(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final Dialog U(WeakReference<Activity> weakReference, Integer num, Integer num2, int i, final rc1<te4> rc1Var) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        pn0 a2 = pn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_dark_register_success, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            num.intValue();
            a2.d.setVisibility(0);
            a2.d.setText(num.intValue());
        }
        if (num2 != null) {
            num2.intValue();
            a2.c.setVisibility(0);
            a2.c.setText(num2.intValue());
        }
        a2.b.setText(i);
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: yg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.W(rc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static /* synthetic */ Dialog V(WeakReference weakReference, Integer num, Integer num2, int i, rc1 rc1Var, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            num = null;
        }
        if ((i2 & 4) != 0) {
            num2 = null;
        }
        if ((i2 & 8) != 0) {
            i = R.string.action_ok;
        }
        if ((i2 & 16) != 0) {
            rc1Var = null;
        }
        return U(weakReference, num, num2, i, rc1Var);
    }

    public static final void W(rc1 rc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (rc1Var != null) {
            rc1Var.invoke();
        }
        dialog.cancel();
    }

    public static final Dialog X(WeakReference<Activity> weakReference, Integer num, Integer num2, int i, final rc1<te4> rc1Var) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        zm0 a2 = zm0.a(activity.getLayoutInflater().inflate(R.layout.dialog_alert_no_title, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            num.intValue();
            a2.d.setVisibility(0);
            a2.d.setText(num.intValue());
        }
        if (num2 != null) {
            num2.intValue();
            a2.c.setVisibility(0);
            a2.c.setText(num2.intValue());
        }
        a2.b.setText(i);
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: zg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.Z(rc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static /* synthetic */ Dialog Y(WeakReference weakReference, Integer num, Integer num2, int i, rc1 rc1Var, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            num = null;
        }
        if ((i2 & 4) != 0) {
            num2 = null;
        }
        if ((i2 & 8) != 0) {
            i = R.string.action_ok;
        }
        if ((i2 & 16) != 0) {
            rc1Var = null;
        }
        return X(weakReference, num, num2, i, rc1Var);
    }

    public static final void Z(rc1 rc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (rc1Var != null) {
            rc1Var.invoke();
        }
        dialog.cancel();
    }

    public static final Dialog a0(WeakReference<Activity> weakReference, Integer num, Integer num2, String str, int i, int i2, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        qn0 a2 = qn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_dark_reset_password_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            num.intValue();
            a2.e.setVisibility(0);
            a2.e.setText(num.intValue());
        }
        if (num2 != null) {
            num2.intValue();
            a2.d.setText(num2.intValue());
        }
        if (str != null) {
            a2.d.setText(el1.a(str, 0));
        }
        a2.c.setText(i);
        a2.b.setText(i2);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: bg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.c0(tc1.this, E, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: gg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.d0(tc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void c0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void d0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final Dialog e0(WeakReference<Activity> weakReference, Integer num, String str, String str2, int i, int i2, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        rn0 a2 = rn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_dark_security_questions_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            num.intValue();
            a2.f.setVisibility(0);
            a2.f.setText(num.intValue());
        }
        if (str != null) {
            a2.d.setText(str);
        }
        if (str2 != null) {
            a2.e.setText(str2);
        }
        a2.c.setText(i);
        a2.b.setText(i2);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: cg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.f0(tc1.this, E, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: hg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.g0(tc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void f0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void g0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void h0(WeakReference<Activity> weakReference, int i, int i2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return;
        }
        a.C0008a c0008a = new a.C0008a(activity);
        c0008a.f(activity.getResources().getText(i)).i(activity.getResources().getText(i2), ug.a);
        c0008a.create();
        c0008a.l();
    }

    public static final void i0(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
    }

    public static final Dialog j0(WeakReference<Activity> weakReference, Integer num, Integer num2, String str, int i, int i2, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        yn0 a2 = yn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_reset_password_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        if (num != null) {
            num.intValue();
            a2.e.setVisibility(0);
            a2.e.setText(num.intValue());
        }
        if (num2 != null) {
            num2.intValue();
            a2.d.setText(num2.intValue());
        }
        if (str != null) {
            a2.d.setText(el1.a(str, 0));
        }
        a2.c.setText(i);
        a2.b.setText(i2);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: eg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.l0(tc1.this, E, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: fg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.m0(tc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void l0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void m0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void n0(Activity activity, String str, final View.OnClickListener onClickListener, final View.OnClickListener onClickListener2) {
        fs1.f(activity, "activity");
        fs1.f(str, Address.TYPE_NAME);
        fs1.f(onClickListener, "onClickAction");
        fs1.f(onClickListener2, "onCancelAction");
        final Dialog E = a.E(activity);
        zn0 a2 = zn0.a(activity.getLayoutInflater().inflate(R.layout.dialog_send_token_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(activity.layoutInfl…oken_confirmation, null))");
        a2.b.setText(activity.getString(R.string.send_confirmation_popup_confirm_button));
        a2.e.setText(activity.getString(R.string.send_confirmation_popup_confirm_button));
        TextView textView = a2.d;
        lu3 lu3Var = lu3.a;
        String string = activity.getString(R.string.send_confirmation_popup_content);
        fs1.e(string, "activity.getString(R.str…nfirmation_popup_content)");
        String format = String.format(string, Arrays.copyOf(new Object[]{str}, 1));
        fs1.e(format, "java.lang.String.format(format, *args)");
        textView.setText(format);
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: qg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.o0(onClickListener, E, view);
            }
        });
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: pg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.p0(onClickListener2, E, view);
            }
        });
        E.setContentView(a2.b());
        E.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: ig
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                bh.q0(onClickListener2, dialogInterface);
            }
        });
        E.show();
    }

    public static final void o0(View.OnClickListener onClickListener, Dialog dialog, View view) {
        fs1.f(onClickListener, "$onClickAction");
        fs1.f(dialog, "$dialog");
        onClickListener.onClick(null);
        dialog.dismiss();
    }

    public static final void p0(View.OnClickListener onClickListener, Dialog dialog, View view) {
        fs1.f(onClickListener, "$onCancelAction");
        fs1.f(dialog, "$dialog");
        onClickListener.onClick(null);
        dialog.dismiss();
    }

    public static final void q0(View.OnClickListener onClickListener, DialogInterface dialogInterface) {
        fs1.f(onClickListener, "$onCancelAction");
        onClickListener.onClick(null);
    }

    public static final void r0(WeakReference<Activity> weakReference, int i, int i2, final rc1<te4> rc1Var) {
        fs1.f(weakReference, "activityReference");
        fs1.f(rc1Var, "onConfirmedClickListener");
        Activity activity = weakReference.get();
        if (activity == null) {
            return;
        }
        a.C0008a c0008a = new a.C0008a(activity);
        c0008a.f(activity.getResources().getText(i)).i(activity.getResources().getText(i2), new DialogInterface.OnClickListener() { // from class: tg
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                bh.s0(rc1.this, dialogInterface, i3);
            }
        }).g(activity.getResources().getText(R.string.cancel), vg.a);
        c0008a.create();
        c0008a.l();
    }

    public static final void s0(rc1 rc1Var, DialogInterface dialogInterface, int i) {
        fs1.f(rc1Var, "$onConfirmedClickListener");
        dialogInterface.cancel();
        rc1Var.invoke();
    }

    public static final void t0(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
    }

    public static final void u0(WeakReference<Activity> weakReference, int i, int i2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return;
        }
        a.C0008a c0008a = new a.C0008a(activity);
        c0008a.setTitle(activity.getResources().getText(R.string.error_text)).f(activity.getResources().getText(i)).i(activity.getResources().getText(i2), wg.a);
        c0008a.create();
        c0008a.l();
    }

    public static final void v0(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
    }

    public static final Dialog w0(WeakReference<Activity> weakReference, int i, int i2, int i3, int i4, final tc1<? super DialogInterface, te4> tc1Var, final tc1<? super DialogInterface, te4> tc1Var2) {
        fs1.f(weakReference, "activityReference");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final Dialog E = a.E(activity);
        jo0 a2 = jo0.a(activity.getLayoutInflater().inflate(R.layout.dialog_unlink_confirmation, (ViewGroup) null));
        fs1.e(a2, "bind(\n                it…          )\n            )");
        a2.e.setText(i);
        a2.d.setText(i2);
        a2.c.setText(i3);
        a2.b.setText(i4);
        a2.c.setOnClickListener(new View.OnClickListener() { // from class: jg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.x0(tc1.this, E, view);
            }
        });
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: yf
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.y0(tc1.this, E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
        return E;
    }

    public static final void x0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void y0(tc1 tc1Var, Dialog dialog, View view) {
        fs1.f(dialog, "$dialog");
        if (tc1Var != null) {
            tc1Var.invoke(dialog);
        }
        dialog.cancel();
    }

    public static final void z0(WeakReference<Activity> weakReference, String str, String str2, String str3, final String str4, final View.OnClickListener onClickListener) {
        fs1.f(weakReference, "activityReference");
        fs1.f(str, "title");
        fs1.f(str2, PublicResolver.FUNC_CONTENT);
        fs1.f(str3, "buttonText");
        fs1.f(str4, "preferenceKey");
        fs1.f(onClickListener, "onClickAction");
        Activity activity = weakReference.get();
        fs1.d(activity);
        fs1.e(activity, "activityReference.get()!!");
        final Activity activity2 = activity;
        final Dialog E = a.E(activity2);
        an0 a2 = an0.a(activity2.getLayoutInflater().inflate(R.layout.dialog_alert_one_button, (ViewGroup) null));
        fs1.e(a2, "bind(activity.layoutInfl…_alert_one_button, null))");
        a2.f.setText(str);
        a2.e.setText(str2);
        a2.b.setText(str3);
        a2.b.setOnClickListener(new View.OnClickListener() { // from class: og
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.A0(onClickListener, E, view);
            }
        });
        a2.c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: sg
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                bh.B0(activity2, str4, compoundButton, z);
            }
        });
        a2.d.setOnClickListener(new View.OnClickListener() { // from class: mg
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                bh.C0(E, view);
            }
        });
        E.setContentView(a2.b());
        E.show();
    }

    public final Dialog E(Context context) {
        Dialog dialog = new Dialog(context, 2132017235);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.getAttributes().gravity = 17;
            window.getAttributes().width = -1;
        }
        return dialog;
    }
}
