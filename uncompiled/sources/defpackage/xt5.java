package defpackage;

import com.google.android.gms.internal.vision.zzht;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: xt5  reason: default package */
/* loaded from: classes.dex */
public final class xt5 extends wn5<String> implements gu5, RandomAccess {
    public static final xt5 g0;
    public final List<Object> f0;

    static {
        xt5 xt5Var = new xt5();
        g0 = xt5Var;
        xt5Var.zzb();
    }

    public xt5() {
        this(10);
    }

    public static String i(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzht) {
            return ((zzht) obj).zzb();
        }
        return vs5.i((byte[]) obj);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        e();
        this.f0.add(i, (String) obj);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.wn5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @Override // defpackage.gu5
    public final List<?> b() {
        return Collections.unmodifiableList(this.f0);
    }

    @Override // defpackage.gu5
    public final gu5 c() {
        return zza() ? new uy5(this) : this;
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final void clear() {
        e();
        this.f0.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.gt5
    public final /* synthetic */ gt5 d(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f0);
            return new xt5(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.gu5
    public final void f1(zzht zzhtVar) {
        e();
        this.f0.add(zzhtVar);
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        Object obj = this.f0.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzht) {
            zzht zzhtVar = (zzht) obj;
            String zzb = zzhtVar.zzb();
            if (zzhtVar.zzc()) {
                this.f0.set(i, zzb);
            }
            return zzb;
        }
        byte[] bArr = (byte[]) obj;
        String i2 = vs5.i(bArr);
        if (vs5.h(bArr)) {
            this.f0.set(i, i2);
        }
        return i2;
    }

    @Override // defpackage.gu5
    public final Object h(int i) {
        return this.f0.get(i);
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        Object remove = this.f0.remove(i);
        ((AbstractList) this).modCount++;
        return i(remove);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        e();
        return i(this.f0.set(i, (String) obj));
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.f0.size();
    }

    public xt5(int i) {
        this(new ArrayList(i));
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final boolean addAll(int i, Collection<? extends String> collection) {
        e();
        if (collection instanceof gu5) {
            collection = ((gu5) collection).b();
        }
        boolean addAll = this.f0.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    public xt5(ArrayList<Object> arrayList) {
        this.f0 = arrayList;
    }
}
