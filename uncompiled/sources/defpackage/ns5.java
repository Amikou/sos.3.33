package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ns5  reason: default package */
/* loaded from: classes.dex */
public final class ns5 implements Runnable {
    public final /* synthetic */ String a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ zzp g0;
    public final /* synthetic */ m h0;
    public final /* synthetic */ p i0;

    public ns5(p pVar, String str, String str2, zzp zzpVar, m mVar) {
        this.i0 = pVar;
        this.a = str;
        this.f0 = str2;
        this.g0 = zzpVar;
        this.h0 = mVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ck5 ck5Var;
        d dVar;
        ArrayList<Bundle> arrayList = new ArrayList<>();
        try {
            try {
                dVar = this.i0.d;
                if (dVar == null) {
                    this.i0.a.w().l().c("Failed to get conditional properties; not connected to service", this.a, this.f0);
                    ck5Var = this.i0.a;
                } else {
                    zt2.j(this.g0);
                    arrayList = sw5.Y(dVar.g(this.a, this.f0, this.g0));
                    this.i0.D();
                    ck5Var = this.i0.a;
                }
            } catch (RemoteException e) {
                this.i0.a.w().l().d("Failed to get conditional properties; remote exception", this.a, this.f0, e);
                ck5Var = this.i0.a;
            }
            ck5Var.G().X(this.h0, arrayList);
        } catch (Throwable th) {
            this.i0.a.G().X(this.h0, arrayList);
            throw th;
        }
    }
}
