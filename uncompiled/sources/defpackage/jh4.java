package defpackage;

import defpackage.if0;
import java.nio.ByteBuffer;

/* compiled from: VideoDecoderOutputBuffer.java */
/* renamed from: jh4  reason: default package */
/* loaded from: classes.dex */
public class jh4 extends if0 {
    public int g0;
    public int h0;
    public ByteBuffer[] i0;
    public int[] j0;
    public int k0;
    public final if0.a<jh4> l0;

    @Override // defpackage.if0
    public void u() {
        this.l0.a(this);
    }
}
