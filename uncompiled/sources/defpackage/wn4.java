package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: WalletScreenMainBinding.java */
/* renamed from: wn4  reason: default package */
/* loaded from: classes2.dex */
public final class wn4 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final ry1 d;
    public final TextView e;
    public final TextView f;
    public final TextView g;

    public wn4(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, ry1 ry1Var, TextView textView, TextView textView2, TextView textView3) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = materialButton3;
        this.d = ry1Var;
        this.e = textView;
        this.f = textView2;
        this.g = textView3;
    }

    public static wn4 a(View view) {
        int i = R.id.btnBuy;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBuy);
        if (materialButton != null) {
            i = R.id.btnReceive;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnReceive);
            if (materialButton2 != null) {
                i = R.id.btnSend;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnSend);
                if (materialButton3 != null) {
                    i = R.id.lSelectTokenType;
                    View a = ai4.a(view, R.id.lSelectTokenType);
                    if (a != null) {
                        ry1 a2 = ry1.a(a);
                        i = R.id.tvMainWallet;
                        TextView textView = (TextView) ai4.a(view, R.id.tvMainWallet);
                        if (textView != null) {
                            i = R.id.tvWalletBlnc;
                            TextView textView2 = (TextView) ai4.a(view, R.id.tvWalletBlnc);
                            if (textView2 != null) {
                                i = R.id.txtSymbol;
                                TextView textView3 = (TextView) ai4.a(view, R.id.txtSymbol);
                                if (textView3 != null) {
                                    return new wn4((ConstraintLayout) view, materialButton, materialButton2, materialButton3, a2, textView, textView2, textView3);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
