package defpackage;

import java.math.BigInteger;

/* renamed from: hl4  reason: default package */
/* loaded from: classes2.dex */
public abstract class hl4 {
    public static final int[] a = {13, 41, 121, 337, 897, 2305};
    public static final byte[] b = new byte[0];
    public static final int[] c = new int[0];
    public static final pt0[] d = new pt0[0];

    /* renamed from: hl4$a */
    /* loaded from: classes2.dex */
    public static class a implements tt2 {
        public final /* synthetic */ gl4 a;
        public final /* synthetic */ qt0 b;
        public final /* synthetic */ boolean c;

        public a(gl4 gl4Var, qt0 qt0Var, boolean z) {
            this.a = gl4Var;
            this.b = qt0Var;
            this.c = z;
        }

        @Override // defpackage.tt2
        public ut2 a(ut2 ut2Var) {
            gl4 gl4Var = new gl4();
            pt0 c = this.a.c();
            if (c != null) {
                gl4Var.f(this.b.a(c));
            }
            pt0[] a = this.a.a();
            int length = a.length;
            pt0[] pt0VarArr = new pt0[length];
            for (int i = 0; i < a.length; i++) {
                pt0VarArr[i] = this.b.a(a[i]);
            }
            gl4Var.d(pt0VarArr);
            if (this.c) {
                pt0[] pt0VarArr2 = new pt0[length];
                for (int i2 = 0; i2 < length; i2++) {
                    pt0VarArr2[i2] = pt0VarArr[i2].z();
                }
                gl4Var.e(pt0VarArr2);
            }
            return gl4Var;
        }
    }

    /* renamed from: hl4$b */
    /* loaded from: classes2.dex */
    public static class b implements tt2 {
        public final /* synthetic */ int a;
        public final /* synthetic */ boolean b;
        public final /* synthetic */ pt0 c;
        public final /* synthetic */ xs0 d;

        public b(int i, boolean z, pt0 pt0Var, xs0 xs0Var) {
            this.a = i;
            this.b = z;
            this.c = pt0Var;
            this.d = xs0Var;
        }

        /* JADX WARN: Removed duplicated region for block: B:44:0x00c5 A[LOOP:0: B:43:0x00c3->B:44:0x00c5, LOOP_END] */
        /* JADX WARN: Removed duplicated region for block: B:55:0x00ea A[LOOP:1: B:54:0x00e8->B:55:0x00ea, LOOP_END] */
        @Override // defpackage.tt2
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public defpackage.ut2 a(defpackage.ut2 r12) {
            /*
                Method dump skipped, instructions count: 260
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.hl4.b.a(ut2):ut2");
        }

        public final boolean b(gl4 gl4Var, int i, boolean z) {
            return gl4Var != null && c(gl4Var.a(), i) && (!z || c(gl4Var.b(), i));
        }

        public final boolean c(pt0[] pt0VarArr, int i) {
            return pt0VarArr != null && pt0VarArr.length >= i;
        }
    }

    public static int[] c(BigInteger bigInteger) {
        if ((bigInteger.bitLength() >>> 16) == 0) {
            if (bigInteger.signum() == 0) {
                return c;
            }
            BigInteger add = bigInteger.shiftLeft(1).add(bigInteger);
            int bitLength = add.bitLength();
            int i = bitLength >> 1;
            int[] iArr = new int[i];
            BigInteger xor = add.xor(bigInteger);
            int i2 = bitLength - 1;
            int i3 = 0;
            int i4 = 1;
            int i5 = 0;
            while (i4 < i2) {
                if (xor.testBit(i4)) {
                    iArr[i3] = i5 | ((bigInteger.testBit(i4) ? -1 : 1) << 16);
                    i4++;
                    i5 = 1;
                    i3++;
                } else {
                    i5++;
                }
                i4++;
            }
            int i6 = i3 + 1;
            iArr[i3] = 65536 | i5;
            return i > i6 ? o(iArr, i6) : iArr;
        }
        throw new IllegalArgumentException("'k' must have bitlength < 2^16");
    }

    public static int[] d(int i, BigInteger bigInteger) {
        if (i == 2) {
            return c(bigInteger);
        }
        if (i < 2 || i > 16) {
            throw new IllegalArgumentException("'width' must be in the range [2, 16]");
        }
        if ((bigInteger.bitLength() >>> 16) == 0) {
            if (bigInteger.signum() == 0) {
                return c;
            }
            int bitLength = (bigInteger.bitLength() / i) + 1;
            int[] iArr = new int[bitLength];
            int i2 = 1 << i;
            int i3 = i2 - 1;
            int i4 = i2 >>> 1;
            int i5 = 0;
            int i6 = 0;
            boolean z = false;
            while (i5 <= bigInteger.bitLength()) {
                if (bigInteger.testBit(i5) == z) {
                    i5++;
                } else {
                    bigInteger = bigInteger.shiftRight(i5);
                    int intValue = bigInteger.intValue() & i3;
                    if (z) {
                        intValue++;
                    }
                    z = (intValue & i4) != 0;
                    if (z) {
                        intValue -= i2;
                    }
                    if (i6 > 0) {
                        i5--;
                    }
                    iArr[i6] = i5 | (intValue << 16);
                    i5 = i;
                    i6++;
                }
            }
            return bitLength > i6 ? o(iArr, i6) : iArr;
        }
        throw new IllegalArgumentException("'k' must have bitlength < 2^16");
    }

    public static byte[] e(BigInteger bigInteger) {
        if (bigInteger.signum() == 0) {
            return b;
        }
        BigInteger add = bigInteger.shiftLeft(1).add(bigInteger);
        int bitLength = add.bitLength() - 1;
        byte[] bArr = new byte[bitLength];
        BigInteger xor = add.xor(bigInteger);
        int i = 1;
        while (i < bitLength) {
            if (xor.testBit(i)) {
                bArr[i - 1] = (byte) (bigInteger.testBit(i) ? -1 : 1);
                i++;
            }
            i++;
        }
        bArr[bitLength - 1] = 1;
        return bArr;
    }

    public static byte[] f(int i, BigInteger bigInteger) {
        if (i == 2) {
            return e(bigInteger);
        }
        if (i < 2 || i > 8) {
            throw new IllegalArgumentException("'width' must be in the range [2, 8]");
        }
        if (bigInteger.signum() == 0) {
            return b;
        }
        int bitLength = bigInteger.bitLength() + 1;
        byte[] bArr = new byte[bitLength];
        int i2 = 1 << i;
        int i3 = i2 - 1;
        int i4 = i2 >>> 1;
        int i5 = 0;
        int i6 = 0;
        boolean z = false;
        while (i5 <= bigInteger.bitLength()) {
            if (bigInteger.testBit(i5) == z) {
                i5++;
            } else {
                bigInteger = bigInteger.shiftRight(i5);
                int intValue = bigInteger.intValue() & i3;
                if (z) {
                    intValue++;
                }
                z = (intValue & i4) != 0;
                if (z) {
                    intValue -= i2;
                }
                if (i6 > 0) {
                    i5--;
                }
                int i7 = i6 + i5;
                bArr[i7] = (byte) intValue;
                i6 = i7 + 1;
                i5 = i;
            }
        }
        return bitLength > i6 ? n(bArr, i6) : bArr;
    }

    public static gl4 g(pt0 pt0Var) {
        return h(pt0Var.i().y(pt0Var, "bc_wnaf"));
    }

    public static gl4 h(ut2 ut2Var) {
        if (ut2Var instanceof gl4) {
            return (gl4) ut2Var;
        }
        return null;
    }

    public static int i(int i) {
        return j(i, a);
    }

    public static int j(int i, int[] iArr) {
        int i2 = 0;
        while (i2 < iArr.length && i >= iArr[i2]) {
            i2++;
        }
        return i2 + 2;
    }

    public static pt0 k(pt0 pt0Var, int i, boolean z, qt0 qt0Var) {
        xs0 i2 = pt0Var.i();
        gl4 l = l(pt0Var, i, z);
        pt0 a2 = qt0Var.a(pt0Var);
        i2.C(a2, "bc_wnaf", new a(l, qt0Var, z));
        return a2;
    }

    public static gl4 l(pt0 pt0Var, int i, boolean z) {
        xs0 i2 = pt0Var.i();
        return (gl4) i2.C(pt0Var, "bc_wnaf", new b(i, z, pt0Var, i2));
    }

    public static pt0[] m(pt0[] pt0VarArr, int i) {
        pt0[] pt0VarArr2 = new pt0[i];
        System.arraycopy(pt0VarArr, 0, pt0VarArr2, 0, pt0VarArr.length);
        return pt0VarArr2;
    }

    public static byte[] n(byte[] bArr, int i) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        return bArr2;
    }

    public static int[] o(int[] iArr, int i) {
        int[] iArr2 = new int[i];
        System.arraycopy(iArr, 0, iArr2, 0, i);
        return iArr2;
    }
}
