package defpackage;

import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.room.RoomCustomToken;

/* compiled from: CustomTokenDataSource.kt */
/* renamed from: yc0  reason: default package */
/* loaded from: classes2.dex */
public final class yc0 {
    public final xc0 a;

    public yc0(xc0 xc0Var) {
        fs1.f(xc0Var, "dao");
        this.a = xc0Var;
    }

    public final Object a(String str, q70<? super te4> q70Var) {
        Object l = this.a.l(str, q70Var);
        return l == gs1.d() ? l : te4.a;
    }

    public final Object b(TokenType tokenType, q70<? super List<RoomCustomToken>> q70Var) {
        return this.a.g(tokenType.getChainId(), q70Var);
    }

    public final Object c(q70<? super List<RoomCustomToken>> q70Var) {
        return this.a.k(q70Var);
    }

    public final Object d(String str, q70<? super RoomCustomToken> q70Var) {
        return this.a.j(str, q70Var);
    }

    public final Object e(IToken iToken, q70<? super te4> q70Var) {
        Object i = this.a.i(new RoomCustomToken(iToken), q70Var);
        return i == gs1.d() ? i : te4.a;
    }

    public final Object f(String str, String str2, q70<? super te4> q70Var) {
        Object h = this.a.h(str, str2, q70Var);
        return h == gs1.d() ? h : te4.a;
    }
}
