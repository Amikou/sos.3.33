package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.lc2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: MyWalletsAdapter.kt */
/* renamed from: lc2  reason: default package */
/* loaded from: classes2.dex */
public final class lc2 extends RecyclerView.Adapter<a> {
    public final Wallet a;
    public final tc1<Wallet, te4> b;
    public final tc1<Wallet, te4> c;
    public final tc1<Wallet, te4> d;
    public final List<Wallet> e;

    /* compiled from: MyWalletsAdapter.kt */
    /* renamed from: lc2$a */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.a0 {
        public final al1 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(al1 al1Var) {
            super(al1Var.b());
            fs1.f(al1Var, "binding");
            this.a = al1Var;
        }

        public static final void e(tc1 tc1Var, Wallet wallet2, View view) {
            fs1.f(wallet2, "$wallet");
            if (tc1Var == null) {
                return;
            }
            tc1Var.invoke(wallet2);
        }

        public static final void f(tc1 tc1Var, Wallet wallet2, View view) {
            fs1.f(wallet2, "$wallet");
            if (tc1Var == null) {
                return;
            }
            tc1Var.invoke(wallet2);
        }

        public static final void g(tc1 tc1Var, Wallet wallet2, View view) {
            fs1.f(wallet2, "$wallet");
            if (tc1Var == null) {
                return;
            }
            tc1Var.invoke(wallet2);
        }

        public final void d(final Wallet wallet2, int i, final tc1<? super Wallet, te4> tc1Var, final tc1<? super Wallet, te4> tc1Var2, final tc1<? super Wallet, te4> tc1Var3) {
            fs1.f(wallet2, "wallet");
            al1 al1Var = this.a;
            Context context = al1Var.b().getContext();
            fs1.e(context, "context");
            Wallet c = e30.c(context);
            al1Var.f.setText(wallet2.displayName());
            al1Var.e.setOnClickListener(new View.OnClickListener() { // from class: ic2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    lc2.a.e(tc1.this, wallet2, view);
                }
            });
            al1Var.b.setOnClickListener(new View.OnClickListener() { // from class: jc2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    lc2.a.f(tc1.this, wallet2, view);
                }
            });
            al1Var.d.setOnClickListener(new View.OnClickListener() { // from class: kc2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    lc2.a.g(tc1.this, wallet2, view);
                }
            });
            if (fs1.b(c == null ? null : c.getAddress(), wallet2.getAddress())) {
                al1Var.f.setTypeface(null, 1);
                al1Var.f.setTextColor(context.getColorStateList(R.color.dark_green));
                al1Var.c.setBackgroundTintList(al1Var.b().getContext().getColorStateList(R.color.active_wallet));
            } else {
                al1Var.c.setBackgroundTintList(al1Var.b().getContext().getColorStateList(R.color.card_bg_1));
                al1Var.f.setTypeface(null, 0);
                al1Var.f.setTextColor(context.getColorStateList(R.color.t1));
            }
            if (!wallet2.isPrimaryWallet()) {
                al1Var.d.setVisibility(0);
                ViewGroup.LayoutParams layoutParams = al1Var.d.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                ((ViewGroup.MarginLayoutParams) layoutParams2).width = context.getResources().getDimensionPixelSize(R.dimen._22sdp);
                ((ViewGroup.MarginLayoutParams) layoutParams2).height = context.getResources().getDimensionPixelSize(R.dimen._22sdp);
                al1Var.d.setLayoutParams(layoutParams2);
                ViewGroup.LayoutParams layoutParams3 = al1Var.f.getLayoutParams();
                Objects.requireNonNull(layoutParams3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams4 = (ConstraintLayout.LayoutParams) layoutParams3;
                layoutParams4.setMarginStart(context.getResources().getDimensionPixelSize(R.dimen._8sdp));
                al1Var.f.setLayoutParams(layoutParams4);
                if (wallet2.isLinked()) {
                    al1Var.d.setImageResource(R.drawable.ic_wallet_linked);
                } else {
                    al1Var.d.setImageResource(R.drawable.ic_wallet_unlinked);
                }
            } else if (i > 1) {
                ViewGroup.LayoutParams layoutParams5 = al1Var.d.getLayoutParams();
                Objects.requireNonNull(layoutParams5, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams6 = (ConstraintLayout.LayoutParams) layoutParams5;
                ((ViewGroup.MarginLayoutParams) layoutParams6).width = context.getResources().getDimensionPixelSize(R.dimen._22sdp);
                ((ViewGroup.MarginLayoutParams) layoutParams6).height = context.getResources().getDimensionPixelSize(R.dimen._29sdp);
                al1Var.d.setLayoutParams(layoutParams6);
                ViewGroup.LayoutParams layoutParams7 = al1Var.f.getLayoutParams();
                Objects.requireNonNull(layoutParams7, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams8 = (ConstraintLayout.LayoutParams) layoutParams7;
                layoutParams8.setMarginStart(context.getResources().getDimensionPixelSize(R.dimen._8sdp));
                al1Var.f.setLayoutParams(layoutParams8);
                al1Var.d.setVisibility(0);
                if (do3.a.d(context)) {
                    al1Var.d.setImageResource(R.drawable.ic_wallets_link_all);
                } else {
                    al1Var.d.setImageResource(R.drawable.ic_wallets_unlink_all);
                }
            } else {
                ViewGroup.LayoutParams layoutParams9 = al1Var.f.getLayoutParams();
                Objects.requireNonNull(layoutParams9, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams10 = (ConstraintLayout.LayoutParams) layoutParams9;
                layoutParams10.setMarginStart(context.getResources().getDimensionPixelSize(R.dimen._3sdp));
                al1Var.f.setLayoutParams(layoutParams10);
                al1Var.d.setVisibility(8);
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public lc2(Context context, Wallet wallet2, tc1<? super Wallet, te4> tc1Var, tc1<? super Wallet, te4> tc1Var2, tc1<? super Wallet, te4> tc1Var3) {
        fs1.f(context, "context");
        this.a = wallet2;
        this.b = tc1Var;
        this.c = tc1Var2;
        this.d = tc1Var3;
        this.e = new ArrayList();
    }

    public final List<Wallet> a() {
        return this.e;
    }

    public final boolean b(RecyclerView.a0 a0Var) {
        if (a0Var == null) {
            return false;
        }
        return a0Var.itemView.isSelected();
    }

    public final void c(int i, int i2) {
        Wallet wallet2 = this.e.get(i2);
        if (this.e.get(i).isPrimaryWallet() || wallet2.isPrimaryWallet()) {
            return;
        }
        Collections.swap(this.e, i, i2);
        notifyItemMoved(i, i2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public void onBindViewHolder(a aVar, int i) {
        boolean z;
        fs1.f(aVar, "holder");
        Wallet wallet2 = this.e.get(i);
        aVar.d(wallet2, this.e.size(), this.b, this.c, this.d);
        if (!wallet2.isPrimaryWallet()) {
            String address = wallet2.getAddress();
            Wallet wallet3 = this.a;
            if (!fs1.b(address, wallet3 == null ? null : wallet3.getAddress())) {
                z = true;
                f(aVar, z);
            }
        }
        z = false;
        f(aVar, z);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        al1 a2 = al1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_wallet, viewGroup, false));
        fs1.e(a2, "bind(\n                La…ent, false)\n            )");
        return new a(a2);
    }

    public final void f(RecyclerView.a0 a0Var, boolean z) {
        if (a0Var == null) {
            return;
        }
        a0Var.itemView.setSelected(z);
    }

    public final void g(List<Wallet> list) {
        fs1.f(list, "newItemList");
        this.e.clear();
        this.e.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.e.size();
    }
}
