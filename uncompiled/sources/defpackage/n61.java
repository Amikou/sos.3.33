package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.util.b;
import defpackage.o61;
import defpackage.p61;
import defpackage.wi3;
import java.io.IOException;

/* compiled from: FlacExtractor.java */
/* renamed from: n61  reason: default package */
/* loaded from: classes.dex */
public final class n61 implements p11 {
    public final byte[] a;
    public final op2 b;
    public final boolean c;
    public final o61.a d;
    public r11 e;
    public f84 f;
    public int g;
    public Metadata h;
    public s61 i;
    public int j;
    public int k;
    public l61 l;
    public int m;
    public long n;

    static {
        m61 m61Var = m61.b;
    }

    public n61() {
        this(0);
    }

    public static /* synthetic */ p11[] k() {
        return new p11[]{new n61()};
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        if (j == 0) {
            this.g = 0;
        } else {
            l61 l61Var = this.l;
            if (l61Var != null) {
                l61Var.h(j2);
            }
        }
        this.n = j2 != 0 ? -1L : 0L;
        this.m = 0;
        this.b.L(0);
    }

    public final long d(op2 op2Var, boolean z) {
        boolean z2;
        ii.e(this.i);
        int e = op2Var.e();
        while (e <= op2Var.f() - 16) {
            op2Var.P(e);
            if (o61.d(op2Var, this.i, this.k, this.d)) {
                op2Var.P(e);
                return this.d.a;
            }
            e++;
        }
        if (z) {
            while (e <= op2Var.f() - this.j) {
                op2Var.P(e);
                try {
                    z2 = o61.d(op2Var, this.i, this.k, this.d);
                } catch (IndexOutOfBoundsException unused) {
                    z2 = false;
                }
                if (op2Var.e() <= op2Var.f() ? z2 : false) {
                    op2Var.P(e);
                    return this.d.a;
                }
                e++;
            }
            op2Var.P(op2Var.f());
            return -1L;
        }
        op2Var.P(e);
        return -1L;
    }

    public final void e(q11 q11Var) throws IOException {
        this.k = p61.b(q11Var);
        ((r11) b.j(this.e)).p(h(q11Var.getPosition(), q11Var.getLength()));
        this.g = 5;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        int i = this.g;
        if (i == 0) {
            n(q11Var);
            return 0;
        } else if (i == 1) {
            i(q11Var);
            return 0;
        } else if (i == 2) {
            p(q11Var);
            return 0;
        } else if (i == 3) {
            o(q11Var);
            return 0;
        } else if (i == 4) {
            e(q11Var);
            return 0;
        } else if (i == 5) {
            return m(q11Var, ot2Var);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        p61.c(q11Var, false);
        return p61.a(q11Var);
    }

    public final wi3 h(long j, long j2) {
        ii.e(this.i);
        s61 s61Var = this.i;
        if (s61Var.k != null) {
            return new r61(s61Var, j);
        }
        if (j2 != -1 && s61Var.j > 0) {
            l61 l61Var = new l61(s61Var, this.k, j, j2);
            this.l = l61Var;
            return l61Var.b();
        }
        return new wi3.b(s61Var.f());
    }

    public final void i(q11 q11Var) throws IOException {
        byte[] bArr = this.a;
        q11Var.n(bArr, 0, bArr.length);
        q11Var.j();
        this.g = 2;
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.e = r11Var;
        this.f = r11Var.f(0, 1);
        r11Var.m();
    }

    public final void l() {
        ((f84) b.j(this.f)).b((this.n * 1000000) / ((s61) b.j(this.i)).e, 1, this.m, 0, null);
    }

    public final int m(q11 q11Var, ot2 ot2Var) throws IOException {
        boolean z;
        ii.e(this.f);
        ii.e(this.i);
        l61 l61Var = this.l;
        if (l61Var != null && l61Var.d()) {
            return this.l.c(q11Var, ot2Var);
        }
        if (this.n == -1) {
            this.n = o61.i(q11Var, this.i);
            return 0;
        }
        int f = this.b.f();
        if (f < 32768) {
            int read = q11Var.read(this.b.d(), f, 32768 - f);
            z = read == -1;
            if (!z) {
                this.b.O(f + read);
            } else if (this.b.a() == 0) {
                l();
                return -1;
            }
        } else {
            z = false;
        }
        int e = this.b.e();
        int i = this.m;
        int i2 = this.j;
        if (i < i2) {
            op2 op2Var = this.b;
            op2Var.Q(Math.min(i2 - i, op2Var.a()));
        }
        long d = d(this.b, z);
        int e2 = this.b.e() - e;
        this.b.P(e);
        this.f.a(this.b, e2);
        this.m += e2;
        if (d != -1) {
            l();
            this.m = 0;
            this.n = d;
        }
        if (this.b.a() < 16) {
            int a = this.b.a();
            System.arraycopy(this.b.d(), this.b.e(), this.b.d(), 0, a);
            this.b.P(0);
            this.b.O(a);
        }
        return 0;
    }

    public final void n(q11 q11Var) throws IOException {
        this.h = p61.d(q11Var, !this.c);
        this.g = 1;
    }

    public final void o(q11 q11Var) throws IOException {
        p61.a aVar = new p61.a(this.i);
        boolean z = false;
        while (!z) {
            z = p61.e(q11Var, aVar);
            this.i = (s61) b.j(aVar.a);
        }
        ii.e(this.i);
        this.j = Math.max(this.i.c, 6);
        ((f84) b.j(this.f)).f(this.i.g(this.a, this.h));
        this.g = 4;
    }

    public final void p(q11 q11Var) throws IOException {
        p61.i(q11Var);
        this.g = 3;
    }

    public n61(int i) {
        this.a = new byte[42];
        this.b = new op2(new byte[32768], 0);
        this.c = (i & 1) != 0;
        this.d = new o61.a();
        this.g = 0;
    }
}
