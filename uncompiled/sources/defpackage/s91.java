package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentChainNetworkBinding.java */
/* renamed from: s91  reason: default package */
/* loaded from: classes2.dex */
public final class s91 {
    public final RecyclerView a;
    public final zd3 b;
    public final rp3 c;

    public s91(ConstraintLayout constraintLayout, RecyclerView recyclerView, zd3 zd3Var, rp3 rp3Var) {
        this.a = recyclerView;
        this.b = zd3Var;
        this.c = rp3Var;
    }

    public static s91 a(View view) {
        int i = R.id.rvChainList;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvChainList);
        if (recyclerView != null) {
            i = R.id.searchBar;
            View a = ai4.a(view, R.id.searchBar);
            if (a != null) {
                zd3 a2 = zd3.a(a);
                View a3 = ai4.a(view, R.id.toolbar);
                if (a3 != null) {
                    return new s91((ConstraintLayout) view, recyclerView, a2, rp3.a(a3));
                }
                i = R.id.toolbar;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
