package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.clearcut.zzc;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

/* renamed from: dd5  reason: default package */
/* loaded from: classes.dex */
public final class dd5 implements Parcelable.Creator<zzc> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzc createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        long j = 0;
        long j2 = 0;
        boolean z = false;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 1) {
                z = SafeParcelReader.w(parcel, C);
            } else if (v == 2) {
                j2 = SafeParcelReader.F(parcel, C);
            } else if (v != 3) {
                SafeParcelReader.I(parcel, C);
            } else {
                j = SafeParcelReader.F(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzc(z, j, j2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzc[] newArray(int i) {
        return new zzc[i];
    }
}
