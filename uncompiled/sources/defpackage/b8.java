package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivitySwitchWalletBinding.java */
/* renamed from: b8  reason: default package */
/* loaded from: classes2.dex */
public final class b8 {
    public final ConstraintLayout a;
    public final RecyclerView b;
    public final g74 c;

    public b8(ConstraintLayout constraintLayout, RecyclerView recyclerView, g74 g74Var) {
        this.a = constraintLayout;
        this.b = recyclerView;
        this.c = g74Var;
    }

    public static b8 a(View view) {
        int i = R.id.rvWallets;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvWallets);
        if (recyclerView != null) {
            i = R.id.toolbarParent;
            View a = ai4.a(view, R.id.toolbarParent);
            if (a != null) {
                return new b8((ConstraintLayout) view, recyclerView, g74.a(a));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static b8 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static b8 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_switch_wallet, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
