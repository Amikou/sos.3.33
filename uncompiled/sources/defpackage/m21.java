package defpackage;

import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: m21  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class m21 implements Executor {
    public static final Executor a = new m21();

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        runnable.run();
    }
}
