package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.startup.InitializationProvider;
import androidx.startup.StartupException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: AppInitializer.java */
/* renamed from: sf  reason: default package */
/* loaded from: classes.dex */
public final class sf {
    public static volatile sf d;
    public static final Object e = new Object();
    public final Context c;
    public final Set<Class<? extends rq1<?>>> b = new HashSet();
    public final Map<Class<?>, Object> a = new HashMap();

    public sf(Context context) {
        this.c = context.getApplicationContext();
    }

    public static sf d(Context context) {
        if (d == null) {
            synchronized (e) {
                if (d == null) {
                    d = new sf(context);
                }
            }
        }
        return d;
    }

    public void a() {
        try {
            try {
                s74.a("Startup");
                b(this.c.getPackageManager().getProviderInfo(new ComponentName(this.c.getPackageName(), InitializationProvider.class.getName()), 128).metaData);
            } catch (PackageManager.NameNotFoundException e2) {
                throw new StartupException(e2);
            }
        } finally {
            s74.b();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public void b(Bundle bundle) {
        String string = this.c.getString(o13.androidx_startup);
        if (bundle != null) {
            try {
                HashSet hashSet = new HashSet();
                for (String str : bundle.keySet()) {
                    if (string.equals(bundle.getString(str, null))) {
                        Class<?> cls = Class.forName(str);
                        if (rq1.class.isAssignableFrom(cls)) {
                            this.b.add(cls);
                        }
                    }
                }
                for (Class<? extends rq1<?>> cls2 : this.b) {
                    c(cls2, hashSet);
                }
            } catch (ClassNotFoundException e2) {
                throw new StartupException(e2);
            }
        }
    }

    public final <T> T c(Class<? extends rq1<?>> cls, Set<Class<?>> set) {
        T t;
        if (s74.d()) {
            try {
                s74.a(cls.getSimpleName());
            } finally {
                s74.b();
            }
        }
        if (!set.contains(cls)) {
            if (!this.a.containsKey(cls)) {
                set.add(cls);
                rq1<?> newInstance = cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                List<Class<? extends rq1<?>>> a = newInstance.a();
                if (!a.isEmpty()) {
                    for (Class<? extends rq1<?>> cls2 : a) {
                        if (!this.a.containsKey(cls2)) {
                            c(cls2, set);
                        }
                    }
                }
                t = (T) newInstance.b(this.c);
                set.remove(cls);
                this.a.put(cls, t);
            } else {
                t = (T) this.a.get(cls);
            }
            return t;
        }
        throw new IllegalStateException(String.format("Cannot initialize %s. Cycle detected.", cls.getName()));
    }
}
