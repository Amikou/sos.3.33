package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: u55  reason: default package */
/* loaded from: classes.dex */
public final class u55 extends c55 {
    public final List<String> g0;
    public final List<z55> h0;
    public wk5 i0;

    public u55(u55 u55Var) {
        super(u55Var.a);
        ArrayList arrayList = new ArrayList(u55Var.g0.size());
        this.g0 = arrayList;
        arrayList.addAll(u55Var.g0);
        ArrayList arrayList2 = new ArrayList(u55Var.h0.size());
        this.h0 = arrayList2;
        arrayList2.addAll(u55Var.h0);
        this.i0 = u55Var.i0;
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        wk5 c = this.i0.c();
        for (int i = 0; i < this.g0.size(); i++) {
            if (i < list.size()) {
                c.f(this.g0.get(i), wk5Var.a(list.get(i)));
            } else {
                c.f(this.g0.get(i), z55.X);
            }
        }
        for (z55 z55Var : this.h0) {
            z55 a = c.a(z55Var);
            if (a instanceof a65) {
                a = c.a(z55Var);
            }
            if (a instanceof v45) {
                return ((v45) a).a();
            }
        }
        return z55.X;
    }

    @Override // defpackage.c55, defpackage.z55
    public final z55 m() {
        return new u55(this);
    }

    public u55(String str, List<z55> list, List<z55> list2, wk5 wk5Var) {
        super(str);
        this.g0 = new ArrayList();
        this.i0 = wk5Var;
        if (!list.isEmpty()) {
            for (z55 z55Var : list) {
                this.g0.add(z55Var.zzc());
            }
        }
        this.h0 = new ArrayList(list2);
    }
}
