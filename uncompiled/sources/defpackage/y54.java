package defpackage;

import java.util.ArrayList;

/* compiled from: Timber.java */
/* renamed from: y54  reason: default package */
/* loaded from: classes3.dex */
public final class y54 {
    public static final b[] a;
    public static volatile b[] b;
    public static final b c;

    /* compiled from: Timber.java */
    /* renamed from: y54$a */
    /* loaded from: classes3.dex */
    public static class a extends b {
        @Override // defpackage.y54.b
        public void a(String str, Object... objArr) {
            for (b bVar : y54.b) {
                bVar.a(str, objArr);
            }
        }
    }

    /* compiled from: Timber.java */
    /* renamed from: y54$b */
    /* loaded from: classes3.dex */
    public static abstract class b {
        public final ThreadLocal<String> a = new ThreadLocal<>();

        public abstract void a(String str, Object... objArr);
    }

    static {
        b[] bVarArr = new b[0];
        a = bVarArr;
        new ArrayList();
        b = bVarArr;
        c = new a();
    }

    public static b a(String str) {
        for (b bVar : b) {
            bVar.a.set(str);
        }
        return c;
    }
}
