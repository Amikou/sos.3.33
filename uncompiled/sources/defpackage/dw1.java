package defpackage;

import com.fasterxml.jackson.core.io.a;
import com.fasterxml.jackson.core.io.g;
import com.fasterxml.jackson.core.util.c;
import java.lang.ref.SoftReference;

/* compiled from: JsonStringEncoder.java */
/* renamed from: dw1  reason: default package */
/* loaded from: classes.dex */
public final class dw1 {
    public static final char[] d = a.d();
    public static final byte[] e = a.c();
    public static final ThreadLocal<SoftReference<dw1>> f = new ThreadLocal<>();
    public c a;
    public ms b;
    public final char[] c;

    public dw1() {
        this.c = r0;
        char[] cArr = {'\\', 0, '0', '0'};
    }

    public static int d(int i, int i2) {
        if (i2 < 56320 || i2 > 57343) {
            throw new IllegalArgumentException("Broken surrogate pair: first char 0x" + Integer.toHexString(i) + ", second 0x" + Integer.toHexString(i2) + "; illegal combination");
        }
        return ((i - 55296) << 10) + 65536 + (i2 - 56320);
    }

    public static void e(int i) {
        throw new IllegalArgumentException(g.c(i));
    }

    public static dw1 g() {
        ThreadLocal<SoftReference<dw1>> threadLocal = f;
        SoftReference<dw1> softReference = threadLocal.get();
        dw1 dw1Var = softReference == null ? null : softReference.get();
        if (dw1Var == null) {
            dw1 dw1Var2 = new dw1();
            threadLocal.set(new SoftReference<>(dw1Var2));
            return dw1Var2;
        }
        return dw1Var;
    }

    public final int a(int i, int i2, ms msVar, int i3) {
        msVar.m(i3);
        msVar.b(92);
        if (i2 < 0) {
            msVar.b(117);
            if (i > 255) {
                int i4 = i >> 8;
                byte[] bArr = e;
                msVar.b(bArr[i4 >> 4]);
                msVar.b(bArr[i4 & 15]);
                i &= 255;
            } else {
                msVar.b(48);
                msVar.b(48);
            }
            byte[] bArr2 = e;
            msVar.b(bArr2[i >> 4]);
            msVar.b(bArr2[i & 15]);
        } else {
            msVar.b((byte) i2);
        }
        return msVar.h();
    }

    public final int b(int i, char[] cArr) {
        cArr[1] = (char) i;
        return 2;
    }

    public final int c(int i, char[] cArr) {
        cArr[1] = 'u';
        char[] cArr2 = d;
        cArr[4] = cArr2[i >> 4];
        cArr[5] = cArr2[i & 15];
        return 6;
    }

    /* JADX WARN: Removed duplicated region for block: B:47:0x00d6  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00dc A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public byte[] f(java.lang.String r11) {
        /*
            Method dump skipped, instructions count: 239
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dw1.f(java.lang.String):byte[]");
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0028, code lost:
        r8 = r6 + 1;
        r6 = r12.charAt(r6);
        r9 = r2[r6];
     */
    /* JADX WARN: Code restructure failed: missing block: B:12:0x0030, code lost:
        if (r9 >= 0) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0032, code lost:
        r6 = c(r6, r11.c);
     */
    /* JADX WARN: Code restructure failed: missing block: B:14:0x0039, code lost:
        r6 = b(r9, r11.c);
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x003f, code lost:
        r9 = r7 + r6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x0042, code lost:
        if (r9 <= r1.length) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0044, code lost:
        r9 = r1.length - r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0046, code lost:
        if (r9 <= 0) goto L19;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0048, code lost:
        java.lang.System.arraycopy(r11.c, 0, r1, r7, r9);
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x004d, code lost:
        r1 = r0.n();
        r6 = r6 - r9;
        java.lang.System.arraycopy(r11.c, r9, r1, 0, r6);
        r7 = r6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0059, code lost:
        java.lang.System.arraycopy(r11.c, 0, r1, r7, r6);
        r7 = r9;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public char[] h(java.lang.String r12) {
        /*
            r11 = this;
            com.fasterxml.jackson.core.util.c r0 = r11.a
            if (r0 != 0) goto Lc
            com.fasterxml.jackson.core.util.c r0 = new com.fasterxml.jackson.core.util.c
            r1 = 0
            r0.<init>(r1)
            r11.a = r0
        Lc:
            char[] r1 = r0.k()
            int[] r2 = com.fasterxml.jackson.core.io.a.e()
            int r3 = r2.length
            int r4 = r12.length()
            r5 = 0
            r6 = r5
            r7 = r6
        L1c:
            if (r6 >= r4) goto L75
        L1e:
            char r8 = r12.charAt(r6)
            if (r8 >= r3) goto L61
            r9 = r2[r8]
            if (r9 == 0) goto L61
            int r8 = r6 + 1
            char r6 = r12.charAt(r6)
            r9 = r2[r6]
            if (r9 >= 0) goto L39
            char[] r9 = r11.c
            int r6 = r11.c(r6, r9)
            goto L3f
        L39:
            char[] r6 = r11.c
            int r6 = r11.b(r9, r6)
        L3f:
            int r9 = r7 + r6
            int r10 = r1.length
            if (r9 <= r10) goto L59
            int r9 = r1.length
            int r9 = r9 - r7
            if (r9 <= 0) goto L4d
            char[] r10 = r11.c
            java.lang.System.arraycopy(r10, r5, r1, r7, r9)
        L4d:
            char[] r1 = r0.n()
            int r6 = r6 - r9
            char[] r7 = r11.c
            java.lang.System.arraycopy(r7, r9, r1, r5, r6)
            r7 = r6
            goto L5f
        L59:
            char[] r10 = r11.c
            java.lang.System.arraycopy(r10, r5, r1, r7, r6)
            r7 = r9
        L5f:
            r6 = r8
            goto L1c
        L61:
            int r9 = r1.length
            if (r7 < r9) goto L69
            char[] r1 = r0.n()
            r7 = r5
        L69:
            int r9 = r7 + 1
            r1[r7] = r8
            int r6 = r6 + 1
            if (r6 < r4) goto L73
            r7 = r9
            goto L75
        L73:
            r7 = r9
            goto L1e
        L75:
            r0.z(r7)
            char[] r12 = r0.g()
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dw1.h(java.lang.String):char[]");
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0041, code lost:
        if (r5 < r2.length) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x0043, code lost:
        r2 = r0.f();
        r5 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0048, code lost:
        r7 = r4 + 1;
        r4 = r11.charAt(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x004e, code lost:
        if (r4 > 127) goto L28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0050, code lost:
        r5 = a(r4, r6[r4], r0, r5);
        r2 = r0.g();
        r4 = r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x005e, code lost:
        if (r4 > 2047) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0060, code lost:
        r6 = r5 + 1;
        r2[r5] = (byte) ((r4 >> 6) | 192);
        r4 = (r4 & '?') | 128;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x006d, code lost:
        r5 = r4;
        r4 = r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x0074, code lost:
        if (r4 < 55296) goto L58;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0079, code lost:
        if (r4 <= 57343) goto L43;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x007f, code lost:
        if (r4 <= 56319) goto L46;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x0081, code lost:
        e(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x0084, code lost:
        if (r7 < r1) goto L48;
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x0086, code lost:
        e(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x0089, code lost:
        r6 = r7 + 1;
        r4 = d(r4, r11.charAt(r7));
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x0096, code lost:
        if (r4 <= 1114111) goto L51;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x0098, code lost:
        e(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x009b, code lost:
        r7 = r5 + 1;
        r2[r5] = (byte) ((r4 >> 18) | 240);
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x00a5, code lost:
        if (r7 < r2.length) goto L54;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00a7, code lost:
        r2 = r0.f();
        r7 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:46:0x00ac, code lost:
        r5 = r7 + 1;
        r2[r7] = (byte) (((r4 >> 12) & 63) | 128);
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x00b8, code lost:
        if (r5 < r2.length) goto L57;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00ba, code lost:
        r2 = r0.f();
        r5 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x00bf, code lost:
        r7 = r5 + 1;
        r2[r5] = (byte) (((r4 >> 6) & 63) | 128);
        r5 = (r4 & 63) | 128;
        r4 = r6;
        r6 = r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00d2, code lost:
        r6 = r5 + 1;
        r2[r5] = (byte) ((r4 >> '\f') | 224);
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00dc, code lost:
        if (r6 < r2.length) goto L61;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00de, code lost:
        r2 = r0.f();
        r6 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00e3, code lost:
        r2[r6] = (byte) (((r4 >> 6) & 63) | 128);
        r4 = (r4 & '?') | 128;
        r6 = r6 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x00f6, code lost:
        if (r6 < r2.length) goto L36;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x00f8, code lost:
        r2 = r0.f();
        r6 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x00fd, code lost:
        r2[r6] = (byte) r5;
        r5 = r6 + 1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public byte[] i(java.lang.String r11) {
        /*
            Method dump skipped, instructions count: 268
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dw1.i(java.lang.String):byte[]");
    }
}
