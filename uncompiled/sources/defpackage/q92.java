package defpackage;

import android.net.Uri;

/* compiled from: MoonPay.kt */
/* renamed from: q92  reason: default package */
/* loaded from: classes2.dex */
public final class q92 {
    public static final q92 a = new q92();

    public final Uri.Builder a(String str, String str2) {
        fs1.f(str, "walletAddress");
        fs1.f(str2, "currencyCode");
        Uri.Builder buildUpon = Uri.parse("https://buy.moonpay.com").buildUpon();
        buildUpon.appendQueryParameter("apiKey", a4.c);
        buildUpon.appendQueryParameter("walletAddress", str);
        if (str2.length() > 0) {
            buildUpon.appendQueryParameter("currencyCode", str2);
        }
        buildUpon.appendQueryParameter("colorCode", "#00a79d");
        buildUpon.appendQueryParameter("baseCurrencyCode", "USD");
        return buildUpon;
    }
}
