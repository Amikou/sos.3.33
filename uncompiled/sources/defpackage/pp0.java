package defpackage;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Builders.common.kt */
/* renamed from: pp0  reason: default package */
/* loaded from: classes2.dex */
public final class pp0<T> extends vd3<T> {
    public static final /* synthetic */ AtomicIntegerFieldUpdater h0 = AtomicIntegerFieldUpdater.newUpdater(pp0.class, "_decision");
    private volatile /* synthetic */ int _decision;

    public pp0(CoroutineContext coroutineContext, q70<? super T> q70Var) {
        super(coroutineContext, q70Var);
        this._decision = 0;
    }

    @Override // defpackage.vd3, defpackage.bu1
    public void E(Object obj) {
        H0(obj);
    }

    @Override // defpackage.vd3, defpackage.t4
    public void H0(Object obj) {
        if (N0()) {
            return;
        }
        op0.c(IntrinsicsKt__IntrinsicsJvmKt.c(this.g0), w30.a(obj, this.g0), null, 2, null);
    }

    public final Object M0() {
        if (O0()) {
            return gs1.d();
        }
        Object h = cu1.h(a0());
        if (h instanceof t30) {
            throw ((t30) h).a;
        }
        return h;
    }

    public final boolean N0() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!h0.compareAndSet(this, 0, 2));
        return true;
    }

    public final boolean O0() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!h0.compareAndSet(this, 0, 1));
        return true;
    }
}
