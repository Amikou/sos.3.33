package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: iu5  reason: default package */
/* loaded from: classes.dex */
public final class iu5 extends n55 {
    public final /* synthetic */ lu5 e;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public iu5(lu5 lu5Var, tl5 tl5Var) {
        super(tl5Var);
        this.e = lu5Var;
    }

    @Override // defpackage.n55
    public final void a() {
        lu5 lu5Var = this.e;
        lu5Var.d.e();
        lu5Var.d(false, false, lu5Var.d.a.a().b());
        lu5Var.d.a.d().h(lu5Var.d.a.a().b());
    }
}
