package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.stats.WakeLockEvent;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: y35  reason: default package */
/* loaded from: classes.dex */
public final class y35 implements Parcelable.Creator<WakeLockEvent> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ WakeLockEvent createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        boolean z = false;
        String str = null;
        ArrayList<String> arrayList = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        float f = 0.0f;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 1:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 2:
                    j = SafeParcelReader.F(parcel, C);
                    break;
                case 3:
                case 7:
                case 9:
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
                case 4:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 5:
                    i3 = SafeParcelReader.E(parcel, C);
                    break;
                case 6:
                    arrayList = SafeParcelReader.r(parcel, C);
                    break;
                case 8:
                    j2 = SafeParcelReader.F(parcel, C);
                    break;
                case 10:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 11:
                    i2 = SafeParcelReader.E(parcel, C);
                    break;
                case 12:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 13:
                    str4 = SafeParcelReader.p(parcel, C);
                    break;
                case 14:
                    i4 = SafeParcelReader.E(parcel, C);
                    break;
                case 15:
                    f = SafeParcelReader.A(parcel, C);
                    break;
                case 16:
                    j3 = SafeParcelReader.F(parcel, C);
                    break;
                case 17:
                    str5 = SafeParcelReader.p(parcel, C);
                    break;
                case 18:
                    z = SafeParcelReader.w(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new WakeLockEvent(i, j, i2, str, i3, arrayList, str2, j2, i4, str3, str4, f, j3, str5, z);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ WakeLockEvent[] newArray(int i) {
        return new WakeLockEvent[i];
    }
}
