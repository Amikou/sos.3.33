package defpackage;

import com.google.android.gms.tasks.c;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: hs5  reason: default package */
/* loaded from: classes.dex */
public final class hs5 implements Runnable {
    public final /* synthetic */ c a;
    public final /* synthetic */ up5 f0;

    public hs5(up5 up5Var, c cVar) {
        this.f0 = up5Var;
        this.a = cVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        im2 im2Var;
        im2 im2Var2;
        obj = this.f0.b;
        synchronized (obj) {
            im2Var = this.f0.c;
            if (im2Var != null) {
                im2Var2 = this.f0.c;
                im2Var2.a(this.a);
            }
        }
    }
}
