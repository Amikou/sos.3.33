package defpackage;

import java.security.InvalidKeyException;

/* compiled from: ChaCha20Poly1305.java */
/* renamed from: zw  reason: default package */
/* loaded from: classes2.dex */
public final class zw extends ax {
    public zw(byte[] bArr) throws InvalidKeyException {
        super(bArr);
    }

    @Override // defpackage.ax
    public yw g(byte[] bArr, int i) throws InvalidKeyException {
        return new xw(bArr, i);
    }
}
