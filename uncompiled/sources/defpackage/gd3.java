package defpackage;

import android.content.Context;
import android.os.Build;
import androidx.work.a;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;

/* compiled from: Schedulers.java */
/* renamed from: gd3  reason: default package */
/* loaded from: classes.dex */
public class gd3 {
    public static final String a = v12.f("Schedulers");

    public static cd3 a(Context context, hq4 hq4Var) {
        if (Build.VERSION.SDK_INT >= 23) {
            y24 y24Var = new y24(context, hq4Var);
            so2.a(context, SystemJobService.class, true);
            v12.c().a(a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return y24Var;
        }
        cd3 c = c(context);
        if (c == null) {
            n24 n24Var = new n24(context);
            so2.a(context, SystemAlarmService.class, true);
            v12.c().a(a, "Created SystemAlarmScheduler", new Throwable[0]);
            return n24Var;
        }
        return c;
    }

    public static void b(a aVar, WorkDatabase workDatabase, List<cd3> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        uq4 P = workDatabase.P();
        workDatabase.e();
        try {
            List<tq4> e = P.e(aVar.h());
            List<tq4> s = P.s(200);
            if (e != null && e.size() > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                for (tq4 tq4Var : e) {
                    P.b(tq4Var.a, currentTimeMillis);
                }
            }
            workDatabase.E();
            if (e != null && e.size() > 0) {
                tq4[] tq4VarArr = (tq4[]) e.toArray(new tq4[e.size()]);
                for (cd3 cd3Var : list) {
                    if (cd3Var.a()) {
                        cd3Var.e(tq4VarArr);
                    }
                }
            }
            if (s == null || s.size() <= 0) {
                return;
            }
            tq4[] tq4VarArr2 = (tq4[]) s.toArray(new tq4[s.size()]);
            for (cd3 cd3Var2 : list) {
                if (!cd3Var2.a()) {
                    cd3Var2.e(tq4VarArr2);
                }
            }
        } finally {
            workDatabase.j();
        }
    }

    public static cd3 c(Context context) {
        try {
            cd3 cd3Var = (cd3) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            v12.c().a(a, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
            return cd3Var;
        } catch (Throwable th) {
            v12.c().a(a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
