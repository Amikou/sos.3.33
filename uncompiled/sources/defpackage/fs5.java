package defpackage;

import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: fs5  reason: default package */
/* loaded from: classes.dex */
public final class fs5 implements Runnable {
    public final /* synthetic */ m a;
    public final /* synthetic */ String f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ boolean h0;
    public final /* synthetic */ AppMeasurementDynamiteService i0;

    public fs5(AppMeasurementDynamiteService appMeasurementDynamiteService, m mVar, String str, String str2, boolean z) {
        this.i0 = appMeasurementDynamiteService;
        this.a = mVar;
        this.f0 = str;
        this.g0 = str2;
        this.h0 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.i0.a.R().Q(this.a, this.f0, this.g0, this.h0);
    }
}
