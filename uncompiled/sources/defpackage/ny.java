package defpackage;

import androidx.media3.common.j;
import java.io.IOException;
import java.util.List;

/* compiled from: ChunkExtractor.java */
/* renamed from: ny  reason: default package */
/* loaded from: classes.dex */
public interface ny {

    /* compiled from: ChunkExtractor.java */
    /* renamed from: ny$a */
    /* loaded from: classes.dex */
    public interface a {
        ny a(int i, j jVar, boolean z, List<j> list, f84 f84Var, ks2 ks2Var);
    }

    /* compiled from: ChunkExtractor.java */
    /* renamed from: ny$b */
    /* loaded from: classes.dex */
    public interface b {
        f84 f(int i, int i2);
    }

    void a();

    boolean b(q11 q11Var) throws IOException;

    void c(b bVar, long j, long j2);

    j[] d();

    py e();
}
