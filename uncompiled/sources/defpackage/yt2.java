package defpackage;

import java.util.Objects;

/* compiled from: Preconditions.java */
/* renamed from: yt2  reason: default package */
/* loaded from: classes.dex */
public final class yt2 {
    public static <T> void a(T t, Class<T> cls) {
        if (t != null) {
            return;
        }
        throw new IllegalStateException(cls.getCanonicalName() + " must be set");
    }

    public static <T> T b(T t) {
        Objects.requireNonNull(t);
        return t;
    }

    public static <T> T c(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }
}
