package defpackage;

import com.onesignal.WebViewManager;
import org.json.JSONObject;

/* compiled from: OSInAppMessageContent.kt */
/* renamed from: qj2  reason: default package */
/* loaded from: classes2.dex */
public class qj2 {
    public String a;
    public boolean b;
    public boolean c;
    public boolean d;
    public WebViewManager.Position e;
    public Double f;
    public int g;

    public qj2(JSONObject jSONObject) {
        fs1.f(jSONObject, "jsonObject");
        this.b = true;
        this.c = true;
        this.a = jSONObject.optString("html");
        this.f = Double.valueOf(jSONObject.optDouble("display_duration"));
        JSONObject optJSONObject = jSONObject.optJSONObject("styles");
        this.b = !(optJSONObject != null ? optJSONObject.optBoolean("remove_height_margin", false) : false);
        this.c = !(optJSONObject != null ? optJSONObject.optBoolean("remove_width_margin", false) : false);
        this.d = !this.b;
    }

    public final String a() {
        return this.a;
    }

    public final Double b() {
        return this.f;
    }

    public final WebViewManager.Position c() {
        return this.e;
    }

    public final int d() {
        return this.g;
    }

    public final boolean e() {
        return this.b;
    }

    public final boolean f() {
        return this.c;
    }

    public final boolean g() {
        return this.d;
    }

    public final void h(String str) {
        this.a = str;
    }

    public final void i(WebViewManager.Position position) {
        this.e = position;
    }

    public final void j(int i) {
        this.g = i;
    }
}
