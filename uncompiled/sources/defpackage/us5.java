package defpackage;

import android.content.ComponentName;
import com.google.android.gms.measurement.internal.p;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: us5  reason: default package */
/* loaded from: classes.dex */
public final class us5 implements Runnable {
    public final /* synthetic */ ComponentName a;
    public final /* synthetic */ dt5 f0;

    public us5(dt5 dt5Var, ComponentName componentName) {
        this.f0 = dt5Var;
        this.a = componentName;
    }

    @Override // java.lang.Runnable
    public final void run() {
        p.x(this.f0.g0, this.a);
    }
}
