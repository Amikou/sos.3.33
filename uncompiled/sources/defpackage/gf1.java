package defpackage;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

/* compiled from: GestureDetectorCompat.java */
/* renamed from: gf1  reason: default package */
/* loaded from: classes.dex */
public final class gf1 {
    public final a a;

    /* compiled from: GestureDetectorCompat.java */
    /* renamed from: gf1$a */
    /* loaded from: classes.dex */
    public interface a {
        boolean b(MotionEvent motionEvent);
    }

    /* compiled from: GestureDetectorCompat.java */
    /* renamed from: gf1$b */
    /* loaded from: classes.dex */
    public static class b implements a {
        public static final int v = ViewConfiguration.getTapTimeout();
        public static final int w = ViewConfiguration.getDoubleTapTimeout();
        public int a;
        public int b;
        public int c;
        public int d;
        public final Handler e;
        public final GestureDetector.OnGestureListener f;
        public GestureDetector.OnDoubleTapListener g;
        public boolean h;
        public boolean i;
        public boolean j;
        public boolean k;
        public boolean l;
        public MotionEvent m;
        public MotionEvent n;
        public boolean o;
        public float p;
        public float q;
        public float r;
        public float s;
        public boolean t;
        public VelocityTracker u;

        public b(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            if (handler != null) {
                this.e = new a(handler);
            } else {
                this.e = new a();
            }
            this.f = onGestureListener;
            if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
                g((GestureDetector.OnDoubleTapListener) onGestureListener);
            }
            e(context);
        }

        public final void a() {
            this.e.removeMessages(1);
            this.e.removeMessages(2);
            this.e.removeMessages(3);
            this.u.recycle();
            this.u = null;
            this.o = false;
            this.h = false;
            this.k = false;
            this.l = false;
            this.i = false;
            if (this.j) {
                this.j = false;
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:107:0x0204  */
        /* JADX WARN: Removed duplicated region for block: B:110:0x021b  */
        @Override // defpackage.gf1.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public boolean b(android.view.MotionEvent r13) {
            /*
                Method dump skipped, instructions count: 589
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.gf1.b.b(android.view.MotionEvent):boolean");
        }

        public final void c() {
            this.e.removeMessages(1);
            this.e.removeMessages(2);
            this.e.removeMessages(3);
            this.o = false;
            this.k = false;
            this.l = false;
            this.i = false;
            if (this.j) {
                this.j = false;
            }
        }

        public void d() {
            this.e.removeMessages(3);
            this.i = false;
            this.j = true;
            this.f.onLongPress(this.m);
        }

        public final void e(Context context) {
            if (context != null) {
                if (this.f != null) {
                    this.t = true;
                    ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                    int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                    int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                    this.c = viewConfiguration.getScaledMinimumFlingVelocity();
                    this.d = viewConfiguration.getScaledMaximumFlingVelocity();
                    this.a = scaledTouchSlop * scaledTouchSlop;
                    this.b = scaledDoubleTapSlop * scaledDoubleTapSlop;
                    return;
                }
                throw new IllegalArgumentException("OnGestureListener must not be null");
            }
            throw new IllegalArgumentException("Context must not be null");
        }

        public final boolean f(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
            if (this.l && motionEvent3.getEventTime() - motionEvent2.getEventTime() <= w) {
                int x = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
                int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
                return (x * x) + (y * y) < this.b;
            }
            return false;
        }

        public void g(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.g = onDoubleTapListener;
        }

        /* compiled from: GestureDetectorCompat.java */
        /* renamed from: gf1$b$a */
        /* loaded from: classes.dex */
        public class a extends Handler {
            public a() {
            }

            @Override // android.os.Handler
            public void handleMessage(Message message) {
                int i = message.what;
                if (i == 1) {
                    b bVar = b.this;
                    bVar.f.onShowPress(bVar.m);
                } else if (i == 2) {
                    b.this.d();
                } else if (i == 3) {
                    b bVar2 = b.this;
                    GestureDetector.OnDoubleTapListener onDoubleTapListener = bVar2.g;
                    if (onDoubleTapListener != null) {
                        if (!bVar2.h) {
                            onDoubleTapListener.onSingleTapConfirmed(bVar2.m);
                        } else {
                            bVar2.i = true;
                        }
                    }
                } else {
                    throw new RuntimeException("Unknown message " + message);
                }
            }

            public a(Handler handler) {
                super(handler.getLooper());
            }
        }
    }

    /* compiled from: GestureDetectorCompat.java */
    /* renamed from: gf1$c */
    /* loaded from: classes.dex */
    public static class c implements a {
        public final GestureDetector a;

        public c(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            this.a = new GestureDetector(context, onGestureListener, handler);
        }

        @Override // defpackage.gf1.a
        public boolean b(MotionEvent motionEvent) {
            return this.a.onTouchEvent(motionEvent);
        }
    }

    public gf1(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, null);
    }

    public boolean a(MotionEvent motionEvent) {
        return this.a.b(motionEvent);
    }

    public gf1(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        if (Build.VERSION.SDK_INT > 17) {
            this.a = new c(context, onGestureListener, handler);
        } else {
            this.a = new b(context, onGestureListener, handler);
        }
    }
}
