package defpackage;

/* compiled from: HandlerWrapper.java */
/* renamed from: pj1  reason: default package */
/* loaded from: classes.dex */
public interface pj1 {

    /* compiled from: HandlerWrapper.java */
    /* renamed from: pj1$a */
    /* loaded from: classes.dex */
    public interface a {
        void a();
    }

    a a(int i, int i2, int i3);

    boolean b(a aVar);

    boolean c(Runnable runnable);

    a d(int i);

    boolean e(int i);

    boolean f(int i);

    a g(int i, int i2, int i3, Object obj);

    boolean h(int i, long j);

    void i(int i);

    a j(int i, Object obj);

    void k(Object obj);
}
