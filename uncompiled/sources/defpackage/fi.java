package defpackage;

import android.annotation.TargetApi;
import android.os.SharedMemory;
import android.system.ErrnoException;
import com.facebook.imagepipeline.memory.b;
import java.io.Closeable;
import java.nio.ByteBuffer;

/* compiled from: AshmemMemoryChunk.java */
@TargetApi(27)
/* renamed from: fi  reason: default package */
/* loaded from: classes.dex */
public class fi implements b, Closeable {
    public SharedMemory a;
    public ByteBuffer f0;
    public final long g0;

    public fi(int i) {
        xt2.b(Boolean.valueOf(i > 0));
        try {
            SharedMemory create = SharedMemory.create("AshmemMemoryChunk", i);
            this.a = create;
            this.f0 = create.mapReadWrite();
            this.g0 = System.identityHashCode(this);
        } catch (ErrnoException e) {
            throw new RuntimeException("Fail to create AshmemMemory", e);
        }
    }

    @Override // com.facebook.imagepipeline.memory.b
    public int a() {
        xt2.g(this.a);
        return this.a.getSize();
    }

    @Override // com.facebook.imagepipeline.memory.b
    public void b(int i, b bVar, int i2, int i3) {
        xt2.g(bVar);
        if (bVar.getUniqueId() == getUniqueId()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Copying from AshmemMemoryChunk ");
            sb.append(Long.toHexString(getUniqueId()));
            sb.append(" to AshmemMemoryChunk ");
            sb.append(Long.toHexString(bVar.getUniqueId()));
            sb.append(" which are the same ");
            xt2.b(Boolean.FALSE);
        }
        if (bVar.getUniqueId() < getUniqueId()) {
            synchronized (bVar) {
                synchronized (this) {
                    d(i, bVar, i2, i3);
                }
            }
            return;
        }
        synchronized (this) {
            synchronized (bVar) {
                d(i, bVar, i2, i3);
            }
        }
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized int c(int i, byte[] bArr, int i2, int i3) {
        int a;
        xt2.g(bArr);
        xt2.g(this.f0);
        a = o72.a(i, i3, a());
        o72.b(i, bArr.length, i2, a, a());
        this.f0.position(i);
        this.f0.put(bArr, i2, a);
        return a;
    }

    @Override // com.facebook.imagepipeline.memory.b, java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() {
        if (!isClosed()) {
            SharedMemory sharedMemory = this.a;
            if (sharedMemory != null) {
                sharedMemory.close();
            }
            ByteBuffer byteBuffer = this.f0;
            if (byteBuffer != null) {
                SharedMemory.unmap(byteBuffer);
            }
            this.f0 = null;
            this.a = null;
        }
    }

    public final void d(int i, b bVar, int i2, int i3) {
        if (bVar instanceof fi) {
            xt2.i(!isClosed());
            xt2.i(!bVar.isClosed());
            xt2.g(this.f0);
            xt2.g(bVar.t());
            o72.b(i, bVar.a(), i2, i3, a());
            this.f0.position(i);
            bVar.t().position(i2);
            byte[] bArr = new byte[i3];
            this.f0.get(bArr, 0, i3);
            bVar.t().put(bArr, 0, i3);
            return;
        }
        throw new IllegalArgumentException("Cannot copy two incompatible MemoryChunks");
    }

    @Override // com.facebook.imagepipeline.memory.b
    public long getUniqueId() {
        return this.g0;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized boolean isClosed() {
        boolean z;
        if (this.f0 != null) {
            z = this.a == null;
        }
        return z;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized byte p(int i) {
        boolean z = true;
        xt2.i(!isClosed());
        xt2.b(Boolean.valueOf(i >= 0));
        if (i >= a()) {
            z = false;
        }
        xt2.b(Boolean.valueOf(z));
        xt2.g(this.f0);
        return this.f0.get(i);
    }

    @Override // com.facebook.imagepipeline.memory.b
    public synchronized int s(int i, byte[] bArr, int i2, int i3) {
        int a;
        xt2.g(bArr);
        xt2.g(this.f0);
        a = o72.a(i, i3, a());
        o72.b(i, bArr.length, i2, a, a());
        this.f0.position(i);
        this.f0.get(bArr, i2, a);
        return a;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public ByteBuffer t() {
        return this.f0;
    }

    @Override // com.facebook.imagepipeline.memory.b
    public long y() {
        throw new UnsupportedOperationException("Cannot get the pointer of an  AshmemMemoryChunk");
    }
}
