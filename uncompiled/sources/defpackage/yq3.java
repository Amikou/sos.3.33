package defpackage;

import java.io.IOException;
import okhttp3.internal.ws.RealWebSocket;

/* compiled from: Sniffer.java */
/* renamed from: yq3  reason: default package */
/* loaded from: classes.dex */
public final class yq3 {
    public final op2 a = new op2(8);
    public int b;

    public final long a(q11 q11Var) throws IOException {
        int i = 0;
        q11Var.n(this.a.d(), 0, 1);
        int i2 = this.a.d()[0] & 255;
        if (i2 == 0) {
            return Long.MIN_VALUE;
        }
        int i3 = 128;
        int i4 = 0;
        while ((i2 & i3) == 0) {
            i3 >>= 1;
            i4++;
        }
        int i5 = i2 & (~i3);
        q11Var.n(this.a.d(), 1, i4);
        while (i < i4) {
            i++;
            i5 = (this.a.d()[i] & 255) + (i5 << 8);
        }
        this.b += i4 + 1;
        return i5;
    }

    public boolean b(q11 q11Var) throws IOException {
        long a;
        int i;
        long length = q11Var.getLength();
        int i2 = (length > (-1L) ? 1 : (length == (-1L) ? 0 : -1));
        long j = RealWebSocket.DEFAULT_MINIMUM_DEFLATE_SIZE;
        if (i2 != 0 && length <= RealWebSocket.DEFAULT_MINIMUM_DEFLATE_SIZE) {
            j = length;
        }
        int i3 = (int) j;
        q11Var.n(this.a.d(), 0, 4);
        long F = this.a.F();
        this.b = 4;
        while (F != 440786851) {
            int i4 = this.b + 1;
            this.b = i4;
            if (i4 == i3) {
                return false;
            }
            q11Var.n(this.a.d(), 0, 1);
            F = ((F << 8) & (-256)) | (this.a.d()[0] & 255);
        }
        long a2 = a(q11Var);
        long j2 = this.b;
        if (a2 == Long.MIN_VALUE) {
            return false;
        }
        if (i2 != 0 && j2 + a2 >= length) {
            return false;
        }
        while (true) {
            int i5 = this.b;
            long j3 = j2 + a2;
            if (i5 >= j3) {
                return ((long) i5) == j3;
            } else if (a(q11Var) != Long.MIN_VALUE && (a(q11Var)) >= 0 && a <= 2147483647L) {
                if (i != 0) {
                    int i6 = (int) a;
                    q11Var.f(i6);
                    this.b += i6;
                }
            }
        }
    }
}
