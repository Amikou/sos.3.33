package defpackage;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import co.nimblehq.recentapps.thumbnailhiding.RecentAppsThumbnailHidingActivity;
import defpackage.wj1;

/* compiled from: RecentAppsThumbnailHidingLifecycleTracker.kt */
/* renamed from: o43  reason: default package */
/* loaded from: classes.dex */
public final class o43 implements Application.ActivityLifecycleCallbacks {
    public wj1 a;
    public je2 f0;
    public pm2 g0;

    /* compiled from: RecentAppsThumbnailHidingLifecycleTracker.kt */
    /* renamed from: o43$a */
    /* loaded from: classes.dex */
    public static final class a implements wj1.b {
        public final /* synthetic */ Activity b;

        public a(Activity activity) {
            this.b = activity;
        }

        @Override // defpackage.wj1.b
        public void a() {
            o43.this.f(this.b, true);
        }

        @Override // defpackage.wj1.b
        public void b() {
            o43.this.f(this.b, true);
        }
    }

    public static final void e(Activity activity, boolean z) {
        fs1.f(activity, "$activity");
        if ((activity instanceof RecentAppsThumbnailHidingActivity) && ((RecentAppsThumbnailHidingActivity) activity).t()) {
            m43.a(activity, z);
        }
    }

    public final void c() {
        wj1 wj1Var = this.a;
        if (wj1Var != null) {
            wj1Var.i();
        }
        this.a = null;
    }

    public final void d() {
        je2 je2Var = this.f0;
        if (je2Var != null) {
            je2Var.h();
            je2Var.g(this.g0);
        }
        this.f0 = null;
        this.g0 = null;
    }

    public final void f(Activity activity, boolean z) {
        if (activity instanceof RecentAppsThumbnailHidingActivity) {
            if (m43.b(activity)) {
                return;
            }
            ((RecentAppsThumbnailHidingActivity) activity).o(activity, z);
        } else if (activity instanceof p43) {
            ((p43) activity).o(activity, z);
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(final Activity activity, Bundle bundle) {
        fs1.f(activity, "activity");
        if (je2.d()) {
            je2 je2Var = this.f0;
            if (!fs1.b(je2Var == null ? null : je2Var.b(), activity)) {
                d();
            }
            this.g0 = new pm2() { // from class: n43
                @Override // defpackage.pm2
                public final void a(boolean z) {
                    o43.e(activity, z);
                }
            };
            je2 c = je2.c();
            c.f(activity);
            c.a(this.g0);
            te4 te4Var = te4.a;
            this.f0 = c;
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        fs1.f(activity, "activity");
        if (je2.d()) {
            je2 je2Var = this.f0;
            if (fs1.b(je2Var == null ? null : je2Var.b(), activity)) {
                d();
            }
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
        fs1.f(activity, "activity");
        f(activity, true);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
        fs1.f(activity, "activity");
        f(activity, false);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        fs1.f(activity, "activity");
        fs1.f(bundle, "outState");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
        fs1.f(activity, "activity");
        wj1 wj1Var = this.a;
        if (!fs1.b(wj1Var == null ? null : wj1Var.d(), activity)) {
            c();
        }
        wj1 wj1Var2 = new wj1(activity);
        wj1Var2.g(new a(activity));
        wj1Var2.h();
        te4 te4Var = te4.a;
        this.a = wj1Var2;
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
        fs1.f(activity, "activity");
        wj1 wj1Var = this.a;
        if (fs1.b(wj1Var == null ? null : wj1Var.d(), activity)) {
            c();
        }
    }
}
