package defpackage;

import android.content.Context;
import com.bumptech.glide.a;
import defpackage.l73;

/* compiled from: GeneratedRequestManagerFactory.java */
/* renamed from: te1  reason: default package */
/* loaded from: classes.dex */
public final class te1 implements l73.b {
    @Override // defpackage.l73.b
    public k73 a(a aVar, kz1 kz1Var, m73 m73Var, Context context) {
        return new kg1(aVar, kz1Var, m73Var, context);
    }
}
