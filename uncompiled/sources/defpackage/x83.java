package defpackage;

/* compiled from: RetryManager.java */
/* renamed from: x83  reason: default package */
/* loaded from: classes.dex */
public class x83 {
    public boolean a;
    public int b;
    public int c;

    public x83() {
        a();
    }

    public void a() {
        this.a = false;
        this.b = 4;
        c();
    }

    public void b() {
        this.c++;
    }

    public void c() {
        this.c = 0;
    }

    public void d(boolean z) {
        this.a = z;
    }

    public boolean e() {
        return this.a && this.c < this.b;
    }
}
