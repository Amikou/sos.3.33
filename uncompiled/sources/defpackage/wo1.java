package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import java.util.Locale;

/* compiled from: ImageStreamCursorProvider.java */
/* renamed from: wo1  reason: default package */
/* loaded from: classes3.dex */
public class wo1 {
    public static final String[] c = {"_id", "_display_name", "_size", "width", "height"};
    public final Context a;
    public final int b;

    public wo1(Context context, int i) {
        this.a = context;
        this.b = i;
    }

    @SuppressLint({"NewApi"})
    public Cursor a(int i) {
        if (this.a == null) {
            return null;
        }
        String b = b();
        if (this.b >= 26) {
            Bundle bundle = new Bundle();
            bundle.putInt("android:query-arg-limit", i);
            bundle.putStringArray("android:query-arg-sort-columns", new String[]{b});
            bundle.putInt("android:query-arg-sort-direction", 1);
            return this.a.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, c, bundle, null);
        }
        return this.a.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, c, null, null, String.format(Locale.US, "%s DESC LIMIT %s", b, Integer.valueOf(i)));
    }

    @SuppressLint({"InlinedApi"})
    public String b() {
        return this.b >= 29 ? "datetaken" : "date_modified";
    }
}
