package defpackage;

import android.os.Looper;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.b;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: s05  reason: default package */
/* loaded from: classes.dex */
public final class s05<O extends a.d> extends e35 {
    public final b<O> c;

    public s05(b<O> bVar) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = bVar;
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final <A extends a.b, T extends com.google.android.gms.common.api.internal.b<? extends l83, A>> T f(T t) {
        return (T) this.c.f(t);
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final Looper g() {
        return this.c.g();
    }

    @Override // com.google.android.gms.common.api.GoogleApiClient
    public final void l(j15 j15Var) {
    }
}
