package defpackage;

/* renamed from: gd2  reason: default package */
/* loaded from: classes2.dex */
public abstract class gd2 {
    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        cd2.x(iArr, iArr2, iArr3);
        cd2.w(iArr, 6, iArr2, 6, iArr3, 12);
        int d = cd2.d(iArr3, 6, iArr3, 12);
        int c = d + cd2.c(iArr3, 18, iArr3, 12, cd2.c(iArr3, 0, iArr3, 6, 0) + d);
        int[] g = cd2.g();
        int[] g2 = cd2.g();
        boolean z = cd2.k(iArr, 6, iArr, 0, g, 0) != cd2.k(iArr2, 6, iArr2, 0, g2, 0);
        int[] i = cd2.i();
        cd2.x(g, g2, i);
        kd2.f(24, c + (z ? kd2.d(12, i, 0, iArr3, 6) : kd2.M(12, i, 0, iArr3, 6)), iArr3, 18);
    }

    public static void b(int[] iArr, int[] iArr2) {
        cd2.D(iArr, iArr2);
        cd2.C(iArr, 6, iArr2, 12);
        int d = cd2.d(iArr2, 6, iArr2, 12);
        int c = d + cd2.c(iArr2, 18, iArr2, 12, cd2.c(iArr2, 0, iArr2, 6, 0) + d);
        int[] g = cd2.g();
        cd2.k(iArr, 6, iArr, 0, g, 0);
        int[] i = cd2.i();
        cd2.D(g, i);
        kd2.f(24, c + kd2.M(12, i, 0, iArr2, 6), iArr2, 18);
    }
}
