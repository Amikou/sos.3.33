package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;

/* compiled from: Protocol.java */
/* renamed from: cw2  reason: default package */
/* loaded from: classes2.dex */
public class cw2 implements om1 {
    public final String a;

    public cw2(String str) {
        if (str != null) {
            this.a = str;
            return;
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.om1
    public om1 a() {
        return new cw2(b());
    }

    @Override // defpackage.om1
    public String b() {
        return this.a;
    }

    @Override // defpackage.om1
    public boolean c(String str) {
        for (String str2 : str.replaceAll(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "").split(",")) {
            if (this.a.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || cw2.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((cw2) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // defpackage.om1
    public String toString() {
        return b();
    }
}
