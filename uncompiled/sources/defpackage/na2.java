package defpackage;

/* compiled from: MultiClassKey.java */
/* renamed from: na2  reason: default package */
/* loaded from: classes.dex */
public class na2 {
    public Class<?> a;
    public Class<?> b;
    public Class<?> c;

    public na2() {
    }

    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || na2.class != obj.getClass()) {
            return false;
        }
        na2 na2Var = (na2) obj;
        return this.a.equals(na2Var.a) && this.b.equals(na2Var.b) && mg4.d(this.c, na2Var.c);
    }

    public int hashCode() {
        int hashCode = ((this.a.hashCode() * 31) + this.b.hashCode()) * 31;
        Class<?> cls = this.c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    public String toString() {
        return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
    }

    public na2(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }
}
