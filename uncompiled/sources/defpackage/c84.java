package defpackage;

import android.os.Bundle;
import androidx.media3.common.e;
import androidx.media3.common.v;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;

/* compiled from: TrackGroupArray.java */
/* renamed from: c84  reason: default package */
/* loaded from: classes.dex */
public final class c84 implements e {
    public static final c84 h0 = new c84(new v[0]);
    public static final e.a<c84> i0 = b84.a;
    public final int a;
    public final ImmutableList<v> f0;
    public int g0;

    public c84(v... vVarArr) {
        this.f0 = ImmutableList.copyOf(vVarArr);
        this.a = vVarArr.length;
        f();
    }

    public static String d(int i) {
        return Integer.toString(i, 36);
    }

    public static /* synthetic */ c84 e(Bundle bundle) {
        ArrayList parcelableArrayList = bundle.getParcelableArrayList(d(0));
        if (parcelableArrayList == null) {
            return new c84(new v[0]);
        }
        return new c84((v[]) is.b(v.j0, parcelableArrayList).toArray(new v[0]));
    }

    public v b(int i) {
        return this.f0.get(i);
    }

    public int c(v vVar) {
        int indexOf = this.f0.indexOf(vVar);
        if (indexOf >= 0) {
            return indexOf;
        }
        return -1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || c84.class != obj.getClass()) {
            return false;
        }
        c84 c84Var = (c84) obj;
        return this.a == c84Var.a && this.f0.equals(c84Var.f0);
    }

    public final void f() {
        int i = 0;
        while (i < this.f0.size()) {
            int i2 = i + 1;
            for (int i3 = i2; i3 < this.f0.size(); i3++) {
                if (this.f0.get(i).equals(this.f0.get(i3))) {
                    p12.d("TrackGroupArray", "", new IllegalArgumentException("Multiple identical TrackGroups added to one TrackGroupArray."));
                }
            }
            i = i2;
        }
    }

    public int hashCode() {
        if (this.g0 == 0) {
            this.g0 = this.f0.hashCode();
        }
        return this.g0;
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(d(0), is.c(this.f0));
        return bundle;
    }
}
