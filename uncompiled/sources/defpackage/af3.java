package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: af3  reason: default package */
/* loaded from: classes2.dex */
public class af3 extends xs0.c {
    public static final BigInteger j = new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000001"));
    public df3 i;

    /* renamed from: af3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ int[] b;

        public a(int i, int[] iArr) {
            this.a = i;
            this.b = iArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            int[] e = dd2.e();
            int[] e2 = dd2.e();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                int i4 = ((i3 ^ i) - 1) >> 31;
                for (int i5 = 0; i5 < 7; i5++) {
                    int i6 = e[i5];
                    int[] iArr = this.b;
                    e[i5] = i6 ^ (iArr[i2 + i5] & i4);
                    e2[i5] = e2[i5] ^ (iArr[(i2 + 7) + i5] & i4);
                }
                i2 += 14;
            }
            return af3.this.i(new cf3(e), new cf3(e2), false);
        }
    }

    public af3() {
        super(j);
        this.i = new df3(this, null, null);
        this.b = n(new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFE")));
        this.c = n(new BigInteger(1, pk1.a("B4050A850C04B3ABF54132565044B0B7D7BFD8BA270B39432355FFB4")));
        this.d = new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFFFFFF16A2E0B8F03E13DD29455C5C2A3D"));
        this.e = BigInteger.valueOf(1L);
        this.f = 2;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 2;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new af3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        int[] iArr = new int[i2 * 7 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            dd2.c(((cf3) pt0Var.n()).f, 0, iArr, i3);
            int i5 = i3 + 7;
            dd2.c(((cf3) pt0Var.o()).f, 0, iArr, i5);
            i3 = i5 + 7;
        }
        return new a(i2, iArr);
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new df3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new df3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new cf3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return j.bitLength();
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.i;
    }
}
