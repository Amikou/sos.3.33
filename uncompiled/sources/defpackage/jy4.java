package defpackage;

import android.content.ComponentName;
import android.content.Context;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.f;
import com.google.android.play.core.common.PlayCoreDialogWrapperActivity;
import com.google.android.play.core.internal.d;

/* renamed from: jy4  reason: default package */
/* loaded from: classes2.dex */
public final class jy4 implements jw4<Object> {
    public final jw4 a;
    public final jw4 b;
    public final /* synthetic */ int c = 0;

    public jy4(jw4<ux4> jw4Var, jw4<Context> jw4Var2) {
        this.a = jw4Var;
        this.b = jw4Var2;
    }

    public jy4(jw4<Context> jw4Var, jw4<c> jw4Var2, byte[] bArr) {
        this.b = jw4Var;
        this.a = jw4Var2;
    }

    public jy4(jw4<Context> jw4Var, jw4<fv4> jw4Var2, char[] cArr) {
        this.a = jw4Var;
        this.b = jw4Var2;
    }

    public jy4(jw4<c> jw4Var, jw4<zy4> jw4Var2, int[] iArr) {
        this.b = jw4Var;
        this.a = jw4Var2;
    }

    public jy4(jw4<Context> jw4Var, jw4<ww4> jw4Var2, short[] sArr) {
        this.a = jw4Var;
        this.b = jw4Var2;
    }

    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ Object a() {
        int i = this.c;
        if (i == 0) {
            Object a = this.a.a();
            Context a2 = ((my4) this.b).a();
            ux4 ux4Var = (ux4) a;
            d.h(a2.getPackageManager(), new ComponentName(a2.getPackageName(), "com.google.android.play.core.assetpacks.AssetPackExtractionService"), 4);
            d.h(a2.getPackageManager(), new ComponentName(a2.getPackageName(), "com.google.android.play.core.assetpacks.ExtractionForegroundService"), 4);
            PlayCoreDialogWrapperActivity.a(a2);
            d.k(ux4Var);
            return ux4Var;
        } else if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    return new yw4((c) this.b.a(), gw4.c(this.a));
                }
                return new c(((my4) this.a).a(), (ww4) this.b.a());
            }
            return new st4(((my4) this.a).a(), (fv4) this.b.a());
        } else {
            return new f(((my4) this.b).a(), (c) this.a.a());
        }
    }
}
