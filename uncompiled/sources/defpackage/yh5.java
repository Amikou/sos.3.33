package defpackage;

import android.os.Bundle;
import defpackage.kb;
import defpackage.tf;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: yh5  reason: default package */
/* loaded from: classes2.dex */
public final class yh5 implements tf.a {
    public final /* synthetic */ pk5 a;

    public yh5(pk5 pk5Var) {
        this.a = pk5Var;
    }

    @Override // defpackage.em5
    public final void a(String str, String str2, Bundle bundle, long j) {
        kb.b bVar;
        if (str == null || str.equals("crash") || !ha5.c(str2)) {
            return;
        }
        Bundle bundle2 = new Bundle();
        bundle2.putString(PublicResolver.FUNC_NAME, str2);
        bundle2.putLong("timestampInMillis", j);
        bundle2.putBundle("params", bundle);
        bVar = this.a.a;
        bVar.a(3, bundle2);
    }
}
