package defpackage;

import android.text.TextUtils;
import com.google.android.gms.internal.measurement.g;
import com.google.android.gms.internal.measurement.l1;
import com.google.android.gms.internal.measurement.m1;
import com.google.android.gms.internal.measurement.x0;
import com.google.android.gms.internal.measurement.y0;
import com.google.android.gms.internal.measurement.zzd;
import com.google.android.gms.internal.measurement.zzkn;
import com.google.android.gms.measurement.internal.r;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: qj5  reason: default package */
/* loaded from: classes.dex */
public final class qj5 extends jv5 implements n45 {
    public final Map<String, Map<String, String>> d;
    public final Map<String, Map<String, Boolean>> e;
    public final Map<String, Map<String, Boolean>> f;
    public final Map<String, x0> g;
    public final Map<String, Map<String, Integer>> h;
    public final t22<String, g> i;
    public final j46 j;
    public final Map<String, String> k;

    public qj5(fw5 fw5Var) {
        super(fw5Var);
        this.d = new rh();
        this.e = new rh();
        this.f = new rh();
        this.g = new rh();
        this.k = new rh();
        this.h = new rh();
        this.i = new ij5(this, 20);
        this.j = new lj5(this);
    }

    public static final Map<String, String> E(x0 x0Var) {
        rh rhVar = new rh();
        if (x0Var != null) {
            for (y0 y0Var : x0Var.B()) {
                rhVar.put(y0Var.x(), y0Var.y());
            }
        }
        return rhVar;
    }

    public static /* synthetic */ g y(qj5 qj5Var, String str) {
        qj5Var.g();
        zt2.f(str);
        a36.a();
        if (qj5Var.a.z().v(null, qf5.B0) && qj5Var.p(str)) {
            if (qj5Var.g.containsKey(str) && qj5Var.g.get(str) != null) {
                qj5Var.C(str, qj5Var.g.get(str));
            } else {
                qj5Var.A(str);
            }
            return qj5Var.i.h().get(str);
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:23:0x008d, code lost:
        if (r2 == null) goto L11;
     */
    /* JADX WARN: Not initialized variable reg: 2, insn: 0x011b: MOVE  (r1 I:??[OBJECT, ARRAY]) = (r2 I:??[OBJECT, ARRAY]), block:B:39:0x011b */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0095  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x00b4  */
    /* JADX WARN: Removed duplicated region for block: B:41:0x011e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void A(java.lang.String r13) {
        /*
            Method dump skipped, instructions count: 291
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.qj5.A(java.lang.String):void");
    }

    public final void B(String str, li5 li5Var) {
        rh rhVar = new rh();
        rh rhVar2 = new rh();
        rh rhVar3 = new rh();
        if (li5Var != null) {
            for (int i = 0; i < li5Var.v(); i++) {
                sh5 s = li5Var.x(i).s();
                if (TextUtils.isEmpty(s.v())) {
                    this.a.w().p().a("EventConfig contained null event name");
                } else {
                    String v = s.v();
                    String b = yl5.b(s.v());
                    if (!TextUtils.isEmpty(b)) {
                        s.x(b);
                        li5Var.y(i, s);
                    }
                    rhVar.put(v, Boolean.valueOf(s.y()));
                    rhVar2.put(s.v(), Boolean.valueOf(s.A()));
                    if (s.B()) {
                        if (s.C() >= 2 && s.C() <= 65535) {
                            rhVar3.put(s.v(), Integer.valueOf(s.C()));
                        } else {
                            this.a.w().p().c("Invalid sampling rate. Event name, sample rate", s.v(), Integer.valueOf(s.C()));
                        }
                    }
                }
            }
        }
        this.e.put(str, rhVar);
        this.f.put(str, rhVar2);
        this.h.put(str, rhVar3);
    }

    public final void C(final String str, x0 x0Var) {
        if (x0Var.H() != 0) {
            this.a.w().v().b("EES programs found", Integer.valueOf(x0Var.H()));
            m1 m1Var = x0Var.G().get(0);
            try {
                g gVar = new g();
                gVar.a("internal.remoteConfig", new Callable(this, str) { // from class: cj5
                    public final qj5 a;
                    public final String b;

                    {
                        this.a = this;
                        this.b = str;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return new uz5("internal.remoteConfig", new nj5(this.a, this.b));
                    }
                });
                gVar.a("internal.logger", new Callable(this) { // from class: gj5
                    public final qj5 a;

                    {
                        this.a = this;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return new u46(this.a.j);
                    }
                });
                gVar.f(m1Var);
                this.i.d(str, gVar);
                this.a.w().v().c("EES program loaded for appId, activities", str, Integer.valueOf(m1Var.y().y()));
                for (l1 l1Var : m1Var.y().x()) {
                    this.a.w().v().b("EES program activity", l1Var.x());
                }
                return;
            } catch (zzd unused) {
                this.a.w().l().b("Failed to load EES program. appId", str);
                return;
            }
        }
        this.i.e(str);
    }

    public final x0 D(String str, byte[] bArr) {
        if (bArr == null) {
            return x0.J();
        }
        try {
            x0 o = ((li5) r.J(x0.I(), bArr)).o();
            this.a.w().v().c("Parsed config. version, gmp_app_id", o.x() ? Long.valueOf(o.y()) : null, o.z() ? o.A() : null);
            return o;
        } catch (zzkn e) {
            this.a.w().p().c("Unable to merge remote config. appId", og5.x(str), e);
            return x0.J();
        } catch (RuntimeException e2) {
            this.a.w().p().c("Unable to merge remote config. appId", og5.x(str), e2);
            return x0.J();
        }
    }

    @Override // defpackage.n45
    public final String c(String str, String str2) {
        e();
        A(str);
        Map<String, String> map = this.d.get(str);
        if (map != null) {
            return map.get(str2);
        }
        return null;
    }

    @Override // defpackage.jv5
    public final boolean h() {
        return false;
    }

    public final x0 j(String str) {
        g();
        e();
        zt2.f(str);
        A(str);
        return this.g.get(str);
    }

    public final String k(String str) {
        e();
        return this.k.get(str);
    }

    public final void l(String str) {
        e();
        this.k.put(str, null);
    }

    public final void n(String str) {
        e();
        this.g.remove(str);
    }

    public final boolean o(String str) {
        e();
        x0 j = j(str);
        if (j == null) {
            return false;
        }
        return j.F();
    }

    public final boolean p(String str) {
        x0 x0Var;
        a36.a();
        return (!this.a.z().v(null, qf5.B0) || TextUtils.isEmpty(str) || (x0Var = this.g.get(str)) == null || x0Var.H() == 0) ? false : true;
    }

    public final boolean r(String str, byte[] bArr, String str2) {
        g();
        e();
        zt2.f(str);
        li5 s = D(str, bArr).s();
        if (s == null) {
            return false;
        }
        B(str, s);
        a36.a();
        if (this.a.z().v(null, qf5.B0)) {
            C(str, s.o());
        }
        this.g.put(str, s.o());
        this.k.put(str, str2);
        this.d.put(str, E(s.o()));
        this.b.V().x(str, new ArrayList(s.A()));
        try {
            s.B();
            bArr = s.o().c();
        } catch (RuntimeException e) {
            this.a.w().p().c("Unable to serialize reduced-size config. Storing full config instead. appId", og5.x(str), e);
        }
        u26.a();
        if (this.a.z().v(null, qf5.z0)) {
            this.b.V().g0(str, bArr, str2);
        } else {
            this.b.V().g0(str, bArr, null);
        }
        this.g.put(str, s.o());
        return true;
    }

    public final boolean s(String str, String str2) {
        Boolean bool;
        e();
        A(str);
        if (v(str) && sw5.F(str2)) {
            return true;
        }
        if (x(str) && sw5.j0(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.e.get(str);
        if (map == null || (bool = map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public final boolean t(String str, String str2) {
        Boolean bool;
        e();
        A(str);
        if ("ecommerce_purchase".equals(str2) || "purchase".equals(str2) || "refund".equals(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.f.get(str);
        if (map == null || (bool = map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public final int u(String str, String str2) {
        Integer num;
        e();
        A(str);
        Map<String, Integer> map = this.h.get(str);
        if (map == null || (num = map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }

    public final boolean v(String str) {
        return "1".equals(c(str, "measurement.upload.blacklist_internal"));
    }

    public final boolean x(String str) {
        return "1".equals(c(str, "measurement.upload.blacklist_public"));
    }
}
