package defpackage;

import java.util.Collections;
import java.util.List;

/* compiled from: AdaptationSet.java */
/* renamed from: i8  reason: default package */
/* loaded from: classes.dex */
public class i8 {
    public final int a;
    public final int b;
    public final List<b73> c;
    public final List<nm0> d;
    public final List<nm0> e;
    public final List<nm0> f;

    public i8(int i, int i2, List<b73> list, List<nm0> list2, List<nm0> list3, List<nm0> list4) {
        this.a = i;
        this.b = i2;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
        this.e = Collections.unmodifiableList(list3);
        this.f = Collections.unmodifiableList(list4);
    }
}
