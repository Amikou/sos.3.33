package defpackage;

import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Size;
import android.util.SizeF;
import android.util.SparseArray;
import androidx.savedstate.SavedStateRegistry;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: SavedStateHandle.java */
/* renamed from: ec3  reason: default package */
/* loaded from: classes.dex */
public final class ec3 {
    public static final Class[] e;
    public final Map<String, Object> a;
    public final Map<String, SavedStateRegistry.b> b;
    public final Map<String, b<?>> c;
    public final SavedStateRegistry.b d;

    /* compiled from: SavedStateHandle.java */
    /* renamed from: ec3$a */
    /* loaded from: classes.dex */
    public class a implements SavedStateRegistry.b {
        public a() {
        }

        @Override // androidx.savedstate.SavedStateRegistry.b
        public Bundle a() {
            for (Map.Entry entry : new HashMap(ec3.this.b).entrySet()) {
                ec3.this.e((String) entry.getKey(), ((SavedStateRegistry.b) entry.getValue()).a());
            }
            Set<String> keySet = ec3.this.a.keySet();
            ArrayList<? extends Parcelable> arrayList = new ArrayList<>(keySet.size());
            ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>(arrayList.size());
            for (String str : keySet) {
                arrayList.add(str);
                arrayList2.add(ec3.this.a.get(str));
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("keys", arrayList);
            bundle.putParcelableArrayList("values", arrayList2);
            return bundle;
        }
    }

    static {
        Class[] clsArr = new Class[29];
        clsArr[0] = Boolean.TYPE;
        clsArr[1] = boolean[].class;
        clsArr[2] = Double.TYPE;
        clsArr[3] = double[].class;
        Class<SizeF> cls = Integer.TYPE;
        clsArr[4] = cls;
        clsArr[5] = int[].class;
        clsArr[6] = Long.TYPE;
        clsArr[7] = long[].class;
        clsArr[8] = String.class;
        clsArr[9] = String[].class;
        clsArr[10] = Binder.class;
        clsArr[11] = Bundle.class;
        clsArr[12] = Byte.TYPE;
        clsArr[13] = byte[].class;
        clsArr[14] = Character.TYPE;
        clsArr[15] = char[].class;
        clsArr[16] = CharSequence.class;
        clsArr[17] = CharSequence[].class;
        clsArr[18] = ArrayList.class;
        clsArr[19] = Float.TYPE;
        clsArr[20] = float[].class;
        clsArr[21] = Parcelable.class;
        clsArr[22] = Parcelable[].class;
        clsArr[23] = Serializable.class;
        clsArr[24] = Short.TYPE;
        clsArr[25] = short[].class;
        clsArr[26] = SparseArray.class;
        int i = Build.VERSION.SDK_INT;
        clsArr[27] = i >= 21 ? Size.class : cls;
        if (i >= 21) {
            cls = SizeF.class;
        }
        clsArr[28] = cls;
        e = clsArr;
    }

    public ec3(Map<String, Object> map) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new a();
        this.a = new HashMap(map);
    }

    public static ec3 a(Bundle bundle, Bundle bundle2) {
        if (bundle == null && bundle2 == null) {
            return new ec3();
        }
        HashMap hashMap = new HashMap();
        if (bundle2 != null) {
            for (String str : bundle2.keySet()) {
                hashMap.put(str, bundle2.get(str));
            }
        }
        if (bundle == null) {
            return new ec3(hashMap);
        }
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("keys");
        ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("values");
        if (parcelableArrayList != null && parcelableArrayList2 != null && parcelableArrayList.size() == parcelableArrayList2.size()) {
            for (int i = 0; i < parcelableArrayList.size(); i++) {
                hashMap.put((String) parcelableArrayList.get(i), parcelableArrayList2.get(i));
            }
            return new ec3(hashMap);
        }
        throw new IllegalStateException("Invalid bundle passed as restored state");
    }

    public static void f(Object obj) {
        if (obj == null) {
            return;
        }
        for (Class cls : e) {
            if (cls.isInstance(obj)) {
                return;
            }
        }
        throw new IllegalArgumentException("Can't put value with type " + obj.getClass() + " into saved state");
    }

    public <T> gb2<T> b(String str) {
        return c(str, false, null);
    }

    public final <T> gb2<T> c(String str, boolean z, T t) {
        b<?> bVar;
        b<?> bVar2 = this.c.get(str);
        if (bVar2 != null) {
            return bVar2;
        }
        if (this.a.containsKey(str)) {
            bVar = new b<>(this, str, this.a.get(str));
        } else if (z) {
            bVar = new b<>(this, str, t);
        } else {
            bVar = new b<>(this, str);
        }
        this.c.put(str, bVar);
        return bVar;
    }

    public SavedStateRegistry.b d() {
        return this.d;
    }

    public <T> void e(String str, T t) {
        f(t);
        b<?> bVar = this.c.get(str);
        if (bVar != null) {
            bVar.setValue(t);
        } else {
            this.a.put(str, t);
        }
    }

    /* compiled from: SavedStateHandle.java */
    /* renamed from: ec3$b */
    /* loaded from: classes.dex */
    public static class b<T> extends gb2<T> {
        public String a;
        public ec3 b;

        public b(ec3 ec3Var, String str, T t) {
            super(t);
            this.a = str;
            this.b = ec3Var;
        }

        @Override // defpackage.gb2, androidx.lifecycle.LiveData
        public void setValue(T t) {
            ec3 ec3Var = this.b;
            if (ec3Var != null) {
                ec3Var.a.put(this.a, t);
            }
            super.setValue(t);
        }

        public b(ec3 ec3Var, String str) {
            this.a = str;
            this.b = ec3Var;
        }
    }

    public ec3() {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new a();
        this.a = new HashMap();
    }
}
