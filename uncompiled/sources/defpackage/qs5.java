package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: qs5  reason: default package */
/* loaded from: classes.dex */
public final class qs5 extends wn5<Integer> implements gt5<Integer>, vw5 {
    public static final qs5 h0;
    public int[] f0;
    public int g0;

    static {
        qs5 qs5Var = new qs5(new int[0], 0);
        h0 = qs5Var;
        qs5Var.zzb();
    }

    public qs5() {
        this(new int[10], 0);
    }

    public static qs5 m() {
        return h0;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        int intValue = ((Integer) obj).intValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            int[] iArr = this.f0;
            if (i2 < iArr.length) {
                System.arraycopy(iArr, i, iArr, i + 1, i2 - i);
            } else {
                int[] iArr2 = new int[((i2 * 3) / 2) + 1];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                System.arraycopy(this.f0, i, iArr2, i + 1, this.g0 - i);
                this.f0 = iArr2;
            }
            this.f0[i] = intValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(o(i));
    }

    @Override // defpackage.wn5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Integer> collection) {
        e();
        vs5.d(collection);
        if (!(collection instanceof qs5)) {
            return super.addAll(collection);
        }
        qs5 qs5Var = (qs5) collection;
        int i = qs5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.f0;
            if (i3 > iArr.length) {
                this.f0 = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(qs5Var.f0, 0, this.f0, this.g0, qs5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.gt5
    public final /* synthetic */ gt5<Integer> d(int i) {
        if (i >= this.g0) {
            return new qs5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qs5)) {
            return super.equals(obj);
        }
        qs5 qs5Var = (qs5) obj;
        if (this.g0 != qs5Var.g0) {
            return false;
        }
        int[] iArr = qs5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(i(i));
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + this.f0[i2];
        }
        return i;
    }

    public final int i(int i) {
        n(i);
        return this.f0[i];
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == intValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    public final void k(int i) {
        e();
        int i2 = this.g0;
        int[] iArr = this.f0;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[((i2 * 3) / 2) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.f0 = iArr2;
        }
        int[] iArr3 = this.f0;
        int i3 = this.g0;
        this.g0 = i3 + 1;
        iArr3[i3] = i;
    }

    public final void n(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(o(i));
        }
    }

    public final String o(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        e();
        n(i);
        int[] iArr = this.f0;
        int i3 = iArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i3);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            int[] iArr = this.f0;
            System.arraycopy(iArr, i2, iArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        e();
        n(i);
        int[] iArr = this.f0;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    public qs5(int[] iArr, int i) {
        this.f0 = iArr;
        this.g0 = i;
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* synthetic */ boolean add(Object obj) {
        k(((Integer) obj).intValue());
        return true;
    }
}
