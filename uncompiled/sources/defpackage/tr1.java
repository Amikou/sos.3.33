package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: IntSummaryStatistics.java */
/* renamed from: tr1  reason: default package */
/* loaded from: classes2.dex */
public class tr1 implements mr1 {
    public long a;
    public long b;
    public int c = Integer.MAX_VALUE;
    public int d = Integer.MIN_VALUE;

    public final double a() {
        return b() > 0 ? e() / b() : Utils.DOUBLE_EPSILON;
    }

    public final long b() {
        return this.a;
    }

    public final int c() {
        return this.d;
    }

    public final int d() {
        return this.c;
    }

    public final long e() {
        return this.b;
    }

    public String toString() {
        return String.format("%s{count=%d, sum=%d, min=%d, average=%f, max=%d}", tr1.class.getSimpleName(), Long.valueOf(b()), Long.valueOf(e()), Integer.valueOf(d()), Double.valueOf(a()), Integer.valueOf(c()));
    }
}
