package defpackage;

/* renamed from: qy2  reason: default package */
/* loaded from: classes.dex */
public final class qy2 {
    public static final int androidx_core_ripple_material_light = 2131099690;
    public static final int androidx_core_secondary_text_default_material_light = 2131099691;
    public static final int exo_black_opacity_60 = 2131099814;
    public static final int exo_black_opacity_70 = 2131099815;
    public static final int exo_bottom_bar_background = 2131099816;
    public static final int exo_edit_mode_background_color = 2131099817;
    public static final int exo_error_message_background_color = 2131099818;
    public static final int exo_styled_error_message_background = 2131099819;
    public static final int exo_white = 2131099820;
    public static final int exo_white_opacity_70 = 2131099821;
    public static final int notification_action_color_filter = 2131099926;
    public static final int notification_icon_bg_color = 2131099928;
    public static final int notification_material_background_media_default_color = 2131099929;
    public static final int primary_text_default_material_dark = 2131099943;
    public static final int ripple_material_light = 2131099959;
    public static final int secondary_text_default_material_dark = 2131099962;
    public static final int secondary_text_default_material_light = 2131099963;
}
