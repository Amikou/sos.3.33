package defpackage;

import java.util.Collections;
import java.util.List;

/* compiled from: Mp4WebvttSubtitle.java */
/* renamed from: ja2  reason: default package */
/* loaded from: classes.dex */
public final class ja2 implements qv3 {
    public final List<kb0> a;

    public ja2(List<kb0> list) {
        this.a = Collections.unmodifiableList(list);
    }

    @Override // defpackage.qv3
    public int a(long j) {
        return j < 0 ? 0 : -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        ii.a(i == 0);
        return 0L;
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return j >= 0 ? this.a : Collections.emptyList();
    }

    @Override // defpackage.qv3
    public int f() {
        return 1;
    }
}
