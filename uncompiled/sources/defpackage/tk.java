package defpackage;

import defpackage.r90;

/* compiled from: AutoValue_CrashlyticsReport_Session_Application_Organization.java */
/* renamed from: tk  reason: default package */
/* loaded from: classes2.dex */
public final class tk extends r90.e.a.b {
    public final String a;

    @Override // defpackage.r90.e.a.b
    public String a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.a.b) {
            return this.a.equals(((r90.e.a.b) obj).a());
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    public String toString() {
        return "Organization{clsId=" + this.a + "}";
    }
}
