package defpackage;

import java.io.IOException;
import java8.util.o;
import org.web3j.protocol.exceptions.TransactionException;

/* compiled from: PollingTransactionReceiptProcessor.java */
/* renamed from: ns2  reason: default package */
/* loaded from: classes3.dex */
public class ns2 extends x84 {
    public final int attempts;
    public final long sleepDuration;

    public ns2(ko4 ko4Var, long j, int i) {
        super(ko4Var);
        this.sleepDuration = j;
        this.attempts = i;
    }

    private w84 getTransactionReceipt(String str, long j, int i) throws IOException, TransactionException {
        o<? extends w84> sendTransactionReceiptRequest = sendTransactionReceiptRequest(str);
        for (int i2 = 0; i2 < i; i2++) {
            if (!sendTransactionReceiptRequest.c()) {
                try {
                    Thread.sleep(j);
                    sendTransactionReceiptRequest = sendTransactionReceiptRequest(str);
                } catch (InterruptedException e) {
                    throw new TransactionException(e);
                }
            } else {
                return sendTransactionReceiptRequest.b();
            }
        }
        throw new TransactionException("Transaction receipt was not generated after " + ((j * i) / 1000) + " seconds for transaction: " + str, str);
    }

    @Override // defpackage.x84
    public w84 waitForTransactionReceipt(String str) throws IOException, TransactionException {
        return getTransactionReceipt(str, this.sleepDuration, this.attempts);
    }
}
