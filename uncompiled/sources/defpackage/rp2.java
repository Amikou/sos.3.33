package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.io.JsonEOFException;
import com.fasterxml.jackson.core.io.d;
import java.io.IOException;

/* compiled from: ParserMinimalBase.java */
/* renamed from: rp2  reason: default package */
/* loaded from: classes.dex */
public abstract class rp2 extends JsonParser {
    public JsonToken g0;

    public rp2(int i) {
        super(i);
    }

    public static final String p1(int i) {
        char c = (char) i;
        if (Character.isISOControl(c)) {
            return "(CTRL-CHAR, code " + i + ")";
        } else if (i > 255) {
            return "'" + c + "' (code " + i + " / 0x" + Integer.toHexString(i) + ")";
        } else {
            return "'" + c + "' (code " + i + ")";
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean F0(JsonToken jsonToken) {
        return this.g0 == jsonToken;
    }

    public void F1(JsonToken jsonToken) throws JsonParseException {
        String str;
        if (jsonToken == JsonToken.VALUE_STRING) {
            str = " in a String value";
        } else {
            str = (jsonToken == JsonToken.VALUE_NUMBER_INT || jsonToken == JsonToken.VALUE_NUMBER_FLOAT) ? " in a Number value" : " in a value";
        }
        z1(str, jsonToken);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean H0(int i) {
        JsonToken jsonToken = this.g0;
        return jsonToken == null ? i == 0 : jsonToken.id() == i;
    }

    public void H1(int i) throws JsonParseException {
        I1(i, "Expected space separating root-level values");
    }

    public void I1(int i, String str) throws JsonParseException {
        if (i < 0) {
            y1();
        }
        String str2 = "Unexpected character (" + p1(i) + ")";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        w1(str2);
    }

    public final void J1() {
        gh4.c();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean K0() {
        return this.g0 == JsonToken.START_ARRAY;
    }

    public void K1(int i) throws JsonParseException {
        w1("Illegal character (" + p1((char) i) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean L0() {
        return this.g0 == JsonToken.START_OBJECT;
    }

    public void L1(int i, String str) throws JsonParseException {
        if (!J0(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS) || i > 32) {
            w1("Illegal unquoted character (" + p1((char) i) + "): has to be escaped using backslash to be included in " + str);
        }
    }

    public final void M1(String str, Throwable th) throws JsonParseException {
        throw m1(str, th);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken U0() throws IOException {
        JsonToken T0 = T0();
        return T0 == JsonToken.FIELD_NAME ? T0() : T0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public void e() {
        if (this.g0 != null) {
            this.g0 = null;
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken f() {
        return this.g0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int i0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_NUMBER_INT && jsonToken != JsonToken.VALUE_NUMBER_FLOAT) {
            return m0(0);
        }
        return F();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser k1() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.START_OBJECT && jsonToken != JsonToken.START_ARRAY) {
            return this;
        }
        int i = 1;
        while (true) {
            JsonToken T0 = T0();
            if (T0 == null) {
                r1();
                return this;
            } else if (T0.isStructStart()) {
                i++;
            } else if (T0.isStructEnd() && i - 1 == 0) {
                return this;
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int m0(int i) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_NUMBER_INT || jsonToken == JsonToken.VALUE_NUMBER_FLOAT) {
            return F();
        }
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 6) {
                switch (id) {
                    case 9:
                        return 1;
                    case 10:
                    case 11:
                        return 0;
                    case 12:
                        Object z = z();
                        return z instanceof Number ? ((Number) z).intValue() : i;
                    default:
                        return i;
                }
            }
            String X = X();
            if (v1(X)) {
                return 0;
            }
            return d.d(X, i);
        }
        return i;
    }

    public final JsonParseException m1(String str, Throwable th) {
        return new JsonParseException(this, str, th);
    }

    public void o1(String str, ms msVar, Base64Variant base64Variant) throws IOException {
        try {
            base64Variant.decode(str, msVar);
        } catch (IllegalArgumentException e) {
            w1(e.getMessage());
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long r0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_NUMBER_INT && jsonToken != JsonToken.VALUE_NUMBER_FLOAT) {
            return w0(0L);
        }
        return M();
    }

    public abstract void r1() throws JsonParseException;

    public char s1(char c) throws JsonProcessingException {
        if (J0(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)) {
            return c;
        }
        if (c == '\'' && J0(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return c;
        }
        w1("Unrecognized character escape " + p1(c));
        return c;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken u() {
        return this.g0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int v() {
        JsonToken jsonToken = this.g0;
        if (jsonToken == null) {
            return 0;
        }
        return jsonToken.id();
    }

    public boolean v1(String str) {
        return "null".equals(str);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long w0(long j) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_NUMBER_INT || jsonToken == JsonToken.VALUE_NUMBER_FLOAT) {
            return M();
        }
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 6) {
                switch (id) {
                    case 9:
                        return 1L;
                    case 10:
                    case 11:
                        return 0L;
                    case 12:
                        Object z = z();
                        return z instanceof Number ? ((Number) z).longValue() : j;
                    default:
                        return j;
                }
            }
            String X = X();
            if (v1(X)) {
                return 0L;
            }
            return d.e(X, j);
        }
        return j;
    }

    public final void w1(String str) throws JsonParseException {
        throw a(str);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String x0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            return X();
        }
        if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        }
        return y0(null);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String y0(String str) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            return X();
        }
        if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        }
        return (jsonToken == null || jsonToken == JsonToken.VALUE_NULL || !jsonToken.isScalarValue()) ? str : X();
    }

    public void y1() throws JsonParseException {
        z1(" in " + this.g0, this.g0);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean z0() {
        return this.g0 != null;
    }

    public void z1(String str, JsonToken jsonToken) throws JsonParseException {
        throw new JsonEOFException(this, jsonToken, "Unexpected end-of-input" + str);
    }
}
