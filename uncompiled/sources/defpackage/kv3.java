package defpackage;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import androidx.media3.extractor.text.a;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: SubripDecoder.java */
/* renamed from: kv3  reason: default package */
/* loaded from: classes.dex */
public final class kv3 extends a {
    public static final Pattern p = Pattern.compile("\\s*((?:(\\d+):)?(\\d+):(\\d+)(?:,(\\d+))?)\\s*-->\\s*((?:(\\d+):)?(\\d+):(\\d+)(?:,(\\d+))?)\\s*");
    public static final Pattern q = Pattern.compile("\\{\\\\.*?\\}");
    public final StringBuilder n;
    public final ArrayList<String> o;

    public kv3() {
        super("SubripDecoder");
        this.n = new StringBuilder();
        this.o = new ArrayList<>();
    }

    public static float D(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    return 0.92f;
                }
                throw new IllegalArgumentException();
            }
            return 0.5f;
        }
        return 0.08f;
    }

    public static long E(Matcher matcher, int i) {
        String group = matcher.group(i + 1);
        long parseLong = (group != null ? Long.parseLong(group) * 60 * 60 * 1000 : 0L) + (Long.parseLong((String) ii.e(matcher.group(i + 2))) * 60 * 1000) + (Long.parseLong((String) ii.e(matcher.group(i + 3))) * 1000);
        String group2 = matcher.group(i + 4);
        if (group2 != null) {
            parseLong += Long.parseLong(group2);
        }
        return parseLong * 1000;
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        e22 e22Var = new e22();
        op2 op2Var = new op2(bArr, i);
        while (true) {
            String p2 = op2Var.p();
            int i2 = 0;
            if (p2 == null) {
                break;
            } else if (p2.length() != 0) {
                try {
                    Integer.parseInt(p2);
                    String p3 = op2Var.p();
                    if (p3 == null) {
                        p12.i("SubripDecoder", "Unexpected end");
                        break;
                    }
                    Matcher matcher = p.matcher(p3);
                    if (matcher.matches()) {
                        e22Var.a(E(matcher, 1));
                        e22Var.a(E(matcher, 6));
                        this.n.setLength(0);
                        this.o.clear();
                        for (String p4 = op2Var.p(); !TextUtils.isEmpty(p4); p4 = op2Var.p()) {
                            if (this.n.length() > 0) {
                                this.n.append("<br>");
                            }
                            this.n.append(F(p4, this.o));
                        }
                        Spanned fromHtml = Html.fromHtml(this.n.toString());
                        String str = null;
                        while (true) {
                            if (i2 >= this.o.size()) {
                                break;
                            }
                            String str2 = this.o.get(i2);
                            if (str2.matches("\\{\\\\an[1-9]\\}")) {
                                str = str2;
                                break;
                            }
                            i2++;
                        }
                        arrayList.add(C(fromHtml, str));
                        arrayList.add(kb0.v0);
                    } else {
                        p12.i("SubripDecoder", "Skipping invalid timing: " + p3);
                    }
                } catch (NumberFormatException unused) {
                    p12.i("SubripDecoder", "Skipping invalid index: " + p2);
                }
            }
        }
        return new lv3((kb0[]) arrayList.toArray(new kb0[0]), e22Var.d());
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public final kb0 C(Spanned spanned, String str) {
        char c;
        char c2;
        kb0.b o = new kb0.b().o(spanned);
        if (str == null) {
            return o.a();
        }
        switch (str.hashCode()) {
            case -685620710:
                if (str.equals("{\\an1}")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case -685620679:
                if (str.equals("{\\an2}")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case -685620648:
                if (str.equals("{\\an3}")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case -685620617:
                if (str.equals("{\\an4}")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case -685620586:
                if (str.equals("{\\an5}")) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            case -685620555:
                if (str.equals("{\\an6}")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case -685620524:
                if (str.equals("{\\an7}")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case -685620493:
                if (str.equals("{\\an8}")) {
                    c = '\b';
                    break;
                }
                c = 65535;
                break;
            case -685620462:
                if (str.equals("{\\an9}")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0 || c == 1 || c == 2) {
            o.l(0);
        } else if (c != 3 && c != 4 && c != 5) {
            o.l(1);
        } else {
            o.l(2);
        }
        switch (str.hashCode()) {
            case -685620710:
                if (str.equals("{\\an1}")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case -685620679:
                if (str.equals("{\\an2}")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case -685620648:
                if (str.equals("{\\an3}")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case -685620617:
                if (str.equals("{\\an4}")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case -685620586:
                if (str.equals("{\\an5}")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case -685620555:
                if (str.equals("{\\an6}")) {
                    c2 = '\b';
                    break;
                }
                c2 = 65535;
                break;
            case -685620524:
                if (str.equals("{\\an7}")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -685620493:
                if (str.equals("{\\an8}")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case -685620462:
                if (str.equals("{\\an9}")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        if (c2 == 0 || c2 == 1 || c2 == 2) {
            o.i(2);
        } else if (c2 != 3 && c2 != 4 && c2 != 5) {
            o.i(1);
        } else {
            o.i(0);
        }
        return o.k(D(o.d())).h(D(o.c()), 0).a();
    }

    public final String F(String str, ArrayList<String> arrayList) {
        String trim = str.trim();
        StringBuilder sb = new StringBuilder(trim);
        Matcher matcher = q.matcher(trim);
        int i = 0;
        while (matcher.find()) {
            String group = matcher.group();
            arrayList.add(group);
            int start = matcher.start() - i;
            int length = group.length();
            sb.replace(start, start + length, "");
            i += length;
        }
        return sb.toString();
    }
}
