package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: fz5  reason: default package */
/* loaded from: classes.dex */
public final class fz5 implements Iterator<Map.Entry> {
    public int a = -1;
    public boolean f0;
    public Iterator<Map.Entry> g0;
    public final /* synthetic */ hz5 h0;

    public /* synthetic */ fz5(hz5 hz5Var, qy5 qy5Var) {
        this.h0 = hz5Var;
    }

    public final Iterator<Map.Entry> a() {
        Map map;
        if (this.g0 == null) {
            map = this.h0.g0;
            this.g0 = map.entrySet().iterator();
        }
        return this.g0;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        List list;
        Map map;
        int i = this.a + 1;
        list = this.h0.f0;
        if (i >= list.size()) {
            map = this.h0.g0;
            return !map.isEmpty() && a().hasNext();
        }
        return true;
    }

    @Override // java.util.Iterator
    public final /* bridge */ /* synthetic */ Map.Entry next() {
        List list;
        List list2;
        this.f0 = true;
        int i = this.a + 1;
        this.a = i;
        list = this.h0.f0;
        if (i < list.size()) {
            list2 = this.h0.f0;
            return (Map.Entry) list2.get(this.a);
        }
        return a().next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        List list;
        if (this.f0) {
            this.f0 = false;
            this.h0.m();
            int i = this.a;
            list = this.h0.f0;
            if (i < list.size()) {
                hz5 hz5Var = this.h0;
                int i2 = this.a;
                this.a = i2 - 1;
                hz5Var.k(i2);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }
}
