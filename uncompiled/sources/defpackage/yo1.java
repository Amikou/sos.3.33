package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.util.ArrayList;
import java.util.List;
import zendesk.belvedere.MediaResult;

/* compiled from: ImageStreamService.java */
/* renamed from: yo1  reason: default package */
/* loaded from: classes3.dex */
public class yo1 {
    public final Context a;
    public final wo1 b;

    public yo1(Context context) {
        this.a = context.getApplicationContext();
        this.b = new wo1(context, Build.VERSION.SDK_INT);
    }

    public boolean a(String str) {
        return rg4.c(str, this.a);
    }

    public List<MediaResult> b(int i) {
        int lastIndexOf;
        ArrayList arrayList = new ArrayList();
        Cursor a = this.b.a(i);
        if (a != null) {
            while (a.moveToNext()) {
                try {
                    Uri contentUri = MediaStore.Files.getContentUri("external", a.getLong(a.getColumnIndex("_id")));
                    long j = a.getLong(a.getColumnIndex("_size"));
                    long j2 = a.getLong(a.getColumnIndex("width"));
                    long j3 = a.getLong(a.getColumnIndex("height"));
                    String string = a.getString(a.getColumnIndex("_display_name"));
                    String str = "image/jpeg";
                    if (!TextUtils.isEmpty(string) && (lastIndexOf = string.lastIndexOf(".")) != -1) {
                        str = MimeTypeMap.getSingleton().getMimeTypeFromExtension(string.substring(lastIndexOf + 1));
                    }
                    arrayList.add(new MediaResult(null, contentUri, contentUri, string, str, j, j2, j3));
                } finally {
                    a.close();
                }
            }
        }
        if (a != null) {
        }
        return arrayList;
    }
}
