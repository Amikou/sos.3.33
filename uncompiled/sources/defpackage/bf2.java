package defpackage;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;

/* compiled from: NetworkNotRoamingController.java */
/* renamed from: bf2  reason: default package */
/* loaded from: classes.dex */
public class bf2 extends d60<cf2> {
    public static final String e = v12.f("NetworkNotRoamingCtrlr");

    public bf2(Context context, q34 q34Var) {
        super(j84.c(context, q34Var).d());
    }

    @Override // defpackage.d60
    public boolean b(tq4 tq4Var) {
        return tq4Var.j.b() == NetworkType.NOT_ROAMING;
    }

    @Override // defpackage.d60
    /* renamed from: i */
    public boolean c(cf2 cf2Var) {
        if (Build.VERSION.SDK_INT >= 24) {
            return (cf2Var.a() && cf2Var.c()) ? false : true;
        }
        v12.c().a(e, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
        return !cf2Var.a();
    }
}
