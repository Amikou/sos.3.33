package defpackage;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import androidx.media3.common.util.b;
import androidx.media3.extractor.text.SubtitleDecoderException;
import androidx.media3.extractor.text.a;
import com.github.mikephil.charting.utils.Utils;
import defpackage.kb0;
import java.util.List;

/* compiled from: Tx3gDecoder.java */
/* renamed from: tc4  reason: default package */
/* loaded from: classes.dex */
public final class tc4 extends a {
    public final op2 n;
    public final boolean o;
    public final int p;
    public final int q;
    public final String r;
    public final float s;
    public final int t;

    public tc4(List<byte[]> list) {
        super("Tx3gDecoder");
        this.n = new op2();
        if (list.size() == 1 && (list.get(0).length == 48 || list.get(0).length == 53)) {
            byte[] bArr = list.get(0);
            this.p = bArr[24];
            this.q = ((bArr[26] & 255) << 24) | ((bArr[27] & 255) << 16) | ((bArr[28] & 255) << 8) | (bArr[29] & 255);
            this.r = "Serif".equals(b.C(bArr, 43, bArr.length - 43)) ? "serif" : "sans-serif";
            int i = bArr[25] * 20;
            this.t = i;
            boolean z = (bArr[0] & 32) != 0;
            this.o = z;
            if (z) {
                this.s = b.p(((bArr[11] & 255) | ((bArr[10] & 255) << 8)) / i, Utils.FLOAT_EPSILON, 0.95f);
                return;
            } else {
                this.s = 0.85f;
                return;
            }
        }
        this.p = 0;
        this.q = -1;
        this.r = "sans-serif";
        this.o = false;
        this.s = 0.85f;
        this.t = -1;
    }

    public static void D(boolean z) throws SubtitleDecoderException {
        if (!z) {
            throw new SubtitleDecoderException("Unexpected subtitle format.");
        }
    }

    public static void E(SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3, int i4, int i5) {
        if (i != i2) {
            spannableStringBuilder.setSpan(new ForegroundColorSpan((i >>> 8) | ((i & 255) << 24)), i3, i4, i5 | 33);
        }
    }

    public static void F(SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3, int i4, int i5) {
        if (i != i2) {
            int i6 = i5 | 33;
            boolean z = (i & 1) != 0;
            boolean z2 = (i & 2) != 0;
            if (z) {
                if (z2) {
                    spannableStringBuilder.setSpan(new StyleSpan(3), i3, i4, i6);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(1), i3, i4, i6);
                }
            } else if (z2) {
                spannableStringBuilder.setSpan(new StyleSpan(2), i3, i4, i6);
            }
            boolean z3 = (i & 4) != 0;
            if (z3) {
                spannableStringBuilder.setSpan(new UnderlineSpan(), i3, i4, i6);
            }
            if (z3 || z || z2) {
                return;
            }
            spannableStringBuilder.setSpan(new StyleSpan(0), i3, i4, i6);
        }
    }

    public static void G(SpannableStringBuilder spannableStringBuilder, String str, int i, int i2) {
        if (str != "sans-serif") {
            spannableStringBuilder.setSpan(new TypefaceSpan(str), i, i2, 16711713);
        }
    }

    public static String H(op2 op2Var) throws SubtitleDecoderException {
        char g;
        D(op2Var.a() >= 2);
        int J = op2Var.J();
        if (J == 0) {
            return "";
        }
        if (op2Var.a() >= 2 && ((g = op2Var.g()) == 65279 || g == 65534)) {
            return op2Var.B(J, cy.e);
        }
        return op2Var.B(J, cy.c);
    }

    @Override // androidx.media3.extractor.text.a
    public qv3 A(byte[] bArr, int i, boolean z) throws SubtitleDecoderException {
        this.n.N(bArr, i);
        String H = H(this.n);
        if (H.isEmpty()) {
            return uc4.f0;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(H);
        F(spannableStringBuilder, this.p, 0, 0, spannableStringBuilder.length(), 16711680);
        E(spannableStringBuilder, this.q, -1, 0, spannableStringBuilder.length(), 16711680);
        G(spannableStringBuilder, this.r, 0, spannableStringBuilder.length());
        float f = this.s;
        while (this.n.a() >= 8) {
            int e = this.n.e();
            int n = this.n.n();
            int n2 = this.n.n();
            if (n2 == 1937013100) {
                D(this.n.a() >= 2);
                int J = this.n.J();
                for (int i2 = 0; i2 < J; i2++) {
                    C(this.n, spannableStringBuilder);
                }
            } else if (n2 == 1952608120 && this.o) {
                D(this.n.a() >= 2);
                f = b.p(this.n.J() / this.t, Utils.FLOAT_EPSILON, 0.95f);
            }
            this.n.P(e + n);
        }
        return new uc4(new kb0.b().o(spannableStringBuilder).h(f, 0).i(0).a());
    }

    public final void C(op2 op2Var, SpannableStringBuilder spannableStringBuilder) throws SubtitleDecoderException {
        int i;
        D(op2Var.a() >= 12);
        int J = op2Var.J();
        int J2 = op2Var.J();
        op2Var.Q(2);
        int D = op2Var.D();
        op2Var.Q(1);
        int n = op2Var.n();
        if (J2 > spannableStringBuilder.length()) {
            p12.i("Tx3gDecoder", "Truncating styl end (" + J2 + ") to cueText.length() (" + spannableStringBuilder.length() + ").");
            i = spannableStringBuilder.length();
        } else {
            i = J2;
        }
        if (J >= i) {
            p12.i("Tx3gDecoder", "Ignoring styl with start (" + J + ") >= end (" + i + ").");
            return;
        }
        int i2 = i;
        F(spannableStringBuilder, D, this.p, J, i2, 0);
        E(spannableStringBuilder, n, this.q, J, i2, 0);
    }
}
