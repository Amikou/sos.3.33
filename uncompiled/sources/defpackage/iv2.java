package defpackage;

import java.util.Map;

/* compiled from: ProducerListener2.java */
/* renamed from: iv2  reason: default package */
/* loaded from: classes.dex */
public interface iv2 {
    void a(ev2 ev2Var, String str, Map<String, String> map);

    void c(ev2 ev2Var, String str, Throwable th, Map<String, String> map);

    void d(ev2 ev2Var, String str, Map<String, String> map);

    void e(ev2 ev2Var, String str, boolean z);

    void i(ev2 ev2Var, String str, String str2);

    boolean j(ev2 ev2Var, String str);

    void k(ev2 ev2Var, String str);
}
