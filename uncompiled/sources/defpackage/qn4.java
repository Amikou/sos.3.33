package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.github.mikephil.charting.utils.Utils;
import defpackage.qn4;
import defpackage.u21;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: WalletMyTokensAdapter.kt */
/* renamed from: qn4  reason: default package */
/* loaded from: classes2.dex */
public final class qn4 extends RecyclerView.Adapter<b> {
    public final a a;
    public final List<UserTokenItemDisplayModel> b;
    public kt1 c;

    /* compiled from: WalletMyTokensAdapter.kt */
    /* renamed from: qn4$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(UserTokenItemDisplayModel userTokenItemDisplayModel);
    }

    /* compiled from: WalletMyTokensAdapter.kt */
    /* renamed from: qn4$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final kt1 a;
        public final /* synthetic */ qn4 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(qn4 qn4Var, kt1 kt1Var) {
            super(kt1Var.b());
            fs1.f(qn4Var, "this$0");
            fs1.f(kt1Var, "binding");
            this.b = qn4Var;
            this.a = kt1Var;
        }

        public static final void c(qn4 qn4Var, UserTokenItemDisplayModel userTokenItemDisplayModel, View view) {
            fs1.f(qn4Var, "this$0");
            fs1.f(userTokenItemDisplayModel, "$item");
            qn4Var.a.a(userTokenItemDisplayModel);
        }

        public final void b(int i) {
            final UserTokenItemDisplayModel userTokenItemDisplayModel = (UserTokenItemDisplayModel) this.b.b.get(i);
            kt1 kt1Var = this.a;
            final qn4 qn4Var = this.b;
            ImageView imageView = kt1Var.c;
            fs1.e(imageView, "ivTokenIcon");
            e30.Q(imageView, userTokenItemDisplayModel.getIconResId(), userTokenItemDisplayModel.getIconFile(), userTokenItemDisplayModel.getSymbol());
            if (e30.H(userTokenItemDisplayModel.getSymbol())) {
                ImageView imageView2 = kt1Var.c;
                fs1.e(imageView2, "ivTokenIcon");
                e30.P(imageView2, userTokenItemDisplayModel.getCmcId(), userTokenItemDisplayModel.getSymbol());
            }
            kt1Var.f.setText(userTokenItemDisplayModel.getName());
            try {
                u21.a aVar = u21.a;
                String t = e30.t(aVar.b());
                TextView textView = kt1Var.h;
                lu3 lu3Var = lu3.a;
                String format = String.format(Locale.getDefault(), t, Arrays.copyOf(new Object[]{aVar.b(), e30.p(v21.a(userTokenItemDisplayModel.getPriceInUsdt()), 0, null, false, 7, null)}, 2));
                fs1.e(format, "java.lang.String.format(locale, format, *args)");
                textView.setText(format);
            } catch (Exception unused) {
            }
            TextView textView2 = kt1Var.g;
            fs1.e(textView2, "tvTokenNativeBalance");
            e30.R(textView2, userTokenItemDisplayModel.getNativeBalance());
            TextView textView3 = kt1Var.e;
            fs1.e(textView3, "tvTokenBalance");
            e30.N(textView3, userTokenItemDisplayModel.getBalanceInUSDT(), true);
            try {
                double percentChange1h = userTokenItemDisplayModel.getPercentChange1h();
                TextView textView4 = kt1Var.d;
                lu3 lu3Var2 = lu3.a;
                String format2 = String.format("%.2f", Arrays.copyOf(new Object[]{Double.valueOf(percentChange1h)}, 1));
                fs1.e(format2, "java.lang.String.format(format, *args)");
                textView4.setText(fs1.l(format2, "%"));
                com.bumptech.glide.a.u(d().b).w(Integer.valueOf(percentChange1h >= Utils.DOUBLE_EPSILON ? R.drawable.arrow_up : R.drawable.arrow_down)).I0(d().b);
            } catch (Exception unused2) {
            }
            kt1Var.b().setOnClickListener(new View.OnClickListener() { // from class: rn4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    qn4.b.c(qn4.this, userTokenItemDisplayModel, view);
                }
            });
        }

        public final kt1 d() {
            return this.a;
        }
    }

    public qn4(a aVar) {
        fs1.f(aVar, "onTokenClickListener");
        this.a = aVar;
        this.b = new ArrayList();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.b(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        kt1 a2 = kt1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_wallet_token_card, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        this.c = a2;
        kt1 kt1Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        fs1.e(a2.b().getContext().getResources(), "binding.root.context.resources");
        kt1 kt1Var2 = this.c;
        if (kt1Var2 == null) {
            fs1.r("binding");
        } else {
            kt1Var = kt1Var2;
        }
        return new b(this, kt1Var);
    }

    public final void e(List<UserTokenItemDisplayModel> list) {
        fs1.f(list, "newItemList");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.b.size();
    }
}
