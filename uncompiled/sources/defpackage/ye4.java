package defpackage;

import android.content.Context;
import android.util.TypedValue;

/* compiled from: UnitsUtils.java */
/* renamed from: ye4  reason: default package */
/* loaded from: classes.dex */
public class ye4 {
    public static float a(Context context, float f) {
        return b(context, 1, f);
    }

    public static float b(Context context, int i, float f) {
        return TypedValue.applyDimension(i, f, context.getResources().getDisplayMetrics());
    }
}
