package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: we5  reason: default package */
/* loaded from: classes.dex */
public final class we5<V> {
    public static final Object h = new Object();
    public final String a;
    public final te5<V> b;
    public final V c;
    public final V d;
    public final Object e = new Object();
    public volatile V f = null;
    public volatile V g = null;

    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ we5(String str, Object obj, Object obj2, te5 te5Var, re5 re5Var) {
        this.a = str;
        this.c = obj;
        this.d = obj2;
        this.b = te5Var;
    }

    public final String a() {
        return this.a;
    }

    public final V b(V v) {
        synchronized (this.e) {
        }
        if (v != null) {
            return v;
        }
        if (ue5.a != null) {
            synchronized (h) {
                if (c66.a()) {
                    return this.g == null ? this.c : this.g;
                }
                try {
                    for (we5 we5Var : qf5.c()) {
                        if (!c66.a()) {
                            V v2 = null;
                            try {
                                te5<V> te5Var = we5Var.b;
                                if (te5Var != null) {
                                    v2 = te5Var.zza();
                                }
                            } catch (IllegalStateException unused) {
                            }
                            synchronized (h) {
                                we5Var.g = v2;
                            }
                        } else {
                            throw new IllegalStateException("Refreshing flag cache must be done on a worker thread.");
                        }
                    }
                } catch (SecurityException unused2) {
                }
                te5<V> te5Var2 = this.b;
                if (te5Var2 == null) {
                    return this.c;
                }
                try {
                    return te5Var2.zza();
                } catch (IllegalStateException unused3) {
                    return this.c;
                } catch (SecurityException unused4) {
                    return this.c;
                }
            }
        }
        return this.c;
    }
}
