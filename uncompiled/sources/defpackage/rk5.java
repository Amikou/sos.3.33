package defpackage;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import defpackage.rg1;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: rk5  reason: default package */
/* loaded from: classes.dex */
public final class rk5 extends rg1 {
    public final Context e;
    public final Handler f;
    public final HashMap<rg1.a, np5> d = new HashMap<>();
    public final v50 g = v50.b();
    public final long h = 5000;
    public final long i = 300000;

    public rk5(Context context) {
        this.e = context.getApplicationContext();
        this.f = new pp5(context.getMainLooper(), new rm5(this));
    }

    @Override // defpackage.rg1
    public final boolean d(rg1.a aVar, ServiceConnection serviceConnection, String str) {
        boolean d;
        zt2.k(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            np5 np5Var = this.d.get(aVar);
            if (np5Var == null) {
                np5Var = new np5(this, aVar);
                np5Var.a(serviceConnection, serviceConnection, str);
                np5Var.c(str);
                this.d.put(aVar, np5Var);
            } else {
                this.f.removeMessages(0, aVar);
                if (!np5Var.e(serviceConnection)) {
                    np5Var.a(serviceConnection, serviceConnection, str);
                    int f = np5Var.f();
                    if (f == 1) {
                        serviceConnection.onServiceConnected(np5Var.j(), np5Var.i());
                    } else if (f == 2) {
                        np5Var.c(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d = np5Var.d();
        }
        return d;
    }

    @Override // defpackage.rg1
    public final void e(rg1.a aVar, ServiceConnection serviceConnection, String str) {
        zt2.k(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            np5 np5Var = this.d.get(aVar);
            if (np5Var != null) {
                if (np5Var.e(serviceConnection)) {
                    np5Var.b(serviceConnection, str);
                    if (np5Var.h()) {
                        this.f.sendMessageDelayed(this.f.obtainMessage(0, aVar), this.h);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 76);
                    sb.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 50);
                sb2.append("Nonexistent connection status for service config: ");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }
}
