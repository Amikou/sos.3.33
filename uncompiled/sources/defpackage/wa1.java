package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSelectChainBinding.java */
/* renamed from: wa1  reason: default package */
/* loaded from: classes2.dex */
public final class wa1 {
    public final ConstraintLayout a;
    public final RecyclerView b;
    public final zd3 c;
    public final rp3 d;

    public wa1(ConstraintLayout constraintLayout, RecyclerView recyclerView, zd3 zd3Var, rp3 rp3Var) {
        this.a = constraintLayout;
        this.b = recyclerView;
        this.c = zd3Var;
        this.d = rp3Var;
    }

    public static wa1 a(View view) {
        int i = R.id.rvTokenTypeList;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvTokenTypeList);
        if (recyclerView != null) {
            i = R.id.searchBar;
            View a = ai4.a(view, R.id.searchBar);
            if (a != null) {
                zd3 a2 = zd3.a(a);
                View a3 = ai4.a(view, R.id.toolbar);
                if (a3 != null) {
                    return new wa1((ConstraintLayout) view, recyclerView, a2, rp3.a(a3));
                }
                i = R.id.toolbar;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
