package defpackage;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import java.util.HashSet;

/* compiled from: DupDetector.java */
/* renamed from: ms0  reason: default package */
/* loaded from: classes.dex */
public class ms0 {
    public final Object a;
    public String b;
    public String c;
    public HashSet<String> d;

    public ms0(Object obj) {
        this.a = obj;
    }

    public static ms0 e(JsonGenerator jsonGenerator) {
        return new ms0(jsonGenerator);
    }

    public static ms0 f(JsonParser jsonParser) {
        return new ms0(jsonParser);
    }

    public ms0 a() {
        return new ms0(this.a);
    }

    public Object b() {
        return this.a;
    }

    public boolean c(String str) throws JsonParseException {
        String str2 = this.b;
        if (str2 == null) {
            this.b = str;
            return false;
        } else if (str.equals(str2)) {
            return true;
        } else {
            String str3 = this.c;
            if (str3 == null) {
                this.c = str;
                return false;
            } else if (str.equals(str3)) {
                return true;
            } else {
                if (this.d == null) {
                    HashSet<String> hashSet = new HashSet<>(16);
                    this.d = hashSet;
                    hashSet.add(this.b);
                    this.d.add(this.c);
                }
                return !this.d.add(str);
            }
        }
    }

    public void d() {
        this.b = null;
        this.c = null;
        this.d = null;
    }
}
