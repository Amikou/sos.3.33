package defpackage;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.GetServiceRequest;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: hf5  reason: default package */
/* loaded from: classes.dex */
public final class hf5 implements Parcelable.Creator<GetServiceRequest> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GetServiceRequest createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        boolean z2 = false;
        String str = null;
        IBinder iBinder = null;
        Scope[] scopeArr = null;
        Bundle bundle = null;
        Account account = null;
        Feature[] featureArr = null;
        Feature[] featureArr2 = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 1:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 2:
                    i2 = SafeParcelReader.E(parcel, C);
                    break;
                case 3:
                    i3 = SafeParcelReader.E(parcel, C);
                    break;
                case 4:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 5:
                    iBinder = SafeParcelReader.D(parcel, C);
                    break;
                case 6:
                    scopeArr = (Scope[]) SafeParcelReader.s(parcel, C, Scope.CREATOR);
                    break;
                case 7:
                    bundle = SafeParcelReader.f(parcel, C);
                    break;
                case 8:
                    account = (Account) SafeParcelReader.o(parcel, C, Account.CREATOR);
                    break;
                case 9:
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
                case 10:
                    featureArr = (Feature[]) SafeParcelReader.s(parcel, C, Feature.CREATOR);
                    break;
                case 11:
                    featureArr2 = (Feature[]) SafeParcelReader.s(parcel, C, Feature.CREATOR);
                    break;
                case 12:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 13:
                    i4 = SafeParcelReader.E(parcel, C);
                    break;
                case 14:
                    z2 = SafeParcelReader.w(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new GetServiceRequest(i, i2, i3, str, iBinder, scopeArr, bundle, account, featureArr, featureArr2, z, i4, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GetServiceRequest[] newArray(int i) {
        return new GetServiceRequest[i];
    }
}
