package defpackage;

import com.google.android.gms.internal.vision.l0;
import com.google.android.gms.internal.vision.o0;
import com.google.android.gms.internal.vision.q0;
import com.google.android.gms.internal.vision.t0;
import com.google.android.gms.internal.vision.u0;
import com.google.protobuf.m;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: dv5  reason: default package */
/* loaded from: classes.dex */
public final class dv5 implements qx5 {
    public static final aw5 b = new av5();
    public final aw5 a;

    public dv5() {
        this(new iv5(ms5.c(), b()));
    }

    public static aw5 b() {
        try {
            Set<String> set = m.a;
            return (aw5) m.class.getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    public static boolean c(tv5 tv5Var) {
        return tv5Var.zza() == bx5.a;
    }

    @Override // defpackage.qx5
    public final <T> t0<T> a(Class<T> cls) {
        u0.o(cls);
        tv5 b2 = this.a.b(cls);
        if (b2.zzb()) {
            if (l0.class.isAssignableFrom(cls)) {
                return q0.d(u0.B(), dr5.a(), b2.zzc());
            }
            return q0.d(u0.f(), dr5.b(), b2.zzc());
        } else if (l0.class.isAssignableFrom(cls)) {
            if (c(b2)) {
                return o0.o(cls, b2, rw5.b(), eu5.c(), u0.B(), dr5.a(), wv5.b());
            }
            return o0.o(cls, b2, rw5.b(), eu5.c(), u0.B(), null, wv5.b());
        } else if (c(b2)) {
            return o0.o(cls, b2, rw5.a(), eu5.a(), u0.f(), dr5.b(), wv5.a());
        } else {
            return o0.o(cls, b2, rw5.a(), eu5.a(), u0.v(), null, wv5.a());
        }
    }

    public dv5(aw5 aw5Var) {
        this.a = (aw5) vs5.f(aw5Var, "messageInfoFactory");
    }
}
