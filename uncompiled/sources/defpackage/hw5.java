package defpackage;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: hw5  reason: default package */
/* loaded from: classes.dex */
public final class hw5 {
    public final Context a;

    public hw5(Context context) {
        zt2.j(context);
        Context applicationContext = context.getApplicationContext();
        zt2.j(applicationContext);
        this.a = applicationContext;
    }
}
