package defpackage;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.a;

/* compiled from: Alarms.java */
/* renamed from: ta  reason: default package */
/* loaded from: classes.dex */
public class ta {
    public static final String a = v12.f("Alarms");

    public static void a(Context context, hq4 hq4Var, String str) {
        v24 M = hq4Var.q().M();
        u24 c = M.c(str);
        if (c != null) {
            b(context, str, c.b);
            v12.c().a(a, String.format("Removing SystemIdInfo for workSpecId (%s)", str), new Throwable[0]);
            M.d(str);
        }
    }

    public static void b(Context context, String str, int i) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent service = PendingIntent.getService(context, i, a.b(context, str), Build.VERSION.SDK_INT >= 23 ? 603979776 : 536870912);
        if (service == null || alarmManager == null) {
            return;
        }
        v12.c().a(a, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", str, Integer.valueOf(i)), new Throwable[0]);
        alarmManager.cancel(service);
    }

    public static void c(Context context, hq4 hq4Var, String str, long j) {
        WorkDatabase q = hq4Var.q();
        v24 M = q.M();
        u24 c = M.c(str);
        if (c != null) {
            b(context, str, c.b);
            d(context, str, c.b, j);
            return;
        }
        int b = new kn1(q).b();
        M.b(new u24(str, b));
        d(context, str, b, j);
    }

    public static void d(Context context, String str, int i, long j) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        int i2 = Build.VERSION.SDK_INT;
        PendingIntent service = PendingIntent.getService(context, i, a.b(context, str), i2 >= 23 ? 201326592 : 134217728);
        if (alarmManager != null) {
            if (i2 >= 19) {
                alarmManager.setExact(0, j, service);
            } else {
                alarmManager.set(0, j, service);
            }
        }
    }
}
