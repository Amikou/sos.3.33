package defpackage;

import net.safemoon.androidwallet.model.RoomTokenInfo;

/* compiled from: TokenInfoDao.kt */
/* renamed from: x64  reason: default package */
/* loaded from: classes2.dex */
public interface x64 {
    Object a(RoomTokenInfo roomTokenInfo, q70<? super Long> q70Var);

    Object b(String str, q70<? super Boolean> q70Var);

    Object c(int i, q70<? super RoomTokenInfo> q70Var);
}
