package defpackage;

import android.util.Base64;
import com.google.android.datatransport.Priority;
import com.google.auto.value.AutoValue;
import defpackage.pl;

/* compiled from: TransportContext.java */
@AutoValue
/* renamed from: ob4  reason: default package */
/* loaded from: classes.dex */
public abstract class ob4 {

    /* compiled from: TransportContext.java */
    @AutoValue.Builder
    /* renamed from: ob4$a */
    /* loaded from: classes.dex */
    public static abstract class a {
        public abstract ob4 a();

        public abstract a b(String str);

        public abstract a c(byte[] bArr);

        public abstract a d(Priority priority);
    }

    public static a a() {
        return new pl.b().d(Priority.DEFAULT);
    }

    public abstract String b();

    public abstract byte[] c();

    public abstract Priority d();

    public ob4 e(Priority priority) {
        return a().b(b()).d(priority).c(c()).a();
    }

    public final String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = b();
        objArr[1] = d();
        objArr[2] = c() == null ? "" : Base64.encodeToString(c(), 2);
        return String.format("TransportContext(%s, %s, %s)", objArr);
    }
}
