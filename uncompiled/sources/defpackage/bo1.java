package defpackage;

import android.graphics.drawable.Animatable;

/* compiled from: ImageLoadingTimeControllerListener.java */
/* renamed from: bo1  reason: default package */
/* loaded from: classes.dex */
public class bo1 extends en {
    public long b = -1;
    public long c = -1;
    public co1 d;

    public bo1(co1 co1Var) {
        this.d = co1Var;
    }

    @Override // defpackage.en, defpackage.m80
    public void b(String str, Object obj, Animatable animatable) {
        long currentTimeMillis = System.currentTimeMillis();
        this.c = currentTimeMillis;
        co1 co1Var = this.d;
        if (co1Var != null) {
            co1Var.a(currentTimeMillis - this.b);
        }
    }

    @Override // defpackage.en, defpackage.m80
    public void e(String str, Object obj) {
        this.b = System.currentTimeMillis();
    }
}
