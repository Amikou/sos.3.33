package defpackage;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: yr5  reason: default package */
/* loaded from: classes.dex */
public interface yr5 extends Iterator<Byte> {
    byte zza();
}
