package defpackage;

/* compiled from: TaskExecutor.java */
/* renamed from: r34  reason: default package */
/* loaded from: classes.dex */
public abstract class r34 {
    public abstract void a(Runnable runnable);

    public void b(Runnable runnable) {
        if (c()) {
            runnable.run();
        } else {
            d(runnable);
        }
    }

    public abstract boolean c();

    public abstract void d(Runnable runnable);
}
