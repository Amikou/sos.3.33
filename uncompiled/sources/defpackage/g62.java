package defpackage;

import androidx.media3.common.j;
import zendesk.support.request.CellBase;

/* compiled from: MediaLoadData.java */
/* renamed from: g62  reason: default package */
/* loaded from: classes.dex */
public final class g62 {
    public final int a;
    public final int b;
    public final j c;
    public final int d;
    public final Object e;
    public final long f;
    public final long g;

    public g62(int i) {
        this(i, -1, null, 0, null, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
    }

    public g62(int i, int i2, j jVar, int i3, Object obj, long j, long j2) {
        this.a = i;
        this.b = i2;
        this.c = jVar;
        this.d = i3;
        this.e = obj;
        this.f = j;
        this.g = j2;
    }
}
