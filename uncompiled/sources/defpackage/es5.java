package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzk;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: es5  reason: default package */
/* loaded from: classes.dex */
public final class es5 implements Parcelable.Creator<zzk> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzk createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        boolean z = false;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 2) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v != 3) {
                SafeParcelReader.I(parcel, C);
            } else {
                z = SafeParcelReader.w(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzk(i, z);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzk[] newArray(int i) {
        return new zzk[i];
    }
}
