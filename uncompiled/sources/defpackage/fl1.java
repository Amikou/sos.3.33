package defpackage;

import android.graphics.Color;
import androidx.media3.common.util.b;

/* compiled from: HtmlUtils.java */
/* renamed from: fl1  reason: default package */
/* loaded from: classes.dex */
public final class fl1 {
    public static String a(String str) {
        return "." + str + ",." + str + " *";
    }

    public static String b(int i) {
        return b.A("rgba(%d,%d,%d,%.3f)", Integer.valueOf(Color.red(i)), Integer.valueOf(Color.green(i)), Integer.valueOf(Color.blue(i)), Double.valueOf(Color.alpha(i) / 255.0d));
    }
}
