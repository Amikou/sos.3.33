package defpackage;

import java.math.BigInteger;
import java.util.List;

/* compiled from: TransactionReceipt.java */
/* renamed from: w84  reason: default package */
/* loaded from: classes3.dex */
public class w84 {
    private String blockHash;
    private String blockNumber;
    private String contractAddress;
    private String cumulativeGasUsed;
    private String from;
    private String gasUsed;
    private List<q12> logs;
    private String logsBloom;
    private String revertReason;
    private String root;
    private String status;
    private String to;
    private String transactionHash;
    private String transactionIndex;

    public w84() {
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof w84) {
            w84 w84Var = (w84) obj;
            if (getTransactionHash() == null ? w84Var.getTransactionHash() == null : getTransactionHash().equals(w84Var.getTransactionHash())) {
                String str = this.transactionIndex;
                if (str == null ? w84Var.transactionIndex == null : str.equals(w84Var.transactionIndex)) {
                    if (getBlockHash() == null ? w84Var.getBlockHash() == null : getBlockHash().equals(w84Var.getBlockHash())) {
                        String str2 = this.blockNumber;
                        if (str2 == null ? w84Var.blockNumber == null : str2.equals(w84Var.blockNumber)) {
                            String str3 = this.cumulativeGasUsed;
                            if (str3 == null ? w84Var.cumulativeGasUsed == null : str3.equals(w84Var.cumulativeGasUsed)) {
                                String str4 = this.gasUsed;
                                if (str4 == null ? w84Var.gasUsed == null : str4.equals(w84Var.gasUsed)) {
                                    if (getContractAddress() == null ? w84Var.getContractAddress() == null : getContractAddress().equals(w84Var.getContractAddress())) {
                                        if (getRoot() == null ? w84Var.getRoot() == null : getRoot().equals(w84Var.getRoot())) {
                                            if (getStatus() == null ? w84Var.getStatus() == null : getStatus().equals(w84Var.getStatus())) {
                                                if (getFrom() == null ? w84Var.getFrom() == null : getFrom().equals(w84Var.getFrom())) {
                                                    if (getTo() == null ? w84Var.getTo() == null : getTo().equals(w84Var.getTo())) {
                                                        if (getLogs() == null ? w84Var.getLogs() == null : getLogs().equals(w84Var.getLogs())) {
                                                            if (getLogsBloom() == null ? w84Var.getLogsBloom() == null : getLogsBloom().equals(w84Var.getLogsBloom())) {
                                                                if (getRevertReason() != null) {
                                                                    return getRevertReason().equals(w84Var.getRevertReason());
                                                                }
                                                                return w84Var.getRevertReason() == null;
                                                            }
                                                            return false;
                                                        }
                                                        return false;
                                                    }
                                                    return false;
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public String getBlockHash() {
        return this.blockHash;
    }

    public BigInteger getBlockNumber() {
        return ej2.decodeQuantity(this.blockNumber);
    }

    public String getBlockNumberRaw() {
        return this.blockNumber;
    }

    public String getContractAddress() {
        return this.contractAddress;
    }

    public BigInteger getCumulativeGasUsed() {
        return ej2.decodeQuantity(this.cumulativeGasUsed);
    }

    public String getCumulativeGasUsedRaw() {
        return this.cumulativeGasUsed;
    }

    public String getFrom() {
        return this.from;
    }

    public BigInteger getGasUsed() {
        return ej2.decodeQuantity(this.gasUsed);
    }

    public String getGasUsedRaw() {
        return this.gasUsed;
    }

    public List<q12> getLogs() {
        return this.logs;
    }

    public String getLogsBloom() {
        return this.logsBloom;
    }

    public String getRevertReason() {
        return this.revertReason;
    }

    public String getRoot() {
        return this.root;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTo() {
        return this.to;
    }

    public String getTransactionHash() {
        return this.transactionHash;
    }

    public BigInteger getTransactionIndex() {
        return ej2.decodeQuantity(this.transactionIndex);
    }

    public String getTransactionIndexRaw() {
        return this.transactionIndex;
    }

    public int hashCode() {
        int hashCode = (getTransactionHash() != null ? getTransactionHash().hashCode() : 0) * 31;
        String str = this.transactionIndex;
        int hashCode2 = (((hashCode + (str != null ? str.hashCode() : 0)) * 31) + (getBlockHash() != null ? getBlockHash().hashCode() : 0)) * 31;
        String str2 = this.blockNumber;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.cumulativeGasUsed;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.gasUsed;
        return ((((((((((((((((hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31) + (getContractAddress() != null ? getContractAddress().hashCode() : 0)) * 31) + (getRoot() != null ? getRoot().hashCode() : 0)) * 31) + (getStatus() != null ? getStatus().hashCode() : 0)) * 31) + (getFrom() != null ? getFrom().hashCode() : 0)) * 31) + (getTo() != null ? getTo().hashCode() : 0)) * 31) + (getLogs() != null ? getLogs().hashCode() : 0)) * 31) + (getLogsBloom() != null ? getLogsBloom().hashCode() : 0)) * 31) + (getRevertReason() != null ? getRevertReason().hashCode() : 0);
    }

    public boolean isStatusOK() {
        if (getStatus() == null) {
            return true;
        }
        return BigInteger.ONE.equals(ej2.decodeQuantity(getStatus()));
    }

    public void setBlockHash(String str) {
        this.blockHash = str;
    }

    public void setBlockNumber(String str) {
        this.blockNumber = str;
    }

    public void setContractAddress(String str) {
        this.contractAddress = str;
    }

    public void setCumulativeGasUsed(String str) {
        this.cumulativeGasUsed = str;
    }

    public void setFrom(String str) {
        this.from = str;
    }

    public void setGasUsed(String str) {
        this.gasUsed = str;
    }

    public void setLogs(List<q12> list) {
        this.logs = list;
    }

    public void setLogsBloom(String str) {
        this.logsBloom = str;
    }

    public void setRevertReason(String str) {
        this.revertReason = str;
    }

    public void setRoot(String str) {
        this.root = str;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public void setTo(String str) {
        this.to = str;
    }

    public void setTransactionHash(String str) {
        this.transactionHash = str;
    }

    public void setTransactionIndex(String str) {
        this.transactionIndex = str;
    }

    public String toString() {
        return "TransactionReceipt{transactionHash='" + this.transactionHash + "', transactionIndex='" + this.transactionIndex + "', blockHash='" + this.blockHash + "', blockNumber='" + this.blockNumber + "', cumulativeGasUsed='" + this.cumulativeGasUsed + "', gasUsed='" + this.gasUsed + "', contractAddress='" + this.contractAddress + "', root='" + this.root + "', status='" + this.status + "', from='" + this.from + "', to='" + this.to + "', logs=" + this.logs + ", logsBloom='" + this.logsBloom + "', revertReason='" + this.revertReason + "'}";
    }

    public w84(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, List<q12> list, String str12, String str13) {
        this.transactionHash = str;
        this.transactionIndex = str2;
        this.blockHash = str3;
        this.blockNumber = str4;
        this.cumulativeGasUsed = str5;
        this.gasUsed = str6;
        this.contractAddress = str7;
        this.root = str8;
        this.status = str9;
        this.from = str10;
        this.to = str11;
        this.logs = list;
        this.logsBloom = str12;
        this.revertReason = str13;
    }
}
