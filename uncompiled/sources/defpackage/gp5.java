package defpackage;

import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: gp5  reason: default package */
/* loaded from: classes.dex */
public final class gp5 implements lp5 {
    public gp5() {
    }

    @Override // defpackage.lp5
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    public /* synthetic */ gp5(so5 so5Var) {
        this();
    }
}
