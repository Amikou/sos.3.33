package defpackage;

import defpackage.rl0;

/* compiled from: OptionalProvider.java */
/* renamed from: tn2  reason: default package */
/* loaded from: classes2.dex */
public class tn2<T> implements fw2<T>, rl0<T> {
    public static final rl0.a<Object> c = rn2.a;
    public static final fw2<Object> d = sn2.a;
    public rl0.a<T> a;
    public volatile fw2<T> b;

    public tn2(rl0.a<T> aVar, fw2<T> fw2Var) {
        this.a = aVar;
        this.b = fw2Var;
    }

    public static <T> tn2<T> e() {
        return new tn2<>(c, d);
    }

    public static /* synthetic */ void f(fw2 fw2Var) {
    }

    public static /* synthetic */ Object g() {
        return null;
    }

    public static /* synthetic */ void h(rl0.a aVar, rl0.a aVar2, fw2 fw2Var) {
        aVar.a(fw2Var);
        aVar2.a(fw2Var);
    }

    public static <T> tn2<T> i(fw2<T> fw2Var) {
        return new tn2<>(null, fw2Var);
    }

    @Override // defpackage.rl0
    public void a(final rl0.a<T> aVar) {
        fw2<T> fw2Var;
        fw2<T> fw2Var2 = this.b;
        fw2<Object> fw2Var3 = d;
        if (fw2Var2 != fw2Var3) {
            aVar.a(fw2Var2);
            return;
        }
        fw2<T> fw2Var4 = null;
        synchronized (this) {
            fw2Var = this.b;
            if (fw2Var != fw2Var3) {
                fw2Var4 = fw2Var;
            } else {
                final rl0.a<T> aVar2 = this.a;
                this.a = new rl0.a() { // from class: qn2
                    @Override // defpackage.rl0.a
                    public final void a(fw2 fw2Var5) {
                        tn2.h(rl0.a.this, aVar, fw2Var5);
                    }
                };
            }
        }
        if (fw2Var4 != null) {
            aVar.a(fw2Var);
        }
    }

    @Override // defpackage.fw2
    public T get() {
        return this.b.get();
    }

    public void j(fw2<T> fw2Var) {
        rl0.a<T> aVar;
        if (this.b == d) {
            synchronized (this) {
                aVar = this.a;
                this.a = null;
                this.b = fw2Var;
            }
            aVar.a(fw2Var);
            return;
        }
        throw new IllegalStateException("provide() can be called only once.");
    }
}
