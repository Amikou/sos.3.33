package defpackage;

import android.content.Context;
import android.view.MotionEvent;

/* compiled from: RotationGestureDetector.java */
/* renamed from: o93  reason: default package */
/* loaded from: classes.dex */
public class o93 {
    public final a a;
    public float b;
    public float c;
    public float d;
    public float e;
    public float f;
    public boolean g;
    public boolean h;

    /* compiled from: RotationGestureDetector.java */
    /* renamed from: o93$a */
    /* loaded from: classes.dex */
    public interface a {
        boolean a(o93 o93Var);

        void b(o93 o93Var);

        boolean c(o93 o93Var);
    }

    public o93(Context context, a aVar) {
        this.a = aVar;
    }

    public final void a() {
        if (this.g) {
            this.g = false;
            if (this.h) {
                this.a.b(this);
                this.h = false;
            }
        }
    }

    public final float b(MotionEvent motionEvent) {
        return (float) Math.toDegrees(Math.atan2(motionEvent.getY(1) - motionEvent.getY(0), motionEvent.getX(1) - motionEvent.getX(0)));
    }

    public float c() {
        return this.b;
    }

    public float d() {
        return this.c;
    }

    public float e() {
        return this.e - this.f;
    }

    public boolean f(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0 && actionMasked != 1) {
            if (actionMasked != 2) {
                if (actionMasked != 3) {
                    if (actionMasked != 5) {
                        if (actionMasked == 6 && motionEvent.getPointerCount() == 2) {
                            a();
                        }
                    } else if (motionEvent.getPointerCount() == 2) {
                        float b = b(motionEvent);
                        this.e = b;
                        this.f = b;
                        this.d = b;
                    }
                }
            } else if (motionEvent.getPointerCount() >= 2 && (!this.g || this.h)) {
                this.e = b(motionEvent);
                boolean z = false;
                this.b = (motionEvent.getX(1) + motionEvent.getX(0)) * 0.5f;
                this.c = (motionEvent.getY(1) + motionEvent.getY(0)) * 0.5f;
                boolean z2 = this.g;
                h();
                if (!z2 || g()) {
                    z = true;
                }
                if (z) {
                    this.f = this.e;
                }
            }
            return true;
        }
        a();
        return true;
    }

    public final boolean g() {
        return this.g && this.h && this.a.c(this);
    }

    public final void h() {
        if (this.g || Math.abs(this.d - this.e) < 5.0f) {
            return;
        }
        this.g = true;
        this.h = this.a.a(this);
    }
}
