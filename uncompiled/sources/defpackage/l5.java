package defpackage;

/* compiled from: AbstractSpinedBuffer.java */
/* renamed from: l5  reason: default package */
/* loaded from: classes2.dex */
public abstract class l5 {
    public final int a = 4;
    public int b;
    public int c;
    public long[] d;

    public int f(int i) {
        int i2;
        if (i != 0 && i != 1) {
            i2 = Math.min((this.a + i) - 1, 30);
        } else {
            i2 = this.a;
        }
        return 1 << i2;
    }
}
