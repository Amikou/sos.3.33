package defpackage;

import java.util.Objects;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: CoroutineContext.kt */
/* renamed from: a90  reason: default package */
/* loaded from: classes2.dex */
public final class a90 extends u4 implements g54<String> {
    public static final a f0 = new a(null);
    public final long a;

    /* compiled from: CoroutineContext.kt */
    /* renamed from: a90$a */
    /* loaded from: classes2.dex */
    public static final class a implements CoroutineContext.b<a90> {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    public a90(long j) {
        super(f0);
        this.a = j;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof a90) && this.a == ((a90) obj).a;
    }

    public final long h() {
        return this.a;
    }

    public int hashCode() {
        return p80.a(this.a);
    }

    @Override // defpackage.g54
    /* renamed from: i */
    public void q(CoroutineContext coroutineContext, String str) {
        Thread.currentThread().setName(str);
    }

    @Override // defpackage.g54
    /* renamed from: j */
    public String C(CoroutineContext coroutineContext) {
        String h;
        b90 b90Var = (b90) coroutineContext.get(b90.f0);
        String str = "coroutine";
        if (b90Var != null && (h = b90Var.h()) != null) {
            str = h;
        }
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        int e0 = StringsKt__StringsKt.e0(name, " @", 0, false, 6, null);
        if (e0 < 0) {
            e0 = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + e0 + 10);
        Objects.requireNonNull(name, "null cannot be cast to non-null type java.lang.String");
        String substring = name.substring(0, e0);
        fs1.e(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        sb.append(substring);
        sb.append(" @");
        sb.append(str);
        sb.append('#');
        sb.append(h());
        te4 te4Var = te4.a;
        String sb2 = sb.toString();
        fs1.e(sb2, "StringBuilder(capacity).…builderAction).toString()");
        currentThread.setName(sb2);
        return name;
    }

    public String toString() {
        return "CoroutineId(" + this.a + ')';
    }
}
