package defpackage;

import android.content.Intent;
import androidx.activity.result.ActivityResult;
import androidx.appcompat.app.AppCompatActivity;

/* compiled from: OpenActivityForResult.kt */
/* renamed from: hn2  reason: default package */
/* loaded from: classes2.dex */
public final class hn2 {
    public final AppCompatActivity a;
    public final w7<Intent> b;
    public tc1<? super Intent, te4> c;

    public hn2(AppCompatActivity appCompatActivity) {
        fs1.f(appCompatActivity, "appCompatActivity");
        this.a = appCompatActivity;
        w7<Intent> registerForActivityResult = appCompatActivity.registerForActivityResult(new v7(), new r7() { // from class: gn2
            @Override // defpackage.r7
            public final void a(Object obj) {
                hn2.c(hn2.this, (ActivityResult) obj);
            }
        });
        fs1.e(registerForActivityResult, "appCompatActivity.regist…)\n            }\n        }");
        this.b = registerForActivityResult;
    }

    public static final void c(hn2 hn2Var, ActivityResult activityResult) {
        fs1.f(hn2Var, "this$0");
        if (activityResult.b() == -1) {
            tc1<? super Intent, te4> tc1Var = hn2Var.c;
            if (tc1Var == null) {
                fs1.r("callback");
                tc1Var = null;
            }
            tc1Var.invoke(activityResult.a());
        }
    }

    public final w7<Intent> b(tc1<? super Intent, te4> tc1Var) {
        fs1.f(tc1Var, "callback");
        this.c = tc1Var;
        return this.b;
    }
}
