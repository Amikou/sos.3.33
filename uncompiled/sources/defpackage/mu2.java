package defpackage;

import android.content.Context;

/* compiled from: PrefillUserDefaultTokenUseCaseProvider.kt */
/* renamed from: mu2  reason: default package */
/* loaded from: classes2.dex */
public final class mu2 implements rm1 {
    public static final mu2 a = new mu2();
    public static nm1 b;

    @Override // defpackage.rm1
    public void a() {
        za.a.a();
        gg4.a.a();
        e53.a.b();
        b = null;
    }

    public final nm1 b(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new nu2(za.a.b(context), gg4.a.b(context));
                }
                te4 te4Var = te4.a;
            }
        }
        nm1 nm1Var = b;
        fs1.d(nm1Var);
        return nm1Var;
    }
}
