package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: a06  reason: default package */
/* loaded from: classes.dex */
public final class a06 implements zz5 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.sdk.collection.validate_param_names_alphabetical", true);

    @Override // defpackage.zz5
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
