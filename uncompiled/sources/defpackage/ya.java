package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: AllTokenListRepository.kt */
/* renamed from: ya  reason: default package */
/* loaded from: classes2.dex */
public final class ya implements pl1 {
    public final ol1 a;
    public final zl1 b;

    public ya(ol1 ol1Var, zl1 zl1Var) {
        fs1.f(ol1Var, "localTokenDataSource");
        fs1.f(zl1Var, "filterTokenListByPhraseUseCase");
        this.a = ol1Var;
        this.b = zl1Var;
    }

    @Override // defpackage.pl1
    public List<IToken> a(TokenType tokenType) {
        fs1.f(tokenType, "tokenType");
        return this.a.a(tokenType);
    }

    @Override // defpackage.pl1
    public LiveData<List<IToken>> b(TokenType tokenType, String str) {
        fs1.f(tokenType, "tokenType");
        fs1.f(str, "filterPhrase");
        return new gb2(this.b.a(a(tokenType), str));
    }
}
