package defpackage;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/* compiled from: LingverActivityLifecycleCallbacks.kt */
/* renamed from: c02  reason: default package */
/* loaded from: classes2.dex */
public final class c02 implements Application.ActivityLifecycleCallbacks {
    public final tc1<Activity, te4> a;

    /* JADX WARN: Multi-variable type inference failed */
    public c02(tc1<? super Activity, te4> tc1Var) {
        fs1.g(tc1Var, "callback");
        this.a = tc1Var;
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        fs1.g(activity, "activity");
        this.a.invoke(activity);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        fs1.g(activity, "activity");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
        fs1.g(activity, "activity");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
        fs1.g(activity, "activity");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        fs1.g(activity, "activity");
        fs1.g(bundle, "outState");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
        fs1.g(activity, "activity");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
        fs1.g(activity, "activity");
    }
}
