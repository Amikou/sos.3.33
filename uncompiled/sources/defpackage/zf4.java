package defpackage;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Map;

/* compiled from: UserMetadata.java */
/* renamed from: zf4  reason: default package */
/* loaded from: classes2.dex */
public class zf4 {
    public final sx1 a = new sx1(64, RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
    public final sx1 b = new sx1(64, 8192);

    public Map<String, String> a() {
        return this.a.a();
    }

    public Map<String, String> b() {
        return this.b.a();
    }

    public void c(Map<String, String> map) {
        this.a.d(map);
    }
}
