package defpackage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.View;
import com.alexvasilkov.gestures.GestureController;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.Field;
import java.util.Locale;

/* compiled from: DebugOverlay.java */
/* renamed from: bf0  reason: default package */
/* loaded from: classes.dex */
public class bf0 {
    public static final Paint a = new Paint();
    public static final RectF b = new RectF();
    public static final Rect c = new Rect();
    public static final Matrix d = new Matrix();
    public static Field e;

    public static void a(View view, Canvas canvas) {
        int i;
        GestureController controller = ((hf1) view).getController();
        tj4 positionAnimator = ((te) view).getPositionAnimator();
        Settings n = controller.n();
        Context context = view.getContext();
        float a2 = ye4.a(context, 2.0f);
        float a3 = ye4.a(context, 16.0f);
        canvas.save();
        canvas.translate(view.getPaddingLeft(), view.getPaddingTop());
        RectF rectF = b;
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, n.u(), n.t());
        b(canvas, rectF, -7829368, a2);
        Rect rect = c;
        si1.d(n, rect);
        rectF.set(rect);
        b(canvas, rectF, -16711936, a2);
        us3 o = controller.o();
        Matrix matrix = d;
        o.d(matrix);
        canvas.save();
        canvas.concat(matrix);
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, n.l(), n.k());
        b(canvas, rectF, -256, a2 / controller.o().h());
        canvas.restore();
        rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, n.l(), n.k());
        controller.o().d(matrix);
        matrix.mapRect(rectF);
        b(canvas, rectF, -65536, a2);
        float u = positionAnimator.u();
        if (u == 1.0f || (u == Utils.FLOAT_EPSILON && positionAnimator.x())) {
            GestureController.StateSource d2 = d(controller);
            c(canvas, n, d2.name(), -16711681, a3);
            if (d2 != GestureController.StateSource.NONE) {
                view.invalidate();
            }
        } else if (i > 0) {
            c(canvas, n, String.format(Locale.US, "%s %.0f%%", positionAnimator.x() ? "EXIT" : "ENTER", Float.valueOf(u * 100.0f)), -65281, a3);
        }
        canvas.restore();
    }

    public static void b(Canvas canvas, RectF rectF, int i, float f) {
        Paint paint = a;
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(f);
        float f2 = f * 0.5f;
        b.inset(f2, f2);
        paint.setColor(i);
        canvas.drawRect(rectF, paint);
    }

    public static void c(Canvas canvas, Settings settings, String str, int i, float f) {
        Paint paint = a;
        paint.setTextSize(f);
        paint.setTypeface(Typeface.MONOSPACE);
        paint.setTextAlign(Paint.Align.CENTER);
        float f2 = f * 0.5f;
        int length = str.length();
        Rect rect = c;
        paint.getTextBounds(str, 0, length, rect);
        RectF rectF = b;
        rectF.set(rect);
        rectF.offset(-rectF.centerX(), -rectF.centerY());
        si1.d(settings, rect);
        rectF.offset(rect.centerX(), rect.centerY());
        float f3 = -f2;
        rectF.inset(f3, f3);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-1);
        canvas.drawRoundRect(rectF, f2, f2, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-7829368);
        canvas.drawRoundRect(rectF, f2, f2, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(i);
        canvas.drawText(str, rectF.centerX(), rectF.bottom - f2, paint);
    }

    public static GestureController.StateSource d(GestureController gestureController) {
        if (e == null) {
            try {
                Field declaredField = GestureController.class.getDeclaredField("B0");
                e = declaredField;
                declaredField.setAccessible(true);
            } catch (Exception unused) {
            }
        }
        Field field = e;
        if (field != null) {
            try {
                return (GestureController.StateSource) field.get(gestureController);
            } catch (Exception unused2) {
            }
        }
        return GestureController.StateSource.NONE;
    }
}
