package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: k85  reason: default package */
/* loaded from: classes.dex */
public final class k85 implements h85 {
    public final wk5 a;
    public final String b;

    public k85(wk5 wk5Var, String str) {
        this.a = wk5Var;
        this.b = str;
    }

    @Override // defpackage.h85
    public final wk5 a(z55 z55Var) {
        this.a.f(this.b, z55Var);
        return this.a;
    }
}
