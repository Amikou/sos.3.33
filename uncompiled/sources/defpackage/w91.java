package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.slidetoact.SlideToActView;

/* compiled from: FragmentConfirmSwapTransactionBinding.java */
/* renamed from: w91  reason: default package */
/* loaded from: classes2.dex */
public final class w91 {
    public final MaterialCardView a;
    public final SlideToActView b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final MaterialButton f;
    public final MaterialTextView g;
    public final MaterialTextView h;
    public final MaterialTextView i;
    public final TextInputEditText j;
    public final MaterialTextView k;

    public w91(ConstraintLayout constraintLayout, MaterialCardView materialCardView, SlideToActView slideToActView, MaterialButton materialButton, MaterialTextView materialTextView, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, TextInputEditText textInputEditText, MaterialTextView materialTextView5) {
        this.a = materialCardView;
        this.b = slideToActView;
        this.c = materialButton;
        this.d = materialButton2;
        this.e = materialButton3;
        this.f = materialButton4;
        this.g = materialTextView2;
        this.h = materialTextView3;
        this.i = materialTextView4;
        this.j = textInputEditText;
        this.k = materialTextView5;
    }

    public static w91 a(View view) {
        int i = R.id.ccWrapper;
        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.ccWrapper);
        if (materialCardView != null) {
            i = R.id.confirmSwap;
            SlideToActView slideToActView = (SlideToActView) ai4.a(view, R.id.confirmSwap);
            if (slideToActView != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton != null) {
                    i = R.id.dialog_title;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_title);
                    if (materialTextView != null) {
                        i = R.id.minimum_received;
                        MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.minimum_received);
                        if (materialButton2 != null) {
                            i = R.id.price_impact;
                            MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.price_impact);
                            if (materialButton3 != null) {
                                i = R.id.swap_fee;
                                MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.swap_fee);
                                if (materialButton4 != null) {
                                    i = R.id.txtLabelReceived;
                                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtLabelReceived);
                                    if (materialTextView2 != null) {
                                        i = R.id.txtMinimumReceived;
                                        MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtMinimumReceived);
                                        if (materialTextView3 != null) {
                                            i = R.id.txtPriceImpact;
                                            MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtPriceImpact);
                                            if (materialTextView4 != null) {
                                                i = R.id.txtSwapFee;
                                                TextInputEditText textInputEditText = (TextInputEditText) ai4.a(view, R.id.txtSwapFee);
                                                if (textInputEditText != null) {
                                                    i = R.id.txtWillRecalculate;
                                                    MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.txtWillRecalculate);
                                                    if (materialTextView5 != null) {
                                                        return new w91((ConstraintLayout) view, materialCardView, slideToActView, materialButton, materialTextView, materialButton2, materialButton3, materialButton4, materialTextView2, materialTextView3, materialTextView4, textInputEditText, materialTextView5);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
