package defpackage;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.t;

/* compiled from: Stats.java */
/* renamed from: kt3  reason: default package */
/* loaded from: classes2.dex */
public class kt3 {
    public final HandlerThread a;
    public final tt b;
    public final Handler c;
    public long d;
    public long e;
    public long f;
    public long g;
    public long h;
    public long i;
    public long j;
    public long k;
    public int l;
    public int m;
    public int n;

    /* compiled from: Stats.java */
    /* renamed from: kt3$a */
    /* loaded from: classes2.dex */
    public static class a extends Handler {
        public final kt3 a;

        /* compiled from: Stats.java */
        /* renamed from: kt3$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class RunnableC0197a implements Runnable {
            public final /* synthetic */ Message a;

            public RunnableC0197a(a aVar, Message message) {
                this.a = message;
            }

            @Override // java.lang.Runnable
            public void run() {
                throw new AssertionError("Unhandled stats message." + this.a.what);
            }
        }

        public a(Looper looper, kt3 kt3Var) {
            super(looper);
            this.a = kt3Var;
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                this.a.j();
            } else if (i == 1) {
                this.a.k();
            } else if (i == 2) {
                this.a.h(message.arg1);
            } else if (i == 3) {
                this.a.i(message.arg1);
            } else if (i != 4) {
                Picasso.p.post(new RunnableC0197a(this, message));
            } else {
                this.a.l((Long) message.obj);
            }
        }
    }

    public kt3(tt ttVar) {
        this.b = ttVar;
        HandlerThread handlerThread = new HandlerThread("Picasso-Stats", 10);
        this.a = handlerThread;
        handlerThread.start();
        t.h(handlerThread.getLooper());
        this.c = new a(handlerThread.getLooper(), this);
    }

    public static long g(int i, long j) {
        return j / i;
    }

    public lt3 a() {
        return new lt3(this.b.a(), this.b.size(), this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, System.currentTimeMillis());
    }

    public void b(Bitmap bitmap) {
        m(bitmap, 2);
    }

    public void c(Bitmap bitmap) {
        m(bitmap, 3);
    }

    public void d() {
        this.c.sendEmptyMessage(0);
    }

    public void e() {
        this.c.sendEmptyMessage(1);
    }

    public void f(long j) {
        Handler handler = this.c;
        handler.sendMessage(handler.obtainMessage(4, Long.valueOf(j)));
    }

    public void h(long j) {
        int i = this.m + 1;
        this.m = i;
        long j2 = this.g + j;
        this.g = j2;
        this.j = g(i, j2);
    }

    public void i(long j) {
        this.n++;
        long j2 = this.h + j;
        this.h = j2;
        this.k = g(this.m, j2);
    }

    public void j() {
        this.d++;
    }

    public void k() {
        this.e++;
    }

    public void l(Long l) {
        this.l++;
        long longValue = this.f + l.longValue();
        this.f = longValue;
        this.i = g(this.l, longValue);
    }

    public final void m(Bitmap bitmap, int i) {
        int i2 = t.i(bitmap);
        Handler handler = this.c;
        handler.sendMessage(handler.obtainMessage(i, i2, 0));
    }
}
