package defpackage;

import android.graphics.RectF;
import java.util.Arrays;

/* compiled from: RelativeCornerSize.java */
/* renamed from: j63  reason: default package */
/* loaded from: classes2.dex */
public final class j63 implements v80 {
    public final float a;

    public j63(float f) {
        this.a = f;
    }

    @Override // defpackage.v80
    public float a(RectF rectF) {
        return this.a * rectF.height();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof j63) && this.a == ((j63) obj).a;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.a)});
    }
}
