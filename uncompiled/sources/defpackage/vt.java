package defpackage;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import java.util.HashMap;
import java.util.Map;

/* compiled from: CacheFragment.java */
/* renamed from: vt  reason: default package */
/* loaded from: classes3.dex */
public class vt extends Fragment {
    public final Map<String, Object> a = new HashMap();

    public static vt e(FragmentActivity fragmentActivity) {
        FragmentManager supportFragmentManager = fragmentActivity.getSupportFragmentManager();
        Fragment k0 = supportFragmentManager.k0("CacheFragment");
        if (k0 instanceof vt) {
            return (vt) k0;
        }
        vt vtVar = new vt();
        vtVar.setRetainInstance(true);
        supportFragmentManager.n().e(vtVar, "CacheFragment").j();
        return vtVar;
    }

    public <T> T f(String str) {
        try {
            return (T) this.a.get(str);
        } catch (Exception unused) {
            return null;
        }
    }

    public <T> void g(String str, T t) {
        this.a.put(str, t);
    }
}
