package defpackage;

import android.content.res.Resources;
import com.facebook.common.internal.ImmutableList;
import com.facebook.imagepipeline.image.a;
import java.util.concurrent.Executor;

/* compiled from: PipelineDraweeControllerFactory.java */
/* renamed from: zq2  reason: default package */
/* loaded from: classes.dex */
public class zq2 {
    public Resources a;
    public tl0 b;
    public uq0 c;
    public Executor d;
    public l72<wt, a> e;
    public ImmutableList<uq0> f;
    public fw3<Boolean> g;

    public void a(Resources resources, tl0 tl0Var, uq0 uq0Var, Executor executor, l72<wt, a> l72Var, ImmutableList<uq0> immutableList, fw3<Boolean> fw3Var) {
        this.a = resources;
        this.b = tl0Var;
        this.c = uq0Var;
        this.d = executor;
        this.e = l72Var;
        this.f = immutableList;
        this.g = fw3Var;
    }

    public wq2 b(Resources resources, tl0 tl0Var, uq0 uq0Var, Executor executor, l72<wt, a> l72Var, ImmutableList<uq0> immutableList) {
        return new wq2(resources, tl0Var, uq0Var, executor, l72Var, immutableList);
    }

    public wq2 c() {
        wq2 b = b(this.a, this.b, this.c, this.d, this.e, this.f);
        fw3<Boolean> fw3Var = this.g;
        if (fw3Var != null) {
            b.B0(fw3Var.get().booleanValue());
        }
        return b;
    }
}
