package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzas;
import com.google.android.gms.measurement.internal.zzkq;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: f45  reason: default package */
/* loaded from: classes.dex */
public final class f45 implements Parcelable.Creator<zzaa> {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzaa createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        String str2 = null;
        zzkq zzkqVar = null;
        String str3 = null;
        zzas zzasVar = null;
        zzas zzasVar2 = null;
        zzas zzasVar3 = null;
        boolean z = false;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 3:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 4:
                    zzkqVar = (zzkq) SafeParcelReader.o(parcel, C, zzkq.CREATOR);
                    break;
                case 5:
                    j = SafeParcelReader.F(parcel, C);
                    break;
                case 6:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 7:
                    str3 = SafeParcelReader.p(parcel, C);
                    break;
                case 8:
                    zzasVar = (zzas) SafeParcelReader.o(parcel, C, zzas.CREATOR);
                    break;
                case 9:
                    j2 = SafeParcelReader.F(parcel, C);
                    break;
                case 10:
                    zzasVar2 = (zzas) SafeParcelReader.o(parcel, C, zzas.CREATOR);
                    break;
                case 11:
                    j3 = SafeParcelReader.F(parcel, C);
                    break;
                case 12:
                    zzasVar3 = (zzas) SafeParcelReader.o(parcel, C, zzas.CREATOR);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzaa(str, str2, zzkqVar, j, z, str3, zzasVar, j2, zzasVar2, j3, zzasVar3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzaa[] newArray(int i) {
        return new zzaa[i];
    }
}
