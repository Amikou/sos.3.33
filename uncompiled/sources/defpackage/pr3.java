package defpackage;

import java.util.List;

/* renamed from: pr3  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class pr3 implements m60 {
    public final List a;

    public pr3(List list) {
        this.a = list;
    }

    public static m60 a(List list) {
        return new pr3(list);
    }

    @Override // defpackage.m60
    public void accept(Object obj) {
        this.a.add(obj);
    }
}
