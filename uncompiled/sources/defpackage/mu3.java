package defpackage;

import android.util.Base64;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: String.kt */
/* renamed from: mu3  reason: default package */
/* loaded from: classes2.dex */
public final class mu3 {
    public static final byte[] a(String str) {
        fs1.f(str, "<this>");
        byte[] decode = Base64.decode(str, 0);
        fs1.e(decode, "decode(this, Base64.DEFAULT)");
        return decode;
    }

    public static final String b(String str) {
        fs1.f(str, "token");
        return fs1.l("https://cloudflare-ipfs.com/ipfs/", str);
    }

    public static final String c(String str) {
        fs1.f(str, "<this>");
        if (dv3.H(str, "ipfs", false, 2, null)) {
            List w0 = StringsKt__StringsKt.w0(str, new String[]{"://"}, false, 0, 6, null);
            String str2 = w0.size() >= 2 ? (String) w0.get(1) : null;
            if (str2 == null) {
                return null;
            }
            return b(str2);
        } else if (dv3.H(str, "http", false, 2, null)) {
            if (StringsKt__StringsKt.M(str, "pinata.cloud", false, 2, null) && StringsKt__StringsKt.M(str, "ipfs/", false, 2, null)) {
                List w02 = StringsKt__StringsKt.w0(str, new String[]{"/ipfs/"}, false, 0, 6, null);
                if (w02.size() >= 2) {
                    str = b((String) w02.get(1));
                }
            }
            return str;
        } else {
            return null;
        }
    }
}
