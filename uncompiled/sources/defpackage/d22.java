package defpackage;

import java.math.BigInteger;

/* renamed from: d22  reason: default package */
/* loaded from: classes2.dex */
public class d22 implements Cloneable {
    public static final short[] f0 = {0, 1, 4, 5, 16, 17, 20, 21, 64, 65, 68, 69, 80, 81, 84, 85, 256, 257, 260, 261, 272, 273, 276, 277, 320, 321, 324, 325, 336, 337, 340, 341, 1024, 1025, 1028, 1029, 1040, 1041, 1044, 1045, 1088, 1089, 1092, 1093, 1104, 1105, 1108, 1109, 1280, 1281, 1284, 1285, 1296, 1297, 1300, 1301, 1344, 1345, 1348, 1349, 1360, 1361, 1364, 1365, 4096, 4097, 4100, 4101, 4112, 4113, 4116, 4117, 4160, 4161, 4164, 4165, 4176, 4177, 4180, 4181, 4352, 4353, 4356, 4357, 4368, 4369, 4372, 4373, 4416, 4417, 4420, 4421, 4432, 4433, 4436, 4437, 5120, 5121, 5124, 5125, 5136, 5137, 5140, 5141, 5184, 5185, 5188, 5189, 5200, 5201, 5204, 5205, 5376, 5377, 5380, 5381, 5392, 5393, 5396, 5397, 5440, 5441, 5444, 5445, 5456, 5457, 5460, 5461, 16384, 16385, 16388, 16389, 16400, 16401, 16404, 16405, 16448, 16449, 16452, 16453, 16464, 16465, 16468, 16469, 16640, 16641, 16644, 16645, 16656, 16657, 16660, 16661, 16704, 16705, 16708, 16709, 16720, 16721, 16724, 16725, 17408, 17409, 17412, 17413, 17424, 17425, 17428, 17429, 17472, 17473, 17476, 17477, 17488, 17489, 17492, 17493, 17664, 17665, 17668, 17669, 17680, 17681, 17684, 17685, 17728, 17729, 17732, 17733, 17744, 17745, 17748, 17749, 20480, 20481, 20484, 20485, 20496, 20497, 20500, 20501, 20544, 20545, 20548, 20549, 20560, 20561, 20564, 20565, 20736, 20737, 20740, 20741, 20752, 20753, 20756, 20757, 20800, 20801, 20804, 20805, 20816, 20817, 20820, 20821, 21504, 21505, 21508, 21509, 21520, 21521, 21524, 21525, 21568, 21569, 21572, 21573, 21584, 21585, 21588, 21589, 21760, 21761, 21764, 21765, 21776, 21777, 21780, 21781, 21824, 21825, 21828, 21829, 21840, 21841, 21844, 21845};
    public static final byte[] g0 = {0, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8};
    public long[] a;

    public d22(int i) {
        this.a = new long[i];
    }

    public d22(BigInteger bigInteger) {
        int i;
        if (bigInteger == null || bigInteger.signum() < 0) {
            throw new IllegalArgumentException("invalid F2m field value");
        }
        if (bigInteger.signum() == 0) {
            this.a = new long[]{0};
            return;
        }
        byte[] byteArray = bigInteger.toByteArray();
        int length = byteArray.length;
        if (byteArray[0] == 0) {
            length--;
            i = 1;
        } else {
            i = 0;
        }
        int i2 = (length + 7) / 8;
        this.a = new long[i2];
        int i3 = i2 - 1;
        int i4 = (length % 8) + i;
        if (i < i4) {
            long j = 0;
            while (i < i4) {
                j = (j << 8) | (byteArray[i] & 255);
                i++;
            }
            this.a[i3] = j;
            i3--;
        }
        while (i3 >= 0) {
            long j2 = 0;
            int i5 = 0;
            while (i5 < 8) {
                j2 = (j2 << 8) | (byteArray[i] & 255);
                i5++;
                i++;
            }
            this.a[i3] = j2;
            i3--;
        }
    }

    public d22(long[] jArr) {
        this.a = jArr;
    }

    public d22(long[] jArr, int i, int i2) {
        if (i == 0 && i2 == jArr.length) {
            this.a = jArr;
            return;
        }
        long[] jArr2 = new long[i2];
        this.a = jArr2;
        System.arraycopy(jArr, i, jArr2, 0, i2);
    }

    public static void H(long j, long[] jArr, int i, long[] jArr2, int i2) {
        if ((j & 1) != 0) {
            a(jArr2, i2, jArr, 0, i);
        }
        long j2 = j;
        int i3 = 1;
        while (true) {
            long j3 = j2 >>> 1;
            if (j3 == 0) {
                return;
            }
            if ((j3 & 1) != 0) {
                long j4 = j(jArr2, i2, jArr, 0, i, i3);
                if (j4 != 0) {
                    int i4 = i2 + i;
                    jArr2[i4] = j4 ^ jArr2[i4];
                }
            }
            i3++;
            j2 = j3;
        }
    }

    public static void J(long[] jArr, int i, int i2, int i3, int[] iArr) {
        r(jArr, i, i2);
        int i4 = i2 - i3;
        int length = iArr.length;
        while (true) {
            length--;
            if (length < 0) {
                r(jArr, i, i4);
                return;
            }
            r(jArr, i, iArr[length] + i4);
        }
    }

    public static void K(long[] jArr, int i, int i2, int i3, int[] iArr) {
        while (true) {
            i2--;
            if (i2 < i3) {
                return;
            }
            if (U(jArr, i, i2)) {
                J(jArr, i, i2, i3, iArr);
            }
        }
    }

    public static int L(long[] jArr, int i, int i2, int i3, int[] iArr) {
        int i4 = (i3 + 63) >>> 6;
        if (i2 < i4) {
            return i2;
        }
        int i5 = i2 << 6;
        int min = Math.min(i5, (i3 << 1) - 1);
        int i6 = i5 - min;
        int i7 = i2;
        while (i6 >= 64) {
            i7--;
            i6 -= 64;
        }
        int length = iArr.length;
        int i8 = iArr[length - 1];
        int i9 = length > 1 ? iArr[length - 2] : 0;
        int max = Math.max(i3, i8 + 64);
        int min2 = (i6 + Math.min(min - max, i3 - i9)) >> 6;
        if (min2 > 1) {
            int i10 = i7 - min2;
            N(jArr, i, i7, i10, i3, iArr);
            while (i7 > i10) {
                i7--;
                jArr[i + i7] = 0;
            }
            min = i10 << 6;
        }
        if (min > max) {
            P(jArr, i, i7, max, i3, iArr);
        } else {
            max = min;
        }
        if (max > i3) {
            K(jArr, i, max, i3, iArr);
        }
        return i4;
    }

    public static d22 M(long[] jArr, int i, int i2, int i3, int[] iArr) {
        return new d22(jArr, i, L(jArr, i, i2, i3, iArr));
    }

    public static void N(long[] jArr, int i, int i2, int i3, int i4, int[] iArr) {
        int i5 = (i3 << 6) - i4;
        int length = iArr.length;
        while (true) {
            length--;
            if (length < 0) {
                s(jArr, i, jArr, i + i3, i2 - i3, i5);
                return;
            }
            s(jArr, i, jArr, i + i3, i2 - i3, i5 + iArr[length]);
        }
    }

    public static void O(long[] jArr, int i, int i2, long j, int i3, int[] iArr) {
        int i4 = i2 - i3;
        int length = iArr.length;
        while (true) {
            length--;
            if (length < 0) {
                t(jArr, i, i4, j);
                return;
            }
            t(jArr, i, iArr[length] + i4, j);
        }
    }

    public static void P(long[] jArr, int i, int i2, int i3, int i4, int[] iArr) {
        int i5 = i3 >>> 6;
        int i6 = i2;
        while (true) {
            int i7 = i6 - 1;
            if (i7 <= i5) {
                break;
            }
            int i8 = i + i7;
            long j = jArr[i8];
            if (j != 0) {
                jArr[i8] = 0;
                O(jArr, i, i7 << 6, j, i4, iArr);
            }
            i6 = i7;
        }
        int i9 = i3 & 63;
        int i10 = i + i5;
        long j2 = jArr[i10] >>> i9;
        if (j2 != 0) {
            jArr[i10] = jArr[i10] ^ (j2 << i9);
            O(jArr, i, i3, j2, i4, iArr);
        }
    }

    public static long R(long[] jArr, int i, long[] jArr2, int i2, int i3, int i4) {
        int i5 = 64 - i4;
        long j = 0;
        for (int i6 = 0; i6 < i3; i6++) {
            long j2 = jArr[i + i6];
            jArr2[i2 + i6] = j | (j2 << i4);
            j = j2 >>> i5;
        }
        return j;
    }

    public static void T(long[] jArr, int i, int i2, int[] iArr) {
        int i3 = i << 1;
        while (true) {
            i--;
            if (i < 0) {
                return;
            }
            long j = jArr[i];
            int i4 = i3 - 1;
            jArr[i4] = x((int) (j >>> 32));
            i3 = i4 - 1;
            jArr[i3] = x((int) j);
        }
    }

    public static boolean U(long[] jArr, int i, int i2) {
        return (jArr[i + (i2 >>> 6)] & (1 << (i2 & 63))) != 0;
    }

    public static void a(long[] jArr, int i, long[] jArr2, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            int i5 = i + i4;
            jArr[i5] = jArr[i5] ^ jArr2[i2 + i4];
        }
    }

    public static void b(long[] jArr, int i, long[] jArr2, int i2, long[] jArr3, int i3, int i4) {
        for (int i5 = 0; i5 < i4; i5++) {
            jArr3[i3 + i5] = jArr[i + i5] ^ jArr2[i2 + i5];
        }
    }

    public static void d(long[] jArr, int i, long[] jArr2, int i2, long[] jArr3, int i3, int i4) {
        for (int i5 = 0; i5 < i4; i5++) {
            int i6 = i + i5;
            jArr[i6] = jArr[i6] ^ (jArr2[i2 + i5] ^ jArr3[i3 + i5]);
        }
    }

    public static long h(long[] jArr, int i, long[] jArr2, int i2, int i3, int i4) {
        int i5 = 64 - i4;
        long j = 0;
        while (true) {
            i3--;
            if (i3 < 0) {
                return j;
            }
            long j2 = jArr2[i2 + i3];
            int i6 = i + i3;
            jArr[i6] = (j | (j2 >>> i4)) ^ jArr[i6];
            j = j2 << i5;
        }
    }

    public static long j(long[] jArr, int i, long[] jArr2, int i2, int i3, int i4) {
        int i5 = 64 - i4;
        long j = 0;
        for (int i6 = 0; i6 < i3; i6++) {
            long j2 = jArr2[i2 + i6];
            int i7 = i + i6;
            jArr[i7] = (j | (j2 << i4)) ^ jArr[i7];
            j = j2 >>> i5;
        }
        return j;
    }

    public static int k(long j) {
        int i;
        int i2 = 32;
        int i3 = (int) (j >>> 32);
        if (i3 == 0) {
            i3 = (int) j;
            i2 = 0;
        }
        int i4 = i3 >>> 16;
        if (i4 == 0) {
            int i5 = i3 >>> 8;
            i = i5 == 0 ? g0[i3] : g0[i5] + 8;
        } else {
            int i6 = i4 >>> 8;
            i = i6 == 0 ? g0[i4] + 16 : g0[i6] + 24;
        }
        return i2 + i;
    }

    public static void r(long[] jArr, int i, int i2) {
        int i3 = i + (i2 >>> 6);
        jArr[i3] = jArr[i3] ^ (1 << (i2 & 63));
    }

    public static void s(long[] jArr, int i, long[] jArr2, int i2, int i3, int i4) {
        int i5 = i + (i4 >>> 6);
        int i6 = i4 & 63;
        if (i6 == 0) {
            a(jArr, i5, jArr2, i2, i3);
        } else {
            jArr[i5] = h(jArr, i5 + 1, jArr2, i2, i3, 64 - i6) ^ jArr[i5];
        }
    }

    public static void t(long[] jArr, int i, int i2, long j) {
        int i3 = i + (i2 >>> 6);
        int i4 = i2 & 63;
        if (i4 == 0) {
            jArr[i3] = jArr[i3] ^ j;
            return;
        }
        jArr[i3] = jArr[i3] ^ (j << i4);
        long j2 = j >>> (64 - i4);
        if (j2 != 0) {
            int i5 = i3 + 1;
            jArr[i5] = j2 ^ jArr[i5];
        }
    }

    public static long x(int i) {
        short[] sArr = f0;
        int i2 = sArr[i & 255] | (sArr[(i >>> 8) & 255] << 16);
        short s = sArr[(i >>> 16) & 255];
        return (i2 & 4294967295L) | ((((sArr[i >>> 24] << 16) | s) & 4294967295L) << 32);
    }

    public boolean A() {
        for (long j : this.a) {
            if (j != 0) {
                return false;
            }
        }
        return true;
    }

    public d22 B(int i, int[] iArr) {
        int o = o();
        if (o == 0) {
            throw new IllegalStateException();
        }
        int i2 = 1;
        if (o == 1) {
            return this;
        }
        int i3 = (i + 63) >>> 6;
        d22 d22Var = new d22(i3);
        J(d22Var.a, 0, i, i, iArr);
        d22 d22Var2 = new d22(i3);
        d22Var2.a[0] = 1;
        d22 d22Var3 = new d22(i3);
        int[] iArr2 = new int[2];
        iArr2[0] = o;
        iArr2[1] = i + 1;
        d22[] d22VarArr = {(d22) clone(), d22Var};
        int[] iArr3 = {1, 0};
        d22[] d22VarArr2 = {d22Var2, d22Var3};
        int i4 = iArr2[1];
        int i5 = iArr3[1];
        int i6 = i4 - iArr2[0];
        while (true) {
            if (i6 < 0) {
                i6 = -i6;
                iArr2[i2] = i4;
                iArr3[i2] = i5;
                int i7 = 1 - i2;
                int i8 = iArr2[i7];
                i5 = iArr3[i7];
                i2 = i7;
                i4 = i8;
            }
            int i9 = 1 - i2;
            d22VarArr[i2].f(d22VarArr[i9], iArr2[i9], i6);
            int p = d22VarArr[i2].p(i4);
            if (p == 0) {
                return d22VarArr2[i9];
            }
            int i10 = iArr3[i9];
            d22VarArr2[i2].f(d22VarArr2[i9], i10, i6);
            int i11 = i10 + i6;
            if (i11 > i5) {
                i5 = i11;
            } else if (i11 == i5) {
                i5 = d22VarArr2[i2].p(i5);
            }
            i6 += p - i4;
            i4 = p;
        }
    }

    public d22 C(d22 d22Var, int i, int[] iArr) {
        int i2;
        int i3;
        d22 d22Var2;
        d22 d22Var3;
        int i4;
        long[] jArr;
        int i5;
        int o = o();
        if (o == 0) {
            return this;
        }
        int o2 = d22Var.o();
        if (o2 == 0) {
            return d22Var;
        }
        if (o > o2) {
            i3 = o;
            i2 = o2;
            d22Var3 = this;
            d22Var2 = d22Var;
        } else {
            i2 = o;
            i3 = o2;
            d22Var2 = this;
            d22Var3 = d22Var;
        }
        int i6 = (i2 + 63) >>> 6;
        int i7 = (i3 + 63) >>> 6;
        int i8 = ((i2 + i3) + 62) >>> 6;
        if (i6 == 1) {
            long j = d22Var2.a[0];
            if (j == 1) {
                return d22Var3;
            }
            long[] jArr2 = new long[i8];
            H(j, d22Var3.a, i7, jArr2, 0);
            return M(jArr2, 0, i8, i, iArr);
        }
        int i9 = ((i3 + 7) + 63) >>> 6;
        int[] iArr2 = new int[16];
        int i10 = i9 << 4;
        long[] jArr3 = new long[i10];
        iArr2[1] = i9;
        System.arraycopy(d22Var3.a, 0, jArr3, i9, i7);
        int i11 = 2;
        int i12 = i9;
        for (int i13 = 16; i11 < i13; i13 = 16) {
            i12 += i9;
            iArr2[i11] = i12;
            if ((i11 & 1) == 0) {
                jArr = jArr3;
                i5 = i10;
                R(jArr3, i12 >>> 1, jArr3, i12, i9, 1);
            } else {
                jArr = jArr3;
                i5 = i10;
                b(jArr, i9, jArr3, i12 - i9, jArr, i12, i9);
            }
            i11++;
            i10 = i5;
            jArr3 = jArr;
        }
        long[] jArr4 = jArr3;
        int i14 = i10;
        long[] jArr5 = new long[i14];
        R(jArr4, 0, jArr5, 0, i14, 4);
        long[] jArr6 = d22Var2.a;
        int i15 = i8 << 3;
        long[] jArr7 = new long[i15];
        int i16 = 0;
        while (i16 < i6) {
            long j2 = jArr6[i16];
            int i17 = i16;
            while (true) {
                long j3 = j2 >>> 4;
                i4 = i16;
                d(jArr7, i17, jArr4, iArr2[((int) j2) & 15], jArr5, iArr2[((int) j3) & 15], i9);
                j2 = j3 >>> 4;
                if (j2 == 0) {
                    break;
                }
                i17 += i8;
                i16 = i4;
            }
            i16 = i4 + 1;
        }
        while (true) {
            i15 -= i8;
            if (i15 == 0) {
                return M(jArr7, 0, i8, i, iArr);
            }
            j(jArr7, i15 - i8, jArr7, i15, i8, 8);
        }
    }

    public d22 D(int i, int[] iArr) {
        int u = u();
        if (u == 0) {
            return this;
        }
        int i2 = u << 1;
        long[] jArr = new long[i2];
        int i3 = 0;
        while (i3 < i2) {
            long j = this.a[i3 >>> 1];
            int i4 = i3 + 1;
            jArr[i3] = x((int) j);
            i3 = i4 + 1;
            jArr[i4] = x((int) (j >>> 32));
        }
        return new d22(jArr, 0, L(jArr, 0, i2, i, iArr));
    }

    public d22 E(int i, int i2, int[] iArr) {
        int u = u();
        if (u == 0) {
            return this;
        }
        int i3 = ((i2 + 63) >>> 6) << 1;
        long[] jArr = new long[i3];
        System.arraycopy(this.a, 0, jArr, 0, u);
        while (true) {
            i--;
            if (i < 0) {
                return new d22(jArr, 0, u);
            }
            T(jArr, u, i2, iArr);
            u = L(jArr, 0, i3, i2, iArr);
        }
    }

    public d22 G(d22 d22Var, int i, int[] iArr) {
        int i2;
        int i3;
        d22 d22Var2;
        d22 d22Var3;
        long[] jArr;
        int i4;
        int o = o();
        if (o == 0) {
            return this;
        }
        int o2 = d22Var.o();
        if (o2 == 0) {
            return d22Var;
        }
        if (o > o2) {
            i3 = o;
            i2 = o2;
            d22Var3 = this;
            d22Var2 = d22Var;
        } else {
            i2 = o;
            i3 = o2;
            d22Var2 = this;
            d22Var3 = d22Var;
        }
        int i5 = (i2 + 63) >>> 6;
        int i6 = (i3 + 63) >>> 6;
        int i7 = ((i2 + i3) + 62) >>> 6;
        if (i5 == 1) {
            long j = d22Var2.a[0];
            if (j == 1) {
                return d22Var3;
            }
            long[] jArr2 = new long[i7];
            H(j, d22Var3.a, i6, jArr2, 0);
            return new d22(jArr2, 0, i7);
        }
        int i8 = ((i3 + 7) + 63) >>> 6;
        int[] iArr2 = new int[16];
        int i9 = i8 << 4;
        long[] jArr3 = new long[i9];
        iArr2[1] = i8;
        System.arraycopy(d22Var3.a, 0, jArr3, i8, i6);
        int i10 = 2;
        int i11 = i8;
        for (int i12 = 16; i10 < i12; i12 = 16) {
            i11 += i8;
            iArr2[i10] = i11;
            if ((i10 & 1) == 0) {
                jArr = jArr3;
                i4 = i9;
                R(jArr3, i11 >>> 1, jArr3, i11, i8, 1);
            } else {
                jArr = jArr3;
                i4 = i9;
                b(jArr, i8, jArr, i11 - i8, jArr3, i11, i8);
            }
            i10++;
            i9 = i4;
            jArr3 = jArr;
        }
        long[] jArr4 = jArr3;
        int i13 = i9;
        long[] jArr5 = new long[i13];
        R(jArr4, 0, jArr5, 0, i13, 4);
        long[] jArr6 = d22Var2.a;
        int i14 = i7 << 3;
        long[] jArr7 = new long[i14];
        for (int i15 = 0; i15 < i5; i15++) {
            long j2 = jArr6[i15];
            int i16 = i15;
            while (true) {
                long j3 = j2 >>> 4;
                d(jArr7, i16, jArr4, iArr2[((int) j2) & 15], jArr5, iArr2[((int) j3) & 15], i8);
                j2 = j3 >>> 4;
                if (j2 == 0) {
                    break;
                }
                i16 += i7;
            }
        }
        while (true) {
            i14 -= i7;
            if (i14 == 0) {
                return new d22(jArr7, 0, i7);
            }
            j(jArr7, i14 - i7, jArr7, i14, i7, 8);
        }
    }

    public void I(int i, int[] iArr) {
        long[] jArr = this.a;
        int L = L(jArr, 0, jArr.length, i, iArr);
        if (L < jArr.length) {
            long[] jArr2 = new long[L];
            this.a = jArr2;
            System.arraycopy(jArr, 0, jArr2, 0, L);
        }
    }

    public final long[] Q(int i) {
        long[] jArr = new long[i];
        long[] jArr2 = this.a;
        System.arraycopy(jArr2, 0, jArr, 0, Math.min(jArr2.length, i));
        return jArr;
    }

    public d22 S(int i, int[] iArr) {
        int u = u();
        if (u == 0) {
            return this;
        }
        int i2 = u << 1;
        long[] jArr = new long[i2];
        int i3 = 0;
        while (i3 < i2) {
            long j = this.a[i3 >>> 1];
            int i4 = i3 + 1;
            jArr[i3] = x((int) j);
            i3 = i4 + 1;
            jArr[i4] = x((int) (j >>> 32));
        }
        return new d22(jArr, 0, i2);
    }

    public boolean V() {
        long[] jArr = this.a;
        return jArr.length > 0 && (1 & jArr[0]) != 0;
    }

    public BigInteger W() {
        int u = u();
        if (u == 0) {
            return ws0.a;
        }
        int i = u - 1;
        long j = this.a[i];
        byte[] bArr = new byte[8];
        int i2 = 0;
        boolean z = false;
        for (int i3 = 7; i3 >= 0; i3--) {
            byte b = (byte) (j >>> (i3 * 8));
            if (z || b != 0) {
                bArr[i2] = b;
                i2++;
                z = true;
            }
        }
        byte[] bArr2 = new byte[(i * 8) + i2];
        for (int i4 = 0; i4 < i2; i4++) {
            bArr2[i4] = bArr[i4];
        }
        for (int i5 = u - 2; i5 >= 0; i5--) {
            long j2 = this.a[i5];
            int i6 = 7;
            while (i6 >= 0) {
                bArr2[i2] = (byte) (j2 >>> (i6 * 8));
                i6--;
                i2++;
            }
        }
        return new BigInteger(1, bArr2);
    }

    public Object clone() {
        return new d22(wh.g(this.a));
    }

    public d22 e() {
        if (this.a.length == 0) {
            return new d22(new long[]{1});
        }
        long[] Q = Q(Math.max(1, u()));
        Q[0] = 1 ^ Q[0];
        return new d22(Q);
    }

    public boolean equals(Object obj) {
        if (obj instanceof d22) {
            d22 d22Var = (d22) obj;
            int u = u();
            if (d22Var.u() != u) {
                return false;
            }
            for (int i = 0; i < u; i++) {
                if (this.a[i] != d22Var.a[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public final void f(d22 d22Var, int i, int i2) {
        int i3 = (i + 63) >>> 6;
        int i4 = i2 >>> 6;
        int i5 = i2 & 63;
        if (i5 == 0) {
            a(this.a, i4, d22Var.a, 0, i3);
            return;
        }
        long j = j(this.a, i4, d22Var.a, 0, i3, i5);
        if (j != 0) {
            long[] jArr = this.a;
            int i6 = i3 + i4;
            jArr[i6] = j ^ jArr[i6];
        }
    }

    public void g(d22 d22Var, int i) {
        int u = d22Var.u();
        if (u == 0) {
            return;
        }
        int i2 = u + i;
        if (i2 > this.a.length) {
            this.a = Q(i2);
        }
        a(this.a, i, d22Var.a, 0, u);
    }

    public int hashCode() {
        int u = u();
        int i = 1;
        for (int i2 = 0; i2 < u; i2++) {
            long j = this.a[i2];
            i = (((i * 31) ^ ((int) j)) * 31) ^ ((int) (j >>> 32));
        }
        return i;
    }

    public void l(long[] jArr, int i) {
        long[] jArr2 = this.a;
        System.arraycopy(jArr2, 0, jArr, i, jArr2.length);
    }

    public int o() {
        int length = this.a.length;
        while (length != 0) {
            length--;
            long j = this.a[length];
            if (j != 0) {
                return (length << 6) + k(j);
            }
        }
        return 0;
    }

    public final int p(int i) {
        int i2 = (i + 62) >>> 6;
        while (i2 != 0) {
            i2--;
            long j = this.a[i2];
            if (j != 0) {
                return (i2 << 6) + k(j);
            }
        }
        return 0;
    }

    public String toString() {
        int u = u();
        if (u == 0) {
            return "0";
        }
        int i = u - 1;
        StringBuffer stringBuffer = new StringBuffer(Long.toBinaryString(this.a[i]));
        while (true) {
            i--;
            if (i < 0) {
                return stringBuffer.toString();
            }
            String binaryString = Long.toBinaryString(this.a[i]);
            int length = binaryString.length();
            if (length < 64) {
                stringBuffer.append("0000000000000000000000000000000000000000000000000000000000000000".substring(length));
            }
            stringBuffer.append(binaryString);
        }
    }

    public int u() {
        return v(this.a.length);
    }

    public int v(int i) {
        long[] jArr = this.a;
        int min = Math.min(i, jArr.length);
        if (min < 1) {
            return 0;
        }
        if (jArr[0] != 0) {
            do {
                min--;
            } while (jArr[min] == 0);
            return min + 1;
        }
        do {
            min--;
            if (jArr[min] != 0) {
                return min + 1;
            }
        } while (min > 0);
        return 0;
    }

    public boolean y() {
        long[] jArr = this.a;
        if (jArr[0] != 1) {
            return false;
        }
        for (int i = 1; i < jArr.length; i++) {
            if (jArr[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
