package defpackage;

import androidx.media3.common.ParserException;
import java.io.EOFException;
import java.io.IOException;

/* compiled from: ExtractorUtil.java */
/* renamed from: s11  reason: default package */
/* loaded from: classes.dex */
public final class s11 {
    public static void a(boolean z, String str) throws ParserException {
        if (!z) {
            throw ParserException.createForMalformedContainer(str, null);
        }
    }

    public static boolean b(q11 q11Var, byte[] bArr, int i, int i2, boolean z) throws IOException {
        try {
            return q11Var.d(bArr, i, i2, z);
        } catch (EOFException e) {
            if (z) {
                return false;
            }
            throw e;
        }
    }

    public static int c(q11 q11Var, byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        while (i3 < i2) {
            int h = q11Var.h(bArr, i + i3, i2 - i3);
            if (h == -1) {
                break;
            }
            i3 += h;
        }
        return i3;
    }

    public static boolean d(q11 q11Var, byte[] bArr, int i, int i2) throws IOException {
        try {
            q11Var.readFully(bArr, i, i2);
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }

    public static boolean e(q11 q11Var, int i) throws IOException {
        try {
            q11Var.k(i);
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }
}
