package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: sh3  reason: default package */
/* loaded from: classes2.dex */
public class sh3 extends xs0.b {
    public th3 j;

    /* renamed from: sh3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] b = jd2.b();
            long[] b2 = jd2.b();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                long j = ((i3 ^ i) - 1) >> 31;
                for (int i4 = 0; i4 < 9; i4++) {
                    long j2 = b[i4];
                    long[] jArr = this.b;
                    b[i4] = j2 ^ (jArr[i2 + i4] & j);
                    b2[i4] = b2[i4] ^ (jArr[(i2 + 9) + i4] & j);
                }
                i2 += 18;
            }
            return sh3.this.i(new rh3(b), new rh3(b2), false);
        }
    }

    public sh3() {
        super(571, 2, 5, 10);
        this.j = new th3(this, null, null);
        this.b = n(BigInteger.valueOf(0L));
        this.c = n(BigInteger.valueOf(1L));
        this.d = new BigInteger(1, pk1.a("020000000000000000000000000000000000000000000000000000000000000000000000131850E1F19A63E4B391A8DB917F4138B630D84BE5D639381E91DEB45CFE778F637C1001"));
        this.e = BigInteger.valueOf(4L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return true;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new sh3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 9 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            jd2.a(((rh3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 9;
            jd2.a(((rh3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 9;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public it0 f() {
        return new ll4();
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new th3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new th3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new rh3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 571;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
