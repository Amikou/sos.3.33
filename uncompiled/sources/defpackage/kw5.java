package defpackage;

import com.google.android.gms.internal.measurement.zzjd;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: kw5  reason: default package */
/* loaded from: classes.dex */
public final class kw5 extends br5<String> implements RandomAccess, nw5 {
    public static final kw5 g0;
    public final List<Object> f0;

    static {
        kw5 kw5Var = new kw5(10);
        g0 = kw5Var;
        kw5Var.zzb();
    }

    public kw5() {
        this(10);
    }

    public static String k(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzjd) {
            return ((zzjd) obj).zzl(cw5.a);
        }
        return cw5.d((byte[]) obj);
    }

    @Override // defpackage.nw5
    public final void M(zzjd zzjdVar) {
        e();
        this.f0.add(zzjdVar);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.zv5
    public final /* bridge */ /* synthetic */ zv5 Q(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f0);
            return new kw5(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ void add(int i, Object obj) {
        e();
        this.f0.add(i, (String) obj);
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.List
    public final boolean addAll(int i, Collection<? extends String> collection) {
        e();
        if (collection instanceof nw5) {
            collection = ((nw5) collection).f();
        }
        boolean addAll = this.f0.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final void clear() {
        e();
        this.f0.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // defpackage.nw5
    public final List<?> f() {
        return Collections.unmodifiableList(this.f0);
    }

    @Override // defpackage.nw5
    public final nw5 g() {
        return zza() ? new nz5(this) : this;
    }

    @Override // java.util.AbstractList, java.util.List
    /* renamed from: i */
    public final String get(int i) {
        Object obj = this.f0.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzjd) {
            zzjd zzjdVar = (zzjd) obj;
            String zzl = zzjdVar.zzl(cw5.a);
            if (zzjdVar.zzh()) {
                this.f0.set(i, zzl);
            }
            return zzl;
        }
        byte[] bArr = (byte[]) obj;
        String d = cw5.d(bArr);
        if (cw5.c(bArr)) {
            this.f0.set(i, d);
        }
        return d;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object remove(int i) {
        e();
        Object remove = this.f0.remove(i);
        ((AbstractList) this).modCount++;
        return k(remove);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        e();
        return k(this.f0.set(i, (String) obj));
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.f0.size();
    }

    @Override // defpackage.nw5
    public final Object v1(int i) {
        return this.f0.get(i);
    }

    public kw5(int i) {
        this.f0 = new ArrayList(i);
    }

    public kw5(ArrayList<Object> arrayList) {
        this.f0 = arrayList;
    }

    @Override // defpackage.br5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }
}
