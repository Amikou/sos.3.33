package defpackage;

import androidx.media3.datasource.b;
import java.io.IOException;

/* compiled from: DataSourceUtil.java */
/* renamed from: he0  reason: default package */
/* loaded from: classes.dex */
public final class he0 {
    public static void a(b bVar) {
        if (bVar != null) {
            try {
                bVar.close();
            } catch (IOException unused) {
            }
        }
    }
}
