package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: RequestListener.java */
/* renamed from: h73  reason: default package */
/* loaded from: classes.dex */
public interface h73 extends jv2 {
    void a(ImageRequest imageRequest, Object obj, String str, boolean z);

    void c(ImageRequest imageRequest, String str, boolean z);

    void g(ImageRequest imageRequest, String str, Throwable th, boolean z);

    void k(String str);
}
