package defpackage;

import android.content.Context;
import androidx.activity.OnBackPressedDispatcher;
import androidx.navigation.NavController;

/* compiled from: NavHostController.java */
/* renamed from: de2  reason: default package */
/* loaded from: classes.dex */
public class de2 extends NavController {
    public de2(Context context) {
        super(context);
    }

    @Override // androidx.navigation.NavController
    public final void I(rz1 rz1Var) {
        super.I(rz1Var);
    }

    @Override // androidx.navigation.NavController
    public final void J(OnBackPressedDispatcher onBackPressedDispatcher) {
        super.J(onBackPressedDispatcher);
    }

    @Override // androidx.navigation.NavController
    public final void K(gj4 gj4Var) {
        super.K(gj4Var);
    }

    @Override // androidx.navigation.NavController
    public final void c(boolean z) {
        super.c(z);
    }
}
