package defpackage;

import defpackage.pt0;
import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: ll4  reason: default package */
/* loaded from: classes2.dex */
public class ll4 extends x4 {

    /* renamed from: ll4$a */
    /* loaded from: classes2.dex */
    public static class a implements tt2 {
        public final /* synthetic */ pt0.b a;
        public final /* synthetic */ byte b;

        public a(pt0.b bVar, byte b) {
            this.a = bVar;
            this.b = b;
        }

        @Override // defpackage.tt2
        public ut2 a(ut2 ut2Var) {
            if (ut2Var instanceof ml4) {
                return ut2Var;
            }
            ml4 ml4Var = new ml4();
            ml4Var.b(q64.d(this.a, this.b));
            return ml4Var;
        }
    }

    public static pt0.b d(pt0.b bVar, byte[] bArr) {
        xs0.b bVar2 = (xs0.b) bVar.i();
        pt0.b[] a2 = ((ml4) bVar2.C(bVar, "bc_wtnaf", new a(bVar, bVar2.o().t().byteValue()))).a();
        pt0.b[] bVarArr = new pt0.b[a2.length];
        for (int i = 0; i < a2.length; i++) {
            bVarArr[i] = (pt0.b) a2[i].z();
        }
        pt0.b bVar3 = (pt0.b) bVar.i().v();
        int i2 = 0;
        for (int length = bArr.length - 1; length >= 0; length--) {
            i2++;
            byte b = bArr[length];
            if (b != 0) {
                bVar3 = (pt0.b) bVar3.L(i2).a(b > 0 ? a2[b >>> 1] : bVarArr[(-b) >>> 1]);
                i2 = 0;
            }
        }
        return i2 > 0 ? bVar3.L(i2) : bVar3;
    }

    @Override // defpackage.x4
    public pt0 c(pt0 pt0Var, BigInteger bigInteger) {
        if (pt0Var instanceof pt0.b) {
            pt0.b bVar = (pt0.b) pt0Var;
            xs0.b bVar2 = (xs0.b) bVar.i();
            int u = bVar2.u();
            byte byteValue = bVar2.o().t().byteValue();
            byte c = q64.c(byteValue);
            return e(bVar, q64.j(bigInteger, u, byteValue, bVar2.G(), c, (byte) 10), byteValue, c);
        }
        throw new IllegalArgumentException("Only ECPoint.AbstractF2m can be used in WTauNafMultiplier");
    }

    public final pt0.b e(pt0.b bVar, qs4 qs4Var, byte b, byte b2) {
        qs4[] qs4VarArr = b == 0 ? q64.d : q64.f;
        return d(bVar, q64.l(b2, qs4Var, (byte) 4, BigInteger.valueOf(16L), q64.g(b2, 4), qs4VarArr));
    }
}
