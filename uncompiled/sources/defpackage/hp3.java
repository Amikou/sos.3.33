package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import java.io.OutputStream;

/* compiled from: SimpleImageTranscoder.java */
/* renamed from: hp3  reason: default package */
/* loaded from: classes.dex */
public class hp3 implements ap1 {
    public final boolean a;
    public final int b;

    public hp3(boolean z, int i) {
        this.a = z;
        this.b = i;
    }

    public static Bitmap.CompressFormat e(wn1 wn1Var) {
        if (wn1Var == null) {
            return Bitmap.CompressFormat.JPEG;
        }
        if (wn1Var == wj0.a) {
            return Bitmap.CompressFormat.JPEG;
        }
        if (wn1Var == wj0.b) {
            return Bitmap.CompressFormat.PNG;
        }
        if (Build.VERSION.SDK_INT >= 14 && wj0.a(wn1Var)) {
            return Bitmap.CompressFormat.WEBP;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @Override // defpackage.ap1
    public String a() {
        return "SimpleImageTranscoder";
    }

    @Override // defpackage.ap1
    public boolean b(wn1 wn1Var) {
        return wn1Var == wj0.k || wn1Var == wj0.a;
    }

    @Override // defpackage.ap1
    public boolean c(zu0 zu0Var, p93 p93Var, p73 p73Var) {
        if (p93Var == null) {
            p93Var = p93.a();
        }
        return this.a && mq0.b(p93Var, p73Var, zu0Var, this.b) > 1;
    }

    @Override // defpackage.ap1
    public zo1 d(zu0 zu0Var, OutputStream outputStream, p93 p93Var, p73 p73Var, wn1 wn1Var, Integer num) {
        hp3 hp3Var;
        p93 p93Var2;
        Bitmap bitmap;
        Throwable th;
        OutOfMemoryError e;
        Integer num2 = num == null ? 85 : num;
        if (p93Var == null) {
            p93Var2 = p93.a();
            hp3Var = this;
        } else {
            hp3Var = this;
            p93Var2 = p93Var;
        }
        int f = hp3Var.f(zu0Var, p93Var2, p73Var);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = f;
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(zu0Var.m(), null, options);
            if (decodeStream == null) {
                v11.h("SimpleImageTranscoder", "Couldn't decode the EncodedImage InputStream ! ");
                return new zo1(2);
            }
            Matrix f2 = ou1.f(zu0Var, p93Var2);
            if (f2 != null) {
                try {
                    bitmap = Bitmap.createBitmap(decodeStream, 0, 0, decodeStream.getWidth(), decodeStream.getHeight(), f2, false);
                } catch (OutOfMemoryError e2) {
                    e = e2;
                    bitmap = decodeStream;
                    v11.i("SimpleImageTranscoder", "Out-Of-Memory during transcode", e);
                    zo1 zo1Var = new zo1(2);
                    bitmap.recycle();
                    decodeStream.recycle();
                    return zo1Var;
                } catch (Throwable th2) {
                    th = th2;
                    bitmap = decodeStream;
                    bitmap.recycle();
                    decodeStream.recycle();
                    throw th;
                }
            } else {
                bitmap = decodeStream;
            }
            try {
                try {
                    bitmap.compress(e(wn1Var), num2.intValue(), outputStream);
                    zo1 zo1Var2 = new zo1(f > 1 ? 0 : 1);
                    bitmap.recycle();
                    decodeStream.recycle();
                    return zo1Var2;
                } catch (Throwable th3) {
                    th = th3;
                    bitmap.recycle();
                    decodeStream.recycle();
                    throw th;
                }
            } catch (OutOfMemoryError e3) {
                e = e3;
                v11.i("SimpleImageTranscoder", "Out-Of-Memory during transcode", e);
                zo1 zo1Var3 = new zo1(2);
                bitmap.recycle();
                decodeStream.recycle();
                return zo1Var3;
            }
        } catch (OutOfMemoryError e4) {
            v11.i("SimpleImageTranscoder", "Out-Of-Memory during transcode", e4);
            return new zo1(2);
        }
    }

    public final int f(zu0 zu0Var, p93 p93Var, p73 p73Var) {
        if (this.a) {
            return mq0.b(p93Var, p73Var, zu0Var, this.b);
        }
        return 1;
    }
}
