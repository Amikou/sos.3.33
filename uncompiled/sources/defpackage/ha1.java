package defpackage;

import android.view.View;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentGoogleBackupBinding.java */
/* renamed from: ha1  reason: default package */
/* loaded from: classes2.dex */
public final class ha1 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final ShapeableImageView c;
    public final MaterialButton d;
    public final MaterialTextView e;

    public ha1(MaterialCardView materialCardView, MaterialButton materialButton, MaterialButton materialButton2, ShapeableImageView shapeableImageView, ShapeableImageView shapeableImageView2, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialButton materialButton3, MaterialTextView materialTextView4) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = shapeableImageView2;
        this.d = materialButton3;
        this.e = materialTextView4;
    }

    public static ha1 a(View view) {
        int i = R.id.btnGotit;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnGotit);
        if (materialButton != null) {
            i = R.id.dialog_cross;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
            if (materialButton2 != null) {
                i = R.id.imgAuthIcon;
                ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgAuthIcon);
                if (shapeableImageView != null) {
                    i = R.id.imgBackupQR;
                    ShapeableImageView shapeableImageView2 = (ShapeableImageView) ai4.a(view, R.id.imgBackupQR);
                    if (shapeableImageView2 != null) {
                        i = R.id.txtAuthKey;
                        MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAuthKey);
                        if (materialTextView != null) {
                            i = R.id.txtAuthScanDesc;
                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtAuthScanDesc);
                            if (materialTextView2 != null) {
                                i = R.id.txtAuthScanTitle;
                                MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtAuthScanTitle);
                                if (materialTextView3 != null) {
                                    i = R.id.txtCC;
                                    MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.txtCC);
                                    if (materialButton3 != null) {
                                        i = R.id.txtKey;
                                        MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtKey);
                                        if (materialTextView4 != null) {
                                            return new ha1((MaterialCardView) view, materialButton, materialButton2, shapeableImageView, shapeableImageView2, materialTextView, materialTextView2, materialTextView3, materialButton3, materialTextView4);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
