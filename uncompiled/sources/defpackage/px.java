package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;
import kotlinx.coroutines.flow.internal.ChannelFlow;
import kotlinx.coroutines.flow.internal.ChannelFlowOperator;

/* compiled from: ChannelFlow.kt */
/* renamed from: px  reason: default package */
/* loaded from: classes2.dex */
public final class px<T> extends ChannelFlowOperator<T, T> {
    public /* synthetic */ px(j71 j71Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow, int i2, qi0 qi0Var) {
        this(j71Var, (i2 & 2) != 0 ? EmptyCoroutineContext.INSTANCE : coroutineContext, (i2 & 4) != 0 ? -3 : i, (i2 & 8) != 0 ? BufferOverflow.SUSPEND : bufferOverflow);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlow
    public ChannelFlow<T> h(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        return new px(this.h0, coroutineContext, i, bufferOverflow);
    }

    @Override // kotlinx.coroutines.flow.internal.ChannelFlowOperator
    public Object o(k71<? super T> k71Var, q70<? super te4> q70Var) {
        Object a = this.h0.a(k71Var, q70Var);
        return a == gs1.d() ? a : te4.a;
    }

    public px(j71<? extends T> j71Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow) {
        super(j71Var, coroutineContext, i, bufferOverflow);
    }
}
