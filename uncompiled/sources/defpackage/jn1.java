package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import zendesk.support.request.CellBase;

/* compiled from: Id3Reader.java */
/* renamed from: jn1  reason: default package */
/* loaded from: classes.dex */
public final class jn1 implements ku0 {
    public f84 b;
    public boolean c;
    public int e;
    public int f;
    public final op2 a = new op2(10);
    public long d = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        ii.i(this.b);
        if (this.c) {
            int a = op2Var.a();
            int i = this.f;
            if (i < 10) {
                int min = Math.min(a, 10 - i);
                System.arraycopy(op2Var.d(), op2Var.e(), this.a.d(), this.f, min);
                if (this.f + min == 10) {
                    this.a.P(0);
                    if (73 == this.a.D() && 68 == this.a.D() && 51 == this.a.D()) {
                        this.a.Q(3);
                        this.e = this.a.C() + 10;
                    } else {
                        p12.i("Id3Reader", "Discarding invalid ID3 tag");
                        this.c = false;
                        return;
                    }
                }
            }
            int min2 = Math.min(a, this.e - this.f);
            this.b.a(op2Var, min2);
            this.f += min2;
        }
    }

    @Override // defpackage.ku0
    public void c() {
        this.c = false;
        this.d = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
        int i;
        ii.i(this.b);
        if (this.c && (i = this.e) != 0 && this.f == i) {
            long j = this.d;
            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                this.b.b(j, 1, i, 0, null);
            }
            this.c = false;
        }
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if ((i & 4) == 0) {
            return;
        }
        this.c = true;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.d = j;
        }
        this.e = 0;
        this.f = 0;
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        f84 f = r11Var.f(dVar.c(), 5);
        this.b = f;
        f.f(new j.b().S(dVar.b()).e0("application/id3").E());
    }
}
