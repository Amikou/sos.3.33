package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.p;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: vz5  reason: default package */
/* loaded from: classes.dex */
public final class vz5 implements cm5 {
    public final p a;
    public final /* synthetic */ AppMeasurementDynamiteService b;

    public vz5(AppMeasurementDynamiteService appMeasurementDynamiteService, p pVar) {
        this.b = appMeasurementDynamiteService;
        this.a = pVar;
    }

    @Override // defpackage.cm5
    public final void a(String str, String str2, Bundle bundle, long j) {
        try {
            this.a.y(str, str2, bundle, j);
        } catch (RemoteException e) {
            ck5 ck5Var = this.b.a;
            if (ck5Var != null) {
                ck5Var.w().p().b("Event interceptor threw exception", e);
            }
        }
    }
}
