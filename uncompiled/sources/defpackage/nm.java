package defpackage;

import android.animation.Animator;
import android.animation.ValueAnimator;
import defpackage.xg4;

/* compiled from: BaseAnimation.java */
/* renamed from: nm  reason: default package */
/* loaded from: classes2.dex */
public abstract class nm<T extends Animator> {
    public xg4.a b;
    public long a = 350;
    public T c = a();

    public nm(xg4.a aVar) {
        this.b = aVar;
    }

    public abstract T a();

    public nm b(long j) {
        this.a = j;
        T t = this.c;
        if (t instanceof ValueAnimator) {
            t.setDuration(j);
        }
        return this;
    }

    public void c() {
        T t = this.c;
        if (t == null || !t.isStarted()) {
            return;
        }
        this.c.end();
    }

    public abstract nm d(float f);

    public void e() {
        T t = this.c;
        if (t == null || t.isRunning()) {
            return;
        }
        this.c.start();
    }
}
