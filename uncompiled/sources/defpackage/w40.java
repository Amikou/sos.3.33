package defpackage;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/* compiled from: ConfigOverride.java */
/* renamed from: w40  reason: default package */
/* loaded from: classes.dex */
public abstract class w40 {
    public JsonFormat.Value _format;
    public JsonIgnoreProperties.Value _ignorals;
    public JsonInclude.Value _include;
    public Boolean _isIgnoredType;

    public w40() {
    }

    public JsonFormat.Value getFormat() {
        return this._format;
    }

    public JsonIgnoreProperties.Value getIgnorals() {
        return this._ignorals;
    }

    public JsonInclude.Value getInclude() {
        return this._include;
    }

    public Boolean getIsIgnoredType() {
        return this._isIgnoredType;
    }

    public w40(w40 w40Var) {
        this._format = w40Var._format;
        this._include = w40Var._include;
        this._ignorals = w40Var._ignorals;
        this._isIgnoredType = w40Var._isIgnoredType;
    }
}
