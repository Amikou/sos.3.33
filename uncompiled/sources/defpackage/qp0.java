package defpackage;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutinesInternalError;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: DispatchedTask.kt */
/* renamed from: qp0  reason: default package */
/* loaded from: classes2.dex */
public abstract class qp0<T> extends k34 {
    public int g0;

    public qp0(int i) {
        this.g0 = i;
    }

    public void a(Object obj, Throwable th) {
    }

    public abstract q70<T> b();

    public Throwable d(Object obj) {
        t30 t30Var = obj instanceof t30 ? (t30) obj : null;
        if (t30Var == null) {
            return null;
        }
        return t30Var.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public <T> T e(Object obj) {
        return obj;
    }

    public final void g(Throwable th, Throwable th2) {
        if (th == null && th2 == null) {
            return;
        }
        if (th != null && th2 != null) {
            py0.a(th, th2);
        }
        if (th == null) {
            th = th2;
        }
        fs1.d(th);
        z80.a(b().getContext(), new CoroutinesInternalError("Fatal exception in coroutines machinery for " + this + ". Please read KDoc to 'handleFatalException' method and report this incident to maintainers", th));
    }

    public abstract Object h();

    @Override // java.lang.Runnable
    public final void run() {
        Object m52constructorimpl;
        Object m52constructorimpl2;
        if (ze0.a()) {
            if (!(this.g0 != -1)) {
                throw new AssertionError();
            }
        }
        p34 p34Var = this.f0;
        try {
            np0 np0Var = (np0) b();
            q70<T> q70Var = np0Var.i0;
            Object obj = np0Var.k0;
            CoroutineContext context = q70Var.getContext();
            Object c = ThreadContextKt.c(context, obj);
            qe4<?> e = c != ThreadContextKt.a ? x80.e(q70Var, context, c) : null;
            CoroutineContext context2 = q70Var.getContext();
            Object h = h();
            Throwable d = d(h);
            st1 st1Var = (d == null && rp0.b(this.g0)) ? (st1) context2.get(st1.f) : null;
            if (st1Var != null && !st1Var.b()) {
                Throwable g = st1Var.g();
                a(h, g);
                Result.a aVar = Result.Companion;
                if (ze0.d() && (q70Var instanceof e90)) {
                    g = hs3.j(g, (e90) q70Var);
                }
                q70Var.resumeWith(Result.m52constructorimpl(o83.a(g)));
            } else if (d != null) {
                Result.a aVar2 = Result.Companion;
                q70Var.resumeWith(Result.m52constructorimpl(o83.a(d)));
            } else {
                T e2 = e(h);
                Result.a aVar3 = Result.Companion;
                q70Var.resumeWith(Result.m52constructorimpl(e2));
            }
            te4 te4Var = te4.a;
            if (e == null || e.M0()) {
                ThreadContextKt.a(context, c);
            }
            try {
                Result.a aVar4 = Result.Companion;
                p34Var.c();
                m52constructorimpl2 = Result.m52constructorimpl(te4Var);
            } catch (Throwable th) {
                Result.a aVar5 = Result.Companion;
                m52constructorimpl2 = Result.m52constructorimpl(o83.a(th));
            }
            g(null, Result.m55exceptionOrNullimpl(m52constructorimpl2));
        } catch (Throwable th2) {
            try {
                Result.a aVar6 = Result.Companion;
                p34Var.c();
                m52constructorimpl = Result.m52constructorimpl(te4.a);
            } catch (Throwable th3) {
                Result.a aVar7 = Result.Companion;
                m52constructorimpl = Result.m52constructorimpl(o83.a(th3));
            }
            g(th2, Result.m55exceptionOrNullimpl(m52constructorimpl));
        }
    }
}
