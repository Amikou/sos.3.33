package defpackage;

import android.util.Pair;
import androidx.media3.exoplayer.drm.DrmSession;
import java.util.Map;
import zendesk.support.request.CellBase;

/* compiled from: WidevineUtil.java */
/* renamed from: ap4  reason: default package */
/* loaded from: classes.dex */
public final class ap4 {
    public static long a(Map<String, String> map, String str) {
        if (map != null) {
            try {
                String str2 = map.get(str);
                return str2 != null ? Long.parseLong(str2) : CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            } catch (NumberFormatException unused) {
                return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            }
        }
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static Pair<Long, Long> b(DrmSession drmSession) {
        Map<String, String> d = drmSession.d();
        if (d == null) {
            return null;
        }
        return new Pair<>(Long.valueOf(a(d, "LicenseDurationRemaining")), Long.valueOf(a(d, "PlaybackDurationRemaining")));
    }
}
