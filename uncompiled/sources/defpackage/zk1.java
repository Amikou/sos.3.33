package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: HolderTokenListBinding.java */
/* renamed from: zk1  reason: default package */
/* loaded from: classes2.dex */
public final class zk1 {
    public final ConstraintLayout a;
    public final ImageView b;
    public final TextView c;
    public final TextView d;
    public final View e;

    public zk1(ConstraintLayout constraintLayout, MaterialButton materialButton, ImageView imageView, ImageView imageView2, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = imageView2;
        this.c = textView2;
        this.d = textView5;
        this.e = view;
    }

    public static zk1 a(View view) {
        int i = R.id.btnDelete;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDelete);
        if (materialButton != null) {
            i = R.id.ivPercentageDirection;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivPercentageDirection);
            if (imageView != null) {
                i = R.id.ivTokenIcon;
                ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivTokenIcon);
                if (imageView2 != null) {
                    i = R.id.rowBG;
                    ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.rowBG);
                    if (constraintLayout != null) {
                        i = R.id.rowFG;
                        ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.rowFG);
                        if (constraintLayout2 != null) {
                            i = R.id.tvTokenBalance;
                            TextView textView = (TextView) ai4.a(view, R.id.tvTokenBalance);
                            if (textView != null) {
                                i = R.id.tvTokenName;
                                TextView textView2 = (TextView) ai4.a(view, R.id.tvTokenName);
                                if (textView2 != null) {
                                    i = R.id.tvTokenNativeBalance;
                                    TextView textView3 = (TextView) ai4.a(view, R.id.tvTokenNativeBalance);
                                    if (textView3 != null) {
                                        i = R.id.tvTokenPercent;
                                        TextView textView4 = (TextView) ai4.a(view, R.id.tvTokenPercent);
                                        if (textView4 != null) {
                                            i = R.id.tvTokenSymbol;
                                            TextView textView5 = (TextView) ai4.a(view, R.id.tvTokenSymbol);
                                            if (textView5 != null) {
                                                i = R.id.vDivider;
                                                View a = ai4.a(view, R.id.vDivider);
                                                if (a != null) {
                                                    i = R.id.vTokenNameContainer;
                                                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vTokenNameContainer);
                                                    if (linearLayout != null) {
                                                        return new zk1((ConstraintLayout) view, materialButton, imageView, imageView2, constraintLayout, constraintLayout2, textView, textView2, textView3, textView4, textView5, a, linearLayout);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
