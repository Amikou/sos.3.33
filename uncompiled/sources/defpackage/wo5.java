package defpackage;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.google.android.gms.internal.measurement.zzhz;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: wo5  reason: default package */
/* loaded from: classes.dex */
public abstract class wo5<T> {
    public static final Object g = new Object();
    public static volatile uo5 h;
    public static final AtomicInteger i;
    public static final /* synthetic */ int j = 0;
    public final ro5 a;
    public final String b;
    public final T c;
    public volatile int d = -1;
    public volatile T e;
    public final boolean f;

    static {
        new AtomicReference();
        new cp5(fo5.a);
        i = new AtomicInteger();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ wo5(ro5 ro5Var, String str, Object obj, boolean z, io5 io5Var) {
        if (ro5Var.b != null) {
            this.a = ro5Var;
            this.b = str;
            this.c = obj;
            this.f = true;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @Deprecated
    public static void b(final Context context) {
        synchronized (g) {
            uo5 uo5Var = h;
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (uo5Var == null || uo5Var.a() != context) {
                bn5.e();
                zo5.c();
                sn5.d();
                h = new jm5(context, gq5.a(new yp5(context) { // from class: do5
                    public final Context a;

                    {
                        this.a = context;
                    }

                    @Override // defpackage.yp5
                    public final Object zza() {
                        zzhz zzc;
                        zzhz zzc2;
                        Context context2 = this.a;
                        int i2 = wo5.j;
                        String str = Build.TYPE;
                        String str2 = Build.TAGS;
                        if ((!str.equals("eng") && !str.equals("userdebug")) || (!str2.contains("dev-keys") && !str2.contains("test-keys"))) {
                            return zzhz.zzc();
                        }
                        if (hm5.a() && !context2.isDeviceProtectedStorage()) {
                            context2 = context2.createDeviceProtectedStorageContext();
                        }
                        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                        try {
                            StrictMode.allowThreadDiskWrites();
                            try {
                                File file = new File(context2.getDir("phenotype_hermetic", 0), "overrides.txt");
                                zzc = file.exists() ? zzhz.zzd(file) : zzhz.zzc();
                            } catch (RuntimeException unused) {
                                zzc = zzhz.zzc();
                            }
                            if (zzc.zza()) {
                                File file2 = (File) zzc.zzb();
                                try {
                                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file2)));
                                    try {
                                        HashMap hashMap = new HashMap();
                                        HashMap hashMap2 = new HashMap();
                                        while (true) {
                                            String readLine = bufferedReader.readLine();
                                            if (readLine == null) {
                                                break;
                                            }
                                            String[] split = readLine.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, 3);
                                            if (split.length != 3) {
                                                if (readLine.length() != 0) {
                                                    "Invalid: ".concat(readLine);
                                                }
                                            } else {
                                                String str3 = new String(split[0]);
                                                String decode = Uri.decode(new String(split[1]));
                                                String str4 = (String) hashMap2.get(split[2]);
                                                if (str4 == null) {
                                                    String str5 = new String(split[2]);
                                                    str4 = Uri.decode(str5);
                                                    if (str4.length() < 1024 || str4 == str5) {
                                                        hashMap2.put(str5, str4);
                                                    }
                                                }
                                                if (!hashMap.containsKey(str3)) {
                                                    hashMap.put(str3, new HashMap());
                                                }
                                                ((Map) hashMap.get(str3)).put(decode, str4);
                                            }
                                        }
                                        String valueOf = String.valueOf(file2);
                                        StringBuilder sb = new StringBuilder(valueOf.length() + 7);
                                        sb.append("Parsed ");
                                        sb.append(valueOf);
                                        vn5 vn5Var = new vn5(hashMap);
                                        bufferedReader.close();
                                        zzc2 = zzhz.zzd(vn5Var);
                                    } catch (Throwable th) {
                                        try {
                                            bufferedReader.close();
                                        } catch (Throwable th2) {
                                            wq5.a(th, th2);
                                        }
                                        throw th;
                                    }
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            } else {
                                zzc2 = zzhz.zzc();
                            }
                            return zzc2;
                        } finally {
                            StrictMode.setThreadPolicy(allowThreadDiskReads);
                        }
                    }
                }));
                i.incrementAndGet();
            }
        }
    }

    public static void c() {
        i.incrementAndGet();
    }

    public abstract T a(Object obj);

    public final String d() {
        String str = this.a.d;
        return this.b;
    }

    /* JADX WARN: Removed duplicated region for block: B:34:0x00a8  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00a9 A[Catch: all -> 0x0103, TryCatch #0 {, blocks: (B:8:0x0016, B:10:0x001a, B:12:0x0020, B:14:0x0035, B:16:0x0041, B:18:0x004a, B:20:0x005a, B:41:0x00cb, B:43:0x00db, B:45:0x00ef, B:46:0x00f2, B:47:0x00f6, B:35:0x00a9, B:37:0x00c1, B:40:0x00c9, B:22:0x005f, B:24:0x0065, B:26:0x0073, B:30:0x0098, B:32:0x00a2, B:28:0x008a, B:48:0x00fb, B:49:0x0100, B:50:0x0101), top: B:57:0x0016 }] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00db A[Catch: all -> 0x0103, TryCatch #0 {, blocks: (B:8:0x0016, B:10:0x001a, B:12:0x0020, B:14:0x0035, B:16:0x0041, B:18:0x004a, B:20:0x005a, B:41:0x00cb, B:43:0x00db, B:45:0x00ef, B:46:0x00f2, B:47:0x00f6, B:35:0x00a9, B:37:0x00c1, B:40:0x00c9, B:22:0x005f, B:24:0x0065, B:26:0x0073, B:30:0x0098, B:32:0x00a2, B:28:0x008a, B:48:0x00fb, B:49:0x0100, B:50:0x0101), top: B:57:0x0016 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final T e() {
        /*
            Method dump skipped, instructions count: 265
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.wo5.e():java.lang.Object");
    }
}
