package defpackage;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: TargetTracker.java */
/* renamed from: j34  reason: default package */
/* loaded from: classes.dex */
public final class j34 implements pz1 {
    public final Set<i34<?>> a = Collections.newSetFromMap(new WeakHashMap());

    @Override // defpackage.pz1
    public void b() {
        for (i34 i34Var : mg4.j(this.a)) {
            i34Var.b();
        }
    }

    public void d() {
        this.a.clear();
    }

    @Override // defpackage.pz1
    public void e() {
        for (i34 i34Var : mg4.j(this.a)) {
            i34Var.e();
        }
    }

    @Override // defpackage.pz1
    public void h() {
        for (i34 i34Var : mg4.j(this.a)) {
            i34Var.h();
        }
    }

    public List<i34<?>> i() {
        return mg4.j(this.a);
    }

    public void n(i34<?> i34Var) {
        this.a.add(i34Var);
    }

    public void o(i34<?> i34Var) {
        this.a.remove(i34Var);
    }
}
