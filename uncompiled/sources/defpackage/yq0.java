package defpackage;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/* compiled from: DrawableResource.java */
/* renamed from: yq0  reason: default package */
/* loaded from: classes.dex */
public abstract class yq0<T extends Drawable> implements s73<T>, oq1 {
    public final T a;

    public yq0(T t) {
        this.a = (T) wt2.d(t);
    }

    public void c() {
        T t = this.a;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof uf1) {
            ((uf1) t).e().prepareToDraw();
        }
    }

    @Override // defpackage.s73
    /* renamed from: e */
    public final T get() {
        Drawable.ConstantState constantState = this.a.getConstantState();
        if (constantState == null) {
            return this.a;
        }
        return (T) constantState.newDrawable();
    }
}
