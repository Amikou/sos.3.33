package defpackage;

import android.os.Bundle;
import androidx.navigation.h;

/* compiled from: NavAction.java */
/* renamed from: wd2  reason: default package */
/* loaded from: classes.dex */
public final class wd2 {
    public final int a;
    public h b;
    public Bundle c;

    public wd2(int i) {
        this(i, null);
    }

    public Bundle a() {
        return this.c;
    }

    public int b() {
        return this.a;
    }

    public h c() {
        return this.b;
    }

    public void d(Bundle bundle) {
        this.c = bundle;
    }

    public void e(h hVar) {
        this.b = hVar;
    }

    public wd2(int i, h hVar) {
        this(i, hVar, null);
    }

    public wd2(int i, h hVar, Bundle bundle) {
        this.a = i;
        this.b = hVar;
        this.c = bundle;
    }
}
