package defpackage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Property;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DrawableWithAnimatedVisibilityChange.java */
/* renamed from: er0  reason: default package */
/* loaded from: classes2.dex */
public abstract class er0 extends Drawable implements Animatable {
    public static final Property<er0, Float> s0 = new c(Float.class, "growFraction");
    public final Context a;
    public final wn f0;
    public ValueAnimator h0;
    public ValueAnimator i0;
    public boolean j0;
    public boolean k0;
    public float l0;
    public List<hd> m0;
    public hd n0;
    public boolean o0;
    public float p0;
    public int r0;
    public final Paint q0 = new Paint();
    public pe g0 = new pe();

    /* compiled from: DrawableWithAnimatedVisibilityChange.java */
    /* renamed from: er0$a */
    /* loaded from: classes2.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            super.onAnimationStart(animator);
            er0.this.e();
        }
    }

    /* compiled from: DrawableWithAnimatedVisibilityChange.java */
    /* renamed from: er0$b */
    /* loaded from: classes2.dex */
    public class b extends AnimatorListenerAdapter {
        public b() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            er0.super.setVisible(false, false);
            er0.this.d();
        }
    }

    /* compiled from: DrawableWithAnimatedVisibilityChange.java */
    /* renamed from: er0$c */
    /* loaded from: classes2.dex */
    public static class c extends Property<er0, Float> {
        public c(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(er0 er0Var) {
            return Float.valueOf(er0Var.g());
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(er0 er0Var, Float f) {
            er0Var.m(f.floatValue());
        }
    }

    public er0(Context context, wn wnVar) {
        this.a = context;
        this.f0 = wnVar;
        setAlpha(255);
    }

    public final void d() {
        hd hdVar = this.n0;
        if (hdVar != null) {
            hdVar.onAnimationEnd(this);
        }
        List<hd> list = this.m0;
        if (list == null || this.o0) {
            return;
        }
        for (hd hdVar2 : list) {
            hdVar2.onAnimationEnd(this);
        }
    }

    public final void e() {
        hd hdVar = this.n0;
        if (hdVar != null) {
            hdVar.onAnimationStart(this);
        }
        List<hd> list = this.m0;
        if (list == null || this.o0) {
            return;
        }
        for (hd hdVar2 : list) {
            hdVar2.onAnimationStart(this);
        }
    }

    public final void f(ValueAnimator... valueAnimatorArr) {
        boolean z = this.o0;
        this.o0 = true;
        for (ValueAnimator valueAnimator : valueAnimatorArr) {
            valueAnimator.end();
        }
        this.o0 = z;
    }

    public float g() {
        if (this.f0.b() || this.f0.a()) {
            if (!this.k0 && !this.j0) {
                return this.p0;
            }
            return this.l0;
        }
        return 1.0f;
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.r0;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public boolean h() {
        return p(false, false, false);
    }

    public boolean i() {
        ValueAnimator valueAnimator = this.i0;
        return (valueAnimator != null && valueAnimator.isRunning()) || this.k0;
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return j() || i();
    }

    public boolean j() {
        ValueAnimator valueAnimator = this.h0;
        return (valueAnimator != null && valueAnimator.isRunning()) || this.j0;
    }

    public final void k() {
        if (this.h0 == null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, s0, Utils.FLOAT_EPSILON, 1.0f);
            this.h0 = ofFloat;
            ofFloat.setDuration(500L);
            this.h0.setInterpolator(ne.b);
            o(this.h0);
        }
        if (this.i0 == null) {
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this, s0, 1.0f, Utils.FLOAT_EPSILON);
            this.i0 = ofFloat2;
            ofFloat2.setDuration(500L);
            this.i0.setInterpolator(ne.b);
            n(this.i0);
        }
    }

    public void l(hd hdVar) {
        if (this.m0 == null) {
            this.m0 = new ArrayList();
        }
        if (this.m0.contains(hdVar)) {
            return;
        }
        this.m0.add(hdVar);
    }

    public void m(float f) {
        if (this.p0 != f) {
            this.p0 = f;
            invalidateSelf();
        }
    }

    public final void n(ValueAnimator valueAnimator) {
        ValueAnimator valueAnimator2 = this.i0;
        if (valueAnimator2 != null && valueAnimator2.isRunning()) {
            throw new IllegalArgumentException("Cannot set hideAnimator while the current hideAnimator is running.");
        }
        this.i0 = valueAnimator;
        valueAnimator.addListener(new b());
    }

    public final void o(ValueAnimator valueAnimator) {
        ValueAnimator valueAnimator2 = this.h0;
        if (valueAnimator2 != null && valueAnimator2.isRunning()) {
            throw new IllegalArgumentException("Cannot set showAnimator while the current showAnimator is running.");
        }
        this.h0 = valueAnimator;
        valueAnimator.addListener(new a());
    }

    public boolean p(boolean z, boolean z2, boolean z3) {
        return q(z, z2, z3 && this.g0.a(this.a.getContentResolver()) > Utils.FLOAT_EPSILON);
    }

    public boolean q(boolean z, boolean z2, boolean z3) {
        k();
        if (isVisible() || z) {
            ValueAnimator valueAnimator = z ? this.h0 : this.i0;
            if (!z3) {
                if (valueAnimator.isRunning()) {
                    valueAnimator.end();
                } else {
                    f(valueAnimator);
                }
                return super.setVisible(z, false);
            } else if (z3 && valueAnimator.isRunning()) {
                return false;
            } else {
                boolean z4 = !z || super.setVisible(z, false);
                if (!(z ? this.f0.b() : this.f0.a())) {
                    f(valueAnimator);
                    return z4;
                }
                if (!z2 && Build.VERSION.SDK_INT >= 19 && valueAnimator.isPaused()) {
                    valueAnimator.resume();
                } else {
                    valueAnimator.start();
                }
                return z4;
            }
        }
        return false;
    }

    public boolean r(hd hdVar) {
        List<hd> list = this.m0;
        if (list == null || !list.contains(hdVar)) {
            return false;
        }
        this.m0.remove(hdVar);
        if (this.m0.isEmpty()) {
            this.m0 = null;
            return true;
        }
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.r0 = i;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.q0.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        return p(z, z2, true);
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        q(true, true, false);
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        q(false, true, false);
    }
}
