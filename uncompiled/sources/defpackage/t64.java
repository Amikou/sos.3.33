package defpackage;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.e;
import retrofit2.o;

/* compiled from: ToStringConverterFactory.kt */
/* renamed from: t64  reason: default package */
/* loaded from: classes2.dex */
public class t64 extends e.a {
    public static final MediaType a;

    /* compiled from: ToStringConverterFactory.kt */
    /* renamed from: t64$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        a = MediaType.Companion.parse("text/plain");
    }

    public static final RequestBody h(String str) {
        RequestBody.Companion companion = RequestBody.Companion;
        fs1.e(str, "value");
        return companion.create(str, a);
    }

    public static final String i(ResponseBody responseBody) {
        return responseBody.string();
    }

    @Override // retrofit2.e.a
    public e<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, o oVar) {
        fs1.f(type, "type");
        fs1.f(annotationArr, "parameterAnnotations");
        fs1.f(annotationArr2, "methodAnnotations");
        fs1.f(oVar, "retrofit");
        if (fs1.b(String.class, type)) {
            return r64.a;
        }
        return null;
    }

    @Override // retrofit2.e.a
    public e<ResponseBody, ?> d(Type type, Annotation[] annotationArr, o oVar) {
        fs1.f(type, "type");
        fs1.f(annotationArr, "annotations");
        fs1.f(oVar, "retrofit");
        if (fs1.b(String.class, type)) {
            return s64.a;
        }
        return null;
    }
}
