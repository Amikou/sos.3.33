package defpackage;

/* compiled from: EventStoreModule_SchemaVersionFactory.java */
/* renamed from: hy0  reason: default package */
/* loaded from: classes.dex */
public final class hy0 implements z11<Integer> {

    /* compiled from: EventStoreModule_SchemaVersionFactory.java */
    /* renamed from: hy0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final hy0 a = new hy0();
    }

    public static hy0 a() {
        return a.a;
    }

    public static int c() {
        return fy0.b();
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public Integer get() {
        return Integer.valueOf(c());
    }
}
