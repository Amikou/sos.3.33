package defpackage;

/* compiled from: KotlinVersion.kt */
/* renamed from: xx1  reason: default package */
/* loaded from: classes2.dex */
public final class xx1 implements Comparable<xx1> {
    public static final xx1 i0;
    public final int a;
    public final int f0;
    public final int g0;
    public final int h0;

    /* compiled from: KotlinVersion.kt */
    /* renamed from: xx1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        i0 = yx1.a();
    }

    public xx1(int i, int i2, int i3) {
        this.f0 = i;
        this.g0 = i2;
        this.h0 = i3;
        this.a = d(i, i2, i3);
    }

    @Override // java.lang.Comparable
    /* renamed from: a */
    public int compareTo(xx1 xx1Var) {
        fs1.f(xx1Var, "other");
        return this.a - xx1Var.a;
    }

    public final int d(int i, int i2, int i3) {
        if (i >= 0 && 255 >= i && i2 >= 0 && 255 >= i2 && i3 >= 0 && 255 >= i3) {
            return (i << 16) + (i2 << 8) + i3;
        }
        throw new IllegalArgumentException(("Version components are out of range: " + i + '.' + i2 + '.' + i3).toString());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof xx1)) {
            obj = null;
        }
        xx1 xx1Var = (xx1) obj;
        return xx1Var != null && this.a == xx1Var.a;
    }

    public int hashCode() {
        return this.a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f0);
        sb.append('.');
        sb.append(this.g0);
        sb.append('.');
        sb.append(this.h0);
        return sb.toString();
    }
}
