package defpackage;

import kotlin.Result;

/* compiled from: CompletionState.kt */
/* renamed from: w30 */
/* loaded from: classes2.dex */
public final class w30 {
    public static final <T> Object a(Object obj, q70<? super T> q70Var) {
        if (obj instanceof t30) {
            Result.a aVar = Result.Companion;
            Throwable th = ((t30) obj).a;
            if (ze0.d() && (q70Var instanceof e90)) {
                th = hs3.j(th, (e90) q70Var);
            }
            return Result.m52constructorimpl(o83.a(th));
        }
        Result.a aVar2 = Result.Companion;
        return Result.m52constructorimpl(obj);
    }

    public static final <T> Object b(Object obj, ov<?> ovVar) {
        Throwable m55exceptionOrNullimpl = Result.m55exceptionOrNullimpl(obj);
        if (m55exceptionOrNullimpl != null) {
            if (ze0.d() && (ovVar instanceof e90)) {
                m55exceptionOrNullimpl = hs3.j(m55exceptionOrNullimpl, (e90) ovVar);
            }
            obj = new t30(m55exceptionOrNullimpl, false, 2, null);
        }
        return obj;
    }

    public static final <T> Object c(Object obj, tc1<? super Throwable, te4> tc1Var) {
        Throwable m55exceptionOrNullimpl = Result.m55exceptionOrNullimpl(obj);
        if (m55exceptionOrNullimpl == null) {
            return tc1Var != null ? new u30(obj, tc1Var) : obj;
        }
        return new t30(m55exceptionOrNullimpl, false, 2, null);
    }

    public static /* synthetic */ Object d(Object obj, tc1 tc1Var, int i, Object obj2) {
        if ((i & 1) != 0) {
            tc1Var = null;
        }
        return c(obj, tc1Var);
    }
}
