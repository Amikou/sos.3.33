package defpackage;

/* compiled from: Retries.java */
/* renamed from: u83  reason: default package */
/* loaded from: classes.dex */
public final class u83 {
    public static <TInput, TResult, TException extends Throwable> TResult a(int i, TInput tinput, dd1<TInput, TResult, TException> dd1Var, y83<TInput, TResult> y83Var) throws Throwable {
        TResult apply;
        if (i < 1) {
            return dd1Var.apply(tinput);
        }
        do {
            apply = dd1Var.apply(tinput);
            tinput = y83Var.a(tinput, apply);
            if (tinput == null) {
                break;
            }
            i--;
        } while (i >= 1);
        return apply;
    }
}
