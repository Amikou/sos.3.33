package defpackage;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.media3.exoplayer.audio.AudioSink;
import androidx.media3.exoplayer.audio.DefaultAudioSink;
import androidx.media3.exoplayer.audio.a;
import androidx.media3.exoplayer.m;
import androidx.media3.exoplayer.mediacodec.c;
import androidx.media3.exoplayer.mediacodec.d;
import androidx.media3.exoplayer.mediacodec.f;
import androidx.media3.exoplayer.video.b;
import java.util.ArrayList;

/* compiled from: DefaultRenderersFactory.java */
/* renamed from: kk0  reason: default package */
/* loaded from: classes.dex */
public class kk0 implements w63 {
    public final Context a;
    public boolean e;
    public boolean g;
    public boolean h;
    public boolean i;
    public final c b = new c();
    public int c = 0;
    public long d = 5000;
    public f f = f.a;

    public kk0(Context context) {
        this.a = context;
    }

    @Override // defpackage.w63
    public m[] a(Handler handler, b bVar, a aVar, n44 n44Var, o82 o82Var) {
        ArrayList<m> arrayList = new ArrayList<>();
        h(this.a, this.c, this.f, this.e, handler, bVar, this.d, arrayList);
        AudioSink c = c(this.a, this.g, this.h, this.i);
        if (c != null) {
            b(this.a, this.c, this.f, this.e, c, handler, aVar, arrayList);
        }
        g(this.a, n44Var, handler.getLooper(), this.c, arrayList);
        e(this.a, o82Var, handler.getLooper(), this.c, arrayList);
        d(this.a, this.c, arrayList);
        f(this.a, handler, this.c, arrayList);
        return (m[]) arrayList.toArray(new m[0]);
    }

    /* JADX WARN: Can't wrap try/catch for region: R(13:(5:9|10|11|12|13)|14|15|16|17|(2:18|19)|21|22|23|24|25|27|28) */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x00c2, code lost:
        r6 = r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00cd, code lost:
        r5 = r6;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void b(android.content.Context r15, int r16, androidx.media3.exoplayer.mediacodec.f r17, boolean r18, androidx.media3.exoplayer.audio.AudioSink r19, android.os.Handler r20, androidx.media3.exoplayer.audio.a r21, java.util.ArrayList<androidx.media3.exoplayer.m> r22) {
        /*
            Method dump skipped, instructions count: 259
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.kk0.b(android.content.Context, int, androidx.media3.exoplayer.mediacodec.f, boolean, androidx.media3.exoplayer.audio.AudioSink, android.os.Handler, androidx.media3.exoplayer.audio.a, java.util.ArrayList):void");
    }

    public AudioSink c(Context context, boolean z, boolean z2, boolean z3) {
        return new DefaultAudioSink.e().g(fj.c(context)).i(z).h(z2).j(z3 ? 1 : 0).f();
    }

    public void d(Context context, int i, ArrayList<m> arrayList) {
        arrayList.add(new androidx.media3.exoplayer.video.spherical.a());
    }

    public void e(Context context, o82 o82Var, Looper looper, int i, ArrayList<m> arrayList) {
        arrayList.add(new p82(o82Var, looper));
    }

    public void f(Context context, Handler handler, int i, ArrayList<m> arrayList) {
    }

    public void g(Context context, n44 n44Var, Looper looper, int i, ArrayList<m> arrayList) {
        arrayList.add(new o44(n44Var, looper));
    }

    public void h(Context context, int i, f fVar, boolean z, Handler handler, b bVar, long j, ArrayList<m> arrayList) {
        int i2;
        arrayList.add(new androidx.media3.exoplayer.video.a(context, i(), fVar, j, z, handler, bVar, 50));
        if (i == 0) {
            return;
        }
        int size = arrayList.size();
        if (i == 2) {
            size--;
        }
        try {
            try {
                i2 = size + 1;
            } catch (ClassNotFoundException unused) {
            }
            try {
                try {
                    arrayList.add(size, (m) Class.forName("androidx.media3.decoder.vp9.LibvpxVideoRenderer").getConstructor(Long.TYPE, Handler.class, b.class, Integer.TYPE).newInstance(Long.valueOf(j), handler, bVar, 50));
                    p12.f("DefaultRenderersFactory", "Loaded LibvpxVideoRenderer.");
                } catch (ClassNotFoundException unused2) {
                    size = i2;
                    i2 = size;
                    arrayList.add(i2, (m) Class.forName("androidx.media3.decoder.av1.Libgav1VideoRenderer").getConstructor(Long.TYPE, Handler.class, b.class, Integer.TYPE).newInstance(Long.valueOf(j), handler, bVar, 50));
                    p12.f("DefaultRenderersFactory", "Loaded Libgav1VideoRenderer.");
                }
                arrayList.add(i2, (m) Class.forName("androidx.media3.decoder.av1.Libgav1VideoRenderer").getConstructor(Long.TYPE, Handler.class, b.class, Integer.TYPE).newInstance(Long.valueOf(j), handler, bVar, 50));
                p12.f("DefaultRenderersFactory", "Loaded Libgav1VideoRenderer.");
            } catch (ClassNotFoundException unused3) {
            } catch (Exception e) {
                throw new RuntimeException("Error instantiating AV1 extension", e);
            }
        } catch (Exception e2) {
            throw new RuntimeException("Error instantiating VP9 extension", e2);
        }
    }

    public d.b i() {
        return this.b;
    }
}
