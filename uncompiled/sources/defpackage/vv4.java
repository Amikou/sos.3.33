package defpackage;

import java.util.List;

/* renamed from: vv4  reason: default package */
/* loaded from: classes2.dex */
public final class vv4 {
    public final String a;
    public final String b;
    public final long c;
    public final List<pv4> d;
    public final int e;
    public final int f;

    public vv4(String str, String str2, long j, List<pv4> list, int i, int i2) {
        this.a = str;
        this.b = str2;
        this.c = j;
        this.d = list;
        this.e = i;
        this.f = i2;
    }
}
