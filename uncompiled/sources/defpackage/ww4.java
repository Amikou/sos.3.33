package defpackage;

import android.content.Context;
import android.content.pm.PackageManager;

/* renamed from: ww4  reason: default package */
/* loaded from: classes2.dex */
public final class ww4 {
    public static final it4 c = new it4("PackageStateCache");
    public final Context a;
    public int b = -1;

    public ww4(Context context) {
        this.a = context;
    }

    public final synchronized int a() {
        if (this.b == -1) {
            try {
                this.b = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
                c.b("The current version of the app could not be retrieved", new Object[0]);
            }
        }
        return this.b;
    }
}
