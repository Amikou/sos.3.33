package defpackage;

import androidx.media3.common.util.b;
import defpackage.wi3;
import java.util.Arrays;

/* compiled from: ChunkIndex.java */
/* renamed from: py  reason: default package */
/* loaded from: classes.dex */
public final class py implements wi3 {
    public final int a;
    public final int[] b;
    public final long[] c;
    public final long[] d;
    public final long[] e;
    public final long f;

    public py(int[] iArr, long[] jArr, long[] jArr2, long[] jArr3) {
        this.b = iArr;
        this.c = jArr;
        this.d = jArr2;
        this.e = jArr3;
        int length = iArr.length;
        this.a = length;
        if (length > 0) {
            this.f = jArr2[length - 1] + jArr3[length - 1];
        } else {
            this.f = 0L;
        }
    }

    public int a(long j) {
        return b.i(this.e, j, true, true);
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        int a = a(j);
        yi3 yi3Var = new yi3(this.e[a], this.c[a]);
        if (yi3Var.a < j && a != this.a - 1) {
            int i = a + 1;
            return new wi3.a(yi3Var, new yi3(this.e[i], this.c[i]));
        }
        return new wi3.a(yi3Var);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.f;
    }

    public String toString() {
        return "ChunkIndex(length=" + this.a + ", sizes=" + Arrays.toString(this.b) + ", offsets=" + Arrays.toString(this.c) + ", timeUs=" + Arrays.toString(this.e) + ", durationsUs=" + Arrays.toString(this.d) + ")";
    }
}
