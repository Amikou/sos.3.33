package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.collectible.RoomCollection;

/* compiled from: CollectibleFragmentDirections.java */
/* renamed from: c10  reason: default package */
/* loaded from: classes2.dex */
public class c10 {

    /* compiled from: CollectibleFragmentDirections.java */
    /* renamed from: c10$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("collection")) {
                RoomCollection roomCollection = (RoomCollection) this.a.get("collection");
                if (!Parcelable.class.isAssignableFrom(RoomCollection.class) && roomCollection != null) {
                    if (Serializable.class.isAssignableFrom(RoomCollection.class)) {
                        bundle.putSerializable("collection", (Serializable) Serializable.class.cast(roomCollection));
                    } else {
                        throw new UnsupportedOperationException(RoomCollection.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("collection", (Parcelable) Parcelable.class.cast(roomCollection));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_navigation_collectibles_to_collectionFragment;
        }

        public RoomCollection c() {
            return (RoomCollection) this.a.get("collection");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("collection") != bVar.a.containsKey("collection")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionNavigationCollectiblesToCollectionFragment(actionId=" + b() + "){collection=" + c() + "}";
        }

        public b(RoomCollection roomCollection) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (roomCollection != null) {
                hashMap.put("collection", roomCollection);
                return;
            }
            throw new IllegalArgumentException("Argument \"collection\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(RoomCollection roomCollection) {
        return new b(roomCollection);
    }
}
