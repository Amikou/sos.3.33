package defpackage;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.t;
import com.fasterxml.jackson.databind.PropertyName;

/* compiled from: ObjectIdInfo.java */
/* renamed from: jl2  reason: default package */
/* loaded from: classes.dex */
public class jl2 {
    public static final jl2 f = new jl2(PropertyName.NO_NAME, Object.class, null, false, null);
    public final PropertyName a;
    public final Class<? extends ObjectIdGenerator<?>> b;
    public final Class<? extends t> c;
    public final Class<?> d;
    public final boolean e;

    public jl2(PropertyName propertyName, Class<?> cls, Class<? extends ObjectIdGenerator<?>> cls2, Class<? extends t> cls3) {
        this(propertyName, cls, cls2, false, cls3);
    }

    public static jl2 a() {
        return f;
    }

    public boolean b() {
        return this.e;
    }

    public Class<? extends ObjectIdGenerator<?>> c() {
        return this.b;
    }

    public PropertyName d() {
        return this.a;
    }

    public Class<? extends t> e() {
        return this.c;
    }

    public Class<?> f() {
        return this.d;
    }

    public jl2 g(boolean z) {
        return this.e == z ? this : new jl2(this.a, this.d, this.b, z, this.c);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ObjectIdInfo: propName=");
        sb.append(this.a);
        sb.append(", scope=");
        Class<?> cls = this.d;
        sb.append(cls == null ? "null" : cls.getName());
        sb.append(", generatorType=");
        Class<? extends ObjectIdGenerator<?>> cls2 = this.b;
        sb.append(cls2 != null ? cls2.getName() : "null");
        sb.append(", alwaysAsId=");
        sb.append(this.e);
        return sb.toString();
    }

    /* JADX WARN: Incorrect type for immutable var: ssa=java.lang.Class<? extends com.fasterxml.jackson.annotation.t>, code=java.lang.Class, for r5v0, types: [java.lang.Class<? extends com.fasterxml.jackson.annotation.t>] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public jl2(com.fasterxml.jackson.databind.PropertyName r1, java.lang.Class<?> r2, java.lang.Class<? extends com.fasterxml.jackson.annotation.ObjectIdGenerator<?>> r3, boolean r4, java.lang.Class r5) {
        /*
            r0 = this;
            r0.<init>()
            r0.a = r1
            r0.d = r2
            r0.b = r3
            r0.e = r4
            if (r5 != 0) goto Lf
            java.lang.Class<com.fasterxml.jackson.annotation.u> r5 = com.fasterxml.jackson.annotation.u.class
        Lf:
            r0.c = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.jl2.<init>(com.fasterxml.jackson.databind.PropertyName, java.lang.Class, java.lang.Class, boolean, java.lang.Class):void");
    }
}
