package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentWalletConnectDetailBinding.java */
/* renamed from: rb1  reason: default package */
/* loaded from: classes2.dex */
public final class rb1 {
    public final MaterialButton a;
    public final AppCompatImageView b;
    public final uk1 c;
    public final rp3 d;
    public final TextView e;
    public final TextView f;
    public final MaterialTextView g;
    public final TextView h;

    public rb1(ConstraintLayout constraintLayout, MaterialButton materialButton, CardView cardView, CardView cardView2, CardView cardView3, View view, View view2, View view3, View view4, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, uk1 uk1Var, rp3 rp3Var, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, MaterialTextView materialTextView, TextView textView7, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3) {
        this.a = materialButton;
        this.b = appCompatImageView2;
        this.c = uk1Var;
        this.d = rp3Var;
        this.e = textView;
        this.f = textView6;
        this.g = materialTextView;
        this.h = textView7;
    }

    public static rb1 a(View view) {
        int i = R.id.btnDisconnect;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDisconnect);
        if (materialButton != null) {
            i = R.id.cvConnected;
            CardView cardView = (CardView) ai4.a(view, R.id.cvConnected);
            if (cardView != null) {
                i = R.id.cvNetwork;
                CardView cardView2 = (CardView) ai4.a(view, R.id.cvNetwork);
                if (cardView2 != null) {
                    i = R.id.cvWallet;
                    CardView cardView3 = (CardView) ai4.a(view, R.id.cvWallet);
                    if (cardView3 != null) {
                        i = R.id.dividerHeaderConnected;
                        View a = ai4.a(view, R.id.dividerHeaderConnected);
                        if (a != null) {
                            i = R.id.dividerHeaderNetwork;
                            View a2 = ai4.a(view, R.id.dividerHeaderNetwork);
                            if (a2 != null) {
                                i = R.id.dividerHeaderWallet;
                                View a3 = ai4.a(view, R.id.dividerHeaderWallet);
                                if (a3 != null) {
                                    i = R.id.dividerHeaderWalletAddress;
                                    View a4 = ai4.a(view, R.id.dividerHeaderWalletAddress);
                                    if (a4 != null) {
                                        i = R.id.imgIconNote;
                                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgIconNote);
                                        if (appCompatImageView != null) {
                                            i = R.id.imgNetwork;
                                            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgNetwork);
                                            if (appCompatImageView2 != null) {
                                                i = R.id.include_dapp;
                                                View a5 = ai4.a(view, R.id.include_dapp);
                                                if (a5 != null) {
                                                    uk1 a6 = uk1.a(a5);
                                                    i = R.id.toolbar;
                                                    View a7 = ai4.a(view, R.id.toolbar);
                                                    if (a7 != null) {
                                                        rp3 a8 = rp3.a(a7);
                                                        i = R.id.txtConnectAt;
                                                        TextView textView = (TextView) ai4.a(view, R.id.txtConnectAt);
                                                        if (textView != null) {
                                                            i = R.id.txtHeaderConnected;
                                                            TextView textView2 = (TextView) ai4.a(view, R.id.txtHeaderConnected);
                                                            if (textView2 != null) {
                                                                i = R.id.txtHeaderNetwork;
                                                                TextView textView3 = (TextView) ai4.a(view, R.id.txtHeaderNetwork);
                                                                if (textView3 != null) {
                                                                    i = R.id.txtHeaderWallet;
                                                                    TextView textView4 = (TextView) ai4.a(view, R.id.txtHeaderWallet);
                                                                    if (textView4 != null) {
                                                                        i = R.id.txtHeaderWalletAddress;
                                                                        TextView textView5 = (TextView) ai4.a(view, R.id.txtHeaderWalletAddress);
                                                                        if (textView5 != null) {
                                                                            i = R.id.txtNetwork;
                                                                            TextView textView6 = (TextView) ai4.a(view, R.id.txtNetwork);
                                                                            if (textView6 != null) {
                                                                                i = R.id.txtWalletAddressName;
                                                                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtWalletAddressName);
                                                                                if (materialTextView != null) {
                                                                                    i = R.id.txtWalletName;
                                                                                    TextView textView7 = (TextView) ai4.a(view, R.id.txtWalletName);
                                                                                    if (textView7 != null) {
                                                                                        i = R.id.walletAddressTitleWrapper;
                                                                                        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.walletAddressTitleWrapper);
                                                                                        if (constraintLayout != null) {
                                                                                            i = R.id.walletTitleWrapper;
                                                                                            ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.walletTitleWrapper);
                                                                                            if (constraintLayout2 != null) {
                                                                                                return new rb1((ConstraintLayout) view, materialButton, cardView, cardView2, cardView3, a, a2, a3, a4, appCompatImageView, appCompatImageView2, a6, a8, textView, textView2, textView3, textView4, textView5, textView6, materialTextView, textView7, constraintLayout, constraintLayout2);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
