package defpackage;

import com.fasterxml.jackson.databind.a;
import com.fasterxml.jackson.databind.jsontype.b;
import com.fasterxml.jackson.databind.jsontype.c;

/* compiled from: TypeSerializerBase.java */
/* renamed from: wd4  reason: default package */
/* loaded from: classes.dex */
public abstract class wd4 extends c {
    public final b a;
    public final a b;

    public wd4(b bVar, a aVar) {
        this.a = bVar;
        this.b = aVar;
    }

    @Override // com.fasterxml.jackson.databind.jsontype.c
    public String b() {
        return null;
    }

    public void o(Object obj) {
    }

    public String p(Object obj) {
        String a = this.a.a(obj);
        if (a == null) {
            o(obj);
        }
        return a;
    }

    public String q(Object obj, Class<?> cls) {
        String e = this.a.e(obj, cls);
        if (e == null) {
            o(obj);
        }
        return e;
    }
}
