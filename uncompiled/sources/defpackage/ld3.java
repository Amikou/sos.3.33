package defpackage;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.d;
import com.fasterxml.jackson.databind.k;
import java.lang.reflect.Type;

/* compiled from: SchemaAware.java */
/* renamed from: ld3  reason: default package */
/* loaded from: classes.dex */
public interface ld3 {
    d getSchema(k kVar, Type type) throws JsonMappingException;

    d getSchema(k kVar, Type type, boolean z) throws JsonMappingException;
}
