package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.internal.ChildCancelledException;

/* compiled from: FlowCoroutine.kt */
/* renamed from: l71  reason: default package */
/* loaded from: classes2.dex */
public final class l71<T> extends vd3<T> {
    public l71(CoroutineContext coroutineContext, q70<? super T> q70Var) {
        super(coroutineContext, q70Var);
    }

    @Override // defpackage.bu1
    public boolean O(Throwable th) {
        if (th instanceof ChildCancelledException) {
            return true;
        }
        return J(th);
    }
}
