package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.internal.UndispatchedContextCollector;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: ChannelFlow.kt */
/* renamed from: ox */
/* loaded from: classes2.dex */
public final class ox {
    public static final <T, V> Object b(CoroutineContext coroutineContext, V v, Object obj, hd1<? super V, ? super q70<? super T>, ? extends Object> hd1Var, q70<? super T> q70Var) {
        Object c = ThreadContextKt.c(coroutineContext, obj);
        try {
            gs3 gs3Var = new gs3(q70Var, coroutineContext);
            if (hd1Var != null) {
                Object invoke = ((hd1) qd4.c(hd1Var, 2)).invoke(v, gs3Var);
                ThreadContextKt.a(coroutineContext, c);
                if (invoke == gs1.d()) {
                    ef0.c(q70Var);
                }
                return invoke;
            }
            throw new NullPointerException("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        } catch (Throwable th) {
            ThreadContextKt.a(coroutineContext, c);
            throw th;
        }
    }

    public static /* synthetic */ Object c(CoroutineContext coroutineContext, Object obj, Object obj2, hd1 hd1Var, q70 q70Var, int i, Object obj3) {
        if ((i & 4) != 0) {
            obj2 = ThreadContextKt.b(coroutineContext);
        }
        return b(coroutineContext, obj, obj2, hd1Var, q70Var);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> k71<T> d(k71<? super T> k71Var, CoroutineContext coroutineContext) {
        return k71Var instanceof qk3 ? true : k71Var instanceof xg2 ? k71Var : new UndispatchedContextCollector(k71Var, coroutineContext);
    }
}
