package defpackage;

/* compiled from: MiddleOutFallbackStrategy.java */
/* renamed from: t82  reason: default package */
/* loaded from: classes2.dex */
public class t82 implements is3 {
    public final int a;
    public final is3[] b;
    public final u82 c;

    public t82(int i, is3... is3VarArr) {
        this.a = i;
        this.b = is3VarArr;
        this.c = new u82(i);
    }

    @Override // defpackage.is3
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        is3[] is3VarArr;
        if (stackTraceElementArr.length <= this.a) {
            return stackTraceElementArr;
        }
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (is3 is3Var : this.b) {
            if (stackTraceElementArr2.length <= this.a) {
                break;
            }
            stackTraceElementArr2 = is3Var.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.a ? this.c.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
