package defpackage;

import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.a;

/* compiled from: JsonReadContext.java */
/* renamed from: hv1  reason: default package */
/* loaded from: classes.dex */
public final class hv1 extends cw1 {
    public final hv1 c;
    public ms0 d;
    public hv1 e;
    public String f;
    public Object g;
    public int h;
    public int i;

    public hv1(hv1 hv1Var, ms0 ms0Var, int i, int i2, int i3) {
        this.c = hv1Var;
        this.d = ms0Var;
        this.a = i;
        this.h = i2;
        this.i = i3;
        this.b = -1;
    }

    public static hv1 n(ms0 ms0Var) {
        return new hv1(null, ms0Var, 0, 1, 0);
    }

    @Override // defpackage.cw1
    public String b() {
        return this.f;
    }

    @Override // defpackage.cw1
    public void h(Object obj) {
        this.g = obj;
    }

    public final void j(ms0 ms0Var, String str) throws JsonProcessingException {
        if (ms0Var.c(str)) {
            Object b = ms0Var.b();
            JsonParser jsonParser = b instanceof JsonParser ? (JsonParser) b : null;
            throw new JsonParseException(jsonParser, "Duplicate field '" + str + "'");
        }
    }

    public hv1 k() {
        return this.c;
    }

    public hv1 l(int i, int i2) {
        hv1 hv1Var = this.e;
        if (hv1Var == null) {
            ms0 ms0Var = this.d;
            hv1Var = new hv1(this, ms0Var == null ? null : ms0Var.a(), 1, i, i2);
            this.e = hv1Var;
        } else {
            hv1Var.s(1, i, i2);
        }
        return hv1Var;
    }

    public hv1 m(int i, int i2) {
        hv1 hv1Var = this.e;
        if (hv1Var == null) {
            ms0 ms0Var = this.d;
            hv1 hv1Var2 = new hv1(this, ms0Var == null ? null : ms0Var.a(), 2, i, i2);
            this.e = hv1Var2;
            return hv1Var2;
        }
        hv1Var.s(2, i, i2);
        return hv1Var;
    }

    public boolean o() {
        int i = this.b + 1;
        this.b = i;
        return this.a != 0 && i > 0;
    }

    public ms0 p() {
        return this.d;
    }

    @Override // defpackage.cw1
    /* renamed from: q */
    public hv1 d() {
        return this.c;
    }

    public JsonLocation r(Object obj) {
        return new JsonLocation(obj, -1L, this.h, this.i);
    }

    public void s(int i, int i2, int i3) {
        this.a = i;
        this.b = -1;
        this.h = i2;
        this.i = i3;
        this.f = null;
        ms0 ms0Var = this.d;
        if (ms0Var != null) {
            ms0Var.d();
        }
    }

    public void t(String str) throws JsonProcessingException {
        this.f = str;
        ms0 ms0Var = this.d;
        if (ms0Var != null) {
            j(ms0Var, str);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        int i = this.a;
        if (i == 0) {
            sb.append("/");
        } else if (i != 1) {
            sb.append('{');
            if (this.f != null) {
                sb.append('\"');
                a.a(sb, this.f);
                sb.append('\"');
            } else {
                sb.append('?');
            }
            sb.append('}');
        } else {
            sb.append('[');
            sb.append(a());
            sb.append(']');
        }
        return sb.toString();
    }

    public hv1 u(ms0 ms0Var) {
        this.d = ms0Var;
        return this;
    }
}
