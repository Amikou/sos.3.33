package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: sf3  reason: default package */
/* loaded from: classes2.dex */
public class sf3 extends ct0.b {
    public static final BigInteger g = qf3.j;
    public int[] f;

    public sf3() {
        this.f = kd2.j(17);
    }

    public sf3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP521R1FieldElement");
        }
        this.f = rf3.c(bigInteger);
    }

    public sf3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] j = kd2.j(17);
        rf3.a(this.f, ((sf3) ct0Var).f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] j = kd2.j(17);
        rf3.b(this.f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] j = kd2.j(17);
        g92.d(rf3.a, ((sf3) ct0Var).f, j);
        rf3.f(j, this.f, j);
        return new sf3(j);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof sf3) {
            return kd2.n(17, this.f, ((sf3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] j = kd2.j(17);
        g92.d(rf3.a, this.f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return kd2.v(17, this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 17);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return kd2.w(17, this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] j = kd2.j(17);
        rf3.f(this.f, ((sf3) ct0Var).f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] j = kd2.j(17);
        rf3.g(this.f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (kd2.w(17, iArr) || kd2.v(17, iArr)) {
            return this;
        }
        int[] j = kd2.j(17);
        int[] j2 = kd2.j(17);
        rf3.k(iArr, 519, j);
        rf3.j(j, j2);
        if (kd2.n(17, iArr, j2)) {
            return new sf3(j);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] j = kd2.j(17);
        rf3.j(this.f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] j = kd2.j(17);
        rf3.l(this.f, ((sf3) ct0Var).f, j);
        return new sf3(j);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return kd2.p(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return kd2.P(17, this.f);
    }
}
