package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: ou0  reason: default package */
/* loaded from: classes2.dex */
public final class ou0 implements dq1 {
    public final boolean a;

    public ou0(boolean z) {
        this.a = z;
    }

    @Override // defpackage.dq1
    public boolean b() {
        return this.a;
    }

    @Override // defpackage.dq1
    public tg2 e() {
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty{");
        sb.append(b() ? "Active" : "New");
        sb.append('}');
        return sb.toString();
    }
}
