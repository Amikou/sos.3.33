package defpackage;

import java.io.PrintStream;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: t46  reason: default package */
/* loaded from: classes.dex */
public final class t46 {
    public static final tz5 a;

    static {
        tz5 i46Var;
        Integer num = null;
        try {
            try {
                num = (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
            } catch (Exception e) {
                System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
                e.printStackTrace(System.err);
            }
            if (num == null || num.intValue() < 19) {
                i46Var = !Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic") ? new j36() : new i46();
            } else {
                i46Var = new o46();
            }
        } catch (Throwable th) {
            PrintStream printStream = System.err;
            String name = i46.class.getName();
            StringBuilder sb = new StringBuilder(name.length() + 133);
            sb.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
            sb.append(name);
            sb.append("will be used. The error is: ");
            printStream.println(sb.toString());
            th.printStackTrace(System.err);
            i46Var = new i46();
        }
        a = i46Var;
        if (num == null) {
            return;
        }
        num.intValue();
    }

    public static void a(Throwable th, Throwable th2) {
        a.a(th, th2);
    }
}
