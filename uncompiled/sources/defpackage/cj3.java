package defpackage;

import androidx.media3.common.j;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: SegmentBase.java */
/* renamed from: cj3  reason: default package */
/* loaded from: classes.dex */
public abstract class cj3 {
    public final s33 a;
    public final long b;
    public final long c;

    /* compiled from: SegmentBase.java */
    /* renamed from: cj3$a */
    /* loaded from: classes.dex */
    public static abstract class a extends cj3 {
        public final long d;
        public final long e;
        public final List<d> f;
        public final long g;
        public final long h;
        public final long i;

        public a(s33 s33Var, long j, long j2, long j3, long j4, List<d> list, long j5, long j6, long j7) {
            super(s33Var, j, j2);
            this.d = j3;
            this.e = j4;
            this.f = list;
            this.i = j5;
            this.g = j6;
            this.h = j7;
        }

        public long c(long j, long j2) {
            long g = g(j);
            return g != -1 ? g : (int) (i((j2 - this.h) + this.i, j) - d(j, j2));
        }

        public long d(long j, long j2) {
            if (g(j) == -1) {
                long j3 = this.g;
                if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    return Math.max(e(), i((j2 - this.h) - j3, j));
                }
            }
            return e();
        }

        public long e() {
            return this.d;
        }

        public long f(long j, long j2) {
            if (this.f != null) {
                return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
            }
            long d = d(j, j2) + c(j, j2);
            return (j(d) + h(d, j)) - this.i;
        }

        public abstract long g(long j);

        public final long h(long j, long j2) {
            List<d> list = this.f;
            if (list != null) {
                return (list.get((int) (j - this.d)).b * 1000000) / this.b;
            }
            long g = g(j2);
            if (g != -1 && j == (e() + g) - 1) {
                return j2 - j(j);
            }
            return (this.e * 1000000) / this.b;
        }

        public long i(long j, long j2) {
            long e = e();
            long g = g(j2);
            if (g == 0) {
                return e;
            }
            if (this.f == null) {
                long j3 = this.d + (j / ((this.e * 1000000) / this.b));
                return j3 < e ? e : g == -1 ? j3 : Math.min(j3, (e + g) - 1);
            }
            long j4 = (g + e) - 1;
            long j5 = e;
            while (j5 <= j4) {
                long j6 = ((j4 - j5) / 2) + j5;
                int i = (j(j6) > j ? 1 : (j(j6) == j ? 0 : -1));
                if (i < 0) {
                    j5 = j6 + 1;
                } else if (i <= 0) {
                    return j6;
                } else {
                    j4 = j6 - 1;
                }
            }
            return j5 == e ? j5 : j4;
        }

        public final long j(long j) {
            long j2;
            List<d> list = this.f;
            if (list != null) {
                j2 = list.get((int) (j - this.d)).a - this.c;
            } else {
                j2 = (j - this.d) * this.e;
            }
            return androidx.media3.common.util.b.J0(j2, 1000000L, this.b);
        }

        public abstract s33 k(b73 b73Var, long j);

        public boolean l() {
            return this.f != null;
        }
    }

    /* compiled from: SegmentBase.java */
    /* renamed from: cj3$b */
    /* loaded from: classes.dex */
    public static final class b extends a {
        public final List<s33> j;

        public b(s33 s33Var, long j, long j2, long j3, long j4, List<d> list, long j5, List<s33> list2, long j6, long j7) {
            super(s33Var, j, j2, j3, j4, list, j5, j6, j7);
            this.j = list2;
        }

        @Override // defpackage.cj3.a
        public long g(long j) {
            return this.j.size();
        }

        @Override // defpackage.cj3.a
        public s33 k(b73 b73Var, long j) {
            return this.j.get((int) (j - this.d));
        }

        @Override // defpackage.cj3.a
        public boolean l() {
            return true;
        }
    }

    /* compiled from: SegmentBase.java */
    /* renamed from: cj3$c */
    /* loaded from: classes.dex */
    public static final class c extends a {
        public final uf4 j;
        public final uf4 k;
        public final long l;

        public c(s33 s33Var, long j, long j2, long j3, long j4, long j5, List<d> list, long j6, uf4 uf4Var, uf4 uf4Var2, long j7, long j8) {
            super(s33Var, j, j2, j3, j5, list, j6, j7, j8);
            this.j = uf4Var;
            this.k = uf4Var2;
            this.l = j4;
        }

        @Override // defpackage.cj3
        public s33 a(b73 b73Var) {
            uf4 uf4Var = this.j;
            if (uf4Var != null) {
                j jVar = b73Var.a;
                return new s33(uf4Var.a(jVar.a, 0L, jVar.l0, 0L), 0L, -1L);
            }
            return super.a(b73Var);
        }

        @Override // defpackage.cj3.a
        public long g(long j) {
            List<d> list = this.f;
            if (list != null) {
                return list.size();
            }
            long j2 = this.l;
            if (j2 != -1) {
                return (j2 - this.d) + 1;
            }
            if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return hp.a(BigInteger.valueOf(j).multiply(BigInteger.valueOf(this.b)), BigInteger.valueOf(this.e).multiply(BigInteger.valueOf(1000000L)), RoundingMode.CEILING).longValue();
            }
            return -1L;
        }

        @Override // defpackage.cj3.a
        public s33 k(b73 b73Var, long j) {
            long j2;
            List<d> list = this.f;
            if (list != null) {
                j2 = list.get((int) (j - this.d)).a;
            } else {
                j2 = (j - this.d) * this.e;
            }
            long j3 = j2;
            uf4 uf4Var = this.k;
            j jVar = b73Var.a;
            return new s33(uf4Var.a(jVar.a, j, jVar.l0, j3), 0L, -1L);
        }
    }

    /* compiled from: SegmentBase.java */
    /* renamed from: cj3$d */
    /* loaded from: classes.dex */
    public static final class d {
        public final long a;
        public final long b;

        public d(long j, long j2) {
            this.a = j;
            this.b = j2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            return this.a == dVar.a && this.b == dVar.b;
        }

        public int hashCode() {
            return (((int) this.a) * 31) + ((int) this.b);
        }
    }

    public cj3(s33 s33Var, long j, long j2) {
        this.a = s33Var;
        this.b = j;
        this.c = j2;
    }

    public s33 a(b73 b73Var) {
        return this.a;
    }

    public long b() {
        return androidx.media3.common.util.b.J0(this.c, 1000000L, this.b);
    }

    /* compiled from: SegmentBase.java */
    /* renamed from: cj3$e */
    /* loaded from: classes.dex */
    public static class e extends cj3 {
        public final long d;
        public final long e;

        public e(s33 s33Var, long j, long j2, long j3, long j4) {
            super(s33Var, j, j2);
            this.d = j3;
            this.e = j4;
        }

        public s33 c() {
            long j = this.e;
            if (j <= 0) {
                return null;
            }
            return new s33(null, this.d, j);
        }

        public e() {
            this(null, 1L, 0L, 0L, 0L);
        }
    }
}
