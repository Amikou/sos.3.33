package defpackage;

import defpackage.xs0;
import java.math.BigInteger;
import java.util.Hashtable;
import org.bouncycastle.asn1.i;

/* renamed from: oe1  reason: default package */
/* loaded from: classes2.dex */
public class oe1 {
    public static qr4 a = new a();
    public static qr4 b = new b();
    public static final Hashtable c = new Hashtable();
    public static final Hashtable d = new Hashtable();
    public static final Hashtable e = new Hashtable();

    /* renamed from: oe1$a */
    /* loaded from: classes2.dex */
    public static class a extends qr4 {
        @Override // defpackage.qr4
        public pr4 a() {
            BigInteger e = oe1.e("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF");
            BigInteger e2 = oe1.e("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC");
            BigInteger e3 = oe1.e("28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93");
            BigInteger e4 = oe1.e("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123");
            BigInteger valueOf = BigInteger.valueOf(1L);
            xs0 c = oe1.c(new xs0.f(e, e2, e3, e4, valueOf));
            return new pr4(c, new rr4(c, pk1.a("0432C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0")), e4, valueOf, (byte[]) null);
        }
    }

    /* renamed from: oe1$b */
    /* loaded from: classes2.dex */
    public static class b extends qr4 {
        @Override // defpackage.qr4
        public pr4 a() {
            BigInteger e = oe1.e("BDB6F4FE3E8B1D9E0DA8C0D46F4C318CEFE4AFE3B6B8551F");
            BigInteger e2 = oe1.e("BB8E5E8FBC115E139FE6A814FE48AAA6F0ADA1AA5DF91985");
            BigInteger e3 = oe1.e("1854BEBDC31B21B7AEFC80AB0ECD10D5B1B3308E6DBF11C1");
            BigInteger e4 = oe1.e("BDB6F4FE3E8B1D9E0DA8C0D40FC962195DFAE76F56564677");
            BigInteger valueOf = BigInteger.valueOf(1L);
            xs0 c = oe1.c(new xs0.f(e, e2, e3, e4, valueOf));
            return new pr4(c, new rr4(c, pk1.a("044AD5F7048DE709AD51236DE65E4D4B482C836DC6E410664002BB3A02D4AAADACAE24817A4CA3A1B014B5270432DB27D2")), e4, valueOf, (byte[]) null);
        }
    }

    static {
        d("wapip192v1", pe1.d, b);
        d("sm2p256v1", pe1.b, a);
    }

    public static xs0 c(xs0 xs0Var) {
        return xs0Var;
    }

    public static void d(String str, i iVar, qr4 qr4Var) {
        c.put(su3.f(str), iVar);
        e.put(iVar, str);
        d.put(iVar, qr4Var);
    }

    public static BigInteger e(String str) {
        return new BigInteger(1, pk1.a(str));
    }

    public static pr4 f(String str) {
        i i = i(str);
        if (i == null) {
            return null;
        }
        return g(i);
    }

    public static pr4 g(i iVar) {
        qr4 qr4Var = (qr4) d.get(iVar);
        if (qr4Var == null) {
            return null;
        }
        return qr4Var.b();
    }

    public static String h(i iVar) {
        return (String) e.get(iVar);
    }

    public static i i(String str) {
        return (i) c.get(su3.f(str));
    }
}
