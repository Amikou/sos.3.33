package defpackage;

import defpackage.yo2;

/* compiled from: PagingData.kt */
/* renamed from: fp2  reason: default package */
/* loaded from: classes.dex */
public final class fp2<T> {
    public static final le4 c;
    public final j71<yo2<T>> a;
    public final le4 b;

    /* compiled from: PagingData.kt */
    /* renamed from: fp2$a */
    /* loaded from: classes.dex */
    public static final class a implements le4 {
        @Override // defpackage.le4
        public void a() {
        }

        @Override // defpackage.le4
        public void b(xk4 xk4Var) {
            fs1.f(xk4Var, "viewportHint");
        }
    }

    /* compiled from: PagingData.kt */
    /* renamed from: fp2$b */
    /* loaded from: classes.dex */
    public static final class b {
        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }
    }

    static {
        new b(null);
        a aVar = new a();
        c = aVar;
        new fp2(n71.o(yo2.b.g.d()), aVar);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public fp2(j71<? extends yo2<T>> j71Var, le4 le4Var) {
        fs1.f(j71Var, "flow");
        fs1.f(le4Var, "receiver");
        this.a = j71Var;
        this.b = le4Var;
    }

    public final j71<yo2<T>> a() {
        return this.a;
    }

    public final le4 b() {
        return this.b;
    }
}
