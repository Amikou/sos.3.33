package defpackage;

import defpackage.pt0;

/* renamed from: re3  reason: default package */
/* loaded from: classes2.dex */
public class re3 extends pt0.c {
    public re3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public re3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public re3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        qe3 qe3Var = (qe3) this.c;
        if (qe3Var.i()) {
            return i.v();
        }
        qe3 qe3Var2 = (qe3) this.b;
        qe3 qe3Var3 = (qe3) this.d[0];
        int[] g = cd2.g();
        pe3.i(qe3Var.f, g);
        int[] g2 = cd2.g();
        pe3.i(g, g2);
        int[] g3 = cd2.g();
        pe3.i(qe3Var2.f, g3);
        pe3.h(cd2.b(g3, g3, g3), g3);
        pe3.d(g, qe3Var2.f, g);
        pe3.h(kd2.G(6, g, 2, 0), g);
        int[] g4 = cd2.g();
        pe3.h(kd2.H(6, g2, 3, 0, g4), g4);
        qe3 qe3Var4 = new qe3(g2);
        pe3.i(g3, qe3Var4.f);
        int[] iArr = qe3Var4.f;
        pe3.k(iArr, g, iArr);
        int[] iArr2 = qe3Var4.f;
        pe3.k(iArr2, g, iArr2);
        qe3 qe3Var5 = new qe3(g);
        pe3.k(g, qe3Var4.f, qe3Var5.f);
        int[] iArr3 = qe3Var5.f;
        pe3.d(iArr3, g3, iArr3);
        int[] iArr4 = qe3Var5.f;
        pe3.k(iArr4, g4, iArr4);
        qe3 qe3Var6 = new qe3(g3);
        pe3.l(qe3Var.f, qe3Var6.f);
        if (!qe3Var3.h()) {
            int[] iArr5 = qe3Var6.f;
            pe3.d(iArr5, qe3Var3.f, iArr5);
        }
        return new re3(i, qe3Var4, qe3Var5, new ct0[]{qe3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        qe3 qe3Var = (qe3) this.b;
        qe3 qe3Var2 = (qe3) this.c;
        qe3 qe3Var3 = (qe3) pt0Var.q();
        qe3 qe3Var4 = (qe3) pt0Var.r();
        qe3 qe3Var5 = (qe3) this.d[0];
        qe3 qe3Var6 = (qe3) pt0Var.s(0);
        int[] i2 = cd2.i();
        int[] g = cd2.g();
        int[] g2 = cd2.g();
        int[] g3 = cd2.g();
        boolean h = qe3Var5.h();
        if (h) {
            iArr = qe3Var3.f;
            iArr2 = qe3Var4.f;
        } else {
            pe3.i(qe3Var5.f, g2);
            pe3.d(g2, qe3Var3.f, g);
            pe3.d(g2, qe3Var5.f, g2);
            pe3.d(g2, qe3Var4.f, g2);
            iArr = g;
            iArr2 = g2;
        }
        boolean h2 = qe3Var6.h();
        if (h2) {
            iArr3 = qe3Var.f;
            iArr4 = qe3Var2.f;
        } else {
            pe3.i(qe3Var6.f, g3);
            pe3.d(g3, qe3Var.f, i2);
            pe3.d(g3, qe3Var6.f, g3);
            pe3.d(g3, qe3Var2.f, g3);
            iArr3 = i2;
            iArr4 = g3;
        }
        int[] g4 = cd2.g();
        pe3.k(iArr3, iArr, g4);
        pe3.k(iArr4, iArr2, g);
        if (cd2.u(g4)) {
            return cd2.u(g) ? J() : i.v();
        }
        pe3.i(g4, g2);
        int[] g5 = cd2.g();
        pe3.d(g2, g4, g5);
        pe3.d(g2, iArr3, g2);
        pe3.f(g5, g5);
        cd2.x(iArr4, g5, i2);
        pe3.h(cd2.b(g2, g2, g5), g5);
        qe3 qe3Var7 = new qe3(g3);
        pe3.i(g, qe3Var7.f);
        int[] iArr5 = qe3Var7.f;
        pe3.k(iArr5, g5, iArr5);
        qe3 qe3Var8 = new qe3(g5);
        pe3.k(g2, qe3Var7.f, qe3Var8.f);
        pe3.e(qe3Var8.f, g, i2);
        pe3.g(i2, qe3Var8.f);
        qe3 qe3Var9 = new qe3(g4);
        if (!h) {
            int[] iArr6 = qe3Var9.f;
            pe3.d(iArr6, qe3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = qe3Var9.f;
            pe3.d(iArr7, qe3Var6.f, iArr7);
        }
        return new re3(i, qe3Var7, qe3Var8, new ct0[]{qe3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new re3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new re3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
