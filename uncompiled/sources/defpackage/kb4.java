package defpackage;

import android.view.View;
import androidx.transition.Transition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: TransitionValues.java */
/* renamed from: kb4  reason: default package */
/* loaded from: classes.dex */
public class kb4 {
    public View b;
    public final Map<String, Object> a = new HashMap();
    public final ArrayList<Transition> c = new ArrayList<>();

    @Deprecated
    public kb4() {
    }

    public boolean equals(Object obj) {
        if (obj instanceof kb4) {
            kb4 kb4Var = (kb4) obj;
            return this.b == kb4Var.b && this.a.equals(kb4Var.a);
        }
        return false;
    }

    public int hashCode() {
        return (this.b.hashCode() * 31) + this.a.hashCode();
    }

    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.b + "\n") + "    values:";
        for (String str2 : this.a.keySet()) {
            str = str + "    " + str2 + ": " + this.a.get(str2) + "\n";
        }
        return str;
    }

    public kb4(View view) {
        this.b = view;
    }
}
