package defpackage;

import androidx.media3.common.util.b;

/* compiled from: Descriptor.java */
/* renamed from: nm0  reason: default package */
/* loaded from: classes.dex */
public final class nm0 {
    public final String a;
    public final String b;
    public final String c;

    public nm0(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || nm0.class != obj.getClass()) {
            return false;
        }
        nm0 nm0Var = (nm0) obj;
        return b.c(this.a, nm0Var.a) && b.c(this.b, nm0Var.b) && b.c(this.c, nm0Var.c);
    }

    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        String str = this.b;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.c;
        return hashCode2 + (str2 != null ? str2.hashCode() : 0);
    }
}
