package defpackage;

import java.util.Iterator;
import java.util.Map;

/* renamed from: mg5  reason: default package */
/* loaded from: classes.dex */
public final class mg5 extends ch5 {
    public final /* synthetic */ fg5 f0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public mg5(fg5 fg5Var) {
        super(fg5Var, null);
        this.f0 = fg5Var;
    }

    public /* synthetic */ mg5(fg5 fg5Var, hg5 hg5Var) {
        this(fg5Var);
    }

    @Override // defpackage.ch5, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new kg5(this.f0, null);
    }
}
