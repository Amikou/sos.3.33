package defpackage;

import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: y75  reason: default package */
/* loaded from: classes.dex */
public final class y75 {
    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    public static z55 a(String str, p45 p45Var, wk5 wk5Var, List<z55> list) {
        String str2;
        char c;
        String str3;
        double min;
        c55 c55Var;
        switch (str.hashCode()) {
            case -1776922004:
                str2 = "toString";
                if (str.equals(str2)) {
                    c = 18;
                    break;
                }
                c = 65535;
                break;
            case -1354795244:
                if (str.equals("concat")) {
                    str2 = "toString";
                    c = 0;
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case -1274492040:
                if (str.equals("filter")) {
                    str2 = "toString";
                    c = 2;
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case -934873754:
                if (str.equals("reduce")) {
                    c = '\n';
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case -895859076:
                if (str.equals("splice")) {
                    c = 17;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case -678635926:
                if (str.equals("forEach")) {
                    c = 3;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case -467511597:
                if (str.equals("lastIndexOf")) {
                    c = 6;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case -277637751:
                if (str.equals("unshift")) {
                    c = 19;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 107868:
                if (str.equals("map")) {
                    c = 7;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 111185:
                if (str.equals("pop")) {
                    c = '\b';
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 3267882:
                if (str.equals("join")) {
                    c = 5;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 3452698:
                if (str.equals("push")) {
                    c = '\t';
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 3536116:
                if (str.equals("some")) {
                    c = 15;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 3536286:
                if (str.equals("sort")) {
                    c = 16;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 96891675:
                if (str.equals("every")) {
                    str2 = "toString";
                    c = 1;
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 109407362:
                if (str.equals("shift")) {
                    c = '\r';
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 109526418:
                if (str.equals("slice")) {
                    c = 14;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 965561430:
                if (str.equals("reduceRight")) {
                    c = 11;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 1099846370:
                if (str.equals("reverse")) {
                    c = '\f';
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            case 1943291465:
                if (str.equals("indexOf")) {
                    c = 4;
                    str2 = "toString";
                    break;
                }
                str2 = "toString";
                c = 65535;
                break;
            default:
                str2 = "toString";
                c = 65535;
                break;
        }
        double d = Utils.DOUBLE_EPSILON;
        switch (c) {
            case 0:
                z55 m = p45Var.m();
                if (!list.isEmpty()) {
                    for (z55 z55Var : list) {
                        z55 a = wk5Var.a(z55Var);
                        if (!(a instanceof v45)) {
                            p45 p45Var2 = (p45) m;
                            int s = p45Var2.s();
                            if (a instanceof p45) {
                                p45 p45Var3 = (p45) a;
                                Iterator<Integer> q = p45Var3.q();
                                while (q.hasNext()) {
                                    Integer next = q.next();
                                    p45Var2.y(next.intValue() + s, p45Var3.w(next.intValue()));
                                }
                            } else {
                                p45Var2.y(s, a);
                            }
                        } else {
                            throw new IllegalStateException("Failed evaluation of arguments");
                        }
                    }
                }
                return m;
            case 1:
                vm5.a("every", 1, list);
                z55 a2 = wk5Var.a(list.get(0));
                if (a2 instanceof u55) {
                    if (p45Var.s() != 0 && c(p45Var, wk5Var, (u55) a2, Boolean.FALSE, Boolean.TRUE).s() != p45Var.s()) {
                        return z55.d0;
                    }
                    return z55.c0;
                }
                throw new IllegalArgumentException("Callback should be a method");
            case 2:
                vm5.a("filter", 1, list);
                z55 a3 = wk5Var.a(list.get(0));
                if (a3 instanceof u55) {
                    if (p45Var.t() == 0) {
                        return new p45();
                    }
                    z55 m2 = p45Var.m();
                    p45 c2 = c(p45Var, wk5Var, (u55) a3, null, Boolean.TRUE);
                    p45 p45Var4 = new p45();
                    Iterator<Integer> q2 = c2.q();
                    while (q2.hasNext()) {
                        p45Var4.y(p45Var4.s(), ((p45) m2).w(q2.next().intValue()));
                    }
                    return p45Var4;
                }
                throw new IllegalArgumentException("Callback should be a method");
            case 3:
                vm5.a("forEach", 1, list);
                z55 a4 = wk5Var.a(list.get(0));
                if (a4 instanceof u55) {
                    if (p45Var.t() == 0) {
                        return z55.X;
                    }
                    c(p45Var, wk5Var, (u55) a4, null, null);
                    return z55.X;
                }
                throw new IllegalArgumentException("Callback should be a method");
            case 4:
                vm5.c("indexOf", 2, list);
                z55 z55Var2 = z55.X;
                if (!list.isEmpty()) {
                    z55Var2 = wk5Var.a(list.get(0));
                }
                if (list.size() > 1) {
                    double i = vm5.i(wk5Var.a(list.get(1)).b().doubleValue());
                    if (i >= p45Var.s()) {
                        return new z45(Double.valueOf(-1.0d));
                    }
                    d = i < Utils.DOUBLE_EPSILON ? p45Var.s() + i : i;
                }
                Iterator<Integer> q3 = p45Var.q();
                while (q3.hasNext()) {
                    int intValue = q3.next().intValue();
                    double d2 = intValue;
                    if (d2 >= d && vm5.f(p45Var.w(intValue), z55Var2)) {
                        return new z45(Double.valueOf(d2));
                    }
                }
                return new z45(Double.valueOf(-1.0d));
            case 5:
                vm5.c("join", 1, list);
                if (p45Var.s() == 0) {
                    return z55.e0;
                }
                if (list.size() > 0) {
                    z55 a5 = wk5Var.a(list.get(0));
                    str3 = ((a5 instanceof t55) || (a5 instanceof i65)) ? "" : a5.zzc();
                } else {
                    str3 = ",";
                }
                return new f65(p45Var.F(str3));
            case 6:
                vm5.c("lastIndexOf", 2, list);
                z55 z55Var3 = z55.X;
                if (!list.isEmpty()) {
                    z55Var3 = wk5Var.a(list.get(0));
                }
                double s2 = p45Var.s() - 1;
                if (list.size() > 1) {
                    z55 a6 = wk5Var.a(list.get(1));
                    s2 = Double.isNaN(a6.b().doubleValue()) ? p45Var.s() - 1 : vm5.i(a6.b().doubleValue());
                    if (s2 < Utils.DOUBLE_EPSILON) {
                        s2 += p45Var.s();
                    }
                }
                if (s2 < Utils.DOUBLE_EPSILON) {
                    return new z45(Double.valueOf(-1.0d));
                }
                for (int min2 = (int) Math.min(p45Var.s(), s2); min2 >= 0; min2--) {
                    if (p45Var.z(min2) && vm5.f(p45Var.w(min2), z55Var3)) {
                        return new z45(Double.valueOf(min2));
                    }
                }
                return new z45(Double.valueOf(-1.0d));
            case 7:
                vm5.a("map", 1, list);
                z55 a7 = wk5Var.a(list.get(0));
                if (a7 instanceof u55) {
                    if (p45Var.s() == 0) {
                        return new p45();
                    }
                    return c(p45Var, wk5Var, (u55) a7, null, null);
                }
                throw new IllegalArgumentException("Callback should be a method");
            case '\b':
                vm5.a("pop", 0, list);
                int s3 = p45Var.s();
                if (s3 == 0) {
                    return z55.X;
                }
                int i2 = s3 - 1;
                z55 w = p45Var.w(i2);
                p45Var.E(i2);
                return w;
            case '\t':
                if (!list.isEmpty()) {
                    for (z55 z55Var4 : list) {
                        p45Var.y(p45Var.s(), wk5Var.a(z55Var4));
                    }
                }
                return new z45(Double.valueOf(p45Var.s()));
            case '\n':
                return b(p45Var, wk5Var, list, true);
            case 11:
                return b(p45Var, wk5Var, list, false);
            case '\f':
                vm5.a("reverse", 0, list);
                int s4 = p45Var.s();
                if (s4 != 0) {
                    for (int i3 = 0; i3 < s4 / 2; i3++) {
                        if (p45Var.z(i3)) {
                            z55 w2 = p45Var.w(i3);
                            p45Var.y(i3, null);
                            int i4 = (s4 - 1) - i3;
                            if (p45Var.z(i4)) {
                                p45Var.y(i3, p45Var.w(i4));
                            }
                            p45Var.y(i4, w2);
                        }
                    }
                }
                return p45Var;
            case '\r':
                vm5.a("shift", 0, list);
                if (p45Var.s() == 0) {
                    return z55.X;
                }
                z55 w3 = p45Var.w(0);
                p45Var.E(0);
                return w3;
            case 14:
                vm5.c("slice", 2, list);
                if (list.isEmpty()) {
                    return p45Var.m();
                }
                double s5 = p45Var.s();
                double i5 = vm5.i(wk5Var.a(list.get(0)).b().doubleValue());
                if (i5 < Utils.DOUBLE_EPSILON) {
                    min = Math.max(i5 + s5, (double) Utils.DOUBLE_EPSILON);
                } else {
                    min = Math.min(i5, s5);
                }
                if (list.size() == 2) {
                    double i6 = vm5.i(wk5Var.a(list.get(1)).b().doubleValue());
                    if (i6 < Utils.DOUBLE_EPSILON) {
                        s5 = Math.max(s5 + i6, (double) Utils.DOUBLE_EPSILON);
                    } else {
                        s5 = Math.min(s5, i6);
                    }
                }
                p45 p45Var5 = new p45();
                for (int i7 = (int) min; i7 < s5; i7++) {
                    p45Var5.y(p45Var5.s(), p45Var.w(i7));
                }
                return p45Var5;
            case 15:
                vm5.a("some", 1, list);
                z55 a8 = wk5Var.a(list.get(0));
                if (a8 instanceof c55) {
                    if (p45Var.s() == 0) {
                        return z55.d0;
                    }
                    c55 c55Var2 = (c55) a8;
                    Iterator<Integer> q4 = p45Var.q();
                    while (q4.hasNext()) {
                        int intValue2 = q4.next().intValue();
                        if (p45Var.z(intValue2) && c55Var2.a(wk5Var, Arrays.asList(p45Var.w(intValue2), new z45(Double.valueOf(intValue2)), p45Var)).c().booleanValue()) {
                            return z55.c0;
                        }
                    }
                    return z55.d0;
                }
                throw new IllegalArgumentException("Callback should be a method");
            case 16:
                vm5.c("sort", 1, list);
                if (p45Var.s() >= 2) {
                    List<z55> p = p45Var.p();
                    if (list.isEmpty()) {
                        c55Var = null;
                    } else {
                        z55 a9 = wk5Var.a(list.get(0));
                        if (a9 instanceof c55) {
                            c55Var = (c55) a9;
                        } else {
                            throw new IllegalArgumentException("Comparator should be a method");
                        }
                    }
                    Collections.sort(p, new w75(c55Var, wk5Var));
                    p45Var.B();
                    int i8 = 0;
                    for (z55 z55Var5 : p) {
                        p45Var.y(i8, z55Var5);
                        i8++;
                    }
                }
                return p45Var;
            case 17:
                if (list.isEmpty()) {
                    return new p45();
                }
                int i9 = (int) vm5.i(wk5Var.a(list.get(0)).b().doubleValue());
                if (i9 < 0) {
                    i9 = Math.max(0, i9 + p45Var.s());
                } else if (i9 > p45Var.s()) {
                    i9 = p45Var.s();
                }
                int s6 = p45Var.s();
                p45 p45Var6 = new p45();
                if (list.size() > 1) {
                    int max = Math.max(0, (int) vm5.i(wk5Var.a(list.get(1)).b().doubleValue()));
                    if (max > 0) {
                        for (int i10 = i9; i10 < Math.min(s6, i9 + max); i10++) {
                            p45Var6.y(p45Var6.s(), p45Var.w(i9));
                            p45Var.E(i9);
                        }
                    }
                    if (list.size() > 2) {
                        for (int i11 = 2; i11 < list.size(); i11++) {
                            z55 a10 = wk5Var.a(list.get(i11));
                            if (!(a10 instanceof v45)) {
                                p45Var.D((i9 + i11) - 2, a10);
                            } else {
                                throw new IllegalArgumentException("Failed to parse elements to add");
                            }
                        }
                    }
                } else {
                    while (i9 < s6) {
                        p45Var6.y(p45Var6.s(), p45Var.w(i9));
                        p45Var.y(i9, null);
                        i9++;
                    }
                }
                return p45Var6;
            case 18:
                vm5.a(str2, 0, list);
                return new f65(p45Var.F(","));
            case 19:
                if (!list.isEmpty()) {
                    p45 p45Var7 = new p45();
                    for (z55 z55Var6 : list) {
                        z55 a11 = wk5Var.a(z55Var6);
                        if (!(a11 instanceof v45)) {
                            p45Var7.y(p45Var7.s(), a11);
                        } else {
                            throw new IllegalStateException("Argument evaluation failed");
                        }
                    }
                    int s7 = p45Var7.s();
                    Iterator<Integer> q5 = p45Var.q();
                    while (q5.hasNext()) {
                        Integer next2 = q5.next();
                        p45Var7.y(next2.intValue() + s7, p45Var.w(next2.intValue()));
                    }
                    p45Var.B();
                    Iterator<Integer> q6 = p45Var7.q();
                    while (q6.hasNext()) {
                        Integer next3 = q6.next();
                        p45Var.y(next3.intValue(), p45Var7.w(next3.intValue()));
                    }
                }
                return new z45(Double.valueOf(p45Var.s()));
            default:
                throw new IllegalArgumentException("Command not supported");
        }
    }

    public static z55 b(p45 p45Var, wk5 wk5Var, List<z55> list, boolean z) {
        z55 z55Var;
        vm5.b("reduce", 1, list);
        vm5.c("reduce", 2, list);
        z55 a = wk5Var.a(list.get(0));
        if (a instanceof c55) {
            if (list.size() == 2) {
                z55Var = wk5Var.a(list.get(1));
                if (z55Var instanceof v45) {
                    throw new IllegalArgumentException("Failed to parse initial value");
                }
            } else if (p45Var.s() == 0) {
                throw new IllegalStateException("Empty array with no initial value error");
            } else {
                z55Var = null;
            }
            c55 c55Var = (c55) a;
            int s = p45Var.s();
            int i = z ? 0 : s - 1;
            int i2 = z ? s - 1 : 0;
            int i3 = true == z ? 1 : -1;
            if (z55Var == null) {
                z55Var = p45Var.w(i);
                i += i3;
            }
            while ((i2 - i) * i3 >= 0) {
                if (p45Var.z(i)) {
                    z55Var = c55Var.a(wk5Var, Arrays.asList(z55Var, p45Var.w(i), new z45(Double.valueOf(i)), p45Var));
                    if (z55Var instanceof v45) {
                        throw new IllegalStateException("Reduce operation failed");
                    }
                    i += i3;
                } else {
                    i += i3;
                }
            }
            return z55Var;
        }
        throw new IllegalArgumentException("Callback should be a method");
    }

    public static p45 c(p45 p45Var, wk5 wk5Var, c55 c55Var, Boolean bool, Boolean bool2) {
        p45 p45Var2 = new p45();
        Iterator<Integer> q = p45Var.q();
        while (q.hasNext()) {
            int intValue = q.next().intValue();
            if (p45Var.z(intValue)) {
                z55 a = c55Var.a(wk5Var, Arrays.asList(p45Var.w(intValue), new z45(Double.valueOf(intValue)), p45Var));
                if (a.c().equals(bool)) {
                    return p45Var2;
                }
                if (bool2 == null || a.c().equals(bool2)) {
                    p45Var2.y(intValue, a);
                }
            }
        }
        return p45Var2;
    }
}
