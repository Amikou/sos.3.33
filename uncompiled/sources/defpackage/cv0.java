package defpackage;

import java.util.Arrays;
import java.util.Objects;

/* compiled from: EncodedPayload.java */
/* renamed from: cv0  reason: default package */
/* loaded from: classes.dex */
public final class cv0 {
    public final hv0 a;
    public final byte[] b;

    public cv0(hv0 hv0Var, byte[] bArr) {
        Objects.requireNonNull(hv0Var, "encoding is null");
        Objects.requireNonNull(bArr, "bytes is null");
        this.a = hv0Var;
        this.b = bArr;
    }

    public byte[] a() {
        return this.b;
    }

    public hv0 b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof cv0) {
            cv0 cv0Var = (cv0) obj;
            if (this.a.equals(cv0Var.a)) {
                return Arrays.equals(this.b, cv0Var.b);
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    public String toString() {
        return "EncodedPayload{encoding=" + this.a + ", bytes=[...]}";
    }
}
