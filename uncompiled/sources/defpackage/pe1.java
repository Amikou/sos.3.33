package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: pe1  reason: default package */
/* loaded from: classes2.dex */
public interface pe1 {
    public static final i a;
    public static final i b;
    public static final i c;
    public static final i d;
    public static final i e;

    static {
        i iVar = new i("1.2.156.10197.1");
        a = iVar;
        iVar.z("101.1");
        iVar.z("101.2");
        iVar.z("101.3");
        iVar.z("101.4");
        iVar.z("102.1");
        iVar.z("102.2");
        iVar.z("102.3");
        iVar.z("102.4");
        iVar.z("102.5");
        iVar.z("102.6");
        iVar.z("103.1");
        iVar.z("103.2");
        iVar.z("103.3");
        iVar.z("103.4");
        iVar.z("103.5");
        iVar.z("103.6");
        iVar.z("104.1");
        iVar.z("104.2");
        iVar.z("104.3");
        iVar.z("104.4");
        iVar.z("104.5");
        iVar.z("104.6");
        iVar.z("104.7");
        iVar.z("104.8");
        iVar.z("104.9");
        iVar.z("104.10");
        iVar.z("104.11");
        iVar.z("104.12");
        iVar.z("104.100");
        iVar.z("201");
        b = iVar.z("301");
        iVar.z("301.1");
        iVar.z("301.2");
        i z = iVar.z("301.3");
        c = z;
        d = iVar.z("301.101");
        z.z("1");
        z.z("2");
        z.z("2.1");
        z.z("2.2");
        z.z("2.3");
        z.z("2.4");
        z.z("2.5");
        z.z("2.6");
        z.z("2.7");
        z.z("2.8");
        z.z("2.9");
        z.z("2.10");
        z.z("2.11");
        iVar.z("302");
        iVar.z("302.1");
        iVar.z("302.2");
        iVar.z("302.3");
        i z2 = iVar.z("401");
        e = z2;
        z2.z("2");
        iVar.z("501");
        iVar.z("502");
        iVar.z("503");
        iVar.z("504");
        iVar.z("505");
        iVar.z("506");
        iVar.z("507");
        iVar.z("520");
        iVar.z("521");
        iVar.z("522");
    }
}
