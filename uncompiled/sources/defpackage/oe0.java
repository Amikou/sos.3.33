package defpackage;

import android.util.Base64;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: DataUrlLoader.java */
/* renamed from: oe0  reason: default package */
/* loaded from: classes.dex */
public final class oe0<Model, Data> implements j92<Model, Data> {
    public final a<Data> a;

    /* compiled from: DataUrlLoader.java */
    /* renamed from: oe0$a */
    /* loaded from: classes.dex */
    public interface a<Data> {
        Class<Data> a();

        void b(Data data) throws IOException;

        Data c(String str) throws IllegalArgumentException;
    }

    /* compiled from: DataUrlLoader.java */
    /* renamed from: oe0$b */
    /* loaded from: classes.dex */
    public static final class b<Data> implements d<Data> {
        public final String a;
        public final a<Data> f0;
        public Data g0;

        public b(String str, a<Data> aVar) {
            this.a = str;
            this.f0 = aVar;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<Data> a() {
            return this.f0.a();
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
            try {
                this.f0.b(this.g0);
            } catch (IOException unused) {
            }
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.Object, Data] */
        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super Data> aVar) {
            try {
                Data c = this.f0.c(this.a);
                this.g0 = c;
                aVar.f(c);
            } catch (IllegalArgumentException e) {
                aVar.c(e);
            }
        }
    }

    /* compiled from: DataUrlLoader.java */
    /* renamed from: oe0$c */
    /* loaded from: classes.dex */
    public static final class c<Model> implements k92<Model, InputStream> {
        public final a<InputStream> a = new a(this);

        /* compiled from: DataUrlLoader.java */
        /* renamed from: oe0$c$a */
        /* loaded from: classes.dex */
        public class a implements a<InputStream> {
            public a(c cVar) {
            }

            @Override // defpackage.oe0.a
            public Class<InputStream> a() {
                return InputStream.class;
            }

            @Override // defpackage.oe0.a
            /* renamed from: d */
            public void b(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            @Override // defpackage.oe0.a
            /* renamed from: e */
            public InputStream c(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf != -1) {
                        if (str.substring(0, indexOf).endsWith(";base64")) {
                            return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                        }
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                    throw new IllegalArgumentException("Missing comma in data URL.");
                }
                throw new IllegalArgumentException("Not a valid image data URL.");
            }
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<Model, InputStream> c(qa2 qa2Var) {
            return new oe0(this.a);
        }
    }

    public oe0(a<Data> aVar) {
        this.a = aVar;
    }

    @Override // defpackage.j92
    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }

    @Override // defpackage.j92
    public j92.a<Data> b(Model model, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(model), new b(model.toString(), this.a));
    }
}
