package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.creageek.segmentedbutton.SegmentedButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: AppTopBarBinding.java */
/* renamed from: wf  reason: default package */
/* loaded from: classes2.dex */
public final class wf {
    public final ImageView a;
    public final ImageView b;
    public final ImageView c;
    public final TextView d;
    public final RadioButton e;
    public final RadioButton f;
    public final RadioButton g;
    public final SegmentedButton h;
    public final MaterialTextView i;

    public wf(ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, ImageView imageView3, TextView textView, RadioButton radioButton, RadioButton radioButton2, RadioButton radioButton3, RadioButton radioButton4, SegmentedButton segmentedButton, MaterialTextView materialTextView) {
        this.a = imageView;
        this.b = imageView2;
        this.c = imageView3;
        this.d = textView;
        this.e = radioButton;
        this.f = radioButton3;
        this.g = radioButton4;
        this.h = segmentedButton;
        this.i = materialTextView;
    }

    public static wf a(View view) {
        int i = R.id.ivNotification;
        ImageView imageView = (ImageView) ai4.a(view, R.id.ivNotification);
        if (imageView != null) {
            i = R.id.ivTokenList;
            ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivTokenList);
            if (imageView2 != null) {
                i = R.id.iv_token_list_add_new;
                ImageView imageView3 = (ImageView) ai4.a(view, R.id.iv_token_list_add_new);
                if (imageView3 != null) {
                    i = R.id.leftText;
                    TextView textView = (TextView) ai4.a(view, R.id.leftText);
                    if (textView != null) {
                        i = R.id.rbCalculate;
                        RadioButton radioButton = (RadioButton) ai4.a(view, R.id.rbCalculate);
                        if (radioButton != null) {
                            i = R.id.rbNftS;
                            RadioButton radioButton2 = (RadioButton) ai4.a(view, R.id.rbNftS);
                            if (radioButton2 != null) {
                                i = R.id.rbReflections;
                                RadioButton radioButton3 = (RadioButton) ai4.a(view, R.id.rbReflections);
                                if (radioButton3 != null) {
                                    i = R.id.rbTokens;
                                    RadioButton radioButton4 = (RadioButton) ai4.a(view, R.id.rbTokens);
                                    if (radioButton4 != null) {
                                        i = R.id.segmentedGroup;
                                        SegmentedButton segmentedButton = (SegmentedButton) ai4.a(view, R.id.segmentedGroup);
                                        if (segmentedButton != null) {
                                            i = R.id.txtUnReadCount;
                                            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtUnReadCount);
                                            if (materialTextView != null) {
                                                return new wf((ConstraintLayout) view, imageView, imageView2, imageView3, textView, radioButton, radioButton2, radioButton3, radioButton4, segmentedButton, materialTextView);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
