package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzfz;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: kk5  reason: default package */
/* loaded from: classes.dex */
public final class kk5 implements Parcelable.Creator<zzfz> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzfz createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        long j = 0;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 1) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 2) {
                i2 = SafeParcelReader.E(parcel, C);
            } else if (v == 3) {
                i3 = SafeParcelReader.E(parcel, C);
            } else if (v == 4) {
                i4 = SafeParcelReader.E(parcel, C);
            } else if (v != 5) {
                SafeParcelReader.I(parcel, C);
            } else {
                j = SafeParcelReader.F(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzfz(i, i2, i3, i4, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzfz[] newArray(int i) {
        return new zzfz[i];
    }
}
