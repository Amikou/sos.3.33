package defpackage;

import com.google.android.gms.internal.measurement.h1;
import com.google.android.gms.internal.measurement.w1;
import com.google.android.gms.internal.measurement.z0;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: jj5  reason: default package */
/* loaded from: classes.dex */
public final class jj5 extends w1<z0, jj5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public jj5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.z0 r0 = com.google.android.gms.internal.measurement.z0.F()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.jj5.<init>():void");
    }

    public final jj5 A(boolean z) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        z0.J((z0) this.f0, z);
        return this;
    }

    public final jj5 v(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        z0.G((z0) this.f0, i);
        return this;
    }

    public final jj5 x(cl5 cl5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        z0.H((z0) this.f0, cl5Var.o());
        return this;
    }

    public final jj5 y(h1 h1Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        z0.I((z0) this.f0, h1Var);
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ jj5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.z0 r1 = com.google.android.gms.internal.measurement.z0.F()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.jj5.<init>(wi5):void");
    }
}
