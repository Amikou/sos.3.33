package defpackage;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: d25  reason: default package */
/* loaded from: classes.dex */
public abstract class d25 extends LifecycleCallback implements DialogInterface.OnCancelListener {
    public volatile boolean f0;
    public final AtomicReference<j25> g0;
    public final Handler h0;
    public final dh1 i0;

    public d25(nz1 nz1Var) {
        this(nz1Var, dh1.q());
    }

    public static int k(j25 j25Var) {
        if (j25Var == null) {
            return -1;
        }
        return j25Var.a();
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void d(int i, int i2, Intent intent) {
        j25 j25Var = this.g0.get();
        if (i != 1) {
            if (i == 2) {
                int i3 = this.i0.i(b());
                r1 = i3 == 0;
                if (j25Var == null) {
                    return;
                }
                if (j25Var.b().I1() == 18 && i3 == 18) {
                    return;
                }
            }
            r1 = false;
        } else if (i2 != -1) {
            if (i2 == 0) {
                if (j25Var == null) {
                    return;
                }
                j25 j25Var2 = new j25(new ConnectionResult(intent != null ? intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13) : 13, null, j25Var.b().toString()), k(j25Var));
                this.g0.set(j25Var2);
                j25Var = j25Var2;
            }
            r1 = false;
        }
        if (r1) {
            n();
        } else if (j25Var != null) {
            m(j25Var.b(), j25Var.a());
        }
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void e(Bundle bundle) {
        super.e(bundle);
        if (bundle != null) {
            this.g0.set(bundle.getBoolean("resolving_error", false) ? new j25(new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void h(Bundle bundle) {
        super.h(bundle);
        j25 j25Var = this.g0.get();
        if (j25Var != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", j25Var.a());
            bundle.putInt("failed_status", j25Var.b().I1());
            bundle.putParcelable("failed_resolution", j25Var.b().K1());
        }
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void i() {
        super.i();
        this.f0 = true;
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        this.f0 = false;
    }

    public abstract void l();

    public abstract void m(ConnectionResult connectionResult, int i);

    public final void n() {
        this.g0.set(null);
        l();
    }

    public final void o(ConnectionResult connectionResult, int i) {
        j25 j25Var = new j25(connectionResult, i);
        if (this.g0.compareAndSet(null, j25Var)) {
            this.h0.post(new g25(this, j25Var));
        }
    }

    @Override // android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        m(new ConnectionResult(13, null), k(this.g0.get()));
        n();
    }

    public d25(nz1 nz1Var, dh1 dh1Var) {
        super(nz1Var);
        this.g0 = new AtomicReference<>(null);
        this.h0 = new q25(Looper.getMainLooper());
        this.i0 = dh1Var;
    }
}
