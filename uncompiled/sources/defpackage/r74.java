package defpackage;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import com.google.firebase.messaging.j;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: r74  reason: default package */
/* loaded from: classes2.dex */
public class r74 implements Runnable {
    public static final Object j0 = new Object();
    public static Boolean k0;
    public static Boolean l0;
    public final Context a;
    public final i82 f0;
    public final PowerManager.WakeLock g0;
    public final j h0;
    public final long i0;

    /* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
    /* renamed from: r74$a */
    /* loaded from: classes2.dex */
    public class a extends BroadcastReceiver {
        public r74 a;

        public a(r74 r74Var) {
            this.a = r74Var;
        }

        public void a() {
            r74.j();
            r74.this.a.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }

        @Override // android.content.BroadcastReceiver
        public synchronized void onReceive(Context context, Intent intent) {
            r74 r74Var = this.a;
            if (r74Var == null) {
                return;
            }
            if (r74Var.i()) {
                r74.j();
                this.a.h0.k(this.a, 0L);
                context.unregisterReceiver(this);
                this.a = null;
            }
        }
    }

    public r74(j jVar, Context context, i82 i82Var, long j) {
        this.h0 = jVar;
        this.a = context;
        this.i0 = j;
        this.f0 = i82Var;
        this.g0 = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "wake:com.google.firebase.messaging");
    }

    public static String e(String str) {
        StringBuilder sb = new StringBuilder(str.length() + 142);
        sb.append("Missing Permission: ");
        sb.append(str);
        sb.append(". This permission should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        return sb.toString();
    }

    public static boolean f(Context context) {
        boolean booleanValue;
        boolean booleanValue2;
        synchronized (j0) {
            Boolean bool = l0;
            if (bool == null) {
                booleanValue = g(context, "android.permission.ACCESS_NETWORK_STATE", bool);
            } else {
                booleanValue = bool.booleanValue();
            }
            Boolean valueOf = Boolean.valueOf(booleanValue);
            l0 = valueOf;
            booleanValue2 = valueOf.booleanValue();
        }
        return booleanValue2;
    }

    public static boolean g(Context context, String str, Boolean bool) {
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean z = context.checkCallingOrSelfPermission(str) == 0;
        if (z || !Log.isLoggable("FirebaseMessaging", 3)) {
            return z;
        }
        e(str);
        return false;
    }

    public static boolean h(Context context) {
        boolean booleanValue;
        boolean booleanValue2;
        synchronized (j0) {
            Boolean bool = k0;
            if (bool == null) {
                booleanValue = g(context, "android.permission.WAKE_LOCK", bool);
            } else {
                booleanValue = bool.booleanValue();
            }
            Boolean valueOf = Boolean.valueOf(booleanValue);
            k0 = valueOf;
            booleanValue2 = valueOf.booleanValue();
        }
        return booleanValue2;
    }

    public static boolean j() {
        return Log.isLoggable("FirebaseMessaging", 3) || (Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3));
    }

    public final synchronized boolean i() {
        boolean z;
        ConnectivityManager connectivityManager = (ConnectivityManager) this.a.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        if (activeNetworkInfo != null) {
            z = activeNetworkInfo.isConnected();
        }
        return z;
    }

    @Override // java.lang.Runnable
    @SuppressLint({"Wakelock"})
    public void run() {
        if (h(this.a)) {
            this.g0.acquire(com.google.firebase.messaging.a.a);
        }
        try {
            try {
                this.h0.l(true);
                if (!this.f0.g()) {
                    this.h0.l(false);
                    if (h(this.a)) {
                        try {
                            this.g0.release();
                        } catch (RuntimeException unused) {
                        }
                    }
                } else if (f(this.a) && !i()) {
                    new a(this).a();
                    if (h(this.a)) {
                        try {
                            this.g0.release();
                        } catch (RuntimeException unused2) {
                        }
                    }
                } else {
                    if (this.h0.o()) {
                        this.h0.l(false);
                    } else {
                        this.h0.p(this.i0);
                    }
                    if (h(this.a)) {
                        try {
                            this.g0.release();
                        } catch (RuntimeException unused3) {
                        }
                    }
                }
            } catch (IOException e) {
                String valueOf = String.valueOf(e.getMessage());
                if (valueOf.length() != 0) {
                    "Failed to sync topics. Won't retry sync. ".concat(valueOf);
                }
                this.h0.l(false);
                if (h(this.a)) {
                    try {
                        this.g0.release();
                    } catch (RuntimeException unused4) {
                    }
                }
            }
        } catch (Throwable th) {
            if (h(this.a)) {
                try {
                    this.g0.release();
                } catch (RuntimeException unused5) {
                }
            }
            throw th;
        }
    }
}
