package defpackage;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.internal.measurement.zzcl;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: gm5  reason: default package */
/* loaded from: classes.dex */
public final class gm5 {
    public final Context a;
    public String b;
    public String c;
    public String d;
    public Boolean e;
    public long f;
    public zzcl g;
    public boolean h;
    public final Long i;
    public String j;

    public gm5(Context context, zzcl zzclVar, Long l) {
        this.h = true;
        zt2.j(context);
        Context applicationContext = context.getApplicationContext();
        zt2.j(applicationContext);
        this.a = applicationContext;
        this.i = l;
        if (zzclVar != null) {
            this.g = zzclVar;
            this.b = zzclVar.j0;
            this.c = zzclVar.i0;
            this.d = zzclVar.h0;
            this.h = zzclVar.g0;
            this.f = zzclVar.f0;
            this.j = zzclVar.l0;
            Bundle bundle = zzclVar.k0;
            if (bundle != null) {
                this.e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
