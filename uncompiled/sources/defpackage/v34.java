package defpackage;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.b;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: TaskStackBuilder.java */
/* renamed from: v34  reason: default package */
/* loaded from: classes.dex */
public final class v34 implements Iterable<Intent> {
    public final ArrayList<Intent> a = new ArrayList<>();
    public final Context f0;

    /* compiled from: TaskStackBuilder.java */
    /* renamed from: v34$a */
    /* loaded from: classes.dex */
    public interface a {
        Intent getSupportParentActivityIntent();
    }

    public v34(Context context) {
        this.f0 = context;
    }

    public static v34 n(Context context) {
        return new v34(context);
    }

    public v34 e(Intent intent) {
        this.a.add(intent);
        return this;
    }

    public v34 i(Intent intent) {
        ComponentName component = intent.getComponent();
        if (component == null) {
            component = intent.resolveActivity(this.f0.getPackageManager());
        }
        if (component != null) {
            m(component);
        }
        e(intent);
        return this;
    }

    @Override // java.lang.Iterable
    @Deprecated
    public Iterator<Intent> iterator() {
        return this.a.iterator();
    }

    public v34 k(Activity activity) {
        Intent supportParentActivityIntent = activity instanceof a ? ((a) activity).getSupportParentActivityIntent() : null;
        if (supportParentActivityIntent == null) {
            supportParentActivityIntent = b.a(activity);
        }
        if (supportParentActivityIntent != null) {
            ComponentName component = supportParentActivityIntent.getComponent();
            if (component == null) {
                component = supportParentActivityIntent.resolveActivity(this.f0.getPackageManager());
            }
            m(component);
            e(supportParentActivityIntent);
        }
        return this;
    }

    public v34 m(ComponentName componentName) {
        int size = this.a.size();
        try {
            Intent b = b.b(this.f0, componentName);
            while (b != null) {
                this.a.add(size, b);
                b = b.b(this.f0, b.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Intent o(int i) {
        return this.a.get(i);
    }

    public int p() {
        return this.a.size();
    }

    public void q() {
        s(null);
    }

    public void s(Bundle bundle) {
        if (!this.a.isEmpty()) {
            ArrayList<Intent> arrayList = this.a;
            Intent[] intentArr = (Intent[]) arrayList.toArray(new Intent[arrayList.size()]);
            intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
            if (m70.j(this.f0, intentArr, bundle)) {
                return;
            }
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.f0.startActivity(intent);
            return;
        }
        throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
    }
}
