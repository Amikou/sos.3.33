package defpackage;

/* compiled from: SimpleSQLiteQuery.java */
/* renamed from: pp3  reason: default package */
/* loaded from: classes.dex */
public final class pp3 implements vw3 {
    public final String a;
    public final Object[] f0;

    public pp3(String str, Object[] objArr) {
        this.a = str;
        this.f0 = objArr;
    }

    public static void c(uw3 uw3Var, int i, Object obj) {
        if (obj == null) {
            uw3Var.Y0(i);
        } else if (obj instanceof byte[]) {
            uw3Var.A0(i, (byte[]) obj);
        } else if (obj instanceof Float) {
            uw3Var.Y(i, ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            uw3Var.Y(i, ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            uw3Var.q0(i, ((Long) obj).longValue());
        } else if (obj instanceof Integer) {
            uw3Var.q0(i, ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            uw3Var.q0(i, ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            uw3Var.q0(i, ((Byte) obj).byteValue());
        } else if (obj instanceof String) {
            uw3Var.L(i, (String) obj);
        } else if (obj instanceof Boolean) {
            uw3Var.q0(i, ((Boolean) obj).booleanValue() ? 1L : 0L);
        } else {
            throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte, string");
        }
    }

    public static void d(uw3 uw3Var, Object[] objArr) {
        if (objArr == null) {
            return;
        }
        int length = objArr.length;
        int i = 0;
        while (i < length) {
            Object obj = objArr[i];
            i++;
            c(uw3Var, i, obj);
        }
    }

    @Override // defpackage.vw3
    public String a() {
        return this.a;
    }

    @Override // defpackage.vw3
    public void b(uw3 uw3Var) {
        d(uw3Var, this.f0);
    }

    public pp3(String str) {
        this(str, null);
    }
}
