package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: Clock.java */
/* renamed from: tz  reason: default package */
/* loaded from: classes.dex */
public interface tz {
    public static final tz a = new q24();

    long a();

    long b();

    pj1 c(Looper looper, Handler.Callback callback);

    void d();
}
