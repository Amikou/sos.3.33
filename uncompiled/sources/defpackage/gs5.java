package defpackage;

import com.google.android.gms.phenotype.zzi;
import java.util.Comparator;

/* renamed from: gs5  reason: default package */
/* loaded from: classes.dex */
public final class gs5 implements Comparator<zzi> {
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(zzi zziVar, zzi zziVar2) {
        zzi zziVar3 = zziVar;
        zzi zziVar4 = zziVar2;
        int i = zziVar3.l0;
        int i2 = zziVar4.l0;
        return i == i2 ? zziVar3.a.compareTo(zziVar4.a) : i - i2;
    }
}
