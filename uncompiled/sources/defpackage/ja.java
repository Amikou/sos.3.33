package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.i;
import com.google.crypto.tink.proto.j;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: AesEaxKeyManager.java */
/* renamed from: ja  reason: default package */
/* loaded from: classes2.dex */
public final class ja extends g<i> {

    /* compiled from: AesEaxKeyManager.java */
    /* renamed from: ja$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, i> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(i iVar) throws GeneralSecurityException {
            return new ia(iVar.H().toByteArray(), iVar.I().E());
        }
    }

    /* compiled from: AesEaxKeyManager.java */
    /* renamed from: ja$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<j, i> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public i a(j jVar) throws GeneralSecurityException {
            return i.K().s(ByteString.copyFrom(p33.c(jVar.D()))).t(jVar.E()).u(ja.this.j()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public j c(ByteString byteString) throws InvalidProtocolBufferException {
            return j.G(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(j jVar) throws GeneralSecurityException {
            ug4.a(jVar.D());
            if (jVar.E().E() != 12 && jVar.E().E() != 16) {
                throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
            }
        }
    }

    public ja() {
        super(i.class, new a(com.google.crypto.tink.a.class));
    }

    public static void l(boolean z) throws GeneralSecurityException {
        q.q(new ja(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesEaxKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, i> e() {
        return new b(j.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int j() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: k */
    public i g(ByteString byteString) throws InvalidProtocolBufferException {
        return i.L(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(i iVar) throws GeneralSecurityException {
        ug4.c(iVar.J(), j());
        ug4.a(iVar.H().size());
        if (iVar.I().E() != 12 && iVar.I().E() != 16) {
            throw new GeneralSecurityException("invalid IV size; acceptable values have 12 or 16 bytes");
        }
    }
}
