package defpackage;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import defpackage.k6;
import java.util.ArrayList;

/* compiled from: SupportActionModeWrapper.java */
/* renamed from: hw3  reason: default package */
/* loaded from: classes.dex */
public class hw3 extends ActionMode {
    public final Context a;
    public final k6 b;

    /* compiled from: SupportActionModeWrapper.java */
    /* renamed from: hw3$a */
    /* loaded from: classes.dex */
    public static class a implements k6.a {
        public final ActionMode.Callback a;
        public final Context b;
        public final ArrayList<hw3> c = new ArrayList<>();
        public final vo3<Menu, Menu> d = new vo3<>();

        public a(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.a = callback;
        }

        @Override // defpackage.k6.a
        public boolean a(k6 k6Var, MenuItem menuItem) {
            return this.a.onActionItemClicked(e(k6Var), new u72(this.b, (lw3) menuItem));
        }

        @Override // defpackage.k6.a
        public void b(k6 k6Var) {
            this.a.onDestroyActionMode(e(k6Var));
        }

        @Override // defpackage.k6.a
        public boolean c(k6 k6Var, Menu menu) {
            return this.a.onCreateActionMode(e(k6Var), f(menu));
        }

        @Override // defpackage.k6.a
        public boolean d(k6 k6Var, Menu menu) {
            return this.a.onPrepareActionMode(e(k6Var), f(menu));
        }

        public ActionMode e(k6 k6Var) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                hw3 hw3Var = this.c.get(i);
                if (hw3Var != null && hw3Var.b == k6Var) {
                    return hw3Var;
                }
            }
            hw3 hw3Var2 = new hw3(this.b, k6Var);
            this.c.add(hw3Var2);
            return hw3Var2;
        }

        public final Menu f(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 == null) {
                w72 w72Var = new w72(this.b, (jw3) menu);
                this.d.put(menu, w72Var);
                return w72Var;
            }
            return menu2;
        }
    }

    public hw3(Context context, k6 k6Var) {
        this.a = context;
        this.b = k6Var;
    }

    @Override // android.view.ActionMode
    public void finish() {
        this.b.c();
    }

    @Override // android.view.ActionMode
    public View getCustomView() {
        return this.b.d();
    }

    @Override // android.view.ActionMode
    public Menu getMenu() {
        return new w72(this.a, (jw3) this.b.e());
    }

    @Override // android.view.ActionMode
    public MenuInflater getMenuInflater() {
        return this.b.f();
    }

    @Override // android.view.ActionMode
    public CharSequence getSubtitle() {
        return this.b.g();
    }

    @Override // android.view.ActionMode
    public Object getTag() {
        return this.b.h();
    }

    @Override // android.view.ActionMode
    public CharSequence getTitle() {
        return this.b.i();
    }

    @Override // android.view.ActionMode
    public boolean getTitleOptionalHint() {
        return this.b.j();
    }

    @Override // android.view.ActionMode
    public void invalidate() {
        this.b.k();
    }

    @Override // android.view.ActionMode
    public boolean isTitleOptional() {
        return this.b.l();
    }

    @Override // android.view.ActionMode
    public void setCustomView(View view) {
        this.b.m(view);
    }

    @Override // android.view.ActionMode
    public void setSubtitle(CharSequence charSequence) {
        this.b.o(charSequence);
    }

    @Override // android.view.ActionMode
    public void setTag(Object obj) {
        this.b.p(obj);
    }

    @Override // android.view.ActionMode
    public void setTitle(CharSequence charSequence) {
        this.b.r(charSequence);
    }

    @Override // android.view.ActionMode
    public void setTitleOptionalHint(boolean z) {
        this.b.s(z);
    }

    @Override // android.view.ActionMode
    public void setSubtitle(int i) {
        this.b.n(i);
    }

    @Override // android.view.ActionMode
    public void setTitle(int i) {
        this.b.q(i);
    }
}
