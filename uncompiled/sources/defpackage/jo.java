package defpackage;

import androidx.constraintlayout.core.c;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.f;
import androidx.constraintlayout.core.widgets.g;
import androidx.constraintlayout.core.widgets.i;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;

/* compiled from: BasicMeasure.java */
/* renamed from: jo  reason: default package */
/* loaded from: classes.dex */
public class jo {
    public final ArrayList<ConstraintWidget> a = new ArrayList<>();
    public a b = new a();
    public d c;

    /* compiled from: BasicMeasure.java */
    /* renamed from: jo$a */
    /* loaded from: classes.dex */
    public static class a {
        public static int k = 0;
        public static int l = 1;
        public static int m = 2;
        public ConstraintWidget.DimensionBehaviour a;
        public ConstraintWidget.DimensionBehaviour b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public boolean h;
        public boolean i;
        public int j;
    }

    /* compiled from: BasicMeasure.java */
    /* renamed from: jo$b */
    /* loaded from: classes.dex */
    public interface b {
        void a();

        void b(ConstraintWidget constraintWidget, a aVar);
    }

    public jo(d dVar) {
        this.c = dVar;
    }

    public final boolean a(b bVar, ConstraintWidget constraintWidget, int i) {
        this.b.a = constraintWidget.C();
        this.b.b = constraintWidget.S();
        this.b.c = constraintWidget.V();
        this.b.d = constraintWidget.z();
        a aVar = this.b;
        aVar.i = false;
        aVar.j = i;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = aVar.a;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
        boolean z = dimensionBehaviour == dimensionBehaviour2;
        boolean z2 = aVar.b == dimensionBehaviour2;
        boolean z3 = z && constraintWidget.b0 > Utils.FLOAT_EPSILON;
        boolean z4 = z2 && constraintWidget.b0 > Utils.FLOAT_EPSILON;
        if (z3 && constraintWidget.u[0] == 4) {
            aVar.a = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (z4 && constraintWidget.u[1] == 4) {
            aVar.b = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        bVar.b(constraintWidget, aVar);
        constraintWidget.h1(this.b.e);
        constraintWidget.I0(this.b.f);
        constraintWidget.H0(this.b.h);
        constraintWidget.x0(this.b.g);
        a aVar2 = this.b;
        aVar2.j = a.k;
        return aVar2.i;
    }

    /* JADX WARN: Code restructure failed: missing block: B:52:0x008d, code lost:
        if (r8 != r9) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x0094, code lost:
        if (r5.b0 <= com.github.mikephil.charting.utils.Utils.FLOAT_EPSILON) goto L55;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void b(androidx.constraintlayout.core.widgets.d r13) {
        /*
            r12 = this;
            java.util.ArrayList<androidx.constraintlayout.core.widgets.ConstraintWidget> r0 = r13.P0
            int r0 = r0.size()
            r1 = 64
            boolean r1 = r13.Q1(r1)
            jo$b r2 = r13.F1()
            r3 = 0
            r4 = r3
        L12:
            if (r4 >= r0) goto La4
            java.util.ArrayList<androidx.constraintlayout.core.widgets.ConstraintWidget> r5 = r13.P0
            java.lang.Object r5 = r5.get(r4)
            androidx.constraintlayout.core.widgets.ConstraintWidget r5 = (androidx.constraintlayout.core.widgets.ConstraintWidget) r5
            boolean r6 = r5 instanceof androidx.constraintlayout.core.widgets.f
            if (r6 == 0) goto L22
            goto La0
        L22:
            boolean r6 = r5 instanceof androidx.constraintlayout.core.widgets.a
            if (r6 == 0) goto L28
            goto La0
        L28:
            boolean r6 = r5.k0()
            if (r6 == 0) goto L30
            goto La0
        L30:
            if (r1 == 0) goto L47
            androidx.constraintlayout.core.widgets.analyzer.c r6 = r5.d
            if (r6 == 0) goto L47
            androidx.constraintlayout.core.widgets.analyzer.d r7 = r5.e
            if (r7 == 0) goto L47
            androidx.constraintlayout.core.widgets.analyzer.a r6 = r6.e
            boolean r6 = r6.j
            if (r6 == 0) goto L47
            androidx.constraintlayout.core.widgets.analyzer.a r6 = r7.e
            boolean r6 = r6.j
            if (r6 == 0) goto L47
            goto La0
        L47:
            androidx.constraintlayout.core.widgets.ConstraintWidget$DimensionBehaviour r6 = r5.w(r3)
            r7 = 1
            androidx.constraintlayout.core.widgets.ConstraintWidget$DimensionBehaviour r8 = r5.w(r7)
            androidx.constraintlayout.core.widgets.ConstraintWidget$DimensionBehaviour r9 = androidx.constraintlayout.core.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r6 != r9) goto L60
            int r10 = r5.s
            if (r10 == r7) goto L60
            if (r8 != r9) goto L60
            int r10 = r5.t
            if (r10 == r7) goto L60
            r10 = r7
            goto L61
        L60:
            r10 = r3
        L61:
            if (r10 != 0) goto L97
            boolean r11 = r13.Q1(r7)
            if (r11 == 0) goto L97
            boolean r11 = r5 instanceof androidx.constraintlayout.core.widgets.i
            if (r11 != 0) goto L97
            if (r6 != r9) goto L7c
            int r11 = r5.s
            if (r11 != 0) goto L7c
            if (r8 == r9) goto L7c
            boolean r11 = r5.h0()
            if (r11 != 0) goto L7c
            r10 = r7
        L7c:
            if (r8 != r9) goto L8b
            int r11 = r5.t
            if (r11 != 0) goto L8b
            if (r6 == r9) goto L8b
            boolean r11 = r5.h0()
            if (r11 != 0) goto L8b
            r10 = r7
        L8b:
            if (r6 == r9) goto L8f
            if (r8 != r9) goto L97
        L8f:
            float r6 = r5.b0
            r8 = 0
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 <= 0) goto L97
            goto L98
        L97:
            r7 = r10
        L98:
            if (r7 == 0) goto L9b
            goto La0
        L9b:
            int r6 = defpackage.jo.a.k
            r12.a(r2, r5, r6)
        La0:
            int r4 = r4 + 1
            goto L12
        La4:
            r2.a()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.jo.b(androidx.constraintlayout.core.widgets.d):void");
    }

    public final void c(d dVar, String str, int i, int i2, int i3) {
        int K = dVar.K();
        int J = dVar.J();
        dVar.X0(0);
        dVar.W0(0);
        dVar.h1(i2);
        dVar.I0(i3);
        dVar.X0(K);
        dVar.W0(J);
        this.c.U1(i);
        this.c.p1();
    }

    public long d(d dVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        boolean z;
        int i10;
        d dVar2;
        int i11;
        boolean z2;
        int i12;
        int i13;
        boolean z3;
        jo joVar = this;
        b F1 = dVar.F1();
        int size = dVar.P0.size();
        int V = dVar.V();
        int z4 = dVar.z();
        boolean b2 = g.b(i, 128);
        boolean z5 = b2 || g.b(i, 64);
        if (z5) {
            for (int i14 = 0; i14 < size; i14++) {
                ConstraintWidget constraintWidget = dVar.P0.get(i14);
                ConstraintWidget.DimensionBehaviour C = constraintWidget.C();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                boolean z6 = (C == dimensionBehaviour) && (constraintWidget.S() == dimensionBehaviour) && constraintWidget.x() > Utils.FLOAT_EPSILON;
                if ((constraintWidget.h0() && z6) || ((constraintWidget.j0() && z6) || (constraintWidget instanceof i) || constraintWidget.h0() || constraintWidget.j0())) {
                    z5 = false;
                    break;
                }
            }
        }
        if (z5) {
            s82 s82Var = c.x;
        }
        boolean z7 = z5 & ((i4 == 1073741824 && i6 == 1073741824) || b2);
        int i15 = 2;
        if (z7) {
            int min = Math.min(dVar.I(), i5);
            int min2 = Math.min(dVar.H(), i7);
            if (i4 == 1073741824 && dVar.V() != min) {
                dVar.h1(min);
                dVar.J1();
            }
            if (i6 == 1073741824 && dVar.z() != min2) {
                dVar.I0(min2);
                dVar.J1();
            }
            if (i4 == 1073741824 && i6 == 1073741824) {
                z = dVar.C1(b2);
                i10 = 2;
            } else {
                boolean D1 = dVar.D1(b2);
                if (i4 == 1073741824) {
                    D1 &= dVar.E1(b2, 0);
                    i10 = 1;
                } else {
                    i10 = 0;
                }
                if (i6 == 1073741824) {
                    z = dVar.E1(b2, 1) & D1;
                    i10++;
                } else {
                    z = D1;
                }
            }
            if (z) {
                dVar.m1(i4 == 1073741824, i6 == 1073741824);
            }
        } else {
            z = false;
            i10 = 0;
        }
        if (z && i10 == 2) {
            return 0L;
        }
        int G1 = dVar.G1();
        if (size > 0) {
            b(dVar);
        }
        e(dVar);
        int size2 = joVar.a.size();
        if (size > 0) {
            c(dVar, "First pass", 0, V, z4);
        }
        if (size2 > 0) {
            ConstraintWidget.DimensionBehaviour C2 = dVar.C();
            ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            boolean z8 = C2 == dimensionBehaviour2;
            boolean z9 = dVar.S() == dimensionBehaviour2;
            int max = Math.max(dVar.V(), joVar.c.K());
            int max2 = Math.max(dVar.z(), joVar.c.J());
            int i16 = 0;
            boolean z10 = false;
            while (i16 < size2) {
                ConstraintWidget constraintWidget2 = joVar.a.get(i16);
                if (constraintWidget2 instanceof i) {
                    int V2 = constraintWidget2.V();
                    int z11 = constraintWidget2.z();
                    i13 = G1;
                    boolean a2 = joVar.a(F1, constraintWidget2, a.l) | z10;
                    int V3 = constraintWidget2.V();
                    int z12 = constraintWidget2.z();
                    if (V3 != V2) {
                        constraintWidget2.h1(V3);
                        if (z8 && constraintWidget2.O() > max) {
                            max = Math.max(max, constraintWidget2.O() + constraintWidget2.q(ConstraintAnchor.Type.RIGHT).f());
                        }
                        z3 = true;
                    } else {
                        z3 = a2;
                    }
                    if (z12 != z11) {
                        constraintWidget2.I0(z12);
                        if (z9 && constraintWidget2.t() > max2) {
                            max2 = Math.max(max2, constraintWidget2.t() + constraintWidget2.q(ConstraintAnchor.Type.BOTTOM).f());
                        }
                        z3 = true;
                    }
                    z10 = z3 | ((i) constraintWidget2).C1();
                } else {
                    i13 = G1;
                }
                i16++;
                G1 = i13;
                i15 = 2;
            }
            int i17 = G1;
            int i18 = i15;
            int i19 = 0;
            while (i19 < i18) {
                int i20 = 0;
                while (i20 < size2) {
                    ConstraintWidget constraintWidget3 = joVar.a.get(i20);
                    if (((constraintWidget3 instanceof lk1) && !(constraintWidget3 instanceof i)) || (constraintWidget3 instanceof f) || constraintWidget3.U() == 8 || ((z7 && constraintWidget3.d.e.j && constraintWidget3.e.e.j) || (constraintWidget3 instanceof i))) {
                        z2 = z7;
                        i12 = size2;
                    } else {
                        int V4 = constraintWidget3.V();
                        int z13 = constraintWidget3.z();
                        z2 = z7;
                        int r = constraintWidget3.r();
                        int i21 = a.l;
                        i12 = size2;
                        if (i19 == 1) {
                            i21 = a.m;
                        }
                        boolean a3 = joVar.a(F1, constraintWidget3, i21) | z10;
                        int V5 = constraintWidget3.V();
                        int z14 = constraintWidget3.z();
                        if (V5 != V4) {
                            constraintWidget3.h1(V5);
                            if (z8 && constraintWidget3.O() > max) {
                                max = Math.max(max, constraintWidget3.O() + constraintWidget3.q(ConstraintAnchor.Type.RIGHT).f());
                            }
                            a3 = true;
                        }
                        if (z14 != z13) {
                            constraintWidget3.I0(z14);
                            if (z9 && constraintWidget3.t() > max2) {
                                max2 = Math.max(max2, constraintWidget3.t() + constraintWidget3.q(ConstraintAnchor.Type.BOTTOM).f());
                            }
                            a3 = true;
                        }
                        z10 = (!constraintWidget3.Y() || r == constraintWidget3.r()) ? a3 : true;
                    }
                    i20++;
                    joVar = this;
                    z7 = z2;
                    size2 = i12;
                }
                boolean z15 = z7;
                int i22 = size2;
                if (!z10) {
                    break;
                }
                i19++;
                c(dVar, "intermediate pass", i19, V, z4);
                joVar = this;
                z7 = z15;
                size2 = i22;
                i18 = 2;
                z10 = false;
            }
            dVar2 = dVar;
            i11 = i17;
        } else {
            dVar2 = dVar;
            i11 = G1;
        }
        dVar2.T1(i11);
        return 0L;
    }

    public void e(d dVar) {
        this.a.clear();
        int size = dVar.P0.size();
        for (int i = 0; i < size; i++) {
            ConstraintWidget constraintWidget = dVar.P0.get(i);
            ConstraintWidget.DimensionBehaviour C = constraintWidget.C();
            ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            if (C == dimensionBehaviour || constraintWidget.S() == dimensionBehaviour) {
                this.a.add(constraintWidget);
            }
        }
        dVar.J1();
    }
}
