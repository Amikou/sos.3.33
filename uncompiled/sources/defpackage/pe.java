package defpackage;

import android.content.ContentResolver;
import android.os.Build;
import android.provider.Settings;

/* compiled from: AnimatorDurationScaleProvider.java */
/* renamed from: pe  reason: default package */
/* loaded from: classes2.dex */
public class pe {
    public static float a = 1.0f;

    public float a(ContentResolver contentResolver) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            return Settings.Global.getFloat(contentResolver, "animator_duration_scale", 1.0f);
        }
        if (i == 16) {
            return Settings.System.getFloat(contentResolver, "animator_duration_scale", 1.0f);
        }
        return a;
    }
}
