package defpackage;

import defpackage.pt0;

/* renamed from: hf3  reason: default package */
/* loaded from: classes2.dex */
public class hf3 extends pt0.c {
    public hf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public hf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public hf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        gf3 gf3Var = (gf3) this.c;
        if (gf3Var.i()) {
            return i.v();
        }
        gf3 gf3Var2 = (gf3) this.b;
        gf3 gf3Var3 = (gf3) this.d[0];
        int[] h = ed2.h();
        ff3.i(gf3Var.f, h);
        int[] h2 = ed2.h();
        ff3.i(h, h2);
        int[] h3 = ed2.h();
        ff3.i(gf3Var2.f, h3);
        ff3.h(ed2.b(h3, h3, h3), h3);
        ff3.d(h, gf3Var2.f, h);
        ff3.h(kd2.G(8, h, 2, 0), h);
        int[] h4 = ed2.h();
        ff3.h(kd2.H(8, h2, 3, 0, h4), h4);
        gf3 gf3Var4 = new gf3(h2);
        ff3.i(h3, gf3Var4.f);
        int[] iArr = gf3Var4.f;
        ff3.k(iArr, h, iArr);
        int[] iArr2 = gf3Var4.f;
        ff3.k(iArr2, h, iArr2);
        gf3 gf3Var5 = new gf3(h);
        ff3.k(h, gf3Var4.f, gf3Var5.f);
        int[] iArr3 = gf3Var5.f;
        ff3.d(iArr3, h3, iArr3);
        int[] iArr4 = gf3Var5.f;
        ff3.k(iArr4, h4, iArr4);
        gf3 gf3Var6 = new gf3(h3);
        ff3.l(gf3Var.f, gf3Var6.f);
        if (!gf3Var3.h()) {
            int[] iArr5 = gf3Var6.f;
            ff3.d(iArr5, gf3Var3.f, iArr5);
        }
        return new hf3(i, gf3Var4, gf3Var5, new ct0[]{gf3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        gf3 gf3Var = (gf3) this.b;
        gf3 gf3Var2 = (gf3) this.c;
        gf3 gf3Var3 = (gf3) pt0Var.q();
        gf3 gf3Var4 = (gf3) pt0Var.r();
        gf3 gf3Var5 = (gf3) this.d[0];
        gf3 gf3Var6 = (gf3) pt0Var.s(0);
        int[] j = ed2.j();
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        int[] h3 = ed2.h();
        boolean h4 = gf3Var5.h();
        if (h4) {
            iArr = gf3Var3.f;
            iArr2 = gf3Var4.f;
        } else {
            ff3.i(gf3Var5.f, h2);
            ff3.d(h2, gf3Var3.f, h);
            ff3.d(h2, gf3Var5.f, h2);
            ff3.d(h2, gf3Var4.f, h2);
            iArr = h;
            iArr2 = h2;
        }
        boolean h5 = gf3Var6.h();
        if (h5) {
            iArr3 = gf3Var.f;
            iArr4 = gf3Var2.f;
        } else {
            ff3.i(gf3Var6.f, h3);
            ff3.d(h3, gf3Var.f, j);
            ff3.d(h3, gf3Var6.f, h3);
            ff3.d(h3, gf3Var2.f, h3);
            iArr3 = j;
            iArr4 = h3;
        }
        int[] h6 = ed2.h();
        ff3.k(iArr3, iArr, h6);
        ff3.k(iArr4, iArr2, h);
        if (ed2.v(h6)) {
            return ed2.v(h) ? J() : i.v();
        }
        ff3.i(h6, h2);
        int[] h7 = ed2.h();
        ff3.d(h2, h6, h7);
        ff3.d(h2, iArr3, h2);
        ff3.f(h7, h7);
        ed2.y(iArr4, h7, j);
        ff3.h(ed2.b(h2, h2, h7), h7);
        gf3 gf3Var7 = new gf3(h3);
        ff3.i(h, gf3Var7.f);
        int[] iArr5 = gf3Var7.f;
        ff3.k(iArr5, h7, iArr5);
        gf3 gf3Var8 = new gf3(h7);
        ff3.k(h2, gf3Var7.f, gf3Var8.f);
        ff3.e(gf3Var8.f, h, j);
        ff3.g(j, gf3Var8.f);
        gf3 gf3Var9 = new gf3(h6);
        if (!h4) {
            int[] iArr6 = gf3Var9.f;
            ff3.d(iArr6, gf3Var5.f, iArr6);
        }
        if (!h5) {
            int[] iArr7 = gf3Var9.f;
            ff3.d(iArr7, gf3Var6.f, iArr7);
        }
        return new hf3(i, gf3Var7, gf3Var8, new ct0[]{gf3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new hf3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new hf3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
