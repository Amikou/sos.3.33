package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import defpackage.ul1;

/* compiled from: CustomTabsServiceConnection.java */
/* renamed from: tc0  reason: default package */
/* loaded from: classes.dex */
public abstract class tc0 implements ServiceConnection {
    public Context a;

    /* compiled from: CustomTabsServiceConnection.java */
    /* renamed from: tc0$a */
    /* loaded from: classes.dex */
    public class a extends androidx.browser.customtabs.a {
        public a(tc0 tc0Var, ul1 ul1Var, ComponentName componentName, Context context) {
            super(ul1Var, componentName, context);
        }
    }

    public abstract void a(ComponentName componentName, androidx.browser.customtabs.a aVar);

    public void b(Context context) {
        this.a = context;
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (this.a != null) {
            a(componentName, new a(this, ul1.a.b(iBinder), componentName, this.a));
            return;
        }
        throw new IllegalStateException("Custom Tabs Service connected before an applicationcontext has been provided.");
    }
}
