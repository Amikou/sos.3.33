package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: jv5  reason: default package */
/* loaded from: classes.dex */
public abstract class jv5 extends hv5 {
    public boolean c;

    public jv5(fw5 fw5Var) {
        super(fw5Var);
        this.b.o();
    }

    public final boolean f() {
        return this.c;
    }

    public final void g() {
        if (!f()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public abstract boolean h();

    public final void i() {
        if (!this.c) {
            h();
            this.b.p();
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
