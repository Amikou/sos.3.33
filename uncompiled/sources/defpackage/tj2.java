package defpackage;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import com.onesignal.OneSignal;
import com.onesignal.e;
import com.onesignal.k;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSInAppMessagePreviewHandler.kt */
/* renamed from: tj2  reason: default package */
/* loaded from: classes2.dex */
public final class tj2 {
    public static final tj2 a = new tj2();

    public static final String a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        fs1.f(jSONObject, "payload");
        try {
            JSONObject b = k.b(jSONObject);
            fs1.e(b, "NotificationBundleProces…CustomJSONObject(payload)");
            if (b.has("a") && (optJSONObject = b.optJSONObject("a")) != null && optJSONObject.has("os_in_app_message_preview_id")) {
                return optJSONObject.optString("os_in_app_message_preview_id");
            }
            return null;
        } catch (JSONException unused) {
            return null;
        }
    }

    public static final boolean b(Activity activity, JSONObject jSONObject) {
        fs1.f(activity, "activity");
        fs1.f(jSONObject, "jsonData");
        String a2 = a(jSONObject);
        if (a2 != null) {
            OneSignal.e1(activity, new JSONArray().put(jSONObject));
            OneSignal.c0().G(a2);
            return true;
        }
        return false;
    }

    public static final boolean c(Context context, Bundle bundle) {
        JSONObject a2 = k.a(bundle);
        fs1.e(a2, "NotificationBundleProces…undleAsJSONObject(bundle)");
        String a3 = a(a2);
        if (a3 != null) {
            if (OneSignal.N0()) {
                OneSignal.c0().G(a3);
                return true;
            } else if (a.d()) {
                e.m(new zj2(context, a2));
                return true;
            } else {
                return true;
            }
        }
        return false;
    }

    public final boolean d() {
        return Build.VERSION.SDK_INT > 18;
    }
}
