package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;

/* compiled from: WakeLockManager.java */
/* renamed from: pl4  reason: default package */
/* loaded from: classes.dex */
public final class pl4 {
    public final PowerManager a;
    public PowerManager.WakeLock b;
    public boolean c;
    public boolean d;

    public pl4(Context context) {
        this.a = (PowerManager) context.getApplicationContext().getSystemService("power");
    }

    public void a(boolean z) {
        if (z && this.b == null) {
            PowerManager powerManager = this.a;
            if (powerManager == null) {
                p12.i("WakeLockManager", "PowerManager is null, therefore not creating the WakeLock.");
                return;
            }
            PowerManager.WakeLock newWakeLock = powerManager.newWakeLock(1, "ExoPlayer:WakeLockManager");
            this.b = newWakeLock;
            newWakeLock.setReferenceCounted(false);
        }
        this.c = z;
        c();
    }

    public void b(boolean z) {
        this.d = z;
        c();
    }

    @SuppressLint({"WakelockTimeout"})
    public final void c() {
        PowerManager.WakeLock wakeLock = this.b;
        if (wakeLock == null) {
            return;
        }
        if (this.c && this.d) {
            wakeLock.acquire();
        } else {
            wakeLock.release();
        }
    }
}
