package defpackage;

import androidx.paging.LoadType;

/* renamed from: w61  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class w61 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[LoadType.values().length];
        a = iArr;
        LoadType loadType = LoadType.PREPEND;
        iArr[loadType.ordinal()] = 1;
        LoadType loadType2 = LoadType.APPEND;
        iArr[loadType2.ordinal()] = 2;
        int[] iArr2 = new int[LoadType.values().length];
        b = iArr2;
        iArr2[LoadType.REFRESH.ordinal()] = 1;
        iArr2[loadType.ordinal()] = 2;
        iArr2[loadType2.ordinal()] = 3;
    }
}
