package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import java.util.Calendar;

/* compiled from: TwilightManager.java */
/* renamed from: sc4  reason: default package */
/* loaded from: classes.dex */
public class sc4 {
    public static sc4 d;
    public final Context a;
    public final LocationManager b;
    public final a c = new a();

    /* compiled from: TwilightManager.java */
    /* renamed from: sc4$a */
    /* loaded from: classes.dex */
    public static class a {
        public boolean a;
        public long b;
    }

    public sc4(Context context, LocationManager locationManager) {
        this.a = context;
        this.b = locationManager;
    }

    public static sc4 a(Context context) {
        if (d == null) {
            Context applicationContext = context.getApplicationContext();
            d = new sc4(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
        }
        return d;
    }

    @SuppressLint({"MissingPermission"})
    public final Location b() {
        Location c = kq2.c(this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? c("network") : null;
        Location c2 = kq2.c(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0 ? c("gps") : null;
        return (c2 == null || c == null) ? c2 != null ? c2 : c : c2.getTime() > c.getTime() ? c2 : c;
    }

    public final Location c(String str) {
        try {
            if (this.b.isProviderEnabled(str)) {
                return this.b.getLastKnownLocation(str);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public boolean d() {
        a aVar = this.c;
        if (e()) {
            return aVar.a;
        }
        Location b = b();
        if (b != null) {
            f(b);
            return aVar.a;
        }
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    public final boolean e() {
        return this.c.b > System.currentTimeMillis();
    }

    public final void f(Location location) {
        long j;
        a aVar = this.c;
        long currentTimeMillis = System.currentTimeMillis();
        rc4 b = rc4.b();
        b.a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        b.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = b.c == 1;
        long j2 = b.b;
        long j3 = b.a;
        b.a(currentTimeMillis + 86400000, location.getLatitude(), location.getLongitude());
        long j4 = b.b;
        if (j2 == -1 || j3 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            j = (currentTimeMillis > j3 ? j4 + 0 : currentTimeMillis > j2 ? j3 + 0 : j2 + 0) + 60000;
        }
        aVar.a = z;
        aVar.b = j;
    }
}
