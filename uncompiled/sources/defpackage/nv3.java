package defpackage;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Queue;
import org.slf4j.helpers.NOPLogger;

/* compiled from: SubstituteLogger.java */
/* renamed from: nv3  reason: default package */
/* loaded from: classes2.dex */
public class nv3 implements x12 {
    public final String a;
    public volatile x12 f0;
    public Boolean g0;
    public Method h0;
    public cy0 i0;
    public Queue<pv3> j0;
    public final boolean k0;

    public nv3(String str, Queue<pv3> queue, boolean z) {
        this.a = str;
        this.j0 = queue;
        this.k0 = z;
    }

    public x12 a() {
        if (this.f0 != null) {
            return this.f0;
        }
        if (this.k0) {
            return NOPLogger.NOP_LOGGER;
        }
        return b();
    }

    public final x12 b() {
        if (this.i0 == null) {
            this.i0 = new cy0(this, this.j0);
        }
        return this.i0;
    }

    public boolean c() {
        Boolean bool = this.g0;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            this.h0 = this.f0.getClass().getMethod("log", b22.class);
            this.g0 = Boolean.TRUE;
        } catch (NoSuchMethodException unused) {
            this.g0 = Boolean.FALSE;
        }
        return this.g0.booleanValue();
    }

    public boolean d() {
        return this.f0 instanceof NOPLogger;
    }

    @Override // defpackage.x12
    public void debug(String str) {
        a().debug(str);
    }

    public boolean e() {
        return this.f0 == null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && nv3.class == obj.getClass() && this.a.equals(((nv3) obj).a);
    }

    @Override // defpackage.x12
    public void error(String str) {
        a().error(str);
    }

    public void f(b22 b22Var) {
        if (c()) {
            try {
                this.h0.invoke(this.f0, b22Var);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException unused) {
            }
        }
    }

    public void g(x12 x12Var) {
        this.f0 = x12Var;
    }

    @Override // defpackage.x12
    public String getName() {
        return this.a;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // defpackage.x12
    public void info(String str) {
        a().info(str);
    }

    @Override // defpackage.x12
    public boolean isDebugEnabled() {
        return a().isDebugEnabled();
    }

    @Override // defpackage.x12
    public void warn(String str) {
        a().warn(str);
    }

    @Override // defpackage.x12
    public void error(String str, Throwable th) {
        a().error(str, th);
    }

    @Override // defpackage.x12
    public void info(String str, Object obj) {
        a().info(str, obj);
    }
}
