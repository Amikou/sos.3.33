package defpackage;

import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import androidx.room.n;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.Callable;

/* compiled from: InvalidationLiveDataContainer.java */
/* renamed from: js1  reason: default package */
/* loaded from: classes.dex */
public class js1 {
    public final Set<LiveData> a = Collections.newSetFromMap(new IdentityHashMap());
    public final RoomDatabase b;

    public js1(RoomDatabase roomDatabase) {
        this.b = roomDatabase;
    }

    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return new n(this.b, this, z, callable, strArr);
    }

    public void b(LiveData liveData) {
        this.a.add(liveData);
    }

    public void c(LiveData liveData) {
        this.a.remove(liveData);
    }
}
