package defpackage;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.zendesk.logger.Logger;
import java.io.IOException;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/* compiled from: ZendeskDateTypeAdapter.java */
/* renamed from: ss4  reason: default package */
/* loaded from: classes2.dex */
public class ss4 extends TypeAdapter<Date> {
    public static final TimeZone a = TimeZone.getTimeZone("UTC");

    public static int c(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return i;
            }
            i++;
        }
        return str.length();
    }

    public final boolean a(String str, int i, char c) {
        return i < str.length() && str.charAt(i) == c;
    }

    public final String b(Date date) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(a, Locale.US);
        gregorianCalendar.setTime(date);
        StringBuilder sb = new StringBuilder(21);
        d(sb, gregorianCalendar.get(1), 4);
        sb.append('-');
        d(sb, gregorianCalendar.get(2) + 1, 2);
        sb.append('-');
        d(sb, gregorianCalendar.get(5), 2);
        sb.append('T');
        d(sb, gregorianCalendar.get(11), 2);
        sb.append(':');
        d(sb, gregorianCalendar.get(12), 2);
        sb.append(':');
        d(sb, gregorianCalendar.get(13), 2);
        sb.append('Z');
        return sb.toString();
    }

    public final void d(StringBuilder sb, int i, int i2) {
        String num = Integer.toString(i);
        for (int length = i2 - num.length(); length > 0; length--) {
            sb.append('0');
        }
        sb.append(num);
    }

    /* JADX WARN: Removed duplicated region for block: B:50:0x00d1 A[Catch: IndexOutOfBoundsException -> 0x012c, TryCatch #0 {IndexOutOfBoundsException -> 0x012c, blocks: (B:3:0x0008, B:5:0x001a, B:6:0x001c, B:8:0x0028, B:9:0x002a, B:11:0x0039, B:13:0x003f, B:17:0x0052, B:19:0x0062, B:20:0x0064, B:22:0x0070, B:23:0x0072, B:25:0x0078, B:30:0x0084, B:35:0x0093, B:37:0x009b, B:48:0x00cb, B:50:0x00d1, B:52:0x00d7, B:54:0x010a, B:55:0x0123, B:56:0x0124, B:57:0x012b, B:41:0x00b3, B:42:0x00b6), top: B:69:0x0008 }] */
    /* JADX WARN: Removed duplicated region for block: B:56:0x0124 A[Catch: IndexOutOfBoundsException -> 0x012c, TryCatch #0 {IndexOutOfBoundsException -> 0x012c, blocks: (B:3:0x0008, B:5:0x001a, B:6:0x001c, B:8:0x0028, B:9:0x002a, B:11:0x0039, B:13:0x003f, B:17:0x0052, B:19:0x0062, B:20:0x0064, B:22:0x0070, B:23:0x0072, B:25:0x0078, B:30:0x0084, B:35:0x0093, B:37:0x009b, B:48:0x00cb, B:50:0x00d1, B:52:0x00d7, B:54:0x010a, B:55:0x0123, B:56:0x0124, B:57:0x012b, B:41:0x00b3, B:42:0x00b6), top: B:69:0x0008 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.Date e(java.lang.String r19, java.text.ParsePosition r20) throws java.text.ParseException {
        /*
            Method dump skipped, instructions count: 405
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ss4.e(java.lang.String, java.text.ParsePosition):java.util.Date");
    }

    public final int f(String str, int i, int i2) throws NumberFormatException {
        int i3;
        int i4;
        if (i < 0 || i2 > str.length() || i > i2) {
            throw new NumberFormatException(str);
        }
        if (i < i2) {
            i4 = i + 1;
            int digit = Character.digit(str.charAt(i), 10);
            if (digit < 0) {
                throw new NumberFormatException("Invalid number: " + str.substring(i, i2));
            }
            i3 = -digit;
        } else {
            i3 = 0;
            i4 = i;
        }
        while (i4 < i2) {
            int i5 = i4 + 1;
            int digit2 = Character.digit(str.charAt(i4), 10);
            if (digit2 < 0) {
                throw new NumberFormatException("Invalid number: " + str.substring(i, i2));
            }
            i3 = (i3 * 10) - digit2;
            i4 = i5;
        }
        return -i3;
    }

    @Override // com.google.gson.TypeAdapter
    public Date read(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        String nextString = jsonReader.nextString();
        try {
            return e(nextString, new ParsePosition(0));
        } catch (ParseException e) {
            Logger.d("ZendeskDateTypeAdapter", String.format(Locale.US, "Failed to parse Date from: %s", nextString), e, new Object[0]);
            return null;
        }
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Date date) throws IOException {
        if (date == null) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(b(date));
        }
    }
}
