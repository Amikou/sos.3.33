package defpackage;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;
import java.util.Iterator;

/* compiled from: WormAnimation.java */
/* renamed from: dr4  reason: default package */
/* loaded from: classes2.dex */
public class dr4 extends nm<AnimatorSet> {
    public int d;
    public int e;
    public int f;
    public boolean g;
    public int h;
    public int i;
    public er4 j;

    /* compiled from: WormAnimation.java */
    /* renamed from: dr4$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ er4 a;
        public final /* synthetic */ boolean f0;

        public a(er4 er4Var, boolean z) {
            this.a = er4Var;
            this.f0 = z;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            dr4.this.l(this.a, valueAnimator, this.f0);
        }
    }

    /* compiled from: WormAnimation.java */
    /* renamed from: dr4$b */
    /* loaded from: classes2.dex */
    public class b {
        public final int a;
        public final int b;
        public final int c;
        public final int d;

        public b(dr4 dr4Var, int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    public dr4(xg4.a aVar) {
        super(aVar);
        this.j = new er4();
    }

    @Override // defpackage.nm
    /* renamed from: g */
    public AnimatorSet a() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        return animatorSet;
    }

    public b h(boolean z) {
        int i;
        int i2;
        int i3;
        int i4;
        if (z) {
            int i5 = this.d;
            int i6 = this.f;
            i = i5 + i6;
            int i7 = this.e;
            i2 = i7 + i6;
            i3 = i5 - i6;
            i4 = i7 - i6;
        } else {
            int i8 = this.d;
            int i9 = this.f;
            i = i8 - i9;
            int i10 = this.e;
            i2 = i10 - i9;
            i3 = i8 + i9;
            i4 = i10 + i9;
        }
        return new b(this, i, i2, i3, i4);
    }

    public ValueAnimator i(int i, int i2, long j, boolean z, er4 er4Var) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.setInterpolator(new AccelerateDecelerateInterpolator());
        ofInt.setDuration(j);
        ofInt.addUpdateListener(new a(er4Var, z));
        return ofInt;
    }

    public dr4 j(long j) {
        super.b(j);
        return this;
    }

    public boolean k(int i, int i2, int i3, boolean z) {
        return (this.d == i && this.e == i2 && this.f == i3 && this.g == z) ? false : true;
    }

    public final void l(er4 er4Var, ValueAnimator valueAnimator, boolean z) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        if (this.g) {
            if (!z) {
                er4Var.c(intValue);
            } else {
                er4Var.d(intValue);
            }
        } else if (!z) {
            er4Var.d(intValue);
        } else {
            er4Var.c(intValue);
        }
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(er4Var);
        }
    }

    @Override // defpackage.nm
    /* renamed from: m */
    public dr4 d(float f) {
        T t = this.c;
        if (t == 0) {
            return this;
        }
        long j = f * ((float) this.a);
        Iterator<Animator> it = ((AnimatorSet) t).getChildAnimations().iterator();
        while (it.hasNext()) {
            ValueAnimator valueAnimator = (ValueAnimator) it.next();
            long duration = valueAnimator.getDuration();
            if (j <= duration) {
                duration = j;
            }
            valueAnimator.setCurrentPlayTime(duration);
            j -= duration;
        }
        return this;
    }

    public dr4 n(int i, int i2, int i3, boolean z) {
        if (k(i, i2, i3, z)) {
            this.c = a();
            this.d = i;
            this.e = i2;
            this.f = i3;
            this.g = z;
            int i4 = i - i3;
            this.h = i4;
            this.i = i + i3;
            this.j.d(i4);
            this.j.c(this.i);
            b h = h(z);
            long j = this.a / 2;
            ((AnimatorSet) this.c).playSequentially(i(h.a, h.b, j, false, this.j), i(h.c, h.d, j, true, this.j));
        }
        return this;
    }
}
