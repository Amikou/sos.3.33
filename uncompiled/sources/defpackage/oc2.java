package defpackage;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.SwapAutoSlippage;

/* compiled from: NFTNetworkFeeInfo.kt */
/* renamed from: oc2  reason: default package */
/* loaded from: classes2.dex */
public final class oc2 extends sn0 {
    public tn0 u0;

    /* compiled from: NFTNetworkFeeInfo.kt */
    /* renamed from: oc2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public static final void w(oc2 oc2Var, View view) {
        fs1.f(oc2Var, "this$0");
        oc2Var.h();
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_nft_network_infomation, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        tn0 a2 = tn0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.a.setOnClickListener(new View.OnClickListener() { // from class: nc2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                oc2.w(oc2.this, view2);
            }
        });
    }

    public final void x(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, SwapAutoSlippage.class.getCanonicalName());
    }
}
