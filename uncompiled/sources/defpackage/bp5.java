package defpackage;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
@TargetApi(14)
/* renamed from: bp5  reason: default package */
/* loaded from: classes.dex */
public final class bp5 implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ dp5 a;

    public /* synthetic */ bp5(dp5 dp5Var, un5 un5Var) {
        this.a = dp5Var;
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        ck5 ck5Var;
        try {
            try {
                this.a.a.w().v().a("onActivityCreated");
                Intent intent = activity.getIntent();
                if (intent == null) {
                    ck5Var = this.a.a;
                } else {
                    Uri data = intent.getData();
                    if (data != null && data.isHierarchical()) {
                        this.a.a.G();
                        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
                        boolean z = true;
                        String str = true != ("android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra)) ? "auto" : "gs";
                        String queryParameter = data.getQueryParameter("referrer");
                        if (bundle != null) {
                            z = false;
                        }
                        this.a.a.q().p(new yo5(this, z, data, str, queryParameter));
                        ck5Var = this.a.a;
                    }
                    ck5Var = this.a.a;
                }
            } catch (RuntimeException e) {
                this.a.a.w().l().b("Throwable caught in onActivityCreated", e);
                ck5Var = this.a.a;
            }
            ck5Var.Q().z(activity, bundle);
        } catch (Throwable th) {
            this.a.a.Q().z(activity, bundle);
            throw th;
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityDestroyed(Activity activity) {
        this.a.a.Q().D(activity);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityPaused(Activity activity) {
        this.a.a.Q().B(activity);
        qu5 C = this.a.a.C();
        C.a.q().p(new yt5(C, C.a.a().b()));
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityResumed(Activity activity) {
        qu5 C = this.a.a.C();
        C.a.q().p(new vt5(C, C.a.a().b()));
        this.a.a.Q().A(activity);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.a.a.Q().C(activity, bundle);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStarted(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public final void onActivityStopped(Activity activity) {
    }
}
