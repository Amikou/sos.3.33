package defpackage;

import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: ai5  reason: default package */
/* loaded from: classes.dex */
public abstract class ai5 extends vc5 {
    public static final WeakReference<byte[]> c = new WeakReference<>(null);
    public WeakReference<byte[]> b;

    public ai5(byte[] bArr) {
        super(bArr);
        this.b = c;
    }

    @Override // defpackage.vc5
    public final byte[] G1() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.b.get();
            if (bArr == null) {
                bArr = I1();
                this.b = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    public abstract byte[] I1();
}
