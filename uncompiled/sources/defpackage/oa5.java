package defpackage;

import android.os.IBinder;

/* renamed from: oa5  reason: default package */
/* loaded from: classes.dex */
public final class oa5 extends m35 implements d75 {
    public oa5(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.phenotype.internal.IPhenotypeService");
    }
}
