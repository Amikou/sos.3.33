package defpackage;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/* compiled from: ListenableFuture.java */
/* renamed from: l02  reason: default package */
/* loaded from: classes2.dex */
public interface l02<V> extends Future<V> {
    void d(Runnable runnable, Executor executor);
}
