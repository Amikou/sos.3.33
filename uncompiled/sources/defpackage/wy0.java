package defpackage;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: ExecutorSupplier.java */
/* renamed from: wy0  reason: default package */
/* loaded from: classes.dex */
public interface wy0 {
    Executor a();

    Executor b();

    Executor c();

    Executor d();

    Executor e();

    Executor f();

    ScheduledExecutorService g();
}
