package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

/* compiled from: RootDrawable.java */
/* renamed from: l93  reason: default package */
/* loaded from: classes.dex */
public class l93 extends d91 implements yk4 {
    public Drawable h0;
    public zk4 i0;

    public l93(Drawable drawable) {
        super(drawable);
        this.h0 = null;
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    @SuppressLint({"WrongCall"})
    public void draw(Canvas canvas) {
        if (isVisible()) {
            zk4 zk4Var = this.i0;
            if (zk4Var != null) {
                zk4Var.a();
            }
            super.draw(canvas);
            Drawable drawable = this.h0;
            if (drawable != null) {
                drawable.setBounds(getBounds());
                this.h0.draw(canvas);
            }
        }
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return -1;
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return -1;
    }

    @Override // defpackage.yk4
    public void k(zk4 zk4Var) {
        this.i0 = zk4Var;
    }

    public void p(Drawable drawable) {
        this.h0 = drawable;
        invalidateSelf();
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        zk4 zk4Var = this.i0;
        if (zk4Var != null) {
            zk4Var.b(z);
        }
        return super.setVisible(z, z2);
    }
}
