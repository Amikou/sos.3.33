package defpackage;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.onesignal.influence.domain.OSInfluenceChannel;

/* compiled from: OSOutcomeTableProvider.kt */
/* renamed from: rk2  reason: default package */
/* loaded from: classes2.dex */
public final class rk2 {
    public final void a(SQLiteDatabase sQLiteDatabase) {
        fs1.f(sQLiteDatabase, "db");
        String str = "_id,name,notification_id";
        String str2 = "_id,name,channel_influence_id";
        try {
            try {
                sQLiteDatabase.execSQL("BEGIN TRANSACTION;");
                sQLiteDatabase.execSQL("CREATE TABLE cached_unique_outcome (_id INTEGER PRIMARY KEY,channel_influence_id TEXT,channel_type TEXT,name TEXT);");
                sQLiteDatabase.execSQL("INSERT INTO cached_unique_outcome(" + str2 + ") SELECT " + str + " FROM cached_unique_outcome_notification;");
                StringBuilder sb = new StringBuilder();
                sb.append("UPDATE cached_unique_outcome SET channel_type = '");
                sb.append(OSInfluenceChannel.NOTIFICATION.toString());
                sb.append("';");
                sQLiteDatabase.execSQL(sb.toString());
                sQLiteDatabase.execSQL("DROP TABLE cached_unique_outcome_notification;");
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        } finally {
            sQLiteDatabase.execSQL("COMMIT;");
        }
    }

    public final void b(SQLiteDatabase sQLiteDatabase) {
        fs1.f(sQLiteDatabase, "db");
        try {
            try {
                sQLiteDatabase.execSQL("BEGIN TRANSACTION;");
                sQLiteDatabase.execSQL("CREATE TEMPORARY TABLE outcome_backup(_id,session,notification_ids,name,timestamp);");
                sQLiteDatabase.execSQL("INSERT INTO outcome_backup SELECT _id,session,notification_ids,name,timestamp FROM outcome;");
                sQLiteDatabase.execSQL("DROP TABLE outcome;");
                sQLiteDatabase.execSQL("CREATE TABLE outcome (_id INTEGER PRIMARY KEY,session TEXT,notification_ids TEXT,name TEXT,timestamp TIMESTAMP,weight FLOAT);");
                sQLiteDatabase.execSQL("INSERT INTO outcome (_id,session,notification_ids,name,timestamp, weight) SELECT _id,session,notification_ids,name,timestamp, 0 FROM outcome_backup;");
                sQLiteDatabase.execSQL("DROP TABLE outcome_backup;");
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        } finally {
            sQLiteDatabase.execSQL("COMMIT;");
        }
    }

    public final void c(SQLiteDatabase sQLiteDatabase) {
        fs1.f(sQLiteDatabase, "db");
        String str = "_id,name,timestamp,notification_ids,weight,session";
        String str2 = "_id,name,timestamp,notification_ids,weight,notification_influence_type";
        try {
            try {
                sQLiteDatabase.execSQL("BEGIN TRANSACTION;");
                sQLiteDatabase.execSQL("ALTER TABLE outcome RENAME TO outcome_aux;");
                sQLiteDatabase.execSQL("CREATE TABLE outcome (_id INTEGER PRIMARY KEY,notification_influence_type TEXT,iam_influence_type TEXT,notification_ids TEXT,iam_ids TEXT,name TEXT,timestamp TIMESTAMP,weight FLOAT);");
                sQLiteDatabase.execSQL("INSERT INTO outcome(" + str2 + ") SELECT " + str + " FROM outcome_aux;");
                StringBuilder sb = new StringBuilder();
                sb.append("DROP TABLE ");
                sb.append("outcome_aux");
                sb.append(';');
                sQLiteDatabase.execSQL(sb.toString());
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        } finally {
            sQLiteDatabase.execSQL("COMMIT;");
        }
    }
}
