package defpackage;

import com.google.protobuf.j0;

/* renamed from: ae5  reason: default package */
/* loaded from: classes.dex */
public final class ae5 {
    public static final wd5 a = c();
    public static final wd5 b = new yd5();

    public static wd5 a() {
        return a;
    }

    public static wd5 b() {
        return b;
    }

    public static wd5 c() {
        try {
            return (wd5) j0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
