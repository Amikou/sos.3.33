package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: PausingDispatcher.kt */
/* renamed from: dq2  reason: default package */
/* loaded from: classes.dex */
public final class dq2 extends CoroutineDispatcher {
    public final mp0 f0 = new mp0();

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        fs1.f(coroutineContext, "context");
        fs1.f(runnable, "block");
        this.f0.c(coroutineContext, runnable);
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public boolean j(CoroutineContext coroutineContext) {
        fs1.f(coroutineContext, "context");
        if (tp0.c().l().j(coroutineContext)) {
            return true;
        }
        return !this.f0.b();
    }
}
