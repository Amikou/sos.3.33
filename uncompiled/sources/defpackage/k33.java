package defpackage;

import java.security.spec.KeySpec;

/* renamed from: k33  reason: default package */
/* loaded from: classes2.dex */
public class k33 implements KeySpec {
    public short[][] a;
    public short[] f0;
    public short[][] g0;
    public short[] h0;
    public int[] i0;
    public ny1[] j0;

    public k33(short[][] sArr, short[] sArr2, short[][] sArr3, short[] sArr4, int[] iArr, ny1[] ny1VarArr) {
        this.a = sArr;
        this.f0 = sArr2;
        this.g0 = sArr3;
        this.h0 = sArr4;
        this.i0 = iArr;
        this.j0 = ny1VarArr;
    }

    public short[] a() {
        return this.f0;
    }

    public short[] b() {
        return this.h0;
    }

    public short[][] c() {
        return this.a;
    }

    public short[][] d() {
        return this.g0;
    }

    public ny1[] e() {
        return this.j0;
    }

    public int[] f() {
        return this.i0;
    }
}
