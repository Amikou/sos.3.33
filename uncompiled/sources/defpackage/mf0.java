package defpackage;

import androidx.media3.common.util.b;
import defpackage.gb;
import java.util.Arrays;

/* compiled from: DefaultAllocator.java */
/* renamed from: mf0  reason: default package */
/* loaded from: classes.dex */
public final class mf0 implements gb {
    public final boolean a;
    public final int b;
    public final byte[] c;
    public int d;
    public int e;
    public int f;
    public fb[] g;

    public mf0(boolean z, int i) {
        this(z, i, 0);
    }

    @Override // defpackage.gb
    public synchronized void a(gb.a aVar) {
        while (aVar != null) {
            fb[] fbVarArr = this.g;
            int i = this.f;
            this.f = i + 1;
            fbVarArr[i] = aVar.a();
            this.e--;
            aVar = aVar.next();
        }
        notifyAll();
    }

    @Override // defpackage.gb
    public synchronized void b(fb fbVar) {
        fb[] fbVarArr = this.g;
        int i = this.f;
        this.f = i + 1;
        fbVarArr[i] = fbVar;
        this.e--;
        notifyAll();
    }

    @Override // defpackage.gb
    public synchronized fb c() {
        fb fbVar;
        this.e++;
        int i = this.f;
        if (i > 0) {
            fb[] fbVarArr = this.g;
            int i2 = i - 1;
            this.f = i2;
            fbVar = (fb) ii.e(fbVarArr[i2]);
            this.g[this.f] = null;
        } else {
            fbVar = new fb(new byte[this.b], 0);
            int i3 = this.e;
            fb[] fbVarArr2 = this.g;
            if (i3 > fbVarArr2.length) {
                this.g = (fb[]) Arrays.copyOf(fbVarArr2, fbVarArr2.length * 2);
            }
        }
        return fbVar;
    }

    @Override // defpackage.gb
    public synchronized void d() {
        int i = 0;
        int max = Math.max(0, b.l(this.d, this.b) - this.e);
        int i2 = this.f;
        if (max >= i2) {
            return;
        }
        if (this.c != null) {
            int i3 = i2 - 1;
            while (i <= i3) {
                fb fbVar = (fb) ii.e(this.g[i]);
                if (fbVar.a == this.c) {
                    i++;
                } else {
                    fb fbVar2 = (fb) ii.e(this.g[i3]);
                    if (fbVar2.a != this.c) {
                        i3--;
                    } else {
                        fb[] fbVarArr = this.g;
                        fbVarArr[i] = fbVar2;
                        fbVarArr[i3] = fbVar;
                        i3--;
                        i++;
                    }
                }
            }
            max = Math.max(max, i);
            if (max >= this.f) {
                return;
            }
        }
        Arrays.fill(this.g, max, this.f, (Object) null);
        this.f = max;
    }

    @Override // defpackage.gb
    public int e() {
        return this.b;
    }

    public synchronized int f() {
        return this.e * this.b;
    }

    public synchronized void g() {
        if (this.a) {
            h(0);
        }
    }

    public synchronized void h(int i) {
        boolean z = i < this.d;
        this.d = i;
        if (z) {
            d();
        }
    }

    public mf0(boolean z, int i, int i2) {
        ii.a(i > 0);
        ii.a(i2 >= 0);
        this.a = z;
        this.b = i;
        this.f = i2;
        this.g = new fb[i2 + 100];
        if (i2 > 0) {
            this.c = new byte[i2 * i];
            for (int i3 = 0; i3 < i2; i3++) {
                this.g[i3] = new fb(this.c, i3 * i);
            }
            return;
        }
        this.c = null;
    }
}
