package defpackage;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: EmptyTextWatcher.kt */
/* renamed from: vu0  reason: default package */
/* loaded from: classes2.dex */
public class vu0 implements TextWatcher {
    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
