package defpackage;

import com.google.android.gms.internal.vision.zzml;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: zn5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class zn5 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[zzml.values().length];
        a = iArr;
        try {
            iArr[zzml.zza.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            a[zzml.zzb.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            a[zzml.zzc.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        try {
            a[zzml.zzd.ordinal()] = 4;
        } catch (NoSuchFieldError unused4) {
        }
        try {
            a[zzml.zze.ordinal()] = 5;
        } catch (NoSuchFieldError unused5) {
        }
        try {
            a[zzml.zzm.ordinal()] = 6;
        } catch (NoSuchFieldError unused6) {
        }
        try {
            a[zzml.zzf.ordinal()] = 7;
        } catch (NoSuchFieldError unused7) {
        }
        try {
            a[zzml.zzp.ordinal()] = 8;
        } catch (NoSuchFieldError unused8) {
        }
        try {
            a[zzml.zzg.ordinal()] = 9;
        } catch (NoSuchFieldError unused9) {
        }
        try {
            a[zzml.zzo.ordinal()] = 10;
        } catch (NoSuchFieldError unused10) {
        }
        try {
            a[zzml.zzh.ordinal()] = 11;
        } catch (NoSuchFieldError unused11) {
        }
        try {
            a[zzml.zzq.ordinal()] = 12;
        } catch (NoSuchFieldError unused12) {
        }
        try {
            a[zzml.zzr.ordinal()] = 13;
        } catch (NoSuchFieldError unused13) {
        }
        try {
            a[zzml.zzn.ordinal()] = 14;
        } catch (NoSuchFieldError unused14) {
        }
        try {
            a[zzml.zzl.ordinal()] = 15;
        } catch (NoSuchFieldError unused15) {
        }
        try {
            a[zzml.zzi.ordinal()] = 16;
        } catch (NoSuchFieldError unused16) {
        }
        try {
            a[zzml.zzj.ordinal()] = 17;
        } catch (NoSuchFieldError unused17) {
        }
        try {
            a[zzml.zzk.ordinal()] = 18;
        } catch (NoSuchFieldError unused18) {
        }
    }
}
