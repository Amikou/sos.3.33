package defpackage;

import android.text.TextUtils;
import androidx.media3.common.util.b;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: WebvttCssParser.java */
/* renamed from: ro4  reason: default package */
/* loaded from: classes.dex */
public final class ro4 {
    public static final Pattern c = Pattern.compile("\\[voice=\"([^\"]*)\"\\]");
    public static final Pattern d = Pattern.compile("^((?:[0-9]*\\.)?[0-9]+)(px|em|%)$");
    public final op2 a = new op2();
    public final StringBuilder b = new StringBuilder();

    public static boolean b(op2 op2Var) {
        int e = op2Var.e();
        int f = op2Var.f();
        byte[] d2 = op2Var.d();
        if (e + 2 > f) {
            return false;
        }
        int i = e + 1;
        if (d2[e] != 47) {
            return false;
        }
        int i2 = i + 1;
        if (d2[i] != 42) {
            return false;
        }
        while (true) {
            int i3 = i2 + 1;
            if (i3 < f) {
                if (((char) d2[i2]) == '*' && ((char) d2[i3]) == '/') {
                    i2 = i3 + 1;
                    f = i2;
                } else {
                    i2 = i3;
                }
            } else {
                op2Var.Q(f - op2Var.e());
                return true;
            }
        }
    }

    public static boolean c(op2 op2Var) {
        char k = k(op2Var, op2Var.e());
        if (k == '\t' || k == '\n' || k == '\f' || k == '\r' || k == ' ') {
            op2Var.Q(1);
            return true;
        }
        return false;
    }

    public static void e(String str, so4 so4Var) {
        Matcher matcher = d.matcher(ei.e(str));
        if (!matcher.matches()) {
            p12.i("WebvttCssParser", "Invalid font-size: '" + str + "'.");
            return;
        }
        String str2 = (String) ii.e(matcher.group(2));
        str2.hashCode();
        char c2 = 65535;
        switch (str2.hashCode()) {
            case 37:
                if (str2.equals("%")) {
                    c2 = 0;
                    break;
                }
                break;
            case 3240:
                if (str2.equals("em")) {
                    c2 = 1;
                    break;
                }
                break;
            case 3592:
                if (str2.equals("px")) {
                    c2 = 2;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                so4Var.t(3);
                break;
            case 1:
                so4Var.t(2);
                break;
            case 2:
                so4Var.t(1);
                break;
            default:
                throw new IllegalStateException();
        }
        so4Var.s(Float.parseFloat((String) ii.e(matcher.group(1))));
    }

    public static String f(op2 op2Var, StringBuilder sb) {
        boolean z = false;
        sb.setLength(0);
        int e = op2Var.e();
        int f = op2Var.f();
        while (e < f && !z) {
            char c2 = (char) op2Var.d()[e];
            if ((c2 < 'A' || c2 > 'Z') && ((c2 < 'a' || c2 > 'z') && !((c2 >= '0' && c2 <= '9') || c2 == '#' || c2 == '-' || c2 == '.' || c2 == '_'))) {
                z = true;
            } else {
                e++;
                sb.append(c2);
            }
        }
        op2Var.Q(e - op2Var.e());
        return sb.toString();
    }

    public static String g(op2 op2Var, StringBuilder sb) {
        n(op2Var);
        if (op2Var.a() == 0) {
            return null;
        }
        String f = f(op2Var, sb);
        if ("".equals(f)) {
            return "" + ((char) op2Var.D());
        }
        return f;
    }

    public static String h(op2 op2Var, StringBuilder sb) {
        StringBuilder sb2 = new StringBuilder();
        boolean z = false;
        while (!z) {
            int e = op2Var.e();
            String g = g(op2Var, sb);
            if (g == null) {
                return null;
            }
            if (!"}".equals(g) && !";".equals(g)) {
                sb2.append(g);
            } else {
                op2Var.P(e);
                z = true;
            }
        }
        return sb2.toString();
    }

    public static String i(op2 op2Var, StringBuilder sb) {
        n(op2Var);
        if (op2Var.a() >= 5 && "::cue".equals(op2Var.A(5))) {
            int e = op2Var.e();
            String g = g(op2Var, sb);
            if (g == null) {
                return null;
            }
            if ("{".equals(g)) {
                op2Var.P(e);
                return "";
            }
            String l = "(".equals(g) ? l(op2Var) : null;
            if (")".equals(g(op2Var, sb))) {
                return l;
            }
            return null;
        }
        return null;
    }

    public static void j(op2 op2Var, so4 so4Var, StringBuilder sb) {
        n(op2Var);
        String f = f(op2Var, sb);
        if (!"".equals(f) && ":".equals(g(op2Var, sb))) {
            n(op2Var);
            String h = h(op2Var, sb);
            if (h == null || "".equals(h)) {
                return;
            }
            int e = op2Var.e();
            String g = g(op2Var, sb);
            if (!";".equals(g)) {
                if (!"}".equals(g)) {
                    return;
                }
                op2Var.P(e);
            }
            if ("color".equals(f)) {
                so4Var.q(x20.b(h));
            } else if ("background-color".equals(f)) {
                so4Var.n(x20.b(h));
            } else {
                boolean z = true;
                if ("ruby-position".equals(f)) {
                    if ("over".equals(h)) {
                        so4Var.v(1);
                    } else if ("under".equals(h)) {
                        so4Var.v(2);
                    }
                } else if ("text-combine-upright".equals(f)) {
                    if (!"all".equals(h) && !h.startsWith("digits")) {
                        z = false;
                    }
                    so4Var.p(z);
                } else if ("text-decoration".equals(f)) {
                    if ("underline".equals(h)) {
                        so4Var.A(true);
                    }
                } else if ("font-family".equals(f)) {
                    so4Var.r(h);
                } else if ("font-weight".equals(f)) {
                    if ("bold".equals(h)) {
                        so4Var.o(true);
                    }
                } else if ("font-style".equals(f)) {
                    if ("italic".equals(h)) {
                        so4Var.u(true);
                    }
                } else if ("font-size".equals(f)) {
                    e(h, so4Var);
                }
            }
        }
    }

    public static char k(op2 op2Var, int i) {
        return (char) op2Var.d()[i];
    }

    public static String l(op2 op2Var) {
        int e = op2Var.e();
        int f = op2Var.f();
        boolean z = false;
        while (e < f && !z) {
            int i = e + 1;
            z = ((char) op2Var.d()[e]) == ')';
            e = i;
        }
        return op2Var.A((e - 1) - op2Var.e()).trim();
    }

    public static void m(op2 op2Var) {
        do {
        } while (!TextUtils.isEmpty(op2Var.p()));
    }

    public static void n(op2 op2Var) {
        while (true) {
            for (boolean z = true; op2Var.a() > 0 && z; z = false) {
                if (!c(op2Var) && !b(op2Var)) {
                }
            }
            return;
        }
    }

    public final void a(so4 so4Var, String str) {
        if ("".equals(str)) {
            return;
        }
        int indexOf = str.indexOf(91);
        if (indexOf != -1) {
            Matcher matcher = c.matcher(str.substring(indexOf));
            if (matcher.matches()) {
                so4Var.z((String) ii.e(matcher.group(1)));
            }
            str = str.substring(0, indexOf);
        }
        String[] L0 = b.L0(str, "\\.");
        String str2 = L0[0];
        int indexOf2 = str2.indexOf(35);
        if (indexOf2 != -1) {
            so4Var.y(str2.substring(0, indexOf2));
            so4Var.x(str2.substring(indexOf2 + 1));
        } else {
            so4Var.y(str2);
        }
        if (L0.length > 1) {
            so4Var.w((String[]) b.D0(L0, 1, L0.length));
        }
    }

    public List<so4> d(op2 op2Var) {
        this.b.setLength(0);
        int e = op2Var.e();
        m(op2Var);
        this.a.N(op2Var.d(), op2Var.e());
        this.a.P(e);
        ArrayList arrayList = new ArrayList();
        while (true) {
            String i = i(this.a, this.b);
            if (i == null || !"{".equals(g(this.a, this.b))) {
                return arrayList;
            }
            so4 so4Var = new so4();
            a(so4Var, i);
            String str = null;
            boolean z = false;
            while (!z) {
                int e2 = this.a.e();
                String g = g(this.a, this.b);
                boolean z2 = g == null || "}".equals(g);
                if (!z2) {
                    this.a.P(e2);
                    j(this.a, so4Var, this.b);
                }
                str = g;
                z = z2;
            }
            if ("}".equals(str)) {
                arrayList.add(so4Var);
            }
        }
    }
}
