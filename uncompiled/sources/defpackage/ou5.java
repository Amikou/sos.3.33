package defpackage;

import android.app.ActivityManager;
import android.os.Bundle;
import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ou5  reason: default package */
/* loaded from: classes.dex */
public final class ou5 {
    public final /* synthetic */ qu5 a;

    public ou5(qu5 qu5Var) {
        this.a = qu5Var;
    }

    public final void a() {
        this.a.e();
        if (this.a.a.A().v(this.a.a.a().a())) {
            this.a.a.A().l.b(true);
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (runningAppProcessInfo.importance == 100) {
                this.a.a.w().v().a("Detected application was in foreground");
                c(this.a.a.a().a(), false);
            }
        }
    }

    public final void b(long j, boolean z) {
        this.a.e();
        this.a.r();
        if (this.a.a.A().v(j)) {
            this.a.a.A().l.b(true);
        }
        this.a.a.A().o.b(j);
        if (this.a.a.A().l.a()) {
            c(j, z);
        }
    }

    public final void c(long j, boolean z) {
        this.a.e();
        if (this.a.a.h()) {
            this.a.a.A().o.b(j);
            this.a.a.w().v().b("Session started, time", Long.valueOf(this.a.a.a().b()));
            Long valueOf = Long.valueOf(j / 1000);
            this.a.a.F().n("auto", "_sid", valueOf, j);
            this.a.a.A().l.b(false);
            Bundle bundle = new Bundle();
            bundle.putLong("_sid", valueOf.longValue());
            if (this.a.a.z().v(null, qf5.i0) && z) {
                bundle.putLong("_aib", 1L);
            }
            this.a.a.F().Y("auto", "_s", j, bundle);
            e16.a();
            if (this.a.a.z().v(null, qf5.n0)) {
                String a = this.a.a.A().t.a();
                if (TextUtils.isEmpty(a)) {
                    return;
                }
                Bundle bundle2 = new Bundle();
                bundle2.putString("_ffr", a);
                this.a.a.F().Y("auto", "_ssr", j, bundle2);
            }
        }
    }
}
