package defpackage;

/* compiled from: ScaleAnimationValue.java */
/* renamed from: ic3  reason: default package */
/* loaded from: classes2.dex */
public class ic3 extends u20 {
    public int c;
    public int d;

    public int e() {
        return this.c;
    }

    public int f() {
        return this.d;
    }

    public void g(int i) {
        this.c = i;
    }

    public void h(int i) {
        this.d = i;
    }
}
