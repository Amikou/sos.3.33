package defpackage;

import com.google.android.gms.internal.measurement.v1;
import com.google.android.gms.internal.measurement.x1;
import defpackage.cv5;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: cv5  reason: default package */
/* loaded from: classes.dex */
public abstract class cv5<MessageType extends cv5<MessageType, BuilderType>, BuilderType> extends x1<MessageType, BuilderType> implements xx5 {
    public final v1 zza = v1.a();
}
