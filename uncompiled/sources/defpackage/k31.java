package defpackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: FileBinaryResource.java */
/* renamed from: k31  reason: default package */
/* loaded from: classes.dex */
public class k31 implements kp {
    public final File a;

    public k31(File file) {
        this.a = (File) xt2.g(file);
    }

    public static k31 b(File file) {
        return new k31(file);
    }

    public static k31 c(File file) {
        if (file != null) {
            return new k31(file);
        }
        return null;
    }

    @Override // defpackage.kp
    public InputStream a() throws IOException {
        return new FileInputStream(this.a);
    }

    public File d() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof k31)) {
            return false;
        }
        return this.a.equals(((k31) obj).a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @Override // defpackage.kp
    public long size() {
        return this.a.length();
    }
}
