package defpackage;

import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

/* compiled from: AnimateLayoutChangeDetector.java */
/* renamed from: id  reason: default package */
/* loaded from: classes.dex */
public final class id {
    public static final ViewGroup.MarginLayoutParams b;
    public LinearLayoutManager a;

    /* compiled from: AnimateLayoutChangeDetector.java */
    /* renamed from: id$a */
    /* loaded from: classes.dex */
    public class a implements Comparator<int[]> {
        public a(id idVar) {
        }

        @Override // java.util.Comparator
        /* renamed from: a */
        public int compare(int[] iArr, int[] iArr2) {
            return iArr[0] - iArr2[0];
        }
    }

    static {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -1);
        b = marginLayoutParams;
        marginLayoutParams.setMargins(0, 0, 0, 0);
    }

    public id(LinearLayoutManager linearLayoutManager) {
        this.a = linearLayoutManager;
    }

    public static boolean c(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            LayoutTransition layoutTransition = viewGroup.getLayoutTransition();
            if (layoutTransition != null && layoutTransition.isChangingLayout()) {
                return true;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (c(viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean a() {
        ViewGroup.MarginLayoutParams marginLayoutParams;
        int top;
        int i;
        int bottom;
        int i2;
        int U = this.a.U();
        if (U == 0) {
            return true;
        }
        boolean z = this.a.v2() == 0;
        int[][] iArr = (int[][]) Array.newInstance(int.class, U, 2);
        for (int i3 = 0; i3 < U; i3++) {
            View T = this.a.T(i3);
            if (T != null) {
                ViewGroup.LayoutParams layoutParams = T.getLayoutParams();
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                } else {
                    marginLayoutParams = b;
                }
                int[] iArr2 = iArr[i3];
                if (z) {
                    top = T.getLeft();
                    i = marginLayoutParams.leftMargin;
                } else {
                    top = T.getTop();
                    i = marginLayoutParams.topMargin;
                }
                iArr2[0] = top - i;
                int[] iArr3 = iArr[i3];
                if (z) {
                    bottom = T.getRight();
                    i2 = marginLayoutParams.rightMargin;
                } else {
                    bottom = T.getBottom();
                    i2 = marginLayoutParams.bottomMargin;
                }
                iArr3[1] = bottom + i2;
            } else {
                throw new IllegalStateException("null view contained in the view hierarchy");
            }
        }
        Arrays.sort(iArr, new a(this));
        for (int i4 = 1; i4 < U; i4++) {
            if (iArr[i4 - 1][1] != iArr[i4][0]) {
                return false;
            }
        }
        return iArr[0][0] <= 0 && iArr[U - 1][1] >= iArr[0][1] - iArr[0][0];
    }

    public final boolean b() {
        int U = this.a.U();
        for (int i = 0; i < U; i++) {
            if (c(this.a.T(i))) {
                return true;
            }
        }
        return false;
    }

    public boolean d() {
        return (!a() || this.a.U() <= 1) && b();
    }
}
