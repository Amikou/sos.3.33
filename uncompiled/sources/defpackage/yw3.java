package defpackage;

import android.graphics.drawable.PictureDrawable;
import com.caverock.androidsvg.SVG;

/* compiled from: SvgDrawableTranscoder.kt */
/* renamed from: yw3  reason: default package */
/* loaded from: classes2.dex */
public final class yw3 implements e83<SVG, PictureDrawable> {
    @Override // defpackage.e83
    public s73<PictureDrawable> a(s73<SVG> s73Var, vn2 vn2Var) {
        fs1.f(s73Var, "toTranscode");
        fs1.f(vn2Var, "options");
        SVG svg = s73Var.get();
        fs1.e(svg, "toTranscode.get()");
        return new op3(new PictureDrawable(svg.n()));
    }
}
