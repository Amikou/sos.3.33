package defpackage;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ForwardingListener.java */
/* renamed from: g91  reason: default package */
/* loaded from: classes.dex */
public abstract class g91 implements View.OnTouchListener, View.OnAttachStateChangeListener {
    public final float a;
    public final int f0;
    public final int g0;
    public final View h0;
    public Runnable i0;
    public Runnable j0;
    public boolean k0;
    public int l0;
    public final int[] m0 = new int[2];

    /* compiled from: ForwardingListener.java */
    /* renamed from: g91$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ViewParent parent = g91.this.h0.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    /* compiled from: ForwardingListener.java */
    /* renamed from: g91$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            g91.this.e();
        }
    }

    public g91(View view) {
        this.h0 = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.a = ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        int tapTimeout = ViewConfiguration.getTapTimeout();
        this.f0 = tapTimeout;
        this.g0 = (tapTimeout + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    public static boolean h(View view, float f, float f2, float f3) {
        float f4 = -f3;
        return f >= f4 && f2 >= f4 && f < ((float) (view.getRight() - view.getLeft())) + f3 && f2 < ((float) (view.getBottom() - view.getTop())) + f3;
    }

    public final void a() {
        Runnable runnable = this.j0;
        if (runnable != null) {
            this.h0.removeCallbacks(runnable);
        }
        Runnable runnable2 = this.i0;
        if (runnable2 != null) {
            this.h0.removeCallbacks(runnable2);
        }
    }

    public abstract po3 b();

    public abstract boolean c();

    public boolean d() {
        po3 b2 = b();
        if (b2 == null || !b2.a()) {
            return true;
        }
        b2.dismiss();
        return true;
    }

    public void e() {
        a();
        View view = this.h0;
        if (view.isEnabled() && !view.isLongClickable() && c()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.k0 = true;
        }
    }

    public final boolean f(MotionEvent motionEvent) {
        bs0 bs0Var;
        View view = this.h0;
        po3 b2 = b();
        if (b2 == null || !b2.a() || (bs0Var = (bs0) b2.k()) == null || !bs0Var.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        i(view, obtainNoHistory);
        j(bs0Var, obtainNoHistory);
        boolean e = bs0Var.e(obtainNoHistory, this.l0);
        obtainNoHistory.recycle();
        int actionMasked = motionEvent.getActionMasked();
        return e && (actionMasked != 1 && actionMasked != 3);
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0017, code lost:
        if (r1 != 3) goto L13;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean g(android.view.MotionEvent r6) {
        /*
            r5 = this;
            android.view.View r0 = r5.h0
            boolean r1 = r0.isEnabled()
            r2 = 0
            if (r1 != 0) goto La
            return r2
        La:
            int r1 = r6.getActionMasked()
            if (r1 == 0) goto L41
            r3 = 1
            if (r1 == r3) goto L3d
            r4 = 2
            if (r1 == r4) goto L1a
            r6 = 3
            if (r1 == r6) goto L3d
            goto L6d
        L1a:
            int r1 = r5.l0
            int r1 = r6.findPointerIndex(r1)
            if (r1 < 0) goto L6d
            float r4 = r6.getX(r1)
            float r6 = r6.getY(r1)
            float r1 = r5.a
            boolean r6 = h(r0, r4, r6, r1)
            if (r6 != 0) goto L6d
            r5.a()
            android.view.ViewParent r6 = r0.getParent()
            r6.requestDisallowInterceptTouchEvent(r3)
            return r3
        L3d:
            r5.a()
            goto L6d
        L41:
            int r6 = r6.getPointerId(r2)
            r5.l0 = r6
            java.lang.Runnable r6 = r5.i0
            if (r6 != 0) goto L52
            g91$a r6 = new g91$a
            r6.<init>()
            r5.i0 = r6
        L52:
            java.lang.Runnable r6 = r5.i0
            int r1 = r5.f0
            long r3 = (long) r1
            r0.postDelayed(r6, r3)
            java.lang.Runnable r6 = r5.j0
            if (r6 != 0) goto L65
            g91$b r6 = new g91$b
            r6.<init>()
            r5.j0 = r6
        L65:
            java.lang.Runnable r6 = r5.j0
            int r1 = r5.g0
            long r3 = (long) r1
            r0.postDelayed(r6, r3)
        L6d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.g91.g(android.view.MotionEvent):boolean");
    }

    public final boolean i(View view, MotionEvent motionEvent) {
        int[] iArr = this.m0;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation(iArr[0], iArr[1]);
        return true;
    }

    public final boolean j(View view, MotionEvent motionEvent) {
        int[] iArr = this.m0;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation(-iArr[0], -iArr[1]);
        return true;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.k0;
        if (z2) {
            z = f(motionEvent) || !d();
        } else {
            z = g(motionEvent) && c();
            if (z) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0);
                this.h0.onTouchEvent(obtain);
                obtain.recycle();
            }
        }
        this.k0 = z;
        return z || z2;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        this.k0 = false;
        this.l0 = -1;
        Runnable runnable = this.i0;
        if (runnable != null) {
            this.h0.removeCallbacks(runnable);
        }
    }
}
