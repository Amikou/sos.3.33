package defpackage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.b;

/* compiled from: Response.java */
@JsonIgnoreProperties(ignoreUnknown = true)
/* renamed from: i83  reason: default package */
/* loaded from: classes3.dex */
public class i83<T> {
    private a error;
    private long id;
    private String jsonrpc;
    private String rawResponse;
    private T result;

    /* compiled from: Response.java */
    /* renamed from: i83$a */
    /* loaded from: classes3.dex */
    public static class a {
        private int code;
        @b(using = org.web3j.protocol.deserializer.a.class)
        private String data;
        private String message;

        public a() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (getCode() != aVar.getCode()) {
                    return false;
                }
                if (getMessage() == null ? aVar.getMessage() == null : getMessage().equals(aVar.getMessage())) {
                    return getData() != null ? getData().equals(aVar.getData()) : aVar.getData() == null;
                }
                return false;
            }
            return false;
        }

        public int getCode() {
            return this.code;
        }

        public String getData() {
            return this.data;
        }

        public String getMessage() {
            return this.message;
        }

        public int hashCode() {
            return (((getCode() * 31) + (getMessage() != null ? getMessage().hashCode() : 0)) * 31) + (getData() != null ? getData().hashCode() : 0);
        }

        public void setCode(int i) {
            this.code = i;
        }

        public void setData(String str) {
            this.data = str;
        }

        public void setMessage(String str) {
            this.message = str;
        }

        public a(int i, String str) {
            this.code = i;
            this.message = str;
        }
    }

    public a getError() {
        return this.error;
    }

    public long getId() {
        return this.id;
    }

    public String getJsonrpc() {
        return this.jsonrpc;
    }

    public String getRawResponse() {
        return this.rawResponse;
    }

    public T getResult() {
        return this.result;
    }

    public boolean hasError() {
        return this.error != null;
    }

    public void setError(a aVar) {
        this.error = aVar;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setJsonrpc(String str) {
        this.jsonrpc = str;
    }

    public void setRawResponse(String str) {
        this.rawResponse = str;
    }

    public void setResult(T t) {
        this.result = t;
    }
}
