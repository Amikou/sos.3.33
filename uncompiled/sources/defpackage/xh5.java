package defpackage;

import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.a;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.e;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: xh5  reason: default package */
/* loaded from: classes.dex */
public final class xh5 implements Runnable {
    public final /* synthetic */ c a;
    public final /* synthetic */ rc5 f0;

    public xh5(rc5 rc5Var, c cVar) {
        this.f0 = rc5Var;
        this.a = cVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        e eVar;
        e eVar2;
        e eVar3;
        a aVar;
        try {
            aVar = this.f0.b;
            c cVar = (c) aVar.a(this.a);
            if (cVar == null) {
                this.f0.b(new NullPointerException("Continuation returned null"));
                return;
            }
            Executor executor = s34.b;
            cVar.g(executor, this.f0);
            cVar.e(executor, this.f0);
            cVar.a(executor, this.f0);
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                eVar3 = this.f0.c;
                eVar3.s((Exception) e.getCause());
                return;
            }
            eVar2 = this.f0.c;
            eVar2.s(e);
        } catch (Exception e2) {
            eVar = this.f0.c;
            eVar.s(e2);
        }
    }
}
