package defpackage;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Callable;
import java8.util.concurrent.CompletableFuture;
import org.web3j.protocol.core.a;
import org.web3j.protocol.core.c;

/* compiled from: Service.java */
/* renamed from: fm3  reason: default package */
/* loaded from: classes3.dex */
public abstract class fm3 implements lo4 {
    public final ObjectMapper objectMapper;

    public fm3(boolean z) {
        this.objectMapper = ml2.getObjectMapper(z);
    }

    @Override // defpackage.lo4
    public abstract /* synthetic */ void close() throws IOException;

    public abstract InputStream performIO(String str) throws IOException;

    @Override // defpackage.lo4
    /* renamed from: send */
    public <T extends i83> T lambda$sendAsync$0(c cVar, Class<T> cls) throws IOException {
        InputStream performIO = performIO(this.objectMapper.writeValueAsString(cVar));
        if (performIO == null) {
            if (performIO != null) {
            }
            return null;
        }
        try {
            return (T) this.objectMapper.readValue(performIO, cls);
        } finally {
            performIO.close();
        }
    }

    @Override // defpackage.lo4
    public <T extends i83> CompletableFuture<T> sendAsync(final c cVar, final Class<T> cls) {
        return ti.run(new Callable() { // from class: em3
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i83 lambda$sendAsync$0;
                lambda$sendAsync$0 = fm3.this.lambda$sendAsync$0(cVar, cls);
                return lambda$sendAsync$0;
            }
        });
    }

    @Override // defpackage.lo4
    /* renamed from: sendBatch */
    public lo lambda$sendBatchAsync$1(a aVar) throws IOException {
        if (aVar.getRequests().isEmpty()) {
            return new lo(Collections.emptyList(), Collections.emptyList());
        }
        InputStream performIO = performIO(this.objectMapper.writeValueAsString(aVar.getRequests()));
        if (performIO != null) {
            try {
                com.fasterxml.jackson.databind.node.a aVar2 = (com.fasterxml.jackson.databind.node.a) this.objectMapper.readTree(performIO);
                ArrayList arrayList = new ArrayList(aVar2.size());
                for (int i = 0; i < aVar2.size(); i++) {
                    arrayList.add((i83) this.objectMapper.treeToValue(aVar2.V(i), aVar.getRequests().get(i).getResponseType()));
                }
                return new lo(aVar.getRequests(), arrayList);
            } catch (IOException e) {
                performIO.close();
                throw e;
            }
        }
        return null;
    }

    @Override // defpackage.lo4
    public CompletableFuture<lo> sendBatchAsync(final a aVar) {
        return ti.run(new Callable() { // from class: dm3
            @Override // java.util.concurrent.Callable
            public final Object call() {
                lo lambda$sendBatchAsync$1;
                lambda$sendBatchAsync$1 = fm3.this.lambda$sendBatchAsync$1(aVar);
                return lambda$sendBatchAsync$1;
            }
        });
    }

    @Override // defpackage.lo4
    public <T extends zg2<?>> q71<T> subscribe(c cVar, String str, Class<T> cls) {
        throw new UnsupportedOperationException(String.format("Service %s does not support subscriptions", getClass().getSimpleName()));
    }
}
