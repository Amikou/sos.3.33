package defpackage;

import android.os.Build;
import android.view.View;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ViewOscillator.java */
/* renamed from: kj4  reason: default package */
/* loaded from: classes.dex */
public abstract class kj4 extends hx1 {

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$a */
    /* loaded from: classes.dex */
    public static class a extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setAlpha(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$b */
    /* loaded from: classes.dex */
    public static class b extends kj4 {
        public float[] g = new float[1];
        public ConstraintAttribute h;

        @Override // defpackage.hx1
        public void c(Object obj) {
            this.h = (ConstraintAttribute) obj;
        }

        @Override // defpackage.kj4
        public void j(View view, float f) {
            this.g[0] = a(f);
            this.h.k(view, this.g);
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$c */
    /* loaded from: classes.dex */
    public static class c extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setElevation(a(f));
            }
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$d */
    /* loaded from: classes.dex */
    public static class d extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
        }

        public void k(View view, float f, double d, double d2) {
            view.setRotation(a(f) + ((float) Math.toDegrees(Math.atan2(d2, d))));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$e */
    /* loaded from: classes.dex */
    public static class e extends kj4 {
        public boolean g = false;

        @Override // defpackage.kj4
        public void j(View view, float f) {
            if (view instanceof MotionLayout) {
                ((MotionLayout) view).setProgress(a(f));
            } else if (this.g) {
            } else {
                Method method = null;
                try {
                    method = view.getClass().getMethod("setProgress", Float.TYPE);
                } catch (NoSuchMethodException unused) {
                    this.g = true;
                }
                if (method != null) {
                    try {
                        method.invoke(view, Float.valueOf(a(f)));
                    } catch (IllegalAccessException | InvocationTargetException unused2) {
                    }
                }
            }
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$f */
    /* loaded from: classes.dex */
    public static class f extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setRotation(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$g */
    /* loaded from: classes.dex */
    public static class g extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setRotationX(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$h */
    /* loaded from: classes.dex */
    public static class h extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setRotationY(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$i */
    /* loaded from: classes.dex */
    public static class i extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setScaleX(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$j */
    /* loaded from: classes.dex */
    public static class j extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setScaleY(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$k */
    /* loaded from: classes.dex */
    public static class k extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setTranslationX(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$l */
    /* loaded from: classes.dex */
    public static class l extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            view.setTranslationY(a(f));
        }
    }

    /* compiled from: ViewOscillator.java */
    /* renamed from: kj4$m */
    /* loaded from: classes.dex */
    public static class m extends kj4 {
        @Override // defpackage.kj4
        public void j(View view, float f) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setTranslationZ(a(f));
            }
        }
    }

    public static kj4 i(String str) {
        if (str.startsWith("CUSTOM")) {
            return new b();
        }
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1249320806:
                if (str.equals("rotationX")) {
                    c2 = 0;
                    break;
                }
                break;
            case -1249320805:
                if (str.equals("rotationY")) {
                    c2 = 1;
                    break;
                }
                break;
            case -1225497657:
                if (str.equals("translationX")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1225497656:
                if (str.equals("translationY")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1225497655:
                if (str.equals("translationZ")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1001078227:
                if (str.equals("progress")) {
                    c2 = 5;
                    break;
                }
                break;
            case -908189618:
                if (str.equals("scaleX")) {
                    c2 = 6;
                    break;
                }
                break;
            case -908189617:
                if (str.equals("scaleY")) {
                    c2 = 7;
                    break;
                }
                break;
            case -797520672:
                if (str.equals("waveVariesBy")) {
                    c2 = '\b';
                    break;
                }
                break;
            case -40300674:
                if (str.equals("rotation")) {
                    c2 = '\t';
                    break;
                }
                break;
            case -4379043:
                if (str.equals("elevation")) {
                    c2 = '\n';
                    break;
                }
                break;
            case 37232917:
                if (str.equals("transitionPathRotate")) {
                    c2 = 11;
                    break;
                }
                break;
            case 92909918:
                if (str.equals("alpha")) {
                    c2 = '\f';
                    break;
                }
                break;
            case 156108012:
                if (str.equals("waveOffset")) {
                    c2 = '\r';
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                return new g();
            case 1:
                return new h();
            case 2:
                return new k();
            case 3:
                return new l();
            case 4:
                return new m();
            case 5:
                return new e();
            case 6:
                return new i();
            case 7:
                return new j();
            case '\b':
                return new a();
            case '\t':
                return new f();
            case '\n':
                return new c();
            case 11:
                return new d();
            case '\f':
                return new a();
            case '\r':
                return new a();
            default:
                return null;
        }
    }

    public abstract void j(View view, float f2);
}
