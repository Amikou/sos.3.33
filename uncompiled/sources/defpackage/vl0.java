package defpackage;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import com.squareup.picasso.o;
import java.lang.ref.WeakReference;

/* compiled from: DeferredRequestCreator.java */
/* renamed from: vl0  reason: default package */
/* loaded from: classes2.dex */
public class vl0 implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    public final o a;
    public final WeakReference<ImageView> f0;
    public xu g0;

    public vl0(o oVar, ImageView imageView, xu xuVar) {
        this.a = oVar;
        this.f0 = new WeakReference<>(imageView);
        this.g0 = xuVar;
        imageView.addOnAttachStateChangeListener(this);
        if (imageView.getWindowToken() != null) {
            onViewAttachedToWindow(imageView);
        }
    }

    public void a() {
        this.a.b();
        this.g0 = null;
        ImageView imageView = this.f0.get();
        if (imageView == null) {
            return;
        }
        this.f0.clear();
        imageView.removeOnAttachStateChangeListener(this);
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this);
        }
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        ImageView imageView = this.f0.get();
        if (imageView == null) {
            return true;
        }
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            int width = imageView.getWidth();
            int height = imageView.getHeight();
            if (width > 0 && height > 0) {
                imageView.removeOnAttachStateChangeListener(this);
                viewTreeObserver.removeOnPreDrawListener(this);
                this.f0.clear();
                this.a.m().k(width, height).g(imageView, this.g0);
            }
            return true;
        }
        return true;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
        view.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        view.getViewTreeObserver().removeOnPreDrawListener(this);
    }
}
