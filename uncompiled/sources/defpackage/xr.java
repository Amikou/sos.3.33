package defpackage;

import com.facebook.cache.disk.f;
import com.facebook.common.memory.PooledByteBuffer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: BufferedDiskCache.java */
/* renamed from: xr  reason: default package */
/* loaded from: classes.dex */
public class xr {
    public static final Class<?> h = xr.class;
    public final f a;
    public final com.facebook.common.memory.b b;
    public final com.facebook.common.memory.c c;
    public final Executor d;
    public final Executor e;
    public final js3 f = js3.b();
    public final qn1 g;

    /* compiled from: BufferedDiskCache.java */
    /* renamed from: xr$a */
    /* loaded from: classes.dex */
    public class a implements Callable<zu0> {
        public final /* synthetic */ Object a;
        public final /* synthetic */ AtomicBoolean b;
        public final /* synthetic */ wt c;

        public a(Object obj, AtomicBoolean atomicBoolean, wt wtVar) {
            this.a = obj;
            this.b = atomicBoolean;
            this.c = wtVar;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public zu0 call() throws Exception {
            Object e = mc1.e(this.a, null);
            try {
                if (!this.b.get()) {
                    zu0 a = xr.this.f.a(this.c);
                    if (a != null) {
                        v11.o(xr.h, "Found image for %s in staging area", this.c.b());
                        xr.this.g.a(this.c);
                    } else {
                        v11.o(xr.h, "Did not find image for %s in staging area", this.c.b());
                        xr.this.g.e(this.c);
                        try {
                            PooledByteBuffer m = xr.this.m(this.c);
                            if (m == null) {
                                return null;
                            }
                            com.facebook.common.references.a v = com.facebook.common.references.a.v(m);
                            try {
                                a = new zu0(v);
                            } finally {
                                com.facebook.common.references.a.g(v);
                            }
                        } catch (Exception unused) {
                            return null;
                        }
                    }
                    if (Thread.interrupted()) {
                        v11.n(xr.h, "Host thread was interrupted, decreasing reference count");
                        a.close();
                        throw new InterruptedException();
                    }
                    return a;
                }
                throw new CancellationException();
            } catch (Throwable th) {
                try {
                    mc1.c(this.a, th);
                    throw th;
                } finally {
                    mc1.f(e);
                }
            }
        }
    }

    /* compiled from: BufferedDiskCache.java */
    /* renamed from: xr$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public final /* synthetic */ Object a;
        public final /* synthetic */ wt f0;
        public final /* synthetic */ zu0 g0;

        public b(Object obj, wt wtVar, zu0 zu0Var) {
            this.a = obj;
            this.f0 = wtVar;
            this.g0 = zu0Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            Object e = mc1.e(this.a, null);
            try {
                xr.this.o(this.f0, this.g0);
            } finally {
            }
        }
    }

    /* compiled from: BufferedDiskCache.java */
    /* renamed from: xr$c */
    /* loaded from: classes.dex */
    public class c implements Callable<Void> {
        public final /* synthetic */ Object a;
        public final /* synthetic */ wt b;

        public c(Object obj, wt wtVar) {
            this.a = obj;
            this.b = wtVar;
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            Object e = mc1.e(this.a, null);
            try {
                xr.this.f.e(this.b);
                xr.this.a.d(this.b);
                return null;
            } finally {
            }
        }
    }

    /* compiled from: BufferedDiskCache.java */
    /* renamed from: xr$d */
    /* loaded from: classes.dex */
    public class d implements com.facebook.cache.common.d {
        public final /* synthetic */ zu0 a;

        public d(zu0 zu0Var) {
            this.a = zu0Var;
        }

        @Override // com.facebook.cache.common.d
        public void a(OutputStream outputStream) throws IOException {
            InputStream m = this.a.m();
            xt2.g(m);
            xr.this.c.a(m, outputStream);
        }
    }

    public xr(f fVar, com.facebook.common.memory.b bVar, com.facebook.common.memory.c cVar, Executor executor, Executor executor2, qn1 qn1Var) {
        this.a = fVar;
        this.b = bVar;
        this.c = cVar;
        this.d = executor;
        this.e = executor2;
        this.g = qn1Var;
    }

    public void h(wt wtVar) {
        xt2.g(wtVar);
        this.a.a(wtVar);
    }

    public final bolts.b<zu0> i(wt wtVar, zu0 zu0Var) {
        v11.o(h, "Found image for %s in staging area", wtVar.b());
        this.g.a(wtVar);
        return bolts.b.h(zu0Var);
    }

    public bolts.b<zu0> j(wt wtVar, AtomicBoolean atomicBoolean) {
        try {
            if (nc1.d()) {
                nc1.a("BufferedDiskCache#get");
            }
            zu0 a2 = this.f.a(wtVar);
            if (a2 != null) {
                return i(wtVar, a2);
            }
            bolts.b<zu0> k = k(wtVar, atomicBoolean);
            if (nc1.d()) {
                nc1.b();
            }
            return k;
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public final bolts.b<zu0> k(wt wtVar, AtomicBoolean atomicBoolean) {
        try {
            return bolts.b.b(new a(mc1.d("BufferedDiskCache_getAsync"), atomicBoolean, wtVar), this.d);
        } catch (Exception e) {
            v11.w(h, e, "Failed to schedule disk-cache read for %s", wtVar.b());
            return bolts.b.g(e);
        }
    }

    public void l(wt wtVar, zu0 zu0Var) {
        try {
            if (nc1.d()) {
                nc1.a("BufferedDiskCache#put");
            }
            xt2.g(wtVar);
            xt2.b(Boolean.valueOf(zu0.F(zu0Var)));
            this.f.d(wtVar, zu0Var);
            zu0 b2 = zu0.b(zu0Var);
            try {
                this.e.execute(new b(mc1.d("BufferedDiskCache_putAsync"), wtVar, b2));
            } catch (Exception e) {
                v11.w(h, e, "Failed to schedule disk-cache write for %s", wtVar.b());
                this.f.f(wtVar, zu0Var);
                zu0.c(b2);
            }
        } finally {
            if (nc1.d()) {
                nc1.b();
            }
        }
    }

    public final PooledByteBuffer m(wt wtVar) throws IOException {
        try {
            Class<?> cls = h;
            v11.o(cls, "Disk cache read for %s", wtVar.b());
            kp c2 = this.a.c(wtVar);
            if (c2 == null) {
                v11.o(cls, "Disk cache miss for %s", wtVar.b());
                this.g.l(wtVar);
                return null;
            }
            v11.o(cls, "Found entry in disk cache for %s", wtVar.b());
            this.g.m(wtVar);
            InputStream a2 = c2.a();
            PooledByteBuffer d2 = this.b.d(a2, (int) c2.size());
            a2.close();
            v11.o(cls, "Successful read from disk cache for %s", wtVar.b());
            return d2;
        } catch (IOException e) {
            v11.w(h, e, "Exception reading from cache for %s", wtVar.b());
            this.g.b(wtVar);
            throw e;
        }
    }

    public bolts.b<Void> n(wt wtVar) {
        xt2.g(wtVar);
        this.f.e(wtVar);
        try {
            return bolts.b.b(new c(mc1.d("BufferedDiskCache_remove"), wtVar), this.e);
        } catch (Exception e) {
            v11.w(h, e, "Failed to schedule disk-cache remove for %s", wtVar.b());
            return bolts.b.g(e);
        }
    }

    public final void o(wt wtVar, zu0 zu0Var) {
        Class<?> cls = h;
        v11.o(cls, "About to write to disk-cache for key %s", wtVar.b());
        try {
            this.a.b(wtVar, new d(zu0Var));
            this.g.g(wtVar);
            v11.o(cls, "Successful disk-cache write for key %s", wtVar.b());
        } catch (IOException e) {
            v11.w(h, e, "Failed to write to disk-cache for key %s", wtVar.b());
        }
    }
}
