package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: pp5  reason: default package */
/* loaded from: classes.dex */
public class pp5 extends Handler {
    public pp5(Looper looper) {
        super(looper);
    }

    public pp5(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
