package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzal;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: j55  reason: default package */
/* loaded from: classes.dex */
public final class j55 implements Parcelable.Creator<zzal> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzal createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            SafeParcelReader.v(C);
            SafeParcelReader.I(parcel, C);
        }
        SafeParcelReader.u(parcel, J);
        return new zzal();
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzal[] newArray(int i) {
        return new zzal[i];
    }
}
