package defpackage;

import com.google.android.gms.internal.firebase_messaging.c;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: nf5  reason: default package */
/* loaded from: classes.dex */
public abstract class nf5 {
    public static final c a;

    static {
        l45 l45Var = new l45();
        bd5.a.a(l45Var);
        a = l45Var.b();
    }

    public static byte[] a(Object obj) {
        c cVar = a;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            cVar.a(obj, byteArrayOutputStream);
        } catch (IOException unused) {
        }
        return byteArrayOutputStream.toByteArray();
    }

    public abstract g82 b();
}
