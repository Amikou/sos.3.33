package defpackage;

import android.os.SystemClock;

/* compiled from: OSTimeImpl.java */
/* renamed from: al2  reason: default package */
/* loaded from: classes2.dex */
public class al2 implements zk2 {
    @Override // defpackage.zk2
    public long a() {
        return System.currentTimeMillis();
    }

    @Override // defpackage.zk2
    public long b() {
        return SystemClock.elapsedRealtime();
    }
}
