package defpackage;

/* compiled from: Lazy.java */
/* renamed from: ty1  reason: default package */
/* loaded from: classes2.dex */
public class ty1<T> implements fw2<T> {
    public static final Object c = new Object();
    public volatile Object a = c;
    public volatile fw2<T> b;

    public ty1(fw2<T> fw2Var) {
        this.b = fw2Var;
    }

    @Override // defpackage.fw2
    public T get() {
        T t = (T) this.a;
        Object obj = c;
        if (t == obj) {
            synchronized (this) {
                t = this.a;
                if (t == obj) {
                    t = this.b.get();
                    this.a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
