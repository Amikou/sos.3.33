package defpackage;

/* renamed from: lc5  reason: default package */
/* loaded from: classes.dex */
public abstract class lc5 {
    public static final lc5 a = new fd5();
    public static final lc5 b = new hd5();

    public lc5() {
    }

    public static lc5 c() {
        return a;
    }

    public static lc5 d() {
        return b;
    }

    public abstract void a(Object obj, long j);

    public abstract <L> void b(Object obj, Object obj2, long j);
}
