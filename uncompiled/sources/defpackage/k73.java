package defpackage;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.bumptech.glide.Priority;
import com.bumptech.glide.e;
import com.bumptech.glide.f;
import defpackage.x50;
import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: RequestManager.java */
/* renamed from: k73  reason: default package */
/* loaded from: classes.dex */
public class k73 implements ComponentCallbacks2, pz1 {
    public static final n73 p0 = n73.w0(Bitmap.class).X();
    public static final n73 q0;
    public final com.bumptech.glide.a a;
    public final Context f0;
    public final kz1 g0;
    public final o73 h0;
    public final m73 i0;
    public final j34 j0;
    public final Runnable k0;
    public final x50 l0;
    public final CopyOnWriteArrayList<j73<Object>> m0;
    public n73 n0;
    public boolean o0;

    /* compiled from: RequestManager.java */
    /* renamed from: k73$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            k73 k73Var = k73.this;
            k73Var.g0.b(k73Var);
        }
    }

    /* compiled from: RequestManager.java */
    /* renamed from: k73$b */
    /* loaded from: classes.dex */
    public class b implements x50.a {
        public final o73 a;

        public b(o73 o73Var) {
            this.a = o73Var;
        }

        @Override // defpackage.x50.a
        public void a(boolean z) {
            if (z) {
                synchronized (k73.this) {
                    this.a.e();
                }
            }
        }
    }

    static {
        n73.w0(uf1.class).X();
        q0 = n73.x0(bp0.b).f0(Priority.LOW).o0(true);
    }

    public k73(com.bumptech.glide.a aVar, kz1 kz1Var, m73 m73Var, Context context) {
        this(aVar, kz1Var, m73Var, new o73(), aVar.g(), context);
    }

    public synchronized void A() {
        this.h0.c();
    }

    public synchronized void B() {
        A();
        for (k73 k73Var : this.i0.a()) {
            k73Var.A();
        }
    }

    public synchronized void C() {
        this.h0.d();
    }

    public synchronized void D() {
        this.h0.f();
    }

    public synchronized void E(n73 n73Var) {
        this.n0 = n73Var.clone().b();
    }

    public synchronized void F(i34<?> i34Var, d73 d73Var) {
        this.j0.n(i34Var);
        this.h0.g(d73Var);
    }

    public synchronized boolean G(i34<?> i34Var) {
        d73 l = i34Var.l();
        if (l == null) {
            return true;
        }
        if (this.h0.a(l)) {
            this.j0.o(i34Var);
            i34Var.c(null);
            return true;
        }
        return false;
    }

    public final void H(i34<?> i34Var) {
        boolean G = G(i34Var);
        d73 l = i34Var.l();
        if (G || this.a.p(i34Var) || l == null) {
            return;
        }
        i34Var.c(null);
        l.clear();
    }

    @Override // defpackage.pz1
    public synchronized void b() {
        D();
        this.j0.b();
    }

    public <ResourceType> e<ResourceType> d(Class<ResourceType> cls) {
        return new e<>(this.a, this, cls, this.f0);
    }

    @Override // defpackage.pz1
    public synchronized void e() {
        this.j0.e();
        for (i34<?> i34Var : this.j0.i()) {
            o(i34Var);
        }
        this.j0.d();
        this.h0.b();
        this.g0.a(this);
        this.g0.a(this.l0);
        mg4.v(this.k0);
        this.a.s(this);
    }

    @Override // defpackage.pz1
    public synchronized void h() {
        C();
        this.j0.h();
    }

    public e<Bitmap> i() {
        return d(Bitmap.class).a(p0);
    }

    public e<Drawable> n() {
        return d(Drawable.class);
    }

    public void o(i34<?> i34Var) {
        if (i34Var == null) {
            return;
        }
        H(i34Var);
    }

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
    }

    @Override // android.content.ComponentCallbacks2
    public void onTrimMemory(int i) {
        if (i == 60 && this.o0) {
            B();
        }
    }

    public e<File> p() {
        return d(File.class).a(q0);
    }

    public List<j73<Object>> q() {
        return this.m0;
    }

    public synchronized n73 r() {
        return this.n0;
    }

    public <T> f<?, T> s(Class<T> cls) {
        return this.a.i().e(cls);
    }

    public e<Drawable> t(Bitmap bitmap) {
        return n().M0(bitmap);
    }

    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.h0 + ", treeNode=" + this.i0 + "}";
    }

    public e<Drawable> u(Uri uri) {
        return n().N0(uri);
    }

    public e<Drawable> v(File file) {
        return n().O0(file);
    }

    public e<Drawable> w(Integer num) {
        return n().P0(num);
    }

    public e<Drawable> x(Object obj) {
        return n().Q0(obj);
    }

    public e<Drawable> y(String str) {
        return n().R0(str);
    }

    public e<Drawable> z(byte[] bArr) {
        return n().S0(bArr);
    }

    public k73(com.bumptech.glide.a aVar, kz1 kz1Var, m73 m73Var, o73 o73Var, y50 y50Var, Context context) {
        this.j0 = new j34();
        a aVar2 = new a();
        this.k0 = aVar2;
        this.a = aVar;
        this.g0 = kz1Var;
        this.i0 = m73Var;
        this.h0 = o73Var;
        this.f0 = context;
        x50 a2 = y50Var.a(context.getApplicationContext(), new b(o73Var));
        this.l0 = a2;
        if (mg4.q()) {
            mg4.u(aVar2);
        } else {
            kz1Var.b(this);
        }
        kz1Var.b(a2);
        this.m0 = new CopyOnWriteArrayList<>(aVar.i().c());
        E(aVar.i().d());
        aVar.o(this);
    }
}
