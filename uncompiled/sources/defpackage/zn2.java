package defpackage;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* compiled from: OrientedDrawable.java */
/* renamed from: zn2  reason: default package */
/* loaded from: classes.dex */
public class zn2 extends d91 {
    public final Matrix h0;
    public int i0;
    public int j0;
    public final Matrix k0;
    public final RectF l0;

    public zn2(Drawable drawable, int i, int i2) {
        super(drawable);
        this.k0 = new Matrix();
        this.l0 = new RectF();
        this.h0 = new Matrix();
        this.i0 = i - (i % 90);
        this.j0 = (i2 < 0 || i2 > 8) ? 0 : 0;
    }

    @Override // defpackage.d91, defpackage.wa4
    public void c(Matrix matrix) {
        m(matrix);
        if (this.h0.isIdentity()) {
            return;
        }
        matrix.preConcat(this.h0);
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int i;
        if (this.i0 <= 0 && ((i = this.j0) == 0 || i == 1)) {
            super.draw(canvas);
            return;
        }
        int save = canvas.save();
        canvas.concat(this.h0);
        super.draw(canvas);
        canvas.restoreToCount(save);
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        int i = this.j0;
        if (i != 5 && i != 7 && this.i0 % 180 == 0) {
            return super.getIntrinsicHeight();
        }
        return super.getIntrinsicWidth();
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        int i = this.j0;
        if (i != 5 && i != 7 && this.i0 % 180 == 0) {
            return super.getIntrinsicWidth();
        }
        return super.getIntrinsicHeight();
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        int i;
        Drawable current = getCurrent();
        int i2 = this.i0;
        if (i2 <= 0 && ((i = this.j0) == 0 || i == 1)) {
            current.setBounds(rect);
            return;
        }
        int i3 = this.j0;
        if (i3 == 2) {
            this.h0.setScale(-1.0f, 1.0f);
        } else if (i3 == 7) {
            this.h0.setRotate(270.0f, rect.centerX(), rect.centerY());
            this.h0.postScale(-1.0f, 1.0f);
        } else if (i3 == 4) {
            this.h0.setScale(1.0f, -1.0f);
        } else if (i3 != 5) {
            this.h0.setRotate(i2, rect.centerX(), rect.centerY());
        } else {
            this.h0.setRotate(270.0f, rect.centerX(), rect.centerY());
            this.h0.postScale(1.0f, -1.0f);
        }
        this.k0.reset();
        this.h0.invert(this.k0);
        this.l0.set(rect);
        this.k0.mapRect(this.l0);
        RectF rectF = this.l0;
        current.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
    }
}
