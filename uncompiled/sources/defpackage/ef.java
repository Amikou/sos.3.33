package defpackage;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.b;
import defpackage.ix1;
import defpackage.k6;

/* compiled from: AppCompatDialog.java */
/* renamed from: ef  reason: default package */
/* loaded from: classes.dex */
public class ef extends Dialog implements cf {
    private b mDelegate;
    private final ix1.a mKeyDispatcher;

    /* compiled from: AppCompatDialog.java */
    /* renamed from: ef$a */
    /* loaded from: classes.dex */
    public class a implements ix1.a {
        public a() {
        }

        @Override // defpackage.ix1.a
        public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
            return ef.this.superDispatchKeyEvent(keyEvent);
        }
    }

    public ef(Context context) {
        this(context, 0);
    }

    private static int getThemeResId(Context context, int i) {
        if (i == 0) {
            TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(jy2.dialogTheme, typedValue, true);
            return typedValue.resourceId;
        }
        return i;
    }

    @Override // android.app.Dialog
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        getDelegate().d(view, layoutParams);
    }

    @Override // android.app.Dialog, android.content.DialogInterface
    public void dismiss() {
        super.dismiss();
        getDelegate().u();
    }

    @Override // android.app.Dialog, android.view.Window.Callback
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return ix1.e(this.mKeyDispatcher, getWindow().getDecorView(), this, keyEvent);
    }

    @Override // android.app.Dialog
    public <T extends View> T findViewById(int i) {
        return (T) getDelegate().k(i);
    }

    public b getDelegate() {
        if (this.mDelegate == null) {
            this.mDelegate = b.j(this, this);
        }
        return this.mDelegate;
    }

    public ActionBar getSupportActionBar() {
        return getDelegate().p();
    }

    @Override // android.app.Dialog
    public void invalidateOptionsMenu() {
        getDelegate().r();
    }

    @Override // android.app.Dialog
    public void onCreate(Bundle bundle) {
        getDelegate().q();
        super.onCreate(bundle);
        getDelegate().t(bundle);
    }

    @Override // android.app.Dialog
    public void onStop() {
        super.onStop();
        getDelegate().z();
    }

    @Override // defpackage.cf
    public void onSupportActionModeFinished(k6 k6Var) {
    }

    @Override // defpackage.cf
    public void onSupportActionModeStarted(k6 k6Var) {
    }

    @Override // defpackage.cf
    public k6 onWindowStartingSupportActionMode(k6.a aVar) {
        return null;
    }

    @Override // android.app.Dialog
    public void setContentView(int i) {
        getDelegate().D(i);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        getDelegate().J(charSequence);
    }

    public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean supportRequestWindowFeature(int i) {
        return getDelegate().C(i);
    }

    public ef(Context context, int i) {
        super(context, getThemeResId(context, i));
        this.mKeyDispatcher = new a();
        b delegate = getDelegate();
        delegate.I(getThemeResId(context, i));
        delegate.t(null);
    }

    @Override // android.app.Dialog
    public void setContentView(View view) {
        getDelegate().E(view);
    }

    @Override // android.app.Dialog
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        getDelegate().F(view, layoutParams);
    }

    @Override // android.app.Dialog
    public void setTitle(int i) {
        super.setTitle(i);
        getDelegate().J(getContext().getString(i));
    }

    public ef(Context context, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z, onCancelListener);
        this.mKeyDispatcher = new a();
    }
}
