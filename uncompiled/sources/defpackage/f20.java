package defpackage;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* compiled from: MutableCollectionsJVM.kt */
/* renamed from: f20  reason: default package */
/* loaded from: classes2.dex */
public class f20 extends e20 {
    public static final <T extends Comparable<? super T>> void t(List<T> list) {
        fs1.f(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }

    public static final <T> void u(List<T> list, Comparator<? super T> comparator) {
        fs1.f(list, "$this$sortWith");
        fs1.f(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }
}
