package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: kg5  reason: default package */
/* loaded from: classes.dex */
public final class kg5 implements Iterator<Map.Entry<K, V>> {
    public int a;
    public Iterator<Map.Entry<K, V>> f0;
    public final /* synthetic */ fg5 g0;

    public kg5(fg5 fg5Var) {
        List list;
        this.g0 = fg5Var;
        list = fg5Var.f0;
        this.a = list.size();
    }

    public /* synthetic */ kg5(fg5 fg5Var, hg5 hg5Var) {
        this(fg5Var);
    }

    public final Iterator<Map.Entry<K, V>> a() {
        Map map;
        if (this.f0 == null) {
            map = this.g0.j0;
            this.f0 = map.entrySet().iterator();
        }
        return this.f0;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        List list;
        int i = this.a;
        if (i > 0) {
            list = this.g0.f0;
            if (i <= list.size()) {
                return true;
            }
        }
        return a().hasNext();
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        List list;
        Object obj;
        if (a().hasNext()) {
            obj = a().next();
        } else {
            list = this.g0.f0;
            int i = this.a - 1;
            this.a = i;
            obj = list.get(i);
        }
        return (Map.Entry) obj;
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
