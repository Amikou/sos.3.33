package defpackage;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: ki5  reason: default package */
/* loaded from: classes.dex */
public final class ki5 extends AbstractList<String> implements jc5, RandomAccess {
    public final jc5 a;

    public ki5(jc5 jc5Var) {
        this.a = jc5Var;
    }

    @Override // defpackage.jc5
    public final List<?> b0() {
        return this.a.b0();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        return (String) this.a.get(i);
    }

    @Override // java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
    public final Iterator<String> iterator() {
        return new pi5(this);
    }

    @Override // defpackage.jc5
    public final Object j(int i) {
        return this.a.j(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public final ListIterator<String> listIterator(int i) {
        return new ni5(this, i);
    }

    @Override // defpackage.jc5
    public final jc5 s1() {
        return this;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.a.size();
    }
}
