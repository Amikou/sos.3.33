package defpackage;

import com.facebook.common.references.SharedReference;
import com.facebook.common.references.a;
import java.io.Closeable;
import java.io.PrintWriter;
import java.io.StringWriter;

/* compiled from: CloseableReferenceFactory.java */
/* renamed from: a00  reason: default package */
/* loaded from: classes.dex */
public class a00 {
    public final a.c a;

    /* compiled from: CloseableReferenceFactory.java */
    /* renamed from: a00$a */
    /* loaded from: classes.dex */
    public class a implements a.c {
        public final /* synthetic */ b00 a;

        public a(a00 a00Var, b00 b00Var) {
            this.a = b00Var;
        }

        @Override // com.facebook.common.references.a.c
        public void a(SharedReference<Object> sharedReference, Throwable th) {
            this.a.b(sharedReference, th);
            Object f = sharedReference.f();
            v11.x("Fresco", "Finalized without closing: %x %x (type = %s).\nStack:\n%s", Integer.valueOf(System.identityHashCode(this)), Integer.valueOf(System.identityHashCode(sharedReference)), f != null ? f.getClass().getName() : "<value is null>", a00.d(th));
        }

        @Override // com.facebook.common.references.a.c
        public boolean b() {
            return this.a.a();
        }
    }

    public a00(b00 b00Var) {
        this.a = new a(this, b00Var);
    }

    public static String d(Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public <U extends Closeable> com.facebook.common.references.a<U> b(U u) {
        return com.facebook.common.references.a.x(u, this.a);
    }

    public <T> com.facebook.common.references.a<T> c(T t, d83<T> d83Var) {
        return com.facebook.common.references.a.C(t, d83Var, this.a);
    }
}
