package defpackage;

import java.math.BigInteger;
import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.i;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: tr4  reason: default package */
/* loaded from: classes2.dex */
public class tr4 extends h implements vr4 {
    public i a;
    public k f0;

    public tr4(int i, int i2) {
        this(i, i2, 0, 0);
    }

    public tr4(int i, int i2, int i3, int i4) {
        this.a = vr4.j;
        d4 d4Var = new d4();
        d4Var.a(new g(i));
        if (i3 == 0) {
            if (i4 != 0) {
                throw new IllegalArgumentException("inconsistent k values");
            }
            d4Var.a(vr4.k);
            d4Var.a(new g(i2));
        } else if (i3 <= i2 || i4 <= i3) {
            throw new IllegalArgumentException("inconsistent k values");
        } else {
            d4Var.a(vr4.l);
            d4 d4Var2 = new d4();
            d4Var2.a(new g(i2));
            d4Var2.a(new g(i3));
            d4Var2.a(new g(i4));
            d4Var.a(new n0(d4Var2));
        }
        this.f0 = new n0(d4Var);
    }

    public tr4(h4 h4Var) {
        this.a = i.G(h4Var.D(0));
        this.f0 = h4Var.D(1).i();
    }

    public tr4(BigInteger bigInteger) {
        this.a = vr4.i;
        this.f0 = new g(bigInteger);
    }

    public static tr4 p(Object obj) {
        if (obj instanceof tr4) {
            return (tr4) obj;
        }
        if (obj != null) {
            return new tr4(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(this.a);
        d4Var.a(this.f0);
        return new n0(d4Var);
    }

    public i o() {
        return this.a;
    }

    public k q() {
        return this.f0;
    }
}
