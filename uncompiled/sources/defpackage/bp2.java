package defpackage;

import androidx.paging.LoadType;

/* renamed from: bp2  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class bp2 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;
    public static final /* synthetic */ int[] c;
    public static final /* synthetic */ int[] d;
    public static final /* synthetic */ int[] e;
    public static final /* synthetic */ int[] f;
    public static final /* synthetic */ int[] g;
    public static final /* synthetic */ int[] h;
    public static final /* synthetic */ int[] i;

    static {
        int[] iArr = new int[LoadType.values().length];
        a = iArr;
        LoadType loadType = LoadType.REFRESH;
        iArr[loadType.ordinal()] = 1;
        LoadType loadType2 = LoadType.PREPEND;
        iArr[loadType2.ordinal()] = 2;
        LoadType loadType3 = LoadType.APPEND;
        iArr[loadType3.ordinal()] = 3;
        int[] iArr2 = new int[LoadType.values().length];
        b = iArr2;
        iArr2[loadType.ordinal()] = 1;
        iArr2[loadType2.ordinal()] = 2;
        iArr2[loadType3.ordinal()] = 3;
        int[] iArr3 = new int[LoadType.values().length];
        c = iArr3;
        iArr3[loadType.ordinal()] = 1;
        iArr3[loadType2.ordinal()] = 2;
        iArr3[loadType3.ordinal()] = 3;
        int[] iArr4 = new int[LoadType.values().length];
        d = iArr4;
        iArr4[loadType.ordinal()] = 1;
        iArr4[loadType2.ordinal()] = 2;
        iArr4[loadType3.ordinal()] = 3;
        int[] iArr5 = new int[LoadType.values().length];
        e = iArr5;
        iArr5[loadType2.ordinal()] = 1;
        iArr5[loadType3.ordinal()] = 2;
        int[] iArr6 = new int[LoadType.values().length];
        f = iArr6;
        iArr6[loadType2.ordinal()] = 1;
        int[] iArr7 = new int[LoadType.values().length];
        g = iArr7;
        iArr7[loadType2.ordinal()] = 1;
        int[] iArr8 = new int[LoadType.values().length];
        h = iArr8;
        iArr8[loadType2.ordinal()] = 1;
        int[] iArr9 = new int[LoadType.values().length];
        i = iArr9;
        iArr9[loadType2.ordinal()] = 1;
    }
}
