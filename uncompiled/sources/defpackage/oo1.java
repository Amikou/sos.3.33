package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: ImagePerfRequestListener.java */
/* renamed from: oo1  reason: default package */
/* loaded from: classes.dex */
public class oo1 extends xn {
    public final o92 a;
    public final po1 b;

    public oo1(o92 o92Var, po1 po1Var) {
        this.a = o92Var;
        this.b = po1Var;
    }

    @Override // defpackage.xn, defpackage.h73
    public void a(ImageRequest imageRequest, Object obj, String str, boolean z) {
        this.b.s(this.a.now());
        this.b.q(imageRequest);
        this.b.d(obj);
        this.b.x(str);
        this.b.w(z);
    }

    @Override // defpackage.xn, defpackage.h73
    public void c(ImageRequest imageRequest, String str, boolean z) {
        this.b.r(this.a.now());
        this.b.q(imageRequest);
        this.b.x(str);
        this.b.w(z);
    }

    @Override // defpackage.xn, defpackage.h73
    public void g(ImageRequest imageRequest, String str, Throwable th, boolean z) {
        this.b.r(this.a.now());
        this.b.q(imageRequest);
        this.b.x(str);
        this.b.w(z);
    }

    @Override // defpackage.xn, defpackage.h73
    public void k(String str) {
        this.b.r(this.a.now());
        this.b.x(str);
    }
}
