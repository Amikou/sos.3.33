package defpackage;

import android.content.SharedPreferences;
import com.google.android.gms.flags.impl.c;

/* renamed from: ci5  reason: default package */
/* loaded from: classes.dex */
public final class ci5 extends g35<Long> {
    public static Long a(SharedPreferences sharedPreferences, String str, Long l) {
        try {
            return (Long) of5.a(new c(sharedPreferences, str, l));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() != 0) {
                "Flag value not available, returning default: ".concat(valueOf);
            }
            return l;
        }
    }
}
