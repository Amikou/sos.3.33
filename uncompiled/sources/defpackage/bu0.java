package defpackage;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EdgeEffect;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: EdgeEffectCompat.java */
/* renamed from: bu0  reason: default package */
/* loaded from: classes.dex */
public final class bu0 {

    /* compiled from: EdgeEffectCompat.java */
    /* renamed from: bu0$a */
    /* loaded from: classes.dex */
    public static class a {
        public static EdgeEffect a(Context context, AttributeSet attributeSet) {
            try {
                return new EdgeEffect(context, attributeSet);
            } catch (Throwable unused) {
                return new EdgeEffect(context);
            }
        }

        public static float b(EdgeEffect edgeEffect) {
            try {
                return edgeEffect.getDistance();
            } catch (Throwable unused) {
                return Utils.FLOAT_EPSILON;
            }
        }

        public static float c(EdgeEffect edgeEffect, float f, float f2) {
            try {
                return edgeEffect.onPullDistance(f, f2);
            } catch (Throwable unused) {
                edgeEffect.onPull(f, f2);
                return Utils.FLOAT_EPSILON;
            }
        }
    }

    public static EdgeEffect a(Context context, AttributeSet attributeSet) {
        if (yr.c()) {
            return a.a(context, attributeSet);
        }
        return new EdgeEffect(context);
    }

    public static float b(EdgeEffect edgeEffect) {
        return yr.c() ? a.b(edgeEffect) : Utils.FLOAT_EPSILON;
    }

    public static void c(EdgeEffect edgeEffect, float f, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            edgeEffect.onPull(f, f2);
        } else {
            edgeEffect.onPull(f);
        }
    }

    public static float d(EdgeEffect edgeEffect, float f, float f2) {
        if (yr.c()) {
            return a.c(edgeEffect, f, f2);
        }
        c(edgeEffect, f, f2);
        return f;
    }
}
