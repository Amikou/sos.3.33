package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: e06  reason: default package */
/* loaded from: classes.dex */
public final class e06 implements yp5<f06> {
    public static final e06 f0 = new e06();
    public final yp5<f06> a = gq5.a(gq5.b(new g06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final f06 zza() {
        return this.a.zza();
    }
}
