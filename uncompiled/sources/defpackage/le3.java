package defpackage;

import java.math.BigInteger;

/* renamed from: le3  reason: default package */
/* loaded from: classes2.dex */
public class le3 {
    public static final int[] a = {-21389, -2, -1, -1, -1};
    public static final int[] b = {457489321, 42778, 1, 0, 0, -42778, -3, -1, -1, -1};
    public static final int[] c = {-457489321, -42779, -2, -1, -1, 42777, 2};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (bd2.a(iArr, iArr2, iArr3) != 0 || (iArr3[4] == -1 && bd2.i(iArr3, a))) {
            kd2.b(5, 21389, iArr3);
        }
    }

    public static void b(int[] iArr, int[] iArr2) {
        if (kd2.s(5, iArr, iArr2) != 0 || (iArr2[4] == -1 && bd2.i(iArr2, a))) {
            kd2.b(5, 21389, iArr2);
        }
    }

    public static int[] c(BigInteger bigInteger) {
        int[] g = bd2.g(bigInteger);
        if (g[4] == -1) {
            int[] iArr = a;
            if (bd2.i(g, iArr)) {
                bd2.t(iArr, g);
            }
        }
        return g;
    }

    public static void d(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] e = bd2.e();
        bd2.l(iArr, iArr2, e);
        g(e, iArr3);
    }

    public static void e(int[] iArr, int[] iArr2, int[] iArr3) {
        if (bd2.p(iArr, iArr2, iArr3) != 0 || (iArr3[9] == -1 && kd2.q(10, iArr3, b))) {
            int[] iArr4 = c;
            if (kd2.e(iArr4.length, iArr4, iArr3) != 0) {
                kd2.t(10, iArr3, iArr4.length);
            }
        }
    }

    public static void f(int[] iArr, int[] iArr2) {
        if (bd2.k(iArr)) {
            bd2.v(iArr2);
        } else {
            bd2.s(a, iArr, iArr2);
        }
    }

    public static void g(int[] iArr, int[] iArr2) {
        if (bd2.n(21389, bd2.m(21389, iArr, 5, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[4] == -1 && bd2.i(iArr2, a))) {
            kd2.b(5, 21389, iArr2);
        }
    }

    public static void h(int i, int[] iArr) {
        if ((i == 0 || bd2.o(21389, i, iArr, 0) == 0) && !(iArr[4] == -1 && bd2.i(iArr, a))) {
            return;
        }
        kd2.b(5, 21389, iArr);
    }

    public static void i(int[] iArr, int[] iArr2) {
        int[] e = bd2.e();
        bd2.r(iArr, e);
        g(e, iArr2);
    }

    public static void j(int[] iArr, int i, int[] iArr2) {
        int[] e = bd2.e();
        bd2.r(iArr, e);
        while (true) {
            g(e, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            bd2.r(iArr2, e);
        }
    }

    public static void k(int[] iArr, int[] iArr2, int[] iArr3) {
        if (bd2.s(iArr, iArr2, iArr3) != 0) {
            kd2.L(5, 21389, iArr3);
        }
    }

    public static void l(int[] iArr, int[] iArr2) {
        if (kd2.E(5, iArr, 0, iArr2) != 0 || (iArr2[4] == -1 && bd2.i(iArr2, a))) {
            kd2.b(5, 21389, iArr2);
        }
    }
}
