package defpackage;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* compiled from: Multimap.java */
/* renamed from: wa2  reason: default package */
/* loaded from: classes2.dex */
public interface wa2<K, V> {
    Map<K, Collection<V>> asMap();

    void clear();

    boolean containsEntry(Object obj, Object obj2);

    boolean containsKey(Object obj);

    Collection<Map.Entry<K, V>> entries();

    Collection<V> get(K k);

    boolean isEmpty();

    Set<K> keySet();

    boolean put(K k, V v);

    boolean remove(Object obj, Object obj2);

    Collection<V> removeAll(Object obj);

    int size();

    Collection<V> values();
}
