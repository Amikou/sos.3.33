package defpackage;

import androidx.paging.LoadType;
import defpackage.xk4;

/* compiled from: PageFetcherSnapshot.kt */
/* renamed from: ap2  reason: default package */
/* loaded from: classes.dex */
public final class ap2 {
    public static final boolean a(ve1 ve1Var, ve1 ve1Var2, LoadType loadType) {
        fs1.f(ve1Var, "$this$shouldPrioritizeOver");
        fs1.f(ve1Var2, "previous");
        fs1.f(loadType, "loadType");
        if (ve1Var.a() <= ve1Var2.a() && (!(ve1Var2.b() instanceof xk4.b) || !(ve1Var.b() instanceof xk4.a))) {
            if ((ve1Var.b() instanceof xk4.b) && (ve1Var2.b() instanceof xk4.a)) {
                return false;
            }
            if (ve1Var.b().a() == ve1Var2.b().a() && ve1Var.b().b() == ve1Var2.b().b()) {
                if (loadType == LoadType.PREPEND && ve1Var2.b().d() < ve1Var.b().d()) {
                    return false;
                }
                if (loadType == LoadType.APPEND && ve1Var2.b().c() < ve1Var.b().c()) {
                    return false;
                }
            }
        }
        return true;
    }
}
