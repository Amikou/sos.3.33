package defpackage;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.j;
import com.bumptech.glide.b;
import com.bumptech.glide.d;
import com.bumptech.glide.manager.RequestManagerFragment;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* compiled from: RequestManagerRetriever.java */
/* renamed from: l73  reason: default package */
/* loaded from: classes.dex */
public class l73 implements Handler.Callback {
    public static final b n0 = new a();
    public volatile k73 a;
    public final Handler h0;
    public final b i0;
    public final bc1 m0;
    public final Map<FragmentManager, RequestManagerFragment> f0 = new HashMap();
    public final Map<androidx.fragment.app.FragmentManager, mw3> g0 = new HashMap();
    public final rh<View, Fragment> j0 = new rh<>();
    public final rh<View, android.app.Fragment> k0 = new rh<>();
    public final Bundle l0 = new Bundle();

    /* compiled from: RequestManagerRetriever.java */
    /* renamed from: l73$a */
    /* loaded from: classes.dex */
    public class a implements b {
        @Override // defpackage.l73.b
        public k73 a(com.bumptech.glide.a aVar, kz1 kz1Var, m73 m73Var, Context context) {
            return new k73(aVar, kz1Var, m73Var, context);
        }
    }

    /* compiled from: RequestManagerRetriever.java */
    /* renamed from: l73$b */
    /* loaded from: classes.dex */
    public interface b {
        k73 a(com.bumptech.glide.a aVar, kz1 kz1Var, m73 m73Var, Context context);
    }

    public l73(b bVar, d dVar) {
        this.i0 = bVar == null ? n0 : bVar;
        this.h0 = new Handler(Looper.getMainLooper(), this);
        this.m0 = b(dVar);
    }

    @TargetApi(17)
    public static void a(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    public static bc1 b(d dVar) {
        if (vj1.h && vj1.g) {
            if (dVar.a(b.e.class)) {
                return new a61();
            }
            return new b61();
        }
        return new dq0();
    }

    public static Activity c(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return c(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    public static void f(Collection<Fragment> collection, Map<View, Fragment> map) {
        if (collection == null) {
            return;
        }
        for (Fragment fragment : collection) {
            if (fragment != null && fragment.getView() != null) {
                map.put(fragment.getView(), fragment);
                f(fragment.getChildFragmentManager().u0(), map);
            }
        }
    }

    public static boolean u(Context context) {
        Activity c = c(context);
        return c == null || !c.isFinishing();
    }

    @TargetApi(26)
    @Deprecated
    public final void d(FragmentManager fragmentManager, rh<View, android.app.Fragment> rhVar) {
        if (Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment fragment : fragmentManager.getFragments()) {
                if (fragment.getView() != null) {
                    rhVar.put(fragment.getView(), fragment);
                    d(fragment.getChildFragmentManager(), rhVar);
                }
            }
            return;
        }
        e(fragmentManager, rhVar);
    }

    @Deprecated
    public final void e(FragmentManager fragmentManager, rh<View, android.app.Fragment> rhVar) {
        int i = 0;
        while (true) {
            int i2 = i + 1;
            this.l0.putInt("key", i);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.l0, "key");
            } catch (Exception unused) {
            }
            if (fragment == null) {
                return;
            }
            if (fragment.getView() != null) {
                rhVar.put(fragment.getView(), fragment);
                if (Build.VERSION.SDK_INT >= 17) {
                    d(fragment.getChildFragmentManager(), rhVar);
                }
            }
            i = i2;
        }
    }

    @Deprecated
    public final android.app.Fragment g(View view, Activity activity) {
        this.k0.clear();
        d(activity.getFragmentManager(), this.k0);
        View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.k0.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.k0.clear();
        return fragment;
    }

    public final Fragment h(View view, FragmentActivity fragmentActivity) {
        this.j0.clear();
        f(fragmentActivity.getSupportFragmentManager().u0(), this.j0);
        View findViewById = fragmentActivity.findViewById(16908290);
        Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.j0.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.j0.clear();
        return fragment;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        FragmentManager fragmentManager;
        FragmentManager fragmentManager2;
        boolean z = false;
        boolean z2 = true;
        boolean z3 = message.arg1 == 1;
        int i = message.what;
        Object obj = null;
        if (i != 1) {
            if (i != 2) {
                z2 = false;
            } else {
                androidx.fragment.app.FragmentManager fragmentManager3 = (androidx.fragment.app.FragmentManager) message.obj;
                if (x(fragmentManager3, z3)) {
                    obj = this.g0.remove(fragmentManager3);
                    fragmentManager = fragmentManager3;
                    z = true;
                    fragmentManager2 = fragmentManager;
                }
            }
            fragmentManager2 = null;
        } else {
            FragmentManager fragmentManager4 = (FragmentManager) message.obj;
            if (w(fragmentManager4, z3)) {
                obj = this.f0.remove(fragmentManager4);
                fragmentManager = fragmentManager4;
                z = true;
                fragmentManager2 = fragmentManager;
            }
            fragmentManager2 = null;
        }
        if (Log.isLoggable("RMRetriever", 5) && z && obj == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to remove expected request manager fragment, manager: ");
            sb.append(fragmentManager2);
        }
        return z2;
    }

    @Deprecated
    public final k73 i(Context context, FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        RequestManagerFragment r = r(fragmentManager, fragment);
        k73 e = r.e();
        if (e == null) {
            e = this.i0.a(com.bumptech.glide.a.c(context), r.c(), r.f(), context);
            if (z) {
                e.b();
            }
            r.k(e);
        }
        return e;
    }

    public k73 j(Activity activity) {
        if (mg4.q()) {
            return l(activity.getApplicationContext());
        }
        if (activity instanceof FragmentActivity) {
            return o((FragmentActivity) activity);
        }
        a(activity);
        this.m0.a(activity);
        return i(activity, activity.getFragmentManager(), null, u(activity));
    }

    @TargetApi(17)
    @Deprecated
    public k73 k(android.app.Fragment fragment) {
        if (fragment.getActivity() != null) {
            if (!mg4.q() && Build.VERSION.SDK_INT >= 17) {
                if (fragment.getActivity() != null) {
                    this.m0.a(fragment.getActivity());
                }
                return i(fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
            }
            return l(fragment.getActivity().getApplicationContext());
        }
        throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
    }

    public k73 l(Context context) {
        if (context != null) {
            if (mg4.r() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return o((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return j((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return l(contextWrapper.getBaseContext());
                    }
                }
            }
            return p(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    public k73 m(View view) {
        if (mg4.q()) {
            return l(view.getContext().getApplicationContext());
        }
        wt2.d(view);
        wt2.e(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        Activity c = c(view.getContext());
        if (c == null) {
            return l(view.getContext().getApplicationContext());
        }
        if (c instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) c;
            Fragment h = h(view, fragmentActivity);
            return h != null ? n(h) : o(fragmentActivity);
        }
        android.app.Fragment g = g(view, c);
        if (g == null) {
            return j(c);
        }
        return k(g);
    }

    public k73 n(Fragment fragment) {
        wt2.e(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (mg4.q()) {
            return l(fragment.getContext().getApplicationContext());
        }
        if (fragment.getActivity() != null) {
            this.m0.a(fragment.getActivity());
        }
        return v(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    public k73 o(FragmentActivity fragmentActivity) {
        if (mg4.q()) {
            return l(fragmentActivity.getApplicationContext());
        }
        a(fragmentActivity);
        this.m0.a(fragmentActivity);
        return v(fragmentActivity, fragmentActivity.getSupportFragmentManager(), null, u(fragmentActivity));
    }

    public final k73 p(Context context) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = this.i0.a(com.bumptech.glide.a.c(context.getApplicationContext()), new ch(), new su0(), context.getApplicationContext());
                }
            }
        }
        return this.a;
    }

    @Deprecated
    public RequestManagerFragment q(Activity activity) {
        return r(activity.getFragmentManager(), null);
    }

    public final RequestManagerFragment r(FragmentManager fragmentManager, android.app.Fragment fragment) {
        RequestManagerFragment requestManagerFragment = this.f0.get(fragmentManager);
        if (requestManagerFragment == null) {
            RequestManagerFragment requestManagerFragment2 = (RequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
            if (requestManagerFragment2 == null) {
                requestManagerFragment2 = new RequestManagerFragment();
                requestManagerFragment2.j(fragment);
                this.f0.put(fragmentManager, requestManagerFragment2);
                fragmentManager.beginTransaction().add(requestManagerFragment2, "com.bumptech.glide.manager").commitAllowingStateLoss();
                this.h0.obtainMessage(1, fragmentManager).sendToTarget();
            }
            return requestManagerFragment2;
        }
        return requestManagerFragment;
    }

    public mw3 s(androidx.fragment.app.FragmentManager fragmentManager) {
        return t(fragmentManager, null);
    }

    public final mw3 t(androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment) {
        mw3 mw3Var = this.g0.get(fragmentManager);
        if (mw3Var == null) {
            mw3 mw3Var2 = (mw3) fragmentManager.k0("com.bumptech.glide.manager");
            if (mw3Var2 == null) {
                mw3Var2 = new mw3();
                mw3Var2.o(fragment);
                this.g0.put(fragmentManager, mw3Var2);
                fragmentManager.n().e(mw3Var2, "com.bumptech.glide.manager").k();
                this.h0.obtainMessage(2, fragmentManager).sendToTarget();
            }
            return mw3Var2;
        }
        return mw3Var;
    }

    public final k73 v(Context context, androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        mw3 t = t(fragmentManager, fragment);
        k73 i = t.i();
        if (i == null) {
            i = this.i0.a(com.bumptech.glide.a.c(context), t.g(), t.j(), context);
            if (z) {
                i.b();
            }
            t.p(i);
        }
        return i;
    }

    public final boolean w(FragmentManager fragmentManager, boolean z) {
        RequestManagerFragment requestManagerFragment = this.f0.get(fragmentManager);
        RequestManagerFragment requestManagerFragment2 = (RequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (requestManagerFragment2 == requestManagerFragment) {
            return true;
        }
        if (requestManagerFragment2 != null && requestManagerFragment2.e() != null) {
            throw new IllegalStateException("We've added two fragments with requests! Old: " + requestManagerFragment2 + " New: " + requestManagerFragment);
        } else if (!z && !fragmentManager.isDestroyed()) {
            FragmentTransaction add = fragmentManager.beginTransaction().add(requestManagerFragment, "com.bumptech.glide.manager");
            if (requestManagerFragment2 != null) {
                add.remove(requestManagerFragment2);
            }
            add.commitAllowingStateLoss();
            this.h0.obtainMessage(1, 1, 0, fragmentManager).sendToTarget();
            return false;
        } else {
            if (Log.isLoggable("RMRetriever", 5)) {
                fragmentManager.isDestroyed();
            }
            requestManagerFragment.c().c();
            return true;
        }
    }

    public final boolean x(androidx.fragment.app.FragmentManager fragmentManager, boolean z) {
        mw3 mw3Var = this.g0.get(fragmentManager);
        mw3 mw3Var2 = (mw3) fragmentManager.k0("com.bumptech.glide.manager");
        if (mw3Var2 == mw3Var) {
            return true;
        }
        if (mw3Var2 != null && mw3Var2.i() != null) {
            throw new IllegalStateException("We've added two fragments with requests! Old: " + mw3Var2 + " New: " + mw3Var);
        } else if (!z && !fragmentManager.G0()) {
            j e = fragmentManager.n().e(mw3Var, "com.bumptech.glide.manager");
            if (mw3Var2 != null) {
                e.r(mw3Var2);
            }
            e.m();
            this.h0.obtainMessage(2, 1, 0, fragmentManager).sendToTarget();
            return false;
        } else {
            fragmentManager.G0();
            mw3Var.g().c();
            return true;
        }
    }
}
