package defpackage;

import org.json.JSONObject;

/* compiled from: OSOutcomeEventsV2Service.kt */
/* renamed from: ok2  reason: default package */
/* loaded from: classes2.dex */
public final class ok2 extends gk2 {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ok2(wm2 wm2Var) {
        super(wm2Var);
        fs1.f(wm2Var, "client");
    }

    @Override // defpackage.co2
    public void a(JSONObject jSONObject, ym2 ym2Var) {
        fs1.f(jSONObject, "jsonObject");
        fs1.f(ym2Var, "responseHandler");
        b().a("outcomes/measure_sources", jSONObject, ym2Var);
    }
}
