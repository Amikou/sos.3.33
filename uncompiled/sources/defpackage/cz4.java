package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.PrintStream;
import java.util.Date;
import java.util.Random;

/* compiled from: uisPRNG.java */
/* renamed from: cz4  reason: default package */
/* loaded from: classes.dex */
public class cz4 {
    public static long a;
    public static int b;
    public static byte[] c = new byte[20];
    public static byte[] d = new byte[20];

    public cz4() {
        a = 0L;
        j();
        g(new byte[20]);
    }

    public static void a(byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        for (int i2 = 19; i2 >= 0; i2--) {
            int i3 = i + (bArr2[i2] >= 0 ? bArr2[i2] : bArr2[i2] + 256) + (bArr3[i2] >= 0 ? bArr3[i2] : bArr3[i2] + 256);
            bArr[i2] = (byte) i3;
            i = i3 >> 8;
        }
    }

    public static boolean b() {
        return a == 0;
    }

    public static void c(byte[] bArr) {
        boolean z;
        ha3 ha3Var = new ha3();
        ha3Var.n();
        ha3Var.d(c, 0, 20);
        ha3Var.d(new byte[64], 0, 44);
        ha3Var.m();
        if (ha3Var.f) {
            byte[] bArr2 = ha3Var.e;
            for (int i = 0; i < bArr2.length; i++) {
                bArr[i] = bArr2[i];
            }
        } else {
            for (int i2 = 0; i2 < 20; i2++) {
                bArr[i2] = 0;
            }
        }
        int i3 = 0;
        while (true) {
            if (i3 >= 20) {
                z = true;
                break;
            } else if (d[i3] != bArr[i3]) {
                z = false;
                break;
            } else {
                i3++;
            }
        }
        if (z) {
            a = st.c;
        }
        for (int i4 = 0; i4 < 20; i4++) {
            d[i4] = bArr[i4];
        }
        byte[] bArr3 = c;
        a(bArr3, bArr3, bArr, 1);
        ha3Var.j();
    }

    public static void d(byte[] bArr, int i) {
        ha3 ha3Var = new ha3();
        ha3Var.n();
        if (bArr == null || i < 1) {
            return;
        }
        ha3Var.d(bArr, 0, i);
        ha3Var.m();
        if (ha3Var.f) {
            byte[] bArr2 = ha3Var.e;
            for (int i2 = 0; i2 < bArr2.length; i2++) {
                c[i2] = bArr2[i2];
            }
        } else {
            for (int i3 = 0; i3 < 20; i3++) {
                c[i3] = 0;
            }
        }
        ha3Var.j();
    }

    public static byte[] e() {
        byte[] bArr = new byte[64];
        Random random = new Random(new Date().getTime());
        long nextLong = random.nextLong();
        int i = 0;
        for (int i2 = 0; i2 < 64; i2++) {
            bArr[i2] = (byte) ((nextLong >> i) & 255);
            i += 8;
            if (i == 56) {
                nextLong = random.nextLong();
                i = 0;
            }
        }
        return bArr;
    }

    public static long f() {
        return a;
    }

    public static boolean g(byte[] bArr) {
        if (b()) {
            c(bArr);
            if (b == 1) {
                PrintStream printStream = System.out;
                printStream.print("\nPRNG 84 GetRandomString=" + bArr.length + "\n");
                for (int i = 0; i < bArr.length; i++) {
                    PrintStream printStream2 = System.out;
                    printStream2.print(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + ((int) bArr[i]));
                }
                System.out.println("");
            }
        }
        return a == 0;
    }

    public static boolean h(byte[] bArr) {
        return i(bArr, bArr.length);
    }

    public static boolean i(byte[] bArr, int i) {
        if (b()) {
            if (b == 1) {
                PrintStream printStream = System.out;
                printStream.print("\n108 random=" + i + "\n");
                for (int i2 = 0; i2 < i; i2++) {
                    PrintStream printStream2 = System.out;
                    printStream2.print(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + ((int) bArr[i2]));
                }
                System.out.println("");
            }
            d(bArr, i);
        }
        return a == 0;
    }

    public void finalize() {
        j();
    }

    public void j() {
        for (int i = 0; i < 20; i++) {
            c[i] = 0;
            d[i] = 0;
        }
        byte[] e = e();
        i(e, e.length);
    }
}
