package defpackage;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import androidx.appcompat.widget.f;
import androidx.appcompat.widget.h;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: TintContextWrapper.java */
/* renamed from: j64  reason: default package */
/* loaded from: classes.dex */
public class j64 extends ContextWrapper {
    public static final Object c = new Object();
    public static ArrayList<WeakReference<j64>> d;
    public final Resources a;
    public final Resources.Theme b;

    public j64(Context context) {
        super(context);
        if (h.b()) {
            h hVar = new h(this, context.getResources());
            this.a = hVar;
            Resources.Theme newTheme = hVar.newTheme();
            this.b = newTheme;
            newTheme.setTo(context.getTheme());
            return;
        }
        this.a = new f(this, context.getResources());
        this.b = null;
    }

    public static boolean a(Context context) {
        if ((context instanceof j64) || (context.getResources() instanceof f) || (context.getResources() instanceof h)) {
            return false;
        }
        return Build.VERSION.SDK_INT < 21 || h.b();
    }

    public static Context b(Context context) {
        if (a(context)) {
            synchronized (c) {
                ArrayList<WeakReference<j64>> arrayList = d;
                if (arrayList == null) {
                    d = new ArrayList<>();
                } else {
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        WeakReference<j64> weakReference = d.get(size);
                        if (weakReference == null || weakReference.get() == null) {
                            d.remove(size);
                        }
                    }
                    for (int size2 = d.size() - 1; size2 >= 0; size2--) {
                        WeakReference<j64> weakReference2 = d.get(size2);
                        j64 j64Var = weakReference2 != null ? weakReference2.get() : null;
                        if (j64Var != null && j64Var.getBaseContext() == context) {
                            return j64Var;
                        }
                    }
                }
                j64 j64Var2 = new j64(context);
                d.add(new WeakReference<>(j64Var2));
                return j64Var2;
            }
        }
        return context;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public AssetManager getAssets() {
        return this.a.getAssets();
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Resources getResources() {
        return this.a;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        return theme == null ? super.getTheme() : theme;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public void setTheme(int i) {
        Resources.Theme theme = this.b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
