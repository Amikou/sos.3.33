package defpackage;

import android.view.View;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ViewBarcodeComponentLayoutBinding.java */
/* renamed from: zh4  reason: default package */
/* loaded from: classes2.dex */
public final class zh4 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final AppCompatImageView c;

    public zh4(RelativeLayout relativeLayout, MaterialButton materialButton, MaterialButton materialButton2, AppCompatImageView appCompatImageView) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = appCompatImageView;
    }

    public static zh4 a(View view) {
        int i = R.id.btnClear;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnClear);
        if (materialButton != null) {
            i = R.id.btnPaste;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnPaste);
            if (materialButton2 != null) {
                i = R.id.btnQr;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnQr);
                if (appCompatImageView != null) {
                    return new zh4((RelativeLayout) view, materialButton, materialButton2, appCompatImageView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
