package defpackage;

import org.slf4j.Marker;
import org.slf4j.event.Level;

/* compiled from: SubstituteLoggingEvent.java */
/* renamed from: pv3  reason: default package */
/* loaded from: classes2.dex */
public class pv3 implements b22 {
    public Marker a;
    public nv3 b;
    public Object[] c;

    public nv3 a() {
        return this.b;
    }

    public void b(Object[] objArr) {
        this.c = objArr;
    }

    public void c(Level level) {
    }

    public void d(nv3 nv3Var) {
        this.b = nv3Var;
    }

    public void e(String str) {
    }

    public void f(Marker marker) {
        this.a = marker;
    }

    public void g(String str) {
    }

    public void h(String str) {
    }

    public void i(Throwable th) {
    }

    public void j(long j) {
    }
}
