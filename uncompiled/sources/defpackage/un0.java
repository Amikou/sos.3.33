package defpackage;

import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: DialogPaySendwyreBinding.java */
/* renamed from: un0  reason: default package */
/* loaded from: classes2.dex */
public final class un0 {
    public final ConstraintLayout a;
    public final RelativeLayout b;
    public final WebView c;
    public final FrameLayout d;

    public un0(ConstraintLayout constraintLayout, RelativeLayout relativeLayout, WebView webView, FrameLayout frameLayout) {
        this.a = constraintLayout;
        this.b = relativeLayout;
        this.c = webView;
        this.d = frameLayout;
    }

    public static un0 a(View view) {
        int i = R.id.vMockPreLoader;
        RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.vMockPreLoader);
        if (relativeLayout != null) {
            i = R.id.wvContent;
            WebView webView = (WebView) ai4.a(view, R.id.wvContent);
            if (webView != null) {
                i = R.id.wvContentParent;
                FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.wvContentParent);
                if (frameLayout != null) {
                    return new un0((ConstraintLayout) view, relativeLayout, webView, frameLayout);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
