package defpackage;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: t56  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class t56 implements o56 {
    public static final o56 a = new t56();

    @Override // defpackage.o56
    public final Executor a(Executor executor) {
        return i56.b(executor);
    }
}
