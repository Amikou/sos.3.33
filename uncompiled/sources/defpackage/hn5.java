package defpackage;

import android.os.Binder;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: hn5  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class hn5 {
    public static <V> V a(kn5<V> kn5Var) {
        try {
            return kn5Var.zza();
        } catch (SecurityException unused) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return kn5Var.zza();
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }
}
