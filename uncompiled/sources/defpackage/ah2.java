package defpackage;

import android.app.NotificationManager;
import android.os.Parcelable;

/* compiled from: NotificationApiHelperForM.java */
/* renamed from: ah2  reason: default package */
/* loaded from: classes.dex */
public class ah2 {
    public static Parcelable[] a(NotificationManager notificationManager) {
        return notificationManager.getActiveNotifications();
    }
}
