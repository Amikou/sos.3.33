package defpackage;

import androidx.media3.exoplayer.upstream.b;
import java.io.IOException;
import java.util.List;

/* compiled from: ChunkSource.java */
/* renamed from: sy  reason: default package */
/* loaded from: classes.dex */
public interface sy {
    void a();

    void b() throws IOException;

    long c(long j, xi3 xi3Var);

    void d(my myVar);

    boolean e(long j, my myVar, List<? extends s52> list);

    void f(long j, long j2, List<? extends s52> list, oy oyVar);

    boolean g(my myVar, boolean z, b.c cVar, b bVar);

    int j(long j, List<? extends s52> list);
}
