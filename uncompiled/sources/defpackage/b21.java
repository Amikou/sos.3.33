package defpackage;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import java.util.Arrays;

/* compiled from: FadeDrawable.java */
/* renamed from: b21  reason: default package */
/* loaded from: classes.dex */
public class b21 extends nh {
    public boolean A0;
    public boolean B0;
    public final Drawable[] m0;
    public final boolean n0;
    public final int o0;
    public final int p0;
    public int q0;
    public int r0;
    public long s0;
    public int[] t0;
    public int[] u0;
    public int v0;
    public boolean[] w0;
    public int x0;
    public lm2 y0;
    public boolean z0;

    public b21(Drawable[] drawableArr, boolean z, int i) {
        super(drawableArr);
        this.B0 = true;
        xt2.j(drawableArr.length >= 1, "At least one layer required!");
        this.m0 = drawableArr;
        this.t0 = new int[drawableArr.length];
        this.u0 = new int[drawableArr.length];
        this.v0 = 255;
        this.w0 = new boolean[drawableArr.length];
        this.x0 = 0;
        this.n0 = z;
        this.o0 = z ? 255 : 0;
        this.p0 = i;
        t();
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0056 A[LOOP:0: B:25:0x0051->B:27:0x0056, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0073  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x007a  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x0071 A[EDGE_INSN: B:32:0x0071->B:28:0x0071 ?: BREAK  , SYNTHETIC] */
    @Override // defpackage.nh, android.graphics.drawable.Drawable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void draw(android.graphics.Canvas r9) {
        /*
            r8 = this;
            int r0 = r8.q0
            r1 = 2
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L2b
            if (r0 == r3) goto La
            goto L51
        La:
            int r0 = r8.r0
            if (r0 <= 0) goto L10
            r0 = r3
            goto L11
        L10:
            r0 = r2
        L11:
            defpackage.xt2.i(r0)
            long r4 = r8.p()
            long r6 = r8.s0
            long r4 = r4 - r6
            float r0 = (float) r4
            int r4 = r8.r0
            float r4 = (float) r4
            float r0 = r0 / r4
            boolean r0 = r8.w(r0)
            if (r0 == 0) goto L27
            goto L28
        L27:
            r1 = r3
        L28:
            r8.q0 = r1
            goto L50
        L2b:
            int[] r0 = r8.u0
            int[] r4 = r8.t0
            android.graphics.drawable.Drawable[] r5 = r8.m0
            int r5 = r5.length
            java.lang.System.arraycopy(r0, r2, r4, r2, r5)
            long r4 = r8.p()
            r8.s0 = r4
            int r0 = r8.r0
            if (r0 != 0) goto L42
            r0 = 1065353216(0x3f800000, float:1.0)
            goto L43
        L42:
            r0 = 0
        L43:
            boolean r0 = r8.w(r0)
            r8.r()
            if (r0 == 0) goto L4d
            goto L4e
        L4d:
            r1 = r3
        L4e:
            r8.q0 = r1
        L50:
            r3 = r0
        L51:
            android.graphics.drawable.Drawable[] r0 = r8.m0
            int r1 = r0.length
            if (r2 >= r1) goto L71
            r0 = r0[r2]
            int[] r1 = r8.u0
            r1 = r1[r2]
            int r4 = r8.v0
            int r1 = r1 * r4
            double r4 = (double) r1
            r6 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r4 = r4 / r6
            double r4 = java.lang.Math.ceil(r4)
            int r1 = (int) r4
            r8.i(r9, r0, r1)
            int r2 = r2 + 1
            goto L51
        L71:
            if (r3 == 0) goto L7a
            r8.q()
            r8.s()
            goto L7d
        L7a:
            r8.invalidateSelf()
        L7d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b21.draw(android.graphics.Canvas):void");
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.v0;
    }

    public void h() {
        this.x0++;
    }

    public final void i(Canvas canvas, Drawable drawable, int i) {
        if (drawable == null || i <= 0) {
            return;
        }
        this.x0++;
        if (this.B0) {
            drawable.mutate();
        }
        drawable.setAlpha(i);
        this.x0--;
        drawable.draw(canvas);
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        if (this.x0 == 0) {
            super.invalidateSelf();
        }
    }

    public void k() {
        this.x0--;
        invalidateSelf();
    }

    public void l() {
        this.q0 = 0;
        Arrays.fill(this.w0, true);
        invalidateSelf();
    }

    public void m(int i) {
        this.q0 = 0;
        this.w0[i] = true;
        invalidateSelf();
    }

    public void n(int i) {
        this.q0 = 0;
        this.w0[i] = false;
        invalidateSelf();
    }

    public void o() {
        this.q0 = 2;
        for (int i = 0; i < this.m0.length; i++) {
            this.u0[i] = this.w0[i] ? 255 : 0;
        }
        invalidateSelf();
    }

    public long p() {
        return SystemClock.uptimeMillis();
    }

    public final void q() {
        if (this.z0) {
            this.z0 = false;
            lm2 lm2Var = this.y0;
            if (lm2Var != null) {
                lm2Var.c();
            }
        }
    }

    public final void r() {
        int i;
        if (!this.z0 && (i = this.p0) >= 0) {
            boolean[] zArr = this.w0;
            if (i < zArr.length && zArr[i]) {
                this.z0 = true;
                lm2 lm2Var = this.y0;
                if (lm2Var != null) {
                    lm2Var.a();
                }
            }
        }
    }

    public final void s() {
        if (this.A0 && this.q0 == 2 && this.w0[this.p0]) {
            lm2 lm2Var = this.y0;
            if (lm2Var != null) {
                lm2Var.b();
            }
            this.A0 = false;
        }
    }

    @Override // defpackage.nh, android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        if (this.v0 != i) {
            this.v0 = i;
            invalidateSelf();
        }
    }

    public final void t() {
        this.q0 = 2;
        Arrays.fill(this.t0, this.o0);
        this.t0[0] = 255;
        Arrays.fill(this.u0, this.o0);
        this.u0[0] = 255;
        Arrays.fill(this.w0, this.n0);
        this.w0[0] = true;
    }

    public void u(lm2 lm2Var) {
        this.y0 = lm2Var;
    }

    public void v(int i) {
        this.r0 = i;
        if (this.q0 == 1) {
            this.q0 = 0;
        }
    }

    public final boolean w(float f) {
        boolean z = true;
        for (int i = 0; i < this.m0.length; i++) {
            boolean[] zArr = this.w0;
            int i2 = zArr[i] ? 1 : -1;
            int[] iArr = this.u0;
            iArr[i] = (int) (this.t0[i] + (i2 * 255 * f));
            if (iArr[i] < 0) {
                iArr[i] = 0;
            }
            if (iArr[i] > 255) {
                iArr[i] = 255;
            }
            if (zArr[i] && iArr[i] < 255) {
                z = false;
            }
            if (!zArr[i] && iArr[i] > 0) {
                z = false;
            }
        }
        return z;
    }
}
