package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: s65  reason: default package */
/* loaded from: classes.dex */
public final class s65 {
    public final Map<String, o65> a = new HashMap();
    public final r85 b = new r85();

    public s65() {
        a(new l65());
        a(new t65());
        a(new w65());
        a(new z75());
        a(new o85());
        a(new q85());
        a(new v85());
    }

    public final void a(o65 o65Var) {
        for (zzbl zzblVar : o65Var.a) {
            this.a.put(zzblVar.zzb().toString(), o65Var);
        }
    }

    public final z55 b(wk5 wk5Var, z55 z55Var) {
        o65 o65Var;
        vm5.k(wk5Var);
        if (z55Var instanceof a65) {
            a65 a65Var = (a65) z55Var;
            ArrayList<z55> d = a65Var.d();
            String a = a65Var.a();
            if (this.a.containsKey(a)) {
                o65Var = this.a.get(a);
            } else {
                o65Var = this.b;
            }
            return o65Var.a(a, wk5Var, d);
        }
        return z55Var;
    }
}
