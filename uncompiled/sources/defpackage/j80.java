package defpackage;

import java.math.BigInteger;

/* compiled from: ContractGasProvider.java */
/* renamed from: j80  reason: default package */
/* loaded from: classes3.dex */
public interface j80 {
    @Deprecated
    BigInteger getGasLimit();

    BigInteger getGasLimit(String str);

    @Deprecated
    BigInteger getGasPrice();

    BigInteger getGasPrice(String str);
}
