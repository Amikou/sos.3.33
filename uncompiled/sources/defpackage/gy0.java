package defpackage;

/* compiled from: EventStoreModule_DbNameFactory.java */
/* renamed from: gy0  reason: default package */
/* loaded from: classes.dex */
public final class gy0 implements z11<String> {

    /* compiled from: EventStoreModule_DbNameFactory.java */
    /* renamed from: gy0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final gy0 a = new gy0();
    }

    public static gy0 a() {
        return a.a;
    }

    public static String b() {
        return (String) yt2.c(fy0.a(), "Cannot return null from a non-@Nullable @Provides method");
    }

    @Override // defpackage.ew2
    /* renamed from: c */
    public String get() {
        return b();
    }
}
