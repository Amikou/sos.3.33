package defpackage;

/* compiled from: OSOutcomeEventsFactory.kt */
/* renamed from: hk2  reason: default package */
/* loaded from: classes2.dex */
public final class hk2 {
    public final fk2 a;
    public ik2 b;
    public final yj2 c;
    public final wm2 d;

    public hk2(yj2 yj2Var, wm2 wm2Var, bn2 bn2Var, vk2 vk2Var) {
        fs1.f(yj2Var, "logger");
        fs1.f(wm2Var, "apiClient");
        this.c = yj2Var;
        this.d = wm2Var;
        fs1.d(bn2Var);
        fs1.d(vk2Var);
        this.a = new fk2(yj2Var, bn2Var, vk2Var);
    }

    public final jk2 a() {
        if (this.a.j()) {
            return new nk2(this.c, this.a, new ok2(this.d));
        }
        return new lk2(this.c, this.a, new mk2(this.d));
    }

    public final ik2 b() {
        return this.b != null ? c() : a();
    }

    public final ik2 c() {
        if (!this.a.j()) {
            ik2 ik2Var = this.b;
            if (ik2Var instanceof lk2) {
                fs1.d(ik2Var);
                return ik2Var;
            }
        }
        if (this.a.j()) {
            ik2 ik2Var2 = this.b;
            if (ik2Var2 instanceof nk2) {
                fs1.d(ik2Var2);
                return ik2Var2;
            }
        }
        return a();
    }
}
