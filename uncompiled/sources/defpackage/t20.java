package defpackage;

import android.animation.ArgbEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;

/* compiled from: ColorAnimation.java */
/* renamed from: t20  reason: default package */
/* loaded from: classes2.dex */
public class t20 extends nm<ValueAnimator> {
    public u20 d;
    public int e;
    public int f;

    /* compiled from: ColorAnimation.java */
    /* renamed from: t20$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            t20.this.j(valueAnimator);
        }
    }

    public t20(xg4.a aVar) {
        super(aVar);
        this.d = new u20();
    }

    @Override // defpackage.nm
    /* renamed from: g */
    public ValueAnimator a() {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(350L);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new a());
        return valueAnimator;
    }

    public PropertyValuesHolder h(boolean z) {
        int i;
        int i2;
        String str;
        if (z) {
            i = this.f;
            i2 = this.e;
            str = "ANIMATION_COLOR_REVERSE";
        } else {
            i = this.e;
            i2 = this.f;
            str = "ANIMATION_COLOR";
        }
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt(str, i, i2);
        ofInt.setEvaluator(new ArgbEvaluator());
        return ofInt;
    }

    public final boolean i(int i, int i2) {
        return (this.e == i && this.f == i2) ? false : true;
    }

    public final void j(ValueAnimator valueAnimator) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COLOR")).intValue();
        int intValue2 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COLOR_REVERSE")).intValue();
        this.d.c(intValue);
        this.d.d(intValue2);
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.d);
        }
    }

    @Override // defpackage.nm
    /* renamed from: k */
    public t20 d(float f) {
        T t = this.c;
        if (t != 0) {
            long j = f * ((float) this.a);
            if (((ValueAnimator) t).getValues() != null && ((ValueAnimator) this.c).getValues().length > 0) {
                ((ValueAnimator) this.c).setCurrentPlayTime(j);
            }
        }
        return this;
    }

    public t20 l(int i, int i2) {
        if (this.c != 0 && i(i, i2)) {
            this.e = i;
            this.f = i2;
            ((ValueAnimator) this.c).setValues(h(false), h(true));
        }
        return this;
    }
}
