package defpackage;

import com.google.android.gms.internal.measurement.zzhz;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: yn5  reason: default package */
/* loaded from: classes.dex */
public final class yn5 {
    public static volatile zzhz<Boolean> a = zzhz.zzc();
    public static final Object b = new Object();

    /* JADX WARN: Can't wrap try/catch for region: R(11:18|(6:33|(2:35|(1:37))|27|28|29|30)(1:20)|21|22|23|24|(1:26)|27|28|29|30) */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean a(android.content.Context r3, android.net.Uri r4) {
        /*
            java.lang.String r4 = r4.getAuthority()
            java.lang.String r0 = "com.google.android.gms.phenotype"
            boolean r0 = r0.equals(r4)
            r1 = 0
            if (r0 != 0) goto L25
            java.lang.String r3 = java.lang.String.valueOf(r4)
            int r3 = r3.length()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            int r3 = r3 + 91
            r0.<init>(r3)
            r0.append(r4)
            java.lang.String r3 = " is an unsupported authority. Only com.google.android.gms.phenotype authority is supported."
            r0.append(r3)
            return r1
        L25:
            com.google.android.gms.internal.measurement.zzhz<java.lang.Boolean> r4 = defpackage.yn5.a
            boolean r4 = r4.zza()
            if (r4 == 0) goto L3a
            com.google.android.gms.internal.measurement.zzhz<java.lang.Boolean> r3 = defpackage.yn5.a
            java.lang.Object r3 = r3.zzb()
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            return r3
        L3a:
            java.lang.Object r4 = defpackage.yn5.b
            monitor-enter(r4)
            com.google.android.gms.internal.measurement.zzhz<java.lang.Boolean> r0 = defpackage.yn5.a     // Catch: java.lang.Throwable -> La0
            boolean r0 = r0.zza()     // Catch: java.lang.Throwable -> La0
            if (r0 == 0) goto L53
            com.google.android.gms.internal.measurement.zzhz<java.lang.Boolean> r3 = defpackage.yn5.a     // Catch: java.lang.Throwable -> La0
            java.lang.Object r3 = r3.zzb()     // Catch: java.lang.Throwable -> La0
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch: java.lang.Throwable -> La0
            boolean r3 = r3.booleanValue()     // Catch: java.lang.Throwable -> La0
            monitor-exit(r4)     // Catch: java.lang.Throwable -> La0
            return r3
        L53:
            java.lang.String r0 = "com.google.android.gms"
            java.lang.String r2 = r3.getPackageName()     // Catch: java.lang.Throwable -> La0
            boolean r0 = r0.equals(r2)     // Catch: java.lang.Throwable -> La0
            if (r0 == 0) goto L60
            goto L77
        L60:
            android.content.pm.PackageManager r0 = r3.getPackageManager()     // Catch: java.lang.Throwable -> La0
            java.lang.String r2 = "com.google.android.gms.phenotype"
            android.content.pm.ProviderInfo r0 = r0.resolveContentProvider(r2, r1)     // Catch: java.lang.Throwable -> La0
            if (r0 == 0) goto L88
            java.lang.String r2 = "com.google.android.gms"
            java.lang.String r0 = r0.packageName     // Catch: java.lang.Throwable -> La0
            boolean r0 = r2.equals(r0)     // Catch: java.lang.Throwable -> La0
            if (r0 != 0) goto L77
            goto L88
        L77:
            android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch: java.lang.Throwable -> La0
            java.lang.String r0 = "com.google.android.gms"
            android.content.pm.ApplicationInfo r3 = r3.getApplicationInfo(r0, r1)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L88 java.lang.Throwable -> La0
            int r3 = r3.flags     // Catch: java.lang.Throwable -> La0
            r3 = r3 & 129(0x81, float:1.81E-43)
            if (r3 == 0) goto L88
            r1 = 1
        L88:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r1)     // Catch: java.lang.Throwable -> La0
            com.google.android.gms.internal.measurement.zzhz r3 = com.google.android.gms.internal.measurement.zzhz.zzd(r3)     // Catch: java.lang.Throwable -> La0
            defpackage.yn5.a = r3     // Catch: java.lang.Throwable -> La0
            monitor-exit(r4)     // Catch: java.lang.Throwable -> La0
            com.google.android.gms.internal.measurement.zzhz<java.lang.Boolean> r3 = defpackage.yn5.a
            java.lang.Object r3 = r3.zzb()
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            return r3
        La0:
            r3 = move-exception
            monitor-exit(r4)     // Catch: java.lang.Throwable -> La0
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.yn5.a(android.content.Context, android.net.Uri):boolean");
    }
}
