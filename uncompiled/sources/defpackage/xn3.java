package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.crypto.tink.j;
import com.google.crypto.tink.proto.t;
import com.google.crypto.tink.proto.y;
import com.google.crypto.tink.shaded.protobuf.n;
import java.io.CharConversionException;
import java.io.FileNotFoundException;
import java.io.IOException;

/* compiled from: SharedPrefKeysetReader.java */
/* renamed from: xn3  reason: default package */
/* loaded from: classes2.dex */
public final class xn3 implements j {
    public final SharedPreferences a;
    public final String b;

    public xn3(Context context, String str, String str2) throws IOException {
        if (str != null) {
            this.b = str;
            Context applicationContext = context.getApplicationContext();
            if (str2 == null) {
                this.a = PreferenceManager.getDefaultSharedPreferences(applicationContext);
                return;
            } else {
                this.a = applicationContext.getSharedPreferences(str2, 0);
                return;
            }
        }
        throw new IllegalArgumentException("keysetName cannot be null");
    }

    @Override // com.google.crypto.tink.j
    public t a() throws IOException {
        return t.I(b(), n.b());
    }

    public final byte[] b() throws IOException {
        try {
            String string = this.a.getString(this.b, null);
            if (string != null) {
                return ok1.a(string);
            }
            throw new FileNotFoundException(String.format("can't read keyset; the pref value %s does not exist", this.b));
        } catch (ClassCastException | IllegalArgumentException unused) {
            throw new CharConversionException(String.format("can't read keyset; the pref value %s is not a valid hex string", this.b));
        }
    }

    @Override // com.google.crypto.tink.j
    public y read() throws IOException {
        return y.N(b(), n.b());
    }
}
