package defpackage;

import com.google.android.play.core.internal.d;
import java.io.File;
import java.util.List;

/* renamed from: uu4  reason: default package */
/* loaded from: classes2.dex */
public final class uu4 implements tu4 {
    public final /* synthetic */ int a = 0;

    public uu4() {
    }

    public uu4(byte[] bArr) {
    }

    @Override // defpackage.tu4
    public final Object[] a(Object obj, List list, List list2) {
        return (Object[]) (this.a != 0 ? d.b(obj, "makePathElements", Object[].class, List.class, list, File.class, null, List.class, list2) : d.a(obj, "makePathElements", Object[].class, List.class, list));
    }
}
