package defpackage;

/* renamed from: ex4  reason: default package */
/* loaded from: classes2.dex */
public final class ex4 extends fw4 {
    public final int c;
    public final long d;
    public final String e;
    public final String f;

    public ex4(int i, String str, int i2, long j, String str2, String str3) {
        super(i, str);
        this.c = i2;
        this.d = j;
        this.e = str2;
        this.f = str3;
    }
}
