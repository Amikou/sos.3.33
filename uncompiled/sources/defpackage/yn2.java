package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import com.facebook.imagepipeline.platform.a;

/* compiled from: OreoDecoder.java */
@TargetApi(26)
/* renamed from: yn2  reason: default package */
/* loaded from: classes.dex */
public class yn2 extends a {
    public yn2(iq iqVar, int i, it2 it2Var) {
        super(iqVar, i, it2Var);
    }

    public static boolean g(BitmapFactory.Options options) {
        ColorSpace colorSpace = options.outColorSpace;
        return (colorSpace == null || !colorSpace.isWideGamut() || options.inPreferredConfig == Bitmap.Config.RGBA_F16) ? false : true;
    }

    @Override // com.facebook.imagepipeline.platform.a
    public int e(int i, int i2, BitmapFactory.Options options) {
        if (g(options)) {
            return i * i2 * 8;
        }
        Bitmap.Config config = options.inPreferredConfig;
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        return rq.d(i, i2, config);
    }
}
