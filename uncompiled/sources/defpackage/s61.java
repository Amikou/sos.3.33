package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.extractor.metadata.flac.PictureFrame;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: FlacStreamMetadata.java */
/* renamed from: s61  reason: default package */
/* loaded from: classes.dex */
public final class s61 {
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;
    public final int h;
    public final int i;
    public final long j;
    public final a k;
    public final Metadata l;

    /* compiled from: FlacStreamMetadata.java */
    /* renamed from: s61$a */
    /* loaded from: classes.dex */
    public static class a {
        public final long[] a;
        public final long[] b;

        public a(long[] jArr, long[] jArr2) {
            this.a = jArr;
            this.b = jArr2;
        }
    }

    public s61(byte[] bArr, int i) {
        np2 np2Var = new np2(bArr);
        np2Var.p(i * 8);
        this.a = np2Var.h(16);
        this.b = np2Var.h(16);
        this.c = np2Var.h(24);
        this.d = np2Var.h(24);
        int h = np2Var.h(20);
        this.e = h;
        this.f = j(h);
        this.g = np2Var.h(3) + 1;
        int h2 = np2Var.h(5) + 1;
        this.h = h2;
        this.i = e(h2);
        this.j = np2Var.j(36);
        this.k = null;
        this.l = null;
    }

    public static int e(int i) {
        if (i != 8) {
            if (i != 12) {
                if (i != 16) {
                    if (i != 20) {
                        return i != 24 ? -1 : 6;
                    }
                    return 5;
                }
                return 4;
            }
            return 2;
        }
        return 1;
    }

    public static int j(int i) {
        switch (i) {
            case 8000:
                return 4;
            case 16000:
                return 5;
            case 22050:
                return 6;
            case 24000:
                return 7;
            case 32000:
                return 8;
            case 44100:
                return 9;
            case 48000:
                return 10;
            case 88200:
                return 1;
            case 96000:
                return 11;
            case 176400:
                return 2;
            case 192000:
                return 3;
            default:
                return -1;
        }
    }

    public s61 a(List<PictureFrame> list) {
        return new s61(this.a, this.b, this.c, this.d, this.e, this.g, this.h, this.j, this.k, h(new Metadata(list)));
    }

    public s61 b(a aVar) {
        return new s61(this.a, this.b, this.c, this.d, this.e, this.g, this.h, this.j, aVar, this.l);
    }

    public s61 c(List<String> list) {
        return new s61(this.a, this.b, this.c, this.d, this.e, this.g, this.h, this.j, this.k, h(dl4.c(list)));
    }

    public long d() {
        long j;
        long j2;
        int i = this.d;
        if (i > 0) {
            j = (i + this.c) / 2;
            j2 = 1;
        } else {
            int i2 = this.a;
            j = ((((i2 != this.b || i2 <= 0) ? 4096L : i2) * this.g) * this.h) / 8;
            j2 = 64;
        }
        return j + j2;
    }

    public long f() {
        long j = this.j;
        return j == 0 ? CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED : (j * 1000000) / this.e;
    }

    public j g(byte[] bArr, Metadata metadata) {
        bArr[4] = Byte.MIN_VALUE;
        int i = this.d;
        if (i <= 0) {
            i = -1;
        }
        return new j.b().e0("audio/flac").W(i).H(this.g).f0(this.e).T(Collections.singletonList(bArr)).X(h(metadata)).E();
    }

    public Metadata h(Metadata metadata) {
        Metadata metadata2 = this.l;
        return metadata2 == null ? metadata : metadata2.b(metadata);
    }

    public long i(long j) {
        return b.r((j * this.e) / 1000000, 0L, this.j - 1);
    }

    public s61(int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, a aVar, Metadata metadata) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = j(i5);
        this.g = i6;
        this.h = i7;
        this.i = e(i7);
        this.j = j;
        this.k = aVar;
        this.l = metadata;
    }
}
