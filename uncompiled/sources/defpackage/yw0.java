package defpackage;

import java.math.BigInteger;

/* compiled from: EthGetTransactionCount.java */
/* renamed from: yw0  reason: default package */
/* loaded from: classes3.dex */
public class yw0 extends i83<String> {
    public BigInteger getTransactionCount() {
        return ej2.decodeQuantity(getResult());
    }
}
