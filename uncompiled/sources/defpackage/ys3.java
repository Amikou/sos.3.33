package defpackage;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.flow.StateFlowImpl;

/* compiled from: StateFlow.kt */
/* renamed from: ys3  reason: default package */
/* loaded from: classes2.dex */
public final class ys3 extends k5<StateFlowImpl<?>> {
    public static final /* synthetic */ AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(ys3.class, Object.class, "_state");
    public volatile /* synthetic */ Object _state = null;

    @Override // defpackage.k5
    /* renamed from: c */
    public boolean a(StateFlowImpl<?> stateFlowImpl) {
        k24 k24Var;
        if (this._state != null) {
            return false;
        }
        k24Var = xs3.a;
        this._state = k24Var;
        return true;
    }

    public final Object d(q70<? super te4> q70Var) {
        k24 k24Var;
        k24 k24Var2;
        pv pvVar = new pv(IntrinsicsKt__IntrinsicsJvmKt.c(q70Var), 1);
        pvVar.A();
        if (!ze0.a() || hr.a(!(this._state instanceof pv)).booleanValue()) {
            AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
            k24Var = xs3.a;
            if (!atomicReferenceFieldUpdater.compareAndSet(this, k24Var, pvVar)) {
                if (ze0.a()) {
                    Object obj = this._state;
                    k24Var2 = xs3.b;
                    if (!hr.a(obj == k24Var2).booleanValue()) {
                        throw new AssertionError();
                    }
                }
                te4 te4Var = te4.a;
                Result.a aVar = Result.Companion;
                pvVar.resumeWith(Result.m52constructorimpl(te4Var));
            }
            Object x = pvVar.x();
            if (x == gs1.d()) {
                ef0.c(q70Var);
            }
            return x == gs1.d() ? x : te4.a;
        }
        throw new AssertionError();
    }

    @Override // defpackage.k5
    /* renamed from: e */
    public Continuation<Unit>[] b(StateFlowImpl<?> stateFlowImpl) {
        this._state = null;
        return j5.a;
    }

    public final void f() {
        k24 k24Var;
        k24 k24Var2;
        k24 k24Var3;
        k24 k24Var4;
        while (true) {
            Object obj = this._state;
            if (obj == null) {
                return;
            }
            k24Var = xs3.b;
            if (obj == k24Var) {
                return;
            }
            k24Var2 = xs3.a;
            if (obj == k24Var2) {
                AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
                k24Var3 = xs3.b;
                if (atomicReferenceFieldUpdater.compareAndSet(this, obj, k24Var3)) {
                    return;
                }
            } else {
                AtomicReferenceFieldUpdater atomicReferenceFieldUpdater2 = a;
                k24Var4 = xs3.a;
                if (atomicReferenceFieldUpdater2.compareAndSet(this, obj, k24Var4)) {
                    te4 te4Var = te4.a;
                    Result.a aVar = Result.Companion;
                    ((pv) obj).resumeWith(Result.m52constructorimpl(te4Var));
                    return;
                }
            }
        }
    }

    public final boolean g() {
        k24 k24Var;
        k24 k24Var2;
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
        k24Var = xs3.a;
        Object andSet = atomicReferenceFieldUpdater.getAndSet(this, k24Var);
        fs1.d(andSet);
        if (!ze0.a() || (!(andSet instanceof pv))) {
            k24Var2 = xs3.b;
            return andSet == k24Var2;
        }
        throw new AssertionError();
    }
}
