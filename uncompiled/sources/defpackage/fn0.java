package defpackage;

import android.view.View;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAnchorPopUpBagBinding.java */
/* renamed from: fn0  reason: default package */
/* loaded from: classes2.dex */
public final class fn0 {
    public final CardView a;
    public final CardView b;
    public final aq1 c;
    public final zp1 d;

    public fn0(ConstraintLayout constraintLayout, CardView cardView, CardView cardView2, aq1 aq1Var, zp1 zp1Var) {
        this.a = cardView;
        this.b = cardView2;
        this.c = aq1Var;
        this.d = zp1Var;
    }

    public static fn0 a(View view) {
        int i = R.id.ccManageBagItemWrapper;
        CardView cardView = (CardView) ai4.a(view, R.id.ccManageBagItemWrapper);
        if (cardView != null) {
            i = R.id.ccToggleItemWrapper;
            CardView cardView2 = (CardView) ai4.a(view, R.id.ccToggleItemWrapper);
            if (cardView2 != null) {
                i = R.id.includeBagPercentage;
                View a = ai4.a(view, R.id.includeBagPercentage);
                if (a != null) {
                    aq1 a2 = aq1.a(a);
                    i = R.id.includeMenuItem;
                    View a3 = ai4.a(view, R.id.includeMenuItem);
                    if (a3 != null) {
                        return new fn0((ConstraintLayout) view, cardView, cardView2, a2, zp1.a(a3));
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
