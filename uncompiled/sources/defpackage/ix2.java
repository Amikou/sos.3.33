package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: QueryInterceptorProgram.java */
/* renamed from: ix2  reason: default package */
/* loaded from: classes.dex */
public final class ix2 implements uw3 {
    public List<Object> a = new ArrayList();

    @Override // defpackage.uw3
    public void A0(int i, byte[] bArr) {
        b(i, bArr);
    }

    @Override // defpackage.uw3
    public void L(int i, String str) {
        b(i, str);
    }

    @Override // defpackage.uw3
    public void Y(int i, double d) {
        b(i, Double.valueOf(d));
    }

    @Override // defpackage.uw3
    public void Y0(int i) {
        b(i, null);
    }

    public List<Object> a() {
        return this.a;
    }

    public final void b(int i, Object obj) {
        int i2 = i - 1;
        if (i2 >= this.a.size()) {
            for (int size = this.a.size(); size <= i2; size++) {
                this.a.add(null);
            }
        }
        this.a.set(i2, obj);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // defpackage.uw3
    public void q0(int i, long j) {
        b(i, Long.valueOf(j));
    }
}
