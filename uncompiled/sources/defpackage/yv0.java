package defpackage;

import androidx.room.RoomDatabase;

/* compiled from: EntityDeletionOrUpdateAdapter.java */
/* renamed from: yv0  reason: default package */
/* loaded from: classes.dex */
public abstract class yv0<T> extends co3 {
    public yv0(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    public abstract void g(ww3 ww3Var, T t);

    public final int h(T t) {
        ww3 a = a();
        try {
            g(a, t);
            return a.T();
        } finally {
            f(a);
        }
    }
}
