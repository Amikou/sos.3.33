package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: m56  reason: default package */
/* loaded from: classes.dex */
public final class m56 extends c55 {
    public m56(w56 w56Var, String str) {
        super("isAndroid");
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        return z55.c0;
    }
}
