package defpackage;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* compiled from: RippleDrawableCompat.java */
/* renamed from: a93  reason: default package */
/* loaded from: classes2.dex */
public class a93 extends Drawable implements sn3, i64 {
    public b a;

    @Override // android.graphics.drawable.Drawable
    /* renamed from: a */
    public a93 mutate() {
        this.a = new b(this.a);
        return this;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        b bVar = this.a;
        if (bVar.b) {
            bVar.a.draw(canvas);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.a.a.getOpacity();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        return true;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.a.a.setBounds(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        if (this.a.a.setState(iArr)) {
            onStateChange = true;
        }
        boolean e = b93.e(iArr);
        b bVar = this.a;
        if (bVar.b != e) {
            bVar.b = e;
            return true;
        }
        return onStateChange;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.a.a.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.a.setColorFilter(colorFilter);
    }

    @Override // defpackage.sn3
    public void setShapeAppearanceModel(pn3 pn3Var) {
        this.a.a.setShapeAppearanceModel(pn3Var);
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTint(int i) {
        this.a.a.setTint(i);
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTintList(ColorStateList colorStateList) {
        this.a.a.setTintList(colorStateList);
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTintMode(PorterDuff.Mode mode) {
        this.a.a.setTintMode(mode);
    }

    public a93(pn3 pn3Var) {
        this(new b(new o42(pn3Var)));
    }

    /* compiled from: RippleDrawableCompat.java */
    /* renamed from: a93$b */
    /* loaded from: classes2.dex */
    public static final class b extends Drawable.ConstantState {
        public o42 a;
        public boolean b;

        public b(o42 o42Var) {
            this.a = o42Var;
            this.b = false;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        /* renamed from: a */
        public a93 newDrawable() {
            return new a93(new b(this));
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public int getChangingConfigurations() {
            return 0;
        }

        public b(b bVar) {
            this.a = (o42) bVar.a.getConstantState().newDrawable();
            this.b = bVar.b;
        }
    }

    public a93(b bVar) {
        this.a = bVar;
    }
}
