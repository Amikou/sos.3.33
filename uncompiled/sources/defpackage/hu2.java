package defpackage;

/* compiled from: Preference.java */
/* renamed from: hu2  reason: default package */
/* loaded from: classes.dex */
public class hu2 {
    public String a;
    public Long b;

    public hu2(String str, boolean z) {
        this(str, z ? 1L : 0L);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof hu2) {
            hu2 hu2Var = (hu2) obj;
            if (this.a.equals(hu2Var.a)) {
                Long l = this.b;
                Long l2 = hu2Var.b;
                return l != null ? l.equals(l2) : l2 == null;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        Long l = this.b;
        return hashCode + (l != null ? l.hashCode() : 0);
    }

    public hu2(String str, long j) {
        this.a = str;
        this.b = Long.valueOf(j);
    }
}
