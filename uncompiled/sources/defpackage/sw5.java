package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.internal.measurement.m;
import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzas;
import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: sw5  reason: default package */
/* loaded from: classes.dex */
public final class sw5 extends sl5 {
    public static final String[] g = {"firebase_", "google_", "ga_"};
    public static final String[] h = {"_err"};
    public static final /* synthetic */ int i = 0;
    public SecureRandom c;
    public final AtomicLong d;
    public int e;
    public Integer f;

    public sw5(ck5 ck5Var) {
        super(ck5Var);
        this.f = null;
        this.d = new AtomicLong(0L);
    }

    public static MessageDigest B() {
        MessageDigest messageDigest;
        for (int i2 = 0; i2 < 2; i2++) {
            try {
                messageDigest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException unused) {
            }
            if (messageDigest != null) {
                return messageDigest;
            }
        }
        return null;
    }

    public static long C(byte[] bArr) {
        zt2.j(bArr);
        int length = bArr.length;
        int i2 = 0;
        zt2.m(length > 0);
        long j = 0;
        for (int i3 = length - 1; i3 >= 0 && i3 >= bArr.length - 8; i3--) {
            j += (bArr[i3] & 255) << i2;
            i2 += 8;
        }
        return j;
    }

    public static boolean D(Context context, boolean z) {
        zt2.j(context);
        if (Build.VERSION.SDK_INT >= 24) {
            return f0(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return f0(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    public static boolean F(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("_");
    }

    public static boolean G(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    public static boolean M(String str) {
        return !h[0].equals(str);
    }

    public static ArrayList<Bundle> Y(List<zzaa> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (zzaa zzaaVar : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", zzaaVar.a);
            bundle.putString("origin", zzaaVar.f0);
            bundle.putLong("creation_timestamp", zzaaVar.h0);
            bundle.putString(PublicResolver.FUNC_NAME, zzaaVar.g0.f0);
            vl5.a(bundle, zzaaVar.g0.I1());
            bundle.putBoolean("active", zzaaVar.i0);
            String str = zzaaVar.j0;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            zzas zzasVar = zzaaVar.k0;
            if (zzasVar != null) {
                bundle.putString("timed_out_event_name", zzasVar.a);
                zzaq zzaqVar = zzaaVar.k0.f0;
                if (zzaqVar != null) {
                    bundle.putBundle("timed_out_event_params", zzaqVar.N1());
                }
            }
            bundle.putLong("trigger_timeout", zzaaVar.l0);
            zzas zzasVar2 = zzaaVar.m0;
            if (zzasVar2 != null) {
                bundle.putString("triggered_event_name", zzasVar2.a);
                zzaq zzaqVar2 = zzaaVar.m0.f0;
                if (zzaqVar2 != null) {
                    bundle.putBundle("triggered_event_params", zzaqVar2.N1());
                }
            }
            bundle.putLong("triggered_timestamp", zzaaVar.g0.g0);
            bundle.putLong("time_to_live", zzaaVar.n0);
            zzas zzasVar3 = zzaaVar.o0;
            if (zzasVar3 != null) {
                bundle.putString("expired_event_name", zzasVar3.a);
                zzaq zzaqVar3 = zzaaVar.o0.f0;
                if (zzaqVar3 != null) {
                    bundle.putBundle("expired_event_params", zzaqVar3.N1());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    public static boolean a0(Context context) {
        ActivityInfo receiverInfo;
        zt2.j(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null && (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) != null) {
                if (receiverInfo.enabled) {
                    return true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }

    public static final boolean b0(Bundle bundle, int i2) {
        if (bundle.getLong("_err") == 0) {
            bundle.putLong("_err", i2);
            return true;
        }
        return false;
    }

    public static final boolean c0(String str) {
        zt2.j(str);
        return str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$");
    }

    public static boolean f0(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null && (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) != null) {
                if (serviceInfo.enabled) {
                    return true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }

    public static boolean g0(String str, String[] strArr) {
        zt2.j(strArr);
        for (String str2 : strArr) {
            if (G(str, str2)) {
                return true;
            }
        }
        return false;
    }

    public static boolean j0(String str) {
        zt2.f(str);
        return str.charAt(0) != '_' || str.equals("_ep");
    }

    public final void A(pw5 pw5Var, String str, int i2, String str2, String str3, int i3, boolean z) {
        Bundle bundle = new Bundle();
        b0(bundle, i2);
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            bundle.putString(str2, str3);
        }
        if (i2 == 6 || i2 == 7 || i2 == 2) {
            bundle.putLong("_el", i3);
        }
        if (z) {
            pw5Var.i(str, "_err", bundle);
            return;
        }
        this.a.b();
        this.a.F().X("auto", "_err", bundle);
    }

    public final boolean E(String str) {
        e();
        if (kr4.a(this.a.m()).a(str) == 0) {
            return true;
        }
        this.a.w().u().b("Permission not granted", str);
        return false;
    }

    public final boolean H(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String D = this.a.z().D();
        this.a.b();
        return D.equals(str);
    }

    public final Bundle I(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object r = r(str, bundle.get(str));
                if (r == null) {
                    this.a.w().s().b("Param value can't be null", this.a.H().o(str));
                } else {
                    z(bundle2, str, r);
                }
            }
        }
        return bundle2;
    }

    public final zzas J(String str, String str2, Bundle bundle, String str3, long j, boolean z, boolean z2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (p0(str2) == 0) {
            if (bundle != null) {
                bundle2 = new Bundle(bundle);
            } else {
                bundle2 = new Bundle();
            }
            Bundle bundle3 = bundle2;
            bundle3.putString("_o", str3);
            Bundle s = s(str, str2, bundle3, m10.b("_o"), false);
            if (z) {
                s = I(s);
            }
            zt2.j(s);
            return new zzas(str2, new zzaq(s), str3, j);
        }
        this.a.w().l().b("Invalid conditional property event name", this.a.H().p(str2));
        throw new IllegalArgumentException();
    }

    public final boolean K(Context context, String str) {
        Signature[] signatureArr;
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo e = kr4.a(context).e(str, 64);
            if (e == null || (signatureArr = e.signatures) == null || signatureArr.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(signatureArr[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (PackageManager.NameNotFoundException e2) {
            this.a.w().l().b("Package name not found", e2);
            return true;
        } catch (CertificateException e3) {
            this.a.w().l().b("Error obtaining certificate", e3);
            return true;
        }
    }

    public final byte[] L(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    public final int N() {
        if (this.f == null) {
            this.f = Integer.valueOf(eh1.h().b(this.a.m()) / 1000);
        }
        return this.f.intValue();
    }

    public final int O(int i2) {
        return eh1.h().j(this.a.m(), qh1.a);
    }

    public final long P(long j, long j2) {
        return (j + (j2 * 60000)) / 86400000;
    }

    public final void Q(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            this.a.w().p().b("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j + j2);
    }

    public final void R(m mVar, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning string value to wrapper", e);
        }
    }

    public final void S(m mVar, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("r", j);
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning long value to wrapper", e);
        }
    }

    public final void T(m mVar, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i2);
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning int value to wrapper", e);
        }
    }

    public final void U(m mVar, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning byte array to wrapper", e);
        }
    }

    public final void V(m mVar, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z);
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning boolean value to wrapper", e);
        }
    }

    public final void W(m mVar, Bundle bundle) {
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning bundle value to wrapper", e);
        }
    }

    public final void X(m mVar, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            mVar.K0(bundle);
        } catch (RemoteException e) {
            this.a.w().p().b("Error returning bundle list to wrapper", e);
        }
    }

    public final URL Z(long j, String str, String str2, long j2) {
        try {
            zt2.f(str2);
            zt2.f(str);
            String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", String.format("v%s.%s", 42004L, Integer.valueOf(N())), str2, str, Long.valueOf(j2));
            if (str.equals(this.a.z().E())) {
                format = format.concat("&ddl_test=1");
            }
            return new URL(format);
        } catch (IllegalArgumentException | MalformedURLException e) {
            this.a.w().l().b("Failed to create BOW URL for Deferred Deep Link. exception", e.getMessage());
            return null;
        }
    }

    public final Object d0(int i2, Object obj, boolean z, boolean z2) {
        Parcelable[] parcelableArr;
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf(((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf(((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf(((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(true != ((Boolean) obj).booleanValue() ? 0L : 1L);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if (!(obj instanceof String) && !(obj instanceof Character) && !(obj instanceof CharSequence)) {
                if (z2 && ((obj instanceof Bundle[]) || (obj instanceof Parcelable[]))) {
                    ArrayList arrayList = new ArrayList();
                    for (Parcelable parcelable : (Parcelable[]) obj) {
                        if (parcelable instanceof Bundle) {
                            Bundle I = I((Bundle) parcelable);
                            if (!I.isEmpty()) {
                                arrayList.add(I);
                            }
                        }
                    }
                    return arrayList.toArray(new Bundle[arrayList.size()]);
                }
                return null;
            }
            return o(String.valueOf(obj), i2, z);
        }
    }

    public final int e0(String str) {
        if ("_ldl".equals(str)) {
            this.a.z();
            return 2048;
        } else if (!"_id".equals(str)) {
            if (this.a.z().v(null, qf5.f0) && "_lgclid".equals(str)) {
                this.a.z();
                return 100;
            }
            this.a.z();
            return 36;
        } else {
            this.a.z();
            return 256;
        }
    }

    @Override // defpackage.sl5
    public final boolean f() {
        return true;
    }

    @Override // defpackage.sl5
    public final void g() {
        e();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                this.a.w().p().a("Utils falling back to Random for random id");
            }
        }
        this.d.set(nextLong);
    }

    public final long h0() {
        long andIncrement;
        long j;
        if (this.d.get() == 0) {
            synchronized (this.d) {
                long nextLong = new Random(System.nanoTime() ^ this.a.a().a()).nextLong();
                int i2 = this.e + 1;
                this.e = i2;
                j = nextLong + i2;
            }
            return j;
        }
        synchronized (this.d) {
            this.d.compareAndSet(-1L, 1L);
            andIncrement = this.d.getAndIncrement();
        }
        return andIncrement;
    }

    public final SecureRandom i0() {
        e();
        if (this.c == null) {
            this.c = new SecureRandom();
        }
        return this.c;
    }

    public final Bundle k0(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri != null) {
            try {
                if (uri.isHierarchical()) {
                    str = uri.getQueryParameter("utm_campaign");
                    str2 = uri.getQueryParameter("utm_source");
                    str3 = uri.getQueryParameter("utm_medium");
                    str4 = uri.getQueryParameter("gclid");
                } else {
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                }
                if (TextUtils.isEmpty(str) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str4)) {
                    return null;
                }
                Bundle bundle = new Bundle();
                if (!TextUtils.isEmpty(str)) {
                    bundle.putString("campaign", str);
                }
                if (!TextUtils.isEmpty(str2)) {
                    bundle.putString("source", str2);
                }
                if (!TextUtils.isEmpty(str3)) {
                    bundle.putString("medium", str3);
                }
                if (!TextUtils.isEmpty(str4)) {
                    bundle.putString("gclid", str4);
                }
                String queryParameter = uri.getQueryParameter("utm_term");
                if (!TextUtils.isEmpty(queryParameter)) {
                    bundle.putString("term", queryParameter);
                }
                String queryParameter2 = uri.getQueryParameter("utm_content");
                if (!TextUtils.isEmpty(queryParameter2)) {
                    bundle.putString(PublicResolver.FUNC_CONTENT, queryParameter2);
                }
                String queryParameter3 = uri.getQueryParameter("aclid");
                if (!TextUtils.isEmpty(queryParameter3)) {
                    bundle.putString("aclid", queryParameter3);
                }
                String queryParameter4 = uri.getQueryParameter("cp1");
                if (!TextUtils.isEmpty(queryParameter4)) {
                    bundle.putString("cp1", queryParameter4);
                }
                String queryParameter5 = uri.getQueryParameter("anid");
                if (!TextUtils.isEmpty(queryParameter5)) {
                    bundle.putString("anid", queryParameter5);
                }
                return bundle;
            } catch (UnsupportedOperationException e) {
                this.a.w().p().b("Install referrer url isn't a hierarchical URI", e);
            }
        }
        return null;
    }

    public final boolean l(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            if (c0(str)) {
                return true;
            }
            if (this.a.K()) {
                this.a.w().o().b("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", og5.x(str));
            }
            return false;
        }
        z16.a();
        if (!this.a.z().v(null, qf5.h0) || TextUtils.isEmpty(str3)) {
            if (!TextUtils.isEmpty(str2)) {
                if (c0(str2)) {
                    return true;
                }
                this.a.w().o().b("Invalid admob_app_id. Analytics disabled.", og5.x(str2));
                return false;
            }
            if (this.a.K()) {
                this.a.w().o().a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            }
            return false;
        }
        return true;
    }

    public final boolean l0(String str, String str2) {
        if (str2 == null) {
            this.a.w().o().b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            this.a.w().o().b("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                this.a.w().o().c("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 != 95 && !Character.isLetterOrDigit(codePointAt2)) {
                    this.a.w().o().c("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
                charCount += Character.charCount(codePointAt2);
            }
            return true;
        }
    }

    public final boolean m0(String str, String str2) {
        if (str2 == null) {
            this.a.w().o().b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            this.a.w().o().b("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                if (codePointAt != 95) {
                    this.a.w().o().c("Name must start with a letter or _ (underscore). Type, name", str, str2);
                    return false;
                }
                codePointAt = 95;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 != 95 && !Character.isLetterOrDigit(codePointAt2)) {
                    this.a.w().o().c("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
                charCount += Character.charCount(codePointAt2);
            }
            return true;
        }
    }

    public final boolean n(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            zt2.j(str);
            return !str.equals(str2);
        } else if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        } else if (isEmpty) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        } else if (TextUtils.isEmpty(str4)) {
            return false;
        } else {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
    }

    public final boolean n0(String str, String[] strArr, String[] strArr2, String str2) {
        if (str2 == null) {
            this.a.w().o().b("Name is required and can't be null. Type", str);
            return false;
        }
        zt2.j(str2);
        String[] strArr3 = g;
        for (int i2 = 0; i2 < 3; i2++) {
            if (str2.startsWith(strArr3[i2])) {
                this.a.w().o().c("Name starts with reserved prefix. Type, name", str, str2);
                return false;
            }
        }
        if (strArr == null || !g0(str2, strArr)) {
            return true;
        }
        if (strArr2 == null || !g0(str2, strArr2)) {
            this.a.w().o().c("Name is reserved. Type, name", str, str2);
            return false;
        }
        return true;
    }

    public final String o(String str, int i2, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) > i2) {
            if (z) {
                return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i2))).concat("...");
            }
            return null;
        }
        return str;
    }

    public final boolean o0(String str, int i2, String str2) {
        if (str2 == null) {
            this.a.w().o().b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) > i2) {
            this.a.w().o().d("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i2), str2);
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:48:0x00dd A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x00de  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final int p(java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.Object r18, android.os.Bundle r19, java.util.List<java.lang.String> r20, boolean r21, boolean r22) {
        /*
            Method dump skipped, instructions count: 371
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.sw5.p(java.lang.String, java.lang.String, java.lang.String, java.lang.Object, android.os.Bundle, java.util.List, boolean, boolean):int");
    }

    public final int p0(String str) {
        if (m0("event", str)) {
            if (n0("event", yl5.a, yl5.b, str)) {
                this.a.z();
                return !o0("event", 40, str) ? 2 : 0;
            }
            return 13;
        }
        return 2;
    }

    public final int q0(String str) {
        if (m0("user property", str)) {
            if (n0("user property", bm5.a, null, str)) {
                this.a.z();
                return !o0("user property", 24, str) ? 6 : 0;
            }
            return 15;
        }
        return 6;
    }

    public final Object r(String str, Object obj) {
        int i2 = 256;
        if ("_ev".equals(str)) {
            this.a.z();
            return d0(256, obj, true, true);
        }
        if (F(str)) {
            this.a.z();
        } else {
            this.a.z();
            i2 = 100;
        }
        return d0(i2, obj, false, true);
    }

    public final int r0(String str) {
        if (l0("event param", str)) {
            if (n0("event param", null, null, str)) {
                this.a.z();
                return !o0("event param", 40, str) ? 3 : 0;
            }
            return 14;
        }
        return 3;
    }

    /* JADX WARN: Removed duplicated region for block: B:48:0x00d6 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0120 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.os.Bundle s(java.lang.String r21, java.lang.String r22, android.os.Bundle r23, java.util.List<java.lang.String> r24, boolean r25) {
        /*
            Method dump skipped, instructions count: 293
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.sw5.s(java.lang.String, java.lang.String, android.os.Bundle, java.util.List, boolean):android.os.Bundle");
    }

    public final int s0(String str) {
        if (m0("event param", str)) {
            if (n0("event param", null, null, str)) {
                this.a.z();
                return !o0("event param", 40, str) ? 3 : 0;
            }
            return 14;
        }
        return 3;
    }

    public final void t(rg5 rg5Var, int i2) {
        int i3 = 0;
        for (String str : new TreeSet(rg5Var.d.keySet())) {
            if (j0(str) && (i3 = i3 + 1) > i2) {
                StringBuilder sb = new StringBuilder(48);
                sb.append("Event can't contain more than ");
                sb.append(i2);
                sb.append(" params");
                this.a.w().o().c(sb.toString(), this.a.H().n(rg5Var.a), this.a.H().r(rg5Var.d));
                b0(rg5Var.d, 5);
                rg5Var.d.remove(str);
            }
        }
    }

    public final boolean t0(Object obj) {
        return (obj instanceof Parcelable[]) || (obj instanceof ArrayList) || (obj instanceof Bundle);
    }

    public final void u(Bundle bundle, Bundle bundle2) {
        if (bundle2 == null) {
            return;
        }
        for (String str : bundle2.keySet()) {
            if (!bundle.containsKey(str)) {
                this.a.G().z(bundle, str, bundle2.get(str));
            }
        }
    }

    public final boolean u0(String str, String str2, int i2, Object obj) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if (!(obj instanceof String) && !(obj instanceof Character) && !(obj instanceof CharSequence)) {
                return false;
            }
            String valueOf = String.valueOf(obj);
            if (valueOf.codePointCount(0, valueOf.length()) > i2) {
                this.a.w().s().d("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                return false;
            }
        }
        return true;
    }

    public final void v(Bundle bundle, int i2, String str, String str2, Object obj) {
        if (b0(bundle, i2)) {
            this.a.z();
            bundle.putString("_ev", o(str, 40, true));
            if (obj != null) {
                zt2.j(bundle);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    bundle.putLong("_el", String.valueOf(obj).length());
                }
            }
        }
    }

    public final void v0(String str, String str2, String str3, Bundle bundle, List<String> list, boolean z) {
        int i2;
        String str4;
        int p;
        if (bundle == null) {
            return;
        }
        this.a.z();
        int i3 = 0;
        for (String str5 : new TreeSet(bundle.keySet())) {
            if (list == null || !list.contains(str5)) {
                int r0 = z ? r0(str5) : 0;
                if (r0 == 0) {
                    r0 = s0(str5);
                }
                i2 = r0;
            } else {
                i2 = 0;
            }
            if (i2 != 0) {
                v(bundle, i2, str5, str5, i2 == 3 ? str5 : null);
                bundle.remove(str5);
            } else {
                if (t0(bundle.get(str5))) {
                    this.a.w().s().d("Nested Bundle parameters are not allowed; discarded. event name, param name, child param name", str2, str3, str5);
                    p = 22;
                    str4 = str5;
                } else {
                    str4 = str5;
                    p = p(str, str2, str5, bundle.get(str5), bundle, list, z, false);
                }
                if (p != 0 && !"_ev".equals(str4)) {
                    v(bundle, p, str4, str4, bundle.get(str4));
                    bundle.remove(str4);
                } else if (j0(str4) && !g0(str4, am5.d) && (i3 = i3 + 1) > 0) {
                    this.a.w().o().c("Item cannot contain custom parameters", this.a.H().n(str2), this.a.H().r(bundle));
                    b0(bundle, 23);
                    bundle.remove(str4);
                }
            }
        }
    }

    public final int x(String str, Object obj) {
        boolean u0;
        if ("_ldl".equals(str)) {
            u0 = u0("user property referrer", str, e0(str), obj);
        } else {
            u0 = u0("user property", str, e0(str), obj);
        }
        return u0 ? 0 : 7;
    }

    public final Object y(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return d0(e0(str), obj, true, false);
        }
        return d0(e0(str), obj, false, false);
    }

    public final void z(Bundle bundle, String str, Object obj) {
        if (bundle == null) {
            return;
        }
        if (obj instanceof Long) {
            bundle.putLong(str, ((Long) obj).longValue());
        } else if (obj instanceof String) {
            bundle.putString(str, String.valueOf(obj));
        } else if (obj instanceof Double) {
            bundle.putDouble(str, ((Double) obj).doubleValue());
        } else if (obj instanceof Bundle[]) {
            bundle.putParcelableArray(str, (Bundle[]) obj);
        } else if (str != null) {
            this.a.w().s().c("Not putting event parameter. Invalid value type. name, type", this.a.H().o(str), obj != null ? obj.getClass().getSimpleName() : null);
        }
    }
}
