package defpackage;

import android.annotation.SuppressLint;

/* compiled from: ActivityResultLauncher.java */
/* renamed from: w7  reason: default package */
/* loaded from: classes.dex */
public abstract class w7<I> {
    public void a(@SuppressLint({"UnknownNullness"}) I i) {
        b(i, null);
    }

    public abstract void b(@SuppressLint({"UnknownNullness"}) I i, o7 o7Var);

    public abstract void c();
}
