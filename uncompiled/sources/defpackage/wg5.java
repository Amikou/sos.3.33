package defpackage;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: wg5  reason: default package */
/* loaded from: classes.dex */
public interface wg5 {
    void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
}
