package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.nft.NFTData;
import org.web3j.abi.datatypes.Address;

/* compiled from: SendToNftFragmentDirections.java */
/* renamed from: pk3  reason: default package */
/* loaded from: classes2.dex */
public class pk3 {

    /* compiled from: SendToNftFragmentDirections.java */
    /* renamed from: pk3$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey(Address.TYPE_NAME)) {
                bundle.putString(Address.TYPE_NAME, (String) this.a.get(Address.TYPE_NAME));
            }
            if (this.a.containsKey("nftData")) {
                NFTData nFTData = (NFTData) this.a.get("nftData");
                if (!Parcelable.class.isAssignableFrom(NFTData.class) && nFTData != null) {
                    if (Serializable.class.isAssignableFrom(NFTData.class)) {
                        bundle.putSerializable("nftData", (Serializable) Serializable.class.cast(nFTData));
                    } else {
                        throw new UnsupportedOperationException(NFTData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("nftData", (Parcelable) Parcelable.class.cast(nFTData));
                }
            }
            if (this.a.containsKey("iContact")) {
                IContact iContact = (IContact) this.a.get("iContact");
                if (!Parcelable.class.isAssignableFrom(IContact.class) && iContact != null) {
                    if (Serializable.class.isAssignableFrom(IContact.class)) {
                        bundle.putSerializable("iContact", (Serializable) Serializable.class.cast(iContact));
                    } else {
                        throw new UnsupportedOperationException(IContact.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("iContact", (Parcelable) Parcelable.class.cast(iContact));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_nft_transfer;
        }

        public String c() {
            return (String) this.a.get(Address.TYPE_NAME);
        }

        public IContact d() {
            return (IContact) this.a.get("iContact");
        }

        public NFTData e() {
            return (NFTData) this.a.get("nftData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey(Address.TYPE_NAME) != bVar.a.containsKey(Address.TYPE_NAME)) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                if (this.a.containsKey("nftData") != bVar.a.containsKey("nftData")) {
                    return false;
                }
                if (e() == null ? bVar.e() == null : e().equals(bVar.e())) {
                    if (this.a.containsKey("iContact") != bVar.a.containsKey("iContact")) {
                        return false;
                    }
                    if (d() == null ? bVar.d() == null : d().equals(bVar.d())) {
                        return b() == bVar.b();
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return (((((((c() != null ? c().hashCode() : 0) + 31) * 31) + (e() != null ? e().hashCode() : 0)) * 31) + (d() != null ? d().hashCode() : 0)) * 31) + b();
        }

        public String toString() {
            return "ActionNftTransfer(actionId=" + b() + "){address=" + c() + ", nftData=" + e() + ", iContact=" + d() + "}";
        }

        public b(String str, NFTData nFTData, IContact iContact) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (str != null) {
                hashMap.put(Address.TYPE_NAME, str);
                if (nFTData != null) {
                    hashMap.put("nftData", nFTData);
                    hashMap.put("iContact", iContact);
                    return;
                }
                throw new IllegalArgumentException("Argument \"nftData\" is marked as non-null but was passed a null value.");
            }
            throw new IllegalArgumentException("Argument \"address\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(String str, NFTData nFTData, IContact iContact) {
        return new b(str, nFTData, iContact);
    }

    public static ce2 b() {
        return new l6(R.id.action_sendtoNFTFragment_to_manageContactsFragment);
    }
}
