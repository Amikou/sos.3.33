package defpackage;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: xi5  reason: default package */
/* loaded from: classes.dex */
public final class xi5 extends WeakReference<Throwable> {
    public final int a;

    public xi5(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        Objects.requireNonNull(th, "The referent cannot be null");
        this.a = System.identityHashCode(th);
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == xi5.class) {
            if (this == obj) {
                return true;
            }
            xi5 xi5Var = (xi5) obj;
            if (this.a == xi5Var.a && get() == xi5Var.get()) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return this.a;
    }
}
