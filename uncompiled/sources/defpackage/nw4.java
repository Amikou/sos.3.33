package defpackage;

import com.google.android.play.core.assetpacks.k;
import java.util.Arrays;

/* renamed from: nw4  reason: default package */
/* loaded from: classes2.dex */
public final class nw4 {
    public byte[] a = new byte[4096];
    public int b;
    public long c;
    public long d;
    public int e;
    public int f;
    public int g;
    public boolean h;
    public String i;

    public nw4() {
        d();
    }

    public final int a(byte[] bArr, int i, int i2) {
        int e = e(30, bArr, i, i2);
        if (e != -1) {
            if (this.c == -1) {
                long b = k.b(this.a, 0);
                this.c = b;
                if (b == 67324752) {
                    this.h = false;
                    this.d = k.b(this.a, 18);
                    this.g = k.c(this.a, 8);
                    this.e = k.c(this.a, 26);
                    int c = this.e + 30 + k.c(this.a, 28);
                    this.f = c;
                    int length = this.a.length;
                    if (length < c) {
                        do {
                            length += length;
                        } while (length < c);
                        this.a = Arrays.copyOf(this.a, length);
                    }
                } else {
                    this.h = true;
                }
            }
            int e2 = e(this.f, bArr, i + e, i2 - e);
            if (e2 == -1) {
                return -1;
            }
            int i3 = e + e2;
            if (!this.h && this.i == null) {
                this.i = new String(this.a, 30, this.e);
            }
            return i3;
        }
        return -1;
    }

    public final gx4 b() {
        int i = this.b;
        int i2 = this.f;
        if (i < i2) {
            return gx4.a(this.i, this.d, this.g, true, Arrays.copyOf(this.a, i), this.h);
        }
        gx4 a = gx4.a(this.i, this.d, this.g, false, Arrays.copyOf(this.a, i2), this.h);
        d();
        return a;
    }

    public final int c() {
        return this.f;
    }

    public final void d() {
        this.b = 0;
        this.e = -1;
        this.c = -1L;
        this.h = false;
        this.f = 30;
        this.d = -1L;
        this.g = -1;
        this.i = null;
    }

    public final int e(int i, byte[] bArr, int i2, int i3) {
        int i4 = this.b;
        if (i4 < i) {
            int min = Math.min(i3, i - i4);
            System.arraycopy(bArr, i2, this.a, this.b, min);
            int i5 = this.b + min;
            this.b = i5;
            if (i5 < i) {
                return -1;
            }
            return min;
        }
        return 0;
    }
}
