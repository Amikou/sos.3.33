package defpackage;

import androidx.media3.extractor.metadata.mp4.MotionPhotoMetadata;
import java.util.List;

/* compiled from: MotionPhotoDescription.java */
/* renamed from: y92  reason: default package */
/* loaded from: classes.dex */
public final class y92 {
    public final long a;
    public final List<a> b;

    /* compiled from: MotionPhotoDescription.java */
    /* renamed from: y92$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final String a;
        public final long b;
        public final long c;

        public a(String str, String str2, long j, long j2) {
            this.a = str;
            this.b = j;
            this.c = j2;
        }
    }

    public y92(long j, List<a> list) {
        this.a = j;
        this.b = list;
    }

    public MotionPhotoMetadata a(long j) {
        long j2;
        if (this.b.size() < 2) {
            return null;
        }
        long j3 = j;
        long j4 = -1;
        long j5 = -1;
        long j6 = -1;
        long j7 = -1;
        boolean z = false;
        for (int size = this.b.size() - 1; size >= 0; size--) {
            a aVar = this.b.get(size);
            boolean equals = "video/mp4".equals(aVar.a) | z;
            if (size == 0) {
                j2 = j3 - aVar.c;
                j3 = 0;
            } else {
                long j8 = j3;
                j3 -= aVar.b;
                j2 = j8;
            }
            if (!equals || j3 == j2) {
                z = equals;
            } else {
                j7 = j2 - j3;
                j6 = j3;
                z = false;
            }
            if (size == 0) {
                j4 = j3;
                j5 = j2;
            }
        }
        if (j6 == -1 || j7 == -1 || j4 == -1 || j5 == -1) {
            return null;
        }
        return new MotionPhotoMetadata(j4, j5, this.a, j6, j7);
    }
}
