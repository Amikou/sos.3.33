package defpackage;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Looper;
import android.util.AttributeSet;
import com.github.mikephil.charting.utils.Utils;
import defpackage.pn3;
import defpackage.qn3;
import defpackage.rn3;
import java.util.BitSet;

/* compiled from: MaterialShapeDrawable.java */
/* renamed from: o42  reason: default package */
/* loaded from: classes2.dex */
public class o42 extends Drawable implements i64, sn3 {
    public static final String A0 = o42.class.getSimpleName();
    public static final Paint B0 = new Paint(1);
    public c a;
    public final rn3.g[] f0;
    public final rn3.g[] g0;
    public final BitSet h0;
    public boolean i0;
    public final Matrix j0;
    public final Path k0;
    public final Path l0;
    public final RectF m0;
    public final RectF n0;
    public final Region o0;
    public final Region p0;
    public pn3 q0;
    public final Paint r0;
    public final Paint s0;
    public final nn3 t0;
    public final qn3.b u0;
    public final qn3 v0;
    public PorterDuffColorFilter w0;
    public PorterDuffColorFilter x0;
    public final RectF y0;
    public boolean z0;

    /* compiled from: MaterialShapeDrawable.java */
    /* renamed from: o42$a */
    /* loaded from: classes2.dex */
    public class a implements qn3.b {
        public a() {
        }

        @Override // defpackage.qn3.b
        public void a(rn3 rn3Var, Matrix matrix, int i) {
            o42.this.h0.set(i, rn3Var.e());
            o42.this.f0[i] = rn3Var.f(matrix);
        }

        @Override // defpackage.qn3.b
        public void b(rn3 rn3Var, Matrix matrix, int i) {
            o42.this.h0.set(i + 4, rn3Var.e());
            o42.this.g0[i] = rn3Var.f(matrix);
        }
    }

    /* compiled from: MaterialShapeDrawable.java */
    /* renamed from: o42$b */
    /* loaded from: classes2.dex */
    public class b implements pn3.c {
        public final /* synthetic */ float a;

        public b(o42 o42Var, float f) {
            this.a = f;
        }

        @Override // defpackage.pn3.c
        public v80 a(v80 v80Var) {
            return v80Var instanceof j63 ? v80Var : new v9(this.a, v80Var);
        }
    }

    public /* synthetic */ o42(c cVar, a aVar) {
        this(cVar);
    }

    public static int U(int i, int i2) {
        return (i * (i2 + (i2 >>> 7))) >>> 8;
    }

    public static o42 m(Context context, float f) {
        int c2 = l42.c(context, gy2.colorSurface, o42.class.getSimpleName());
        o42 o42Var = new o42();
        o42Var.P(context);
        o42Var.a0(ColorStateList.valueOf(c2));
        o42Var.Z(f);
        return o42Var;
    }

    public int A() {
        c cVar = this.a;
        return (int) (cVar.s * Math.sin(Math.toRadians(cVar.t)));
    }

    public int B() {
        c cVar = this.a;
        return (int) (cVar.s * Math.cos(Math.toRadians(cVar.t)));
    }

    public int C() {
        return this.a.r;
    }

    public pn3 D() {
        return this.a.a;
    }

    public ColorStateList E() {
        return this.a.e;
    }

    public final float F() {
        return O() ? this.s0.getStrokeWidth() / 2.0f : Utils.FLOAT_EPSILON;
    }

    public float G() {
        return this.a.l;
    }

    public ColorStateList H() {
        return this.a.g;
    }

    public float I() {
        return this.a.a.r().a(u());
    }

    public float J() {
        return this.a.a.t().a(u());
    }

    public float K() {
        return this.a.p;
    }

    public float L() {
        return w() + K();
    }

    public final boolean M() {
        c cVar = this.a;
        int i = cVar.q;
        return i != 1 && cVar.r > 0 && (i == 2 || W());
    }

    public final boolean N() {
        Paint.Style style = this.a.v;
        return style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.FILL;
    }

    public final boolean O() {
        Paint.Style style = this.a.v;
        return (style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.STROKE) && this.s0.getStrokeWidth() > Utils.FLOAT_EPSILON;
    }

    public void P(Context context) {
        this.a.b = new lu0(context);
        p0();
    }

    public final void Q() {
        super.invalidateSelf();
    }

    public boolean R() {
        lu0 lu0Var = this.a.b;
        return lu0Var != null && lu0Var.e();
    }

    public boolean S() {
        return this.a.a.u(u());
    }

    public final void T(Canvas canvas) {
        if (M()) {
            canvas.save();
            V(canvas);
            if (!this.z0) {
                n(canvas);
                canvas.restore();
                return;
            }
            int width = (int) (this.y0.width() - getBounds().width());
            int height = (int) (this.y0.height() - getBounds().height());
            if (width >= 0 && height >= 0) {
                Bitmap createBitmap = Bitmap.createBitmap(((int) this.y0.width()) + (this.a.r * 2) + width, ((int) this.y0.height()) + (this.a.r * 2) + height, Bitmap.Config.ARGB_8888);
                Canvas canvas2 = new Canvas(createBitmap);
                float f = (getBounds().left - this.a.r) - width;
                float f2 = (getBounds().top - this.a.r) - height;
                canvas2.translate(-f, -f2);
                n(canvas2);
                canvas.drawBitmap(createBitmap, f, f2, (Paint) null);
                createBitmap.recycle();
                canvas.restore();
                return;
            }
            throw new IllegalStateException("Invalid shadow bounds. Check that the treatments result in a valid path.");
        }
    }

    public final void V(Canvas canvas) {
        int A = A();
        int B = B();
        if (Build.VERSION.SDK_INT < 21 && this.z0) {
            Rect clipBounds = canvas.getClipBounds();
            int i = this.a.r;
            clipBounds.inset(-i, -i);
            clipBounds.offset(A, B);
            canvas.clipRect(clipBounds, Region.Op.REPLACE);
        }
        canvas.translate(A, B);
    }

    public boolean W() {
        int i = Build.VERSION.SDK_INT;
        return i < 21 || !(S() || this.k0.isConvex() || i >= 29);
    }

    public void X(float f) {
        setShapeAppearanceModel(this.a.a.w(f));
    }

    public void Y(v80 v80Var) {
        setShapeAppearanceModel(this.a.a.x(v80Var));
    }

    public void Z(float f) {
        c cVar = this.a;
        if (cVar.o != f) {
            cVar.o = f;
            p0();
        }
    }

    public void a0(ColorStateList colorStateList) {
        c cVar = this.a;
        if (cVar.d != colorStateList) {
            cVar.d = colorStateList;
            onStateChange(getState());
        }
    }

    public void b0(float f) {
        c cVar = this.a;
        if (cVar.k != f) {
            cVar.k = f;
            this.i0 = true;
            invalidateSelf();
        }
    }

    public void c0(int i, int i2, int i3, int i4) {
        c cVar = this.a;
        if (cVar.i == null) {
            cVar.i = new Rect();
        }
        this.a.i.set(i, i2, i3, i4);
        invalidateSelf();
    }

    public void d0(Paint.Style style) {
        this.a.v = style;
        Q();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.r0.setColorFilter(this.w0);
        int alpha = this.r0.getAlpha();
        this.r0.setAlpha(U(alpha, this.a.m));
        this.s0.setColorFilter(this.x0);
        this.s0.setStrokeWidth(this.a.l);
        int alpha2 = this.s0.getAlpha();
        this.s0.setAlpha(U(alpha2, this.a.m));
        if (this.i0) {
            i();
            g(u(), this.k0);
            this.i0 = false;
        }
        T(canvas);
        if (N()) {
            o(canvas);
        }
        if (O()) {
            r(canvas);
        }
        this.r0.setAlpha(alpha);
        this.s0.setAlpha(alpha2);
    }

    public void e0(float f) {
        c cVar = this.a;
        if (cVar.n != f) {
            cVar.n = f;
            p0();
        }
    }

    public final PorterDuffColorFilter f(Paint paint, boolean z) {
        int color;
        int l;
        if (!z || (l = l((color = paint.getColor()))) == color) {
            return null;
        }
        return new PorterDuffColorFilter(l, PorterDuff.Mode.SRC_IN);
    }

    public void f0(boolean z) {
        this.z0 = z;
    }

    public final void g(RectF rectF, Path path) {
        h(rectF, path);
        if (this.a.j != 1.0f) {
            this.j0.reset();
            Matrix matrix = this.j0;
            float f = this.a.j;
            matrix.setScale(f, f, rectF.width() / 2.0f, rectF.height() / 2.0f);
            path.transform(this.j0);
        }
        path.computeBounds(this.y0, true);
    }

    public void g0(int i) {
        this.t0.d(i);
        this.a.u = false;
        Q();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        return this.a;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.a.q == 2) {
            return;
        }
        if (S()) {
            outline.setRoundRect(getBounds(), I() * this.a.k);
            return;
        }
        g(u(), this.k0);
        if (this.k0.isConvex() || Build.VERSION.SDK_INT >= 29) {
            try {
                outline.setConvexPath(this.k0);
            } catch (IllegalArgumentException unused) {
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        Rect rect2 = this.a.i;
        if (rect2 != null) {
            rect.set(rect2);
            return true;
        }
        return super.getPadding(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public Region getTransparentRegion() {
        this.o0.set(getBounds());
        g(u(), this.k0);
        this.p0.setPath(this.k0, this.o0);
        this.o0.op(this.p0, Region.Op.DIFFERENCE);
        return this.o0;
    }

    public final void h(RectF rectF, Path path) {
        qn3 qn3Var = this.v0;
        c cVar = this.a;
        qn3Var.d(cVar.a, cVar.k, rectF, this.u0, path);
    }

    public void h0(int i) {
        c cVar = this.a;
        if (cVar.t != i) {
            cVar.t = i;
            Q();
        }
    }

    public final void i() {
        pn3 y = D().y(new b(this, -F()));
        this.q0 = y;
        this.v0.e(y, this.a.k, v(), this.l0);
    }

    public void i0(int i) {
        c cVar = this.a;
        if (cVar.q != i) {
            cVar.q = i;
            Q();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        this.i0 = true;
        super.invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3;
        ColorStateList colorStateList4;
        return super.isStateful() || ((colorStateList = this.a.g) != null && colorStateList.isStateful()) || (((colorStateList2 = this.a.f) != null && colorStateList2.isStateful()) || (((colorStateList3 = this.a.e) != null && colorStateList3.isStateful()) || ((colorStateList4 = this.a.d) != null && colorStateList4.isStateful())));
    }

    public final PorterDuffColorFilter j(ColorStateList colorStateList, PorterDuff.Mode mode, boolean z) {
        int colorForState = colorStateList.getColorForState(getState(), 0);
        if (z) {
            colorForState = l(colorForState);
        }
        return new PorterDuffColorFilter(colorForState, mode);
    }

    public void j0(float f, int i) {
        m0(f);
        l0(ColorStateList.valueOf(i));
    }

    public final PorterDuffColorFilter k(ColorStateList colorStateList, PorterDuff.Mode mode, Paint paint, boolean z) {
        if (colorStateList != null && mode != null) {
            return j(colorStateList, mode, z);
        }
        return f(paint, z);
    }

    public void k0(float f, ColorStateList colorStateList) {
        m0(f);
        l0(colorStateList);
    }

    public int l(int i) {
        float L = L() + z();
        lu0 lu0Var = this.a.b;
        return lu0Var != null ? lu0Var.c(i, L) : i;
    }

    public void l0(ColorStateList colorStateList) {
        c cVar = this.a;
        if (cVar.e != colorStateList) {
            cVar.e = colorStateList;
            onStateChange(getState());
        }
    }

    public void m0(float f) {
        this.a.l = f;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        this.a = new c(this.a);
        return this;
    }

    public final void n(Canvas canvas) {
        this.h0.cardinality();
        if (this.a.s != 0) {
            canvas.drawPath(this.k0, this.t0.c());
        }
        for (int i = 0; i < 4; i++) {
            this.f0[i].a(this.t0, this.a.r, canvas);
            this.g0[i].a(this.t0, this.a.r, canvas);
        }
        if (this.z0) {
            int A = A();
            int B = B();
            canvas.translate(-A, -B);
            canvas.drawPath(this.k0, B0);
            canvas.translate(A, B);
        }
    }

    public final boolean n0(int[] iArr) {
        boolean z;
        int color;
        int colorForState;
        int color2;
        int colorForState2;
        if (this.a.d == null || color2 == (colorForState2 = this.a.d.getColorForState(iArr, (color2 = this.r0.getColor())))) {
            z = false;
        } else {
            this.r0.setColor(colorForState2);
            z = true;
        }
        if (this.a.e == null || color == (colorForState = this.a.e.getColorForState(iArr, (color = this.s0.getColor())))) {
            return z;
        }
        this.s0.setColor(colorForState);
        return true;
    }

    public final void o(Canvas canvas) {
        p(canvas, this.r0, this.k0, this.a.a, u());
    }

    public final boolean o0() {
        PorterDuffColorFilter porterDuffColorFilter = this.w0;
        PorterDuffColorFilter porterDuffColorFilter2 = this.x0;
        c cVar = this.a;
        this.w0 = k(cVar.g, cVar.h, this.r0, true);
        c cVar2 = this.a;
        this.x0 = k(cVar2.f, cVar2.h, this.s0, false);
        c cVar3 = this.a;
        if (cVar3.u) {
            this.t0.d(cVar3.g.getColorForState(getState(), 0));
        }
        return (sl2.a(porterDuffColorFilter, this.w0) && sl2.a(porterDuffColorFilter2, this.x0)) ? false : true;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        this.i0 = true;
        super.onBoundsChange(rect);
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i44.b
    public boolean onStateChange(int[] iArr) {
        boolean z = n0(iArr) || o0();
        if (z) {
            invalidateSelf();
        }
        return z;
    }

    public final void p(Canvas canvas, Paint paint, Path path, pn3 pn3Var, RectF rectF) {
        if (pn3Var.u(rectF)) {
            float a2 = pn3Var.t().a(rectF) * this.a.k;
            canvas.drawRoundRect(rectF, a2, a2, paint);
            return;
        }
        canvas.drawPath(path, paint);
    }

    public final void p0() {
        float L = L();
        this.a.r = (int) Math.ceil(0.75f * L);
        this.a.s = (int) Math.ceil(L * 0.25f);
        o0();
        Q();
    }

    public void q(Canvas canvas, Paint paint, Path path, RectF rectF) {
        p(canvas, paint, path, this.a.a, rectF);
    }

    public final void r(Canvas canvas) {
        p(canvas, this.s0, this.l0, this.q0, v());
    }

    public float s() {
        return this.a.a.j().a(u());
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        c cVar = this.a;
        if (cVar.m != i) {
            cVar.m = i;
            Q();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.c = colorFilter;
        Q();
    }

    @Override // defpackage.sn3
    public void setShapeAppearanceModel(pn3 pn3Var) {
        this.a.a = pn3Var;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTintList(ColorStateList colorStateList) {
        this.a.g = colorStateList;
        o0();
        Q();
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.a;
        if (cVar.h != mode) {
            cVar.h = mode;
            o0();
            Q();
        }
    }

    public float t() {
        return this.a.a.l().a(u());
    }

    public RectF u() {
        this.m0.set(getBounds());
        return this.m0;
    }

    public final RectF v() {
        this.n0.set(u());
        float F = F();
        this.n0.inset(F, F);
        return this.n0;
    }

    public float w() {
        return this.a.o;
    }

    public ColorStateList x() {
        return this.a.d;
    }

    public float y() {
        return this.a.k;
    }

    public float z() {
        return this.a.n;
    }

    public o42() {
        this(new pn3());
    }

    public o42(Context context, AttributeSet attributeSet, int i, int i2) {
        this(pn3.e(context, attributeSet, i, i2).m());
    }

    public o42(pn3 pn3Var) {
        this(new c(pn3Var, null));
    }

    public o42(c cVar) {
        this.f0 = new rn3.g[4];
        this.g0 = new rn3.g[4];
        this.h0 = new BitSet(8);
        this.j0 = new Matrix();
        this.k0 = new Path();
        this.l0 = new Path();
        this.m0 = new RectF();
        this.n0 = new RectF();
        this.o0 = new Region();
        this.p0 = new Region();
        Paint paint = new Paint(1);
        this.r0 = paint;
        Paint paint2 = new Paint(1);
        this.s0 = paint2;
        this.t0 = new nn3();
        this.v0 = Looper.getMainLooper().getThread() == Thread.currentThread() ? qn3.k() : new qn3();
        this.y0 = new RectF();
        this.z0 = true;
        this.a = cVar;
        paint2.setStyle(Paint.Style.STROKE);
        paint.setStyle(Paint.Style.FILL);
        Paint paint3 = B0;
        paint3.setColor(-1);
        paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        o0();
        n0(getState());
        this.u0 = new a();
    }

    /* compiled from: MaterialShapeDrawable.java */
    /* renamed from: o42$c */
    /* loaded from: classes2.dex */
    public static final class c extends Drawable.ConstantState {
        public pn3 a;
        public lu0 b;
        public ColorFilter c;
        public ColorStateList d;
        public ColorStateList e;
        public ColorStateList f;
        public ColorStateList g;
        public PorterDuff.Mode h;
        public Rect i;
        public float j;
        public float k;
        public float l;
        public int m;
        public float n;
        public float o;
        public float p;
        public int q;
        public int r;
        public int s;
        public int t;
        public boolean u;
        public Paint.Style v;

        public c(pn3 pn3Var, lu0 lu0Var) {
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = null;
            this.h = PorterDuff.Mode.SRC_IN;
            this.i = null;
            this.j = 1.0f;
            this.k = 1.0f;
            this.m = 255;
            this.n = Utils.FLOAT_EPSILON;
            this.o = Utils.FLOAT_EPSILON;
            this.p = Utils.FLOAT_EPSILON;
            this.q = 0;
            this.r = 0;
            this.s = 0;
            this.t = 0;
            this.u = false;
            this.v = Paint.Style.FILL_AND_STROKE;
            this.a = pn3Var;
            this.b = lu0Var;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public int getChangingConfigurations() {
            return 0;
        }

        @Override // android.graphics.drawable.Drawable.ConstantState
        public Drawable newDrawable() {
            o42 o42Var = new o42(this, null);
            o42Var.i0 = true;
            return o42Var;
        }

        public c(c cVar) {
            this.d = null;
            this.e = null;
            this.f = null;
            this.g = null;
            this.h = PorterDuff.Mode.SRC_IN;
            this.i = null;
            this.j = 1.0f;
            this.k = 1.0f;
            this.m = 255;
            this.n = Utils.FLOAT_EPSILON;
            this.o = Utils.FLOAT_EPSILON;
            this.p = Utils.FLOAT_EPSILON;
            this.q = 0;
            this.r = 0;
            this.s = 0;
            this.t = 0;
            this.u = false;
            this.v = Paint.Style.FILL_AND_STROKE;
            this.a = cVar.a;
            this.b = cVar.b;
            this.l = cVar.l;
            this.c = cVar.c;
            this.d = cVar.d;
            this.e = cVar.e;
            this.h = cVar.h;
            this.g = cVar.g;
            this.m = cVar.m;
            this.j = cVar.j;
            this.s = cVar.s;
            this.q = cVar.q;
            this.u = cVar.u;
            this.k = cVar.k;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
            this.r = cVar.r;
            this.t = cVar.t;
            this.f = cVar.f;
            this.v = cVar.v;
            if (cVar.i != null) {
                this.i = new Rect(cVar.i);
            }
        }
    }
}
