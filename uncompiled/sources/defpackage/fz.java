package defpackage;

/* compiled from: ClassicIdenticonHash.kt */
/* renamed from: fz  reason: default package */
/* loaded from: classes2.dex */
public final class fz {
    public static final a b = new a(null);
    public final int[] a;

    /* compiled from: ClassicIdenticonHash.kt */
    /* renamed from: fz$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final int a(long j, int i) {
            return Math.abs((int) (j % i));
        }
    }

    public fz(int i) {
        this.a = r0;
        a aVar = b;
        int[] iArr = {aVar.a(i, 8) * 4, aVar.a(i >> 3, 32), aVar.a(i >> 8, 32), aVar.a(i >> 13, 2), aVar.a(i >> 14, 2), aVar.a(i >> 15, 2), aVar.a(i >> 16, 16), aVar.a(i >> 20, 16), aVar.a(i >> 24, 16), aVar.a(i >> 28, 4), aVar.a(i >> 30, 4)};
    }

    public final int a() {
        return this.a[8];
    }

    public final int b() {
        return this.a[1];
    }

    public final int c() {
        return this.a[9];
    }

    public final int d() {
        return this.a[7];
    }

    public final int e() {
        return this.a[4];
    }

    public final int f() {
        return this.a[3];
    }

    public final int g() {
        return this.a[5];
    }

    public final int h() {
        return this.a[0];
    }

    public final int i() {
        return this.a[6];
    }

    public final int j() {
        return this.a[2];
    }

    public final int k() {
        return this.a[10];
    }
}
