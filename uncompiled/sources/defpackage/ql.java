package defpackage;

/* compiled from: AuxEffectInfo.java */
/* renamed from: ql  reason: default package */
/* loaded from: classes.dex */
public final class ql {
    public final int a;
    public final float b;

    public ql(int i, float f) {
        this.a = i;
        this.b = f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ql.class != obj.getClass()) {
            return false;
        }
        ql qlVar = (ql) obj;
        return this.a == qlVar.a && Float.compare(qlVar.b, this.b) == 0;
    }

    public int hashCode() {
        return ((527 + this.a) * 31) + Float.floatToIntBits(this.b);
    }
}
