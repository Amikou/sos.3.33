package defpackage;

import defpackage.kb;
import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: ff5  reason: default package */
/* loaded from: classes2.dex */
public final class ff5 {
    public final Set<String> a;
    public final kb.b b;
    public final tf c;
    public final tc5 d;

    public ff5(tf tfVar, kb.b bVar) {
        this.b = bVar;
        this.c = tfVar;
        tc5 tc5Var = new tc5(this);
        this.d = tc5Var;
        tfVar.b(tc5Var);
        this.a = new HashSet();
    }
}
