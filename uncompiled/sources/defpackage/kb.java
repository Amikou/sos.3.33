package defpackage;

import android.os.Bundle;

/* compiled from: com.google.firebase:firebase-measurement-connector@@19.0.0 */
/* renamed from: kb  reason: default package */
/* loaded from: classes2.dex */
public interface kb {

    /* compiled from: com.google.firebase:firebase-measurement-connector@@19.0.0 */
    /* renamed from: kb$a */
    /* loaded from: classes2.dex */
    public interface a {
    }

    /* compiled from: com.google.firebase:firebase-measurement-connector@@19.0.0 */
    /* renamed from: kb$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a(int i, Bundle bundle);
    }

    a a(String str, b bVar);

    void b(String str, String str2, Bundle bundle);

    void c(String str, String str2, Object obj);
}
