package defpackage;

import android.database.Cursor;
import android.os.Build;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: TableInfo.java */
/* renamed from: f34  reason: default package */
/* loaded from: classes.dex */
public final class f34 {
    public final String a;
    public final Map<String, a> b;
    public final Set<b> c;
    public final Set<d> d;

    /* compiled from: TableInfo.java */
    /* renamed from: f34$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final String a;
        public final String b;
        public final int c;
        public final boolean d;
        public final int e;
        public final String f;
        public final int g;

        public a(String str, String str2, boolean z, int i, String str3, int i2) {
            this.a = str;
            this.b = str2;
            this.d = z;
            this.e = i;
            this.c = c(str2);
            this.f = str3;
            this.g = i2;
        }

        public static boolean a(String str) {
            if (str.length() == 0) {
                return false;
            }
            int i = 0;
            for (int i2 = 0; i2 < str.length(); i2++) {
                char charAt = str.charAt(i2);
                if (i2 == 0 && charAt != '(') {
                    return false;
                }
                if (charAt == '(') {
                    i++;
                } else if (charAt == ')' && i - 1 == 0 && i2 != str.length() - 1) {
                    return false;
                }
            }
            return i == 0;
        }

        public static boolean b(String str, String str2) {
            if (str2 == null) {
                return false;
            }
            if (str.equals(str2)) {
                return true;
            }
            if (a(str)) {
                return str.substring(1, str.length() - 1).trim().equals(str2);
            }
            return false;
        }

        public static int c(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
        }

        public boolean d() {
            return this.e > 0;
        }

        public boolean equals(Object obj) {
            String str;
            String str2;
            String str3;
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (Build.VERSION.SDK_INT >= 20) {
                    if (this.e != aVar.e) {
                        return false;
                    }
                } else if (d() != aVar.d()) {
                    return false;
                }
                if (this.a.equals(aVar.a) && this.d == aVar.d) {
                    if (this.g != 1 || aVar.g != 2 || (str3 = this.f) == null || b(str3, aVar.f)) {
                        if (this.g != 2 || aVar.g != 1 || (str2 = aVar.f) == null || b(str2, this.f)) {
                            int i = this.g;
                            return (i == 0 || i != aVar.g || ((str = this.f) == null ? aVar.f == null : b(str, aVar.f))) && this.c == aVar.c;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.c) * 31) + (this.d ? 1231 : 1237)) * 31) + this.e;
        }

        public String toString() {
            return "Column{name='" + this.a + "', type='" + this.b + "', affinity='" + this.c + "', notNull=" + this.d + ", primaryKeyPosition=" + this.e + ", defaultValue='" + this.f + "'}";
        }
    }

    /* compiled from: TableInfo.java */
    /* renamed from: f34$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final String a;
        public final String b;
        public final String c;
        public final List<String> d;
        public final List<String> e;

        public b(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = Collections.unmodifiableList(list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof b) {
                b bVar = (b) obj;
                if (this.a.equals(bVar.a) && this.b.equals(bVar.b) && this.c.equals(bVar.c) && this.d.equals(bVar.d)) {
                    return this.e.equals(bVar.e);
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return (((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
        }

        public String toString() {
            return "ForeignKey{referenceTable='" + this.a + "', onDelete='" + this.b + "', onUpdate='" + this.c + "', columnNames=" + this.d + ", referenceColumnNames=" + this.e + '}';
        }
    }

    /* compiled from: TableInfo.java */
    /* renamed from: f34$c */
    /* loaded from: classes.dex */
    public static class c implements Comparable<c> {
        public final int a;
        public final int f0;
        public final String g0;
        public final String h0;

        public c(int i, int i2, String str, String str2) {
            this.a = i;
            this.f0 = i2;
            this.g0 = str;
            this.h0 = str2;
        }

        @Override // java.lang.Comparable
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i = this.a - cVar.a;
            return i == 0 ? this.f0 - cVar.f0 : i;
        }
    }

    /* compiled from: TableInfo.java */
    /* renamed from: f34$d */
    /* loaded from: classes.dex */
    public static final class d {
        public final String a;
        public final boolean b;
        public final List<String> c;

        public d(String str, boolean z, List<String> list) {
            this.a = str;
            this.b = z;
            this.c = list;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof d) {
                d dVar = (d) obj;
                if (this.b == dVar.b && this.c.equals(dVar.c)) {
                    if (this.a.startsWith("index_")) {
                        return dVar.a.startsWith("index_");
                    }
                    return this.a.equals(dVar.a);
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return ((((this.a.startsWith("index_") ? -1184239155 : this.a.hashCode()) * 31) + (this.b ? 1 : 0)) * 31) + this.c.hashCode();
        }

        public String toString() {
            return "Index{name='" + this.a + "', unique=" + this.b + ", columns=" + this.c + '}';
        }
    }

    public f34(String str, Map<String, a> map, Set<b> set, Set<d> set2) {
        this.a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        this.d = set2 == null ? null : Collections.unmodifiableSet(set2);
    }

    public static f34 a(sw3 sw3Var, String str) {
        return new f34(str, b(sw3Var, str), d(sw3Var, str), f(sw3Var, str));
    }

    public static Map<String, a> b(sw3 sw3Var, String str) {
        Cursor E0 = sw3Var.E0("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (E0.getColumnCount() > 0) {
                int columnIndex = E0.getColumnIndex(PublicResolver.FUNC_NAME);
                int columnIndex2 = E0.getColumnIndex("type");
                int columnIndex3 = E0.getColumnIndex("notnull");
                int columnIndex4 = E0.getColumnIndex("pk");
                int columnIndex5 = E0.getColumnIndex("dflt_value");
                while (E0.moveToNext()) {
                    String string = E0.getString(columnIndex);
                    hashMap.put(string, new a(string, E0.getString(columnIndex2), E0.getInt(columnIndex3) != 0, E0.getInt(columnIndex4), E0.getString(columnIndex5), 2));
                }
            }
            return hashMap;
        } finally {
            E0.close();
        }
    }

    public static List<c> c(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public static Set<b> d(sw3 sw3Var, String str) {
        HashSet hashSet = new HashSet();
        Cursor E0 = sw3Var.E0("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = E0.getColumnIndex("id");
            int columnIndex2 = E0.getColumnIndex("seq");
            int columnIndex3 = E0.getColumnIndex("table");
            int columnIndex4 = E0.getColumnIndex("on_delete");
            int columnIndex5 = E0.getColumnIndex("on_update");
            List<c> c2 = c(E0);
            int count = E0.getCount();
            for (int i = 0; i < count; i++) {
                E0.moveToPosition(i);
                if (E0.getInt(columnIndex2) == 0) {
                    int i2 = E0.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (c cVar : c2) {
                        if (cVar.a == i2) {
                            arrayList.add(cVar.g0);
                            arrayList2.add(cVar.h0);
                        }
                    }
                    hashSet.add(new b(E0.getString(columnIndex3), E0.getString(columnIndex4), E0.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            E0.close();
        }
    }

    public static d e(sw3 sw3Var, String str, boolean z) {
        Cursor E0 = sw3Var.E0("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = E0.getColumnIndex("seqno");
            int columnIndex2 = E0.getColumnIndex("cid");
            int columnIndex3 = E0.getColumnIndex(PublicResolver.FUNC_NAME);
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                TreeMap treeMap = new TreeMap();
                while (E0.moveToNext()) {
                    if (E0.getInt(columnIndex2) >= 0) {
                        int i = E0.getInt(columnIndex);
                        treeMap.put(Integer.valueOf(i), E0.getString(columnIndex3));
                    }
                }
                ArrayList arrayList = new ArrayList(treeMap.size());
                arrayList.addAll(treeMap.values());
                return new d(str, z, arrayList);
            }
            return null;
        } finally {
            E0.close();
        }
    }

    public static Set<d> f(sw3 sw3Var, String str) {
        Cursor E0 = sw3Var.E0("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = E0.getColumnIndex(PublicResolver.FUNC_NAME);
            int columnIndex2 = E0.getColumnIndex("origin");
            int columnIndex3 = E0.getColumnIndex("unique");
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                HashSet hashSet = new HashSet();
                while (E0.moveToNext()) {
                    if ("c".equals(E0.getString(columnIndex2))) {
                        String string = E0.getString(columnIndex);
                        boolean z = true;
                        if (E0.getInt(columnIndex3) != 1) {
                            z = false;
                        }
                        d e = e(sw3Var, string, z);
                        if (e == null) {
                            return null;
                        }
                        hashSet.add(e);
                    }
                }
                return hashSet;
            }
            return null;
        } finally {
            E0.close();
        }
    }

    public boolean equals(Object obj) {
        Set<d> set;
        if (this == obj) {
            return true;
        }
        if (obj instanceof f34) {
            f34 f34Var = (f34) obj;
            String str = this.a;
            if (str == null ? f34Var.a == null : str.equals(f34Var.a)) {
                Map<String, a> map = this.b;
                if (map == null ? f34Var.b == null : map.equals(f34Var.b)) {
                    Set<b> set2 = this.c;
                    if (set2 == null ? f34Var.c == null : set2.equals(f34Var.c)) {
                        Set<d> set3 = this.d;
                        if (set3 == null || (set = f34Var.d) == null) {
                            return true;
                        }
                        return set3.equals(set);
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, a> map = this.b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        Set<b> set = this.c;
        return hashCode2 + (set != null ? set.hashCode() : 0);
    }

    public String toString() {
        return "TableInfo{name='" + this.a + "', columns=" + this.b + ", foreignKeys=" + this.c + ", indices=" + this.d + '}';
    }
}
