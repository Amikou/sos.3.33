package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.utils.Utils;
import defpackage.fe;

/* compiled from: BitmapAnimationBackend.java */
/* renamed from: pp  reason: default package */
/* loaded from: classes.dex */
public class pp implements de, fe.b {
    public static final Class<?> m = pp.class;
    public final br2 a;
    public final xp b;
    public final ke c;
    public final aq d;
    public final yp e;
    public final zp f;
    public Rect h;
    public int i;
    public int j;
    public a l;
    public Bitmap.Config k = Bitmap.Config.ARGB_8888;
    public final Paint g = new Paint(6);

    /* compiled from: BitmapAnimationBackend.java */
    /* renamed from: pp$a */
    /* loaded from: classes.dex */
    public interface a {
        void a(pp ppVar, int i);

        void b(pp ppVar, int i);

        void c(pp ppVar, int i, int i2);
    }

    public pp(br2 br2Var, xp xpVar, ke keVar, aq aqVar, yp ypVar, zp zpVar) {
        this.a = br2Var;
        this.b = xpVar;
        this.c = keVar;
        this.d = aqVar;
        this.e = ypVar;
        this.f = zpVar;
        n();
    }

    @Override // defpackage.ke
    public int a() {
        return this.c.a();
    }

    @Override // defpackage.ke
    public int b() {
        return this.c.b();
    }

    @Override // defpackage.de
    public int c() {
        return this.j;
    }

    @Override // defpackage.de
    public void clear() {
        this.b.clear();
    }

    @Override // defpackage.de
    public void d(Rect rect) {
        this.h = rect;
        this.d.d(rect);
        n();
    }

    @Override // defpackage.de
    public int e() {
        return this.i;
    }

    @Override // defpackage.fe.b
    public void f() {
        clear();
    }

    @Override // defpackage.de
    public void g(ColorFilter colorFilter) {
        this.g.setColorFilter(colorFilter);
    }

    @Override // defpackage.ke
    public int h(int i) {
        return this.c.h(i);
    }

    @Override // defpackage.de
    public void i(int i) {
        this.g.setAlpha(i);
    }

    @Override // defpackage.de
    public boolean j(Drawable drawable, Canvas canvas, int i) {
        zp zpVar;
        a aVar;
        a aVar2 = this.l;
        if (aVar2 != null) {
            aVar2.a(this, i);
        }
        boolean l = l(canvas, i, 0);
        if (!l && (aVar = this.l) != null) {
            aVar.b(this, i);
        }
        yp ypVar = this.e;
        if (ypVar != null && (zpVar = this.f) != null) {
            ypVar.a(zpVar, this.b, this, i);
        }
        return l;
    }

    public final boolean k(int i, com.facebook.common.references.a<Bitmap> aVar, Canvas canvas, int i2) {
        if (com.facebook.common.references.a.u(aVar)) {
            if (this.h == null) {
                canvas.drawBitmap(aVar.j(), Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, this.g);
            } else {
                canvas.drawBitmap(aVar.j(), (Rect) null, this.h, this.g);
            }
            if (i2 != 3) {
                this.b.e(i, aVar, i2);
            }
            a aVar2 = this.l;
            if (aVar2 != null) {
                aVar2.c(this, i, i2);
                return true;
            }
            return true;
        }
        return false;
    }

    public final boolean l(Canvas canvas, int i, int i2) {
        com.facebook.common.references.a<Bitmap> d;
        boolean k;
        int i3 = 3;
        boolean z = false;
        try {
            if (i2 != 0) {
                if (i2 == 1) {
                    d = this.b.a(i, this.i, this.j);
                    if (m(i, d) && k(i, d, canvas, 1)) {
                        z = true;
                    }
                    i3 = 2;
                } else if (i2 == 2) {
                    d = this.a.a(this.i, this.j, this.k);
                    if (m(i, d) && k(i, d, canvas, 2)) {
                        z = true;
                    }
                } else if (i2 != 3) {
                    return false;
                } else {
                    d = this.b.f(i);
                    k = k(i, d, canvas, 3);
                    i3 = -1;
                }
                k = z;
            } else {
                d = this.b.d(i);
                k = k(i, d, canvas, 0);
                i3 = 1;
            }
            com.facebook.common.references.a.g(d);
            return (k || i3 == -1) ? k : l(canvas, i, i3);
        } catch (RuntimeException e) {
            v11.u(m, "Failed to create frame bitmap", e);
            return false;
        } finally {
            com.facebook.common.references.a.g(null);
        }
    }

    public final boolean m(int i, com.facebook.common.references.a<Bitmap> aVar) {
        if (com.facebook.common.references.a.u(aVar)) {
            boolean a2 = this.d.a(i, aVar.j());
            if (!a2) {
                com.facebook.common.references.a.g(aVar);
            }
            return a2;
        }
        return false;
    }

    public final void n() {
        int e = this.d.e();
        this.i = e;
        if (e == -1) {
            Rect rect = this.h;
            this.i = rect == null ? -1 : rect.width();
        }
        int c = this.d.c();
        this.j = c;
        if (c == -1) {
            Rect rect2 = this.h;
            this.j = rect2 != null ? rect2.height() : -1;
        }
    }
}
