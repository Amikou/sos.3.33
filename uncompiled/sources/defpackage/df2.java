package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

/* compiled from: NetworkStateTracker.java */
/* renamed from: df2  reason: default package */
/* loaded from: classes.dex */
public class df2 extends g60<cf2> {
    public static final String j = v12.f("NetworkStateTracker");
    public final ConnectivityManager g;
    public b h;
    public a i;

    /* compiled from: NetworkStateTracker.java */
    /* renamed from: df2$a */
    /* loaded from: classes.dex */
    public class a extends BroadcastReceiver {
        public a() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null || !intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                return;
            }
            v12.c().a(df2.j, "Network broadcast received", new Throwable[0]);
            df2 df2Var = df2.this;
            df2Var.d(df2Var.g());
        }
    }

    /* compiled from: NetworkStateTracker.java */
    /* renamed from: df2$b */
    /* loaded from: classes.dex */
    public class b extends ConnectivityManager.NetworkCallback {
        public b() {
        }

        @Override // android.net.ConnectivityManager.NetworkCallback
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            v12.c().a(df2.j, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
            df2 df2Var = df2.this;
            df2Var.d(df2Var.g());
        }

        @Override // android.net.ConnectivityManager.NetworkCallback
        public void onLost(Network network) {
            v12.c().a(df2.j, "Network connection lost", new Throwable[0]);
            df2 df2Var = df2.this;
            df2Var.d(df2Var.g());
        }
    }

    public df2(Context context, q34 q34Var) {
        super(context, q34Var);
        this.g = (ConnectivityManager) this.b.getSystemService("connectivity");
        if (j()) {
            this.h = new b();
        } else {
            this.i = new a();
        }
    }

    public static boolean j() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @Override // defpackage.g60
    public void e() {
        if (j()) {
            try {
                v12.c().a(j, "Registering network callback", new Throwable[0]);
                this.g.registerDefaultNetworkCallback(this.h);
                return;
            } catch (IllegalArgumentException | SecurityException e) {
                v12.c().b(j, "Received exception while registering network callback", e);
                return;
            }
        }
        v12.c().a(j, "Registering broadcast receiver", new Throwable[0]);
        this.b.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override // defpackage.g60
    public void f() {
        if (j()) {
            try {
                v12.c().a(j, "Unregistering network callback", new Throwable[0]);
                this.g.unregisterNetworkCallback(this.h);
                return;
            } catch (IllegalArgumentException | SecurityException e) {
                v12.c().b(j, "Received exception while unregistering network callback", e);
                return;
            }
        }
        v12.c().a(j, "Unregistering broadcast receiver", new Throwable[0]);
        this.b.unregisterReceiver(this.i);
    }

    public cf2 g() {
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        boolean z = true;
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean i = i();
        boolean a2 = w50.a(this.g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new cf2(z2, i, a2, z);
    }

    @Override // defpackage.g60
    /* renamed from: h */
    public cf2 b() {
        return g();
    }

    public boolean i() {
        if (Build.VERSION.SDK_INT < 23) {
            return false;
        }
        try {
            NetworkCapabilities networkCapabilities = this.g.getNetworkCapabilities(this.g.getActiveNetwork());
            if (networkCapabilities != null) {
                return networkCapabilities.hasCapability(16);
            }
            return false;
        } catch (SecurityException e) {
            v12.c().b(j, "Unable to validate active network", e);
            return false;
        }
    }
}
