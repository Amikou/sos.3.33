package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityCreateWalletBinding.java */
/* renamed from: b7  reason: default package */
/* loaded from: classes2.dex */
public final class b7 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final AppCompatButton c;
    public final TextView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;
    public final TextView h;
    public final TextView i;
    public final TextView j;
    public final TextView k;
    public final TextView l;
    public final TextView m;
    public final TextView n;
    public final TextView o;

    public b7(ConstraintLayout constraintLayout, MaterialButton materialButton, AppCompatButton appCompatButton, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, TextView textView9, TextView textView10, TextView textView11, TextView textView12, ScrollView scrollView, TextView textView13, TextView textView14, TextView textView15, TextView textView16, TextView textView17, TextView textView18, TextView textView19, TextView textView20, TextView textView21, TextView textView22, TextView textView23, TextView textView24) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = appCompatButton;
        this.d = textView13;
        this.e = textView14;
        this.f = textView15;
        this.g = textView16;
        this.h = textView17;
        this.i = textView18;
        this.j = textView19;
        this.k = textView20;
        this.l = textView21;
        this.m = textView22;
        this.n = textView23;
        this.o = textView24;
    }

    public static b7 a(View view) {
        int i = R.id.btnBack;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnBack);
        if (materialButton != null) {
            i = R.id.btnContinue;
            AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btnContinue);
            if (appCompatButton != null) {
                i = R.id.indexWord1;
                TextView textView = (TextView) ai4.a(view, R.id.indexWord1);
                if (textView != null) {
                    i = R.id.indexWord10;
                    TextView textView2 = (TextView) ai4.a(view, R.id.indexWord10);
                    if (textView2 != null) {
                        i = R.id.indexWord11;
                        TextView textView3 = (TextView) ai4.a(view, R.id.indexWord11);
                        if (textView3 != null) {
                            i = R.id.indexWord12;
                            TextView textView4 = (TextView) ai4.a(view, R.id.indexWord12);
                            if (textView4 != null) {
                                i = R.id.indexWord2;
                                TextView textView5 = (TextView) ai4.a(view, R.id.indexWord2);
                                if (textView5 != null) {
                                    i = R.id.indexWord3;
                                    TextView textView6 = (TextView) ai4.a(view, R.id.indexWord3);
                                    if (textView6 != null) {
                                        i = R.id.indexWord4;
                                        TextView textView7 = (TextView) ai4.a(view, R.id.indexWord4);
                                        if (textView7 != null) {
                                            i = R.id.indexWord5;
                                            TextView textView8 = (TextView) ai4.a(view, R.id.indexWord5);
                                            if (textView8 != null) {
                                                i = R.id.indexWord6;
                                                TextView textView9 = (TextView) ai4.a(view, R.id.indexWord6);
                                                if (textView9 != null) {
                                                    i = R.id.indexWord7;
                                                    TextView textView10 = (TextView) ai4.a(view, R.id.indexWord7);
                                                    if (textView10 != null) {
                                                        i = R.id.indexWord8;
                                                        TextView textView11 = (TextView) ai4.a(view, R.id.indexWord8);
                                                        if (textView11 != null) {
                                                            i = R.id.indexWord9;
                                                            TextView textView12 = (TextView) ai4.a(view, R.id.indexWord9);
                                                            if (textView12 != null) {
                                                                i = R.id.scrollView2;
                                                                ScrollView scrollView = (ScrollView) ai4.a(view, R.id.scrollView2);
                                                                if (scrollView != null) {
                                                                    i = R.id.word1;
                                                                    TextView textView13 = (TextView) ai4.a(view, R.id.word1);
                                                                    if (textView13 != null) {
                                                                        i = R.id.word10;
                                                                        TextView textView14 = (TextView) ai4.a(view, R.id.word10);
                                                                        if (textView14 != null) {
                                                                            i = R.id.word11;
                                                                            TextView textView15 = (TextView) ai4.a(view, R.id.word11);
                                                                            if (textView15 != null) {
                                                                                i = R.id.word12;
                                                                                TextView textView16 = (TextView) ai4.a(view, R.id.word12);
                                                                                if (textView16 != null) {
                                                                                    i = R.id.word2;
                                                                                    TextView textView17 = (TextView) ai4.a(view, R.id.word2);
                                                                                    if (textView17 != null) {
                                                                                        i = R.id.word3;
                                                                                        TextView textView18 = (TextView) ai4.a(view, R.id.word3);
                                                                                        if (textView18 != null) {
                                                                                            i = R.id.word4;
                                                                                            TextView textView19 = (TextView) ai4.a(view, R.id.word4);
                                                                                            if (textView19 != null) {
                                                                                                i = R.id.word5;
                                                                                                TextView textView20 = (TextView) ai4.a(view, R.id.word5);
                                                                                                if (textView20 != null) {
                                                                                                    i = R.id.word6;
                                                                                                    TextView textView21 = (TextView) ai4.a(view, R.id.word6);
                                                                                                    if (textView21 != null) {
                                                                                                        i = R.id.word7;
                                                                                                        TextView textView22 = (TextView) ai4.a(view, R.id.word7);
                                                                                                        if (textView22 != null) {
                                                                                                            i = R.id.word8;
                                                                                                            TextView textView23 = (TextView) ai4.a(view, R.id.word8);
                                                                                                            if (textView23 != null) {
                                                                                                                i = R.id.word9;
                                                                                                                TextView textView24 = (TextView) ai4.a(view, R.id.word9);
                                                                                                                if (textView24 != null) {
                                                                                                                    return new b7((ConstraintLayout) view, materialButton, appCompatButton, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9, textView10, textView11, textView12, scrollView, textView13, textView14, textView15, textView16, textView17, textView18, textView19, textView20, textView21, textView22, textView23, textView24);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static b7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static b7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_create_wallet, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
