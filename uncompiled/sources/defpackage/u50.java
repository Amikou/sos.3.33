package defpackage;

import android.os.Bundle;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: u50  reason: default package */
/* loaded from: classes.dex */
public interface u50 {
    void onConnected(Bundle bundle);

    void onConnectionSuspended(@RecentlyNonNull int i);
}
