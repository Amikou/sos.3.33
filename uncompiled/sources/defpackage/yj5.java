package defpackage;

import com.google.android.gms.internal.measurement.d1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: yj5  reason: default package */
/* loaded from: classes.dex */
public final class yj5 extends w1<d1, yj5> implements xx5 {
    public yj5() {
        super(d1.K());
    }

    public final yj5 A(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.O((d1) this.f0, j);
        return this;
    }

    public final yj5 B() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.P((d1) this.f0);
        return this;
    }

    public final yj5 C(double d) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.Q((d1) this.f0, d);
        return this;
    }

    public final yj5 D() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.R((d1) this.f0);
        return this;
    }

    public final int E() {
        return ((d1) this.f0).I();
    }

    public final yj5 G(yj5 yj5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.S((d1) this.f0, yj5Var.o());
        return this;
    }

    public final yj5 H(Iterable<? extends d1> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.T((d1) this.f0, iterable);
        return this;
    }

    public final yj5 I() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.U((d1) this.f0);
        return this;
    }

    public final yj5 v(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.L((d1) this.f0, str);
        return this;
    }

    public final yj5 x(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.M((d1) this.f0, str);
        return this;
    }

    public final yj5 y() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        d1.N((d1) this.f0);
        return this;
    }

    public /* synthetic */ yj5(wi5 wi5Var) {
        super(d1.K());
    }
}
