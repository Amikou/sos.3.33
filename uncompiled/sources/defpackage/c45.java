package defpackage;

import com.google.android.gms.internal.firebase_messaging.b;
import com.google.firebase.encoders.c;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: c45  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class c45 implements hl2 {
    public static final hl2 a = new c45();

    @Override // com.google.firebase.encoders.b
    public final void a(Object obj, c cVar) {
        b.k((Map.Entry) obj, cVar);
    }
}
