package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ViewUtils.java */
/* renamed from: mk4  reason: default package */
/* loaded from: classes2.dex */
public class mk4 {

    /* compiled from: ViewUtils.java */
    /* renamed from: mk4$a */
    /* loaded from: classes2.dex */
    public static class a implements Runnable {
        public final /* synthetic */ View a;

        public a(View view) {
            this.a = view;
        }

        @Override // java.lang.Runnable
        public void run() {
            ((InputMethodManager) this.a.getContext().getSystemService("input_method")).showSoftInput(this.a, 1);
        }
    }

    /* compiled from: ViewUtils.java */
    /* renamed from: mk4$b */
    /* loaded from: classes2.dex */
    public static class b implements e {
        public final /* synthetic */ boolean a;
        public final /* synthetic */ boolean b;
        public final /* synthetic */ boolean c;
        public final /* synthetic */ e d;

        public b(boolean z, boolean z2, boolean z3, e eVar) {
            this.a = z;
            this.b = z2;
            this.c = z3;
            this.d = eVar;
        }

        @Override // defpackage.mk4.e
        public jp4 a(View view, jp4 jp4Var, f fVar) {
            if (this.a) {
                fVar.d += jp4Var.j();
            }
            boolean h = mk4.h(view);
            if (this.b) {
                if (h) {
                    fVar.c += jp4Var.k();
                } else {
                    fVar.a += jp4Var.k();
                }
            }
            if (this.c) {
                if (h) {
                    fVar.a += jp4Var.l();
                } else {
                    fVar.c += jp4Var.l();
                }
            }
            fVar.a(view);
            e eVar = this.d;
            return eVar != null ? eVar.a(view, jp4Var, fVar) : jp4Var;
        }
    }

    /* compiled from: ViewUtils.java */
    /* renamed from: mk4$c */
    /* loaded from: classes2.dex */
    public static class c implements em2 {
        public final /* synthetic */ e a;
        public final /* synthetic */ f b;

        public c(e eVar, f fVar) {
            this.a = eVar;
            this.b = fVar;
        }

        @Override // defpackage.em2
        public jp4 a(View view, jp4 jp4Var) {
            return this.a.a(view, jp4Var, new f(this.b));
        }
    }

    /* compiled from: ViewUtils.java */
    /* renamed from: mk4$d */
    /* loaded from: classes2.dex */
    public static class d implements View.OnAttachStateChangeListener {
        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
            view.removeOnAttachStateChangeListener(this);
            ei4.q0(view);
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
        }
    }

    /* compiled from: ViewUtils.java */
    /* renamed from: mk4$e */
    /* loaded from: classes2.dex */
    public interface e {
        jp4 a(View view, jp4 jp4Var, f fVar);
    }

    public static void a(View view, e eVar) {
        ei4.G0(view, new c(eVar, new f(ei4.J(view), view.getPaddingTop(), ei4.I(view), view.getPaddingBottom())));
        j(view);
    }

    public static void b(View view, AttributeSet attributeSet, int i, int i2, e eVar) {
        TypedArray obtainStyledAttributes = view.getContext().obtainStyledAttributes(attributeSet, o23.Insets, i, i2);
        boolean z = obtainStyledAttributes.getBoolean(o23.Insets_paddingBottomSystemWindowInsets, false);
        boolean z2 = obtainStyledAttributes.getBoolean(o23.Insets_paddingLeftSystemWindowInsets, false);
        boolean z3 = obtainStyledAttributes.getBoolean(o23.Insets_paddingRightSystemWindowInsets, false);
        obtainStyledAttributes.recycle();
        a(view, new b(z, z2, z3, eVar));
    }

    public static float c(Context context, int i) {
        return TypedValue.applyDimension(1, i, context.getResources().getDisplayMetrics());
    }

    public static ViewGroup d(View view) {
        if (view == null) {
            return null;
        }
        View rootView = view.getRootView();
        ViewGroup viewGroup = (ViewGroup) rootView.findViewById(16908290);
        if (viewGroup != null) {
            return viewGroup;
        }
        if (rootView == view || !(rootView instanceof ViewGroup)) {
            return null;
        }
        return (ViewGroup) rootView;
    }

    public static pj4 e(View view) {
        return f(d(view));
    }

    public static pj4 f(View view) {
        if (view == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            return new nj4(view);
        }
        return mj4.c(view);
    }

    public static float g(View view) {
        float f2 = Utils.FLOAT_EPSILON;
        for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
            f2 += ei4.y((View) parent);
        }
        return f2;
    }

    public static boolean h(View view) {
        return ei4.E(view) == 1;
    }

    public static PorterDuff.Mode i(int i, PorterDuff.Mode mode) {
        if (i != 3) {
            if (i != 5) {
                if (i != 9) {
                    switch (i) {
                        case 14:
                            return PorterDuff.Mode.MULTIPLY;
                        case 15:
                            return PorterDuff.Mode.SCREEN;
                        case 16:
                            return PorterDuff.Mode.ADD;
                        default:
                            return mode;
                    }
                }
                return PorterDuff.Mode.SRC_ATOP;
            }
            return PorterDuff.Mode.SRC_IN;
        }
        return PorterDuff.Mode.SRC_OVER;
    }

    public static void j(View view) {
        if (ei4.V(view)) {
            ei4.q0(view);
        } else {
            view.addOnAttachStateChangeListener(new d());
        }
    }

    public static void k(View view) {
        view.requestFocus();
        view.post(new a(view));
    }

    /* compiled from: ViewUtils.java */
    /* renamed from: mk4$f */
    /* loaded from: classes2.dex */
    public static class f {
        public int a;
        public int b;
        public int c;
        public int d;

        public f(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }

        public void a(View view) {
            ei4.H0(view, this.a, this.b, this.c, this.d);
        }

        public f(f fVar) {
            this.a = fVar.a;
            this.b = fVar.b;
            this.c = fVar.c;
            this.d = fVar.d;
        }
    }
}
