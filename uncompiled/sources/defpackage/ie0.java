package defpackage;

/* compiled from: DataSources.java */
/* renamed from: ie0  reason: default package */
/* loaded from: classes.dex */
public class ie0 {

    /* compiled from: DataSources.java */
    /* renamed from: ie0$a */
    /* loaded from: classes.dex */
    public static class a implements fw3<ge0<T>> {
        public final /* synthetic */ Throwable a;

        public a(Throwable th) {
            this.a = th;
        }

        @Override // defpackage.fw3
        /* renamed from: a */
        public ge0<T> get() {
            return ie0.b(this.a);
        }
    }

    public static <T> fw3<ge0<T>> a(Throwable th) {
        return new a(th);
    }

    public static <T> ge0<T> b(Throwable th) {
        ap3 x = ap3.x();
        x.p(th);
        return x;
    }
}
