package defpackage;

import java.io.IOException;
import java.io.InputStream;
import okio.b;
import okio.k;
import okio.n;
import okio.o;

/* compiled from: JvmOkio.kt */
/* renamed from: ar1  reason: default package */
/* loaded from: classes2.dex */
public final class ar1 implements n {
    public final InputStream a;
    public final o f0;

    public ar1(InputStream inputStream, o oVar) {
        fs1.f(inputStream, "input");
        fs1.f(oVar, "timeout");
        this.a = inputStream;
        this.f0 = oVar;
    }

    @Override // okio.n, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @Override // okio.n
    public long read(b bVar, long j) {
        fs1.f(bVar, "sink");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i == 0) {
            return 0L;
        }
        if (i >= 0) {
            try {
                this.f0.throwIfReached();
                bj3 f0 = bVar.f0(1);
                int read = this.a.read(f0.a, f0.c, (int) Math.min(j, 8192 - f0.c));
                if (read == -1) {
                    if (f0.b == f0.c) {
                        bVar.a = f0.b();
                        dj3.b(f0);
                        return -1L;
                    }
                    return -1L;
                }
                f0.c += read;
                long j2 = read;
                bVar.X(bVar.a0() + j2);
                return j2;
            } catch (AssertionError e) {
                if (k.e(e)) {
                    throw new IOException(e);
                }
                throw e;
            }
        }
        throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
    }

    @Override // okio.n
    public o timeout() {
        return this.f0;
    }

    public String toString() {
        return "source(" + this.a + ')';
    }
}
