package defpackage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.internal.MainDispatcherFactory;

/* compiled from: FastServiceLoader.kt */
/* renamed from: h21  reason: default package */
/* loaded from: classes2.dex */
public final class h21 {
    public static final h21 a = new h21();

    public final <S> S a(String str, ClassLoader classLoader, Class<S> cls) {
        Class<?> cls2 = Class.forName(str, false, classLoader);
        if (cls.isAssignableFrom(cls2)) {
            return cls.cast(cls2.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        }
        throw new IllegalArgumentException(("Expected service of class " + cls + ", but found " + cls2).toString());
    }

    public final <S> List<S> b(Class<S> cls, ClassLoader classLoader) {
        try {
            return d(cls, classLoader);
        } catch (Throwable unused) {
            return j20.k0(ServiceLoader.load(cls, classLoader));
        }
    }

    public final List<MainDispatcherFactory> c() {
        MainDispatcherFactory mainDispatcherFactory;
        if (!i21.a()) {
            return b(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
        try {
            ArrayList arrayList = new ArrayList(2);
            MainDispatcherFactory mainDispatcherFactory2 = null;
            try {
                mainDispatcherFactory = (MainDispatcherFactory) MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.android.AndroidDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } catch (ClassNotFoundException unused) {
                mainDispatcherFactory = null;
            }
            if (mainDispatcherFactory != null) {
                arrayList.add(mainDispatcherFactory);
            }
            try {
                mainDispatcherFactory2 = (MainDispatcherFactory) MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.test.internal.TestMainDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
            } catch (ClassNotFoundException unused2) {
            }
            if (mainDispatcherFactory2 == null) {
                return arrayList;
            }
            arrayList.add(mainDispatcherFactory2);
            return arrayList;
        } catch (Throwable unused3) {
            return b(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
    }

    public final <S> List<S> d(Class<S> cls, ClassLoader classLoader) {
        ArrayList<URL> list = Collections.list(classLoader.getResources(fs1.l("META-INF/services/", cls.getName())));
        fs1.e(list, "java.util.Collections.list(this)");
        ArrayList arrayList = new ArrayList();
        for (URL url : list) {
            g20.v(arrayList, a.e(url));
        }
        Set<String> o0 = j20.o0(arrayList);
        if (!o0.isEmpty()) {
            ArrayList arrayList2 = new ArrayList(c20.q(o0, 10));
            for (String str : o0) {
                arrayList2.add(a.a(str, classLoader, cls));
            }
            return arrayList2;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }

    public final List<String> e(URL url) {
        String url2 = url.toString();
        if (dv3.H(url2, "jar", false, 2, null)) {
            String I0 = StringsKt__StringsKt.I0(StringsKt__StringsKt.D0(url2, "jar:file:", null, 2, null), '!', null, 2, null);
            String D0 = StringsKt__StringsKt.D0(url2, "!/", null, 2, null);
            JarFile jarFile = new JarFile(I0, false);
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(jarFile.getInputStream(new ZipEntry(D0)), "UTF-8"));
                List<String> f = a.f(bufferedReader);
                yz.a(bufferedReader, null);
                jarFile.close();
                return f;
            } catch (Throwable th) {
                try {
                    throw th;
                } catch (Throwable th2) {
                    try {
                        jarFile.close();
                        throw th2;
                    } catch (Throwable th3) {
                        py0.a(th, th3);
                        throw th;
                    }
                }
            }
        }
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(url.openStream()));
        try {
            List<String> f2 = a.f(bufferedReader2);
            yz.a(bufferedReader2, null);
            return f2;
        } catch (Throwable th4) {
            try {
                throw th4;
            } catch (Throwable th5) {
                yz.a(bufferedReader2, th4);
                throw th5;
            }
        }
    }

    public final List<String> f(BufferedReader bufferedReader) {
        boolean z;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return j20.k0(linkedHashSet);
            }
            String J0 = StringsKt__StringsKt.J0(readLine, "#", null, 2, null);
            Objects.requireNonNull(J0, "null cannot be cast to non-null type kotlin.CharSequence");
            String obj = StringsKt__StringsKt.K0(J0).toString();
            int i = 0;
            while (true) {
                if (i >= obj.length()) {
                    z = true;
                    break;
                }
                char charAt = obj.charAt(i);
                if (!(charAt == '.' || Character.isJavaIdentifierPart(charAt))) {
                    z = false;
                    break;
                }
                i++;
            }
            if (z) {
                if (obj.length() > 0) {
                    linkedHashSet.add(obj);
                }
            } else {
                throw new IllegalArgumentException(fs1.l("Illegal service provider class name: ", obj).toString());
            }
        }
    }
}
