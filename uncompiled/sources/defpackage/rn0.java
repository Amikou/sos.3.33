package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogDarkSecurityQuestionsConfirmationBinding.java */
/* renamed from: rn0  reason: default package */
/* loaded from: classes2.dex */
public final class rn0 {
    public final CardView a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final TextView d;
    public final TextView e;
    public final TextView f;

    public rn0(CardView cardView, MaterialButton materialButton, MaterialButton materialButton2, TextView textView, TextView textView2, TextView textView3) {
        this.a = cardView;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = textView;
        this.e = textView2;
        this.f = textView3;
    }

    public static rn0 a(View view) {
        int i = R.id.btnCancel;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnCancel);
        if (materialButton != null) {
            i = R.id.btnConfirm;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnConfirm);
            if (materialButton2 != null) {
                i = R.id.tvAnswer1;
                TextView textView = (TextView) ai4.a(view, R.id.tvAnswer1);
                if (textView != null) {
                    i = R.id.tvAnswer2;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvAnswer2);
                    if (textView2 != null) {
                        i = R.id.tvDialogTitle;
                        TextView textView3 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                        if (textView3 != null) {
                            return new rn0((CardView) view, materialButton, materialButton2, textView, textView2, textView3);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
