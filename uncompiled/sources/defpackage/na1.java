package defpackage;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.l;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: FragmentManagerViewModel.java */
/* renamed from: na1  reason: default package */
/* loaded from: classes.dex */
public final class na1 extends dj4 {
    public static final l.b h = new a();
    public final boolean d;
    public final HashMap<String, Fragment> a = new HashMap<>();
    public final HashMap<String, na1> b = new HashMap<>();
    public final HashMap<String, gj4> c = new HashMap<>();
    public boolean e = false;
    public boolean f = false;
    public boolean g = false;

    /* compiled from: FragmentManagerViewModel.java */
    /* renamed from: na1$a */
    /* loaded from: classes.dex */
    public class a implements l.b {
        @Override // androidx.lifecycle.l.b
        public <T extends dj4> T a(Class<T> cls) {
            return new na1(true);
        }
    }

    public na1(boolean z) {
        this.d = z;
    }

    public static na1 e(gj4 gj4Var) {
        return (na1) new l(gj4Var, h).a(na1.class);
    }

    public void a(Fragment fragment) {
        if (this.g) {
            FragmentManager.H0(2);
        } else if (this.a.containsKey(fragment.mWho)) {
        } else {
            this.a.put(fragment.mWho, fragment);
            if (FragmentManager.H0(2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Updating retained Fragments: Added ");
                sb.append(fragment);
            }
        }
    }

    public void b(Fragment fragment) {
        if (FragmentManager.H0(3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Clearing non-config state for ");
            sb.append(fragment);
        }
        na1 na1Var = this.b.get(fragment.mWho);
        if (na1Var != null) {
            na1Var.onCleared();
            this.b.remove(fragment.mWho);
        }
        gj4 gj4Var = this.c.get(fragment.mWho);
        if (gj4Var != null) {
            gj4Var.a();
            this.c.remove(fragment.mWho);
        }
    }

    public Fragment c(String str) {
        return this.a.get(str);
    }

    public na1 d(Fragment fragment) {
        na1 na1Var = this.b.get(fragment.mWho);
        if (na1Var == null) {
            na1 na1Var2 = new na1(this.d);
            this.b.put(fragment.mWho, na1Var2);
            return na1Var2;
        }
        return na1Var;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || na1.class != obj.getClass()) {
            return false;
        }
        na1 na1Var = (na1) obj;
        return this.a.equals(na1Var.a) && this.b.equals(na1Var.b) && this.c.equals(na1Var.c);
    }

    public Collection<Fragment> f() {
        return new ArrayList(this.a.values());
    }

    public gj4 g(Fragment fragment) {
        gj4 gj4Var = this.c.get(fragment.mWho);
        if (gj4Var == null) {
            gj4 gj4Var2 = new gj4();
            this.c.put(fragment.mWho, gj4Var2);
            return gj4Var2;
        }
        return gj4Var;
    }

    public boolean h() {
        return this.e;
    }

    public int hashCode() {
        return (((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    public void i(Fragment fragment) {
        if (this.g) {
            FragmentManager.H0(2);
            return;
        }
        if ((this.a.remove(fragment.mWho) != null) && FragmentManager.H0(2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Updating retained Fragments: Removed ");
            sb.append(fragment);
        }
    }

    public void j(boolean z) {
        this.g = z;
    }

    public boolean k(Fragment fragment) {
        if (this.a.containsKey(fragment.mWho)) {
            if (this.d) {
                return this.e;
            }
            return !this.f;
        }
        return true;
    }

    @Override // defpackage.dj4
    public void onCleared() {
        if (FragmentManager.H0(3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("onCleared called for ");
            sb.append(this);
        }
        this.e = true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator<Fragment> it = this.a.values().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator<String> it2 = this.b.keySet().iterator();
        while (it2.hasNext()) {
            sb.append(it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator<String> it3 = this.c.keySet().iterator();
        while (it3.hasNext()) {
            sb.append(it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}
