package defpackage;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: s46  reason: default package */
/* loaded from: classes.dex */
public final class s46 {
    public static Object a = new Object();
    public static boolean b;
    public static int c;

    public static int a(Context context) {
        b(context);
        return c;
    }

    public static void b(Context context) {
        Bundle bundle;
        synchronized (a) {
            if (b) {
                return;
            }
            b = true;
            try {
                bundle = kr4.a(context).c(context.getPackageName(), 128).metaData;
            } catch (PackageManager.NameNotFoundException e) {
                Log.wtf("MetadataValueReader", "This should never happen.", e);
            }
            if (bundle == null) {
                return;
            }
            bundle.getString("com.google.app.id");
            c = bundle.getInt("com.google.android.gms.version");
        }
    }
}
