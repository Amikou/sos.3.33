package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: g36  reason: default package */
/* loaded from: classes.dex */
public final class g36 implements yp5<o36> {
    public static final g36 f0 = new g36();
    public final yp5<o36> a = gq5.a(gq5.b(new p36()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final o36 zza() {
        return this.a.zza();
    }
}
