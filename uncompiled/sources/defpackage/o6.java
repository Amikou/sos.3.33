package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktAnswerQuestionBinding.java */
/* renamed from: o6  reason: default package */
/* loaded from: classes2.dex */
public final class o6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final LinearLayout c;
    public final TextInputLayout d;
    public final TextInputLayout e;
    public final sp3 f;
    public final TextView g;
    public final TextView h;

    public o6(ConstraintLayout constraintLayout, MaterialButton materialButton, LinearLayout linearLayout, ConstraintLayout constraintLayout2, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, AppCompatImageView appCompatImageView, sp3 sp3Var, TextView textView, TextView textView2) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = linearLayout;
        this.d = textInputLayout;
        this.e = textInputLayout2;
        this.f = sp3Var;
        this.g = textView;
        this.h = textView2;
    }

    public static o6 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            i = R.id.content_layout;
            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.content_layout);
            if (linearLayout != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                i = R.id.et_first_answer;
                TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.et_first_answer);
                if (textInputLayout != null) {
                    i = R.id.et_second_answer;
                    TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.et_second_answer);
                    if (textInputLayout2 != null) {
                        i = R.id.img_logo;
                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                        if (appCompatImageView != null) {
                            i = R.id.toolbar;
                            View a = ai4.a(view, R.id.toolbar);
                            if (a != null) {
                                sp3 a2 = sp3.a(a);
                                i = R.id.txt_first_question_header;
                                TextView textView = (TextView) ai4.a(view, R.id.txt_first_question_header);
                                if (textView != null) {
                                    i = R.id.txt_second_question_header;
                                    TextView textView2 = (TextView) ai4.a(view, R.id.txt_second_question_header);
                                    if (textView2 != null) {
                                        return new o6(constraintLayout, materialButton, linearLayout, constraintLayout, textInputLayout, textInputLayout2, appCompatImageView, a2, textView, textView2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static o6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static o6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_answer_question, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
