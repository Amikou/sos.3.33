package defpackage;

import android.app.Application;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import net.safemoon.androidwallet.common.TokenType;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.o;
import zendesk.core.Constants;

/* compiled from: APIClient.java */
/* renamed from: a4  reason: default package */
/* loaded from: classes2.dex */
public class a4 {
    public static String a = "";
    public static String b = "";
    public static String c = "";
    public static String d = "";
    public static String e = "";
    public static String f = "";
    public static String g = "";
    public static Application h;
    public static o i;
    public static o j;
    public static o k;
    public static final HashMap<Integer, o> l = new HashMap<>();
    public static o m = null;
    public static o n = null;
    public static o o = null;
    public static o p = null;
    public static final HttpLoggingInterceptor.Level q = HttpLoggingInterceptor.Level.NONE;

    public static jt e() {
        File file;
        if (k == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            if (h != null) {
                file = new File(h.getCacheDir(), "http-cache");
            } else {
                file = new File("/data/user/0/net.safemoon.androidwallet/cache", "http-cache");
            }
            Cache cache = new Cache(file, 10485760);
            OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            k = new o.b().b("https://pro-api.coinmarketcap.com").a(yi1.f()).f(am2.c(addInterceptor.readTimeout(30L, timeUnit).connectTimeout(30L, timeUnit).addInterceptor(w3.a)).d(10L, TimeUnit.MINUTES).a().cache(cache).build()).d();
        }
        return (jt) k.b(jt.class);
    }

    public static Object f(int i2, String str) {
        return kt.h("https://s2.coinmarketcap.com/static/img/coins/64x64/" + i2 + ".png", str, n());
    }

    public static Object g(int i2, String str, TokenType tokenType) {
        return kt.h("https://s2.coinmarketcap.com/static/img/coins/64x64/" + i2 + ".png", str, tokenType);
    }

    public static p00 h() {
        if (n == null) {
            new HttpLoggingInterceptor().setLevel(q);
            OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(y3.a);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            n = new o.b().b("https://api.opensea.io/").a(yi1.f()).f(addInterceptor.readTimeout(30L, timeUnit).connectTimeout(30L, timeUnit).build()).d();
        }
        return (p00) n.b(p00.class);
    }

    public static rs0 i() {
        if (p == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            p = new o.b().b("https://anydummy.com").a(new t64()).a(yi1.f()).f(new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()).d();
        }
        return (rs0) p.b(rs0.class);
    }

    public static wq j(TokenType tokenType) {
        File file;
        o oVar;
        HashMap<Integer, o> hashMap = l;
        if (hashMap.containsKey(Integer.valueOf(tokenType.getChainId()))) {
            oVar = hashMap.get(Integer.valueOf(tokenType.getChainId()));
        } else {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            if (h != null) {
                file = new File(h.getCacheDir(), "http-cache");
            } else {
                file = new File("/data/user/0/net.safemoon.androidwallet/cache", "http-cache");
            }
            Cache cache = new Cache(file, 10485760);
            OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            o d2 = new o.b().b(tokenType.getScanApi()).a(yi1.f()).f(am2.c(addInterceptor.readTimeout(30L, timeUnit).connectTimeout(30L, timeUnit).addInterceptor(x3.a)).d(1L, TimeUnit.MINUTES).a().cache(cache).build()).d();
            hashMap.put(Integer.valueOf(tokenType.getChainId()), d2);
            oVar = d2;
        }
        return (wq) oVar.b(wq.class);
    }

    public static e42 k() {
        if (j == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            j = new o.b().b("https://marketdata.safemoon.net").a(yi1.f()).f(addInterceptor.readTimeout(30L, timeUnit).connectTimeout(30L, timeUnit).build()).d();
        }
        return (e42) j.b(e42.class);
    }

    public static r92 l() {
        if (o == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(z3.a).addInterceptor(httpLoggingInterceptor);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            o = new o.b().b("https://deep-index.moralis.io/").a(yi1.f()).f(addInterceptor.readTimeout(30L, timeUnit).connectTimeout(30L, timeUnit).build()).d();
        }
        return (r92) o.b(r92.class);
    }

    public static ac3 m() {
        if (i == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            i = new o.b().b("https://mainnet.safemoon.net/").a(yi1.f()).f(addInterceptor.readTimeout(30L, timeUnit).connectTimeout(30L, timeUnit).build()).d();
        }
        return (ac3) i.b(ac3.class);
    }

    public static TokenType n() {
        return TokenType.fromValue(bo3.j(h, "DEFAULT_GATEWAY", TokenType.BEP_20.getName()));
    }

    public static m84 o() {
        if (m == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(q);
            m = new o.b().b("https://symbol-search.tradingview.com").a(yi1.f()).f(new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()).d();
        }
        return (m84) m.b(m84.class);
    }

    public static void p(Application application) {
        h = application;
    }

    public static /* synthetic */ Response q(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder method = request.newBuilder().header(Constants.USER_AGENT_HEADER_KEY, "Android-Agent").method(request.method(), request.body());
        method.header("X-CMC_PRO_API_KEY", e);
        return chain.proceed(method.build());
    }

    public static /* synthetic */ Response r(Interceptor.Chain chain) throws IOException {
        Request.Builder newBuilder = chain.request().newBuilder();
        if (!zr.b.booleanValue()) {
            newBuilder.header("X-API-KEY", a);
        }
        return chain.proceed(newBuilder.build());
    }

    public static /* synthetic */ Response s(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        return chain.proceed(request.newBuilder().header(Constants.USER_AGENT_HEADER_KEY, "Android-Agent").method(request.method(), request.body()).build());
    }

    public static /* synthetic */ Response t(Interceptor.Chain chain) throws IOException {
        Request.Builder newBuilder = chain.request().newBuilder();
        if (!zr.b.booleanValue()) {
            newBuilder.header("X-API-KEY", b);
        }
        return chain.proceed(newBuilder.build());
    }
}
