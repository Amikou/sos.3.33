package defpackage;

import com.facebook.common.time.RealtimeSinceBootClock;

/* compiled from: BitmapMemoryCacheKey.java */
/* renamed from: eq  reason: default package */
/* loaded from: classes.dex */
public class eq implements wt {
    public final String a;
    public final p73 b;
    public final p93 c;
    public final rn1 d;
    public final wt e;
    public final String f;
    public final int g;
    public final Object h;

    public eq(String str, p73 p73Var, p93 p93Var, rn1 rn1Var, wt wtVar, String str2, Object obj) {
        this.a = (String) xt2.g(str);
        this.c = p93Var;
        this.d = rn1Var;
        this.e = wtVar;
        this.f = str2;
        this.g = ck1.d(Integer.valueOf(str.hashCode()), 0, Integer.valueOf(p93Var.hashCode()), rn1Var, wtVar, str2);
        this.h = obj;
        RealtimeSinceBootClock.get().now();
    }

    @Override // defpackage.wt
    public boolean a() {
        return false;
    }

    @Override // defpackage.wt
    public String b() {
        return this.a;
    }

    @Override // defpackage.wt
    public boolean equals(Object obj) {
        if (obj instanceof eq) {
            eq eqVar = (eq) obj;
            return this.g == eqVar.g && this.a.equals(eqVar.a) && ol2.a(this.b, eqVar.b) && ol2.a(this.c, eqVar.c) && ol2.a(this.d, eqVar.d) && ol2.a(this.e, eqVar.e) && ol2.a(this.f, eqVar.f);
        }
        return false;
    }

    @Override // defpackage.wt
    public int hashCode() {
        return this.g;
    }

    public String toString() {
        return String.format(null, "%s_%s_%s_%s_%s_%s_%d", this.a, this.b, this.c, this.d, this.e, this.f, Integer.valueOf(this.g));
    }
}
