package defpackage;

/* renamed from: hu4  reason: default package */
/* loaded from: classes2.dex */
public final class hu4 extends ki {
    public final int a;
    public final String b;
    public final String c;

    public hu4(int i, String str, String str2) {
        this.a = i;
        this.b = str;
        this.c = str2;
    }

    @Override // defpackage.ki
    public final String a() {
        return this.c;
    }

    @Override // defpackage.ki
    public final int c() {
        return this.a;
    }

    @Override // defpackage.ki
    public final String d() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        String str;
        if (obj == this) {
            return true;
        }
        if (obj instanceof ki) {
            ki kiVar = (ki) obj;
            if (this.a == kiVar.c() && ((str = this.b) != null ? str.equals(kiVar.d()) : kiVar.d() == null)) {
                String str2 = this.c;
                String a = kiVar.a();
                if (str2 != null ? str2.equals(a) : a == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = (this.a ^ 1000003) * 1000003;
        String str = this.b;
        int hashCode = (i ^ (str == null ? 0 : str.hashCode())) * 1000003;
        String str2 = this.c;
        return hashCode ^ (str2 != null ? str2.hashCode() : 0);
    }

    public final String toString() {
        int i = this.a;
        String str = this.b;
        String str2 = this.c;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 68 + String.valueOf(str2).length());
        sb.append("AssetPackLocation{packStorageMethod=");
        sb.append(i);
        sb.append(", path=");
        sb.append(str);
        sb.append(", assetsPath=");
        sb.append(str2);
        sb.append("}");
        return sb.toString();
    }
}
