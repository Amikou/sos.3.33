package defpackage;

import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.ProtoSyntax;
import com.google.protobuf.a0;
import com.google.protobuf.e0;
import com.google.protobuf.m;
import com.google.protobuf.p0;
import com.google.protobuf.q0;
import com.google.protobuf.y;
import com.google.protobuf.y0;
import com.google.protobuf.z0;
import java.util.Set;

/* compiled from: ManifestSchemaFactory.java */
/* renamed from: q32  reason: default package */
/* loaded from: classes2.dex */
public final class q32 implements md3 {
    public static final b82 b = new a();
    public final b82 a;

    /* compiled from: ManifestSchemaFactory.java */
    /* renamed from: q32$a */
    /* loaded from: classes2.dex */
    public static class a implements b82 {
        @Override // defpackage.b82
        public z72 a(Class<?> cls) {
            throw new IllegalStateException("This should never be called.");
        }

        @Override // defpackage.b82
        public boolean b(Class<?> cls) {
            return false;
        }
    }

    /* compiled from: ManifestSchemaFactory.java */
    /* renamed from: q32$b */
    /* loaded from: classes2.dex */
    public static class b implements b82 {
        public b82[] a;

        public b(b82... b82VarArr) {
            this.a = b82VarArr;
        }

        @Override // defpackage.b82
        public z72 a(Class<?> cls) {
            b82[] b82VarArr;
            for (b82 b82Var : this.a) {
                if (b82Var.b(cls)) {
                    return b82Var.a(cls);
                }
            }
            throw new UnsupportedOperationException("No factory is available for message type: " + cls.getName());
        }

        @Override // defpackage.b82
        public boolean b(Class<?> cls) {
            for (b82 b82Var : this.a) {
                if (b82Var.b(cls)) {
                    return true;
                }
            }
            return false;
        }
    }

    public q32() {
        this(b());
    }

    public static b82 b() {
        return new b(y.c(), c());
    }

    public static b82 c() {
        try {
            Set<String> set = m.a;
            return (b82) m.class.getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    public static boolean d(z72 z72Var) {
        return z72Var.c() == ProtoSyntax.PROTO2;
    }

    public static <T> y0<T> e(Class<T> cls, z72 z72Var) {
        if (GeneratedMessageLite.class.isAssignableFrom(cls)) {
            if (d(z72Var)) {
                return p0.R(cls, z72Var, kf2.b(), e0.b(), z0.O(), k11.b(), u32.b());
            }
            return p0.R(cls, z72Var, kf2.b(), e0.b(), z0.O(), null, u32.b());
        } else if (d(z72Var)) {
            return p0.R(cls, z72Var, kf2.a(), e0.a(), z0.I(), k11.a(), u32.a());
        } else {
            return p0.R(cls, z72Var, kf2.a(), e0.a(), z0.J(), null, u32.a());
        }
    }

    @Override // defpackage.md3
    public <T> y0<T> a(Class<T> cls) {
        z0.K(cls);
        z72 a2 = this.a.a(cls);
        if (a2.a()) {
            if (GeneratedMessageLite.class.isAssignableFrom(cls)) {
                return q0.m(z0.O(), k11.b(), a2.b());
            }
            return q0.m(z0.I(), k11.a(), a2.b());
        }
        return e(cls, a2);
    }

    public q32(b82 b82Var) {
        this.a = (b82) a0.b(b82Var, "messageInfoFactory");
    }
}
