package defpackage;

import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.ext.a;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import java.util.logging.Logger;

/* compiled from: Java7Support.java */
/* renamed from: qt1  reason: default package */
/* loaded from: classes.dex */
public abstract class qt1 {
    public static final qt1 a;

    static {
        qt1 qt1Var;
        try {
            qt1Var = (qt1) a.class.newInstance();
        } catch (Throwable unused) {
            Logger.getLogger(qt1.class.getName()).warning("Unable to load JDK7 types (annotations, java.nio.file.Path): no Java7 support added");
            qt1Var = null;
        }
        a = qt1Var;
    }

    public static qt1 f() {
        return a;
    }

    public abstract PropertyName a(AnnotatedParameter annotatedParameter);

    public abstract Boolean b(ue ueVar);

    public abstract c<?> c(Class<?> cls);

    public abstract f<?> d(Class<?> cls);

    public abstract Boolean e(ue ueVar);
}
