package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.navigation.NavController;
import androidx.navigation.d;
import androidx.navigation.e;
import java.util.ArrayDeque;
import java.util.Iterator;

/* compiled from: NavDeepLinkBuilder.java */
/* renamed from: ae2  reason: default package */
/* loaded from: classes.dex */
public final class ae2 {
    public final Context a;
    public final Intent b;
    public e c;
    public int d;

    public ae2(Context context) {
        this.a = context;
        if (context instanceof Activity) {
            this.b = new Intent(context, context.getClass());
        } else {
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            this.b = launchIntentForPackage == null ? new Intent() : launchIntentForPackage;
        }
        this.b.addFlags(268468224);
    }

    public v34 a() {
        if (this.b.getIntArrayExtra("android-support-nav:controller:deepLinkIds") == null) {
            if (this.c == null) {
                throw new IllegalStateException("You must call setGraph() before constructing the deep link");
            }
            throw new IllegalStateException("You must call setDestination() before constructing the deep link");
        }
        v34 i = v34.n(this.a).i(new Intent(this.b));
        for (int i2 = 0; i2 < i.p(); i2++) {
            i.o(i2).putExtra("android-support-nav:controller:deepLinkIntent", this.b);
        }
        return i;
    }

    public final void b() {
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.add(this.c);
        d dVar = null;
        while (!arrayDeque.isEmpty() && dVar == null) {
            d dVar2 = (d) arrayDeque.poll();
            if (dVar2.s() == this.d) {
                dVar = dVar2;
            } else if (dVar2 instanceof e) {
                Iterator<d> it = ((e) dVar2).iterator();
                while (it.hasNext()) {
                    arrayDeque.add(it.next());
                }
            }
        }
        if (dVar != null) {
            this.b.putExtra("android-support-nav:controller:deepLinkIds", dVar.m());
            return;
        }
        String q = d.q(this.a, this.d);
        throw new IllegalArgumentException("Navigation destination " + q + " cannot be found in the navigation graph " + this.c);
    }

    public ae2 c(Bundle bundle) {
        this.b.putExtra("android-support-nav:controller:deepLinkExtras", bundle);
        return this;
    }

    public ae2 d(int i) {
        this.d = i;
        if (this.c != null) {
            b();
        }
        return this;
    }

    public ae2(NavController navController) {
        this(navController.g());
        this.c = navController.k();
    }
}
