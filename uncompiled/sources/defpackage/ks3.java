package defpackage;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.widget.ActionBarContextView;
import defpackage.k6;
import java.lang.ref.WeakReference;

/* compiled from: StandaloneActionMode.java */
/* renamed from: ks3  reason: default package */
/* loaded from: classes.dex */
public class ks3 extends k6 implements e.a {
    public Context g0;
    public ActionBarContextView h0;
    public k6.a i0;
    public WeakReference<View> j0;
    public boolean k0;
    public e l0;

    public ks3(Context context, ActionBarContextView actionBarContextView, k6.a aVar, boolean z) {
        this.g0 = context;
        this.h0 = actionBarContextView;
        this.i0 = aVar;
        e W = new e(actionBarContextView.getContext()).W(1);
        this.l0 = W;
        W.V(this);
    }

    @Override // androidx.appcompat.view.menu.e.a
    public boolean a(e eVar, MenuItem menuItem) {
        return this.i0.a(this, menuItem);
    }

    @Override // androidx.appcompat.view.menu.e.a
    public void b(e eVar) {
        k();
        this.h0.l();
    }

    @Override // defpackage.k6
    public void c() {
        if (this.k0) {
            return;
        }
        this.k0 = true;
        this.h0.sendAccessibilityEvent(32);
        this.i0.b(this);
    }

    @Override // defpackage.k6
    public View d() {
        WeakReference<View> weakReference = this.j0;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    @Override // defpackage.k6
    public Menu e() {
        return this.l0;
    }

    @Override // defpackage.k6
    public MenuInflater f() {
        return new kw3(this.h0.getContext());
    }

    @Override // defpackage.k6
    public CharSequence g() {
        return this.h0.getSubtitle();
    }

    @Override // defpackage.k6
    public CharSequence i() {
        return this.h0.getTitle();
    }

    @Override // defpackage.k6
    public void k() {
        this.i0.d(this, this.l0);
    }

    @Override // defpackage.k6
    public boolean l() {
        return this.h0.j();
    }

    @Override // defpackage.k6
    public void m(View view) {
        this.h0.setCustomView(view);
        this.j0 = view != null ? new WeakReference<>(view) : null;
    }

    @Override // defpackage.k6
    public void n(int i) {
        o(this.g0.getString(i));
    }

    @Override // defpackage.k6
    public void o(CharSequence charSequence) {
        this.h0.setSubtitle(charSequence);
    }

    @Override // defpackage.k6
    public void q(int i) {
        r(this.g0.getString(i));
    }

    @Override // defpackage.k6
    public void r(CharSequence charSequence) {
        this.h0.setTitle(charSequence);
    }

    @Override // defpackage.k6
    public void s(boolean z) {
        super.s(z);
        this.h0.setTitleOptional(z);
    }
}
