package defpackage;

import com.google.android.gms.signin.internal.zam;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: zz4  reason: default package */
/* loaded from: classes.dex */
public final class zz4 extends q05 {
    public final /* synthetic */ rz4 b;
    public final /* synthetic */ zam c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public zz4(a05 a05Var, j05 j05Var, rz4 rz4Var, zam zamVar) {
        super(j05Var);
        this.b = rz4Var;
        this.c = zamVar;
    }

    @Override // defpackage.q05
    public final void a() {
        this.b.i(this.c);
    }
}
