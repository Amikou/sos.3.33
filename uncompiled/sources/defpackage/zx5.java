package defpackage;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.f2;
import com.google.android.gms.internal.measurement.x1;
import com.google.android.gms.internal.measurement.z1;
import com.google.android.gms.internal.measurement.zzjd;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: zx5  reason: default package */
/* loaded from: classes.dex */
public final class zx5 {
    public static String a(z1 z1Var, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(str);
        c(z1Var, sb, 0);
        return sb.toString();
    }

    public static final void b(StringBuilder sb, int i, String str, Object obj) {
        if (obj instanceof List) {
            for (Object obj2 : (List) obj) {
                b(sb, i, str, obj2);
            }
        } else if (obj instanceof Map) {
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                b(sb, i, str, entry);
            }
        } else {
            sb.append('\n');
            int i2 = 0;
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"");
                sb.append(jz5.a(zzjd.zzk((String) obj)));
                sb.append('\"');
            } else if (obj instanceof zzjd) {
                sb.append(": \"");
                sb.append(jz5.a((zzjd) obj));
                sb.append('\"');
            } else if (obj instanceof x1) {
                sb.append(" {");
                c((x1) obj, sb, i + 2);
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else if (obj instanceof Map.Entry) {
                sb.append(" {");
                Map.Entry entry2 = (Map.Entry) obj;
                int i4 = i + 2;
                b(sb, i4, "key", entry2.getKey());
                b(sb, i4, "value", entry2.getValue());
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else {
                sb.append(": ");
                sb.append(obj.toString());
            }
        }
    }

    public static void c(z1 z1Var, StringBuilder sb, int i) {
        Method[] declaredMethods;
        boolean equals;
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        TreeSet<String> treeSet = new TreeSet();
        for (Method method : z1Var.getClass().getDeclaredMethods()) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    treeSet.add(method.getName());
                }
            }
        }
        for (String str : treeSet) {
            String substring = str.startsWith("get") ? str.substring(3) : str;
            if (substring.endsWith("List") && !substring.endsWith("OrBuilderList") && !substring.equals("List")) {
                String valueOf = String.valueOf(substring.substring(0, 1).toLowerCase());
                String valueOf2 = String.valueOf(substring.substring(1, substring.length() - 4));
                String concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                Method method2 = (Method) hashMap.get(str);
                if (method2 != null && method2.getReturnType().equals(List.class)) {
                    b(sb, i, d(concat), x1.k(method2, z1Var, new Object[0]));
                }
            }
            if (substring.endsWith("Map") && !substring.equals("Map")) {
                String valueOf3 = String.valueOf(substring.substring(0, 1).toLowerCase());
                String valueOf4 = String.valueOf(substring.substring(1, substring.length() - 3));
                String concat2 = valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3);
                Method method3 = (Method) hashMap.get(str);
                if (method3 != null && method3.getReturnType().equals(Map.class) && !method3.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method3.getModifiers())) {
                    b(sb, i, d(concat2), x1.k(method3, z1Var, new Object[0]));
                }
            }
            if (((Method) hashMap2.get(substring.length() != 0 ? "set".concat(substring) : new String("set"))) != null) {
                if (substring.endsWith("Bytes")) {
                    String valueOf5 = String.valueOf(substring.substring(0, substring.length() - 5));
                    if (!hashMap.containsKey(valueOf5.length() != 0 ? "get".concat(valueOf5) : new String("get"))) {
                    }
                }
                String valueOf6 = String.valueOf(substring.substring(0, 1).toLowerCase());
                String valueOf7 = String.valueOf(substring.substring(1));
                String concat3 = valueOf7.length() != 0 ? valueOf6.concat(valueOf7) : new String(valueOf6);
                Method method4 = (Method) hashMap.get(substring.length() != 0 ? "get".concat(substring) : new String("get"));
                Method method5 = (Method) hashMap.get(substring.length() != 0 ? "has".concat(substring) : new String("has"));
                if (method4 != null) {
                    Object k = x1.k(method4, z1Var, new Object[0]);
                    if (method5 == null) {
                        if (k instanceof Boolean) {
                            if (((Boolean) k).booleanValue()) {
                                b(sb, i, d(concat3), k);
                            }
                        } else if (k instanceof Integer) {
                            if (((Integer) k).intValue() != 0) {
                                b(sb, i, d(concat3), k);
                            }
                        } else if (k instanceof Float) {
                            if (((Float) k).floatValue() != Utils.FLOAT_EPSILON) {
                                b(sb, i, d(concat3), k);
                            }
                        } else if (k instanceof Double) {
                            if (((Double) k).doubleValue() != Utils.DOUBLE_EPSILON) {
                                b(sb, i, d(concat3), k);
                            }
                        } else {
                            if (k instanceof String) {
                                equals = k.equals("");
                            } else if (k instanceof zzjd) {
                                equals = k.equals(zzjd.zzb);
                            } else if (k instanceof z1) {
                                if (k != ((z1) k).g()) {
                                    b(sb, i, d(concat3), k);
                                }
                            } else {
                                if ((k instanceof Enum) && ((Enum) k).ordinal() == 0) {
                                }
                                b(sb, i, d(concat3), k);
                            }
                            if (!equals) {
                                b(sb, i, d(concat3), k);
                            }
                        }
                    } else if (((Boolean) x1.k(method5, z1Var, new Object[0])).booleanValue()) {
                        b(sb, i, d(concat3), k);
                    }
                }
            }
        }
        if (!(z1Var instanceof cv5)) {
            f2 f2Var = ((x1) z1Var).zzc;
            if (f2Var != null) {
                f2Var.g(sb, i);
                return;
            }
            return;
        }
        cv5 cv5Var = (cv5) z1Var;
        throw null;
    }

    public static final String d(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }
}
