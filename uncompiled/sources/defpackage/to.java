package defpackage;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.d;
import com.fasterxml.jackson.databind.c;
import com.fasterxml.jackson.databind.deser.AbstractDeserializer;
import com.fasterxml.jackson.databind.deser.BeanDeserializer;
import com.fasterxml.jackson.databind.deser.BuilderBasedDeserializer;
import com.fasterxml.jackson.databind.deser.SettableAnyProperty;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.i;
import com.fasterxml.jackson.databind.deser.impl.BeanPropertyMap;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdValueProperty;
import com.fasterxml.jackson.databind.deser.impl.g;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: BeanDeserializerBuilder.java */
/* renamed from: to  reason: default package */
/* loaded from: classes.dex */
public class to {
    public final DeserializationConfig a;
    public final so b;
    public final Map<String, SettableBeanProperty> c = new LinkedHashMap();
    public List<g> d;
    public HashMap<String, SettableBeanProperty> e;
    public HashSet<String> f;
    public i g;
    public ObjectIdReader h;
    public SettableAnyProperty i;
    public boolean j;
    public AnnotatedMethod k;

    public to(so soVar, DeserializationConfig deserializationConfig) {
        this.b = soVar;
        this.a = deserializationConfig;
    }

    public final void a(Collection<SettableBeanProperty> collection) {
        for (SettableBeanProperty settableBeanProperty : collection) {
            settableBeanProperty.fixAccess(this.a);
        }
        SettableAnyProperty settableAnyProperty = this.i;
        if (settableAnyProperty != null) {
            settableAnyProperty.fixAccess(this.a);
        }
        AnnotatedMethod annotatedMethod = this.k;
        if (annotatedMethod != null) {
            annotatedMethod.fixAccess(this.a.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS));
        }
    }

    public void b(String str, SettableBeanProperty settableBeanProperty) {
        if (this.e == null) {
            this.e = new HashMap<>(4);
        }
        settableBeanProperty.fixAccess(this.a);
        this.e.put(str, settableBeanProperty);
        Map<String, SettableBeanProperty> map = this.c;
        if (map != null) {
            map.remove(settableBeanProperty.getName());
        }
    }

    public void c(SettableBeanProperty settableBeanProperty) {
        g(settableBeanProperty);
    }

    public void d(String str) {
        if (this.f == null) {
            this.f = new HashSet<>();
        }
        this.f.add(str);
    }

    public void e(PropertyName propertyName, JavaType javaType, xe xeVar, AnnotatedMember annotatedMember, Object obj) {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        boolean canOverrideAccessModifiers = this.a.canOverrideAccessModifiers();
        boolean z = canOverrideAccessModifiers && this.a.isEnabled(MapperFeature.OVERRIDE_PUBLIC_ACCESS_MODIFIERS);
        if (canOverrideAccessModifiers) {
            annotatedMember.fixAccess(z);
        }
        this.d.add(new g(propertyName, javaType, xeVar, annotatedMember, obj));
    }

    public void f(SettableBeanProperty settableBeanProperty, boolean z) {
        this.c.put(settableBeanProperty.getName(), settableBeanProperty);
    }

    public void g(SettableBeanProperty settableBeanProperty) {
        SettableBeanProperty put = this.c.put(settableBeanProperty.getName(), settableBeanProperty);
        if (put == null || put == settableBeanProperty) {
            return;
        }
        throw new IllegalArgumentException("Duplicate property '" + settableBeanProperty.getName() + "' for " + this.b.y());
    }

    public c<?> h() {
        boolean z;
        Collection<SettableBeanProperty> values = this.c.values();
        a(values);
        BeanPropertyMap construct = BeanPropertyMap.construct(values, this.a.isEnabled(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES));
        construct.assignIndexes();
        boolean z2 = !this.a.isEnabled(MapperFeature.DEFAULT_VIEW_INCLUSION);
        if (!z2) {
            for (SettableBeanProperty settableBeanProperty : values) {
                if (settableBeanProperty.hasViews()) {
                    z = true;
                    break;
                }
            }
        }
        z = z2;
        if (this.h != null) {
            construct = construct.withProperty(new ObjectIdValueProperty(this.h, PropertyMetadata.STD_REQUIRED));
        }
        return new BeanDeserializer(this, this.b, construct, this.e, this.f, this.j, z);
    }

    public AbstractDeserializer i() {
        return new AbstractDeserializer(this, this.b, this.e);
    }

    public c<?> j(JavaType javaType, String str) {
        boolean z;
        AnnotatedMethod annotatedMethod = this.k;
        if (annotatedMethod == null) {
            if (!str.isEmpty()) {
                throw new IllegalArgumentException(String.format("Builder class %s does not have build method (name: '%s')", this.b.r().getName(), str));
            }
        } else {
            Class<?> rawReturnType = annotatedMethod.getRawReturnType();
            Class<?> rawClass = javaType.getRawClass();
            if (rawReturnType != rawClass && !rawReturnType.isAssignableFrom(rawClass) && !rawClass.isAssignableFrom(rawReturnType)) {
                throw new IllegalArgumentException("Build method '" + this.k.getFullName() + " has bad return type (" + rawReturnType.getName() + "), not compatible with POJO type (" + javaType.getRawClass().getName() + ")");
            }
        }
        Collection<SettableBeanProperty> values = this.c.values();
        a(values);
        BeanPropertyMap construct = BeanPropertyMap.construct(values, this.a.isEnabled(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES));
        construct.assignIndexes();
        boolean z2 = !this.a.isEnabled(MapperFeature.DEFAULT_VIEW_INCLUSION);
        if (!z2) {
            for (SettableBeanProperty settableBeanProperty : values) {
                if (settableBeanProperty.hasViews()) {
                    z = true;
                    break;
                }
            }
        }
        z = z2;
        if (this.h != null) {
            construct = construct.withProperty(new ObjectIdValueProperty(this.h, PropertyMetadata.STD_REQUIRED));
        }
        return new BuilderBasedDeserializer(this, this.b, construct, this.e, this.f, this.j, z);
    }

    public SettableBeanProperty k(PropertyName propertyName) {
        return this.c.get(propertyName.getSimpleName());
    }

    public SettableAnyProperty l() {
        return this.i;
    }

    public AnnotatedMethod m() {
        return this.k;
    }

    public List<g> n() {
        return this.d;
    }

    public ObjectIdReader o() {
        return this.h;
    }

    public i p() {
        return this.g;
    }

    public void q(SettableAnyProperty settableAnyProperty) {
        if (this.i != null && settableAnyProperty != null) {
            throw new IllegalStateException("_anySetter already set to non-null");
        }
        this.i = settableAnyProperty;
    }

    public void r(boolean z) {
        this.j = z;
    }

    public void s(ObjectIdReader objectIdReader) {
        this.h = objectIdReader;
    }

    public void t(AnnotatedMethod annotatedMethod, d.a aVar) {
        this.k = annotatedMethod;
    }

    public void u(i iVar) {
        this.g = iVar;
    }
}
