package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: me3  reason: default package */
/* loaded from: classes2.dex */
public class me3 extends ct0.b {
    public static final BigInteger g = ke3.j;
    public int[] f;

    public me3() {
        this.f = bd2.d();
    }

    public me3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP160R2FieldElement");
        }
        this.f = le3.c(bigInteger);
    }

    public me3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] d = bd2.d();
        le3.a(this.f, ((me3) ct0Var).f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] d = bd2.d();
        le3.b(this.f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] d = bd2.d();
        g92.d(le3.a, ((me3) ct0Var).f, d);
        le3.d(d, this.f, d);
        return new me3(d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof me3) {
            return bd2.f(this.f, ((me3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] d = bd2.d();
        g92.d(le3.a, this.f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return bd2.j(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 5);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return bd2.k(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] d = bd2.d();
        le3.d(this.f, ((me3) ct0Var).f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] d = bd2.d();
        le3.f(this.f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (bd2.k(iArr) || bd2.j(iArr)) {
            return this;
        }
        int[] d = bd2.d();
        le3.i(iArr, d);
        le3.d(d, iArr, d);
        int[] d2 = bd2.d();
        le3.i(d, d2);
        le3.d(d2, iArr, d2);
        int[] d3 = bd2.d();
        le3.i(d2, d3);
        le3.d(d3, iArr, d3);
        int[] d4 = bd2.d();
        le3.j(d3, 3, d4);
        le3.d(d4, d2, d4);
        le3.j(d4, 7, d3);
        le3.d(d3, d4, d3);
        le3.j(d3, 3, d4);
        le3.d(d4, d2, d4);
        int[] d5 = bd2.d();
        le3.j(d4, 14, d5);
        le3.d(d5, d3, d5);
        le3.j(d5, 31, d3);
        le3.d(d3, d5, d3);
        le3.j(d3, 62, d5);
        le3.d(d5, d3, d5);
        le3.j(d5, 3, d3);
        le3.d(d3, d2, d3);
        le3.j(d3, 18, d3);
        le3.d(d3, d4, d3);
        le3.j(d3, 2, d3);
        le3.d(d3, iArr, d3);
        le3.j(d3, 3, d3);
        le3.d(d3, d, d3);
        le3.j(d3, 6, d3);
        le3.d(d3, d2, d3);
        le3.j(d3, 2, d3);
        le3.d(d3, iArr, d3);
        le3.i(d3, d);
        if (bd2.f(iArr, d)) {
            return new me3(d3);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] d = bd2.d();
        le3.i(this.f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] d = bd2.d();
        le3.k(this.f, ((me3) ct0Var).f, d);
        return new me3(d);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return bd2.h(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return bd2.u(this.f);
    }
}
