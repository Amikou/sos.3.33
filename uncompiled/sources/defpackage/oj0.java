package defpackage;

import java.math.BigInteger;

/* compiled from: DefaultGasProvider.java */
/* renamed from: oj0  reason: default package */
/* loaded from: classes3.dex */
public class oj0 extends it3 {
    public static final BigInteger GAS_LIMIT = BigInteger.valueOf(9000000);
    public static final BigInteger GAS_PRICE = BigInteger.valueOf(4100000000L);

    public oj0() {
        super(GAS_PRICE, GAS_LIMIT);
    }
}
