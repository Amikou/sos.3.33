package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: ThreadContextElement.kt */
/* renamed from: g54  reason: default package */
/* loaded from: classes2.dex */
public interface g54<S> extends CoroutineContext.a {
    S C(CoroutineContext coroutineContext);

    void q(CoroutineContext coroutineContext, S s);
}
