package defpackage;

import java.math.BigInteger;
import java.security.SecureRandom;

/* renamed from: zs0  reason: default package */
/* loaded from: classes2.dex */
public class zs0 implements ws0 {
    public final od0 f;
    public ft0 g;
    public SecureRandom h;

    public zs0(od0 od0Var) {
        this.f = od0Var;
    }

    public BigInteger a(BigInteger bigInteger, byte[] bArr) {
        int bitLength = bigInteger.bitLength();
        int length = bArr.length * 8;
        BigInteger bigInteger2 = new BigInteger(1, bArr);
        return bitLength < length ? bigInteger2.shiftRight(length - bitLength) : bigInteger2;
    }

    public it0 b() {
        return new f61();
    }

    public BigInteger[] c(byte[] bArr) {
        at0 a = this.g.a();
        BigInteger d = a.d();
        BigInteger a2 = a(d, bArr);
        BigInteger b = ((st0) this.g).b();
        if (this.f.b()) {
            this.f.d(d, b, bArr);
        } else {
            this.f.c(d, this.h);
        }
        it0 b2 = b();
        while (true) {
            BigInteger a3 = this.f.a();
            BigInteger mod = b2.a(a.b(), a3).A().f().t().mod(d);
            BigInteger bigInteger = ws0.a;
            if (!mod.equals(bigInteger)) {
                BigInteger mod2 = a3.modInverse(d).multiply(a2.add(b.multiply(mod))).mod(d);
                if (!mod2.equals(bigInteger)) {
                    return new BigInteger[]{mod, mod2};
                }
            }
        }
    }

    public void d(boolean z, ty tyVar) {
        ft0 ft0Var;
        SecureRandom secureRandom;
        if (!z) {
            ft0Var = (ut0) tyVar;
        } else if (tyVar instanceof kp2) {
            kp2 kp2Var = (kp2) tyVar;
            this.g = (st0) kp2Var.a();
            secureRandom = kp2Var.b();
            this.h = e((z || this.f.b()) ? false : true, secureRandom);
        } else {
            ft0Var = (st0) tyVar;
        }
        this.g = ft0Var;
        secureRandom = null;
        this.h = e((z || this.f.b()) ? false : true, secureRandom);
    }

    public SecureRandom e(boolean z, SecureRandom secureRandom) {
        if (z) {
            return secureRandom != null ? secureRandom : ib0.b();
        }
        return null;
    }
}
