package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import defpackage.tf1;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;

/* compiled from: ByteBufferGifDecoder.java */
/* renamed from: rs  reason: default package */
/* loaded from: classes.dex */
public class rs implements com.bumptech.glide.load.b<ByteBuffer, uf1> {
    public static final a f = new a();
    public static final b g = new b();
    public final Context a;
    public final List<ImageHeaderParser> b;
    public final b c;
    public final a d;
    public final sf1 e;

    /* compiled from: ByteBufferGifDecoder.java */
    /* renamed from: rs$a */
    /* loaded from: classes.dex */
    public static class a {
        public tf1 a(tf1.a aVar, cg1 cg1Var, ByteBuffer byteBuffer, int i) {
            return new ns3(aVar, cg1Var, byteBuffer, i);
        }
    }

    /* compiled from: ByteBufferGifDecoder.java */
    /* renamed from: rs$b */
    /* loaded from: classes.dex */
    public static class b {
        public final Queue<dg1> a = mg4.f(0);

        public synchronized dg1 a(ByteBuffer byteBuffer) {
            dg1 poll;
            poll = this.a.poll();
            if (poll == null) {
                poll = new dg1();
            }
            return poll.p(byteBuffer);
        }

        public synchronized void b(dg1 dg1Var) {
            dg1Var.a();
            this.a.offer(dg1Var);
        }
    }

    public rs(Context context, List<ImageHeaderParser> list, jq jqVar, sh shVar) {
        this(context, list, jqVar, shVar, g, f);
    }

    public static int e(cg1 cg1Var, int i, int i2) {
        int min = Math.min(cg1Var.a() / i2, cg1Var.d() / i);
        int max = Math.max(1, min == 0 ? 0 : Integer.highestOneBit(min));
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Downsampling GIF, sampleSize: ");
            sb.append(max);
            sb.append(", target dimens: [");
            sb.append(i);
            sb.append("x");
            sb.append(i2);
            sb.append("], actual dimens: [");
            sb.append(cg1Var.d());
            sb.append("x");
            sb.append(cg1Var.a());
            sb.append("]");
        }
        return max;
    }

    public final xf1 c(ByteBuffer byteBuffer, int i, int i2, dg1 dg1Var, vn2 vn2Var) {
        Bitmap.Config config;
        long b2 = t12.b();
        try {
            cg1 c = dg1Var.c();
            if (c.b() > 0 && c.c() == 0) {
                if (vn2Var.c(eg1.a) == DecodeFormat.PREFER_RGB_565) {
                    config = Bitmap.Config.RGB_565;
                } else {
                    config = Bitmap.Config.ARGB_8888;
                }
                tf1 a2 = this.d.a(this.e, c, byteBuffer, e(c, i, i2));
                a2.e(config);
                a2.c();
                Bitmap b3 = a2.b();
                if (b3 == null) {
                    return null;
                }
                xf1 xf1Var = new xf1(new uf1(this.a, a2, xe4.c(), i, i2, b3));
                if (Log.isLoggable("BufferGifDecoder", 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Decoded GIF from stream in ");
                    sb.append(t12.a(b2));
                }
                return xf1Var;
            }
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Decoded GIF from stream in ");
                sb2.append(t12.a(b2));
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Decoded GIF from stream in ");
                sb3.append(t12.a(b2));
            }
        }
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: d */
    public xf1 b(ByteBuffer byteBuffer, int i, int i2, vn2 vn2Var) {
        dg1 a2 = this.c.a(byteBuffer);
        try {
            return c(byteBuffer, i, i2, a2, vn2Var);
        } finally {
            this.c.b(a2);
        }
    }

    @Override // com.bumptech.glide.load.b
    /* renamed from: f */
    public boolean a(ByteBuffer byteBuffer, vn2 vn2Var) throws IOException {
        return !((Boolean) vn2Var.c(eg1.b)).booleanValue() && com.bumptech.glide.load.a.g(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    public rs(Context context, List<ImageHeaderParser> list, jq jqVar, sh shVar, b bVar, a aVar) {
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.e = new sf1(jqVar, shVar);
        this.c = bVar;
    }
}
