package defpackage;

import com.facebook.cache.common.CacheEventListener;
import com.facebook.cache.common.a;

/* compiled from: NoOpCacheEventListener.java */
/* renamed from: lg2  reason: default package */
/* loaded from: classes.dex */
public class lg2 implements CacheEventListener {
    public static lg2 a;

    public static synchronized lg2 h() {
        lg2 lg2Var;
        synchronized (lg2.class) {
            if (a == null) {
                a = new lg2();
            }
            lg2Var = a;
        }
        return lg2Var;
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void a(a aVar) {
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void b(a aVar) {
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void c(a aVar) {
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void d(a aVar) {
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void e(a aVar) {
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void f(a aVar) {
    }

    @Override // com.facebook.cache.common.CacheEventListener
    public void g(a aVar) {
    }
}
