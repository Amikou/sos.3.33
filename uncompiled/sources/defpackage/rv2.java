package defpackage;

import com.facebook.common.internal.b;
import com.facebook.common.internal.d;
import com.facebook.common.util.a;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.internal.http2.Http2;

/* compiled from: ProgressiveJpegParser.java */
/* renamed from: rv2  reason: default package */
/* loaded from: classes.dex */
public class rv2 {
    public boolean g;
    public final os h;
    public int c = 0;
    public int b = 0;
    public int d = 0;
    public int f = 0;
    public int e = 0;
    public int a = 0;

    public rv2(os osVar) {
        this.h = (os) xt2.g(osVar);
    }

    public static boolean b(int i) {
        if (i == 1) {
            return false;
        }
        if (i < 208 || i > 215) {
            return (i == 217 || i == 216) ? false : true;
        }
        return false;
    }

    public final boolean a(InputStream inputStream) {
        int read;
        int i = this.e;
        while (this.a != 6 && (read = inputStream.read()) != -1) {
            try {
                int i2 = this.c + 1;
                this.c = i2;
                if (this.g) {
                    this.a = 6;
                    this.g = false;
                    return false;
                }
                int i3 = this.a;
                if (i3 != 0) {
                    if (i3 != 1) {
                        if (i3 != 2) {
                            if (i3 != 3) {
                                if (i3 == 4) {
                                    this.a = 5;
                                } else if (i3 != 5) {
                                    xt2.i(false);
                                } else {
                                    int i4 = ((this.b << 8) + read) - 2;
                                    a.a(inputStream, i4);
                                    this.c += i4;
                                    this.a = 2;
                                }
                            } else if (read == 255) {
                                this.a = 3;
                            } else if (read == 0) {
                                this.a = 2;
                            } else if (read == 217) {
                                this.g = true;
                                f(i2 - 2);
                                this.a = 2;
                            } else {
                                if (read == 218) {
                                    f(i2 - 2);
                                }
                                if (b(read)) {
                                    this.a = 4;
                                } else {
                                    this.a = 2;
                                }
                            }
                        } else if (read == 255) {
                            this.a = 3;
                        }
                    } else if (read == 216) {
                        this.a = 2;
                    } else {
                        this.a = 6;
                    }
                } else if (read == 255) {
                    this.a = 1;
                } else {
                    this.a = 6;
                }
                this.b = read;
            } catch (IOException e) {
                d.a(e);
            }
        }
        return (this.a == 6 || this.e == i) ? false : true;
    }

    public int c() {
        return this.f;
    }

    public int d() {
        return this.e;
    }

    public boolean e() {
        return this.g;
    }

    public final void f(int i) {
        int i2 = this.d;
        if (i2 > 0) {
            this.f = i;
        }
        this.d = i2 + 1;
        this.e = i2;
    }

    public boolean g(zu0 zu0Var) {
        if (this.a != 6 && zu0Var.u() > this.c) {
            com.facebook.common.memory.a aVar = new com.facebook.common.memory.a(zu0Var.n(), this.h.get(Http2.INITIAL_MAX_FRAME_SIZE), this.h);
            try {
                a.a(aVar, this.c);
                return a(aVar);
            } catch (IOException e) {
                d.a(e);
                return false;
            } finally {
                b.b(aVar);
            }
        }
        return false;
    }
}
