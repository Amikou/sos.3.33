package defpackage;

/* renamed from: by2  reason: default package */
/* loaded from: classes.dex */
public final class by2 {
    public static final int cardBackgroundColor = 2130968729;
    public static final int cardCornerRadius = 2130968730;
    public static final int cardElevation = 2130968731;
    public static final int cardMaxElevation = 2130968733;
    public static final int cardPreventCornerOverlap = 2130968734;
    public static final int cardUseCompatPadding = 2130968735;
    public static final int cardViewStyle = 2130968736;
    public static final int contentPadding = 2130968848;
    public static final int contentPaddingBottom = 2130968849;
    public static final int contentPaddingLeft = 2130968851;
    public static final int contentPaddingRight = 2130968852;
    public static final int contentPaddingTop = 2130968854;
}
