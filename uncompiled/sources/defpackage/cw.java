package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.github.mikephil.charting.utils.Utils;
import defpackage.r93;

/* compiled from: CardViewBaseImpl.java */
/* renamed from: cw  reason: default package */
/* loaded from: classes.dex */
public class cw implements ew {
    public final RectF a = new RectF();

    /* compiled from: CardViewBaseImpl.java */
    /* renamed from: cw$a */
    /* loaded from: classes.dex */
    public class a implements r93.a {
        public a() {
        }

        @Override // defpackage.r93.a
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            float f2 = 2.0f * f;
            float width = (rectF.width() - f2) - 1.0f;
            float height = (rectF.height() - f2) - 1.0f;
            if (f >= 1.0f) {
                float f3 = f + 0.5f;
                float f4 = -f3;
                cw.this.a.set(f4, f4, f3, f3);
                int save = canvas.save();
                canvas.translate(rectF.left + f3, rectF.top + f3);
                canvas.drawArc(cw.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(width, Utils.FLOAT_EPSILON);
                canvas.rotate(90.0f);
                canvas.drawArc(cw.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(height, Utils.FLOAT_EPSILON);
                canvas.rotate(90.0f);
                canvas.drawArc(cw.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(width, Utils.FLOAT_EPSILON);
                canvas.rotate(90.0f);
                canvas.drawArc(cw.this.a, 180.0f, 90.0f, true, paint);
                canvas.restoreToCount(save);
                float f5 = rectF.top;
                canvas.drawRect((rectF.left + f3) - 1.0f, f5, (rectF.right - f3) + 1.0f, f5 + f3, paint);
                float f6 = rectF.bottom;
                canvas.drawRect((rectF.left + f3) - 1.0f, f6 - f3, (rectF.right - f3) + 1.0f, f6, paint);
            }
            canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom - f, paint);
        }
    }

    @Override // defpackage.ew
    public void a(dw dwVar, float f) {
        q(dwVar).p(f);
        d(dwVar);
    }

    @Override // defpackage.ew
    public float b(dw dwVar) {
        return q(dwVar).k();
    }

    @Override // defpackage.ew
    public void c(dw dwVar) {
    }

    @Override // defpackage.ew
    public void d(dw dwVar) {
        Rect rect = new Rect();
        q(dwVar).h(rect);
        dwVar.b((int) Math.ceil(b(dwVar)), (int) Math.ceil(g(dwVar)));
        dwVar.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @Override // defpackage.ew
    public float e(dw dwVar) {
        return q(dwVar).g();
    }

    @Override // defpackage.ew
    public ColorStateList f(dw dwVar) {
        return q(dwVar).f();
    }

    @Override // defpackage.ew
    public float g(dw dwVar) {
        return q(dwVar).j();
    }

    @Override // defpackage.ew
    public void h(dw dwVar, float f) {
        q(dwVar).r(f);
    }

    @Override // defpackage.ew
    public float i(dw dwVar) {
        return q(dwVar).i();
    }

    @Override // defpackage.ew
    public void j(dw dwVar) {
        q(dwVar).m(dwVar.d());
        d(dwVar);
    }

    @Override // defpackage.ew
    public float k(dw dwVar) {
        return q(dwVar).l();
    }

    @Override // defpackage.ew
    public void l() {
        r93.r = new a();
    }

    @Override // defpackage.ew
    public void m(dw dwVar, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        r93 p = p(context, colorStateList, f, f2, f3);
        p.m(dwVar.d());
        dwVar.c(p);
        d(dwVar);
    }

    @Override // defpackage.ew
    public void n(dw dwVar, ColorStateList colorStateList) {
        q(dwVar).o(colorStateList);
    }

    @Override // defpackage.ew
    public void o(dw dwVar, float f) {
        q(dwVar).q(f);
        d(dwVar);
    }

    public final r93 p(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new r93(context.getResources(), colorStateList, f, f2, f3);
    }

    public final r93 q(dw dwVar) {
        return (r93) dwVar.f();
    }
}
