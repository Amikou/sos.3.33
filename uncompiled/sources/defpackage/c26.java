package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: c26  reason: default package */
/* loaded from: classes.dex */
public final class c26 implements yp5<d26> {
    public static final c26 f0 = new c26();
    public final yp5<d26> a = gq5.a(gq5.b(new h26()));

    public static boolean a() {
        return f0.zza().zza();
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    public static boolean c() {
        return f0.zza().zzc();
    }

    public static boolean d() {
        return f0.zza().b();
    }

    @Override // defpackage.yp5
    /* renamed from: e */
    public final d26 zza() {
        return this.a.zza();
    }
}
