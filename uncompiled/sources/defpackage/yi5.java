package defpackage;

import android.content.Context;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: yi5  reason: default package */
/* loaded from: classes.dex */
public final class yi5 {
    public final vi5 a;

    public yi5(vi5 vi5Var) {
        zt2.j(vi5Var);
        this.a = vi5Var;
    }

    public final void a(Context context, Intent intent) {
        ck5 e = ck5.e(context, null, null);
        og5 w = e.w();
        if (intent == null) {
            w.p().a("Receiver called with null intent");
            return;
        }
        e.b();
        String action = intent.getAction();
        w.v().b("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            w.v().a("Starting wakeful intent.");
            this.a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            w.p().a("Install Referrer Broadcasts are deprecated");
        }
    }
}
