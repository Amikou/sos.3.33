package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: qu5  reason: default package */
/* loaded from: classes.dex */
public final class qu5 extends vh5 {
    public Handler c;
    public final ou5 d;
    public final lu5 e;
    public final fu5 f;

    public qu5(ck5 ck5Var) {
        super(ck5Var);
        this.d = new ou5(this);
        this.e = new lu5(this);
        this.f = new fu5(this);
    }

    public static /* synthetic */ void l(qu5 qu5Var, long j) {
        qu5Var.e();
        qu5Var.r();
        qu5Var.a.w().v().b("Activity resumed, time", Long.valueOf(j));
        q45 z = qu5Var.a.z();
        we5<Boolean> we5Var = qf5.s0;
        if (z.v(null, we5Var)) {
            if (qu5Var.a.z().C() || qu5Var.a.A().q.a()) {
                qu5Var.e.a(j);
            }
            qu5Var.f.a();
        } else {
            qu5Var.f.a();
            if (qu5Var.a.z().C()) {
                qu5Var.e.a(j);
            }
        }
        ou5 ou5Var = qu5Var.d;
        ou5Var.a.e();
        if (ou5Var.a.a.h()) {
            if (!ou5Var.a.a.z().v(null, we5Var)) {
                ou5Var.a.a.A().q.b(false);
            }
            ou5Var.b(ou5Var.a.a.a().a(), false);
        }
    }

    public static /* synthetic */ void n(qu5 qu5Var, long j) {
        qu5Var.e();
        qu5Var.r();
        qu5Var.a.w().v().b("Activity paused, time", Long.valueOf(j));
        qu5Var.f.b(j);
        if (qu5Var.a.z().C()) {
            qu5Var.e.b(j);
        }
        ou5 ou5Var = qu5Var.d;
        if (ou5Var.a.a.z().v(null, qf5.s0)) {
            return;
        }
        ou5Var.a.a.A().q.b(true);
    }

    @Override // defpackage.vh5
    public final boolean j() {
        return false;
    }

    public final void r() {
        e();
        if (this.c == null) {
            this.c = new z95(Looper.getMainLooper());
        }
    }
}
