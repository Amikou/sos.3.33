package defpackage;

import defpackage.wh;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;
import org.bouncycastle.asn1.z0;

/* renamed from: h4  reason: default package */
/* loaded from: classes2.dex */
public abstract class h4 extends k implements Iterable {
    public Vector a = new Vector();

    public h4() {
    }

    public h4(d4 d4Var) {
        for (int i = 0; i != d4Var.c(); i++) {
            this.a.addElement(d4Var.b(i));
        }
    }

    public static h4 z(Object obj) {
        if (obj == null || (obj instanceof h4)) {
            return (h4) obj;
        }
        if (obj instanceof i4) {
            return z(((i4) obj).i());
        }
        if (obj instanceof byte[]) {
            try {
                return z(k.s((byte[]) obj));
            } catch (IOException e) {
                throw new IllegalArgumentException("failed to construct sequence from byte[]: " + e.getMessage());
            }
        }
        if (obj instanceof c4) {
            k i = ((c4) obj).i();
            if (i instanceof h4) {
                return (h4) i;
            }
        }
        throw new IllegalArgumentException("unknown object in getInstance: " + obj.getClass().getName());
    }

    public final c4 B(Enumeration enumeration) {
        return (c4) enumeration.nextElement();
    }

    public c4 D(int i) {
        return (c4) this.a.elementAt(i);
    }

    public Enumeration E() {
        return this.a.elements();
    }

    public c4[] F() {
        c4[] c4VarArr = new c4[size()];
        for (int i = 0; i != size(); i++) {
            c4VarArr[i] = D(i);
        }
        return c4VarArr;
    }

    @Override // org.bouncycastle.asn1.h
    public int hashCode() {
        Enumeration E = E();
        int size = size();
        while (E.hasMoreElements()) {
            size = (size * 17) ^ B(E).hashCode();
        }
        return size;
    }

    @Override // java.lang.Iterable
    public Iterator<c4> iterator() {
        return new wh.a(F());
    }

    @Override // org.bouncycastle.asn1.k
    public boolean o(k kVar) {
        if (kVar instanceof h4) {
            h4 h4Var = (h4) kVar;
            if (size() != h4Var.size()) {
                return false;
            }
            Enumeration E = E();
            Enumeration E2 = h4Var.E();
            while (E.hasMoreElements()) {
                c4 B = B(E);
                c4 B2 = B(E2);
                k i = B.i();
                k i2 = B2.i();
                if (i != i2 && !i.equals(i2)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public int size() {
        return this.a.size();
    }

    @Override // org.bouncycastle.asn1.k
    public boolean t() {
        return true;
    }

    public String toString() {
        return this.a.toString();
    }

    @Override // org.bouncycastle.asn1.k
    public k w() {
        n0 n0Var = new n0();
        n0Var.a = this.a;
        return n0Var;
    }

    @Override // org.bouncycastle.asn1.k
    public k y() {
        z0 z0Var = new z0();
        z0Var.a = this.a;
        return z0Var;
    }
}
