package defpackage;

import java.util.Arrays;
import java.util.List;

/* compiled from: RlpEncoder.java */
/* renamed from: d93  reason: default package */
/* loaded from: classes3.dex */
public class d93 {
    private static byte[] concat(byte[] bArr, byte[] bArr2) {
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length + bArr2.length);
        System.arraycopy(bArr2, 0, copyOf, bArr.length, bArr2.length);
        return copyOf;
    }

    public static byte[] encode(g93 g93Var) {
        if (g93Var instanceof f93) {
            return encodeString((f93) g93Var);
        }
        return encodeList((e93) g93Var);
    }

    public static byte[] encodeList(e93 e93Var) {
        List<g93> values = e93Var.getValues();
        if (values.isEmpty()) {
            return encode(new byte[0], c93.OFFSET_SHORT_LIST);
        }
        byte[] bArr = new byte[0];
        for (g93 g93Var : values) {
            bArr = concat(bArr, encode(g93Var));
        }
        return encode(bArr, c93.OFFSET_SHORT_LIST);
    }

    public static byte[] encodeString(f93 f93Var) {
        return encode(f93Var.getBytes(), c93.OFFSET_SHORT_STRING);
    }

    private static byte[] toByteArray(int i) {
        return new byte[]{(byte) ((i >> 24) & 255), (byte) ((i >> 16) & 255), (byte) ((i >> 8) & 255), (byte) (i & 255)};
    }

    private static byte[] toMinimalByteArray(int i) {
        byte[] byteArray = toByteArray(i);
        for (int i2 = 0; i2 < byteArray.length; i2++) {
            if (byteArray[i2] != 0) {
                return Arrays.copyOfRange(byteArray, i2, byteArray.length);
            }
        }
        return new byte[0];
    }

    private static byte[] encode(byte[] bArr, int i) {
        if (bArr.length != 1 || i != c93.OFFSET_SHORT_STRING || bArr[0] < 0 || bArr[0] > Byte.MAX_VALUE) {
            if (bArr.length <= 55) {
                byte[] bArr2 = new byte[bArr.length + 1];
                bArr2[0] = (byte) (i + bArr.length);
                System.arraycopy(bArr, 0, bArr2, 1, bArr.length);
                return bArr2;
            }
            byte[] minimalByteArray = toMinimalByteArray(bArr.length);
            byte[] bArr3 = new byte[bArr.length + minimalByteArray.length + 1];
            bArr3[0] = (byte) (i + 55 + minimalByteArray.length);
            System.arraycopy(minimalByteArray, 0, bArr3, 1, minimalByteArray.length);
            System.arraycopy(bArr, 0, bArr3, minimalByteArray.length + 1, bArr.length);
            return bArr3;
        }
        return bArr;
    }
}
