package defpackage;

import defpackage.pt0;

/* renamed from: vh3  reason: default package */
/* loaded from: classes2.dex */
public class vh3 extends pt0.b {
    public vh3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public vh3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public vh3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 J() {
        long[] jArr;
        if (u()) {
            return this;
        }
        xs0 i = i();
        rh3 rh3Var = (rh3) this.b;
        if (rh3Var.i()) {
            return i.v();
        }
        rh3 rh3Var2 = (rh3) this.c;
        rh3 rh3Var3 = (rh3) this.d[0];
        long[] b = jd2.b();
        long[] b2 = jd2.b();
        long[] p = rh3Var3.h() ? null : qh3.p(rh3Var3.f);
        long[] jArr2 = rh3Var2.f;
        if (p == null) {
            jArr = rh3Var3.f;
        } else {
            qh3.n(jArr2, p, b);
            qh3.t(rh3Var3.f, b2);
            jArr2 = b;
            jArr = b2;
        }
        long[] b3 = jd2.b();
        qh3.t(rh3Var2.f, b3);
        qh3.d(jArr2, jArr, b3);
        if (jd2.g(b3)) {
            return new vh3(i, new rh3(b3), uh3.l, this.e);
        }
        long[] c = jd2.c();
        qh3.m(b3, jArr2, c);
        rh3 rh3Var4 = new rh3(b);
        qh3.t(b3, rh3Var4.f);
        rh3 rh3Var5 = new rh3(b3);
        if (p != null) {
            long[] jArr3 = rh3Var5.f;
            qh3.l(jArr3, jArr, jArr3);
        }
        long[] jArr4 = rh3Var.f;
        if (p != null) {
            qh3.n(jArr4, p, b2);
            jArr4 = b2;
        }
        qh3.u(jArr4, c);
        qh3.q(c, b2);
        qh3.d(rh3Var4.f, rh3Var5.f, b2);
        return new vh3(i, rh3Var4, new rh3(b2), new ct0[]{rh3Var5}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return J();
        }
        xs0 i = i();
        rh3 rh3Var = (rh3) this.b;
        if (rh3Var.i()) {
            return pt0Var;
        }
        rh3 rh3Var2 = (rh3) pt0Var.n();
        rh3 rh3Var3 = (rh3) pt0Var.s(0);
        if (rh3Var2.i() || !rh3Var3.h()) {
            return J().a(pt0Var);
        }
        rh3 rh3Var4 = (rh3) this.c;
        rh3 rh3Var5 = (rh3) this.d[0];
        rh3 rh3Var6 = (rh3) pt0Var.o();
        long[] b = jd2.b();
        long[] b2 = jd2.b();
        long[] b3 = jd2.b();
        long[] b4 = jd2.b();
        qh3.t(rh3Var.f, b);
        qh3.t(rh3Var4.f, b2);
        qh3.t(rh3Var5.f, b3);
        qh3.l(rh3Var4.f, rh3Var5.f, b4);
        qh3.d(b3, b2, b4);
        long[] p = qh3.p(b3);
        qh3.n(rh3Var6.f, p, b3);
        qh3.b(b3, b2, b3);
        long[] c = jd2.c();
        qh3.m(b3, b4, c);
        qh3.o(b, p, c);
        qh3.q(c, b3);
        qh3.n(rh3Var2.f, p, b);
        qh3.b(b, b4, b2);
        qh3.t(b2, b2);
        if (jd2.g(b2)) {
            return jd2.g(b3) ? pt0Var.J() : i.v();
        } else if (jd2.g(b3)) {
            return new vh3(i, new rh3(b3), uh3.l, this.e);
        } else {
            rh3 rh3Var7 = new rh3();
            qh3.t(b3, rh3Var7.f);
            long[] jArr = rh3Var7.f;
            qh3.l(jArr, b, jArr);
            rh3 rh3Var8 = new rh3(b);
            qh3.l(b3, b2, rh3Var8.f);
            long[] jArr2 = rh3Var8.f;
            qh3.n(jArr2, p, jArr2);
            rh3 rh3Var9 = new rh3(b2);
            qh3.b(b3, b2, rh3Var9.f);
            long[] jArr3 = rh3Var9.f;
            qh3.t(jArr3, jArr3);
            kd2.R(18, c);
            qh3.m(rh3Var9.f, b4, c);
            qh3.f(rh3Var6.f, b4);
            qh3.m(b4, rh3Var8.f, c);
            qh3.q(c, rh3Var9.f);
            return new vh3(i, rh3Var7, rh3Var9, new ct0[]{rh3Var8}, this.e);
        }
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        long[] jArr;
        long[] jArr2;
        long[] jArr3;
        rh3 rh3Var;
        rh3 rh3Var2;
        rh3 rh3Var3;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        xs0 i = i();
        rh3 rh3Var4 = (rh3) this.b;
        rh3 rh3Var5 = (rh3) pt0Var.n();
        if (rh3Var4.i()) {
            return rh3Var5.i() ? i.v() : pt0Var.a(this);
        }
        rh3 rh3Var6 = (rh3) this.c;
        rh3 rh3Var7 = (rh3) this.d[0];
        rh3 rh3Var8 = (rh3) pt0Var.o();
        rh3 rh3Var9 = (rh3) pt0Var.s(0);
        long[] b = jd2.b();
        long[] b2 = jd2.b();
        long[] b3 = jd2.b();
        long[] b4 = jd2.b();
        long[] p = rh3Var7.h() ? null : qh3.p(rh3Var7.f);
        if (p == null) {
            jArr = rh3Var5.f;
            jArr2 = rh3Var8.f;
        } else {
            qh3.n(rh3Var5.f, p, b2);
            qh3.n(rh3Var8.f, p, b4);
            jArr = b2;
            jArr2 = b4;
        }
        long[] p2 = rh3Var9.h() ? null : qh3.p(rh3Var9.f);
        long[] jArr4 = rh3Var4.f;
        if (p2 == null) {
            jArr3 = rh3Var6.f;
        } else {
            qh3.n(jArr4, p2, b);
            qh3.n(rh3Var6.f, p2, b3);
            jArr4 = b;
            jArr3 = b3;
        }
        qh3.b(jArr3, jArr2, b3);
        qh3.b(jArr4, jArr, b4);
        if (jd2.g(b4)) {
            return jd2.g(b3) ? J() : i.v();
        }
        if (rh3Var5.i()) {
            pt0 A = A();
            rh3 rh3Var10 = (rh3) A.q();
            ct0 r = A.r();
            ct0 d = r.a(rh3Var8).d(rh3Var10);
            rh3Var = (rh3) d.o().a(d).a(rh3Var10).b();
            if (rh3Var.i()) {
                return new vh3(i, rh3Var, uh3.l, this.e);
            }
            rh3Var3 = (rh3) i.n(ws0.b);
            rh3Var2 = (rh3) d.j(rh3Var10.a(rh3Var)).a(rh3Var).a(r).d(rh3Var).a(rh3Var);
        } else {
            qh3.t(b4, b4);
            long[] p3 = qh3.p(b3);
            qh3.n(jArr4, p3, b);
            qh3.n(jArr, p3, b2);
            rh3 rh3Var11 = new rh3(b);
            qh3.l(b, b2, rh3Var11.f);
            if (rh3Var11.i()) {
                return new vh3(i, rh3Var11, uh3.l, this.e);
            }
            rh3 rh3Var12 = new rh3(b3);
            qh3.n(b4, p3, rh3Var12.f);
            if (p2 != null) {
                long[] jArr5 = rh3Var12.f;
                qh3.n(jArr5, p2, jArr5);
            }
            long[] c = jd2.c();
            qh3.b(b2, b4, b4);
            qh3.u(b4, c);
            qh3.b(rh3Var6.f, rh3Var7.f, b4);
            qh3.m(b4, rh3Var12.f, c);
            rh3 rh3Var13 = new rh3(b4);
            qh3.q(c, rh3Var13.f);
            if (p != null) {
                long[] jArr6 = rh3Var12.f;
                qh3.n(jArr6, p, jArr6);
            }
            rh3Var = rh3Var11;
            rh3Var2 = rh3Var13;
            rh3Var3 = rh3Var12;
        }
        return new vh3(i, rh3Var, rh3Var2, new ct0[]{rh3Var3}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new vh3(null, f(), g());
    }

    @Override // defpackage.pt0
    public boolean h() {
        ct0 n = n();
        return (n.i() || o().s() == n.s()) ? false : true;
    }

    @Override // defpackage.pt0
    public ct0 r() {
        ct0 ct0Var = this.b;
        ct0 ct0Var2 = this.c;
        if (u() || ct0Var.i()) {
            return ct0Var2;
        }
        ct0 j = ct0Var2.a(ct0Var).j(ct0Var);
        ct0 ct0Var3 = this.d[0];
        return !ct0Var3.h() ? j.d(ct0Var3) : j;
    }

    @Override // defpackage.pt0
    public pt0 z() {
        if (u()) {
            return this;
        }
        ct0 ct0Var = this.b;
        if (ct0Var.i()) {
            return this;
        }
        ct0 ct0Var2 = this.c;
        ct0 ct0Var3 = this.d[0];
        return new vh3(this.a, ct0Var, ct0Var2.a(ct0Var3), new ct0[]{ct0Var3}, this.e);
    }
}
