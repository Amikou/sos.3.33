package defpackage;

import defpackage.i80;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java8.util.o;
import java8.util.stream.h;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.exceptions.TransactionException;
import org.web3j.tx.exceptions.ContractCallException;

/* compiled from: Contract.java */
/* renamed from: i80  reason: default package */
/* loaded from: classes3.dex */
public abstract class i80 extends o32 {
    public static final String BIN_NOT_PROVIDED = "Bin file was not provided";
    public static final String FUNC_DEPLOY = "deploy";
    public static final BigInteger GAS_LIMIT = BigInteger.valueOf(4300000);
    public String contractAddress;
    public final String contractBinary;
    public gi0 defaultBlockParameter;
    public Map<String, String> deployedAddresses;
    public j80 gasProvider;
    public w84 transactionReceipt;

    /* compiled from: Contract.java */
    /* renamed from: i80$b */
    /* loaded from: classes3.dex */
    public static class b {
        private final ky0 eventValues;
        private final q12 log;

        public List<zc4> getIndexedValues() {
            return this.eventValues.getIndexedValues();
        }

        public q12 getLog() {
            return this.log;
        }

        public List<zc4> getNonIndexedValues() {
            return this.eventValues.getNonIndexedValues();
        }

        private b(ky0 ky0Var, q12 q12Var) {
            this.eventValues = ky0Var;
            this.log = q12Var;
        }
    }

    public i80(String str, String str2, ko4 ko4Var, u84 u84Var, j80 j80Var) {
        this(new org.web3j.ens.a(ko4Var), str, str2, ko4Var, u84Var, j80Var);
    }

    public static <S extends zc4, T> List<T> convertToNative(List<S> list) {
        ArrayList arrayList = new ArrayList();
        for (S s : list) {
            arrayList.add(s.getValue());
        }
        return arrayList;
    }

    private static <T extends i80> T create(T t, String str, String str2, BigInteger bigInteger) throws IOException, TransactionException {
        w84 executeTransaction = t.executeTransaction(str + str2, bigInteger, FUNC_DEPLOY, true);
        String contractAddress = executeTransaction.getContractAddress();
        if (contractAddress != null) {
            t.setContractAddress(contractAddress);
            t.setTransactionReceipt(executeTransaction);
            return t;
        }
        throw new RuntimeException("Empty contract address returned");
    }

    public static <T extends i80> T deploy(Class<T> cls, ko4 ko4Var, ma0 ma0Var, j80 j80Var, String str, String str2, BigInteger bigInteger) throws RuntimeException, TransactionException {
        try {
            Constructor<T> declaredConstructor = cls.getDeclaredConstructor(String.class, ko4.class, ma0.class, j80.class);
            declaredConstructor.setAccessible(true);
            return (T) create(declaredConstructor.newInstance(null, ko4Var, ma0Var, j80Var), str, str2, bigInteger);
        } catch (TransactionException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(final Class<T> cls, final ko4 ko4Var, final ma0 ma0Var, final BigInteger bigInteger, final BigInteger bigInteger2, final String str, final String str2, final BigInteger bigInteger3) {
        return new org.web3j.protocol.core.b<>(new Callable() { // from class: v70
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i80 deploy;
                deploy = i80.deploy(cls, ko4Var, ma0Var, bigInteger, bigInteger2, str, str2, bigInteger3);
                return deploy;
            }
        });
    }

    private List<zc4> executeCall(id1 id1Var) throws IOException {
        return yd1.decode(call(this.contractAddress, org.web3j.abi.b.encode(id1Var), this.defaultBlockParameter), id1Var.getOutputParameters());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ i80 lambda$deployRemoteCall$10(Class cls, ko4 ko4Var, u84 u84Var, j80 j80Var, String str, String str2) throws Exception {
        return deploy(cls, ko4Var, u84Var, j80Var, str, str2, BigInteger.ZERO);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ i80 lambda$deployRemoteCall$7(Class cls, ko4 ko4Var, ma0 ma0Var, j80 j80Var, String str, String str2) throws Exception {
        return deploy(cls, ko4Var, ma0Var, j80Var, str, str2, BigInteger.ZERO);
    }

    public static ky0 staticExtractEventParameters(px0 px0Var, q12 q12Var) {
        List<String> topics = q12Var.getTopics();
        String encode = ux0.encode(px0Var);
        if (topics == null || topics.size() == 0) {
            return null;
        }
        int i = 0;
        if (topics.get(0).equals(encode)) {
            ArrayList arrayList = new ArrayList();
            List<zc4> decode = yd1.decode(q12Var.getData(), px0Var.getNonIndexedParameters());
            List<TypeReference<zc4>> indexedParameters = px0Var.getIndexedParameters();
            while (i < indexedParameters.size()) {
                int i2 = i + 1;
                arrayList.add(yd1.decodeIndexedValue(topics.get(i2), indexedParameters.get(i)));
                i = i2;
            }
            return new ky0(arrayList, decode);
        }
        return null;
    }

    public static b staticExtractEventParametersWithLog(px0 px0Var, q12 q12Var) {
        ky0 staticExtractEventParameters = staticExtractEventParameters(px0Var, q12Var);
        if (staticExtractEventParameters == null) {
            return null;
        }
        return new b(staticExtractEventParameters, q12Var);
    }

    /* renamed from: executeCallMultipleValueReturn */
    public List<zc4> lambda$executeRemoteCallMultipleValueReturn$2(id1 id1Var) throws IOException {
        return executeCall(id1Var);
    }

    /* renamed from: executeCallSingleValueReturn */
    public <T extends zc4> T lambda$executeRemoteCallSingleValueReturn$0(id1 id1Var) throws IOException {
        List<zc4> executeCall = executeCall(id1Var);
        if (executeCall.isEmpty()) {
            return null;
        }
        return (T) executeCall.get(0);
    }

    public m63<List<zc4>> executeRemoteCallMultipleValueReturn(final id1 id1Var) {
        return new m63<>(id1Var, new Callable() { // from class: e80
            @Override // java.util.concurrent.Callable
            public final Object call() {
                List lambda$executeRemoteCallMultipleValueReturn$2;
                lambda$executeRemoteCallMultipleValueReturn$2 = i80.this.lambda$executeRemoteCallMultipleValueReturn$2(id1Var);
                return lambda$executeRemoteCallMultipleValueReturn$2;
            }
        });
    }

    public <T extends zc4> m63<T> executeRemoteCallSingleValueReturn(final id1 id1Var) {
        return new m63<>(id1Var, new Callable() { // from class: c80
            @Override // java.util.concurrent.Callable
            public final Object call() {
                zc4 lambda$executeRemoteCallSingleValueReturn$0;
                lambda$executeRemoteCallSingleValueReturn$0 = i80.this.lambda$executeRemoteCallSingleValueReturn$0(id1Var);
                return lambda$executeRemoteCallSingleValueReturn$0;
            }
        });
    }

    public m63<w84> executeRemoteCallTransaction(final id1 id1Var) {
        return new m63<>(id1Var, new Callable() { // from class: d80
            @Override // java.util.concurrent.Callable
            public final Object call() {
                w84 lambda$executeRemoteCallTransaction$3;
                lambda$executeRemoteCallTransaction$3 = i80.this.lambda$executeRemoteCallTransaction$3(id1Var);
                return lambda$executeRemoteCallTransaction$3;
            }
        });
    }

    /* renamed from: executeTransaction */
    public w84 lambda$executeRemoteCallTransaction$3(id1 id1Var) throws IOException, TransactionException {
        return lambda$executeRemoteCallTransaction$4(id1Var, BigInteger.ZERO);
    }

    /* renamed from: extractEventParameters */
    public ky0 lambda$extractEventParameters$11(px0 px0Var, q12 q12Var) {
        return staticExtractEventParameters(px0Var, q12Var);
    }

    /* renamed from: extractEventParametersWithLog */
    public b lambda$extractEventParametersWithLog$12(px0 px0Var, q12 q12Var) {
        return staticExtractEventParametersWithLog(px0Var, q12Var);
    }

    public String getContractAddress() {
        return this.contractAddress;
    }

    public String getContractBinary() {
        return this.contractBinary;
    }

    public final String getDeployedAddress(String str) {
        Map<String, String> map = this.deployedAddresses;
        String str2 = map != null ? map.get(str) : null;
        return str2 == null ? getStaticDeployedAddress(str) : str2;
    }

    public BigInteger getGasPrice() {
        return this.gasProvider.getGasPrice();
    }

    public String getStaticDeployedAddress(String str) {
        return null;
    }

    public o<w84> getTransactionReceipt() {
        return o.e(this.transactionReceipt);
    }

    public boolean isValid() throws IOException {
        if (!this.contractBinary.equals(BIN_NOT_PROVIDED)) {
            if (!this.contractAddress.equals("")) {
                vw0 code = this.transactionManager.getCode(this.contractAddress, DefaultBlockParameterName.LATEST);
                if (code.hasError()) {
                    return false;
                }
                String cleanHexPrefix = ej2.cleanHexPrefix(code.getCode());
                int indexOf = cleanHexPrefix.indexOf("a165627a7a72305820");
                if (indexOf != -1) {
                    cleanHexPrefix = cleanHexPrefix.substring(0, indexOf);
                }
                return !cleanHexPrefix.isEmpty() && this.contractBinary.contains(cleanHexPrefix);
            }
            throw new UnsupportedOperationException("Contract binary not present, you will need to regenerate your smart contract wrapper with web3j v2.2.0+");
        }
        throw new UnsupportedOperationException("Contract binary not present in contract wrapper, please generate your wrapper using -abiFile=<file>");
    }

    public String resolveContractAddress(String str) {
        return this.ensResolver.resolve(str);
    }

    public void setContractAddress(String str) {
        this.contractAddress = str;
    }

    public void setDefaultBlockParameter(gi0 gi0Var) {
        this.defaultBlockParameter = gi0Var;
    }

    public final void setDeployedAddress(String str, String str2) {
        if (this.deployedAddresses == null) {
            this.deployedAddresses = new HashMap();
        }
        this.deployedAddresses.put(str, str2);
    }

    public void setGasPrice(BigInteger bigInteger) {
        this.gasProvider = new it3(bigInteger, this.gasProvider.getGasLimit());
    }

    public void setGasProvider(j80 j80Var) {
        this.gasProvider = j80Var;
    }

    public void setTransactionReceipt(w84 w84Var) {
        this.transactionReceipt = w84Var;
    }

    public i80(org.web3j.ens.a aVar, String str, String str2, ko4 ko4Var, u84 u84Var, j80 j80Var) {
        super(aVar, ko4Var, u84Var);
        this.defaultBlockParameter = DefaultBlockParameterName.LATEST;
        this.contractAddress = resolveContractAddress(str2);
        this.contractBinary = str;
        this.gasProvider = j80Var;
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(Class<T> cls, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2, String str, String str2) {
        return deployRemoteCall(cls, ko4Var, ma0Var, bigInteger, bigInteger2, str, str2, BigInteger.ZERO);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: executeTransaction */
    public w84 lambda$executeRemoteCallTransaction$4(id1 id1Var, BigInteger bigInteger) throws IOException, TransactionException {
        return executeTransaction(org.web3j.abi.b.encode(id1Var), bigInteger, id1Var.getName());
    }

    public <T> m63<T> executeRemoteCallSingleValueReturn(final id1 id1Var, final Class<T> cls) {
        return new m63<>(id1Var, new Callable() { // from class: f80
            @Override // java.util.concurrent.Callable
            public final Object call() {
                Object lambda$executeRemoteCallSingleValueReturn$1;
                lambda$executeRemoteCallSingleValueReturn$1 = i80.this.lambda$executeRemoteCallSingleValueReturn$1(id1Var, cls);
                return lambda$executeRemoteCallSingleValueReturn$1;
            }
        });
    }

    public m63<w84> executeRemoteCallTransaction(final id1 id1Var, final BigInteger bigInteger) {
        return new m63<>(id1Var, new Callable() { // from class: g80
            @Override // java.util.concurrent.Callable
            public final Object call() {
                w84 lambda$executeRemoteCallTransaction$4;
                lambda$executeRemoteCallTransaction$4 = i80.this.lambda$executeRemoteCallTransaction$4(id1Var, bigInteger);
                return lambda$executeRemoteCallTransaction$4;
            }
        });
    }

    public List<ky0> extractEventParameters(final px0 px0Var, w84 w84Var) {
        return (List) h.a(w84Var.getLogs()).c(new nd1() { // from class: z70
            @Override // defpackage.nd1
            public final Object apply(Object obj) {
                ky0 lambda$extractEventParameters$11;
                lambda$extractEventParameters$11 = i80.this.lambda$extractEventParameters$11(px0Var, (q12) obj);
                return lambda$extractEventParameters$11;
            }
        }).b(a80.a).f(java8.util.stream.b.e());
    }

    public List<b> extractEventParametersWithLog(final px0 px0Var, w84 w84Var) {
        return (List) h.a(w84Var.getLogs()).c(new nd1() { // from class: t70
            @Override // defpackage.nd1
            public final Object apply(Object obj) {
                i80.b lambda$extractEventParametersWithLog$12;
                lambda$extractEventParametersWithLog$12 = i80.this.lambda$extractEventParametersWithLog$12(px0Var, (q12) obj);
                return lambda$extractEventParametersWithLog$12;
            }
        }).b(b80.a).f(java8.util.stream.b.e());
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(final Class<T> cls, final ko4 ko4Var, final ma0 ma0Var, final j80 j80Var, final String str, final String str2, final BigInteger bigInteger) {
        return new org.web3j.protocol.core.b<>(new Callable() { // from class: u70
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i80 deploy;
                deploy = i80.deploy(cls, ko4Var, ma0Var, j80Var, str, str2, bigInteger);
                return deploy;
            }
        });
    }

    public w84 executeTransaction(String str, BigInteger bigInteger, String str2) throws TransactionException, IOException {
        return executeTransaction(str, bigInteger, str2, false);
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(final Class<T> cls, final ko4 ko4Var, final ma0 ma0Var, final j80 j80Var, final String str, final String str2) {
        return new org.web3j.protocol.core.b<>(new Callable() { // from class: h80
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i80 lambda$deployRemoteCall$7;
                lambda$deployRemoteCall$7 = i80.lambda$deployRemoteCall$7(cls, ko4Var, ma0Var, j80Var, str, str2);
                return lambda$deployRemoteCall$7;
            }
        });
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [R, zc4, java.lang.Object] */
    /* renamed from: executeCallSingleValueReturn */
    public <T extends zc4, R> R lambda$executeRemoteCallSingleValueReturn$1(id1 id1Var, Class<R> cls) throws IOException {
        ?? r4 = (R) lambda$executeRemoteCallSingleValueReturn$0(id1Var);
        if (r4 != 0) {
            R r = (R) r4.getValue();
            if (cls.isAssignableFrom(r4.getClass())) {
                return r4;
            }
            if (cls.isAssignableFrom(r.getClass())) {
                return r;
            }
            if (r4.getClass().equals(Address.class) && cls.equals(String.class)) {
                return (R) r4.toString();
            }
            throw new ContractCallException("Unable to convert response: " + r + " to expected type: " + cls.getSimpleName());
        }
        throw new ContractCallException("Empty value (0x) returned from contract");
    }

    public w84 executeTransaction(String str, BigInteger bigInteger, String str2, boolean z) throws TransactionException, IOException {
        w84 send = send(this.contractAddress, str, bigInteger, this.gasProvider.getGasPrice(str2), this.gasProvider.getGasLimit(str2), z);
        if (send.isStatusOK()) {
            return send;
        }
        Object[] objArr = new Object[4];
        objArr[0] = send.getTransactionHash();
        objArr[1] = send.getStatus();
        objArr[2] = send.getGasUsedRaw() != null ? send.getGasUsed().toString() : "unknown";
        objArr[3] = org.web3j.utils.b.extractRevertReason(send, str, this.web3j, Boolean.TRUE);
        throw new TransactionException(String.format("Transaction %s has failed with status: %s. Gas used: %s. Revert reason: '%s'.", objArr), send);
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(final Class<T> cls, final ko4 ko4Var, final u84 u84Var, final BigInteger bigInteger, final BigInteger bigInteger2, final String str, final String str2, final BigInteger bigInteger3) {
        return new org.web3j.protocol.core.b<>(new Callable() { // from class: y70
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i80 deploy;
                deploy = i80.deploy(cls, ko4Var, u84Var, bigInteger, bigInteger2, str, str2, bigInteger3);
                return deploy;
            }
        });
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(Class<T> cls, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2, String str, String str2) {
        return deployRemoteCall(cls, ko4Var, u84Var, bigInteger, bigInteger2, str, str2, BigInteger.ZERO);
    }

    public i80(String str, String str2, ko4 ko4Var, ma0 ma0Var, j80 j80Var) {
        this(new org.web3j.ens.a(ko4Var), str, str2, ko4Var, new z33(ko4Var, ma0Var), j80Var);
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(final Class<T> cls, final ko4 ko4Var, final u84 u84Var, final j80 j80Var, final String str, final String str2, final BigInteger bigInteger) {
        return new org.web3j.protocol.core.b<>(new Callable() { // from class: x70
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i80 deploy;
                deploy = i80.deploy(cls, ko4Var, u84Var, j80Var, str, str2, bigInteger);
                return deploy;
            }
        });
    }

    @Deprecated
    public i80(String str, String str2, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        this(new org.web3j.ens.a(ko4Var), str, str2, ko4Var, u84Var, new it3(bigInteger, bigInteger2));
    }

    public static <T extends i80> T deploy(Class<T> cls, ko4 ko4Var, u84 u84Var, j80 j80Var, String str, String str2, BigInteger bigInteger) throws RuntimeException, TransactionException {
        try {
            Constructor<T> declaredConstructor = cls.getDeclaredConstructor(String.class, ko4.class, u84.class, j80.class);
            declaredConstructor.setAccessible(true);
            return (T) create(declaredConstructor.newInstance(null, ko4Var, u84Var, j80Var), str, str2, bigInteger);
        } catch (TransactionException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public static <T extends i80> org.web3j.protocol.core.b<T> deployRemoteCall(final Class<T> cls, final ko4 ko4Var, final u84 u84Var, final j80 j80Var, final String str, final String str2) {
        return new org.web3j.protocol.core.b<>(new Callable() { // from class: w70
            @Override // java.util.concurrent.Callable
            public final Object call() {
                i80 lambda$deployRemoteCall$10;
                lambda$deployRemoteCall$10 = i80.lambda$deployRemoteCall$10(cls, ko4Var, u84Var, j80Var, str, str2);
                return lambda$deployRemoteCall$10;
            }
        });
    }

    @Deprecated
    public i80(String str, String str2, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        this(str, str2, ko4Var, new z33(ko4Var, ma0Var), bigInteger, bigInteger2);
    }

    @Deprecated
    public i80(String str, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2) {
        this("", str, ko4Var, u84Var, bigInteger, bigInteger2);
    }

    @Deprecated
    public i80(String str, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2) {
        this("", str, ko4Var, new z33(ko4Var, ma0Var), bigInteger, bigInteger2);
    }

    @Deprecated
    public static <T extends i80> T deploy(Class<T> cls, ko4 ko4Var, ma0 ma0Var, BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3) throws RuntimeException, TransactionException {
        return (T) deploy(cls, ko4Var, ma0Var, new it3(bigInteger, bigInteger2), str, str2, bigInteger3);
    }

    @Deprecated
    public static <T extends i80> T deploy(Class<T> cls, ko4 ko4Var, u84 u84Var, BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3) throws RuntimeException, TransactionException {
        return (T) deploy(cls, ko4Var, u84Var, new it3(bigInteger, bigInteger2), str, str2, bigInteger3);
    }
}
