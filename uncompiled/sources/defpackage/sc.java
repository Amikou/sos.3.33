package defpackage;

/* compiled from: Android.java */
/* renamed from: sc  reason: default package */
/* loaded from: classes2.dex */
public final class sc {
    public static final Class<?> a = a("libcore.io.Memory");
    public static final boolean b;

    static {
        b = a("org.robolectric.Robolectric") != null;
    }

    public static <T> Class<T> a(String str) {
        try {
            return (Class<T>) Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static Class<?> b() {
        return a;
    }

    public static boolean c() {
        return (a == null || b) ? false : true;
    }
}
