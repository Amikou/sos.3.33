package defpackage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: wk5  reason: default package */
/* loaded from: classes.dex */
public final class wk5 {
    public final wk5 a;
    public final s65 b;
    public final Map<String, z55> c = new HashMap();
    public final Map<String, Boolean> d = new HashMap();

    public wk5(wk5 wk5Var, s65 s65Var) {
        this.a = wk5Var;
        this.b = s65Var;
    }

    public final z55 a(z55 z55Var) {
        return this.b.b(this, z55Var);
    }

    public final z55 b(p45 p45Var) {
        z55 z55Var = z55.X;
        Iterator<Integer> q = p45Var.q();
        while (q.hasNext()) {
            z55Var = this.b.b(this, p45Var.w(q.next().intValue()));
            if (z55Var instanceof v45) {
                break;
            }
        }
        return z55Var;
    }

    public final wk5 c() {
        return new wk5(this, this.b);
    }

    public final boolean d(String str) {
        if (this.c.containsKey(str)) {
            return true;
        }
        wk5 wk5Var = this.a;
        if (wk5Var != null) {
            return wk5Var.d(str);
        }
        return false;
    }

    public final void e(String str, z55 z55Var) {
        wk5 wk5Var;
        if (!this.c.containsKey(str) && (wk5Var = this.a) != null && wk5Var.d(str)) {
            this.a.e(str, z55Var);
        } else if (this.d.containsKey(str)) {
        } else {
            if (z55Var == null) {
                this.c.remove(str);
            } else {
                this.c.put(str, z55Var);
            }
        }
    }

    public final void f(String str, z55 z55Var) {
        if (this.d.containsKey(str)) {
            return;
        }
        if (z55Var == null) {
            this.c.remove(str);
        } else {
            this.c.put(str, z55Var);
        }
    }

    public final void g(String str, z55 z55Var) {
        f(str, z55Var);
        this.d.put(str, Boolean.TRUE);
    }

    public final z55 h(String str) {
        if (this.c.containsKey(str)) {
            return this.c.get(str);
        }
        wk5 wk5Var = this.a;
        if (wk5Var != null) {
            return wk5Var.h(str);
        }
        throw new IllegalArgumentException(String.format("%s is not defined", str));
    }
}
