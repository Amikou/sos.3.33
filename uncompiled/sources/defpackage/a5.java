package defpackage;

import kotlin.collections.State;

/* renamed from: a5  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class a5 {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[State.values().length];
        a = iArr;
        iArr[State.Done.ordinal()] = 1;
        iArr[State.Ready.ordinal()] = 2;
    }
}
