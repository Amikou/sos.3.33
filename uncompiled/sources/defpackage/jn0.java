package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAnchorSwitchWalletBinding.java */
/* renamed from: jn0  reason: default package */
/* loaded from: classes2.dex */
public final class jn0 {
    public final LinearLayoutCompat a;

    public jn0(ConstraintLayout constraintLayout, LinearLayoutCompat linearLayoutCompat, AppCompatTextView appCompatTextView) {
        this.a = linearLayoutCompat;
    }

    public static jn0 a(View view) {
        int i = R.id.buttonWrapper;
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.buttonWrapper);
        if (linearLayoutCompat != null) {
            i = R.id.txtTitle;
            AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtTitle);
            if (appCompatTextView != null) {
                return new jn0((ConstraintLayout) view, linearLayoutCompat, appCompatTextView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
