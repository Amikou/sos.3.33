package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.imageview.ShapeableImageView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.CustomVideoPlayer;

/* compiled from: ItemCollectibleBinding.java */
/* renamed from: ws1  reason: default package */
/* loaded from: classes2.dex */
public final class ws1 {
    public final ConstraintLayout a;
    public final ShapeableImageView b;
    public final ShapeableImageView c;
    public final ImageView d;
    public final TextView e;
    public final SimpleDraweeView f;
    public final CustomVideoPlayer g;

    public ws1(ConstraintLayout constraintLayout, ShapeableImageView shapeableImageView, ShapeableImageView shapeableImageView2, ImageView imageView, TextView textView, SimpleDraweeView simpleDraweeView, CustomVideoPlayer customVideoPlayer) {
        this.a = constraintLayout;
        this.b = shapeableImageView;
        this.c = shapeableImageView2;
        this.d = imageView;
        this.e = textView;
        this.f = simpleDraweeView;
        this.g = customVideoPlayer;
    }

    public static ws1 a(View view) {
        int i = R.id.imgDeleteCollection;
        ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgDeleteCollection);
        if (shapeableImageView != null) {
            i = R.id.imgHideEye;
            ShapeableImageView shapeableImageView2 = (ShapeableImageView) ai4.a(view, R.id.imgHideEye);
            if (shapeableImageView2 != null) {
                i = R.id.nftIcon;
                ImageView imageView = (ImageView) ai4.a(view, R.id.nftIcon);
                if (imageView != null) {
                    i = R.id.nftName;
                    TextView textView = (TextView) ai4.a(view, R.id.nftName);
                    if (textView != null) {
                        i = R.id.sdvImage;
                        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) ai4.a(view, R.id.sdvImage);
                        if (simpleDraweeView != null) {
                            i = R.id.videoPlayer;
                            CustomVideoPlayer customVideoPlayer = (CustomVideoPlayer) ai4.a(view, R.id.videoPlayer);
                            if (customVideoPlayer != null) {
                                return new ws1((ConstraintLayout) view, shapeableImageView, shapeableImageView2, imageView, textView, simpleDraweeView, customVideoPlayer);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static ws1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_collectible, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
