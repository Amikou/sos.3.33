package defpackage;

import com.google.android.gms.internal.clearcut.c;
import com.google.android.gms.internal.clearcut.o;
import defpackage.h65;

/* renamed from: h65  reason: default package */
/* loaded from: classes.dex */
public abstract class h65<MessageType extends c<MessageType, BuilderType>, BuilderType extends h65<MessageType, BuilderType>> implements he5 {
    public abstract BuilderType b(MessageType messagetype);

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.he5
    public final /* synthetic */ he5 z(o oVar) {
        if (a().getClass().isInstance(oVar)) {
            return b((c) oVar);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
