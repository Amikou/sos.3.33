package defpackage;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import defpackage.vb0;

/* compiled from: CursorAdapter.java */
/* renamed from: ub0  reason: default package */
/* loaded from: classes.dex */
public abstract class ub0 extends BaseAdapter implements Filterable, vb0.a {
    public boolean a;
    public boolean f0;
    public Cursor g0;
    public Context h0;
    public int i0;
    public a j0;
    public DataSetObserver k0;
    public vb0 l0;

    /* compiled from: CursorAdapter.java */
    /* renamed from: ub0$a */
    /* loaded from: classes.dex */
    public class a extends ContentObserver {
        public a() {
            super(new Handler());
        }

        @Override // android.database.ContentObserver
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override // android.database.ContentObserver
        public void onChange(boolean z) {
            ub0.this.i();
        }
    }

    /* compiled from: CursorAdapter.java */
    /* renamed from: ub0$b */
    /* loaded from: classes.dex */
    public class b extends DataSetObserver {
        public b() {
        }

        @Override // android.database.DataSetObserver
        public void onChanged() {
            ub0 ub0Var = ub0.this;
            ub0Var.a = true;
            ub0Var.notifyDataSetChanged();
        }

        @Override // android.database.DataSetObserver
        public void onInvalidated() {
            ub0 ub0Var = ub0.this;
            ub0Var.a = false;
            ub0Var.notifyDataSetInvalidated();
        }
    }

    public ub0(Context context, Cursor cursor, boolean z) {
        f(context, cursor, z ? 1 : 2);
    }

    public void a(Cursor cursor) {
        Cursor j = j(cursor);
        if (j != null) {
            j.close();
        }
    }

    @Override // defpackage.vb0.a
    public Cursor b() {
        return this.g0;
    }

    public abstract CharSequence c(Cursor cursor);

    public abstract void e(View view, Context context, Cursor cursor);

    public void f(Context context, Cursor cursor, int i) {
        if ((i & 1) == 1) {
            i |= 2;
            this.f0 = true;
        } else {
            this.f0 = false;
        }
        boolean z = cursor != null;
        this.g0 = cursor;
        this.a = z;
        this.h0 = context;
        this.i0 = z ? cursor.getColumnIndexOrThrow("_id") : -1;
        if ((i & 2) == 2) {
            this.j0 = new a();
            this.k0 = new b();
        } else {
            this.j0 = null;
            this.k0 = null;
        }
        if (z) {
            a aVar = this.j0;
            if (aVar != null) {
                cursor.registerContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.k0;
            if (dataSetObserver != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }
    }

    public abstract View g(Context context, Cursor cursor, ViewGroup viewGroup);

    @Override // android.widget.Adapter
    public int getCount() {
        Cursor cursor;
        if (!this.a || (cursor = this.g0) == null) {
            return 0;
        }
        return cursor.getCount();
    }

    @Override // android.widget.BaseAdapter, android.widget.SpinnerAdapter
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (this.a) {
            this.g0.moveToPosition(i);
            if (view == null) {
                view = g(this.h0, this.g0, viewGroup);
            }
            e(view, this.h0, this.g0);
            return view;
        }
        return null;
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        if (this.l0 == null) {
            this.l0 = new vb0(this);
        }
        return this.l0;
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        Cursor cursor;
        if (!this.a || (cursor = this.g0) == null) {
            return null;
        }
        cursor.moveToPosition(i);
        return this.g0;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        Cursor cursor;
        if (this.a && (cursor = this.g0) != null && cursor.moveToPosition(i)) {
            return this.g0.getLong(this.i0);
        }
        return 0L;
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (this.a) {
            if (this.g0.moveToPosition(i)) {
                if (view == null) {
                    view = h(this.h0, this.g0, viewGroup);
                }
                e(view, this.h0, this.g0);
                return view;
            }
            throw new IllegalStateException("couldn't move cursor to position " + i);
        }
        throw new IllegalStateException("this should only be called when the cursor is valid");
    }

    public abstract View h(Context context, Cursor cursor, ViewGroup viewGroup);

    public void i() {
        Cursor cursor;
        if (!this.f0 || (cursor = this.g0) == null || cursor.isClosed()) {
            return;
        }
        this.a = this.g0.requery();
    }

    public Cursor j(Cursor cursor) {
        Cursor cursor2 = this.g0;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            a aVar = this.j0;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.k0;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.g0 = cursor;
        if (cursor != null) {
            a aVar2 = this.j0;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.k0;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.i0 = cursor.getColumnIndexOrThrow("_id");
            this.a = true;
            notifyDataSetChanged();
        } else {
            this.i0 = -1;
            this.a = false;
            notifyDataSetInvalidated();
        }
        return cursor2;
    }
}
