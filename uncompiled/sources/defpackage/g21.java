package defpackage;

import defpackage.vb3;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FastSafeIterableMap.java */
/* renamed from: g21  reason: default package */
/* loaded from: classes.dex */
public class g21<K, V> extends vb3<K, V> {
    public HashMap<K, vb3.c<K, V>> i0 = new HashMap<>();

    public boolean contains(K k) {
        return this.i0.containsKey(k);
    }

    @Override // defpackage.vb3
    public vb3.c<K, V> i(K k) {
        return this.i0.get(k);
    }

    @Override // defpackage.vb3
    public V o(K k, V v) {
        vb3.c<K, V> i = i(k);
        if (i != null) {
            return i.f0;
        }
        this.i0.put(k, n(k, v));
        return null;
    }

    @Override // defpackage.vb3
    public V p(K k) {
        V v = (V) super.p(k);
        this.i0.remove(k);
        return v;
    }

    public Map.Entry<K, V> q(K k) {
        if (contains(k)) {
            return this.i0.get(k).h0;
        }
        return null;
    }
}
