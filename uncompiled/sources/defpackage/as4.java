package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: as4  reason: default package */
/* loaded from: classes2.dex */
public class as4 extends h {
    public final g a;
    public final int f0;
    public final int g0;
    public final va h0;

    public as4(int i, int i2, va vaVar) {
        this.a = new g(0L);
        this.f0 = i;
        this.g0 = i2;
        this.h0 = vaVar;
    }

    public as4(h4 h4Var) {
        this.a = g.z(h4Var.D(0));
        this.f0 = g.z(h4Var.D(1)).B().intValue();
        this.g0 = g.z(h4Var.D(2)).B().intValue();
        this.h0 = va.p(h4Var.D(3));
    }

    public static as4 p(Object obj) {
        if (obj instanceof as4) {
            return (as4) obj;
        }
        if (obj != null) {
            return new as4(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(this.a);
        d4Var.a(new g(this.f0));
        d4Var.a(new g(this.g0));
        d4Var.a(this.h0);
        return new n0(d4Var);
    }

    public int o() {
        return this.f0;
    }

    public int q() {
        return this.g0;
    }

    public va s() {
        return this.h0;
    }
}
