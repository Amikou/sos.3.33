package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: b06  reason: default package */
/* loaded from: classes.dex */
public final class b06 implements yp5<c06> {
    public static final b06 f0 = new b06();
    public final yp5<c06> a = gq5.a(gq5.b(new d06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final c06 zza() {
        return this.a.zza();
    }
}
