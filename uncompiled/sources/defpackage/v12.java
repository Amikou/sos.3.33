package defpackage;

/* compiled from: Logger.java */
/* renamed from: v12  reason: default package */
/* loaded from: classes.dex */
public abstract class v12 {
    public static v12 a = null;
    public static final int b = 20;

    /* compiled from: Logger.java */
    /* renamed from: v12$a */
    /* loaded from: classes.dex */
    public static class a extends v12 {
        public int c;

        public a(int i) {
            super(i);
            this.c = i;
        }

        @Override // defpackage.v12
        public void a(String str, String str2, Throwable... thArr) {
            if (this.c > 3 || thArr == null || thArr.length < 1) {
                return;
            }
            Throwable th = thArr[0];
        }

        @Override // defpackage.v12
        public void b(String str, String str2, Throwable... thArr) {
            if (this.c > 6 || thArr == null || thArr.length < 1) {
                return;
            }
            Throwable th = thArr[0];
        }

        @Override // defpackage.v12
        public void d(String str, String str2, Throwable... thArr) {
            if (this.c > 4 || thArr == null || thArr.length < 1) {
                return;
            }
            Throwable th = thArr[0];
        }

        @Override // defpackage.v12
        public void g(String str, String str2, Throwable... thArr) {
            if (this.c > 2 || thArr == null || thArr.length < 1) {
                return;
            }
            Throwable th = thArr[0];
        }

        @Override // defpackage.v12
        public void h(String str, String str2, Throwable... thArr) {
            if (this.c > 5 || thArr == null || thArr.length < 1) {
                return;
            }
            Throwable th = thArr[0];
        }
    }

    public v12(int i) {
    }

    public static synchronized v12 c() {
        v12 v12Var;
        synchronized (v12.class) {
            if (a == null) {
                a = new a(3);
            }
            v12Var = a;
        }
        return v12Var;
    }

    public static synchronized void e(v12 v12Var) {
        synchronized (v12.class) {
            a = v12Var;
        }
    }

    public static String f(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        int i = b;
        if (length >= i) {
            sb.append(str.substring(0, i));
        } else {
            sb.append(str);
        }
        return sb.toString();
    }

    public abstract void a(String str, String str2, Throwable... thArr);

    public abstract void b(String str, String str2, Throwable... thArr);

    public abstract void d(String str, String str2, Throwable... thArr);

    public abstract void g(String str, String str2, Throwable... thArr);

    public abstract void h(String str, String str2, Throwable... thArr);
}
