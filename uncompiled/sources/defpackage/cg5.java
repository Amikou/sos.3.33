package defpackage;

import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import okhttp3.HttpUrl;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: cg5  reason: default package */
/* loaded from: classes.dex */
public final class cg5 extends sl5 {
    public static final AtomicReference<String[]> c = new AtomicReference<>();
    public static final AtomicReference<String[]> d = new AtomicReference<>();
    public static final AtomicReference<String[]> e = new AtomicReference<>();

    public cg5(ck5 ck5Var) {
        super(ck5Var);
    }

    public static final String t(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        zt2.j(strArr);
        zt2.j(strArr2);
        zt2.j(atomicReference);
        zt2.a(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (sw5.G(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    str2 = strArr3[i];
                    if (str2 == null) {
                        str2 = strArr2[i] + "(" + strArr[i] + ")";
                        strArr3[i] = str2;
                    }
                }
                return str2;
            }
        }
        return str;
    }

    @Override // defpackage.sl5
    public final boolean f() {
        return false;
    }

    public final boolean l() {
        this.a.b();
        return this.a.K() && Log.isLoggable(this.a.w().z(), 3);
    }

    public final String n(String str) {
        if (str == null) {
            return null;
        }
        return !l() ? str : t(str, yl5.c, yl5.a, c);
    }

    public final String o(String str) {
        if (str == null) {
            return null;
        }
        return !l() ? str : t(str, am5.b, am5.a, d);
    }

    public final String p(String str) {
        if (str == null) {
            return null;
        }
        if (l()) {
            if (str.startsWith("_exp_")) {
                return "experiment_id(" + str + ")";
            }
            return t(str, bm5.b, bm5.a, e);
        }
        return str;
    }

    public final String r(Bundle bundle) {
        String valueOf;
        if (bundle == null) {
            return null;
        }
        if (!l()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Bundle[{");
        for (String str : bundle.keySet()) {
            if (sb.length() != 8) {
                sb.append(", ");
            }
            sb.append(o(str));
            sb.append("=");
            Object obj = bundle.get(str);
            if (obj instanceof Bundle) {
                valueOf = s(new Object[]{obj});
            } else if (obj instanceof Object[]) {
                valueOf = s((Object[]) obj);
            } else if (obj instanceof ArrayList) {
                valueOf = s(((ArrayList) obj).toArray());
            } else {
                valueOf = String.valueOf(obj);
            }
            sb.append(valueOf);
        }
        sb.append("}]");
        return sb.toString();
    }

    public final String s(Object[] objArr) {
        String valueOf;
        if (objArr == null) {
            return HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object obj : objArr) {
            if (obj instanceof Bundle) {
                valueOf = r((Bundle) obj);
            } else {
                valueOf = String.valueOf(obj);
            }
            if (valueOf != null) {
                if (sb.length() != 1) {
                    sb.append(", ");
                }
                sb.append(valueOf);
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
