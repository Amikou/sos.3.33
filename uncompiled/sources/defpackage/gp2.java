package defpackage;

import androidx.paging.LoadType;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: PagingSource.kt */
/* renamed from: gp2  reason: default package */
/* loaded from: classes.dex */
public abstract class gp2<Key, Value> {
    public final CopyOnWriteArrayList<rc1<te4>> a = new CopyOnWriteArrayList<>();
    public final AtomicBoolean b = new AtomicBoolean(false);

    /* compiled from: PagingSource.kt */
    /* renamed from: gp2$a */
    /* loaded from: classes.dex */
    public static abstract class a<Key> {
        public static final b c = new b(null);
        public final int a;
        public final boolean b;

        /* compiled from: PagingSource.kt */
        /* renamed from: gp2$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0178a<Key> extends a<Key> {
            public final Key d;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0178a(Key key, int i, boolean z) {
                super(i, z, null);
                fs1.f(key, "key");
                this.d = key;
            }

            @Override // defpackage.gp2.a
            public Key a() {
                return this.d;
            }
        }

        /* compiled from: PagingSource.kt */
        /* renamed from: gp2$a$b */
        /* loaded from: classes.dex */
        public static final class b {
            public b() {
            }

            public final <Key> a<Key> a(LoadType loadType, Key key, int i, boolean z) {
                fs1.f(loadType, "loadType");
                int i2 = hp2.a[loadType.ordinal()];
                if (i2 != 1) {
                    if (i2 == 2) {
                        if (key != null) {
                            return new c(key, i, z);
                        }
                        throw new IllegalArgumentException("key cannot be null for prepend".toString());
                    } else if (i2 == 3) {
                        if (key != null) {
                            return new C0178a(key, i, z);
                        }
                        throw new IllegalArgumentException("key cannot be null for append".toString());
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                }
                return new d(key, i, z);
            }

            public /* synthetic */ b(qi0 qi0Var) {
                this();
            }
        }

        /* compiled from: PagingSource.kt */
        /* renamed from: gp2$a$c */
        /* loaded from: classes.dex */
        public static final class c<Key> extends a<Key> {
            public final Key d;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(Key key, int i, boolean z) {
                super(i, z, null);
                fs1.f(key, "key");
                this.d = key;
            }

            @Override // defpackage.gp2.a
            public Key a() {
                return this.d;
            }
        }

        /* compiled from: PagingSource.kt */
        /* renamed from: gp2$a$d */
        /* loaded from: classes.dex */
        public static final class d<Key> extends a<Key> {
            public final Key d;

            public d(Key key, int i, boolean z) {
                super(i, z, null);
                this.d = key;
            }

            @Override // defpackage.gp2.a
            public Key a() {
                return this.d;
            }
        }

        public a(int i, boolean z) {
            this.a = i;
            this.b = z;
        }

        public abstract Key a();

        public final int b() {
            return this.a;
        }

        public final boolean c() {
            return this.b;
        }

        public /* synthetic */ a(int i, boolean z, qi0 qi0Var) {
            this(i, z);
        }
    }

    /* compiled from: PagingSource.kt */
    /* renamed from: gp2$b */
    /* loaded from: classes.dex */
    public static abstract class b<Key, Value> {

        /* compiled from: PagingSource.kt */
        /* renamed from: gp2$b$a */
        /* loaded from: classes.dex */
        public static final class a<Key, Value> extends b<Key, Value> {
            public final Throwable a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(Throwable th) {
                super(null);
                fs1.f(th, "throwable");
                this.a = th;
            }

            public final Throwable a() {
                return this.a;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof a) && fs1.b(this.a, ((a) obj).a);
                }
                return true;
            }

            public int hashCode() {
                Throwable th = this.a;
                if (th != null) {
                    return th.hashCode();
                }
                return 0;
            }

            public String toString() {
                return "Error(throwable=" + this.a + ")";
            }
        }

        public b() {
        }

        public /* synthetic */ b(qi0 qi0Var) {
            this();
        }

        /* compiled from: PagingSource.kt */
        /* renamed from: gp2$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0179b<Key, Value> extends b<Key, Value> {
            public final List<Value> a;
            public final Key b;
            public final Key c;
            public final int d;
            public final int e;

            /* compiled from: PagingSource.kt */
            /* renamed from: gp2$b$b$a */
            /* loaded from: classes.dex */
            public static final class a {
                public a() {
                }

                public /* synthetic */ a(qi0 qi0Var) {
                    this();
                }
            }

            static {
                new a(null);
                new C0179b(b20.g(), null, null, 0, 0);
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public C0179b(List<? extends Value> list, Key key, Key key2, int i, int i2) {
                super(null);
                fs1.f(list, "data");
                this.a = list;
                this.b = key;
                this.c = key2;
                this.d = i;
                this.e = i2;
                boolean z = false;
                if (!(i == Integer.MIN_VALUE || i >= 0)) {
                    throw new IllegalArgumentException("itemsBefore cannot be negative".toString());
                }
                if (!((i2 == Integer.MIN_VALUE || i2 >= 0) ? true : z)) {
                    throw new IllegalArgumentException("itemsAfter cannot be negative".toString());
                }
            }

            public final List<Value> a() {
                return this.a;
            }

            public final int b() {
                return this.e;
            }

            public final int c() {
                return this.d;
            }

            public final Key d() {
                return this.c;
            }

            public final Key e() {
                return this.b;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    if (obj instanceof C0179b) {
                        C0179b c0179b = (C0179b) obj;
                        return fs1.b(this.a, c0179b.a) && fs1.b(this.b, c0179b.b) && fs1.b(this.c, c0179b.c) && this.d == c0179b.d && this.e == c0179b.e;
                    }
                    return false;
                }
                return true;
            }

            public int hashCode() {
                List<Value> list = this.a;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                Key key = this.b;
                int hashCode2 = (hashCode + (key != null ? key.hashCode() : 0)) * 31;
                Key key2 = this.c;
                return ((((hashCode2 + (key2 != null ? key2.hashCode() : 0)) * 31) + this.d) * 31) + this.e;
            }

            public String toString() {
                return "Page(data=" + this.a + ", prevKey=" + this.b + ", nextKey=" + this.c + ", itemsBefore=" + this.d + ", itemsAfter=" + this.e + ")";
            }

            /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
            public C0179b(List<? extends Value> list, Key key, Key key2) {
                this(list, key, key2, Integer.MIN_VALUE, Integer.MIN_VALUE);
                fs1.f(list, "data");
            }
        }
    }

    public final boolean a() {
        return this.b.get();
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return false;
    }

    public abstract Key d(ip2<Key, Value> ip2Var);

    public final void e() {
        if (this.b.compareAndSet(false, true)) {
            Iterator<T> it = this.a.iterator();
            while (it.hasNext()) {
                ((rc1) it.next()).invoke();
            }
        }
    }

    public abstract Object f(a<Key> aVar, q70<? super b<Key, Value>> q70Var);

    public final void g(rc1<te4> rc1Var) {
        fs1.f(rc1Var, "onInvalidatedCallback");
        this.a.add(rc1Var);
    }

    public final void h(rc1<te4> rc1Var) {
        fs1.f(rc1Var, "onInvalidatedCallback");
        this.a.remove(rc1Var);
    }
}
