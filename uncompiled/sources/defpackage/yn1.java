package defpackage;

import java.io.UnsupportedEncodingException;

/* compiled from: ImageFormatCheckerUtils.java */
/* renamed from: yn1  reason: default package */
/* loaded from: classes.dex */
public class yn1 {
    public static byte[] a(String str) {
        xt2.g(str);
        try {
            return str.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("ASCII not found!", e);
        }
    }

    public static boolean b(byte[] bArr, byte[] bArr2, int i) {
        xt2.g(bArr);
        xt2.g(bArr2);
        if (bArr2.length + i > bArr.length) {
            return false;
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            if (bArr[i + i2] != bArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    public static boolean c(byte[] bArr, byte[] bArr2) {
        return b(bArr, bArr2, 0);
    }
}
