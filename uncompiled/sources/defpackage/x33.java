package defpackage;

import com.google.crypto.tink.shaded.protobuf.ProtoSyntax;
import com.google.crypto.tink.shaded.protobuf.e0;

/* compiled from: RawMessageInfo.java */
/* renamed from: x33  reason: default package */
/* loaded from: classes2.dex */
public final class x33 implements a82 {
    public final e0 a;
    public final String b;
    public final Object[] c;
    public final int d;

    public x33(e0 e0Var, String str, Object[] objArr) {
        this.a = e0Var;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.d = charAt;
            return;
        }
        int i = charAt & 8191;
        int i2 = 13;
        int i3 = 1;
        while (true) {
            int i4 = i3 + 1;
            char charAt2 = str.charAt(i3);
            if (charAt2 < 55296) {
                this.d = i | (charAt2 << i2);
                return;
            }
            i |= (charAt2 & 8191) << i2;
            i2 += 13;
            i3 = i4;
        }
    }

    @Override // defpackage.a82
    public boolean a() {
        return (this.d & 2) == 2;
    }

    @Override // defpackage.a82
    public e0 b() {
        return this.a;
    }

    @Override // defpackage.a82
    public ProtoSyntax c() {
        return (this.d & 1) == 1 ? ProtoSyntax.PROTO2 : ProtoSyntax.PROTO3;
    }

    public Object[] d() {
        return this.c;
    }

    public String e() {
        return this.b;
    }
}
