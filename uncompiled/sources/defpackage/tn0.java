package defpackage;

import android.view.View;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogNftNetworkInfomationBinding.java */
/* renamed from: tn0  reason: default package */
/* loaded from: classes2.dex */
public final class tn0 {
    public final MaterialButton a;

    public tn0(MaterialCardView materialCardView, MaterialCardView materialCardView2, MaterialButton materialButton, View view, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = materialButton;
    }

    public static tn0 a(View view) {
        MaterialCardView materialCardView = (MaterialCardView) view;
        int i = R.id.dialogCross;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialogCross);
        if (materialButton != null) {
            i = R.id.paddingView;
            View a = ai4.a(view, R.id.paddingView);
            if (a != null) {
                i = R.id.txtHeaderDescribe;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtHeaderDescribe);
                if (materialTextView != null) {
                    i = R.id.txtHeaderTitle;
                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtHeaderTitle);
                    if (materialTextView2 != null) {
                        return new tn0(materialCardView, materialCardView, materialButton, a, materialTextView, materialTextView2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
