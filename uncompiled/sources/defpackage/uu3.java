package defpackage;

import java.util.List;

/* compiled from: Strings.java */
/* renamed from: uu3  reason: default package */
/* loaded from: classes3.dex */
public class uu3 {
    private uu3() {
    }

    public static String capitaliseFirstLetter(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String join(List<String> list, String str) {
        if (list == null) {
            return null;
        }
        return m30.join(str, (CharSequence[]) list.toArray(new String[0]));
    }

    public static String lowercaseFirstLetter(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    public static String repeat(char c, int i) {
        return new String(new char[i]).replace("\u0000", String.valueOf(c));
    }

    public static String toCsv(List<String> list) {
        return join(list, ", ");
    }

    public static String zeros(int i) {
        return repeat('0', i);
    }
}
