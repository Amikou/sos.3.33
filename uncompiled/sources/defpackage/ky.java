package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.ky;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: ChooseNetworkAdapter.kt */
/* renamed from: ky  reason: default package */
/* loaded from: classes2.dex */
public final class ky extends RecyclerView.Adapter<b> implements Filterable {
    public final TokenType a;
    public final tc1<TokenType, te4> f0;
    public final List<TokenType> g0;
    public final List<TokenType> h0;

    /* compiled from: ChooseNetworkAdapter.kt */
    /* renamed from: ky$a */
    /* loaded from: classes2.dex */
    public final class a extends Filter {
        public final /* synthetic */ ky a;

        public a(ky kyVar) {
            fs1.f(kyVar, "this$0");
            this.a = kyVar;
        }

        @Override // android.widget.Filter
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String obj;
            String obj2;
            Filter.FilterResults filterResults = new Filter.FilterResults();
            filterResults.values = this.a.g0;
            if (charSequence != null && (obj = charSequence.toString()) != null && (obj2 = StringsKt__StringsKt.K0(obj).toString()) != null) {
                String lowerCase = obj2.toLowerCase(Locale.ROOT);
                fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                if (lowerCase != null) {
                    List list = this.a.g0;
                    ArrayList arrayList = new ArrayList();
                    for (Object obj3 : list) {
                        TokenType tokenType = (TokenType) obj3;
                        boolean z = true;
                        if (!(lowerCase.length() == 0)) {
                            String planeName = tokenType.getPlaneName();
                            Objects.requireNonNull(planeName, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase2 = planeName.toLowerCase(Locale.ROOT);
                            fs1.e(lowerCase2, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
                            if (!StringsKt__StringsKt.M(lowerCase2, lowerCase, false, 2, null)) {
                                z = false;
                            }
                        }
                        if (z) {
                            arrayList.add(obj3);
                        }
                    }
                    filterResults.values = arrayList;
                }
            }
            return filterResults;
        }

        @Override // android.widget.Filter
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.values == null) {
                return;
            }
            ky kyVar = this.a;
            kyVar.h0.clear();
            List list = kyVar.h0;
            Object obj = filterResults.values;
            Objects.requireNonNull(obj, "null cannot be cast to non-null type java.util.ArrayList<net.safemoon.androidwallet.common.TokenType>{ kotlin.collections.TypeAliasesKt.ArrayList<net.safemoon.androidwallet.common.TokenType> }");
            list.addAll((ArrayList) obj);
            kyVar.notifyDataSetChanged();
        }
    }

    /* compiled from: ChooseNetworkAdapter.kt */
    /* renamed from: ky$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final tk1 a;
        public final /* synthetic */ ky b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(ky kyVar, tk1 tk1Var) {
            super(tk1Var.b());
            fs1.f(kyVar, "this$0");
            fs1.f(tk1Var, "binding");
            this.b = kyVar;
            this.a = tk1Var;
        }

        public static final void c(ky kyVar, TokenType tokenType, View view) {
            fs1.f(kyVar, "this$0");
            fs1.f(tokenType, "$tt");
            kyVar.f0.invoke(tokenType);
        }

        public final void b(final TokenType tokenType) {
            fs1.f(tokenType, "_tokenType");
            final ky kyVar = this.b;
            tk1 tk1Var = this.a;
            tk1Var.d.setText(tokenType.getTitle());
            com.bumptech.glide.a.u(tk1Var.c).w(Integer.valueOf(tokenType.getIcon())).d0(200, 200).a(n73.v0()).I0(tk1Var.c);
            tk1Var.b().setOnClickListener(new View.OnClickListener() { // from class: ly
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ky.b.c(ky.this, tokenType, view);
                }
            });
            tk1Var.b.setChecked(kyVar.a.getChainId() == tokenType.getChainId());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public ky(TokenType tokenType, tc1<? super TokenType, te4> tc1Var) {
        fs1.f(tokenType, "tokenType");
        fs1.f(tc1Var, "callBack");
        this.a = tokenType;
        this.f0 = tc1Var;
        List<TokenType> a2 = b30.a.a();
        this.g0 = a2;
        ArrayList arrayList = new ArrayList();
        this.h0 = arrayList;
        arrayList.addAll(a2);
    }

    @Override // android.widget.Filterable
    /* renamed from: e */
    public a getFilter() {
        return new a(this);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.b(this.h0.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: g */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        tk1 a2 = tk1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_choose_network, viewGroup, false));
        fs1.e(a2, "bind(\n                La…ent, false)\n            )");
        return new b(this, a2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.h0.size();
    }
}
