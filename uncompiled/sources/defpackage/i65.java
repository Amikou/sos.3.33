package defpackage;

import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: i65  reason: default package */
/* loaded from: classes.dex */
public final class i65 implements z55 {
    @Override // defpackage.z55
    public final Double b() {
        return Double.valueOf(Double.NaN);
    }

    @Override // defpackage.z55
    public final Boolean c() {
        return Boolean.FALSE;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return obj instanceof i65;
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return null;
    }

    @Override // defpackage.z55
    public final z55 m() {
        return z55.X;
    }

    @Override // defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        throw new IllegalStateException(String.format("Undefined has no function %s", str));
    }

    @Override // defpackage.z55
    public final String zzc() {
        return "undefined";
    }
}
