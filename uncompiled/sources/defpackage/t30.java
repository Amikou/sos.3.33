package defpackage;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/* compiled from: CompletionState.kt */
/* renamed from: t30  reason: default package */
/* loaded from: classes2.dex */
public class t30 {
    public static final /* synthetic */ AtomicIntegerFieldUpdater b = AtomicIntegerFieldUpdater.newUpdater(t30.class, "_handled");
    private volatile /* synthetic */ int _handled;
    public final Throwable a;

    public t30(Throwable th, boolean z) {
        this.a = th;
        this._handled = z ? 1 : 0;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final boolean a() {
        return this._handled;
    }

    public final boolean b() {
        return b.compareAndSet(this, 0, 1);
    }

    public String toString() {
        return ff0.a(this) + '[' + this.a + ']';
    }

    public /* synthetic */ t30(Throwable th, boolean z, int i, qi0 qi0Var) {
        this(th, (i & 2) != 0 ? false : z);
    }
}
