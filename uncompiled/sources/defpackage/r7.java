package defpackage;

import android.annotation.SuppressLint;

/* compiled from: ActivityResultCallback.java */
/* renamed from: r7  reason: default package */
/* loaded from: classes.dex */
public interface r7<O> {
    void a(@SuppressLint({"UnknownNullness"}) O o);
}
