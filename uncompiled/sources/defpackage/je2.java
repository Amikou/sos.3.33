package defpackage;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: NavigationBarObserver.java */
/* renamed from: je2  reason: default package */
/* loaded from: classes.dex */
public final class je2 extends ContentObserver {
    public ArrayList<pm2> a;
    public Context b;
    public Context c;
    public Boolean d;

    /* compiled from: NavigationBarObserver.java */
    /* renamed from: je2$b */
    /* loaded from: classes.dex */
    public static class b {
        public static final je2 a = new je2();
    }

    public static je2 c() {
        return b.a;
    }

    public static boolean d() {
        return (dl2.g() || dl2.c()) && Build.VERSION.SDK_INT >= 17;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x004e A[ORIG_RETURN, RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static boolean e(android.content.Context r4) {
        /*
            android.content.Context r4 = r4.getApplicationContext()
            boolean r0 = d()
            r1 = 0
            if (r0 == 0) goto L4b
            if (r4 == 0) goto L4b
            android.content.ContentResolver r0 = r4.getContentResolver()
            if (r0 == 0) goto L4b
            boolean r0 = defpackage.dl2.g()
            if (r0 == 0) goto L24
            android.content.ContentResolver r4 = r4.getContentResolver()
            java.lang.String r0 = "force_fsg_nav_bar"
            int r4 = android.provider.Settings.Global.getInt(r4, r0, r1)
            goto L4c
        L24:
            boolean r0 = defpackage.dl2.c()
            if (r0 == 0) goto L4b
            boolean r0 = defpackage.dl2.f()
            java.lang.String r2 = "navigationbar_is_min"
            if (r0 != 0) goto L42
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 21
            if (r0 >= r3) goto L39
            goto L42
        L39:
            android.content.ContentResolver r4 = r4.getContentResolver()
            int r4 = android.provider.Settings.Global.getInt(r4, r2, r1)
            goto L4c
        L42:
            android.content.ContentResolver r4 = r4.getContentResolver()
            int r4 = android.provider.Settings.System.getInt(r4, r2, r1)
            goto L4c
        L4b:
            r4 = r1
        L4c:
            if (r4 == 0) goto L4f
            r1 = 1
        L4f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.je2.e(android.content.Context):boolean");
    }

    public void a(pm2 pm2Var) {
        if (pm2Var == null) {
            return;
        }
        if (this.a == null) {
            this.a = new ArrayList<>();
        }
        if (this.a.contains(pm2Var)) {
            return;
        }
        this.a.add(pm2Var);
    }

    public Context b() {
        return this.b;
    }

    public void f(Context context) {
        this.b = context;
        this.c = context.getApplicationContext();
        int i = Build.VERSION.SDK_INT;
        if (i < 17 || context.getContentResolver() == null || this.d.booleanValue()) {
            return;
        }
        Uri uri = null;
        if (dl2.g()) {
            uri = Settings.Global.getUriFor("force_fsg_nav_bar");
        } else if (dl2.c()) {
            if (!dl2.f() && i >= 21) {
                uri = Settings.Global.getUriFor("navigationbar_is_min");
            } else {
                uri = Settings.System.getUriFor("navigationbar_is_min");
            }
        }
        if (uri != null) {
            context.getContentResolver().registerContentObserver(uri, true, this);
            this.d = Boolean.TRUE;
        }
    }

    public void g(pm2 pm2Var) {
        ArrayList<pm2> arrayList;
        if (this.d.booleanValue()) {
            this.c.getContentResolver().unregisterContentObserver(this);
            this.d = Boolean.FALSE;
        }
        this.c = null;
        if (pm2Var == null || (arrayList = this.a) == null) {
            return;
        }
        arrayList.remove(pm2Var);
    }

    public void h() {
        this.c.getContentResolver().unregisterContentObserver(this);
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        super.onChange(z);
        ArrayList<pm2> arrayList = this.a;
        if (arrayList == null || arrayList.isEmpty()) {
            return;
        }
        boolean e = e(this.c);
        Iterator<pm2> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().a(e);
        }
    }

    public je2() {
        super(new Handler(Looper.getMainLooper()));
        this.d = Boolean.FALSE;
    }
}
