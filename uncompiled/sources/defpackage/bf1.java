package defpackage;

import java.math.BigInteger;

/* renamed from: bf1  reason: default package */
/* loaded from: classes2.dex */
public class bf1 implements qs2 {
    public final z41 a;
    public final ps2 b;

    public bf1(z41 z41Var, ps2 ps2Var) {
        this.a = z41Var;
        this.b = ps2Var;
    }

    @Override // defpackage.qs2
    public ps2 a() {
        return this.b;
    }

    @Override // defpackage.z41
    public int b() {
        return this.a.b() * this.b.b();
    }

    @Override // defpackage.z41
    public BigInteger c() {
        return this.a.c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof bf1) {
            bf1 bf1Var = (bf1) obj;
            return this.a.equals(bf1Var.a) && this.b.equals(bf1Var.b);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode() ^ yr1.a(this.b.hashCode(), 16);
    }
}
