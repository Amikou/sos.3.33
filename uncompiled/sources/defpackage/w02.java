package defpackage;

/* compiled from: LoadState.kt */
/* renamed from: w02  reason: default package */
/* loaded from: classes.dex */
public abstract class w02 {
    public final boolean a;

    /* compiled from: LoadState.kt */
    /* renamed from: w02$a */
    /* loaded from: classes.dex */
    public static final class a extends w02 {
        public final Throwable b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Throwable th) {
            super(false, null);
            fs1.f(th, "error");
            this.b = th;
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (a() == aVar.a() && fs1.b(this.b, aVar.b)) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return q80.a(a()) + this.b.hashCode();
        }

        public String toString() {
            return "Error(endOfPaginationReached=" + a() + ", error=" + this.b + ')';
        }
    }

    /* compiled from: LoadState.kt */
    /* renamed from: w02$b */
    /* loaded from: classes.dex */
    public static final class b extends w02 {
        public static final b b = new b();

        public b() {
            super(false, null);
        }

        public boolean equals(Object obj) {
            return (obj instanceof b) && a() == ((b) obj).a();
        }

        public int hashCode() {
            return q80.a(a());
        }

        public String toString() {
            return "Loading(endOfPaginationReached=" + a() + ')';
        }
    }

    /* compiled from: LoadState.kt */
    /* renamed from: w02$c */
    /* loaded from: classes.dex */
    public static final class c extends w02 {
        public static final a d = new a(null);
        public static final c b = new c(true);
        public static final c c = new c(false);

        /* compiled from: LoadState.kt */
        /* renamed from: w02$c$a */
        /* loaded from: classes.dex */
        public static final class a {
            public a() {
            }

            public final c a() {
                return c.b;
            }

            public final c b() {
                return c.c;
            }

            public /* synthetic */ a(qi0 qi0Var) {
                this();
            }
        }

        public c(boolean z) {
            super(z, null);
        }

        public boolean equals(Object obj) {
            return (obj instanceof c) && a() == ((c) obj).a();
        }

        public int hashCode() {
            return q80.a(a());
        }

        public String toString() {
            return "NotLoading(endOfPaginationReached=" + a() + ')';
        }
    }

    public w02(boolean z) {
        this.a = z;
    }

    public final boolean a() {
        return this.a;
    }

    public /* synthetic */ w02(boolean z, qi0 qi0Var) {
        this(z);
    }
}
