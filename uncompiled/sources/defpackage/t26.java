package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: t26  reason: default package */
/* loaded from: classes.dex */
public final class t26 implements s26 {
    public static final wo5<Boolean> a;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.validation.internal_limits_internal_event_params", false);
        ro5Var.a("measurement.id.validation.internal_limits_internal_event_params", 0L);
    }

    @Override // defpackage.s26
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
