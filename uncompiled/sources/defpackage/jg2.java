package defpackage;

import android.graphics.drawable.Drawable;
import com.google.android.material.textfield.TextInputLayout;

/* compiled from: NoEndIconDelegate.java */
/* renamed from: jg2  reason: default package */
/* loaded from: classes2.dex */
public class jg2 extends kv0 {
    public jg2(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @Override // defpackage.kv0
    public void a() {
        this.a.setEndIconOnClickListener(null);
        this.a.setEndIconDrawable((Drawable) null);
        this.a.setEndIconContentDescription((CharSequence) null);
    }
}
