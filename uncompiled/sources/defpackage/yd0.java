package defpackage;

import zendesk.support.request.CellBase;

/* compiled from: DashWrappingSegmentIndex.java */
/* renamed from: yd0  reason: default package */
/* loaded from: classes.dex */
public final class yd0 implements wd0 {
    public final py a;
    public final long b;

    public yd0(py pyVar, long j) {
        this.a = pyVar;
        this.b = j;
    }

    @Override // defpackage.wd0
    public long b(long j) {
        return this.a.e[(int) j] - this.b;
    }

    @Override // defpackage.wd0
    public long c(long j, long j2) {
        return this.a.d[(int) j];
    }

    @Override // defpackage.wd0
    public long d(long j, long j2) {
        return 0L;
    }

    @Override // defpackage.wd0
    public long e(long j, long j2) {
        return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.wd0
    public s33 f(long j) {
        py pyVar = this.a;
        int i = (int) j;
        return new s33(null, pyVar.c[i], pyVar.b[i]);
    }

    @Override // defpackage.wd0
    public long g(long j, long j2) {
        return this.a.a(j + this.b);
    }

    @Override // defpackage.wd0
    public boolean h() {
        return true;
    }

    @Override // defpackage.wd0
    public long i() {
        return 0L;
    }

    @Override // defpackage.wd0
    public long j(long j) {
        return this.a.a;
    }

    @Override // defpackage.wd0
    public long k(long j, long j2) {
        return this.a.a;
    }
}
