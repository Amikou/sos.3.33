package defpackage;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: ze  reason: default package */
/* loaded from: classes.dex */
public final class ze<O extends a.d> {
    public final int a;
    public final a<O> b;
    public final O c;

    public ze(a<O> aVar, O o) {
        this.b = aVar;
        this.c = o;
        this.a = pl2.b(aVar, o);
    }

    @RecentlyNonNull
    public static <O extends a.d> ze<O> b(@RecentlyNonNull a<O> aVar, O o) {
        return new ze<>(aVar, o);
    }

    @RecentlyNonNull
    public final String a() {
        return this.b.d();
    }

    @RecentlyNonNull
    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof ze) {
            ze zeVar = (ze) obj;
            return pl2.a(this.b, zeVar.b) && pl2.a(this.c, zeVar.c);
        }
        return false;
    }

    @RecentlyNonNull
    public final int hashCode() {
        return this.a;
    }
}
