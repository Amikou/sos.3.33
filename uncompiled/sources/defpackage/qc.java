package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.List;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: AnchorSelectWalletsAddress.kt */
/* renamed from: qc  reason: default package */
/* loaded from: classes2.dex */
public final class qc {
    public final List<Wallet> a;
    public PopupWindow b;

    public qc(List<Wallet> list) {
        fs1.f(list, "list");
        this.a = list;
    }

    public static /* synthetic */ PopupWindow c(qc qcVar, View view, View view2, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 4;
        }
        return qcVar.b(view, view2, i);
    }

    public static final void g(dn1 dn1Var, Wallet wallet2, View view) {
        fs1.f(dn1Var, "$onWalletItemClickListener");
        fs1.f(wallet2, "$wallet");
        dn1Var.a(wallet2);
    }

    public final PopupWindow b(View view, View view2, int i) {
        PopupWindow popupWindow = new PopupWindow(view, view2 == null ? -1 : view2.getWidth(), -2);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void d() {
        PopupWindow popupWindow;
        if (!e() || (popupWindow = this.b) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final boolean e() {
        PopupWindow popupWindow = this.b;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final qc f(Context context, View view, View view2, final dn1 dn1Var) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        fs1.f(dn1Var, "onWalletItemClickListener");
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        LayoutInflater layoutInflater = (LayoutInflater) systemService;
        View inflate = layoutInflater.inflate(R.layout.dialog_anchor_select_wallets_address, (ViewGroup) null);
        in0 a = in0.a(inflate);
        fs1.e(a, "bind(view)");
        a.a.removeAllViews();
        Wallet c = e30.c(context);
        try {
            for (final Wallet wallet2 : this.a) {
                if (!fs1.b(wallet2.getAddress(), c == null ? null : c.getAddress())) {
                    dt1 c2 = dt1.c(layoutInflater, a.a, false);
                    c2.c.setText(wallet2.displayName());
                    c2.b.setText(wallet2.getAddress());
                    a.a.addView(c2.b());
                    c2.b().setOnClickListener(new View.OnClickListener() { // from class: pc
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view3) {
                            qc.g(dn1.this, wallet2, view3);
                        }
                    });
                }
            }
        } catch (Exception unused) {
        }
        fs1.e(inflate, "view");
        PopupWindow c3 = c(this, inflate, view2, 0, 4, null);
        this.b = c3;
        if (c3 != null) {
            c3.showAsDropDown(view);
        }
        return this;
    }
}
