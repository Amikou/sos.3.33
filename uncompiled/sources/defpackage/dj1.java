package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import java.util.Arrays;
import java.util.Collections;
import zendesk.support.request.CellBase;

/* compiled from: H263Reader.java */
/* renamed from: dj1  reason: default package */
/* loaded from: classes.dex */
public final class dj1 implements ku0 {
    public static final float[] l = {1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 1.0f};
    public final xf4 a;
    public final op2 b;
    public final vc2 e;
    public b f;
    public long g;
    public String h;
    public f84 i;
    public boolean j;
    public final boolean[] c = new boolean[4];
    public final a d = new a(128);
    public long k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    /* compiled from: H263Reader.java */
    /* renamed from: dj1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final byte[] f = {0, 0, 1};
        public boolean a;
        public int b;
        public int c;
        public int d;
        public byte[] e;

        public a(int i) {
            this.e = new byte[i];
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.a) {
                int i3 = i2 - i;
                byte[] bArr2 = this.e;
                int length = bArr2.length;
                int i4 = this.c;
                if (length < i4 + i3) {
                    this.e = Arrays.copyOf(bArr2, (i4 + i3) * 2);
                }
                System.arraycopy(bArr, i, this.e, this.c, i3);
                this.c += i3;
            }
        }

        public boolean b(int i, int i2) {
            int i3 = this.b;
            if (i3 != 0) {
                if (i3 != 1) {
                    if (i3 != 2) {
                        if (i3 != 3) {
                            if (i3 != 4) {
                                throw new IllegalStateException();
                            }
                            if (i == 179 || i == 181) {
                                this.c -= i2;
                                this.a = false;
                                return true;
                            }
                        } else if ((i & 240) != 32) {
                            p12.i("H263Reader", "Unexpected start code value");
                            c();
                        } else {
                            this.d = this.c;
                            this.b = 4;
                        }
                    } else if (i > 31) {
                        p12.i("H263Reader", "Unexpected start code value");
                        c();
                    } else {
                        this.b = 3;
                    }
                } else if (i != 181) {
                    p12.i("H263Reader", "Unexpected start code value");
                    c();
                } else {
                    this.b = 2;
                }
            } else if (i == 176) {
                this.b = 1;
                this.a = true;
            }
            byte[] bArr = f;
            a(bArr, 0, bArr.length);
            return false;
        }

        public void c() {
            this.a = false;
            this.c = 0;
            this.b = 0;
        }
    }

    /* compiled from: H263Reader.java */
    /* renamed from: dj1$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final f84 a;
        public boolean b;
        public boolean c;
        public boolean d;
        public int e;
        public int f;
        public long g;
        public long h;

        public b(f84 f84Var) {
            this.a = f84Var;
        }

        public void a(byte[] bArr, int i, int i2) {
            if (this.c) {
                int i3 = this.f;
                int i4 = (i + 1) - i3;
                if (i4 < i2) {
                    this.d = ((bArr[i4] & 192) >> 6) == 0;
                    this.c = false;
                    return;
                }
                this.f = i3 + (i2 - i);
            }
        }

        public void b(long j, int i, boolean z) {
            if (this.e == 182 && z && this.b) {
                long j2 = this.h;
                if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                    this.a.b(j2, this.d ? 1 : 0, (int) (j - this.g), i, null);
                }
            }
            if (this.e != 179) {
                this.g = j;
            }
        }

        public void c(int i, long j) {
            this.e = i;
            this.d = false;
            this.b = i == 182 || i == 179;
            this.c = i == 182;
            this.f = 0;
            this.h = j;
        }

        public void d() {
            this.b = false;
            this.c = false;
            this.d = false;
            this.e = -1;
        }
    }

    public dj1(xf4 xf4Var) {
        this.a = xf4Var;
        if (xf4Var != null) {
            this.e = new vc2(178, 128);
            this.b = new op2();
            return;
        }
        this.e = null;
        this.b = null;
    }

    public static j b(a aVar, int i, String str) {
        byte[] copyOf = Arrays.copyOf(aVar.e, aVar.c);
        np2 np2Var = new np2(copyOf);
        np2Var.s(i);
        np2Var.s(4);
        np2Var.q();
        np2Var.r(8);
        if (np2Var.g()) {
            np2Var.r(4);
            np2Var.r(3);
        }
        int h = np2Var.h(4);
        float f = 1.0f;
        if (h == 15) {
            int h2 = np2Var.h(8);
            int h3 = np2Var.h(8);
            if (h3 == 0) {
                p12.i("H263Reader", "Invalid aspect ratio");
            } else {
                f = h2 / h3;
            }
        } else {
            float[] fArr = l;
            if (h < fArr.length) {
                f = fArr[h];
            } else {
                p12.i("H263Reader", "Invalid aspect ratio");
            }
        }
        if (np2Var.g()) {
            np2Var.r(2);
            np2Var.r(1);
            if (np2Var.g()) {
                np2Var.r(15);
                np2Var.q();
                np2Var.r(15);
                np2Var.q();
                np2Var.r(15);
                np2Var.q();
                np2Var.r(3);
                np2Var.r(11);
                np2Var.q();
                np2Var.r(15);
                np2Var.q();
            }
        }
        if (np2Var.h(2) != 0) {
            p12.i("H263Reader", "Unhandled video object layer shape");
        }
        np2Var.q();
        int h4 = np2Var.h(16);
        np2Var.q();
        if (np2Var.g()) {
            if (h4 == 0) {
                p12.i("H263Reader", "Invalid vop_increment_time_resolution");
            } else {
                int i2 = 0;
                for (int i3 = h4 - 1; i3 > 0; i3 >>= 1) {
                    i2++;
                }
                np2Var.r(i2);
            }
        }
        np2Var.q();
        int h5 = np2Var.h(13);
        np2Var.q();
        int h6 = np2Var.h(13);
        np2Var.q();
        np2Var.q();
        return new j.b().S(str).e0("video/mp4v-es").j0(h5).Q(h6).a0(f).T(Collections.singletonList(copyOf)).E();
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        ii.i(this.f);
        ii.i(this.i);
        int e = op2Var.e();
        int f = op2Var.f();
        byte[] d = op2Var.d();
        this.g += op2Var.a();
        this.i.a(op2Var, op2Var.a());
        while (true) {
            int c = wc2.c(d, e, f, this.c);
            if (c == f) {
                break;
            }
            int i = c + 3;
            int i2 = op2Var.d()[i] & 255;
            int i3 = c - e;
            int i4 = 0;
            if (!this.j) {
                if (i3 > 0) {
                    this.d.a(d, e, c);
                }
                if (this.d.b(i2, i3 < 0 ? -i3 : 0)) {
                    f84 f84Var = this.i;
                    a aVar = this.d;
                    f84Var.f(b(aVar, aVar.d, (String) ii.e(this.h)));
                    this.j = true;
                }
            }
            this.f.a(d, e, c);
            vc2 vc2Var = this.e;
            if (vc2Var != null) {
                if (i3 > 0) {
                    vc2Var.a(d, e, c);
                } else {
                    i4 = -i3;
                }
                if (this.e.b(i4)) {
                    vc2 vc2Var2 = this.e;
                    ((op2) androidx.media3.common.util.b.j(this.b)).N(this.e.d, wc2.q(vc2Var2.d, vc2Var2.e));
                    ((xf4) androidx.media3.common.util.b.j(this.a)).a(this.k, this.b);
                }
                if (i2 == 178 && op2Var.d()[c + 2] == 1) {
                    this.e.e(i2);
                }
            }
            int i5 = f - c;
            this.f.b(this.g - i5, i5, this.j);
            this.f.c(i2, this.k);
            e = i;
        }
        if (!this.j) {
            this.d.a(d, e, f);
        }
        this.f.a(d, e, f);
        vc2 vc2Var3 = this.e;
        if (vc2Var3 != null) {
            vc2Var3.a(d, e, f);
        }
    }

    @Override // defpackage.ku0
    public void c() {
        wc2.a(this.c);
        this.d.c();
        b bVar = this.f;
        if (bVar != null) {
            bVar.d();
        }
        vc2 vc2Var = this.e;
        if (vc2Var != null) {
            vc2Var.d();
        }
        this.g = 0L;
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.k = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.h = dVar.b();
        f84 f = r11Var.f(dVar.c(), 2);
        this.i = f;
        this.f = new b(f);
        xf4 xf4Var = this.a;
        if (xf4Var != null) {
            xf4Var.b(r11Var, dVar);
        }
    }
}
