package defpackage;

import android.content.Context;
import android.net.Uri;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ro5  reason: default package */
/* loaded from: classes.dex */
public final class ro5 {
    public final Uri b;
    public final String a = null;
    public final String c = "";
    public final String d = "";
    public final boolean e = false;
    public final boolean f = false;
    public final boolean g = false;
    public final boolean h = false;
    public final jp5<Context, Boolean> i = null;

    public ro5(Uri uri) {
        this.b = uri;
    }

    public final wo5<Long> a(String str, long j) {
        return new io5(this, str, Long.valueOf(j), true);
    }

    public final wo5<Boolean> b(String str, boolean z) {
        return new ko5(this, str, Boolean.valueOf(z), true);
    }

    public final wo5<Double> c(String str, double d) {
        return new mo5(this, "measurement.test.double_flag", Double.valueOf(-3.0d), true);
    }

    public final wo5<String> d(String str, String str2) {
        return new oo5(this, str, str2, true);
    }
}
