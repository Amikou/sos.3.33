package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: ey  reason: default package */
/* loaded from: classes2.dex */
public final class ey extends tt1 {
    public final pv<?> i0;

    public ey(pv<?> pvVar) {
        this.i0 = pvVar;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        y(th);
        return te4.a;
    }

    @Override // defpackage.v30
    public void y(Throwable th) {
        pv<?> pvVar = this.i0;
        pvVar.H(pvVar.w(z()));
    }
}
