package defpackage;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: tn5  reason: default package */
/* loaded from: classes.dex */
public final class tn5 {
    public static final Class<?> a = a("libcore.io.Memory");
    public static final boolean b;

    static {
        b = a("org.robolectric.Robolectric") != null;
    }

    public static <T> Class<T> a(String str) {
        try {
            return (Class<T>) Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static boolean b() {
        return (a == null || b) ? false : true;
    }

    public static Class<?> c() {
        return a;
    }
}
