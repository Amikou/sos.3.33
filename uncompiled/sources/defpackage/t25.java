package defpackage;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: t25 */
/* loaded from: classes.dex */
public final class t25 implements v05 {
    public final Context a;
    public final d05 b;
    public final i05 c;
    public final i05 d;
    public final Map<a.c<?>, i05> e;
    public final a.f g;
    public Bundle h;
    public final Lock l;
    public final Set<to3> f = Collections.newSetFromMap(new WeakHashMap());
    public ConnectionResult i = null;
    public ConnectionResult j = null;
    public boolean k = false;
    public int m = 0;

    public t25(Context context, d05 d05Var, Lock lock, Looper looper, eh1 eh1Var, Map<a.c<?>, a.f> map, Map<a.c<?>, a.f> map2, kz kzVar, a.AbstractC0106a<? extends p15, uo3> abstractC0106a, a.f fVar, ArrayList<r25> arrayList, ArrayList<r25> arrayList2, Map<a<?>, Boolean> map3, Map<a<?>, Boolean> map4) {
        this.a = context;
        this.b = d05Var;
        this.l = lock;
        this.g = fVar;
        this.c = new i05(context, d05Var, lock, looper, eh1Var, map2, null, map4, null, arrayList2, new v25(this, null));
        this.d = new i05(context, d05Var, lock, looper, eh1Var, map, kzVar, map3, abstractC0106a, arrayList, new y25(this, null));
        rh rhVar = new rh();
        for (a.c<?> cVar : map2.keySet()) {
            rhVar.put(cVar, this.c);
        }
        for (a.c<?> cVar2 : map.keySet()) {
            rhVar.put(cVar2, this.d);
        }
        this.e = Collections.unmodifiableMap(rhVar);
    }

    public static t25 i(Context context, d05 d05Var, Lock lock, Looper looper, eh1 eh1Var, Map<a.c<?>, a.f> map, kz kzVar, Map<a<?>, Boolean> map2, a.AbstractC0106a<? extends p15, uo3> abstractC0106a, ArrayList<r25> arrayList) {
        rh rhVar = new rh();
        rh rhVar2 = new rh();
        a.f fVar = null;
        for (Map.Entry<a.c<?>, a.f> entry : map.entrySet()) {
            a.f value = entry.getValue();
            if (value.e()) {
                fVar = value;
            }
            if (value.u()) {
                rhVar.put(entry.getKey(), value);
            } else {
                rhVar2.put(entry.getKey(), value);
            }
        }
        zt2.n(!rhVar.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        rh rhVar3 = new rh();
        rh rhVar4 = new rh();
        for (a<?> aVar : map2.keySet()) {
            a.c<?> c = aVar.c();
            if (rhVar.containsKey(c)) {
                rhVar3.put(aVar, map2.get(aVar));
            } else if (rhVar2.containsKey(c)) {
                rhVar4.put(aVar, map2.get(aVar));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            r25 r25Var = arrayList.get(i);
            i++;
            r25 r25Var2 = r25Var;
            if (rhVar3.containsKey(r25Var2.a)) {
                arrayList2.add(r25Var2);
            } else if (rhVar4.containsKey(r25Var2.a)) {
                arrayList3.add(r25Var2);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new t25(context, d05Var, lock, looper, eh1Var, rhVar, rhVar2, kzVar, abstractC0106a, fVar, arrayList2, arrayList3, rhVar3, rhVar4);
    }

    public static boolean r(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.M1();
    }

    public final PendingIntent A() {
        if (this.g == null) {
            return null;
        }
        return PendingIntent.getActivity(this.a, System.identityHashCode(this.b), this.g.t(), 134217728);
    }

    @Override // defpackage.v05
    public final void a() {
        this.m = 2;
        this.k = false;
        this.j = null;
        this.i = null;
        this.c.a();
        this.d.a();
    }

    @Override // defpackage.v05
    public final void c() {
        this.j = null;
        this.i = null;
        this.m = 0;
        this.c.c();
        this.d.c();
        y();
    }

    @Override // defpackage.v05
    public final void d() {
        this.c.d();
        this.d.d();
    }

    /* JADX WARN: Code restructure failed: missing block: B:30:0x001e, code lost:
        if (r2.m == 1) goto L12;
     */
    @Override // defpackage.v05
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final boolean e() {
        /*
            r2 = this;
            java.util.concurrent.locks.Lock r0 = r2.l
            r0.lock()
            i05 r0 = r2.c     // Catch: java.lang.Throwable -> L28
            boolean r0 = r0.e()     // Catch: java.lang.Throwable -> L28
            r1 = 1
            if (r0 == 0) goto L21
            i05 r0 = r2.d     // Catch: java.lang.Throwable -> L28
            boolean r0 = r0.e()     // Catch: java.lang.Throwable -> L28
            if (r0 != 0) goto L22
            boolean r0 = r2.z()     // Catch: java.lang.Throwable -> L28
            if (r0 != 0) goto L22
            int r0 = r2.m     // Catch: java.lang.Throwable -> L28
            if (r0 != r1) goto L21
            goto L22
        L21:
            r1 = 0
        L22:
            java.util.concurrent.locks.Lock r0 = r2.l
            r0.unlock()
            return r1
        L28:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r2.l
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.t25.e():boolean");
    }

    @Override // defpackage.v05
    public final void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("authClient").println(":");
        this.d.f(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append("anonClient").println(":");
        this.c.f(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }

    @Override // defpackage.v05
    public final <A extends a.b, T extends b<? extends l83, A>> T g(T t) {
        if (s(t)) {
            if (z()) {
                t.w(new Status(4, (String) null, A()));
                return t;
            }
            return (T) this.d.g(t);
        }
        return (T) this.c.g(t);
    }

    public final void j(int i, boolean z) {
        this.b.b(i, z);
        this.j = null;
        this.i = null;
    }

    public final void k(Bundle bundle) {
        Bundle bundle2 = this.h;
        if (bundle2 == null) {
            this.h = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    public final void l(ConnectionResult connectionResult) {
        int i = this.m;
        if (i != 1) {
            if (i != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.m = 0;
            }
            this.b.a(connectionResult);
        }
        y();
        this.m = 0;
    }

    public final boolean s(b<? extends l83, ? extends a.b> bVar) {
        i05 i05Var = this.e.get(bVar.s());
        zt2.k(i05Var, "GoogleApiClient is not configured to use the API required for this call.");
        return i05Var.equals(this.d);
    }

    public final void x() {
        ConnectionResult connectionResult;
        if (r(this.i)) {
            if (!r(this.j) && !z()) {
                ConnectionResult connectionResult2 = this.j;
                if (connectionResult2 != null) {
                    if (this.m == 1) {
                        y();
                        return;
                    }
                    l(connectionResult2);
                    this.c.c();
                    return;
                }
                return;
            }
            int i = this.m;
            if (i != 1) {
                if (i != 2) {
                    Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                    this.m = 0;
                }
                ((d05) zt2.j(this.b)).d(this.h);
            }
            y();
            this.m = 0;
        } else if (this.i != null && r(this.j)) {
            this.d.c();
            l((ConnectionResult) zt2.j(this.i));
        } else {
            ConnectionResult connectionResult3 = this.i;
            if (connectionResult3 == null || (connectionResult = this.j) == null) {
                return;
            }
            if (this.d.l < this.c.l) {
                connectionResult3 = connectionResult;
            }
            l(connectionResult3);
        }
    }

    public final void y() {
        for (to3 to3Var : this.f) {
            to3Var.a();
        }
        this.f.clear();
    }

    public final boolean z() {
        ConnectionResult connectionResult = this.j;
        return connectionResult != null && connectionResult.I1() == 4;
    }
}
