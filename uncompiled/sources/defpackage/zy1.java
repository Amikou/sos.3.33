package defpackage;

import kotlin.LazyThreadSafetyMode;
import kotlin.NoWhenBranchMatchedException;
import kotlin.SafePublicationLazyImpl;
import kotlin.SynchronizedLazyImpl;
import kotlin.UnsafeLazyImpl;

/* compiled from: LazyJVM.kt */
/* renamed from: zy1  reason: default package */
/* loaded from: classes2.dex */
public class zy1 {
    public static final <T> sy1<T> a(rc1<? extends T> rc1Var) {
        fs1.f(rc1Var, "initializer");
        return new SynchronizedLazyImpl(rc1Var, null, 2, null);
    }

    public static final <T> sy1<T> b(LazyThreadSafetyMode lazyThreadSafetyMode, rc1<? extends T> rc1Var) {
        fs1.f(lazyThreadSafetyMode, "mode");
        fs1.f(rc1Var, "initializer");
        int i = yy1.a[lazyThreadSafetyMode.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return new UnsafeLazyImpl(rc1Var);
                }
                throw new NoWhenBranchMatchedException();
            }
            return new SafePublicationLazyImpl(rc1Var);
        }
        return new SynchronizedLazyImpl(rc1Var, null, 2, null);
    }
}
