package defpackage;

/* renamed from: v41  reason: default package */
/* loaded from: classes2.dex */
public class v41 {
    public static char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    public final byte[] a;

    public v41(byte[] bArr) {
        this.a = a(bArr);
    }

    public static byte[] a(byte[] bArr) {
        oa3 oa3Var = new oa3(160);
        oa3Var.b(bArr, 0, bArr.length);
        byte[] bArr2 = new byte[oa3Var.h()];
        oa3Var.a(bArr2, 0);
        return bArr2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof v41) {
            return wh.a(((v41) obj).a, this.a);
        }
        return false;
    }

    public int hashCode() {
        return wh.r(this.a);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i != this.a.length; i++) {
            if (i > 0) {
                stringBuffer.append(":");
            }
            stringBuffer.append(b[(this.a[i] >>> 4) & 15]);
            stringBuffer.append(b[this.a[i] & 15]);
        }
        return stringBuffer.toString();
    }
}
