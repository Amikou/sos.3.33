package defpackage;

import android.graphics.Bitmap;

/* compiled from: SimpleBitmapReleaser.java */
/* renamed from: yo3  reason: default package */
/* loaded from: classes.dex */
public class yo3 implements d83<Bitmap> {
    public static yo3 a;

    public static yo3 b() {
        if (a == null) {
            a = new yo3();
        }
        return a;
    }

    @Override // defpackage.d83
    /* renamed from: c */
    public void a(Bitmap bitmap) {
        bitmap.recycle();
    }
}
