package defpackage;

import java.lang.Comparable;

/* compiled from: Ranges.kt */
/* renamed from: e00  reason: default package */
/* loaded from: classes2.dex */
public interface e00<T extends Comparable<? super T>> extends f00<T> {
    boolean e(T t, T t2);

    @Override // defpackage.f00
    boolean isEmpty();
}
