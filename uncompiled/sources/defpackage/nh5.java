package defpackage;

import android.content.SharedPreferences;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: nh5  reason: default package */
/* loaded from: classes.dex */
public final class nh5 {
    public final String a;
    public final boolean b;
    public boolean c;
    public boolean d;
    public final /* synthetic */ mi5 e;

    public nh5(mi5 mi5Var, String str, boolean z) {
        this.e = mi5Var;
        zt2.f(str);
        this.a = str;
        this.b = z;
    }

    public final boolean a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.n().getBoolean(this.a, this.b);
        }
        return this.d;
    }

    public final void b(boolean z) {
        SharedPreferences.Editor edit = this.e.n().edit();
        edit.putBoolean(this.a, z);
        edit.apply();
        this.d = z;
    }
}
