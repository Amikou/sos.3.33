package defpackage;

import android.accounts.Account;
import android.view.View;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: kz  reason: default package */
/* loaded from: classes.dex */
public final class kz {
    public final Account a;
    public final Set<Scope> b;
    public final Set<Scope> c;
    public final Map<com.google.android.gms.common.api.a<?>, b> d;
    public final String e;
    public final String f;
    public final uo3 g;
    public Integer h;

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: kz$a */
    /* loaded from: classes.dex */
    public static final class a {
        public Account a;
        public uh<Scope> b;
        public String c;
        public String d;
        public uo3 e = uo3.a;

        @RecentlyNonNull
        public final kz a() {
            return new kz(this.a, this.b, null, 0, null, this.c, this.d, this.e, false);
        }

        @RecentlyNonNull
        public final a b(@RecentlyNonNull String str) {
            this.c = str;
            return this;
        }

        @RecentlyNonNull
        public final a c(Account account) {
            this.a = account;
            return this;
        }

        @RecentlyNonNull
        public final a d(@RecentlyNonNull String str) {
            this.d = str;
            return this;
        }

        @RecentlyNonNull
        public final a e(@RecentlyNonNull Collection<Scope> collection) {
            if (this.b == null) {
                this.b = new uh<>();
            }
            this.b.addAll(collection);
            return this;
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
    /* renamed from: kz$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final Set<Scope> a;
    }

    public kz(Account account, @RecentlyNonNull Set<Scope> set, @RecentlyNonNull Map<com.google.android.gms.common.api.a<?>, b> map, @RecentlyNonNull int i, @RecentlyNonNull View view, @RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull uo3 uo3Var, @RecentlyNonNull boolean z) {
        this.a = account;
        Set<Scope> emptySet = set == null ? Collections.emptySet() : Collections.unmodifiableSet(set);
        this.b = emptySet;
        map = map == null ? Collections.emptyMap() : map;
        this.d = map;
        this.e = str;
        this.f = str2;
        this.g = uo3Var;
        HashSet hashSet = new HashSet(emptySet);
        for (b bVar : map.values()) {
            hashSet.addAll(bVar.a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    @RecentlyNullable
    public final Account a() {
        return this.a;
    }

    @RecentlyNonNull
    public final Account b() {
        Account account = this.a;
        return account != null ? account : new Account("<<default account>>", "com.google");
    }

    @RecentlyNonNull
    public final Set<Scope> c() {
        return this.c;
    }

    @RecentlyNullable
    public final String d() {
        return this.e;
    }

    @RecentlyNonNull
    public final Set<Scope> e() {
        return this.b;
    }

    @RecentlyNonNull
    public final Map<com.google.android.gms.common.api.a<?>, b> f() {
        return this.d;
    }

    public final void g(@RecentlyNonNull Integer num) {
        this.h = num;
    }

    @RecentlyNullable
    public final String h() {
        return this.f;
    }

    @RecentlyNonNull
    public final uo3 i() {
        return this.g;
    }

    @RecentlyNullable
    public final Integer j() {
        return this.h;
    }
}
