package defpackage;

import android.content.Context;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: j90  reason: default package */
/* loaded from: classes.dex */
public final class j90 {
    @RecentlyNonNull
    public static boolean a(@RecentlyNonNull Context context, @RecentlyNonNull Throwable th) {
        return b(context, th, 536870912);
    }

    public static boolean b(Context context, Throwable th, int i) {
        try {
            zt2.j(context);
            zt2.j(th);
        } catch (Exception unused) {
        }
        return false;
    }
}
