package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.measurement.internal.zzaq;
import com.google.android.gms.measurement.internal.zzas;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: g65  reason: default package */
/* loaded from: classes.dex */
public final class g65 implements Parcelable.Creator<zzas> {
    public static void a(zzas zzasVar, Parcel parcel, int i) {
        int a = yb3.a(parcel);
        yb3.s(parcel, 2, zzasVar.a, false);
        yb3.r(parcel, 3, zzasVar.f0, i, false);
        yb3.s(parcel, 4, zzasVar.g0, false);
        yb3.o(parcel, 5, zzasVar.h0);
        yb3.b(parcel, a);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzas createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        String str = null;
        zzaq zzaqVar = null;
        String str2 = null;
        long j = 0;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 2) {
                str = SafeParcelReader.p(parcel, C);
            } else if (v == 3) {
                zzaqVar = (zzaq) SafeParcelReader.o(parcel, C, zzaq.CREATOR);
            } else if (v == 4) {
                str2 = SafeParcelReader.p(parcel, C);
            } else if (v != 5) {
                SafeParcelReader.I(parcel, C);
            } else {
                j = SafeParcelReader.F(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzas(str, zzaqVar, str2, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ zzas[] newArray(int i) {
        return new zzas[i];
    }
}
