package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzab;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: d45  reason: default package */
/* loaded from: classes.dex */
public final class d45 implements Parcelable.Creator<zzab> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzab createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        float f = 0.0f;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 2) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 3) {
                i2 = SafeParcelReader.E(parcel, C);
            } else if (v == 4) {
                i3 = SafeParcelReader.E(parcel, C);
            } else if (v == 5) {
                i4 = SafeParcelReader.E(parcel, C);
            } else if (v != 6) {
                SafeParcelReader.I(parcel, C);
            } else {
                f = SafeParcelReader.A(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzab(i, i2, i3, i4, f);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzab[] newArray(int i) {
        return new zzab[i];
    }
}
