package defpackage;

import java.security.InvalidKeyException;
import java.util.Arrays;

/* compiled from: XChaCha20.java */
/* renamed from: wr4  reason: default package */
/* loaded from: classes2.dex */
public class wr4 extends yw {
    public wr4(byte[] bArr, int i) throws InvalidKeyException {
        super(bArr, i);
    }

    public static int[] n(int[] iArr, int[] iArr2) {
        yw.k(r0, iArr);
        int[] iArr3 = {0, 0, 0, 0, iArr3[12], iArr3[13], iArr3[14], iArr3[15], 0, 0, 0, 0, iArr2[0], iArr2[1], iArr2[2], iArr2[3]};
        yw.l(iArr3);
        return Arrays.copyOf(iArr3, 8);
    }

    @Override // defpackage.yw
    public int[] d(int[] iArr, int i) {
        if (iArr.length == g() / 4) {
            int[] iArr2 = new int[16];
            yw.k(iArr2, n(this.a, iArr));
            iArr2[12] = i;
            iArr2[13] = 0;
            iArr2[14] = iArr[4];
            iArr2[15] = iArr[5];
            return iArr2;
        }
        throw new IllegalArgumentException(String.format("XChaCha20 uses 192-bit nonces, but got a %d-bit nonce", Integer.valueOf(iArr.length * 32)));
    }

    @Override // defpackage.yw
    public int g() {
        return 24;
    }
}
