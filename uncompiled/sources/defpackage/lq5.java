package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: lq5  reason: default package */
/* loaded from: classes.dex */
public final class lq5 implements Runnable {
    public final /* synthetic */ bq5 a;
    public final /* synthetic */ long f0;
    public final /* synthetic */ qq5 g0;

    public lq5(qq5 qq5Var, bq5 bq5Var, long j) {
        this.g0 = qq5Var;
        this.a = bq5Var;
        this.f0 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.g0.o(this.a, false, this.f0);
        qq5 qq5Var = this.g0;
        qq5Var.e = null;
        qq5Var.a.R().W(null);
    }
}
