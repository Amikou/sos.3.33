package defpackage;

import com.google.android.gms.internal.vision.i0;
import com.google.android.gms.internal.vision.j0;
import com.google.protobuf.t;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: dr5  reason: default package */
/* loaded from: classes.dex */
public final class dr5 {
    public static final j0<?> a = new i0();
    public static final j0<?> b = c();

    public static j0<?> a() {
        return a;
    }

    public static j0<?> b() {
        j0<?> j0Var = b;
        if (j0Var != null) {
            return j0Var;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    public static j0<?> c() {
        try {
            int i = t.b;
            return (j0) t.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
