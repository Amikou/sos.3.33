package defpackage;

import defpackage.k6;

/* compiled from: AppCompatCallback.java */
/* renamed from: cf  reason: default package */
/* loaded from: classes.dex */
public interface cf {
    void onSupportActionModeFinished(k6 k6Var);

    void onSupportActionModeStarted(k6 k6Var);

    k6 onWindowStartingSupportActionMode(k6.a aVar);
}
