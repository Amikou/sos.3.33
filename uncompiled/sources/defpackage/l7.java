package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityLoginBinding.java */
/* renamed from: l7  reason: default package */
/* loaded from: classes2.dex */
public final class l7 {
    public final ConstraintLayout a;
    public final AppCompatImageView b;
    public final MaterialButton c;
    public final LinearLayout d;
    public final SwitchMaterial e;
    public final TextInputLayout f;
    public final TextInputLayout g;
    public final sp3 h;
    public final TextView i;
    public final TextView j;
    public final TextView k;

    public l7(ConstraintLayout constraintLayout, AppCompatImageView appCompatImageView, MaterialButton materialButton, LinearLayout linearLayout, bq1 bq1Var, SwitchMaterial switchMaterial, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, sp3 sp3Var, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
        this.a = constraintLayout;
        this.b = appCompatImageView;
        this.c = materialButton;
        this.d = linearLayout;
        this.e = switchMaterial;
        this.f = textInputLayout;
        this.g = textInputLayout2;
        this.h = sp3Var;
        this.i = textView;
        this.j = textView3;
        this.k = textView4;
    }

    public static l7 a(View view) {
        int i = R.id.btnBiometrics;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnBiometrics);
        if (appCompatImageView != null) {
            i = R.id.btnLogin;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnLogin);
            if (materialButton != null) {
                i = R.id.content_layout;
                LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.content_layout);
                if (linearLayout != null) {
                    i = R.id.layoutSafeMoonBrand;
                    View a = ai4.a(view, R.id.layoutSafeMoonBrand);
                    if (a != null) {
                        bq1 a2 = bq1.a(a);
                        i = R.id.switchTwoLayerSignInBio;
                        SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchTwoLayerSignInBio);
                        if (switchMaterial != null) {
                            i = R.id.tilPassWord;
                            TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilPassWord);
                            if (textInputLayout != null) {
                                i = R.id.tilUserName;
                                TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.tilUserName);
                                if (textInputLayout2 != null) {
                                    i = R.id.toolbar;
                                    View a3 = ai4.a(view, R.id.toolbar);
                                    if (a3 != null) {
                                        sp3 a4 = sp3.a(a3);
                                        i = R.id.tvForgotPassword;
                                        TextView textView = (TextView) ai4.a(view, R.id.tvForgotPassword);
                                        if (textView != null) {
                                            i = R.id.tvSignInBioText;
                                            TextView textView2 = (TextView) ai4.a(view, R.id.tvSignInBioText);
                                            if (textView2 != null) {
                                                i = R.id.tvWipeData;
                                                TextView textView3 = (TextView) ai4.a(view, R.id.tvWipeData);
                                                if (textView3 != null) {
                                                    i = R.id.tv_wipe_notice;
                                                    TextView textView4 = (TextView) ai4.a(view, R.id.tv_wipe_notice);
                                                    if (textView4 != null) {
                                                        return new l7((ConstraintLayout) view, appCompatImageView, materialButton, linearLayout, a2, switchMaterial, textInputLayout, textInputLayout2, a4, textView, textView2, textView3, textView4);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static l7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static l7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_login, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
