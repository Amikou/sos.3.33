package defpackage;

import java.io.Closeable;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
@Deprecated
/* renamed from: km1  reason: default package */
/* loaded from: classes.dex */
public final class km1 {
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }
}
