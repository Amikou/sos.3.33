package defpackage;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;

/* compiled from: FloatingActionButtonImplLollipop.java */
/* renamed from: f71  reason: default package */
/* loaded from: classes2.dex */
public class f71 extends com.google.android.material.floatingactionbutton.a {

    /* compiled from: FloatingActionButtonImplLollipop.java */
    /* renamed from: f71$a */
    /* loaded from: classes2.dex */
    public static class a extends o42 {
        public a(pn3 pn3Var) {
            super(pn3Var);
        }

        @Override // defpackage.o42, android.graphics.drawable.Drawable
        public boolean isStateful() {
            return true;
        }
    }

    public f71(FloatingActionButton floatingActionButton, on3 on3Var) {
        super(floatingActionButton, on3Var);
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void A() {
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void C() {
        f0();
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void E(int[] iArr) {
        if (Build.VERSION.SDK_INT == 21) {
            if (this.y.isEnabled()) {
                this.y.setElevation(this.h);
                if (this.y.isPressed()) {
                    this.y.setTranslationZ(this.j);
                    return;
                } else if (!this.y.isFocused() && !this.y.isHovered()) {
                    this.y.setTranslationZ(Utils.FLOAT_EPSILON);
                    return;
                } else {
                    this.y.setTranslationZ(this.i);
                    return;
                }
            }
            this.y.setElevation(Utils.FLOAT_EPSILON);
            this.y.setTranslationZ(Utils.FLOAT_EPSILON);
        }
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void F(float f, float f2, float f3) {
        int i = Build.VERSION.SDK_INT;
        if (i == 21) {
            this.y.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(com.google.android.material.floatingactionbutton.a.G, j0(f, f3));
            stateListAnimator.addState(com.google.android.material.floatingactionbutton.a.H, j0(f, f2));
            stateListAnimator.addState(com.google.android.material.floatingactionbutton.a.I, j0(f, f2));
            stateListAnimator.addState(com.google.android.material.floatingactionbutton.a.J, j0(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.y, "elevation", f).setDuration(0L));
            if (i >= 22 && i <= 24) {
                FloatingActionButton floatingActionButton = this.y;
                arrayList.add(ObjectAnimator.ofFloat(floatingActionButton, View.TRANSLATION_Z, floatingActionButton.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.y, View.TRANSLATION_Z, Utils.FLOAT_EPSILON).setDuration(100L));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(com.google.android.material.floatingactionbutton.a.F);
            stateListAnimator.addState(com.google.android.material.floatingactionbutton.a.K, animatorSet);
            stateListAnimator.addState(com.google.android.material.floatingactionbutton.a.L, j0(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON));
            this.y.setStateListAnimator(stateListAnimator);
        }
        if (Z()) {
            f0();
        }
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public boolean K() {
        return false;
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void V(ColorStateList colorStateList) {
        Drawable drawable = this.c;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(b93.d(colorStateList));
        } else {
            super.V(colorStateList);
        }
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public boolean Z() {
        return this.z.d() || !b0();
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void d0() {
    }

    public dr i0(int i, ColorStateList colorStateList) {
        Context context = this.y.getContext();
        dr drVar = new dr((pn3) du2.e(this.a));
        drVar.e(m70.d(context, ty2.design_fab_stroke_top_outer_color), m70.d(context, ty2.design_fab_stroke_top_inner_color), m70.d(context, ty2.design_fab_stroke_end_inner_color), m70.d(context, ty2.design_fab_stroke_end_outer_color));
        drVar.d(i);
        drVar.c(colorStateList);
        return drVar;
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public o42 j() {
        return new a((pn3) du2.e(this.a));
    }

    public final Animator j0(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(this.y, "elevation", f).setDuration(0L)).with(ObjectAnimator.ofFloat(this.y, View.TRANSLATION_Z, f2).setDuration(100L));
        animatorSet.setInterpolator(com.google.android.material.floatingactionbutton.a.F);
        return animatorSet;
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public float n() {
        return this.y.getElevation();
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void s(Rect rect) {
        if (this.z.d()) {
            super.s(rect);
        } else if (!b0()) {
            int sizeDimension = (this.k - this.y.getSizeDimension()) / 2;
            rect.set(sizeDimension, sizeDimension, sizeDimension, sizeDimension);
        } else {
            rect.set(0, 0, 0, 0);
        }
    }

    @Override // com.google.android.material.floatingactionbutton.a
    public void x(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        Drawable drawable;
        o42 j = j();
        this.b = j;
        j.setTintList(colorStateList);
        if (mode != null) {
            this.b.setTintMode(mode);
        }
        this.b.P(this.y.getContext());
        if (i > 0) {
            this.d = i0(i, colorStateList);
            drawable = new LayerDrawable(new Drawable[]{(Drawable) du2.e(this.d), (Drawable) du2.e(this.b)});
        } else {
            this.d = null;
            drawable = this.b;
        }
        RippleDrawable rippleDrawable = new RippleDrawable(b93.d(colorStateList2), drawable, null);
        this.c = rippleDrawable;
        this.e = rippleDrawable;
    }
}
