package defpackage;

import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.i;

/* renamed from: jl4  reason: default package */
/* loaded from: classes2.dex */
public final class jl4 {
    public final es4 a;
    public final qo0 b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;

    public jl4(qo0 qo0Var) {
        Objects.requireNonNull(qo0Var, "digest == null");
        this.b = qo0Var;
        int h = i.h(qo0Var);
        this.c = h;
        this.d = 16;
        int ceil = (int) Math.ceil((h * 8) / i.n(16));
        this.f = ceil;
        int floor = ((int) Math.floor(i.n((16 - 1) * ceil) / i.n(16))) + 1;
        this.g = floor;
        int i = ceil + floor;
        this.e = i;
        il4 b = il4.b(qo0Var.g(), h, 16, i);
        this.a = b;
        if (b != null) {
            return;
        }
        throw new IllegalArgumentException("cannot find OID for digest algorithm: " + qo0Var.g());
    }

    public qo0 a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public int c() {
        return this.e;
    }

    public int d() {
        return this.d;
    }
}
