package defpackage;

import com.google.android.gms.internal.measurement.j1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: il5  reason: default package */
/* loaded from: classes.dex */
public final class il5 extends w1<j1, il5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public il5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.j1 r0 = com.google.android.gms.internal.measurement.j1.H()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.il5.<init>():void");
    }

    public final il5 A() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.L((j1) this.f0);
        return this;
    }

    public final il5 B(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.M((j1) this.f0, j);
        return this;
    }

    public final il5 C() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.N((j1) this.f0);
        return this;
    }

    public final il5 D(double d) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.O((j1) this.f0, d);
        return this;
    }

    public final il5 E() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.P((j1) this.f0);
        return this;
    }

    public final il5 v(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.I((j1) this.f0, j);
        return this;
    }

    public final il5 x(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.J((j1) this.f0, str);
        return this;
    }

    public final il5 y(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        j1.K((j1) this.f0, str);
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ il5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.j1 r1 = com.google.android.gms.internal.measurement.j1.H()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.il5.<init>(wi5):void");
    }
}
