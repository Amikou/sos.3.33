package defpackage;

import com.fasterxml.jackson.core.b;

/* compiled from: JsonPointerBasedFilter.java */
/* renamed from: gv1  reason: default package */
/* loaded from: classes.dex */
public class gv1 extends v64 {
    public final b b;

    public gv1(String str) {
        this(b.e(str));
    }

    @Override // defpackage.v64
    public boolean a() {
        return this.b.h();
    }

    @Override // defpackage.v64
    public v64 c() {
        return this;
    }

    @Override // defpackage.v64
    public v64 d() {
        return this;
    }

    @Override // defpackage.v64
    public v64 e(int i) {
        b f = this.b.f(i);
        if (f == null) {
            return null;
        }
        if (f.h()) {
            return v64.a;
        }
        return new gv1(f);
    }

    @Override // defpackage.v64
    public v64 f(String str) {
        b g = this.b.g(str);
        if (g == null) {
            return null;
        }
        if (g.h()) {
            return v64.a;
        }
        return new gv1(g);
    }

    @Override // defpackage.v64
    public String toString() {
        return "[JsonPointerFilter at: " + this.b + "]";
    }

    public gv1(b bVar) {
        this.b = bVar;
    }
}
