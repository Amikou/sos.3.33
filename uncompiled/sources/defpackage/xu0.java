package defpackage;

import com.facebook.common.memory.PooledByteBuffer;

/* compiled from: EncodedCountingMemoryCacheFactory.java */
/* renamed from: xu0  reason: default package */
/* loaded from: classes.dex */
public class xu0 {

    /* compiled from: EncodedCountingMemoryCacheFactory.java */
    /* renamed from: xu0$a */
    /* loaded from: classes.dex */
    public static class a implements yg4<PooledByteBuffer> {
        @Override // defpackage.yg4
        /* renamed from: b */
        public int a(PooledByteBuffer pooledByteBuffer) {
            return pooledByteBuffer.size();
        }
    }

    public static i90<wt, PooledByteBuffer> a(fw3<m72> fw3Var, r72 r72Var) {
        u22 u22Var = new u22(new a(), new sd2(), fw3Var, null, false, false);
        r72Var.a(u22Var);
        return u22Var;
    }
}
