package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.a;

/* compiled from: EncryptedSharedPrefs.java */
/* renamed from: jv0  reason: default package */
/* loaded from: classes2.dex */
public class jv0 {
    public static String a = "safemoon_encrypted_shared_prefs";
    public static String b = "safemoon_shared_prefs";

    public static void a(Context context) {
        b(context).edit().clear().apply();
    }

    public static SharedPreferences b(Context context) {
        try {
            return EncryptedSharedPreferences.a(a, a.c(a.a), context, EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV, EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
        } catch (Exception e) {
            e.printStackTrace();
            y54.a("Khang").a("Exception: %s", e.getLocalizedMessage());
            return context.getSharedPreferences(b, 0);
        }
    }

    public static String c(Context context, String str) {
        return b(context).getString(str, "");
    }

    public static String d(Context context, String str, String str2) {
        return b(context).getString(str, str2);
    }

    public static void e(Context context, String str, String str2) {
        b(context).edit().putString(str, str2).apply();
    }
}
