package defpackage;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/* compiled from: JobSupport.kt */
/* renamed from: ns1  reason: default package */
/* loaded from: classes2.dex */
public final class ns1 extends tt1 {
    public static final /* synthetic */ AtomicIntegerFieldUpdater j0 = AtomicIntegerFieldUpdater.newUpdater(ns1.class, "_invoked");
    private volatile /* synthetic */ int _invoked = 0;
    public final tc1<Throwable, te4> i0;

    /* JADX WARN: Multi-variable type inference failed */
    public ns1(tc1<? super Throwable, te4> tc1Var) {
        this.i0 = tc1Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        y(th);
        return te4.a;
    }

    @Override // defpackage.v30
    public void y(Throwable th) {
        if (j0.compareAndSet(this, 0, 1)) {
            this.i0.invoke(th);
        }
    }
}
