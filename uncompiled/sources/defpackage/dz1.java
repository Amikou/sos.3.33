package defpackage;

import com.google.crypto.tink.shaded.protobuf.ByteString;
import java.util.List;

/* compiled from: LazyStringList.java */
/* renamed from: dz1  reason: default package */
/* loaded from: classes2.dex */
public interface dz1 extends List {
    void i1(ByteString byteString);

    Object j(int i);

    List<?> r();

    dz1 x();
}
