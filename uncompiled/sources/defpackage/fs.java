package defpackage;

import android.os.PersistableBundle;

/* compiled from: BundleCompat.java */
/* renamed from: fs  reason: default package */
/* loaded from: classes2.dex */
public class fs implements cs<PersistableBundle> {
    public PersistableBundle a = new PersistableBundle();

    @Override // defpackage.cs
    public void a(String str, Long l) {
        this.a.putLong(str, l.longValue());
    }

    @Override // defpackage.cs
    public Long b(String str) {
        return Long.valueOf(this.a.getLong(str));
    }

    @Override // defpackage.cs
    public Integer d(String str) {
        return Integer.valueOf(this.a.getInt(str));
    }

    @Override // defpackage.cs
    public String e(String str) {
        return this.a.getString(str);
    }

    @Override // defpackage.cs
    public boolean f(String str) {
        return this.a.containsKey(str);
    }

    @Override // defpackage.cs
    /* renamed from: g */
    public PersistableBundle c() {
        return this.a;
    }

    @Override // defpackage.cs
    public boolean getBoolean(String str, boolean z) {
        return this.a.getBoolean(str, z);
    }

    @Override // defpackage.cs
    public void putString(String str, String str2) {
        this.a.putString(str, str2);
    }
}
