package defpackage;

import android.graphics.Bitmap;
import android.os.Build;
import com.facebook.common.references.a;

/* compiled from: TransformationUtils.java */
/* renamed from: bb4  reason: default package */
/* loaded from: classes.dex */
public final class bb4 {
    public static boolean a(pq pqVar, a<Bitmap> aVar) {
        if (pqVar == null || aVar == null) {
            return false;
        }
        Bitmap j = aVar.j();
        if (Build.VERSION.SDK_INT >= 12 && pqVar.a()) {
            j.setHasAlpha(true);
        }
        pqVar.transform(j);
        return true;
    }
}
