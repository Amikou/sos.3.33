package defpackage;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import java.lang.ref.WeakReference;
import java.util.List;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.i;

/* compiled from: ResolveUriTask.java */
/* renamed from: q73  reason: default package */
/* loaded from: classes3.dex */
public class q73 extends AsyncTask<Uri, Void, List<MediaResult>> {
    public final WeakReference<uu<List<MediaResult>>> a;
    public final Context b;
    public final vt3 c;
    public final String d;

    public q73(Context context, vt3 vt3Var, uu<List<MediaResult>> uuVar, String str) {
        this.b = context;
        this.c = vt3Var;
        this.d = str;
        this.a = new WeakReference<>(uuVar);
    }

    public static void c(Context context, vt3 vt3Var, uu<List<MediaResult>> uuVar, List<Uri> list) {
        d(context, vt3Var, uuVar, list, null);
    }

    public static void d(Context context, vt3 vt3Var, uu<List<MediaResult>> uuVar, List<Uri> list, String str) {
        new q73(context, vt3Var, uuVar, str).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Uri[]) list.toArray(new Uri[list.size()]));
    }

    /* JADX WARN: Removed duplicated region for block: B:107:0x0112 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:111:0x0149 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:117:0x011d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:121:0x013e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:138:0x0152 A[SYNTHETIC] */
    @Override // android.os.AsyncTask
    /* renamed from: a */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public java.util.List<zendesk.belvedere.MediaResult> doInBackground(android.net.Uri... r27) {
        /*
            Method dump skipped, instructions count: 370
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.q73.doInBackground(android.net.Uri[]):java.util.List");
    }

    @Override // android.os.AsyncTask
    /* renamed from: b */
    public void onPostExecute(List<MediaResult> list) {
        super.onPostExecute(list);
        uu<List<MediaResult>> uuVar = this.a.get();
        if (uuVar != null) {
            uuVar.internalSuccess(list);
        } else {
            i.e("Belvedere", "Callback null");
        }
    }
}
