package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: lj5  reason: default package */
/* loaded from: classes.dex */
public final class lj5 implements j46 {
    public final /* synthetic */ qj5 a;

    public lj5(qj5 qj5Var) {
        this.a = qj5Var;
    }

    @Override // defpackage.j46
    public final void a(int i, String str, List<String> list, boolean z, boolean z2) {
        jg5 u;
        int i2 = i - 1;
        if (i2 == 0) {
            u = this.a.a.w().u();
        } else if (i2 != 1) {
            if (i2 == 3) {
                u = this.a.a.w().v();
            } else if (i2 != 4) {
                u = this.a.a.w().t();
            } else if (z) {
                u = this.a.a.w().r();
            } else if (!z2) {
                u = this.a.a.w().s();
            } else {
                u = this.a.a.w().p();
            }
        } else if (z) {
            u = this.a.a.w().n();
        } else if (!z2) {
            u = this.a.a.w().o();
        } else {
            u = this.a.a.w().l();
        }
        int size = list.size();
        if (size == 1) {
            u.b(str, list.get(0));
        } else if (size == 2) {
            u.c(str, list.get(0), list.get(1));
        } else if (size != 3) {
            u.a(str);
        } else {
            u.d(str, list.get(0), list.get(1), list.get(2));
        }
    }
}
