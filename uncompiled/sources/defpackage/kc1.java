package defpackage;

import android.content.Context;
import com.facebook.drawee.view.SimpleDraweeView;

/* compiled from: Fresco.java */
/* renamed from: kc1  reason: default package */
/* loaded from: classes.dex */
public class kc1 {
    public static final Class<?> a = kc1.class;
    public static yq2 b = null;
    public static volatile boolean c = false;

    public static void a(Context context) {
        b(context, null, null);
    }

    public static void b(Context context, ro1 ro1Var, hr0 hr0Var) {
        c(context, ro1Var, hr0Var, true);
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0049, code lost:
        if (defpackage.nc1.d() != false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x005a, code lost:
        if (defpackage.nc1.d() == false) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0069, code lost:
        if (defpackage.nc1.d() == false) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x0078, code lost:
        if (defpackage.nc1.d() == false) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x0087, code lost:
        if (defpackage.nc1.d() == false) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x0089, code lost:
        defpackage.nc1.b();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static void c(android.content.Context r5, defpackage.ro1 r6, defpackage.hr0 r7, boolean r8) {
        /*
            boolean r0 = defpackage.nc1.d()
            if (r0 == 0) goto Lb
            java.lang.String r0 = "Fresco#initialize"
            defpackage.nc1.a(r0)
        Lb:
            boolean r0 = defpackage.kc1.c
            r1 = 1
            if (r0 == 0) goto L18
            java.lang.Class<?> r0 = defpackage.kc1.a
            java.lang.String r2 = "Fresco has already been initialized! `Fresco.initialize(...)` should only be called 1 single time to avoid memory leaks!"
            defpackage.v11.t(r0, r2)
            goto L1a
        L18:
            defpackage.kc1.c = r1
        L1a:
            defpackage.ld2.b(r8)
            boolean r8 = defpackage.pd2.c()
            if (r8 != 0) goto L97
            boolean r8 = defpackage.nc1.d()
            if (r8 == 0) goto L2e
            java.lang.String r8 = "Fresco.initialize->SoLoader.init"
            defpackage.nc1.a(r8)
        L2e:
            java.lang.Class<com.facebook.imagepipeline.nativecode.NativeCodeInitializer> r8 = com.facebook.imagepipeline.nativecode.NativeCodeInitializer.class
            java.lang.String r0 = "init"
            java.lang.Class[] r2 = new java.lang.Class[r1]     // Catch: java.lang.Throwable -> L4c java.lang.NoSuchMethodException -> L4e java.lang.reflect.InvocationTargetException -> L5d java.lang.IllegalAccessException -> L6c java.lang.ClassNotFoundException -> L7b
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r4 = 0
            r2[r4] = r3     // Catch: java.lang.Throwable -> L4c java.lang.NoSuchMethodException -> L4e java.lang.reflect.InvocationTargetException -> L5d java.lang.IllegalAccessException -> L6c java.lang.ClassNotFoundException -> L7b
            java.lang.reflect.Method r8 = r8.getMethod(r0, r2)     // Catch: java.lang.Throwable -> L4c java.lang.NoSuchMethodException -> L4e java.lang.reflect.InvocationTargetException -> L5d java.lang.IllegalAccessException -> L6c java.lang.ClassNotFoundException -> L7b
            r0 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch: java.lang.Throwable -> L4c java.lang.NoSuchMethodException -> L4e java.lang.reflect.InvocationTargetException -> L5d java.lang.IllegalAccessException -> L6c java.lang.ClassNotFoundException -> L7b
            r1[r4] = r5     // Catch: java.lang.Throwable -> L4c java.lang.NoSuchMethodException -> L4e java.lang.reflect.InvocationTargetException -> L5d java.lang.IllegalAccessException -> L6c java.lang.ClassNotFoundException -> L7b
            r8.invoke(r0, r1)     // Catch: java.lang.Throwable -> L4c java.lang.NoSuchMethodException -> L4e java.lang.reflect.InvocationTargetException -> L5d java.lang.IllegalAccessException -> L6c java.lang.ClassNotFoundException -> L7b
            boolean r8 = defpackage.nc1.d()
            if (r8 == 0) goto L97
            goto L89
        L4c:
            r5 = move-exception
            goto L8d
        L4e:
            s24 r8 = new s24     // Catch: java.lang.Throwable -> L4c
            r8.<init>()     // Catch: java.lang.Throwable -> L4c
            defpackage.pd2.b(r8)     // Catch: java.lang.Throwable -> L4c
            boolean r8 = defpackage.nc1.d()
            if (r8 == 0) goto L97
            goto L89
        L5d:
            s24 r8 = new s24     // Catch: java.lang.Throwable -> L4c
            r8.<init>()     // Catch: java.lang.Throwable -> L4c
            defpackage.pd2.b(r8)     // Catch: java.lang.Throwable -> L4c
            boolean r8 = defpackage.nc1.d()
            if (r8 == 0) goto L97
            goto L89
        L6c:
            s24 r8 = new s24     // Catch: java.lang.Throwable -> L4c
            r8.<init>()     // Catch: java.lang.Throwable -> L4c
            defpackage.pd2.b(r8)     // Catch: java.lang.Throwable -> L4c
            boolean r8 = defpackage.nc1.d()
            if (r8 == 0) goto L97
            goto L89
        L7b:
            s24 r8 = new s24     // Catch: java.lang.Throwable -> L4c
            r8.<init>()     // Catch: java.lang.Throwable -> L4c
            defpackage.pd2.b(r8)     // Catch: java.lang.Throwable -> L4c
            boolean r8 = defpackage.nc1.d()
            if (r8 == 0) goto L97
        L89:
            defpackage.nc1.b()
            goto L97
        L8d:
            boolean r6 = defpackage.nc1.d()
            if (r6 == 0) goto L96
            defpackage.nc1.b()
        L96:
            throw r5
        L97:
            android.content.Context r5 = r5.getApplicationContext()
            if (r6 != 0) goto La1
            defpackage.uo1.v(r5)
            goto La4
        La1:
            defpackage.uo1.u(r6)
        La4:
            d(r5, r7)
            boolean r5 = defpackage.nc1.d()
            if (r5 == 0) goto Lb0
            defpackage.nc1.b()
        Lb0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.kc1.c(android.content.Context, ro1, hr0, boolean):void");
    }

    public static void d(Context context, hr0 hr0Var) {
        if (nc1.d()) {
            nc1.a("Fresco.initializeDrawee");
        }
        yq2 yq2Var = new yq2(context, hr0Var);
        b = yq2Var;
        SimpleDraweeView.i(yq2Var);
        if (nc1.d()) {
            nc1.b();
        }
    }

    public static xq2 e() {
        return b.get();
    }
}
