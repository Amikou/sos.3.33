package defpackage;

import android.text.TextUtils;
import java.util.regex.Pattern;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: o74  reason: default package */
/* loaded from: classes2.dex */
public final class o74 {
    public static final Pattern d = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");
    public final String a;
    public final String b;
    public final String c;

    public o74(String str, String str2) {
        this.a = d(str2, str);
        this.b = str;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("!");
        sb.append(str2);
        this.c = sb.toString();
    }

    public static o74 a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String[] split = str.split("!", -1);
        if (split.length != 2) {
            return null;
        }
        return new o74(split[0], split[1]);
    }

    public static String d(String str, String str2) {
        if (str != null && str.startsWith("/topics/")) {
            String.format("Format /topics/topic-name is deprecated. Only 'topic-name' should be used in %s.", str2);
            str = str.substring(8);
        }
        if (str == null || !d.matcher(str).matches()) {
            throw new IllegalArgumentException(String.format("Invalid topic name: %s does not match the allowed format %s.", str, "[a-zA-Z0-9-_.~%]{1,900}"));
        }
        return str;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.a;
    }

    public String e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj instanceof o74) {
            o74 o74Var = (o74) obj;
            return this.a.equals(o74Var.a) && this.b.equals(o74Var.b);
        }
        return false;
    }

    public int hashCode() {
        return pl2.b(this.b, this.a);
    }
}
