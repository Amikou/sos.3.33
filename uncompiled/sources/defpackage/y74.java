package defpackage;

import android.content.Context;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.onesignal.OneSignal;
import com.onesignal.g0;
import com.onesignal.l0;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: TrackFirebaseAnalytics.java */
/* renamed from: y74  reason: default package */
/* loaded from: classes2.dex */
public class y74 {
    public static Class<?> c;
    public static AtomicLong d;
    public static AtomicLong e;
    public static g0 f;
    public Object a;
    public Context b;

    public y74(Context context) {
        this.b = context;
    }

    public static boolean a() {
        try {
            c = FirebaseAnalytics.class;
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    public static Method d(Class cls) {
        try {
            return cls.getMethod("getInstance", Context.class);
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Method e(Class cls) {
        try {
            return cls.getMethod("logEvent", String.class, Bundle.class);
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final String b(g0 g0Var) {
        if (g0Var.j().isEmpty() || g0Var.i().isEmpty()) {
            return g0Var.k() != null ? g0Var.k().substring(0, Math.min(10, g0Var.k().length())) : "";
        }
        return g0Var.j() + " - " + g0Var.i();
    }

    public final Object c(Context context) {
        if (this.a == null) {
            try {
                this.a = d(c).invoke(null, context);
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }
        return this.a;
    }

    public void f() {
        if (d == null || f == null) {
            return;
        }
        long a = OneSignal.w0().a();
        if (a - d.get() > 120000) {
            return;
        }
        AtomicLong atomicLong = e;
        if (atomicLong == null || a - atomicLong.get() >= 30000) {
            try {
                Object c2 = c(this.b);
                Method e2 = e(c);
                Bundle bundle = new Bundle();
                bundle.putString("source", "OneSignal");
                bundle.putString("medium", "notification");
                bundle.putString("notification_id", f.g());
                bundle.putString("campaign", b(f));
                e2.invoke(c2, "os_notification_influence_open", bundle);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void g(l0 l0Var) {
        if (e == null) {
            e = new AtomicLong();
        }
        e.set(OneSignal.w0().a());
        try {
            Object c2 = c(this.b);
            Method e2 = e(c);
            Bundle bundle = new Bundle();
            bundle.putString("source", "OneSignal");
            bundle.putString("medium", "notification");
            bundle.putString("notification_id", l0Var.d().g());
            bundle.putString("campaign", b(l0Var.d()));
            e2.invoke(c2, "os_notification_opened", bundle);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void h(l0 l0Var) {
        try {
            Object c2 = c(this.b);
            Method e2 = e(c);
            Bundle bundle = new Bundle();
            bundle.putString("source", "OneSignal");
            bundle.putString("medium", "notification");
            bundle.putString("notification_id", l0Var.d().g());
            bundle.putString("campaign", b(l0Var.d()));
            e2.invoke(c2, "os_notification_received", bundle);
            if (d == null) {
                d = new AtomicLong();
            }
            d.set(OneSignal.w0().a());
            f = l0Var.d();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
