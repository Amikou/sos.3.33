package defpackage;

import defpackage.l80;

/* compiled from: BaseControllerListener2.java */
/* renamed from: dn  reason: default package */
/* loaded from: classes.dex */
public class dn<INFO> implements l80<INFO> {
    static {
        new dn();
    }

    @Override // defpackage.l80
    public void a(String str, INFO info2) {
    }

    @Override // defpackage.l80
    public void b(String str, INFO info2, l80.a aVar) {
    }

    @Override // defpackage.l80
    public void c(String str, l80.a aVar) {
    }

    @Override // defpackage.l80
    public void d(String str, Throwable th, l80.a aVar) {
    }

    @Override // defpackage.l80
    public void e(String str, Object obj, l80.a aVar) {
    }

    @Override // defpackage.l80
    public void f(String str) {
    }
}
