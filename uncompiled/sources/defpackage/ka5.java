package defpackage;

import android.os.Parcel;

/* renamed from: ka5  reason: default package */
/* loaded from: classes.dex */
public class ka5 {
    static {
        ka5.class.getClassLoader();
    }

    public static void a(Parcel parcel, boolean z) {
        parcel.writeInt(1);
    }

    public static boolean b(Parcel parcel) {
        return parcel.readInt() != 0;
    }
}
