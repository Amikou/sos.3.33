package defpackage;

import com.google.android.gms.internal.measurement.b;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: fx5  reason: default package */
/* loaded from: classes.dex */
public final class fx5 extends p55 {
    public final b f0;

    public fx5(b bVar) {
        this.f0 = bVar;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // defpackage.p55, defpackage.z55
    public final z55 n(String str, wk5 wk5Var, List<z55> list) {
        char c;
        switch (str.hashCode()) {
            case 21624207:
                if (str.equals("getEventName")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 45521504:
                if (str.equals("getTimestamp")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 146575578:
                if (str.equals("getParamValue")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 700587132:
                if (str.equals("getParams")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 920706790:
                if (str.equals("setParamValue")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case 1570616835:
                if (str.equals("setEventName")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            vm5.a("getEventName", 0, list);
            return new f65(this.f0.c().b());
        } else if (c == 1) {
            vm5.a("getParamValue", 1, list);
            return rp5.a(this.f0.c().e(wk5Var.a(list.get(0)).zzc()));
        } else if (c == 2) {
            vm5.a("getParams", 0, list);
            Map<String, Object> f = this.f0.c().f();
            p55 p55Var = new p55();
            for (String str2 : f.keySet()) {
                p55Var.k(str2, rp5.a(f.get(str2)));
            }
            return p55Var;
        } else if (c == 3) {
            vm5.a("getTimestamp", 0, list);
            return new z45(Double.valueOf(this.f0.c().a()));
        } else if (c != 4) {
            if (c != 5) {
                return super.n(str, wk5Var, list);
            }
            vm5.a("setParamValue", 2, list);
            String zzc = wk5Var.a(list.get(0)).zzc();
            z55 a = wk5Var.a(list.get(1));
            this.f0.c().d(zzc, vm5.j(a));
            return a;
        } else {
            vm5.a("setEventName", 1, list);
            z55 a2 = wk5Var.a(list.get(0));
            if (!z55.X.equals(a2) && !z55.Y.equals(a2)) {
                this.f0.c().c(a2.zzc());
                return new f65(a2.zzc());
            }
            throw new IllegalArgumentException("Illegal event name");
        }
    }
}
