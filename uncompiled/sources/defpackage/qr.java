package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import com.facebook.imagepipeline.memory.BasePool;

/* compiled from: BucketsBitmapPool.java */
@TargetApi(21)
/* renamed from: qr  reason: default package */
/* loaded from: classes.dex */
public class qr extends BasePool<Bitmap> implements iq {
    public qr(r72 r72Var, ys2 ys2Var, zs2 zs2Var, boolean z) {
        super(r72Var, ys2Var, zs2Var, z);
        r();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: A */
    public void j(Bitmap bitmap) {
        xt2.g(bitmap);
        bitmap.recycle();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: B */
    public int n(Bitmap bitmap) {
        xt2.g(bitmap);
        return bitmap.getAllocationByteCount();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: C */
    public Bitmap p(or<Bitmap> orVar) {
        Bitmap bitmap = (Bitmap) super.p(orVar);
        if (bitmap != null) {
            bitmap.eraseColor(0);
        }
        return bitmap;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: D */
    public boolean t(Bitmap bitmap) {
        xt2.g(bitmap);
        return !bitmap.isRecycled() && bitmap.isMutable();
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int m(int i) {
        return i;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    public int o(int i) {
        return i;
    }

    @Override // com.facebook.imagepipeline.memory.BasePool
    /* renamed from: z */
    public Bitmap f(int i) {
        return Bitmap.createBitmap(1, (int) Math.ceil(i / 2.0d), Bitmap.Config.RGB_565);
    }
}
