package defpackage;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: hz5  reason: default package */
/* loaded from: classes.dex */
public class hz5<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public final int a;
    public boolean h0;
    public volatile gz5 i0;
    public List<dz5> f0 = Collections.emptyList();
    public Map<K, V> g0 = Collections.emptyMap();
    public Map<K, V> j0 = Collections.emptyMap();

    public void a() {
        Map<K, V> unmodifiableMap;
        Map<K, V> unmodifiableMap2;
        if (this.h0) {
            return;
        }
        if (this.g0.isEmpty()) {
            unmodifiableMap = Collections.emptyMap();
        } else {
            unmodifiableMap = Collections.unmodifiableMap(this.g0);
        }
        this.g0 = unmodifiableMap;
        if (this.j0.isEmpty()) {
            unmodifiableMap2 = Collections.emptyMap();
        } else {
            unmodifiableMap2 = Collections.unmodifiableMap(this.j0);
        }
        this.j0 = unmodifiableMap2;
        this.h0 = true;
    }

    public final boolean b() {
        return this.h0;
    }

    public final int c() {
        return this.f0.size();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final void clear() {
        m();
        if (!this.f0.isEmpty()) {
            this.f0.clear();
        }
        if (this.g0.isEmpty()) {
            return;
        }
        this.g0.clear();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public final boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return l(comparable) >= 0 || this.g0.containsKey(comparable);
    }

    public final Map.Entry<K, V> d(int i) {
        return this.f0.get(i);
    }

    public final Iterable<Map.Entry<K, V>> e() {
        return this.g0.isEmpty() ? vy5.a() : this.g0.entrySet();
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final Set<Map.Entry<K, V>> entrySet() {
        if (this.i0 == null) {
            this.i0 = new gz5(this, null);
        }
        return this.i0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hz5)) {
            return super.equals(obj);
        }
        hz5 hz5Var = (hz5) obj;
        int size = size();
        if (size != hz5Var.size()) {
            return false;
        }
        int c = c();
        if (c == hz5Var.c()) {
            for (int i = 0; i < c; i++) {
                if (!d(i).equals(hz5Var.d(i))) {
                    return false;
                }
            }
            if (c != size) {
                return this.g0.equals(hz5Var.g0);
            }
            return true;
        }
        return entrySet().equals(hz5Var.entrySet());
    }

    @Override // java.util.AbstractMap, java.util.Map
    /* renamed from: f */
    public final V put(K k, V v) {
        m();
        int l = l(k);
        if (l >= 0) {
            return (V) this.f0.get(l).setValue(v);
        }
        m();
        if (this.f0.isEmpty() && !(this.f0 instanceof ArrayList)) {
            this.f0 = new ArrayList(this.a);
        }
        int i = -(l + 1);
        if (i >= this.a) {
            return n().put(k, v);
        }
        int size = this.f0.size();
        int i2 = this.a;
        if (size == i2) {
            dz5 remove = this.f0.remove(i2 - 1);
            n().put((K) remove.a(), (V) remove.getValue());
        }
        this.f0.add(i, new dz5(this, k, v));
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public final V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int l = l(comparable);
        if (l >= 0) {
            return (V) this.f0.get(l).getValue();
        }
        return this.g0.get(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final int hashCode() {
        int c = c();
        int i = 0;
        for (int i2 = 0; i2 < c; i2++) {
            i += this.f0.get(i2).hashCode();
        }
        return this.g0.size() > 0 ? i + this.g0.hashCode() : i;
    }

    public final V k(int i) {
        m();
        V v = (V) this.f0.remove(i).getValue();
        if (!this.g0.isEmpty()) {
            Iterator<Map.Entry<K, V>> it = n().entrySet().iterator();
            List<dz5> list = this.f0;
            Map.Entry<K, V> next = it.next();
            list.add(new dz5(this, next.getKey(), next.getValue()));
            it.remove();
        }
        return v;
    }

    public final int l(K k) {
        int size = this.f0.size() - 1;
        int i = 0;
        if (size >= 0) {
            int compareTo = k.compareTo(this.f0.get(size).a());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo(this.f0.get(i2).a());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    public final void m() {
        if (this.h0) {
            throw new UnsupportedOperationException();
        }
    }

    public final SortedMap<K, V> n() {
        m();
        if (this.g0.isEmpty() && !(this.g0 instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.g0 = treeMap;
            this.j0 = treeMap.descendingMap();
        }
        return (SortedMap) this.g0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public final V remove(Object obj) {
        m();
        Comparable comparable = (Comparable) obj;
        int l = l(comparable);
        if (l >= 0) {
            return (V) k(l);
        }
        if (this.g0.isEmpty()) {
            return null;
        }
        return this.g0.remove(comparable);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public final int size() {
        return this.f0.size() + this.g0.size();
    }
}
