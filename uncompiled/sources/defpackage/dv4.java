package defpackage;

import com.google.android.play.core.assetpacks.bj;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: dv4  reason: default package */
/* loaded from: classes2.dex */
public final class dv4 {
    public static final it4 j = new it4("ExtractorLooper");
    public final zv4 a;
    public final cv4 b;
    public final fx4 c;
    public final pw4 d;
    public final uw4 e;
    public final yw4 f;
    public final cw4<zy4> g;
    public final hw4 h;
    public final AtomicBoolean i = new AtomicBoolean(false);

    public dv4(zv4 zv4Var, cw4<zy4> cw4Var, cv4 cv4Var, fx4 fx4Var, pw4 pw4Var, uw4 uw4Var, yw4 yw4Var, hw4 hw4Var) {
        this.a = zv4Var;
        this.g = cw4Var;
        this.b = cv4Var;
        this.c = fx4Var;
        this.d = pw4Var;
        this.e = uw4Var;
        this.f = yw4Var;
        this.h = hw4Var;
    }

    public final void a() {
        it4 it4Var = j;
        it4Var.a("Run extractor loop", new Object[0]);
        if (!this.i.compareAndSet(false, true)) {
            it4Var.e("runLoop already looping; return", new Object[0]);
            return;
        }
        while (true) {
            fw4 fw4Var = null;
            try {
                fw4Var = this.h.a();
            } catch (bj e) {
                j.b("Error while getting next extraction task: %s", e.getMessage());
                if (e.a >= 0) {
                    this.g.a().b(e.a);
                    b(e.a, e);
                }
            }
            if (fw4Var == null) {
                this.i.set(false);
                return;
            }
            try {
                if (fw4Var instanceof bv4) {
                    this.b.a((bv4) fw4Var);
                } else if (fw4Var instanceof ex4) {
                    this.c.a((ex4) fw4Var);
                } else if (fw4Var instanceof ow4) {
                    this.d.a((ow4) fw4Var);
                } else if (fw4Var instanceof rw4) {
                    this.e.a((rw4) fw4Var);
                } else if (fw4Var instanceof xw4) {
                    this.f.a((xw4) fw4Var);
                } else {
                    j.b("Unknown task type: %s", fw4Var.getClass().getName());
                }
            } catch (Exception e2) {
                j.b("Error during extraction task: %s", e2.getMessage());
                this.g.a().b(fw4Var.a);
                b(fw4Var.a, e2);
            }
        }
    }

    public final void b(int i, Exception exc) {
        try {
            this.a.o(i);
            this.a.g(i);
        } catch (bj unused) {
            j.b("Error during error handling: %s", exc.getMessage());
        }
    }
}
