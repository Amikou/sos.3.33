package defpackage;

import defpackage.pt0;

/* renamed from: ve3  reason: default package */
/* loaded from: classes2.dex */
public class ve3 extends pt0.c {
    public ve3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public ve3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public ve3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        ue3 ue3Var = (ue3) this.c;
        if (ue3Var.i()) {
            return i.v();
        }
        ue3 ue3Var2 = (ue3) this.b;
        ue3 ue3Var3 = (ue3) this.d[0];
        int[] g = cd2.g();
        int[] g2 = cd2.g();
        int[] g3 = cd2.g();
        te3.j(ue3Var.f, g3);
        int[] g4 = cd2.g();
        te3.j(g3, g4);
        boolean h = ue3Var3.h();
        int[] iArr = ue3Var3.f;
        if (!h) {
            te3.j(iArr, g2);
            iArr = g2;
        }
        te3.m(ue3Var2.f, iArr, g);
        te3.a(ue3Var2.f, iArr, g2);
        te3.e(g2, g, g2);
        te3.i(cd2.b(g2, g2, g2), g2);
        te3.e(g3, ue3Var2.f, g3);
        te3.i(kd2.G(6, g3, 2, 0), g3);
        te3.i(kd2.H(6, g4, 3, 0, g), g);
        ue3 ue3Var4 = new ue3(g4);
        te3.j(g2, ue3Var4.f);
        int[] iArr2 = ue3Var4.f;
        te3.m(iArr2, g3, iArr2);
        int[] iArr3 = ue3Var4.f;
        te3.m(iArr3, g3, iArr3);
        ue3 ue3Var5 = new ue3(g3);
        te3.m(g3, ue3Var4.f, ue3Var5.f);
        int[] iArr4 = ue3Var5.f;
        te3.e(iArr4, g2, iArr4);
        int[] iArr5 = ue3Var5.f;
        te3.m(iArr5, g, iArr5);
        ue3 ue3Var6 = new ue3(g2);
        te3.n(ue3Var.f, ue3Var6.f);
        if (!h) {
            int[] iArr6 = ue3Var6.f;
            te3.e(iArr6, ue3Var3.f, iArr6);
        }
        return new ve3(i, ue3Var4, ue3Var5, new ct0[]{ue3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        ue3 ue3Var = (ue3) this.b;
        ue3 ue3Var2 = (ue3) this.c;
        ue3 ue3Var3 = (ue3) pt0Var.q();
        ue3 ue3Var4 = (ue3) pt0Var.r();
        ue3 ue3Var5 = (ue3) this.d[0];
        ue3 ue3Var6 = (ue3) pt0Var.s(0);
        int[] i2 = cd2.i();
        int[] g = cd2.g();
        int[] g2 = cd2.g();
        int[] g3 = cd2.g();
        boolean h = ue3Var5.h();
        if (h) {
            iArr = ue3Var3.f;
            iArr2 = ue3Var4.f;
        } else {
            te3.j(ue3Var5.f, g2);
            te3.e(g2, ue3Var3.f, g);
            te3.e(g2, ue3Var5.f, g2);
            te3.e(g2, ue3Var4.f, g2);
            iArr = g;
            iArr2 = g2;
        }
        boolean h2 = ue3Var6.h();
        if (h2) {
            iArr3 = ue3Var.f;
            iArr4 = ue3Var2.f;
        } else {
            te3.j(ue3Var6.f, g3);
            te3.e(g3, ue3Var.f, i2);
            te3.e(g3, ue3Var6.f, g3);
            te3.e(g3, ue3Var2.f, g3);
            iArr3 = i2;
            iArr4 = g3;
        }
        int[] g4 = cd2.g();
        te3.m(iArr3, iArr, g4);
        te3.m(iArr4, iArr2, g);
        if (cd2.u(g4)) {
            return cd2.u(g) ? J() : i.v();
        }
        te3.j(g4, g2);
        int[] g5 = cd2.g();
        te3.e(g2, g4, g5);
        te3.e(g2, iArr3, g2);
        te3.g(g5, g5);
        cd2.x(iArr4, g5, i2);
        te3.i(cd2.b(g2, g2, g5), g5);
        ue3 ue3Var7 = new ue3(g3);
        te3.j(g, ue3Var7.f);
        int[] iArr5 = ue3Var7.f;
        te3.m(iArr5, g5, iArr5);
        ue3 ue3Var8 = new ue3(g5);
        te3.m(g2, ue3Var7.f, ue3Var8.f);
        te3.f(ue3Var8.f, g, i2);
        te3.h(i2, ue3Var8.f);
        ue3 ue3Var9 = new ue3(g4);
        if (!h) {
            int[] iArr6 = ue3Var9.f;
            te3.e(iArr6, ue3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = ue3Var9.f;
            te3.e(iArr7, ue3Var6.f, iArr7);
        }
        return new ve3(i, ue3Var7, ue3Var8, new ct0[]{ue3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new ve3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new ve3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
