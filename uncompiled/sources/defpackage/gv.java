package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.SystemClock;
import android.view.SurfaceHolder;
import androidx.annotation.RecentlyNonNull;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.yb1;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.IdentityHashMap;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: gv  reason: default package */
/* loaded from: classes.dex */
public class gv {
    public Context a;
    public final Object b;
    public Camera c;
    public int d;
    public int e;
    public bq3 f;
    public float g;
    public int h;
    public int i;
    public boolean j;
    public String k;
    public Thread l;
    public b m;
    public final IdentityHashMap<byte[], ByteBuffer> n;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: gv$a */
    /* loaded from: classes.dex */
    public static class a {
        public final rm0<?> a;
        public gv b;

        public a(@RecentlyNonNull Context context, @RecentlyNonNull rm0<?> rm0Var) {
            gv gvVar = new gv();
            this.b = gvVar;
            if (context == null) {
                throw new IllegalArgumentException("No context supplied.");
            }
            if (rm0Var != null) {
                this.a = rm0Var;
                gvVar.a = context;
                return;
            }
            throw new IllegalArgumentException("No detector supplied.");
        }

        @RecentlyNonNull
        public gv a() {
            gv gvVar = this.b;
            gvVar.getClass();
            gvVar.m = new b(this.a);
            return this.b;
        }

        @RecentlyNonNull
        public a b(boolean z) {
            this.b.j = z;
            return this;
        }

        @RecentlyNonNull
        public a c(int i, int i2) {
            if (i > 0 && i <= 1000000 && i2 > 0 && i2 <= 1000000) {
                this.b.h = i;
                this.b.i = i2;
                return this;
            }
            StringBuilder sb = new StringBuilder(45);
            sb.append("Invalid preview size: ");
            sb.append(i);
            sb.append("x");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: gv$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public rm0<?> a;
        public long i0;
        public ByteBuffer k0;
        public long f0 = SystemClock.elapsedRealtime();
        public final Object g0 = new Object();
        public boolean h0 = true;
        public int j0 = 0;

        public b(rm0<?> rm0Var) {
            this.a = rm0Var;
        }

        @SuppressLint({"Assert"})
        public final void a() {
            rm0<?> rm0Var = this.a;
            if (rm0Var != null) {
                rm0Var.d();
                this.a = null;
            }
        }

        public final void b(boolean z) {
            synchronized (this.g0) {
                this.h0 = z;
                this.g0.notifyAll();
            }
        }

        public final void c(byte[] bArr, Camera camera) {
            synchronized (this.g0) {
                ByteBuffer byteBuffer = this.k0;
                if (byteBuffer != null) {
                    camera.addCallbackBuffer(byteBuffer.array());
                    this.k0 = null;
                }
                if (gv.this.n.containsKey(bArr)) {
                    this.i0 = SystemClock.elapsedRealtime() - this.f0;
                    this.j0++;
                    this.k0 = (ByteBuffer) gv.this.n.get(bArr);
                    this.g0.notifyAll();
                }
            }
        }

        @Override // java.lang.Runnable
        @SuppressLint({"InlinedApi"})
        public final void run() {
            boolean z;
            yb1 a;
            ByteBuffer byteBuffer;
            while (true) {
                synchronized (this.g0) {
                    while (true) {
                        z = this.h0;
                        if (!z || this.k0 != null) {
                            break;
                        }
                        try {
                            this.g0.wait();
                        } catch (InterruptedException unused) {
                            return;
                        }
                    }
                    if (!z) {
                        return;
                    }
                    a = new yb1.a().d((ByteBuffer) zt2.j(this.k0), gv.this.f.b(), gv.this.f.a(), 17).c(this.j0).f(this.i0).e(gv.this.e).a();
                    byteBuffer = this.k0;
                    this.k0 = null;
                }
                try {
                    ((rm0) zt2.j(this.a)).c(a);
                } catch (Exception unused2) {
                } finally {
                    ((Camera) zt2.j(gv.this.c)).addCallbackBuffer(((ByteBuffer) zt2.j(byteBuffer)).array());
                }
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: gv$c */
    /* loaded from: classes.dex */
    public class c implements Camera.PreviewCallback {
        public c() {
        }

        @Override // android.hardware.Camera.PreviewCallback
        public final void onPreviewFrame(byte[] bArr, Camera camera) {
            gv.this.m.c(bArr, camera);
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: gv$d */
    /* loaded from: classes.dex */
    public static class d {
        public bq3 a;
        public bq3 b;

        public d(Camera.Size size, Camera.Size size2) {
            this.a = new bq3(size.width, size.height);
            if (size2 != null) {
                this.b = new bq3(size2.width, size2.height);
            }
        }

        public final bq3 a() {
            return this.a;
        }

        public final bq3 b() {
            return this.b;
        }
    }

    public gv() {
        this.b = new Object();
        this.d = 0;
        this.g = 30.0f;
        this.h = RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
        this.i = 768;
        this.j = false;
        this.n = new IdentityHashMap<>();
    }

    public void a() {
        synchronized (this.b) {
            c();
            this.m.a();
        }
    }

    @RecentlyNonNull
    public gv b(@RecentlyNonNull SurfaceHolder surfaceHolder) throws IOException {
        synchronized (this.b) {
            if (this.c != null) {
                return this;
            }
            Camera g = g();
            this.c = g;
            g.setPreviewDisplay(surfaceHolder);
            this.c.startPreview();
            this.l = new Thread(this.m);
            this.m.b(true);
            Thread thread = this.l;
            if (thread != null) {
                thread.start();
            }
            return this;
        }
    }

    public void c() {
        synchronized (this.b) {
            this.m.b(false);
            Thread thread = this.l;
            if (thread != null) {
                try {
                    thread.join();
                } catch (InterruptedException unused) {
                }
                this.l = null;
            }
            Camera camera = this.c;
            if (camera != null) {
                camera.stopPreview();
                this.c.setPreviewCallbackWithBuffer(null);
                try {
                    this.c.setPreviewTexture(null);
                    this.c.setPreviewDisplay(null);
                } catch (Exception e) {
                    String valueOf = String.valueOf(e);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 32);
                    sb.append("Failed to clear camera preview: ");
                    sb.append(valueOf);
                }
                ((Camera) zt2.j(this.c)).release();
                this.c = null;
            }
            this.n.clear();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:58:0x018f  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x0199  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x01af  */
    @android.annotation.SuppressLint({"InlinedApi"})
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final android.hardware.Camera g() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 565
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.gv.g():android.hardware.Camera");
    }

    @SuppressLint({"InlinedApi"})
    public final byte[] i(bq3 bq3Var) {
        byte[] bArr = new byte[((int) Math.ceil(((bq3Var.a() * bq3Var.b()) * ImageFormat.getBitsPerPixel(17)) / 8.0d)) + 1];
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        if (wrap.hasArray() && wrap.array() == bArr) {
            this.n.put(bArr, wrap);
            return bArr;
        }
        throw new IllegalStateException("Failed to create valid buffer for camera source.");
    }
}
