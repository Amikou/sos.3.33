package defpackage;

import android.graphics.Path;

/* compiled from: PathExtensions.kt */
/* renamed from: wp2  reason: default package */
/* loaded from: classes2.dex */
public final class wp2 {
    public static final void a(Path path, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        fs1.f(path, "<this>");
        float f = i;
        float f2 = i2;
        path.moveTo(f, f2);
        path.lineTo(i3, i4);
        path.lineTo(i5, i6);
        path.lineTo(i7, i8);
        path.lineTo(f, f2);
    }

    public static final void b(Path path, int i, int i2, int i3, int i4) {
        fs1.f(path, "<this>");
        float f = i;
        float f2 = i2;
        path.moveTo(f, f2);
        float f3 = i3;
        path.lineTo(f3, f2);
        float f4 = i4;
        path.lineTo(f3, f4);
        path.lineTo(f, f4);
        path.lineTo(f, f2);
    }

    public static final void c(Path path, int i, int i2, int i3, int i4, int i5, int i6) {
        fs1.f(path, "<this>");
        float f = i;
        float f2 = i2;
        path.moveTo(f, f2);
        path.lineTo(i3, i4);
        path.lineTo(i5, i6);
        path.lineTo(f, f2);
    }
}
