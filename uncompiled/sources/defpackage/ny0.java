package defpackage;

import java.util.concurrent.CancellationException;

/* compiled from: Exceptions.kt */
/* renamed from: ny0  reason: default package */
/* loaded from: classes2.dex */
public final class ny0 {
    public static final CancellationException a(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }
}
