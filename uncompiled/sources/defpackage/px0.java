package defpackage;

import java.util.ArrayList;
import java.util.List;
import org.web3j.abi.TypeReference;
import org.web3j.abi.d;

/* compiled from: Event.java */
/* renamed from: px0  reason: default package */
/* loaded from: classes3.dex */
public class px0 {
    private String name;
    private List<TypeReference<zc4>> parameters;

    public px0(String str, List<TypeReference<?>> list) {
        this.name = str;
        this.parameters = d.convert(list);
    }

    public List<TypeReference<zc4>> getIndexedParameters() {
        ArrayList arrayList = new ArrayList(this.parameters.size());
        for (TypeReference<zc4> typeReference : this.parameters) {
            if (typeReference.isIndexed()) {
                arrayList.add(typeReference);
            }
        }
        return arrayList;
    }

    public String getName() {
        return this.name;
    }

    public List<TypeReference<zc4>> getNonIndexedParameters() {
        ArrayList arrayList = new ArrayList(this.parameters.size());
        for (TypeReference<zc4> typeReference : this.parameters) {
            if (!typeReference.isIndexed()) {
                arrayList.add(typeReference);
            }
        }
        return arrayList;
    }

    public List<TypeReference<zc4>> getParameters() {
        return this.parameters;
    }
}
