package defpackage;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import defpackage.lt2;
import java.lang.ref.WeakReference;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Pair;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;

/* compiled from: PopListTokenType.kt */
/* renamed from: lt2  reason: default package */
/* loaded from: classes2.dex */
public final class lt2 {
    public static final lt2 a = new lt2();

    /* compiled from: PopListTokenType.kt */
    /* renamed from: lt2$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(TokenType tokenType);
    }

    /* compiled from: PopListTokenType.kt */
    /* renamed from: lt2$b */
    /* loaded from: classes2.dex */
    public static final class b<T> extends ArrayAdapter<T> {
        public final List<T> a;
        public final List<T> f0;
        public final T g0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public b(Context context, int i, List<? extends T> list, List<? extends T> list2, T t) {
            super(context, i, (int) R.id.text, list);
            fs1.f(context, "context");
            fs1.f(list, "list");
            fs1.f(list2, "listOfToken");
            this.a = list;
            this.f0 = list2;
            this.g0 = t;
        }

        public final List<T> a() {
            return this.a;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            fs1.f(viewGroup, "parent");
            View view2 = super.getView(i, view, viewGroup);
            fs1.e(view2, "super.getView(position, convertView, parent)");
            ImageView imageView = (ImageView) view2.findViewById(R.id.dropDownIcon);
            ImageView imageView2 = (ImageView) view2.findViewById(R.id.iconChainImage);
            if (i == 0) {
                view2.setBackgroundResource(R.drawable.pop_menu_top_corner);
            } else if (i == a().size() - 1) {
                view2.setBackgroundResource(R.drawable.pop_menu_bottom_corner);
            } else {
                view2.setBackgroundResource(0);
            }
            imageView.setImageResource(i == 0 ? R.drawable.ic_baseline_keyboard_arrow_up_24 : 0);
            if (this.f0.get(i) instanceof TokenType) {
                T t = this.f0.get(i);
                Objects.requireNonNull(t, "null cannot be cast to non-null type net.safemoon.androidwallet.common.TokenType");
                imageView2.setImageResource(((TokenType) t).getIcon());
            }
            Drawable r = androidx.core.graphics.drawable.a.r(view2.getBackground());
            if (r != null) {
                if (fs1.b(this.g0, this.f0.get(i))) {
                    androidx.core.graphics.drawable.a.n(r, m70.d(view2.getContext(), R.color.dropdown_selected_color));
                } else {
                    androidx.core.graphics.drawable.a.n(r, m70.d(view2.getContext(), R.color.dropdown_item_color));
                }
            }
            return view2;
        }
    }

    /* compiled from: Comparisons.kt */
    /* renamed from: lt2$c */
    /* loaded from: classes2.dex */
    public static final class c<T> implements Comparator {
        public final /* synthetic */ TokenType a;

        public c(TokenType tokenType) {
            this.a = tokenType;
        }

        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return l30.a(Boolean.valueOf(((Pair) t2).getSecond() == this.a), Boolean.valueOf(((Pair) t).getSecond() == this.a));
        }
    }

    public static final void d(a aVar, Map map, List list, ListPopupWindow listPopupWindow, AdapterView adapterView, View view, int i, long j) {
        fs1.f(aVar, "$listener");
        fs1.f(map, "$items");
        fs1.f(list, "$displayNameList");
        fs1.f(listPopupWindow, "$listPopupWindow");
        Object obj = map.get(list.get(i));
        fs1.d(obj);
        aVar.a((TokenType) obj);
        listPopupWindow.dismiss();
    }

    public static final void e(rc1 rc1Var) {
        fs1.f(rc1Var, "$onClosedListener");
        rc1Var.invoke();
    }

    public final ListPopupWindow c(WeakReference<Activity> weakReference, View view, Map<String, ? extends TokenType> map, TokenType tokenType, final a aVar, final rc1<te4> rc1Var) {
        fs1.f(weakReference, "activityReference");
        fs1.f(view, "anchorView");
        fs1.f(map, "_items");
        fs1.f(tokenType, "itemSelect");
        fs1.f(aVar, "listener");
        fs1.f(rc1Var, "onClosedListener");
        Activity activity = weakReference.get();
        if (activity == null) {
            return null;
        }
        final ListPopupWindow listPopupWindow = new ListPopupWindow(activity, null, R.attr.listPopupWindowStyle);
        listPopupWindow.setContentWidth(activity.getResources().getDimensionPixelSize(R.dimen._150sdp));
        listPopupWindow.setAnchorView(view);
        final Map i = z32.i(j20.e0(a42.n(map), new c(tokenType)));
        final List k0 = j20.k0(i.keySet());
        listPopupWindow.setAdapter(new b(activity, R.layout.list_popup_token_window_item, k0, j20.k0(i.values()), tokenType));
        listPopupWindow.setBackgroundDrawable(g83.f(activity.getResources(), R.drawable.pop_menu_corner, null));
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: jt2
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view2, int i2, long j) {
                lt2.d(lt2.a.this, i, k0, listPopupWindow, adapterView, view2, i2, j);
            }
        });
        listPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: kt2
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                lt2.e(rc1.this);
            }
        });
        return listPopupWindow;
    }
}
