package defpackage;

import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentTermScreenWebViewBinding.java */
/* renamed from: fb1  reason: default package */
/* loaded from: classes2.dex */
public final class fb1 {
    public final ConstraintLayout a;
    public final AppCompatButton b;
    public final LinearLayout c;
    public final ProgressBar d;
    public final rp3 e;
    public final WebView f;

    public fb1(ConstraintLayout constraintLayout, AppCompatButton appCompatButton, ConstraintLayout constraintLayout2, LinearLayout linearLayout, ProgressBar progressBar, rp3 rp3Var, WebView webView) {
        this.a = constraintLayout;
        this.b = appCompatButton;
        this.c = linearLayout;
        this.d = progressBar;
        this.e = rp3Var;
        this.f = webView;
    }

    public static fb1 a(View view) {
        int i = R.id.btnTryAgain;
        AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btnTryAgain);
        if (appCompatButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.lErrorMsgContainer;
            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.lErrorMsgContainer);
            if (linearLayout != null) {
                i = R.id.pbContentLoading;
                ProgressBar progressBar = (ProgressBar) ai4.a(view, R.id.pbContentLoading);
                if (progressBar != null) {
                    i = R.id.toolbar;
                    View a = ai4.a(view, R.id.toolbar);
                    if (a != null) {
                        rp3 a2 = rp3.a(a);
                        i = R.id.wvContent;
                        WebView webView = (WebView) ai4.a(view, R.id.wvContent);
                        if (webView != null) {
                            return new fb1(constraintLayout, appCompatButton, constraintLayout, linearLayout, progressBar, a2, webView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
