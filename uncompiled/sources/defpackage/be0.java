package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.gms.tasks.c;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.crashlytics.internal.common.j;

/* compiled from: DataCollectionArbiter.java */
/* renamed from: be0  reason: default package */
/* loaded from: classes2.dex */
public class be0 {
    public final SharedPreferences a;
    public final c51 b;
    public final Object c;
    public n34<Void> d;
    public boolean e;
    public Boolean f;
    public final n34<Void> g;

    public be0(c51 c51Var) {
        Object obj = new Object();
        this.c = obj;
        this.d = new n34<>();
        this.e = false;
        this.g = new n34<>();
        Context h = c51Var.h();
        this.b = c51Var;
        this.a = CommonUtils.r(h);
        Boolean b = b();
        this.f = b == null ? a(h) : b;
        synchronized (obj) {
            if (d()) {
                this.d.e(null);
            }
        }
    }

    public static Boolean f(Context context) {
        ApplicationInfo applicationInfo;
        Bundle bundle;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128)) == null || (bundle = applicationInfo.metaData) == null || !bundle.containsKey("firebase_crashlytics_collection_enabled")) {
                return null;
            }
            return Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_crashlytics_collection_enabled"));
        } catch (PackageManager.NameNotFoundException e) {
            w12.f().e("Could not read data collection permission from manifest", e);
            return null;
        }
    }

    public final Boolean a(Context context) {
        Boolean f = f(context);
        if (f == null) {
            this.e = false;
            return null;
        }
        this.e = true;
        return Boolean.valueOf(Boolean.TRUE.equals(f));
    }

    public final Boolean b() {
        if (this.a.contains("firebase_crashlytics_collection_enabled")) {
            this.e = false;
            return Boolean.valueOf(this.a.getBoolean("firebase_crashlytics_collection_enabled", true));
        }
        return null;
    }

    public void c(boolean z) {
        if (z) {
            this.g.e(null);
            return;
        }
        throw new IllegalStateException("An invalid data collection token was used.");
    }

    public synchronized boolean d() {
        boolean q;
        Boolean bool = this.f;
        if (bool != null) {
            q = bool.booleanValue();
        } else {
            q = this.b.q();
        }
        e(q);
        return q;
    }

    public final void e(boolean z) {
        String str;
        String str2 = z ? "ENABLED" : "DISABLED";
        if (this.f == null) {
            str = "global Firebase setting";
        } else {
            str = this.e ? "firebase_crashlytics_collection_enabled manifest flag" : "API";
        }
        w12.f().b(String.format("Crashlytics automatic data collection %s by %s.", str2, str));
    }

    public c<Void> g() {
        c<Void> a;
        synchronized (this.c) {
            a = this.d.a();
        }
        return a;
    }

    public c<Void> h() {
        return j.e(this.g.a(), g());
    }
}
