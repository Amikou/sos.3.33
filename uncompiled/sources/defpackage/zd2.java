package defpackage;

import androidx.lifecycle.l;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

/* compiled from: NavControllerViewModel.java */
/* renamed from: zd2  reason: default package */
/* loaded from: classes.dex */
public class zd2 extends dj4 {
    public static final l.b b = new a();
    public final HashMap<UUID, gj4> a = new HashMap<>();

    /* compiled from: NavControllerViewModel.java */
    /* renamed from: zd2$a */
    /* loaded from: classes.dex */
    public class a implements l.b {
        @Override // androidx.lifecycle.l.b
        public <T extends dj4> T a(Class<T> cls) {
            return new zd2();
        }
    }

    public static zd2 b(gj4 gj4Var) {
        return (zd2) new l(gj4Var, b).a(zd2.class);
    }

    public void a(UUID uuid) {
        gj4 remove = this.a.remove(uuid);
        if (remove != null) {
            remove.a();
        }
    }

    public gj4 c(UUID uuid) {
        gj4 gj4Var = this.a.get(uuid);
        if (gj4Var == null) {
            gj4 gj4Var2 = new gj4();
            this.a.put(uuid, gj4Var2);
            return gj4Var2;
        }
        return gj4Var;
    }

    @Override // defpackage.dj4
    public void onCleared() {
        for (gj4 gj4Var : this.a.values()) {
            gj4Var.a();
        }
        this.a.clear();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("NavControllerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} ViewModelStores (");
        Iterator<UUID> it = this.a.keySet().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}
