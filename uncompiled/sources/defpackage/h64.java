package defpackage;

import zendesk.support.request.CellBase;

/* compiled from: TimestampAdjuster.java */
/* renamed from: h64  reason: default package */
/* loaded from: classes.dex */
public final class h64 {
    public long a;
    public long b;
    public long c;
    public final ThreadLocal<Long> d = new ThreadLocal<>();

    public h64(long j) {
        g(j);
    }

    public static long f(long j) {
        return (j * 1000000) / 90000;
    }

    public static long h(long j) {
        return (j * 90000) / 1000000;
    }

    public synchronized long a(long j) {
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        if (this.b == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            long j2 = this.a;
            if (j2 == 9223372036854775806L) {
                j2 = ((Long) ii.e(this.d.get())).longValue();
            }
            this.b = j2 - j;
            notifyAll();
        }
        this.c = j;
        return j + this.b;
    }

    public synchronized long b(long j) {
        if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            return CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        long j2 = this.c;
        if (j2 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            long h = h(j2);
            long j3 = (4294967296L + h) / 8589934592L;
            long j4 = ((j3 - 1) * 8589934592L) + j;
            j += j3 * 8589934592L;
            if (Math.abs(j4 - h) < Math.abs(j - h)) {
                j = j4;
            }
        }
        return a(f(j));
    }

    public synchronized long c() {
        long j;
        j = this.a;
        if (j == Long.MAX_VALUE || j == 9223372036854775806L) {
            j = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        }
        return j;
    }

    public synchronized long d() {
        long c;
        long j = this.c;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            c = j + this.b;
        } else {
            c = c();
        }
        return c;
    }

    public synchronized long e() {
        return this.b;
    }

    public synchronized void g(long j) {
        this.a = j;
        this.b = j == Long.MAX_VALUE ? 0L : -9223372036854775807L;
        this.c = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
