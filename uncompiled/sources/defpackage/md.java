package defpackage;

import android.graphics.Bitmap;
import android.graphics.Rect;
import com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor;

/* compiled from: AnimatedDrawableBackendFrameRenderer.java */
/* renamed from: md  reason: default package */
/* loaded from: classes.dex */
public class md implements aq {
    public static final Class<?> e = md.class;
    public final xp a;
    public kd b;
    public AnimatedImageCompositor c;
    public final AnimatedImageCompositor.b d;

    /* compiled from: AnimatedDrawableBackendFrameRenderer.java */
    /* renamed from: md$a */
    /* loaded from: classes.dex */
    public class a implements AnimatedImageCompositor.b {
        public a() {
        }

        @Override // com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor.b
        public void a(int i, Bitmap bitmap) {
        }

        @Override // com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor.b
        public com.facebook.common.references.a<Bitmap> b(int i) {
            return md.this.a.d(i);
        }
    }

    public md(xp xpVar, kd kdVar) {
        a aVar = new a();
        this.d = aVar;
        this.a = xpVar;
        this.b = kdVar;
        this.c = new AnimatedImageCompositor(kdVar, aVar);
    }

    @Override // defpackage.aq
    public boolean a(int i, Bitmap bitmap) {
        try {
            this.c.g(i, bitmap);
            return true;
        } catch (IllegalStateException e2) {
            v11.g(e, e2, "Rendering of frame unsuccessful. Frame number: %d", Integer.valueOf(i));
            return false;
        }
    }

    @Override // defpackage.aq
    public int c() {
        return this.b.getHeight();
    }

    @Override // defpackage.aq
    public void d(Rect rect) {
        kd f = this.b.f(rect);
        if (f != this.b) {
            this.b = f;
            this.c = new AnimatedImageCompositor(f, this.d);
        }
    }

    @Override // defpackage.aq
    public int e() {
        return this.b.getWidth();
    }
}
