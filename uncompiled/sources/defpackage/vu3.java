package defpackage;

import androidx.annotation.RecentlyNonNull;
import java.util.regex.Pattern;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: vu3  reason: default package */
/* loaded from: classes.dex */
public class vu3 {
    static {
        Pattern.compile("\\$\\{(.*?)\\}");
    }

    @RecentlyNonNull
    public static boolean a(String str) {
        return str == null || str.trim().isEmpty();
    }
}
