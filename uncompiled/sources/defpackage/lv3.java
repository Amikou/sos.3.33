package defpackage;

import androidx.media3.common.util.b;
import java.util.Collections;
import java.util.List;

/* compiled from: SubripSubtitle.java */
/* renamed from: lv3  reason: default package */
/* loaded from: classes.dex */
public final class lv3 implements qv3 {
    public final kb0[] a;
    public final long[] f0;

    public lv3(kb0[] kb0VarArr, long[] jArr) {
        this.a = kb0VarArr;
        this.f0 = jArr;
    }

    @Override // defpackage.qv3
    public int a(long j) {
        int e = b.e(this.f0, j, false, false);
        if (e < this.f0.length) {
            return e;
        }
        return -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        ii.a(i >= 0);
        ii.a(i < this.f0.length);
        return this.f0[i];
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        int i = b.i(this.f0, j, true, false);
        if (i != -1) {
            kb0[] kb0VarArr = this.a;
            if (kb0VarArr[i] != kb0.v0) {
                return Collections.singletonList(kb0VarArr[i]);
            }
        }
        return Collections.emptyList();
    }

    @Override // defpackage.qv3
    public int f() {
        return this.f0.length;
    }
}
