package defpackage;

import androidx.work.b;
import java.util.List;

/* compiled from: InputMerger.java */
/* renamed from: yq1  reason: default package */
/* loaded from: classes.dex */
public abstract class yq1 {
    public static final String a = v12.f("InputMerger");

    public static yq1 a(String str) {
        try {
            return (yq1) Class.forName(str).newInstance();
        } catch (Exception e) {
            v12 c = v12.c();
            String str2 = a;
            c.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    public abstract b b(List<b> list);
}
