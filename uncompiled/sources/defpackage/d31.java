package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.fiat.gson.Fiat;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: FiatTokenDataSource.kt */
/* renamed from: d31  reason: default package */
/* loaded from: classes2.dex */
public final class d31 implements yl1 {
    public final a31 a;

    public d31(a31 a31Var) {
        fs1.f(a31Var, "fiatDao");
        this.a = a31Var;
    }

    @Override // defpackage.yl1
    public LiveData<List<RoomFiat>> a() {
        return this.a.a();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.yl1
    public Object b(Fiat[] fiatArr, q70<? super te4> q70Var) {
        for (Fiat fiat : fiatArr) {
            if (!this.a.e(fiat.getSymbol())) {
                this.a.c(new RoomFiat(fiat));
            } else {
                String name = fiat.getName();
                if (name != null) {
                    this.a.d(fiat.getSymbol(), name);
                }
                Double rate = fiat.getRate();
                if (rate != null) {
                    this.a.b(fiat.getSymbol(), rate.doubleValue());
                }
            }
        }
        return fiatArr == gs1.d() ? fiatArr : te4.a;
    }
}
