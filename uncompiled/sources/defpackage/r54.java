package defpackage;

import com.facebook.imagepipeline.image.EncodedImage;
import com.facebook.imagepipeline.producers.ThumbnailProducer;

/* compiled from: ThumbnailBranchProducer.java */
/* renamed from: r54  reason: default package */
/* loaded from: classes.dex */
public class r54 implements dv2<zu0> {
    public final ThumbnailProducer<EncodedImage>[] a;

    public r54(ThumbnailProducer<EncodedImage>... thumbnailProducerArr) {
        s54[] s54VarArr = (s54[]) xt2.g(thumbnailProducerArr);
        this.a = s54VarArr;
        xt2.e(0, s54VarArr.length);
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        ev2Var.c().q();
        l60Var.d(null, 1);
    }
}
