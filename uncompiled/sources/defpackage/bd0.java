package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: CutoutDrawable.java */
/* renamed from: bd0  reason: default package */
/* loaded from: classes2.dex */
public class bd0 extends o42 {
    public final Paint C0;
    public final RectF D0;
    public int E0;

    public bd0() {
        this(null);
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        s0(canvas);
        super.draw(canvas);
        canvas.drawRect(this.D0, this.C0);
        r0(canvas);
    }

    public boolean q0() {
        return !this.D0.isEmpty();
    }

    public final void r0(Canvas canvas) {
        if (y0(getCallback())) {
            return;
        }
        canvas.restoreToCount(this.E0);
    }

    public final void s0(Canvas canvas) {
        Drawable.Callback callback = getCallback();
        if (y0(callback)) {
            View view = (View) callback;
            if (view.getLayerType() != 2) {
                view.setLayerType(2, null);
                return;
            }
            return;
        }
        u0(canvas);
    }

    public void t0() {
        v0(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON);
    }

    public final void u0(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.E0 = canvas.saveLayer(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, canvas.getWidth(), canvas.getHeight(), null);
        } else {
            this.E0 = canvas.saveLayer(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, canvas.getWidth(), canvas.getHeight(), null, 31);
        }
    }

    public void v0(float f, float f2, float f3, float f4) {
        RectF rectF = this.D0;
        if (f == rectF.left && f2 == rectF.top && f3 == rectF.right && f4 == rectF.bottom) {
            return;
        }
        rectF.set(f, f2, f3, f4);
        invalidateSelf();
    }

    public void w0(RectF rectF) {
        v0(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    public final void x0() {
        this.C0.setStyle(Paint.Style.FILL_AND_STROKE);
        this.C0.setColor(-1);
        this.C0.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    public final boolean y0(Drawable.Callback callback) {
        return callback instanceof View;
    }

    public bd0(pn3 pn3Var) {
        super(pn3Var == null ? new pn3() : pn3Var);
        this.C0 = new Paint(1);
        x0();
        this.D0 = new RectF();
    }
}
