package defpackage;

import android.os.RemoteException;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: gr5  reason: default package */
/* loaded from: classes.dex */
public final class gr5 implements Runnable {
    public final /* synthetic */ bq5 a;
    public final /* synthetic */ p f0;

    public gr5(p pVar, bq5 bq5Var) {
        this.f0 = pVar;
        this.a = bq5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d dVar;
        dVar = this.f0.d;
        if (dVar != null) {
            try {
                bq5 bq5Var = this.a;
                if (bq5Var == null) {
                    dVar.Q(0L, null, null, this.f0.a.m().getPackageName());
                } else {
                    dVar.Q(bq5Var.c, bq5Var.a, bq5Var.b, this.f0.a.m().getPackageName());
                }
                this.f0.D();
                return;
            } catch (RemoteException e) {
                this.f0.a.w().l().b("Failed to send current screen to the service", e);
                return;
            }
        }
        this.f0.a.w().l().a("Failed to send current screen to service");
    }
}
