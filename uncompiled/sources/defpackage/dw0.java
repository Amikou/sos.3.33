package defpackage;

/* compiled from: ErrorResponseAdapter.java */
/* renamed from: dw0  reason: default package */
/* loaded from: classes2.dex */
public class dw0 implements cw0 {
    public final String a;

    public dw0(String str) {
        this.a = str;
    }

    @Override // defpackage.cw0
    public int e() {
        return -1;
    }

    @Override // defpackage.cw0
    public String f() {
        return "text/plain; charset=UTF8";
    }

    @Override // defpackage.cw0
    public String g() {
        return "";
    }

    @Override // defpackage.cw0
    public String h() {
        return this.a;
    }

    @Override // defpackage.cw0
    public boolean i() {
        return false;
    }

    @Override // defpackage.cw0
    public boolean j() {
        return false;
    }

    @Override // defpackage.cw0
    public String k() {
        return this.a;
    }
}
