package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;
import defpackage.l80;

/* compiled from: ImagePerfState.java */
/* renamed from: po1  reason: default package */
/* loaded from: classes.dex */
public class po1 {
    public String A;
    public uo0 B;
    public l80.a C;
    public String a;
    public String b;
    public ImageRequest c;
    public Object d;
    public ao1 e;
    public ImageRequest f;
    public ImageRequest g;
    public ImageRequest[] h;
    public String q;
    public boolean r;
    public Throwable u;
    public long i = -1;
    public long j = -1;
    public long k = -1;
    public long l = -1;
    public long m = -1;
    public long n = -1;
    public long o = -1;
    public int p = 1;
    public int s = -1;
    public int t = -1;
    public int v = -1;
    public int w = -1;
    public long x = -1;
    public long y = -1;
    public long z = -1;

    public void A(boolean z) {
        this.w = z ? 1 : 2;
    }

    public jo1 B() {
        return new jo1(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t, this.u, this.w, this.x, this.y, this.A, this.z, this.B, this.C);
    }

    public int a() {
        return this.v;
    }

    public void b() {
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.p = 1;
        this.q = null;
        this.r = false;
        this.s = -1;
        this.t = -1;
        this.u = null;
        this.v = -1;
        this.w = -1;
        this.A = null;
        this.C = null;
        c();
    }

    public void c() {
        this.n = -1L;
        this.o = -1L;
        this.i = -1L;
        this.k = -1L;
        this.l = -1L;
        this.m = -1L;
        this.x = -1L;
        this.y = -1L;
        this.z = -1L;
    }

    public void d(Object obj) {
        this.d = obj;
    }

    public void e(long j) {
        this.m = j;
    }

    public void f(long j) {
        this.l = j;
    }

    public void g(long j) {
        this.k = j;
    }

    public void h(String str) {
        this.a = str;
    }

    public void i(ImageRequest imageRequest, ImageRequest imageRequest2, ImageRequest[] imageRequestArr) {
        this.f = imageRequest;
        this.g = imageRequest2;
        this.h = imageRequestArr;
    }

    public void j(long j) {
        this.j = j;
    }

    public void k(long j) {
        this.i = j;
    }

    public void l(Throwable th) {
        this.u = th;
    }

    public void m(l80.a aVar) {
        this.C = aVar;
    }

    public void n(ao1 ao1Var) {
        this.e = ao1Var;
    }

    public void o(int i) {
        this.v = i;
    }

    public void p(int i) {
        this.p = i;
    }

    public void q(ImageRequest imageRequest) {
        this.c = imageRequest;
    }

    public void r(long j) {
        this.o = j;
    }

    public void s(long j) {
        this.n = j;
    }

    public void t(long j) {
        this.y = j;
    }

    public void u(int i) {
        this.t = i;
    }

    public void v(int i) {
        this.s = i;
    }

    public void w(boolean z) {
        this.r = z;
    }

    public void x(String str) {
        this.b = str;
    }

    public void y(String str) {
        this.q = str;
    }

    public void z(long j) {
        this.x = j;
    }
}
