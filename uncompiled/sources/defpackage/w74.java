package defpackage;

import androidx.media3.common.j;

/* compiled from: Track.java */
/* renamed from: w74  reason: default package */
/* loaded from: classes.dex */
public final class w74 {
    public final int a;
    public final int b;
    public final long c;
    public final long d;
    public final long e;
    public final j f;
    public final int g;
    public final long[] h;
    public final long[] i;
    public final int j;
    public final x74[] k;

    public w74(int i, int i2, long j, long j2, long j3, j jVar, int i3, x74[] x74VarArr, int i4, long[] jArr, long[] jArr2) {
        this.a = i;
        this.b = i2;
        this.c = j;
        this.d = j2;
        this.e = j3;
        this.f = jVar;
        this.g = i3;
        this.k = x74VarArr;
        this.j = i4;
        this.h = jArr;
        this.i = jArr2;
    }

    public x74 a(int i) {
        x74[] x74VarArr = this.k;
        if (x74VarArr == null) {
            return null;
        }
        return x74VarArr[i];
    }
}
