package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import defpackage.wi3;
import java.io.IOException;
import java.util.ArrayList;
import zendesk.support.request.CellBase;

/* compiled from: AviExtractor.java */
/* renamed from: tl  reason: default package */
/* loaded from: classes.dex */
public final class tl implements p11 {
    public int c;
    public ul e;
    public long h;
    public qy i;
    public int m;
    public boolean n;
    public final op2 a = new op2(12);
    public final c b = new c();
    public r11 d = new js0();
    public qy[] g = new qy[0];
    public long k = -1;
    public long l = -1;
    public int j = -1;
    public long f = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;

    /* compiled from: AviExtractor.java */
    /* renamed from: tl$b */
    /* loaded from: classes.dex */
    public class b implements wi3 {
        public final long a;

        public b(long j) {
            this.a = j;
        }

        @Override // defpackage.wi3
        public boolean e() {
            return true;
        }

        @Override // defpackage.wi3
        public wi3.a h(long j) {
            wi3.a i = tl.this.g[0].i(j);
            for (int i2 = 1; i2 < tl.this.g.length; i2++) {
                wi3.a i3 = tl.this.g[i2].i(j);
                if (i3.a.b < i.a.b) {
                    i = i3;
                }
            }
            return i;
        }

        @Override // defpackage.wi3
        public long i() {
            return this.a;
        }
    }

    /* compiled from: AviExtractor.java */
    /* renamed from: tl$c */
    /* loaded from: classes.dex */
    public static class c {
        public int a;
        public int b;
        public int c;

        public c() {
        }

        public void a(op2 op2Var) {
            this.a = op2Var.q();
            this.b = op2Var.q();
            this.c = 0;
        }

        public void b(op2 op2Var) throws ParserException {
            a(op2Var);
            if (this.a == 1414744396) {
                this.c = op2Var.q();
                return;
            }
            throw ParserException.createForMalformedContainer("LIST expected, found: " + this.a, null);
        }
    }

    public static void d(q11 q11Var) throws IOException {
        if ((q11Var.getPosition() & 1) == 1) {
            q11Var.k(1);
        }
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.h = -1L;
        this.i = null;
        for (qy qyVar : this.g) {
            qyVar.o(j);
        }
        if (j == 0) {
            if (this.g.length == 0) {
                this.c = 0;
                return;
            } else {
                this.c = 3;
                return;
            }
        }
        this.c = 6;
    }

    public final qy e(int i) {
        qy[] qyVarArr;
        for (qy qyVar : this.g) {
            if (qyVar.j(i)) {
                return qyVar;
            }
        }
        return null;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        if (n(q11Var, ot2Var)) {
            return 1;
        }
        switch (this.c) {
            case 0:
                if (g(q11Var)) {
                    q11Var.k(12);
                    this.c = 1;
                    return 0;
                }
                throw ParserException.createForMalformedContainer("AVI Header List not found", null);
            case 1:
                q11Var.readFully(this.a.d(), 0, 12);
                this.a.P(0);
                this.b.b(this.a);
                c cVar = this.b;
                if (cVar.c == 1819436136) {
                    this.j = cVar.b;
                    this.c = 2;
                    return 0;
                }
                throw ParserException.createForMalformedContainer("hdrl expected, found: " + this.b.c, null);
            case 2:
                int i = this.j - 4;
                op2 op2Var = new op2(i);
                q11Var.readFully(op2Var.d(), 0, i);
                h(op2Var);
                this.c = 3;
                return 0;
            case 3:
                if (this.k != -1) {
                    long position = q11Var.getPosition();
                    long j = this.k;
                    if (position != j) {
                        this.h = j;
                        return 0;
                    }
                }
                q11Var.n(this.a.d(), 0, 12);
                q11Var.j();
                this.a.P(0);
                this.b.a(this.a);
                int q = this.a.q();
                int i2 = this.b.a;
                if (i2 == 1179011410) {
                    q11Var.k(12);
                    return 0;
                } else if (i2 == 1414744396 && q == 1769369453) {
                    long position2 = q11Var.getPosition();
                    this.k = position2;
                    this.l = position2 + this.b.b + 8;
                    if (!this.n) {
                        if (((ul) ii.e(this.e)).a()) {
                            this.c = 4;
                            this.h = this.l;
                            return 0;
                        }
                        this.d.p(new wi3.b(this.f));
                        this.n = true;
                    }
                    this.h = q11Var.getPosition() + 12;
                    this.c = 6;
                    return 0;
                } else {
                    this.h = q11Var.getPosition() + this.b.b + 8;
                    return 0;
                }
            case 4:
                q11Var.readFully(this.a.d(), 0, 8);
                this.a.P(0);
                int q2 = this.a.q();
                int q3 = this.a.q();
                if (q2 == 829973609) {
                    this.c = 5;
                    this.m = q3;
                } else {
                    this.h = q11Var.getPosition() + q3;
                }
                return 0;
            case 5:
                op2 op2Var2 = new op2(this.m);
                q11Var.readFully(op2Var2.d(), 0, this.m);
                i(op2Var2);
                this.c = 6;
                this.h = this.k;
                return 0;
            case 6:
                return m(q11Var);
            default:
                throw new AssertionError();
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        q11Var.n(this.a.d(), 0, 12);
        this.a.P(0);
        if (this.a.q() != 1179011410) {
            return false;
        }
        this.a.Q(4);
        return this.a.q() == 541677121;
    }

    public final void h(op2 op2Var) throws IOException {
        g02 c2 = g02.c(1819436136, op2Var);
        if (c2.getType() == 1819436136) {
            ul ulVar = (ul) c2.b(ul.class);
            if (ulVar != null) {
                this.e = ulVar;
                this.f = ulVar.c * ulVar.a;
                ArrayList arrayList = new ArrayList();
                af4<sl> it = c2.a.iterator();
                int i = 0;
                while (it.hasNext()) {
                    sl next = it.next();
                    if (next.getType() == 1819440243) {
                        int i2 = i + 1;
                        qy l = l((g02) next, i);
                        if (l != null) {
                            arrayList.add(l);
                        }
                        i = i2;
                    }
                }
                this.g = (qy[]) arrayList.toArray(new qy[0]);
                this.d.m();
                return;
            }
            throw ParserException.createForMalformedContainer("AviHeader not found", null);
        }
        throw ParserException.createForMalformedContainer("Unexpected header list type " + c2.getType(), null);
    }

    public final void i(op2 op2Var) {
        long k = k(op2Var);
        while (op2Var.a() >= 16) {
            int q = op2Var.q();
            int q2 = op2Var.q();
            long q3 = op2Var.q() + k;
            op2Var.q();
            qy e = e(q);
            if (e != null) {
                if ((q2 & 16) == 16) {
                    e.b(q3);
                }
                e.k();
            }
        }
        for (qy qyVar : this.g) {
            qyVar.c();
        }
        this.n = true;
        this.d.p(new b(this.f));
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.c = 0;
        this.d = r11Var;
        this.h = -1L;
    }

    public final long k(op2 op2Var) {
        if (op2Var.a() < 16) {
            return 0L;
        }
        int e = op2Var.e();
        op2Var.Q(8);
        long j = this.k;
        long j2 = ((long) op2Var.q()) <= j ? 8 + j : 0L;
        op2Var.P(e);
        return j2;
    }

    public final qy l(g02 g02Var, int i) {
        vl vlVar = (vl) g02Var.b(vl.class);
        cu3 cu3Var = (cu3) g02Var.b(cu3.class);
        if (vlVar == null) {
            p12.i("AviExtractor", "Missing Stream Header");
            return null;
        } else if (cu3Var == null) {
            p12.i("AviExtractor", "Missing Stream Format");
            return null;
        } else {
            long a2 = vlVar.a();
            j jVar = cu3Var.a;
            j.b b2 = jVar.b();
            b2.R(i);
            int i2 = vlVar.e;
            if (i2 != 0) {
                b2.W(i2);
            }
            eu3 eu3Var = (eu3) g02Var.b(eu3.class);
            if (eu3Var != null) {
                b2.U(eu3Var.a);
            }
            int i3 = y82.i(jVar.p0);
            if (i3 == 1 || i3 == 2) {
                f84 f = this.d.f(i, i3);
                f.f(b2.E());
                qy qyVar = new qy(i, i3, a2, vlVar.d, f);
                this.f = a2;
                return qyVar;
            }
            return null;
        }
    }

    public final int m(q11 q11Var) throws IOException {
        if (q11Var.getPosition() >= this.l) {
            return -1;
        }
        qy qyVar = this.i;
        if (qyVar != null) {
            if (qyVar.m(q11Var)) {
                this.i = null;
            }
        } else {
            d(q11Var);
            q11Var.n(this.a.d(), 0, 12);
            this.a.P(0);
            int q = this.a.q();
            if (q == 1414744396) {
                this.a.P(8);
                q11Var.k(this.a.q() != 1769369453 ? 8 : 12);
                q11Var.j();
                return 0;
            }
            int q2 = this.a.q();
            if (q == 1263424842) {
                this.h = q11Var.getPosition() + q2 + 8;
                return 0;
            }
            q11Var.k(8);
            q11Var.j();
            qy e = e(q);
            if (e == null) {
                this.h = q11Var.getPosition() + q2;
                return 0;
            }
            e.n(q2);
            this.i = e;
        }
        return 0;
    }

    public final boolean n(q11 q11Var, ot2 ot2Var) throws IOException {
        boolean z;
        if (this.h != -1) {
            long position = q11Var.getPosition();
            long j = this.h;
            if (j >= position && j <= 262144 + position) {
                q11Var.k((int) (j - position));
            } else {
                ot2Var.a = j;
                z = true;
                this.h = -1L;
                return z;
            }
        }
        z = false;
        this.h = -1L;
        return z;
    }
}
