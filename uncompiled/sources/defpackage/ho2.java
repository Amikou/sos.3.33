package defpackage;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.o;
import com.google.android.material.imageview.ShapeableImageView;
import defpackage.ho2;
import net.safemoon.androidwallet.model.priceAlert.PAToken;

/* compiled from: PATokenAdapter.kt */
/* renamed from: ho2  reason: default package */
/* loaded from: classes2.dex */
public final class ho2 extends o<PAToken, a> {
    public final tc1<PAToken, te4> a;

    /* compiled from: PATokenAdapter.kt */
    /* renamed from: ho2$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final vs1 a;
        public final /* synthetic */ ho2 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(ho2 ho2Var, vs1 vs1Var) {
            super(vs1Var.b());
            fs1.f(ho2Var, "this$0");
            fs1.f(vs1Var, "binding");
            this.b = ho2Var;
            this.a = vs1Var;
        }

        public static final void c(ho2 ho2Var, PAToken pAToken, View view) {
            fs1.f(ho2Var, "this$0");
            fs1.f(pAToken, "$item");
            ho2Var.a().invoke(pAToken);
        }

        public final void b(final PAToken pAToken) {
            fs1.f(pAToken, "item");
            vs1 vs1Var = this.a;
            final ho2 ho2Var = this.b;
            TextView textView = vs1Var.g;
            textView.setText(pAToken.getName() + " (" + ((Object) pAToken.getSymbol()) + ')');
            com.bumptech.glide.a.u(vs1Var.c).x(pAToken.getIcon()).a(n73.v0()).I0(vs1Var.c);
            ShapeableImageView shapeableImageView = vs1Var.f;
            fs1.e(shapeableImageView, "statusForCheck");
            shapeableImageView.setVisibility(pAToken.getHasPriceAlert() ^ true ? 4 : 0);
            vs1Var.e.setOnClickListener(new View.OnClickListener() { // from class: go2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ho2.a.c(ho2.this, pAToken, view);
                }
            });
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public ho2(tc1<? super PAToken, te4> tc1Var) {
        super(wl1.a);
        fs1.f(tc1Var, "callback");
        this.a = tc1Var;
    }

    public final tc1<PAToken, te4> a() {
        return this.a;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        PAToken item = getItem(i);
        fs1.e(item, "getItem(position)");
        aVar.b(item);
        Log.println(7, "position", String.valueOf(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: c */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        vs1 c = vs1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        fs1.e(c, "inflate(LayoutInflater.f…t.context),parent, false)");
        return new a(this, c);
    }
}
