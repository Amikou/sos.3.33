package defpackage;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import androidx.viewpager2.widget.c;

/* compiled from: FakeDrag.java */
/* renamed from: c21  reason: default package */
/* loaded from: classes.dex */
public final class c21 {
    public final c a;

    public c21(ViewPager2 viewPager2, c cVar, RecyclerView recyclerView) {
        this.a = cVar;
    }

    public boolean a() {
        return this.a.g();
    }
}
