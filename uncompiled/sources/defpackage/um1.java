package defpackage;

/* compiled from: ITokenExtraParams.kt */
/* renamed from: um1  reason: default package */
/* loaded from: classes2.dex */
public final class um1 {
    public static final tm1 a(String str) {
        fs1.f(str, "<this>");
        tm1 tm1Var = new tm1();
        switch (str.hashCode()) {
            case -1745273226:
                if (str.equals("BEP_BUSD")) {
                    tm1Var.e(true);
                    tm1Var.h("busd_bsc");
                    break;
                }
                break;
            case -1425685164:
                if (str.equals("AVALANCHE_C_AVAX")) {
                    tm1Var.e(true);
                    tm1Var.h("avax_cchain");
                    break;
                }
                break;
            case -774393741:
                if (str.equals("ERC_AXS")) {
                    tm1Var.e(true);
                    tm1Var.h("axs");
                    break;
                }
                break;
            case -774391581:
                if (str.equals("ERC_DAI")) {
                    tm1Var.e(true);
                    tm1Var.h("dai");
                    break;
                }
                break;
            case -774390032:
                if (str.equals("ERC_ETH")) {
                    tm1Var.e(true);
                    tm1Var.h("eth");
                    break;
                }
                break;
            case -774374841:
                if (str.equals("ERC_UNI")) {
                    tm1Var.e(true);
                    tm1Var.h("uni");
                    break;
                }
                break;
            case -62428427:
                if (str.equals("POLYGON_MATIC")) {
                    tm1Var.e(true);
                    tm1Var.h("matic_polygon");
                    break;
                }
                break;
            case 295942048:
                if (str.equals("BEP_SAFEMOON")) {
                    tm1Var.g(false);
                    tm1Var.f(false);
                    break;
                }
                break;
            case 413883298:
                if (str.equals("POLYGON_USDC")) {
                    tm1Var.e(true);
                    tm1Var.h("usdc_polygon");
                    break;
                }
                break;
            case 497889956:
                if (str.equals("BEP_BNB")) {
                    tm1Var.e(true);
                    tm1Var.h("bnb_bsc");
                    break;
                }
                break;
            case 1763575864:
                if (str.equals("ERC_AAVE")) {
                    tm1Var.e(true);
                    tm1Var.h("aave");
                    break;
                }
                break;
            case 1763933104:
                if (str.equals("ERC_MANA")) {
                    tm1Var.e(true);
                    tm1Var.h("mana");
                    break;
                }
                break;
            case 1764188422:
                if (str.equals("ERC_USDC")) {
                    tm1Var.e(true);
                    tm1Var.h("usdc");
                    break;
                }
                break;
            case 1764188439:
                if (str.equals("ERC_USDT")) {
                    tm1Var.e(true);
                    tm1Var.h("usdt");
                    break;
                }
                break;
        }
        return tm1Var;
    }
}
