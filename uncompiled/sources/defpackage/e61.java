package defpackage;

/* compiled from: FixedNumberBitmapFramePreparationStrategy.java */
/* renamed from: e61  reason: default package */
/* loaded from: classes.dex */
public class e61 implements yp {
    public static final Class<?> b = e61.class;
    public final int a;

    public e61() {
        this(3);
    }

    @Override // defpackage.yp
    public void a(zp zpVar, xp xpVar, de deVar, int i) {
        for (int i2 = 1; i2 <= this.a; i2++) {
            int a = (i + i2) % deVar.a();
            if (v11.m(2)) {
                v11.p(b, "Preparing frame %d, last drawn: %d", Integer.valueOf(a), Integer.valueOf(i));
            }
            if (!zpVar.a(xpVar, deVar, a)) {
                return;
            }
        }
    }

    public e61(int i) {
        this.a = i;
    }
}
