package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: g95  reason: default package */
/* loaded from: classes.dex */
public final class g95 extends n65<Double> implements rb5<Double> {
    public double[] f0;
    public int g0;

    static {
        new g95().v();
    }

    public g95() {
        this(new double[10], 0);
    }

    public g95(double[] dArr, int i) {
        this.f0 = dArr;
        this.g0 = i;
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5<Double> T0(int i) {
        if (i >= this.g0) {
            return new g95(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        k(i, ((Double) obj).doubleValue());
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Double> collection) {
        e();
        gb5.a(collection);
        if (collection instanceof g95) {
            g95 g95Var = (g95) collection;
            int i = g95Var.g0;
            if (i == 0) {
                return false;
            }
            int i2 = this.g0;
            if (Integer.MAX_VALUE - i2 >= i) {
                int i3 = i2 + i;
                double[] dArr = this.f0;
                if (i3 > dArr.length) {
                    this.f0 = Arrays.copyOf(dArr, i3);
                }
                System.arraycopy(g95Var.f0, 0, this.f0, this.g0, g95Var.g0);
                this.g0 = i3;
                ((AbstractList) this).modCount++;
                return true;
            }
            throw new OutOfMemoryError();
        }
        return super.addAll(collection);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof g95) {
            g95 g95Var = (g95) obj;
            if (this.g0 != g95Var.g0) {
                return false;
            }
            double[] dArr = g95Var.f0;
            for (int i = 0; i < this.g0; i++) {
                if (this.f0[i] != dArr[i]) {
                    return false;
                }
            }
            return true;
        }
        return super.equals(obj);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        m(i);
        return Double.valueOf(this.f0[i]);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + gb5.j(Double.doubleToLongBits(this.f0[i2]));
        }
        return i;
    }

    public final void i(double d) {
        k(this.g0, d);
    }

    public final void k(int i, double d) {
        int i2;
        e();
        if (i < 0 || i > (i2 = this.g0)) {
            throw new IndexOutOfBoundsException(n(i));
        }
        double[] dArr = this.f0;
        if (i2 < dArr.length) {
            System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
        } else {
            double[] dArr2 = new double[((i2 * 3) / 2) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            System.arraycopy(this.f0, i, dArr2, i + 1, this.g0 - i);
            this.f0 = dArr2;
        }
        this.f0[i] = d;
        this.g0++;
        ((AbstractList) this).modCount++;
    }

    public final void m(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(n(i));
        }
    }

    public final String n(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        m(i);
        double[] dArr = this.f0;
        double d = dArr[i];
        int i2 = this.g0;
        if (i < i2 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, i2 - i);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d);
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Double.valueOf(this.f0[i]))) {
                double[] dArr = this.f0;
                System.arraycopy(dArr, i + 1, dArr, i, this.g0 - i);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        double[] dArr = this.f0;
        System.arraycopy(dArr, i2, dArr, i, this.g0 - i2);
        this.g0 -= i2 - i;
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        e();
        m(i);
        double[] dArr = this.f0;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }
}
