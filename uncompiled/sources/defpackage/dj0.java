package defpackage;

import java.util.concurrent.TimeUnit;

/* compiled from: DefaultEncodedMemoryCacheParamsSupplier.java */
/* renamed from: dj0  reason: default package */
/* loaded from: classes.dex */
public class dj0 implements fw3<m72> {
    public static final long a = TimeUnit.MINUTES.toMillis(5);

    @Override // defpackage.fw3
    /* renamed from: a */
    public m72 get() {
        int b = b();
        return new m72(b, Integer.MAX_VALUE, b, Integer.MAX_VALUE, b / 8, a);
    }

    public final int b() {
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        if (min < 16777216) {
            return 1048576;
        }
        return min < 33554432 ? 2097152 : 4194304;
    }
}
