package defpackage;

import android.graphics.Bitmap;
import android.util.SparseArray;
import com.facebook.common.references.a;

/* compiled from: FrescoFrameCache.java */
/* renamed from: lc1  reason: default package */
/* loaded from: classes.dex */
public class lc1 implements xp {
    public static final Class<?> e = lc1.class;
    public final sd a;
    public final boolean b;
    public final SparseArray<a<com.facebook.imagepipeline.image.a>> c = new SparseArray<>();
    public a<com.facebook.imagepipeline.image.a> d;

    public lc1(sd sdVar, boolean z) {
        this.a = sdVar;
        this.b = z;
    }

    public static a<Bitmap> g(a<com.facebook.imagepipeline.image.a> aVar) {
        c00 c00Var;
        try {
            if (a.u(aVar) && (aVar.j() instanceof c00) && (c00Var = (c00) aVar.j()) != null) {
                return c00Var.g();
            }
            return null;
        } finally {
            a.g(aVar);
        }
    }

    public static a<com.facebook.imagepipeline.image.a> h(a<Bitmap> aVar) {
        return a.v(new c00(aVar, jp1.d, 0));
    }

    @Override // defpackage.xp
    public synchronized a<Bitmap> a(int i, int i2, int i3) {
        if (this.b) {
            return g(this.a.d());
        }
        return null;
    }

    @Override // defpackage.xp
    public synchronized void b(int i, a<Bitmap> aVar, int i2) {
        xt2.g(aVar);
        a<com.facebook.imagepipeline.image.a> h = h(aVar);
        if (h == null) {
            a.g(h);
            return;
        }
        a<com.facebook.imagepipeline.image.a> a = this.a.a(i, h);
        if (a.u(a)) {
            a.g(this.c.get(i));
            this.c.put(i, a);
            v11.p(e, "cachePreparedFrame(%d) cached. Pending frames: %s", Integer.valueOf(i), this.c);
        }
        a.g(h);
    }

    @Override // defpackage.xp
    public synchronized boolean c(int i) {
        return this.a.b(i);
    }

    @Override // defpackage.xp
    public synchronized void clear() {
        a.g(this.d);
        this.d = null;
        for (int i = 0; i < this.c.size(); i++) {
            a.g(this.c.valueAt(i));
        }
        this.c.clear();
    }

    @Override // defpackage.xp
    public synchronized a<Bitmap> d(int i) {
        return g(this.a.c(i));
    }

    @Override // defpackage.xp
    public synchronized void e(int i, a<Bitmap> aVar, int i2) {
        xt2.g(aVar);
        i(i);
        a<com.facebook.imagepipeline.image.a> h = h(aVar);
        if (h != null) {
            a.g(this.d);
            this.d = this.a.a(i, h);
        }
        a.g(h);
    }

    @Override // defpackage.xp
    public synchronized a<Bitmap> f(int i) {
        return g(a.e(this.d));
    }

    public final synchronized void i(int i) {
        a<com.facebook.imagepipeline.image.a> aVar = this.c.get(i);
        if (aVar != null) {
            this.c.delete(i);
            a.g(aVar);
            v11.p(e, "removePreparedReference(%d) removed. Pending frames: %s", Integer.valueOf(i), this.c);
        }
    }
}
