package defpackage;

import androidx.lifecycle.LiveData;

/* compiled from: Transformations.java */
/* renamed from: cb4  reason: default package */
/* loaded from: classes.dex */
public class cb4 {

    /* compiled from: Transformations.java */
    /* renamed from: cb4$a */
    /* loaded from: classes.dex */
    public class a implements tl2<X> {
        public final /* synthetic */ g72 a;
        public final /* synthetic */ ud1 b;

        public a(g72 g72Var, ud1 ud1Var) {
            this.a = g72Var;
            this.b = ud1Var;
        }

        @Override // defpackage.tl2
        public void onChanged(X x) {
            this.a.setValue(this.b.apply(x));
        }
    }

    /* compiled from: Transformations.java */
    /* renamed from: cb4$b */
    /* loaded from: classes.dex */
    public class b implements tl2<X> {
        public LiveData<Y> a;
        public final /* synthetic */ ud1 b;
        public final /* synthetic */ g72 c;

        /* compiled from: Transformations.java */
        /* renamed from: cb4$b$a */
        /* loaded from: classes.dex */
        public class a implements tl2<Y> {
            public a() {
            }

            @Override // defpackage.tl2
            public void onChanged(Y y) {
                b.this.c.setValue(y);
            }
        }

        public b(ud1 ud1Var, g72 g72Var) {
            this.b = ud1Var;
            this.c = g72Var;
        }

        @Override // defpackage.tl2
        public void onChanged(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            Object obj = this.a;
            if (obj == liveData) {
                return;
            }
            if (obj != null) {
                this.c.b(obj);
            }
            this.a = liveData;
            if (liveData != 0) {
                this.c.a(liveData, new a());
            }
        }
    }

    /* compiled from: Transformations.java */
    /* renamed from: cb4$c */
    /* loaded from: classes.dex */
    public class c implements tl2<X> {
        public boolean a = true;
        public final /* synthetic */ g72 b;

        public c(g72 g72Var) {
            this.b = g72Var;
        }

        @Override // defpackage.tl2
        public void onChanged(X x) {
            T value = this.b.getValue();
            if (this.a || ((value == 0 && x != 0) || !(value == 0 || value.equals(x)))) {
                this.a = false;
                this.b.setValue(x);
            }
        }
    }

    public static <X> LiveData<X> a(LiveData<X> liveData) {
        g72 g72Var = new g72();
        g72Var.a(liveData, new c(g72Var));
        return g72Var;
    }

    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, ud1<X, Y> ud1Var) {
        g72 g72Var = new g72();
        g72Var.a(liveData, new a(g72Var, ud1Var));
        return g72Var;
    }

    public static <X, Y> LiveData<Y> c(LiveData<X> liveData, ud1<X, LiveData<Y>> ud1Var) {
        g72 g72Var = new g72();
        g72Var.a(liveData, new b(ud1Var, g72Var));
        return g72Var;
    }
}
