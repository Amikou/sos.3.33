package defpackage;

import java.text.SimpleDateFormat;
import java.util.Locale;

/* compiled from: OneSignalSimpleDateFormat.java */
/* renamed from: dn2  reason: default package */
/* loaded from: classes2.dex */
public class dn2 {
    public static SimpleDateFormat a() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    }
}
