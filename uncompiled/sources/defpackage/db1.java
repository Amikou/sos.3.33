package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.helper.widget.Flow;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSwapBinding.java */
/* renamed from: db1  reason: default package */
/* loaded from: classes2.dex */
public final class db1 {
    public final FrameLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialButton e;
    public final ConstraintLayout f;
    public final ConstraintLayout g;
    public final Chip h;
    public final Chip i;
    public final Chip j;
    public final FrameLayout k;
    public final ImageButton l;
    public final AppCompatImageView m;
    public final ry1 n;
    public final ConstraintLayout o;
    public final Chip p;
    public final MaterialTextView q;
    public final MaterialTextView r;
    public final MaterialTextView s;
    public final TextView t;

    public db1(FrameLayout frameLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, AppCompatImageView appCompatImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, View view, ChipGroup chipGroup, HorizontalScrollView horizontalScrollView, Chip chip, Chip chip2, Chip chip3, FrameLayout frameLayout2, Flow flow, ImageButton imageButton, AppCompatImageView appCompatImageView2, ry1 ry1Var, ConstraintLayout constraintLayout3, View view2, View view3, CircularProgressIndicator circularProgressIndicator, Chip chip4, NestedScrollView nestedScrollView, View view4, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, TextView textView) {
        this.a = frameLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = materialButton4;
        this.f = constraintLayout;
        this.g = constraintLayout2;
        this.h = chip;
        this.i = chip2;
        this.j = chip3;
        this.k = frameLayout2;
        this.l = imageButton;
        this.m = appCompatImageView2;
        this.n = ry1Var;
        this.o = constraintLayout3;
        this.p = chip4;
        this.q = materialTextView;
        this.r = materialTextView2;
        this.s = materialTextView3;
        this.t = textView;
    }

    public static db1 a(View view) {
        int i = R.id.btnApprove;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnApprove);
        if (materialButton != null) {
            i = R.id.btnEnterValue;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnEnterValue);
            if (materialButton2 != null) {
                i = R.id.btnSwap;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnSwap);
                if (materialButton3 != null) {
                    i = R.id.btnV2PopupLaunch;
                    MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.btnV2PopupLaunch);
                    if (materialButton4 != null) {
                        i = R.id.bubble;
                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.bubble);
                        if (appCompatImageView != null) {
                            i = R.id.ccDestination;
                            ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.ccDestination);
                            if (constraintLayout != null) {
                                i = R.id.ccSource;
                                ConstraintLayout constraintLayout2 = (ConstraintLayout) ai4.a(view, R.id.ccSource);
                                if (constraintLayout2 != null) {
                                    i = R.id.centerToolbar;
                                    View a = ai4.a(view, R.id.centerToolbar);
                                    if (a != null) {
                                        i = R.id.cgSetting;
                                        ChipGroup chipGroup = (ChipGroup) ai4.a(view, R.id.cgSetting);
                                        if (chipGroup != null) {
                                            i = R.id.cgSettingParent;
                                            HorizontalScrollView horizontalScrollView = (HorizontalScrollView) ai4.a(view, R.id.cgSettingParent);
                                            if (horizontalScrollView != null) {
                                                i = R.id.chipSwapMinTxn;
                                                Chip chip = (Chip) ai4.a(view, R.id.chipSwapMinTxn);
                                                if (chip != null) {
                                                    i = R.id.chipSwapSlipPage;
                                                    Chip chip2 = (Chip) ai4.a(view, R.id.chipSwapSlipPage);
                                                    if (chip2 != null) {
                                                        i = R.id.chipSwapStandardSpeed;
                                                        Chip chip3 = (Chip) ai4.a(view, R.id.chipSwapStandardSpeed);
                                                        if (chip3 != null) {
                                                            FrameLayout frameLayout = (FrameLayout) view;
                                                            i = R.id.dummyForSwapButton;
                                                            Flow flow = (Flow) ai4.a(view, R.id.dummyForSwapButton);
                                                            if (flow != null) {
                                                                i = R.id.imgArrow;
                                                                ImageButton imageButton = (ImageButton) ai4.a(view, R.id.imgArrow);
                                                                if (imageButton != null) {
                                                                    i = R.id.iv_back;
                                                                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.iv_back);
                                                                    if (appCompatImageView2 != null) {
                                                                        i = R.id.lSelectTokenType;
                                                                        View a2 = ai4.a(view, R.id.lSelectTokenType);
                                                                        if (a2 != null) {
                                                                            ry1 a3 = ry1.a(a2);
                                                                            i = R.id.lWalletBalance;
                                                                            ConstraintLayout constraintLayout3 = (ConstraintLayout) ai4.a(view, R.id.lWalletBalance);
                                                                            if (constraintLayout3 != null) {
                                                                                i = R.id.paddingEnd;
                                                                                View a4 = ai4.a(view, R.id.paddingEnd);
                                                                                if (a4 != null) {
                                                                                    i = R.id.paddingStart;
                                                                                    View a5 = ai4.a(view, R.id.paddingStart);
                                                                                    if (a5 != null) {
                                                                                        i = R.id.progressLoading;
                                                                                        CircularProgressIndicator circularProgressIndicator = (CircularProgressIndicator) ai4.a(view, R.id.progressLoading);
                                                                                        if (circularProgressIndicator != null) {
                                                                                            i = R.id.requireGasFee;
                                                                                            Chip chip4 = (Chip) ai4.a(view, R.id.requireGasFee);
                                                                                            if (chip4 != null) {
                                                                                                i = R.id.swapCardWrapper;
                                                                                                NestedScrollView nestedScrollView = (NestedScrollView) ai4.a(view, R.id.swapCardWrapper);
                                                                                                if (nestedScrollView != null) {
                                                                                                    i = R.id.toolbar;
                                                                                                    View a6 = ai4.a(view, R.id.toolbar);
                                                                                                    if (a6 != null) {
                                                                                                        i = R.id.tvMainWallet;
                                                                                                        MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tvMainWallet);
                                                                                                        if (materialTextView != null) {
                                                                                                            i = R.id.tv_total;
                                                                                                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.tv_total);
                                                                                                            if (materialTextView2 != null) {
                                                                                                                i = R.id.txtGasFee;
                                                                                                                MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtGasFee);
                                                                                                                if (materialTextView3 != null) {
                                                                                                                    i = R.id.txtSwapTitle;
                                                                                                                    MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtSwapTitle);
                                                                                                                    if (materialTextView4 != null) {
                                                                                                                        i = R.id.txtSymbol;
                                                                                                                        TextView textView = (TextView) ai4.a(view, R.id.txtSymbol);
                                                                                                                        if (textView != null) {
                                                                                                                            return new db1(frameLayout, materialButton, materialButton2, materialButton3, materialButton4, appCompatImageView, constraintLayout, constraintLayout2, a, chipGroup, horizontalScrollView, chip, chip2, chip3, frameLayout, flow, imageButton, appCompatImageView2, a3, constraintLayout3, a4, a5, circularProgressIndicator, chip4, nestedScrollView, a6, materialTextView, materialTextView2, materialTextView3, materialTextView4, textView);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public FrameLayout b() {
        return this.a;
    }
}
