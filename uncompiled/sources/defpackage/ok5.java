package defpackage;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: ok5  reason: default package */
/* loaded from: classes.dex */
public final class ok5 implements Runnable {
    public final /* synthetic */ pm5 a;

    public ok5(pm5 pm5Var) {
        this.a = pm5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        hm2 hm2Var;
        hm2 hm2Var2;
        obj = this.a.b;
        synchronized (obj) {
            hm2Var = this.a.c;
            if (hm2Var != null) {
                hm2Var2 = this.a.c;
                hm2Var2.d();
            }
        }
    }
}
