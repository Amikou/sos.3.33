package defpackage;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.UserManager;

/* renamed from: e45  reason: default package */
/* loaded from: classes.dex */
public class e45 {
    public static volatile UserManager a;
    public static volatile boolean b = !b();

    public static boolean a(Context context) {
        return b() && !c(context);
    }

    public static boolean b() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @TargetApi(24)
    public static boolean c(Context context) {
        boolean z = b;
        if (!z) {
            UserManager userManager = a;
            if (userManager == null) {
                synchronized (e45.class) {
                    userManager = a;
                    if (userManager == null) {
                        UserManager userManager2 = (UserManager) context.getSystemService(UserManager.class);
                        a = userManager2;
                        if (userManager2 == null) {
                            b = true;
                            return true;
                        }
                        userManager = userManager2;
                    }
                }
            }
            z = userManager.isUserUnlocked();
            b = z;
            if (z) {
                a = null;
            }
        }
        return z;
    }
}
