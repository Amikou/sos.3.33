package defpackage;

/* renamed from: bc3  reason: default package */
/* loaded from: classes2.dex */
public class bc3 {
    static {
        ro2.h(su3.e("expand 16-byte kexpand 32-byte k"), 0, 8);
        su3.e("expand 32-byte k");
        su3.e("expand 16-byte k");
    }

    public static int a(int i, int i2) {
        return (i >>> (-i2)) | (i << i2);
    }

    public static void b(int i, int[] iArr, int[] iArr2) {
        if (iArr.length != 16) {
            throw new IllegalArgumentException();
        }
        if (iArr2.length != 16) {
            throw new IllegalArgumentException();
        }
        if (i % 2 != 0) {
            throw new IllegalArgumentException("Number of rounds must be even");
        }
        char c = 0;
        int i2 = iArr[0];
        int i3 = iArr[1];
        int i4 = iArr[2];
        int i5 = iArr[3];
        int i6 = iArr[4];
        int i7 = iArr[5];
        int i8 = iArr[6];
        int i9 = 7;
        int i10 = iArr[7];
        int i11 = iArr[8];
        int i12 = 9;
        int i13 = iArr[9];
        int i14 = iArr[10];
        int i15 = iArr[11];
        int i16 = iArr[12];
        int i17 = 13;
        int i18 = iArr[13];
        int i19 = iArr[14];
        int i20 = iArr[15];
        int i21 = i19;
        int i22 = i18;
        int i23 = i16;
        int i24 = i15;
        int i25 = i14;
        int i26 = i13;
        int i27 = i11;
        int i28 = i10;
        int i29 = i8;
        int i30 = i7;
        int i31 = i6;
        int i32 = i5;
        int i33 = i4;
        int i34 = i3;
        int i35 = i2;
        int i36 = i;
        while (i36 > 0) {
            int a = a(i35 + i23, i9) ^ i31;
            int a2 = i27 ^ a(a + i35, i12);
            int a3 = i23 ^ a(a2 + a, i17);
            int a4 = a(a3 + a2, 18) ^ i35;
            int a5 = i26 ^ a(i30 + i34, i9);
            int a6 = i22 ^ a(a5 + i30, i12);
            int a7 = i34 ^ a(a6 + a5, i17);
            int a8 = a(a7 + a6, 18) ^ i30;
            int a9 = i21 ^ a(i25 + i29, 7);
            int a10 = i33 ^ a(a9 + i25, 9);
            int a11 = i29 ^ a(a10 + a9, 13);
            int a12 = i25 ^ a(a11 + a10, 18);
            int a13 = i32 ^ a(i20 + i24, 7);
            int a14 = i28 ^ a(a13 + i20, 9);
            int i37 = i36;
            int a15 = i24 ^ a(a14 + a13, 13);
            int a16 = i20 ^ a(a15 + a14, 18);
            i34 = a7 ^ a(a4 + a13, 7);
            i33 = a10 ^ a(i34 + a4, 9);
            int a17 = a13 ^ a(i33 + i34, 13);
            int a18 = a4 ^ a(a17 + i33, 18);
            i29 = a11 ^ a(a8 + a, 7);
            i28 = a14 ^ a(i29 + a8, 9);
            int a19 = a(i28 + i29, 13) ^ a;
            i30 = a8 ^ a(a19 + i28, 18);
            i24 = a15 ^ a(a12 + a5, 7);
            int a20 = a(i24 + a12, 9) ^ a2;
            i26 = a5 ^ a(a20 + i24, 13);
            i25 = a12 ^ a(i26 + a20, 18);
            i23 = a3 ^ a(a16 + a9, 7);
            i22 = a6 ^ a(i23 + a16, 9);
            i21 = a9 ^ a(i22 + i23, 13);
            i20 = a16 ^ a(i21 + i22, 18);
            i32 = a17;
            i27 = a20;
            i35 = a18;
            i31 = a19;
            c = 0;
            i17 = 13;
            i12 = 9;
            i9 = 7;
            i36 = i37 - 2;
        }
        char c2 = c;
        iArr2[c2] = i35 + iArr[c2];
        iArr2[1] = i34 + iArr[1];
        iArr2[2] = i33 + iArr[2];
        iArr2[3] = i32 + iArr[3];
        iArr2[4] = i31 + iArr[4];
        iArr2[5] = i30 + iArr[5];
        iArr2[6] = i29 + iArr[6];
        iArr2[7] = i28 + iArr[7];
        iArr2[8] = i27 + iArr[8];
        iArr2[9] = i26 + iArr[9];
        iArr2[10] = i25 + iArr[10];
        iArr2[11] = i24 + iArr[11];
        iArr2[12] = i23 + iArr[12];
        iArr2[13] = i22 + iArr[13];
        iArr2[14] = i21 + iArr[14];
        iArr2[15] = i20 + iArr[15];
    }
}
