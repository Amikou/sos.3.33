package defpackage;

/* compiled from: BytesResource.java */
/* renamed from: dt  reason: default package */
/* loaded from: classes.dex */
public class dt implements s73<byte[]> {
    public final byte[] a;

    public dt(byte[] bArr) {
        this.a = (byte[]) wt2.d(bArr);
    }

    @Override // defpackage.s73
    public int a() {
        return this.a.length;
    }

    @Override // defpackage.s73
    public void b() {
    }

    @Override // defpackage.s73
    /* renamed from: c */
    public byte[] get() {
        return this.a;
    }

    @Override // defpackage.s73
    public Class<byte[]> d() {
        return byte[].class;
    }
}
