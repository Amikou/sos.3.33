package defpackage;

import com.google.protobuf.j0;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: px5  reason: default package */
/* loaded from: classes.dex */
public final class px5 {
    public static final ox5 a;
    public static final ox5 b;

    static {
        ox5 ox5Var;
        try {
            ox5Var = (ox5) j0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            ox5Var = null;
        }
        a = ox5Var;
        b = new ox5();
    }

    public static ox5 a() {
        return a;
    }

    public static ox5 b() {
        return b;
    }
}
