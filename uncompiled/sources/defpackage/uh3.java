package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: uh3  reason: default package */
/* loaded from: classes2.dex */
public class uh3 extends xs0.b {
    public static final rh3 k;
    public static final rh3 l;
    public vh3 j;

    /* renamed from: uh3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] b = jd2.b();
            long[] b2 = jd2.b();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                long j = ((i3 ^ i) - 1) >> 31;
                for (int i4 = 0; i4 < 9; i4++) {
                    long j2 = b[i4];
                    long[] jArr = this.b;
                    b[i4] = j2 ^ (jArr[i2 + i4] & j);
                    b2[i4] = b2[i4] ^ (jArr[(i2 + 9) + i4] & j);
                }
                i2 += 18;
            }
            return uh3.this.i(new rh3(b), new rh3(b2), false);
        }
    }

    static {
        rh3 rh3Var = new rh3(new BigInteger(1, pk1.a("02F40E7E2221F295DE297117B7F3D62F5C6A97FFCB8CEFF1CD6BA8CE4A9A18AD84FFABBD8EFA59332BE7AD6756A66E294AFD185A78FF12AA520E4DE739BACA0C7FFEFF7F2955727A")));
        k = rh3Var;
        l = (rh3) rh3Var.n();
    }

    public uh3() {
        super(571, 2, 5, 10);
        this.j = new vh3(this, null, null);
        this.b = n(BigInteger.valueOf(1L));
        this.c = k;
        this.d = new BigInteger(1, pk1.a("03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE661CE18FF55987308059B186823851EC7DD9CA1161DE93D5174D66E8382E9BB2FE84E47"));
        this.e = BigInteger.valueOf(2L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return false;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new uh3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 9 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            jd2.a(((rh3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 9;
            jd2.a(((rh3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 9;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new vh3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new vh3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new rh3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 571;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
