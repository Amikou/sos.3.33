package defpackage;

import java.util.List;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ContactCarouselViewData.kt */
/* renamed from: s60  reason: default package */
/* loaded from: classes2.dex */
public final class s60 {
    public int a;
    public List<? extends IContact> b;

    public s60(int i, List<? extends IContact> list) {
        fs1.f(list, "contactList");
        this.a = i;
        this.b = list;
    }

    public final List<IContact> a() {
        return this.b;
    }

    public final int b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof s60) {
            s60 s60Var = (s60) obj;
            return this.a == s60Var.a && fs1.b(this.b, s60Var.b);
        }
        return false;
    }

    public int hashCode() {
        return (this.a * 31) + this.b.hashCode();
    }

    public String toString() {
        return "ContactCarouselViewData(preScrolledDefaultPosition=" + this.a + ", contactList=" + this.b + ')';
    }
}
