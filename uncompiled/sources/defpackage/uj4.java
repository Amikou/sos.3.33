package defpackage;

import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;

/* compiled from: ViewPositionHolder.java */
/* renamed from: uj4  reason: default package */
/* loaded from: classes.dex */
public class uj4 implements ViewTreeObserver.OnPreDrawListener {
    public final sj4 a = sj4.f();
    public b f0;
    public View g0;
    public View.OnAttachStateChangeListener h0;
    public boolean i0;

    /* compiled from: ViewPositionHolder.java */
    /* renamed from: uj4$a */
    /* loaded from: classes.dex */
    public class a implements View.OnAttachStateChangeListener {
        public a() {
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
            uj4.this.f(view, true);
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
            uj4.this.f(view, false);
        }
    }

    /* compiled from: ViewPositionHolder.java */
    /* renamed from: uj4$b */
    /* loaded from: classes.dex */
    public interface b {
        void a(sj4 sj4Var);
    }

    public static boolean d(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isAttachedToWindow();
        }
        return view.getWindowToken() != null;
    }

    public static boolean e(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isLaidOut();
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    public void b() {
        View view = this.g0;
        if (view != null) {
            view.removeOnAttachStateChangeListener(this.h0);
            f(this.g0, false);
        }
        this.a.a.setEmpty();
        this.a.b.setEmpty();
        this.a.d.setEmpty();
        this.g0 = null;
        this.h0 = null;
        this.f0 = null;
        this.i0 = false;
    }

    public void c(View view, b bVar) {
        b();
        this.g0 = view;
        this.f0 = bVar;
        a aVar = new a();
        this.h0 = aVar;
        view.addOnAttachStateChangeListener(aVar);
        f(view, d(view));
        if (e(view)) {
            h();
        }
    }

    public final void f(View view, boolean z) {
        view.getViewTreeObserver().removeOnPreDrawListener(this);
        if (z) {
            view.getViewTreeObserver().addOnPreDrawListener(this);
        }
    }

    public void g(boolean z) {
        if (this.i0 == z) {
            return;
        }
        this.i0 = z;
        h();
    }

    public final void h() {
        View view = this.g0;
        if (view == null || this.f0 == null || this.i0 || !sj4.b(this.a, view)) {
            return;
        }
        this.f0.a(this.a);
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        h();
        return true;
    }
}
