package defpackage;

import java.lang.reflect.Array;

/* renamed from: sh1  reason: default package */
/* loaded from: classes2.dex */
public final class sh1 {
    public static he1 a(je1 je1Var, rs2 rs2Var) {
        int d = je1Var.d();
        int i = 1 << d;
        int g = rs2Var.g();
        int i2 = 0;
        int[][] iArr = (int[][]) Array.newInstance(int.class, g, i);
        int[][] iArr2 = (int[][]) Array.newInstance(int.class, g, i);
        for (int i3 = 0; i3 < i; i3++) {
            iArr2[0][i3] = je1Var.f(rs2Var.e(i3));
        }
        for (int i4 = 1; i4 < g; i4++) {
            for (int i5 = 0; i5 < i; i5++) {
                iArr2[i4][i5] = je1Var.h(iArr2[i4 - 1][i5], i5);
            }
        }
        int i6 = 0;
        while (i6 < g) {
            int i7 = i2;
            while (i7 < i) {
                for (int i8 = i2; i8 <= i6; i8++) {
                    iArr[i6][i7] = je1Var.a(iArr[i6][i7], je1Var.h(iArr2[i8][i7], rs2Var.f((g + i8) - i6)));
                }
                i7++;
                i2 = 0;
            }
            i6++;
            i2 = 0;
        }
        int[][] iArr3 = (int[][]) Array.newInstance(int.class, g * d, (i + 31) >>> 5);
        for (int i9 = 0; i9 < i; i9++) {
            int i10 = i9 >>> 5;
            int i11 = 1 << (i9 & 31);
            for (int i12 = 0; i12 < g; i12++) {
                int i13 = iArr[i12][i9];
                for (int i14 = 0; i14 < d; i14++) {
                    if (((i13 >>> i14) & 1) != 0) {
                        int[] iArr4 = iArr3[(((i12 + 1) * d) - i14) - 1];
                        iArr4[i10] = iArr4[i10] ^ i11;
                    }
                }
            }
        }
        return new he1(i, iArr3);
    }
}
