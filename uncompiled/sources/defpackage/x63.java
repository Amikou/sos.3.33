package defpackage;

import kotlinx.coroutines.channels.AbstractChannel;

/* compiled from: RendezvousChannel.kt */
/* renamed from: x63  reason: default package */
/* loaded from: classes2.dex */
public class x63<E> extends AbstractChannel<E> {
    public x63(tc1<? super E, te4> tc1Var) {
        super(tc1Var);
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean G() {
        return true;
    }

    @Override // kotlinx.coroutines.channels.AbstractChannel
    public final boolean H() {
        return true;
    }

    @Override // defpackage.h5
    public final boolean s() {
        return true;
    }

    @Override // defpackage.h5
    public final boolean t() {
        return true;
    }
}
