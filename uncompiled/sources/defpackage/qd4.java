package defpackage;

import java.util.List;

/* compiled from: TypeIntrinsics.java */
/* renamed from: qd4  reason: default package */
/* loaded from: classes2.dex */
public class qd4 {
    public static Iterable a(Object obj) {
        if ((obj instanceof tw1) && !(obj instanceof uw1)) {
            j(obj, "kotlin.collections.MutableIterable");
        }
        return d(obj);
    }

    public static List b(Object obj) {
        if ((obj instanceof tw1) && !(obj instanceof vw1)) {
            j(obj, "kotlin.collections.MutableList");
        }
        return e(obj);
    }

    public static Object c(Object obj, int i) {
        if (obj != null && !g(obj, i)) {
            j(obj, "kotlin.jvm.functions.Function" + i);
        }
        return obj;
    }

    public static Iterable d(Object obj) {
        try {
            return (Iterable) obj;
        } catch (ClassCastException e) {
            throw i(e);
        }
    }

    public static List e(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            throw i(e);
        }
    }

    public static int f(Object obj) {
        if (obj instanceof vd1) {
            return ((vd1) obj).getArity();
        }
        if (obj instanceof rc1) {
            return 0;
        }
        if (obj instanceof tc1) {
            return 1;
        }
        if (obj instanceof hd1) {
            return 2;
        }
        if (obj instanceof kd1) {
            return 3;
        }
        if (obj instanceof md1) {
            return 4;
        }
        if (obj instanceof od1) {
            return 5;
        }
        if (obj instanceof qd1) {
            return 6;
        }
        if (obj instanceof rd1) {
            return 7;
        }
        if (obj instanceof sd1) {
            return 8;
        }
        if (obj instanceof td1) {
            return 9;
        }
        if (obj instanceof sc1) {
            return 10;
        }
        if (obj instanceof uc1) {
            return 11;
        }
        if (obj instanceof vc1) {
            return 12;
        }
        if (obj instanceof wc1) {
            return 13;
        }
        if (obj instanceof xc1) {
            return 14;
        }
        if (obj instanceof yc1) {
            return 15;
        }
        if (obj instanceof zc1) {
            return 16;
        }
        if (obj instanceof ad1) {
            return 17;
        }
        if (obj instanceof bd1) {
            return 18;
        }
        if (obj instanceof cd1) {
            return 19;
        }
        if (obj instanceof ed1) {
            return 20;
        }
        if (obj instanceof fd1) {
            return 21;
        }
        return obj instanceof gd1 ? 22 : -1;
    }

    public static boolean g(Object obj, int i) {
        return (obj instanceof pd1) && f(obj) == i;
    }

    public static <T extends Throwable> T h(T t) {
        return (T) fs1.k(t, qd4.class.getName());
    }

    public static ClassCastException i(ClassCastException classCastException) {
        throw ((ClassCastException) h(classCastException));
    }

    public static void j(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        k(name + " cannot be cast to " + str);
    }

    public static void k(String str) {
        throw i(new ClassCastException(str));
    }
}
