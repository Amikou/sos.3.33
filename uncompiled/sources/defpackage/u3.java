package defpackage;

import defpackage.xs0;
import java.math.BigInteger;
import java.util.Hashtable;
import org.bouncycastle.asn1.i;

/* renamed from: u3  reason: default package */
/* loaded from: classes2.dex */
public class u3 {
    public static qr4 a = new a();
    public static final Hashtable b = new Hashtable();
    public static final Hashtable c = new Hashtable();
    public static final Hashtable d = new Hashtable();

    /* renamed from: u3$a */
    /* loaded from: classes2.dex */
    public static class a extends qr4 {
        @Override // defpackage.qr4
        public pr4 a() {
            BigInteger e = u3.e("F1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C03");
            BigInteger e2 = u3.e("F1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C00");
            BigInteger e3 = u3.e("EE353FCA5428A9300D4ABA754A44C00FDFEC0C9AE4B1A1803075ED967B7BB73F");
            BigInteger e4 = u3.e("F1FD178C0B3AD58F10126DE8CE42435B53DC67E140D2BF941FFDD459C6D655E1");
            BigInteger valueOf = BigInteger.valueOf(1L);
            xs0 c = u3.c(new xs0.f(e, e2, e3, e4, valueOf));
            return new pr4(c, new rr4(c, pk1.a("04B6B3D4C356C139EB31183D4749D423958C27D2DCAF98B70164C97A2DD98F5CFF6142E0F7C8B204911F9271F0F3ECEF8C2701C307E8E4C9E183115A1554062CFB")), e4, valueOf, (byte[]) null);
        }
    }

    static {
        d("FRP256v1", v3.a, a);
    }

    public static xs0 c(xs0 xs0Var) {
        return xs0Var;
    }

    public static void d(String str, i iVar, qr4 qr4Var) {
        b.put(su3.f(str), iVar);
        d.put(iVar, str);
        c.put(iVar, qr4Var);
    }

    public static BigInteger e(String str) {
        return new BigInteger(1, pk1.a(str));
    }

    public static pr4 f(String str) {
        i i = i(str);
        if (i == null) {
            return null;
        }
        return g(i);
    }

    public static pr4 g(i iVar) {
        qr4 qr4Var = (qr4) c.get(iVar);
        if (qr4Var == null) {
            return null;
        }
        return qr4Var.b();
    }

    public static String h(i iVar) {
        return (String) d.get(iVar);
    }

    public static i i(String str) {
        return (i) b.get(su3.f(str));
    }
}
