package defpackage;

import android.net.Uri;
import androidx.media3.extractor.Extractor;
import androidx.media3.extractor.flv.b;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: DefaultExtractorsFactory.java */
/* renamed from: kj0  reason: default package */
/* loaded from: classes.dex */
public final class kj0 implements u11 {
    public static final int[] n = {5, 4, 12, 8, 3, 10, 9, 11, 6, 2, 0, 1, 7, 16, 15, 14};
    public static final a o = new a(ij0.a);
    public static final a p = new a(jj0.a);
    public boolean b;
    public boolean c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public int i;
    public int j;
    public int l;
    public int k = 1;
    public int m = 112800;

    /* compiled from: DefaultExtractorsFactory.java */
    /* renamed from: kj0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final InterfaceC0194a a;
        public final AtomicBoolean b = new AtomicBoolean(false);
        public Constructor<? extends p11> c;

        /* compiled from: DefaultExtractorsFactory.java */
        /* renamed from: kj0$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public interface InterfaceC0194a {
            Constructor<? extends p11> a() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, ClassNotFoundException;
        }

        public a(InterfaceC0194a interfaceC0194a) {
            this.a = interfaceC0194a;
        }

        public p11 a(Object... objArr) {
            Constructor<? extends p11> b = b();
            if (b == null) {
                return null;
            }
            try {
                return b.newInstance(objArr);
            } catch (Exception e) {
                throw new IllegalStateException("Unexpected error creating extractor", e);
            }
        }

        public final Constructor<? extends p11> b() {
            synchronized (this.b) {
                if (this.b.get()) {
                    return this.c;
                }
                try {
                    return this.a.a();
                } catch (ClassNotFoundException unused) {
                    this.b.set(true);
                    return this.c;
                } catch (Exception e) {
                    throw new RuntimeException("Error instantiating extension", e);
                }
            }
        }
    }

    public static Constructor<? extends p11> f() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (Boolean.TRUE.equals(Class.forName("androidx.media3.decoder.flac.FlacLibrary").getMethod("isAvailable", new Class[0]).invoke(null, new Object[0]))) {
            return Class.forName("androidx.media3.decoder.flac.FlacExtractor").asSubclass(p11.class).getConstructor(Integer.TYPE);
        }
        return null;
    }

    public static Constructor<? extends p11> g() throws ClassNotFoundException, NoSuchMethodException {
        return Class.forName("androidx.media3.decoder.midi.MidiExtractor").asSubclass(p11.class).getConstructor(new Class[0]);
    }

    @Override // defpackage.u11
    public synchronized p11[] a() {
        return b(Uri.EMPTY, new HashMap());
    }

    @Override // defpackage.u11
    public synchronized Extractor[] b(Uri uri, Map<String, List<String>> map) {
        ArrayList arrayList;
        int[] iArr = n;
        arrayList = new ArrayList(iArr.length);
        int b = y31.b(map);
        if (b != -1) {
            e(b, arrayList);
        }
        int c = y31.c(uri);
        if (c != -1 && c != b) {
            e(c, arrayList);
        }
        for (int i : iArr) {
            if (i != b && i != c) {
                e(i, arrayList);
            }
        }
        return (p11[]) arrayList.toArray(new p11[arrayList.size()]);
    }

    public final void e(int i, List<p11> list) {
        switch (i) {
            case 0:
                list.add(new r5());
                return;
            case 1:
                list.add(new v5());
                return;
            case 2:
                list.add(new z9((this.c ? 2 : 0) | this.d | (this.b ? 1 : 0)));
                return;
            case 3:
                list.add(new ib((this.c ? 2 : 0) | this.e | (this.b ? 1 : 0)));
                return;
            case 4:
                p11 a2 = o.a(Integer.valueOf(this.f));
                if (a2 != null) {
                    list.add(a2);
                    return;
                } else {
                    list.add(new n61(this.f));
                    return;
                }
            case 5:
                list.add(new b());
                return;
            case 6:
                list.add(new d52(this.g));
                return;
            case 7:
                list.add(new ea2((this.c ? 2 : 0) | this.j | (this.b ? 1 : 0)));
                return;
            case 8:
                list.add(new xb1(this.i));
                list.add(new ha2(this.h));
                return;
            case 9:
                list.add(new wl2());
                return;
            case 10:
                list.add(new lw2());
                return;
            case 11:
                list.add(new fc4(this.k, this.l, this.m));
                return;
            case 12:
                list.add(new co4());
                return;
            case 13:
            default:
                return;
            case 14:
                list.add(new nu1());
                return;
            case 15:
                p11 a3 = p.a(new Object[0]);
                if (a3 != null) {
                    list.add(a3);
                    return;
                }
                return;
            case 16:
                list.add(new tl());
                return;
        }
    }
}
