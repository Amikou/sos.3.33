package defpackage;

import android.animation.IntEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;

/* compiled from: FillAnimation.java */
/* renamed from: e41  reason: default package */
/* loaded from: classes2.dex */
public class e41 extends t20 {
    public f41 g;
    public int h;
    public int i;

    /* compiled from: FillAnimation.java */
    /* renamed from: e41$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            e41.this.j(valueAnimator);
        }
    }

    public e41(xg4.a aVar) {
        super(aVar);
        this.g = new f41();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void j(ValueAnimator valueAnimator) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COLOR")).intValue();
        int intValue2 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_COLOR_REVERSE")).intValue();
        int intValue3 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_RADIUS")).intValue();
        int intValue4 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_RADIUS_REVERSE")).intValue();
        int intValue5 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_STROKE")).intValue();
        int intValue6 = ((Integer) valueAnimator.getAnimatedValue("ANIMATION_STROKE_REVERSE")).intValue();
        this.g.c(intValue);
        this.g.d(intValue2);
        this.g.i(intValue3);
        this.g.j(intValue4);
        this.g.k(intValue5);
        this.g.l(intValue6);
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.g);
        }
    }

    @Override // defpackage.t20, defpackage.nm
    /* renamed from: g */
    public ValueAnimator a() {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(350L);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new a());
        return valueAnimator;
    }

    public final PropertyValuesHolder n(boolean z) {
        int i;
        int i2;
        String str;
        if (z) {
            i2 = this.h;
            i = i2 / 2;
            str = "ANIMATION_RADIUS_REVERSE";
        } else {
            i = this.h;
            i2 = i / 2;
            str = "ANIMATION_RADIUS";
        }
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt(str, i, i2);
        ofInt.setEvaluator(new IntEvaluator());
        return ofInt;
    }

    public final PropertyValuesHolder o(boolean z) {
        String str;
        int i;
        int i2;
        if (z) {
            i2 = this.h;
            str = "ANIMATION_STROKE_REVERSE";
            i = 0;
        } else {
            str = "ANIMATION_STROKE";
            i = this.h;
            i2 = 0;
        }
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt(str, i2, i);
        ofInt.setEvaluator(new IntEvaluator());
        return ofInt;
    }

    public final boolean p(int i, int i2, int i3, int i4) {
        return (this.e == i && this.f == i2 && this.h == i3 && this.i == i4) ? false : true;
    }

    public e41 q(int i, int i2, int i3, int i4) {
        if (this.c != 0 && p(i, i2, i3, i4)) {
            this.e = i;
            this.f = i2;
            this.h = i3;
            this.i = i4;
            ((ValueAnimator) this.c).setValues(h(false), h(true), n(false), n(true), o(false), o(true));
        }
        return this;
    }
}
