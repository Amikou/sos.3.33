package defpackage;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: MathUtils.java */
/* renamed from: v42  reason: default package */
/* loaded from: classes.dex */
public class v42 {
    public static final Matrix a = new Matrix();
    public static final Matrix b = new Matrix();
    public static final RectF c = new RectF();

    public static void a(float[] fArr, us3 us3Var, us3 us3Var2) {
        Matrix matrix = a;
        us3Var.d(matrix);
        Matrix matrix2 = b;
        matrix.invert(matrix2);
        matrix2.mapPoints(fArr);
        us3Var2.d(matrix);
        matrix.mapPoints(fArr);
    }

    public static float b(float f, float f2, float f3) {
        return f + ((f2 - f) * f3);
    }

    public static void c(us3 us3Var, us3 us3Var2, float f, float f2, us3 us3Var3, float f3, float f4, float f5) {
        us3Var.m(us3Var2);
        if (!us3.c(us3Var2.h(), us3Var3.h())) {
            us3Var.r(b(us3Var2.h(), us3Var3.h(), f5), f, f2);
        }
        float e = us3Var2.e();
        float e2 = us3Var3.e();
        float f6 = Float.NaN;
        if (Math.abs(e - e2) <= 180.0f) {
            if (!us3.c(e, e2)) {
                f6 = b(e, e2, f5);
            }
        } else {
            if (e < Utils.FLOAT_EPSILON) {
                e += 360.0f;
            }
            if (e2 < Utils.FLOAT_EPSILON) {
                e2 += 360.0f;
            }
            if (!us3.c(e, e2)) {
                f6 = b(e, e2, f5);
            }
        }
        if (!Float.isNaN(f6)) {
            us3Var.k(f6, f, f2);
        }
        us3Var.n(b(Utils.FLOAT_EPSILON, f3 - f, f5), b(Utils.FLOAT_EPSILON, f4 - f2, f5));
    }

    public static void d(RectF rectF, RectF rectF2, RectF rectF3, float f) {
        rectF.left = b(rectF2.left, rectF3.left, f);
        rectF.top = b(rectF2.top, rectF3.top, f);
        rectF.right = b(rectF2.right, rectF3.right, f);
        rectF.bottom = b(rectF2.bottom, rectF3.bottom, f);
    }

    public static void e(Matrix matrix, Rect rect) {
        RectF rectF = c;
        rectF.set(rect);
        matrix.mapRect(rectF);
        rect.set((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
    }

    public static float f(float f, float f2, float f3) {
        return Math.max(f2, Math.min(f, f3));
    }
}
