package defpackage;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;

/* compiled from: KeyguardUtils.java */
/* renamed from: rx1  reason: default package */
/* loaded from: classes.dex */
public class rx1 {

    /* compiled from: KeyguardUtils.java */
    /* renamed from: rx1$a */
    /* loaded from: classes.dex */
    public static class a {
        public static boolean a(KeyguardManager keyguardManager) {
            return keyguardManager.isKeyguardSecure();
        }
    }

    /* compiled from: KeyguardUtils.java */
    /* renamed from: rx1$b */
    /* loaded from: classes.dex */
    public static class b {
        public static KeyguardManager a(Context context) {
            return (KeyguardManager) context.getSystemService(KeyguardManager.class);
        }

        public static boolean b(KeyguardManager keyguardManager) {
            return keyguardManager.isDeviceSecure();
        }
    }

    public static KeyguardManager a(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return b.a(context);
        }
        Object systemService = context.getSystemService("keyguard");
        if (systemService instanceof KeyguardManager) {
            return (KeyguardManager) systemService;
        }
        return null;
    }

    public static boolean b(Context context) {
        KeyguardManager a2 = a(context);
        if (a2 == null) {
            return false;
        }
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            return b.b(a2);
        }
        if (i >= 16) {
            return a.a(a2);
        }
        return false;
    }
}
