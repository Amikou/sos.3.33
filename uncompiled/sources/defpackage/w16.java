package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: w16  reason: default package */
/* loaded from: classes.dex */
public final class w16 implements yp5<x16> {
    public static final w16 f0 = new w16();
    public final yp5<x16> a = gq5.a(gq5.b(new y16()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final x16 zza() {
        return this.a.zza();
    }
}
