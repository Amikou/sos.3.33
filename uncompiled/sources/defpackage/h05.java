package defpackage;

import android.os.Looper;
import android.os.Message;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: h05  reason: default package */
/* loaded from: classes.dex */
public final class h05 extends q25 {
    public final /* synthetic */ d05 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h05(d05 d05Var, Looper looper) {
        super(looper);
        this.a = d05Var;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            this.a.v();
        } else if (i == 2) {
            this.a.u();
        } else {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
        }
    }
}
