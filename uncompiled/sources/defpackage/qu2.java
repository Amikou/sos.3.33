package defpackage;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.Mac;

/* compiled from: PrfHmacJce.java */
/* renamed from: qu2  reason: default package */
/* loaded from: classes2.dex */
public final class qu2 implements ou2 {
    public final ThreadLocal<Mac> a;
    public final String b;
    public final Key c;
    public final int d;

    /* compiled from: PrfHmacJce.java */
    /* renamed from: qu2$a */
    /* loaded from: classes2.dex */
    public class a extends ThreadLocal<Mac> {
        public a() {
        }

        @Override // java.lang.ThreadLocal
        /* renamed from: a */
        public Mac initialValue() {
            try {
                Mac a = lv0.g.a(qu2.this.b);
                a.init(qu2.this.c);
                return a;
            } catch (GeneralSecurityException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public qu2(String str, Key key) throws GeneralSecurityException {
        a aVar = new a();
        this.a = aVar;
        this.b = str;
        this.c = key;
        if (key.getEncoded().length >= 16) {
            str.hashCode();
            char c = 65535;
            switch (str.hashCode()) {
                case -1823053428:
                    if (str.equals("HMACSHA1")) {
                        c = 0;
                        break;
                    }
                    break;
                case 392315118:
                    if (str.equals("HMACSHA256")) {
                        c = 1;
                        break;
                    }
                    break;
                case 392316170:
                    if (str.equals("HMACSHA384")) {
                        c = 2;
                        break;
                    }
                    break;
                case 392317873:
                    if (str.equals("HMACSHA512")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    this.d = 20;
                    break;
                case 1:
                    this.d = 32;
                    break;
                case 2:
                    this.d = 48;
                    break;
                case 3:
                    this.d = 64;
                    break;
                default:
                    throw new NoSuchAlgorithmException("unknown Hmac algorithm: " + str);
            }
            aVar.get();
            return;
        }
        throw new InvalidAlgorithmParameterException("key size too small, need at least 16 bytes");
    }

    @Override // defpackage.ou2
    public byte[] a(byte[] bArr, int i) throws GeneralSecurityException {
        if (i <= this.d) {
            this.a.get().update(bArr);
            return Arrays.copyOf(this.a.get().doFinal(), i);
        }
        throw new InvalidAlgorithmParameterException("tag size too big");
    }
}
