package defpackage;

import kotlinx.coroutines.internal.OnUndeliveredElementKt;

/* compiled from: AbstractChannel.kt */
/* renamed from: wj3  reason: default package */
/* loaded from: classes2.dex */
public final class wj3<E> extends vj3<E> {
    public final tc1<E, te4> j0;

    /* JADX WARN: Multi-variable type inference failed */
    public wj3(E e, ov<? super te4> ovVar, tc1<? super E, te4> tc1Var) {
        super(e, ovVar);
        this.j0 = tc1Var;
    }

    @Override // defpackage.tj3
    public void C() {
        OnUndeliveredElementKt.b(this.j0, z(), this.i0.getContext());
    }

    @Override // defpackage.l12
    public boolean t() {
        if (super.t()) {
            C();
            return true;
        }
        return false;
    }
}
