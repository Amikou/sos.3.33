package defpackage;

import com.google.android.gms.internal.measurement.z1;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: rx5  reason: default package */
/* loaded from: classes.dex */
public interface rx5 {
    boolean zza();

    z1 zzb();

    int zzc();
}
