package defpackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.PropertyMetadata;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.POJOPropertyBuilder;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* compiled from: POJOPropertyBuilder.java */
/* renamed from: no2  reason: default package */
/* loaded from: classes.dex */
public class no2 extends vo implements Comparable<no2> {
    public final boolean f0;
    public final MapperConfig<?> g0;
    public final AnnotationIntrospector h0;
    public final PropertyName i0;
    public final PropertyName j0;
    public k<AnnotatedField> k0;
    public k<AnnotatedParameter> l0;
    public k<AnnotatedMethod> m0;
    public k<AnnotatedMethod> n0;

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[JsonProperty.Access.values().length];
            a = iArr;
            try {
                iArr[JsonProperty.Access.READ_ONLY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[JsonProperty.Access.READ_WRITE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[JsonProperty.Access.WRITE_ONLY.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[JsonProperty.Access.AUTO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$b */
    /* loaded from: classes.dex */
    public class b implements m<Class<?>[]> {
        public b() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public Class<?>[] a(AnnotatedMember annotatedMember) {
            return no2.this.h0.findViews(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$c */
    /* loaded from: classes.dex */
    public class c implements m<AnnotationIntrospector.ReferenceProperty> {
        public c() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public AnnotationIntrospector.ReferenceProperty a(AnnotatedMember annotatedMember) {
            return no2.this.h0.findReferenceType(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$d */
    /* loaded from: classes.dex */
    public class d implements m<Boolean> {
        public d() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public Boolean a(AnnotatedMember annotatedMember) {
            return no2.this.h0.isTypeId(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$e */
    /* loaded from: classes.dex */
    public class e implements m<Boolean> {
        public e() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public Boolean a(AnnotatedMember annotatedMember) {
            return no2.this.h0.hasRequiredMarker(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$f */
    /* loaded from: classes.dex */
    public class f implements m<String> {
        public f() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public String a(AnnotatedMember annotatedMember) {
            return no2.this.h0.findPropertyDescription(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$g */
    /* loaded from: classes.dex */
    public class g implements m<Integer> {
        public g() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public Integer a(AnnotatedMember annotatedMember) {
            return no2.this.h0.findPropertyIndex(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$h */
    /* loaded from: classes.dex */
    public class h implements m<String> {
        public h() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public String a(AnnotatedMember annotatedMember) {
            return no2.this.h0.findPropertyDefaultValue(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$i */
    /* loaded from: classes.dex */
    public class i implements m<jl2> {
        public i() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public jl2 a(AnnotatedMember annotatedMember) {
            jl2 findObjectIdInfo = no2.this.h0.findObjectIdInfo(annotatedMember);
            return findObjectIdInfo != null ? no2.this.h0.findObjectReferenceInfo(annotatedMember, findObjectIdInfo) : findObjectIdInfo;
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$j */
    /* loaded from: classes.dex */
    public class j implements m<JsonProperty.Access> {
        public j() {
        }

        @Override // defpackage.no2.m
        /* renamed from: b */
        public JsonProperty.Access a(AnnotatedMember annotatedMember) {
            return no2.this.h0.findPropertyAccess(annotatedMember);
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$k */
    /* loaded from: classes.dex */
    public static final class k<T> {
        public final T a;
        public final k<T> b;
        public final PropertyName c;
        public final boolean d;
        public final boolean e;
        public final boolean f;

        public k(T t, k<T> kVar, PropertyName propertyName, boolean z, boolean z2, boolean z3) {
            this.a = t;
            this.b = kVar;
            PropertyName propertyName2 = (propertyName == null || propertyName.isEmpty()) ? null : propertyName;
            this.c = propertyName2;
            if (z) {
                if (propertyName2 != null) {
                    if (!propertyName.hasSimpleName()) {
                        z = false;
                    }
                } else {
                    throw new IllegalArgumentException("Can not pass true for 'explName' if name is null/empty");
                }
            }
            this.d = z;
            this.e = z2;
            this.f = z3;
        }

        public k<T> a(k<T> kVar) {
            k<T> kVar2 = this.b;
            if (kVar2 == null) {
                return c(kVar);
            }
            return c(kVar2.a(kVar));
        }

        public k<T> b() {
            k<T> kVar = this.b;
            if (kVar == null) {
                return this;
            }
            k<T> b = kVar.b();
            if (this.c != null) {
                if (b.c == null) {
                    return c(null);
                }
                return c(b);
            } else if (b.c != null) {
                return b;
            } else {
                boolean z = this.e;
                if (z == b.e) {
                    return c(b);
                }
                return z ? c(null) : b;
            }
        }

        public k<T> c(k<T> kVar) {
            return kVar == this.b ? this : new k<>(this.a, kVar, this.c, this.d, this.e, this.f);
        }

        public k<T> d(T t) {
            return t == this.a ? this : new k<>(t, this.b, this.c, this.d, this.e, this.f);
        }

        public k<T> e() {
            k<T> e;
            if (this.f) {
                k<T> kVar = this.b;
                if (kVar == null) {
                    return null;
                }
                return kVar.e();
            }
            k<T> kVar2 = this.b;
            return (kVar2 == null || (e = kVar2.e()) == this.b) ? this : c(e);
        }

        public k<T> f() {
            return this.b == null ? this : new k<>(this.a, null, this.c, this.d, this.e, this.f);
        }

        public k<T> g() {
            k<T> kVar = this.b;
            k<T> g = kVar == null ? null : kVar.g();
            return this.e ? c(g) : g;
        }

        public String toString() {
            String str = this.a.toString() + "[visible=" + this.e + ",ignore=" + this.f + ",explicitName=" + this.d + "]";
            if (this.b != null) {
                return str + ", " + this.b.toString();
            }
            return str;
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$l */
    /* loaded from: classes.dex */
    public static class l<T extends AnnotatedMember> implements Iterator<T> {
        public k<T> a;

        public l(k<T> kVar) {
            this.a = kVar;
        }

        @Override // java.util.Iterator
        /* renamed from: a */
        public T next() {
            k<T> kVar = this.a;
            if (kVar != null) {
                T t = kVar.a;
                this.a = kVar.b;
                return t;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.a != null;
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /* compiled from: POJOPropertyBuilder.java */
    /* renamed from: no2$m */
    /* loaded from: classes.dex */
    public interface m<T> {
        T a(AnnotatedMember annotatedMember);
    }

    public no2(MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector, boolean z, PropertyName propertyName) {
        this(mapperConfig, annotationIntrospector, z, propertyName, propertyName);
    }

    public static <T> k<T> r0(k<T> kVar, k<T> kVar2) {
        return kVar == null ? kVar2 : kVar2 == null ? kVar : kVar.a(kVar2);
    }

    @Override // defpackage.vo
    public boolean A() {
        return this.l0 != null;
    }

    @Override // defpackage.vo
    public boolean B() {
        return this.k0 != null;
    }

    @Override // defpackage.vo
    public boolean C() {
        return this.m0 != null;
    }

    @Override // defpackage.vo
    public boolean D(PropertyName propertyName) {
        return this.i0.equals(propertyName);
    }

    @Override // defpackage.vo
    public boolean E() {
        return this.n0 != null;
    }

    @Override // defpackage.vo
    public boolean G() {
        return K(this.k0) || K(this.m0) || K(this.n0) || J(this.l0);
    }

    @Override // defpackage.vo
    public boolean H() {
        return J(this.k0) || J(this.m0) || J(this.n0) || J(this.l0);
    }

    @Override // defpackage.vo
    public boolean I() {
        Boolean bool = (Boolean) n0(new d());
        return bool != null && bool.booleanValue();
    }

    public final <T> boolean J(k<T> kVar) {
        while (kVar != null) {
            if (kVar.c != null && kVar.d) {
                return true;
            }
            kVar = kVar.b;
        }
        return false;
    }

    public final <T> boolean K(k<T> kVar) {
        while (kVar != null) {
            PropertyName propertyName = kVar.c;
            if (propertyName != null && propertyName.hasSimpleName()) {
                return true;
            }
            kVar = kVar.b;
        }
        return false;
    }

    public final <T> boolean L(k<T> kVar) {
        while (kVar != null) {
            if (kVar.f) {
                return true;
            }
            kVar = kVar.b;
        }
        return false;
    }

    public final <T> boolean M(k<T> kVar) {
        while (kVar != null) {
            if (kVar.e) {
                return true;
            }
            kVar = kVar.b;
        }
        return false;
    }

    public final <T extends AnnotatedMember> k<T> N(k<T> kVar, we weVar) {
        AnnotatedMember annotatedMember = (AnnotatedMember) kVar.a.withAnnotations(weVar);
        k<T> kVar2 = kVar.b;
        k kVar3 = kVar;
        if (kVar2 != null) {
            kVar3 = (k<T>) kVar.c(N(kVar2, weVar));
        }
        return kVar3.d(annotatedMember);
    }

    public final void O(Collection<PropertyName> collection, Map<PropertyName, no2> map, k<?> kVar) {
        for (k kVar2 = kVar; kVar2 != null; kVar2 = kVar2.b) {
            PropertyName propertyName = kVar2.c;
            if (kVar2.d && propertyName != null) {
                no2 no2Var = map.get(propertyName);
                if (no2Var == null) {
                    no2Var = new no2(this.g0, this.h0, this.f0, this.j0, propertyName);
                    map.put(propertyName, no2Var);
                }
                if (kVar == this.k0) {
                    no2Var.k0 = kVar2.c(no2Var.k0);
                } else if (kVar == this.m0) {
                    no2Var.m0 = kVar2.c(no2Var.m0);
                } else if (kVar == this.n0) {
                    no2Var.n0 = kVar2.c(no2Var.n0);
                } else if (kVar == this.l0) {
                    no2Var.l0 = kVar2.c(no2Var.l0);
                } else {
                    throw new IllegalStateException("Internal error: mismatched accessors, property: " + this);
                }
            } else if (kVar2.e) {
                throw new IllegalStateException("Conflicting/ambiguous property name definitions (implicit name '" + this.i0 + "'): found multiple explicit names: " + collection + ", but also implicit accessor: " + kVar2);
            }
        }
    }

    public String P() {
        return (String) n0(new h());
    }

    public String Q() {
        return (String) n0(new f());
    }

    /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
        r2 = r2;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.util.Set<com.fasterxml.jackson.databind.PropertyName> R(defpackage.no2.k<? extends com.fasterxml.jackson.databind.introspect.AnnotatedMember> r2, java.util.Set<com.fasterxml.jackson.databind.PropertyName> r3) {
        /*
            r1 = this;
        L0:
            if (r2 == 0) goto L1a
            boolean r0 = r2.d
            if (r0 == 0) goto L17
            com.fasterxml.jackson.databind.PropertyName r0 = r2.c
            if (r0 != 0) goto Lb
            goto L17
        Lb:
            if (r3 != 0) goto L12
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
        L12:
            com.fasterxml.jackson.databind.PropertyName r0 = r2.c
            r3.add(r0)
        L17:
            no2$k<T> r2 = r2.b
            goto L0
        L1a:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.no2.R(no2$k, java.util.Set):java.util.Set");
    }

    public Integer S() {
        return (Integer) n0(new g());
    }

    public Boolean T() {
        return (Boolean) n0(new e());
    }

    public final <T extends AnnotatedMember> we U(k<T> kVar) {
        we allAnnotations = kVar.a.getAllAnnotations();
        k<T> kVar2 = kVar.b;
        return kVar2 != null ? we.h(allAnnotations, U(kVar2)) : allAnnotations;
    }

    public int V(AnnotatedMethod annotatedMethod) {
        String name = annotatedMethod.getName();
        if (!name.startsWith("get") || name.length() <= 3) {
            return (!name.startsWith("is") || name.length() <= 2) ? 3 : 2;
        }
        return 1;
    }

    public final we W(int i2, POJOPropertyBuilder.Linked<? extends AnnotatedMember>... linkedArr) {
        we U = U(linkedArr[i2]);
        do {
            i2++;
            if (i2 >= linkedArr.length) {
                return U;
            }
        } while (linkedArr[i2] == null);
        return we.h(U, W(i2, linkedArr));
    }

    public final <T> k<T> X(k<T> kVar) {
        return kVar == null ? kVar : kVar.e();
    }

    public final <T> k<T> Y(k<T> kVar) {
        return kVar == null ? kVar : kVar.g();
    }

    public int Z(AnnotatedMethod annotatedMethod) {
        String name = annotatedMethod.getName();
        return (!name.startsWith("set") || name.length() <= 3) ? 2 : 1;
    }

    @Override // defpackage.vo
    public boolean a() {
        return (this.l0 == null && this.n0 == null && this.k0 == null) ? false : true;
    }

    public final <T> k<T> a0(k<T> kVar) {
        return kVar == null ? kVar : kVar.b();
    }

    public void b0(no2 no2Var) {
        this.k0 = r0(this.k0, no2Var.k0);
        this.l0 = r0(this.l0, no2Var.l0);
        this.m0 = r0(this.m0, no2Var.m0);
        this.n0 = r0(this.n0, no2Var.n0);
    }

    public void c0(AnnotatedParameter annotatedParameter, PropertyName propertyName, boolean z, boolean z2, boolean z3) {
        this.l0 = new k<>(annotatedParameter, this.l0, propertyName, z, z2, z3);
    }

    @Override // defpackage.vo
    public boolean d() {
        return (this.m0 == null && this.k0 == null) ? false : true;
    }

    public void d0(AnnotatedField annotatedField, PropertyName propertyName, boolean z, boolean z2, boolean z3) {
        this.k0 = new k<>(annotatedField, this.k0, propertyName, z, z2, z3);
    }

    @Override // defpackage.vo
    public JsonInclude.Value e() {
        AnnotatedMember j2 = j();
        AnnotationIntrospector annotationIntrospector = this.h0;
        JsonInclude.Value findPropertyInclusion = annotationIntrospector == null ? null : annotationIntrospector.findPropertyInclusion(j2);
        return findPropertyInclusion == null ? JsonInclude.Value.empty() : findPropertyInclusion;
    }

    public void e0(AnnotatedMethod annotatedMethod, PropertyName propertyName, boolean z, boolean z2, boolean z3) {
        this.m0 = new k<>(annotatedMethod, this.m0, propertyName, z, z2, z3);
    }

    @Override // defpackage.vo
    public jl2 f() {
        return (jl2) n0(new i());
    }

    public void f0(AnnotatedMethod annotatedMethod, PropertyName propertyName, boolean z, boolean z2, boolean z3) {
        this.n0 = new k<>(annotatedMethod, this.n0, propertyName, z, z2, z3);
    }

    @Override // defpackage.vo
    public AnnotationIntrospector.ReferenceProperty g() {
        return (AnnotationIntrospector.ReferenceProperty) n0(new c());
    }

    public boolean g0() {
        return L(this.k0) || L(this.m0) || L(this.n0) || L(this.l0);
    }

    @Override // defpackage.vo
    public Class<?>[] h() {
        return (Class[]) n0(new b());
    }

    public boolean h0() {
        return M(this.k0) || M(this.m0) || M(this.n0) || M(this.l0);
    }

    @Override // defpackage.vo
    public AnnotatedMember j() {
        AnnotatedMethod p = p();
        return p == null ? l() : p;
    }

    @Override // java.lang.Comparable
    /* renamed from: j0 */
    public int compareTo(no2 no2Var) {
        if (this.l0 != null) {
            if (no2Var.l0 == null) {
                return -1;
            }
        } else if (no2Var.l0 != null) {
            return 1;
        }
        return t().compareTo(no2Var.t());
    }

    @Override // defpackage.vo
    public Iterator<AnnotatedParameter> k() {
        k<AnnotatedParameter> kVar = this.l0;
        if (kVar == null) {
            return com.fasterxml.jackson.databind.util.c.k();
        }
        return new l(kVar);
    }

    public Collection<no2> k0(Collection<PropertyName> collection) {
        HashMap hashMap = new HashMap();
        O(collection, hashMap, this.k0);
        O(collection, hashMap, this.m0);
        O(collection, hashMap, this.n0);
        O(collection, hashMap, this.l0);
        return hashMap.values();
    }

    @Override // defpackage.vo
    public AnnotatedField l() {
        k<AnnotatedField> kVar = this.k0;
        if (kVar == null) {
            return null;
        }
        AnnotatedField annotatedField = kVar.a;
        for (k kVar2 = kVar.b; kVar2 != null; kVar2 = kVar2.b) {
            AnnotatedField annotatedField2 = (AnnotatedField) kVar2.a;
            Class<?> declaringClass = annotatedField.getDeclaringClass();
            Class<?> declaringClass2 = annotatedField2.getDeclaringClass();
            if (declaringClass != declaringClass2) {
                if (declaringClass.isAssignableFrom(declaringClass2)) {
                    annotatedField = annotatedField2;
                } else if (declaringClass2.isAssignableFrom(declaringClass)) {
                }
            }
            throw new IllegalArgumentException("Multiple fields representing property \"" + t() + "\": " + annotatedField.getFullName() + " vs " + annotatedField2.getFullName());
        }
        return annotatedField;
    }

    public JsonProperty.Access l0() {
        return (JsonProperty.Access) o0(new j(), JsonProperty.Access.AUTO);
    }

    public Set<PropertyName> m0() {
        Set<PropertyName> R = R(this.l0, R(this.n0, R(this.m0, R(this.k0, null))));
        return R == null ? Collections.emptySet() : R;
    }

    public <T> T n0(m<T> mVar) {
        k<AnnotatedMethod> kVar;
        k<AnnotatedField> kVar2;
        if (this.h0 != null) {
            if (this.f0) {
                k<AnnotatedMethod> kVar3 = this.m0;
                if (kVar3 != null) {
                    r1 = mVar.a(kVar3.a);
                }
            } else {
                k<AnnotatedParameter> kVar4 = this.l0;
                r1 = kVar4 != null ? mVar.a(kVar4.a) : null;
                if (r1 == null && (kVar = this.n0) != null) {
                    r1 = mVar.a(kVar.a);
                }
            }
            return (r1 != null || (kVar2 = this.k0) == null) ? r1 : mVar.a(kVar2.a);
        }
        return null;
    }

    @Override // defpackage.vo
    public PropertyName o() {
        return this.i0;
    }

    public <T> T o0(m<T> mVar, T t) {
        T a2;
        T a3;
        T a4;
        T a5;
        T a6;
        T a7;
        T a8;
        T a9;
        if (this.h0 == null) {
            return null;
        }
        if (this.f0) {
            k<AnnotatedMethod> kVar = this.m0;
            if (kVar == null || (a9 = mVar.a(kVar.a)) == null || a9 == t) {
                k<AnnotatedField> kVar2 = this.k0;
                if (kVar2 == null || (a8 = mVar.a(kVar2.a)) == null || a8 == t) {
                    k<AnnotatedParameter> kVar3 = this.l0;
                    if (kVar3 == null || (a7 = mVar.a(kVar3.a)) == null || a7 == t) {
                        k<AnnotatedMethod> kVar4 = this.n0;
                        if (kVar4 == null || (a6 = mVar.a(kVar4.a)) == null || a6 == t) {
                            return null;
                        }
                        return a6;
                    }
                    return a7;
                }
                return a8;
            }
            return a9;
        }
        k<AnnotatedParameter> kVar5 = this.l0;
        if (kVar5 == null || (a5 = mVar.a(kVar5.a)) == null || a5 == t) {
            k<AnnotatedMethod> kVar6 = this.n0;
            if (kVar6 == null || (a4 = mVar.a(kVar6.a)) == null || a4 == t) {
                k<AnnotatedField> kVar7 = this.k0;
                if (kVar7 == null || (a3 = mVar.a(kVar7.a)) == null || a3 == t) {
                    k<AnnotatedMethod> kVar8 = this.m0;
                    if (kVar8 == null || (a2 = mVar.a(kVar8.a)) == null || a2 == t) {
                        return null;
                    }
                    return a2;
                }
                return a3;
            }
            return a4;
        }
        return a5;
    }

    @Override // defpackage.vo
    public AnnotatedMethod p() {
        k<AnnotatedMethod> kVar = this.m0;
        if (kVar == null) {
            return null;
        }
        k<AnnotatedMethod> kVar2 = kVar.b;
        if (kVar2 == null) {
            return kVar.a;
        }
        for (k<AnnotatedMethod> kVar3 = kVar2; kVar3 != null; kVar3 = kVar3.b) {
            Class<?> declaringClass = kVar.a.getDeclaringClass();
            Class<?> declaringClass2 = kVar3.a.getDeclaringClass();
            if (declaringClass != declaringClass2) {
                if (!declaringClass.isAssignableFrom(declaringClass2)) {
                    if (declaringClass2.isAssignableFrom(declaringClass)) {
                        continue;
                    }
                }
                kVar = kVar3;
            }
            int V = V(kVar3.a);
            int V2 = V(kVar.a);
            if (V != V2) {
                if (V >= V2) {
                }
                kVar = kVar3;
            } else {
                throw new IllegalArgumentException("Conflicting getter definitions for property \"" + t() + "\": " + kVar.a.getFullName() + " vs " + kVar3.a.getFullName());
            }
        }
        this.m0 = kVar.f();
        return kVar.a;
    }

    public AnnotatedParameter p0() {
        k kVar = this.l0;
        if (kVar == null) {
            return null;
        }
        while (!(((AnnotatedParameter) kVar.a).getOwner() instanceof AnnotatedConstructor)) {
            kVar = kVar.b;
            if (kVar == null) {
                return this.l0.a;
            }
        }
        return (AnnotatedParameter) kVar.a;
    }

    public String q0() {
        return this.j0.getSimpleName();
    }

    @Override // defpackage.vo
    public PropertyMetadata r() {
        Boolean T = T();
        String Q = Q();
        Integer S = S();
        String P = P();
        if (T == null && S == null && P == null) {
            PropertyMetadata propertyMetadata = PropertyMetadata.STD_REQUIRED_OR_OPTIONAL;
            return Q == null ? propertyMetadata : propertyMetadata.withDescription(Q);
        }
        return PropertyMetadata.construct(T, Q, S, P);
    }

    @Override // defpackage.vo
    public AnnotatedMember s() {
        AnnotatedParameter p0 = p0();
        if (p0 == null) {
            AnnotatedMethod x = x();
            return x == null ? l() : x;
        }
        return p0;
    }

    public void s0(boolean z) {
        if (z) {
            k<AnnotatedMethod> kVar = this.m0;
            if (kVar != null) {
                this.m0 = N(this.m0, W(0, kVar, this.k0, this.l0, this.n0));
                return;
            }
            k<AnnotatedField> kVar2 = this.k0;
            if (kVar2 != null) {
                this.k0 = N(this.k0, W(0, kVar2, this.l0, this.n0));
                return;
            }
            return;
        }
        k<AnnotatedParameter> kVar3 = this.l0;
        if (kVar3 != null) {
            this.l0 = N(this.l0, W(0, kVar3, this.n0, this.k0, this.m0));
            return;
        }
        k<AnnotatedMethod> kVar4 = this.n0;
        if (kVar4 != null) {
            this.n0 = N(this.n0, W(0, kVar4, this.k0, this.m0));
            return;
        }
        k<AnnotatedField> kVar5 = this.k0;
        if (kVar5 != null) {
            this.k0 = N(this.k0, W(0, kVar5, this.m0));
        }
    }

    @Override // defpackage.vo
    public String t() {
        PropertyName propertyName = this.i0;
        if (propertyName == null) {
            return null;
        }
        return propertyName.getSimpleName();
    }

    public void t0() {
        this.l0 = null;
    }

    public String toString() {
        return "[Property '" + this.i0 + "'; ctors: " + this.l0 + ", field(s): " + this.k0 + ", getter(s): " + this.m0 + ", setter(s): " + this.n0 + "]";
    }

    @Override // defpackage.vo
    public AnnotatedMember u() {
        AnnotatedMethod x = x();
        return x == null ? l() : x;
    }

    public void u0() {
        this.k0 = X(this.k0);
        this.m0 = X(this.m0);
        this.n0 = X(this.n0);
        this.l0 = X(this.l0);
    }

    @Override // defpackage.vo
    public AnnotatedMember v() {
        if (this.f0) {
            return j();
        }
        return s();
    }

    public JsonProperty.Access v0(boolean z) {
        JsonProperty.Access l0 = l0();
        if (l0 == null) {
            l0 = JsonProperty.Access.AUTO;
        }
        int i2 = a.a[l0.ordinal()];
        if (i2 == 1) {
            this.n0 = null;
            this.l0 = null;
            if (!this.f0) {
                this.k0 = null;
            }
        } else if (i2 != 2) {
            if (i2 != 3) {
                this.m0 = Y(this.m0);
                this.l0 = Y(this.l0);
                if (!z || this.m0 == null) {
                    this.k0 = Y(this.k0);
                    this.n0 = Y(this.n0);
                }
            } else {
                this.m0 = null;
                if (this.f0) {
                    this.k0 = null;
                }
            }
        }
        return l0;
    }

    public void w0() {
        this.k0 = a0(this.k0);
        this.m0 = a0(this.m0);
        this.n0 = a0(this.n0);
        this.l0 = a0(this.l0);
    }

    @Override // defpackage.vo
    public AnnotatedMethod x() {
        k<AnnotatedMethod> kVar = this.n0;
        if (kVar == null) {
            return null;
        }
        k<AnnotatedMethod> kVar2 = kVar.b;
        if (kVar2 == null) {
            return kVar.a;
        }
        for (k<AnnotatedMethod> kVar3 = kVar2; kVar3 != null; kVar3 = kVar3.b) {
            Class<?> declaringClass = kVar.a.getDeclaringClass();
            Class<?> declaringClass2 = kVar3.a.getDeclaringClass();
            if (declaringClass != declaringClass2) {
                if (!declaringClass.isAssignableFrom(declaringClass2)) {
                    if (declaringClass2.isAssignableFrom(declaringClass)) {
                        continue;
                    }
                }
                kVar = kVar3;
            }
            AnnotatedMethod annotatedMethod = kVar3.a;
            AnnotatedMethod annotatedMethod2 = kVar.a;
            int Z = Z(annotatedMethod);
            int Z2 = Z(annotatedMethod2);
            if (Z == Z2) {
                AnnotationIntrospector annotationIntrospector = this.h0;
                if (annotationIntrospector != null) {
                    AnnotatedMethod resolveSetterConflict = annotationIntrospector.resolveSetterConflict(this.g0, annotatedMethod2, annotatedMethod);
                    if (resolveSetterConflict != annotatedMethod2) {
                        if (resolveSetterConflict != annotatedMethod) {
                        }
                        kVar = kVar3;
                    } else {
                        continue;
                    }
                }
                throw new IllegalArgumentException(String.format("Conflicting setter definitions for property \"%s\": %s vs %s", t(), kVar.a.getFullName(), kVar3.a.getFullName()));
            }
            if (Z >= Z2) {
            }
            kVar = kVar3;
        }
        this.n0 = kVar.f();
        return kVar.a;
    }

    public no2 x0(PropertyName propertyName) {
        return new no2(this, propertyName);
    }

    @Override // defpackage.vo
    public PropertyName y() {
        AnnotationIntrospector annotationIntrospector;
        AnnotatedMember v = v();
        if (v == null || (annotationIntrospector = this.h0) == null) {
            return null;
        }
        return annotationIntrospector.findWrapperName(v);
    }

    public no2 z0(String str) {
        PropertyName withSimpleName = this.i0.withSimpleName(str);
        return withSimpleName == this.i0 ? this : new no2(this, withSimpleName);
    }

    public no2(MapperConfig<?> mapperConfig, AnnotationIntrospector annotationIntrospector, boolean z, PropertyName propertyName, PropertyName propertyName2) {
        this.g0 = mapperConfig;
        this.h0 = annotationIntrospector;
        this.j0 = propertyName;
        this.i0 = propertyName2;
        this.f0 = z;
    }

    public no2(no2 no2Var, PropertyName propertyName) {
        this.g0 = no2Var.g0;
        this.h0 = no2Var.h0;
        this.j0 = no2Var.j0;
        this.i0 = propertyName;
        this.k0 = no2Var.k0;
        this.l0 = no2Var.l0;
        this.m0 = no2Var.m0;
        this.n0 = no2Var.n0;
        this.f0 = no2Var.f0;
    }
}
