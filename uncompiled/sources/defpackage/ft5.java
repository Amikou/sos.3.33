package defpackage;

import android.util.Pair;
import defpackage.ba;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ft5  reason: default package */
/* loaded from: classes.dex */
public final class ft5 extends jv5 {
    public String d;
    public boolean e;
    public long f;
    public final ph5 g;
    public final ph5 h;
    public final ph5 i;
    public final ph5 j;
    public final ph5 k;

    public ft5(fw5 fw5Var) {
        super(fw5Var);
        mi5 A = this.a.A();
        A.getClass();
        this.g = new ph5(A, "last_delete_stale", 0L);
        mi5 A2 = this.a.A();
        A2.getClass();
        this.h = new ph5(A2, "backoff", 0L);
        mi5 A3 = this.a.A();
        A3.getClass();
        this.i = new ph5(A3, "last_upload", 0L);
        mi5 A4 = this.a.A();
        A4.getClass();
        this.j = new ph5(A4, "last_upload_attempt", 0L);
        mi5 A5 = this.a.A();
        A5.getClass();
        this.k = new ph5(A5, "midnight_offset", 0L);
    }

    @Override // defpackage.jv5
    public final boolean h() {
        return false;
    }

    public final Pair<String, Boolean> j(String str, t45 t45Var) {
        if (t45Var.f()) {
            return k(str);
        }
        return new Pair<>("", Boolean.FALSE);
    }

    @Deprecated
    public final Pair<String, Boolean> k(String str) {
        e();
        long b = this.a.a().b();
        String str2 = this.d;
        if (str2 != null && b < this.f) {
            return new Pair<>(str2, Boolean.valueOf(this.e));
        }
        this.f = b + this.a.z().r(str, qf5.b);
        ba.d(true);
        try {
            ba.a b2 = ba.b(this.a.m());
            this.d = "";
            String a = b2.a();
            if (a != null) {
                this.d = a;
            }
            this.e = b2.b();
        } catch (Exception e) {
            this.a.w().u().b("Unable to get advertising id", e);
            this.d = "";
        }
        ba.d(false);
        return new Pair<>(this.d, Boolean.valueOf(this.e));
    }

    @Deprecated
    public final String l(String str) {
        e();
        String str2 = (String) k(str).first;
        MessageDigest B = sw5.B();
        if (B == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, B.digest(str2.getBytes())));
    }
}
