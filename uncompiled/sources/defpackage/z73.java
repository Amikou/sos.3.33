package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: ResourceEncoderRegistry.java */
/* renamed from: z73  reason: default package */
/* loaded from: classes.dex */
public class z73 {
    public final List<a<?>> a = new ArrayList();

    /* compiled from: ResourceEncoderRegistry.java */
    /* renamed from: z73$a */
    /* loaded from: classes.dex */
    public static final class a<T> {
        public final Class<T> a;
        public final y73<T> b;

        public a(Class<T> cls, y73<T> y73Var) {
            this.a = cls;
            this.b = y73Var;
        }

        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    public synchronized <Z> void a(Class<Z> cls, y73<Z> y73Var) {
        this.a.add(new a<>(cls, y73Var));
    }

    public synchronized <Z> y73<Z> b(Class<Z> cls) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            a<?> aVar = this.a.get(i);
            if (aVar.a(cls)) {
                return (y73<Z>) aVar.b;
            }
        }
        return null;
    }
}
