package defpackage;

import android.content.res.AssetManager;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import androidx.exifinterface.media.ExifInterface;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

/* compiled from: ExifInterface.java */
/* renamed from: az0  reason: default package */
/* loaded from: classes.dex */
public class az0 {
    public static SimpleDateFormat J;
    public static final String[] K;
    public static final int[] L;
    public static final byte[] M;
    public static final d[] N;
    public static final d[] O;
    public static final d[] P;
    public static final d[] Q;
    public static final d[] R;
    public static final d S;
    public static final d[] T;
    public static final d[] U;
    public static final d[] V;
    public static final d[] W;
    public static final d[][] X;
    public static final d[] Y;
    public static final HashMap<Integer, ExifInterface.ExifTag>[] Z;
    public static final HashMap<String, ExifInterface.ExifTag>[] a0;
    public static final HashSet<String> b0;
    public static final HashMap<Integer, Integer> c0;
    public static final Charset d0;
    public static final byte[] e0;
    public static final byte[] f0;
    public String a;
    public FileDescriptor b;
    public AssetManager.AssetInputStream c;
    public int d;
    public boolean e;
    public final HashMap<String, ExifInterface.ExifAttribute>[] f;
    public Set<Integer> g;
    public ByteOrder h;
    public boolean i;
    public int j;
    public int k;
    public int l;
    public int m;
    public int n;
    public int o;
    public int p;
    public int q;
    public static final boolean r = Log.isLoggable("ExifInterface", 3);
    public static final List<Integer> s = Arrays.asList(1, 6, 3, 8);
    public static final List<Integer> t = Arrays.asList(2, 7, 4, 5);
    public static final int[] u = {8, 8, 8};
    public static final int[] v = {8};
    public static final byte[] w = {-1, -40, -1};
    public static final byte[] x = {102, 116, 121, 112};
    public static final byte[] y = {109, 105, 102, 49};
    public static final byte[] z = {104, 101, 105, 99};
    public static final byte[] A = {79, 76, 89, 77, 80, 0};
    public static final byte[] B = {79, 76, 89, 77, 80, 85, 83, 0, 73, 73};
    public static final byte[] C = {-119, 80, 78, 71, 13, 10, 26, 10};
    public static final byte[] D = {101, 88, 73, 102};
    public static final byte[] E = {73, 72, 68, 82};
    public static final byte[] F = {73, 69, 78, 68};
    public static final byte[] G = {82, 73, 70, 70};
    public static final byte[] H = {87, 69, 66, 80};
    public static final byte[] I = {69, 88, 73, 70};

    /* compiled from: ExifInterface.java */
    /* renamed from: az0$a */
    /* loaded from: classes.dex */
    public class a extends MediaDataSource {
        public long a;
        public final /* synthetic */ b f0;

        public a(az0 az0Var, b bVar) {
            this.f0 = bVar;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @Override // android.media.MediaDataSource
        public long getSize() throws IOException {
            return -1L;
        }

        @Override // android.media.MediaDataSource
        public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
            if (i2 == 0) {
                return 0;
            }
            if (j < 0) {
                return -1;
            }
            try {
                long j2 = this.a;
                if (j2 != j) {
                    if (j2 >= 0 && j >= j2 + this.f0.available()) {
                        return -1;
                    }
                    this.f0.d(j);
                    this.a = j;
                }
                if (i2 > this.f0.available()) {
                    i2 = this.f0.available();
                }
                int read = this.f0.read(bArr, i, i2);
                if (read >= 0) {
                    this.a += read;
                    return read;
                }
            } catch (IOException unused) {
            }
            this.a = -1L;
            return -1;
        }
    }

    /* compiled from: ExifInterface.java */
    /* renamed from: az0$b */
    /* loaded from: classes.dex */
    public static class b extends InputStream implements DataInput {
        public static final ByteOrder i0 = ByteOrder.LITTLE_ENDIAN;
        public static final ByteOrder j0 = ByteOrder.BIG_ENDIAN;
        public DataInputStream a;
        public ByteOrder f0;
        public final int g0;
        public int h0;

        public b(InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }

        public int a() {
            return this.g0;
        }

        @Override // java.io.InputStream
        public int available() throws IOException {
            return this.a.available();
        }

        public int b() {
            return this.h0;
        }

        public long c() throws IOException {
            return readInt() & 4294967295L;
        }

        public void d(long j) throws IOException {
            int i = this.h0;
            if (i > j) {
                this.h0 = 0;
                this.a.reset();
                this.a.mark(this.g0);
            } else {
                j -= i;
            }
            int i2 = (int) j;
            if (skipBytes(i2) != i2) {
                throw new IOException("Couldn't seek up to the byteCount");
            }
        }

        public void e(ByteOrder byteOrder) {
            this.f0 = byteOrder;
        }

        @Override // java.io.InputStream
        public int read() throws IOException {
            this.h0++;
            return this.a.read();
        }

        @Override // java.io.DataInput
        public boolean readBoolean() throws IOException {
            this.h0++;
            return this.a.readBoolean();
        }

        @Override // java.io.DataInput
        public byte readByte() throws IOException {
            int i = this.h0 + 1;
            this.h0 = i;
            if (i <= this.g0) {
                int read = this.a.read();
                if (read >= 0) {
                    return (byte) read;
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @Override // java.io.DataInput
        public char readChar() throws IOException {
            this.h0 += 2;
            return this.a.readChar();
        }

        @Override // java.io.DataInput
        public double readDouble() throws IOException {
            return Double.longBitsToDouble(readLong());
        }

        @Override // java.io.DataInput
        public float readFloat() throws IOException {
            return Float.intBitsToFloat(readInt());
        }

        @Override // java.io.DataInput
        public void readFully(byte[] bArr, int i, int i2) throws IOException {
            int i3 = this.h0 + i2;
            this.h0 = i3;
            if (i3 <= this.g0) {
                if (this.a.read(bArr, i, i2) != i2) {
                    throw new IOException("Couldn't read up to the length of buffer");
                }
                return;
            }
            throw new EOFException();
        }

        @Override // java.io.DataInput
        public int readInt() throws IOException {
            int i = this.h0 + 4;
            this.h0 = i;
            if (i <= this.g0) {
                int read = this.a.read();
                int read2 = this.a.read();
                int read3 = this.a.read();
                int read4 = this.a.read();
                if ((read | read2 | read3 | read4) >= 0) {
                    ByteOrder byteOrder = this.f0;
                    if (byteOrder == i0) {
                        return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
                    }
                    if (byteOrder == j0) {
                        return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
                    }
                    throw new IOException("Invalid byte order: " + this.f0);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @Override // java.io.DataInput
        public String readLine() throws IOException {
            return null;
        }

        @Override // java.io.DataInput
        public long readLong() throws IOException {
            int i = this.h0 + 8;
            this.h0 = i;
            if (i <= this.g0) {
                int read = this.a.read();
                int read2 = this.a.read();
                int read3 = this.a.read();
                int read4 = this.a.read();
                int read5 = this.a.read();
                int read6 = this.a.read();
                int read7 = this.a.read();
                int read8 = this.a.read();
                if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) >= 0) {
                    ByteOrder byteOrder = this.f0;
                    if (byteOrder == i0) {
                        return (read8 << 56) + (read7 << 48) + (read6 << 40) + (read5 << 32) + (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
                    }
                    if (byteOrder == j0) {
                        return (read << 56) + (read2 << 48) + (read3 << 40) + (read4 << 32) + (read5 << 24) + (read6 << 16) + (read7 << 8) + read8;
                    }
                    throw new IOException("Invalid byte order: " + this.f0);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @Override // java.io.DataInput
        public short readShort() throws IOException {
            int i = this.h0 + 2;
            this.h0 = i;
            if (i <= this.g0) {
                int read = this.a.read();
                int read2 = this.a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.f0;
                    if (byteOrder == i0) {
                        return (short) ((read2 << 8) + read);
                    }
                    if (byteOrder == j0) {
                        return (short) ((read << 8) + read2);
                    }
                    throw new IOException("Invalid byte order: " + this.f0);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @Override // java.io.DataInput
        public String readUTF() throws IOException {
            this.h0 += 2;
            return this.a.readUTF();
        }

        @Override // java.io.DataInput
        public int readUnsignedByte() throws IOException {
            this.h0++;
            return this.a.readUnsignedByte();
        }

        @Override // java.io.DataInput
        public int readUnsignedShort() throws IOException {
            int i = this.h0 + 2;
            this.h0 = i;
            if (i <= this.g0) {
                int read = this.a.read();
                int read2 = this.a.read();
                if ((read | read2) >= 0) {
                    ByteOrder byteOrder = this.f0;
                    if (byteOrder == i0) {
                        return (read2 << 8) + read;
                    }
                    if (byteOrder == j0) {
                        return (read << 8) + read2;
                    }
                    throw new IOException("Invalid byte order: " + this.f0);
                }
                throw new EOFException();
            }
            throw new EOFException();
        }

        @Override // java.io.DataInput
        public int skipBytes(int i) throws IOException {
            int min = Math.min(i, this.g0 - this.h0);
            int i2 = 0;
            while (i2 < min) {
                i2 += this.a.skipBytes(min - i2);
            }
            this.h0 += i2;
            return i2;
        }

        public b(InputStream inputStream, ByteOrder byteOrder) throws IOException {
            this.f0 = ByteOrder.BIG_ENDIAN;
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            this.a = dataInputStream;
            int available = dataInputStream.available();
            this.g0 = available;
            this.h0 = 0;
            this.a.mark(available);
            this.f0 = byteOrder;
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.a.read(bArr, i, i2);
            this.h0 += read;
            return read;
        }

        @Override // java.io.DataInput
        public void readFully(byte[] bArr) throws IOException {
            int length = this.h0 + bArr.length;
            this.h0 = length;
            if (length <= this.g0) {
                if (this.a.read(bArr, 0, bArr.length) != bArr.length) {
                    throw new IOException("Couldn't read up to the length of buffer");
                }
                return;
            }
            throw new EOFException();
        }

        public b(byte[] bArr) throws IOException {
            this(new ByteArrayInputStream(bArr));
        }
    }

    /* compiled from: ExifInterface.java */
    /* renamed from: az0$c */
    /* loaded from: classes.dex */
    public static class c {
        public final int a;
        public final int b;
        public final byte[] c;

        public c(int i, int i2, byte[] bArr) {
            this(i, i2, -1L, bArr);
        }

        public static c a(String str) {
            byte[] bytes = (str + (char) 0).getBytes(az0.d0);
            return new c(2, bytes.length, bytes);
        }

        public static c b(long j, ByteOrder byteOrder) {
            return c(new long[]{j}, byteOrder);
        }

        public static c c(long[] jArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[az0.L[4] * jArr.length]);
            wrap.order(byteOrder);
            for (long j : jArr) {
                wrap.putInt((int) j);
            }
            return new c(4, jArr.length, wrap.array());
        }

        public static c d(e eVar, ByteOrder byteOrder) {
            return e(new e[]{eVar}, byteOrder);
        }

        public static c e(e[] eVarArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[az0.L[5] * eVarArr.length]);
            wrap.order(byteOrder);
            for (e eVar : eVarArr) {
                wrap.putInt((int) eVar.a);
                wrap.putInt((int) eVar.b);
            }
            return new c(5, eVarArr.length, wrap.array());
        }

        public static c f(int i, ByteOrder byteOrder) {
            return g(new int[]{i}, byteOrder);
        }

        public static c g(int[] iArr, ByteOrder byteOrder) {
            ByteBuffer wrap = ByteBuffer.wrap(new byte[az0.L[3] * iArr.length]);
            wrap.order(byteOrder);
            for (int i : iArr) {
                wrap.putShort((short) i);
            }
            return new c(3, iArr.length, wrap.array());
        }

        public double h(ByteOrder byteOrder) {
            Object k = k(byteOrder);
            if (k != null) {
                if (k instanceof String) {
                    return Double.parseDouble((String) k);
                }
                if (k instanceof long[]) {
                    long[] jArr = (long[]) k;
                    if (jArr.length == 1) {
                        return jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof int[]) {
                    int[] iArr = (int[]) k;
                    if (iArr.length == 1) {
                        return iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof double[]) {
                    double[] dArr = (double[]) k;
                    if (dArr.length == 1) {
                        return dArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof e[]) {
                    e[] eVarArr = (e[]) k;
                    if (eVarArr.length == 1) {
                        return eVarArr[0].a();
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a double value");
                }
            }
            throw new NumberFormatException("NULL can't be converted to a double value");
        }

        public int i(ByteOrder byteOrder) {
            Object k = k(byteOrder);
            if (k != null) {
                if (k instanceof String) {
                    return Integer.parseInt((String) k);
                }
                if (k instanceof long[]) {
                    long[] jArr = (long[]) k;
                    if (jArr.length == 1) {
                        return (int) jArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else if (k instanceof int[]) {
                    int[] iArr = (int[]) k;
                    if (iArr.length == 1) {
                        return iArr[0];
                    }
                    throw new NumberFormatException("There are more than one component");
                } else {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
            }
            throw new NumberFormatException("NULL can't be converted to a integer value");
        }

        public String j(ByteOrder byteOrder) {
            Object k = k(byteOrder);
            if (k == null) {
                return null;
            }
            if (k instanceof String) {
                return (String) k;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (k instanceof long[]) {
                long[] jArr = (long[]) k;
                while (i < jArr.length) {
                    sb.append(jArr[i]);
                    i++;
                    if (i != jArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (k instanceof int[]) {
                int[] iArr = (int[]) k;
                while (i < iArr.length) {
                    sb.append(iArr[i]);
                    i++;
                    if (i != iArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (k instanceof double[]) {
                double[] dArr = (double[]) k;
                while (i < dArr.length) {
                    sb.append(dArr[i]);
                    i++;
                    if (i != dArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else if (k instanceof e[]) {
                e[] eVarArr = (e[]) k;
                while (i < eVarArr.length) {
                    sb.append(eVarArr[i].a);
                    sb.append('/');
                    sb.append(eVarArr[i].b);
                    i++;
                    if (i != eVarArr.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            } else {
                return null;
            }
        }

        public Object k(ByteOrder byteOrder) {
            b bVar;
            byte b;
            byte[] bArr;
            b bVar2 = null;
            try {
                bVar = new b(this.c);
            } catch (IOException unused) {
                bVar = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                bVar.e(byteOrder);
                boolean z = true;
                int i = 0;
                switch (this.a) {
                    case 1:
                    case 6:
                        byte[] bArr2 = this.c;
                        if (bArr2.length == 1 && bArr2[0] >= 0 && bArr2[0] <= 1) {
                            String str = new String(new char[]{(char) (bArr2[0] + 48)});
                            try {
                                bVar.close();
                            } catch (IOException unused2) {
                            }
                            return str;
                        }
                        String str2 = new String(bArr2, az0.d0);
                        try {
                            bVar.close();
                        } catch (IOException unused3) {
                        }
                        return str2;
                    case 2:
                    case 7:
                        if (this.b >= az0.M.length) {
                            int i2 = 0;
                            while (true) {
                                bArr = az0.M;
                                if (i2 < bArr.length) {
                                    if (this.c[i2] != bArr[i2]) {
                                        z = false;
                                    } else {
                                        i2++;
                                    }
                                }
                            }
                            if (z) {
                                i = bArr.length;
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        while (i < this.b && (b = this.c[i]) != 0) {
                            if (b >= 32) {
                                sb.append((char) b);
                            } else {
                                sb.append('?');
                            }
                            i++;
                        }
                        String sb2 = sb.toString();
                        try {
                            bVar.close();
                        } catch (IOException unused4) {
                        }
                        return sb2;
                    case 3:
                        int[] iArr = new int[this.b];
                        while (i < this.b) {
                            iArr[i] = bVar.readUnsignedShort();
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused5) {
                        }
                        return iArr;
                    case 4:
                        long[] jArr = new long[this.b];
                        while (i < this.b) {
                            jArr[i] = bVar.c();
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused6) {
                        }
                        return jArr;
                    case 5:
                        e[] eVarArr = new e[this.b];
                        while (i < this.b) {
                            eVarArr[i] = new e(bVar.c(), bVar.c());
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused7) {
                        }
                        return eVarArr;
                    case 8:
                        int[] iArr2 = new int[this.b];
                        while (i < this.b) {
                            iArr2[i] = bVar.readShort();
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused8) {
                        }
                        return iArr2;
                    case 9:
                        int[] iArr3 = new int[this.b];
                        while (i < this.b) {
                            iArr3[i] = bVar.readInt();
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused9) {
                        }
                        return iArr3;
                    case 10:
                        e[] eVarArr2 = new e[this.b];
                        while (i < this.b) {
                            eVarArr2[i] = new e(bVar.readInt(), bVar.readInt());
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused10) {
                        }
                        return eVarArr2;
                    case 11:
                        double[] dArr = new double[this.b];
                        while (i < this.b) {
                            dArr[i] = bVar.readFloat();
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused11) {
                        }
                        return dArr;
                    case 12:
                        double[] dArr2 = new double[this.b];
                        while (i < this.b) {
                            dArr2[i] = bVar.readDouble();
                            i++;
                        }
                        try {
                            bVar.close();
                        } catch (IOException unused12) {
                        }
                        return dArr2;
                    default:
                        try {
                            bVar.close();
                        } catch (IOException unused13) {
                        }
                        return null;
                }
            } catch (IOException unused14) {
                if (bVar != null) {
                    try {
                        bVar.close();
                    } catch (IOException unused15) {
                    }
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                bVar2 = bVar;
                if (bVar2 != null) {
                    try {
                        bVar2.close();
                    } catch (IOException unused16) {
                    }
                }
                throw th;
            }
        }

        public String toString() {
            return "(" + az0.K[this.a] + ", data length:" + this.c.length + ")";
        }

        public c(int i, int i2, long j, byte[] bArr) {
            this.a = i;
            this.b = i2;
            this.c = bArr;
        }
    }

    /* compiled from: ExifInterface.java */
    /* renamed from: az0$e */
    /* loaded from: classes.dex */
    public static class e {
        public final long a;
        public final long b;

        public e(long j, long j2) {
            if (j2 == 0) {
                this.a = 0L;
                this.b = 1L;
                return;
            }
            this.a = j;
            this.b = j2;
        }

        public double a() {
            return this.a / this.b;
        }

        public String toString() {
            return this.a + "/" + this.b;
        }
    }

    static {
        ExifInterface.ExifTag[] exifTagArr;
        "VP8X".getBytes(Charset.defaultCharset());
        "VP8L".getBytes(Charset.defaultCharset());
        "VP8 ".getBytes(Charset.defaultCharset());
        "ANIM".getBytes(Charset.defaultCharset());
        "ANMF".getBytes(Charset.defaultCharset());
        "XMP ".getBytes(Charset.defaultCharset());
        K = new String[]{"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD"};
        L = new int[]{0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
        M = new byte[]{65, 83, 67, 73, 73, 0, 0, 0};
        d[] dVarArr = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ImageWidth", 256, 3, 4), new d("ImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("Orientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", 301, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", 531, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("SensorTopBorder", 4, 4), new d("SensorLeftBorder", 5, 4), new d("SensorBottomBorder", 6, 4), new d("SensorRightBorder", 7, 4), new d("ISO", 23, 3), new d("JpgFromRaw", 46, 7), new d("Xmp", 700, 1)};
        N = dVarArr;
        d[] dVarArr2 = {new d("ExposureTime", 33434, 5), new d("FNumber", 33437, 5), new d("ExposureProgram", 34850, 3), new d("SpectralSensitivity", 34852, 2), new d("PhotographicSensitivity", 34855, 3), new d("OECF", 34856, 7), new d("SensitivityType", 34864, 3), new d("StandardOutputSensitivity", 34865, 4), new d("RecommendedExposureIndex", 34866, 4), new d("ISOSpeed", 34867, 4), new d("ISOSpeedLatitudeyyy", 34868, 4), new d("ISOSpeedLatitudezzz", 34869, 4), new d("ExifVersion", 36864, 2), new d("DateTimeOriginal", 36867, 2), new d("DateTimeDigitized", 36868, 2), new d("OffsetTime", 36880, 2), new d("OffsetTimeOriginal", 36881, 2), new d("OffsetTimeDigitized", 36882, 2), new d("ComponentsConfiguration", 37121, 7), new d("CompressedBitsPerPixel", 37122, 5), new d("ShutterSpeedValue", 37377, 10), new d("ApertureValue", 37378, 5), new d("BrightnessValue", 37379, 10), new d("ExposureBiasValue", 37380, 10), new d("MaxApertureValue", 37381, 5), new d("SubjectDistance", 37382, 5), new d("MeteringMode", 37383, 3), new d("LightSource", 37384, 3), new d("Flash", 37385, 3), new d("FocalLength", 37386, 5), new d("SubjectArea", 37396, 3), new d("MakerNote", 37500, 7), new d("UserComment", 37510, 7), new d("SubSecTime", 37520, 2), new d("SubSecTimeOriginal", 37521, 2), new d("SubSecTimeDigitized", 37522, 2), new d("FlashpixVersion", 40960, 7), new d("ColorSpace", 40961, 3), new d("PixelXDimension", 40962, 3, 4), new d("PixelYDimension", 40963, 3, 4), new d("RelatedSoundFile", 40964, 2), new d("InteroperabilityIFDPointer", 40965, 4), new d("FlashEnergy", 41483, 5), new d("SpatialFrequencyResponse", 41484, 7), new d("FocalPlaneXResolution", 41486, 5), new d("FocalPlaneYResolution", 41487, 5), new d("FocalPlaneResolutionUnit", 41488, 3), new d("SubjectLocation", 41492, 3), new d("ExposureIndex", 41493, 5), new d("SensingMethod", 41495, 3), new d("FileSource", 41728, 7), new d("SceneType", 41729, 7), new d("CFAPattern", 41730, 7), new d("CustomRendered", 41985, 3), new d("ExposureMode", 41986, 3), new d("WhiteBalance", 41987, 3), new d("DigitalZoomRatio", 41988, 5), new d("FocalLengthIn35mmFilm", 41989, 3), new d("SceneCaptureType", 41990, 3), new d("GainControl", 41991, 3), new d("Contrast", 41992, 3), new d("Saturation", 41993, 3), new d("Sharpness", 41994, 3), new d("DeviceSettingDescription", 41995, 7), new d("SubjectDistanceRange", 41996, 3), new d("ImageUniqueID", 42016, 2), new d("CameraOwnerName", 42032, 2), new d("BodySerialNumber", 42033, 2), new d("LensSpecification", 42034, 5), new d("LensMake", 42035, 2), new d("LensModel", 42036, 2), new d("Gamma", 42240, 5), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
        O = dVarArr2;
        d[] dVarArr3 = {new d("GPSVersionID", 0, 1), new d("GPSLatitudeRef", 1, 2), new d("GPSLatitude", 2, 5), new d("GPSLongitudeRef", 3, 2), new d("GPSLongitude", 4, 5), new d("GPSAltitudeRef", 5, 1), new d("GPSAltitude", 6, 5), new d("GPSTimeStamp", 7, 5), new d("GPSSatellites", 8, 2), new d("GPSStatus", 9, 2), new d("GPSMeasureMode", 10, 2), new d("GPSDOP", 11, 5), new d("GPSSpeedRef", 12, 2), new d("GPSSpeed", 13, 5), new d("GPSTrackRef", 14, 2), new d("GPSTrack", 15, 5), new d("GPSImgDirectionRef", 16, 2), new d("GPSImgDirection", 17, 5), new d("GPSMapDatum", 18, 2), new d("GPSDestLatitudeRef", 19, 2), new d("GPSDestLatitude", 20, 5), new d("GPSDestLongitudeRef", 21, 2), new d("GPSDestLongitude", 22, 5), new d("GPSDestBearingRef", 23, 2), new d("GPSDestBearing", 24, 5), new d("GPSDestDistanceRef", 25, 2), new d("GPSDestDistance", 26, 5), new d("GPSProcessingMethod", 27, 7), new d("GPSAreaInformation", 28, 7), new d("GPSDateStamp", 29, 2), new d("GPSDifferential", 30, 3), new d("GPSHPositioningError", 31, 5)};
        P = dVarArr3;
        d[] dVarArr4 = {new d("InteroperabilityIndex", 1, 2)};
        Q = dVarArr4;
        d[] dVarArr5 = {new d("NewSubfileType", 254, 4), new d("SubfileType", 255, 4), new d("ThumbnailImageWidth", 256, 3, 4), new d("ThumbnailImageLength", 257, 3, 4), new d("BitsPerSample", 258, 3), new d("Compression", 259, 3), new d("PhotometricInterpretation", 262, 3), new d("ImageDescription", 270, 2), new d("Make", 271, 2), new d("Model", 272, 2), new d("StripOffsets", 273, 3, 4), new d("ThumbnailOrientation", 274, 3), new d("SamplesPerPixel", 277, 3), new d("RowsPerStrip", 278, 3, 4), new d("StripByteCounts", 279, 3, 4), new d("XResolution", 282, 5), new d("YResolution", 283, 5), new d("PlanarConfiguration", 284, 3), new d("ResolutionUnit", 296, 3), new d("TransferFunction", 301, 3), new d("Software", 305, 2), new d("DateTime", 306, 2), new d("Artist", 315, 2), new d("WhitePoint", 318, 5), new d("PrimaryChromaticities", 319, 5), new d("SubIFDPointer", 330, 4), new d("JPEGInterchangeFormat", 513, 4), new d("JPEGInterchangeFormatLength", 514, 4), new d("YCbCrCoefficients", 529, 5), new d("YCbCrSubSampling", 530, 3), new d("YCbCrPositioning", 531, 3), new d("ReferenceBlackWhite", 532, 5), new d("Copyright", 33432, 2), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("DNGVersion", 50706, 1), new d("DefaultCropSize", 50720, 3, 4)};
        R = dVarArr5;
        S = new d("StripOffsets", 273, 3);
        d[] dVarArr6 = {new d("ThumbnailImage", 256, 7), new d("CameraSettingsIFDPointer", 8224, 4), new d("ImageProcessingIFDPointer", 8256, 4)};
        T = dVarArr6;
        d[] dVarArr7 = {new d("PreviewImageStart", 257, 4), new d("PreviewImageLength", 258, 4)};
        U = dVarArr7;
        d[] dVarArr8 = {new d("AspectFrame", 4371, 3)};
        V = dVarArr8;
        d[] dVarArr9 = {new d("ColorSpace", 55, 3)};
        W = dVarArr9;
        d[][] dVarArr10 = {dVarArr, dVarArr2, dVarArr3, dVarArr4, dVarArr5, dVarArr, dVarArr6, dVarArr7, dVarArr8, dVarArr9};
        X = dVarArr10;
        Y = new d[]{new d("SubIFDPointer", 330, 4), new d("ExifIFDPointer", 34665, 4), new d("GPSInfoIFDPointer", 34853, 4), new d("InteroperabilityIFDPointer", 40965, 4), new d("CameraSettingsIFDPointer", 8224, 1), new d("ImageProcessingIFDPointer", 8256, 1)};
        new d("JPEGInterchangeFormat", 513, 4);
        new d("JPEGInterchangeFormatLength", 514, 4);
        Z = new HashMap[dVarArr10.length];
        a0 = new HashMap[dVarArr10.length];
        b0 = new HashSet<>(Arrays.asList("FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"));
        c0 = new HashMap<>();
        Charset forName = Charset.forName("US-ASCII");
        d0 = forName;
        e0 = "Exif\u0000\u0000".getBytes(forName);
        f0 = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(forName);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        J = simpleDateFormat;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        int i = 0;
        while (true) {
            ExifInterface.ExifTag[][] exifTagArr2 = X;
            if (i < exifTagArr2.length) {
                Z[i] = new HashMap<>();
                a0[i] = new HashMap<>();
                for (ExifInterface.ExifTag exifTag : exifTagArr2[i]) {
                    Z[i].put(Integer.valueOf(exifTag.a), exifTag);
                    a0[i].put(exifTag.b, exifTag);
                }
                i++;
            } else {
                HashMap<Integer, Integer> hashMap = c0;
                d[] dVarArr11 = Y;
                hashMap.put(Integer.valueOf(dVarArr11[0].a), 5);
                hashMap.put(Integer.valueOf(dVarArr11[1].a), 1);
                hashMap.put(Integer.valueOf(dVarArr11[2].a), 2);
                hashMap.put(Integer.valueOf(dVarArr11[3].a), 3);
                hashMap.put(Integer.valueOf(dVarArr11[4].a), 7);
                hashMap.put(Integer.valueOf(dVarArr11[5].a), 8);
                Pattern.compile(".*[1-9].*");
                Pattern.compile("^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$");
                return;
            }
        }
    }

    public az0(String str) throws IOException {
        d[][] dVarArr = X;
        this.f = new HashMap[dVarArr.length];
        this.g = new HashSet(dVarArr.length);
        this.h = ByteOrder.BIG_ENDIAN;
        Objects.requireNonNull(str, "filename cannot be null");
        t(str);
    }

    public static boolean B(FileDescriptor fileDescriptor) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Os.lseek(fileDescriptor, 0L, OsConstants.SEEK_CUR);
                return true;
            } catch (Exception unused) {
            }
        }
        return false;
    }

    public static boolean N(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length < bArr2.length) {
            return false;
        }
        for (int i = 0; i < bArr2.length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    public static String b(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i = 0; i < bArr.length; i++) {
            sb.append(String.format("%02x", Byte.valueOf(bArr[i])));
        }
        return sb.toString();
    }

    public static void c(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
            }
        }
    }

    public static long[] d(Object obj) {
        if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            long[] jArr = new long[iArr.length];
            for (int i = 0; i < iArr.length; i++) {
                jArr[i] = iArr[i];
            }
            return jArr;
        } else if (obj instanceof long[]) {
            return (long[]) obj;
        } else {
            return null;
        }
    }

    public static boolean u(BufferedInputStream bufferedInputStream) throws IOException {
        byte[] bArr = e0;
        bufferedInputStream.mark(bArr.length);
        byte[] bArr2 = new byte[bArr.length];
        bufferedInputStream.read(bArr2);
        bufferedInputStream.reset();
        int i = 0;
        while (true) {
            byte[] bArr3 = e0;
            if (i >= bArr3.length) {
                return true;
            }
            if (bArr2[i] != bArr3[i]) {
                return false;
            }
            i++;
        }
    }

    public static boolean w(byte[] bArr) throws IOException {
        int i = 0;
        while (true) {
            byte[] bArr2 = w;
            if (i >= bArr2.length) {
                return true;
            }
            if (bArr[i] != bArr2[i]) {
                return false;
            }
            i++;
        }
    }

    public final boolean A(byte[] bArr) throws IOException {
        b bVar;
        b bVar2 = null;
        try {
            bVar = new b(bArr);
        } catch (Exception unused) {
        } catch (Throwable th) {
            th = th;
        }
        try {
            ByteOrder I2 = I(bVar);
            this.h = I2;
            bVar.e(I2);
            boolean z2 = bVar.readShort() == 85;
            bVar.close();
            return z2;
        } catch (Exception unused2) {
            bVar2 = bVar;
            if (bVar2 != null) {
                bVar2.close();
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            bVar2 = bVar;
            if (bVar2 != null) {
                bVar2.close();
            }
            throw th;
        }
    }

    public final boolean C(HashMap hashMap) throws IOException {
        c cVar;
        c cVar2 = (c) hashMap.get("BitsPerSample");
        if (cVar2 != null) {
            int[] iArr = (int[]) cVar2.k(this.h);
            int[] iArr2 = u;
            if (Arrays.equals(iArr2, iArr)) {
                return true;
            }
            if (this.d != 3 || (cVar = (c) hashMap.get("PhotometricInterpretation")) == null) {
                return false;
            }
            int i = cVar.i(this.h);
            return (i == 1 && Arrays.equals(iArr, v)) || (i == 6 && Arrays.equals(iArr, iArr2));
        }
        return false;
    }

    public final boolean D(HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("ImageLength");
        c cVar2 = (c) hashMap.get("ImageWidth");
        if (cVar == null || cVar2 == null) {
            return false;
        }
        return cVar.i(this.h) <= 512 && cVar2.i(this.h) <= 512;
    }

    public final boolean E(byte[] bArr) throws IOException {
        int i = 0;
        while (true) {
            byte[] bArr2 = G;
            if (i >= bArr2.length) {
                int i2 = 0;
                while (true) {
                    byte[] bArr3 = H;
                    if (i2 >= bArr3.length) {
                        return true;
                    }
                    if (bArr[G.length + i2 + 4] != bArr3[i2]) {
                        return false;
                    }
                    i2++;
                }
            } else if (bArr[i] != bArr2[i]) {
                return false;
            } else {
                i++;
            }
        }
    }

    public final void F(InputStream inputStream) {
        Objects.requireNonNull(inputStream, "inputstream shouldn't be null");
        for (int i = 0; i < X.length; i++) {
            try {
                try {
                    this.f[i] = new HashMap<>();
                } catch (IOException unused) {
                    boolean z2 = r;
                    a();
                    if (!z2) {
                        return;
                    }
                }
            } finally {
                a();
                if (r) {
                    H();
                }
            }
        }
        if (!this.e) {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 5000);
            this.d = j(bufferedInputStream);
            inputStream = bufferedInputStream;
        }
        b bVar = new b(inputStream);
        if (!this.e) {
            switch (this.d) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 5:
                case 6:
                case 8:
                case 11:
                    n(bVar);
                    break;
                case 4:
                    i(bVar, 0, 0);
                    break;
                case 7:
                    k(bVar);
                    break;
                case 9:
                    m(bVar);
                    break;
                case 10:
                    o(bVar);
                    break;
                case 12:
                    h(bVar);
                    break;
                case 13:
                    l(bVar);
                    break;
                case 14:
                    q(bVar);
                    break;
            }
        } else {
            p(bVar);
        }
        M(bVar);
    }

    public final void G(b bVar, int i) throws IOException {
        ByteOrder I2 = I(bVar);
        this.h = I2;
        bVar.e(I2);
        int readUnsignedShort = bVar.readUnsignedShort();
        int i2 = this.d;
        if (i2 != 7 && i2 != 10 && readUnsignedShort != 42) {
            throw new IOException("Invalid start code: " + Integer.toHexString(readUnsignedShort));
        }
        int readInt = bVar.readInt();
        if (readInt >= 8 && readInt < i) {
            int i3 = readInt - 8;
            if (i3 <= 0 || bVar.skipBytes(i3) == i3) {
                return;
            }
            throw new IOException("Couldn't jump to first Ifd: " + i3);
        }
        throw new IOException("Invalid first Ifd offset: " + readInt);
    }

    public final void H() {
        for (int i = 0; i < this.f.length; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append("The size of tag group[");
            sb.append(i);
            sb.append("]: ");
            sb.append(this.f[i].size());
            for (Map.Entry<String, ExifInterface.ExifAttribute> entry : this.f[i].entrySet()) {
                c value = entry.getValue();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("tagName: ");
                sb2.append(entry.getKey());
                sb2.append(", tagType: ");
                sb2.append(value.toString());
                sb2.append(", tagValue: '");
                sb2.append(value.j(this.h));
                sb2.append("'");
            }
        }
    }

    public final ByteOrder I(b bVar) throws IOException {
        short readShort = bVar.readShort();
        if (readShort != 18761) {
            if (readShort == 19789) {
                return ByteOrder.BIG_ENDIAN;
            }
            throw new IOException("Invalid byte order: " + Integer.toHexString(readShort));
        }
        return ByteOrder.LITTLE_ENDIAN;
    }

    public final void J(byte[] bArr, int i) throws IOException {
        b bVar = new b(bArr);
        G(bVar, bArr.length);
        K(bVar, i);
    }

    /* JADX WARN: Removed duplicated region for block: B:104:0x0242  */
    /* JADX WARN: Removed duplicated region for block: B:115:0x0299  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x0119  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x0125  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void K(defpackage.az0.b r30, int r31) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 933
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.az0.K(az0$b, int):void");
    }

    public final void L(b bVar, int i) throws IOException {
        c cVar;
        c cVar2 = this.f[i].get("ImageLength");
        c cVar3 = this.f[i].get("ImageWidth");
        if ((cVar2 == null || cVar3 == null) && (cVar = this.f[i].get("JPEGInterchangeFormat")) != null) {
            i(bVar, cVar.i(this.h), i);
        }
    }

    public final void M(b bVar) throws IOException {
        HashMap<String, ExifInterface.ExifAttribute> hashMap = this.f[4];
        c cVar = hashMap.get("Compression");
        if (cVar != null) {
            int i = cVar.i(this.h);
            this.l = i;
            if (i != 1) {
                if (i == 6) {
                    r(bVar, hashMap);
                    return;
                } else if (i != 7) {
                    return;
                }
            }
            if (C(hashMap)) {
                s(bVar, hashMap);
                return;
            }
            return;
        }
        this.l = 6;
        r(bVar, hashMap);
    }

    public final void O(int i, int i2) throws IOException {
        if (this.f[i].isEmpty() || this.f[i2].isEmpty()) {
            return;
        }
        c cVar = this.f[i].get("ImageLength");
        c cVar2 = this.f[i].get("ImageWidth");
        c cVar3 = this.f[i2].get("ImageLength");
        c cVar4 = this.f[i2].get("ImageWidth");
        if (cVar == null || cVar2 == null || cVar3 == null || cVar4 == null) {
            return;
        }
        int i3 = cVar.i(this.h);
        int i4 = cVar2.i(this.h);
        int i5 = cVar3.i(this.h);
        int i6 = cVar4.i(this.h);
        if (i3 >= i5 || i4 >= i6) {
            return;
        }
        HashMap<String, ExifInterface.ExifAttribute>[] hashMapArr = this.f;
        HashMap<String, ExifInterface.ExifAttribute> hashMap = hashMapArr[i];
        hashMapArr[i] = hashMapArr[i2];
        hashMapArr[i2] = hashMap;
    }

    public final void P(b bVar, int i) throws IOException {
        c f;
        c f2;
        c cVar = this.f[i].get("DefaultCropSize");
        c cVar2 = this.f[i].get("SensorTopBorder");
        c cVar3 = this.f[i].get("SensorLeftBorder");
        c cVar4 = this.f[i].get("SensorBottomBorder");
        c cVar5 = this.f[i].get("SensorRightBorder");
        if (cVar == null) {
            if (cVar2 != null && cVar3 != null && cVar4 != null && cVar5 != null) {
                int i2 = cVar2.i(this.h);
                int i3 = cVar4.i(this.h);
                int i4 = cVar5.i(this.h);
                int i5 = cVar3.i(this.h);
                if (i3 <= i2 || i4 <= i5) {
                    return;
                }
                ExifInterface.ExifAttribute f3 = c.f(i3 - i2, this.h);
                ExifInterface.ExifAttribute f4 = c.f(i4 - i5, this.h);
                this.f[i].put("ImageLength", f3);
                this.f[i].put("ImageWidth", f4);
                return;
            }
            L(bVar, i);
            return;
        }
        if (cVar.a == 5) {
            e[] eVarArr = (e[]) cVar.k(this.h);
            if (eVarArr != null && eVarArr.length == 2) {
                f = c.d(eVarArr[0], this.h);
                f2 = c.d(eVarArr[1], this.h);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Invalid crop size values. cropSize=");
                sb.append(Arrays.toString(eVarArr));
                return;
            }
        } else {
            int[] iArr = (int[]) cVar.k(this.h);
            if (iArr != null && iArr.length == 2) {
                f = c.f(iArr[0], this.h);
                f2 = c.f(iArr[1], this.h);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Invalid crop size values. cropSize=");
                sb2.append(Arrays.toString(iArr));
                return;
            }
        }
        this.f[i].put("ImageWidth", f);
        this.f[i].put("ImageLength", f2);
    }

    public final void Q() throws IOException {
        O(0, 5);
        O(0, 4);
        O(5, 4);
        ExifInterface.ExifAttribute exifAttribute = (c) this.f[1].get("PixelXDimension");
        ExifInterface.ExifAttribute exifAttribute2 = (c) this.f[1].get("PixelYDimension");
        if (exifAttribute != null && exifAttribute2 != null) {
            this.f[0].put("ImageWidth", exifAttribute);
            this.f[0].put("ImageLength", exifAttribute2);
        }
        if (this.f[4].isEmpty() && D(this.f[5])) {
            HashMap<String, ExifInterface.ExifAttribute>[] hashMapArr = this.f;
            hashMapArr[4] = hashMapArr[5];
            hashMapArr[5] = new HashMap<>();
        }
        D(this.f[4]);
    }

    public final void a() {
        String e2 = e("DateTimeOriginal");
        if (e2 != null && e("DateTime") == null) {
            this.f[0].put("DateTime", c.a(e2));
        }
        if (e("ImageWidth") == null) {
            this.f[0].put("ImageWidth", c.b(0L, this.h));
        }
        if (e("ImageLength") == null) {
            this.f[0].put("ImageLength", c.b(0L, this.h));
        }
        if (e("Orientation") == null) {
            this.f[0].put("Orientation", c.b(0L, this.h));
        }
        if (e("LightSource") == null) {
            this.f[1].put("LightSource", c.b(0L, this.h));
        }
    }

    public String e(String str) {
        Objects.requireNonNull(str, "tag shouldn't be null");
        c g = g(str);
        if (g != null) {
            if (!b0.contains(str)) {
                return g.j(this.h);
            }
            if (str.equals("GPSTimeStamp")) {
                int i = g.a;
                if (i != 5 && i != 10) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("GPS Timestamp format is not rational. format=");
                    sb.append(g.a);
                    return null;
                }
                e[] eVarArr = (e[]) g.k(this.h);
                if (eVarArr != null && eVarArr.length == 3) {
                    return String.format("%02d:%02d:%02d", Integer.valueOf((int) (((float) eVarArr[0].a) / ((float) eVarArr[0].b))), Integer.valueOf((int) (((float) eVarArr[1].a) / ((float) eVarArr[1].b))), Integer.valueOf((int) (((float) eVarArr[2].a) / ((float) eVarArr[2].b))));
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Invalid GPS Timestamp array. array=");
                sb2.append(Arrays.toString(eVarArr));
                return null;
            }
            try {
                return Double.toString(g.h(this.h));
            } catch (NumberFormatException unused) {
            }
        }
        return null;
    }

    public int f(String str, int i) {
        Objects.requireNonNull(str, "tag shouldn't be null");
        c g = g(str);
        if (g == null) {
            return i;
        }
        try {
            return g.i(this.h);
        } catch (NumberFormatException unused) {
            return i;
        }
    }

    public final c g(String str) {
        Objects.requireNonNull(str, "tag shouldn't be null");
        if ("ISOSpeedRatings".equals(str)) {
            str = "PhotographicSensitivity";
        }
        for (int i = 0; i < X.length; i++) {
            c cVar = this.f[i].get(str);
            if (cVar != null) {
                return cVar;
            }
        }
        return null;
    }

    public final void h(b bVar) throws IOException {
        String str;
        String str2;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                mediaMetadataRetriever.setDataSource(new a(this, bVar));
            } else {
                FileDescriptor fileDescriptor = this.b;
                if (fileDescriptor != null) {
                    mediaMetadataRetriever.setDataSource(fileDescriptor);
                } else {
                    String str3 = this.a;
                    if (str3 == null) {
                        return;
                    }
                    mediaMetadataRetriever.setDataSource(str3);
                }
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(33);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(34);
            String extractMetadata3 = mediaMetadataRetriever.extractMetadata(26);
            String extractMetadata4 = mediaMetadataRetriever.extractMetadata(17);
            String str4 = null;
            if ("yes".equals(extractMetadata3)) {
                str4 = mediaMetadataRetriever.extractMetadata(29);
                str = mediaMetadataRetriever.extractMetadata(30);
                str2 = mediaMetadataRetriever.extractMetadata(31);
            } else if ("yes".equals(extractMetadata4)) {
                str4 = mediaMetadataRetriever.extractMetadata(18);
                str = mediaMetadataRetriever.extractMetadata(19);
                str2 = mediaMetadataRetriever.extractMetadata(24);
            } else {
                str = null;
                str2 = null;
            }
            if (str4 != null) {
                this.f[0].put("ImageWidth", c.f(Integer.parseInt(str4), this.h));
            }
            if (str != null) {
                this.f[0].put("ImageLength", c.f(Integer.parseInt(str), this.h));
            }
            if (str2 != null) {
                int i = 1;
                int parseInt = Integer.parseInt(str2);
                if (parseInt == 90) {
                    i = 6;
                } else if (parseInt == 180) {
                    i = 3;
                } else if (parseInt == 270) {
                    i = 8;
                }
                this.f[0].put("Orientation", c.f(i, this.h));
            }
            if (extractMetadata != null && extractMetadata2 != null) {
                int parseInt2 = Integer.parseInt(extractMetadata);
                int parseInt3 = Integer.parseInt(extractMetadata2);
                if (parseInt3 > 6) {
                    bVar.d(parseInt2);
                    byte[] bArr = new byte[6];
                    if (bVar.read(bArr) == 6) {
                        int i2 = parseInt2 + 6;
                        int i3 = parseInt3 - 6;
                        if (Arrays.equals(bArr, e0)) {
                            byte[] bArr2 = new byte[i3];
                            if (bVar.read(bArr2) == i3) {
                                this.m = i2;
                                J(bArr2, 0);
                            } else {
                                throw new IOException("Can't read exif");
                            }
                        } else {
                            throw new IOException("Invalid identifier");
                        }
                    } else {
                        throw new IOException("Can't read identifier");
                    }
                } else {
                    throw new IOException("Invalid exif length");
                }
            }
            if (r) {
                StringBuilder sb = new StringBuilder();
                sb.append("Heif meta: ");
                sb.append(str4);
                sb.append("x");
                sb.append(str);
                sb.append(", rotation ");
                sb.append(str2);
            }
        } finally {
            mediaMetadataRetriever.release();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:66:0x0178, code lost:
        r19.e(r18.h);
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x017d, code lost:
        return;
     */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00a9 A[FALL_THROUGH] */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0159  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x016c A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void i(defpackage.az0.b r19, int r20, int r21) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 508
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.az0.i(az0$b, int, int):void");
    }

    public final int j(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(5000);
        byte[] bArr = new byte[5000];
        bufferedInputStream.read(bArr);
        bufferedInputStream.reset();
        if (w(bArr)) {
            return 4;
        }
        if (z(bArr)) {
            return 9;
        }
        if (v(bArr)) {
            return 12;
        }
        if (x(bArr)) {
            return 7;
        }
        if (A(bArr)) {
            return 10;
        }
        if (y(bArr)) {
            return 13;
        }
        return E(bArr) ? 14 : 0;
    }

    public final void k(b bVar) throws IOException {
        n(bVar);
        c cVar = this.f[1].get("MakerNote");
        if (cVar != null) {
            b bVar2 = new b(cVar.c);
            bVar2.e(this.h);
            byte[] bArr = A;
            byte[] bArr2 = new byte[bArr.length];
            bVar2.readFully(bArr2);
            bVar2.d(0L);
            byte[] bArr3 = B;
            byte[] bArr4 = new byte[bArr3.length];
            bVar2.readFully(bArr4);
            if (Arrays.equals(bArr2, bArr)) {
                bVar2.d(8L);
            } else if (Arrays.equals(bArr4, bArr3)) {
                bVar2.d(12L);
            }
            K(bVar2, 6);
            ExifInterface.ExifAttribute exifAttribute = (c) this.f[7].get("PreviewImageStart");
            ExifInterface.ExifAttribute exifAttribute2 = (c) this.f[7].get("PreviewImageLength");
            if (exifAttribute != null && exifAttribute2 != null) {
                this.f[5].put("JPEGInterchangeFormat", exifAttribute);
                this.f[5].put("JPEGInterchangeFormatLength", exifAttribute2);
            }
            c cVar2 = this.f[8].get("AspectFrame");
            if (cVar2 != null) {
                int[] iArr = (int[]) cVar2.k(this.h);
                if (iArr != null && iArr.length == 4) {
                    if (iArr[2] <= iArr[0] || iArr[3] <= iArr[1]) {
                        return;
                    }
                    int i = (iArr[2] - iArr[0]) + 1;
                    int i2 = (iArr[3] - iArr[1]) + 1;
                    if (i < i2) {
                        int i3 = i + i2;
                        i2 = i3 - i2;
                        i = i3 - i2;
                    }
                    ExifInterface.ExifAttribute f = c.f(i, this.h);
                    ExifInterface.ExifAttribute f2 = c.f(i2, this.h);
                    this.f[0].put("ImageWidth", f);
                    this.f[0].put("ImageLength", f2);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Invalid aspect frame values. frame=");
                sb.append(Arrays.toString(iArr));
            }
        }
    }

    public final void l(b bVar) throws IOException {
        if (r) {
            StringBuilder sb = new StringBuilder();
            sb.append("getPngAttributes starting with: ");
            sb.append(bVar);
        }
        bVar.e(ByteOrder.BIG_ENDIAN);
        byte[] bArr = C;
        bVar.skipBytes(bArr.length);
        int length = bArr.length + 0;
        while (true) {
            try {
                int readInt = bVar.readInt();
                int i = length + 4;
                byte[] bArr2 = new byte[4];
                if (bVar.read(bArr2) == 4) {
                    int i2 = i + 4;
                    if (i2 == 16 && !Arrays.equals(bArr2, E)) {
                        throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                    }
                    if (Arrays.equals(bArr2, F)) {
                        return;
                    }
                    if (Arrays.equals(bArr2, D)) {
                        byte[] bArr3 = new byte[readInt];
                        if (bVar.read(bArr3) == readInt) {
                            int readInt2 = bVar.readInt();
                            CRC32 crc32 = new CRC32();
                            crc32.update(bArr2);
                            crc32.update(bArr3);
                            if (((int) crc32.getValue()) == readInt2) {
                                this.m = i2;
                                J(bArr3, 0);
                                Q();
                                return;
                            }
                            throw new IOException("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: " + readInt2 + ", calculated CRC value: " + crc32.getValue());
                        }
                        throw new IOException("Failed to read given length for given PNG chunk type: " + b(bArr2));
                    }
                    int i3 = readInt + 4;
                    bVar.skipBytes(i3);
                    length = i2 + i3;
                } else {
                    throw new IOException("Encountered invalid length while parsing PNG chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt PNG file.");
            }
        }
    }

    public final void m(b bVar) throws IOException {
        bVar.skipBytes(84);
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[4];
        bVar.read(bArr);
        bVar.skipBytes(4);
        bVar.read(bArr2);
        int i = ByteBuffer.wrap(bArr).getInt();
        int i2 = ByteBuffer.wrap(bArr2).getInt();
        i(bVar, i, 5);
        bVar.d(i2);
        bVar.e(ByteOrder.BIG_ENDIAN);
        int readInt = bVar.readInt();
        if (r) {
            StringBuilder sb = new StringBuilder();
            sb.append("numberOfDirectoryEntry: ");
            sb.append(readInt);
        }
        for (int i3 = 0; i3 < readInt; i3++) {
            int readUnsignedShort = bVar.readUnsignedShort();
            int readUnsignedShort2 = bVar.readUnsignedShort();
            if (readUnsignedShort == S.a) {
                short readShort = bVar.readShort();
                short readShort2 = bVar.readShort();
                ExifInterface.ExifAttribute f = c.f(readShort, this.h);
                ExifInterface.ExifAttribute f2 = c.f(readShort2, this.h);
                this.f[0].put("ImageLength", f);
                this.f[0].put("ImageWidth", f2);
                if (r) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Updated to length: ");
                    sb2.append((int) readShort);
                    sb2.append(", width: ");
                    sb2.append((int) readShort2);
                    return;
                }
                return;
            }
            bVar.skipBytes(readUnsignedShort2);
        }
    }

    public final void n(b bVar) throws IOException {
        c cVar;
        G(bVar, bVar.available());
        K(bVar, 0);
        P(bVar, 0);
        P(bVar, 5);
        P(bVar, 4);
        Q();
        if (this.d != 8 || (cVar = this.f[1].get("MakerNote")) == null) {
            return;
        }
        b bVar2 = new b(cVar.c);
        bVar2.e(this.h);
        bVar2.d(6L);
        K(bVar2, 9);
        ExifInterface.ExifAttribute exifAttribute = (c) this.f[9].get("ColorSpace");
        if (exifAttribute != null) {
            this.f[1].put("ColorSpace", exifAttribute);
        }
    }

    public final void o(b bVar) throws IOException {
        n(bVar);
        if (this.f[0].get("JpgFromRaw") != null) {
            i(bVar, this.q, 5);
        }
        ExifInterface.ExifAttribute exifAttribute = (c) this.f[0].get("ISO");
        c cVar = this.f[1].get("PhotographicSensitivity");
        if (exifAttribute == null || cVar != null) {
            return;
        }
        this.f[1].put("PhotographicSensitivity", exifAttribute);
    }

    public final void p(b bVar) throws IOException {
        byte[] bArr = e0;
        bVar.skipBytes(bArr.length);
        byte[] bArr2 = new byte[bVar.available()];
        bVar.readFully(bArr2);
        this.m = bArr.length;
        J(bArr2, 0);
    }

    public final void q(b bVar) throws IOException {
        if (r) {
            StringBuilder sb = new StringBuilder();
            sb.append("getWebpAttributes starting with: ");
            sb.append(bVar);
        }
        bVar.e(ByteOrder.LITTLE_ENDIAN);
        bVar.skipBytes(G.length);
        int readInt = bVar.readInt() + 8;
        int skipBytes = bVar.skipBytes(H.length) + 8;
        while (true) {
            try {
                byte[] bArr = new byte[4];
                if (bVar.read(bArr) == 4) {
                    int readInt2 = bVar.readInt();
                    int i = skipBytes + 4 + 4;
                    if (Arrays.equals(I, bArr)) {
                        byte[] bArr2 = new byte[readInt2];
                        if (bVar.read(bArr2) == readInt2) {
                            this.m = i;
                            J(bArr2, 0);
                            this.m = i;
                            return;
                        }
                        throw new IOException("Failed to read given length for given PNG chunk type: " + b(bArr));
                    }
                    if (readInt2 % 2 == 1) {
                        readInt2++;
                    }
                    int i2 = i + readInt2;
                    if (i2 == readInt) {
                        return;
                    }
                    if (i2 <= readInt) {
                        int skipBytes2 = bVar.skipBytes(readInt2);
                        if (skipBytes2 != readInt2) {
                            throw new IOException("Encountered WebP file with invalid chunk size");
                        }
                        skipBytes = i + skipBytes2;
                    } else {
                        throw new IOException("Encountered WebP file with invalid chunk size");
                    }
                } else {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
            } catch (EOFException unused) {
                throw new IOException("Encountered corrupt WebP file.");
            }
        }
    }

    public final void r(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("JPEGInterchangeFormat");
        c cVar2 = (c) hashMap.get("JPEGInterchangeFormatLength");
        if (cVar == null || cVar2 == null) {
            return;
        }
        int i = cVar.i(this.h);
        int i2 = cVar2.i(this.h);
        if (this.d == 7) {
            i += this.n;
        }
        int min = Math.min(i2, bVar.a() - i);
        if (i > 0 && min > 0) {
            int i3 = this.m + i;
            this.j = i3;
            this.k = min;
            if (this.a == null && this.c == null && this.b == null) {
                bVar.d(i3);
                bVar.readFully(new byte[min]);
            }
        }
        if (r) {
            StringBuilder sb = new StringBuilder();
            sb.append("Setting thumbnail attributes with offset: ");
            sb.append(i);
            sb.append(", length: ");
            sb.append(min);
        }
    }

    public final void s(b bVar, HashMap hashMap) throws IOException {
        c cVar = (c) hashMap.get("StripOffsets");
        c cVar2 = (c) hashMap.get("StripByteCounts");
        if (cVar == null || cVar2 == null) {
            return;
        }
        long[] d2 = d(cVar.k(this.h));
        long[] d3 = d(cVar2.k(this.h));
        if (d2 == null || d2.length == 0 || d3 == null || d3.length == 0 || d2.length != d3.length) {
            return;
        }
        long j = 0;
        for (long j2 : d3) {
            j += j2;
        }
        int i = (int) j;
        byte[] bArr = new byte[i];
        this.i = true;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < d2.length; i4++) {
            int i5 = (int) d2[i4];
            int i6 = (int) d3[i4];
            if (i4 < d2.length - 1 && i5 + i6 != d2[i4 + 1]) {
                this.i = false;
            }
            int i7 = i5 - i2;
            bVar.d(i7);
            int i8 = i2 + i7;
            byte[] bArr2 = new byte[i6];
            bVar.read(bArr2);
            i2 = i8 + i6;
            System.arraycopy(bArr2, 0, bArr, i3, i6);
            i3 += i6;
        }
        if (this.i) {
            this.j = ((int) d2[0]) + this.m;
            this.k = i;
        }
    }

    public final void t(String str) throws IOException {
        FileInputStream fileInputStream;
        Objects.requireNonNull(str, "filename cannot be null");
        FileInputStream fileInputStream2 = null;
        this.c = null;
        this.a = str;
        try {
            fileInputStream = new FileInputStream(str);
        } catch (Throwable th) {
            th = th;
        }
        try {
            if (B(fileInputStream.getFD())) {
                this.b = fileInputStream.getFD();
            } else {
                this.b = null;
            }
            F(fileInputStream);
            c(fileInputStream);
        } catch (Throwable th2) {
            th = th2;
            fileInputStream2 = fileInputStream;
            c(fileInputStream2);
            throw th;
        }
    }

    public final boolean v(byte[] bArr) throws IOException {
        b bVar;
        long readInt;
        byte[] bArr2;
        b bVar2 = null;
        try {
            try {
                bVar = new b(bArr);
                try {
                    readInt = bVar.readInt();
                    bArr2 = new byte[4];
                    bVar.read(bArr2);
                } catch (Exception unused) {
                    bVar2 = bVar;
                    boolean z2 = r;
                    if (bVar2 != null) {
                        bVar2.close();
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    bVar2 = bVar;
                    if (bVar2 != null) {
                        bVar2.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Exception unused2) {
        }
        if (!Arrays.equals(bArr2, x)) {
            bVar.close();
            return false;
        }
        long j = 16;
        if (readInt == 1) {
            readInt = bVar.readLong();
            if (readInt < 16) {
                bVar.close();
                return false;
            }
        } else {
            j = 8;
        }
        if (readInt > bArr.length) {
            readInt = bArr.length;
        }
        long j2 = readInt - j;
        if (j2 < 8) {
            bVar.close();
            return false;
        }
        byte[] bArr3 = new byte[4];
        boolean z3 = false;
        boolean z4 = false;
        for (long j3 = 0; j3 < j2 / 4; j3++) {
            if (bVar.read(bArr3) != 4) {
                bVar.close();
                return false;
            }
            if (j3 != 1) {
                if (Arrays.equals(bArr3, y)) {
                    z3 = true;
                } else if (Arrays.equals(bArr3, z)) {
                    z4 = true;
                }
                if (z3 && z4) {
                    bVar.close();
                    return true;
                }
            }
        }
        bVar.close();
        return false;
    }

    public final boolean x(byte[] bArr) throws IOException {
        b bVar;
        boolean z2 = false;
        b bVar2 = null;
        try {
            bVar = new b(bArr);
        } catch (Exception unused) {
        } catch (Throwable th) {
            th = th;
        }
        try {
            ByteOrder I2 = I(bVar);
            this.h = I2;
            bVar.e(I2);
            short readShort = bVar.readShort();
            z2 = (readShort == 20306 || readShort == 21330) ? true : true;
            bVar.close();
            return z2;
        } catch (Exception unused2) {
            bVar2 = bVar;
            if (bVar2 != null) {
                bVar2.close();
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            bVar2 = bVar;
            if (bVar2 != null) {
                bVar2.close();
            }
            throw th;
        }
    }

    public final boolean y(byte[] bArr) throws IOException {
        int i = 0;
        while (true) {
            byte[] bArr2 = C;
            if (i >= bArr2.length) {
                return true;
            }
            if (bArr[i] != bArr2[i]) {
                return false;
            }
            i++;
        }
    }

    public final boolean z(byte[] bArr) throws IOException {
        byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i = 0; i < bytes.length; i++) {
            if (bArr[i] != bytes[i]) {
                return false;
            }
        }
        return true;
    }

    /* compiled from: ExifInterface.java */
    /* renamed from: az0$d */
    /* loaded from: classes.dex */
    public static class d {
        public final int a;
        public final String b;
        public final int c;
        public final int d;

        public d(String str, int i, int i2) {
            this.b = str;
            this.a = i;
            this.c = i2;
            this.d = -1;
        }

        public boolean a(int i) {
            int i2;
            int i3 = this.c;
            if (i3 == 7 || i == 7 || i3 == i || (i2 = this.d) == i) {
                return true;
            }
            if ((i3 == 4 || i2 == 4) && i == 3) {
                return true;
            }
            if ((i3 == 9 || i2 == 9) && i == 8) {
                return true;
            }
            return (i3 == 12 || i2 == 12) && i == 11;
        }

        public d(String str, int i, int i2, int i3) {
            this.b = str;
            this.a = i;
            this.c = i2;
            this.d = i3;
        }
    }

    public az0(InputStream inputStream) throws IOException {
        this(inputStream, false);
    }

    public az0(InputStream inputStream, boolean z2) throws IOException {
        d[][] dVarArr = X;
        this.f = new HashMap[dVarArr.length];
        this.g = new HashSet(dVarArr.length);
        this.h = ByteOrder.BIG_ENDIAN;
        Objects.requireNonNull(inputStream, "inputStream cannot be null");
        this.a = null;
        if (z2) {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 5000);
            if (!u(bufferedInputStream)) {
                return;
            }
            this.e = true;
            this.c = null;
            this.b = null;
            inputStream = bufferedInputStream;
        } else if (inputStream instanceof AssetManager.AssetInputStream) {
            this.c = (AssetManager.AssetInputStream) inputStream;
            this.b = null;
        } else {
            if (inputStream instanceof FileInputStream) {
                FileInputStream fileInputStream = (FileInputStream) inputStream;
                if (B(fileInputStream.getFD())) {
                    this.c = null;
                    this.b = fileInputStream.getFD();
                }
            }
            this.c = null;
            this.b = null;
        }
        F(inputStream);
    }
}
