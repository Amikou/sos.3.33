package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktSecurityQuestionsBinding.java */
/* renamed from: w6  reason: default package */
/* loaded from: classes2.dex */
public final class w6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialCheckBox c;
    public final MaterialTextView d;
    public final LinearLayout e;
    public final View f;
    public final View g;
    public final EditText h;
    public final EditText i;
    public final EditText j;
    public final EditText k;
    public final LinearLayout l;
    public final LinearLayout m;
    public final sp3 n;
    public final TextView o;
    public final TextView p;
    public final AppCompatTextView q;
    public final TextView r;
    public final TextView s;
    public final TextView t;
    public final AppCompatTextView u;

    public w6(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialCheckBox materialCheckBox, MaterialTextView materialTextView, LinearLayout linearLayout, ConstraintLayout constraintLayout2, View view, View view2, EditText editText, EditText editText2, EditText editText3, EditText editText4, LinearLayout linearLayout2, LinearLayout linearLayout3, sp3 sp3Var, TextView textView, TextView textView2, AppCompatTextView appCompatTextView, TextView textView3, TextView textView4, TextView textView5, TextView textView6, TextView textView7, TextView textView8, AppCompatTextView appCompatTextView2, TextView textView9) {
        this.a = constraintLayout;
        this.b = materialButton2;
        this.c = materialCheckBox;
        this.d = materialTextView;
        this.e = linearLayout;
        this.f = view;
        this.g = view2;
        this.h = editText;
        this.i = editText2;
        this.j = editText3;
        this.k = editText4;
        this.l = linearLayout2;
        this.m = linearLayout3;
        this.n = sp3Var;
        this.o = textView;
        this.p = textView2;
        this.q = appCompatTextView;
        this.r = textView4;
        this.s = textView7;
        this.t = textView8;
        this.u = appCompatTextView2;
    }

    public static w6 a(View view) {
        int i = R.id.btnMnemonic;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnMnemonic);
        if (materialButton != null) {
            i = R.id.btnRegister;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnRegister);
            if (materialButton2 != null) {
                i = R.id.chkTermsAgree;
                MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chkTermsAgree);
                if (materialCheckBox != null) {
                    i = R.id.chkTermsAgreeTC;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.chkTermsAgreeTC);
                    if (materialTextView != null) {
                        i = R.id.content_layout;
                        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.content_layout);
                        if (linearLayout != null) {
                            ConstraintLayout constraintLayout = (ConstraintLayout) view;
                            i = R.id.divider_second_answer_et;
                            View a = ai4.a(view, R.id.divider_second_answer_et);
                            if (a != null) {
                                i = R.id.divider_second_question_spinner;
                                View a2 = ai4.a(view, R.id.divider_second_question_spinner);
                                if (a2 != null) {
                                    i = R.id.et_first_answer;
                                    EditText editText = (EditText) ai4.a(view, R.id.et_first_answer);
                                    if (editText != null) {
                                        i = R.id.et_first_your_question;
                                        EditText editText2 = (EditText) ai4.a(view, R.id.et_first_your_question);
                                        if (editText2 != null) {
                                            i = R.id.et_second_answer;
                                            EditText editText3 = (EditText) ai4.a(view, R.id.et_second_answer);
                                            if (editText3 != null) {
                                                i = R.id.et_second_your_question;
                                                EditText editText4 = (EditText) ai4.a(view, R.id.et_second_your_question);
                                                if (editText4 != null) {
                                                    i = R.id.firstQuestionLayout;
                                                    LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.firstQuestionLayout);
                                                    if (linearLayout2 != null) {
                                                        i = R.id.secondQuestionLayout;
                                                        LinearLayout linearLayout3 = (LinearLayout) ai4.a(view, R.id.secondQuestionLayout);
                                                        if (linearLayout3 != null) {
                                                            i = R.id.toolbar;
                                                            View a3 = ai4.a(view, R.id.toolbar);
                                                            if (a3 != null) {
                                                                sp3 a4 = sp3.a(a3);
                                                                i = R.id.tv_not_first_answer;
                                                                TextView textView = (TextView) ai4.a(view, R.id.tv_not_first_answer);
                                                                if (textView != null) {
                                                                    i = R.id.tv_not_second_answer;
                                                                    TextView textView2 = (TextView) ai4.a(view, R.id.tv_not_second_answer);
                                                                    if (textView2 != null) {
                                                                        i = R.id.txt_email;
                                                                        AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txt_email);
                                                                        if (appCompatTextView != null) {
                                                                            i = R.id.txt_email_header;
                                                                            TextView textView3 = (TextView) ai4.a(view, R.id.txt_email_header);
                                                                            if (textView3 != null) {
                                                                                i = R.id.txt_first_question_content;
                                                                                TextView textView4 = (TextView) ai4.a(view, R.id.txt_first_question_content);
                                                                                if (textView4 != null) {
                                                                                    i = R.id.txt_first_question_header;
                                                                                    TextView textView5 = (TextView) ai4.a(view, R.id.txt_first_question_header);
                                                                                    if (textView5 != null) {
                                                                                        i = R.id.txt_questions_header;
                                                                                        TextView textView6 = (TextView) ai4.a(view, R.id.txt_questions_header);
                                                                                        if (textView6 != null) {
                                                                                            i = R.id.txt_second_question_content;
                                                                                            TextView textView7 = (TextView) ai4.a(view, R.id.txt_second_question_content);
                                                                                            if (textView7 != null) {
                                                                                                i = R.id.txt_second_question_header;
                                                                                                TextView textView8 = (TextView) ai4.a(view, R.id.txt_second_question_header);
                                                                                                if (textView8 != null) {
                                                                                                    i = R.id.txt_username;
                                                                                                    AppCompatTextView appCompatTextView2 = (AppCompatTextView) ai4.a(view, R.id.txt_username);
                                                                                                    if (appCompatTextView2 != null) {
                                                                                                        i = R.id.txt_username_header;
                                                                                                        TextView textView9 = (TextView) ai4.a(view, R.id.txt_username_header);
                                                                                                        if (textView9 != null) {
                                                                                                            return new w6(constraintLayout, materialButton, materialButton2, materialCheckBox, materialTextView, linearLayout, constraintLayout, a, a2, editText, editText2, editText3, editText4, linearLayout2, linearLayout3, a4, textView, textView2, appCompatTextView, textView3, textView4, textView5, textView6, textView7, textView8, appCompatTextView2, textView9);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static w6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static w6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_security_questions, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
