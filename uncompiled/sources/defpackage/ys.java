package defpackage;

import okio.ByteString;
import okio.b;

/* compiled from: ByteString.kt */
/* renamed from: ys  reason: default package */
/* loaded from: classes2.dex */
public final class ys {
    public static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* JADX WARN: Code restructure failed: missing block: B:43:0x006b, code lost:
        return -1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final int c(byte[] r19, int r20) {
        /*
            Method dump skipped, instructions count: 495
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ys.c(byte[], int):int");
    }

    public static final void d(ByteString byteString, b bVar, int i, int i2) {
        fs1.f(byteString, "$this$commonWrite");
        fs1.f(bVar, "buffer");
        bVar.M0(byteString.getData$okio(), i, i2);
    }

    public static final int e(char c) {
        if ('0' <= c && '9' >= c) {
            return c - '0';
        }
        char c2 = 'a';
        if ('a' > c || 'f' < c) {
            c2 = 'A';
            if ('A' > c || 'F' < c) {
                throw new IllegalArgumentException("Unexpected hex digit: " + c);
            }
        }
        return (c - c2) + 10;
    }

    public static final char[] f() {
        return a;
    }
}
