package defpackage;

import android.view.View;
import android.widget.RelativeLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.OtpEditText;

/* compiled from: FragmentVerficationg2faBinding.java */
/* renamed from: nb1  reason: default package */
/* loaded from: classes2.dex */
public final class nb1 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final OtpEditText c;
    public final MaterialTextView d;

    public nb1(RelativeLayout relativeLayout, MaterialButton materialButton, MaterialButton materialButton2, OtpEditText otpEditText, ShapeableImageView shapeableImageView, MaterialTextView materialTextView, MaterialTextView materialTextView2) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = otpEditText;
        this.d = materialTextView2;
    }

    public static nb1 a(View view) {
        int i = R.id.btnPasteCode;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnPasteCode);
        if (materialButton != null) {
            i = R.id.dialog_cross;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
            if (materialButton2 != null) {
                i = R.id.edtOTP;
                OtpEditText otpEditText = (OtpEditText) ai4.a(view, R.id.edtOTP);
                if (otpEditText != null) {
                    i = R.id.imgAuthIcon;
                    ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgAuthIcon);
                    if (shapeableImageView != null) {
                        i = R.id.txtAuthEnableTitle;
                        MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.txtAuthEnableTitle);
                        if (materialTextView != null) {
                            i = R.id.txtRemainSecond;
                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtRemainSecond);
                            if (materialTextView2 != null) {
                                return new nb1((RelativeLayout) view, materialButton, materialButton2, otpEditText, shapeableImageView, materialTextView, materialTextView2);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
