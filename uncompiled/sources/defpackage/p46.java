package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: p46  reason: default package */
/* loaded from: classes.dex */
public final class p46 extends c55 {
    public final boolean g0;
    public final boolean h0;
    public final /* synthetic */ u46 i0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p46(u46 u46Var, boolean z, boolean z2) {
        super("log");
        this.i0 = u46Var;
        this.g0 = z;
        this.h0 = z2;
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x006f  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0083  */
    @Override // defpackage.c55
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.z55 a(defpackage.wk5 r12, java.util.List<defpackage.z55> r13) {
        /*
            r11 = this;
            java.lang.String r0 = "log"
            r1 = 1
            defpackage.vm5.b(r0, r1, r13)
            int r0 = r13.size()
            r2 = 0
            if (r0 != r1) goto L30
            u46 r0 = r11.i0
            j46 r3 = defpackage.u46.f(r0)
            r4 = 3
            java.lang.Object r13 = r13.get(r2)
            z55 r13 = (defpackage.z55) r13
            z55 r12 = r12.a(r13)
            java.lang.String r5 = r12.zzc()
            java.util.List r6 = java.util.Collections.emptyList()
            boolean r7 = r11.g0
            boolean r8 = r11.h0
            r3.a(r4, r5, r6, r7, r8)
            z55 r12 = defpackage.z55.X
            return r12
        L30:
            java.lang.Object r0 = r13.get(r2)
            z55 r0 = (defpackage.z55) r0
            z55 r0 = r12.a(r0)
            java.lang.Double r0 = r0.b()
            double r2 = r0.doubleValue()
            int r0 = defpackage.vm5.g(r2)
            r2 = 3
            r3 = 5
            r4 = 2
            if (r0 == r4) goto L59
            if (r0 == r2) goto L57
            if (r0 == r3) goto L55
            r5 = 6
            if (r0 == r5) goto L53
            goto L5a
        L53:
            r6 = r4
            goto L5b
        L55:
            r6 = r3
            goto L5b
        L57:
            r6 = r1
            goto L5b
        L59:
            r2 = 4
        L5a:
            r6 = r2
        L5b:
            java.lang.Object r0 = r13.get(r1)
            z55 r0 = (defpackage.z55) r0
            z55 r0 = r12.a(r0)
            java.lang.String r7 = r0.zzc()
            int r0 = r13.size()
            if (r0 != r4) goto L83
            u46 r12 = r11.i0
            j46 r5 = defpackage.u46.f(r12)
            java.util.List r8 = java.util.Collections.emptyList()
            boolean r9 = r11.g0
            boolean r10 = r11.h0
            r5.a(r6, r7, r8, r9, r10)
            z55 r12 = defpackage.z55.X
            return r12
        L83:
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
        L88:
            int r0 = r13.size()
            int r0 = java.lang.Math.min(r0, r3)
            if (r4 >= r0) goto La6
            java.lang.Object r0 = r13.get(r4)
            z55 r0 = (defpackage.z55) r0
            z55 r0 = r12.a(r0)
            java.lang.String r0 = r0.zzc()
            r8.add(r0)
            int r4 = r4 + 1
            goto L88
        La6:
            u46 r12 = r11.i0
            j46 r5 = defpackage.u46.f(r12)
            boolean r9 = r11.g0
            boolean r10 = r11.h0
            r5.a(r6, r7, r8, r9, r10)
            z55 r12 = defpackage.z55.X
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.p46.a(wk5, java.util.List):z55");
    }
}
