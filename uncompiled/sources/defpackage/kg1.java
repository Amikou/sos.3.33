package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.bumptech.glide.a;
import java.io.File;
import net.safemoon.androidwallet.utils.svg.b;

/* compiled from: GlideRequests.java */
/* renamed from: kg1  reason: default package */
/* loaded from: classes2.dex */
public class kg1 extends k73 {
    public kg1(a aVar, kz1 kz1Var, m73 m73Var, Context context) {
        super(aVar, kz1Var, m73Var, context);
    }

    @Override // defpackage.k73
    public void E(n73 n73Var) {
        if (n73Var instanceof net.safemoon.androidwallet.utils.svg.a) {
            super.E(n73Var);
        } else {
            super.E(new net.safemoon.androidwallet.utils.svg.a().a(n73Var));
        }
    }

    @Override // defpackage.k73
    /* renamed from: I */
    public <ResourceType> b<ResourceType> d(Class<ResourceType> cls) {
        return new b<>(this.a, this, cls, this.f0);
    }

    @Override // defpackage.k73
    /* renamed from: J */
    public b<Bitmap> i() {
        return (b) super.i();
    }

    @Override // defpackage.k73
    /* renamed from: K */
    public b<Drawable> n() {
        return (b) super.n();
    }

    @Override // defpackage.k73
    /* renamed from: L */
    public b<File> p() {
        return (b) super.p();
    }

    @Override // defpackage.k73
    /* renamed from: M */
    public b<Drawable> t(Bitmap bitmap) {
        return (b) super.t(bitmap);
    }

    @Override // defpackage.k73
    /* renamed from: N */
    public b<Drawable> u(Uri uri) {
        return (b) super.u(uri);
    }

    @Override // defpackage.k73
    /* renamed from: O */
    public b<Drawable> v(File file) {
        return (b) super.v(file);
    }

    @Override // defpackage.k73
    /* renamed from: P */
    public b<Drawable> w(Integer num) {
        return (b) super.w(num);
    }

    @Override // defpackage.k73
    /* renamed from: Q */
    public b<Drawable> x(Object obj) {
        return (b) super.x(obj);
    }

    @Override // defpackage.k73
    /* renamed from: R */
    public b<Drawable> y(String str) {
        return (b) super.y(str);
    }

    @Override // defpackage.k73
    /* renamed from: S */
    public b<Drawable> z(byte[] bArr) {
        return (b) super.z(bArr);
    }
}
