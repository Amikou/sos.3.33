package defpackage;

import com.google.android.gms.internal.measurement.a1;
import com.google.android.gms.internal.measurement.h1;
import com.google.android.gms.internal.measurement.i1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: cl5  reason: default package */
/* loaded from: classes.dex */
public final class cl5 extends w1<h1, cl5> implements xx5 {
    public cl5() {
        super(h1.J());
    }

    public final cl5 A() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.N((h1) this.f0);
        return this;
    }

    public final cl5 B(Iterable<? extends a1> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.O((h1) this.f0, iterable);
        return this;
    }

    public final cl5 C(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.P((h1) this.f0, i);
        return this;
    }

    public final cl5 D(Iterable<? extends i1> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.Q((h1) this.f0, iterable);
        return this;
    }

    public final cl5 E(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.R((h1) this.f0, i);
        return this;
    }

    public final cl5 v(Iterable<? extends Long> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.K((h1) this.f0, iterable);
        return this;
    }

    public final cl5 x() {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.L((h1) this.f0);
        return this;
    }

    public final cl5 y(Iterable<? extends Long> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        h1.M((h1) this.f0, iterable);
        return this;
    }

    public /* synthetic */ cl5(wi5 wi5Var) {
        super(h1.J());
    }
}
