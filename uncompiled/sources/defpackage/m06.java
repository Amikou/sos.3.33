package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: m06  reason: default package */
/* loaded from: classes.dex */
public final class m06 implements l06 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.service.click_identifier_control", false);

    @Override // defpackage.l06
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
