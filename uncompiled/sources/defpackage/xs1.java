package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import net.safemoon.androidwallet.R;

/* compiled from: ItemDropDownContactAddressBinding.java */
/* renamed from: xs1  reason: default package */
/* loaded from: classes2.dex */
public final class xs1 {
    public final LinearLayout a;
    public final LinearLayout b;
    public final ImageView c;
    public final TextView d;
    public final TextView e;
    public final View f;

    public xs1(LinearLayout linearLayout, LinearLayout linearLayout2, CardView cardView, ImageView imageView, TextView textView, TextView textView2, View view) {
        this.a = linearLayout;
        this.b = linearLayout2;
        this.c = imageView;
        this.d = textView;
        this.e = textView2;
        this.f = view;
    }

    public static xs1 a(View view) {
        LinearLayout linearLayout = (LinearLayout) view;
        int i = R.id.cvContactIconContainer;
        CardView cardView = (CardView) ai4.a(view, R.id.cvContactIconContainer);
        if (cardView != null) {
            i = R.id.ivContactIcon;
            ImageView imageView = (ImageView) ai4.a(view, R.id.ivContactIcon);
            if (imageView != null) {
                i = R.id.tvContactAddress;
                TextView textView = (TextView) ai4.a(view, R.id.tvContactAddress);
                if (textView != null) {
                    i = R.id.tvContactName;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvContactName);
                    if (textView2 != null) {
                        i = R.id.vDivider;
                        View a = ai4.a(view, R.id.vDivider);
                        if (a != null) {
                            return new xs1(linearLayout, linearLayout, cardView, imageView, textView, textView2, a);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public LinearLayout b() {
        return this.a;
    }
}
