package defpackage;

import android.content.res.Resources;
import android.util.TypedValue;
import androidx.lifecycle.LiveData;

/* compiled from: ComputeData.kt */
/* renamed from: t40  reason: default package */
/* loaded from: classes2.dex */
public final class t40 {

    /* compiled from: ComputeData.kt */
    /* renamed from: t40$a */
    /* loaded from: classes2.dex */
    public static final class a implements tl2<T> {
        public boolean a;
        public T b;
        public final /* synthetic */ g72<T> c;

        public a(g72<T> g72Var) {
            this.c = g72Var;
        }

        @Override // defpackage.tl2
        public void onChanged(T t) {
            if (!this.a) {
                this.a = true;
                this.b = t;
                LiveData liveData = this.c;
                fs1.d(t);
                liveData.postValue(t);
            } else if ((t != 0 || this.b == 0) && fs1.b(t, this.b)) {
            } else {
                this.b = t;
                LiveData liveData2 = this.c;
                fs1.d(t);
                liveData2.postValue(t);
            }
        }
    }

    public static final float a(Number number) {
        fs1.f(number, "<this>");
        return TypedValue.applyDimension(1, number.floatValue(), Resources.getSystem().getDisplayMetrics());
    }

    public static final <T> LiveData<T> b(LiveData<T> liveData) {
        fs1.f(liveData, "<this>");
        g72 g72Var = new g72();
        g72Var.a(liveData, new a(g72Var));
        return g72Var;
    }
}
