package defpackage;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;

/* compiled from: NetworkConnectedController.java */
/* renamed from: ze2  reason: default package */
/* loaded from: classes.dex */
public class ze2 extends d60<cf2> {
    public ze2(Context context, q34 q34Var) {
        super(j84.c(context, q34Var).d());
    }

    @Override // defpackage.d60
    public boolean b(tq4 tq4Var) {
        return tq4Var.j.b() == NetworkType.CONNECTED;
    }

    @Override // defpackage.d60
    /* renamed from: i */
    public boolean c(cf2 cf2Var) {
        if (Build.VERSION.SDK_INT >= 26) {
            return (cf2Var.a() && cf2Var.d()) ? false : true;
        }
        return !cf2Var.a();
    }
}
