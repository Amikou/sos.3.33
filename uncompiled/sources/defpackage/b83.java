package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import androidx.appcompat.widget.h;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: ResourceManagerInternal.java */
/* renamed from: b83  reason: default package */
/* loaded from: classes.dex */
public final class b83 {
    public static b83 i;
    public WeakHashMap<Context, lr3<ColorStateList>> a;
    public vo3<String, e> b;
    public lr3<String> c;
    public final WeakHashMap<Context, i22<WeakReference<Drawable.ConstantState>>> d = new WeakHashMap<>(0);
    public TypedValue e;
    public boolean f;
    public f g;
    public static final PorterDuff.Mode h = PorterDuff.Mode.SRC_IN;
    public static final c j = new c(6);

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$a */
    /* loaded from: classes.dex */
    public static class a implements e {
        @Override // defpackage.b83.e
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return ae.m(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception unused) {
                return null;
            }
        }
    }

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$b */
    /* loaded from: classes.dex */
    public static class b implements e {
        @Override // defpackage.b83.e
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return be.b(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception unused) {
                return null;
            }
        }
    }

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$c */
    /* loaded from: classes.dex */
    public static class c extends t22<Integer, PorterDuffColorFilter> {
        public c(int i) {
            super(i);
        }

        public static int j(int i, PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        public PorterDuffColorFilter k(int i, PorterDuff.Mode mode) {
            return c(Integer.valueOf(j(i, mode)));
        }

        public PorterDuffColorFilter l(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return d(Integer.valueOf(j(i, mode)), porterDuffColorFilter);
        }
    }

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$d */
    /* loaded from: classes.dex */
    public static class d implements e {
        @Override // defpackage.b83.e
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            String classAttribute = attributeSet.getClassAttribute();
            if (classAttribute != null) {
                try {
                    Drawable drawable = (Drawable) d.class.getClassLoader().loadClass(classAttribute).asSubclass(Drawable.class).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                    if (Build.VERSION.SDK_INT >= 21) {
                        drawable.inflate(context.getResources(), xmlPullParser, attributeSet, theme);
                    } else {
                        drawable.inflate(context.getResources(), xmlPullParser, attributeSet);
                    }
                    return drawable;
                } catch (Exception unused) {
                }
            }
            return null;
        }
    }

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$e */
    /* loaded from: classes.dex */
    public interface e {
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$f */
    /* loaded from: classes.dex */
    public interface f {
        Drawable a(b83 b83Var, Context context, int i);

        boolean b(Context context, int i, Drawable drawable);

        PorterDuff.Mode c(int i);

        ColorStateList d(Context context, int i);

        boolean e(Context context, int i, Drawable drawable);
    }

    /* compiled from: ResourceManagerInternal.java */
    /* renamed from: b83$g */
    /* loaded from: classes.dex */
    public static class g implements e {
        @Override // defpackage.b83.e
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return eh4.c(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception unused) {
                return null;
            }
        }
    }

    public static long e(TypedValue typedValue) {
        return (typedValue.assetCookie << 32) | typedValue.data;
    }

    public static PorterDuffColorFilter g(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return l(colorStateList.getColorForState(iArr, 0), mode);
    }

    public static synchronized b83 h() {
        b83 b83Var;
        synchronized (b83.class) {
            if (i == null) {
                b83 b83Var2 = new b83();
                i = b83Var2;
                p(b83Var2);
            }
            b83Var = i;
        }
        return b83Var;
    }

    public static synchronized PorterDuffColorFilter l(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter k;
        synchronized (b83.class) {
            c cVar = j;
            k = cVar.k(i2, mode);
            if (k == null) {
                k = new PorterDuffColorFilter(i2, mode);
                cVar.l(i2, mode, k);
            }
        }
        return k;
    }

    public static void p(b83 b83Var) {
        if (Build.VERSION.SDK_INT < 24) {
            b83Var.a("vector", new g());
            b83Var.a("animated-vector", new b());
            b83Var.a("animated-selector", new a());
            b83Var.a("drawable", new d());
        }
    }

    public static boolean q(Drawable drawable) {
        return (drawable instanceof eh4) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }

    public static void w(Drawable drawable, k64 k64Var, int[] iArr) {
        if (!dr0.a(drawable) || drawable.mutate() == drawable) {
            boolean z = k64Var.d;
            if (!z && !k64Var.c) {
                drawable.clearColorFilter();
            } else {
                drawable.setColorFilter(g(z ? k64Var.a : null, k64Var.c ? k64Var.b : h, iArr));
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
            }
        }
    }

    public final void a(String str, e eVar) {
        if (this.b == null) {
            this.b = new vo3<>();
        }
        this.b.put(str, eVar);
    }

    public final synchronized boolean b(Context context, long j2, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState != null) {
            i22<WeakReference<Drawable.ConstantState>> i22Var = this.d.get(context);
            if (i22Var == null) {
                i22Var = new i22<>();
                this.d.put(context, i22Var);
            }
            i22Var.o(j2, new WeakReference<>(constantState));
            return true;
        }
        return false;
    }

    public final void c(Context context, int i2, ColorStateList colorStateList) {
        if (this.a == null) {
            this.a = new WeakHashMap<>();
        }
        lr3<ColorStateList> lr3Var = this.a.get(context);
        if (lr3Var == null) {
            lr3Var = new lr3<>();
            this.a.put(context, lr3Var);
        }
        lr3Var.a(i2, colorStateList);
    }

    public final void d(Context context) {
        if (this.f) {
            return;
        }
        this.f = true;
        Drawable j2 = j(context, mz2.abc_vector_test);
        if (j2 == null || !q(j2)) {
            this.f = false;
            throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
        }
    }

    public final Drawable f(Context context, int i2) {
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        context.getResources().getValue(i2, typedValue, true);
        long e2 = e(typedValue);
        Drawable i3 = i(context, e2);
        if (i3 != null) {
            return i3;
        }
        f fVar = this.g;
        Drawable a2 = fVar == null ? null : fVar.a(this, context, i2);
        if (a2 != null) {
            a2.setChangingConfigurations(typedValue.changingConfigurations);
            b(context, e2, a2);
        }
        return a2;
    }

    public final synchronized Drawable i(Context context, long j2) {
        i22<WeakReference<Drawable.ConstantState>> i22Var = this.d.get(context);
        if (i22Var == null) {
            return null;
        }
        WeakReference<Drawable.ConstantState> g2 = i22Var.g(j2);
        if (g2 != null) {
            Drawable.ConstantState constantState = g2.get();
            if (constantState != null) {
                return constantState.newDrawable(context.getResources());
            }
            i22Var.r(j2);
        }
        return null;
    }

    public synchronized Drawable j(Context context, int i2) {
        return k(context, i2, false);
    }

    public synchronized Drawable k(Context context, int i2, boolean z) {
        Drawable r;
        d(context);
        r = r(context, i2);
        if (r == null) {
            r = f(context, i2);
        }
        if (r == null) {
            r = m70.f(context, i2);
        }
        if (r != null) {
            r = v(context, i2, z, r);
        }
        if (r != null) {
            dr0.b(r);
        }
        return r;
    }

    public synchronized ColorStateList m(Context context, int i2) {
        ColorStateList n;
        n = n(context, i2);
        if (n == null) {
            f fVar = this.g;
            n = fVar == null ? null : fVar.d(context, i2);
            if (n != null) {
                c(context, i2, n);
            }
        }
        return n;
    }

    public final ColorStateList n(Context context, int i2) {
        lr3<ColorStateList> lr3Var;
        WeakHashMap<Context, lr3<ColorStateList>> weakHashMap = this.a;
        if (weakHashMap == null || (lr3Var = weakHashMap.get(context)) == null) {
            return null;
        }
        return lr3Var.f(i2);
    }

    public PorterDuff.Mode o(int i2) {
        f fVar = this.g;
        if (fVar == null) {
            return null;
        }
        return fVar.c(i2);
    }

    public final Drawable r(Context context, int i2) {
        int next;
        vo3<String, e> vo3Var = this.b;
        if (vo3Var == null || vo3Var.isEmpty()) {
            return null;
        }
        lr3<String> lr3Var = this.c;
        if (lr3Var != null) {
            String f2 = lr3Var.f(i2);
            if ("appcompat_skip_skip".equals(f2) || (f2 != null && this.b.get(f2) == null)) {
                return null;
            }
        } else {
            this.c = new lr3<>();
        }
        if (this.e == null) {
            this.e = new TypedValue();
        }
        TypedValue typedValue = this.e;
        Resources resources = context.getResources();
        resources.getValue(i2, typedValue, true);
        long e2 = e(typedValue);
        Drawable i3 = i(context, e2);
        if (i3 != null) {
            return i3;
        }
        CharSequence charSequence = typedValue.string;
        if (charSequence != null && charSequence.toString().endsWith(".xml")) {
            try {
                XmlResourceParser xml = resources.getXml(i2);
                AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
                while (true) {
                    next = xml.next();
                    if (next == 2 || next == 1) {
                        break;
                    }
                }
                if (next == 2) {
                    String name = xml.getName();
                    this.c.a(i2, name);
                    e eVar = this.b.get(name);
                    if (eVar != null) {
                        i3 = eVar.a(context, xml, asAttributeSet, context.getTheme());
                    }
                    if (i3 != null) {
                        i3.setChangingConfigurations(typedValue.changingConfigurations);
                        b(context, e2, i3);
                    }
                } else {
                    throw new XmlPullParserException("No start tag found");
                }
            } catch (Exception unused) {
            }
        }
        if (i3 == null) {
            this.c.a(i2, "appcompat_skip_skip");
        }
        return i3;
    }

    public synchronized void s(Context context) {
        i22<WeakReference<Drawable.ConstantState>> i22Var = this.d.get(context);
        if (i22Var != null) {
            i22Var.b();
        }
    }

    public synchronized Drawable t(Context context, h hVar, int i2) {
        Drawable r = r(context, i2);
        if (r == null) {
            r = hVar.c(i2);
        }
        if (r != null) {
            return v(context, i2, false, r);
        }
        return null;
    }

    public synchronized void u(f fVar) {
        this.g = fVar;
    }

    public final Drawable v(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList m = m(context, i2);
        if (m != null) {
            if (dr0.a(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable r = androidx.core.graphics.drawable.a.r(drawable);
            androidx.core.graphics.drawable.a.o(r, m);
            PorterDuff.Mode o = o(i2);
            if (o != null) {
                androidx.core.graphics.drawable.a.p(r, o);
                return r;
            }
            return r;
        }
        f fVar = this.g;
        if ((fVar == null || !fVar.e(context, i2, drawable)) && !x(context, i2, drawable) && z) {
            return null;
        }
        return drawable;
    }

    public boolean x(Context context, int i2, Drawable drawable) {
        f fVar = this.g;
        return fVar != null && fVar.b(context, i2, drawable);
    }
}
