package defpackage;

import android.content.SharedPreferences;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ji5  reason: default package */
/* loaded from: classes.dex */
public final class ji5 {
    public final String a;
    public boolean b;
    public String c;
    public final /* synthetic */ mi5 d;

    public ji5(mi5 mi5Var, String str, String str2) {
        this.d = mi5Var;
        zt2.f(str);
        this.a = str;
    }

    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.n().getString(this.a, null);
        }
        return this.c;
    }

    public final void b(String str) {
        SharedPreferences.Editor edit = this.d.n().edit();
        edit.putString(this.a, str);
        edit.apply();
        this.c = str;
    }
}
