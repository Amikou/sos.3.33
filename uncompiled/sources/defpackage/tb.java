package defpackage;

import android.util.SparseArray;
import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.i;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.u;
import androidx.media3.common.x;
import androidx.media3.common.y;
import androidx.media3.common.z;
import androidx.media3.exoplayer.source.j;
import java.io.IOException;
import java.util.List;

/* compiled from: AnalyticsListener.java */
/* renamed from: tb  reason: default package */
/* loaded from: classes.dex */
public interface tb {

    /* compiled from: AnalyticsListener.java */
    /* renamed from: tb$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final long a;
        public final u b;
        public final int c;
        public final j.b d;
        public final long e;
        public final u f;
        public final int g;
        public final j.b h;
        public final long i;
        public final long j;

        public a(long j, u uVar, int i, j.b bVar, long j2, u uVar2, int i2, j.b bVar2, long j3, long j4) {
            this.a = j;
            this.b = uVar;
            this.c = i;
            this.d = bVar;
            this.e = j2;
            this.f = uVar2;
            this.g = i2;
            this.h = bVar2;
            this.i = j3;
            this.j = j4;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.c == aVar.c && this.e == aVar.e && this.g == aVar.g && this.i == aVar.i && this.j == aVar.j && ql2.a(this.b, aVar.b) && ql2.a(this.d, aVar.d) && ql2.a(this.f, aVar.f) && ql2.a(this.h, aVar.h);
        }

        public int hashCode() {
            return ql2.b(Long.valueOf(this.a), this.b, Integer.valueOf(this.c), this.d, Long.valueOf(this.e), this.f, Integer.valueOf(this.g), this.h, Long.valueOf(this.i), Long.valueOf(this.j));
        }
    }

    /* compiled from: AnalyticsListener.java */
    /* renamed from: tb$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final i a;
        public final SparseArray<a> b;

        public b(i iVar, SparseArray<a> sparseArray) {
            this.a = iVar;
            SparseArray<a> sparseArray2 = new SparseArray<>(iVar.d());
            for (int i = 0; i < iVar.d(); i++) {
                int c = iVar.c(i);
                sparseArray2.append(c, (a) ii.e(sparseArray.get(c)));
            }
            this.b = sparseArray2;
        }

        public boolean a(int i) {
            return this.a.a(i);
        }

        public int b(int i) {
            return this.a.c(i);
        }

        public a c(int i) {
            return (a) ii.e(this.b.get(i));
        }

        public int d() {
            return this.a.d();
        }
    }

    void A(a aVar, g62 g62Var);

    void B(a aVar, int i, int i2);

    void C(a aVar, String str, long j, long j2);

    void D(a aVar, int i);

    void E(a aVar, m mVar, int i);

    void G(a aVar, x xVar);

    void H(a aVar, y yVar);

    void I(a aVar, Exception exc);

    void J(a aVar, n nVar);

    void K(a aVar, int i);

    void L(a aVar, String str, long j, long j2);

    @Deprecated
    void M(a aVar, int i, int i2, int i3, float f);

    void N(a aVar, hf0 hf0Var);

    @Deprecated
    void O(a aVar, boolean z, int i);

    @Deprecated
    void P(a aVar);

    @Deprecated
    void Q(a aVar, int i, String str, long j);

    void R(q qVar, b bVar);

    void S(a aVar, Object obj, long j);

    void T(a aVar, String str);

    @Deprecated
    void U(a aVar, String str, long j);

    void W(a aVar, hf0 hf0Var);

    void X(a aVar, int i);

    @Deprecated
    void Y(a aVar, androidx.media3.common.j jVar);

    void Z(a aVar, u02 u02Var, g62 g62Var);

    void a(a aVar, Metadata metadata);

    @Deprecated
    void a0(a aVar, int i, androidx.media3.common.j jVar);

    void b(a aVar, boolean z);

    void b0(a aVar, PlaybackException playbackException);

    void c(a aVar);

    void c0(a aVar, int i, long j, long j2);

    void d(a aVar, boolean z);

    void d0(a aVar, int i, long j, long j2);

    void e(a aVar, Exception exc);

    void e0(a aVar, hf0 hf0Var);

    void f(a aVar, q.b bVar);

    void f0(a aVar, g62 g62Var);

    void g0(a aVar, int i, boolean z);

    void h(a aVar, Exception exc);

    void h0(a aVar, PlaybackException playbackException);

    @Deprecated
    void i(a aVar, int i, hf0 hf0Var);

    @Deprecated
    void i0(a aVar, int i, hf0 hf0Var);

    @Deprecated
    void j(a aVar, List<kb0> list);

    void j0(a aVar);

    void k(a aVar);

    void k0(a aVar, nb0 nb0Var);

    void l(a aVar, String str);

    void l0(a aVar, int i, long j);

    void m(a aVar, boolean z);

    void m0(a aVar, u02 u02Var, g62 g62Var);

    void n0(a aVar, boolean z);

    void o(a aVar, u02 u02Var, g62 g62Var, IOException iOException, boolean z);

    void o0(a aVar, boolean z, int i);

    void p(a aVar, long j, int i);

    @Deprecated
    void p0(a aVar, androidx.media3.common.j jVar);

    void q(a aVar, int i);

    void q0(a aVar, hf0 hf0Var);

    void r(a aVar, p pVar);

    @Deprecated
    void r0(a aVar, int i);

    void s(a aVar, androidx.media3.common.j jVar, jf0 jf0Var);

    void s0(a aVar, Exception exc);

    @Deprecated
    void t(a aVar);

    @Deprecated
    void t0(a aVar, boolean z);

    void u(a aVar, long j);

    void u0(a aVar, q.e eVar, q.e eVar2, int i);

    @Deprecated
    void v(a aVar);

    @Deprecated
    void v0(a aVar, String str, long j);

    void w(a aVar, u02 u02Var, g62 g62Var);

    void w0(a aVar, int i);

    void x(a aVar);

    void x0(a aVar);

    void y(a aVar, androidx.media3.common.j jVar, jf0 jf0Var);

    void y0(a aVar, z zVar);

    void z(a aVar, h hVar);
}
