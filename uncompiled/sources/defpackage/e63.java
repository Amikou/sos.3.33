package defpackage;

import android.os.Bundle;
import java.util.HashMap;
import net.safemoon.androidwallet.R;

/* compiled from: ReflectionsFragmentDirections.java */
/* renamed from: e63  reason: default package */
/* loaded from: classes2.dex */
public class e63 {

    /* compiled from: ReflectionsFragmentDirections.java */
    /* renamed from: e63$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("tokenChainId")) {
                bundle.putInt("tokenChainId", ((Integer) this.a.get("tokenChainId")).intValue());
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_reflectionsFragment_to_addCustomReflectionContractFragment;
        }

        public int c() {
            return ((Integer) this.a.get("tokenChainId")).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            return this.a.containsKey("tokenChainId") == bVar.a.containsKey("tokenChainId") && c() == bVar.c() && b() == bVar.b();
        }

        public int hashCode() {
            return ((c() + 31) * 31) + b();
        }

        public String toString() {
            return "ActionReflectionsFragmentToAddCustomReflectionContractFragment(actionId=" + b() + "){tokenChainId=" + c() + "}";
        }

        public b(int i) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            hashMap.put("tokenChainId", Integer.valueOf(i));
        }
    }

    /* compiled from: ReflectionsFragmentDirections.java */
    /* renamed from: e63$c */
    /* loaded from: classes2.dex */
    public static class c implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("symbolWithType")) {
                bundle.putString("symbolWithType", (String) this.a.get("symbolWithType"));
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_reflectionsFragment_to_reflectionsAdvanceFragment;
        }

        public String c() {
            return (String) this.a.get("symbolWithType");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.a.containsKey("symbolWithType") != cVar.a.containsKey("symbolWithType")) {
                return false;
            }
            if (c() == null ? cVar.c() == null : c().equals(cVar.c())) {
                return b() == cVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionReflectionsFragmentToReflectionsAdvanceFragment(actionId=" + b() + "){symbolWithType=" + c() + "}";
        }

        public c(String str) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (str != null) {
                hashMap.put("symbolWithType", str);
                return;
            }
            throw new IllegalArgumentException("Argument \"symbolWithType\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(int i) {
        return new b(i);
    }

    public static ce2 b() {
        return new l6(R.id.action_reflectionsFragment_to_notificationHistoryFragment);
    }

    public static c c(String str) {
        return new c(str);
    }
}
