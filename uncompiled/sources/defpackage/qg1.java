package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;

/* compiled from: CoroutineScope.kt */
/* renamed from: qg1  reason: default package */
/* loaded from: classes2.dex */
public final class qg1 implements c90 {
    public static final qg1 a = new qg1();

    @Override // defpackage.c90
    public CoroutineContext m() {
        return EmptyCoroutineContext.INSTANCE;
    }
}
