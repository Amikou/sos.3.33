package defpackage;

import okio.SegmentedByteString;

/* compiled from: SegmentedByteString.kt */
/* renamed from: ej3  reason: default package */
/* loaded from: classes2.dex */
public final class ej3 {
    public static final int a(int[] iArr, int i, int i2, int i3) {
        fs1.f(iArr, "$this$binarySearch");
        int i4 = i3 - 1;
        while (i2 <= i4) {
            int i5 = (i2 + i4) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i) {
                i2 = i5 + 1;
            } else if (i6 <= i) {
                return i5;
            } else {
                i4 = i5 - 1;
            }
        }
        return (-i2) - 1;
    }

    public static final int b(SegmentedByteString segmentedByteString, int i) {
        fs1.f(segmentedByteString, "$this$segment");
        int a = a(segmentedByteString.getDirectory$okio(), i + 1, 0, segmentedByteString.getSegments$okio().length);
        return a >= 0 ? a : ~a;
    }
}
