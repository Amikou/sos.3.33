package defpackage;

import com.google.firebase.crashlytics.internal.common.e;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReportWithSessionId.java */
/* renamed from: mk  reason: default package */
/* loaded from: classes2.dex */
public final class mk extends e {
    public final r90 a;
    public final String b;

    public mk(r90 r90Var, String str) {
        Objects.requireNonNull(r90Var, "Null report");
        this.a = r90Var;
        Objects.requireNonNull(str, "Null sessionId");
        this.b = str;
    }

    @Override // com.google.firebase.crashlytics.internal.common.e
    public r90 b() {
        return this.a;
    }

    @Override // com.google.firebase.crashlytics.internal.common.e
    public String c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof e) {
            e eVar = (e) obj;
            return this.a.equals(eVar.b()) && this.b.equals(eVar.c());
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    public String toString() {
        return "CrashlyticsReportWithSessionId{report=" + this.a + ", sessionId=" + this.b + "}";
    }
}
