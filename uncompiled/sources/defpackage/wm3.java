package defpackage;

import androidx.work.impl.utils.futures.AbstractFuture;

/* compiled from: SettableFuture.java */
/* renamed from: wm3  reason: default package */
/* loaded from: classes.dex */
public final class wm3<V> extends AbstractFuture<V> {
    public static <V> wm3<V> t() {
        return new wm3<>();
    }

    @Override // androidx.work.impl.utils.futures.AbstractFuture
    public boolean p(V v) {
        return super.p(v);
    }

    @Override // androidx.work.impl.utils.futures.AbstractFuture
    public boolean q(Throwable th) {
        return super.q(th);
    }

    @Override // androidx.work.impl.utils.futures.AbstractFuture
    public boolean r(l02<? extends V> l02Var) {
        return super.r(l02Var);
    }
}
