package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import com.alexvasilkov.gestures.GestureController;
import com.alexvasilkov.gestures.Settings;
import com.github.mikephil.charting.utils.Utils;
import defpackage.uj4;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ViewPositionAnimator.java */
/* renamed from: tj4  reason: default package */
/* loaded from: classes.dex */
public class tj4 {
    public static final Matrix I = new Matrix();
    public static final float[] J = new float[2];
    public static final Point K = new Point();
    public boolean A;
    public boolean B;
    public boolean C;
    public boolean D;
    public boolean E;
    public boolean F;
    public final uj4 G;
    public final uj4 H;
    public final he d;
    public final GestureController e;
    public final oz f;
    public final mz g;
    public float j;
    public float k;
    public float l;
    public float m;
    public final Rect n;
    public final RectF o;
    public final RectF p;
    public final RectF q;
    public final RectF r;
    public final RectF s;
    public sj4 t;
    public sj4 u;
    public boolean v;
    public View w;
    public boolean x;
    public float y;
    public float z;
    public final List<e> a = new ArrayList();
    public final List<e> b = new ArrayList();
    public final e71 c = new e71();
    public final us3 h = new us3();
    public final us3 i = new us3();

    /* compiled from: ViewPositionAnimator.java */
    /* renamed from: tj4$a */
    /* loaded from: classes.dex */
    public class a implements uj4.b {
        public a() {
        }

        @Override // defpackage.uj4.b
        public void a(sj4 sj4Var) {
            if (ef1.a()) {
                StringBuilder sb = new StringBuilder();
                sb.append("'From' view position updated: ");
                sb.append(sj4Var.g());
            }
            tj4.this.t = sj4Var;
            tj4.this.A();
            tj4.this.n();
        }
    }

    /* compiled from: ViewPositionAnimator.java */
    /* renamed from: tj4$b */
    /* loaded from: classes.dex */
    public class b implements GestureController.e {
        public b() {
        }

        @Override // com.alexvasilkov.gestures.GestureController.e
        public void a(us3 us3Var) {
            tj4.this.e.p().c(tj4.this.h);
            tj4.this.e.p().c(tj4.this.i);
        }

        @Override // com.alexvasilkov.gestures.GestureController.e
        public void b(us3 us3Var, us3 us3Var2) {
            if (tj4.this.x) {
                if (ef1.a()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("State reset in listener: ");
                    sb.append(us3Var2);
                }
                tj4.this.D(us3Var2, 1.0f);
                tj4.this.n();
            }
        }
    }

    /* compiled from: ViewPositionAnimator.java */
    /* renamed from: tj4$c */
    /* loaded from: classes.dex */
    public class c implements uj4.b {
        public c() {
        }

        @Override // defpackage.uj4.b
        public void a(sj4 sj4Var) {
            if (ef1.a()) {
                StringBuilder sb = new StringBuilder();
                sb.append("'To' view position updated: ");
                sb.append(sj4Var.g());
            }
            tj4.this.u = sj4Var;
            tj4.this.B();
            tj4.this.A();
            tj4.this.n();
        }
    }

    /* compiled from: ViewPositionAnimator.java */
    /* renamed from: tj4$d */
    /* loaded from: classes.dex */
    public class d extends he {
        public d(View view) {
            super(view);
        }

        @Override // defpackage.he
        public boolean a() {
            if (tj4.this.c.e()) {
                return false;
            }
            tj4.this.c.a();
            tj4 tj4Var = tj4.this;
            tj4Var.z = tj4Var.c.c();
            tj4.this.n();
            if (tj4.this.c.e()) {
                tj4.this.z();
                return true;
            }
            return true;
        }
    }

    /* compiled from: ViewPositionAnimator.java */
    /* renamed from: tj4$e */
    /* loaded from: classes.dex */
    public interface e {
        void a(float f, boolean z);
    }

    public tj4(hf1 hf1Var) {
        Rect rect = new Rect();
        this.n = rect;
        this.o = new RectF();
        this.p = new RectF();
        this.q = new RectF();
        this.r = new RectF();
        this.s = new RectF();
        this.x = false;
        this.y = 1.0f;
        this.z = Utils.FLOAT_EPSILON;
        this.A = true;
        this.B = false;
        uj4 uj4Var = new uj4();
        this.G = uj4Var;
        uj4 uj4Var2 = new uj4();
        this.H = uj4Var2;
        new a();
        if (hf1Var instanceof View) {
            View view = (View) hf1Var;
            this.f = hf1Var instanceof oz ? (oz) hf1Var : null;
            this.g = hf1Var instanceof mz ? (mz) hf1Var : null;
            this.d = new d(view);
            t(view.getContext(), rect);
            GestureController controller = hf1Var.getController();
            this.e = controller;
            controller.j(new b());
            uj4Var2.c(view, new c());
            uj4Var.g(true);
            uj4Var2.g(true);
            return;
        }
        throw new IllegalArgumentException("Argument 'to' should be an instance of View");
    }

    public static Activity s(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        throw new IllegalArgumentException("Illegal context");
    }

    public static void t(Context context, Rect rect) {
        WindowManager windowManager = s(context).getWindowManager();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int i = Build.VERSION.SDK_INT;
        if (i >= 30) {
            context.getDisplay().getRealMetrics(displayMetrics);
        } else if (i >= 17) {
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);
        } else {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        rect.set(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    public final void A() {
        this.E = false;
    }

    public final void B() {
        this.F = false;
    }

    public void C(float f, boolean z, boolean z2) {
        if (this.x) {
            F();
            if (f < Utils.FLOAT_EPSILON) {
                f = 0.0f;
            } else if (f > 1.0f) {
                f = 1.0f;
            }
            this.z = f;
            this.A = z;
            if (z2) {
                E();
            }
            n();
            return;
        }
        throw new IllegalStateException("You should call enter(...) before calling setState(...)");
    }

    public void D(us3 us3Var, float f) {
        if (f <= Utils.FLOAT_EPSILON) {
            throw new IllegalArgumentException("'To' position cannot be <= 0");
        }
        if (f <= 1.0f) {
            if (ef1.a()) {
                StringBuilder sb = new StringBuilder();
                sb.append("State reset: ");
                sb.append(us3Var);
                sb.append(" at ");
                sb.append(f);
            }
            this.y = f;
            this.i.m(us3Var);
            B();
            A();
            return;
        }
        throw new IllegalArgumentException("'To' position cannot be > 1");
    }

    public final void E() {
        float f;
        float f2;
        long e2 = this.e.n().e();
        float f3 = this.y;
        if (f3 == 1.0f) {
            f2 = this.A ? this.z : 1.0f - this.z;
        } else {
            if (this.A) {
                f = this.z;
            } else {
                f = 1.0f - this.z;
                f3 = 1.0f - f3;
            }
            f2 = f / f3;
        }
        this.c.f(((float) e2) * f2);
        this.c.g(this.z, this.A ? Utils.FLOAT_EPSILON : 1.0f);
        this.d.c();
        y();
    }

    public void F() {
        this.c.b();
        z();
    }

    public final void G() {
        if (this.E) {
            return;
        }
        GestureController gestureController = this.e;
        Settings n = gestureController == null ? null : gestureController.n();
        if (this.v && n != null && this.u != null) {
            sj4 sj4Var = this.t;
            if (sj4Var == null) {
                sj4Var = sj4.f();
            }
            this.t = sj4Var;
            Point point = K;
            si1.a(n, point);
            Rect rect = this.u.a;
            point.offset(rect.left, rect.top);
            sj4.a(this.t, point);
        }
        if (this.u == null || this.t == null || n == null || !n.v()) {
            return;
        }
        this.j = this.t.d.centerX() - this.u.b.left;
        this.k = this.t.d.centerY() - this.u.b.top;
        float l = n.l();
        float k = n.k();
        float max = Math.max(l == Utils.FLOAT_EPSILON ? 1.0f : this.t.d.width() / l, k != Utils.FLOAT_EPSILON ? this.t.d.height() / k : 1.0f);
        this.h.l((this.t.d.centerX() - ((l * 0.5f) * max)) - this.u.b.left, (this.t.d.centerY() - ((k * 0.5f) * max)) - this.u.b.top, max, Utils.FLOAT_EPSILON);
        this.o.set(this.t.b);
        RectF rectF = this.o;
        Rect rect2 = this.u.a;
        rectF.offset(-rect2.left, -rect2.top);
        RectF rectF2 = this.q;
        Rect rect3 = this.n;
        int i = rect3.left;
        Rect rect4 = this.u.a;
        int i2 = rect4.left;
        int i3 = rect3.top;
        int i4 = rect4.top;
        rectF2.set(i - i2, i3 - i4, rect3.right - i2, rect3.bottom - i4);
        RectF rectF3 = this.q;
        float f = rectF3.left;
        sj4 sj4Var2 = this.t;
        rectF3.left = p(f, sj4Var2.a.left, sj4Var2.c.left, this.u.a.left);
        RectF rectF4 = this.q;
        float f2 = rectF4.top;
        sj4 sj4Var3 = this.t;
        rectF4.top = p(f2, sj4Var3.a.top, sj4Var3.c.top, this.u.a.top);
        RectF rectF5 = this.q;
        float f3 = rectF5.right;
        sj4 sj4Var4 = this.t;
        rectF5.right = p(f3, sj4Var4.a.right, sj4Var4.c.right, this.u.a.left);
        RectF rectF6 = this.q;
        float f4 = rectF6.bottom;
        sj4 sj4Var5 = this.t;
        rectF6.bottom = p(f4, sj4Var5.a.bottom, sj4Var5.c.bottom, this.u.a.top);
        this.E = true;
        ef1.a();
    }

    public final void H() {
        if (this.F) {
            return;
        }
        GestureController gestureController = this.e;
        Settings n = gestureController == null ? null : gestureController.n();
        if (this.u == null || n == null || !n.v()) {
            return;
        }
        us3 us3Var = this.i;
        Matrix matrix = I;
        us3Var.d(matrix);
        this.p.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, n.l(), n.k());
        float[] fArr = J;
        fArr[0] = this.p.centerX();
        fArr[1] = this.p.centerY();
        matrix.mapPoints(fArr);
        this.l = fArr[0];
        this.m = fArr[1];
        matrix.postRotate(-this.i.e(), this.l, this.m);
        matrix.mapRect(this.p);
        RectF rectF = this.p;
        sj4 sj4Var = this.u;
        Rect rect = sj4Var.b;
        int i = rect.left;
        Rect rect2 = sj4Var.a;
        rectF.offset(i - rect2.left, rect.top - rect2.top);
        this.r.set(this.n);
        RectF rectF2 = this.r;
        Rect rect3 = this.u.a;
        rectF2.offset(-rect3.left, -rect3.top);
        this.F = true;
        ef1.a();
    }

    public void m(e eVar) {
        this.a.add(eVar);
        this.b.remove(eVar);
    }

    public final void n() {
        if (this.x) {
            boolean z = true;
            if (this.C) {
                this.D = true;
                return;
            }
            this.C = true;
            boolean z2 = !this.A ? this.z != 1.0f : this.z != Utils.FLOAT_EPSILON;
            this.G.g(z2);
            this.H.g(z2);
            if (!this.F) {
                H();
            }
            if (!this.E) {
                G();
            }
            if (ef1.a()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Applying state: ");
                sb.append(this.z);
                sb.append(" / ");
                sb.append(this.A);
                sb.append(", 'to' ready = ");
                sb.append(this.F);
                sb.append(", 'from' ready = ");
                sb.append(this.E);
            }
            float f = this.z;
            float f2 = this.y;
            boolean z3 = f < f2 || (this.B && f == f2);
            if (this.F && this.E && z3) {
                us3 o = this.e.o();
                v42.c(o, this.h, this.j, this.k, this.i, this.l, this.m, this.z / this.y);
                this.e.V();
                float f3 = this.z;
                float f4 = this.y;
                if (f3 < f4 && (f3 != Utils.FLOAT_EPSILON || !this.A)) {
                    z = false;
                }
                float f5 = f3 / f4;
                if (this.f != null) {
                    v42.d(this.s, this.o, this.p, f5);
                    this.f.a(z ? null : this.s, o.e());
                }
                if (this.g != null) {
                    v42.d(this.s, this.q, this.r, f5);
                    this.g.b(z ? null : this.s);
                }
            }
            int size = this.a.size();
            for (int i = 0; i < size && !this.D; i++) {
                this.a.get(i).a(this.z, this.A);
            }
            q();
            if (this.z == Utils.FLOAT_EPSILON && this.A) {
                o();
                this.x = false;
                this.e.Q();
            }
            this.C = false;
            if (this.D) {
                this.D = false;
                n();
            }
        }
    }

    public final void o() {
        ef1.a();
        View view = this.w;
        if (view != null) {
            view.setVisibility(0);
        }
        oz ozVar = this.f;
        if (ozVar != null) {
            ozVar.a(null, Utils.FLOAT_EPSILON);
        }
        this.G.b();
        this.w = null;
        this.t = null;
        this.v = false;
        this.F = false;
        this.E = false;
    }

    public final float p(float f, int i, int i2, int i3) {
        int i4 = i - i2;
        return (-1 > i4 || i4 > 1) ? i2 - i3 : f;
    }

    public final void q() {
        this.a.removeAll(this.b);
        this.b.clear();
    }

    public void r(boolean z) {
        if (ef1.a()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Exiting, with animation = ");
            sb.append(z);
        }
        if (this.x) {
            boolean z2 = this.B;
            float f = Utils.FLOAT_EPSILON;
            if ((!z2 || this.z > this.y) && this.z > Utils.FLOAT_EPSILON) {
                D(this.e.o(), this.z);
            }
            if (z) {
                f = this.z;
            }
            C(f, true, z);
            return;
        }
        throw new IllegalStateException("You should call enter(...) before calling exit(...)");
    }

    public float u() {
        return this.z;
    }

    public float v() {
        return this.y;
    }

    public boolean w() {
        return this.B;
    }

    public boolean x() {
        return this.A;
    }

    public final void y() {
        if (this.B) {
            return;
        }
        this.B = true;
        ef1.a();
        this.e.n().a().b();
        this.e.S();
        GestureController gestureController = this.e;
        if (gestureController instanceof df1) {
            ((df1) gestureController).Y(true);
        }
    }

    public final void z() {
        if (this.B) {
            this.B = false;
            ef1.a();
            this.e.n().c().d();
            GestureController gestureController = this.e;
            if (gestureController instanceof df1) {
                ((df1) gestureController).Y(false);
            }
            this.e.k();
        }
    }
}
