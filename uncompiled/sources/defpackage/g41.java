package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;

/* compiled from: FillDrawer.java */
/* renamed from: g41  reason: default package */
/* loaded from: classes2.dex */
public class g41 extends hn {
    public Paint c;

    public g41(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
        Paint paint2 = new Paint();
        this.c = paint2;
        paint2.setStyle(Paint.Style.STROKE);
        this.c.setAntiAlias(true);
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2, int i3) {
        if (wg4Var instanceof f41) {
            f41 f41Var = (f41) wg4Var;
            int t = this.b.t();
            float m = this.b.m();
            int s = this.b.s();
            int q = this.b.q();
            int r = this.b.r();
            int f = this.b.f();
            if (this.b.z()) {
                if (i == r) {
                    t = f41Var.a();
                    m = f41Var.e();
                    s = f41Var.g();
                } else if (i == q) {
                    t = f41Var.b();
                    m = f41Var.f();
                    s = f41Var.h();
                }
            } else if (i == q) {
                t = f41Var.a();
                m = f41Var.e();
                s = f41Var.g();
            } else if (i == f) {
                t = f41Var.b();
                m = f41Var.f();
                s = f41Var.h();
            }
            this.c.setColor(t);
            this.c.setStrokeWidth(this.b.s());
            float f2 = i2;
            float f3 = i3;
            canvas.drawCircle(f2, f3, this.b.m(), this.c);
            this.c.setStrokeWidth(s);
            canvas.drawCircle(f2, f3, m, this.c);
        }
    }
}
