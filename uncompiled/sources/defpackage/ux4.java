package defpackage;

import android.os.Handler;
import android.os.Looper;
import com.google.android.play.core.assetpacks.c;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: ux4  reason: default package */
/* loaded from: classes2.dex */
public final class ux4 {
    public static final it4 f = new it4("AssetPackManager");
    public final c a;
    public final cw4<zy4> b;
    public final au4 c;
    public final vu4 d;
    public final cw4<Executor> e;

    public ux4(c cVar, cw4<zy4> cw4Var, au4 au4Var, oy4 oy4Var, zv4 zv4Var, fv4 fv4Var, vu4 vu4Var, cw4<Executor> cw4Var2, ws4 ws4Var) {
        new Handler(Looper.getMainLooper());
        this.a = cVar;
        this.b = cw4Var;
        this.c = au4Var;
        this.d = vu4Var;
        this.e = cw4Var2;
    }

    public final void a(boolean z) {
        boolean e = this.c.e();
        this.c.c(z);
        if (!z || e) {
            return;
        }
        f();
    }

    public final /* synthetic */ void b() {
        this.a.I();
        this.a.F();
        this.a.J();
    }

    public final /* synthetic */ void c() {
        l34<List<String>> f2 = this.b.a().f(this.a.q());
        c cVar = this.a;
        cVar.getClass();
        f2.b(this.e.a(), nx4.b(cVar));
        f2.a(this.e.a(), ox4.a);
    }

    public final void f() {
        this.e.a().execute(new Runnable(this, null) { // from class: kx4
            public final ux4 a;
            public final /* synthetic */ int f0 = 1;

            {
                this.a = this;
            }

            @Override // java.lang.Runnable
            public final void run() {
                if (this.f0 != 0) {
                    this.a.c();
                } else {
                    this.a.b();
                }
            }
        });
    }
}
