package defpackage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import defpackage.xm1;

/* compiled from: IUnusedAppRestrictionsBackportService.java */
/* renamed from: ym1  reason: default package */
/* loaded from: classes.dex */
public interface ym1 extends IInterface {

    /* compiled from: IUnusedAppRestrictionsBackportService.java */
    /* renamed from: ym1$a */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements ym1 {
        public a() {
            attachInterface(this, "androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportService");
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportService");
                B(xm1.a.b(parcel.readStrongBinder()));
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportService");
                return true;
            }
        }
    }

    void B(xm1 xm1Var) throws RemoteException;
}
