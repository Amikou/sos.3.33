package defpackage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.google.android.material.textview.MaterialTextView;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import com.trustwallet.walletconnect.models.ethereum.WCEthereumSignMessage;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.dialogs.common.BaseBottomSheetDialogFragment;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfo;
import net.safemoon.androidwallet.model.walletConnect.RoomExtensionKt;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: WalletConnectSignatureRequest.kt */
/* renamed from: jm4  reason: default package */
/* loaded from: classes2.dex */
public final class jm4 extends BaseBottomSheetDialogFragment {
    public static final a B0 = new a(null);
    public tb1 A0;
    public b w0;
    public RoomConnectedInfo x0;
    public WCEthereumSignMessage y0;
    public Wallet z0;

    /* compiled from: WalletConnectSignatureRequest.kt */
    /* renamed from: jm4$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final jm4 a() {
            jm4 jm4Var = new jm4();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            jm4Var.setArguments(bundle);
            return jm4Var;
        }
    }

    /* compiled from: WalletConnectSignatureRequest.kt */
    /* renamed from: jm4$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a();

        void b();
    }

    public static final void H(jm4 jm4Var, View view) {
        fs1.f(jm4Var, "this$0");
        jm4Var.h();
    }

    public static final void I(jm4 jm4Var, View view) {
        fs1.f(jm4Var, "this$0");
        b bVar = jm4Var.w0;
        if (bVar == null) {
            return;
        }
        bVar.b();
    }

    public final void J(RoomConnectedInfo roomConnectedInfo, long j, WCEthereumSignMessage wCEthereumSignMessage, Wallet wallet2, b bVar) {
        fs1.f(roomConnectedInfo, "roomConnectedInfo");
        fs1.f(wCEthereumSignMessage, ThrowableDeserializer.PROP_NAME_MESSAGE);
        fs1.f(wallet2, "wallet");
        fs1.f(bVar, "iWalletConnectSignatureContract");
        this.x0 = roomConnectedInfo;
        this.y0 = wCEthereumSignMessage;
        this.z0 = wallet2;
        this.w0 = bVar;
    }

    public final void K(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, jm4.class.getCanonicalName());
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_wallet_connect_signature_request, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        b bVar = this.w0;
        if (bVar == null) {
            return;
        }
        bVar.a();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String data;
        String peerMeta;
        WCPeerMeta peerMeta2;
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        tb1 a2 = tb1.a(view);
        this.A0 = a2;
        if (a2 != null) {
            a2.b.c.setText(getString(R.string.signature_request_screen_title));
            a2.b.a.setOnClickListener(new View.OnClickListener() { // from class: hm4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    jm4.H(jm4.this, view2);
                }
            });
        }
        tb1 tb1Var = this.A0;
        if (tb1Var == null) {
            return;
        }
        tb1Var.a.setOnClickListener(new View.OnClickListener() { // from class: im4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                jm4.I(jm4.this, view2);
            }
        });
        Wallet wallet2 = this.z0;
        if (wallet2 != null) {
            MaterialTextView materialTextView = tb1Var.e;
            materialTextView.setText(wallet2.displayName() + "\n(" + e30.d(wallet2.getAddress()) + ')');
        }
        RoomConnectedInfo roomConnectedInfo = this.x0;
        if (roomConnectedInfo != null && (peerMeta = roomConnectedInfo.getPeerMeta()) != null && (peerMeta2 = RoomExtensionKt.toPeerMeta(peerMeta)) != null) {
            tb1Var.c.setText(peerMeta2.getUrl());
        }
        WCEthereumSignMessage wCEthereumSignMessage = this.y0;
        if (wCEthereumSignMessage == null) {
            return;
        }
        if (wCEthereumSignMessage.getType() == WCEthereumSignMessage.WCSignType.TYPED_MESSAGE) {
            tb1Var.d.setText(wCEthereumSignMessage.getData());
        } else if (wCEthereumSignMessage.getType() == WCEthereumSignMessage.WCSignType.ETH_SIGN_TYPE_DATA_V4) {
            tb1Var.d.setText(wCEthereumSignMessage.getData());
        } else {
            MaterialTextView materialTextView2 = tb1Var.d;
            try {
                data = dv3.q(e30.F(wCEthereumSignMessage.getData()), 1, 0, false, 6, null);
            } catch (Exception unused) {
                data = wCEthereumSignMessage.getData();
            }
            materialTextView2.setText(data);
        }
    }
}
