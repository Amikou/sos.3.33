package defpackage;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import defpackage.l83;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: j15  reason: default package */
/* loaded from: classes.dex */
public final class j15<R extends l83> extends eb4<R> implements m83<R> {
    public q83<? super R, ? extends l83> a;
    public j15<? extends l83> b;
    public volatile n83<? super R> c;
    public final Object d;
    public Status e;
    public final WeakReference<GoogleApiClient> f;
    public final l15 g;

    public static void d(l83 l83Var) {
        if (l83Var instanceof k63) {
            try {
                ((k63) l83Var).a();
            } catch (RuntimeException unused) {
                String valueOf = String.valueOf(l83Var);
                StringBuilder sb = new StringBuilder(valueOf.length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
            }
        }
    }

    @Override // defpackage.m83
    public final void a(R r) {
        synchronized (this.d) {
            if (r.e().L1()) {
                if (this.a != null) {
                    d15.a().submit(new i15(this, r));
                } else if (j()) {
                    ((n83) zt2.j(this.c)).c(r);
                }
            } else {
                e(r.e());
                d(r);
            }
        }
    }

    public final void c() {
        this.c = null;
    }

    public final void e(Status status) {
        synchronized (this.d) {
            this.e = status;
            h(status);
        }
    }

    public final void h(Status status) {
        synchronized (this.d) {
            q83<? super R, ? extends l83> q83Var = this.a;
            if (q83Var != null) {
                ((j15) zt2.j(this.b)).e((Status) zt2.k(q83Var.a(status), "onFailure must not return null"));
            } else if (j()) {
                ((n83) zt2.j(this.c)).b(status);
            }
        }
    }

    public final boolean j() {
        return (this.c == null || this.f.get() == null) ? false : true;
    }
}
