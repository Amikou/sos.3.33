package defpackage;

import java.util.Locale;

/* compiled from: LocaleStore.kt */
/* renamed from: g12  reason: default package */
/* loaded from: classes2.dex */
public interface g12 {
    boolean a();

    void b(boolean z);

    void c(Locale locale);

    Locale d();
}
