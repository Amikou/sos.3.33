package defpackage;

import androidx.recyclerview.widget.g;

/* compiled from: NullPaddedListDiffHelper.kt */
/* renamed from: ui2  reason: default package */
/* loaded from: classes.dex */
public final class ui2 {
    public final g.e a;
    public final boolean b;

    public ui2(g.e eVar, boolean z) {
        fs1.f(eVar, "diff");
        this.a = eVar;
        this.b = z;
    }

    public final g.e a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }
}
