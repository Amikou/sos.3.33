package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityVerifyEmailBinding.java */
/* renamed from: c8  reason: default package */
/* loaded from: classes2.dex */
public final class c8 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final ConstraintLayout c;
    public final AppCompatImageView d;
    public final TextView e;
    public final TextInputLayout f;
    public final sp3 g;
    public final View h;

    public c8(ConstraintLayout constraintLayout, MaterialButton materialButton, ConstraintLayout constraintLayout2, AppCompatImageView appCompatImageView, TextView textView, TextInputLayout textInputLayout, sp3 sp3Var, View view) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = constraintLayout2;
        this.d = appCompatImageView;
        this.e = textView;
        this.f = textInputLayout;
        this.g = sp3Var;
        this.h = view;
    }

    public static c8 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.img_logo;
            AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
            if (appCompatImageView != null) {
                i = R.id.tilResendVerifyCodeTxt;
                TextView textView = (TextView) ai4.a(view, R.id.tilResendVerifyCodeTxt);
                if (textView != null) {
                    i = R.id.tilVerifyCode;
                    TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilVerifyCode);
                    if (textInputLayout != null) {
                        i = R.id.toolbar;
                        View a = ai4.a(view, R.id.toolbar);
                        if (a != null) {
                            sp3 a2 = sp3.a(a);
                            i = R.id.verifyCodeDivider;
                            View a3 = ai4.a(view, R.id.verifyCodeDivider);
                            if (a3 != null) {
                                return new c8(constraintLayout, materialButton, constraintLayout, appCompatImageView, textView, textInputLayout, a2, a3);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static c8 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static c8 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_verify_email, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
