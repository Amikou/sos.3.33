package defpackage;

import java.math.BigInteger;
import org.bouncycastle.asn1.i;

/* renamed from: wt0  reason: default package */
/* loaded from: classes2.dex */
public class wt0 {
    public static pt0 a(BigInteger bigInteger, ot0 ot0Var) {
        return ot0Var.b().y(bigInteger).A();
    }

    public static int[] b(int[] iArr) {
        int[] iArr2 = new int[3];
        if (iArr.length == 1) {
            iArr2[0] = iArr[0];
        } else if (iArr.length != 3) {
            throw new IllegalArgumentException("Only Trinomials and pentanomials supported");
        } else {
            if (iArr[0] < iArr[1] && iArr[0] < iArr[2]) {
                iArr2[0] = iArr[0];
                if (iArr[1] < iArr[2]) {
                    iArr2[1] = iArr[1];
                    iArr2[2] = iArr[2];
                } else {
                    iArr2[1] = iArr[2];
                    iArr2[2] = iArr[1];
                }
            } else if (iArr[1] < iArr[2]) {
                iArr2[0] = iArr[1];
                if (iArr[0] < iArr[2]) {
                    iArr2[1] = iArr[0];
                    iArr2[2] = iArr[2];
                } else {
                    iArr2[1] = iArr[2];
                    iArr2[2] = iArr[0];
                }
            } else {
                iArr2[0] = iArr[2];
                if (iArr[0] < iArr[1]) {
                    iArr2[1] = iArr[0];
                    iArr2[2] = iArr[1];
                } else {
                    iArr2[1] = iArr[1];
                    iArr2[2] = iArr[0];
                }
            }
        }
        return iArr2;
    }

    public static String c(pt0 pt0Var, ot0 ot0Var) {
        xs0 a = ot0Var.a();
        return a != null ? new v41(wh.k(pt0Var.l(false), a.o().e(), a.p().e(), ot0Var.b().l(false))).toString() : new v41(pt0Var.l(false)).toString();
    }

    public static String d(i iVar) {
        return lt0.d(iVar);
    }

    public static at0 e(gw2 gw2Var, ot0 ot0Var) {
        if (ot0Var instanceof jt0) {
            jt0 jt0Var = (jt0) ot0Var;
            return new mt0(h(jt0Var.f()), jt0Var.a(), jt0Var.b(), jt0Var.d(), jt0Var.c(), jt0Var.e());
        } else if (ot0Var == null) {
            ot0 b = gw2Var.b();
            return new at0(b.a(), b.b(), b.d(), b.c(), b.e());
        } else {
            return new at0(ot0Var.a(), ot0Var.b(), ot0Var.d(), ot0Var.c(), ot0Var.e());
        }
    }

    public static at0 f(gw2 gw2Var, nr4 nr4Var) {
        at0 at0Var;
        if (nr4Var.s()) {
            i G = i.G(nr4Var.p());
            pr4 g = g(G);
            if (g == null) {
                g = (pr4) gw2Var.a().get(G);
            }
            return new mt0(G, g.o(), g.p(), g.t(), g.q(), g.w());
        }
        if (nr4Var.q()) {
            ot0 b = gw2Var.b();
            at0Var = new at0(b.a(), b.b(), b.d(), b.c(), b.e());
        } else {
            pr4 s = pr4.s(nr4Var.p());
            at0Var = new at0(s.o(), s.p(), s.t(), s.q(), s.w());
        }
        return at0Var;
    }

    public static pr4 g(i iVar) {
        pr4 i = hc0.i(iVar);
        return i == null ? lt0.c(iVar) : i;
    }

    public static i h(String str) {
        int indexOf = str.indexOf(32);
        if (indexOf > 0) {
            str = str.substring(indexOf + 1);
        }
        try {
            if (str.charAt(0) >= '0' && str.charAt(0) <= '2') {
                return new i(str);
            }
        } catch (IllegalArgumentException unused) {
        }
        return lt0.e(str);
    }

    public static int i(gw2 gw2Var, BigInteger bigInteger, BigInteger bigInteger2) {
        if (bigInteger == null) {
            ot0 b = gw2Var.b();
            return b == null ? bigInteger2.bitLength() : b.d().bitLength();
        }
        return bigInteger.bitLength();
    }

    public static String j(String str, BigInteger bigInteger, ot0 ot0Var) {
        StringBuffer stringBuffer = new StringBuffer();
        String d = su3.d();
        pt0 a = a(bigInteger, ot0Var);
        stringBuffer.append(str);
        stringBuffer.append(" Private Key [");
        stringBuffer.append(c(a, ot0Var));
        stringBuffer.append("]");
        stringBuffer.append(d);
        stringBuffer.append("            X: ");
        stringBuffer.append(a.f().t().toString(16));
        stringBuffer.append(d);
        stringBuffer.append("            Y: ");
        stringBuffer.append(a.g().t().toString(16));
        stringBuffer.append(d);
        return stringBuffer.toString();
    }

    public static String k(String str, pt0 pt0Var, ot0 ot0Var) {
        StringBuffer stringBuffer = new StringBuffer();
        String d = su3.d();
        stringBuffer.append(str);
        stringBuffer.append(" Public Key [");
        stringBuffer.append(c(pt0Var, ot0Var));
        stringBuffer.append("]");
        stringBuffer.append(d);
        stringBuffer.append("            X: ");
        stringBuffer.append(pt0Var.f().t().toString(16));
        stringBuffer.append(d);
        stringBuffer.append("            Y: ");
        stringBuffer.append(pt0Var.g().t().toString(16));
        stringBuffer.append(d);
        return stringBuffer.toString();
    }
}
