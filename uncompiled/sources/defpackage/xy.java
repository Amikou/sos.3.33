package defpackage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.util.Property;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.progressindicator.CircularProgressIndicatorSpec;

/* compiled from: CircularIndeterminateAnimatorDelegate.java */
/* renamed from: xy  reason: default package */
/* loaded from: classes2.dex */
public final class xy extends hq1<ObjectAnimator> {
    public static final int[] l = {0, 1350, 2700, 4050};
    public static final int[] m = {667, 2017, 3367, 4717};
    public static final int[] n = {1000, 2350, 3700, 5050};
    public static final Property<xy, Float> o = new c(Float.class, "animationFraction");
    public static final Property<xy, Float> p = new d(Float.class, "completeEndFraction");
    public ObjectAnimator d;
    public ObjectAnimator e;
    public final f21 f;
    public final wn g;
    public int h;
    public float i;
    public float j;
    public hd k;

    /* compiled from: CircularIndeterminateAnimatorDelegate.java */
    /* renamed from: xy$a */
    /* loaded from: classes2.dex */
    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
            super.onAnimationRepeat(animator);
            xy xyVar = xy.this;
            xyVar.h = (xyVar.h + 4) % xy.this.g.c.length;
        }
    }

    /* compiled from: CircularIndeterminateAnimatorDelegate.java */
    /* renamed from: xy$b */
    /* loaded from: classes2.dex */
    public class b extends AnimatorListenerAdapter {
        public b() {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            xy.this.a();
            xy xyVar = xy.this;
            xyVar.k.onAnimationEnd(xyVar.a);
        }
    }

    /* compiled from: CircularIndeterminateAnimatorDelegate.java */
    /* renamed from: xy$c */
    /* loaded from: classes2.dex */
    public static class c extends Property<xy, Float> {
        public c(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(xy xyVar) {
            return Float.valueOf(xyVar.o());
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(xy xyVar, Float f) {
            xyVar.t(f.floatValue());
        }
    }

    /* compiled from: CircularIndeterminateAnimatorDelegate.java */
    /* renamed from: xy$d */
    /* loaded from: classes2.dex */
    public static class d extends Property<xy, Float> {
        public d(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(xy xyVar) {
            return Float.valueOf(xyVar.p());
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(xy xyVar, Float f) {
            xyVar.u(f.floatValue());
        }
    }

    public xy(CircularProgressIndicatorSpec circularProgressIndicatorSpec) {
        super(1);
        this.h = 0;
        this.k = null;
        this.g = circularProgressIndicatorSpec;
        this.f = new f21();
    }

    @Override // defpackage.hq1
    public void a() {
        ObjectAnimator objectAnimator = this.d;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
    }

    @Override // defpackage.hq1
    public void c() {
        s();
    }

    @Override // defpackage.hq1
    public void d(hd hdVar) {
        this.k = hdVar;
    }

    @Override // defpackage.hq1
    public void f() {
        if (this.e.isRunning()) {
            return;
        }
        if (this.a.isVisible()) {
            this.e.start();
        } else {
            a();
        }
    }

    @Override // defpackage.hq1
    public void g() {
        q();
        s();
        this.d.start();
    }

    @Override // defpackage.hq1
    public void h() {
        this.k = null;
    }

    public final float o() {
        return this.i;
    }

    public final float p() {
        return this.j;
    }

    public final void q() {
        if (this.d == null) {
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, o, Utils.FLOAT_EPSILON, 1.0f);
            this.d = ofFloat;
            ofFloat.setDuration(5400L);
            this.d.setInterpolator(null);
            this.d.setRepeatCount(-1);
            this.d.addListener(new a());
        }
        if (this.e == null) {
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this, p, Utils.FLOAT_EPSILON, 1.0f);
            this.e = ofFloat2;
            ofFloat2.setDuration(333L);
            this.e.setInterpolator(this.f);
            this.e.addListener(new b());
        }
    }

    public final void r(int i) {
        for (int i2 = 0; i2 < 4; i2++) {
            float b2 = b(i, n[i2], 333);
            if (b2 >= Utils.FLOAT_EPSILON && b2 <= 1.0f) {
                int i3 = i2 + this.h;
                int[] iArr = this.g.c;
                int length = i3 % iArr.length;
                this.c[0] = ih.b().evaluate(this.f.getInterpolation(b2), Integer.valueOf(l42.a(iArr[length], this.a.getAlpha())), Integer.valueOf(l42.a(this.g.c[(length + 1) % iArr.length], this.a.getAlpha()))).intValue();
                return;
            }
        }
    }

    public void s() {
        this.h = 0;
        this.c[0] = l42.a(this.g.c[0], this.a.getAlpha());
        this.j = Utils.FLOAT_EPSILON;
    }

    public void t(float f) {
        this.i = f;
        int i = (int) (f * 5400.0f);
        v(i);
        r(i);
        this.a.invalidateSelf();
    }

    public final void u(float f) {
        this.j = f;
    }

    public final void v(int i) {
        float[] fArr = this.b;
        float f = this.i;
        fArr[0] = (f * 1520.0f) - 20.0f;
        fArr[1] = f * 1520.0f;
        for (int i2 = 0; i2 < 4; i2++) {
            float b2 = b(i, l[i2], 667);
            float[] fArr2 = this.b;
            fArr2[1] = fArr2[1] + (this.f.getInterpolation(b2) * 250.0f);
            float b3 = b(i, m[i2], 667);
            float[] fArr3 = this.b;
            fArr3[0] = fArr3[0] + (this.f.getInterpolation(b3) * 250.0f);
        }
        float[] fArr4 = this.b;
        fArr4[0] = fArr4[0] + ((fArr4[1] - fArr4[0]) * this.j);
        fArr4[0] = fArr4[0] / 360.0f;
        fArr4[1] = fArr4[1] / 360.0f;
    }
}
