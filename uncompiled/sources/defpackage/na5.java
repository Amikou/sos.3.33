package defpackage;

import android.os.Parcel;

/* renamed from: na5  reason: default package */
/* loaded from: classes.dex */
public class na5 {
    static {
        na5.class.getClassLoader();
    }

    public static void a(Parcel parcel, boolean z) {
        parcel.writeInt(z ? 1 : 0);
    }

    public static boolean b(Parcel parcel) {
        return parcel.readInt() != 0;
    }
}
