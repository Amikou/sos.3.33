package defpackage;

import android.util.SparseArray;
import androidx.media3.common.j;
import defpackage.gc4;
import defpackage.wc2;
import java.util.ArrayList;
import java.util.Arrays;
import zendesk.support.request.CellBase;

/* compiled from: H264Reader.java */
/* renamed from: ej1  reason: default package */
/* loaded from: classes.dex */
public final class ej1 implements ku0 {
    public final fj3 a;
    public final boolean b;
    public final boolean c;
    public long g;
    public String i;
    public f84 j;
    public b k;
    public boolean l;
    public boolean n;
    public final boolean[] h = new boolean[3];
    public final vc2 d = new vc2(7, 128);
    public final vc2 e = new vc2(8, 128);
    public final vc2 f = new vc2(6, 128);
    public long m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    public final op2 o = new op2();

    /* compiled from: H264Reader.java */
    /* renamed from: ej1$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final f84 a;
        public final boolean b;
        public final boolean c;
        public final pp2 f;
        public byte[] g;
        public int h;
        public int i;
        public long j;
        public boolean k;
        public long l;
        public boolean o;
        public long p;
        public long q;
        public boolean r;
        public final SparseArray<wc2.c> d = new SparseArray<>();
        public final SparseArray<wc2.b> e = new SparseArray<>();
        public a m = new a();
        public a n = new a();

        /* compiled from: H264Reader.java */
        /* renamed from: ej1$b$a */
        /* loaded from: classes.dex */
        public static final class a {
            public boolean a;
            public boolean b;
            public wc2.c c;
            public int d;
            public int e;
            public int f;
            public int g;
            public boolean h;
            public boolean i;
            public boolean j;
            public boolean k;
            public int l;
            public int m;
            public int n;
            public int o;
            public int p;

            public a() {
            }

            public void b() {
                this.b = false;
                this.a = false;
            }

            public final boolean c(a aVar) {
                int i;
                int i2;
                int i3;
                boolean z;
                if (this.a) {
                    if (aVar.a) {
                        wc2.c cVar = (wc2.c) ii.i(this.c);
                        wc2.c cVar2 = (wc2.c) ii.i(aVar.c);
                        return (this.f == aVar.f && this.g == aVar.g && this.h == aVar.h && (!this.i || !aVar.i || this.j == aVar.j) && (((i = this.d) == (i2 = aVar.d) || (i != 0 && i2 != 0)) && (((i3 = cVar.k) != 0 || cVar2.k != 0 || (this.m == aVar.m && this.n == aVar.n)) && ((i3 != 1 || cVar2.k != 1 || (this.o == aVar.o && this.p == aVar.p)) && (z = this.k) == aVar.k && (!z || this.l == aVar.l))))) ? false : true;
                    }
                    return true;
                }
                return false;
            }

            public boolean d() {
                int i;
                return this.b && ((i = this.e) == 7 || i == 2);
            }

            public void e(wc2.c cVar, int i, int i2, int i3, int i4, boolean z, boolean z2, boolean z3, boolean z4, int i5, int i6, int i7, int i8, int i9) {
                this.c = cVar;
                this.d = i;
                this.e = i2;
                this.f = i3;
                this.g = i4;
                this.h = z;
                this.i = z2;
                this.j = z3;
                this.k = z4;
                this.l = i5;
                this.m = i6;
                this.n = i7;
                this.o = i8;
                this.p = i9;
                this.a = true;
                this.b = true;
            }

            public void f(int i) {
                this.e = i;
                this.b = true;
            }
        }

        public b(f84 f84Var, boolean z, boolean z2) {
            this.a = f84Var;
            this.b = z;
            this.c = z2;
            byte[] bArr = new byte[128];
            this.g = bArr;
            this.f = new pp2(bArr, 0, 0);
            g();
        }

        /* JADX WARN: Removed duplicated region for block: B:53:0x00ff  */
        /* JADX WARN: Removed duplicated region for block: B:54:0x0102  */
        /* JADX WARN: Removed duplicated region for block: B:56:0x0106  */
        /* JADX WARN: Removed duplicated region for block: B:60:0x0118  */
        /* JADX WARN: Removed duplicated region for block: B:63:0x011e  */
        /* JADX WARN: Removed duplicated region for block: B:74:0x0152  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void a(byte[] r24, int r25, int r26) {
            /*
                Method dump skipped, instructions count: 414
                To view this dump change 'Code comments level' option to 'DEBUG'
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.ej1.b.a(byte[], int, int):void");
        }

        public boolean b(long j, int i, boolean z, boolean z2) {
            boolean z3 = false;
            if (this.i == 9 || (this.c && this.n.c(this.m))) {
                if (z && this.o) {
                    d(i + ((int) (j - this.j)));
                }
                this.p = this.j;
                this.q = this.l;
                this.r = false;
                this.o = true;
            }
            if (this.b) {
                z2 = this.n.d();
            }
            boolean z4 = this.r;
            int i2 = this.i;
            if (i2 == 5 || (z2 && i2 == 1)) {
                z3 = true;
            }
            boolean z5 = z4 | z3;
            this.r = z5;
            return z5;
        }

        public boolean c() {
            return this.c;
        }

        public final void d(int i) {
            long j = this.q;
            if (j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
                return;
            }
            boolean z = this.r;
            this.a.b(j, z ? 1 : 0, (int) (this.j - this.p), i, null);
        }

        public void e(wc2.b bVar) {
            this.e.append(bVar.a, bVar);
        }

        public void f(wc2.c cVar) {
            this.d.append(cVar.d, cVar);
        }

        public void g() {
            this.k = false;
            this.o = false;
            this.n.b();
        }

        public void h(long j, int i, long j2) {
            this.i = i;
            this.l = j2;
            this.j = j;
            if (!this.b || i != 1) {
                if (!this.c) {
                    return;
                }
                if (i != 5 && i != 1 && i != 2) {
                    return;
                }
            }
            a aVar = this.m;
            this.m = this.n;
            this.n = aVar;
            aVar.b();
            this.h = 0;
            this.k = true;
        }
    }

    public ej1(fj3 fj3Var, boolean z, boolean z2) {
        this.a = fj3Var;
        this.b = z;
        this.c = z2;
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) {
        b();
        int e = op2Var.e();
        int f = op2Var.f();
        byte[] d = op2Var.d();
        this.g += op2Var.a();
        this.j.a(op2Var, op2Var.a());
        while (true) {
            int c = wc2.c(d, e, f, this.h);
            if (c == f) {
                h(d, e, f);
                return;
            }
            int f2 = wc2.f(d, c);
            int i = c - e;
            if (i > 0) {
                h(d, e, c);
            }
            int i2 = f - c;
            long j = this.g - i2;
            g(j, i2, i < 0 ? -i : 0, this.m);
            i(j, f2, this.m);
            e = c + 3;
        }
    }

    public final void b() {
        ii.i(this.j);
        androidx.media3.common.util.b.j(this.k);
    }

    @Override // defpackage.ku0
    public void c() {
        this.g = 0L;
        this.n = false;
        this.m = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        wc2.a(this.h);
        this.d.d();
        this.e.d();
        this.f.d();
        b bVar = this.k;
        if (bVar != null) {
            bVar.g();
        }
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.m = j;
        }
        this.n |= (i & 2) != 0;
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.i = dVar.b();
        f84 f = r11Var.f(dVar.c(), 2);
        this.j = f;
        this.k = new b(f, this.b, this.c);
        this.a.b(r11Var, dVar);
    }

    public final void g(long j, int i, int i2, long j2) {
        if (!this.l || this.k.c()) {
            this.d.b(i2);
            this.e.b(i2);
            if (!this.l) {
                if (this.d.c() && this.e.c()) {
                    ArrayList arrayList = new ArrayList();
                    vc2 vc2Var = this.d;
                    arrayList.add(Arrays.copyOf(vc2Var.d, vc2Var.e));
                    vc2 vc2Var2 = this.e;
                    arrayList.add(Arrays.copyOf(vc2Var2.d, vc2Var2.e));
                    vc2 vc2Var3 = this.d;
                    wc2.c l = wc2.l(vc2Var3.d, 3, vc2Var3.e);
                    vc2 vc2Var4 = this.e;
                    wc2.b j3 = wc2.j(vc2Var4.d, 3, vc2Var4.e);
                    this.j.f(new j.b().S(this.i).e0("video/avc").I(h00.a(l.a, l.b, l.c)).j0(l.e).Q(l.f).a0(l.g).T(arrayList).E());
                    this.l = true;
                    this.k.f(l);
                    this.k.e(j3);
                    this.d.d();
                    this.e.d();
                }
            } else if (this.d.c()) {
                vc2 vc2Var5 = this.d;
                this.k.f(wc2.l(vc2Var5.d, 3, vc2Var5.e));
                this.d.d();
            } else if (this.e.c()) {
                vc2 vc2Var6 = this.e;
                this.k.e(wc2.j(vc2Var6.d, 3, vc2Var6.e));
                this.e.d();
            }
        }
        if (this.f.b(i2)) {
            vc2 vc2Var7 = this.f;
            this.o.N(this.f.d, wc2.q(vc2Var7.d, vc2Var7.e));
            this.o.P(4);
            this.a.a(j2, this.o);
        }
        if (this.k.b(j, i, this.l, this.n)) {
            this.n = false;
        }
    }

    public final void h(byte[] bArr, int i, int i2) {
        if (!this.l || this.k.c()) {
            this.d.a(bArr, i, i2);
            this.e.a(bArr, i, i2);
        }
        this.f.a(bArr, i, i2);
        this.k.a(bArr, i, i2);
    }

    public final void i(long j, int i, long j2) {
        if (!this.l || this.k.c()) {
            this.d.e(i);
            this.e.e(i);
        }
        this.f.e(i);
        this.k.h(j, i, j2);
    }
}
