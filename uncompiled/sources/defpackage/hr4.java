package defpackage;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.a;

/* compiled from: WrappedDrawableApi14.java */
/* renamed from: hr4  reason: default package */
/* loaded from: classes.dex */
public class hr4 extends Drawable implements Drawable.Callback, gr4, i64 {
    public static final PorterDuff.Mode k0 = PorterDuff.Mode.SRC_IN;
    public int a;
    public PorterDuff.Mode f0;
    public boolean g0;
    public jr4 h0;
    public boolean i0;
    public Drawable j0;

    public hr4(jr4 jr4Var, Resources resources) {
        this.h0 = jr4Var;
        e(resources);
    }

    @Override // defpackage.gr4
    public final void a(Drawable drawable) {
        Drawable drawable2 = this.j0;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.j0 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            jr4 jr4Var = this.h0;
            if (jr4Var != null) {
                jr4Var.b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }

    @Override // defpackage.gr4
    public final Drawable b() {
        return this.j0;
    }

    public boolean c() {
        return true;
    }

    public final jr4 d() {
        return new jr4(this.h0);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.j0.draw(canvas);
    }

    public final void e(Resources resources) {
        Drawable.ConstantState constantState;
        jr4 jr4Var = this.h0;
        if (jr4Var == null || (constantState = jr4Var.b) == null) {
            return;
        }
        a(constantState.newDrawable(resources));
    }

    public final boolean f(int[] iArr) {
        if (c()) {
            jr4 jr4Var = this.h0;
            ColorStateList colorStateList = jr4Var.c;
            PorterDuff.Mode mode = jr4Var.d;
            if (colorStateList != null && mode != null) {
                int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
                if (!this.g0 || colorForState != this.a || mode != this.f0) {
                    setColorFilter(colorForState, mode);
                    this.a = colorForState;
                    this.f0 = mode;
                    this.g0 = true;
                    return true;
                }
            } else {
                this.g0 = false;
                clearColorFilter();
            }
            return false;
        }
        return false;
    }

    @Override // android.graphics.drawable.Drawable
    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        jr4 jr4Var = this.h0;
        return changingConfigurations | (jr4Var != null ? jr4Var.getChangingConfigurations() : 0) | this.j0.getChangingConfigurations();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        jr4 jr4Var = this.h0;
        if (jr4Var == null || !jr4Var.a()) {
            return null;
        }
        this.h0.a = getChangingConfigurations();
        return this.h0;
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable getCurrent() {
        return this.j0.getCurrent();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.j0.getIntrinsicHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.j0.getIntrinsicWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getLayoutDirection() {
        return a.f(this.j0);
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumHeight() {
        return this.j0.getMinimumHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumWidth() {
        return this.j0.getMinimumWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.j0.getOpacity();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        return this.j0.getPadding(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public int[] getState() {
        return this.j0.getState();
    }

    @Override // android.graphics.drawable.Drawable
    public Region getTransparentRegion() {
        return this.j0.getTransparentRegion();
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isAutoMirrored() {
        return a.h(this.j0);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        jr4 jr4Var;
        ColorStateList colorStateList = (!c() || (jr4Var = this.h0) == null) ? null : jr4Var.c;
        return (colorStateList != null && colorStateList.isStateful()) || this.j0.isStateful();
    }

    @Override // android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        this.j0.jumpToCurrentState();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        if (!this.i0 && super.mutate() == this) {
            this.h0 = d();
            Drawable drawable = this.j0;
            if (drawable != null) {
                drawable.mutate();
            }
            jr4 jr4Var = this.h0;
            if (jr4Var != null) {
                Drawable drawable2 = this.j0;
                jr4Var.b = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.i0 = true;
        }
        return this;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.j0;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLayoutDirectionChanged(int i) {
        return a.m(this.j0, i);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        return this.j0.setLevel(i);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.j0.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAutoMirrored(boolean z) {
        a.j(this.j0, z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setChangingConfigurations(int i) {
        this.j0.setChangingConfigurations(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.j0.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable
    public void setDither(boolean z) {
        this.j0.setDither(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setFilterBitmap(boolean z) {
        this.j0.setFilterBitmap(z);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setState(int[] iArr) {
        return f(iArr) || this.j0.setState(iArr);
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTintList(ColorStateList colorStateList) {
        this.h0.c = colorStateList;
        f(getState());
    }

    @Override // android.graphics.drawable.Drawable, defpackage.i64
    public void setTintMode(PorterDuff.Mode mode) {
        this.h0.d = mode;
        f(getState());
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.j0.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    public hr4(Drawable drawable) {
        this.h0 = d();
        a(drawable);
    }
}
