package defpackage;

import android.graphics.Typeface;
import android.view.accessibility.CaptioningManager;
import androidx.media3.common.util.b;

/* compiled from: CaptionStyleCompat.java */
/* renamed from: zv  reason: default package */
/* loaded from: classes.dex */
public final class zv {
    public static final zv g = new zv(-1, -16777216, 0, 0, -1, null);
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final Typeface f;

    public zv(int i, int i2, int i3, int i4, int i5, Typeface typeface) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = typeface;
    }

    public static zv a(CaptioningManager.CaptionStyle captionStyle) {
        if (b.a >= 21) {
            return c(captionStyle);
        }
        return b(captionStyle);
    }

    public static zv b(CaptioningManager.CaptionStyle captionStyle) {
        return new zv(captionStyle.foregroundColor, captionStyle.backgroundColor, 0, captionStyle.edgeType, captionStyle.edgeColor, captionStyle.getTypeface());
    }

    public static zv c(CaptioningManager.CaptionStyle captionStyle) {
        return new zv(captionStyle.hasForegroundColor() ? captionStyle.foregroundColor : g.a, captionStyle.hasBackgroundColor() ? captionStyle.backgroundColor : g.b, captionStyle.hasWindowColor() ? captionStyle.windowColor : g.c, captionStyle.hasEdgeType() ? captionStyle.edgeType : g.d, captionStyle.hasEdgeColor() ? captionStyle.edgeColor : g.e, captionStyle.getTypeface());
    }
}
