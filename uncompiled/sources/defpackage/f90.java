package defpackage;

import androidx.room.RoomDatabase;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CoroutinesRoom.kt */
/* renamed from: f90  reason: default package */
/* loaded from: classes.dex */
public final class f90 {
    public static final CoroutineDispatcher a(RoomDatabase roomDatabase) {
        fs1.f(roomDatabase, "<this>");
        Map<String, Object> l = roomDatabase.l();
        fs1.e(l, "backingFieldMap");
        Object obj = l.get("QueryDispatcher");
        if (obj == null) {
            Executor p = roomDatabase.p();
            fs1.e(p, "queryExecutor");
            obj = zy0.a(p);
            l.put("QueryDispatcher", obj);
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
        return (CoroutineDispatcher) obj;
    }

    public static final CoroutineDispatcher b(RoomDatabase roomDatabase) {
        fs1.f(roomDatabase, "<this>");
        Map<String, Object> l = roomDatabase.l();
        fs1.e(l, "backingFieldMap");
        Object obj = l.get("TransactionDispatcher");
        if (obj == null) {
            Executor s = roomDatabase.s();
            fs1.e(s, "transactionExecutor");
            obj = zy0.a(s);
            l.put("TransactionDispatcher", obj);
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
        return (CoroutineDispatcher) obj;
    }
}
