package defpackage;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import org.web3j.abi.datatypes.Address;
import org.web3j.protocol.core.a;
import org.web3j.protocol.core.c;

/* compiled from: JsonRpc2_0Web3j.java */
/* renamed from: zv1  reason: default package */
/* loaded from: classes3.dex */
public class zv1 implements ko4 {
    public static final int DEFAULT_BLOCK_TIME = 15000;
    private final long blockTime;
    private final ScheduledExecutorService scheduledExecutorService;
    private final yv1 web3jRx;
    public final lo4 web3jService;

    public zv1(lo4 lo4Var) {
        this(lo4Var, u84.DEFAULT_POLLING_FREQUENCY, ti.defaultExecutorService());
    }

    private Map<String, Object> createLogsParams(List<String> list, List<String> list2) {
        HashMap hashMap = new HashMap();
        if (!list.isEmpty()) {
            hashMap.put(Address.TYPE_NAME, list);
        }
        if (!list2.isEmpty()) {
            hashMap.put("topics", list2);
        }
        return hashMap;
    }

    @Override // defpackage.ko4
    public c<?, w9> adminNodeInfo() {
        return new c<>("admin_nodeInfo", Collections.emptyList(), this.web3jService, w9.class);
    }

    @Override // defpackage.ko4
    public c<?, x9> adminPeers() {
        return new c<>("admin_peers", Collections.emptyList(), this.web3jService, x9.class);
    }

    @Override // defpackage.ko4
    public q71<gw0> blockFlowable(boolean z) {
        return this.web3jRx.blockFlowable(z, this.blockTime);
    }

    @Override // defpackage.ko4
    public c<?, te0> dbGetHex(String str, String str2) {
        return new c<>("db_getHex", Arrays.asList(str, str2), this.web3jService, te0.class);
    }

    @Override // defpackage.ko4
    public c<?, ue0> dbGetString(String str, String str2) {
        return new c<>("db_getString", Arrays.asList(str, str2), this.web3jService, ue0.class);
    }

    @Override // defpackage.ko4
    public c<?, ve0> dbPutHex(String str, String str2, String str3) {
        return new c<>("db_putHex", Arrays.asList(str, str2, str3), this.web3jService, ve0.class);
    }

    @Override // defpackage.ko4
    public c<?, we0> dbPutString(String str, String str2, String str3) {
        return new c<>("db_putString", Arrays.asList(str, str2, str3), this.web3jService, we0.class);
    }

    @Override // defpackage.ko4
    public c<?, fw0> ethAccounts() {
        return new c<>("eth_accounts", Collections.emptyList(), this.web3jService, fw0.class);
    }

    @Override // defpackage.ko4
    public q71<String> ethBlockHashFlowable() {
        return this.web3jRx.ethBlockHashFlowable(this.blockTime);
    }

    @Override // defpackage.ko4
    public c<?, hw0> ethBlockNumber() {
        return new c<>("eth_blockNumber", Collections.emptyList(), this.web3jService, hw0.class);
    }

    @Override // defpackage.ko4
    public c<?, iw0> ethCall(p84 p84Var, gi0 gi0Var) {
        return new c<>("eth_call", Arrays.asList(p84Var, gi0Var), this.web3jService, iw0.class);
    }

    @Override // defpackage.ko4
    public c<?, jw0> ethChainId() {
        return new c<>("eth_chainId", Collections.emptyList(), this.web3jService, jw0.class);
    }

    @Override // defpackage.ko4
    public c<?, kw0> ethCoinbase() {
        return new c<>("eth_coinbase", Collections.emptyList(), this.web3jService, kw0.class);
    }

    @Override // defpackage.ko4
    public c<?, lw0> ethCompileLLL(String str) {
        return new c<>("eth_compileLLL", Arrays.asList(str), this.web3jService, lw0.class);
    }

    @Override // defpackage.ko4
    public c<?, mw0> ethCompileSerpent(String str) {
        return new c<>("eth_compileSerpent", Arrays.asList(str), this.web3jService, mw0.class);
    }

    @Override // defpackage.ko4
    public c<?, nw0> ethCompileSolidity(String str) {
        return new c<>("eth_compileSolidity", Arrays.asList(str), this.web3jService, nw0.class);
    }

    @Override // defpackage.ko4
    public c<?, ow0> ethEstimateGas(p84 p84Var) {
        return new c<>("eth_estimateGas", Arrays.asList(p84Var), this.web3jService, ow0.class);
    }

    @Override // defpackage.ko4
    public c<?, rw0> ethGasPrice() {
        return new c<>("eth_gasPrice", Collections.emptyList(), this.web3jService, rw0.class);
    }

    @Override // defpackage.ko4
    public c<?, sw0> ethGetBalance(String str, gi0 gi0Var) {
        return new c<>("eth_getBalance", Arrays.asList(str, gi0Var.getValue()), this.web3jService, sw0.class);
    }

    @Override // defpackage.ko4
    public c<?, gw0> ethGetBlockByHash(String str, boolean z) {
        return new c<>("eth_getBlockByHash", Arrays.asList(str, Boolean.valueOf(z)), this.web3jService, gw0.class);
    }

    @Override // defpackage.ko4
    public c<?, gw0> ethGetBlockByNumber(gi0 gi0Var, boolean z) {
        return new c<>("eth_getBlockByNumber", Arrays.asList(gi0Var.getValue(), Boolean.valueOf(z)), this.web3jService, gw0.class);
    }

    @Override // defpackage.ko4
    public c<?, tw0> ethGetBlockTransactionCountByHash(String str) {
        return new c<>("eth_getBlockTransactionCountByHash", Arrays.asList(str), this.web3jService, tw0.class);
    }

    @Override // defpackage.ko4
    public c<?, uw0> ethGetBlockTransactionCountByNumber(gi0 gi0Var) {
        return new c<>("eth_getBlockTransactionCountByNumber", Arrays.asList(gi0Var.getValue()), this.web3jService, uw0.class);
    }

    @Override // defpackage.ko4
    public c<?, vw0> ethGetCode(String str, gi0 gi0Var) {
        return new c<>("eth_getCode", Arrays.asList(str, gi0Var.getValue()), this.web3jService, vw0.class);
    }

    @Override // defpackage.ko4
    public c<?, ww0> ethGetCompilers() {
        return new c<>("eth_getCompilers", Collections.emptyList(), this.web3jService, ww0.class);
    }

    @Override // defpackage.ko4
    public c<?, ex0> ethGetFilterChanges(BigInteger bigInteger) {
        return new c<>("eth_getFilterChanges", Arrays.asList(ej2.toHexStringWithPrefixSafe(bigInteger)), this.web3jService, ex0.class);
    }

    @Override // defpackage.ko4
    public c<?, ex0> ethGetFilterLogs(BigInteger bigInteger) {
        return new c<>("eth_getFilterLogs", Arrays.asList(ej2.toHexStringWithPrefixSafe(bigInteger)), this.web3jService, ex0.class);
    }

    @Override // defpackage.ko4
    public c<?, ex0> ethGetLogs(qw0 qw0Var) {
        return new c<>("eth_getLogs", Arrays.asList(qw0Var), this.web3jService, ex0.class);
    }

    @Override // defpackage.ko4
    public c<?, xw0> ethGetStorageAt(String str, BigInteger bigInteger, gi0 gi0Var) {
        return new c<>("eth_getStorageAt", Arrays.asList(str, ej2.encodeQuantity(bigInteger), gi0Var.getValue()), this.web3jService, xw0.class);
    }

    @Override // defpackage.ko4
    public c<?, nx0> ethGetTransactionByBlockHashAndIndex(String str, BigInteger bigInteger) {
        return new c<>("eth_getTransactionByBlockHashAndIndex", Arrays.asList(str, ej2.encodeQuantity(bigInteger)), this.web3jService, nx0.class);
    }

    @Override // defpackage.ko4
    public c<?, nx0> ethGetTransactionByBlockNumberAndIndex(gi0 gi0Var, BigInteger bigInteger) {
        return new c<>("eth_getTransactionByBlockNumberAndIndex", Arrays.asList(gi0Var.getValue(), ej2.encodeQuantity(bigInteger)), this.web3jService, nx0.class);
    }

    @Override // defpackage.ko4
    public c<?, nx0> ethGetTransactionByHash(String str) {
        return new c<>("eth_getTransactionByHash", Arrays.asList(str), this.web3jService, nx0.class);
    }

    @Override // defpackage.ko4
    public c<?, yw0> ethGetTransactionCount(String str, gi0 gi0Var) {
        return new c<>("eth_getTransactionCount", Arrays.asList(str, gi0Var.getValue()), this.web3jService, yw0.class);
    }

    @Override // defpackage.ko4
    public c<?, zw0> ethGetTransactionReceipt(String str) {
        return new c<>("eth_getTransactionReceipt", Arrays.asList(str), this.web3jService, zw0.class);
    }

    @Override // defpackage.ko4
    public c<?, gw0> ethGetUncleByBlockHashAndIndex(String str, BigInteger bigInteger) {
        return new c<>("eth_getUncleByBlockHashAndIndex", Arrays.asList(str, ej2.encodeQuantity(bigInteger)), this.web3jService, gw0.class);
    }

    @Override // defpackage.ko4
    public c<?, gw0> ethGetUncleByBlockNumberAndIndex(gi0 gi0Var, BigInteger bigInteger) {
        return new c<>("eth_getUncleByBlockNumberAndIndex", Arrays.asList(gi0Var.getValue(), ej2.encodeQuantity(bigInteger)), this.web3jService, gw0.class);
    }

    @Override // defpackage.ko4
    public c<?, ax0> ethGetUncleCountByBlockHash(String str) {
        return new c<>("eth_getUncleCountByBlockHash", Arrays.asList(str), this.web3jService, ax0.class);
    }

    @Override // defpackage.ko4
    public c<?, bx0> ethGetUncleCountByBlockNumber(gi0 gi0Var) {
        return new c<>("eth_getUncleCountByBlockNumber", Arrays.asList(gi0Var.getValue()), this.web3jService, bx0.class);
    }

    @Override // defpackage.ko4
    public c<?, cx0> ethGetWork() {
        return new c<>("eth_getWork", Collections.emptyList(), this.web3jService, cx0.class);
    }

    @Override // defpackage.ko4
    public c<?, dx0> ethHashrate() {
        return new c<>("eth_hashrate", Collections.emptyList(), this.web3jService, dx0.class);
    }

    @Override // defpackage.ko4
    public q71<q12> ethLogFlowable(qw0 qw0Var) {
        return this.web3jRx.ethLogFlowable(qw0Var, this.blockTime);
    }

    @Override // defpackage.ko4
    public c<?, fx0> ethMining() {
        return new c<>("eth_mining", Collections.emptyList(), this.web3jService, fx0.class);
    }

    @Override // defpackage.ko4
    public c<?, pw0> ethNewBlockFilter() {
        return new c<>("eth_newBlockFilter", Collections.emptyList(), this.web3jService, pw0.class);
    }

    @Override // defpackage.ko4
    public c<?, pw0> ethNewFilter(qw0 qw0Var) {
        return new c<>("eth_newFilter", Arrays.asList(qw0Var), this.web3jService, pw0.class);
    }

    @Override // defpackage.ko4
    public c<?, pw0> ethNewPendingTransactionFilter() {
        return new c<>("eth_newPendingTransactionFilter", Collections.emptyList(), this.web3jService, pw0.class);
    }

    @Override // defpackage.ko4
    public q71<String> ethPendingTransactionHashFlowable() {
        return this.web3jRx.ethPendingTransactionHashFlowable(this.blockTime);
    }

    @Override // defpackage.ko4
    public c<?, gx0> ethProtocolVersion() {
        return new c<>("eth_protocolVersion", Collections.emptyList(), this.web3jService, gx0.class);
    }

    @Override // defpackage.ko4
    public c<?, hx0> ethSendRawTransaction(String str) {
        return new c<>("eth_sendRawTransaction", Arrays.asList(str), this.web3jService, hx0.class);
    }

    @Override // defpackage.ko4
    public c<?, hx0> ethSendTransaction(p84 p84Var) {
        return new c<>("eth_sendTransaction", Arrays.asList(p84Var), this.web3jService, hx0.class);
    }

    @Override // defpackage.ko4
    public c<?, ix0> ethSign(String str, String str2) {
        return new c<>("eth_sign", Arrays.asList(str, str2), this.web3jService, ix0.class);
    }

    @Override // defpackage.ko4
    public c<?, jx0> ethSubmitHashrate(String str, String str2) {
        return new c<>("eth_submitHashrate", Arrays.asList(str, str2), this.web3jService, jx0.class);
    }

    @Override // defpackage.ko4
    public c<?, kx0> ethSubmitWork(String str, String str2, String str3) {
        return new c<>("eth_submitWork", Arrays.asList(str, str2, str3), this.web3jService, kx0.class);
    }

    @Override // defpackage.ko4
    public c<?, mx0> ethSyncing() {
        return new c<>("eth_syncing", Collections.emptyList(), this.web3jService, mx0.class);
    }

    @Override // defpackage.ko4
    public c<?, ox0> ethUninstallFilter(BigInteger bigInteger) {
        return new c<>("eth_uninstallFilter", Arrays.asList(ej2.toHexStringWithPrefixSafe(bigInteger)), this.web3jService, ox0.class);
    }

    @Override // defpackage.ko4
    public q71<s12> logsNotifications(List<String> list, List<String> list2) {
        return this.web3jService.subscribe(new c("eth_subscribe", Arrays.asList("logs", createLogsParams(list, list2)), this.web3jService, lx0.class), "eth_unsubscribe", s12.class);
    }

    @Override // defpackage.ko4
    public c<?, ve2> netListening() {
        return new c<>("net_listening", Collections.emptyList(), this.web3jService, ve2.class);
    }

    @Override // defpackage.ko4
    public c<?, we2> netPeerCount() {
        return new c<>("net_peerCount", Collections.emptyList(), this.web3jService, we2.class);
    }

    @Override // defpackage.ko4
    public c<?, ye2> netVersion() {
        return new c<>("net_version", Collections.emptyList(), this.web3jService, ye2.class);
    }

    @Override // defpackage.ko4
    public a newBatch() {
        return new a(this.web3jService);
    }

    @Override // defpackage.ko4
    public q71<hf2> newHeadsNotifications() {
        return this.web3jService.subscribe(new c("eth_subscribe", Collections.singletonList("newHeads"), this.web3jService, lx0.class), "eth_unsubscribe", hf2.class);
    }

    @Override // defpackage.ko4
    public q71<o84> pendingTransactionFlowable() {
        return this.web3jRx.pendingTransactionFlowable(this.blockTime);
    }

    @Override // defpackage.ko4
    public q71<gw0> replayPastAndFutureBlocksFlowable(gi0 gi0Var, boolean z) {
        return this.web3jRx.replayPastAndFutureBlocksFlowable(gi0Var, z, this.blockTime);
    }

    @Override // defpackage.ko4
    public q71<o84> replayPastAndFutureTransactionsFlowable(gi0 gi0Var) {
        return this.web3jRx.replayPastAndFutureTransactionsFlowable(gi0Var, this.blockTime);
    }

    @Override // defpackage.ko4
    public q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, gi0 gi0Var2, boolean z) {
        return this.web3jRx.replayBlocksFlowable(gi0Var, gi0Var2, z);
    }

    @Override // defpackage.ko4
    public q71<o84> replayPastTransactionsFlowable(gi0 gi0Var, gi0 gi0Var2) {
        return this.web3jRx.replayTransactionsFlowable(gi0Var, gi0Var2);
    }

    @Override // defpackage.ko4
    public c<?, eo3> shhAddToGroup(String str) {
        return new c<>("shh_addToGroup", Arrays.asList(str), this.web3jService, eo3.class);
    }

    @Override // defpackage.ko4
    public c<?, ho3> shhGetFilterChanges(BigInteger bigInteger) {
        return new c<>("shh_getFilterChanges", Arrays.asList(ej2.toHexStringWithPrefixSafe(bigInteger)), this.web3jService, ho3.class);
    }

    @Override // defpackage.ko4
    public c<?, ho3> shhGetMessages(BigInteger bigInteger) {
        return new c<>("shh_getMessages", Arrays.asList(ej2.toHexStringWithPrefixSafe(bigInteger)), this.web3jService, ho3.class);
    }

    @Override // defpackage.ko4
    public c<?, go3> shhHasIdentity(String str) {
        return new c<>("shh_hasIdentity", Arrays.asList(str), this.web3jService, go3.class);
    }

    @Override // defpackage.ko4
    public c<?, io3> shhNewFilter(fo3 fo3Var) {
        return new c<>("shh_newFilter", Arrays.asList(fo3Var), this.web3jService, io3.class);
    }

    @Override // defpackage.ko4
    public c<?, jo3> shhNewGroup() {
        return new c<>("shh_newGroup", Collections.emptyList(), this.web3jService, jo3.class);
    }

    @Override // defpackage.ko4
    public c<?, ko3> shhNewIdentity() {
        return new c<>("shh_newIdentity", Collections.emptyList(), this.web3jService, ko3.class);
    }

    @Override // defpackage.ko4
    public c<?, lo3> shhPost(mo3 mo3Var) {
        return new c<>("shh_post", Arrays.asList(mo3Var), this.web3jService, lo3.class);
    }

    @Override // defpackage.ko4
    public c<?, no3> shhUninstallFilter(BigInteger bigInteger) {
        return new c<>("shh_uninstallFilter", Arrays.asList(ej2.toHexStringWithPrefixSafe(bigInteger)), this.web3jService, no3.class);
    }

    @Override // defpackage.ko4
    public c<?, oo3> shhVersion() {
        return new c<>("shh_version", Collections.emptyList(), this.web3jService, oo3.class);
    }

    @Override // defpackage.ko4
    public void shutdown() {
        this.scheduledExecutorService.shutdown();
        try {
            this.web3jService.close();
        } catch (IOException e) {
            throw new RuntimeException("Failed to close web3j service", e);
        }
    }

    @Override // defpackage.ko4
    public q71<o84> transactionFlowable() {
        return this.web3jRx.transactionFlowable(this.blockTime);
    }

    @Override // defpackage.ko4
    public c<?, ho4> web3ClientVersion() {
        return new c<>("web3_clientVersion", Collections.emptyList(), this.web3jService, ho4.class);
    }

    @Override // defpackage.ko4
    public c<?, io4> web3Sha3(String str) {
        return new c<>("web3_sha3", Arrays.asList(str), this.web3jService, io4.class);
    }

    public zv1(lo4 lo4Var, long j, ScheduledExecutorService scheduledExecutorService) {
        this.web3jService = lo4Var;
        this.web3jRx = new yv1(this, scheduledExecutorService);
        this.blockTime = j;
        this.scheduledExecutorService = scheduledExecutorService;
    }

    @Override // defpackage.ko4
    public q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, gi0 gi0Var2, boolean z, boolean z2) {
        return this.web3jRx.replayBlocksFlowable(gi0Var, gi0Var2, z, z2);
    }

    @Override // defpackage.ko4
    public q71<o84> replayPastTransactionsFlowable(gi0 gi0Var) {
        return this.web3jRx.replayPastTransactionsFlowable(gi0Var);
    }

    @Override // defpackage.ko4
    public q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, boolean z, q71<gw0> q71Var) {
        return this.web3jRx.replayPastBlocksFlowable(gi0Var, z, q71Var);
    }

    @Override // defpackage.ko4
    public q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, boolean z) {
        return this.web3jRx.replayPastBlocksFlowable(gi0Var, z);
    }
}
