package defpackage;

import android.os.Build;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: hm5  reason: default package */
/* loaded from: classes.dex */
public final class hm5 {
    static {
        a();
    }

    public static boolean a() {
        return Build.VERSION.SDK_INT >= 24;
    }
}
