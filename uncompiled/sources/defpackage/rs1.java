package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: ItemAktFiatBinding.java */
/* renamed from: rs1  reason: default package */
/* loaded from: classes2.dex */
public final class rs1 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final TextView c;

    public rs1(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, TextView textView, View view) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = textView;
    }

    public static rs1 a(View view) {
        int i = R.id.cbSelectFiat;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbSelectFiat);
        if (materialCheckBox != null) {
            i = R.id.tvFiatName;
            TextView textView = (TextView) ai4.a(view, R.id.tvFiatName);
            if (textView != null) {
                i = R.id.vDivider;
                View a = ai4.a(view, R.id.vDivider);
                if (a != null) {
                    return new rs1((ConstraintLayout) view, materialCheckBox, textView, a);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
