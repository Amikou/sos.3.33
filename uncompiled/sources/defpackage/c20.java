package defpackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/* compiled from: Iterables.kt */
/* renamed from: c20  reason: default package */
/* loaded from: classes2.dex */
public class c20 extends b20 {
    public static final <T> int q(Iterable<? extends T> iterable, int i) {
        fs1.f(iterable, "$this$collectionSizeOrDefault");
        return iterable instanceof Collection ? ((Collection) iterable).size() : i;
    }

    public static final <T> Collection<T> r(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        fs1.f(iterable, "$this$convertToSetForSetOperationWith");
        fs1.f(iterable2, "source");
        if (iterable instanceof Set) {
            return (Collection) iterable;
        }
        if (iterable instanceof Collection) {
            if (!(iterable2 instanceof Collection) || ((Collection) iterable2).size() >= 2) {
                Collection<T> collection = (Collection) iterable;
                return s(collection) ? j20.i0(iterable) : collection;
            }
            return (Collection) iterable;
        }
        return j20.i0(iterable);
    }

    public static final <T> boolean s(Collection<? extends T> collection) {
        return collection.size() > 2 && (collection instanceof ArrayList);
    }
}
