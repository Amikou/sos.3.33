package defpackage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import retrofit2.n;

/* compiled from: RetrofitErrorResponse.java */
/* renamed from: v83  reason: default package */
/* loaded from: classes2.dex */
public class v83 implements cw0 {
    public Throwable a;
    public n b;

    public v83(Throwable th) {
        this.a = th;
    }

    public static v83 a(n nVar) {
        return new v83(nVar);
    }

    public static v83 b(Throwable th) {
        return new v83(th);
    }

    @Override // defpackage.cw0
    public int e() {
        n nVar = this.b;
        if (nVar != null) {
            return nVar.b();
        }
        return -1;
    }

    @Override // defpackage.cw0
    public String f() {
        n nVar = this.b;
        return (nVar == null || nVar.d() == null) ? "" : this.b.d().contentType().toString();
    }

    @Override // defpackage.cw0
    public String g() {
        n nVar = this.b;
        return (nVar == null || nVar.g().request() == null || this.b.g().request().url() == null) ? "" : this.b.g().request().url().toString();
    }

    @Override // defpackage.cw0
    public String h() {
        Throwable th = this.a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        n nVar = this.b;
        if (nVar != null) {
            if (ru3.b(nVar.f())) {
                sb.append(this.b.f());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }

    @Override // defpackage.cw0
    public boolean i() {
        n nVar;
        return (this.a != null || (nVar = this.b) == null || nVar.e()) ? false : true;
    }

    @Override // defpackage.cw0
    public boolean j() {
        Throwable th = this.a;
        return th != null && (th instanceof IOException);
    }

    @Override // defpackage.cw0
    public String k() {
        n nVar = this.b;
        if (nVar != null && nVar.d() != null) {
            try {
                return new String(this.b.d().bytes(), "UTF-8");
            } catch (UnsupportedEncodingException unused) {
                throw new AssertionError("UTF-8 must be supported");
            } catch (IOException unused2) {
            }
        }
        return "";
    }

    public v83(n nVar) {
        this.b = nVar;
    }
}
