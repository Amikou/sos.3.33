package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: q26  reason: default package */
/* loaded from: classes.dex */
public final class q26 implements p26 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.client.properties.non_null_origin", true);

    @Override // defpackage.p26
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
