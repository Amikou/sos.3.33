package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: ys5  reason: default package */
/* loaded from: classes.dex */
public class ys5 {
    public /* synthetic */ ys5(ts5 ts5Var) {
    }

    public static int a(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    public static long b(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }
}
