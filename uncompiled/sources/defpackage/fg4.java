package defpackage;

import androidx.lifecycle.LiveData;
import java.util.Arrays;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.model.token.room.RoomToken;

/* compiled from: UserTokenListRepository.kt */
/* renamed from: fg4  reason: default package */
/* loaded from: classes2.dex */
public final class fg4 implements bn1 {
    public final zm1 a;

    public fg4(zm1 zm1Var) {
        fs1.f(zm1Var, "localUserTokenDataSource");
        this.a = zm1Var;
    }

    @Override // defpackage.bn1
    public LiveData<List<RoomToken>> a(TokenType tokenType) {
        fs1.f(tokenType, "tokenType");
        return this.a.a(tokenType);
    }

    @Override // defpackage.bn1
    public Object b(String str, String str2, q70<? super te4> q70Var) {
        Object b = this.a.b(str, str2, q70Var);
        return b == gs1.d() ? b : te4.a;
    }

    @Override // defpackage.bn1
    public void c(String str, double d) {
        fs1.f(str, "symbolWithType");
        this.a.c(str, d);
    }

    @Override // defpackage.bn1
    public LiveData<RoomToken> d(String str) {
        fs1.f(str, "symbolWithType");
        return this.a.d(str);
    }

    @Override // defpackage.bn1
    public Object e(String str, q70<? super RoomToken> q70Var) {
        return this.a.e(str, q70Var);
    }

    @Override // defpackage.bn1
    public void f(String str, double d, double d2) {
        fs1.f(str, "symbolWithType");
        this.a.f(str, d, d2);
    }

    @Override // defpackage.bn1
    public boolean g(IToken iToken, TokenType tokenType) {
        fs1.f(iToken, "token");
        fs1.f(tokenType, "tokenType");
        return this.a.g(iToken, tokenType);
    }

    @Override // defpackage.bn1
    public void h(IToken iToken) {
        fs1.f(iToken, "token");
        this.a.h(iToken);
    }

    @Override // defpackage.bn1
    public void i(IToken iToken) {
        fs1.f(iToken, "token");
        this.a.i(iToken);
    }

    @Override // defpackage.bn1
    public void j(Pair<String, Integer>... pairArr) {
        fs1.f(pairArr, "orders");
        this.a.j((Pair[]) Arrays.copyOf(pairArr, pairArr.length));
    }

    @Override // defpackage.bn1
    public LiveData<List<RoomToken>> k() {
        return this.a.k();
    }
}
