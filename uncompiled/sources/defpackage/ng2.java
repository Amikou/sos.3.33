package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;

/* compiled from: SafeCollector.kt */
/* renamed from: ng2  reason: default package */
/* loaded from: classes2.dex */
public final class ng2 implements q70<Object> {
    public static final ng2 a = new ng2();
    public static final CoroutineContext f0 = EmptyCoroutineContext.INSTANCE;

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        return f0;
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
    }
}
