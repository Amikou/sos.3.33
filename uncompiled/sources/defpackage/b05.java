package defpackage;

import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: b05  reason: default package */
/* loaded from: classes.dex */
public abstract class b05 implements Runnable {
    public final /* synthetic */ rz4 a;

    public b05(rz4 rz4Var) {
        this.a = rz4Var;
    }

    public abstract void a();

    @Override // java.lang.Runnable
    public void run() {
        Lock lock;
        Lock lock2;
        i05 i05Var;
        lock = this.a.b;
        lock.lock();
        try {
            if (Thread.interrupted()) {
                return;
            }
            a();
        } catch (RuntimeException e) {
            i05Var = this.a.a;
            i05Var.j(e);
        } finally {
            lock2 = this.a.b;
            lock2.unlock();
        }
    }

    public /* synthetic */ b05(rz4 rz4Var, uz4 uz4Var) {
        this(rz4Var);
    }
}
