package defpackage;

import android.database.sqlite.SQLiteProgram;

/* compiled from: FrameworkSQLiteProgram.java */
/* renamed from: ic1  reason: default package */
/* loaded from: classes.dex */
public class ic1 implements uw3 {
    public final SQLiteProgram a;

    public ic1(SQLiteProgram sQLiteProgram) {
        this.a = sQLiteProgram;
    }

    @Override // defpackage.uw3
    public void A0(int i, byte[] bArr) {
        this.a.bindBlob(i, bArr);
    }

    @Override // defpackage.uw3
    public void L(int i, String str) {
        this.a.bindString(i, str);
    }

    @Override // defpackage.uw3
    public void Y(int i, double d) {
        this.a.bindDouble(i, d);
    }

    @Override // defpackage.uw3
    public void Y0(int i) {
        this.a.bindNull(i);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @Override // defpackage.uw3
    public void q0(int i, long j) {
        this.a.bindLong(i, j);
    }
}
