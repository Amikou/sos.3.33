package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogBestMaxFragmentBinding.java */
/* renamed from: ln0  reason: default package */
/* loaded from: classes2.dex */
public final class ln0 {
    public final MaterialButton a;
    public final MaterialButton b;
    public final AppCompatImageView c;

    public ln0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4, MaterialTextView materialTextView5, MaterialTextView materialTextView6, AppCompatImageView appCompatImageView, FrameLayout frameLayout) {
        this.a = materialButton;
        this.b = materialButton2;
        this.c = appCompatImageView;
    }

    public static ln0 a(View view) {
        int i = R.id.clkCancel;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.clkCancel);
        if (materialButton != null) {
            i = R.id.clkOk;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.clkOk);
            if (materialButton2 != null) {
                i = R.id.dialog_result1;
                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_result1);
                if (materialTextView != null) {
                    i = R.id.dialog_result2;
                    MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.dialog_result2);
                    if (materialTextView2 != null) {
                        i = R.id.dialog_result3;
                        MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.dialog_result3);
                        if (materialTextView3 != null) {
                            i = R.id.dialog_result4;
                            MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.dialog_result4);
                            if (materialTextView4 != null) {
                                i = R.id.dialog_result5;
                                MaterialTextView materialTextView5 = (MaterialTextView) ai4.a(view, R.id.dialog_result5);
                                if (materialTextView5 != null) {
                                    i = R.id.dialog_title;
                                    MaterialTextView materialTextView6 = (MaterialTextView) ai4.a(view, R.id.dialog_title);
                                    if (materialTextView6 != null) {
                                        i = R.id.progressLoading;
                                        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.progressLoading);
                                        if (appCompatImageView != null) {
                                            i = R.id.progressLoadingParent;
                                            FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.progressLoadingParent);
                                            if (frameLayout != null) {
                                                return new ln0((ConstraintLayout) view, materialButton, materialButton2, materialTextView, materialTextView2, materialTextView3, materialTextView4, materialTextView5, materialTextView6, appCompatImageView, frameLayout);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
