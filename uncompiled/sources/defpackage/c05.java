package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: c05  reason: default package */
/* loaded from: classes.dex */
public final class c05 implements GoogleApiClient.b, GoogleApiClient.c {
    public final /* synthetic */ rz4 a;

    public c05(rz4 rz4Var) {
        this.a = rz4Var;
    }

    @Override // defpackage.u50
    public final void onConnected(Bundle bundle) {
        kz kzVar;
        p15 p15Var;
        kzVar = this.a.r;
        kz kzVar2 = (kz) zt2.j(kzVar);
        p15Var = this.a.k;
        ((p15) zt2.j(p15Var)).j(new a05(this.a));
    }

    @Override // defpackage.jm2
    public final void onConnectionFailed(ConnectionResult connectionResult) {
        Lock lock;
        Lock lock2;
        boolean n;
        lock = this.a.b;
        lock.lock();
        try {
            n = this.a.n(connectionResult);
            if (n) {
                this.a.D();
                this.a.y();
            } else {
                this.a.q(connectionResult);
            }
        } finally {
            lock2 = this.a.b;
            lock2.unlock();
        }
    }

    @Override // defpackage.u50
    public final void onConnectionSuspended(int i) {
    }

    public /* synthetic */ c05(rz4 rz4Var, uz4 uz4Var) {
        this(rz4Var);
    }
}
