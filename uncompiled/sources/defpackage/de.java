package defpackage;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* compiled from: AnimationBackend.java */
/* renamed from: de  reason: default package */
/* loaded from: classes.dex */
public interface de extends ke {
    int c();

    void clear();

    void d(Rect rect);

    int e();

    void g(ColorFilter colorFilter);

    void i(int i);

    boolean j(Drawable drawable, Canvas canvas, int i);
}
