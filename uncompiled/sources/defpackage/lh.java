package defpackage;

import java.lang.reflect.Array;
import java.util.HashSet;

/* compiled from: ArrayBuilders.java */
/* renamed from: lh  reason: default package */
/* loaded from: classes.dex */
public final class lh {
    public b a = null;
    public c b = null;
    public h c = null;
    public f d = null;
    public g e = null;
    public e f = null;
    public d g = null;

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$a */
    /* loaded from: classes.dex */
    public static class a {
        public final /* synthetic */ Class a;
        public final /* synthetic */ int b;
        public final /* synthetic */ Object c;

        public a(Class cls, int i, Object obj) {
            this.a = cls;
            this.b = i;
            this.c = obj;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj != null && obj.getClass() == this.a && Array.getLength(obj) == this.b) {
                for (int i = 0; i < this.b; i++) {
                    Object obj2 = Array.get(this.c, i);
                    Object obj3 = Array.get(obj, i);
                    if (obj2 != obj3 && obj2 != null && !obj2.equals(obj3)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$b */
    /* loaded from: classes.dex */
    public static final class b extends tu2<boolean[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final boolean[] a(int i) {
            return new boolean[i];
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$c */
    /* loaded from: classes.dex */
    public static final class c extends tu2<byte[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final byte[] a(int i) {
            return new byte[i];
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$d */
    /* loaded from: classes.dex */
    public static final class d extends tu2<double[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final double[] a(int i) {
            return new double[i];
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$e */
    /* loaded from: classes.dex */
    public static final class e extends tu2<float[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final float[] a(int i) {
            return new float[i];
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$f */
    /* loaded from: classes.dex */
    public static final class f extends tu2<int[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final int[] a(int i) {
            return new int[i];
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$g */
    /* loaded from: classes.dex */
    public static final class g extends tu2<long[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final long[] a(int i) {
            return new long[i];
        }
    }

    /* compiled from: ArrayBuilders.java */
    /* renamed from: lh$h */
    /* loaded from: classes.dex */
    public static final class h extends tu2<short[]> {
        @Override // defpackage.tu2
        /* renamed from: g */
        public final short[] a(int i) {
            return new short[i];
        }
    }

    public static <T> HashSet<T> a(T[] tArr) {
        HashSet<T> hashSet = new HashSet<>();
        if (tArr != null) {
            for (T t : tArr) {
                hashSet.add(t);
            }
        }
        return hashSet;
    }

    public static Object b(Object obj) {
        return new a(obj.getClass(), Array.getLength(obj), obj);
    }

    public static <T> T[] j(T[] tArr, T t) {
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            if (tArr[i] == t) {
                if (i == 0) {
                    return tArr;
                }
                T[] tArr2 = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), length));
                System.arraycopy(tArr, 0, tArr2, 1, i);
                tArr2[0] = t;
                int i2 = i + 1;
                int i3 = length - i2;
                if (i3 > 0) {
                    System.arraycopy(tArr, i2, tArr2, i2, i3);
                }
                return tArr2;
            }
        }
        T[] tArr3 = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + 1));
        if (length > 0) {
            System.arraycopy(tArr, 0, tArr3, 1, length);
        }
        tArr3[0] = t;
        return tArr3;
    }

    public b c() {
        if (this.a == null) {
            this.a = new b();
        }
        return this.a;
    }

    public c d() {
        if (this.b == null) {
            this.b = new c();
        }
        return this.b;
    }

    public d e() {
        if (this.g == null) {
            this.g = new d();
        }
        return this.g;
    }

    public e f() {
        if (this.f == null) {
            this.f = new e();
        }
        return this.f;
    }

    public f g() {
        if (this.d == null) {
            this.d = new f();
        }
        return this.d;
    }

    public g h() {
        if (this.e == null) {
            this.e = new g();
        }
        return this.e;
    }

    public h i() {
        if (this.c == null) {
            this.c = new h();
        }
        return this.c;
    }
}
