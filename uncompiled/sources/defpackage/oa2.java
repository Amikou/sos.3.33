package defpackage;

/* compiled from: MultiImageTranscoderFactory.java */
/* renamed from: oa2  reason: default package */
/* loaded from: classes.dex */
public class oa2 implements bp1 {
    public final int a;
    public final boolean b;
    public final bp1 c;
    public final Integer d;
    public final boolean e;

    public oa2(int i, boolean z, bp1 bp1Var, Integer num, boolean z2) {
        this.a = i;
        this.b = z;
        this.c = bp1Var;
        this.d = num;
        this.e = z2;
    }

    public final ap1 a(wn1 wn1Var, boolean z) {
        bp1 bp1Var = this.c;
        if (bp1Var == null) {
            return null;
        }
        return bp1Var.createImageTranscoder(wn1Var, z);
    }

    public final ap1 b(wn1 wn1Var, boolean z) {
        Integer num = this.d;
        if (num == null) {
            return null;
        }
        int intValue = num.intValue();
        if (intValue != 0) {
            if (intValue == 1) {
                return d(wn1Var, z);
            }
            throw new IllegalArgumentException("Invalid ImageTranscoderType");
        }
        return c(wn1Var, z);
    }

    public final ap1 c(wn1 wn1Var, boolean z) {
        return nd2.a(this.a, this.b, this.e).createImageTranscoder(wn1Var, z);
    }

    @Override // defpackage.bp1
    public ap1 createImageTranscoder(wn1 wn1Var, boolean z) {
        ap1 a = a(wn1Var, z);
        if (a == null) {
            a = b(wn1Var, z);
        }
        if (a == null && ld2.a()) {
            a = c(wn1Var, z);
        }
        return a == null ? d(wn1Var, z) : a;
    }

    public final ap1 d(wn1 wn1Var, boolean z) {
        return new ip3(this.a).createImageTranscoder(wn1Var, z);
    }
}
