package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.m;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.a0;
import com.google.crypto.tink.proto.b0;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: KmsAeadKeyManager.java */
/* renamed from: tx1  reason: default package */
/* loaded from: classes2.dex */
public class tx1 extends g<a0> {

    /* compiled from: KmsAeadKeyManager.java */
    /* renamed from: tx1$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, a0> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(a0 a0Var) throws GeneralSecurityException {
            String E = a0Var.G().E();
            return m.a(E).b(E);
        }
    }

    /* compiled from: KmsAeadKeyManager.java */
    /* renamed from: tx1$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<b0, a0> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public a0 a(b0 b0Var) throws GeneralSecurityException {
            return a0.I().s(b0Var).t(tx1.this.j()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public b0 c(ByteString byteString) throws InvalidProtocolBufferException {
            return b0.G(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(b0 b0Var) throws GeneralSecurityException {
        }
    }

    public tx1() {
        super(a0.class, new a(com.google.crypto.tink.a.class));
    }

    public static void l(boolean z) throws GeneralSecurityException {
        q.q(new tx1(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.KmsAeadKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, a0> e() {
        return new b(b0.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.REMOTE;
    }

    public int j() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: k */
    public a0 g(ByteString byteString) throws InvalidProtocolBufferException {
        return a0.J(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(a0 a0Var) throws GeneralSecurityException {
        ug4.c(a0Var.H(), j());
    }
}
