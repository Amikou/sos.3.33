package defpackage;

import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.LinearLayoutCompat;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.editText.autoSize.AutofitEdittext;

/* compiled from: FragmentCryptoPriceAlertBinding.java */
/* renamed from: z91  reason: default package */
/* loaded from: classes2.dex */
public final class z91 {
    public final MaterialButton a;
    public final xp1 b;
    public final AutofitEdittext c;
    public final xp1 d;
    public final ShapeableImageView e;
    public final ShapeableImageView f;
    public final ShapeableImageView g;
    public final ShapeableImageView h;
    public final xp1 i;
    public final LinearLayoutCompat j;
    public final SwitchMaterial k;
    public final SwitchMaterial l;
    public final MaterialTextView m;

    public z91(MaterialCardView materialCardView, MaterialButton materialButton, xp1 xp1Var, AutofitEdittext autofitEdittext, xp1 xp1Var2, ShapeableImageView shapeableImageView, ShapeableImageView shapeableImageView2, ShapeableImageView shapeableImageView3, ShapeableImageView shapeableImageView4, xp1 xp1Var3, LinearLayoutCompat linearLayoutCompat, HorizontalScrollView horizontalScrollView, SwitchMaterial switchMaterial, MaterialTextView materialTextView, SwitchMaterial switchMaterial2, MaterialTextView materialTextView2, LinearLayout linearLayout) {
        this.a = materialButton;
        this.b = xp1Var;
        this.c = autofitEdittext;
        this.d = xp1Var2;
        this.e = shapeableImageView;
        this.f = shapeableImageView2;
        this.g = shapeableImageView3;
        this.h = shapeableImageView4;
        this.i = xp1Var3;
        this.j = linearLayoutCompat;
        this.k = switchMaterial;
        this.l = switchMaterial2;
        this.m = materialTextView2;
    }

    public static z91 a(View view) {
        int i = R.id.clkConfirm;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.clkConfirm);
        if (materialButton != null) {
            i = R.id.decreaseOf;
            View a = ai4.a(view, R.id.decreaseOf);
            if (a != null) {
                xp1 a2 = xp1.a(a);
                i = R.id.edtMaximum;
                AutofitEdittext autofitEdittext = (AutofitEdittext) ai4.a(view, R.id.edtMaximum);
                if (autofitEdittext != null) {
                    i = R.id.equalOf;
                    View a3 = ai4.a(view, R.id.equalOf);
                    if (a3 != null) {
                        xp1 a4 = xp1.a(a3);
                        i = R.id.imgClose;
                        ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.imgClose);
                        if (shapeableImageView != null) {
                            i = R.id.imgCpAdd;
                            ShapeableImageView shapeableImageView2 = (ShapeableImageView) ai4.a(view, R.id.imgCpAdd);
                            if (shapeableImageView2 != null) {
                                i = R.id.imgCpDelete;
                                ShapeableImageView shapeableImageView3 = (ShapeableImageView) ai4.a(view, R.id.imgCpDelete);
                                if (shapeableImageView3 != null) {
                                    i = R.id.imgTokenThumb;
                                    ShapeableImageView shapeableImageView4 = (ShapeableImageView) ai4.a(view, R.id.imgTokenThumb);
                                    if (shapeableImageView4 != null) {
                                        i = R.id.increaseOf;
                                        View a5 = ai4.a(view, R.id.increaseOf);
                                        if (a5 != null) {
                                            xp1 a6 = xp1.a(a5);
                                            i = R.id.parentOfNavigator;
                                            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.parentOfNavigator);
                                            if (linearLayoutCompat != null) {
                                                i = R.id.scrollOfNavigator;
                                                HorizontalScrollView horizontalScrollView = (HorizontalScrollView) ai4.a(view, R.id.scrollOfNavigator);
                                                if (horizontalScrollView != null) {
                                                    i = R.id.switchPersistent;
                                                    SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switchPersistent);
                                                    if (switchMaterial != null) {
                                                        i = R.id.titleAlertView;
                                                        MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.titleAlertView);
                                                        if (materialTextView != null) {
                                                            i = R.id.titleSwitch;
                                                            SwitchMaterial switchMaterial2 = (SwitchMaterial) ai4.a(view, R.id.titleSwitch);
                                                            if (switchMaterial2 != null) {
                                                                i = R.id.txtMaximumHint;
                                                                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtMaximumHint);
                                                                if (materialTextView2 != null) {
                                                                    i = R.id.wrapperTxt;
                                                                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.wrapperTxt);
                                                                    if (linearLayout != null) {
                                                                        return new z91((MaterialCardView) view, materialButton, a2, autofitEdittext, a4, shapeableImageView, shapeableImageView2, shapeableImageView3, shapeableImageView4, a6, linearLayoutCompat, horizontalScrollView, switchMaterial, materialTextView, switchMaterial2, materialTextView2, linearLayout);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
