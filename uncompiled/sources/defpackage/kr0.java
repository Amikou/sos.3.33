package defpackage;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import com.facebook.drawee.components.DraweeEventTracker;
import defpackage.jr0;

/* compiled from: DraweeHolder.java */
/* renamed from: kr0  reason: default package */
/* loaded from: classes.dex */
public class kr0<DH extends jr0> implements zk4 {
    public DH d;
    public boolean a = false;
    public boolean b = false;
    public boolean c = true;
    public ir0 e = null;
    public final DraweeEventTracker f = DraweeEventTracker.a();

    public kr0(DH dh) {
        if (dh != null) {
            p(dh);
        }
    }

    public static <DH extends jr0> kr0<DH> e(DH dh, Context context) {
        kr0<DH> kr0Var = new kr0<>(dh);
        kr0Var.n(context);
        return kr0Var;
    }

    @Override // defpackage.zk4
    public void a() {
        if (this.a) {
            return;
        }
        v11.v(DraweeEventTracker.class, "%x: Draw requested for a non-attached controller %x. %s", Integer.valueOf(System.identityHashCode(this)), Integer.valueOf(System.identityHashCode(this.e)), toString());
        this.b = true;
        this.c = true;
        d();
    }

    @Override // defpackage.zk4
    public void b(boolean z) {
        if (this.c == z) {
            return;
        }
        this.f.b(z ? DraweeEventTracker.Event.ON_DRAWABLE_SHOW : DraweeEventTracker.Event.ON_DRAWABLE_HIDE);
        this.c = z;
        d();
    }

    public final void c() {
        if (this.a) {
            return;
        }
        this.f.b(DraweeEventTracker.Event.ON_ATTACH_CONTROLLER);
        this.a = true;
        ir0 ir0Var = this.e;
        if (ir0Var == null || ir0Var.d() == null) {
            return;
        }
        this.e.g();
    }

    public final void d() {
        if (this.b && this.c) {
            c();
        } else {
            f();
        }
    }

    public final void f() {
        if (this.a) {
            this.f.b(DraweeEventTracker.Event.ON_DETACH_CONTROLLER);
            this.a = false;
            if (j()) {
                this.e.c();
            }
        }
    }

    public ir0 g() {
        return this.e;
    }

    public DH h() {
        return (DH) xt2.g(this.d);
    }

    public Drawable i() {
        DH dh = this.d;
        if (dh == null) {
            return null;
        }
        return dh.f();
    }

    public boolean j() {
        ir0 ir0Var = this.e;
        return ir0Var != null && ir0Var.d() == this.d;
    }

    public void k() {
        this.f.b(DraweeEventTracker.Event.ON_HOLDER_ATTACH);
        this.b = true;
        d();
    }

    public void l() {
        this.f.b(DraweeEventTracker.Event.ON_HOLDER_DETACH);
        this.b = false;
        d();
    }

    public boolean m(MotionEvent motionEvent) {
        if (j()) {
            return this.e.b(motionEvent);
        }
        return false;
    }

    public void n(Context context) {
    }

    public void o(ir0 ir0Var) {
        boolean z = this.a;
        if (z) {
            f();
        }
        if (j()) {
            this.f.b(DraweeEventTracker.Event.ON_CLEAR_OLD_CONTROLLER);
            this.e.e(null);
        }
        this.e = ir0Var;
        if (ir0Var != null) {
            this.f.b(DraweeEventTracker.Event.ON_SET_CONTROLLER);
            this.e.e(this.d);
        } else {
            this.f.b(DraweeEventTracker.Event.ON_CLEAR_CONTROLLER);
        }
        if (z) {
            c();
        }
    }

    public void p(DH dh) {
        this.f.b(DraweeEventTracker.Event.ON_SET_HIERARCHY);
        boolean j = j();
        q(null);
        DH dh2 = (DH) xt2.g(dh);
        this.d = dh2;
        Drawable f = dh2.f();
        b(f == null || f.isVisible());
        q(this);
        if (j) {
            this.e.e(dh);
        }
    }

    public final void q(zk4 zk4Var) {
        Drawable i = i();
        if (i instanceof yk4) {
            ((yk4) i).k(zk4Var);
        }
    }

    public String toString() {
        return ol2.c(this).c("controllerAttached", this.a).c("holderAttached", this.b).c("drawableVisible", this.c).b("events", this.f.toString()).toString();
    }
}
