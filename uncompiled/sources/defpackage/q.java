package defpackage;

/* compiled from: AKTCoin.java */
/* renamed from: q  reason: default package */
/* loaded from: classes.dex */
public class q {
    static {
        new zl(1);
        new zl(0);
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02x", Byte.valueOf(bArr[i])));
        }
        return sb.toString();
    }

    public static String b(String str) {
        zl zlVar = new zl(1);
        zl zlVar2 = new zl(2);
        String substring = str.substring(2, (str.length() / 2) + 1);
        String substring2 = str.substring(66);
        i(str);
        zl h = h(substring2);
        h.p(zlVar2);
        if (zl.d(h, zlVar) == 0) {
            return "03" + substring;
        }
        return "02" + substring;
    }

    public static dz4 c(String str, String str2) {
        dz4 dz4Var = new dz4();
        dz4Var.p(str2);
        dz4Var.q(str);
        dz4Var.o();
        return dz4Var;
    }

    public static String d(String str, String str2) {
        byte[] i = i(str);
        bz4 bz4Var = new bz4(false);
        bz4Var.g(i);
        return bz4Var.b(str2);
    }

    public static String e(String str, String str2) {
        byte[] i = i(str);
        bz4 bz4Var = new bz4(false);
        bz4Var.g(i);
        return bz4Var.d(str2);
    }

    public static String f(String str, String str2) {
        String a = lm.a(str);
        dz4 dz4Var = new dz4();
        dz4Var.t(a);
        dz4Var.m(true);
        String b = dz4Var.b(str2);
        return b.length() != 32 ? "error" : ay1.g(b);
    }

    public static String g(String str) {
        String substring = str.substring(0, 2);
        nt0 nt0Var = new nt0(h(str.substring(2, 66)));
        if (substring.compareTo("03") == 0) {
            nt0Var.m();
        }
        zl i = nt0Var.i();
        zl j = nt0Var.j();
        return "04" + i.toString() + j.toString();
    }

    public static zl h(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        if (length < 64) {
            int i = 64 - length;
            for (int i2 = 0; i2 < i; i2 += 2) {
                stringBuffer.append("00");
            }
            str = String.valueOf(stringBuffer.toString()) + str;
            length = str.length();
        }
        byte[] bArr = new byte[length / 2];
        for (int i3 = 0; i3 < length; i3 += 2) {
            bArr[i3 / 2] = (byte) ((Character.digit(str.charAt(i3), 16) << 4) + Character.digit(str.charAt(i3 + 1), 16));
        }
        return zl.i(bArr, 0);
    }

    public static byte[] i(String str) {
        int length = str.length();
        byte[] bArr = new byte[length / 2];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }

    public static String j(String str, String str2, String str3) {
        return d(l(str, str2), str3);
    }

    public static String k(String str, String str2, String str3) {
        return e(l(str, str2), str3);
    }

    public static String l(String str, String str2) {
        nt0 nt0Var = new nt0(zl.h(i(str2.substring(2, (str2.length() / 2) + 1))), zl.h(i(str2.substring((str2.length() / 2) + 1))));
        new nt0();
        nt0 d = nt0Var.d(zl.h(i(str)), new zl(f33.e));
        String zlVar = d.i().toString();
        String str3 = String.valueOf(zlVar) + d.j().toString();
        new la3();
        return a(la3.a(ay1.a(str3)));
    }

    public static String m(String str) {
        nt0 h = nt0.h();
        new nt0();
        nt0 d = h.d(zl.h(i(str)), new zl(f33.e));
        zl i = d.i();
        zl j = d.j();
        String zlVar = i.toString();
        String zlVar2 = j.toString();
        return "04" + zlVar + zlVar2;
    }
}
