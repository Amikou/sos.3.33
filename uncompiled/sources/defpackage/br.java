package defpackage;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: BoltsExecutors.java */
/* renamed from: br  reason: default package */
/* loaded from: classes.dex */
public final class br {
    public static final br c = new br();
    public final ExecutorService a;
    public final Executor b;

    /* compiled from: BoltsExecutors.java */
    /* renamed from: br$b */
    /* loaded from: classes.dex */
    public static class b implements Executor {
        public ThreadLocal<Integer> a;

        public b() {
            this.a = new ThreadLocal<>();
        }

        public final int a() {
            Integer num = this.a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.a.remove();
            } else {
                this.a.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        public final int b() {
            Integer num = this.a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.a.set(Integer.valueOf(intValue));
            return intValue;
        }

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            try {
                if (b() <= 15) {
                    runnable.run();
                } else {
                    br.a().execute(runnable);
                }
            } finally {
                a();
            }
        }
    }

    public br() {
        this.a = !c() ? Executors.newCachedThreadPool() : uc.b();
        Executors.newSingleThreadScheduledExecutor();
        this.b = new b();
    }

    public static ExecutorService a() {
        return c.a;
    }

    public static Executor b() {
        return c.b;
    }

    public static boolean c() {
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(Locale.US).contains("android");
    }
}
