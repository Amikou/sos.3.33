package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: tf  reason: default package */
/* loaded from: classes.dex */
public class tf {
    public final wf5 a;

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
    /* renamed from: tf$a */
    /* loaded from: classes.dex */
    public interface a extends em5 {
    }

    public tf(wf5 wf5Var) {
        this.a = wf5Var;
    }

    public void a(String str, String str2, Bundle bundle) {
        this.a.w(str, str2, bundle);
    }

    public void b(a aVar) {
        this.a.u(aVar);
    }

    public void c(String str, String str2, Object obj) {
        this.a.x(str, str2, obj, true);
    }

    public final void d(boolean z) {
        this.a.e(z);
    }
}
