package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: ef3  reason: default package */
/* loaded from: classes2.dex */
public class ef3 extends xs0.c {
    public static final BigInteger j = new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F"));
    public hf3 i;

    /* renamed from: ef3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ int[] b;

        public a(int i, int[] iArr) {
            this.a = i;
            this.b = iArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            int[] h = ed2.h();
            int[] h2 = ed2.h();
            int i2 = 0;
            for (int i3 = 0; i3 < this.a; i3++) {
                int i4 = ((i3 ^ i) - 1) >> 31;
                for (int i5 = 0; i5 < 8; i5++) {
                    int i6 = h[i5];
                    int[] iArr = this.b;
                    h[i5] = i6 ^ (iArr[i2 + i5] & i4);
                    h2[i5] = h2[i5] ^ (iArr[(i2 + 8) + i5] & i4);
                }
                i2 += 16;
            }
            return ef3.this.i(new gf3(h), new gf3(h2), false);
        }
    }

    public ef3() {
        super(j);
        this.i = new hf3(this, null, null);
        this.b = n(ws0.a);
        this.c = n(BigInteger.valueOf(7L));
        this.d = new BigInteger(1, pk1.a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141"));
        this.e = BigInteger.valueOf(1L);
        this.f = 2;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 2;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new ef3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        int[] iArr = new int[i2 * 8 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            ed2.f(((gf3) pt0Var.n()).f, 0, iArr, i3);
            int i5 = i3 + 8;
            ed2.f(((gf3) pt0Var.o()).f, 0, iArr, i5);
            i3 = i5 + 8;
        }
        return new a(i2, iArr);
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new hf3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new hf3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new gf3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return j.bitLength();
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.i;
    }
}
