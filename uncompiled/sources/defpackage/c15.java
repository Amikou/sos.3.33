package defpackage;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: c15  reason: default package */
/* loaded from: classes.dex */
public final class c15 implements Comparator<Scope> {
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(Scope scope, Scope scope2) {
        return scope.I1().compareTo(scope2.I1());
    }
}
