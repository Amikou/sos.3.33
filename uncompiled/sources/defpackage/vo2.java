package defpackage;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

/* compiled from: PackageUtils.java */
/* renamed from: vo2  reason: default package */
/* loaded from: classes.dex */
public class vo2 {

    /* compiled from: PackageUtils.java */
    /* renamed from: vo2$a */
    /* loaded from: classes.dex */
    public static class a {
        public static boolean a(PackageManager packageManager) {
            return packageManager.hasSystemFeature("android.hardware.fingerprint");
        }
    }

    public static boolean a(Context context) {
        return Build.VERSION.SDK_INT >= 23 && context != null && context.getPackageManager() != null && a.a(context.getPackageManager());
    }
}
