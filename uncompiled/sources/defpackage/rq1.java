package defpackage;

import android.content.Context;
import java.util.List;

/* compiled from: Initializer.java */
/* renamed from: rq1  reason: default package */
/* loaded from: classes.dex */
public interface rq1<T> {
    List<Class<? extends rq1<?>>> a();

    T b(Context context);
}
