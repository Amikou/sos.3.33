package defpackage;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Objects;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.e;
import retrofit2.o;

/* compiled from: GsonConverterFactory.java */
/* renamed from: yi1  reason: default package */
/* loaded from: classes3.dex */
public final class yi1 extends e.a {
    public final Gson a;

    public yi1(Gson gson) {
        this.a = gson;
    }

    public static yi1 f() {
        return g(new Gson());
    }

    public static yi1 g(Gson gson) {
        Objects.requireNonNull(gson, "gson == null");
        return new yi1(gson);
    }

    @Override // retrofit2.e.a
    public e<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, o oVar) {
        return new zi1(this.a, this.a.getAdapter(TypeToken.get(type)));
    }

    @Override // retrofit2.e.a
    public e<ResponseBody, ?> d(Type type, Annotation[] annotationArr, o oVar) {
        return new aj1(this.a, this.a.getAdapter(TypeToken.get(type)));
    }
}
