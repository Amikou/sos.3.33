package defpackage;

import java.math.BigInteger;

/* renamed from: pd0  reason: default package */
/* loaded from: classes2.dex */
public class pd0 implements ty {
    public BigInteger a;
    public BigInteger f0;
    public BigInteger g0;
    public qd0 h0;

    public pd0(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, qd0 qd0Var) {
        this.a = bigInteger3;
        this.g0 = bigInteger;
        this.f0 = bigInteger2;
        this.h0 = qd0Var;
    }

    public BigInteger a() {
        return this.a;
    }

    public BigInteger b() {
        return this.g0;
    }

    public BigInteger c() {
        return this.f0;
    }

    public qd0 d() {
        return this.h0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof pd0) {
            pd0 pd0Var = (pd0) obj;
            return pd0Var.b().equals(this.g0) && pd0Var.c().equals(this.f0) && pd0Var.a().equals(this.a);
        }
        return false;
    }

    public int hashCode() {
        return (b().hashCode() ^ c().hashCode()) ^ a().hashCode();
    }
}
