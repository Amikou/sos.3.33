package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: n26  reason: default package */
/* loaded from: classes.dex */
public final class n26 implements m26 {
    public static final wo5<Boolean> a;
    public static final wo5<Boolean> b;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.b("measurement.sdk.screen.manual_screen_view_logging", true);
        b = ro5Var.b("measurement.sdk.screen.disabling_automatic_reporting", true);
    }

    @Override // defpackage.m26
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.m26
    public final boolean zzb() {
        return a.e().booleanValue();
    }

    @Override // defpackage.m26
    public final boolean zzc() {
        return b.e().booleanValue();
    }
}
