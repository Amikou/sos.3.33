package defpackage;

import androidx.media3.common.u;

/* compiled from: ForwardingTimeline.java */
/* renamed from: l91  reason: default package */
/* loaded from: classes.dex */
public abstract class l91 extends u {
    public final u f0;

    public l91(u uVar) {
        this.f0 = uVar;
    }

    @Override // androidx.media3.common.u
    public int a(boolean z) {
        return this.f0.a(z);
    }

    @Override // androidx.media3.common.u
    public int b(Object obj) {
        return this.f0.b(obj);
    }

    @Override // androidx.media3.common.u
    public int c(boolean z) {
        return this.f0.c(z);
    }

    @Override // androidx.media3.common.u
    public int e(int i, int i2, boolean z) {
        return this.f0.e(i, i2, z);
    }

    @Override // androidx.media3.common.u
    public u.b g(int i, u.b bVar, boolean z) {
        return this.f0.g(i, bVar, z);
    }

    @Override // androidx.media3.common.u
    public int i() {
        return this.f0.i();
    }

    @Override // androidx.media3.common.u
    public int l(int i, int i2, boolean z) {
        return this.f0.l(i, i2, z);
    }

    @Override // androidx.media3.common.u
    public Object m(int i) {
        return this.f0.m(i);
    }

    @Override // androidx.media3.common.u
    public u.c o(int i, u.c cVar, long j) {
        return this.f0.o(i, cVar, j);
    }

    @Override // androidx.media3.common.u
    public int p() {
        return this.f0.p();
    }
}
