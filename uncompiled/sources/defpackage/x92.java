package defpackage;

import android.view.View;
import androidx.constraintlayout.motion.widget.a;
import androidx.constraintlayout.motion.widget.d;
import androidx.constraintlayout.widget.ConstraintAttribute;
import androidx.constraintlayout.widget.a;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;
import java.util.LinkedHashMap;

/* compiled from: MotionPaths.java */
/* renamed from: x92  reason: default package */
/* loaded from: classes.dex */
public class x92 implements Comparable<x92> {
    public static String[] v0 = {"position", "x", "y", "width", "height", "pathRotate"};
    public yt0 a;
    public float g0;
    public float h0;
    public float i0;
    public float j0;
    public float k0;
    public float l0;
    public int n0;
    public int o0;
    public float p0;
    public u92 q0;
    public LinkedHashMap<String, ConstraintAttribute> r0;
    public int s0;
    public double[] t0;
    public double[] u0;
    public int f0 = 0;
    public float m0 = Float.NaN;

    public x92() {
        int i = a.f;
        this.n0 = i;
        this.o0 = i;
        this.p0 = Float.NaN;
        this.q0 = null;
        this.r0 = new LinkedHashMap<>();
        this.s0 = 0;
        this.t0 = new double[18];
        this.u0 = new double[18];
    }

    public void A(u92 u92Var, x92 x92Var) {
        double d = ((this.i0 + (this.k0 / 2.0f)) - x92Var.i0) - (x92Var.k0 / 2.0f);
        double d2 = ((this.j0 + (this.l0 / 2.0f)) - x92Var.j0) - (x92Var.l0 / 2.0f);
        this.q0 = u92Var;
        this.i0 = (float) Math.hypot(d2, d);
        if (Float.isNaN(this.p0)) {
            this.j0 = (float) (Math.atan2(d2, d) + 1.5707963267948966d);
        } else {
            this.j0 = (float) Math.toRadians(this.p0);
        }
    }

    public void a(a.C0021a c0021a) {
        this.a = yt0.c(c0021a.d.d);
        a.c cVar = c0021a.d;
        this.n0 = cVar.e;
        this.o0 = cVar.b;
        this.m0 = cVar.i;
        this.f0 = cVar.f;
        int i = cVar.c;
        float f = c0021a.c.e;
        this.p0 = c0021a.e.B;
        for (String str : c0021a.g.keySet()) {
            ConstraintAttribute constraintAttribute = c0021a.g.get(str);
            if (constraintAttribute != null && constraintAttribute.g()) {
                this.r0.put(str, constraintAttribute);
            }
        }
    }

    @Override // java.lang.Comparable
    /* renamed from: d */
    public int compareTo(x92 x92Var) {
        return Float.compare(this.h0, x92Var.h0);
    }

    public final boolean e(float f, float f2) {
        return (Float.isNaN(f) || Float.isNaN(f2)) ? Float.isNaN(f) != Float.isNaN(f2) : Math.abs(f - f2) > 1.0E-6f;
    }

    public void f(x92 x92Var, boolean[] zArr, String[] strArr, boolean z) {
        boolean e = e(this.i0, x92Var.i0);
        boolean e2 = e(this.j0, x92Var.j0);
        zArr[0] = zArr[0] | e(this.h0, x92Var.h0);
        boolean z2 = e | e2 | z;
        zArr[1] = zArr[1] | z2;
        zArr[2] = z2 | zArr[2];
        zArr[3] = zArr[3] | e(this.k0, x92Var.k0);
        zArr[4] = e(this.l0, x92Var.l0) | zArr[4];
    }

    public void g(double[] dArr, int[] iArr) {
        float[] fArr = {this.h0, this.i0, this.j0, this.k0, this.l0, this.m0};
        int i = 0;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (iArr[i2] < 6) {
                dArr[i] = fArr[iArr[i2]];
                i++;
            }
        }
    }

    public void h(double d, int[] iArr, double[] dArr, float[] fArr, int i) {
        float f = this.i0;
        float f2 = this.j0;
        float f3 = this.k0;
        float f4 = this.l0;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            float f5 = (float) dArr[i2];
            int i3 = iArr[i2];
            if (i3 == 1) {
                f = f5;
            } else if (i3 == 2) {
                f2 = f5;
            } else if (i3 == 3) {
                f3 = f5;
            } else if (i3 == 4) {
                f4 = f5;
            }
        }
        u92 u92Var = this.q0;
        if (u92Var != null) {
            float[] fArr2 = new float[2];
            u92Var.i(d, fArr2, new float[2]);
            float f6 = fArr2[0];
            float f7 = fArr2[1];
            double d2 = f6;
            double d3 = f;
            double d4 = f2;
            f = (float) ((d2 + (Math.sin(d4) * d3)) - (f3 / 2.0f));
            f2 = (float) ((f7 - (d3 * Math.cos(d4))) - (f4 / 2.0f));
        }
        fArr[i] = f + (f3 / 2.0f) + Utils.FLOAT_EPSILON;
        fArr[i + 1] = f2 + (f4 / 2.0f) + Utils.FLOAT_EPSILON;
    }

    public void j(double d, int[] iArr, double[] dArr, float[] fArr, double[] dArr2, float[] fArr2) {
        float f;
        float f2 = this.i0;
        float f3 = this.j0;
        float f4 = this.k0;
        float f5 = this.l0;
        float f6 = Utils.FLOAT_EPSILON;
        float f7 = Utils.FLOAT_EPSILON;
        float f8 = Utils.FLOAT_EPSILON;
        float f9 = Utils.FLOAT_EPSILON;
        for (int i = 0; i < iArr.length; i++) {
            float f10 = (float) dArr[i];
            float f11 = (float) dArr2[i];
            int i2 = iArr[i];
            if (i2 == 1) {
                f2 = f10;
                f6 = f11;
            } else if (i2 == 2) {
                f3 = f10;
                f8 = f11;
            } else if (i2 == 3) {
                f4 = f10;
                f7 = f11;
            } else if (i2 == 4) {
                f5 = f10;
                f9 = f11;
            }
        }
        float f12 = 2.0f;
        float f13 = (f7 / 2.0f) + f6;
        float f14 = (f9 / 2.0f) + f8;
        u92 u92Var = this.q0;
        if (u92Var != null) {
            float[] fArr3 = new float[2];
            float[] fArr4 = new float[2];
            u92Var.i(d, fArr3, fArr4);
            float f15 = fArr3[0];
            float f16 = fArr3[1];
            float f17 = fArr4[0];
            float f18 = fArr4[1];
            double d2 = f2;
            double d3 = f3;
            f = f4;
            double d4 = f6;
            double d5 = f8;
            float sin = (float) (f17 + (Math.sin(d3) * d4) + (Math.cos(d3) * d5));
            f14 = (float) ((f18 - (d4 * Math.cos(d3))) + (Math.sin(d3) * d5));
            f13 = sin;
            f2 = (float) ((f15 + (Math.sin(d3) * d2)) - (f4 / 2.0f));
            f3 = (float) ((f16 - (d2 * Math.cos(d3))) - (f5 / 2.0f));
            f12 = 2.0f;
        } else {
            f = f4;
        }
        fArr[0] = f2 + (f / f12) + Utils.FLOAT_EPSILON;
        fArr[1] = f3 + (f5 / f12) + Utils.FLOAT_EPSILON;
        fArr2[0] = f13;
        fArr2[1] = f14;
    }

    public int k(String str, double[] dArr, int i) {
        ConstraintAttribute constraintAttribute = this.r0.get(str);
        int i2 = 0;
        if (constraintAttribute == null) {
            return 0;
        }
        if (constraintAttribute.h() == 1) {
            dArr[i] = constraintAttribute.e();
            return 1;
        }
        int h = constraintAttribute.h();
        float[] fArr = new float[h];
        constraintAttribute.f(fArr);
        while (i2 < h) {
            dArr[i] = fArr[i2];
            i2++;
            i++;
        }
        return h;
    }

    public int l(String str) {
        ConstraintAttribute constraintAttribute = this.r0.get(str);
        if (constraintAttribute == null) {
            return 0;
        }
        return constraintAttribute.h();
    }

    public void o(int[] iArr, double[] dArr, float[] fArr, int i) {
        float f = this.i0;
        float f2 = this.j0;
        float f3 = this.k0;
        float f4 = this.l0;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            float f5 = (float) dArr[i2];
            int i3 = iArr[i2];
            if (i3 == 1) {
                f = f5;
            } else if (i3 == 2) {
                f2 = f5;
            } else if (i3 == 3) {
                f3 = f5;
            } else if (i3 == 4) {
                f4 = f5;
            }
        }
        u92 u92Var = this.q0;
        if (u92Var != null) {
            float j = u92Var.j();
            double d = f;
            double d2 = f2;
            f2 = (float) ((this.q0.k() - (d * Math.cos(d2))) - (f4 / 2.0f));
            f = (float) ((j + (Math.sin(d2) * d)) - (f3 / 2.0f));
        }
        float f6 = f3 + f;
        float f7 = f4 + f2;
        Float.isNaN(Float.NaN);
        Float.isNaN(Float.NaN);
        float f8 = f + Utils.FLOAT_EPSILON;
        float f9 = f2 + Utils.FLOAT_EPSILON;
        float f10 = f6 + Utils.FLOAT_EPSILON;
        float f11 = f2 + Utils.FLOAT_EPSILON;
        float f12 = f6 + Utils.FLOAT_EPSILON;
        float f13 = f7 + Utils.FLOAT_EPSILON;
        float f14 = f + Utils.FLOAT_EPSILON;
        float f15 = f7 + Utils.FLOAT_EPSILON;
        int i4 = i + 1;
        fArr[i] = f8;
        int i5 = i4 + 1;
        fArr[i4] = f9;
        int i6 = i5 + 1;
        fArr[i5] = f10;
        int i7 = i6 + 1;
        fArr[i6] = f11;
        int i8 = i7 + 1;
        fArr[i7] = f12;
        int i9 = i8 + 1;
        fArr[i8] = f13;
        fArr[i9] = f14;
        fArr[i9 + 1] = f15;
    }

    public boolean p(String str) {
        return this.r0.containsKey(str);
    }

    public void r(d dVar, x92 x92Var, x92 x92Var2) {
        float f = dVar.a / 100.0f;
        this.g0 = f;
        this.f0 = dVar.j;
        float f2 = Float.isNaN(dVar.k) ? f : dVar.k;
        float f3 = Float.isNaN(dVar.l) ? f : dVar.l;
        float f4 = x92Var2.k0;
        float f5 = x92Var.k0;
        float f6 = x92Var2.l0;
        float f7 = x92Var.l0;
        this.h0 = this.g0;
        float f8 = x92Var.i0;
        float f9 = x92Var.j0;
        float f10 = (x92Var2.i0 + (f4 / 2.0f)) - ((f5 / 2.0f) + f8);
        float f11 = (x92Var2.j0 + (f6 / 2.0f)) - (f9 + (f7 / 2.0f));
        float f12 = (f4 - f5) * f2;
        float f13 = f12 / 2.0f;
        this.i0 = (int) ((f8 + (f10 * f)) - f13);
        float f14 = (f6 - f7) * f3;
        float f15 = f14 / 2.0f;
        this.j0 = (int) ((f9 + (f11 * f)) - f15);
        this.k0 = (int) (f5 + f12);
        this.l0 = (int) (f7 + f14);
        float f16 = Float.isNaN(dVar.m) ? f : dVar.m;
        boolean isNaN = Float.isNaN(dVar.p);
        float f17 = Utils.FLOAT_EPSILON;
        float f18 = isNaN ? 0.0f : dVar.p;
        if (!Float.isNaN(dVar.n)) {
            f = dVar.n;
        }
        if (!Float.isNaN(dVar.o)) {
            f17 = dVar.o;
        }
        this.s0 = 0;
        this.i0 = (int) (((x92Var.i0 + (f16 * f10)) + (f17 * f11)) - f13);
        this.j0 = (int) (((x92Var.j0 + (f10 * f18)) + (f11 * f)) - f15);
        this.a = yt0.c(dVar.h);
        this.n0 = dVar.i;
    }

    public void s(d dVar, x92 x92Var, x92 x92Var2) {
        float f;
        float f2;
        float f3 = dVar.a / 100.0f;
        this.g0 = f3;
        this.f0 = dVar.j;
        float f4 = Float.isNaN(dVar.k) ? f3 : dVar.k;
        float f5 = Float.isNaN(dVar.l) ? f3 : dVar.l;
        float f6 = x92Var2.k0 - x92Var.k0;
        float f7 = x92Var2.l0 - x92Var.l0;
        this.h0 = this.g0;
        if (!Float.isNaN(dVar.m)) {
            f3 = dVar.m;
        }
        float f8 = x92Var.i0;
        float f9 = x92Var.k0;
        float f10 = x92Var.j0;
        float f11 = x92Var.l0;
        float f12 = (x92Var2.i0 + (x92Var2.k0 / 2.0f)) - ((f9 / 2.0f) + f8);
        float f13 = (x92Var2.j0 + (x92Var2.l0 / 2.0f)) - ((f11 / 2.0f) + f10);
        float f14 = f12 * f3;
        float f15 = (f6 * f4) / 2.0f;
        this.i0 = (int) ((f8 + f14) - f15);
        float f16 = f3 * f13;
        float f17 = (f7 * f5) / 2.0f;
        this.j0 = (int) ((f10 + f16) - f17);
        this.k0 = (int) (f9 + f);
        this.l0 = (int) (f11 + f2);
        float f18 = Float.isNaN(dVar.n) ? Utils.FLOAT_EPSILON : dVar.n;
        this.s0 = 1;
        float f19 = (int) ((x92Var.i0 + f14) - f15);
        this.i0 = f19;
        float f20 = (int) ((x92Var.j0 + f16) - f17);
        this.j0 = f20;
        this.i0 = f19 + ((-f13) * f18);
        this.j0 = f20 + (f12 * f18);
        this.o0 = this.o0;
        this.a = yt0.c(dVar.h);
        this.n0 = dVar.i;
    }

    public void t(int i, int i2, d dVar, x92 x92Var, x92 x92Var2) {
        float min;
        float f;
        float f2 = dVar.a / 100.0f;
        this.g0 = f2;
        this.f0 = dVar.j;
        this.s0 = dVar.q;
        float f3 = Float.isNaN(dVar.k) ? f2 : dVar.k;
        float f4 = Float.isNaN(dVar.l) ? f2 : dVar.l;
        float f5 = x92Var2.k0;
        float f6 = x92Var.k0;
        float f7 = x92Var2.l0;
        float f8 = x92Var.l0;
        this.h0 = this.g0;
        this.k0 = (int) (f6 + ((f5 - f6) * f3));
        this.l0 = (int) (f8 + ((f7 - f8) * f4));
        int i3 = dVar.q;
        if (i3 == 1) {
            float f9 = Float.isNaN(dVar.m) ? f2 : dVar.m;
            float f10 = x92Var2.i0;
            float f11 = x92Var.i0;
            this.i0 = (f9 * (f10 - f11)) + f11;
            if (!Float.isNaN(dVar.n)) {
                f2 = dVar.n;
            }
            float f12 = x92Var2.j0;
            float f13 = x92Var.j0;
            this.j0 = (f2 * (f12 - f13)) + f13;
        } else if (i3 != 2) {
            float f14 = Float.isNaN(dVar.m) ? f2 : dVar.m;
            float f15 = x92Var2.i0;
            float f16 = x92Var.i0;
            this.i0 = (f14 * (f15 - f16)) + f16;
            if (!Float.isNaN(dVar.n)) {
                f2 = dVar.n;
            }
            float f17 = x92Var2.j0;
            float f18 = x92Var.j0;
            this.j0 = (f2 * (f17 - f18)) + f18;
        } else {
            if (Float.isNaN(dVar.m)) {
                float f19 = x92Var2.i0;
                float f20 = x92Var.i0;
                min = ((f19 - f20) * f2) + f20;
            } else {
                min = Math.min(f4, f3) * dVar.m;
            }
            this.i0 = min;
            if (Float.isNaN(dVar.n)) {
                float f21 = x92Var2.j0;
                float f22 = x92Var.j0;
                f = (f2 * (f21 - f22)) + f22;
            } else {
                f = dVar.n;
            }
            this.j0 = f;
        }
        this.o0 = x92Var.o0;
        this.a = yt0.c(dVar.h);
        this.n0 = dVar.i;
    }

    public void u(int i, int i2, d dVar, x92 x92Var, x92 x92Var2) {
        float f = dVar.a / 100.0f;
        this.g0 = f;
        this.f0 = dVar.j;
        float f2 = Float.isNaN(dVar.k) ? f : dVar.k;
        float f3 = Float.isNaN(dVar.l) ? f : dVar.l;
        float f4 = x92Var2.k0;
        float f5 = x92Var.k0;
        float f6 = x92Var2.l0;
        float f7 = x92Var.l0;
        this.h0 = this.g0;
        float f8 = x92Var.i0;
        float f9 = x92Var.j0;
        float f10 = x92Var2.i0 + (f4 / 2.0f);
        float f11 = x92Var2.j0 + (f6 / 2.0f);
        float f12 = (f4 - f5) * f2;
        this.i0 = (int) ((f8 + ((f10 - ((f5 / 2.0f) + f8)) * f)) - (f12 / 2.0f));
        float f13 = (f6 - f7) * f3;
        this.j0 = (int) ((f9 + ((f11 - (f9 + (f7 / 2.0f))) * f)) - (f13 / 2.0f));
        this.k0 = (int) (f5 + f12);
        this.l0 = (int) (f7 + f13);
        this.s0 = 2;
        if (!Float.isNaN(dVar.m)) {
            this.i0 = (int) (dVar.m * ((int) (i - this.k0)));
        }
        if (!Float.isNaN(dVar.n)) {
            this.j0 = (int) (dVar.n * ((int) (i2 - this.l0)));
        }
        this.o0 = this.o0;
        this.a = yt0.c(dVar.h);
        this.n0 = dVar.i;
    }

    public void v(float f, float f2, float f3, float f4) {
        this.i0 = f;
        this.j0 = f2;
        this.k0 = f3;
        this.l0 = f4;
    }

    public void x(float f, float f2, float[] fArr, int[] iArr, double[] dArr, double[] dArr2) {
        float f3 = 0.0f;
        float f4 = 0.0f;
        float f5 = 0.0f;
        float f6 = 0.0f;
        for (int i = 0; i < iArr.length; i++) {
            float f7 = (float) dArr[i];
            double d = dArr2[i];
            int i2 = iArr[i];
            if (i2 == 1) {
                f3 = f7;
            } else if (i2 == 2) {
                f5 = f7;
            } else if (i2 == 3) {
                f4 = f7;
            } else if (i2 == 4) {
                f6 = f7;
            }
        }
        float f8 = f3 - ((Utils.FLOAT_EPSILON * f4) / 2.0f);
        float f9 = f5 - ((Utils.FLOAT_EPSILON * f6) / 2.0f);
        fArr[0] = (f8 * (1.0f - f)) + (((f4 * 1.0f) + f8) * f) + Utils.FLOAT_EPSILON;
        fArr[1] = (f9 * (1.0f - f2)) + (((f6 * 1.0f) + f9) * f2) + Utils.FLOAT_EPSILON;
    }

    public void y(float f, View view, int[] iArr, double[] dArr, double[] dArr2, double[] dArr3) {
        float f2;
        boolean z;
        boolean z2;
        float f3;
        float f4 = this.i0;
        float f5 = this.j0;
        float f6 = this.k0;
        float f7 = this.l0;
        if (iArr.length != 0 && this.t0.length <= iArr[iArr.length - 1]) {
            int i = iArr[iArr.length - 1] + 1;
            this.t0 = new double[i];
            this.u0 = new double[i];
        }
        Arrays.fill(this.t0, Double.NaN);
        for (int i2 = 0; i2 < iArr.length; i2++) {
            this.t0[iArr[i2]] = dArr[i2];
            this.u0[iArr[i2]] = dArr2[i2];
        }
        float f8 = Float.NaN;
        int i3 = 0;
        float f9 = Utils.FLOAT_EPSILON;
        float f10 = Utils.FLOAT_EPSILON;
        float f11 = Utils.FLOAT_EPSILON;
        float f12 = Utils.FLOAT_EPSILON;
        while (true) {
            double[] dArr4 = this.t0;
            if (i3 >= dArr4.length) {
                break;
            }
            boolean isNaN = Double.isNaN(dArr4[i3]);
            double d = Utils.DOUBLE_EPSILON;
            if (isNaN && (dArr3 == null || dArr3[i3] == Utils.DOUBLE_EPSILON)) {
                f3 = f8;
            } else {
                if (dArr3 != null) {
                    d = dArr3[i3];
                }
                if (!Double.isNaN(this.t0[i3])) {
                    d = this.t0[i3] + d;
                }
                f3 = f8;
                float f13 = (float) d;
                float f14 = (float) this.u0[i3];
                if (i3 == 1) {
                    f8 = f3;
                    f9 = f14;
                    f4 = f13;
                } else if (i3 == 2) {
                    f8 = f3;
                    f10 = f14;
                    f5 = f13;
                } else if (i3 == 3) {
                    f8 = f3;
                    f11 = f14;
                    f6 = f13;
                } else if (i3 == 4) {
                    f8 = f3;
                    f12 = f14;
                    f7 = f13;
                } else if (i3 == 5) {
                    f8 = f13;
                }
                i3++;
            }
            f8 = f3;
            i3++;
        }
        float f15 = f8;
        u92 u92Var = this.q0;
        if (u92Var != null) {
            float[] fArr = new float[2];
            float[] fArr2 = new float[2];
            u92Var.i(f, fArr, fArr2);
            float f16 = fArr[0];
            float f17 = fArr[1];
            float f18 = fArr2[0];
            float f19 = fArr2[1];
            double d2 = f4;
            double d3 = f5;
            float sin = (float) ((f16 + (Math.sin(d3) * d2)) - (f6 / 2.0f));
            f2 = f7;
            float cos = (float) ((f17 - (Math.cos(d3) * d2)) - (f7 / 2.0f));
            double d4 = f9;
            double d5 = f10;
            float sin2 = (float) (f18 + (Math.sin(d3) * d4) + (Math.cos(d3) * d2 * d5));
            float cos2 = (float) ((f19 - (d4 * Math.cos(d3))) + (d2 * Math.sin(d3) * d5));
            if (dArr2.length >= 2) {
                z = false;
                dArr2[0] = sin2;
                z2 = true;
                dArr2[1] = cos2;
            } else {
                z = false;
                z2 = true;
            }
            if (!Float.isNaN(f15)) {
                view.setRotation((float) (f15 + Math.toDegrees(Math.atan2(cos2, sin2))));
            }
            f4 = sin;
            f5 = cos;
        } else {
            f2 = f7;
            z = false;
            z2 = true;
            if (!Float.isNaN(f15)) {
                view.setRotation((float) (((double) Utils.FLOAT_EPSILON) + f15 + Math.toDegrees(Math.atan2(f10 + (f12 / 2.0f), f9 + (f11 / 2.0f)))));
            }
        }
        if (view instanceof c71) {
            ((c71) view).a(f4, f5, f6 + f4, f5 + f2);
            return;
        }
        float f20 = f4 + 0.5f;
        int i4 = (int) f20;
        float f21 = f5 + 0.5f;
        int i5 = (int) f21;
        int i6 = (int) (f20 + f6);
        int i7 = (int) (f21 + f2);
        int i8 = i6 - i4;
        int i9 = i7 - i5;
        if (i8 != view.getMeasuredWidth() || i9 != view.getMeasuredHeight()) {
            z = z2;
        }
        if (z) {
            view.measure(View.MeasureSpec.makeMeasureSpec(i8, 1073741824), View.MeasureSpec.makeMeasureSpec(i9, 1073741824));
        }
        view.layout(i4, i5, i6, i7);
    }

    public x92(int i, int i2, d dVar, x92 x92Var, x92 x92Var2) {
        int i3 = androidx.constraintlayout.motion.widget.a.f;
        this.n0 = i3;
        this.o0 = i3;
        this.p0 = Float.NaN;
        this.q0 = null;
        this.r0 = new LinkedHashMap<>();
        this.s0 = 0;
        this.t0 = new double[18];
        this.u0 = new double[18];
        if (x92Var.o0 != androidx.constraintlayout.motion.widget.a.f) {
            t(i, i2, dVar, x92Var, x92Var2);
            return;
        }
        int i4 = dVar.q;
        if (i4 == 1) {
            s(dVar, x92Var, x92Var2);
        } else if (i4 != 2) {
            r(dVar, x92Var, x92Var2);
        } else {
            u(i, i2, dVar, x92Var, x92Var2);
        }
    }
}
