package defpackage;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.clearcut.p0;

/* renamed from: op5  reason: default package */
/* loaded from: classes.dex */
public final class op5 extends tk5 {
    public final /* synthetic */ p0 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public op5(p0 p0Var) {
        super(null);
        this.a = p0Var;
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void O0(Status status) {
        this.a.g(status);
    }
}
