package defpackage;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.c;

/* renamed from: ze5  reason: default package */
/* loaded from: classes.dex */
public final class ze5 extends c<d75> {
    public ze5(Context context, Looper looper, kz kzVar, GoogleApiClient.b bVar, GoogleApiClient.c cVar) {
        super(context, looper, 51, kzVar, bVar, cVar);
    }

    @Override // com.google.android.gms.common.internal.b
    public final String H() {
        return "com.google.android.gms.phenotype.internal.IPhenotypeService";
    }

    @Override // com.google.android.gms.common.internal.b
    public final String I() {
        return "com.google.android.gms.phenotype.service.START";
    }

    @Override // com.google.android.gms.common.internal.b, com.google.android.gms.common.api.a.f
    public final int q() {
        return 11925000;
    }

    @Override // com.google.android.gms.common.internal.b
    public final /* synthetic */ IInterface y(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.phenotype.internal.IPhenotypeService");
        return queryLocalInterface instanceof d75 ? (d75) queryLocalInterface : new oa5(iBinder);
    }
}
