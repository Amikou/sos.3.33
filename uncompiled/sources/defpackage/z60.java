package defpackage;

import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;

/* compiled from: ContactSearchAddressAdapter.kt */
/* renamed from: z60  reason: default package */
/* loaded from: classes2.dex */
public final class z60 extends RecyclerView.Adapter<b> {
    public final List<IContact> a;
    public final EditText b;
    public final sl1 c;
    public final List<IContact> d;
    public xs1 e;

    /* compiled from: ContactSearchAddressAdapter.kt */
    /* renamed from: z60$a */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        /* JADX WARN: Code restructure failed: missing block: B:12:0x0025, code lost:
            if ((r3.length() == 0) == true) goto L4;
         */
        @Override // android.text.TextWatcher
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public void afterTextChanged(android.text.Editable r11) {
            /*
                r10 = this;
                z60 r0 = defpackage.z60.this
                java.util.List r0 = defpackage.z60.b(r0)
                r0.clear()
                z60 r0 = defpackage.z60.this
                java.util.List r0 = defpackage.z60.b(r0)
                r1 = 1
                r2 = 0
                if (r11 != 0) goto L15
            L13:
                r1 = r2
                goto L27
            L15:
                java.lang.CharSequence r3 = kotlin.text.StringsKt__StringsKt.K0(r11)
                if (r3 != 0) goto L1c
                goto L13
            L1c:
                int r3 = r3.length()
                if (r3 != 0) goto L24
                r3 = r1
                goto L25
            L24:
                r3 = r2
            L25:
                if (r3 != r1) goto L13
            L27:
                if (r1 == 0) goto L30
                z60 r11 = defpackage.z60.this
                java.util.List r11 = defpackage.z60.c(r11)
                goto L80
            L30:
                z60 r1 = defpackage.z60.this
                java.util.List r1 = defpackage.z60.c(r1)
                java.util.ArrayList r3 = new java.util.ArrayList
                r3.<init>()
                java.util.Iterator r1 = r1.iterator()
            L3f:
                boolean r4 = r1.hasNext()
                if (r4 == 0) goto L7f
                java.lang.Object r4 = r1.next()
                r5 = r4
                net.safemoon.androidwallet.model.contact.abstraction.IContact r5 = (net.safemoon.androidwallet.model.contact.abstraction.IContact) r5
                java.lang.String r5 = r5.getName()
                java.lang.String r6 = "null cannot be cast to non-null type java.lang.String"
                java.util.Objects.requireNonNull(r5, r6)
                java.util.Locale r6 = java.util.Locale.ROOT
                java.lang.String r5 = r5.toLowerCase(r6)
                java.lang.String r7 = "(this as java.lang.Strin….toLowerCase(Locale.ROOT)"
                defpackage.fs1.e(r5, r7)
                r8 = 0
                if (r11 != 0) goto L65
                r9 = r8
                goto L69
            L65:
                java.lang.CharSequence r9 = kotlin.text.StringsKt__StringsKt.K0(r11)
            L69:
                java.lang.String r9 = java.lang.String.valueOf(r9)
                java.lang.String r6 = r9.toLowerCase(r6)
                defpackage.fs1.e(r6, r7)
                r7 = 2
                boolean r5 = kotlin.text.StringsKt__StringsKt.M(r5, r6, r2, r7, r8)
                if (r5 == 0) goto L3f
                r3.add(r4)
                goto L3f
            L7f:
                r11 = r3
            L80:
                r0.addAll(r11)
                z60 r11 = defpackage.z60.this
                r11.notifyDataSetChanged()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.z60.a.afterTextChanged(android.text.Editable):void");
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: ContactSearchAddressAdapter.kt */
    /* renamed from: z60$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final ImageView a;
        public final TextView b;
        public final TextView c;
        public final View d;
        public final LinearLayout e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(z60 z60Var, xs1 xs1Var) {
            super(xs1Var.b());
            fs1.f(z60Var, "this$0");
            fs1.f(xs1Var, "binding");
            ImageView imageView = xs1Var.c;
            fs1.e(imageView, "binding.ivContactIcon");
            this.a = imageView;
            TextView textView = xs1Var.e;
            fs1.e(textView, "binding.tvContactName");
            this.b = textView;
            TextView textView2 = xs1Var.d;
            fs1.e(textView2, "binding.tvContactAddress");
            this.c = textView2;
            View view = xs1Var.f;
            fs1.e(view, "binding.vDivider");
            this.d = view;
            LinearLayout linearLayout = xs1Var.b;
            fs1.e(linearLayout, "binding.container");
            this.e = linearLayout;
        }

        public final TextView a() {
            return this.c;
        }

        public final LinearLayout b() {
            return this.e;
        }

        public final View c() {
            return this.d;
        }

        public final ImageView d() {
            return this.a;
        }

        public final TextView e() {
            return this.b;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public z60(List<? extends IContact> list, EditText editText, sl1 sl1Var) {
        fs1.f(list, "mainItem");
        fs1.f(editText, "editText");
        fs1.f(sl1Var, "clickListener");
        this.a = list;
        this.b = editText;
        this.c = sl1Var;
        ArrayList arrayList = new ArrayList();
        this.d = arrayList;
        arrayList.addAll(list);
        editText.addTextChangedListener(new a());
    }

    public static final void e(z60 z60Var, IContact iContact, View view) {
        fs1.f(z60Var, "this$0");
        fs1.f(iContact, "$model");
        z60Var.c.a(iContact);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        final IContact iContact = this.d.get(i);
        bVar.e().setText(iContact.getName());
        bVar.a().setText(iContact.getAddress());
        bVar.c().setVisibility(i != this.d.size() - 1 ? 0 : 8);
        com.bumptech.glide.a.u(bVar.d()).y(iContact.getProfilePath()).e0(R.drawable.contact_no_icon).l(R.drawable.contact_no_icon).a(n73.v0()).d0(250, 250).I0(bVar.d());
        bVar.b().setOnClickListener(new View.OnClickListener() { // from class: y60
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                z60.e(z60.this, iContact, view);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        xs1 a2 = xs1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_drop_down_contact_address, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        this.e = a2;
        xs1 xs1Var = this.e;
        if (xs1Var == null) {
            fs1.r("binding");
            xs1Var = null;
        }
        return new b(this, xs1Var);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.d.size();
    }
}
