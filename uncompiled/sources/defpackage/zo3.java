package defpackage;

/* compiled from: SimpleCacheKey.java */
/* renamed from: zo3  reason: default package */
/* loaded from: classes.dex */
public class zo3 implements wt {
    public final String a;
    public final boolean b;

    public zo3(String str) {
        this(str, false);
    }

    @Override // defpackage.wt
    public boolean a() {
        return this.b;
    }

    @Override // defpackage.wt
    public String b() {
        return this.a;
    }

    @Override // defpackage.wt
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof zo3) {
            return this.a.equals(((zo3) obj).a);
        }
        return false;
    }

    @Override // defpackage.wt
    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.a;
    }

    public zo3(String str, boolean z) {
        this.a = (String) xt2.g(str);
        this.b = z;
    }
}
