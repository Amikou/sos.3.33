package defpackage;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSOutcomeEventsV2Repository.kt */
/* renamed from: nk2  reason: default package */
/* loaded from: classes2.dex */
public final class nk2 extends jk2 {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public nk2(yj2 yj2Var, fk2 fk2Var, co2 co2Var) {
        super(yj2Var, fk2Var, co2Var);
        fs1.f(yj2Var, "logger");
        fs1.f(fk2Var, "outcomeEventsCache");
        fs1.f(co2Var, "outcomeEventsService");
    }

    @Override // defpackage.ik2
    public void g(String str, int i, dk2 dk2Var, ym2 ym2Var) {
        fs1.f(str, "appId");
        fs1.f(dk2Var, "event");
        fs1.f(ym2Var, "responseHandler");
        try {
            JSONObject put = dk2Var.g().put("app_id", str).put("device_type", i);
            co2 k = k();
            fs1.e(put, "jsonObject");
            k.a(put, ym2Var);
        } catch (JSONException e) {
            j().error("Generating indirect outcome:JSON Failed.", e);
        }
    }
}
