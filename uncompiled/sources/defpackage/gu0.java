package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.arguments.TokenTypes;

/* compiled from: EditContactFragmentDirections.java */
/* renamed from: gu0  reason: default package */
/* loaded from: classes2.dex */
public class gu0 {

    /* compiled from: EditContactFragmentDirections.java */
    /* renamed from: gu0$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("selectedChain")) {
                TokenTypes tokenTypes = (TokenTypes) this.a.get("selectedChain");
                if (!Parcelable.class.isAssignableFrom(TokenTypes.class) && tokenTypes != null) {
                    if (Serializable.class.isAssignableFrom(TokenTypes.class)) {
                        bundle.putSerializable("selectedChain", (Serializable) Serializable.class.cast(tokenTypes));
                    } else {
                        throw new UnsupportedOperationException(TokenTypes.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("selectedChain", (Parcelable) Parcelable.class.cast(tokenTypes));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_editContactFragment_to_selectChainForContact;
        }

        public TokenTypes c() {
            return (TokenTypes) this.a.get("selectedChain");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("selectedChain") != bVar.a.containsKey("selectedChain")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionEditContactFragmentToSelectChainForContact(actionId=" + b() + "){selectedChain=" + c() + "}";
        }

        public b(TokenTypes tokenTypes) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (tokenTypes != null) {
                hashMap.put("selectedChain", tokenTypes);
                return;
            }
            throw new IllegalArgumentException("Argument \"selectedChain\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(TokenTypes tokenTypes) {
        return new b(tokenTypes);
    }
}
