package defpackage;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.views.CustomVideoPlayer;
import net.safemoon.androidwallet.views.zoomImage.ZoomageView;

/* compiled from: ViewDetailNfcBinding.java */
/* renamed from: ii4  reason: default package */
/* loaded from: classes2.dex */
public final class ii4 {
    public final MaterialButton a;
    public final FrameLayout b;
    public final Chip c;
    public final CustomVideoPlayer d;
    public final ZoomageView e;

    public ii4(NestedScrollView nestedScrollView, MaterialButton materialButton, TextView textView, FrameLayout frameLayout, TextView textView2, TextView textView3, RecyclerView recyclerView, TextView textView4, TextView textView5, TextView textView6, Chip chip, ConstraintLayout constraintLayout, CustomVideoPlayer customVideoPlayer, ZoomageView zoomageView) {
        this.a = materialButton;
        this.b = frameLayout;
        this.c = chip;
        this.d = customVideoPlayer;
        this.e = zoomageView;
    }

    public static ii4 a(View view) {
        int i = R.id.btnSend;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnSend);
        if (materialButton != null) {
            i = R.id.contract_address;
            TextView textView = (TextView) ai4.a(view, R.id.contract_address);
            if (textView != null) {
                i = R.id.main_media_frame;
                FrameLayout frameLayout = (FrameLayout) ai4.a(view, R.id.main_media_frame);
                if (frameLayout != null) {
                    i = R.id.network;
                    TextView textView2 = (TextView) ai4.a(view, R.id.network);
                    if (textView2 != null) {
                        i = R.id.nftSource;
                        TextView textView3 = (TextView) ai4.a(view, R.id.nftSource);
                        if (textView3 != null) {
                            i = R.id.propertyRecycle;
                            RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.propertyRecycle);
                            if (recyclerView != null) {
                                i = R.id.token_id;
                                TextView textView4 = (TextView) ai4.a(view, R.id.token_id);
                                if (textView4 != null) {
                                    i = R.id.token_standard;
                                    TextView textView5 = (TextView) ai4.a(view, R.id.token_standard);
                                    if (textView5 != null) {
                                        i = R.id.txtDescription;
                                        TextView textView6 = (TextView) ai4.a(view, R.id.txtDescription);
                                        if (textView6 != null) {
                                            i = R.id.txtError;
                                            Chip chip = (Chip) ai4.a(view, R.id.txtError);
                                            if (chip != null) {
                                                i = R.id.videoParent;
                                                ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.videoParent);
                                                if (constraintLayout != null) {
                                                    i = R.id.videoView;
                                                    CustomVideoPlayer customVideoPlayer = (CustomVideoPlayer) ai4.a(view, R.id.videoView);
                                                    if (customVideoPlayer != null) {
                                                        i = R.id.zoom_image;
                                                        ZoomageView zoomageView = (ZoomageView) ai4.a(view, R.id.zoom_image);
                                                        if (zoomageView != null) {
                                                            return new ii4((NestedScrollView) view, materialButton, textView, frameLayout, textView2, textView3, recyclerView, textView4, textView5, textView6, chip, constraintLayout, customVideoPlayer, zoomageView);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
