package defpackage;

import defpackage.uj3;
import kotlin.coroutines.CoroutineContext;

/* compiled from: Produce.kt */
/* renamed from: gv2  reason: default package */
/* loaded from: classes2.dex */
public class gv2<E> extends mx<E> implements kv2<E> {
    public gv2(CoroutineContext coroutineContext, kx<E> kxVar) {
        super(coroutineContext, kxVar, true, true);
    }

    @Override // defpackage.t4
    public void I0(Throwable th, boolean z) {
        if (L0().l(th) || z) {
            return;
        }
        z80.a(getContext(), th);
    }

    @Override // defpackage.t4
    /* renamed from: M0 */
    public void J0(te4 te4Var) {
        uj3.a.a(L0(), null, 1, null);
    }

    @Override // defpackage.t4, defpackage.bu1, defpackage.st1
    public boolean b() {
        return super.b();
    }
}
