package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: DelayProducer.java */
/* renamed from: yl0  reason: default package */
/* loaded from: classes.dex */
public class yl0 implements dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> {
    public final dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> a;
    public final ScheduledExecutorService b;

    /* compiled from: DelayProducer.java */
    /* renamed from: yl0$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ l60 a;
        public final /* synthetic */ ev2 f0;

        public a(l60 l60Var, ev2 ev2Var) {
            this.a = l60Var;
            this.f0 = ev2Var;
        }

        @Override // java.lang.Runnable
        public void run() {
            yl0.this.a.a(this.a, this.f0);
        }
    }

    public yl0(dv2<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> dv2Var, ScheduledExecutorService scheduledExecutorService) {
        this.a = dv2Var;
        this.b = scheduledExecutorService;
    }

    @Override // defpackage.dv2
    public void a(l60<com.facebook.common.references.a<com.facebook.imagepipeline.image.a>> l60Var, ev2 ev2Var) {
        ImageRequest c = ev2Var.c();
        ScheduledExecutorService scheduledExecutorService = this.b;
        if (scheduledExecutorService != null) {
            scheduledExecutorService.schedule(new a(l60Var, ev2Var), c.f(), TimeUnit.MILLISECONDS);
        } else {
            this.a.a(l60Var, ev2Var);
        }
    }
}
