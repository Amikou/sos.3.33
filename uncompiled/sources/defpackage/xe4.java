package defpackage;

import android.content.Context;
import java.security.MessageDigest;

/* compiled from: UnitTransformation.java */
/* renamed from: xe4  reason: default package */
/* loaded from: classes.dex */
public final class xe4<T> implements za4<T> {
    public static final za4<?> b = new xe4();

    public static <T> xe4<T> c() {
        return (xe4) b;
    }

    @Override // defpackage.za4
    public s73<T> a(Context context, s73<T> s73Var, int i, int i2) {
        return s73Var;
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
    }
}
