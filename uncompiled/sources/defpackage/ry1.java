package defpackage;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: LayoutSelectTokenTypeBinding.java */
/* renamed from: ry1  reason: default package */
/* loaded from: classes2.dex */
public final class ry1 {
    public final MaterialButton a;
    public final ConstraintLayout b;
    public final ImageView c;
    public final MaterialTextView d;

    public ry1(ConstraintLayout constraintLayout, MaterialButton materialButton, ConstraintLayout constraintLayout2, ImageView imageView, ImageView imageView2, MaterialTextView materialTextView) {
        this.a = materialButton;
        this.b = constraintLayout2;
        this.c = imageView2;
        this.d = materialTextView;
    }

    public static ry1 a(View view) {
        int i = R.id.bnbSelectedTypeName;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.bnbSelectedTypeName);
        if (materialButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.dropDownIcon;
            ImageView imageView = (ImageView) ai4.a(view, R.id.dropDownIcon);
            if (imageView != null) {
                i = R.id.iconChainImage;
                ImageView imageView2 = (ImageView) ai4.a(view, R.id.iconChainImage);
                if (imageView2 != null) {
                    i = R.id.text;
                    MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.text);
                    if (materialTextView != null) {
                        return new ry1(constraintLayout, materialButton, constraintLayout, imageView, imageView2, materialTextView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
