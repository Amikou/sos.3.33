package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: CarouselViewAdapter.java */
/* renamed from: kw  reason: default package */
/* loaded from: classes2.dex */
public class kw extends RecyclerView.Adapter<a> {
    public lw a;
    public int b;
    public int c;
    public RecyclerView d;
    public hw e = new hw();
    public boolean f;
    public int g;

    /* compiled from: CarouselViewAdapter.java */
    /* renamed from: kw$a */
    /* loaded from: classes2.dex */
    public static class a extends RecyclerView.a0 {
        public a(View view) {
            super(view);
        }
    }

    public kw(lw lwVar, int i, int i2, RecyclerView recyclerView, int i3, boolean z) {
        this.a = lwVar;
        this.b = i;
        this.c = i2;
        this.d = recyclerView;
        this.f = z;
        this.g = i3;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        lw lwVar = this.a;
        if (lwVar != null) {
            lwVar.a(aVar.itemView, i);
        }
        this.e.a(this.d, aVar.itemView, this.g, this.f);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new a(LayoutInflater.from(viewGroup.getContext()).inflate(this.b, viewGroup, false));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.c;
    }
}
