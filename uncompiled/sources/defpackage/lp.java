package defpackage;

import defpackage.wi3;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: BinarySearchSeeker.java */
/* renamed from: lp  reason: default package */
/* loaded from: classes.dex */
public abstract class lp {
    public final a a;
    public final f b;
    public c c;
    public final int d;

    /* compiled from: BinarySearchSeeker.java */
    /* renamed from: lp$a */
    /* loaded from: classes.dex */
    public static class a implements wi3 {
        public final d a;
        public final long b;
        public final long c;
        public final long d;
        public final long e;
        public final long f;
        public final long g;

        public a(d dVar, long j, long j2, long j3, long j4, long j5, long j6) {
            this.a = dVar;
            this.b = j;
            this.c = j2;
            this.d = j3;
            this.e = j4;
            this.f = j5;
            this.g = j6;
        }

        @Override // defpackage.wi3
        public boolean e() {
            return true;
        }

        @Override // defpackage.wi3
        public wi3.a h(long j) {
            return new wi3.a(new yi3(j, c.h(this.a.a(j), this.c, this.d, this.e, this.f, this.g)));
        }

        @Override // defpackage.wi3
        public long i() {
            return this.b;
        }

        public long k(long j) {
            return this.a.a(j);
        }
    }

    /* compiled from: BinarySearchSeeker.java */
    /* renamed from: lp$b */
    /* loaded from: classes.dex */
    public static final class b implements d {
        @Override // defpackage.lp.d
        public long a(long j) {
            return j;
        }
    }

    /* compiled from: BinarySearchSeeker.java */
    /* renamed from: lp$c */
    /* loaded from: classes.dex */
    public static class c {
        public final long a;
        public final long b;
        public final long c;
        public long d;
        public long e;
        public long f;
        public long g;
        public long h;

        public c(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
            this.a = j;
            this.b = j2;
            this.d = j3;
            this.e = j4;
            this.f = j5;
            this.g = j6;
            this.c = j7;
            this.h = h(j2, j3, j4, j5, j6, j7);
        }

        public static long h(long j, long j2, long j3, long j4, long j5, long j6) {
            if (j4 + 1 >= j5 || j2 + 1 >= j3) {
                return j4;
            }
            long j7 = ((float) (j - j2)) * (((float) (j5 - j4)) / ((float) (j3 - j2)));
            return androidx.media3.common.util.b.r(((j7 + j4) - j6) - (j7 / 20), j4, j5 - 1);
        }

        public final long i() {
            return this.g;
        }

        public final long j() {
            return this.f;
        }

        public final long k() {
            return this.h;
        }

        public final long l() {
            return this.a;
        }

        public final long m() {
            return this.b;
        }

        public final void n() {
            this.h = h(this.b, this.d, this.e, this.f, this.g, this.c);
        }

        public final void o(long j, long j2) {
            this.e = j;
            this.g = j2;
            n();
        }

        public final void p(long j, long j2) {
            this.d = j;
            this.f = j2;
            n();
        }
    }

    /* compiled from: BinarySearchSeeker.java */
    /* renamed from: lp$d */
    /* loaded from: classes.dex */
    public interface d {
        long a(long j);
    }

    /* compiled from: BinarySearchSeeker.java */
    /* renamed from: lp$e */
    /* loaded from: classes.dex */
    public static final class e {
        public static final e d = new e(-3, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, -1);
        public final int a;
        public final long b;
        public final long c;

        public e(int i, long j, long j2) {
            this.a = i;
            this.b = j;
            this.c = j2;
        }

        public static e d(long j, long j2) {
            return new e(-1, j, j2);
        }

        public static e e(long j) {
            return new e(0, CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED, j);
        }

        public static e f(long j, long j2) {
            return new e(-2, j, j2);
        }
    }

    /* compiled from: BinarySearchSeeker.java */
    /* renamed from: lp$f */
    /* loaded from: classes.dex */
    public interface f {
        e a(q11 q11Var, long j) throws IOException;

        void b();
    }

    public lp(d dVar, f fVar, long j, long j2, long j3, long j4, long j5, long j6, int i) {
        this.b = fVar;
        this.d = i;
        this.a = new a(dVar, j, j2, j3, j4, j5, j6);
    }

    public c a(long j) {
        return new c(j, this.a.k(j), this.a.c, this.a.d, this.a.e, this.a.f, this.a.g);
    }

    public final wi3 b() {
        return this.a;
    }

    public int c(q11 q11Var, ot2 ot2Var) throws IOException {
        while (true) {
            c cVar = (c) ii.i(this.c);
            long j = cVar.j();
            long i = cVar.i();
            long k = cVar.k();
            if (i - j <= this.d) {
                e(false, j);
                return g(q11Var, j, ot2Var);
            } else if (!i(q11Var, k)) {
                return g(q11Var, k, ot2Var);
            } else {
                q11Var.j();
                e a2 = this.b.a(q11Var, cVar.m());
                int i2 = a2.a;
                if (i2 == -3) {
                    e(false, k);
                    return g(q11Var, k, ot2Var);
                } else if (i2 == -2) {
                    cVar.p(a2.b, a2.c);
                } else if (i2 != -1) {
                    if (i2 == 0) {
                        i(q11Var, a2.c);
                        e(true, a2.c);
                        return g(q11Var, a2.c, ot2Var);
                    }
                    throw new IllegalStateException("Invalid case");
                } else {
                    cVar.o(a2.b, a2.c);
                }
            }
        }
    }

    public final boolean d() {
        return this.c != null;
    }

    public final void e(boolean z, long j) {
        this.c = null;
        this.b.b();
        f(z, j);
    }

    public void f(boolean z, long j) {
    }

    public final int g(q11 q11Var, long j, ot2 ot2Var) {
        if (j == q11Var.getPosition()) {
            return 0;
        }
        ot2Var.a = j;
        return 1;
    }

    public final void h(long j) {
        c cVar = this.c;
        if (cVar == null || cVar.l() != j) {
            this.c = a(j);
        }
    }

    public final boolean i(q11 q11Var, long j) throws IOException {
        long position = j - q11Var.getPosition();
        if (position < 0 || position > 262144) {
            return false;
        }
        q11Var.k((int) position);
        return true;
    }
}
