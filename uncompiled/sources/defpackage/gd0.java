package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.trustwallet.walletconnect.models.WCPeerMeta;
import defpackage.gd0;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;
import net.safemoon.androidwallet.model.common.HeaderItemHistory;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWallet;
import net.safemoon.androidwallet.model.walletConnect.RoomConnectedInfoAndWalletListItem;
import net.safemoon.androidwallet.model.walletConnect.RoomExtensionKt;
import net.safemoon.androidwallet.model.wallets.Wallet;

/* compiled from: DAppConnectedAdapter.kt */
/* renamed from: gd0  reason: default package */
/* loaded from: classes2.dex */
public final class gd0 extends RecyclerView.Adapter<RecyclerView.a0> {
    public final Context a;
    public final tc1<RoomConnectedInfoAndWallet, te4> b;
    public final List<HistoryListItem> c;

    /* compiled from: DAppConnectedAdapter.kt */
    /* renamed from: gd0$a */
    /* loaded from: classes2.dex */
    public final class a extends RecyclerView.a0 {
        public final uk1 a;
        public final /* synthetic */ gd0 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(gd0 gd0Var, uk1 uk1Var) {
            super(uk1Var.b());
            fs1.f(gd0Var, "this$0");
            fs1.f(uk1Var, "binding");
            this.b = gd0Var;
            this.a = uk1Var;
        }

        public static final void e(gd0 gd0Var, RoomConnectedInfoAndWallet roomConnectedInfoAndWallet, View view) {
            fs1.f(gd0Var, "this$0");
            fs1.f(roomConnectedInfoAndWallet, "$roomConnectedInfoAndWallet");
            gd0Var.d().invoke(roomConnectedInfoAndWallet);
        }

        public static final void f(gd0 gd0Var, RoomConnectedInfoAndWallet roomConnectedInfoAndWallet, View view) {
            fs1.f(gd0Var, "this$0");
            fs1.f(roomConnectedInfoAndWallet, "$roomConnectedInfoAndWallet");
            gd0Var.d().invoke(roomConnectedInfoAndWallet);
        }

        public static final void g(gd0 gd0Var, RoomConnectedInfoAndWallet roomConnectedInfoAndWallet, View view) {
            fs1.f(gd0Var, "this$0");
            fs1.f(roomConnectedInfoAndWallet, "$roomConnectedInfoAndWallet");
            gd0Var.d().invoke(roomConnectedInfoAndWallet);
        }

        public final void d(int i) {
            final RoomConnectedInfoAndWallet result = ((RoomConnectedInfoAndWalletListItem) this.b.c.get(i)).getResult();
            fs1.d(result);
            WCPeerMeta peerMeta = RoomExtensionKt.toPeerMeta(result.getDApp().getPeerMeta());
            uk1 uk1Var = this.a;
            final gd0 gd0Var = this.b;
            if (!peerMeta.getIcons().isEmpty()) {
                String str = peerMeta.getIcons().get(0);
                int f = bo3.f(gd0Var.b(), "MODE_NIGHT", 1);
                if (StringsKt__StringsKt.M(str, "swap.safemoon.com", false, 2, null) && f == 1) {
                    com.bumptech.glide.a.u(uk1Var.d).y("https://safemoon.com/img/logo_dark.png").d0(200, 200).a(n73.v0()).I0(uk1Var.d);
                } else {
                    com.bumptech.glide.a.u(uk1Var.d).y(str).d0(200, 200).a(n73.v0()).I0(uk1Var.d);
                }
            }
            uk1Var.f.setText(peerMeta.getName());
            uk1Var.e.setText(peerMeta.getUrl());
            uk1Var.b().setOnClickListener(new View.OnClickListener() { // from class: fd0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    gd0.a.e(gd0.this, result, view);
                }
            });
            uk1Var.b.setOnClickListener(new View.OnClickListener() { // from class: dd0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    gd0.a.f(gd0.this, result, view);
                }
            });
            uk1Var.c.setOnClickListener(new View.OnClickListener() { // from class: ed0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    gd0.a.g(gd0.this, result, view);
                }
            });
        }
    }

    /* compiled from: DAppConnectedAdapter.kt */
    /* renamed from: gd0$b */
    /* loaded from: classes2.dex */
    public static final class b extends RecyclerView.a0 {
        public final zs1 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(zs1 zs1Var) {
            super(zs1Var.b());
            fs1.f(zs1Var, "binding");
            this.a = zs1Var;
        }

        public final void a(HistoryListItem historyListItem) {
            fs1.f(historyListItem, "item");
            this.a.b().setBackgroundResource(17170445);
            this.a.b.setText(((HeaderItemHistory) historyListItem).getTitle());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public gd0(Context context, tc1<? super RoomConnectedInfoAndWallet, te4> tc1Var) {
        fs1.f(context, "context");
        fs1.f(tc1Var, "onItemClick");
        this.a = context;
        this.b = tc1Var;
        this.c = new ArrayList();
    }

    public final Context b() {
        return this.a;
    }

    public final RoomConnectedInfoAndWallet c(int i) {
        if (this.c.get(i) instanceof RoomConnectedInfoAndWalletListItem) {
            return ((RoomConnectedInfoAndWalletListItem) this.c.get(i)).getResult();
        }
        return null;
    }

    public final tc1<RoomConnectedInfoAndWallet, te4> d() {
        return this.b;
    }

    public final void e(List<RoomConnectedInfoAndWallet> list) {
        fs1.f(list, "it");
        this.c.clear();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : list) {
            Wallet wallet2 = ((RoomConnectedInfoAndWallet) obj).getWallet();
            Object obj2 = linkedHashMap.get(wallet2);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(wallet2, obj2);
            }
            ((List) obj2).add(obj);
        }
        for (Wallet wallet3 : linkedHashMap.keySet()) {
            List<HistoryListItem> list2 = this.c;
            HeaderItemHistory headerItemHistory = new HeaderItemHistory();
            headerItemHistory.setTitle(wallet3 == null ? null : wallet3.displayName());
            te4 te4Var = te4.a;
            list2.add(headerItemHistory);
            List<RoomConnectedInfoAndWallet> list3 = (List) linkedHashMap.get(wallet3);
            if (list3 != null && (r1 = list3.iterator()) != null) {
                for (RoomConnectedInfoAndWallet roomConnectedInfoAndWallet : list3) {
                    List<HistoryListItem> list4 = this.c;
                    RoomConnectedInfoAndWalletListItem roomConnectedInfoAndWalletListItem = new RoomConnectedInfoAndWalletListItem();
                    roomConnectedInfoAndWalletListItem.setResult(roomConnectedInfoAndWallet);
                    te4 te4Var2 = te4.a;
                    list4.add(roomConnectedInfoAndWalletListItem);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.c.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.c.get(i).getType();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        fs1.f(a0Var, "holder");
        HistoryListItem historyListItem = this.c.get(i);
        if (getItemViewType(i) == 0) {
            if (a0Var instanceof b) {
                ((b) a0Var).a(historyListItem);
            }
        } else if (getItemViewType(i) != 1 && (a0Var instanceof a)) {
            ((a) a0Var).d(i);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        if (i == 0) {
            zs1 c = zs1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
            fs1.e(c, "inflate(LayoutInflater.f….context), parent, false)");
            return new b(c);
        }
        uk1 c2 = uk1.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        fs1.e(c2, "inflate(LayoutInflater.f….context), parent, false)");
        return new a(this, c2);
    }
}
