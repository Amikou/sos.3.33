package defpackage;

/* compiled from: InternalRequestListener.java */
/* renamed from: es1  reason: default package */
/* loaded from: classes.dex */
public class es1 extends ds1 implements i73 {
    public final h73 c;
    public final i73 d;

    public es1(h73 h73Var, i73 i73Var) {
        super(h73Var, i73Var);
        this.c = h73Var;
        this.d = i73Var;
    }

    @Override // defpackage.i73
    public void b(ev2 ev2Var) {
        h73 h73Var = this.c;
        if (h73Var != null) {
            h73Var.a(ev2Var.c(), ev2Var.a(), ev2Var.getId(), ev2Var.h());
        }
        i73 i73Var = this.d;
        if (i73Var != null) {
            i73Var.b(ev2Var);
        }
    }

    @Override // defpackage.i73
    public void f(ev2 ev2Var, Throwable th) {
        h73 h73Var = this.c;
        if (h73Var != null) {
            h73Var.g(ev2Var.c(), ev2Var.getId(), th, ev2Var.h());
        }
        i73 i73Var = this.d;
        if (i73Var != null) {
            i73Var.f(ev2Var, th);
        }
    }

    @Override // defpackage.i73
    public void g(ev2 ev2Var) {
        h73 h73Var = this.c;
        if (h73Var != null) {
            h73Var.c(ev2Var.c(), ev2Var.getId(), ev2Var.h());
        }
        i73 i73Var = this.d;
        if (i73Var != null) {
            i73Var.g(ev2Var);
        }
    }

    @Override // defpackage.i73
    public void h(ev2 ev2Var) {
        h73 h73Var = this.c;
        if (h73Var != null) {
            h73Var.k(ev2Var.getId());
        }
        i73 i73Var = this.d;
        if (i73Var != null) {
            i73Var.h(ev2Var);
        }
    }
}
