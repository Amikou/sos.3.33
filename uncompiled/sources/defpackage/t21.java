package defpackage;

import android.net.Uri;

/* compiled from: FetchState.java */
/* renamed from: t21  reason: default package */
/* loaded from: classes.dex */
public class t21 {
    public final l60<zu0> a;
    public final ev2 b;
    public long c = 0;
    public int d;
    public ct e;

    public t21(l60<zu0> l60Var, ev2 ev2Var) {
        this.a = l60Var;
        this.b = ev2Var;
    }

    public l60<zu0> a() {
        return this.a;
    }

    public ev2 b() {
        return this.b;
    }

    public long c() {
        return this.c;
    }

    public iv2 d() {
        return this.b.l();
    }

    public int e() {
        return this.d;
    }

    public ct f() {
        return this.e;
    }

    public Uri g() {
        return this.b.c().u();
    }

    public void h(long j) {
        this.c = j;
    }
}
