package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: za5  reason: default package */
/* loaded from: classes.dex */
public final class za5 extends n65<Float> implements rb5<Float> {
    public float[] f0;
    public int g0;

    static {
        new za5().v();
    }

    public za5() {
        this(new float[10], 0);
    }

    public za5(float[] fArr, int i) {
        this.f0 = fArr;
        this.g0 = i;
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5<Float> T0(int i) {
        if (i >= this.g0) {
            return new za5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        k(i, ((Float) obj).floatValue());
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Float> collection) {
        e();
        gb5.a(collection);
        if (collection instanceof za5) {
            za5 za5Var = (za5) collection;
            int i = za5Var.g0;
            if (i == 0) {
                return false;
            }
            int i2 = this.g0;
            if (Integer.MAX_VALUE - i2 >= i) {
                int i3 = i2 + i;
                float[] fArr = this.f0;
                if (i3 > fArr.length) {
                    this.f0 = Arrays.copyOf(fArr, i3);
                }
                System.arraycopy(za5Var.f0, 0, this.f0, this.g0, za5Var.g0);
                this.g0 = i3;
                ((AbstractList) this).modCount++;
                return true;
            }
            throw new OutOfMemoryError();
        }
        return super.addAll(collection);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof za5) {
            za5 za5Var = (za5) obj;
            if (this.g0 != za5Var.g0) {
                return false;
            }
            float[] fArr = za5Var.f0;
            for (int i = 0; i < this.g0; i++) {
                if (this.f0[i] != fArr[i]) {
                    return false;
                }
            }
            return true;
        }
        return super.equals(obj);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        m(i);
        return Float.valueOf(this.f0[i]);
    }

    @Override // defpackage.n65, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f0[i2]);
        }
        return i;
    }

    public final void i(float f) {
        k(this.g0, f);
    }

    public final void k(int i, float f) {
        int i2;
        e();
        if (i < 0 || i > (i2 = this.g0)) {
            throw new IndexOutOfBoundsException(n(i));
        }
        float[] fArr = this.f0;
        if (i2 < fArr.length) {
            System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
        } else {
            float[] fArr2 = new float[((i2 * 3) / 2) + 1];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            System.arraycopy(this.f0, i, fArr2, i + 1, this.g0 - i);
            this.f0 = fArr2;
        }
        this.f0[i] = f;
        this.g0++;
        ((AbstractList) this).modCount++;
    }

    public final void m(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(n(i));
        }
    }

    public final String n(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        e();
        m(i);
        float[] fArr = this.f0;
        float f = fArr[i];
        int i2 = this.g0;
        if (i < i2 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, i2 - i);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Float.valueOf(f);
    }

    @Override // defpackage.n65, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean remove(Object obj) {
        e();
        for (int i = 0; i < this.g0; i++) {
            if (obj.equals(Float.valueOf(this.f0[i]))) {
                float[] fArr = this.f0;
                System.arraycopy(fArr, i + 1, fArr, i, this.g0 - i);
                this.g0--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 < i) {
            throw new IndexOutOfBoundsException("toIndex < fromIndex");
        }
        float[] fArr = this.f0;
        System.arraycopy(fArr, i2, fArr, i, this.g0 - i2);
        this.g0 -= i2 - i;
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        e();
        m(i);
        float[] fArr = this.f0;
        float f = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }
}
