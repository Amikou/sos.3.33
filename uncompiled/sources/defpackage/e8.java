package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityWipeDataBinding.java */
/* renamed from: e8  reason: default package */
/* loaded from: classes2.dex */
public final class e8 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final sp3 d;

    public e8(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, sp3 sp3Var, TextView textView, TextView textView2, TextView textView3) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = sp3Var;
    }

    public static e8 a(View view) {
        int i = R.id.btnCancel;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnCancel);
        if (materialButton != null) {
            i = R.id.btnConfirm;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnConfirm);
            if (materialButton2 != null) {
                i = R.id.imgCancel;
                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgCancel);
                if (appCompatImageView != null) {
                    i = R.id.imgCard;
                    AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.imgCard);
                    if (appCompatImageView2 != null) {
                        i = R.id.toolbar;
                        View a = ai4.a(view, R.id.toolbar);
                        if (a != null) {
                            sp3 a2 = sp3.a(a);
                            i = R.id.tvLoginNote;
                            TextView textView = (TextView) ai4.a(view, R.id.tvLoginNote);
                            if (textView != null) {
                                i = R.id.tvNoticeContent;
                                TextView textView2 = (TextView) ai4.a(view, R.id.tvNoticeContent);
                                if (textView2 != null) {
                                    i = R.id.tvResetWalletsHeader;
                                    TextView textView3 = (TextView) ai4.a(view, R.id.tvResetWalletsHeader);
                                    if (textView3 != null) {
                                        return new e8((ConstraintLayout) view, materialButton, materialButton2, appCompatImageView, appCompatImageView2, a2, textView, textView2, textView3);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static e8 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static e8 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_wipe_data, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
