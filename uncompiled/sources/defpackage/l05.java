package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: l05  reason: default package */
/* loaded from: classes.dex */
public abstract class l05 {
    public int a;

    public final void a(Context context, Bitmap bitmap, boolean z) {
        ji.c(bitmap);
        c(new BitmapDrawable(context.getResources(), bitmap), z, false, true);
    }

    public final void b(Context context, z15 z15Var, boolean z) {
        int i = this.a;
        c(i != 0 ? context.getResources().getDrawable(i) : null, z, false, false);
    }

    public abstract void c(Drawable drawable, boolean z, boolean z2, boolean z3);
}
