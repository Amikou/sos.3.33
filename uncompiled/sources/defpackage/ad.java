package defpackage;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

/* compiled from: AndroidResourceSignature.java */
/* renamed from: ad  reason: default package */
/* loaded from: classes.dex */
public final class ad implements fx1 {
    public final int b;
    public final fx1 c;

    public ad(int i, fx1 fx1Var) {
        this.b = i;
        this.c = fx1Var;
    }

    public static fx1 c(Context context) {
        return new ad(context.getResources().getConfiguration().uiMode & 48, dh.c(context));
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        this.c.b(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof ad) {
            ad adVar = (ad) obj;
            return this.b == adVar.b && this.c.equals(adVar.c);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return mg4.o(this.c, this.b);
    }
}
