package defpackage;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: sz4  reason: default package */
/* loaded from: classes.dex */
public final class sz4 extends q05 {
    public final /* synthetic */ qz4 b;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public sz4(qz4 qz4Var, j05 j05Var) {
        super(j05Var);
        this.b = qz4Var;
    }

    @Override // defpackage.q05
    public final void a() {
        i05 i05Var;
        i05Var = this.b.a;
        i05Var.n.d(null);
    }
}
