package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;

/* compiled from: ColorDrawer.java */
/* renamed from: v20  reason: default package */
/* loaded from: classes2.dex */
public class v20 extends hn {
    public v20(Paint paint, mq1 mq1Var) {
        super(paint, mq1Var);
    }

    public void a(Canvas canvas, wg4 wg4Var, int i, int i2, int i3) {
        if (wg4Var instanceof u20) {
            u20 u20Var = (u20) wg4Var;
            float m = this.b.m();
            int p = this.b.p();
            int q = this.b.q();
            int r = this.b.r();
            int f = this.b.f();
            if (this.b.z()) {
                if (i == r) {
                    p = u20Var.a();
                } else if (i == q) {
                    p = u20Var.b();
                }
            } else if (i == q) {
                p = u20Var.a();
            } else if (i == f) {
                p = u20Var.b();
            }
            this.a.setColor(p);
            canvas.drawCircle(i2, i3, m, this.a);
        }
    }
}
