package defpackage;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ViewPropertyAnimatorCompatSet.java */
/* renamed from: wj4  reason: default package */
/* loaded from: classes.dex */
public class wj4 {
    public Interpolator c;
    public xj4 d;
    public boolean e;
    public long b = -1;
    public final yj4 f = new a();
    public final ArrayList<vj4> a = new ArrayList<>();

    /* compiled from: ViewPropertyAnimatorCompatSet.java */
    /* renamed from: wj4$a */
    /* loaded from: classes.dex */
    public class a extends yj4 {
        public boolean a = false;
        public int b = 0;

        public a() {
        }

        @Override // defpackage.xj4
        public void b(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == wj4.this.a.size()) {
                xj4 xj4Var = wj4.this.d;
                if (xj4Var != null) {
                    xj4Var.b(null);
                }
                d();
            }
        }

        @Override // defpackage.yj4, defpackage.xj4
        public void c(View view) {
            if (this.a) {
                return;
            }
            this.a = true;
            xj4 xj4Var = wj4.this.d;
            if (xj4Var != null) {
                xj4Var.c(null);
            }
        }

        public void d() {
            this.b = 0;
            this.a = false;
            wj4.this.b();
        }
    }

    public void a() {
        if (this.e) {
            Iterator<vj4> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
            this.e = false;
        }
    }

    public void b() {
        this.e = false;
    }

    public wj4 c(vj4 vj4Var) {
        if (!this.e) {
            this.a.add(vj4Var);
        }
        return this;
    }

    public wj4 d(vj4 vj4Var, vj4 vj4Var2) {
        this.a.add(vj4Var);
        vj4Var2.h(vj4Var.c());
        this.a.add(vj4Var2);
        return this;
    }

    public wj4 e(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    public wj4 f(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    public wj4 g(xj4 xj4Var) {
        if (!this.e) {
            this.d = xj4Var;
        }
        return this;
    }

    public void h() {
        if (this.e) {
            return;
        }
        Iterator<vj4> it = this.a.iterator();
        while (it.hasNext()) {
            vj4 next = it.next();
            long j = this.b;
            if (j >= 0) {
                next.d(j);
            }
            Interpolator interpolator = this.c;
            if (interpolator != null) {
                next.e(interpolator);
            }
            if (this.d != null) {
                next.f(this.f);
            }
            next.j();
        }
        this.e = true;
    }
}
