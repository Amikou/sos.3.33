package defpackage;

import com.google.android.gms.internal.measurement.g;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ij5  reason: default package */
/* loaded from: classes.dex */
public final class ij5 extends t22<String, g> {
    public final /* synthetic */ qj5 i;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ij5(qj5 qj5Var, int i) {
        super(20);
        this.i = qj5Var;
    }

    @Override // defpackage.t22
    public final /* bridge */ /* synthetic */ g a(String str) {
        String str2 = str;
        zt2.f(str2);
        return qj5.y(this.i, str2);
    }
}
