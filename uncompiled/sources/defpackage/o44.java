package defpackage;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.media3.common.j;
import androidx.media3.common.util.b;
import androidx.media3.exoplayer.c;
import androidx.media3.extractor.text.SubtitleDecoderException;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: TextRenderer.java */
/* renamed from: o44  reason: default package */
/* loaded from: classes.dex */
public final class o44 extends c implements Handler.Callback {
    public tv3 A0;
    public uv3 B0;
    public uv3 C0;
    public int D0;
    public long E0;
    public final Handler q0;
    public final n44 r0;
    public final sv3 s0;
    public final y81 t0;
    public boolean u0;
    public boolean v0;
    public boolean w0;
    public int x0;
    public j y0;
    public rv3 z0;

    public o44(n44 n44Var, Looper looper) {
        this(n44Var, looper, sv3.a);
    }

    @Override // androidx.media3.exoplayer.c
    public void G() {
        this.y0 = null;
        this.E0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        Q();
        W();
    }

    @Override // androidx.media3.exoplayer.c
    public void I(long j, boolean z) {
        Q();
        this.u0 = false;
        this.v0 = false;
        this.E0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        if (this.x0 != 0) {
            X();
            return;
        }
        V();
        ((rv3) ii.e(this.z0)).flush();
    }

    @Override // androidx.media3.exoplayer.c
    public void M(j[] jVarArr, long j, long j2) {
        this.y0 = jVarArr[0];
        if (this.z0 != null) {
            this.x0 = 1;
        } else {
            T();
        }
    }

    public final void Q() {
        Z(Collections.emptyList());
    }

    public final long R() {
        if (this.D0 == -1) {
            return Long.MAX_VALUE;
        }
        ii.e(this.B0);
        if (this.D0 >= this.B0.f()) {
            return Long.MAX_VALUE;
        }
        return this.B0.d(this.D0);
    }

    public final void S(SubtitleDecoderException subtitleDecoderException) {
        p12.d("TextRenderer", "Subtitle decoding failed. streamFormat=" + this.y0, subtitleDecoderException);
        Q();
        X();
    }

    public final void T() {
        this.w0 = true;
        this.z0 = this.s0.b((j) ii.e(this.y0));
    }

    public final void U(List<kb0> list) {
        this.r0.u(list);
        this.r0.o(new nb0(list));
    }

    public final void V() {
        this.A0 = null;
        this.D0 = -1;
        uv3 uv3Var = this.B0;
        if (uv3Var != null) {
            uv3Var.u();
            this.B0 = null;
        }
        uv3 uv3Var2 = this.C0;
        if (uv3Var2 != null) {
            uv3Var2.u();
            this.C0 = null;
        }
    }

    public final void W() {
        V();
        ((rv3) ii.e(this.z0)).a();
        this.z0 = null;
        this.x0 = 0;
    }

    public final void X() {
        W();
        T();
    }

    public void Y(long j) {
        ii.g(w());
        this.E0 = j;
    }

    public final void Z(List<kb0> list) {
        Handler handler = this.q0;
        if (handler != null) {
            handler.obtainMessage(0, list).sendToTarget();
        } else {
            U(list);
        }
    }

    @Override // androidx.media3.exoplayer.n
    public int a(j jVar) {
        if (this.s0.a(jVar)) {
            return u63.a(jVar.I0 == 0 ? 4 : 2);
        } else if (y82.p(jVar.p0)) {
            return u63.a(1);
        } else {
            return u63.a(0);
        }
    }

    @Override // androidx.media3.exoplayer.m
    public boolean d() {
        return this.v0;
    }

    @Override // androidx.media3.exoplayer.m
    public boolean f() {
        return true;
    }

    @Override // androidx.media3.exoplayer.m, androidx.media3.exoplayer.n
    public String getName() {
        return "TextRenderer";
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            U((List) message.obj);
            return true;
        }
        throw new IllegalStateException();
    }

    @Override // androidx.media3.exoplayer.m
    public void r(long j, long j2) {
        boolean z;
        if (w()) {
            long j3 = this.E0;
            if (j3 != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && j >= j3) {
                V();
                this.v0 = true;
            }
        }
        if (this.v0) {
            return;
        }
        if (this.C0 == null) {
            ((rv3) ii.e(this.z0)).b(j);
            try {
                this.C0 = ((rv3) ii.e(this.z0)).c();
            } catch (SubtitleDecoderException e) {
                S(e);
                return;
            }
        }
        if (getState() != 2) {
            return;
        }
        if (this.B0 != null) {
            long R = R();
            z = false;
            while (R <= j) {
                this.D0++;
                R = R();
                z = true;
            }
        } else {
            z = false;
        }
        uv3 uv3Var = this.C0;
        if (uv3Var != null) {
            if (uv3Var.p()) {
                if (!z && R() == Long.MAX_VALUE) {
                    if (this.x0 == 2) {
                        X();
                    } else {
                        V();
                        this.v0 = true;
                    }
                }
            } else if (uv3Var.f0 <= j) {
                uv3 uv3Var2 = this.B0;
                if (uv3Var2 != null) {
                    uv3Var2.u();
                }
                this.D0 = uv3Var.a(j);
                this.B0 = uv3Var;
                this.C0 = null;
                z = true;
            }
        }
        if (z) {
            ii.e(this.B0);
            Z(this.B0.e(j));
        }
        if (this.x0 == 2) {
            return;
        }
        while (!this.u0) {
            try {
                tv3 tv3Var = this.A0;
                if (tv3Var == null) {
                    tv3Var = ((rv3) ii.e(this.z0)).d();
                    if (tv3Var == null) {
                        return;
                    }
                    this.A0 = tv3Var;
                }
                if (this.x0 == 1) {
                    tv3Var.t(4);
                    ((rv3) ii.e(this.z0)).e(tv3Var);
                    this.A0 = null;
                    this.x0 = 2;
                    return;
                }
                int N = N(this.t0, tv3Var, 0);
                if (N == -4) {
                    if (tv3Var.p()) {
                        this.u0 = true;
                        this.w0 = false;
                    } else {
                        j jVar = this.t0.b;
                        if (jVar == null) {
                            return;
                        }
                        tv3Var.m0 = jVar.t0;
                        tv3Var.x();
                        this.w0 &= !tv3Var.s();
                    }
                    if (!this.w0) {
                        ((rv3) ii.e(this.z0)).e(tv3Var);
                        this.A0 = null;
                    }
                } else if (N == -3) {
                    return;
                }
            } catch (SubtitleDecoderException e2) {
                S(e2);
                return;
            }
        }
    }

    public o44(n44 n44Var, Looper looper, sv3 sv3Var) {
        super(3);
        this.r0 = (n44) ii.e(n44Var);
        this.q0 = looper == null ? null : b.u(looper, this);
        this.s0 = sv3Var;
        this.t0 = new y81();
        this.E0 = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }
}
