package defpackage;

import android.text.TextUtils;
import java.util.Collection;
import java.util.Objects;

/* compiled from: Preconditions.java */
/* renamed from: wt2  reason: default package */
/* loaded from: classes.dex */
public final class wt2 {
    public static void a(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Must not be null or empty");
        }
        return str;
    }

    public static <T extends Collection<Y>, Y> T c(T t) {
        if (t.isEmpty()) {
            throw new IllegalArgumentException("Must not be empty.");
        }
        return t;
    }

    public static <T> T d(T t) {
        return (T) e(t, "Argument must not be null");
    }

    public static <T> T e(T t, String str) {
        Objects.requireNonNull(t, str);
        return t;
    }
}
