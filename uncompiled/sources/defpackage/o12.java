package defpackage;

import defpackage.a21;

/* compiled from: LockedResource.java */
/* renamed from: o12  reason: default package */
/* loaded from: classes.dex */
public final class o12<Z> implements s73<Z>, a21.f {
    public static final et2<o12<?>> i0 = a21.d(20, new a());
    public final et3 a = et3.a();
    public s73<Z> f0;
    public boolean g0;
    public boolean h0;

    /* compiled from: LockedResource.java */
    /* renamed from: o12$a */
    /* loaded from: classes.dex */
    public class a implements a21.d<o12<?>> {
        @Override // defpackage.a21.d
        /* renamed from: b */
        public o12<?> a() {
            return new o12<>();
        }
    }

    public static <Z> o12<Z> e(s73<Z> s73Var) {
        o12<Z> o12Var = (o12) wt2.d(i0.b());
        o12Var.c(s73Var);
        return o12Var;
    }

    @Override // defpackage.s73
    public int a() {
        return this.f0.a();
    }

    @Override // defpackage.s73
    public synchronized void b() {
        this.a.c();
        this.h0 = true;
        if (!this.g0) {
            this.f0.b();
            f();
        }
    }

    public final void c(s73<Z> s73Var) {
        this.h0 = false;
        this.g0 = true;
        this.f0 = s73Var;
    }

    @Override // defpackage.s73
    public Class<Z> d() {
        return this.f0.d();
    }

    public final void f() {
        this.f0 = null;
        i0.a(this);
    }

    @Override // defpackage.a21.f
    public et3 g() {
        return this.a;
    }

    @Override // defpackage.s73
    public Z get() {
        return this.f0.get();
    }

    public synchronized void h() {
        this.a.c();
        if (this.g0) {
            this.g0 = false;
            if (this.h0) {
                b();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }
}
