package defpackage;

import com.zendesk.logger.Logger;

/* compiled from: SafeZendeskCallback.java */
/* renamed from: zb3  reason: default package */
/* loaded from: classes2.dex */
public class zb3<T> extends rs4<T> {
    public boolean a = false;
    public final rs4<T> b;

    public zb3(rs4<T> rs4Var) {
        this.b = rs4Var;
    }

    public static <T> zb3<T> a(rs4<T> rs4Var) {
        return new zb3<>(rs4Var);
    }

    public void cancel() {
        this.a = true;
    }

    @Override // defpackage.rs4
    public void onError(cw0 cw0Var) {
        rs4<T> rs4Var;
        if (!this.a && (rs4Var = this.b) != null) {
            rs4Var.onError(cw0Var);
        } else {
            Logger.c("SafeZendeskCallback", cw0Var);
        }
    }

    @Override // defpackage.rs4
    public void onSuccess(T t) {
        rs4<T> rs4Var;
        if (!this.a && (rs4Var = this.b) != null) {
            rs4Var.onSuccess(t);
        } else {
            Logger.k("SafeZendeskCallback", "Operation was a success but callback is null or was cancelled", new Object[0]);
        }
    }
}
