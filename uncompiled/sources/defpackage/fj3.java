package defpackage;

import androidx.media3.common.j;
import defpackage.gc4;
import java.util.List;

/* compiled from: SeiReader.java */
/* renamed from: fj3  reason: default package */
/* loaded from: classes.dex */
public final class fj3 {
    public final List<j> a;
    public final f84[] b;

    public fj3(List<j> list) {
        this.a = list;
        this.b = new f84[list.size()];
    }

    public void a(long j, op2 op2Var) {
        uw.a(j, op2Var, this.b);
    }

    public void b(r11 r11Var, gc4.d dVar) {
        for (int i = 0; i < this.b.length; i++) {
            dVar.a();
            f84 f = r11Var.f(dVar.c(), 3);
            j jVar = this.a.get(i);
            String str = jVar.p0;
            ii.b("application/cea-608".equals(str) || "application/cea-708".equals(str), "Invalid closed caption mime type provided: " + str);
            String str2 = jVar.a;
            if (str2 == null) {
                str2 = dVar.b();
            }
            f.f(new j.b().S(str2).e0(str).g0(jVar.h0).V(jVar.g0).F(jVar.H0).T(jVar.r0).E());
            this.b[i] = f;
        }
    }
}
