package defpackage;

import android.content.Context;
import android.graphics.Rect;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import androidx.constraintlayout.motion.widget.b;
import androidx.constraintlayout.motion.widget.c;
import androidx.constraintlayout.motion.widget.d;
import androidx.constraintlayout.motion.widget.e;
import androidx.constraintlayout.motion.widget.f;
import androidx.constraintlayout.widget.ConstraintAttribute;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.a;
import com.github.mikephil.charting.utils.Utils;
import defpackage.ak4;
import defpackage.gk4;
import defpackage.kj4;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: MotionController.java */
/* renamed from: u92  reason: default package */
/* loaded from: classes.dex */
public class u92 {
    public HashMap<String, ak4> A;
    public HashMap<String, kj4> B;
    public f[] C;
    public int D;
    public int E;
    public View F;
    public int G;
    public float H;
    public Interpolator I;
    public boolean J;
    public View b;
    public int c;
    public bc0[] i;
    public bc0 j;
    public float n;
    public float o;
    public int[] p;
    public double[] q;
    public double[] r;
    public String[] s;
    public int[] t;
    public HashMap<String, gk4> z;
    public Rect a = new Rect();
    public int d = -1;
    public x92 e = new x92();
    public x92 f = new x92();
    public t92 g = new t92();
    public t92 h = new t92();
    public float k = Float.NaN;
    public float l = Utils.FLOAT_EPSILON;
    public float m = 1.0f;
    public int u = 4;
    public float[] v = new float[4];
    public ArrayList<x92> w = new ArrayList<>();
    public float[] x = new float[1];
    public ArrayList<androidx.constraintlayout.motion.widget.a> y = new ArrayList<>();

    /* compiled from: MotionController.java */
    /* renamed from: u92$a */
    /* loaded from: classes.dex */
    public static class a implements Interpolator {
        public final /* synthetic */ yt0 a;

        public a(yt0 yt0Var) {
            this.a = yt0Var;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            return (float) this.a.a(f);
        }
    }

    public u92(View view) {
        int i = androidx.constraintlayout.motion.widget.a.f;
        this.D = i;
        this.E = i;
        this.F = null;
        this.G = i;
        this.H = Float.NaN;
        this.I = null;
        this.J = false;
        G(view);
    }

    public static Interpolator p(Context context, int i, String str, int i2) {
        if (i != -2) {
            if (i != -1) {
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 4) {
                                if (i != 5) {
                                    return null;
                                }
                                return new OvershootInterpolator();
                            }
                            return new BounceInterpolator();
                        }
                        return new DecelerateInterpolator();
                    }
                    return new AccelerateInterpolator();
                }
                return new AccelerateDecelerateInterpolator();
            }
            return new a(yt0.c(str));
        }
        return AnimationUtils.loadInterpolator(context, i2);
    }

    public void A(View view) {
        x92 x92Var = this.e;
        x92Var.g0 = Utils.FLOAT_EPSILON;
        x92Var.h0 = Utils.FLOAT_EPSILON;
        this.J = true;
        x92Var.v(view.getX(), view.getY(), view.getWidth(), view.getHeight());
        this.f.v(view.getX(), view.getY(), view.getWidth(), view.getHeight());
        this.g.o(view);
        this.h.o(view);
    }

    public void B(Rect rect, androidx.constraintlayout.widget.a aVar, int i, int i2) {
        int i3 = aVar.c;
        if (i3 != 0) {
            z(rect, this.a, i3, i, i2);
            rect = this.a;
        }
        x92 x92Var = this.f;
        x92Var.g0 = 1.0f;
        x92Var.h0 = 1.0f;
        y(x92Var);
        this.f.v(rect.left, rect.top, rect.width(), rect.height());
        this.f.a(aVar.z(this.c));
        this.h.l(rect, aVar, i3, this.c);
    }

    public void C(int i) {
        this.D = i;
    }

    public void D(View view) {
        x92 x92Var = this.e;
        x92Var.g0 = Utils.FLOAT_EPSILON;
        x92Var.h0 = Utils.FLOAT_EPSILON;
        x92Var.v(view.getX(), view.getY(), view.getWidth(), view.getHeight());
        this.g.o(view);
    }

    public void E(bk4 bk4Var, View view, int i, int i2, int i3) {
        x92 x92Var = this.e;
        x92Var.g0 = Utils.FLOAT_EPSILON;
        x92Var.h0 = Utils.FLOAT_EPSILON;
        Rect rect = new Rect();
        if (i == 1) {
            int i4 = bk4Var.b + bk4Var.d;
            rect.left = ((bk4Var.c + bk4Var.e) - bk4Var.b()) / 2;
            rect.top = i2 - ((i4 + bk4Var.a()) / 2);
            rect.right = rect.left + bk4Var.b();
            rect.bottom = rect.top + bk4Var.a();
        } else if (i == 2) {
            int i5 = bk4Var.b + bk4Var.d;
            rect.left = i3 - (((bk4Var.c + bk4Var.e) + bk4Var.b()) / 2);
            rect.top = (i5 - bk4Var.a()) / 2;
            rect.right = rect.left + bk4Var.b();
            rect.bottom = rect.top + bk4Var.a();
        }
        this.e.v(rect.left, rect.top, rect.width(), rect.height());
        this.g.k(rect, view, i, bk4Var.a);
    }

    public void F(Rect rect, androidx.constraintlayout.widget.a aVar, int i, int i2) {
        int i3 = aVar.c;
        if (i3 != 0) {
            z(rect, this.a, i3, i, i2);
        }
        x92 x92Var = this.e;
        x92Var.g0 = Utils.FLOAT_EPSILON;
        x92Var.h0 = Utils.FLOAT_EPSILON;
        y(x92Var);
        this.e.v(rect.left, rect.top, rect.width(), rect.height());
        a.C0021a z = aVar.z(this.c);
        this.e.a(z);
        this.k = z.d.g;
        this.g.l(rect, aVar, i3, this.c);
        this.E = z.f.i;
        a.c cVar = z.d;
        this.G = cVar.k;
        this.H = cVar.j;
        Context context = this.b.getContext();
        a.c cVar2 = z.d;
        this.I = p(context, cVar2.m, cVar2.l, cVar2.n);
    }

    public void G(View view) {
        this.b = view;
        this.c = view.getId();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.LayoutParams) {
            ((ConstraintLayout.LayoutParams) layoutParams).a();
        }
    }

    public void H(int i, int i2, float f, long j) {
        ArrayList arrayList;
        String[] strArr;
        ConstraintAttribute constraintAttribute;
        gk4 h;
        ConstraintAttribute constraintAttribute2;
        Integer num;
        ak4 g;
        ConstraintAttribute constraintAttribute3;
        new HashSet();
        HashSet<String> hashSet = new HashSet<>();
        HashSet<String> hashSet2 = new HashSet<>();
        HashSet<String> hashSet3 = new HashSet<>();
        HashMap<String, Integer> hashMap = new HashMap<>();
        int i3 = this.D;
        if (i3 != androidx.constraintlayout.motion.widget.a.f) {
            this.e.n0 = i3;
        }
        this.g.h(this.h, hashSet2);
        ArrayList<androidx.constraintlayout.motion.widget.a> arrayList2 = this.y;
        if (arrayList2 != null) {
            Iterator<androidx.constraintlayout.motion.widget.a> it = arrayList2.iterator();
            arrayList = null;
            while (it.hasNext()) {
                androidx.constraintlayout.motion.widget.a next = it.next();
                if (next instanceof d) {
                    d dVar = (d) next;
                    w(new x92(i, i2, dVar, this.e, this.f));
                    int i4 = dVar.g;
                    if (i4 != androidx.constraintlayout.motion.widget.a.f) {
                        this.d = i4;
                    }
                } else if (next instanceof c) {
                    next.d(hashSet3);
                } else if (next instanceof e) {
                    next.d(hashSet);
                } else if (next instanceof f) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add((f) next);
                } else {
                    next.h(hashMap);
                    next.d(hashSet2);
                }
            }
        } else {
            arrayList = null;
        }
        int i5 = 0;
        if (arrayList != null) {
            this.C = (f[]) arrayList.toArray(new f[0]);
        }
        char c = 1;
        if (!hashSet2.isEmpty()) {
            this.A = new HashMap<>();
            Iterator<String> it2 = hashSet2.iterator();
            while (it2.hasNext()) {
                String next2 = it2.next();
                if (next2.startsWith("CUSTOM,")) {
                    SparseArray sparseArray = new SparseArray();
                    String str = next2.split(",")[c];
                    Iterator<androidx.constraintlayout.motion.widget.a> it3 = this.y.iterator();
                    while (it3.hasNext()) {
                        androidx.constraintlayout.motion.widget.a next3 = it3.next();
                        HashMap<String, ConstraintAttribute> hashMap2 = next3.e;
                        if (hashMap2 != null && (constraintAttribute3 = hashMap2.get(str)) != null) {
                            sparseArray.append(next3.a, constraintAttribute3);
                        }
                    }
                    g = ak4.f(next2, sparseArray);
                } else {
                    g = ak4.g(next2);
                }
                if (g != null) {
                    g.d(next2);
                    this.A.put(next2, g);
                }
                c = 1;
            }
            ArrayList<androidx.constraintlayout.motion.widget.a> arrayList3 = this.y;
            if (arrayList3 != null) {
                Iterator<androidx.constraintlayout.motion.widget.a> it4 = arrayList3.iterator();
                while (it4.hasNext()) {
                    androidx.constraintlayout.motion.widget.a next4 = it4.next();
                    if (next4 instanceof b) {
                        next4.a(this.A);
                    }
                }
            }
            this.g.a(this.A, 0);
            this.h.a(this.A, 100);
            for (String str2 : this.A.keySet()) {
                int intValue = (!hashMap.containsKey(str2) || (num = hashMap.get(str2)) == null) ? 0 : num.intValue();
                ak4 ak4Var = this.A.get(str2);
                if (ak4Var != null) {
                    ak4Var.e(intValue);
                }
            }
        }
        if (!hashSet.isEmpty()) {
            if (this.z == null) {
                this.z = new HashMap<>();
            }
            Iterator<String> it5 = hashSet.iterator();
            while (it5.hasNext()) {
                String next5 = it5.next();
                if (!this.z.containsKey(next5)) {
                    if (next5.startsWith("CUSTOM,")) {
                        SparseArray sparseArray2 = new SparseArray();
                        String str3 = next5.split(",")[1];
                        Iterator<androidx.constraintlayout.motion.widget.a> it6 = this.y.iterator();
                        while (it6.hasNext()) {
                            androidx.constraintlayout.motion.widget.a next6 = it6.next();
                            HashMap<String, ConstraintAttribute> hashMap3 = next6.e;
                            if (hashMap3 != null && (constraintAttribute2 = hashMap3.get(str3)) != null) {
                                sparseArray2.append(next6.a, constraintAttribute2);
                            }
                        }
                        h = gk4.g(next5, sparseArray2);
                    } else {
                        h = gk4.h(next5, j);
                    }
                    if (h != null) {
                        h.d(next5);
                        this.z.put(next5, h);
                    }
                }
            }
            ArrayList<androidx.constraintlayout.motion.widget.a> arrayList4 = this.y;
            if (arrayList4 != null) {
                Iterator<androidx.constraintlayout.motion.widget.a> it7 = arrayList4.iterator();
                while (it7.hasNext()) {
                    androidx.constraintlayout.motion.widget.a next7 = it7.next();
                    if (next7 instanceof e) {
                        ((e) next7).U(this.z);
                    }
                }
            }
            for (String str4 : this.z.keySet()) {
                this.z.get(str4).e(hashMap.containsKey(str4) ? hashMap.get(str4).intValue() : 0);
            }
        }
        int i6 = 2;
        int size = this.w.size() + 2;
        x92[] x92VarArr = new x92[size];
        x92VarArr[0] = this.e;
        x92VarArr[size - 1] = this.f;
        if (this.w.size() > 0 && this.d == -1) {
            this.d = 0;
        }
        Iterator<x92> it8 = this.w.iterator();
        int i7 = 1;
        while (it8.hasNext()) {
            x92VarArr[i7] = it8.next();
            i7++;
        }
        HashSet hashSet4 = new HashSet();
        for (String str5 : this.f.r0.keySet()) {
            if (this.e.r0.containsKey(str5)) {
                if (!hashSet2.contains("CUSTOM," + str5)) {
                    hashSet4.add(str5);
                }
            }
        }
        String[] strArr2 = (String[]) hashSet4.toArray(new String[0]);
        this.s = strArr2;
        this.t = new int[strArr2.length];
        int i8 = 0;
        while (true) {
            strArr = this.s;
            if (i8 >= strArr.length) {
                break;
            }
            String str6 = strArr[i8];
            this.t[i8] = 0;
            int i9 = 0;
            while (true) {
                if (i9 >= size) {
                    break;
                }
                if (x92VarArr[i9].r0.containsKey(str6) && (constraintAttribute = x92VarArr[i9].r0.get(str6)) != null) {
                    int[] iArr = this.t;
                    iArr[i8] = iArr[i8] + constraintAttribute.h();
                    break;
                }
                i9++;
            }
            i8++;
        }
        boolean z = x92VarArr[0].n0 != androidx.constraintlayout.motion.widget.a.f;
        int length = 18 + strArr.length;
        boolean[] zArr = new boolean[length];
        for (int i10 = 1; i10 < size; i10++) {
            x92VarArr[i10].f(x92VarArr[i10 - 1], zArr, this.s, z);
        }
        int i11 = 0;
        for (int i12 = 1; i12 < length; i12++) {
            if (zArr[i12]) {
                i11++;
            }
        }
        this.p = new int[i11];
        int max = Math.max(2, i11);
        this.q = new double[max];
        this.r = new double[max];
        int i13 = 0;
        for (int i14 = 1; i14 < length; i14++) {
            if (zArr[i14]) {
                this.p[i13] = i14;
                i13++;
            }
        }
        double[][] dArr = (double[][]) Array.newInstance(double.class, size, this.p.length);
        double[] dArr2 = new double[size];
        for (int i15 = 0; i15 < size; i15++) {
            x92VarArr[i15].g(dArr[i15], this.p);
            dArr2[i15] = x92VarArr[i15].g0;
        }
        int i16 = 0;
        while (true) {
            int[] iArr2 = this.p;
            if (i16 >= iArr2.length) {
                break;
            }
            if (iArr2[i16] < x92.v0.length) {
                String str7 = x92.v0[this.p[i16]] + " [";
                for (int i17 = 0; i17 < size; i17++) {
                    str7 = str7 + dArr[i17][i16];
                }
            }
            i16++;
        }
        this.i = new bc0[this.s.length + 1];
        int i18 = 0;
        while (true) {
            String[] strArr3 = this.s;
            if (i18 >= strArr3.length) {
                break;
            }
            String str8 = strArr3[i18];
            int i19 = i5;
            int i20 = i19;
            double[] dArr3 = null;
            double[][] dArr4 = null;
            while (i19 < size) {
                if (x92VarArr[i19].p(str8)) {
                    if (dArr4 == null) {
                        dArr3 = new double[size];
                        int[] iArr3 = new int[i6];
                        iArr3[1] = x92VarArr[i19].l(str8);
                        iArr3[i5] = size;
                        dArr4 = (double[][]) Array.newInstance(double.class, iArr3);
                    }
                    dArr3[i20] = x92VarArr[i19].g0;
                    x92VarArr[i19].k(str8, dArr4[i20], 0);
                    i20++;
                }
                i19++;
                i6 = 2;
                i5 = 0;
            }
            i18++;
            this.i[i18] = bc0.a(this.d, Arrays.copyOf(dArr3, i20), (double[][]) Arrays.copyOf(dArr4, i20));
            i6 = 2;
            i5 = 0;
        }
        this.i[0] = bc0.a(this.d, dArr2, dArr);
        if (x92VarArr[0].n0 != androidx.constraintlayout.motion.widget.a.f) {
            int[] iArr4 = new int[size];
            double[] dArr5 = new double[size];
            double[][] dArr6 = (double[][]) Array.newInstance(double.class, size, 2);
            for (int i21 = 0; i21 < size; i21++) {
                iArr4[i21] = x92VarArr[i21].n0;
                dArr5[i21] = x92VarArr[i21].g0;
                dArr6[i21][0] = x92VarArr[i21].i0;
                dArr6[i21][1] = x92VarArr[i21].j0;
            }
            this.j = bc0.b(iArr4, dArr5, dArr6);
        }
        float f2 = Float.NaN;
        this.B = new HashMap<>();
        if (this.y != null) {
            Iterator<String> it9 = hashSet3.iterator();
            while (it9.hasNext()) {
                String next8 = it9.next();
                kj4 i22 = kj4.i(next8);
                if (i22 != null) {
                    if (i22.h() && Float.isNaN(f2)) {
                        f2 = s();
                    }
                    i22.f(next8);
                    this.B.put(next8, i22);
                }
            }
            Iterator<androidx.constraintlayout.motion.widget.a> it10 = this.y.iterator();
            while (it10.hasNext()) {
                androidx.constraintlayout.motion.widget.a next9 = it10.next();
                if (next9 instanceof c) {
                    ((c) next9).Y(this.B);
                }
            }
            for (kj4 kj4Var : this.B.values()) {
                kj4Var.g(f2);
            }
        }
    }

    public void I(u92 u92Var) {
        this.e.A(u92Var, u92Var.e);
        this.f.A(u92Var, u92Var.f);
    }

    public void a(androidx.constraintlayout.motion.widget.a aVar) {
        this.y.add(aVar);
    }

    public void b(ArrayList<androidx.constraintlayout.motion.widget.a> arrayList) {
        this.y.addAll(arrayList);
    }

    public int c(float[] fArr, int[] iArr) {
        if (fArr != null) {
            double[] h = this.i[0].h();
            if (iArr != null) {
                Iterator<x92> it = this.w.iterator();
                int i = 0;
                while (it.hasNext()) {
                    iArr[i] = it.next().s0;
                    i++;
                }
            }
            int i2 = 0;
            for (int i3 = 0; i3 < h.length; i3++) {
                this.i[0].d(h[i3], this.q);
                this.e.h(h[i3], this.p, this.q, fArr, i2);
                i2 += 2;
            }
            return i2 / 2;
        }
        return 0;
    }

    public void d(float[] fArr, int i) {
        double d;
        float f;
        float f2 = 1.0f;
        float f3 = 1.0f / (i - 1);
        HashMap<String, ak4> hashMap = this.A;
        ak4 ak4Var = hashMap == null ? null : hashMap.get("translationX");
        HashMap<String, ak4> hashMap2 = this.A;
        ak4 ak4Var2 = hashMap2 == null ? null : hashMap2.get("translationY");
        HashMap<String, kj4> hashMap3 = this.B;
        kj4 kj4Var = hashMap3 == null ? null : hashMap3.get("translationX");
        HashMap<String, kj4> hashMap4 = this.B;
        kj4 kj4Var2 = hashMap4 != null ? hashMap4.get("translationY") : null;
        int i2 = 0;
        while (i2 < i) {
            float f4 = i2 * f3;
            float f5 = this.m;
            if (f5 != f2) {
                float f6 = this.l;
                if (f4 < f6) {
                    f4 = Utils.FLOAT_EPSILON;
                }
                if (f4 > f6 && f4 < 1.0d) {
                    f4 = Math.min((f4 - f6) * f5, f2);
                }
            }
            float f7 = f4;
            double d2 = f7;
            yt0 yt0Var = this.e.a;
            float f8 = Float.NaN;
            Iterator<x92> it = this.w.iterator();
            float f9 = Utils.FLOAT_EPSILON;
            while (it.hasNext()) {
                x92 next = it.next();
                yt0 yt0Var2 = next.a;
                double d3 = d2;
                if (yt0Var2 != null) {
                    float f10 = next.g0;
                    if (f10 < f7) {
                        f9 = f10;
                        yt0Var = yt0Var2;
                    } else if (Float.isNaN(f8)) {
                        f8 = next.g0;
                    }
                }
                d2 = d3;
            }
            double d4 = d2;
            if (yt0Var != null) {
                if (Float.isNaN(f8)) {
                    f8 = 1.0f;
                }
                d = (((float) yt0Var.a((f7 - f9) / f)) * (f8 - f9)) + f9;
            } else {
                d = d4;
            }
            this.i[0].d(d, this.q);
            bc0 bc0Var = this.j;
            if (bc0Var != null) {
                double[] dArr = this.q;
                if (dArr.length > 0) {
                    bc0Var.d(d, dArr);
                }
            }
            int i3 = i2 * 2;
            int i4 = i2;
            this.e.h(d, this.p, this.q, fArr, i3);
            if (kj4Var != null) {
                fArr[i3] = fArr[i3] + kj4Var.a(f7);
            } else if (ak4Var != null) {
                fArr[i3] = fArr[i3] + ak4Var.a(f7);
            }
            if (kj4Var2 != null) {
                int i5 = i3 + 1;
                fArr[i5] = fArr[i5] + kj4Var2.a(f7);
            } else if (ak4Var2 != null) {
                int i6 = i3 + 1;
                fArr[i6] = fArr[i6] + ak4Var2.a(f7);
            }
            i2 = i4 + 1;
            f2 = 1.0f;
        }
    }

    public void e(float f, float[] fArr, int i) {
        this.i[0].d(g(f, null), this.q);
        this.e.o(this.p, this.q, fArr, i);
    }

    public void f(boolean z) {
        if (!"button".equals(xe0.d(this.b)) || this.C == null) {
            return;
        }
        int i = 0;
        while (true) {
            f[] fVarArr = this.C;
            if (i >= fVarArr.length) {
                return;
            }
            fVarArr[i].y(z ? -100.0f : 100.0f, this.b);
            i++;
        }
    }

    public final float g(float f, float[] fArr) {
        float f2 = Utils.FLOAT_EPSILON;
        if (fArr != null) {
            fArr[0] = 1.0f;
        } else {
            float f3 = this.m;
            if (f3 != 1.0d) {
                float f4 = this.l;
                if (f < f4) {
                    f = 0.0f;
                }
                if (f > f4 && f < 1.0d) {
                    f = Math.min((f - f4) * f3, 1.0f);
                }
            }
        }
        yt0 yt0Var = this.e.a;
        float f5 = Float.NaN;
        Iterator<x92> it = this.w.iterator();
        while (it.hasNext()) {
            x92 next = it.next();
            yt0 yt0Var2 = next.a;
            if (yt0Var2 != null) {
                float f6 = next.g0;
                if (f6 < f) {
                    yt0Var = yt0Var2;
                    f2 = f6;
                } else if (Float.isNaN(f5)) {
                    f5 = next.g0;
                }
            }
        }
        if (yt0Var != null) {
            float f7 = (Float.isNaN(f5) ? 1.0f : f5) - f2;
            double d = (f - f2) / f7;
            f = (((float) yt0Var.a(d)) * f7) + f2;
            if (fArr != null) {
                fArr[0] = (float) yt0Var.b(d);
            }
        }
        return f;
    }

    public int h() {
        return this.e.o0;
    }

    public void i(double d, float[] fArr, float[] fArr2) {
        double[] dArr = new double[4];
        double[] dArr2 = new double[4];
        this.i[0].d(d, dArr);
        this.i[0].g(d, dArr2);
        Arrays.fill(fArr2, (float) Utils.FLOAT_EPSILON);
        this.e.j(d, this.p, dArr, fArr, dArr2, fArr2);
    }

    public float j() {
        return this.n;
    }

    public float k() {
        return this.o;
    }

    public void l(float f, float f2, float f3, float[] fArr) {
        double[] dArr;
        float g = g(f, this.x);
        bc0[] bc0VarArr = this.i;
        int i = 0;
        if (bc0VarArr != null) {
            double d = g;
            bc0VarArr[0].g(d, this.r);
            this.i[0].d(d, this.q);
            float f4 = this.x[0];
            while (true) {
                dArr = this.r;
                if (i >= dArr.length) {
                    break;
                }
                dArr[i] = dArr[i] * f4;
                i++;
            }
            bc0 bc0Var = this.j;
            if (bc0Var != null) {
                double[] dArr2 = this.q;
                if (dArr2.length > 0) {
                    bc0Var.d(d, dArr2);
                    this.j.g(d, this.r);
                    this.e.x(f2, f3, fArr, this.p, this.r, this.q);
                    return;
                }
                return;
            }
            this.e.x(f2, f3, fArr, this.p, dArr, this.q);
            return;
        }
        x92 x92Var = this.f;
        float f5 = x92Var.i0;
        x92 x92Var2 = this.e;
        float f6 = f5 - x92Var2.i0;
        float f7 = x92Var.j0 - x92Var2.j0;
        float f8 = (x92Var.k0 - x92Var2.k0) + f6;
        float f9 = (x92Var.l0 - x92Var2.l0) + f7;
        fArr[0] = (f6 * (1.0f - f2)) + (f8 * f2);
        fArr[1] = (f7 * (1.0f - f3)) + (f9 * f3);
    }

    public int m() {
        int i = this.e.f0;
        Iterator<x92> it = this.w.iterator();
        while (it.hasNext()) {
            i = Math.max(i, it.next().f0);
        }
        return Math.max(i, this.f.f0);
    }

    public float n() {
        return this.f.i0;
    }

    public float o() {
        return this.f.j0;
    }

    public x92 q(int i) {
        return this.w.get(i);
    }

    public void r(float f, int i, int i2, float f2, float f3, float[] fArr) {
        float g = g(f, this.x);
        HashMap<String, ak4> hashMap = this.A;
        ak4 ak4Var = hashMap == null ? null : hashMap.get("translationX");
        HashMap<String, ak4> hashMap2 = this.A;
        ak4 ak4Var2 = hashMap2 == null ? null : hashMap2.get("translationY");
        HashMap<String, ak4> hashMap3 = this.A;
        ak4 ak4Var3 = hashMap3 == null ? null : hashMap3.get("rotation");
        HashMap<String, ak4> hashMap4 = this.A;
        ak4 ak4Var4 = hashMap4 == null ? null : hashMap4.get("scaleX");
        HashMap<String, ak4> hashMap5 = this.A;
        ak4 ak4Var5 = hashMap5 == null ? null : hashMap5.get("scaleY");
        HashMap<String, kj4> hashMap6 = this.B;
        kj4 kj4Var = hashMap6 == null ? null : hashMap6.get("translationX");
        HashMap<String, kj4> hashMap7 = this.B;
        kj4 kj4Var2 = hashMap7 == null ? null : hashMap7.get("translationY");
        HashMap<String, kj4> hashMap8 = this.B;
        kj4 kj4Var3 = hashMap8 == null ? null : hashMap8.get("rotation");
        HashMap<String, kj4> hashMap9 = this.B;
        kj4 kj4Var4 = hashMap9 == null ? null : hashMap9.get("scaleX");
        HashMap<String, kj4> hashMap10 = this.B;
        kj4 kj4Var5 = hashMap10 != null ? hashMap10.get("scaleY") : null;
        fh4 fh4Var = new fh4();
        fh4Var.b();
        fh4Var.d(ak4Var3, g);
        fh4Var.h(ak4Var, ak4Var2, g);
        fh4Var.f(ak4Var4, ak4Var5, g);
        fh4Var.c(kj4Var3, g);
        fh4Var.g(kj4Var, kj4Var2, g);
        fh4Var.e(kj4Var4, kj4Var5, g);
        bc0 bc0Var = this.j;
        if (bc0Var != null) {
            double[] dArr = this.q;
            if (dArr.length > 0) {
                double d = g;
                bc0Var.d(d, dArr);
                this.j.g(d, this.r);
                this.e.x(f2, f3, fArr, this.p, this.r, this.q);
            }
            fh4Var.a(f2, f3, i, i2, fArr);
            return;
        }
        int i3 = 0;
        if (this.i != null) {
            double g2 = g(g, this.x);
            this.i[0].g(g2, this.r);
            this.i[0].d(g2, this.q);
            float f4 = this.x[0];
            while (true) {
                double[] dArr2 = this.r;
                if (i3 < dArr2.length) {
                    dArr2[i3] = dArr2[i3] * f4;
                    i3++;
                } else {
                    this.e.x(f2, f3, fArr, this.p, dArr2, this.q);
                    fh4Var.a(f2, f3, i, i2, fArr);
                    return;
                }
            }
        } else {
            x92 x92Var = this.f;
            float f5 = x92Var.i0;
            x92 x92Var2 = this.e;
            float f6 = f5 - x92Var2.i0;
            float f7 = x92Var.j0 - x92Var2.j0;
            kj4 kj4Var6 = kj4Var4;
            float f8 = (x92Var.l0 - x92Var2.l0) + f7;
            fArr[0] = (f6 * (1.0f - f2)) + (((x92Var.k0 - x92Var2.k0) + f6) * f2);
            fArr[1] = (f7 * (1.0f - f3)) + (f8 * f3);
            fh4Var.b();
            fh4Var.d(ak4Var3, g);
            fh4Var.h(ak4Var, ak4Var2, g);
            fh4Var.f(ak4Var4, ak4Var5, g);
            fh4Var.c(kj4Var3, g);
            fh4Var.g(kj4Var, kj4Var2, g);
            fh4Var.e(kj4Var6, kj4Var5, g);
            fh4Var.a(f2, f3, i, i2, fArr);
        }
    }

    public final float s() {
        char c;
        float f;
        float f2;
        float[] fArr = new float[2];
        float f3 = 1.0f / 99;
        double d = 0.0d;
        double d2 = 0.0d;
        float f4 = Utils.FLOAT_EPSILON;
        int i = 0;
        while (i < 100) {
            float f5 = i * f3;
            double d3 = f5;
            yt0 yt0Var = this.e.a;
            Iterator<x92> it = this.w.iterator();
            float f6 = Float.NaN;
            float f7 = Utils.FLOAT_EPSILON;
            while (it.hasNext()) {
                x92 next = it.next();
                yt0 yt0Var2 = next.a;
                if (yt0Var2 != null) {
                    float f8 = next.g0;
                    if (f8 < f5) {
                        yt0Var = yt0Var2;
                        f7 = f8;
                    } else if (Float.isNaN(f6)) {
                        f6 = next.g0;
                    }
                }
            }
            if (yt0Var != null) {
                if (Float.isNaN(f6)) {
                    f6 = 1.0f;
                }
                d3 = (((float) yt0Var.a((f5 - f7) / f2)) * (f6 - f7)) + f7;
            }
            this.i[0].d(d3, this.q);
            float f9 = f4;
            int i2 = i;
            this.e.h(d3, this.p, this.q, fArr, 0);
            if (i2 > 0) {
                c = 0;
                f = (float) (f9 + Math.hypot(d2 - fArr[1], d - fArr[0]));
            } else {
                c = 0;
                f = f9;
            }
            d = fArr[c];
            i = i2 + 1;
            f4 = f;
            d2 = fArr[1];
        }
        return f4;
    }

    public float t() {
        return this.e.i0;
    }

    public String toString() {
        return " start: x: " + this.e.i0 + " y: " + this.e.j0 + " end: x: " + this.f.i0 + " y: " + this.f.j0;
    }

    public float u() {
        return this.e.j0;
    }

    public View v() {
        return this.b;
    }

    public final void w(x92 x92Var) {
        int binarySearch = Collections.binarySearch(this.w, x92Var);
        if (binarySearch == 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(" KeyPath position \"");
            sb.append(x92Var.h0);
            sb.append("\" outside of range");
        }
        this.w.add((-binarySearch) - 1, x92Var);
    }

    public boolean x(View view, float f, long j, gx1 gx1Var) {
        gk4.d dVar;
        boolean z;
        int i;
        double d;
        View view2;
        float f2;
        float g = g(f, null);
        int i2 = this.G;
        if (i2 != androidx.constraintlayout.motion.widget.a.f) {
            float f3 = 1.0f / i2;
            float floor = ((float) Math.floor(g / f3)) * f3;
            float f4 = (g % f3) / f3;
            if (!Float.isNaN(this.H)) {
                f4 = (f4 + this.H) % 1.0f;
            }
            Interpolator interpolator = this.I;
            if (interpolator != null) {
                f2 = interpolator.getInterpolation(f4);
            } else {
                f2 = ((double) f4) > 0.5d ? 1.0f : Utils.FLOAT_EPSILON;
            }
            g = (f2 * f3) + floor;
        }
        float f5 = g;
        HashMap<String, ak4> hashMap = this.A;
        if (hashMap != null) {
            for (ak4 ak4Var : hashMap.values()) {
                ak4Var.h(view, f5);
            }
        }
        HashMap<String, gk4> hashMap2 = this.z;
        if (hashMap2 != null) {
            dVar = null;
            boolean z2 = false;
            for (gk4 gk4Var : hashMap2.values()) {
                if (gk4Var instanceof gk4.d) {
                    dVar = (gk4.d) gk4Var;
                } else {
                    z2 |= gk4Var.i(view, f5, j, gx1Var);
                }
            }
            z = z2;
        } else {
            dVar = null;
            z = false;
        }
        bc0[] bc0VarArr = this.i;
        if (bc0VarArr != null) {
            double d2 = f5;
            bc0VarArr[0].d(d2, this.q);
            this.i[0].g(d2, this.r);
            bc0 bc0Var = this.j;
            if (bc0Var != null) {
                double[] dArr = this.q;
                if (dArr.length > 0) {
                    bc0Var.d(d2, dArr);
                    this.j.g(d2, this.r);
                }
            }
            if (this.J) {
                d = d2;
            } else {
                d = d2;
                this.e.y(f5, view, this.p, this.q, this.r, null);
            }
            if (this.E != androidx.constraintlayout.motion.widget.a.f) {
                if (this.F == null) {
                    this.F = ((View) view.getParent()).findViewById(this.E);
                }
                if (this.F != null) {
                    float top = (view2.getTop() + this.F.getBottom()) / 2.0f;
                    float left = (this.F.getLeft() + this.F.getRight()) / 2.0f;
                    if (view.getRight() - view.getLeft() > 0 && view.getBottom() - view.getTop() > 0) {
                        view.setPivotX(left - view.getLeft());
                        view.setPivotY(top - view.getTop());
                    }
                }
            }
            HashMap<String, ak4> hashMap3 = this.A;
            if (hashMap3 != null) {
                for (ak4 ak4Var2 : hashMap3.values()) {
                    if (ak4Var2 instanceof ak4.d) {
                        double[] dArr2 = this.r;
                        if (dArr2.length > 1) {
                            ((ak4.d) ak4Var2).i(view, f5, dArr2[0], dArr2[1]);
                        }
                    }
                }
            }
            if (dVar != null) {
                double[] dArr3 = this.r;
                i = 1;
                z |= dVar.j(view, gx1Var, f5, j, dArr3[0], dArr3[1]);
            } else {
                i = 1;
            }
            int i3 = i;
            while (true) {
                bc0[] bc0VarArr2 = this.i;
                if (i3 >= bc0VarArr2.length) {
                    break;
                }
                bc0VarArr2[i3].e(d, this.v);
                this.e.r0.get(this.s[i3 - 1]).k(view, this.v);
                i3++;
            }
            t92 t92Var = this.g;
            if (t92Var.f0 == 0) {
                if (f5 <= Utils.FLOAT_EPSILON) {
                    view.setVisibility(t92Var.g0);
                } else if (f5 >= 1.0f) {
                    view.setVisibility(this.h.g0);
                } else if (this.h.g0 != t92Var.g0) {
                    view.setVisibility(0);
                }
            }
            if (this.C != null) {
                int i4 = 0;
                while (true) {
                    f[] fVarArr = this.C;
                    if (i4 >= fVarArr.length) {
                        break;
                    }
                    fVarArr[i4].y(f5, view);
                    i4++;
                }
            }
        } else {
            i = 1;
            x92 x92Var = this.e;
            float f6 = x92Var.i0;
            x92 x92Var2 = this.f;
            float f7 = f6 + ((x92Var2.i0 - f6) * f5);
            float f8 = x92Var.j0;
            float f9 = f8 + ((x92Var2.j0 - f8) * f5);
            float f10 = x92Var.k0;
            float f11 = x92Var2.k0;
            float f12 = x92Var.l0;
            float f13 = x92Var2.l0;
            float f14 = f7 + 0.5f;
            int i5 = (int) f14;
            float f15 = f9 + 0.5f;
            int i6 = (int) f15;
            int i7 = (int) (f14 + ((f11 - f10) * f5) + f10);
            int i8 = (int) (f15 + ((f13 - f12) * f5) + f12);
            int i9 = i7 - i5;
            int i10 = i8 - i6;
            if (f11 != f10 || f13 != f12) {
                view.measure(View.MeasureSpec.makeMeasureSpec(i9, 1073741824), View.MeasureSpec.makeMeasureSpec(i10, 1073741824));
            }
            view.layout(i5, i6, i7, i8);
        }
        HashMap<String, kj4> hashMap4 = this.B;
        if (hashMap4 != null) {
            for (kj4 kj4Var : hashMap4.values()) {
                if (kj4Var instanceof kj4.d) {
                    double[] dArr4 = this.r;
                    ((kj4.d) kj4Var).k(view, f5, dArr4[0], dArr4[i]);
                } else {
                    kj4Var.j(view, f5);
                }
            }
        }
        return z;
    }

    public final void y(x92 x92Var) {
        x92Var.v((int) this.b.getX(), (int) this.b.getY(), this.b.getWidth(), this.b.getHeight());
    }

    public void z(Rect rect, Rect rect2, int i, int i2, int i3) {
        if (i == 1) {
            int i4 = rect.left + rect.right;
            rect2.left = ((rect.top + rect.bottom) - rect.width()) / 2;
            rect2.top = i3 - ((i4 + rect.height()) / 2);
            rect2.right = rect2.left + rect.width();
            rect2.bottom = rect2.top + rect.height();
        } else if (i == 2) {
            int i5 = rect.left + rect.right;
            rect2.left = i2 - (((rect.top + rect.bottom) + rect.width()) / 2);
            rect2.top = (i5 - rect.height()) / 2;
            rect2.right = rect2.left + rect.width();
            rect2.bottom = rect2.top + rect.height();
        } else if (i == 3) {
            int i6 = rect.left + rect.right;
            rect2.left = ((rect.height() / 2) + rect.top) - (i6 / 2);
            rect2.top = i3 - ((i6 + rect.height()) / 2);
            rect2.right = rect2.left + rect.width();
            rect2.bottom = rect2.top + rect.height();
        } else if (i != 4) {
        } else {
            int i7 = rect.left + rect.right;
            rect2.left = i2 - (((rect.bottom + rect.top) + rect.width()) / 2);
            rect2.top = (i7 - rect.height()) / 2;
            rect2.right = rect2.left + rect.width();
            rect2.bottom = rect2.top + rect.height();
        }
    }
}
