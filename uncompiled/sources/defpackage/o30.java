package defpackage;

/* compiled from: CompletableDeferred.kt */
/* renamed from: o30  reason: default package */
/* loaded from: classes2.dex */
public final class o30<T> extends bu1 implements n30<T> {
    public o30(st1 st1Var) {
        super(true);
        d0(st1Var);
    }

    @Override // defpackage.bu1
    public boolean X() {
        return true;
    }

    @Override // defpackage.sl0
    public Object r(q70<? super T> q70Var) {
        return G(q70Var);
    }

    @Override // defpackage.n30
    public boolean u(T t) {
        return j0(t);
    }
}
