package defpackage;

import java.util.concurrent.Executor;

/* compiled from: Executors.kt */
/* renamed from: uy0  reason: default package */
/* loaded from: classes2.dex */
public final class uy0 extends ty0 {
    public final Executor g0;

    public uy0(Executor executor) {
        this.g0 = executor;
        M();
    }

    @Override // kotlinx.coroutines.ExecutorCoroutineDispatcher
    public Executor l() {
        return this.g0;
    }
}
