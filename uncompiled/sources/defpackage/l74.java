package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import defpackage.i44;

/* compiled from: TooltipDrawable.java */
/* renamed from: l74  reason: default package */
/* loaded from: classes2.dex */
public class l74 extends o42 implements i44.b {
    public CharSequence C0;
    public final Context D0;
    public final Paint.FontMetrics E0;
    public final i44 F0;
    public final View.OnLayoutChangeListener G0;
    public final Rect H0;
    public int I0;
    public int J0;
    public int K0;
    public int L0;
    public int M0;
    public int N0;
    public float O0;
    public float P0;
    public float Q0;
    public float R0;

    /* compiled from: TooltipDrawable.java */
    /* renamed from: l74$a */
    /* loaded from: classes2.dex */
    public class a implements View.OnLayoutChangeListener {
        public a() {
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            l74.this.E0(view);
        }
    }

    public l74(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.E0 = new Paint.FontMetrics();
        i44 i44Var = new i44(this);
        this.F0 = i44Var;
        this.G0 = new a();
        this.H0 = new Rect();
        this.O0 = 1.0f;
        this.P0 = 1.0f;
        this.Q0 = 0.5f;
        this.R0 = 1.0f;
        this.D0 = context;
        i44Var.e().density = context.getResources().getDisplayMetrics().density;
        i44Var.e().setTextAlign(Paint.Align.CENTER);
    }

    public static l74 u0(Context context, AttributeSet attributeSet, int i, int i2) {
        l74 l74Var = new l74(context, attributeSet, i, i2);
        l74Var.z0(attributeSet, i, i2);
        return l74Var;
    }

    public void A0(View view) {
        if (view == null) {
            return;
        }
        E0(view);
        view.addOnLayoutChangeListener(this.G0);
    }

    public void B0(float f) {
        this.Q0 = 1.2f;
        this.O0 = f;
        this.P0 = f;
        this.R0 = ne.b(Utils.FLOAT_EPSILON, 1.0f, 0.19f, 1.0f, f);
        invalidateSelf();
    }

    public void C0(CharSequence charSequence) {
        if (TextUtils.equals(this.C0, charSequence)) {
            return;
        }
        this.C0 = charSequence;
        this.F0.i(true);
        invalidateSelf();
    }

    public void D0(d44 d44Var) {
        this.F0.h(d44Var, this.D0);
    }

    public final void E0(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        this.N0 = iArr[0];
        view.getWindowVisibleDisplayFrame(this.H0);
    }

    @Override // defpackage.i44.b
    public void a() {
        invalidateSelf();
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.scale(this.O0, this.P0, getBounds().left + (getBounds().width() * 0.5f), getBounds().top + (getBounds().height() * this.Q0));
        canvas.translate(r0(), (float) (-((this.M0 * Math.sqrt(2.0d)) - this.M0)));
        super.draw(canvas);
        x0(canvas);
        canvas.restore();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return (int) Math.max(this.F0.e().getTextSize(), this.K0);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return (int) Math.max((this.I0 * 2) + y0(), this.J0);
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        setShapeAppearanceModel(D().v().s(v0()).m());
    }

    @Override // defpackage.o42, android.graphics.drawable.Drawable, defpackage.i44.b
    public boolean onStateChange(int[] iArr) {
        return super.onStateChange(iArr);
    }

    public final float r0() {
        int i;
        if (((this.H0.right - getBounds().right) - this.N0) - this.L0 < 0) {
            i = ((this.H0.right - getBounds().right) - this.N0) - this.L0;
        } else if (((this.H0.left - getBounds().left) - this.N0) + this.L0 <= 0) {
            return Utils.FLOAT_EPSILON;
        } else {
            i = ((this.H0.left - getBounds().left) - this.N0) + this.L0;
        }
        return i;
    }

    public final float s0() {
        this.F0.e().getFontMetrics(this.E0);
        Paint.FontMetrics fontMetrics = this.E0;
        return (fontMetrics.descent + fontMetrics.ascent) / 2.0f;
    }

    public final float t0(Rect rect) {
        return rect.centerY() - s0();
    }

    public final cu0 v0() {
        float width = ((float) (getBounds().width() - (this.M0 * Math.sqrt(2.0d)))) / 2.0f;
        return new ul2(new d42(this.M0), Math.min(Math.max(-r0(), -width), width));
    }

    public void w0(View view) {
        if (view == null) {
            return;
        }
        view.removeOnLayoutChangeListener(this.G0);
    }

    public final void x0(Canvas canvas) {
        if (this.C0 == null) {
            return;
        }
        Rect bounds = getBounds();
        int t0 = (int) t0(bounds);
        if (this.F0.d() != null) {
            this.F0.e().drawableState = getState();
            this.F0.j(this.D0);
            this.F0.e().setAlpha((int) (this.R0 * 255.0f));
        }
        CharSequence charSequence = this.C0;
        canvas.drawText(charSequence, 0, charSequence.length(), bounds.centerX(), t0, this.F0.e());
    }

    public final float y0() {
        CharSequence charSequence = this.C0;
        return charSequence == null ? Utils.FLOAT_EPSILON : this.F0.f(charSequence.toString());
    }

    public final void z0(AttributeSet attributeSet, int i, int i2) {
        TypedArray h = a54.h(this.D0, attributeSet, o23.Tooltip, i, i2, new int[0]);
        this.M0 = this.D0.getResources().getDimensionPixelSize(jz2.mtrl_tooltip_arrowSize);
        setShapeAppearanceModel(D().v().s(v0()).m());
        C0(h.getText(o23.Tooltip_android_text));
        D0(n42.f(this.D0, h, o23.Tooltip_android_textAppearance));
        a0(ColorStateList.valueOf(h.getColor(o23.Tooltip_backgroundTint, l42.g(z20.j(l42.c(this.D0, 16842801, l74.class.getCanonicalName()), 229), z20.j(l42.c(this.D0, gy2.colorOnBackground, l74.class.getCanonicalName()), 153)))));
        l0(ColorStateList.valueOf(l42.c(this.D0, gy2.colorSurface, l74.class.getCanonicalName())));
        this.I0 = h.getDimensionPixelSize(o23.Tooltip_android_padding, 0);
        this.J0 = h.getDimensionPixelSize(o23.Tooltip_android_minWidth, 0);
        this.K0 = h.getDimensionPixelSize(o23.Tooltip_android_minHeight, 0);
        this.L0 = h.getDimensionPixelSize(o23.Tooltip_android_layout_margin, 0);
        h.recycle();
    }
}
