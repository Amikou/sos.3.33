package defpackage;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.IOException;
import java.util.List;

/* compiled from: FrameworkSQLiteDatabase.java */
/* renamed from: fc1  reason: default package */
/* loaded from: classes.dex */
public class fc1 implements sw3 {
    public static final String[] f0 = new String[0];
    public final SQLiteDatabase a;

    /* compiled from: FrameworkSQLiteDatabase.java */
    /* renamed from: fc1$a */
    /* loaded from: classes.dex */
    public class a implements SQLiteDatabase.CursorFactory {
        public final /* synthetic */ vw3 a;

        public a(fc1 fc1Var, vw3 vw3Var) {
            this.a = vw3Var;
        }

        @Override // android.database.sqlite.SQLiteDatabase.CursorFactory
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.b(new ic1(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    /* compiled from: FrameworkSQLiteDatabase.java */
    /* renamed from: fc1$b */
    /* loaded from: classes.dex */
    public class b implements SQLiteDatabase.CursorFactory {
        public final /* synthetic */ vw3 a;

        public b(fc1 fc1Var, vw3 vw3Var) {
            this.a = vw3Var;
        }

        @Override // android.database.sqlite.SQLiteDatabase.CursorFactory
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.b(new ic1(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    public fc1(SQLiteDatabase sQLiteDatabase) {
        this.a = sQLiteDatabase;
    }

    @Override // defpackage.sw3
    public void B() {
        this.a.beginTransaction();
    }

    @Override // defpackage.sw3
    public Cursor D(vw3 vw3Var) {
        return this.a.rawQueryWithFactory(new a(this, vw3Var), vw3Var.a(), f0, null);
    }

    @Override // defpackage.sw3
    public Cursor E0(String str) {
        return D(new pp3(str));
    }

    @Override // defpackage.sw3
    public List<Pair<String, String>> G() {
        return this.a.getAttachedDbs();
    }

    @Override // defpackage.sw3
    public void K(String str) throws SQLException {
        this.a.execSQL(str);
    }

    @Override // defpackage.sw3
    public void N0() {
        this.a.endTransaction();
    }

    @Override // defpackage.sw3
    public ww3 U(String str) {
        return new jc1(this.a.compileStatement(str));
    }

    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.a == sQLiteDatabase;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    @Override // defpackage.sw3
    public String getPath() {
        return this.a.getPath();
    }

    @Override // defpackage.sw3
    public boolean h1() {
        return this.a.inTransaction();
    }

    @Override // defpackage.sw3
    public boolean isOpen() {
        return this.a.isOpen();
    }

    @Override // defpackage.sw3
    public boolean q1() {
        return nw3.d(this.a);
    }

    @Override // defpackage.sw3
    public void s0() {
        this.a.setTransactionSuccessful();
    }

    @Override // defpackage.sw3
    public Cursor t0(vw3 vw3Var, CancellationSignal cancellationSignal) {
        return nw3.e(this.a, vw3Var.a(), f0, null, cancellationSignal, new b(this, vw3Var));
    }

    @Override // defpackage.sw3
    public void u0(String str, Object[] objArr) throws SQLException {
        this.a.execSQL(str, objArr);
    }

    @Override // defpackage.sw3
    public void v0() {
        this.a.beginTransactionNonExclusive();
    }
}
