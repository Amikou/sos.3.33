package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: e46  reason: default package */
/* loaded from: classes.dex */
public final class e46 implements d46 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.collection.service.update_with_analytics_fix", false);

    @Override // defpackage.d46
    public final boolean zza() {
        return a.e().booleanValue();
    }
}
