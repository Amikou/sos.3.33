package defpackage;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

/* renamed from: v56  reason: default package */
/* loaded from: classes.dex */
public class v56 {
    public static HashMap<String, String> f;
    public static Object k;
    public static boolean l;
    public static final Uri a = Uri.parse("content://com.google.android.gsf.gservices");
    public static final Uri b = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    public static final Pattern c = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
    public static final Pattern d = Pattern.compile("^(0|false|f|off|no|n)$", 2);
    public static final AtomicBoolean e = new AtomicBoolean();
    public static final HashMap<String, Boolean> g = new HashMap<>();
    public static final HashMap<String, Integer> h = new HashMap<>();
    public static final HashMap<String, Long> i = new HashMap<>();
    public static final HashMap<String, Float> j = new HashMap<>();
    public static String[] m = new String[0];

    public static long a(ContentResolver contentResolver, String str, long j2) {
        Object i2 = i(contentResolver);
        long j3 = 0;
        Long l2 = (Long) b(i, str, 0L);
        if (l2 != null) {
            return l2.longValue();
        }
        String c2 = c(contentResolver, str, null);
        if (c2 != null) {
            try {
                long parseLong = Long.parseLong(c2);
                l2 = Long.valueOf(parseLong);
                j3 = parseLong;
            } catch (NumberFormatException unused) {
            }
        }
        g(i2, i, str, l2);
        return j3;
    }

    public static <T> T b(HashMap<String, T> hashMap, String str, T t) {
        synchronized (v56.class) {
            if (hashMap.containsKey(str)) {
                T t2 = hashMap.get(str);
                if (t2 != null) {
                    t = t2;
                }
                return t;
            }
            return null;
        }
    }

    public static String c(ContentResolver contentResolver, String str, String str2) {
        synchronized (v56.class) {
            e(contentResolver);
            Object obj = k;
            if (f.containsKey(str)) {
                String str3 = f.get(str);
                return str3 != null ? str3 : null;
            }
            for (String str4 : m) {
                if (str.startsWith(str4)) {
                    if (!l || f.isEmpty()) {
                        f.putAll(d(contentResolver, m));
                        l = true;
                        if (f.containsKey(str)) {
                            String str5 = f.get(str);
                            return str5 != null ? str5 : null;
                        }
                    }
                    return null;
                }
            }
            Cursor query = contentResolver.query(a, null, null, new String[]{str}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        String string = query.getString(1);
                        if (string != null && string.equals(null)) {
                            string = null;
                        }
                        f(obj, str, string);
                        String str6 = string != null ? string : null;
                        query.close();
                        return str6;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            f(obj, str, null);
            return null;
        }
    }

    public static Map<String, String> d(ContentResolver contentResolver, String... strArr) {
        Cursor query = contentResolver.query(b, null, null, strArr, null);
        TreeMap treeMap = new TreeMap();
        if (query == null) {
            return treeMap;
        }
        while (query.moveToNext()) {
            try {
                treeMap.put(query.getString(0), query.getString(1));
            } finally {
                query.close();
            }
        }
        return treeMap;
    }

    public static void e(ContentResolver contentResolver) {
        if (f == null) {
            e.set(false);
            f = new HashMap<>();
            k = new Object();
            l = false;
            contentResolver.registerContentObserver(a, true, new a66(null));
        } else if (e.getAndSet(false)) {
            f.clear();
            g.clear();
            h.clear();
            i.clear();
            j.clear();
            k = new Object();
            l = false;
        }
    }

    public static void f(Object obj, String str, String str2) {
        synchronized (v56.class) {
            if (obj == k) {
                f.put(str, str2);
            }
        }
    }

    public static <T> void g(Object obj, HashMap<String, T> hashMap, String str, T t) {
        synchronized (v56.class) {
            if (obj == k) {
                hashMap.put(str, t);
                f.remove(str);
            }
        }
    }

    public static boolean h(ContentResolver contentResolver, String str, boolean z) {
        Object i2 = i(contentResolver);
        HashMap<String, Boolean> hashMap = g;
        Boolean bool = (Boolean) b(hashMap, str, Boolean.valueOf(z));
        if (bool != null) {
            return bool.booleanValue();
        }
        String c2 = c(contentResolver, str, null);
        if (c2 != null && !c2.equals("")) {
            if (c.matcher(c2).matches()) {
                z = true;
                bool = Boolean.TRUE;
            } else if (d.matcher(c2).matches()) {
                z = false;
                bool = Boolean.FALSE;
            } else {
                StringBuilder sb = new StringBuilder("attempt to read gservices key ");
                sb.append(str);
                sb.append(" (value \"");
                sb.append(c2);
                sb.append("\") as boolean");
            }
        }
        g(i2, hashMap, str, bool);
        return z;
    }

    public static Object i(ContentResolver contentResolver) {
        Object obj;
        synchronized (v56.class) {
            e(contentResolver);
            obj = k;
        }
        return obj;
    }
}
