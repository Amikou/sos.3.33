package defpackage;

import com.google.android.gms.internal.measurement.i1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: fl5  reason: default package */
/* loaded from: classes.dex */
public final class fl5 extends w1<i1, fl5> implements xx5 {
    public fl5() {
        super(i1.D());
    }

    public final fl5 v(int i) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        i1.E((i1) this.f0, i);
        return this;
    }

    public final fl5 x(Iterable<? extends Long> iterable) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        i1.F((i1) this.f0, iterable);
        return this;
    }

    public /* synthetic */ fl5(wi5 wi5Var) {
        super(i1.D());
    }
}
