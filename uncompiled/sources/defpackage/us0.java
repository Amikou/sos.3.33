package defpackage;

import defpackage.xs0;
import java.math.BigInteger;
import java.security.spec.ECField;
import java.security.spec.ECFieldF2m;
import java.security.spec.ECFieldFp;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.EllipticCurve;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.bouncycastle.asn1.i;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/* renamed from: us0  reason: default package */
/* loaded from: classes2.dex */
public class us0 {
    public static Map a = new HashMap();

    static {
        Enumeration j = hc0.j();
        while (j.hasMoreElements()) {
            String str = (String) j.nextElement();
            pr4 b = lt0.b(str);
            if (b != null) {
                a.put(b.o(), hc0.h(str).o());
            }
        }
        pr4 h = hc0.h("Curve25519");
        a.put(new xs0.f(h.o().t().c(), h.o().o().t(), h.o().p().t()), h.o());
    }

    public static xs0 a(EllipticCurve ellipticCurve) {
        ECField field = ellipticCurve.getField();
        BigInteger a2 = ellipticCurve.getA();
        BigInteger b = ellipticCurve.getB();
        if (field instanceof ECFieldFp) {
            xs0.f fVar = new xs0.f(((ECFieldFp) field).getP(), a2, b);
            return a.containsKey(fVar) ? (xs0) a.get(fVar) : fVar;
        }
        ECFieldF2m eCFieldF2m = (ECFieldF2m) field;
        int m = eCFieldF2m.getM();
        int[] b2 = wt0.b(eCFieldF2m.getMidTermsOfReductionPolynomial());
        return new xs0.e(m, b2[0], b2[1], b2[2], a2, b);
    }

    public static EllipticCurve b(xs0 xs0Var, byte[] bArr) {
        return new EllipticCurve(c(xs0Var.t()), xs0Var.o().t(), xs0Var.p().t(), null);
    }

    public static ECField c(z41 z41Var) {
        if (vs0.m(z41Var)) {
            return new ECFieldFp(z41Var.c());
        }
        ps2 a2 = ((qs2) z41Var).a();
        int[] a3 = a2.a();
        return new ECFieldF2m(a2.b(), wh.z(wh.m(a3, 1, a3.length - 1)));
    }

    public static pt0 d(xs0 xs0Var, ECPoint eCPoint, boolean z) {
        return xs0Var.g(eCPoint.getAffineX(), eCPoint.getAffineY());
    }

    public static pt0 e(ECParameterSpec eCParameterSpec, ECPoint eCPoint, boolean z) {
        return d(a(eCParameterSpec.getCurve()), eCPoint, z);
    }

    public static ECPoint f(pt0 pt0Var) {
        pt0 A = pt0Var.A();
        return new ECPoint(A.f().t(), A.g().t());
    }

    public static ot0 g(ECParameterSpec eCParameterSpec, boolean z) {
        xs0 a2 = a(eCParameterSpec.getCurve());
        return new ot0(a2, d(a2, eCParameterSpec.getGenerator(), z), eCParameterSpec.getOrder(), BigInteger.valueOf(eCParameterSpec.getCofactor()), eCParameterSpec.getCurve().getSeed());
    }

    public static ECParameterSpec h(EllipticCurve ellipticCurve, ot0 ot0Var) {
        return ot0Var instanceof jt0 ? new kt0(((jt0) ot0Var).f(), ellipticCurve, f(ot0Var.b()), ot0Var.d(), ot0Var.c()) : new ECParameterSpec(ellipticCurve, f(ot0Var.b()), ot0Var.d(), ot0Var.c().intValue());
    }

    public static ECParameterSpec i(nr4 nr4Var, xs0 xs0Var) {
        if (!nr4Var.s()) {
            if (nr4Var.q()) {
                return null;
            }
            pr4 s = pr4.s(nr4Var.p());
            EllipticCurve b = b(xs0Var, s.w());
            return s.q() != null ? new ECParameterSpec(b, f(s.p()), s.t(), s.q().intValue()) : new ECParameterSpec(b, f(s.p()), s.t(), 1);
        }
        i iVar = (i) nr4Var.p();
        pr4 g = wt0.g(iVar);
        if (g == null) {
            Map a2 = BouncyCastleProvider.CONFIGURATION.a();
            if (!a2.isEmpty()) {
                g = (pr4) a2.get(iVar);
            }
        }
        return new kt0(wt0.d(iVar), b(xs0Var, g.w()), f(g.p()), g.t(), g.q());
    }

    public static xs0 j(gw2 gw2Var, nr4 nr4Var) {
        Set c = gw2Var.c();
        if (!nr4Var.s()) {
            if (nr4Var.q()) {
                return gw2Var.b().a();
            }
            if (c.isEmpty()) {
                return pr4.s(nr4Var.p()).o();
            }
            throw new IllegalStateException("encoded parameters not acceptable");
        }
        i G = i.G(nr4Var.p());
        if (c.isEmpty() || c.contains(G)) {
            pr4 g = wt0.g(G);
            if (g == null) {
                g = (pr4) gw2Var.a().get(G);
            }
            return g.o();
        }
        throw new IllegalStateException("named curve not acceptable");
    }

    public static at0 k(gw2 gw2Var, ECParameterSpec eCParameterSpec) {
        if (eCParameterSpec == null) {
            ot0 b = gw2Var.b();
            return new at0(b.a(), b.b(), b.d(), b.c(), b.e());
        }
        return wt0.e(gw2Var, g(eCParameterSpec, false));
    }
}
