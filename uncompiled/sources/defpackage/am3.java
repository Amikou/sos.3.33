package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.f;
import com.fasterxml.jackson.databind.jsontype.c;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.ReferenceType;

/* compiled from: Serializers.java */
/* renamed from: am3  reason: default package */
/* loaded from: classes.dex */
public interface am3 {

    /* compiled from: Serializers.java */
    /* renamed from: am3$a */
    /* loaded from: classes.dex */
    public static class a implements am3 {
        @Override // defpackage.am3
        public f<?> findArraySerializer(SerializationConfig serializationConfig, ArrayType arrayType, so soVar, c cVar, f<Object> fVar) {
            return null;
        }

        @Override // defpackage.am3
        public f<?> findCollectionLikeSerializer(SerializationConfig serializationConfig, CollectionLikeType collectionLikeType, so soVar, c cVar, f<Object> fVar) {
            return null;
        }

        @Override // defpackage.am3
        public f<?> findCollectionSerializer(SerializationConfig serializationConfig, CollectionType collectionType, so soVar, c cVar, f<Object> fVar) {
            return null;
        }

        @Override // defpackage.am3
        public f<?> findMapLikeSerializer(SerializationConfig serializationConfig, MapLikeType mapLikeType, so soVar, f<Object> fVar, c cVar, f<Object> fVar2) {
            return null;
        }

        @Override // defpackage.am3
        public f<?> findMapSerializer(SerializationConfig serializationConfig, MapType mapType, so soVar, f<Object> fVar, c cVar, f<Object> fVar2) {
            return null;
        }

        @Override // defpackage.am3
        public f<?> findReferenceSerializer(SerializationConfig serializationConfig, ReferenceType referenceType, so soVar, c cVar, f<Object> fVar) {
            return findSerializer(serializationConfig, referenceType, soVar);
        }

        @Override // defpackage.am3
        public f<?> findSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar) {
            return null;
        }
    }

    f<?> findArraySerializer(SerializationConfig serializationConfig, ArrayType arrayType, so soVar, c cVar, f<Object> fVar);

    f<?> findCollectionLikeSerializer(SerializationConfig serializationConfig, CollectionLikeType collectionLikeType, so soVar, c cVar, f<Object> fVar);

    f<?> findCollectionSerializer(SerializationConfig serializationConfig, CollectionType collectionType, so soVar, c cVar, f<Object> fVar);

    f<?> findMapLikeSerializer(SerializationConfig serializationConfig, MapLikeType mapLikeType, so soVar, f<Object> fVar, c cVar, f<Object> fVar2);

    f<?> findMapSerializer(SerializationConfig serializationConfig, MapType mapType, so soVar, f<Object> fVar, c cVar, f<Object> fVar2);

    f<?> findReferenceSerializer(SerializationConfig serializationConfig, ReferenceType referenceType, so soVar, c cVar, f<Object> fVar);

    f<?> findSerializer(SerializationConfig serializationConfig, JavaType javaType, so soVar);
}
