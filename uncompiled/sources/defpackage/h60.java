package defpackage;

import android.os.Build;
import androidx.work.NetworkType;

/* compiled from: Constraints.java */
/* renamed from: h60  reason: default package */
/* loaded from: classes.dex */
public final class h60 {
    public static final h60 i = new a().a();
    public NetworkType a;
    public boolean b;
    public boolean c;
    public boolean d;
    public boolean e;
    public long f;
    public long g;
    public j70 h;

    /* compiled from: Constraints.java */
    /* renamed from: h60$a */
    /* loaded from: classes.dex */
    public static final class a {
        public boolean a = false;
        public boolean b = false;
        public NetworkType c = NetworkType.NOT_REQUIRED;
        public boolean d = false;
        public boolean e = false;
        public long f = -1;
        public long g = -1;
        public j70 h = new j70();

        public h60 a() {
            return new h60(this);
        }

        public a b(NetworkType networkType) {
            this.c = networkType;
            return this;
        }
    }

    public h60() {
        this.a = NetworkType.NOT_REQUIRED;
        this.f = -1L;
        this.g = -1L;
        this.h = new j70();
    }

    public j70 a() {
        return this.h;
    }

    public NetworkType b() {
        return this.a;
    }

    public long c() {
        return this.f;
    }

    public long d() {
        return this.g;
    }

    public boolean e() {
        return this.h.c() > 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || h60.class != obj.getClass()) {
            return false;
        }
        h60 h60Var = (h60) obj;
        if (this.b == h60Var.b && this.c == h60Var.c && this.d == h60Var.d && this.e == h60Var.e && this.f == h60Var.f && this.g == h60Var.g && this.a == h60Var.a) {
            return this.h.equals(h60Var.h);
        }
        return false;
    }

    public boolean f() {
        return this.d;
    }

    public boolean g() {
        return this.b;
    }

    public boolean h() {
        return this.c;
    }

    public int hashCode() {
        long j = this.f;
        long j2 = this.g;
        return (((((((((((((this.a.hashCode() * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.h.hashCode();
    }

    public boolean i() {
        return this.e;
    }

    public void j(j70 j70Var) {
        this.h = j70Var;
    }

    public void k(NetworkType networkType) {
        this.a = networkType;
    }

    public void l(boolean z) {
        this.d = z;
    }

    public void m(boolean z) {
        this.b = z;
    }

    public void n(boolean z) {
        this.c = z;
    }

    public void o(boolean z) {
        this.e = z;
    }

    public void p(long j) {
        this.f = j;
    }

    public void q(long j) {
        this.g = j;
    }

    public h60(a aVar) {
        this.a = NetworkType.NOT_REQUIRED;
        this.f = -1L;
        this.g = -1L;
        this.h = new j70();
        this.b = aVar.a;
        int i2 = Build.VERSION.SDK_INT;
        this.c = i2 >= 23 && aVar.b;
        this.a = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        if (i2 >= 24) {
            this.h = aVar.h;
            this.f = aVar.f;
            this.g = aVar.g;
        }
    }

    public h60(h60 h60Var) {
        this.a = NetworkType.NOT_REQUIRED;
        this.f = -1L;
        this.g = -1L;
        this.h = new j70();
        this.b = h60Var.b;
        this.c = h60Var.c;
        this.a = h60Var.a;
        this.d = h60Var.d;
        this.e = h60Var.e;
        this.h = h60Var.h;
    }
}
