package defpackage;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/* compiled from: EventBus.java */
/* renamed from: sx0  reason: default package */
/* loaded from: classes2.dex */
public class sx0 implements mv3, ow2 {
    public final Map<Class<?>, ConcurrentHashMap<vx0<Object>, Executor>> a = new HashMap();
    public Queue<qx0<?>> b = new ArrayDeque();
    public final Executor c;

    public sx0(Executor executor) {
        this.c = executor;
    }

    public static /* synthetic */ void f(Map.Entry entry, qx0 qx0Var) {
        ((vx0) entry.getKey()).a(qx0Var);
    }

    @Override // defpackage.mv3
    public synchronized <T> void a(Class<T> cls, Executor executor, vx0<? super T> vx0Var) {
        bu2.b(cls);
        bu2.b(vx0Var);
        bu2.b(executor);
        if (!this.a.containsKey(cls)) {
            this.a.put(cls, new ConcurrentHashMap<>());
        }
        this.a.get(cls).put(vx0Var, executor);
    }

    @Override // defpackage.mv3
    public <T> void b(Class<T> cls, vx0<? super T> vx0Var) {
        a(cls, this.c, vx0Var);
    }

    public void d() {
        Queue<qx0<?>> queue;
        synchronized (this) {
            queue = this.b;
            if (queue != null) {
                this.b = null;
            } else {
                queue = null;
            }
        }
        if (queue != null) {
            for (qx0<?> qx0Var : queue) {
                g(qx0Var);
            }
        }
    }

    public final synchronized Set<Map.Entry<vx0<Object>, Executor>> e(qx0<?> qx0Var) {
        ConcurrentHashMap<vx0<Object>, Executor> concurrentHashMap;
        concurrentHashMap = this.a.get(qx0Var.b());
        return concurrentHashMap == null ? Collections.emptySet() : concurrentHashMap.entrySet();
    }

    public void g(final qx0<?> qx0Var) {
        bu2.b(qx0Var);
        synchronized (this) {
            Queue<qx0<?>> queue = this.b;
            if (queue != null) {
                queue.add(qx0Var);
                return;
            }
            for (final Map.Entry<vx0<Object>, Executor> entry : e(qx0Var)) {
                entry.getValue().execute(new Runnable() { // from class: rx0
                    @Override // java.lang.Runnable
                    public final void run() {
                        sx0.f(entry, qx0Var);
                    }
                });
            }
        }
    }
}
