package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogRefreshWalletsConfirmationBinding.java */
/* renamed from: xn0  reason: default package */
/* loaded from: classes2.dex */
public final class xn0 {
    public final CardView a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final TextView d;
    public final TextView e;

    public xn0(CardView cardView, MaterialButton materialButton, MaterialButton materialButton2, TextView textView, TextView textView2) {
        this.a = cardView;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = textView;
        this.e = textView2;
    }

    public static xn0 a(View view) {
        int i = R.id.btnCancel;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnCancel);
        if (materialButton != null) {
            i = R.id.btnConfirm;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnConfirm);
            if (materialButton2 != null) {
                i = R.id.tvDialogContent;
                TextView textView = (TextView) ai4.a(view, R.id.tvDialogContent);
                if (textView != null) {
                    i = R.id.tvDialogTitle;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                    if (textView2 != null) {
                        return new xn0((CardView) view, materialButton, materialButton2, textView, textView2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
