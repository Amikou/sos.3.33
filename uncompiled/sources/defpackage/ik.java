package defpackage;

import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: AutoScrollHelper.java */
/* renamed from: ik  reason: default package */
/* loaded from: classes.dex */
public abstract class ik implements View.OnTouchListener {
    public static final int v0 = ViewConfiguration.getTapTimeout();
    public final View g0;
    public Runnable h0;
    public int k0;
    public int l0;
    public boolean p0;
    public boolean q0;
    public boolean r0;
    public boolean s0;
    public boolean t0;
    public boolean u0;
    public final a a = new a();
    public final Interpolator f0 = new AccelerateInterpolator();
    public float[] i0 = {Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON};
    public float[] j0 = {Float.MAX_VALUE, Float.MAX_VALUE};
    public float[] m0 = {Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON};
    public float[] n0 = {Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON};
    public float[] o0 = {Float.MAX_VALUE, Float.MAX_VALUE};

    /* compiled from: AutoScrollHelper.java */
    /* renamed from: ik$a */
    /* loaded from: classes.dex */
    public static class a {
        public int a;
        public int b;
        public float c;
        public float d;
        public float j;
        public int k;
        public long e = Long.MIN_VALUE;
        public long i = -1;
        public long f = 0;
        public int g = 0;
        public int h = 0;

        public void a() {
            if (this.f != 0) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float g = g(e(currentAnimationTimeMillis));
                this.f = currentAnimationTimeMillis;
                float f = ((float) (currentAnimationTimeMillis - this.f)) * g;
                this.g = (int) (this.c * f);
                this.h = (int) (f * this.d);
                return;
            }
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }

        public int b() {
            return this.g;
        }

        public int c() {
            return this.h;
        }

        public int d() {
            float f = this.c;
            return (int) (f / Math.abs(f));
        }

        public final float e(long j) {
            long j2 = this.e;
            if (j < j2) {
                return Utils.FLOAT_EPSILON;
            }
            long j3 = this.i;
            if (j3 >= 0 && j >= j3) {
                float f = this.j;
                return (1.0f - f) + (f * ik.e(((float) (j - j3)) / this.k, Utils.FLOAT_EPSILON, 1.0f));
            }
            return ik.e(((float) (j - j2)) / this.a, Utils.FLOAT_EPSILON, 1.0f) * 0.5f;
        }

        public int f() {
            float f = this.d;
            return (int) (f / Math.abs(f));
        }

        public final float g(float f) {
            return ((-4.0f) * f * f) + (f * 4.0f);
        }

        public boolean h() {
            return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
        }

        public void i() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.k = ik.f((int) (currentAnimationTimeMillis - this.e), 0, this.b);
            this.j = e(currentAnimationTimeMillis);
            this.i = currentAnimationTimeMillis;
        }

        public void j(int i) {
            this.b = i;
        }

        public void k(int i) {
            this.a = i;
        }

        public void l(float f, float f2) {
            this.c = f;
            this.d = f2;
        }

        public void m() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.e = currentAnimationTimeMillis;
            this.i = -1L;
            this.f = currentAnimationTimeMillis;
            this.j = 0.5f;
            this.g = 0;
            this.h = 0;
        }
    }

    /* compiled from: AutoScrollHelper.java */
    /* renamed from: ik$b */
    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ik ikVar = ik.this;
            if (ikVar.s0) {
                if (ikVar.q0) {
                    ikVar.q0 = false;
                    ikVar.a.m();
                }
                a aVar = ik.this.a;
                if (!aVar.h() && ik.this.u()) {
                    ik ikVar2 = ik.this;
                    if (ikVar2.r0) {
                        ikVar2.r0 = false;
                        ikVar2.c();
                    }
                    aVar.a();
                    ik.this.j(aVar.b(), aVar.c());
                    ei4.l0(ik.this.g0, this);
                    return;
                }
                ik.this.s0 = false;
            }
        }
    }

    public ik(View view) {
        this.g0 = view;
        float f = Resources.getSystem().getDisplayMetrics().density;
        float f2 = (int) ((1575.0f * f) + 0.5f);
        o(f2, f2);
        float f3 = (int) ((f * 315.0f) + 0.5f);
        p(f3, f3);
        l(1);
        n(Float.MAX_VALUE, Float.MAX_VALUE);
        s(0.2f, 0.2f);
        t(1.0f, 1.0f);
        k(v0);
        r(500);
        q(500);
    }

    public static float e(float f, float f2, float f3) {
        return f > f3 ? f3 : f < f2 ? f2 : f;
    }

    public static int f(int i, int i2, int i3) {
        return i > i3 ? i3 : i < i2 ? i2 : i;
    }

    public abstract boolean a(int i);

    public abstract boolean b(int i);

    public void c() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, 0);
        this.g0.onTouchEvent(obtain);
        obtain.recycle();
    }

    public final float d(int i, float f, float f2, float f3) {
        float h = h(this.i0[i], f2, this.j0[i], f);
        int i2 = (h > Utils.FLOAT_EPSILON ? 1 : (h == Utils.FLOAT_EPSILON ? 0 : -1));
        if (i2 == 0) {
            return Utils.FLOAT_EPSILON;
        }
        float f4 = this.m0[i];
        float f5 = this.n0[i];
        float f6 = this.o0[i];
        float f7 = f4 * f3;
        if (i2 > 0) {
            return e(h * f7, f5, f6);
        }
        return -e((-h) * f7, f5, f6);
    }

    public final float g(float f, float f2) {
        if (f2 == Utils.FLOAT_EPSILON) {
            return Utils.FLOAT_EPSILON;
        }
        int i = this.k0;
        if (i == 0 || i == 1) {
            if (f < f2) {
                if (f >= Utils.FLOAT_EPSILON) {
                    return 1.0f - (f / f2);
                }
                if (this.s0 && i == 1) {
                    return 1.0f;
                }
            }
        } else if (i == 2 && f < Utils.FLOAT_EPSILON) {
            return f / (-f2);
        }
        return Utils.FLOAT_EPSILON;
    }

    public final float h(float f, float f2, float f3, float f4) {
        float interpolation;
        float e = e(f * f2, Utils.FLOAT_EPSILON, f3);
        float g = g(f2 - f4, e) - g(f4, e);
        if (g < Utils.FLOAT_EPSILON) {
            interpolation = -this.f0.getInterpolation(-g);
        } else if (g <= Utils.FLOAT_EPSILON) {
            return Utils.FLOAT_EPSILON;
        } else {
            interpolation = this.f0.getInterpolation(g);
        }
        return e(interpolation, -1.0f, 1.0f);
    }

    public final void i() {
        if (this.q0) {
            this.s0 = false;
        } else {
            this.a.i();
        }
    }

    public abstract void j(int i, int i2);

    public ik k(int i) {
        this.l0 = i;
        return this;
    }

    public ik l(int i) {
        this.k0 = i;
        return this;
    }

    public ik m(boolean z) {
        if (this.t0 && !z) {
            i();
        }
        this.t0 = z;
        return this;
    }

    public ik n(float f, float f2) {
        float[] fArr = this.j0;
        fArr[0] = f;
        fArr[1] = f2;
        return this;
    }

    public ik o(float f, float f2) {
        float[] fArr = this.o0;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0013, code lost:
        if (r0 != 3) goto L12;
     */
    @Override // android.view.View.OnTouchListener
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public boolean onTouch(android.view.View r6, android.view.MotionEvent r7) {
        /*
            r5 = this;
            boolean r0 = r5.t0
            r1 = 0
            if (r0 != 0) goto L6
            return r1
        L6:
            int r0 = r7.getActionMasked()
            r2 = 1
            if (r0 == 0) goto L1a
            if (r0 == r2) goto L16
            r3 = 2
            if (r0 == r3) goto L1e
            r6 = 3
            if (r0 == r6) goto L16
            goto L58
        L16:
            r5.i()
            goto L58
        L1a:
            r5.r0 = r2
            r5.p0 = r1
        L1e:
            float r0 = r7.getX()
            int r3 = r6.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r5.g0
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r0 = r5.d(r1, r0, r3, r4)
            float r7 = r7.getY()
            int r6 = r6.getHeight()
            float r6 = (float) r6
            android.view.View r3 = r5.g0
            int r3 = r3.getHeight()
            float r3 = (float) r3
            float r6 = r5.d(r2, r7, r6, r3)
            ik$a r7 = r5.a
            r7.l(r0, r6)
            boolean r6 = r5.s0
            if (r6 != 0) goto L58
            boolean r6 = r5.u()
            if (r6 == 0) goto L58
            r5.v()
        L58:
            boolean r6 = r5.u0
            if (r6 == 0) goto L61
            boolean r6 = r5.s0
            if (r6 == 0) goto L61
            r1 = r2
        L61:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ik.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    public ik p(float f, float f2) {
        float[] fArr = this.n0;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    public ik q(int i) {
        this.a.j(i);
        return this;
    }

    public ik r(int i) {
        this.a.k(i);
        return this;
    }

    public ik s(float f, float f2) {
        float[] fArr = this.i0;
        fArr[0] = f;
        fArr[1] = f2;
        return this;
    }

    public ik t(float f, float f2) {
        float[] fArr = this.m0;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    public boolean u() {
        a aVar = this.a;
        int f = aVar.f();
        int d = aVar.d();
        return (f != 0 && b(f)) || (d != 0 && a(d));
    }

    public final void v() {
        int i;
        if (this.h0 == null) {
            this.h0 = new b();
        }
        this.s0 = true;
        this.q0 = true;
        if (!this.p0 && (i = this.l0) > 0) {
            ei4.m0(this.g0, this.h0, i);
        } else {
            this.h0.run();
        }
        this.p0 = true;
    }
}
