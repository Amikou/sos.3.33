package defpackage;

import java.util.Arrays;
import java.util.Objects;

/* compiled from: Objects.java */
/* renamed from: rl2  reason: default package */
/* loaded from: classes2.dex */
public final class rl2 {
    public static int a(int i, int i2) {
        if (i < 0 || i >= i2) {
            throw new IndexOutOfBoundsException(String.format("Index %d out-of-bounds for length %d", Integer.valueOf(i), Integer.valueOf(i2)));
        }
        return i;
    }

    public static boolean b(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int c(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    public static int d(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public static boolean e(Object obj) {
        return obj != null;
    }

    public static <T> T f(T t) {
        Objects.requireNonNull(t);
        return t;
    }
}
