package defpackage;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;

/* compiled from: BitmapDrawableEncoder.java */
/* renamed from: up  reason: default package */
/* loaded from: classes.dex */
public class up implements y73<BitmapDrawable> {
    public final jq a;
    public final y73<Bitmap> b;

    public up(jq jqVar, y73<Bitmap> y73Var) {
        this.a = jqVar;
        this.b = y73Var;
    }

    @Override // defpackage.y73
    public EncodeStrategy b(vn2 vn2Var) {
        return this.b.b(vn2Var);
    }

    @Override // defpackage.ev0
    /* renamed from: c */
    public boolean a(s73<BitmapDrawable> s73Var, File file, vn2 vn2Var) {
        return this.b.a(new oq(s73Var.get().getBitmap(), this.a), file, vn2Var);
    }
}
