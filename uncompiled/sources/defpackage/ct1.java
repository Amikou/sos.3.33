package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ItemManageContactBinding.java */
/* renamed from: ct1  reason: default package */
/* loaded from: classes2.dex */
public final class ct1 {
    public final ConstraintLayout a;
    public final ConstraintLayout b;
    public final ImageView c;
    public final TextView d;
    public final TextView e;

    public ct1(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, ImageView imageView2, TextView textView, TextView textView2) {
        this.a = constraintLayout;
        this.b = constraintLayout2;
        this.c = imageView2;
        this.d = textView;
        this.e = textView2;
    }

    public static ct1 a(View view) {
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        int i = R.id.imgIndicator;
        ImageView imageView = (ImageView) ai4.a(view, R.id.imgIndicator);
        if (imageView != null) {
            i = R.id.ivContactIcon;
            ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivContactIcon);
            if (imageView2 != null) {
                i = R.id.tvContactAddress;
                TextView textView = (TextView) ai4.a(view, R.id.tvContactAddress);
                if (textView != null) {
                    i = R.id.tvContactName;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvContactName);
                    if (textView2 != null) {
                        return new ct1(constraintLayout, constraintLayout, imageView, imageView2, textView, textView2);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
