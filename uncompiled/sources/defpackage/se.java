package defpackage;

import android.animation.Animator;

/* compiled from: AnimatorTracker.java */
/* renamed from: se  reason: default package */
/* loaded from: classes2.dex */
public class se {
    public Animator a;

    public void a() {
        Animator animator = this.a;
        if (animator != null) {
            animator.cancel();
        }
    }

    public void b() {
        this.a = null;
    }

    public void c(Animator animator) {
        a();
        this.a = animator;
    }
}
