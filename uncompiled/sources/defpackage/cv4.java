package defpackage;

import com.google.android.play.core.assetpacks.c;
import java.io.File;

/* renamed from: cv4  reason: default package */
/* loaded from: classes2.dex */
public final class cv4 {
    public static final it4 f = new it4("ExtractChunkTaskHandler");
    public final byte[] a = new byte[8192];
    public final c b;
    public final cw4<zy4> c;
    public final cw4<au4> d;
    public final fv4 e;

    public cv4(c cVar, cw4<zy4> cw4Var, cw4<au4> cw4Var2, fv4 fv4Var) {
        this.b = cVar;
        this.c = cw4Var;
        this.d = cw4Var2;
        this.e = fv4Var;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(19:1|(1:3)|4|5|6|(1:8)(2:112|113)|9|(2:11|(12:13|(1:(1:(2:17|(2:82|83))(2:84|85))(2:86|(10:88|(7:21|(4:22|(2:26|(1:35)(4:30|(1:32)|33|34))|36|(1:38)(1:62))|40|41|(1:43)|44|(2:46|(1:48)(2:49|(1:51)(3:52|(2:54|(1:56)(2:58|59))(1:61)|57))))|63|64|(2:76|77)|66|67|68|69|(2:71|72)(1:73))(2:89|90)))(2:91|(4:93|(4:94|(1:96)|97|(1:100)(1:106))|103|(1:105))(2:107|108))|19|(0)|63|64|(0)|66|67|68|69|(0)(0))(2:109|110))|111|(0)|63|64|(0)|66|67|68|69|(0)(0)|(1:(0))) */
    /* JADX WARN: Code restructure failed: missing block: B:100:0x02ed, code lost:
        defpackage.cv4.f.e("Could not close file for chunk %s of slice %s of pack %s.", java.lang.Integer.valueOf(r23.g), r23.e, r23.b);
     */
    /* JADX WARN: Removed duplicated region for block: B:103:0x030c  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x0291 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:126:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:51:0x017d A[Catch: all -> 0x032d, TryCatch #5 {IOException -> 0x0339, blocks: (B:6:0x002f, B:89:0x0288, B:9:0x0037, B:11:0x003f, B:13:0x0045, B:15:0x0053, B:19:0x005d, B:51:0x017d, B:52:0x0186, B:54:0x0190, B:56:0x0196, B:58:0x019c, B:60:0x01a2, B:62:0x01c6, B:63:0x01d2, B:64:0x01d6, B:65:0x01dd, B:67:0x01e3, B:69:0x01e9, B:71:0x01ef, B:72:0x01ff, B:74:0x0205, B:76:0x020b, B:77:0x021e, B:79:0x0224, B:80:0x0233, B:82:0x0239, B:88:0x027a, B:85:0x0261, B:86:0x0268, B:87:0x0269, B:22:0x0077, B:23:0x0080, B:24:0x0081, B:25:0x009a, B:26:0x009b, B:28:0x00c2, B:29:0x00ce, B:30:0x00d7, B:31:0x00d8, B:33:0x00f6, B:34:0x0108, B:36:0x011b, B:37:0x0120, B:42:0x012f, B:44:0x0138, B:45:0x0150, B:46:0x0159, B:47:0x015a, B:48:0x0179), top: B:118:0x002f }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void a(defpackage.bv4 r23) {
        /*
            Method dump skipped, instructions count: 883
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cv4.a(bv4):void");
    }

    public final File b(bv4 bv4Var) {
        File v = this.b.v(bv4Var.b, bv4Var.c, bv4Var.d, bv4Var.e);
        if (!v.exists()) {
            v.mkdirs();
        }
        return v;
    }
}
