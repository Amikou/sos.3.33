package defpackage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;

/* compiled from: TokenFilterContext.java */
/* renamed from: w64  reason: default package */
/* loaded from: classes.dex */
public class w64 extends cw1 {
    public final w64 c;
    public w64 d;
    public String e;
    public v64 f;
    public boolean g;
    public boolean h;

    public w64(int i, w64 w64Var, v64 v64Var, boolean z) {
        this.a = i;
        this.c = w64Var;
        this.f = v64Var;
        this.b = -1;
        this.g = z;
        this.h = false;
    }

    public static w64 n(v64 v64Var) {
        return new w64(0, null, v64Var, true);
    }

    @Override // defpackage.cw1
    public final String b() {
        return this.e;
    }

    @Override // defpackage.cw1
    public void h(Object obj) {
    }

    public void j(StringBuilder sb) {
        w64 w64Var = this.c;
        if (w64Var != null) {
            w64Var.j(sb);
        }
        int i = this.a;
        if (i != 2) {
            if (i == 1) {
                sb.append('[');
                sb.append(a());
                sb.append(']');
                return;
            }
            sb.append("/");
            return;
        }
        sb.append('{');
        if (this.e != null) {
            sb.append('\"');
            sb.append(this.e);
            sb.append('\"');
        } else {
            sb.append('?');
        }
        sb.append('}');
    }

    public v64 k(v64 v64Var) {
        int i = this.a;
        if (i == 2) {
            return v64Var;
        }
        int i2 = this.b + 1;
        this.b = i2;
        if (i == 1) {
            return v64Var.e(i2);
        }
        return v64Var.g(i2);
    }

    public w64 l(v64 v64Var, boolean z) {
        w64 w64Var = this.d;
        if (w64Var == null) {
            w64 w64Var2 = new w64(1, this, v64Var, z);
            this.d = w64Var2;
            return w64Var2;
        }
        return w64Var.t(1, v64Var, z);
    }

    public w64 m(v64 v64Var, boolean z) {
        w64 w64Var = this.d;
        if (w64Var == null) {
            w64 w64Var2 = new w64(2, this, v64Var, z);
            this.d = w64Var2;
            return w64Var2;
        }
        return w64Var.t(2, v64Var, z);
    }

    public w64 o(w64 w64Var) {
        w64 w64Var2 = this.c;
        if (w64Var2 == w64Var) {
            return this;
        }
        while (w64Var2 != null) {
            w64 w64Var3 = w64Var2.c;
            if (w64Var3 == w64Var) {
                return w64Var2;
            }
            w64Var2 = w64Var3;
        }
        return null;
    }

    public v64 p() {
        return this.f;
    }

    @Override // defpackage.cw1
    /* renamed from: q */
    public final w64 d() {
        return this.c;
    }

    public boolean r() {
        return this.g;
    }

    public JsonToken s() {
        if (!this.g) {
            this.g = true;
            if (this.a == 2) {
                return JsonToken.START_OBJECT;
            }
            return JsonToken.START_ARRAY;
        } else if (this.h && this.a == 2) {
            this.h = false;
            return JsonToken.FIELD_NAME;
        } else {
            return null;
        }
    }

    public w64 t(int i, v64 v64Var, boolean z) {
        this.a = i;
        this.f = v64Var;
        this.b = -1;
        this.e = null;
        this.g = z;
        this.h = false;
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        j(sb);
        return sb.toString();
    }

    public v64 u(String str) throws JsonProcessingException {
        this.e = str;
        this.h = true;
        return this.f;
    }
}
