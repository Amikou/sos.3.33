package defpackage;

import java.io.IOException;

/* compiled from: ForwardingExtractorInput.java */
/* renamed from: e91  reason: default package */
/* loaded from: classes.dex */
public class e91 implements q11 {
    public final q11 a;

    public e91(q11 q11Var) {
        this.a = q11Var;
    }

    @Override // defpackage.q11
    public boolean a(byte[] bArr, int i, int i2, boolean z) throws IOException {
        return this.a.a(bArr, i, i2, z);
    }

    @Override // defpackage.q11
    public boolean d(byte[] bArr, int i, int i2, boolean z) throws IOException {
        return this.a.d(bArr, i, i2, z);
    }

    @Override // defpackage.q11
    public long e() {
        return this.a.e();
    }

    @Override // defpackage.q11
    public void f(int i) throws IOException {
        this.a.f(i);
    }

    @Override // defpackage.q11
    public int g(int i) throws IOException {
        return this.a.g(i);
    }

    @Override // defpackage.q11
    public long getLength() {
        return this.a.getLength();
    }

    @Override // defpackage.q11
    public long getPosition() {
        return this.a.getPosition();
    }

    @Override // defpackage.q11
    public int h(byte[] bArr, int i, int i2) throws IOException {
        return this.a.h(bArr, i, i2);
    }

    @Override // defpackage.q11
    public void j() {
        this.a.j();
    }

    @Override // defpackage.q11
    public void k(int i) throws IOException {
        this.a.k(i);
    }

    @Override // defpackage.q11
    public boolean l(int i, boolean z) throws IOException {
        return this.a.l(i, z);
    }

    @Override // defpackage.q11
    public void n(byte[] bArr, int i, int i2) throws IOException {
        this.a.n(bArr, i, i2);
    }

    @Override // defpackage.q11, androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws IOException {
        return this.a.read(bArr, i, i2);
    }

    @Override // defpackage.q11
    public void readFully(byte[] bArr, int i, int i2) throws IOException {
        this.a.readFully(bArr, i, i2);
    }
}
