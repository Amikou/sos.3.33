package defpackage;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/* compiled from: DrawableImageViewTarget.java */
/* renamed from: vq0  reason: default package */
/* loaded from: classes.dex */
public class vq0 extends ep1<Drawable> {
    public vq0(ImageView imageView) {
        super(imageView);
    }

    @Override // defpackage.ep1
    /* renamed from: t */
    public void r(Drawable drawable) {
        ((ImageView) this.a).setImageDrawable(drawable);
    }
}
