package defpackage;

import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: g15  reason: default package */
/* loaded from: classes.dex */
public final class g15 implements Runnable {
    public final /* synthetic */ e15 a;

    public g15(e15 e15Var) {
        this.a = e15Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        h15 h15Var;
        h15Var = this.a.g;
        h15Var.a(new ConnectionResult(4));
    }
}
