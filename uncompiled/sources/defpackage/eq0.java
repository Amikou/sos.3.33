package defpackage;

/* compiled from: DolbyVisionConfig.java */
/* renamed from: eq0  reason: default package */
/* loaded from: classes.dex */
public final class eq0 {
    public final String a;

    public eq0(int i, int i2, String str) {
        this.a = str;
    }

    public static eq0 a(op2 op2Var) {
        String str;
        op2Var.Q(2);
        int D = op2Var.D();
        int i = D >> 1;
        int D2 = ((op2Var.D() >> 3) & 31) | ((D & 1) << 5);
        if (i == 4 || i == 5 || i == 7) {
            str = "dvhe";
        } else if (i == 8) {
            str = "hev1";
        } else if (i != 9) {
            return null;
        } else {
            str = "avc3";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".0");
        sb.append(i);
        sb.append(D2 >= 10 ? "." : ".0");
        sb.append(D2);
        return new eq0(i, D2, sb.toString());
    }
}
