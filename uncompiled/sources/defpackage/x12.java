package defpackage;

/* compiled from: Logger.java */
/* renamed from: x12  reason: default package */
/* loaded from: classes2.dex */
public interface x12 {
    void debug(String str);

    void error(String str);

    void error(String str, Throwable th);

    String getName();

    void info(String str);

    void info(String str, Object obj);

    boolean isDebugEnabled();

    void warn(String str);
}
