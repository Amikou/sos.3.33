package defpackage;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSecurityBinding.java */
/* renamed from: va1  reason: default package */
/* loaded from: classes2.dex */
public final class va1 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final CardView c;
    public final CardView d;
    public final MaterialCardView e;
    public final MaterialCardView f;
    public final Group g;
    public final Group h;
    public final SwitchMaterial i;
    public final SwitchMaterial j;
    public final SwitchMaterial k;
    public final TextView l;
    public final TextView m;
    public final rp3 n;
    public final MaterialTextView o;
    public final TextView p;
    public final TextView q;
    public final TextView r;
    public final TextView s;
    public final TextView t;
    public final TextView u;

    public va1(ConstraintLayout constraintLayout, MaterialButton materialButton, ConstraintLayout constraintLayout2, CardView cardView, CardView cardView2, CardView cardView3, MaterialCardView materialCardView, MaterialCardView materialCardView2, MaterialCardView materialCardView3, CardView cardView4, EditText editText, Group group, Group group2, SwitchMaterial switchMaterial, SwitchMaterial switchMaterial2, SwitchMaterial switchMaterial3, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, rp3 rp3Var, TextView textView7, MaterialTextView materialTextView, TextView textView8, TextView textView9, TextView textView10, TextView textView11, TextView textView12, TextView textView13, MaterialTextView materialTextView2, MaterialTextView materialTextView3, MaterialTextView materialTextView4) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = cardView;
        this.d = cardView2;
        this.e = materialCardView;
        this.f = materialCardView2;
        this.g = group;
        this.h = group2;
        this.i = switchMaterial;
        this.j = switchMaterial2;
        this.k = switchMaterial3;
        this.l = textView;
        this.m = textView2;
        this.n = rp3Var;
        this.o = materialTextView;
        this.p = textView8;
        this.q = textView9;
        this.r = textView10;
        this.s = textView11;
        this.t = textView12;
        this.u = textView13;
    }

    public static va1 a(View view) {
        int i = R.id.btnUnpair;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnUnpair);
        if (materialButton != null) {
            ConstraintLayout constraintLayout = (ConstraintLayout) view;
            i = R.id.cv_2;
            CardView cardView = (CardView) ai4.a(view, R.id.cv_2);
            if (cardView != null) {
                i = R.id.cv2Layer;
                CardView cardView2 = (CardView) ai4.a(view, R.id.cv2Layer);
                if (cardView2 != null) {
                    i = R.id.cv_3;
                    CardView cardView3 = (CardView) ai4.a(view, R.id.cv_3);
                    if (cardView3 != null) {
                        i = R.id.cvChangeEmail;
                        MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.cvChangeEmail);
                        if (materialCardView != null) {
                            i = R.id.cvChangePassword;
                            MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.cvChangePassword);
                            if (materialCardView2 != null) {
                                i = R.id.cvGoogleAuth;
                                MaterialCardView materialCardView3 = (MaterialCardView) ai4.a(view, R.id.cvGoogleAuth);
                                if (materialCardView3 != null) {
                                    i = R.id.cvLayerTransactionSigning;
                                    CardView cardView4 = (CardView) ai4.a(view, R.id.cvLayerTransactionSigning);
                                    if (cardView4 != null) {
                                        i = R.id.et2FaPassword;
                                        EditText editText = (EditText) ai4.a(view, R.id.et2FaPassword);
                                        if (editText != null) {
                                            i = R.id.gp_pass;
                                            Group group = (Group) ai4.a(view, R.id.gp_pass);
                                            if (group != null) {
                                                i = R.id.group2;
                                                Group group2 = (Group) ai4.a(view, R.id.group2);
                                                if (group2 != null) {
                                                    i = R.id.switch2stepVerification;
                                                    SwitchMaterial switchMaterial = (SwitchMaterial) ai4.a(view, R.id.switch2stepVerification);
                                                    if (switchMaterial != null) {
                                                        i = R.id.switchAuthTransactionSign;
                                                        SwitchMaterial switchMaterial2 = (SwitchMaterial) ai4.a(view, R.id.switchAuthTransactionSign);
                                                        if (switchMaterial2 != null) {
                                                            i = R.id.switchTwoLayerConfirmation;
                                                            SwitchMaterial switchMaterial3 = (SwitchMaterial) ai4.a(view, R.id.switchTwoLayerConfirmation);
                                                            if (switchMaterial3 != null) {
                                                                i = R.id.textView20;
                                                                TextView textView = (TextView) ai4.a(view, R.id.textView20);
                                                                if (textView != null) {
                                                                    i = R.id.textView21;
                                                                    TextView textView2 = (TextView) ai4.a(view, R.id.textView21);
                                                                    if (textView2 != null) {
                                                                        i = R.id.textView22;
                                                                        TextView textView3 = (TextView) ai4.a(view, R.id.textView22);
                                                                        if (textView3 != null) {
                                                                            i = R.id.textView25;
                                                                            TextView textView4 = (TextView) ai4.a(view, R.id.textView25);
                                                                            if (textView4 != null) {
                                                                                i = R.id.textView35;
                                                                                TextView textView5 = (TextView) ai4.a(view, R.id.textView35);
                                                                                if (textView5 != null) {
                                                                                    i = R.id.textView8;
                                                                                    TextView textView6 = (TextView) ai4.a(view, R.id.textView8);
                                                                                    if (textView6 != null) {
                                                                                        i = R.id.toolbar;
                                                                                        View a = ai4.a(view, R.id.toolbar);
                                                                                        if (a != null) {
                                                                                            rp3 a2 = rp3.a(a);
                                                                                            i = R.id.tv2FaErrorVerification;
                                                                                            TextView textView7 = (TextView) ai4.a(view, R.id.tv2FaErrorVerification);
                                                                                            if (textView7 != null) {
                                                                                                i = R.id.tv2FaInfo;
                                                                                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tv2FaInfo);
                                                                                                if (materialTextView != null) {
                                                                                                    i = R.id.tv_copy;
                                                                                                    TextView textView8 = (TextView) ai4.a(view, R.id.tv_copy);
                                                                                                    if (textView8 != null) {
                                                                                                        i = R.id.tv_passpharese_reveal;
                                                                                                        TextView textView9 = (TextView) ai4.a(view, R.id.tv_passpharese_reveal);
                                                                                                        if (textView9 != null) {
                                                                                                            i = R.id.tv_passphrase;
                                                                                                            TextView textView10 = (TextView) ai4.a(view, R.id.tv_passphrase);
                                                                                                            if (textView10 != null) {
                                                                                                                i = R.id.tv_private;
                                                                                                                TextView textView11 = (TextView) ai4.a(view, R.id.tv_private);
                                                                                                                if (textView11 != null) {
                                                                                                                    i = R.id.tv_private_copy;
                                                                                                                    TextView textView12 = (TextView) ai4.a(view, R.id.tv_private_copy);
                                                                                                                    if (textView12 != null) {
                                                                                                                        i = R.id.tv_private_key_reveal;
                                                                                                                        TextView textView13 = (TextView) ai4.a(view, R.id.tv_private_key_reveal);
                                                                                                                        if (textView13 != null) {
                                                                                                                            i = R.id.txtTitleAuthTransactionSign;
                                                                                                                            MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtTitleAuthTransactionSign);
                                                                                                                            if (materialTextView2 != null) {
                                                                                                                                i = R.id.txtTitleSecurity;
                                                                                                                                MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtTitleSecurity);
                                                                                                                                if (materialTextView3 != null) {
                                                                                                                                    i = R.id.txtTitleTV;
                                                                                                                                    MaterialTextView materialTextView4 = (MaterialTextView) ai4.a(view, R.id.txtTitleTV);
                                                                                                                                    if (materialTextView4 != null) {
                                                                                                                                        return new va1(constraintLayout, materialButton, constraintLayout, cardView, cardView2, cardView3, materialCardView, materialCardView2, materialCardView3, cardView4, editText, group, group2, switchMaterial, switchMaterial2, switchMaterial3, textView, textView2, textView3, textView4, textView5, textView6, a2, textView7, materialTextView, textView8, textView9, textView10, textView11, textView12, textView13, materialTextView2, materialTextView3, materialTextView4);
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
