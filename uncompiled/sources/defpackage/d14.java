package defpackage;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentManager;
import net.safemoon.androidwallet.R;

/* compiled from: SwapSlipHelpPrompt.kt */
/* renamed from: d14  reason: default package */
/* loaded from: classes2.dex */
public final class d14 extends sn0 {
    public static final a v0 = new a(null);
    public ao0 u0;

    /* compiled from: SwapSlipHelpPrompt.kt */
    /* renamed from: d14$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }

        public final d14 a() {
            d14 d14Var = new d14();
            Bundle bundle = new Bundle();
            te4 te4Var = te4.a;
            d14Var.setArguments(bundle);
            return d14Var;
        }
    }

    public static final void w(d14 d14Var, View view) {
        fs1.f(d14Var, "this$0");
        d14Var.h();
    }

    @Override // defpackage.sn0
    public Dialog m(Bundle bundle) {
        Dialog m = super.m(bundle);
        fs1.e(m, "super.onCreateDialog(savedInstanceState)");
        m.requestWindowFeature(1);
        return m;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fs1.f(layoutInflater, "inflater");
        return LayoutInflater.from(requireContext()).inflate(R.layout.dialog_swap_slip_help_prompt, viewGroup, false);
    }

    @Override // defpackage.sn0, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog k = k();
        if (k == null) {
            return;
        }
        Window window = k.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
        Window window2 = k.getWindow();
        if (window2 == null) {
            return;
        }
        window2.setLayout(-1, -2);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        fs1.f(view, "view");
        super.onViewCreated(view, bundle);
        ao0 a2 = ao0.a(view);
        fs1.e(a2, "bind(view)");
        this.u0 = a2;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        a2.a.setOnClickListener(new View.OnClickListener() { // from class: c14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                d14.w(d14.this, view2);
            }
        });
    }

    public final void x(FragmentManager fragmentManager) {
        fs1.f(fragmentManager, "manager");
        super.u(fragmentManager, d14.class.getCanonicalName());
    }
}
