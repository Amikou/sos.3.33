package defpackage;

import java.math.BigInteger;

/* compiled from: BlockTime.kt */
/* renamed from: dc3  reason: default package */
/* loaded from: classes2.dex */
public final class dc3 {
    public final BigInteger a;
    public final String b;

    public dc3(BigInteger bigInteger, String str) {
        fs1.f(bigInteger, "timestamp");
        fs1.f(str, "number");
        this.a = bigInteger;
        this.b = str;
    }

    public final String a() {
        return this.b;
    }

    public final BigInteger b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof dc3) {
            dc3 dc3Var = (dc3) obj;
            return fs1.b(this.a, dc3Var.a) && fs1.b(this.b, dc3Var.b);
        }
        return false;
    }

    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    public String toString() {
        return "SaveBlocks(timestamp=" + this.a + ", number=" + this.b + ')';
    }
}
