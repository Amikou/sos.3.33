package defpackage;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import defpackage.qc3;

/* compiled from: ScaleTypeDrawable.java */
/* renamed from: oc3  reason: default package */
/* loaded from: classes.dex */
public class oc3 extends d91 {
    public qc3.b h0;
    public Object i0;
    public PointF j0;
    public int k0;
    public int l0;
    public Matrix m0;
    public Matrix n0;

    public oc3(Drawable drawable, qc3.b bVar) {
        super(drawable);
        this.j0 = null;
        this.k0 = 0;
        this.l0 = 0;
        this.n0 = new Matrix();
        this.h0 = bVar;
    }

    @Override // defpackage.d91, defpackage.wa4
    public void c(Matrix matrix) {
        m(matrix);
        q();
        Matrix matrix2 = this.m0;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        q();
        if (this.m0 != null) {
            int save = canvas.save();
            canvas.clipRect(getBounds());
            canvas.concat(this.m0);
            super.draw(canvas);
            canvas.restoreToCount(save);
            return;
        }
        super.draw(canvas);
    }

    @Override // defpackage.d91
    public Drawable n(Drawable drawable) {
        Drawable n = super.n(drawable);
        p();
        return n;
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        p();
    }

    public void p() {
        Drawable current = getCurrent();
        if (current == null) {
            this.l0 = 0;
            this.k0 = 0;
            this.m0 = null;
            return;
        }
        Rect bounds = getBounds();
        int width = bounds.width();
        int height = bounds.height();
        int intrinsicWidth = current.getIntrinsicWidth();
        this.k0 = intrinsicWidth;
        int intrinsicHeight = current.getIntrinsicHeight();
        this.l0 = intrinsicHeight;
        if (intrinsicWidth <= 0 || intrinsicHeight <= 0) {
            current.setBounds(bounds);
            this.m0 = null;
        } else if (intrinsicWidth == width && intrinsicHeight == height) {
            current.setBounds(bounds);
            this.m0 = null;
        } else if (this.h0 == qc3.b.a) {
            current.setBounds(bounds);
            this.m0 = null;
        } else {
            current.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            qc3.b bVar = this.h0;
            Matrix matrix = this.n0;
            PointF pointF = this.j0;
            bVar.a(matrix, bounds, intrinsicWidth, intrinsicHeight, pointF != null ? pointF.x : 0.5f, pointF != null ? pointF.y : 0.5f);
            this.m0 = this.n0;
        }
    }

    public final void q() {
        boolean z;
        qc3.b bVar = this.h0;
        boolean z2 = true;
        if (bVar instanceof qc3.n) {
            Object state = ((qc3.n) bVar).getState();
            z = state == null || !state.equals(this.i0);
            this.i0 = state;
        } else {
            z = false;
        }
        Drawable current = getCurrent();
        if (current == null) {
            return;
        }
        if (this.k0 == current.getIntrinsicWidth() && this.l0 == current.getIntrinsicHeight()) {
            z2 = false;
        }
        if (z2 || z) {
            p();
        }
    }

    public PointF r() {
        return this.j0;
    }

    public qc3.b s() {
        return this.h0;
    }

    public void t(PointF pointF) {
        if (ol2.a(this.j0, pointF)) {
            return;
        }
        if (pointF == null) {
            this.j0 = null;
        } else {
            if (this.j0 == null) {
                this.j0 = new PointF();
            }
            this.j0.set(pointF);
        }
        p();
        invalidateSelf();
    }
}
