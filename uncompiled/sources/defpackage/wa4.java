package defpackage;

import android.graphics.Matrix;
import android.graphics.RectF;

/* compiled from: TransformCallback.java */
/* renamed from: wa4  reason: default package */
/* loaded from: classes.dex */
public interface wa4 {
    void c(Matrix matrix);

    void g(RectF rectF);
}
