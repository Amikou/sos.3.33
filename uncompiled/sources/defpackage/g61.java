package defpackage;

/* renamed from: g61  reason: default package */
/* loaded from: classes2.dex */
public class g61 implements ut2 {
    public pt0 a = null;
    public ht0 b = null;
    public int c = -1;

    public ht0 a() {
        return this.b;
    }

    public pt0 b() {
        return this.a;
    }

    public int c() {
        return this.c;
    }

    public void d(ht0 ht0Var) {
        this.b = ht0Var;
    }

    public void e(pt0 pt0Var) {
        this.a = pt0Var;
    }

    public void f(int i) {
        this.c = i;
    }
}
