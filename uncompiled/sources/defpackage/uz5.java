package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: uz5  reason: default package */
/* loaded from: classes.dex */
public final class uz5 extends c55 {
    public uz5(String str, a16 a16Var) {
        super("internal.remoteConfig");
        this.f0.put("getValue", new yy5(this, "getValue", a16Var));
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        return z55.X;
    }
}
