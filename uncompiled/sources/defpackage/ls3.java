package defpackage;

import kotlin.coroutines.CoroutineContext;

/* compiled from: Builders.common.kt */
/* renamed from: ls3  reason: default package */
/* loaded from: classes2.dex */
public class ls3 extends t4<te4> {
    public ls3(CoroutineContext coroutineContext, boolean z) {
        super(coroutineContext, true, z);
    }

    @Override // defpackage.bu1
    public boolean b0(Throwable th) {
        z80.a(getContext(), th);
        return true;
    }
}
