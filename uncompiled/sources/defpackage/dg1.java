package defpackage;

import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/* compiled from: GifHeaderParser.java */
/* renamed from: dg1  reason: default package */
/* loaded from: classes.dex */
public class dg1 {
    public ByteBuffer b;
    public cg1 c;
    public final byte[] a = new byte[256];
    public int d = 0;

    public void a() {
        this.b = null;
        this.c = null;
    }

    public final boolean b() {
        return this.c.b != 0;
    }

    public cg1 c() {
        if (this.b != null) {
            if (b()) {
                return this.c;
            }
            k();
            if (!b()) {
                h();
                cg1 cg1Var = this.c;
                if (cg1Var.c < 0) {
                    cg1Var.b = 1;
                }
            }
            return this.c;
        }
        throw new IllegalStateException("You must call setData() before parseHeader()");
    }

    public final int d() {
        try {
            return this.b.get() & 255;
        } catch (Exception unused) {
            this.c.b = 1;
            return 0;
        }
    }

    public final void e() {
        this.c.d.a = n();
        this.c.d.b = n();
        this.c.d.c = n();
        this.c.d.d = n();
        int d = d();
        boolean z = (d & 128) != 0;
        int pow = (int) Math.pow(2.0d, (d & 7) + 1);
        zf1 zf1Var = this.c.d;
        zf1Var.e = (d & 64) != 0;
        if (z) {
            zf1Var.k = g(pow);
        } else {
            zf1Var.k = null;
        }
        this.c.d.j = this.b.position();
        r();
        if (b()) {
            return;
        }
        cg1 cg1Var = this.c;
        cg1Var.c++;
        cg1Var.e.add(cg1Var.d);
    }

    public final void f() {
        int d = d();
        this.d = d;
        if (d <= 0) {
            return;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            try {
                i2 = this.d;
                if (i >= i2) {
                    return;
                }
                i2 -= i;
                this.b.get(this.a, i, i2);
                i += i2;
            } catch (Exception unused) {
                if (Log.isLoggable("GifHeaderParser", 3)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Error Reading Block n: ");
                    sb.append(i);
                    sb.append(" count: ");
                    sb.append(i2);
                    sb.append(" blockSize: ");
                    sb.append(this.d);
                }
                this.c.b = 1;
                return;
            }
        }
    }

    public final int[] g(int i) {
        byte[] bArr = new byte[i * 3];
        int[] iArr = null;
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                int i6 = i5 + 1;
                int i7 = i2 + 1;
                iArr[i2] = ((bArr[i3] & 255) << 16) | (-16777216) | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
                i3 = i6;
                i2 = i7;
            }
        } catch (BufferUnderflowException unused) {
            this.c.b = 1;
        }
        return iArr;
    }

    public final void h() {
        i(Integer.MAX_VALUE);
    }

    public final void i(int i) {
        boolean z = false;
        while (!z && !b() && this.c.c <= i) {
            int d = d();
            if (d == 33) {
                int d2 = d();
                if (d2 == 1) {
                    q();
                } else if (d2 == 249) {
                    this.c.d = new zf1();
                    j();
                } else if (d2 == 254) {
                    q();
                } else if (d2 != 255) {
                    q();
                } else {
                    f();
                    StringBuilder sb = new StringBuilder();
                    for (int i2 = 0; i2 < 11; i2++) {
                        sb.append((char) this.a[i2]);
                    }
                    if (sb.toString().equals("NETSCAPE2.0")) {
                        m();
                    } else {
                        q();
                    }
                }
            } else if (d == 44) {
                cg1 cg1Var = this.c;
                if (cg1Var.d == null) {
                    cg1Var.d = new zf1();
                }
                e();
            } else if (d != 59) {
                this.c.b = 1;
            } else {
                z = true;
            }
        }
    }

    public final void j() {
        d();
        int d = d();
        zf1 zf1Var = this.c.d;
        int i = (d & 28) >> 2;
        zf1Var.g = i;
        if (i == 0) {
            zf1Var.g = 1;
        }
        zf1Var.f = (d & 1) != 0;
        int n = n();
        if (n < 2) {
            n = 10;
        }
        zf1 zf1Var2 = this.c.d;
        zf1Var2.i = n * 10;
        zf1Var2.h = d();
        d();
    }

    public final void k() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append((char) d());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.c.b = 1;
            return;
        }
        l();
        if (!this.c.h || b()) {
            return;
        }
        cg1 cg1Var = this.c;
        cg1Var.a = g(cg1Var.i);
        cg1 cg1Var2 = this.c;
        cg1Var2.l = cg1Var2.a[cg1Var2.j];
    }

    public final void l() {
        this.c.f = n();
        this.c.g = n();
        int d = d();
        cg1 cg1Var = this.c;
        cg1Var.h = (d & 128) != 0;
        cg1Var.i = (int) Math.pow(2.0d, (d & 7) + 1);
        this.c.j = d();
        this.c.k = d();
    }

    public final void m() {
        do {
            f();
            byte[] bArr = this.a;
            if (bArr[0] == 1) {
                this.c.m = ((bArr[2] & 255) << 8) | (bArr[1] & 255);
            }
            if (this.d <= 0) {
                return;
            }
        } while (!b());
    }

    public final int n() {
        return this.b.getShort();
    }

    public final void o() {
        this.b = null;
        Arrays.fill(this.a, (byte) 0);
        this.c = new cg1();
        this.d = 0;
    }

    public dg1 p(ByteBuffer byteBuffer) {
        o();
        ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        this.b = asReadOnlyBuffer;
        asReadOnlyBuffer.position(0);
        this.b.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    public final void q() {
        int d;
        do {
            d = d();
            this.b.position(Math.min(this.b.position() + d, this.b.limit()));
        } while (d > 0);
    }

    public final void r() {
        d();
        q();
    }
}
