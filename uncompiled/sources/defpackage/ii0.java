package defpackage;

import android.util.SparseIntArray;
import okhttp3.internal.http2.Http2;

/* compiled from: DefaultByteArrayPoolParams.java */
/* renamed from: ii0  reason: default package */
/* loaded from: classes.dex */
public class ii0 {
    public static ys2 a() {
        SparseIntArray sparseIntArray = new SparseIntArray();
        sparseIntArray.put(Http2.INITIAL_MAX_FRAME_SIZE, 5);
        return new ys2(81920, 1048576, sparseIntArray);
    }
}
