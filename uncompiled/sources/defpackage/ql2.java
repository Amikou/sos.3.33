package defpackage;

import java.util.Arrays;

/* compiled from: Objects.java */
/* renamed from: ql2  reason: default package */
/* loaded from: classes2.dex */
public final class ql2 extends o11 {
    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static int b(Object... objArr) {
        return Arrays.hashCode(objArr);
    }
}
