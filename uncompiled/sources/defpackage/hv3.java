package defpackage;

import android.annotation.SuppressLint;

/* compiled from: Styleable.java */
@SuppressLint({"InlinedApi"})
/* renamed from: hv3  reason: default package */
/* loaded from: classes.dex */
public class hv3 {
    public static final int[] a = {16843073, 16843160, 16843746, 16843855};
    public static final int[] b = {16843983};
    public static final int[] c = {16843900};
    public static final int[] d = {16843745};
    public static final int[] e = {16843964, 16843965};
    public static final int[] f = {16843824};
    public static final int[] g = {16843744};
    public static final int[] h = {16843901, 16843902, 16843903};
    public static final int[] i = {16843978};
}
