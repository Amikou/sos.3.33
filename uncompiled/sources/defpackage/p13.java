package defpackage;

/* renamed from: p13  reason: default package */
/* loaded from: classes.dex */
public final class p13 {
    public static final int common_google_play_services_enable_button = 2131951930;
    public static final int common_google_play_services_enable_text = 2131951931;
    public static final int common_google_play_services_enable_title = 2131951932;
    public static final int common_google_play_services_install_button = 2131951933;
    public static final int common_google_play_services_install_text = 2131951934;
    public static final int common_google_play_services_install_title = 2131951935;
    public static final int common_google_play_services_notification_channel_name = 2131951936;
    public static final int common_google_play_services_notification_ticker = 2131951937;
    public static final int common_google_play_services_unsupported_text = 2131951939;
    public static final int common_google_play_services_update_button = 2131951940;
    public static final int common_google_play_services_update_text = 2131951941;
    public static final int common_google_play_services_update_title = 2131951942;
    public static final int common_google_play_services_updating_text = 2131951943;
    public static final int common_google_play_services_wear_update_text = 2131951944;
    public static final int common_open_on_phone = 2131951945;
    public static final int common_signin_button_text = 2131951946;
    public static final int common_signin_button_text_long = 2131951947;
}
