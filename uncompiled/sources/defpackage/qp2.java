package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.io.d;
import com.fasterxml.jackson.core.util.c;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/* compiled from: ParserBase.java */
/* renamed from: qp2  reason: default package */
/* loaded from: classes.dex */
public abstract class qp2 extends rp2 {
    public static final BigInteger G0;
    public static final BigInteger H0;
    public static final BigInteger I0;
    public static final BigInteger J0;
    public static final BigDecimal K0;
    public static final BigDecimal L0;
    public static final BigDecimal M0;
    public static final BigDecimal N0;
    public long A0;
    public double B0;
    public BigInteger C0;
    public BigDecimal D0;
    public boolean E0;
    public int F0;
    public final jm1 h0;
    public boolean i0;
    public int j0;
    public int k0;
    public long l0;
    public int m0;
    public int n0;
    public long o0;
    public int p0;
    public int q0;
    public hv1 r0;
    public JsonToken s0;
    public final c t0;
    public char[] u0;
    public boolean v0;
    public ms w0;
    public byte[] x0;
    public int y0;
    public int z0;

    static {
        BigInteger valueOf = BigInteger.valueOf(-2147483648L);
        G0 = valueOf;
        BigInteger valueOf2 = BigInteger.valueOf(2147483647L);
        H0 = valueOf2;
        BigInteger valueOf3 = BigInteger.valueOf(Long.MIN_VALUE);
        I0 = valueOf3;
        BigInteger valueOf4 = BigInteger.valueOf(Long.MAX_VALUE);
        J0 = valueOf4;
        K0 = new BigDecimal(valueOf3);
        L0 = new BigDecimal(valueOf4);
        M0 = new BigDecimal(valueOf);
        N0 = new BigDecimal(valueOf2);
    }

    public qp2(jm1 jm1Var, int i) {
        super(i);
        this.m0 = 1;
        this.p0 = 1;
        this.y0 = 0;
        this.h0 = jm1Var;
        this.t0 = jm1Var.k();
        this.r0 = hv1.n(JsonParser.Feature.STRICT_DUPLICATE_DETECTION.enabledIn(i) ? ms0.f(this) : null);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public float A() throws IOException {
        return (float) x();
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public boolean B0() {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            return true;
        }
        if (jsonToken == JsonToken.FIELD_NAME) {
            return this.v0;
        }
        return false;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int F() throws IOException {
        int i = this.y0;
        if ((i & 1) == 0) {
            if (i == 0) {
                return U1();
            }
            if ((i & 1) == 0) {
                d2();
            }
        }
        return this.z0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public long M() throws IOException {
        int i = this.y0;
        if ((i & 2) == 0) {
            if (i == 0) {
                V1(2);
            }
            if ((this.y0 & 2) == 0) {
                e2();
            }
        }
        return this.A0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser.NumberType N() throws IOException {
        if (this.y0 == 0) {
            V1(0);
        }
        if (this.g0 == JsonToken.VALUE_NUMBER_INT) {
            int i = this.y0;
            if ((i & 1) != 0) {
                return JsonParser.NumberType.INT;
            }
            if ((i & 2) != 0) {
                return JsonParser.NumberType.LONG;
            }
            return JsonParser.NumberType.BIG_INTEGER;
        } else if ((this.y0 & 16) != 0) {
            return JsonParser.NumberType.BIG_DECIMAL;
        } else {
            return JsonParser.NumberType.DOUBLE;
        }
    }

    public void N1(int i, int i2) {
        int mask = JsonParser.Feature.STRICT_DUPLICATE_DETECTION.getMask();
        if ((i2 & mask) == 0 || (i & mask) == 0) {
            return;
        }
        if (this.r0.p() == null) {
            this.r0 = this.r0.u(ms0.f(this));
        } else {
            this.r0 = this.r0.u(null);
        }
    }

    public abstract void O1() throws IOException;

    public final int P1(Base64Variant base64Variant, char c, int i) throws IOException {
        if (c == '\\') {
            char R1 = R1();
            if (R1 > ' ' || i != 0) {
                int decodeBase64Char = base64Variant.decodeBase64Char(R1);
                if (decodeBase64Char >= 0) {
                    return decodeBase64Char;
                }
                throw g2(base64Variant, R1, i);
            }
            return -1;
        }
        throw g2(base64Variant, c, i);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Number Q() throws IOException {
        if (this.y0 == 0) {
            V1(0);
        }
        if (this.g0 == JsonToken.VALUE_NUMBER_INT) {
            int i = this.y0;
            if ((i & 1) != 0) {
                return Integer.valueOf(this.z0);
            }
            if ((i & 2) != 0) {
                return Long.valueOf(this.A0);
            }
            if ((i & 4) != 0) {
                return this.C0;
            }
            return this.D0;
        }
        int i2 = this.y0;
        if ((i2 & 16) != 0) {
            return this.D0;
        }
        if ((i2 & 8) == 0) {
            J1();
        }
        return Double.valueOf(this.B0);
    }

    public final int Q1(Base64Variant base64Variant, int i, int i2) throws IOException {
        if (i == 92) {
            char R1 = R1();
            if (R1 > ' ' || i2 != 0) {
                int decodeBase64Char = base64Variant.decodeBase64Char((int) R1);
                if (decodeBase64Char >= 0) {
                    return decodeBase64Char;
                }
                throw g2(base64Variant, R1, i2);
            }
            return -1;
        }
        throw g2(base64Variant, i, i2);
    }

    public abstract char R1() throws IOException;

    public final int S1() throws JsonParseException {
        r1();
        return -1;
    }

    public ms T1() {
        ms msVar = this.w0;
        if (msVar == null) {
            this.w0 = new ms();
        } else {
            msVar.j();
        }
        return this.w0;
    }

    public int U1() throws IOException {
        if (this.g0 == JsonToken.VALUE_NUMBER_INT) {
            char[] q = this.t0.q();
            int r = this.t0.r();
            int i = this.F0;
            if (this.E0) {
                r++;
            }
            if (i <= 9) {
                int k = d.k(q, r, i);
                if (this.E0) {
                    k = -k;
                }
                this.z0 = k;
                this.y0 = 1;
                return k;
            }
        }
        V1(1);
        if ((this.y0 & 1) == 0) {
            d2();
        }
        return this.z0;
    }

    public void V1(int i) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_NUMBER_INT) {
            char[] q = this.t0.q();
            int r = this.t0.r();
            int i2 = this.F0;
            if (this.E0) {
                r++;
            }
            if (i2 <= 9) {
                int k = d.k(q, r, i2);
                if (this.E0) {
                    k = -k;
                }
                this.z0 = k;
                this.y0 = 1;
            } else if (i2 <= 18) {
                long m = d.m(q, r, i2);
                boolean z = this.E0;
                if (z) {
                    m = -m;
                }
                if (i2 == 10) {
                    if (z) {
                        if (m >= -2147483648L) {
                            this.z0 = (int) m;
                            this.y0 = 1;
                            return;
                        }
                    } else if (m <= 2147483647L) {
                        this.z0 = (int) m;
                        this.y0 = 1;
                        return;
                    }
                }
                this.A0 = m;
                this.y0 = 2;
            } else {
                X1(i, q, r, i2);
            }
        } else if (jsonToken == JsonToken.VALUE_NUMBER_FLOAT) {
            W1(i);
        } else {
            w1("Current token (" + this.g0 + ") not numeric, can not use numeric value accessors");
        }
    }

    public final void W1(int i) throws IOException {
        try {
            if (i == 16) {
                this.D0 = this.t0.h();
                this.y0 = 16;
            } else {
                this.B0 = this.t0.i();
                this.y0 = 8;
            }
        } catch (NumberFormatException e) {
            M1("Malformed numeric value '" + this.t0.j() + "'", e);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser X0(int i, int i2) {
        int i3 = this.a;
        int i4 = (i & i2) | ((~i2) & i3);
        int i5 = i3 ^ i4;
        if (i5 != 0) {
            this.a = i4;
            N1(i4, i5);
        }
        return this;
    }

    public final void X1(int i, char[] cArr, int i2, int i3) throws IOException {
        String j = this.t0.j();
        try {
            if (d.c(cArr, i2, i3, this.E0)) {
                this.A0 = Long.parseLong(j);
                this.y0 = 2;
            } else {
                this.C0 = new BigInteger(j);
                this.y0 = 4;
            }
        } catch (NumberFormatException e) {
            M1("Malformed numeric value '" + j + "'", e);
        }
    }

    public void Y1() throws IOException {
        this.t0.s();
        char[] cArr = this.u0;
        if (cArr != null) {
            this.u0 = null;
            this.h0.q(cArr);
        }
    }

    public void Z1(int i, char c) throws JsonParseException {
        w1("Unexpected close marker '" + ((char) i) + "': expected '" + c + "' (for " + this.r0.i() + " starting at " + ("" + this.r0.r(this.h0.m())) + ")");
    }

    public void a2() throws IOException {
        int i = this.y0;
        if ((i & 8) != 0) {
            this.D0 = d.f(X());
        } else if ((i & 4) != 0) {
            this.D0 = new BigDecimal(this.C0);
        } else if ((i & 2) != 0) {
            this.D0 = BigDecimal.valueOf(this.A0);
        } else if ((i & 1) != 0) {
            this.D0 = BigDecimal.valueOf(this.z0);
        } else {
            J1();
        }
        this.y0 |= 16;
    }

    public void b2() throws IOException {
        int i = this.y0;
        if ((i & 16) != 0) {
            this.C0 = this.D0.toBigInteger();
        } else if ((i & 2) != 0) {
            this.C0 = BigInteger.valueOf(this.A0);
        } else if ((i & 1) != 0) {
            this.C0 = BigInteger.valueOf(this.z0);
        } else if ((i & 8) != 0) {
            this.C0 = BigDecimal.valueOf(this.B0).toBigInteger();
        } else {
            J1();
        }
        this.y0 |= 4;
    }

    public void c2() throws IOException {
        int i = this.y0;
        if ((i & 16) != 0) {
            this.B0 = this.D0.doubleValue();
        } else if ((i & 4) != 0) {
            this.B0 = this.C0.doubleValue();
        } else if ((i & 2) != 0) {
            this.B0 = this.A0;
        } else if ((i & 1) != 0) {
            this.B0 = this.z0;
        } else {
            J1();
        }
        this.y0 |= 8;
    }

    @Override // com.fasterxml.jackson.core.JsonParser, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (this.i0) {
            return;
        }
        this.i0 = true;
        try {
            O1();
        } finally {
            Y1();
        }
    }

    public void d2() throws IOException {
        int i = this.y0;
        if ((i & 2) != 0) {
            long j = this.A0;
            int i2 = (int) j;
            if (i2 != j) {
                w1("Numeric value (" + X() + ") out of range of int");
            }
            this.z0 = i2;
        } else if ((i & 4) != 0) {
            if (G0.compareTo(this.C0) > 0 || H0.compareTo(this.C0) < 0) {
                j2();
            }
            this.z0 = this.C0.intValue();
        } else if ((i & 8) != 0) {
            double d = this.B0;
            if (d < -2.147483648E9d || d > 2.147483647E9d) {
                j2();
            }
            this.z0 = (int) this.B0;
        } else if ((i & 16) != 0) {
            if (M0.compareTo(this.D0) > 0 || N0.compareTo(this.D0) < 0) {
                j2();
            }
            this.z0 = this.D0.intValue();
        } else {
            J1();
        }
        this.y0 |= 1;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public void e1(Object obj) {
        this.r0.h(obj);
    }

    public void e2() throws IOException {
        int i = this.y0;
        if ((i & 1) != 0) {
            this.A0 = this.z0;
        } else if ((i & 4) != 0) {
            if (I0.compareTo(this.C0) > 0 || J0.compareTo(this.C0) < 0) {
                k2();
            }
            this.A0 = this.C0.longValue();
        } else if ((i & 8) != 0) {
            double d = this.B0;
            if (d < -9.223372036854776E18d || d > 9.223372036854776E18d) {
                k2();
            }
            this.A0 = (long) this.B0;
        } else if ((i & 16) != 0) {
            if (K0.compareTo(this.D0) > 0 || L0.compareTo(this.D0) < 0) {
                k2();
            }
            this.A0 = this.D0.longValue();
        } else {
            J1();
        }
        this.y0 |= 2;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    @Deprecated
    public JsonParser f1(int i) {
        int i2 = this.a ^ i;
        if (i2 != 0) {
            this.a = i;
            N1(i, i2);
        }
        return this;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    /* renamed from: f2 */
    public hv1 S() {
        return this.r0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonParser g(JsonParser.Feature feature) {
        this.a |= feature.getMask();
        if (feature == JsonParser.Feature.STRICT_DUPLICATE_DETECTION && this.r0.p() == null) {
            this.r0 = this.r0.u(ms0.f(this));
        }
        return this;
    }

    public IllegalArgumentException g2(Base64Variant base64Variant, int i, int i2) throws IllegalArgumentException {
        return h2(base64Variant, i, i2, null);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public BigInteger h() throws IOException {
        int i = this.y0;
        if ((i & 4) == 0) {
            if (i == 0) {
                V1(4);
            }
            if ((this.y0 & 4) == 0) {
                b2();
            }
        }
        return this.C0;
    }

    public IllegalArgumentException h2(Base64Variant base64Variant, int i, int i2, String str) throws IllegalArgumentException {
        String str2;
        if (i <= 32) {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(i) + ") as character #" + (i2 + 1) + " of 4-char base64 unit: can only used between units";
        } else if (base64Variant.usesPaddingChar(i)) {
            str2 = "Unexpected padding character ('" + base64Variant.getPaddingChar() + "') as character #" + (i2 + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (Character.isDefined(i) && !Character.isISOControl(i)) {
            str2 = "Illegal character '" + ((char) i) + "' (code 0x" + Integer.toHexString(i) + ") in base64 content";
        } else {
            str2 = "Illegal character (code 0x" + Integer.toHexString(i) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        return new IllegalArgumentException(str2);
    }

    public void i2(String str) throws JsonParseException {
        w1("Invalid numeric value: " + str);
    }

    public void j2() throws IOException {
        w1(String.format("Numeric value (%s) out of range of int (%d - %s)", X(), Integer.MIN_VALUE, Integer.MAX_VALUE));
    }

    public void k2() throws IOException {
        w1(String.format("Numeric value (%s) out of range of long (%d - %s)", X(), Long.MIN_VALUE, Long.MAX_VALUE));
    }

    public void l2(int i, String str) throws JsonParseException {
        String str2 = "Unexpected character (" + rp2.p1(i) + ") in numeric value";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        w1(str2);
    }

    public final JsonToken m2(boolean z, int i, int i2, int i3) {
        if (i2 < 1 && i3 < 1) {
            return p2(z, i);
        }
        return o2(z, i, i2, i3);
    }

    public final JsonToken n2(String str, double d) {
        this.t0.w(str);
        this.B0 = d;
        this.y0 = 8;
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    public final JsonToken o2(boolean z, int i, int i2, int i3) {
        this.E0 = z;
        this.F0 = i;
        this.y0 = 0;
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    public final JsonToken p2(boolean z, int i) {
        this.E0 = z;
        this.F0 = i;
        this.y0 = 0;
        return JsonToken.VALUE_NUMBER_INT;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String r() throws IOException {
        hv1 d;
        JsonToken jsonToken = this.g0;
        if ((jsonToken == JsonToken.START_OBJECT || jsonToken == JsonToken.START_ARRAY) && (d = this.r0.d()) != null) {
            return d.b();
        }
        return this.r0.b();
    }

    @Override // defpackage.rp2
    public void r1() throws JsonParseException {
        if (this.r0.g()) {
            return;
        }
        z1(String.format(": expected close marker for %s (start marker at %s)", this.r0.e() ? "Array" : "Object", this.r0.r(this.h0.m())), null);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public BigDecimal w() throws IOException {
        int i = this.y0;
        if ((i & 16) == 0) {
            if (i == 0) {
                V1(16);
            }
            if ((this.y0 & 16) == 0) {
                a2();
            }
        }
        return this.D0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public double x() throws IOException {
        int i = this.y0;
        if ((i & 8) == 0) {
            if (i == 0) {
                V1(8);
            }
            if ((this.y0 & 8) == 0) {
                c2();
            }
        }
        return this.B0;
    }
}
