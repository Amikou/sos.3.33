package defpackage;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;

/* compiled from: JsonWriteContext.java */
/* renamed from: mw1  reason: default package */
/* loaded from: classes.dex */
public class mw1 extends cw1 {
    public final mw1 c;
    public ms0 d;
    public mw1 e;
    public String f;
    public Object g;
    public boolean h;

    public mw1(int i, mw1 mw1Var, ms0 ms0Var) {
        this.a = i;
        this.c = mw1Var;
        this.d = ms0Var;
        this.b = -1;
    }

    public static mw1 o(ms0 ms0Var) {
        return new mw1(0, null, ms0Var);
    }

    @Override // defpackage.cw1
    public final String b() {
        return this.f;
    }

    @Override // defpackage.cw1
    public void h(Object obj) {
        this.g = obj;
    }

    public final void j(ms0 ms0Var, String str) throws JsonProcessingException {
        if (ms0Var.c(str)) {
            Object b = ms0Var.b();
            throw new JsonGenerationException("Duplicate field '" + str + "'", b instanceof JsonGenerator ? (JsonGenerator) b : null);
        }
    }

    public void k(StringBuilder sb) {
        int i = this.a;
        if (i != 2) {
            if (i == 1) {
                sb.append('[');
                sb.append(a());
                sb.append(']');
                return;
            }
            sb.append("/");
            return;
        }
        sb.append('{');
        if (this.f != null) {
            sb.append('\"');
            sb.append(this.f);
            sb.append('\"');
        } else {
            sb.append('?');
        }
        sb.append('}');
    }

    public mw1 l() {
        return this.c;
    }

    public mw1 m() {
        mw1 mw1Var = this.e;
        if (mw1Var == null) {
            ms0 ms0Var = this.d;
            mw1 mw1Var2 = new mw1(1, this, ms0Var == null ? null : ms0Var.a());
            this.e = mw1Var2;
            return mw1Var2;
        }
        return mw1Var.r(1);
    }

    public mw1 n() {
        mw1 mw1Var = this.e;
        if (mw1Var == null) {
            ms0 ms0Var = this.d;
            mw1 mw1Var2 = new mw1(2, this, ms0Var == null ? null : ms0Var.a());
            this.e = mw1Var2;
            return mw1Var2;
        }
        return mw1Var.r(2);
    }

    public ms0 p() {
        return this.d;
    }

    @Override // defpackage.cw1
    /* renamed from: q */
    public final mw1 d() {
        return this.c;
    }

    public mw1 r(int i) {
        this.a = i;
        this.b = -1;
        this.f = null;
        this.h = false;
        ms0 ms0Var = this.d;
        if (ms0Var != null) {
            ms0Var.d();
        }
        return this;
    }

    public mw1 s(ms0 ms0Var) {
        this.d = ms0Var;
        return this;
    }

    public int t(String str) throws JsonProcessingException {
        if (this.a != 2 || this.h) {
            return 4;
        }
        this.h = true;
        this.f = str;
        ms0 ms0Var = this.d;
        if (ms0Var != null) {
            j(ms0Var, str);
        }
        return this.b < 0 ? 0 : 1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        k(sb);
        return sb.toString();
    }

    public int u() {
        int i = this.a;
        if (i == 2) {
            if (this.h) {
                this.h = false;
                this.b++;
                return 2;
            }
            return 5;
        } else if (i == 1) {
            int i2 = this.b;
            this.b = i2 + 1;
            return i2 < 0 ? 0 : 1;
        } else {
            int i3 = this.b + 1;
            this.b = i3;
            return i3 == 0 ? 0 : 3;
        }
    }
}
