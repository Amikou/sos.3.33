package defpackage;

import com.google.gson.JsonObject;
import net.safemoon.androidwallet.model.HistoricalList;
import net.safemoon.androidwallet.model.cmc.coinPrice.CoinPriceStats;
import net.safemoon.androidwallet.model.cmcTokenInfo.CmcTokenInfo;
import retrofit2.b;

/* compiled from: CMCApiInterface.kt */
/* renamed from: jt  reason: default package */
/* loaded from: classes2.dex */
public interface jt {
    @hk1({"Cache-Control: no-cache"})
    @ge1("/v1/cryptocurrency/quotes/latest?")
    b<JsonObject> a(@yw2("symbol") String str, @yw2("convert") String str2);

    @hk1({"Cache-Control: no-cache"})
    @ge1("/v1/cryptocurrency/quotes/latest?")
    b<JsonObject> b(@yw2("id") String str, @yw2("convert") String str2);

    @ge1("/v1/cryptocurrency/quotes/historical?")
    b<HistoricalList> c(@yw2("symbol") String str, @yw2("interval") String str2, @yw2("time_start") String str3, @yw2("time_end") String str4, @yw2("convert") String str5);

    @hk1({"Cache-Control:max-age=600"})
    @ge1("/v1/cryptocurrency/info")
    b<CmcTokenInfo> d(@yw2("address") String str);

    @ge1("/v1/cryptocurrency/ohlcv/historical")
    b<CoinPriceStats> e(@yw2("id") String str, @yw2("time_start") String str2, @yw2("time_end") String str3, @yw2("convert") String str4, @yw2("time_period") String str5, @yw2("interval") String str6);

    @hk1({"Cache-Control:max-age=600"})
    @ge1("/v1/cryptocurrency/info")
    b<CmcTokenInfo> f(@yw2("id") int i);

    @ge1("/v1/cryptocurrency/ohlcv/historical")
    b<CoinPriceStats> g(@yw2("symbol") String str, @yw2("time_start") String str2, @yw2("time_end") String str3, @yw2("convert") String str4, @yw2("time_period") String str5, @yw2("interval") String str6);

    @ge1("/v1/cryptocurrency/quotes/historical?")
    b<HistoricalList> h(@yw2("id") String str, @yw2("interval") String str2, @yw2("time_start") String str3, @yw2("time_end") String str4, @yw2("convert") String str5);
}
