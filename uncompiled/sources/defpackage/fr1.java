package defpackage;

/* compiled from: InstanceFactory.java */
/* renamed from: fr1  reason: default package */
/* loaded from: classes2.dex */
public final class fr1<T> implements y11<T> {
    public final T a;

    static {
        new fr1(null);
    }

    public fr1(T t) {
        this.a = t;
    }

    public static <T> y11<T> a(T t) {
        return new fr1(cu2.c(t, "instance cannot be null"));
    }

    @Override // defpackage.ew2
    public T get() {
        return this.a;
    }
}
