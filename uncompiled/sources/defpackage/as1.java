package defpackage;

import android.util.SparseArray;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.i;

/* compiled from: IntentRegistry.java */
/* renamed from: as1  reason: default package */
/* loaded from: classes3.dex */
public class as1 {
    public SparseArray<MediaResult> a = new SparseArray<>();

    public void a(int i) {
        synchronized (this) {
            this.a.remove(i);
        }
    }

    public MediaResult b(int i) {
        MediaResult mediaResult;
        synchronized (this) {
            mediaResult = this.a.get(i);
        }
        return mediaResult;
    }

    public final int c() {
        for (int i = 1600; i < 1650; i++) {
            if (this.a.get(i) == null) {
                return i;
            }
        }
        i.a("Belvedere", "No slot free. Clearing registry.");
        this.a.clear();
        return c();
    }

    public int d() {
        int c;
        synchronized (this) {
            c = c();
            this.a.put(c, MediaResult.d());
        }
        return c;
    }

    public void e(int i, MediaResult mediaResult) {
        synchronized (this) {
            this.a.put(i, mediaResult);
        }
    }
}
