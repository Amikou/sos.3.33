package defpackage;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-measurement-api@@19.0.0 */
/* renamed from: s35  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class s35 implements Executor {
    public static final Executor a = new s35();

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
