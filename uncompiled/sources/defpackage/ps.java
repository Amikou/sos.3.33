package defpackage;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: ByteBufferEncoder.java */
/* renamed from: ps  reason: default package */
/* loaded from: classes.dex */
public class ps implements ev0<ByteBuffer> {
    @Override // defpackage.ev0
    /* renamed from: c */
    public boolean a(ByteBuffer byteBuffer, File file, vn2 vn2Var) {
        try {
            ts.f(byteBuffer, file);
            return true;
        } catch (IOException unused) {
            return false;
        }
    }
}
