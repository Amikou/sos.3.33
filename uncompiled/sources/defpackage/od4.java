package defpackage;

import java.math.BigInteger;
import java.util.ArrayList;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Array;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Bytes;
import org.web3j.abi.datatypes.BytesType;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.DynamicBytes;
import org.web3j.abi.datatypes.DynamicStruct;
import org.web3j.abi.datatypes.NumericType;
import org.web3j.abi.datatypes.StaticArray;
import org.web3j.abi.datatypes.Ufixed;
import org.web3j.abi.datatypes.Uint;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.primitive.PrimitiveType;

/* compiled from: TypeEncoder.java */
/* renamed from: od4  reason: default package */
/* loaded from: classes2.dex */
public class od4 {
    private od4() {
    }

    public static String encode(zc4 zc4Var) {
        if (zc4Var instanceof NumericType) {
            return encodeNumeric((NumericType) zc4Var);
        }
        if (zc4Var instanceof Address) {
            return encodeAddress((Address) zc4Var);
        }
        if (zc4Var instanceof Bool) {
            return encodeBool((Bool) zc4Var);
        }
        if (zc4Var instanceof Bytes) {
            return encodeBytes((Bytes) zc4Var);
        }
        if (zc4Var instanceof DynamicBytes) {
            return encodeDynamicBytes((DynamicBytes) zc4Var);
        }
        if (zc4Var instanceof Utf8String) {
            return encodeString((Utf8String) zc4Var);
        }
        if (zc4Var instanceof StaticArray) {
            return encodeArrayValues((StaticArray) zc4Var);
        }
        if (zc4Var instanceof DynamicStruct) {
            return encodeDynamicStruct((DynamicStruct) zc4Var);
        }
        if (zc4Var instanceof DynamicArray) {
            return encodeDynamicArray((DynamicArray) zc4Var);
        }
        if (zc4Var instanceof PrimitiveType) {
            return encode(((PrimitiveType) zc4Var).toSolidityType());
        }
        throw new UnsupportedOperationException("Type cannot be encoded: " + zc4Var.getClass());
    }

    public static String encodeAddress(Address address) {
        return encodeNumeric(address.toUint());
    }

    public static <T extends zc4> String encodeArrayValues(Array<T> array) {
        StringBuilder sb = new StringBuilder();
        for (T t : array.getValue()) {
            sb.append(encode(t));
        }
        return sb.toString();
    }

    private static <T extends zc4> String encodeArrayValuesOffsets(DynamicArray<T> dynamicArray) {
        int length;
        StringBuilder sb = new StringBuilder();
        boolean z = !dynamicArray.getValue().isEmpty() && (dynamicArray.getValue().get(0) instanceof DynamicBytes);
        boolean z2 = !dynamicArray.getValue().isEmpty() && (dynamicArray.getValue().get(0) instanceof Utf8String);
        if (z || z2) {
            long j = 0;
            for (int i = 0; i < dynamicArray.getValue().size(); i++) {
                if (i == 0) {
                    j = dynamicArray.getValue().size() * 32;
                } else {
                    if (z) {
                        length = ((byte[]) dynamicArray.getValue().get(i - 1).getValue()).length;
                    } else {
                        length = ((String) dynamicArray.getValue().get(i - 1).getValue()).length();
                    }
                    j += ((((length + 32) - 1) / 32) * 32) + 32;
                }
                sb.append(ej2.toHexStringNoPrefix(ej2.toBytesPadded(new BigInteger(Long.toString(j)), 32)));
            }
        }
        return sb.toString();
    }

    public static String encodeBool(Bool bool) {
        byte[] bArr = new byte[32];
        if (bool.getValue().booleanValue()) {
            bArr[31] = 1;
        }
        return ej2.toHexStringNoPrefix(bArr);
    }

    public static String encodeBytes(BytesType bytesType) {
        byte[] value = bytesType.getValue();
        int length = value.length;
        int i = length % 32;
        if (i != 0) {
            byte[] bArr = new byte[(32 - i) + length];
            System.arraycopy(value, 0, bArr, 0, length);
            value = bArr;
        }
        return ej2.toHexStringNoPrefix(value);
    }

    public static <T extends zc4> String encodeDynamicArray(DynamicArray<T> dynamicArray) {
        String encode = encode(new Uint(BigInteger.valueOf(dynamicArray.getValue().size())));
        String encodeArrayValuesOffsets = encodeArrayValuesOffsets(dynamicArray);
        String encodeArrayValues = encodeArrayValues(dynamicArray);
        return encode + encodeArrayValuesOffsets + encodeArrayValues;
    }

    public static String encodeDynamicBytes(DynamicBytes dynamicBytes) {
        String encode = encode(new Uint(BigInteger.valueOf(dynamicBytes.getValue().length)));
        String encodeBytes = encodeBytes(dynamicBytes);
        return encode + encodeBytes;
    }

    public static String encodeDynamicStruct(DynamicStruct dynamicStruct) {
        String encodeDynamicStructValues = encodeDynamicStructValues(dynamicStruct);
        return encodeDynamicStructValues;
    }

    private static String encodeDynamicStructValues(DynamicStruct dynamicStruct) {
        int i = 0;
        for (int i2 = 0; i2 < dynamicStruct.getValue().size(); i2++) {
            zc4 zc4Var = (zc4) dynamicStruct.getValue().get(i2);
            i = isDynamic(zc4Var) ? i + 32 : i + zc4Var.bytes32PaddedLength();
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < dynamicStruct.getValue().size(); i3++) {
            zc4 zc4Var2 = (zc4) dynamicStruct.getValue().get(i3);
            if (isDynamic(zc4Var2)) {
                arrayList.add(ej2.toHexStringNoPrefix(ej2.toBytesPadded(new BigInteger(Long.toString(i)), 32)));
                arrayList2.add(encode(zc4Var2));
                i += zc4Var2.bytes32PaddedLength();
            } else {
                arrayList.add(encode((zc4) dynamicStruct.getValue().get(i3)));
            }
        }
        ArrayList arrayList3 = new ArrayList();
        arrayList3.addAll(arrayList);
        arrayList3.addAll(arrayList2);
        return m30.join("", (CharSequence[]) arrayList3.toArray(new String[0]));
    }

    public static String encodeNumeric(NumericType numericType) {
        byte[] byteArray = toByteArray(numericType);
        byte paddingValue = getPaddingValue(numericType);
        byte[] bArr = new byte[32];
        if (paddingValue != 0) {
            for (int i = 0; i < 32; i++) {
                bArr[i] = paddingValue;
            }
        }
        System.arraycopy(byteArray, 0, bArr, 32 - byteArray.length, byteArray.length);
        return ej2.toHexStringNoPrefix(bArr);
    }

    public static String encodeString(Utf8String utf8String) {
        return encodeDynamicBytes(new DynamicBytes(utf8String.getValue().getBytes(m30.UTF_8)));
    }

    private static byte getPaddingValue(NumericType numericType) {
        return numericType.getValue().signum() == -1 ? (byte) -1 : (byte) 0;
    }

    public static boolean isDynamic(zc4 zc4Var) {
        return (zc4Var instanceof DynamicBytes) || (zc4Var instanceof Utf8String) || (zc4Var instanceof DynamicArray);
    }

    private static byte[] toByteArray(NumericType numericType) {
        BigInteger value = numericType.getValue();
        if (((numericType instanceof Ufixed) || (numericType instanceof Uint)) && value.bitLength() == 256) {
            byte[] bArr = new byte[32];
            System.arraycopy(value.toByteArray(), 1, bArr, 0, 32);
            return bArr;
        }
        return value.toByteArray();
    }
}
