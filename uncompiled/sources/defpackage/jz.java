package defpackage;

import android.content.Context;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: jz  reason: default package */
/* loaded from: classes.dex */
public class jz {
    @RecentlyNonNull
    public static boolean a() {
        return false;
    }

    @RecentlyNonNull
    public static boolean b(@RecentlyNonNull Context context, @RecentlyNonNull String str) {
        "com.google.android.gms".equals(str);
        return (kr4.a(context).c(str, 0).flags & 2097152) != 0;
    }
}
