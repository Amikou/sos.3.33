package defpackage;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: i75  reason: default package */
/* loaded from: classes.dex */
public final class i75 {
    public final Object b = new Object();
    public long c = Long.MIN_VALUE;
    public final long a = Math.round(30000.0d);

    public i75(double d) {
    }

    public final boolean a() {
        synchronized (this.b) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.c + this.a > currentTimeMillis) {
                return false;
            }
            this.c = currentTimeMillis;
            return true;
        }
    }
}
