package defpackage;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigInteger;
import java.util.List;

/* compiled from: ShhPost.java */
@JsonInclude(JsonInclude.Include.NON_NULL)
/* renamed from: mo3  reason: default package */
/* loaded from: classes3.dex */
public class mo3 {
    private String from;
    private String payload;
    private BigInteger priority;
    private String to;
    private List<String> topics;
    private BigInteger ttl;

    public mo3(List<String> list, String str, BigInteger bigInteger, BigInteger bigInteger2) {
        this.topics = list;
        this.payload = str;
        this.priority = bigInteger;
        this.ttl = bigInteger2;
    }

    private String convert(BigInteger bigInteger) {
        if (bigInteger != null) {
            return ej2.encodeQuantity(bigInteger);
        }
        return null;
    }

    public String getFrom() {
        return this.from;
    }

    public String getPayload() {
        return this.payload;
    }

    public String getPriority() {
        return convert(this.priority);
    }

    public String getTo() {
        return this.to;
    }

    public List<String> getTopics() {
        return this.topics;
    }

    public String getTtl() {
        return convert(this.ttl);
    }

    public mo3(String str, String str2, List<String> list, String str3, BigInteger bigInteger, BigInteger bigInteger2) {
        this.from = str;
        this.to = str2;
        this.topics = list;
        this.payload = str3;
        this.priority = bigInteger;
        this.ttl = bigInteger2;
    }
}
