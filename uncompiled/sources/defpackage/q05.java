package defpackage;

import java.util.concurrent.locks.Lock;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: q05  reason: default package */
/* loaded from: classes.dex */
public abstract class q05 {
    public final j05 a;

    public q05(j05 j05Var) {
        this.a = j05Var;
    }

    public abstract void a();

    public final void b(i05 i05Var) {
        Lock lock;
        Lock lock2;
        j05 j05Var;
        lock = i05Var.a;
        lock.lock();
        try {
            j05Var = i05Var.k;
            if (j05Var != this.a) {
                return;
            }
            a();
        } finally {
            lock2 = i05Var.a;
            lock2.unlock();
        }
    }
}
