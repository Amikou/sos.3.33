package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;

/* compiled from: PlatformBitmapFactory.java */
/* renamed from: br2  reason: default package */
/* loaded from: classes.dex */
public abstract class br2 {
    public a<Bitmap> a(int i, int i2, Bitmap.Config config) {
        return b(i, i2, config, null);
    }

    public a<Bitmap> b(int i, int i2, Bitmap.Config config, Object obj) {
        return c(i, i2, config);
    }

    public abstract a<Bitmap> c(int i, int i2, Bitmap.Config config);
}
