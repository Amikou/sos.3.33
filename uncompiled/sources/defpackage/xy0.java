package defpackage;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: ExecutorUtils.java */
/* renamed from: xy0  reason: default package */
/* loaded from: classes2.dex */
public final class xy0 {

    /* compiled from: ExecutorUtils.java */
    /* renamed from: xy0$a */
    /* loaded from: classes2.dex */
    public class a implements ThreadFactory {
        public final /* synthetic */ String a;
        public final /* synthetic */ AtomicLong f0;

        /* compiled from: ExecutorUtils.java */
        /* renamed from: xy0$a$a  reason: collision with other inner class name */
        /* loaded from: classes2.dex */
        public class C0291a extends cm {
            public final /* synthetic */ Runnable a;

            public C0291a(a aVar, Runnable runnable) {
                this.a = runnable;
            }

            @Override // defpackage.cm
            public void a() {
                this.a.run();
            }
        }

        public a(String str, AtomicLong atomicLong) {
            this.a = str;
            this.f0 = atomicLong;
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new C0291a(this, runnable));
            newThread.setName(this.a + this.f0.getAndIncrement());
            return newThread;
        }
    }

    /* compiled from: ExecutorUtils.java */
    /* renamed from: xy0$b */
    /* loaded from: classes2.dex */
    public class b extends cm {
        public final /* synthetic */ String a;
        public final /* synthetic */ ExecutorService f0;
        public final /* synthetic */ long g0;
        public final /* synthetic */ TimeUnit h0;

        public b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.a = str;
            this.f0 = executorService;
            this.g0 = j;
            this.h0 = timeUnit;
        }

        @Override // defpackage.cm
        public void a() {
            try {
                w12 f = w12.f();
                f.b("Executing shutdown hook for " + this.a);
                this.f0.shutdown();
                if (this.f0.awaitTermination(this.g0, this.h0)) {
                    return;
                }
                w12 f2 = w12.f();
                f2.b(this.a + " did not shut down in the allocated time. Requesting immediate shutdown.");
                this.f0.shutdownNow();
            } catch (InterruptedException unused) {
                w12.f().b(String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", this.a));
                this.f0.shutdownNow();
            }
        }
    }

    public static void a(String str, ExecutorService executorService) {
        b(str, executorService, 2L, TimeUnit.SECONDS);
    }

    public static void b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        b bVar = new b(str, executorService, j, timeUnit);
        runtime.addShutdownHook(new Thread(bVar, "Crashlytics Shutdown Hook for " + str));
    }

    public static ExecutorService c(String str) {
        ExecutorService e = e(d(str), new ThreadPoolExecutor.DiscardPolicy());
        a(str, e);
        return e;
    }

    public static ThreadFactory d(String str) {
        return new a(str, new AtomicLong(1L));
    }

    public static ExecutorService e(ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {
        return Executors.unconfigurableExecutorService(new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(), threadFactory, rejectedExecutionHandler));
    }
}
