package defpackage;

import com.facebook.imagepipeline.request.ImageRequest;

/* compiled from: DiskCacheWriteProducer.java */
/* renamed from: dp0  reason: default package */
/* loaded from: classes.dex */
public class dp0 implements dv2<zu0> {
    public final xr a;
    public final xr b;
    public final xt c;
    public final dv2<zu0> d;

    /* compiled from: DiskCacheWriteProducer.java */
    /* renamed from: dp0$b */
    /* loaded from: classes.dex */
    public static class b extends bm0<zu0, zu0> {
        public final ev2 c;
        public final xr d;
        public final xr e;
        public final xt f;

        @Override // defpackage.qm
        /* renamed from: q */
        public void i(zu0 zu0Var, int i) {
            this.c.l().k(this.c, "DiskCacheWriteProducer");
            if (!qm.f(i) && zu0Var != null && !qm.m(i, 10) && zu0Var.l() != wn1.b) {
                ImageRequest c = this.c.c();
                wt d = this.f.d(c, this.c.a());
                if (c.d() == ImageRequest.CacheChoice.SMALL) {
                    this.e.l(d, zu0Var);
                } else {
                    this.d.l(d, zu0Var);
                }
                this.c.l().a(this.c, "DiskCacheWriteProducer", null);
                p().d(zu0Var, i);
                return;
            }
            this.c.l().a(this.c, "DiskCacheWriteProducer", null);
            p().d(zu0Var, i);
        }

        public b(l60<zu0> l60Var, ev2 ev2Var, xr xrVar, xr xrVar2, xt xtVar) {
            super(l60Var);
            this.c = ev2Var;
            this.d = xrVar;
            this.e = xrVar2;
            this.f = xtVar;
        }
    }

    public dp0(xr xrVar, xr xrVar2, xt xtVar, dv2<zu0> dv2Var) {
        this.a = xrVar;
        this.b = xrVar2;
        this.c = xtVar;
        this.d = dv2Var;
    }

    @Override // defpackage.dv2
    public void a(l60<zu0> l60Var, ev2 ev2Var) {
        b(l60Var, ev2Var);
    }

    public final void b(l60<zu0> l60Var, ev2 ev2Var) {
        if (ev2Var.n().getValue() >= ImageRequest.RequestLevel.DISK_CACHE.getValue()) {
            ev2Var.f("disk", "nil-result_write");
            l60Var.d(null, 1);
            return;
        }
        if (ev2Var.c().x(32)) {
            l60Var = new b(l60Var, ev2Var, this.a, this.b, this.c);
        }
        this.d.a(l60Var, ev2Var);
    }
}
