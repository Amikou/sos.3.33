package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: xq5  reason: default package */
/* loaded from: classes.dex */
public final class xq5 extends wn5<Double> implements gt5<Double>, vw5 {
    public double[] f0;
    public int g0;

    static {
        new xq5(new double[0], 0).zzb();
    }

    public xq5() {
        this(new double[10], 0);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        double doubleValue = ((Double) obj).doubleValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            double[] dArr = this.f0;
            if (i2 < dArr.length) {
                System.arraycopy(dArr, i, dArr, i + 1, i2 - i);
            } else {
                double[] dArr2 = new double[((i2 * 3) / 2) + 1];
                System.arraycopy(dArr, 0, dArr2, 0, i);
                System.arraycopy(this.f0, i, dArr2, i + 1, this.g0 - i);
                this.f0 = dArr2;
            }
            this.f0[i] = doubleValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(m(i));
    }

    @Override // defpackage.wn5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Double> collection) {
        e();
        vs5.d(collection);
        if (!(collection instanceof xq5)) {
            return super.addAll(collection);
        }
        xq5 xq5Var = (xq5) collection;
        int i = xq5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            double[] dArr = this.f0;
            if (i3 > dArr.length) {
                this.f0 = Arrays.copyOf(dArr, i3);
            }
            System.arraycopy(xq5Var.f0, 0, this.f0, this.g0, xq5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.gt5
    public final /* synthetic */ gt5<Double> d(int i) {
        if (i >= this.g0) {
            return new xq5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof xq5)) {
            return super.equals(obj);
        }
        xq5 xq5Var = (xq5) obj;
        if (this.g0 != xq5Var.g0) {
            return false;
        }
        double[] dArr = xq5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (Double.doubleToLongBits(this.f0[i]) != Double.doubleToLongBits(dArr[i])) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        k(i);
        return Double.valueOf(this.f0[i]);
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + vs5.b(Double.doubleToLongBits(this.f0[i2]));
        }
        return i;
    }

    public final void i(double d) {
        e();
        int i = this.g0;
        double[] dArr = this.f0;
        if (i == dArr.length) {
            double[] dArr2 = new double[((i * 3) / 2) + 1];
            System.arraycopy(dArr, 0, dArr2, 0, i);
            this.f0 = dArr2;
        }
        double[] dArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        dArr3[i2] = d;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.f0[i] == doubleValue) {
                    return i;
                }
            }
            return -1;
        }
        return -1;
    }

    public final void k(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(m(i));
        }
    }

    public final String m(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        e();
        k(i);
        double[] dArr = this.f0;
        double d = dArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(dArr, i + 1, dArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            double[] dArr = this.f0;
            System.arraycopy(dArr, i2, dArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        e();
        k(i);
        double[] dArr = this.f0;
        double d = dArr[i];
        dArr[i] = doubleValue;
        return Double.valueOf(d);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    public xq5(double[] dArr, int i) {
        this.f0 = dArr;
        this.g0 = i;
    }

    @Override // defpackage.wn5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* synthetic */ boolean add(Object obj) {
        i(((Double) obj).doubleValue());
        return true;
    }
}
