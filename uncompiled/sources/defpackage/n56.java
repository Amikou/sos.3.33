package defpackage;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.t0;
import com.google.android.gms.internal.measurement.v0;
import com.google.android.gms.internal.measurement.zzep;
import com.google.android.gms.internal.measurement.zzew;
import com.google.android.gms.measurement.internal.r;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: n56  reason: default package */
/* loaded from: classes.dex */
public abstract class n56 {
    public final String a;
    public final int b;
    public Boolean c;
    public Boolean d;
    public Long e;
    public Long f;

    public n56(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public static Boolean d(String str, zzew zzewVar, boolean z, String str2, List<String> list, String str3, og5 og5Var) {
        if (zzewVar == zzew.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && zzewVar != zzew.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        zzep zzepVar = zzep.UNKNOWN_COMPARISON_TYPE;
        switch (zzewVar.ordinal()) {
            case 1:
                if (str3 == null) {
                    return null;
                }
                try {
                    return Boolean.valueOf(Pattern.compile(str3, true != z ? 66 : 0).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    if (og5Var != null) {
                        og5Var.p().b("Invalid regular expression in REGEXP audience filter. expression", str3);
                    }
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                if (list == null) {
                    return null;
                }
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    public static Boolean e(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    public static Boolean f(String str, v0 v0Var, og5 og5Var) {
        String A;
        List<String> list;
        zt2.j(v0Var);
        if (str == null || !v0Var.x() || v0Var.y() == zzew.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        zzew y = v0Var.y();
        zzew zzewVar = zzew.IN_LIST;
        if (y == zzewVar) {
            if (v0Var.E() == 0) {
                return null;
            }
        } else if (!v0Var.z()) {
            return null;
        }
        zzew y2 = v0Var.y();
        boolean C = v0Var.C();
        if (!C && y2 != zzew.REGEXP && y2 != zzewVar) {
            A = v0Var.A().toUpperCase(Locale.ENGLISH);
        } else {
            A = v0Var.A();
        }
        String str2 = A;
        if (v0Var.E() == 0) {
            list = null;
        } else {
            List<String> D = v0Var.D();
            if (!C) {
                ArrayList arrayList = new ArrayList(D.size());
                for (String str3 : D) {
                    arrayList.add(str3.toUpperCase(Locale.ENGLISH));
                }
                D = Collections.unmodifiableList(arrayList);
            }
            list = D;
        }
        return d(str, y2, C, str2, list, y2 == zzew.REGEXP ? str2 : null, og5Var);
    }

    public static Boolean g(long j, t0 t0Var) {
        try {
            return j(new BigDecimal(j), t0Var, Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public static Boolean h(double d, t0 t0Var) {
        try {
            return j(new BigDecimal(d), t0Var, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public static Boolean i(String str, t0 t0Var) {
        if (r.C(str)) {
            try {
                return j(new BigDecimal(str), t0Var, Utils.DOUBLE_EPSILON);
            } catch (NumberFormatException unused) {
                return null;
            }
        }
        return null;
    }

    public static Boolean j(BigDecimal bigDecimal, t0 t0Var, double d) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        zt2.j(t0Var);
        if (t0Var.x() && t0Var.y() != zzep.UNKNOWN_COMPARISON_TYPE) {
            zzep y = t0Var.y();
            zzep zzepVar = zzep.BETWEEN;
            if (y == zzepVar) {
                if (!t0Var.D() || !t0Var.F()) {
                    return null;
                }
            } else if (!t0Var.B()) {
                return null;
            }
            zzep y2 = t0Var.y();
            if (t0Var.y() == zzepVar) {
                if (r.C(t0Var.E()) && r.C(t0Var.G())) {
                    try {
                        BigDecimal bigDecimal5 = new BigDecimal(t0Var.E());
                        bigDecimal4 = new BigDecimal(t0Var.G());
                        bigDecimal3 = bigDecimal5;
                        bigDecimal2 = null;
                    } catch (NumberFormatException unused) {
                    }
                }
                return null;
            } else if (!r.C(t0Var.C())) {
                return null;
            } else {
                try {
                    bigDecimal2 = new BigDecimal(t0Var.C());
                    bigDecimal3 = null;
                    bigDecimal4 = null;
                } catch (NumberFormatException unused2) {
                }
            }
            if (y2 == zzepVar) {
                if (bigDecimal3 == null) {
                    return null;
                }
            } else if (bigDecimal2 == null) {
                return null;
            }
            zzew zzewVar = zzew.UNKNOWN_MATCH_TYPE;
            int ordinal = y2.ordinal();
            if (ordinal == 1) {
                if (bigDecimal2 == null) {
                    return null;
                }
                return Boolean.valueOf(bigDecimal.compareTo(bigDecimal2) < 0);
            } else if (ordinal == 2) {
                if (bigDecimal2 == null) {
                    return null;
                }
                return Boolean.valueOf(bigDecimal.compareTo(bigDecimal2) > 0);
            } else if (ordinal != 3) {
                if (ordinal == 4 && bigDecimal3 != null) {
                    if (bigDecimal.compareTo(bigDecimal3) < 0 || bigDecimal.compareTo(bigDecimal4) > 0) {
                        r2 = false;
                    }
                    return Boolean.valueOf(r2);
                }
                return null;
            } else if (bigDecimal2 == null) {
                return null;
            } else {
                if (d == Utils.DOUBLE_EPSILON) {
                    return Boolean.valueOf(bigDecimal.compareTo(bigDecimal2) == 0);
                }
                if (bigDecimal.compareTo(bigDecimal2.subtract(new BigDecimal(d).multiply(new BigDecimal(2)))) <= 0 || bigDecimal.compareTo(bigDecimal2.add(new BigDecimal(d).multiply(new BigDecimal(2)))) >= 0) {
                    r2 = false;
                }
                return Boolean.valueOf(r2);
            }
        }
        return null;
    }

    public abstract int a();

    public abstract boolean b();

    public abstract boolean c();
}
