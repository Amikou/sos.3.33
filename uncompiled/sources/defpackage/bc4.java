package defpackage;

import android.os.IBinder;
import defpackage.vm1;

/* compiled from: TrustedWebActivityCallbackRemote.java */
/* renamed from: bc4  reason: default package */
/* loaded from: classes.dex */
public class bc4 {
    public final vm1 a;

    public bc4(vm1 vm1Var) {
        this.a = vm1Var;
    }

    public static bc4 a(IBinder iBinder) {
        vm1 b = iBinder == null ? null : vm1.a.b(iBinder);
        if (b == null) {
            return null;
        }
        return new bc4(b);
    }
}
