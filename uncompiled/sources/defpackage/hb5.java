package defpackage;

import android.os.IBinder;
import com.google.android.gms.internal.measurement.c;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: hb5  reason: default package */
/* loaded from: classes.dex */
public final class hb5 extends c implements kb5 {
    public hb5(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IStringProvider");
    }
}
