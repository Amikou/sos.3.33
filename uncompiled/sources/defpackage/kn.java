package defpackage;

import defpackage.at2;
import java.util.Queue;

/* compiled from: BaseKeyPool.java */
/* renamed from: kn  reason: default package */
/* loaded from: classes.dex */
public abstract class kn<T extends at2> {
    public final Queue<T> a = mg4.f(20);

    public abstract T a();

    public T b() {
        T poll = this.a.poll();
        return poll == null ? a() : poll;
    }

    public void c(T t) {
        if (this.a.size() < 20) {
            this.a.offer(t);
        }
    }
}
