package defpackage;

import java.util.LinkedHashSet;

/* compiled from: BoundedLinkedHashSet.java */
/* renamed from: gr  reason: default package */
/* loaded from: classes.dex */
public class gr<E> {
    public int a;
    public LinkedHashSet<E> b;

    public gr(int i) {
        this.b = new LinkedHashSet<>(i);
        this.a = i;
    }

    public synchronized boolean a(E e) {
        if (this.b.size() == this.a) {
            LinkedHashSet<E> linkedHashSet = this.b;
            linkedHashSet.remove(linkedHashSet.iterator().next());
        }
        this.b.remove(e);
        return this.b.add(e);
    }

    public synchronized boolean b(E e) {
        return this.b.contains(e);
    }
}
