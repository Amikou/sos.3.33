package defpackage;

import androidx.media3.common.j;
import defpackage.wi3;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: StreamReader.java */
/* renamed from: fu3  reason: default package */
/* loaded from: classes.dex */
public abstract class fu3 {
    public f84 b;
    public r11 c;
    public zl2 d;
    public long e;
    public long f;
    public long g;
    public int h;
    public int i;
    public long k;
    public boolean l;
    public boolean m;
    public final xl2 a = new xl2();
    public b j = new b();

    /* compiled from: StreamReader.java */
    /* renamed from: fu3$b */
    /* loaded from: classes.dex */
    public static class b {
        public j a;
        public zl2 b;
    }

    /* compiled from: StreamReader.java */
    /* renamed from: fu3$c */
    /* loaded from: classes.dex */
    public static final class c implements zl2 {
        public c() {
        }

        @Override // defpackage.zl2
        public wi3 a() {
            return new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED);
        }

        @Override // defpackage.zl2
        public long b(q11 q11Var) {
            return -1L;
        }

        @Override // defpackage.zl2
        public void c(long j) {
        }
    }

    public final void a() {
        ii.i(this.b);
        androidx.media3.common.util.b.j(this.c);
    }

    public long b(long j) {
        return (j * 1000000) / this.i;
    }

    public long c(long j) {
        return (this.i * j) / 1000000;
    }

    public void d(r11 r11Var, f84 f84Var) {
        this.c = r11Var;
        this.b = f84Var;
        l(true);
    }

    public void e(long j) {
        this.g = j;
    }

    public abstract long f(op2 op2Var);

    public final int g(q11 q11Var, ot2 ot2Var) throws IOException {
        a();
        int i = this.h;
        if (i != 0) {
            if (i == 1) {
                q11Var.k((int) this.f);
                this.h = 2;
                return 0;
            } else if (i == 2) {
                androidx.media3.common.util.b.j(this.d);
                return k(q11Var, ot2Var);
            } else if (i == 3) {
                return -1;
            } else {
                throw new IllegalStateException();
            }
        }
        return j(q11Var);
    }

    public final boolean h(q11 q11Var) throws IOException {
        while (this.a.d(q11Var)) {
            this.k = q11Var.getPosition() - this.f;
            if (!i(this.a.c(), this.f, this.j)) {
                return true;
            }
            this.f = q11Var.getPosition();
        }
        this.h = 3;
        return false;
    }

    public abstract boolean i(op2 op2Var, long j, b bVar) throws IOException;

    public final int j(q11 q11Var) throws IOException {
        if (h(q11Var)) {
            j jVar = this.j.a;
            this.i = jVar.D0;
            if (!this.m) {
                this.b.f(jVar);
                this.m = true;
            }
            zl2 zl2Var = this.j.b;
            if (zl2Var != null) {
                this.d = zl2Var;
            } else if (q11Var.getLength() == -1) {
                this.d = new c();
            } else {
                yl2 b2 = this.a.b();
                this.d = new gk0(this, this.f, q11Var.getLength(), b2.e + b2.f, b2.c, (b2.b & 4) != 0);
            }
            this.h = 2;
            this.a.f();
            return 0;
        }
        return -1;
    }

    public final int k(q11 q11Var, ot2 ot2Var) throws IOException {
        long b2 = this.d.b(q11Var);
        if (b2 >= 0) {
            ot2Var.a = b2;
            return 1;
        }
        if (b2 < -1) {
            e(-(b2 + 2));
        }
        if (!this.l) {
            this.c.p((wi3) ii.i(this.d.a()));
            this.l = true;
        }
        if (this.k <= 0 && !this.a.d(q11Var)) {
            this.h = 3;
            return -1;
        }
        this.k = 0L;
        op2 c2 = this.a.c();
        long f = f(c2);
        if (f >= 0) {
            long j = this.g;
            if (j + f >= this.e) {
                long b3 = b(j);
                this.b.a(c2, c2.f());
                this.b.b(b3, 1, c2.f(), 0, null);
                this.e = -1L;
            }
        }
        this.g += f;
        return 0;
    }

    public void l(boolean z) {
        if (z) {
            this.j = new b();
            this.f = 0L;
            this.h = 0;
        } else {
            this.h = 1;
        }
        this.e = -1L;
        this.g = 0L;
    }

    public final void m(long j, long j2) {
        this.a.e();
        if (j == 0) {
            l(!this.l);
        } else if (this.h != 0) {
            this.e = c(j2);
            ((zl2) androidx.media3.common.util.b.j(this.d)).c(this.e);
            this.h = 2;
        }
    }
}
