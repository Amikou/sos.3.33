package defpackage;

import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.MutablePropertyReference0;

/* compiled from: ReflectionFactory.java */
/* renamed from: f53  reason: default package */
/* loaded from: classes2.dex */
public class f53 {
    public sw1 a(FunctionReference functionReference) {
        return functionReference;
    }

    public qw1 b(Class cls) {
        return new bz(cls);
    }

    public rw1 c(Class cls, String str) {
        return new uo2(cls, str);
    }

    public xw1 d(MutablePropertyReference0 mutablePropertyReference0) {
        return mutablePropertyReference0;
    }

    public String e(vd1 vd1Var) {
        String obj = vd1Var.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }

    public String f(Lambda lambda) {
        return e(lambda);
    }
}
