package defpackage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/* compiled from: Notification.java */
@JsonIgnoreProperties(ignoreUnknown = true)
/* renamed from: zg2  reason: default package */
/* loaded from: classes3.dex */
public class zg2<T> {
    private String jsonrpc;
    private String method;
    private ri2<T> params;

    public String getJsonrpc() {
        return this.jsonrpc;
    }

    public String getMethod() {
        return this.method;
    }

    public ri2<T> getParams() {
        return this.params;
    }
}
