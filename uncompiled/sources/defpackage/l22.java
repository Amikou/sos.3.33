package defpackage;

/* compiled from: Longs.java */
/* renamed from: l22  reason: default package */
/* loaded from: classes2.dex */
public final class l22 {
    public static int a(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static long b(long... jArr) {
        au2.d(jArr.length > 0);
        long j = jArr[0];
        for (int i = 1; i < jArr.length; i++) {
            if (jArr[i] > j) {
                j = jArr[i];
            }
        }
        return j;
    }
}
