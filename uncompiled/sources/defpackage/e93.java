package defpackage;

import java.util.Arrays;
import java.util.List;

/* compiled from: RlpList.java */
/* renamed from: e93  reason: default package */
/* loaded from: classes3.dex */
public class e93 implements g93 {
    private final List<g93> values;

    public e93(g93... g93VarArr) {
        this.values = Arrays.asList(g93VarArr);
    }

    public List<g93> getValues() {
        return this.values;
    }

    public e93(List<g93> list) {
        this.values = list;
    }
}
