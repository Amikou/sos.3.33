package defpackage;

/* compiled from: FillAnimationValue.java */
/* renamed from: f41  reason: default package */
/* loaded from: classes2.dex */
public class f41 extends u20 {
    public int c;
    public int d;
    public int e;
    public int f;

    public int e() {
        return this.c;
    }

    public int f() {
        return this.d;
    }

    public int g() {
        return this.e;
    }

    public int h() {
        return this.f;
    }

    public void i(int i) {
        this.c = i;
    }

    public void j(int i) {
        this.d = i;
    }

    public void k(int i) {
        this.e = i;
    }

    public void l(int i) {
        this.f = i;
    }
}
