package defpackage;

import com.google.android.gms.internal.vision.zzht;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: oy5  reason: default package */
/* loaded from: classes.dex */
public final class oy5 implements my5 {
    public final /* synthetic */ zzht a;

    public oy5(zzht zzhtVar) {
        this.a = zzhtVar;
    }

    @Override // defpackage.my5
    public final byte d(int i) {
        return this.a.zza(i);
    }

    @Override // defpackage.my5
    public final int zza() {
        return this.a.zza();
    }
}
