package defpackage;

import android.webkit.MimeTypeMap;
import com.facebook.common.internal.ImmutableMap;
import java.util.Map;

/* compiled from: MimeTypeMapWrapper.java */
/* renamed from: x82  reason: default package */
/* loaded from: classes.dex */
public class x82 {
    public static final MimeTypeMap a = MimeTypeMap.getSingleton();
    public static final Map<String, String> b;

    static {
        ImmutableMap.of("image/heif", "heif", "image/heic", "heic");
        b = ImmutableMap.of("heif", "image/heif", "heic", "image/heic");
    }

    public static String a(String str) {
        String str2 = b.get(str);
        return str2 != null ? str2 : a.getMimeTypeFromExtension(str);
    }
}
