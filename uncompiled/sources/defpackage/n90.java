package defpackage;

import java.io.File;
import java.io.IOException;

/* compiled from: CrashlyticsFileMarker.java */
/* renamed from: n90  reason: default package */
/* loaded from: classes2.dex */
public class n90 {
    public final String a;
    public final s31 b;

    public n90(String str, s31 s31Var) {
        this.a = str;
        this.b = s31Var;
    }

    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            w12 f = w12.f();
            f.e("Error creating marker: " + this.a, e);
            return false;
        }
    }

    public final File b() {
        return new File(this.b.b(), this.a);
    }

    public boolean c() {
        return b().exists();
    }

    public boolean d() {
        return b().delete();
    }
}
