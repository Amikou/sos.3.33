package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: t06  reason: default package */
/* loaded from: classes.dex */
public final class t06 implements yp5<u06> {
    public static final t06 f0 = new t06();
    public final yp5<u06> a = gq5.a(gq5.b(new v06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final u06 zza() {
        return this.a.zza();
    }
}
