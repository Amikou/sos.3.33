package defpackage;

import bolts.b;

/* compiled from: TaskCompletionSource.java */
/* renamed from: o34  reason: default package */
/* loaded from: classes.dex */
public class o34<TResult> {
    public final b<TResult> a = new b<>();

    public b<TResult> a() {
        return this.a;
    }

    public void b() {
        if (!e()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    public void c(Exception exc) {
        if (!f(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }

    public void d(TResult tresult) {
        if (!g(tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    public boolean e() {
        return this.a.p();
    }

    public boolean f(Exception exc) {
        return this.a.q(exc);
    }

    public boolean g(TResult tresult) {
        return this.a.r(tresult);
    }
}
