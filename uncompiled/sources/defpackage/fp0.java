package defpackage;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: DiskLruCache.java */
/* renamed from: fp0  reason: default package */
/* loaded from: classes.dex */
public final class fp0 implements Closeable {
    public final File a;
    public final File f0;
    public final File g0;
    public final File h0;
    public final int i0;
    public long j0;
    public final int k0;
    public Writer m0;
    public int o0;
    public long l0 = 0;
    public final LinkedHashMap<String, d> n0 = new LinkedHashMap<>(0, 0.75f, true);
    public long p0 = 0;
    public final ThreadPoolExecutor q0 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new b(null));
    public final Callable<Void> r0 = new a();

    /* compiled from: DiskLruCache.java */
    /* renamed from: fp0$a */
    /* loaded from: classes.dex */
    public class a implements Callable<Void> {
        public a() {
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            synchronized (fp0.this) {
                if (fp0.this.m0 == null) {
                    return null;
                }
                fp0.this.Q();
                if (fp0.this.w()) {
                    fp0.this.F();
                    fp0.this.o0 = 0;
                }
                return null;
            }
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: fp0$b */
    /* loaded from: classes.dex */
    public static final class b implements ThreadFactory {
        public b() {
        }

        @Override // java.util.concurrent.ThreadFactory
        public synchronized Thread newThread(Runnable runnable) {
            Thread thread;
            thread = new Thread(runnable, "glide-disk-lru-cache-thread");
            thread.setPriority(1);
            return thread;
        }

        public /* synthetic */ b(a aVar) {
            this();
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: fp0$c */
    /* loaded from: classes.dex */
    public final class c {
        public final d a;
        public final boolean[] b;
        public boolean c;

        public /* synthetic */ c(fp0 fp0Var, d dVar, a aVar) {
            this(dVar);
        }

        public void a() throws IOException {
            fp0.this.l(this, false);
        }

        public void b() {
            if (this.c) {
                return;
            }
            try {
                a();
            } catch (IOException unused) {
            }
        }

        public void e() throws IOException {
            fp0.this.l(this, true);
            this.c = true;
        }

        public File f(int i) throws IOException {
            File k;
            synchronized (fp0.this) {
                if (this.a.f == this) {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    k = this.a.k(i);
                    fp0.this.a.mkdirs();
                } else {
                    throw new IllegalStateException();
                }
            }
            return k;
        }

        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.e ? null : new boolean[fp0.this.k0];
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: fp0$d */
    /* loaded from: classes.dex */
    public final class d {
        public final String a;
        public final long[] b;
        public File[] c;
        public File[] d;
        public boolean e;
        public c f;
        public long g;

        public /* synthetic */ d(fp0 fp0Var, String str, a aVar) {
            this(str);
        }

        public File j(int i) {
            return this.c[i];
        }

        public File k(int i) {
            return this.d[i];
        }

        public String l() throws IOException {
            long[] jArr;
            StringBuilder sb = new StringBuilder();
            for (long j : this.b) {
                sb.append(' ');
                sb.append(j);
            }
            return sb.toString();
        }

        public final IOException m(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public final void n(String[] strArr) throws IOException {
            if (strArr.length == fp0.this.k0) {
                for (int i = 0; i < strArr.length; i++) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                    } catch (NumberFormatException unused) {
                        throw m(strArr);
                    }
                }
                return;
            }
            throw m(strArr);
        }

        public d(String str) {
            this.a = str;
            this.b = new long[fp0.this.k0];
            this.c = new File[fp0.this.k0];
            this.d = new File[fp0.this.k0];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < fp0.this.k0; i++) {
                sb.append(i);
                this.c[i] = new File(fp0.this.a, sb.toString());
                sb.append(".tmp");
                this.d[i] = new File(fp0.this.a, sb.toString());
                sb.setLength(length);
            }
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: fp0$e */
    /* loaded from: classes.dex */
    public final class e {
        public final File[] a;

        public /* synthetic */ e(fp0 fp0Var, String str, long j, File[] fileArr, long[] jArr, a aVar) {
            this(fp0Var, str, j, fileArr, jArr);
        }

        public File a(int i) {
            return this.a[i];
        }

        public e(fp0 fp0Var, String str, long j, File[] fileArr, long[] jArr) {
            this.a = fileArr;
        }
    }

    public fp0(File file, int i, int i2, long j) {
        this.a = file;
        this.i0 = i;
        this.f0 = new File(file, "journal");
        this.g0 = new File(file, "journal.tmp");
        this.h0 = new File(file, "journal.bkp");
        this.k0 = i2;
        this.j0 = j;
    }

    public static void N(File file, File file2, boolean z) throws IOException {
        if (z) {
            n(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    @TargetApi(26)
    public static void j(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.close();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.close();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public static void n(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    @TargetApi(26)
    public static void u(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.flush();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.flush();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public static fp0 x(File file, int i, int i2, long j) throws IOException {
        if (j > 0) {
            if (i2 > 0) {
                File file2 = new File(file, "journal.bkp");
                if (file2.exists()) {
                    File file3 = new File(file, "journal");
                    if (file3.exists()) {
                        file2.delete();
                    } else {
                        N(file2, file3, false);
                    }
                }
                fp0 fp0Var = new fp0(file, i, i2, j);
                if (fp0Var.f0.exists()) {
                    try {
                        fp0Var.A();
                        fp0Var.z();
                        return fp0Var;
                    } catch (IOException e2) {
                        PrintStream printStream = System.out;
                        printStream.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                        fp0Var.m();
                    }
                }
                file.mkdirs();
                fp0 fp0Var2 = new fp0(file, i, i2, j);
                fp0Var2.F();
                return fp0Var2;
            }
            throw new IllegalArgumentException("valueCount <= 0");
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    public final void A() throws IOException {
        ku3 ku3Var = new ku3(new FileInputStream(this.f0), og4.a);
        try {
            String d2 = ku3Var.d();
            String d3 = ku3Var.d();
            String d4 = ku3Var.d();
            String d5 = ku3Var.d();
            String d6 = ku3Var.d();
            if (!"libcore.io.DiskLruCache".equals(d2) || !"1".equals(d3) || !Integer.toString(this.i0).equals(d4) || !Integer.toString(this.k0).equals(d5) || !"".equals(d6)) {
                throw new IOException("unexpected journal header: [" + d2 + ", " + d3 + ", " + d5 + ", " + d6 + "]");
            }
            int i = 0;
            while (true) {
                try {
                    C(ku3Var.d());
                    i++;
                } catch (EOFException unused) {
                    this.o0 = i - this.n0.size();
                    if (ku3Var.c()) {
                        F();
                    } else {
                        this.m0 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f0, true), og4.a));
                    }
                    og4.a(ku3Var);
                    return;
                }
            }
        } catch (Throwable th) {
            og4.a(ku3Var);
            throw th;
        }
    }

    public final void C(String str) throws IOException {
        String substring;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(32, i);
            if (indexOf2 == -1) {
                substring = str.substring(i);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.n0.remove(substring);
                    return;
                }
            } else {
                substring = str.substring(i, indexOf2);
            }
            d dVar = this.n0.get(substring);
            if (dVar == null) {
                dVar = new d(this, substring, null);
                this.n0.put(substring, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                dVar.e = true;
                dVar.f = null;
                dVar.n(split);
                return;
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f = new c(this, dVar, null);
                return;
            } else if (indexOf2 == -1 && indexOf == 4 && str.startsWith("READ")) {
                return;
            } else {
                throw new IOException("unexpected journal line: " + str);
            }
        }
        throw new IOException("unexpected journal line: " + str);
    }

    public final synchronized void F() throws IOException {
        Writer writer = this.m0;
        if (writer != null) {
            j(writer);
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.g0), og4.a));
        bufferedWriter.write("libcore.io.DiskLruCache");
        bufferedWriter.write("\n");
        bufferedWriter.write("1");
        bufferedWriter.write("\n");
        bufferedWriter.write(Integer.toString(this.i0));
        bufferedWriter.write("\n");
        bufferedWriter.write(Integer.toString(this.k0));
        bufferedWriter.write("\n");
        bufferedWriter.write("\n");
        for (d dVar : this.n0.values()) {
            if (dVar.f != null) {
                bufferedWriter.write("DIRTY " + dVar.a + '\n');
            } else {
                bufferedWriter.write("CLEAN " + dVar.a + dVar.l() + '\n');
            }
        }
        j(bufferedWriter);
        if (this.f0.exists()) {
            N(this.f0, this.h0, true);
        }
        N(this.g0, this.f0, false);
        this.h0.delete();
        this.m0 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f0, true), og4.a));
    }

    public synchronized boolean M(String str) throws IOException {
        i();
        d dVar = this.n0.get(str);
        if (dVar != null && dVar.f == null) {
            for (int i = 0; i < this.k0; i++) {
                File j = dVar.j(i);
                if (j.exists() && !j.delete()) {
                    throw new IOException("failed to delete " + j);
                }
                this.l0 -= dVar.b[i];
                dVar.b[i] = 0;
            }
            this.o0++;
            this.m0.append((CharSequence) "REMOVE");
            this.m0.append(' ');
            this.m0.append((CharSequence) str);
            this.m0.append('\n');
            this.n0.remove(str);
            if (w()) {
                this.q0.submit(this.r0);
            }
            return true;
        }
        return false;
    }

    public final void Q() throws IOException {
        while (this.l0 > this.j0) {
            M(this.n0.entrySet().iterator().next().getKey());
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        if (this.m0 == null) {
            return;
        }
        Iterator it = new ArrayList(this.n0.values()).iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.f != null) {
                dVar.f.a();
            }
        }
        Q();
        j(this.m0);
        this.m0 = null;
    }

    public final void i() {
        if (this.m0 == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public final synchronized void l(c cVar, boolean z) throws IOException {
        d dVar = cVar.a;
        if (dVar.f == cVar) {
            if (z && !dVar.e) {
                for (int i = 0; i < this.k0; i++) {
                    if (cVar.b[i]) {
                        if (!dVar.k(i).exists()) {
                            cVar.a();
                            return;
                        }
                    } else {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                    }
                }
            }
            for (int i2 = 0; i2 < this.k0; i2++) {
                File k = dVar.k(i2);
                if (z) {
                    if (k.exists()) {
                        File j = dVar.j(i2);
                        k.renameTo(j);
                        long j2 = dVar.b[i2];
                        long length = j.length();
                        dVar.b[i2] = length;
                        this.l0 = (this.l0 - j2) + length;
                    }
                } else {
                    n(k);
                }
            }
            this.o0++;
            dVar.f = null;
            if (dVar.e | z) {
                dVar.e = true;
                this.m0.append((CharSequence) "CLEAN");
                this.m0.append(' ');
                this.m0.append((CharSequence) dVar.a);
                this.m0.append((CharSequence) dVar.l());
                this.m0.append('\n');
                if (z) {
                    long j3 = this.p0;
                    this.p0 = 1 + j3;
                    dVar.g = j3;
                }
            } else {
                this.n0.remove(dVar.a);
                this.m0.append((CharSequence) "REMOVE");
                this.m0.append(' ');
                this.m0.append((CharSequence) dVar.a);
                this.m0.append('\n');
            }
            u(this.m0);
            if (this.l0 > this.j0 || w()) {
                this.q0.submit(this.r0);
            }
            return;
        }
        throw new IllegalStateException();
    }

    public void m() throws IOException {
        close();
        og4.b(this.a);
    }

    public c q(String str) throws IOException {
        return r(str, -1L);
    }

    public final synchronized c r(String str, long j) throws IOException {
        i();
        d dVar = this.n0.get(str);
        if (j == -1 || (dVar != null && dVar.g == j)) {
            if (dVar != null) {
                if (dVar.f != null) {
                    return null;
                }
            } else {
                dVar = new d(this, str, null);
                this.n0.put(str, dVar);
            }
            c cVar = new c(this, dVar, null);
            dVar.f = cVar;
            this.m0.append((CharSequence) "DIRTY");
            this.m0.append(' ');
            this.m0.append((CharSequence) str);
            this.m0.append('\n');
            u(this.m0);
            return cVar;
        }
        return null;
    }

    public synchronized e v(String str) throws IOException {
        i();
        d dVar = this.n0.get(str);
        if (dVar == null) {
            return null;
        }
        if (dVar.e) {
            for (File file : dVar.c) {
                if (!file.exists()) {
                    return null;
                }
            }
            this.o0++;
            this.m0.append((CharSequence) "READ");
            this.m0.append(' ');
            this.m0.append((CharSequence) str);
            this.m0.append('\n');
            if (w()) {
                this.q0.submit(this.r0);
            }
            return new e(this, str, dVar.g, dVar.c, dVar.b, null);
        }
        return null;
    }

    public final boolean w() {
        int i = this.o0;
        return i >= 2000 && i >= this.n0.size();
    }

    public final void z() throws IOException {
        n(this.g0);
        Iterator<d> it = this.n0.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i = 0;
            if (next.f != null) {
                next.f = null;
                while (i < this.k0) {
                    n(next.j(i));
                    n(next.k(i));
                    i++;
                }
                it.remove();
            } else {
                while (i < this.k0) {
                    this.l0 += next.b[i];
                    i++;
                }
            }
        }
    }
}
