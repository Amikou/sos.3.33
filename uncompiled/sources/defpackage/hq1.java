package defpackage;

import android.animation.Animator;

/* compiled from: IndeterminateAnimatorDelegate.java */
/* renamed from: hq1  reason: default package */
/* loaded from: classes2.dex */
public abstract class hq1<T extends Animator> {
    public iq1 a;
    public final float[] b;
    public final int[] c;

    public hq1(int i) {
        this.b = new float[i * 2];
        this.c = new int[i];
    }

    public abstract void a();

    public float b(int i, int i2, int i3) {
        return (i - i2) / i3;
    }

    public abstract void c();

    public abstract void d(hd hdVar);

    public void e(iq1 iq1Var) {
        this.a = iq1Var;
    }

    public abstract void f();

    public abstract void g();

    public abstract void h();
}
