package defpackage;

import com.facebook.common.internal.d;
import com.facebook.common.memory.PooledByteBuffer;
import java.io.IOException;
import java.io.OutputStream;

/* compiled from: PooledByteBufferOutputStream.java */
/* renamed from: ct2  reason: default package */
/* loaded from: classes.dex */
public abstract class ct2 extends OutputStream {
    public abstract PooledByteBuffer a();

    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        try {
            super.close();
        } catch (IOException e) {
            d.a(e);
        }
    }

    public abstract int size();
}
