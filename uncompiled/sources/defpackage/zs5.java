package defpackage;

import android.content.ComponentName;
import android.content.Context;
import com.google.android.gms.measurement.internal.p;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: zs5  reason: default package */
/* loaded from: classes.dex */
public final class zs5 implements Runnable {
    public final /* synthetic */ dt5 a;

    public zs5(dt5 dt5Var) {
        this.a = dt5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        p pVar = this.a.g0;
        Context m = pVar.a.m();
        this.a.g0.a.b();
        p.x(pVar, new ComponentName(m, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
