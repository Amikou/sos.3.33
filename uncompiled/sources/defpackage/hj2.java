package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: hj2  reason: default package */
/* loaded from: classes2.dex */
public interface hj2 {
    public static final i a;

    static {
        new i("1.3.14.3.2.2");
        new i("1.3.14.3.2.3");
        new i("1.3.14.3.2.4");
        new i("1.3.14.3.2.6");
        new i("1.3.14.3.2.7");
        new i("1.3.14.3.2.8");
        new i("1.3.14.3.2.9");
        new i("1.3.14.3.2.17");
        a = new i("1.3.14.3.2.26");
        new i("1.3.14.3.2.27");
        new i("1.3.14.3.2.29");
        new i("1.3.14.7.2.1.1");
    }
}
