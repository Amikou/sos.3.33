package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;

/* compiled from: ThemeUtils.java */
/* renamed from: b54  reason: default package */
/* loaded from: classes.dex */
public class b54 {
    public static final ThreadLocal<TypedValue> a = new ThreadLocal<>();
    public static final int[] b = {-16842910};
    public static final int[] c = {16842908};
    public static final int[] d = {16842919};
    public static final int[] e = {16842912};
    public static final int[] f = new int[0];
    public static final int[] g = new int[1];

    public static void a(View view, Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(d33.AppCompatTheme);
        try {
            if (!obtainStyledAttributes.hasValue(d33.AppCompatTheme_windowActionBar)) {
                StringBuilder sb = new StringBuilder();
                sb.append("View ");
                sb.append(view.getClass());
                sb.append(" is an AppCompat widget that can only be used with a Theme.AppCompat theme (or descendant).");
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public static int b(Context context, int i) {
        ColorStateList e2 = e(context, i);
        if (e2 != null && e2.isStateful()) {
            return e2.getColorForState(b, e2.getDefaultColor());
        }
        TypedValue f2 = f();
        context.getTheme().resolveAttribute(16842803, f2, true);
        return d(context, i, f2.getFloat());
    }

    public static int c(Context context, int i) {
        int[] iArr = g;
        iArr[0] = i;
        l64 u = l64.u(context, null, iArr);
        try {
            return u.b(0, 0);
        } finally {
            u.w();
        }
    }

    public static int d(Context context, int i, float f2) {
        int c2 = c(context, i);
        return z20.j(c2, Math.round(Color.alpha(c2) * f2));
    }

    public static ColorStateList e(Context context, int i) {
        int[] iArr = g;
        iArr[0] = i;
        l64 u = l64.u(context, null, iArr);
        try {
            return u.c(0);
        } finally {
            u.w();
        }
    }

    public static TypedValue f() {
        ThreadLocal<TypedValue> threadLocal = a;
        TypedValue typedValue = threadLocal.get();
        if (typedValue == null) {
            TypedValue typedValue2 = new TypedValue();
            threadLocal.set(typedValue2);
            return typedValue2;
        }
        return typedValue;
    }
}
