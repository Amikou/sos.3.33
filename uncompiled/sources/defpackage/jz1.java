package defpackage;

import android.content.Context;

/* compiled from: LibraryVersionComponent.java */
/* renamed from: jz1  reason: default package */
/* loaded from: classes2.dex */
public class jz1 {

    /* compiled from: LibraryVersionComponent.java */
    /* renamed from: jz1$a */
    /* loaded from: classes2.dex */
    public interface a<T> {
        String extract(T t);
    }

    public static a40<?> b(String str, String str2) {
        return a40.i(hz1.a(str, str2), hz1.class);
    }

    public static a40<?> c(final String str, final a<Context> aVar) {
        return a40.j(hz1.class).b(hm0.j(Context.class)).f(new e40() { // from class: iz1
            @Override // defpackage.e40
            public final Object a(b40 b40Var) {
                hz1 d;
                d = jz1.d(str, aVar, b40Var);
                return d;
            }
        }).d();
    }

    public static /* synthetic */ hz1 d(String str, a aVar, b40 b40Var) {
        return hz1.a(str, aVar.extract((Context) b40Var.a(Context.class)));
    }
}
