package defpackage;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: LruCache.java */
/* renamed from: s22  reason: default package */
/* loaded from: classes.dex */
public class s22<T, Y> {
    public final Map<T, a<Y>> a = new LinkedHashMap(100, 0.75f, true);
    public long b;
    public long c;

    /* compiled from: LruCache.java */
    /* renamed from: s22$a */
    /* loaded from: classes.dex */
    public static final class a<Y> {
        public final Y a;
        public final int b;

        public a(Y y, int i) {
            this.a = y;
            this.b = i;
        }
    }

    public s22(long j) {
        this.b = j;
    }

    public void b() {
        m(0L);
    }

    public final void f() {
        m(this.b);
    }

    public synchronized Y g(T t) {
        a<Y> aVar;
        aVar = this.a.get(t);
        return aVar != null ? aVar.a : null;
    }

    public synchronized long h() {
        return this.b;
    }

    public int i(Y y) {
        return 1;
    }

    public void j(T t, Y y) {
    }

    public synchronized Y k(T t, Y y) {
        int i = i(y);
        long j = i;
        if (j >= this.b) {
            j(t, y);
            return null;
        }
        if (y != null) {
            this.c += j;
        }
        a<Y> put = this.a.put(t, y == null ? null : new a<>(y, i));
        if (put != null) {
            this.c -= put.b;
            if (!put.a.equals(y)) {
                j(t, put.a);
            }
        }
        f();
        return put != null ? put.a : null;
    }

    public synchronized Y l(T t) {
        a<Y> remove = this.a.remove(t);
        if (remove == null) {
            return null;
        }
        this.c -= remove.b;
        return remove.a;
    }

    public synchronized void m(long j) {
        while (this.c > j) {
            Iterator<Map.Entry<T, a<Y>>> it = this.a.entrySet().iterator();
            Map.Entry<T, a<Y>> next = it.next();
            a<Y> value = next.getValue();
            this.c -= value.b;
            T key = next.getKey();
            it.remove();
            j(key, value.a);
        }
    }
}
