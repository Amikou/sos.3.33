package defpackage;

import androidx.work.WorkerParameters;

/* compiled from: StartWorkRunnable.java */
/* renamed from: ts3  reason: default package */
/* loaded from: classes.dex */
public class ts3 implements Runnable {
    public hq4 a;
    public String f0;
    public WorkerParameters.a g0;

    public ts3(hq4 hq4Var, String str, WorkerParameters.a aVar) {
        this.a = hq4Var;
        this.f0 = str;
        this.g0 = aVar;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.a.o().k(this.f0, this.g0);
    }
}
