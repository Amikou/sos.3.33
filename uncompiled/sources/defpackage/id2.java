package defpackage;

/* renamed from: id2  reason: default package */
/* loaded from: classes2.dex */
public abstract class id2 {
    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        ed2.y(iArr, iArr2, iArr3);
        ed2.x(iArr, 8, iArr2, 8, iArr3, 16);
        int e = ed2.e(iArr3, 8, iArr3, 16);
        int c = e + ed2.c(iArr3, 24, iArr3, 16, ed2.c(iArr3, 0, iArr3, 8, 0) + e);
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        boolean z = ed2.l(iArr, 8, iArr, 0, h, 0) != ed2.l(iArr2, 8, iArr2, 0, h2, 0);
        int[] j = ed2.j();
        ed2.y(h, h2, j);
        kd2.f(32, c + (z ? kd2.d(16, j, 0, iArr3, 8) : kd2.M(16, j, 0, iArr3, 8)), iArr3, 24);
    }

    public static void b(int[] iArr, int[] iArr2) {
        ed2.F(iArr, iArr2);
        ed2.E(iArr, 8, iArr2, 16);
        int e = ed2.e(iArr2, 8, iArr2, 16);
        int c = e + ed2.c(iArr2, 24, iArr2, 16, ed2.c(iArr2, 0, iArr2, 8, 0) + e);
        int[] h = ed2.h();
        ed2.l(iArr, 8, iArr, 0, h, 0);
        int[] j = ed2.j();
        ed2.F(h, j);
        kd2.f(32, c + kd2.M(16, j, 0, iArr2, 8), iArr2, 24);
    }
}
