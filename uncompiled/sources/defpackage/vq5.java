package defpackage;

import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzkq;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: vq5  reason: default package */
/* loaded from: classes.dex */
public final class vq5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ boolean f0;
    public final /* synthetic */ zzkq g0;
    public final /* synthetic */ p h0;

    public vq5(p pVar, zzp zzpVar, boolean z, zzkq zzkqVar) {
        this.h0 = pVar;
        this.a = zzpVar;
        this.f0 = z;
        this.g0 = zzkqVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d dVar;
        dVar = this.h0.d;
        if (dVar == null) {
            this.h0.a.w().l().a("Discarding data. Failed to set user property");
            return;
        }
        zt2.j(this.a);
        this.h0.K(dVar, this.f0 ? null : this.g0, this.a);
        this.h0.D();
    }
}
