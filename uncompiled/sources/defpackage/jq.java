package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapPool.java */
/* renamed from: jq  reason: default package */
/* loaded from: classes.dex */
public interface jq {
    void a(int i);

    void b();

    void c(Bitmap bitmap);

    Bitmap d(int i, int i2, Bitmap.Config config);

    Bitmap e(int i, int i2, Bitmap.Config config);
}
