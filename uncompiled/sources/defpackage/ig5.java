package defpackage;

import com.google.android.gms.internal.measurement.s0;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ig5  reason: default package */
/* loaded from: classes.dex */
public final class ig5 extends w1<s0, ig5> implements xx5 {
    public ig5() {
        super(s0.G());
    }

    public final ig5 v(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        s0.H((s0) this.f0, str);
        return this;
    }

    public /* synthetic */ ig5(zf5 zf5Var) {
        super(s0.G());
    }
}
