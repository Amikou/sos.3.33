package defpackage;

import com.google.android.gms.clearcut.zzc;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.clearcut.t0;

/* renamed from: tk5  reason: default package */
/* loaded from: classes.dex */
public class tk5 extends t0 {
    public tk5() {
    }

    public /* synthetic */ tk5(ei5 ei5Var) {
        this();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void A0(Status status, zze[] zzeVarArr) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void G0(Status status, zzc zzcVar) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void Y0(Status status) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void r(Status status, long j) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void s1(Status status) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void u1(Status status, zzc zzcVar) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void v(DataHolder dataHolder) {
        throw new UnsupportedOperationException();
    }

    @Override // com.google.android.gms.internal.clearcut.s0
    public final void y1(Status status, long j) {
        throw new UnsupportedOperationException();
    }
}
