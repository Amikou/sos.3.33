package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.b;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: rh1  reason: default package */
/* loaded from: classes.dex */
public class rh1 {
    public static rh1 c;
    public final Context a;
    public volatile String b;

    public rh1(Context context) {
        this.a = context.getApplicationContext();
    }

    @RecentlyNonNull
    public static rh1 a(@RecentlyNonNull Context context) {
        zt2.j(context);
        synchronized (rh1.class) {
            if (c == null) {
                b.c(context);
                c = new rh1(context);
            }
        }
        return c;
    }

    public static vc5 d(PackageInfo packageInfo, vc5... vc5VarArr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr != null && signatureArr.length == 1) {
            qk5 qk5Var = new qk5(packageInfo.signatures[0].toByteArray());
            for (int i = 0; i < vc5VarArr.length; i++) {
                if (vc5VarArr[i].equals(qk5Var)) {
                    return vc5VarArr[i];
                }
            }
            return null;
        }
        return null;
    }

    @RecentlyNonNull
    public static boolean f(@RecentlyNonNull PackageInfo packageInfo, @RecentlyNonNull boolean z) {
        if (packageInfo != null && packageInfo.signatures != null) {
            if ((z ? d(packageInfo, mp5.a) : d(packageInfo, mp5.a[0])) != null) {
                return true;
            }
        }
        return false;
    }

    @RecentlyNonNull
    public boolean b(@RecentlyNonNull PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (f(packageInfo, false)) {
            return true;
        }
        return f(packageInfo, true) && qh1.f(this.a);
    }

    @RecentlyNonNull
    public boolean c(@RecentlyNonNull int i) {
        dx5 c2;
        String[] packagesForUid = this.a.getPackageManager().getPackagesForUid(i);
        if (packagesForUid != null && packagesForUid.length != 0) {
            c2 = null;
            int length = packagesForUid.length;
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    c2 = e(packagesForUid[i2], false, false);
                    if (c2.a) {
                        break;
                    }
                    i2++;
                } else {
                    c2 = (dx5) zt2.j(c2);
                    break;
                }
            }
        } else {
            c2 = dx5.c("no pkgs");
        }
        c2.g();
        return c2.a;
    }

    @SuppressLint({"PackageManagerGetSignatures"})
    public final dx5 e(String str, boolean z, boolean z2) {
        dx5 c2;
        ApplicationInfo applicationInfo;
        if (str == null) {
            return dx5.c("null pkg");
        }
        if (str.equals(this.b)) {
            return dx5.b();
        }
        try {
            PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo(str, 64);
            boolean f = qh1.f(this.a);
            if (packageInfo == null) {
                c2 = dx5.c("null pkg");
            } else {
                Signature[] signatureArr = packageInfo.signatures;
                if (signatureArr != null && signatureArr.length == 1) {
                    qk5 qk5Var = new qk5(packageInfo.signatures[0].toByteArray());
                    String str2 = packageInfo.packageName;
                    dx5 b = b.b(str2, qk5Var, f, false);
                    c2 = (!b.a || (applicationInfo = packageInfo.applicationInfo) == null || (applicationInfo.flags & 2) == 0 || !b.b(str2, qk5Var, false, true).a) ? b : dx5.c("debuggable release cert app rejected");
                } else {
                    c2 = dx5.c("single cert required");
                }
            }
            if (c2.a) {
                this.b = str;
            }
            return c2;
        } catch (PackageManager.NameNotFoundException e) {
            return dx5.d(str.length() != 0 ? "no pkg ".concat(str) : new String("no pkg "), e);
        }
    }
}
