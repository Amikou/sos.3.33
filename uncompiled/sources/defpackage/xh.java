package defpackage;

import java.lang.reflect.Array;
import java.util.Objects;

/* compiled from: ArraysJVM.kt */
/* renamed from: xh  reason: default package */
/* loaded from: classes2.dex */
public class xh {
    public static final <T> T[] a(T[] tArr, int i) {
        fs1.f(tArr, "reference");
        Object newInstance = Array.newInstance(tArr.getClass().getComponentType(), i);
        Objects.requireNonNull(newInstance, "null cannot be cast to non-null type kotlin.Array<T>");
        return (T[]) ((Object[]) newInstance);
    }

    public static final void b(int i, int i2) {
        if (i <= i2) {
            return;
        }
        throw new IndexOutOfBoundsException("toIndex (" + i + ") is greater than size (" + i2 + ").");
    }
}
