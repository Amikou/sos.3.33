package defpackage;

import androidx.media3.common.util.b;
import defpackage.xv2;
import java.util.ArrayList;
import java.util.zip.Inflater;

/* compiled from: ProjectionDecoder.java */
/* renamed from: yv2  reason: default package */
/* loaded from: classes.dex */
public final class yv2 {
    public static xv2 a(byte[] bArr, int i) {
        ArrayList<xv2.a> arrayList;
        op2 op2Var = new op2(bArr);
        try {
            arrayList = c(op2Var) ? f(op2Var) : e(op2Var);
        } catch (ArrayIndexOutOfBoundsException unused) {
            arrayList = null;
        }
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        if (size != 1) {
            if (size != 2) {
                return null;
            }
            return new xv2(arrayList.get(0), arrayList.get(1), i);
        }
        return new xv2(arrayList.get(0), i);
    }

    public static int b(int i) {
        return (-(i & 1)) ^ (i >> 1);
    }

    public static boolean c(op2 op2Var) {
        op2Var.Q(4);
        int n = op2Var.n();
        op2Var.P(0);
        return n == 1886547818;
    }

    public static xv2.a d(op2 op2Var) {
        int n = op2Var.n();
        if (n > 10000) {
            return null;
        }
        float[] fArr = new float[n];
        for (int i = 0; i < n; i++) {
            fArr[i] = op2Var.m();
        }
        int n2 = op2Var.n();
        if (n2 > 32000) {
            return null;
        }
        double d = 2.0d;
        double log = Math.log(2.0d);
        int ceil = (int) Math.ceil(Math.log(n * 2.0d) / log);
        np2 np2Var = new np2(op2Var.d());
        int i2 = 8;
        np2Var.p(op2Var.e() * 8);
        float[] fArr2 = new float[n2 * 5];
        int i3 = 5;
        int[] iArr = new int[5];
        int i4 = 0;
        int i5 = 0;
        while (i4 < n2) {
            int i6 = 0;
            while (i6 < i3) {
                int b = iArr[i6] + b(np2Var.h(ceil));
                if (b >= n || b < 0) {
                    return null;
                }
                fArr2[i5] = fArr[b];
                iArr[i6] = b;
                i6++;
                i5++;
                i3 = 5;
            }
            i4++;
            i3 = 5;
        }
        np2Var.p((np2Var.e() + 7) & (-8));
        int i7 = 32;
        int h = np2Var.h(32);
        xv2.b[] bVarArr = new xv2.b[h];
        int i8 = 0;
        while (i8 < h) {
            int h2 = np2Var.h(i2);
            int h3 = np2Var.h(i2);
            int h4 = np2Var.h(i7);
            if (h4 > 128000) {
                return null;
            }
            int ceil2 = (int) Math.ceil(Math.log(n2 * d) / log);
            float[] fArr3 = new float[h4 * 3];
            float[] fArr4 = new float[h4 * 2];
            int i9 = 0;
            for (int i10 = 0; i10 < h4; i10++) {
                i9 += b(np2Var.h(ceil2));
                if (i9 < 0 || i9 >= n2) {
                    return null;
                }
                int i11 = i10 * 3;
                int i12 = i9 * 5;
                fArr3[i11] = fArr2[i12];
                fArr3[i11 + 1] = fArr2[i12 + 1];
                fArr3[i11 + 2] = fArr2[i12 + 2];
                int i13 = i10 * 2;
                fArr4[i13] = fArr2[i12 + 3];
                fArr4[i13 + 1] = fArr2[i12 + 4];
            }
            bVarArr[i8] = new xv2.b(h2, fArr3, fArr4, h3);
            i8++;
            i7 = 32;
            d = 2.0d;
            i2 = 8;
        }
        return new xv2.a(bVarArr);
    }

    public static ArrayList<xv2.a> e(op2 op2Var) {
        if (op2Var.D() != 0) {
            return null;
        }
        op2Var.Q(7);
        int n = op2Var.n();
        if (n == 1684433976) {
            op2 op2Var2 = new op2();
            Inflater inflater = new Inflater(true);
            try {
                if (!b.n0(op2Var, op2Var2, inflater)) {
                    return null;
                }
                inflater.end();
                op2Var = op2Var2;
            } finally {
                inflater.end();
            }
        } else if (n != 1918990112) {
            return null;
        }
        return g(op2Var);
    }

    public static ArrayList<xv2.a> f(op2 op2Var) {
        int n;
        op2Var.Q(8);
        int e = op2Var.e();
        int f = op2Var.f();
        while (e < f && (n = op2Var.n() + e) > e && n <= f) {
            int n2 = op2Var.n();
            if (n2 != 2037673328 && n2 != 1836279920) {
                op2Var.P(n);
                e = n;
            } else {
                op2Var.O(n);
                return e(op2Var);
            }
        }
        return null;
    }

    public static ArrayList<xv2.a> g(op2 op2Var) {
        ArrayList<xv2.a> arrayList = new ArrayList<>();
        int e = op2Var.e();
        int f = op2Var.f();
        while (e < f) {
            int n = op2Var.n() + e;
            if (n <= e || n > f) {
                return null;
            }
            if (op2Var.n() == 1835365224) {
                xv2.a d = d(op2Var);
                if (d == null) {
                    return null;
                }
                arrayList.add(d);
            }
            op2Var.P(n);
            e = n;
        }
        return arrayList;
    }
}
