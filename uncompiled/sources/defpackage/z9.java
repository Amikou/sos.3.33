package defpackage;

import androidx.media3.common.ParserException;
import defpackage.gc4;
import defpackage.wi3;
import java.io.EOFException;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: AdtsExtractor.java */
/* renamed from: z9  reason: default package */
/* loaded from: classes.dex */
public final class z9 implements p11 {
    public final int a;
    public final aa b;
    public final op2 c;
    public final op2 d;
    public final np2 e;
    public r11 f;
    public long g;
    public long h;
    public int i;
    public boolean j;
    public boolean k;
    public boolean l;

    static {
        y9 y9Var = y9.b;
    }

    public z9() {
        this(0);
    }

    public static int e(int i, long j) {
        return (int) (((i * 8) * 1000000) / j);
    }

    public static /* synthetic */ p11[] i() {
        return new p11[]{new z9()};
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.k = false;
        this.b.c();
        this.g = j2;
    }

    public final void d(q11 q11Var) throws IOException {
        int h;
        if (this.j) {
            return;
        }
        this.i = -1;
        q11Var.j();
        long j = 0;
        if (q11Var.getPosition() == 0) {
            l(q11Var);
        }
        int i = 0;
        int i2 = 0;
        do {
            try {
                if (!q11Var.d(this.d.d(), 0, 2, true)) {
                    break;
                }
                this.d.P(0);
                if (!aa.m(this.d.J())) {
                    break;
                } else if (!q11Var.d(this.d.d(), 0, 4, true)) {
                    break;
                } else {
                    this.e.p(14);
                    h = this.e.h(13);
                    if (h <= 6) {
                        this.j = true;
                        throw ParserException.createForMalformedContainer("Malformed ADTS stream", null);
                    }
                    j += h;
                    i2++;
                    if (i2 == 1000) {
                        break;
                    }
                }
            } catch (EOFException unused) {
            }
        } while (q11Var.l(h - 6, true));
        i = i2;
        q11Var.j();
        if (i > 0) {
            this.i = (int) (j / i);
        } else {
            this.i = -1;
        }
        this.j = true;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        ii.i(this.f);
        long length = q11Var.getLength();
        int i = this.a;
        if (((i & 2) == 0 && ((i & 1) == 0 || length == -1)) ? false : true) {
            d(q11Var);
        }
        int read = q11Var.read(this.c.d(), 0, 2048);
        boolean z = read == -1;
        k(length, z);
        if (z) {
            return -1;
        }
        this.c.P(0);
        this.c.O(read);
        if (!this.k) {
            this.b.e(this.g, 4);
            this.k = true;
        }
        this.b.a(this.c);
        return 0;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        int l = l(q11Var);
        int i = l;
        int i2 = 0;
        int i3 = 0;
        do {
            q11Var.n(this.d.d(), 0, 2);
            this.d.P(0);
            if (aa.m(this.d.J())) {
                i2++;
                if (i2 >= 4 && i3 > 188) {
                    return true;
                }
                q11Var.n(this.d.d(), 0, 4);
                this.e.p(14);
                int h = this.e.h(13);
                if (h <= 6) {
                    i++;
                    q11Var.j();
                    q11Var.f(i);
                } else {
                    q11Var.f(h - 6);
                    i3 += h;
                }
            } else {
                i++;
                q11Var.j();
                q11Var.f(i);
            }
            i2 = 0;
            i3 = 0;
        } while (i - l < 8192);
        return false;
    }

    public final wi3 h(long j, boolean z) {
        return new z50(j, this.h, e(this.i, this.b.k()), this.i, z);
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.f = r11Var;
        this.b.f(r11Var, new gc4.d(0, 1));
        r11Var.m();
    }

    public final void k(long j, boolean z) {
        if (this.l) {
            return;
        }
        boolean z2 = (this.a & 1) != 0 && this.i > 0;
        if (z2 && this.b.k() == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED && !z) {
            return;
        }
        if (z2 && this.b.k() != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.f.p(h(j, (this.a & 2) != 0));
        } else {
            this.f.p(new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
        }
        this.l = true;
    }

    public final int l(q11 q11Var) throws IOException {
        int i = 0;
        while (true) {
            q11Var.n(this.d.d(), 0, 10);
            this.d.P(0);
            if (this.d.G() != 4801587) {
                break;
            }
            this.d.Q(3);
            int C = this.d.C();
            i += C + 10;
            q11Var.f(C);
        }
        q11Var.j();
        q11Var.f(i);
        if (this.h == -1) {
            this.h = i;
        }
        return i;
    }

    public z9(int i) {
        this.a = (i & 2) != 0 ? i | 1 : i;
        this.b = new aa(true);
        this.c = new op2(2048);
        this.i = -1;
        this.h = -1L;
        op2 op2Var = new op2(10);
        this.d = op2Var;
        this.e = new np2(op2Var.d());
    }
}
