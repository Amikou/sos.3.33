package defpackage;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: DispatchedContinuation.kt */
/* renamed from: np0  reason: default package */
/* loaded from: classes2.dex */
public final class np0<T> extends qp0<T> implements e90, q70<T> {
    public static final /* synthetic */ AtomicReferenceFieldUpdater l0 = AtomicReferenceFieldUpdater.newUpdater(np0.class, Object.class, "_reusableCancellableContinuation");
    private volatile /* synthetic */ Object _reusableCancellableContinuation;
    public final CoroutineDispatcher h0;
    public final q70<T> i0;
    public Object j0;
    public final Object k0;

    /* JADX WARN: Multi-variable type inference failed */
    public np0(CoroutineDispatcher coroutineDispatcher, q70<? super T> q70Var) {
        super(-1);
        this.h0 = coroutineDispatcher;
        this.i0 = q70Var;
        this.j0 = op0.a();
        this.k0 = ThreadContextKt.b(getContext());
        this._reusableCancellableContinuation = null;
    }

    @Override // defpackage.qp0
    public void a(Object obj, Throwable th) {
        if (obj instanceof u30) {
            ((u30) obj).b.invoke(th);
        }
    }

    @Override // defpackage.qp0
    public q70<T> b() {
        return this;
    }

    @Override // defpackage.e90
    public e90 getCallerFrame() {
        q70<T> q70Var = this.i0;
        if (q70Var instanceof e90) {
            return (e90) q70Var;
        }
        return null;
    }

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        return this.i0.getContext();
    }

    @Override // defpackage.e90
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @Override // defpackage.qp0
    public Object h() {
        Object obj = this.j0;
        if (ze0.a()) {
            if (!(obj != op0.a())) {
                throw new AssertionError();
            }
        }
        this.j0 = op0.a();
        return obj;
    }

    public final void l() {
        do {
        } while (this._reusableCancellableContinuation == op0.b);
    }

    public final pv<T> m() {
        while (true) {
            Object obj = this._reusableCancellableContinuation;
            if (obj == null) {
                this._reusableCancellableContinuation = op0.b;
                return null;
            } else if (obj instanceof pv) {
                if (l0.compareAndSet(this, obj, op0.b)) {
                    return (pv) obj;
                }
            } else if (obj != op0.b && !(obj instanceof Throwable)) {
                throw new IllegalStateException(fs1.l("Inconsistent state ", obj).toString());
            }
        }
    }

    public final void n(CoroutineContext coroutineContext, T t) {
        this.j0 = t;
        this.g0 = 1;
        this.h0.i(coroutineContext, this);
    }

    public final pv<?> p() {
        Object obj = this._reusableCancellableContinuation;
        if (obj instanceof pv) {
            return (pv) obj;
        }
        return null;
    }

    public final boolean q(pv<?> pvVar) {
        Object obj = this._reusableCancellableContinuation;
        if (obj == null) {
            return false;
        }
        return !(obj instanceof pv) || obj == pvVar;
    }

    public final boolean r(Throwable th) {
        while (true) {
            Object obj = this._reusableCancellableContinuation;
            k24 k24Var = op0.b;
            if (fs1.b(obj, k24Var)) {
                if (l0.compareAndSet(this, k24Var, th)) {
                    return true;
                }
            } else if (obj instanceof Throwable) {
                return true;
            } else {
                if (l0.compareAndSet(this, obj, null)) {
                    return false;
                }
            }
        }
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
        CoroutineContext context = this.i0.getContext();
        Object d = w30.d(obj, null, 1, null);
        if (this.h0.j(context)) {
            this.j0 = d;
            this.g0 = 0;
            this.h0.h(context, this);
            return;
        }
        ze0.a();
        xx0 b = j54.a.b();
        if (b.W()) {
            this.j0 = d;
            this.g0 = 0;
            b.N(this);
            return;
        }
        b.R(true);
        try {
            CoroutineContext context2 = getContext();
            Object c = ThreadContextKt.c(context2, this.k0);
            this.i0.resumeWith(obj);
            te4 te4Var = te4.a;
            ThreadContextKt.a(context2, c);
            do {
            } while (b.b0());
        } finally {
            try {
            } finally {
            }
        }
    }

    public final void t() {
        l();
        pv<?> p = p();
        if (p == null) {
            return;
        }
        p.t();
    }

    public String toString() {
        return "DispatchedContinuation[" + this.h0 + ", " + ff0.c(this.i0) + ']';
    }

    public final Throwable u(ov<?> ovVar) {
        k24 k24Var;
        do {
            Object obj = this._reusableCancellableContinuation;
            k24Var = op0.b;
            if (obj != k24Var) {
                if (obj instanceof Throwable) {
                    if (l0.compareAndSet(this, obj, null)) {
                        return (Throwable) obj;
                    }
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
                throw new IllegalStateException(fs1.l("Inconsistent state ", obj).toString());
            }
        } while (!l0.compareAndSet(this, k24Var, ovVar));
        return null;
    }
}
