package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: os1  reason: default package */
/* loaded from: classes2.dex */
public final class os1 extends au1 {
    public final tc1<Throwable, te4> i0;

    /* JADX WARN: Multi-variable type inference failed */
    public os1(tc1<? super Throwable, te4> tc1Var) {
        this.i0 = tc1Var;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        y(th);
        return te4.a;
    }

    @Override // defpackage.v30
    public void y(Throwable th) {
        this.i0.invoke(th);
    }
}
