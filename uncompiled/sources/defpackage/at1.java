package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: ItemLanguageBinding.java */
/* renamed from: at1  reason: default package */
/* loaded from: classes2.dex */
public final class at1 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final TextView c;
    public final TextView d;
    public final View e;

    public at1(ConstraintLayout constraintLayout, MaterialCheckBox materialCheckBox, TextView textView, TextView textView2, View view) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = textView;
        this.d = textView2;
        this.e = view;
    }

    public static at1 a(View view) {
        int i = R.id.cbSelectLanguage;
        MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.cbSelectLanguage);
        if (materialCheckBox != null) {
            i = R.id.tvLanguageName;
            TextView textView = (TextView) ai4.a(view, R.id.tvLanguageName);
            if (textView != null) {
                i = R.id.tvLanguageRegion;
                TextView textView2 = (TextView) ai4.a(view, R.id.tvLanguageRegion);
                if (textView2 != null) {
                    i = R.id.vDivider;
                    View a = ai4.a(view, R.id.vDivider);
                    if (a != null) {
                        return new at1((ConstraintLayout) view, materialCheckBox, textView, textView2, a);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
