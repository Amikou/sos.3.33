package defpackage;

import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: ao3  reason: default package */
/* loaded from: classes2.dex */
public final class ao3 {
    public final SharedPreferences a;
    public final Executor e;
    public final ArrayDeque<String> d = new ArrayDeque<>();
    public boolean f = false;
    public final String b = "topic_operation_queue";
    public final String c = ",";

    public ao3(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        this.a = sharedPreferences;
        this.e = executor;
    }

    public static ao3 c(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        ao3 ao3Var = new ao3(sharedPreferences, "topic_operation_queue", ",", executor);
        ao3Var.d();
        return ao3Var;
    }

    public final boolean b(boolean z) {
        if (!z || this.f) {
            return z;
        }
        i();
        return true;
    }

    public final void d() {
        String[] split;
        synchronized (this.d) {
            this.d.clear();
            String string = this.a.getString(this.b, "");
            if (!TextUtils.isEmpty(string) && string.contains(this.c)) {
                for (String str : string.split(this.c, -1)) {
                    if (!TextUtils.isEmpty(str)) {
                        this.d.add(str);
                    }
                }
            }
        }
    }

    public String e() {
        String peek;
        synchronized (this.d) {
            peek = this.d.peek();
        }
        return peek;
    }

    public boolean f(Object obj) {
        boolean remove;
        synchronized (this.d) {
            remove = this.d.remove(obj);
            b(remove);
        }
        return remove;
    }

    public String g() {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = this.d.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            sb.append(this.c);
        }
        return sb.toString();
    }

    /* renamed from: h */
    public final void a() {
        synchronized (this.d) {
            this.a.edit().putString(this.b, g()).commit();
        }
    }

    public final void i() {
        this.e.execute(new Runnable(this) { // from class: zn3
            public final ao3 a;

            {
                this.a = this;
            }

            @Override // java.lang.Runnable
            public void run() {
                this.a.a();
            }
        });
    }
}
