package defpackage;

import com.google.android.gms.tasks.c;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: yu5  reason: default package */
/* loaded from: classes.dex */
public final class yu5 implements Runnable {
    public final /* synthetic */ c a;
    public final /* synthetic */ ix5 f0;

    public yu5(ix5 ix5Var, c cVar) {
        this.f0 = ix5Var;
        this.a = cVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        nm2 nm2Var;
        nm2 nm2Var2;
        obj = this.f0.b;
        synchronized (obj) {
            nm2Var = this.f0.c;
            if (nm2Var != null) {
                nm2Var2 = this.f0.c;
                nm2Var2.b((Exception) zt2.j(this.a.k()));
            }
        }
    }
}
