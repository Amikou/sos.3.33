package defpackage;

import com.google.protobuf.t;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: zt5  reason: default package */
/* loaded from: classes.dex */
public final class zt5 {
    public static final tt5<?> a = new wt5();
    public static final tt5<?> b;

    static {
        tt5<?> tt5Var;
        try {
            int i = t.b;
            tt5Var = (tt5) t.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            tt5Var = null;
        }
        b = tt5Var;
    }

    public static tt5<?> a() {
        return a;
    }

    public static tt5<?> b() {
        tt5<?> tt5Var = b;
        if (tt5Var != null) {
            return tt5Var;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
