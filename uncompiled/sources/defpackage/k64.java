package defpackage;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintInfo.java */
/* renamed from: k64  reason: default package */
/* loaded from: classes.dex */
public class k64 {
    public ColorStateList a;
    public PorterDuff.Mode b;
    public boolean c;
    public boolean d;

    public void a() {
        this.a = null;
        this.d = false;
        this.b = null;
        this.c = false;
    }
}
