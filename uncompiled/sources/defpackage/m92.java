package defpackage;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: ModelToResourceClassCache.java */
/* renamed from: m92  reason: default package */
/* loaded from: classes.dex */
public class m92 {
    public final AtomicReference<na2> a = new AtomicReference<>();
    public final rh<na2, List<Class<?>>> b = new rh<>();

    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        List<Class<?>> list;
        na2 andSet = this.a.getAndSet(null);
        if (andSet == null) {
            andSet = new na2(cls, cls2, cls3);
        } else {
            andSet.a(cls, cls2, cls3);
        }
        synchronized (this.b) {
            list = this.b.get(andSet);
        }
        this.a.set(andSet);
        return list;
    }

    public void b(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new na2(cls, cls2, cls3), list);
        }
    }
}
