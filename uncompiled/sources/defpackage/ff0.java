package defpackage;

import kotlin.Result;

/* compiled from: DebugStrings.kt */
/* renamed from: ff0  reason: default package */
/* loaded from: classes2.dex */
public final class ff0 {
    public static final String a(Object obj) {
        return obj.getClass().getSimpleName();
    }

    public static final String b(Object obj) {
        return Integer.toHexString(System.identityHashCode(obj));
    }

    public static final String c(q70<?> q70Var) {
        String m52constructorimpl;
        if (q70Var instanceof np0) {
            return q70Var.toString();
        }
        try {
            Result.a aVar = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(q70Var + '@' + b(q70Var));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            m52constructorimpl = Result.m52constructorimpl(o83.a(th));
        }
        if (Result.m55exceptionOrNullimpl(m52constructorimpl) != null) {
            m52constructorimpl = ((Object) q70Var.getClass().getName()) + '@' + b(q70Var);
        }
        return (String) m52constructorimpl;
    }
}
