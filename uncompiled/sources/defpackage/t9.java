package defpackage;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.checkbox.MaterialCheckBox;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.database.room.ApplicationRoomDatabase;

/* compiled from: AddNewTokensAdapter.kt */
/* renamed from: t9  reason: default package */
/* loaded from: classes2.dex */
public final class t9 extends RecyclerView.Adapter<a> {
    public final ArrayList<q9> a;
    public final b b;
    public qs1 c;
    public Resources d;
    public final List<String> e;

    /* compiled from: AddNewTokensAdapter.kt */
    /* renamed from: t9$a */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.a0 {
        public final qs1 a;
        public final ImageView b;
        public final TextView c;
        public final MaterialCheckBox d;
        public final MaterialCardView e;
        public final MaterialButton f;
        public final MaterialCardView g;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(qs1 qs1Var) {
            super(qs1Var.b());
            fs1.f(qs1Var, "binding");
            this.a = qs1Var;
            ImageView imageView = qs1Var.d;
            fs1.e(imageView, "binding.ivTokenIcon");
            this.b = imageView;
            TextView textView = qs1Var.g;
            fs1.e(textView, "binding.tvTokenNameAndSymbol");
            this.c = textView;
            MaterialCheckBox materialCheckBox = qs1Var.c;
            fs1.e(materialCheckBox, "binding.cbAddToken");
            this.d = materialCheckBox;
            MaterialCardView materialCardView = qs1Var.e;
            fs1.e(materialCardView, "binding.rowBG");
            this.e = materialCardView;
            MaterialButton materialButton = qs1Var.b;
            fs1.e(materialButton, "binding.btnDelete");
            this.f = materialButton;
            MaterialCardView materialCardView2 = qs1Var.f;
            fs1.e(materialCardView2, "binding.rowFG");
            this.g = materialCardView2;
        }

        public final MaterialButton a() {
            return this.f;
        }

        public final MaterialCheckBox b() {
            return this.d;
        }

        public final MaterialCardView c() {
            return this.g;
        }

        public final ImageView d() {
            return this.b;
        }

        public final MaterialCardView e() {
            return this.e;
        }

        public final TextView f() {
            return this.c;
        }
    }

    /* compiled from: AddNewTokensAdapter.kt */
    /* renamed from: t9$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a(q9 q9Var, boolean z);
    }

    public t9(ArrayList<q9> arrayList, b bVar) {
        fs1.f(arrayList, "items");
        fs1.f(bVar, "onItemCheckChangedListener");
        this.a = arrayList;
        this.b = bVar;
        ArrayList arrayList2 = new ArrayList();
        for (Pair<TokenType, List<String>> pair : ApplicationRoomDatabase.n.d()) {
            arrayList2.addAll(pair.getSecond());
        }
        te4 te4Var = te4.a;
        this.e = arrayList2;
    }

    public static final void f(q9 q9Var, t9 t9Var, int i, View view) {
        fs1.f(q9Var, "$model");
        fs1.f(t9Var, "this$0");
        boolean z = !q9Var.i();
        t9Var.a.get(i).j(z);
        t9Var.b.a(q9Var, z);
    }

    public static final void g(q9 q9Var, t9 t9Var, int i, a aVar, View view) {
        fs1.f(q9Var, "$model");
        fs1.f(t9Var, "this$0");
        fs1.f(aVar, "$holder");
        boolean z = !q9Var.i();
        t9Var.a.get(i).j(z);
        aVar.b().setChecked(z);
        t9Var.b.a(q9Var, z);
    }

    public final q9 c(int i) {
        q9 q9Var = this.a.get(i);
        fs1.e(q9Var, "items[position]");
        return q9Var;
    }

    public final ArrayList<q9> d() {
        return this.a;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: e */
    public void onBindViewHolder(final a aVar, final int i) {
        fs1.f(aVar, "holder");
        q9 q9Var = this.a.get(i);
        fs1.e(q9Var, "items[position]");
        final q9 q9Var2 = q9Var;
        aVar.b().setChecked(q9Var2.i());
        TextView f = aVar.f();
        Resources resources = this.d;
        if (resources == null) {
            fs1.r("resources");
            resources = null;
        }
        f.setText(resources.getString(R.string.add_new_tokens_item_name, q9Var2.f(), q9Var2.g()));
        e30.Q(aVar.d(), q9Var2.e(), q9Var2.d(), q9Var2.g());
        if (e30.H(q9Var2.g())) {
            e30.P(aVar.d(), kt.c(q9Var2.a(), q9Var2.g(), null, 2, null), q9Var2.g());
        }
        aVar.b().setOnClickListener(new View.OnClickListener() { // from class: r9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                t9.f(q9.this, this, i, view);
            }
        });
        aVar.c().setOnClickListener(new View.OnClickListener() { // from class: s9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                t9.g(q9.this, this, i, aVar, view);
            }
        });
        aVar.b().setVisibility(e30.k0(this.e.contains(q9Var2.h())));
        MaterialCardView e = aVar.e();
        b30 b30Var = b30.a;
        e.setVisibility(e30.l0(!b30Var.s(q9Var2.h())));
        aVar.a().setEnabled(b30Var.s(q9Var2.h()));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: h */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        qs1 a2 = qs1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_add_new_token, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout… parent, false)\n        )");
        this.c = a2;
        qs1 qs1Var = null;
        if (a2 == null) {
            fs1.r("binding");
            a2 = null;
        }
        Resources resources = a2.b().getContext().getResources();
        fs1.e(resources, "binding.root.context.resources");
        this.d = resources;
        qs1 qs1Var2 = this.c;
        if (qs1Var2 == null) {
            fs1.r("binding");
        } else {
            qs1Var = qs1Var2;
        }
        return new a(qs1Var);
    }

    public final void removeItem(int i) {
        this.a.remove(i);
        notifyDataSetChanged();
    }
}
