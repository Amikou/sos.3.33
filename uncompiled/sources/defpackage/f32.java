package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import kotlinx.coroutines.internal.MainDispatcherFactory;

/* compiled from: MainDispatchers.kt */
/* renamed from: f32  reason: default package */
/* loaded from: classes2.dex */
public final class f32 {
    public static final f32 a;
    public static final boolean b;
    public static final e32 c;

    static {
        f32 f32Var = new f32();
        a = f32Var;
        b = a34.e("kotlinx.coroutines.fast.service.loader", true);
        c = f32Var.a();
    }

    public final e32 a() {
        List<MainDispatcherFactory> m;
        Object next;
        try {
            if (b) {
                m = h21.a.c();
            } else {
                m = vl3.m(tl3.c(ServiceLoader.load(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader()).iterator()));
            }
            Iterator<T> it = m.iterator();
            if (it.hasNext()) {
                next = it.next();
                if (it.hasNext()) {
                    int loadPriority = ((MainDispatcherFactory) next).getLoadPriority();
                    do {
                        Object next2 = it.next();
                        int loadPriority2 = ((MainDispatcherFactory) next2).getLoadPriority();
                        if (loadPriority < loadPriority2) {
                            next = next2;
                            loadPriority = loadPriority2;
                        }
                    } while (it.hasNext());
                }
            } else {
                next = null;
            }
            MainDispatcherFactory mainDispatcherFactory = (MainDispatcherFactory) next;
            if (mainDispatcherFactory == null) {
                return g32.b(null, null, 3, null);
            }
            return g32.d(mainDispatcherFactory, m);
        } catch (Throwable th) {
            return g32.b(th, null, 2, null);
        }
    }
}
