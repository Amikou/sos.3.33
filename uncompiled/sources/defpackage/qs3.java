package defpackage;

import defpackage.wi3;

/* compiled from: StartOffsetExtractorOutput.java */
/* renamed from: qs3  reason: default package */
/* loaded from: classes.dex */
public final class qs3 implements r11 {
    public final long a;
    public final r11 f0;

    /* compiled from: StartOffsetExtractorOutput.java */
    /* renamed from: qs3$a */
    /* loaded from: classes.dex */
    public class a implements wi3 {
        public final /* synthetic */ wi3 a;

        public a(wi3 wi3Var) {
            this.a = wi3Var;
        }

        @Override // defpackage.wi3
        public boolean e() {
            return this.a.e();
        }

        @Override // defpackage.wi3
        public wi3.a h(long j) {
            wi3.a h = this.a.h(j);
            yi3 yi3Var = h.a;
            yi3 yi3Var2 = new yi3(yi3Var.a, yi3Var.b + qs3.this.a);
            yi3 yi3Var3 = h.b;
            return new wi3.a(yi3Var2, new yi3(yi3Var3.a, yi3Var3.b + qs3.this.a));
        }

        @Override // defpackage.wi3
        public long i() {
            return this.a.i();
        }
    }

    public qs3(long j, r11 r11Var) {
        this.a = j;
        this.f0 = r11Var;
    }

    @Override // defpackage.r11
    public f84 f(int i, int i2) {
        return this.f0.f(i, i2);
    }

    @Override // defpackage.r11
    public void m() {
        this.f0.m();
    }

    @Override // defpackage.r11
    public void p(wi3 wi3Var) {
        this.f0.p(new a(wi3Var));
    }
}
