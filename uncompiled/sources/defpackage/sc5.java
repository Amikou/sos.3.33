package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.vision.face.internal.client.FaceParcel;
import com.google.android.gms.vision.face.internal.client.LandmarkParcel;
import com.google.android.gms.vision.face.internal.client.zza;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: sc5  reason: default package */
/* loaded from: classes.dex */
public final class sc5 implements Parcelable.Creator<FaceParcel> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ FaceParcel createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        LandmarkParcel[] landmarkParcelArr = null;
        zza[] zzaVarArr = null;
        int i = 0;
        int i2 = 0;
        float f = Float.MAX_VALUE;
        float f2 = Float.MAX_VALUE;
        float f3 = Float.MAX_VALUE;
        float f4 = 0.0f;
        float f5 = 0.0f;
        float f6 = 0.0f;
        float f7 = 0.0f;
        float f8 = 0.0f;
        float f9 = 0.0f;
        float f10 = 0.0f;
        float f11 = -1.0f;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 1:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 2:
                    i2 = SafeParcelReader.E(parcel, C);
                    break;
                case 3:
                    f4 = SafeParcelReader.A(parcel, C);
                    break;
                case 4:
                    f5 = SafeParcelReader.A(parcel, C);
                    break;
                case 5:
                    f6 = SafeParcelReader.A(parcel, C);
                    break;
                case 6:
                    f7 = SafeParcelReader.A(parcel, C);
                    break;
                case 7:
                    f = SafeParcelReader.A(parcel, C);
                    break;
                case 8:
                    f2 = SafeParcelReader.A(parcel, C);
                    break;
                case 9:
                    landmarkParcelArr = (LandmarkParcel[]) SafeParcelReader.s(parcel, C, LandmarkParcel.CREATOR);
                    break;
                case 10:
                    f8 = SafeParcelReader.A(parcel, C);
                    break;
                case 11:
                    f9 = SafeParcelReader.A(parcel, C);
                    break;
                case 12:
                    f10 = SafeParcelReader.A(parcel, C);
                    break;
                case 13:
                    zzaVarArr = (zza[]) SafeParcelReader.s(parcel, C, zza.CREATOR);
                    break;
                case 14:
                    f3 = SafeParcelReader.A(parcel, C);
                    break;
                case 15:
                    f11 = SafeParcelReader.A(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new FaceParcel(i, i2, f4, f5, f6, f7, f, f2, f3, landmarkParcelArr, f8, f9, f10, zzaVarArr, f11);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ FaceParcel[] newArray(int i) {
        return new FaceParcel[i];
    }
}
