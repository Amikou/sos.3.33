package defpackage;

/* renamed from: fy2  reason: default package */
/* loaded from: classes.dex */
public final class fy2 {
    public static final int alpha = 2130968628;
    public static final int fastScrollEnabled = 2130968993;
    public static final int fastScrollHorizontalThumbDrawable = 2130968994;
    public static final int fastScrollHorizontalTrackDrawable = 2130968995;
    public static final int fastScrollVerticalThumbDrawable = 2130968996;
    public static final int fastScrollVerticalTrackDrawable = 2130968997;
    public static final int font = 2130969021;
    public static final int fontProviderAuthority = 2130969023;
    public static final int fontProviderCerts = 2130969024;
    public static final int fontProviderFetchStrategy = 2130969025;
    public static final int fontProviderFetchTimeout = 2130969026;
    public static final int fontProviderPackage = 2130969027;
    public static final int fontProviderQuery = 2130969028;
    public static final int fontStyle = 2130969030;
    public static final int fontVariationSettings = 2130969031;
    public static final int fontWeight = 2130969032;
    public static final int layoutManager = 2130969169;
    public static final int recyclerViewStyle = 2130969474;
    public static final int reverseLayout = 2130969484;
    public static final int spanCount = 2130969583;
    public static final int stackFromEnd = 2130969595;
    public static final int ttcIndex = 2130969779;
}
