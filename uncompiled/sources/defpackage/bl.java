package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread.java */
/* renamed from: bl  reason: default package */
/* loaded from: classes2.dex */
public final class bl extends r90.e.d.a.b.AbstractC0272e {
    public final String a;
    public final int b;
    public final ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> c;

    /* compiled from: AutoValue_CrashlyticsReport_Session_Event_Application_Execution_Thread.java */
    /* renamed from: bl$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.d.a.b.AbstractC0272e.AbstractC0273a {
        public String a;
        public Integer b;
        public ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> c;

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0273a
        public r90.e.d.a.b.AbstractC0272e a() {
            String str = "";
            if (this.a == null) {
                str = " name";
            }
            if (this.b == null) {
                str = str + " importance";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (str.isEmpty()) {
                return new bl(this.a, this.b.intValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0273a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0273a b(ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> ip1Var) {
            Objects.requireNonNull(ip1Var, "Null frames");
            this.c = ip1Var;
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0273a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0273a c(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.d.a.b.AbstractC0272e.AbstractC0273a
        public r90.e.d.a.b.AbstractC0272e.AbstractC0273a d(String str) {
            Objects.requireNonNull(str, "Null name");
            this.a = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e
    public ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> b() {
        return this.c;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e
    public int c() {
        return this.b;
    }

    @Override // defpackage.r90.e.d.a.b.AbstractC0272e
    public String d() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.d.a.b.AbstractC0272e) {
            r90.e.d.a.b.AbstractC0272e abstractC0272e = (r90.e.d.a.b.AbstractC0272e) obj;
            return this.a.equals(abstractC0272e.d()) && this.b == abstractC0272e.c() && this.c.equals(abstractC0272e.b());
        }
        return false;
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c.hashCode();
    }

    public String toString() {
        return "Thread{name=" + this.a + ", importance=" + this.b + ", frames=" + this.c + "}";
    }

    public bl(String str, int i, ip1<r90.e.d.a.b.AbstractC0272e.AbstractC0274b> ip1Var) {
        this.a = str;
        this.b = i;
        this.c = ip1Var;
    }
}
