package defpackage;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: OnBackPressedCallback.java */
/* renamed from: fm2  reason: default package */
/* loaded from: classes.dex */
public abstract class fm2 {
    public boolean a;
    public CopyOnWriteArrayList<nv> b = new CopyOnWriteArrayList<>();

    public fm2(boolean z) {
        this.a = z;
    }

    public void a(nv nvVar) {
        this.b.add(nvVar);
    }

    public abstract void b();

    public final boolean c() {
        return this.a;
    }

    public final void d() {
        Iterator<nv> it = this.b.iterator();
        while (it.hasNext()) {
            it.next().cancel();
        }
    }

    public void e(nv nvVar) {
        this.b.remove(nvVar);
    }

    public final void f(boolean z) {
        this.a = z;
    }
}
