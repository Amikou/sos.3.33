package defpackage;

import androidx.media3.common.util.b;
import defpackage.gc4;

/* compiled from: SectionReader.java */
/* renamed from: xh3  reason: default package */
/* loaded from: classes.dex */
public final class xh3 implements gc4 {
    public final wh3 a;
    public final op2 b = new op2(32);
    public int c;
    public int d;
    public boolean e;
    public boolean f;

    public xh3(wh3 wh3Var) {
        this.a = wh3Var;
    }

    @Override // defpackage.gc4
    public void a(op2 op2Var, int i) {
        boolean z = (i & 1) != 0;
        int e = z ? op2Var.e() + op2Var.D() : -1;
        if (this.f) {
            if (!z) {
                return;
            }
            this.f = false;
            op2Var.P(e);
            this.d = 0;
        }
        while (op2Var.a() > 0) {
            int i2 = this.d;
            if (i2 < 3) {
                if (i2 == 0) {
                    int D = op2Var.D();
                    op2Var.P(op2Var.e() - 1);
                    if (D == 255) {
                        this.f = true;
                        return;
                    }
                }
                int min = Math.min(op2Var.a(), 3 - this.d);
                op2Var.j(this.b.d(), this.d, min);
                int i3 = this.d + min;
                this.d = i3;
                if (i3 == 3) {
                    this.b.P(0);
                    this.b.O(3);
                    this.b.Q(1);
                    int D2 = this.b.D();
                    int D3 = this.b.D();
                    this.e = (D2 & 128) != 0;
                    this.c = (((D2 & 15) << 8) | D3) + 3;
                    int b = this.b.b();
                    int i4 = this.c;
                    if (b < i4) {
                        this.b.c(Math.min(4098, Math.max(i4, this.b.b() * 2)));
                    }
                }
            } else {
                int min2 = Math.min(op2Var.a(), this.c - this.d);
                op2Var.j(this.b.d(), this.d, min2);
                int i5 = this.d + min2;
                this.d = i5;
                int i6 = this.c;
                if (i5 != i6) {
                    continue;
                } else {
                    if (this.e) {
                        if (b.s(this.b.d(), 0, this.c, -1) != 0) {
                            this.f = true;
                            return;
                        }
                        this.b.O(this.c - 4);
                    } else {
                        this.b.O(i6);
                    }
                    this.b.P(0);
                    this.a.a(this.b);
                    this.d = 0;
                }
            }
        }
    }

    @Override // defpackage.gc4
    public void b(h64 h64Var, r11 r11Var, gc4.d dVar) {
        this.a.b(h64Var, r11Var, dVar);
        this.f = true;
    }

    @Override // defpackage.gc4
    public void c() {
        this.f = true;
    }
}
