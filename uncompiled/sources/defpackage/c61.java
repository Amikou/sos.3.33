package defpackage;

import android.graphics.Bitmap;
import com.bumptech.glide.load.resource.bitmap.l;
import java.security.MessageDigest;

/* compiled from: FitCenter.java */
/* renamed from: c61  reason: default package */
/* loaded from: classes.dex */
public class c61 extends qq {
    public static final byte[] b = "com.bumptech.glide.load.resource.bitmap.FitCenter".getBytes(fx1.a);

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        messageDigest.update(b);
    }

    @Override // defpackage.qq
    public Bitmap c(jq jqVar, Bitmap bitmap, int i, int i2) {
        return l.f(jqVar, bitmap, i, i2);
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        return obj instanceof c61;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return 1572326941;
    }
}
