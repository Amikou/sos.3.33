package defpackage;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* compiled from: ByteArrayLoader.java */
/* renamed from: ns  reason: default package */
/* loaded from: classes.dex */
public class ns<Data> implements j92<byte[], Data> {
    public final b<Data> a;

    /* compiled from: ByteArrayLoader.java */
    /* renamed from: ns$a */
    /* loaded from: classes.dex */
    public static class a implements k92<byte[], ByteBuffer> {

        /* compiled from: ByteArrayLoader.java */
        /* renamed from: ns$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0254a implements b<ByteBuffer> {
            public C0254a(a aVar) {
            }

            @Override // defpackage.ns.b
            public Class<ByteBuffer> a() {
                return ByteBuffer.class;
            }

            @Override // defpackage.ns.b
            /* renamed from: c */
            public ByteBuffer b(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<byte[], ByteBuffer> c(qa2 qa2Var) {
            return new ns(new C0254a(this));
        }
    }

    /* compiled from: ByteArrayLoader.java */
    /* renamed from: ns$b */
    /* loaded from: classes.dex */
    public interface b<Data> {
        Class<Data> a();

        Data b(byte[] bArr);
    }

    /* compiled from: ByteArrayLoader.java */
    /* renamed from: ns$c */
    /* loaded from: classes.dex */
    public static class c<Data> implements com.bumptech.glide.load.data.d<Data> {
        public final byte[] a;
        public final b<Data> f0;

        public c(byte[] bArr, b<Data> bVar) {
            this.a = bArr;
            this.f0 = bVar;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<Data> a() {
            return this.f0.a();
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super Data> aVar) {
            aVar.f((Data) this.f0.b(this.a));
        }
    }

    /* compiled from: ByteArrayLoader.java */
    /* renamed from: ns$d */
    /* loaded from: classes.dex */
    public static class d implements k92<byte[], InputStream> {

        /* compiled from: ByteArrayLoader.java */
        /* renamed from: ns$d$a */
        /* loaded from: classes.dex */
        public class a implements b<InputStream> {
            public a(d dVar) {
            }

            @Override // defpackage.ns.b
            public Class<InputStream> a() {
                return InputStream.class;
            }

            @Override // defpackage.ns.b
            /* renamed from: c */
            public InputStream b(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }
        }

        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<byte[], InputStream> c(qa2 qa2Var) {
            return new ns(new a(this));
        }
    }

    public ns(b<Data> bVar) {
        this.a = bVar;
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<Data> b(byte[] bArr, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(bArr), new c(bArr, this.a));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(byte[] bArr) {
        return true;
    }
}
