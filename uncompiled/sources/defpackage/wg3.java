package defpackage;

import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: wg3  reason: default package */
/* loaded from: classes2.dex */
public class wg3 extends xs0.b {
    public xg3 j;

    /* renamed from: wg3$a */
    /* loaded from: classes2.dex */
    public class a implements ht0 {
        public final /* synthetic */ int a;
        public final /* synthetic */ long[] b;

        public a(int i, long[] jArr) {
            this.a = i;
            this.b = jArr;
        }

        @Override // defpackage.ht0
        public int a() {
            return this.a;
        }

        @Override // defpackage.ht0
        public pt0 b(int i) {
            long[] i2 = ed2.i();
            long[] i3 = ed2.i();
            int i4 = 0;
            for (int i5 = 0; i5 < this.a; i5++) {
                long j = ((i5 ^ i) - 1) >> 31;
                for (int i6 = 0; i6 < 4; i6++) {
                    long j2 = i2[i6];
                    long[] jArr = this.b;
                    i2[i6] = j2 ^ (jArr[i4 + i6] & j);
                    i3[i6] = i3[i6] ^ (jArr[(i4 + 4) + i6] & j);
                }
                i4 += 8;
            }
            return wg3.this.i(new vg3(i2), new vg3(i3), false);
        }
    }

    public wg3() {
        super(233, 74, 0, 0);
        this.j = new xg3(this, null, null);
        this.b = n(BigInteger.valueOf(0L));
        this.c = n(BigInteger.valueOf(1L));
        this.d = new BigInteger(1, pk1.a("8000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF"));
        this.e = BigInteger.valueOf(4L);
        this.f = 6;
    }

    @Override // defpackage.xs0
    public boolean D(int i) {
        return i == 6;
    }

    @Override // defpackage.xs0.b
    public boolean H() {
        return true;
    }

    @Override // defpackage.xs0
    public xs0 c() {
        return new wg3();
    }

    @Override // defpackage.xs0
    public ht0 e(pt0[] pt0VarArr, int i, int i2) {
        long[] jArr = new long[i2 * 4 * 2];
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            pt0 pt0Var = pt0VarArr[i + i4];
            ed2.g(((vg3) pt0Var.n()).f, 0, jArr, i3);
            int i5 = i3 + 4;
            ed2.g(((vg3) pt0Var.o()).f, 0, jArr, i5);
            i3 = i5 + 4;
        }
        return new a(i2, jArr);
    }

    @Override // defpackage.xs0
    public it0 f() {
        return new ll4();
    }

    @Override // defpackage.xs0
    public pt0 i(ct0 ct0Var, ct0 ct0Var2, boolean z) {
        return new xg3(this, ct0Var, ct0Var2, z);
    }

    @Override // defpackage.xs0
    public pt0 j(ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        return new xg3(this, ct0Var, ct0Var2, ct0VarArr, z);
    }

    @Override // defpackage.xs0
    public ct0 n(BigInteger bigInteger) {
        return new vg3(bigInteger);
    }

    @Override // defpackage.xs0
    public int u() {
        return 233;
    }

    @Override // defpackage.xs0
    public pt0 v() {
        return this.j;
    }
}
