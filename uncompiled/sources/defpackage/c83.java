package defpackage;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* compiled from: ResourceRecycler.java */
/* renamed from: c83  reason: default package */
/* loaded from: classes.dex */
public class c83 {
    public boolean a;
    public final Handler b = new Handler(Looper.getMainLooper(), new a());

    /* compiled from: ResourceRecycler.java */
    /* renamed from: c83$a */
    /* loaded from: classes.dex */
    public static final class a implements Handler.Callback {
        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            if (message.what == 1) {
                ((s73) message.obj).b();
                return true;
            }
            return false;
        }
    }

    public synchronized void a(s73<?> s73Var, boolean z) {
        if (!this.a && !z) {
            this.a = true;
            s73Var.b();
            this.a = false;
        }
        this.b.obtainMessage(1, s73Var).sendToTarget();
    }
}
