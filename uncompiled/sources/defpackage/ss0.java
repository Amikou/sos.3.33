package defpackage;

import android.os.Looper;
import android.util.AndroidRuntimeException;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import defpackage.je;
import defpackage.ss0;
import java.util.ArrayList;

/* compiled from: DynamicAnimation.java */
/* renamed from: ss0  reason: default package */
/* loaded from: classes.dex */
public abstract class ss0<T extends ss0<T>> implements je.b {
    public static final r m;
    public static final r n;
    public static final r o;
    public static final r p;
    public static final r q;
    public static final r r;
    public final Object d;
    public final d71 e;
    public float j;
    public float a = Utils.FLOAT_EPSILON;
    public float b = Float.MAX_VALUE;
    public boolean c = false;
    public boolean f = false;
    public float g = Float.MAX_VALUE;
    public float h = -Float.MAX_VALUE;
    public long i = 0;
    public final ArrayList<p> k = new ArrayList<>();
    public final ArrayList<q> l = new ArrayList<>();

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$a */
    /* loaded from: classes.dex */
    public static class a extends r {
        public a(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getY();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setY(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$b */
    /* loaded from: classes.dex */
    public static class b extends r {
        public b(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return ei4.Q(view);
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            ei4.O0(view, f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$c */
    /* loaded from: classes.dex */
    public static class c extends r {
        public c(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getAlpha();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setAlpha(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$d */
    /* loaded from: classes.dex */
    public static class d extends r {
        public d(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getScrollX();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setScrollX((int) f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$e */
    /* loaded from: classes.dex */
    public static class e extends r {
        public e(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getScrollY();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setScrollY((int) f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$f */
    /* loaded from: classes.dex */
    public static class f extends r {
        public f(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getTranslationX();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setTranslationX(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$g */
    /* loaded from: classes.dex */
    public static class g extends r {
        public g(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getTranslationY();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setTranslationY(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$h */
    /* loaded from: classes.dex */
    public static class h extends r {
        public h(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return ei4.O(view);
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            ei4.M0(view, f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$i */
    /* loaded from: classes.dex */
    public static class i extends r {
        public i(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getScaleX();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setScaleX(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$j */
    /* loaded from: classes.dex */
    public static class j extends r {
        public j(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getScaleY();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setScaleY(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$k */
    /* loaded from: classes.dex */
    public static class k extends r {
        public k(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getRotation();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setRotation(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$l */
    /* loaded from: classes.dex */
    public static class l extends r {
        public l(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getRotationX();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setRotationX(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$m */
    /* loaded from: classes.dex */
    public static class m extends r {
        public m(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getRotationY();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setRotationY(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$n */
    /* loaded from: classes.dex */
    public static class n extends r {
        public n(String str) {
            super(str, null);
        }

        @Override // defpackage.d71
        /* renamed from: c */
        public float a(View view) {
            return view.getX();
        }

        @Override // defpackage.d71
        /* renamed from: d */
        public void b(View view, float f) {
            view.setX(f);
        }
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$o */
    /* loaded from: classes.dex */
    public static class o {
        public float a;
        public float b;
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$p */
    /* loaded from: classes.dex */
    public interface p {
        void a(ss0 ss0Var, boolean z, float f, float f2);
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$q */
    /* loaded from: classes.dex */
    public interface q {
        void a(ss0 ss0Var, float f, float f2);
    }

    /* compiled from: DynamicAnimation.java */
    /* renamed from: ss0$r */
    /* loaded from: classes.dex */
    public static abstract class r extends d71<View> {
        public /* synthetic */ r(String str, f fVar) {
            this(str);
        }

        public r(String str) {
            super(str);
        }
    }

    static {
        new f("translationX");
        new g("translationY");
        new h("translationZ");
        m = new i("scaleX");
        n = new j("scaleY");
        o = new k("rotation");
        p = new l("rotationX");
        q = new m("rotationY");
        new n("x");
        new a("y");
        new b("z");
        r = new c("alpha");
        new d("scrollX");
        new e("scrollY");
    }

    public <K> ss0(K k2, d71<K> d71Var) {
        this.d = k2;
        this.e = d71Var;
        if (d71Var != o && d71Var != p && d71Var != q) {
            if (d71Var == r) {
                this.j = 0.00390625f;
                return;
            } else if (d71Var != m && d71Var != n) {
                this.j = 1.0f;
                return;
            } else {
                this.j = 0.00390625f;
                return;
            }
        }
        this.j = 0.1f;
    }

    public static <T> void g(ArrayList<T> arrayList) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size) == null) {
                arrayList.remove(size);
            }
        }
    }

    @Override // defpackage.je.b
    public boolean a(long j2) {
        long j3 = this.i;
        if (j3 == 0) {
            this.i = j2;
            h(this.b);
            return false;
        }
        this.i = j2;
        boolean l2 = l(j2 - j3);
        float min = Math.min(this.b, this.g);
        this.b = min;
        float max = Math.max(min, this.h);
        this.b = max;
        h(max);
        if (l2) {
            c(false);
        }
        return l2;
    }

    public void b() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (this.f) {
                c(true);
                return;
            }
            return;
        }
        throw new AndroidRuntimeException("Animations may only be canceled on the main thread");
    }

    public final void c(boolean z) {
        this.f = false;
        je.d().g(this);
        this.i = 0L;
        this.c = false;
        for (int i2 = 0; i2 < this.k.size(); i2++) {
            if (this.k.get(i2) != null) {
                this.k.get(i2).a(this, z, this.b, this.a);
            }
        }
        g(this.k);
    }

    public final float d() {
        return this.e.a(this.d);
    }

    public float e() {
        return this.j * 0.75f;
    }

    public boolean f() {
        return this.f;
    }

    public void h(float f2) {
        this.e.b(this.d, f2);
        for (int i2 = 0; i2 < this.l.size(); i2++) {
            if (this.l.get(i2) != null) {
                this.l.get(i2).a(this, this.b, this.a);
            }
        }
        g(this.l);
    }

    public T i(float f2) {
        this.b = f2;
        this.c = true;
        return this;
    }

    public void j() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (this.f) {
                return;
            }
            k();
            return;
        }
        throw new AndroidRuntimeException("Animations may only be started on the main thread");
    }

    public final void k() {
        if (this.f) {
            return;
        }
        this.f = true;
        if (!this.c) {
            this.b = d();
        }
        float f2 = this.b;
        if (f2 <= this.g && f2 >= this.h) {
            je.d().a(this, 0L);
            return;
        }
        throw new IllegalArgumentException("Starting value need to be in between min value and max value");
    }

    public abstract boolean l(long j2);
}
