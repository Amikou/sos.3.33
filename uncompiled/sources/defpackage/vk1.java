package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: HolderNotificationHistoryItemHeaderBinding.java */
/* renamed from: vk1  reason: default package */
/* loaded from: classes2.dex */
public final class vk1 {
    public final ConstraintLayout a;
    public final TextView b;

    public vk1(ConstraintLayout constraintLayout, TextView textView, TextView textView2) {
        this.a = constraintLayout;
        this.b = textView;
    }

    public static vk1 a(View view) {
        int i = R.id.tvNotificationDate;
        TextView textView = (TextView) ai4.a(view, R.id.tvNotificationDate);
        if (textView != null) {
            i = R.id.tvNotificationText;
            TextView textView2 = (TextView) ai4.a(view, R.id.tvNotificationText);
            if (textView2 != null) {
                return new vk1((ConstraintLayout) view, textView, textView2);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
