package defpackage;

import com.google.android.gms.internal.firebase_messaging.c;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: l45  reason: default package */
/* loaded from: classes.dex */
public final class l45 implements fv0<l45> {
    public static final hl2<Object> d = h45.a;
    public static final /* synthetic */ int e = 0;
    public final Map<Class<?>, hl2<?>> a = new HashMap();
    public final Map<Class<?>, zg4<?>> b = new HashMap();
    public final hl2<Object> c = d;

    @Override // defpackage.fv0
    public final /* bridge */ /* synthetic */ l45 a(Class cls, hl2 hl2Var) {
        this.a.put(cls, hl2Var);
        this.b.remove(cls);
        return this;
    }

    public final c b() {
        return new c(new HashMap(this.a), new HashMap(this.b), this.c);
    }
}
