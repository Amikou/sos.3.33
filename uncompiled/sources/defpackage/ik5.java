package defpackage;

import com.google.android.gms.internal.measurement.g1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ik5  reason: default package */
/* loaded from: classes.dex */
public final class ik5 extends w1<g1, ik5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public ik5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.g1 r0 = com.google.android.gms.internal.measurement.g1.y()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ik5.<init>():void");
    }

    public final ik5 v(wj5 wj5Var) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        g1.z((g1) this.f0, wj5Var.o());
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ ik5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.g1 r1 = com.google.android.gms.internal.measurement.g1.y()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ik5.<init>(wi5):void");
    }
}
