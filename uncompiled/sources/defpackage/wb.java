package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.Objects;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.viewmodels.MyTokensListViewModel;

/* compiled from: AnchorChainBalance.kt */
/* renamed from: wb  reason: default package */
/* loaded from: classes2.dex */
public final class wb {
    public final MyTokensListViewModel a;
    public PopupWindow b;
    public final int c;

    public wb(MyTokensListViewModel myTokensListViewModel) {
        fs1.f(myTokensListViewModel, "viewModel");
        this.a = myTokensListViewModel;
        this.c = R.color.curve_green;
    }

    public static final void h(wb wbVar, View view) {
        fs1.f(wbVar, "this$0");
        wbVar.e().H(false);
        wbVar.d();
    }

    public static final void i(wb wbVar, View view) {
        fs1.f(wbVar, "this$0");
        wbVar.e().H(true);
        wbVar.d();
    }

    public final PopupWindow c(View view, View view2) {
        PopupWindow popupWindow = new PopupWindow(view, view2 == null ? -1 : view2.getWidth(), -2);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void d() {
        PopupWindow popupWindow;
        if (!f() || (popupWindow = this.b) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final MyTokensListViewModel e() {
        return this.a;
    }

    public final boolean f() {
        PopupWindow popupWindow = this.b;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public final wb g(Context context, View view, View view2) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        View inflate = ((LayoutInflater) systemService).inflate(R.layout.dialog_anchor_chain_balance, (ViewGroup) null);
        bn0 a = bn0.a(inflate);
        fs1.e(a, "bind(view)");
        if (fs1.b(e().C().getValue(), Boolean.TRUE)) {
            a.b.setTextColor(m70.d(context, this.c));
        } else {
            a.a.setTextColor(m70.d(context, this.c));
        }
        a.a.setOnClickListener(new View.OnClickListener() { // from class: vb
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                wb.h(wb.this, view3);
            }
        });
        a.b.setOnClickListener(new View.OnClickListener() { // from class: ub
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                wb.i(wb.this, view3);
            }
        });
        fs1.e(inflate, "view");
        PopupWindow c = c(inflate, view2);
        this.b = c;
        if (c != null) {
            c.showAsDropDown(view);
        }
        return this;
    }
}
