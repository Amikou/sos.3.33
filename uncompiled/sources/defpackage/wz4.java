package defpackage;

import android.content.Context;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: wz4  reason: default package */
/* loaded from: classes.dex */
public final class wz4 extends b05 {
    public final Map<a.f, tz4> f0;
    public final /* synthetic */ rz4 g0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public wz4(rz4 rz4Var, Map<a.f, tz4> map) {
        super(rz4Var, null);
        this.g0 = rz4Var;
        this.f0 = map;
    }

    @Override // defpackage.b05
    public final void a() {
        eh1 eh1Var;
        Context context;
        boolean z;
        Context context2;
        i05 i05Var;
        p15 p15Var;
        p15 p15Var2;
        i05 i05Var2;
        Context context3;
        boolean z2;
        eh1Var = this.g0.d;
        y15 y15Var = new y15(eh1Var);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (a.f fVar : this.f0.keySet()) {
            if (fVar.p()) {
                z2 = this.f0.get(fVar).c;
                if (!z2) {
                    arrayList.add(fVar);
                }
            }
            arrayList2.add(fVar);
        }
        int i = -1;
        int i2 = 0;
        if (arrayList.isEmpty()) {
            int size = arrayList2.size();
            while (i2 < size) {
                Object obj = arrayList2.get(i2);
                i2++;
                context3 = this.g0.c;
                i = y15Var.a(context3, (a.f) obj);
                if (i == 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList.size();
            while (i2 < size2) {
                Object obj2 = arrayList.get(i2);
                i2++;
                context = this.g0.c;
                i = y15Var.a(context, (a.f) obj2);
                if (i != 0) {
                    break;
                }
            }
        }
        if (i == 0) {
            z = this.g0.m;
            if (z) {
                p15Var = this.g0.k;
                if (p15Var != null) {
                    p15Var2 = this.g0.k;
                    p15Var2.f();
                }
            }
            for (a.f fVar2 : this.f0.keySet()) {
                tz4 tz4Var = this.f0.get(fVar2);
                if (fVar2.p()) {
                    context2 = this.g0.c;
                    if (y15Var.a(context2, fVar2) != 0) {
                        i05Var = this.g0.a;
                        i05Var.k(new yz4(this, this.g0, tz4Var));
                    }
                }
                fVar2.m(tz4Var);
            }
            return;
        }
        ConnectionResult connectionResult = new ConnectionResult(i, null);
        i05Var2 = this.g0.a;
        i05Var2.k(new vz4(this, this.g0, connectionResult));
    }
}
