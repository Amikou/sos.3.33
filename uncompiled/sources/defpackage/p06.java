package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: p06  reason: default package */
/* loaded from: classes.dex */
public final class p06 implements o06 {
    public static final wo5<Long> A;
    public static final wo5<Long> B;
    public static final wo5<Long> C;
    public static final wo5<Long> D;
    public static final wo5<Long> E;
    public static final wo5<Long> F;
    public static final wo5<Long> G;
    public static final wo5<String> H;
    public static final wo5<Long> I;
    public static final wo5<Long> a;
    public static final wo5<Long> b;
    public static final wo5<Long> c;
    public static final wo5<String> d;
    public static final wo5<String> e;
    public static final wo5<Long> f;
    public static final wo5<Long> g;
    public static final wo5<Long> h;
    public static final wo5<Long> i;
    public static final wo5<Long> j;
    public static final wo5<Long> k;
    public static final wo5<Long> l;
    public static final wo5<Long> m;
    public static final wo5<Long> n;
    public static final wo5<Long> o;
    public static final wo5<Long> p;
    public static final wo5<Long> q;
    public static final wo5<Long> r;
    public static final wo5<Long> s;
    public static final wo5<Long> t;
    public static final wo5<Long> u;
    public static final wo5<Long> v;
    public static final wo5<Long> w;
    public static final wo5<Long> x;
    public static final wo5<Long> y;
    public static final wo5<Long> z;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        a = ro5Var.a("measurement.ad_id_cache_time", 10000L);
        b = ro5Var.a("measurement.max_bundles_per_iteration", 100L);
        c = ro5Var.a("measurement.config.cache_time", 86400000L);
        ro5Var.d("measurement.log_tag", "FA");
        d = ro5Var.d("measurement.config.url_authority", "app-measurement.com");
        e = ro5Var.d("measurement.config.url_scheme", "https");
        f = ro5Var.a("measurement.upload.debug_upload_interval", 1000L);
        g = ro5Var.a("measurement.lifetimevalue.max_currency_tracked", 4L);
        h = ro5Var.a("measurement.store.max_stored_events_per_app", 100000L);
        i = ro5Var.a("measurement.experiment.max_ids", 50L);
        j = ro5Var.a("measurement.audience.filter_result_max_count", 200L);
        k = ro5Var.a("measurement.alarm_manager.minimum_interval", 60000L);
        l = ro5Var.a("measurement.upload.minimum_delay", 500L);
        m = ro5Var.a("measurement.monitoring.sample_period_millis", 86400000L);
        n = ro5Var.a("measurement.upload.realtime_upload_interval", 10000L);
        o = ro5Var.a("measurement.upload.refresh_blacklisted_config_interval", 604800000L);
        ro5Var.a("measurement.config.cache_time.service", 3600000L);
        p = ro5Var.a("measurement.service_client.idle_disconnect_millis", 5000L);
        ro5Var.d("measurement.log_tag.service", "FA-SVC");
        q = ro5Var.a("measurement.upload.stale_data_deletion_interval", 86400000L);
        r = ro5Var.a("measurement.sdk.attribution.cache.ttl", 604800000L);
        s = ro5Var.a("measurement.upload.backoff_period", 43200000L);
        t = ro5Var.a("measurement.upload.initial_upload_delay_time", u84.DEFAULT_POLLING_FREQUENCY);
        u = ro5Var.a("measurement.upload.interval", 3600000L);
        v = ro5Var.a("measurement.upload.max_bundle_size", 65536L);
        w = ro5Var.a("measurement.upload.max_bundles", 100L);
        x = ro5Var.a("measurement.upload.max_conversions_per_day", 500L);
        y = ro5Var.a("measurement.upload.max_error_events_per_day", 1000L);
        z = ro5Var.a("measurement.upload.max_events_per_bundle", 1000L);
        A = ro5Var.a("measurement.upload.max_events_per_day", 100000L);
        B = ro5Var.a("measurement.upload.max_public_events_per_day", 50000L);
        C = ro5Var.a("measurement.upload.max_queue_time", 2419200000L);
        D = ro5Var.a("measurement.upload.max_realtime_events_per_day", 10L);
        E = ro5Var.a("measurement.upload.max_batch_size", 65536L);
        F = ro5Var.a("measurement.upload.retry_count", 6L);
        G = ro5Var.a("measurement.upload.retry_time", 1800000L);
        H = ro5Var.d("measurement.upload.url", "https://app-measurement.com/a");
        I = ro5Var.a("measurement.upload.window_interval", 3600000L);
    }

    @Override // defpackage.o06
    public final long A() {
        return B.e().longValue();
    }

    @Override // defpackage.o06
    public final long B() {
        return C.e().longValue();
    }

    @Override // defpackage.o06
    public final String C() {
        return H.e();
    }

    @Override // defpackage.o06
    public final long D() {
        return s.e().longValue();
    }

    @Override // defpackage.o06
    public final long E() {
        return I.e().longValue();
    }

    @Override // defpackage.o06
    public final long F() {
        return A.e().longValue();
    }

    @Override // defpackage.o06
    public final long a() {
        return k.e().longValue();
    }

    @Override // defpackage.o06
    public final String b() {
        return d.e();
    }

    @Override // defpackage.o06
    public final String c() {
        return e.e();
    }

    @Override // defpackage.o06
    public final long d() {
        return p.e().longValue();
    }

    @Override // defpackage.o06
    public final long e() {
        return r.e().longValue();
    }

    @Override // defpackage.o06
    public final long f() {
        return h.e().longValue();
    }

    @Override // defpackage.o06
    public final long g() {
        return i.e().longValue();
    }

    @Override // defpackage.o06
    public final long h() {
        return g.e().longValue();
    }

    @Override // defpackage.o06
    public final long i() {
        return f.e().longValue();
    }

    @Override // defpackage.o06
    public final long j() {
        return q.e().longValue();
    }

    @Override // defpackage.o06
    public final long k() {
        return m.e().longValue();
    }

    @Override // defpackage.o06
    public final long l() {
        return j.e().longValue();
    }

    @Override // defpackage.o06
    public final long m() {
        return t.e().longValue();
    }

    @Override // defpackage.o06
    public final long n() {
        return F.e().longValue();
    }

    @Override // defpackage.o06
    public final long o() {
        return y.e().longValue();
    }

    @Override // defpackage.o06
    public final long p() {
        return z.e().longValue();
    }

    @Override // defpackage.o06
    public final long q() {
        return G.e().longValue();
    }

    @Override // defpackage.o06
    public final long r() {
        return n.e().longValue();
    }

    @Override // defpackage.o06
    public final long s() {
        return w.e().longValue();
    }

    @Override // defpackage.o06
    public final long t() {
        return D.e().longValue();
    }

    @Override // defpackage.o06
    public final long u() {
        return u.e().longValue();
    }

    @Override // defpackage.o06
    public final long v() {
        return v.e().longValue();
    }

    @Override // defpackage.o06
    public final long w() {
        return o.e().longValue();
    }

    @Override // defpackage.o06
    public final long x() {
        return E.e().longValue();
    }

    @Override // defpackage.o06
    public final long y() {
        return x.e().longValue();
    }

    @Override // defpackage.o06
    public final long z() {
        return l.e().longValue();
    }

    @Override // defpackage.o06
    public final long zza() {
        return a.e().longValue();
    }

    @Override // defpackage.o06
    public final long zzb() {
        return b.e().longValue();
    }

    @Override // defpackage.o06
    public final long zzc() {
        return c.e().longValue();
    }
}
