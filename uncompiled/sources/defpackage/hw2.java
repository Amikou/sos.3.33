package defpackage;

import java.io.File;

/* compiled from: ProviderProxyNativeComponent.java */
/* renamed from: hw2  reason: default package */
/* loaded from: classes2.dex */
public final class hw2 implements o90 {
    public static final ud2 b = new b();
    public final fw2<o90> a;

    /* compiled from: ProviderProxyNativeComponent.java */
    /* renamed from: hw2$b */
    /* loaded from: classes2.dex */
    public static final class b implements ud2 {
        public b() {
        }

        @Override // defpackage.ud2
        public File a() {
            return null;
        }

        @Override // defpackage.ud2
        public File b() {
            return null;
        }

        @Override // defpackage.ud2
        public File c() {
            return null;
        }

        @Override // defpackage.ud2
        public File d() {
            return null;
        }

        @Override // defpackage.ud2
        public File e() {
            return null;
        }

        @Override // defpackage.ud2
        public File f() {
            return null;
        }
    }

    public hw2(fw2<o90> fw2Var) {
        this.a = fw2Var;
    }

    @Override // defpackage.o90
    public boolean a(String str) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            return o90Var.a(str);
        }
        return true;
    }

    @Override // defpackage.o90
    public ud2 b(String str) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            return o90Var.b(str);
        }
        return b;
    }

    @Override // defpackage.o90
    public void c(String str, int i, String str2, int i2, long j, long j2, boolean z, int i3, String str3, String str4) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            o90Var.c(str, i, str2, i2, j, j2, z, i3, str3, str4);
        }
    }

    @Override // defpackage.o90
    public void d(String str, String str2, long j) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            o90Var.d(str, str2, j);
        }
    }

    @Override // defpackage.o90
    public boolean e(String str) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            return o90Var.e(str);
        }
        return false;
    }

    @Override // defpackage.o90
    public void f(String str, String str2, String str3, String str4, String str5, int i, String str6) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            o90Var.f(str, str2, str3, str4, str5, i, str6);
        }
    }

    @Override // defpackage.o90
    public void g(String str, String str2, String str3, boolean z) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            o90Var.g(str, str2, str3, z);
        }
    }

    @Override // defpackage.o90
    public boolean h(String str) {
        o90 o90Var = this.a.get();
        if (o90Var != null) {
            return o90Var.h(str);
        }
        return true;
    }
}
