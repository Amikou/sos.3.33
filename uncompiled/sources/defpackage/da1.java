package defpackage;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentFiatListBinding.java */
/* renamed from: da1  reason: default package */
/* loaded from: classes2.dex */
public final class da1 {
    public final ConstraintLayout a;
    public final EditText b;
    public final RecyclerView c;
    public final rp3 d;
    public final TextView e;

    public da1(ConstraintLayout constraintLayout, EditText editText, ProgressBar progressBar, RecyclerView recyclerView, rp3 rp3Var, TextView textView, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = editText;
        this.c = recyclerView;
        this.d = rp3Var;
        this.e = textView;
    }

    public static da1 a(View view) {
        int i = R.id.etSearch;
        EditText editText = (EditText) ai4.a(view, R.id.etSearch);
        if (editText != null) {
            i = R.id.pbRealCurrency;
            ProgressBar progressBar = (ProgressBar) ai4.a(view, R.id.pbRealCurrency);
            if (progressBar != null) {
                i = R.id.rvRealCurrency;
                RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvRealCurrency);
                if (recyclerView != null) {
                    i = R.id.toolbar;
                    View a = ai4.a(view, R.id.toolbar);
                    if (a != null) {
                        rp3 a2 = rp3.a(a);
                        i = R.id.tv_not_fount;
                        TextView textView = (TextView) ai4.a(view, R.id.tv_not_fount);
                        if (textView != null) {
                            i = R.id.vSearchContainer;
                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vSearchContainer);
                            if (linearLayout != null) {
                                return new da1((ConstraintLayout) view, editText, progressBar, recyclerView, a2, textView, linearLayout);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
