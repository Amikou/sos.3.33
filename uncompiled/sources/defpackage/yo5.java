package defpackage;

import android.net.Uri;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: yo5  reason: default package */
/* loaded from: classes.dex */
public final class yo5 implements Runnable {
    public final /* synthetic */ boolean a;
    public final /* synthetic */ Uri f0;
    public final /* synthetic */ String g0;
    public final /* synthetic */ String h0;
    public final /* synthetic */ bp5 i0;

    public yo5(bp5 bp5Var, boolean z, Uri uri, String str, String str2) {
        this.i0 = bp5Var;
        this.a = z;
        this.f0 = uri;
        this.g0 = str;
        this.h0 = str2;
    }

    /* JADX WARN: Removed duplicated region for block: B:31:0x00ab A[Catch: RuntimeException -> 0x01d6, TRY_ENTER, TryCatch #0 {RuntimeException -> 0x01d6, blocks: (B:3:0x0011, B:6:0x002e, B:8:0x003e, B:31:0x00ab, B:33:0x00b9, B:35:0x00cd, B:38:0x00d5, B:40:0x00db, B:41:0x00ef, B:43:0x00fd, B:46:0x0110, B:49:0x0123, B:52:0x012b, B:54:0x0131, B:55:0x013d, B:58:0x0145, B:62:0x016b, B:64:0x0189, B:63:0x0178, B:66:0x0191, B:68:0x0197, B:70:0x019d, B:72:0x01a3, B:74:0x01a9, B:76:0x01b1, B:78:0x01b9, B:80:0x01bf, B:82:0x01c6, B:10:0x004e, B:13:0x005e, B:15:0x0064, B:17:0x006a, B:19:0x0070, B:21:0x0076, B:22:0x0084, B:24:0x008c, B:26:0x0097, B:28:0x00a1, B:25:0x0091), top: B:87:0x0011 }] */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00fc  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0143  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0145 A[Catch: RuntimeException -> 0x01d6, TRY_LEAVE, TryCatch #0 {RuntimeException -> 0x01d6, blocks: (B:3:0x0011, B:6:0x002e, B:8:0x003e, B:31:0x00ab, B:33:0x00b9, B:35:0x00cd, B:38:0x00d5, B:40:0x00db, B:41:0x00ef, B:43:0x00fd, B:46:0x0110, B:49:0x0123, B:52:0x012b, B:54:0x0131, B:55:0x013d, B:58:0x0145, B:62:0x016b, B:64:0x0189, B:63:0x0178, B:66:0x0191, B:68:0x0197, B:70:0x019d, B:72:0x01a3, B:74:0x01a9, B:76:0x01b1, B:78:0x01b9, B:80:0x01bf, B:82:0x01c6, B:10:0x004e, B:13:0x005e, B:15:0x0064, B:17:0x006a, B:19:0x0070, B:21:0x0076, B:22:0x0084, B:24:0x008c, B:26:0x0097, B:28:0x00a1, B:25:0x0091), top: B:87:0x0011 }] */
    @Override // java.lang.Runnable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void run() {
        /*
            Method dump skipped, instructions count: 489
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.yo5.run():void");
    }
}
