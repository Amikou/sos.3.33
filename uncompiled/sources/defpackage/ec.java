package defpackage;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.android.material.textfield.TextInputEditText;
import java.util.Objects;
import net.safemoon.androidwallet.R;

/* compiled from: AnchorFiatCurrency.kt */
/* renamed from: ec  reason: default package */
/* loaded from: classes2.dex */
public abstract class ec {
    public final Context a;
    public final w21 b;
    public final View c;
    public final rc1<te4> d;
    public PopupWindow e;

    /* compiled from: TextView.kt */
    /* renamed from: ec$a */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public a() {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ec.this.h(charSequence == null ? null : charSequence.toString());
        }
    }

    public ec(Context context, w21 w21Var, View view, final rc1<te4> rc1Var) {
        fs1.f(context, "context");
        fs1.f(w21Var, "adapter");
        fs1.f(rc1Var, "onDismissListener");
        this.a = context;
        this.b = w21Var;
        this.c = view;
        this.d = rc1Var;
        Object systemService = context.getSystemService("layout_inflater");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.LayoutInflater");
        View inflate = ((LayoutInflater) systemService).inflate(R.layout.dialog_anchor_fiat_currency, (ViewGroup) null);
        en0 a2 = en0.a(inflate);
        fs1.e(a2, "bind(view)");
        a2.a.setLayoutManager(new LinearLayoutManager(f(), 1, false));
        a2.a.setAdapter(e());
        TextInputEditText textInputEditText = a2.b.b;
        fs1.e(textInputEditText, "searchBar.etSearch");
        textInputEditText.addTextChangedListener(new a());
        fs1.e(inflate, "view");
        PopupWindow c = c(inflate, view);
        this.e = c;
        if (c == null) {
            return;
        }
        c.setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: dc
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                ec.b(rc1.this);
            }
        });
    }

    public static final void b(rc1 rc1Var) {
        fs1.f(rc1Var, "$tmp0");
        rc1Var.invoke();
    }

    public final PopupWindow c(View view, View view2) {
        PopupWindow popupWindow = new PopupWindow(view, (int) ((view2 == null ? -1 : view2.getWidth()) * 0.8d), (int) t40.a(400));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setInputMethodMode(1);
        popupWindow.setAttachedInDecor(false);
        return popupWindow;
    }

    public final void d() {
        PopupWindow popupWindow;
        if (!g() || (popupWindow = this.e) == null) {
            return;
        }
        popupWindow.dismiss();
    }

    public final w21 e() {
        return this.b;
    }

    public final Context f() {
        return this.a;
    }

    public final boolean g() {
        PopupWindow popupWindow = this.e;
        if (popupWindow == null) {
            return false;
        }
        return popupWindow.isShowing();
    }

    public abstract void h(String str);

    public final ec i(View view) {
        fs1.f(view, "anchorView");
        PopupWindow popupWindow = this.e;
        if (popupWindow != null) {
            popupWindow.showAsDropDown(view);
        }
        return this;
    }
}
