package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentManageContactsBinding.java */
/* renamed from: la1  reason: default package */
/* loaded from: classes2.dex */
public final class la1 {
    public final ConstraintLayout a;
    public final RecyclerView b;
    public final zd3 c;
    public final g74 d;
    public final TextView e;

    public la1(ConstraintLayout constraintLayout, RecyclerView recyclerView, zd3 zd3Var, g74 g74Var, TextView textView) {
        this.a = constraintLayout;
        this.b = recyclerView;
        this.c = zd3Var;
        this.d = g74Var;
        this.e = textView;
    }

    public static la1 a(View view) {
        int i = R.id.rvContactList;
        RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvContactList);
        if (recyclerView != null) {
            i = R.id.searchBar;
            View a = ai4.a(view, R.id.searchBar);
            if (a != null) {
                zd3 a2 = zd3.a(a);
                i = R.id.toolbar;
                View a3 = ai4.a(view, R.id.toolbar);
                if (a3 != null) {
                    g74 a4 = g74.a(a3);
                    i = R.id.tvNoItemsMsg;
                    TextView textView = (TextView) ai4.a(view, R.id.tvNoItemsMsg);
                    if (textView != null) {
                        return new la1((ConstraintLayout) view, recyclerView, a2, a4, textView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
