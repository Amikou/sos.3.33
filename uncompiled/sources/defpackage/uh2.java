package defpackage;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.recyclerview.widget.RecyclerView;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.fragments.NotificationHistoryFragment;
import net.safemoon.androidwallet.model.common.HeaderItemHistory;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;
import net.safemoon.androidwallet.model.notificationHistory.ResultItemHistory;

/* compiled from: NotificationHistoryAdapter.java */
/* renamed from: uh2  reason: default package */
/* loaded from: classes2.dex */
public class uh2 extends RecyclerView.Adapter<RecyclerView.a0> {
    public b c;
    public final List<HistoryListItem> d;
    public final f e;
    public vk1 f;
    public pi2 g;
    public List<String> a = new ArrayList();
    public boolean b = false;
    public DateFormat h = new SimpleDateFormat("hh.mm aa");

    /* compiled from: NotificationHistoryAdapter.java */
    /* renamed from: uh2$a */
    /* loaded from: classes2.dex */
    public class a implements CompoundButton.OnCheckedChangeListener {
        public final /* synthetic */ NotificationHistoryResult a;

        public a(NotificationHistoryResult notificationHistoryResult) {
            this.a = notificationHistoryResult;
        }

        @Override // android.widget.CompoundButton.OnCheckedChangeListener
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (z) {
                uh2.this.a.add(this.a.id);
            } else {
                uh2.this.a.remove(this.a.id);
            }
            if (uh2.this.c != null) {
                uh2.this.c.a(uh2.this.a);
            }
        }
    }

    /* compiled from: NotificationHistoryAdapter.java */
    /* renamed from: uh2$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a(List<String> list);
    }

    /* compiled from: NotificationHistoryAdapter.java */
    /* renamed from: uh2$c */
    /* loaded from: classes2.dex */
    public class c extends RecyclerView.a0 {
        public c(uh2 uh2Var, View view) {
            super(view);
        }
    }

    /* compiled from: NotificationHistoryAdapter.java */
    /* renamed from: uh2$d */
    /* loaded from: classes2.dex */
    public static class d extends RecyclerView.a0 {
        public final vk1 a;

        public d(View view, vk1 vk1Var) {
            super(view);
            this.a = vk1Var;
        }

        public void a(HistoryListItem historyListItem) {
            this.a.b.setText(((HeaderItemHistory) historyListItem).getTitle());
        }
    }

    /* compiled from: NotificationHistoryAdapter.java */
    /* renamed from: uh2$e */
    /* loaded from: classes2.dex */
    public class e extends RecyclerView.a0 {
        public pi2 a;

        /* compiled from: NotificationHistoryAdapter.java */
        /* renamed from: uh2$e$a */
        /* loaded from: classes2.dex */
        public class a implements View.OnClickListener {
            public final /* synthetic */ NotificationHistoryResult a;

            public a(NotificationHistoryResult notificationHistoryResult) {
                this.a = notificationHistoryResult;
            }

            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                if (NotificationHistoryFragment.m0) {
                    e.this.a.b.setChecked(!e.this.a.b.isChecked());
                } else {
                    uh2.this.e.a(this.a);
                }
            }
        }

        public e(View view, pi2 pi2Var) {
            super(view);
            this.a = pi2Var;
        }

        public void b(HistoryListItem historyListItem) {
            NotificationHistoryResult result = ((ResultItemHistory) historyListItem).getResult();
            if (result == null) {
                return;
            }
            this.a.c.setBackgroundResource(result.read ? R.drawable.trans_button_bg : R.drawable.trans_button_bg_unread);
            this.a.d.setText(result.body);
            this.a.f.setText(result.title);
            this.a.e.setText(uh2.this.f(result.createdAt));
            this.itemView.setOnClickListener(new a(result));
            uh2.this.g(this.a, result);
        }
    }

    /* compiled from: NotificationHistoryAdapter.java */
    /* renamed from: uh2$f */
    /* loaded from: classes2.dex */
    public interface f {
        void a(NotificationHistoryResult notificationHistoryResult);
    }

    public uh2(List<HistoryListItem> list, f fVar, b bVar) {
        this.d = list;
        this.e = fVar;
        this.c = bVar;
    }

    public final String f(String str) {
        try {
            return this.h.format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).parse(str));
        } catch (ParseException e2) {
            e2.printStackTrace();
            return "-";
        }
    }

    public final void g(pi2 pi2Var, NotificationHistoryResult notificationHistoryResult) {
        if (this.b) {
            pi2Var.b.setVisibility(0);
        } else {
            pi2Var.b.setVisibility(8);
        }
        pi2Var.b.setChecked(false);
        pi2Var.b.setOnCheckedChangeListener(new a(notificationHistoryResult));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.d.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.d.get(i).getType();
    }

    public void h(List<HistoryListItem> list) {
        this.d.clear();
        this.d.addAll(list);
        notifyDataSetChanged();
    }

    public void i(boolean z) {
        this.b = z;
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    @SuppressLint({"SetTextI18n"})
    public void onBindViewHolder(RecyclerView.a0 a0Var, int i) {
        HistoryListItem historyListItem = this.d.get(i);
        int type = historyListItem.getType();
        if (type == 0) {
            ((d) a0Var).a(historyListItem);
        } else if (type != 1) {
            if (type == 2) {
                ((e) a0Var).b(historyListItem);
                return;
            }
            throw new IllegalStateException("Unexpected value: " + historyListItem.getType());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.a0 onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 0) {
            this.f = vk1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_notification_history_item_header, viewGroup, false));
            return new d(this.f.b(), this.f);
        } else if (i == 1) {
            return new c(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_notification_history_item_footer, viewGroup, false));
        } else {
            this.g = pi2.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_history_item, viewGroup, false));
            return new e(this.g.b(), this.g);
        }
    }
}
