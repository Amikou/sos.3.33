package defpackage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

/* compiled from: ExceptionPassthroughInputStream.java */
/* renamed from: ly0  reason: default package */
/* loaded from: classes.dex */
public final class ly0 extends InputStream {
    public static final Queue<ly0> g0 = mg4.f(0);
    public InputStream a;
    public IOException f0;

    public static ly0 b(InputStream inputStream) {
        ly0 poll;
        Queue<ly0> queue = g0;
        synchronized (queue) {
            poll = queue.poll();
        }
        if (poll == null) {
            poll = new ly0();
        }
        poll.d(inputStream);
        return poll;
    }

    public IOException a() {
        return this.f0;
    }

    @Override // java.io.InputStream
    public int available() throws IOException {
        return this.a.available();
    }

    public void c() {
        this.f0 = null;
        this.a = null;
        Queue<ly0> queue = g0;
        synchronized (queue) {
            queue.offer(this);
        }
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    public void d(InputStream inputStream) {
        this.a = inputStream;
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        this.a.mark(i);
    }

    @Override // java.io.InputStream
    public boolean markSupported() {
        return this.a.markSupported();
    }

    @Override // java.io.InputStream
    public int read() throws IOException {
        try {
            return this.a.read();
        } catch (IOException e) {
            this.f0 = e;
            throw e;
        }
    }

    @Override // java.io.InputStream
    public synchronized void reset() throws IOException {
        this.a.reset();
    }

    @Override // java.io.InputStream
    public long skip(long j) throws IOException {
        try {
            return this.a.skip(j);
        } catch (IOException e) {
            this.f0 = e;
            throw e;
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) throws IOException {
        try {
            return this.a.read(bArr);
        } catch (IOException e) {
            this.f0 = e;
            throw e;
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        try {
            return this.a.read(bArr, i, i2);
        } catch (IOException e) {
            this.f0 = e;
            throw e;
        }
    }
}
