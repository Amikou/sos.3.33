package defpackage;

import java.util.Map;

/* JADX WARN: Incorrect field signature: TK; */
/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: by5  reason: default package */
/* loaded from: classes.dex */
public final class by5 implements Comparable<by5>, Map.Entry<K, V> {
    public final Comparable a;
    public V f0;
    public final /* synthetic */ ux5 g0;

    public by5(ux5 ux5Var, Map.Entry<K, V> entry) {
        this(ux5Var, (Comparable) entry.getKey(), entry.getValue());
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(by5 by5Var) {
        return ((Comparable) getKey()).compareTo((Comparable) by5Var.getKey());
    }

    @Override // java.util.Map.Entry
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            return a(this.a, entry.getKey()) && a(this.f0, entry.getValue());
        }
        return false;
    }

    @Override // java.util.Map.Entry
    public final /* synthetic */ Object getKey() {
        return this.a;
    }

    @Override // java.util.Map.Entry
    public final V getValue() {
        return this.f0;
    }

    @Override // java.util.Map.Entry
    public final int hashCode() {
        Comparable comparable = this.a;
        int hashCode = comparable == null ? 0 : comparable.hashCode();
        V v = this.f0;
        return hashCode ^ (v != 0 ? v.hashCode() : 0);
    }

    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        this.g0.p();
        V v2 = this.f0;
        this.f0 = v;
        return v2;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.f0);
        StringBuilder sb = new StringBuilder(valueOf.length() + 1 + valueOf2.length());
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        return sb.toString();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public by5(ux5 ux5Var, K k, V v) {
        this.g0 = ux5Var;
        this.a = k;
        this.f0 = v;
    }
}
