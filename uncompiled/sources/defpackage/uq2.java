package defpackage;

import androidx.fragment.app.Fragment;
import java.util.LinkedHashSet;

/* compiled from: PickerFragment.java */
/* renamed from: uq2  reason: default package */
/* loaded from: classes2.dex */
public abstract class uq2<S> extends Fragment {
    public final LinkedHashSet<sm2<S>> a = new LinkedHashSet<>();

    public boolean e(sm2<S> sm2Var) {
        return this.a.add(sm2Var);
    }

    public void f() {
        this.a.clear();
    }
}
