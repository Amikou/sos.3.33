package defpackage;

import net.safemoon.androidwallet.R;

/* compiled from: AllTokensListFragmentDirections.java */
/* renamed from: eb  reason: default package */
/* loaded from: classes2.dex */
public class eb {
    public static ce2 a() {
        return new l6(R.id.action_allTokensListFragment_to_myTokensListFragment);
    }

    public static ce2 b() {
        return new l6(R.id.action_allTokensListFragment_to_notificationHistoryFragment);
    }
}
