package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: w65  reason: default package */
/* loaded from: classes.dex */
public final class w65 extends o65 {
    public w65() {
        this.a.add(zzbl.APPLY);
        this.a.add(zzbl.BLOCK);
        this.a.add(zzbl.BREAK);
        this.a.add(zzbl.CASE);
        this.a.add(zzbl.DEFAULT);
        this.a.add(zzbl.CONTINUE);
        this.a.add(zzbl.DEFINE_FUNCTION);
        this.a.add(zzbl.FN);
        this.a.add(zzbl.IF);
        this.a.add(zzbl.QUOTE);
        this.a.add(zzbl.RETURN);
        this.a.add(zzbl.SWITCH);
        this.a.add(zzbl.TERNARY);
    }

    public static z55 c(wk5 wk5Var, List<z55> list) {
        vm5.b(zzbl.FN.name(), 2, list);
        z55 a = wk5Var.a(list.get(0));
        z55 a2 = wk5Var.a(list.get(1));
        if (a2 instanceof p45) {
            List<z55> p = ((p45) a2).p();
            List<z55> arrayList = new ArrayList<>();
            if (list.size() > 2) {
                arrayList = list.subList(2, list.size());
            }
            return new u55(a.zzc(), p, arrayList, wk5Var);
        }
        throw new IllegalArgumentException(String.format("FN requires an ArrayValue of parameter names found %s", a2.getClass().getCanonicalName()));
    }

    /* JADX WARN: Code restructure failed: missing block: B:61:0x0129, code lost:
        if (r8.equals("continue") == false) goto L67;
     */
    @Override // defpackage.o65
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final defpackage.z55 a(java.lang.String r8, defpackage.wk5 r9, java.util.List<defpackage.z55> r10) {
        /*
            Method dump skipped, instructions count: 636
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.w65.a(java.lang.String, wk5, java.util.List):z55");
    }
}
