package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAcknowledgmentWarningBinding.java */
/* renamed from: xm0  reason: default package */
/* loaded from: classes2.dex */
public final class xm0 {
    public final CardView a;
    public final MaterialButton b;
    public final MaterialButton c;

    public xm0(CardView cardView, MaterialButton materialButton, MaterialButton materialButton2, TextView textView, TextView textView2, TextView textView3, TextView textView4, LinearLayout linearLayout) {
        this.a = cardView;
        this.b = materialButton;
        this.c = materialButton2;
    }

    public static xm0 a(View view) {
        int i = R.id.btnAction;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAction);
        if (materialButton != null) {
            i = R.id.dialog_cross;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
            if (materialButton2 != null) {
                i = R.id.tvDialogContentLine1;
                TextView textView = (TextView) ai4.a(view, R.id.tvDialogContentLine1);
                if (textView != null) {
                    i = R.id.tvDialogContentLine2;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvDialogContentLine2);
                    if (textView2 != null) {
                        i = R.id.tvDialogContentLine3;
                        TextView textView3 = (TextView) ai4.a(view, R.id.tvDialogContentLine3);
                        if (textView3 != null) {
                            i = R.id.tvDialogTitle;
                            TextView textView4 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                            if (textView4 != null) {
                                i = R.id.vDialogContentContainer;
                                LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vDialogContentContainer);
                                if (linearLayout != null) {
                                    return new xm0((CardView) view, materialButton, materialButton2, textView, textView2, textView3, textView4, linearLayout);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
