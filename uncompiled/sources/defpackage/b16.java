package defpackage;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.p;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@19.0.0 */
/* renamed from: b16  reason: default package */
/* loaded from: classes.dex */
public final class b16 implements em5 {
    public final p a;
    public final /* synthetic */ AppMeasurementDynamiteService b;

    public b16(AppMeasurementDynamiteService appMeasurementDynamiteService, p pVar) {
        this.b = appMeasurementDynamiteService;
        this.a = pVar;
    }

    @Override // defpackage.em5
    public final void a(String str, String str2, Bundle bundle, long j) {
        try {
            this.a.y(str, str2, bundle, j);
        } catch (RemoteException e) {
            ck5 ck5Var = this.b.a;
            if (ck5Var != null) {
                ck5Var.w().p().b("Event listener threw exception", e);
            }
        }
    }
}
