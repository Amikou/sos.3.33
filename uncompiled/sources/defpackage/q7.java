package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityRegisterBinding.java */
/* renamed from: q7  reason: default package */
/* loaded from: classes2.dex */
public final class q7 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final LinearLayout c;
    public final TextInputLayout d;
    public final TextInputLayout e;
    public final TextInputLayout f;
    public final ImageView g;
    public final ImageView h;
    public final ImageView i;
    public final TextView j;
    public final sp3 k;
    public final TextView l;
    public final TextView m;
    public final TextView n;
    public final AppCompatTextView o;
    public final TextView p;

    public q7(ConstraintLayout constraintLayout, MaterialButton materialButton, LinearLayout linearLayout, ConstraintLayout constraintLayout2, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, ImageView imageView, ImageView imageView2, ImageView imageView3, TextView textView, TextView textView2, sp3 sp3Var, TextView textView3, TextView textView4, TextView textView5, TextView textView6, AppCompatTextView appCompatTextView, TextView textView7, TextView textView8, TextView textView9, TextView textView10) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = linearLayout;
        this.d = textInputLayout;
        this.e = textInputLayout2;
        this.f = textInputLayout3;
        this.g = imageView;
        this.h = imageView2;
        this.i = imageView3;
        this.j = textView;
        this.k = sp3Var;
        this.l = textView3;
        this.m = textView4;
        this.n = textView5;
        this.o = appCompatTextView;
        this.p = textView10;
    }

    public static q7 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            i = R.id.content_layout;
            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.content_layout);
            if (linearLayout != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                i = R.id.et_confirm_password;
                TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.et_confirm_password);
                if (textInputLayout != null) {
                    i = R.id.et_password;
                    TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.et_password);
                    if (textInputLayout2 != null) {
                        i = R.id.et_username;
                        TextInputLayout textInputLayout3 = (TextInputLayout) ai4.a(view, R.id.et_username);
                        if (textInputLayout3 != null) {
                            i = R.id.iv_1;
                            ImageView imageView = (ImageView) ai4.a(view, R.id.iv_1);
                            if (imageView != null) {
                                i = R.id.iv_2;
                                ImageView imageView2 = (ImageView) ai4.a(view, R.id.iv_2);
                                if (imageView2 != null) {
                                    i = R.id.iv_4;
                                    ImageView imageView3 = (ImageView) ai4.a(view, R.id.iv_4);
                                    if (imageView3 != null) {
                                        i = R.id.min_char;
                                        TextView textView = (TextView) ai4.a(view, R.id.min_char);
                                        if (textView != null) {
                                            i = R.id.textView21;
                                            TextView textView2 = (TextView) ai4.a(view, R.id.textView21);
                                            if (textView2 != null) {
                                                i = R.id.toolbar;
                                                View a = ai4.a(view, R.id.toolbar);
                                                if (a != null) {
                                                    sp3 a2 = sp3.a(a);
                                                    i = R.id.tv_not;
                                                    TextView textView3 = (TextView) ai4.a(view, R.id.tv_not);
                                                    if (textView3 != null) {
                                                        i = R.id.tv_number;
                                                        TextView textView4 = (TextView) ai4.a(view, R.id.tv_number);
                                                        if (textView4 != null) {
                                                            i = R.id.tv_special;
                                                            TextView textView5 = (TextView) ai4.a(view, R.id.tv_special);
                                                            if (textView5 != null) {
                                                                i = R.id.txt_confirm_password_header;
                                                                TextView textView6 = (TextView) ai4.a(view, R.id.txt_confirm_password_header);
                                                                if (textView6 != null) {
                                                                    i = R.id.txt_email;
                                                                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txt_email);
                                                                    if (appCompatTextView != null) {
                                                                        i = R.id.txt_email_header;
                                                                        TextView textView7 = (TextView) ai4.a(view, R.id.txt_email_header);
                                                                        if (textView7 != null) {
                                                                            i = R.id.txt_password_header;
                                                                            TextView textView8 = (TextView) ai4.a(view, R.id.txt_password_header);
                                                                            if (textView8 != null) {
                                                                                i = R.id.txt_username_header;
                                                                                TextView textView9 = (TextView) ai4.a(view, R.id.txt_username_header);
                                                                                if (textView9 != null) {
                                                                                    i = R.id.txt_username_invalid;
                                                                                    TextView textView10 = (TextView) ai4.a(view, R.id.txt_username_invalid);
                                                                                    if (textView10 != null) {
                                                                                        return new q7(constraintLayout, materialButton, linearLayout, constraintLayout, textInputLayout, textInputLayout2, textInputLayout3, imageView, imageView2, imageView3, textView, textView2, a2, textView3, textView4, textView5, textView6, appCompatTextView, textView7, textView8, textView9, textView10);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static q7 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static q7 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_register, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
