package defpackage;

import android.os.Bundle;
import com.google.android.gms.measurement.internal.zzaa;
import com.google.android.gms.measurement.internal.zzas;
import com.google.android.gms.measurement.internal.zzkq;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: pn5  reason: default package */
/* loaded from: classes.dex */
public final class pn5 implements Runnable {
    public final /* synthetic */ Bundle a;
    public final /* synthetic */ dp5 f0;

    public pn5(dp5 dp5Var, Bundle bundle) {
        this.f0 = dp5Var;
        this.a = bundle;
    }

    @Override // java.lang.Runnable
    public final void run() {
        dp5 dp5Var = this.f0;
        Bundle bundle = this.a;
        dp5Var.e();
        dp5Var.g();
        zt2.j(bundle);
        String string = bundle.getString(PublicResolver.FUNC_NAME);
        String string2 = bundle.getString("origin");
        zt2.f(string);
        zt2.f(string2);
        zt2.j(bundle.get("value"));
        if (!dp5Var.a.h()) {
            dp5Var.a.w().v().a("Conditional property not set since app measurement is disabled");
            return;
        }
        zzkq zzkqVar = new zzkq(string, bundle.getLong("triggered_timestamp"), bundle.get("value"), string2);
        try {
            zzas J = dp5Var.a.G().J(bundle.getString("app_id"), bundle.getString("triggered_event_name"), bundle.getBundle("triggered_event_params"), string2, 0L, true, false);
            dp5Var.a.R().M(new zzaa(bundle.getString("app_id"), string2, zzkqVar, bundle.getLong("creation_timestamp"), false, bundle.getString("trigger_event_name"), dp5Var.a.G().J(bundle.getString("app_id"), bundle.getString("timed_out_event_name"), bundle.getBundle("timed_out_event_params"), string2, 0L, true, false), bundle.getLong("trigger_timeout"), J, bundle.getLong("time_to_live"), dp5Var.a.G().J(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), string2, 0L, true, false)));
        } catch (IllegalArgumentException unused) {
        }
    }
}
