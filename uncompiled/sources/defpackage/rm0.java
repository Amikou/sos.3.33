package defpackage;

import android.util.SparseArray;
import androidx.annotation.RecentlyNonNull;
import defpackage.yb1;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: rm0  reason: default package */
/* loaded from: classes.dex */
public abstract class rm0<T> {
    public final Object a = new Object();
    public b<T> b;

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: rm0$a */
    /* loaded from: classes.dex */
    public static class a<T> {
        public final SparseArray<T> a;

        public a(@RecentlyNonNull SparseArray<T> sparseArray, @RecentlyNonNull yb1.b bVar, boolean z) {
            this.a = sparseArray;
        }

        @RecentlyNonNull
        public SparseArray<T> a() {
            return this.a;
        }
    }

    /* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
    /* renamed from: rm0$b */
    /* loaded from: classes.dex */
    public interface b<T> {
        void a();

        void b(@RecentlyNonNull a<T> aVar);
    }

    @RecentlyNonNull
    public abstract SparseArray<T> a(@RecentlyNonNull yb1 yb1Var);

    public boolean b() {
        return true;
    }

    public void c(@RecentlyNonNull yb1 yb1Var) {
        yb1.b bVar = new yb1.b(yb1Var.c());
        bVar.i();
        a<T> aVar = new a<>(a(yb1Var), bVar, b());
        synchronized (this.a) {
            b<T> bVar2 = this.b;
            if (bVar2 != null) {
                bVar2.b(aVar);
            } else {
                throw new IllegalStateException("Detector processor must first be set with setProcessor in order to receive detection results.");
            }
        }
    }

    public void d() {
        synchronized (this.a) {
            b<T> bVar = this.b;
            if (bVar != null) {
                bVar.a();
                this.b = null;
            }
        }
    }

    public void e(@RecentlyNonNull b<T> bVar) {
        synchronized (this.a) {
            b<T> bVar2 = this.b;
            if (bVar2 != null) {
                bVar2.a();
            }
            this.b = bVar;
        }
    }
}
