package defpackage;

import org.bouncycastle.asn1.i;

/* renamed from: so0  reason: default package */
/* loaded from: classes2.dex */
public class so0 {
    public static qo0 a(i iVar) {
        if (iVar.equals(tc2.c)) {
            return new ka3();
        }
        if (iVar.equals(tc2.e)) {
            return new na3();
        }
        if (iVar.equals(tc2.g)) {
            return new pa3(128);
        }
        if (iVar.equals(tc2.h)) {
            return new pa3(256);
        }
        throw new IllegalArgumentException("unrecognized digest OID: " + iVar);
    }

    public static String b(i iVar) {
        if (iVar.equals(tc2.c)) {
            return "SHA256";
        }
        if (iVar.equals(tc2.e)) {
            return "SHA512";
        }
        if (iVar.equals(tc2.g)) {
            return "SHAKE128";
        }
        if (iVar.equals(tc2.h)) {
            return "SHAKE256";
        }
        throw new IllegalArgumentException("unrecognized digest OID: " + iVar);
    }
}
