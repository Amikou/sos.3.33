package defpackage;

import java.math.BigInteger;

/* compiled from: EthGetBlockTransactionCountByNumber.java */
/* renamed from: uw0  reason: default package */
/* loaded from: classes3.dex */
public class uw0 extends i83<String> {
    public BigInteger getTransactionCount() {
        return ej2.decodeQuantity(getResult());
    }
}
