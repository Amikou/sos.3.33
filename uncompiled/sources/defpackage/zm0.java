package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAlertNoTitleBinding.java */
/* renamed from: zm0  reason: default package */
/* loaded from: classes2.dex */
public final class zm0 {
    public final CardView a;
    public final MaterialButton b;
    public final TextView c;
    public final TextView d;

    public zm0(CardView cardView, MaterialButton materialButton, TextView textView, TextView textView2) {
        this.a = cardView;
        this.b = materialButton;
        this.c = textView;
        this.d = textView2;
    }

    public static zm0 a(View view) {
        int i = R.id.btnAction;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnAction);
        if (materialButton != null) {
            i = R.id.tvDialogContent;
            TextView textView = (TextView) ai4.a(view, R.id.tvDialogContent);
            if (textView != null) {
                i = R.id.tvDialogTitle;
                TextView textView2 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                if (textView2 != null) {
                    return new zm0((CardView) view, materialButton, textView, textView2);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
