package defpackage;

import android.content.Intent;
import android.view.View;
import net.safemoon.androidwallet.model.request.RequestTransaction;
import net.safemoon.androidwallet.utils.StoragePermissionLauncher;
import net.safemoon.androidwallet.viewmodels.wc.MultipleWalletConnect;
import net.safemoon.androidwallet.views.CustomKeyBoard;

/* compiled from: IHomeActivity.kt */
/* renamed from: gm1  reason: default package */
/* loaded from: classes2.dex */
public interface gm1 {
    hn2 a();

    void b();

    void c(RequestTransaction requestTransaction);

    StoragePermissionLauncher d();

    void h(int i);

    Intent j();

    View l();

    MultipleWalletConnect m();

    CustomKeyBoard n();

    void p();
}
