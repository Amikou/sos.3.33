package defpackage;

import android.content.Context;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.nio.charset.StandardCharsets;
import net.safemoon.androidwallet.MyApplicationClass;

/* compiled from: AKTEncUtil.java */
/* renamed from: w  reason: default package */
/* loaded from: classes2.dex */
public class w {
    public static String a(Context context, String str) {
        String c = jv0.c(context, "UK5");
        if (c.isEmpty()) {
            c = i2.p(MyApplicationClass.c().l0, "UK5");
        }
        byte[] a = ay1.a(q.d(q.f(c, bo3.i(context, "KA")), str));
        return a == null ? "" : new String(a, StandardCharsets.UTF_8);
    }

    public static String b(Context context, String str) {
        String c = jv0.c(context, "UK5");
        if (c.isEmpty()) {
            c = i2.p(MyApplicationClass.c().l0, "UK5");
        }
        String f = q.f(c, bo3.i(context, "KA"));
        String e = q.e(f, ay1.f(str.getBytes(StandardCharsets.UTF_8)));
        q.d(f, e);
        return e;
    }

    public static String c(Context context) {
        return d(context, bo3.i(context, "SAFEMOON_PRIVATE_KEY"));
    }

    public static String d(Context context, String str) {
        String c = jv0.c(context, "UK5");
        if (c.isEmpty()) {
            c = i2.p(MyApplicationClass.c().l0, "UK5");
        }
        String i = bo3.i(context, "KA");
        String i2 = i2.i(c, i, str);
        if (i2.length() != 64) {
            String c2 = jv0.c(context, "K5");
            if (c2.isEmpty()) {
                i2.p(MyApplicationClass.c().l0, "K5");
            }
            return i2.i(c2, i, str);
        }
        return i2;
    }

    public static String e(Context context, String str) {
        return i2.l(d(context, str));
    }

    public static String f(Context context, String str) {
        String c = jv0.c(context, "UK5");
        if (c.isEmpty()) {
            c = i2.p(MyApplicationClass.c().l0, "UK5");
        }
        String i = bo3.i(context, "KA");
        String j = i2.j(c, i, str);
        if (j.compareTo("error") == 0) {
            String c2 = jv0.c(context, "K5");
            if (c2.isEmpty()) {
                i2.p(MyApplicationClass.c().l0, "K5");
            }
            return i2.j(c2, i, str);
        }
        return j;
    }

    public static String g(Context context, String str) {
        if (str.isEmpty()) {
            return "";
        }
        String n = i2.n(str.trim().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "|"));
        String c = jv0.c(context, "UK5");
        if (c.isEmpty()) {
            c = i2.p(MyApplicationClass.c().l0, "UK5");
        }
        String i = bo3.i(context, "KA");
        String j = i2.j(c, i, n);
        if (j.compareTo("error") == 0) {
            String c2 = jv0.c(context, "K5");
            if (c2.isEmpty()) {
                i2.p(MyApplicationClass.c().l0, "K5");
            }
            return i2.j(c2, i, n);
        }
        return j;
    }
}
