package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSwapTransactionBinding.java */
/* renamed from: go0  reason: default package */
/* loaded from: classes2.dex */
public final class go0 {
    public final CardView a;
    public final TextView b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final TextView e;

    public go0(CardView cardView, TextView textView, MaterialButton materialButton, MaterialButton materialButton2, ImageView imageView, ConstraintLayout constraintLayout, TextView textView2, TextView textView3, RelativeLayout relativeLayout) {
        this.a = cardView;
        this.b = textView;
        this.c = materialButton;
        this.d = materialButton2;
        this.e = textView3;
    }

    public static go0 a(View view) {
        int i = R.id.btnCopy;
        TextView textView = (TextView) ai4.a(view, R.id.btnCopy);
        if (textView != null) {
            i = R.id.btnTransaction;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnTransaction);
            if (materialButton != null) {
                i = R.id.dialog_cross;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.dialog_cross);
                if (materialButton2 != null) {
                    i = R.id.imgAlert;
                    ImageView imageView = (ImageView) ai4.a(view, R.id.imgAlert);
                    if (imageView != null) {
                        i = R.id.parentAlert;
                        ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.parentAlert);
                        if (constraintLayout != null) {
                            i = R.id.tvDialogContent;
                            TextView textView2 = (TextView) ai4.a(view, R.id.tvDialogContent);
                            if (textView2 != null) {
                                i = R.id.tvDialogTitle;
                                TextView textView3 = (TextView) ai4.a(view, R.id.tvDialogTitle);
                                if (textView3 != null) {
                                    i = R.id.vDialogContentContainer;
                                    RelativeLayout relativeLayout = (RelativeLayout) ai4.a(view, R.id.vDialogContentContainer);
                                    if (relativeLayout != null) {
                                        return new go0((CardView) view, textView, materialButton, materialButton2, imageView, constraintLayout, textView2, textView3, relativeLayout);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
