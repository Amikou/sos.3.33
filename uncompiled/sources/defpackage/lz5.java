package defpackage;

import java.util.ListIterator;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: lz5  reason: default package */
/* loaded from: classes.dex */
public final class lz5 implements ListIterator<String> {
    public final ListIterator<String> a;
    public final /* synthetic */ int f0;
    public final /* synthetic */ nz5 g0;

    public lz5(nz5 nz5Var, int i) {
        nw5 nw5Var;
        this.g0 = nz5Var;
        this.f0 = i;
        nw5Var = nz5Var.a;
        this.a = nw5Var.listIterator(i);
    }

    @Override // java.util.ListIterator
    public final /* bridge */ /* synthetic */ void add(String str) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.ListIterator
    public final boolean hasPrevious() {
        return this.a.hasPrevious();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final /* bridge */ /* synthetic */ Object next() {
        return this.a.next();
    }

    @Override // java.util.ListIterator
    public final int nextIndex() {
        return this.a.nextIndex();
    }

    @Override // java.util.ListIterator
    public final /* bridge */ /* synthetic */ String previous() {
        return this.a.previous();
    }

    @Override // java.util.ListIterator
    public final int previousIndex() {
        return this.a.previousIndex();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator
    public final /* bridge */ /* synthetic */ void set(String str) {
        throw new UnsupportedOperationException();
    }
}
