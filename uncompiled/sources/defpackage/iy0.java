package defpackage;

/* compiled from: EventStoreModule_StoreConfigFactory.java */
/* renamed from: iy0  reason: default package */
/* loaded from: classes.dex */
public final class iy0 implements z11<ey0> {

    /* compiled from: EventStoreModule_StoreConfigFactory.java */
    /* renamed from: iy0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final iy0 a = new iy0();
    }

    public static iy0 a() {
        return a.a;
    }

    public static ey0 c() {
        return (ey0) yt2.c(fy0.c(), "Cannot return null from a non-@Nullable @Provides method");
    }

    @Override // defpackage.ew2
    /* renamed from: b */
    public ey0 get() {
        return c();
    }
}
