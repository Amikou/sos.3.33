package defpackage;

/* compiled from: Observer.java */
/* renamed from: tl2  reason: default package */
/* loaded from: classes.dex */
public interface tl2<T> {
    void onChanged(T t);
}
