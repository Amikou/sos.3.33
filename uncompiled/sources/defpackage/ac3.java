package defpackage;

import net.safemoon.androidwallet.model.DeviceModel;
import net.safemoon.androidwallet.model.FCMNotification;
import net.safemoon.androidwallet.model.NotificationRead;
import net.safemoon.androidwallet.model.SingleFCMRegister;
import net.safemoon.androidwallet.model.UpdateFCMToken;
import net.safemoon.androidwallet.model.notificationHistory.NotificationDeleteRequest;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationMarkReadRequest;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertToken;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenData;
import net.safemoon.androidwallet.model.priceAlert.PriceAlertTokenListData;
import net.safemoon.androidwallet.model.request.RequestTransaction;
import net.safemoon.androidwallet.model.transaction.details.TransactionDetails;
import okhttp3.ResponseBody;
import retrofit2.b;

/* compiled from: SafemoonAPIInterface.java */
/* renamed from: ac3  reason: default package */
/* loaded from: classes2.dex */
public interface ac3 {
    @ge1("/api/notification/{address}/list")
    b<NotificationHistory> a(@ek1("lang") String str, @vp2("address") String str2, @yw2("limit") Integer num, @yw2("offset") Integer num2);

    @ge1("/api/v2/bsc/transaction/{transactionHash}")
    b<TransactionDetails> b(@vp2("transactionHash") String str);

    @oo2("/api/v2/device")
    b<DeviceModel> c(@ar SingleFCMRegister singleFCMRegister);

    @jj1(hasBody = true, method = "DELETE", path = "/api/notification/price-alert/delete-all")
    b<PriceAlertTokenData> d(@ar PriceAlertToken priceAlertToken);

    @oo2("/api/v2/device/update")
    b<DeviceModel> e(@ar FCMNotification fCMNotification);

    @ge1("/api/notification/price-alert")
    b<PriceAlertTokenListData> f(@yw2("walletAddress") String str, @yw2("tokenAddress") String str2, @yw2("tokenSymbol") String str3, @yw2("fcmToken") String str4);

    @jj1(hasBody = true, method = "DELETE", path = "/api/notification/price-alert/delete")
    b<PriceAlertTokenData> g(@ar PriceAlertToken priceAlertToken);

    @qo2("/api/notification/price-alert/update")
    b<PriceAlertTokenData> h(@ar PriceAlertToken priceAlertToken);

    @qo2("/api/notification/{notificationId}/read")
    b<NotificationRead> i(@vp2("notificationId") String str);

    @oo2("/api/notification/price-alert/create")
    b<PriceAlertTokenData> j(@ar PriceAlertToken priceAlertToken);

    @qo2("/api/notification/read-notifications")
    b<ResponseBody> k(@ar NotificationMarkReadRequest notificationMarkReadRequest);

    @qo2("/api/notification/delete-notifications")
    b<ResponseBody> l(@ar NotificationDeleteRequest notificationDeleteRequest);

    @oo2("/api/v5/bsc/transaction")
    b<Object> m(@ar RequestTransaction requestTransaction);

    @oo2("/api/v2/device/update-fcmtoken")
    b<DeviceModel> n(@ar UpdateFCMToken updateFCMToken);
}
