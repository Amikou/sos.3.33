package defpackage;

/* compiled from: ECP.java */
/* renamed from: nt0  reason: default package */
/* loaded from: classes2.dex */
public final class nt0 {
    public x11 a;
    public x11 b;
    public x11 c;

    public nt0() {
        this.a = new x11();
        this.b = new x11(1);
        this.c = new x11();
    }

    public static x11 a(x11 x11Var) {
        x11 x11Var2 = new x11(x11Var);
        x11Var2.x();
        x11 x11Var3 = new x11(new zl(f33.d));
        x11Var2.k(x11Var);
        x11Var2.a(x11Var3);
        x11Var2.u();
        return x11Var2;
    }

    public static nt0 h() {
        return new nt0(new zl(f33.f), new zl(f33.g));
    }

    public static int p(int i, int i2) {
        return (((i ^ i2) - 1) >> 31) & 1;
    }

    public void b(nt0 nt0Var) {
        x11 x11Var = new x11(this.a);
        x11Var.k(nt0Var.a);
        x11 x11Var2 = new x11(this.b);
        x11Var2.k(nt0Var.b);
        x11 x11Var3 = new x11(this.c);
        x11Var3.k(nt0Var.c);
        x11 x11Var4 = new x11(this.a);
        x11Var4.a(this.b);
        x11Var4.m();
        x11 x11Var5 = new x11(nt0Var.a);
        x11Var5.a(nt0Var.b);
        x11Var5.m();
        x11Var4.k(x11Var5);
        x11Var5.c(x11Var);
        x11Var5.a(x11Var2);
        x11Var4.z(x11Var5);
        x11Var4.m();
        x11Var5.c(this.b);
        x11Var5.a(this.c);
        x11Var5.m();
        x11 x11Var6 = new x11(nt0Var.b);
        x11Var6.a(nt0Var.c);
        x11Var6.m();
        x11Var5.k(x11Var6);
        x11Var6.c(x11Var2);
        x11Var6.a(x11Var3);
        x11Var5.z(x11Var6);
        x11Var5.m();
        x11Var6.c(this.a);
        x11Var6.a(this.c);
        x11Var6.m();
        x11 x11Var7 = new x11(nt0Var.a);
        x11Var7.a(nt0Var.c);
        x11Var7.m();
        x11Var6.k(x11Var7);
        x11Var7.c(x11Var);
        x11Var7.a(x11Var3);
        x11Var7.v(x11Var6);
        x11Var7.m();
        x11Var6.c(x11Var);
        x11Var6.a(x11Var);
        x11Var.a(x11Var6);
        x11Var.m();
        x11Var3.e(21);
        x11 x11Var8 = new x11(x11Var2);
        x11Var8.a(x11Var3);
        x11Var8.m();
        x11Var2.z(x11Var3);
        x11Var2.m();
        x11Var7.e(21);
        x11Var6.c(x11Var7);
        x11Var6.k(x11Var5);
        x11Var3.c(x11Var4);
        x11Var3.k(x11Var2);
        x11Var6.v(x11Var3);
        x11Var7.k(x11Var);
        x11Var2.k(x11Var8);
        x11Var7.a(x11Var2);
        x11Var.k(x11Var4);
        x11Var8.k(x11Var5);
        x11Var8.a(x11Var);
        this.a.c(x11Var6);
        this.a.m();
        this.b.c(x11Var7);
        this.b.m();
        this.c.c(x11Var8);
        this.c.m();
    }

    public void c() {
        if (l()) {
            return;
        }
        x11 x11Var = new x11(1);
        if (this.c.d(x11Var)) {
            return;
        }
        this.c.f(null);
        this.a.k(this.c);
        this.a.u();
        this.b.k(this.c);
        this.b.u();
        this.c.c(x11Var);
    }

    public nt0 d(zl zlVar, zl zlVar2) {
        if (!zlVar.n() && !l()) {
            nt0 nt0Var = new nt0();
            zl zlVar3 = new zl(zlVar);
            zlVar3.x(zlVar2);
            int u = zlVar3.u();
            zl zlVar4 = new zl();
            zl zlVar5 = new zl();
            nt0 nt0Var2 = new nt0();
            nt0 nt0Var3 = new nt0();
            nt0[] nt0VarArr = new nt0[8];
            byte[] bArr = new byte[71];
            nt0Var2.f(this);
            nt0Var2.g();
            nt0VarArr[0] = new nt0();
            nt0VarArr[0].f(this);
            for (int i = 1; i < 8; i++) {
                nt0VarArr[i] = new nt0();
                nt0VarArr[i].f(nt0VarArr[i - 1]);
                nt0VarArr[i].b(nt0Var2);
            }
            zlVar5.e(zlVar);
            int y = zlVar5.y();
            zlVar5.l(1);
            zlVar5.v();
            int y2 = zlVar5.y();
            zlVar4.e(zlVar5);
            zlVar4.l(1);
            zlVar4.v();
            zlVar5.c(zlVar4, y);
            nt0Var2.e(this, y2);
            nt0Var3.f(nt0Var2);
            int i2 = ((u + 3) / 4) + 1;
            for (int i3 = 0; i3 < i2; i3++) {
                bArr[i3] = (byte) (zlVar5.o(5) - 16);
                zlVar5.g(bArr[i3]);
                zlVar5.v();
                zlVar5.k(4);
            }
            bArr[i2] = (byte) zlVar5.o(5);
            nt0Var.n(nt0VarArr, bArr[i2]);
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                nt0Var2.n(nt0VarArr, bArr[i4]);
                nt0Var.g();
                nt0Var.g();
                nt0Var.g();
                nt0Var.g();
                nt0Var.b(nt0Var2);
            }
            nt0Var.o(nt0Var3);
            return nt0Var;
        }
        return new nt0();
    }

    public final void e(nt0 nt0Var, int i) {
        this.a.b(nt0Var.a, i);
        this.b.b(nt0Var.b, i);
        this.c.b(nt0Var.c, i);
    }

    public void f(nt0 nt0Var) {
        this.a.c(nt0Var.a);
        this.b.c(nt0Var.b);
        this.c.c(nt0Var.c);
    }

    public void g() {
        x11 x11Var = new x11(this.b);
        x11Var.x();
        x11 x11Var2 = new x11(this.b);
        x11Var2.k(this.c);
        x11 x11Var3 = new x11(this.c);
        x11Var3.x();
        this.c.c(x11Var);
        this.c.a(x11Var);
        this.c.m();
        x11 x11Var4 = this.c;
        x11Var4.a(x11Var4);
        x11 x11Var5 = this.c;
        x11Var5.a(x11Var5);
        this.c.m();
        x11Var3.e(21);
        x11 x11Var6 = new x11(x11Var3);
        x11Var6.k(this.c);
        x11 x11Var7 = new x11(x11Var);
        x11Var7.a(x11Var3);
        x11Var7.m();
        this.c.k(x11Var2);
        x11Var2.c(x11Var3);
        x11Var2.a(x11Var3);
        x11Var3.a(x11Var2);
        x11Var.z(x11Var3);
        x11Var.m();
        x11Var7.k(x11Var);
        x11Var7.a(x11Var6);
        x11Var2.c(this.a);
        x11Var2.k(this.b);
        this.a.c(x11Var);
        this.a.m();
        this.a.k(x11Var2);
        x11 x11Var8 = this.a;
        x11Var8.a(x11Var8);
        this.a.m();
        this.b.c(x11Var7);
        this.b.m();
    }

    public zl i() {
        nt0 nt0Var = new nt0(this);
        nt0Var.c();
        return nt0Var.a.t();
    }

    public zl j() {
        nt0 nt0Var = new nt0(this);
        nt0Var.c();
        return nt0Var.b.t();
    }

    public void k() {
        this.a.A();
        this.b.o();
        this.c.A();
    }

    public boolean l() {
        return this.a.h() && this.c.h();
    }

    public void m() {
        this.b.l();
        this.b.m();
    }

    public final void n(nt0[] nt0VarArr, int i) {
        nt0 nt0Var = new nt0();
        int i2 = i >> 31;
        int i3 = (((i ^ i2) - i2) - 1) / 2;
        e(nt0VarArr[0], p(i3, 0));
        e(nt0VarArr[1], p(i3, 1));
        e(nt0VarArr[2], p(i3, 2));
        e(nt0VarArr[3], p(i3, 3));
        e(nt0VarArr[4], p(i3, 4));
        e(nt0VarArr[5], p(i3, 5));
        e(nt0VarArr[6], p(i3, 6));
        e(nt0VarArr[7], p(i3, 7));
        nt0Var.f(this);
        nt0Var.m();
        e(nt0Var, i2 & 1);
    }

    public void o(nt0 nt0Var) {
        nt0 nt0Var2 = new nt0(nt0Var);
        nt0Var2.m();
        b(nt0Var2);
    }

    public String toString() {
        nt0 nt0Var = new nt0(this);
        nt0Var.c();
        if (nt0Var.l()) {
            return "infinity";
        }
        return "(" + nt0Var.a.t().toString() + "," + nt0Var.b.t().toString() + ")";
    }

    public nt0(nt0 nt0Var) {
        this.a = new x11(nt0Var.a);
        this.b = new x11(nt0Var.b);
        this.c = new x11(nt0Var.c);
    }

    public nt0(zl zlVar, zl zlVar2) {
        this.a = new x11(zlVar);
        this.b = new x11(zlVar2);
        this.c = new x11(1);
        this.a.m();
        x11 a = a(this.a);
        x11 x11Var = new x11(this.b);
        x11Var.x();
        if (x11Var.d(a)) {
            return;
        }
        k();
    }

    public nt0(zl zlVar) {
        x11 x11Var = new x11(zlVar);
        this.a = x11Var;
        x11Var.m();
        x11 a = a(this.a);
        x11 x11Var2 = new x11();
        this.b = new x11();
        this.c = new x11(1);
        if (a.r(x11Var2) == 1) {
            this.b.c(a.y(x11Var2));
        } else {
            k();
        }
    }
}
