package defpackage;

import java.util.concurrent.Executor;

/* renamed from: bx4  reason: default package */
/* loaded from: classes2.dex */
public final class bx4<ResultT> implements px4<ResultT> {
    public final Executor a;
    public final Object b = new Object();
    public final mm2 c;

    public bx4(Executor executor, mm2 mm2Var) {
        this.a = executor;
        this.c = mm2Var;
    }

    @Override // defpackage.px4
    public final void a(l34<ResultT> l34Var) {
        if (l34Var.f()) {
            return;
        }
        synchronized (this.b) {
            if (this.c == null) {
                return;
            }
            this.a.execute(new aw4(this, l34Var));
        }
    }
}
