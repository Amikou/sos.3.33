package defpackage;

import android.content.Context;
import android.os.Build;
import android.os.Process;

/* compiled from: AndroidSupportV4Compat.java */
/* renamed from: ed  reason: default package */
/* loaded from: classes2.dex */
public class ed {
    public static int a(Context context, String str) {
        try {
            return context.checkPermission(str, Process.myPid(), Process.myUid());
        } catch (Throwable unused) {
            return -1;
        }
    }

    public static int b(Context context, int i) {
        if (Build.VERSION.SDK_INT > 22) {
            return context.getColor(i);
        }
        return context.getResources().getColor(i);
    }
}
