package defpackage;

import com.google.android.gms.internal.vision.h0;
import com.google.android.gms.internal.vision.n0;
import com.google.android.gms.internal.vision.zzht;
import com.google.android.gms.internal.vision.zzjk;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: au5  reason: default package */
/* loaded from: classes.dex */
public class au5 {
    public volatile n0 a;
    public volatile zzht b;

    static {
        h0.b();
    }

    public final n0 a(n0 n0Var) {
        n0 n0Var2 = this.a;
        this.b = null;
        this.a = n0Var;
        return n0Var2;
    }

    public final int b() {
        if (this.b != null) {
            return this.b.zza();
        }
        if (this.a != null) {
            return this.a.k();
        }
        return 0;
    }

    public final n0 c(n0 n0Var) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    try {
                        this.a = n0Var;
                        this.b = zzht.zza;
                    } catch (zzjk unused) {
                        this.a = n0Var;
                        this.b = zzht.zza;
                    }
                }
            }
        }
        return this.a;
    }

    public final zzht d() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                return this.b;
            }
            if (this.a == null) {
                this.b = zzht.zza;
            } else {
                this.b = this.a.h();
            }
            return this.b;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof au5) {
            au5 au5Var = (au5) obj;
            n0 n0Var = this.a;
            n0 n0Var2 = au5Var.a;
            if (n0Var == null && n0Var2 == null) {
                return d().equals(au5Var.d());
            }
            if (n0Var == null || n0Var2 == null) {
                if (n0Var != null) {
                    return n0Var.equals(au5Var.c(n0Var.e()));
                }
                return c(n0Var2.e()).equals(n0Var2);
            }
            return n0Var.equals(n0Var2);
        }
        return false;
    }

    public int hashCode() {
        return 1;
    }
}
