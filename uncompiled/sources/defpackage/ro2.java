package defpackage;

/* renamed from: ro2  reason: default package */
/* loaded from: classes2.dex */
public abstract class ro2 {
    public static int a(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        return (bArr[i3 + 1] & 255) | (bArr[i] << 24) | ((bArr[i2] & 255) << 16) | ((bArr[i3] & 255) << 8);
    }

    public static long b(byte[] bArr, int i) {
        int a = a(bArr, i);
        return (a(bArr, i + 4) & 4294967295L) | ((a & 4294967295L) << 32);
    }

    public static void c(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >>> 24);
        int i3 = i2 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >>> 8);
        bArr[i4 + 1] = (byte) i;
    }

    public static void d(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) i;
        int i3 = i2 + 1;
        bArr[i3] = (byte) (i >>> 8);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >>> 16);
        bArr[i4 + 1] = (byte) (i >>> 24);
    }

    public static void e(int[] iArr, byte[] bArr, int i) {
        for (int i2 : iArr) {
            d(i2, bArr, i);
            i += 4;
        }
    }

    public static int f(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        return (bArr[i3 + 1] << 24) | (bArr[i] & 255) | ((bArr[i2] & 255) << 8) | ((bArr[i3] & 255) << 16);
    }

    public static void g(byte[] bArr, int i, int[] iArr) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = f(bArr, i);
            i += 4;
        }
    }

    public static int[] h(byte[] bArr, int i, int i2) {
        int[] iArr = new int[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            iArr[i3] = f(bArr, i);
            i += 4;
        }
        return iArr;
    }

    public static long i(byte[] bArr, int i) {
        return ((f(bArr, i + 4) & 4294967295L) << 32) | (f(bArr, i) & 4294967295L);
    }

    public static short j(byte[] bArr, int i) {
        return (short) (((bArr[i + 1] & 255) << 8) | (bArr[i] & 255));
    }

    public static void k(long j, byte[] bArr, int i) {
        c((int) (j >>> 32), bArr, i);
        c((int) (j & 4294967295L), bArr, i + 4);
    }

    public static void l(long j, byte[] bArr, int i) {
        d((int) (4294967295L & j), bArr, i);
        d((int) (j >>> 32), bArr, i + 4);
    }

    public static void m(long[] jArr, int i, int i2, byte[] bArr, int i3) {
        for (int i4 = 0; i4 < i2; i4++) {
            l(jArr[i + i4], bArr, i3);
            i3 += 8;
        }
    }

    public static byte[] n(long j) {
        byte[] bArr = new byte[8];
        l(j, bArr, 0);
        return bArr;
    }

    public static void o(short s, byte[] bArr, int i) {
        bArr[i] = (byte) s;
        bArr[i + 1] = (byte) (s >>> 8);
    }
}
