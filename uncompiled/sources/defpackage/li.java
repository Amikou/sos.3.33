package defpackage;

import android.os.Bundle;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.play.core.assetpacks.AssetPackState;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: li  reason: default package */
/* loaded from: classes2.dex */
public abstract class li {
    public static li a(long j, Map<String, AssetPackState> map) {
        return new lu4(j, map);
    }

    public static li b(Bundle bundle, fv4 fv4Var) {
        return c(bundle, fv4Var, new ArrayList());
    }

    public static li c(Bundle bundle, fv4 fv4Var, List<String> list) {
        return d(bundle, fv4Var, list, du4.b);
    }

    public static li d(Bundle bundle, fv4 fv4Var, List<String> list, bu4 bu4Var) {
        ArrayList<String> stringArrayList = bundle.getStringArrayList("pack_names");
        HashMap hashMap = new HashMap();
        int size = stringArrayList.size();
        for (int i = 0; i < size; i++) {
            String str = stringArrayList.get(i);
            hashMap.put(str, AssetPackState.d(bundle, str, fv4Var, bu4Var));
        }
        for (String str2 : list) {
            hashMap.put(str2, AssetPackState.b(str2, 4, 0, 0L, 0L, Utils.DOUBLE_EPSILON, 1));
        }
        return a(bundle.getLong("total_bytes_to_download"), hashMap);
    }

    public abstract Map<String, AssetPackState> e();

    public abstract long f();
}
