package defpackage;

import android.text.InputFilter;
import android.text.Spanned;

/* compiled from: InputFilterMinMax.java */
/* renamed from: xq1  reason: default package */
/* loaded from: classes2.dex */
public class xq1 implements InputFilter {
    public Double a;
    public Double b;

    public xq1(Double d, Double d2) {
        this.a = d;
        this.b = d2;
    }

    public final boolean a(Double d, Double d2, Double d3) {
        int i = (d2.doubleValue() > d.doubleValue() ? 1 : (d2.doubleValue() == d.doubleValue() ? 0 : -1));
        double doubleValue = d3.doubleValue();
        if (i > 0) {
            if (doubleValue >= d.doubleValue() && d3.doubleValue() <= d2.doubleValue()) {
                return true;
            }
        } else if (doubleValue >= d2.doubleValue() && d3.doubleValue() <= d.doubleValue()) {
            return true;
        }
        return false;
    }

    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        String str;
        try {
            str = spanned.toString() + charSequence.toString();
        } catch (NumberFormatException unused) {
        }
        if (e30.j0(str)) {
            if (a(this.a, this.b, Double.valueOf(e30.K(str)))) {
                return null;
            }
            return "";
        }
        return "";
    }

    public xq1(String str, String str2) {
        this.a = Double.valueOf(e30.K(str));
        this.b = Double.valueOf(e30.K(str2));
    }
}
