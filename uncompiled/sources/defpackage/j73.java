package defpackage;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;

/* compiled from: RequestListener.java */
/* renamed from: j73  reason: default package */
/* loaded from: classes.dex */
public interface j73<R> {
    boolean i(GlideException glideException, Object obj, i34<R> i34Var, boolean z);

    boolean n(R r, Object obj, i34<R> i34Var, DataSource dataSource, boolean z);
}
