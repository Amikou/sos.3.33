package defpackage;

/* compiled from: OptionalLong.java */
/* renamed from: pn2  reason: default package */
/* loaded from: classes2.dex */
public final class pn2 {
    public static final pn2 c = new pn2();
    public final boolean a;
    public final long b;

    /* compiled from: OptionalLong.java */
    /* renamed from: pn2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static final pn2[] a = new pn2[256];

        static {
            int i = 0;
            while (true) {
                pn2[] pn2VarArr = a;
                if (i >= pn2VarArr.length) {
                    return;
                }
                pn2VarArr[i] = new pn2(i - 128);
                i++;
            }
        }
    }

    public pn2() {
        this.a = false;
        this.b = 0L;
    }

    public static pn2 a() {
        return c;
    }

    public static pn2 c(long j) {
        if (j >= -128 && j <= 127) {
            return a.a[((int) j) + 128];
        }
        return new pn2(j);
    }

    public boolean b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof pn2) {
            pn2 pn2Var = (pn2) obj;
            boolean z = this.a;
            if (z && pn2Var.a) {
                if (this.b == pn2Var.b) {
                    return true;
                }
            } else if (z == pn2Var.a) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        if (this.a) {
            return k22.a(this.b);
        }
        return 0;
    }

    public String toString() {
        return this.a ? String.format("OptionalLong[%s]", Long.valueOf(this.b)) : "OptionalLong.empty";
    }

    public pn2(long j) {
        this.a = true;
        this.b = j;
    }
}
