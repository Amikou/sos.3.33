package defpackage;

import defpackage.pt0;
import defpackage.xs0;
import java.math.BigInteger;

/* renamed from: q64  reason: default package */
/* loaded from: classes2.dex */
public class q64 {
    public static final BigInteger a;
    public static final BigInteger b;
    public static final BigInteger c;
    public static final qs4[] d;
    public static final byte[][] e;
    public static final qs4[] f;
    public static final byte[][] g;

    static {
        BigInteger bigInteger = ws0.b;
        BigInteger negate = bigInteger.negate();
        a = negate;
        b = ws0.c.negate();
        BigInteger negate2 = ws0.d.negate();
        c = negate2;
        BigInteger bigInteger2 = ws0.a;
        d = new qs4[]{null, new qs4(bigInteger, bigInteger2), null, new qs4(negate2, negate), null, new qs4(negate, negate), null, new qs4(bigInteger, negate), null};
        e = new byte[][]{null, new byte[]{1}, null, new byte[]{-1, 0, 1}, null, new byte[]{1, 0, 1}, null, new byte[]{-1, 0, 0, 1}};
        f = new qs4[]{null, new qs4(bigInteger, bigInteger2), null, new qs4(negate2, bigInteger), null, new qs4(negate, bigInteger), null, new qs4(bigInteger, bigInteger), null};
        g = new byte[][]{null, new byte[]{1}, null, new byte[]{-1, 0, 1}, null, new byte[]{1, 0, 1}, null, new byte[]{-1, 0, 0, -1}};
    }

    public static xo3 a(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, byte b2, int i, int i2) {
        int i3 = ((i + 5) / 2) + i2;
        BigInteger multiply = bigInteger2.multiply(bigInteger.shiftRight(((i - i3) - 2) + b2));
        BigInteger add = multiply.add(bigInteger3.multiply(multiply.shiftRight(i)));
        int i4 = i3 - i2;
        BigInteger shiftRight = add.shiftRight(i4);
        if (add.testBit(i4 - 1)) {
            shiftRight = shiftRight.add(ws0.b);
        }
        return new xo3(shiftRight, i2);
    }

    public static BigInteger[] b(byte b2, int i, boolean z) {
        BigInteger bigInteger;
        BigInteger bigInteger2;
        if (b2 == 1 || b2 == -1) {
            if (z) {
                bigInteger = ws0.c;
                bigInteger2 = BigInteger.valueOf(b2);
            } else {
                bigInteger = ws0.a;
                bigInteger2 = ws0.b;
            }
            int i2 = 1;
            while (i2 < i) {
                i2++;
                BigInteger bigInteger3 = bigInteger2;
                bigInteger2 = (b2 == 1 ? bigInteger2 : bigInteger2.negate()).subtract(bigInteger.shiftLeft(1));
                bigInteger = bigInteger3;
            }
            return new BigInteger[]{bigInteger, bigInteger2};
        }
        throw new IllegalArgumentException("mu must be 1 or -1");
    }

    public static byte c(int i) {
        return (byte) (i == 0 ? -1 : 1);
    }

    public static pt0.b[] d(pt0.b bVar, byte b2) {
        byte[][] bArr = b2 == 0 ? e : g;
        pt0.b[] bVarArr = new pt0.b[(bArr.length + 1) >>> 1];
        bVarArr[0] = bVar;
        int length = bArr.length;
        for (int i = 3; i < length; i += 2) {
            bVarArr[i >>> 1] = h(bVar, bArr[i]);
        }
        bVar.i().A(bVarArr);
        return bVarArr;
    }

    public static int e(BigInteger bigInteger) {
        if (bigInteger != null) {
            if (bigInteger.equals(ws0.c)) {
                return 1;
            }
            if (bigInteger.equals(ws0.e)) {
                return 2;
            }
        }
        throw new IllegalArgumentException("h (Cofactor) must be 2 or 4");
    }

    public static BigInteger[] f(xs0.b bVar) {
        if (bVar.H()) {
            int u = bVar.u();
            int intValue = bVar.o().t().intValue();
            byte c2 = c(intValue);
            int e2 = e(bVar.q());
            BigInteger[] b2 = b(c2, (u + 3) - intValue, false);
            if (c2 == 1) {
                b2[0] = b2[0].negate();
                b2[1] = b2[1].negate();
            }
            BigInteger bigInteger = ws0.b;
            return new BigInteger[]{bigInteger.add(b2[1]).shiftRight(e2), bigInteger.add(b2[0]).shiftRight(e2).negate()};
        }
        throw new IllegalArgumentException("si is defined for Koblitz curves only");
    }

    public static BigInteger g(byte b2, int i) {
        if (i == 4) {
            return b2 == 1 ? BigInteger.valueOf(6L) : BigInteger.valueOf(10L);
        }
        BigInteger[] b3 = b(b2, i, false);
        BigInteger bit = ws0.a.setBit(i);
        return ws0.c.multiply(b3[0]).multiply(b3[1].modInverse(bit)).mod(bit);
    }

    public static pt0.b h(pt0.b bVar, byte[] bArr) {
        pt0.b bVar2 = (pt0.b) bVar.i().v();
        pt0.b bVar3 = (pt0.b) bVar.z();
        int i = 0;
        for (int length = bArr.length - 1; length >= 0; length--) {
            i++;
            byte b2 = bArr[length];
            if (b2 != 0) {
                bVar2 = (pt0.b) bVar2.L(i).a(b2 > 0 ? bVar : bVar3);
                i = 0;
            }
        }
        return i > 0 ? bVar2.L(i) : bVar2;
    }

    public static BigInteger i(byte b2, qs4 qs4Var) {
        BigInteger subtract;
        BigInteger bigInteger = qs4Var.a;
        BigInteger multiply = bigInteger.multiply(bigInteger);
        BigInteger multiply2 = qs4Var.a.multiply(qs4Var.b);
        BigInteger bigInteger2 = qs4Var.b;
        BigInteger shiftLeft = bigInteger2.multiply(bigInteger2).shiftLeft(1);
        if (b2 == 1) {
            subtract = multiply.add(multiply2);
        } else if (b2 != -1) {
            throw new IllegalArgumentException("mu must be 1 or -1");
        } else {
            subtract = multiply.subtract(multiply2);
        }
        return subtract.add(shiftLeft);
    }

    public static qs4 j(BigInteger bigInteger, int i, byte b2, BigInteger[] bigIntegerArr, byte b3, byte b4) {
        BigInteger add = b3 == 1 ? bigIntegerArr[0].add(bigIntegerArr[1]) : bigIntegerArr[0].subtract(bigIntegerArr[1]);
        BigInteger bigInteger2 = b(b3, i, true)[1];
        qs4 k = k(a(bigInteger, bigIntegerArr[0], bigInteger2, b2, i, b4), a(bigInteger, bigIntegerArr[1], bigInteger2, b2, i, b4), b3);
        return new qs4(bigInteger.subtract(add.multiply(k.a)).subtract(BigInteger.valueOf(2L).multiply(bigIntegerArr[1]).multiply(k.b)), bigIntegerArr[1].multiply(k.a).subtract(bigIntegerArr[0].multiply(k.b)));
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0066, code lost:
        if (r5.d(defpackage.q64.a) < 0) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x0081, code lost:
        if (r5.d(r9) >= 0) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x008a, code lost:
        if (r8.d(defpackage.q64.b) < 0) goto L25;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.qs4 k(defpackage.xo3 r8, defpackage.xo3 r9, byte r10) {
        /*
            int r0 = r8.f()
            int r1 = r9.f()
            if (r1 != r0) goto La7
            r0 = -1
            r1 = 1
            if (r10 == r1) goto L19
            if (r10 != r0) goto L11
            goto L19
        L11:
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            java.lang.String r9 = "mu must be 1 or -1"
            r8.<init>(r9)
            throw r8
        L19:
            java.math.BigInteger r2 = r8.h()
            java.math.BigInteger r3 = r9.h()
            xo3 r8 = r8.j(r2)
            xo3 r9 = r9.j(r3)
            xo3 r4 = r8.a(r8)
            if (r10 != r1) goto L34
            xo3 r4 = r4.a(r9)
            goto L38
        L34:
            xo3 r4 = r4.i(r9)
        L38:
            xo3 r5 = r9.a(r9)
            xo3 r5 = r5.a(r9)
            xo3 r9 = r5.a(r9)
            if (r10 != r1) goto L4f
            xo3 r5 = r8.i(r5)
            xo3 r8 = r8.a(r9)
            goto L57
        L4f:
            xo3 r5 = r8.a(r5)
            xo3 r8 = r8.i(r9)
        L57:
            java.math.BigInteger r9 = defpackage.ws0.b
            int r6 = r4.d(r9)
            r7 = 0
            if (r6 < 0) goto L69
            java.math.BigInteger r6 = defpackage.q64.a
            int r6 = r5.d(r6)
            if (r6 >= 0) goto L75
            goto L71
        L69:
            java.math.BigInteger r1 = defpackage.ws0.c
            int r1 = r8.d(r1)
            if (r1 < 0) goto L74
        L71:
            r1 = r7
            r7 = r10
            goto L75
        L74:
            r1 = r7
        L75:
            java.math.BigInteger r6 = defpackage.q64.a
            int r4 = r4.d(r6)
            if (r4 >= 0) goto L84
            int r8 = r5.d(r9)
            if (r8 < 0) goto L8f
            goto L8c
        L84:
            java.math.BigInteger r9 = defpackage.q64.b
            int r8 = r8.d(r9)
            if (r8 >= 0) goto L8e
        L8c:
            int r8 = -r10
            byte r7 = (byte) r8
        L8e:
            r0 = r1
        L8f:
            long r8 = (long) r0
            java.math.BigInteger r8 = java.math.BigInteger.valueOf(r8)
            java.math.BigInteger r8 = r2.add(r8)
            long r9 = (long) r7
            java.math.BigInteger r9 = java.math.BigInteger.valueOf(r9)
            java.math.BigInteger r9 = r3.add(r9)
            qs4 r10 = new qs4
            r10.<init>(r8, r9)
            return r10
        La7:
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            java.lang.String r9 = "lambda0 and lambda1 do not have same scale"
            r8.<init>(r9)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.q64.k(xo3, xo3, byte):qs4");
    }

    public static byte[] l(byte b2, qs4 qs4Var, byte b3, BigInteger bigInteger, BigInteger bigInteger2, qs4[] qs4VarArr) {
        boolean z;
        if (b2 != 1 && b2 != -1) {
            throw new IllegalArgumentException("mu must be 1 or -1");
        }
        int bitLength = i(b2, qs4Var).bitLength();
        byte[] bArr = new byte[bitLength > 30 ? bitLength + 4 + b3 : b3 + 34];
        BigInteger shiftRight = bigInteger.shiftRight(1);
        BigInteger bigInteger3 = qs4Var.a;
        BigInteger bigInteger4 = qs4Var.b;
        int i = 0;
        while (true) {
            BigInteger bigInteger5 = ws0.a;
            if (bigInteger3.equals(bigInteger5) && bigInteger4.equals(bigInteger5)) {
                return bArr;
            }
            if (bigInteger3.testBit(0)) {
                BigInteger mod = bigInteger3.add(bigInteger4.multiply(bigInteger2)).mod(bigInteger);
                if (mod.compareTo(shiftRight) >= 0) {
                    mod = mod.subtract(bigInteger);
                }
                byte intValue = (byte) mod.intValue();
                bArr[i] = intValue;
                if (intValue < 0) {
                    intValue = (byte) (-intValue);
                    z = false;
                } else {
                    z = true;
                }
                if (z) {
                    bigInteger3 = bigInteger3.subtract(qs4VarArr[intValue].a);
                    bigInteger4 = bigInteger4.subtract(qs4VarArr[intValue].b);
                } else {
                    bigInteger3 = bigInteger3.add(qs4VarArr[intValue].a);
                    bigInteger4 = bigInteger4.add(qs4VarArr[intValue].b);
                }
            } else {
                bArr[i] = 0;
            }
            BigInteger shiftRight2 = bigInteger3.shiftRight(1);
            BigInteger add = b2 == 1 ? bigInteger4.add(shiftRight2) : bigInteger4.subtract(shiftRight2);
            BigInteger negate = bigInteger3.shiftRight(1).negate();
            i++;
            bigInteger3 = add;
            bigInteger4 = negate;
        }
    }
}
