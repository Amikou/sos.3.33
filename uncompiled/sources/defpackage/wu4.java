package defpackage;

import com.google.android.play.core.internal.d;
import java.io.File;

/* renamed from: wu4  reason: default package */
/* loaded from: classes2.dex */
public final class wu4 implements iu4 {
    public final /* synthetic */ int a = 0;

    public wu4() {
    }

    public wu4(byte[] bArr) {
    }

    public wu4(char[] cArr) {
    }

    @Override // defpackage.iu4
    public final boolean a(Object obj, File file, File file2) {
        int i = this.a;
        if (i == 0) {
            try {
                return !((Boolean) d.f(Class.forName("dalvik.system.DexFile"), Boolean.class, String.class, file.getPath())).booleanValue();
            } catch (ClassNotFoundException unused) {
                return false;
            }
        } else if (i != 1) {
            return true;
        } else {
            return new File((String) d.g(obj.getClass(), String.class, File.class, file, File.class, file2)).exists();
        }
    }
}
