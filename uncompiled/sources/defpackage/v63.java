package defpackage;

/* compiled from: RendererConfiguration.java */
/* renamed from: v63  reason: default package */
/* loaded from: classes.dex */
public final class v63 {
    public static final v63 b = new v63(false);
    public final boolean a;

    public v63(boolean z) {
        this.a = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && v63.class == obj.getClass() && this.a == ((v63) obj).a;
    }

    public int hashCode() {
        return !this.a ? 1 : 0;
    }
}
