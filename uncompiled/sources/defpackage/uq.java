package defpackage;

/* renamed from: uq  reason: default package */
/* loaded from: classes2.dex */
public class uq extends wl implements Cloneable {
    public uq() {
        super(new vq(256));
    }

    @Override // java.security.MessageDigest, java.security.MessageDigestSpi
    public Object clone() throws CloneNotSupportedException {
        uq uqVar = (uq) super.clone();
        uqVar.a = new vq((vq) this.a);
        return uqVar;
    }
}
