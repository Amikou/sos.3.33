package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.github.mikephil.charting.utils.Utils;
import com.lelloman.identicon.drawable.ClassicIdenticonTile;

/* compiled from: ClassicIdenticonDrawable.kt */
/* renamed from: ez  reason: default package */
/* loaded from: classes2.dex */
public final class ez extends on1 {
    public fz g;
    public final Paint h;
    public final Paint i;
    public final x54 j;

    public ez(int i, int i2, int i3) {
        super(i, i2, i3, 3);
        Paint paint = new Paint(1);
        this.h = paint;
        Paint paint2 = new Paint(1);
        this.i = paint2;
        d(i3);
        paint.setStyle(Paint.Style.FILL);
        paint2.setStyle(Paint.Style.FILL);
        paint.setColor(0);
        paint2.setColor(-16777216);
        this.j = new x54(i / 3, i2 / 3);
        c();
    }

    @Override // defpackage.on1
    public void a(Canvas canvas) {
        fs1.f(canvas, "canvas");
        if (this.g == null) {
            return;
        }
        this.i.setColor(i());
        canvas.drawColor(0);
        g(canvas);
        f(canvas);
        h(canvas);
    }

    @Override // defpackage.on1
    public void d(int i) {
        this.g = new fz(i);
    }

    public final void f(Canvas canvas) {
        fz fzVar = this.g;
        fs1.d(fzVar);
        ClassicIdenticonTile.Tiles j = j(fzVar.b());
        fz fzVar2 = this.g;
        fs1.d(fzVar2);
        boolean z = fzVar2.e() == 0;
        fz fzVar3 = this.g;
        fs1.d(fzVar3);
        int c = fzVar3.c() * 90;
        Paint paint = z ? this.i : this.h;
        Paint paint2 = z ? this.h : this.i;
        j.draw(canvas, this.j, c + 0, paint2, paint);
        canvas.save();
        canvas.translate(this.j.n(), Utils.FLOAT_EPSILON);
        j.draw(canvas, this.j, c + 90, paint2, paint);
        canvas.restore();
        canvas.save();
        canvas.translate(this.j.n(), this.j.g());
        j.draw(canvas, this.j, c + 180, paint2, paint);
        canvas.restore();
        canvas.save();
        canvas.translate(Utils.FLOAT_EPSILON, this.j.g());
        j.draw(canvas, this.j, c + 270, paint2, paint);
        canvas.restore();
    }

    public final void g(Canvas canvas) {
        fz fzVar = this.g;
        fs1.d(fzVar);
        boolean z = fzVar.f() == 0;
        Paint paint = z ? this.i : this.h;
        Paint paint2 = z ? this.h : this.i;
        canvas.save();
        canvas.translate(this.j.m(), this.j.f());
        fz fzVar2 = this.g;
        fs1.d(fzVar2);
        j(fzVar2.h()).draw(canvas, this.j, 0, paint2, paint);
        canvas.restore();
    }

    public final void h(Canvas canvas) {
        fz fzVar = this.g;
        fs1.d(fzVar);
        ClassicIdenticonTile.Tiles j = j(fzVar.j());
        fz fzVar2 = this.g;
        fs1.d(fzVar2);
        boolean z = fzVar2.g() == 0;
        fz fzVar3 = this.g;
        fs1.d(fzVar3);
        int k = fzVar3.k() * 90;
        Paint paint = z ? this.i : this.h;
        Paint paint2 = z ? this.h : this.i;
        canvas.save();
        canvas.translate(Utils.FLOAT_EPSILON, this.j.f());
        j.draw(canvas, this.j, k + 0, paint2, paint);
        canvas.restore();
        canvas.save();
        canvas.translate(this.j.m(), this.j.g());
        j.draw(canvas, this.j, k + 90, paint2, paint);
        canvas.restore();
        canvas.save();
        canvas.translate(this.j.n(), this.j.f());
        j.draw(canvas, this.j, k + 180, paint2, paint);
        canvas.restore();
        canvas.save();
        canvas.translate(this.j.m(), Utils.FLOAT_EPSILON);
        j.draw(canvas, this.j, k + 270, paint2, paint);
        canvas.restore();
    }

    public final int i() {
        fz fzVar = this.g;
        fs1.d(fzVar);
        fz fzVar2 = this.g;
        fs1.d(fzVar2);
        fz fzVar3 = this.g;
        fs1.d(fzVar3);
        return ((((fzVar.i() * 8) + 100) * 65536) - 16777216) + (((fzVar2.d() * 8) + 100) * 256) + (fzVar3.a() * 8) + 100;
    }

    public final ClassicIdenticonTile.Tiles j(int i) {
        return ClassicIdenticonTile.a.a()[i];
    }
}
