package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: m16  reason: default package */
/* loaded from: classes.dex */
public final class m16 implements l16 {
    public static final wo5<Boolean> a;
    public static final wo5<Boolean> b;
    public static final wo5<Boolean> c;

    static {
        ro5 ro5Var = new ro5(bo5.a("com.google.android.gms.measurement"));
        ro5Var.b("measurement.service.audience.fix_skip_audience_with_failed_filters", true);
        a = ro5Var.b("measurement.audience.refresh_event_count_filters_timestamp", false);
        b = ro5Var.b("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false);
        c = ro5Var.b("measurement.audience.use_bundle_timestamp_for_event_count_filters", false);
    }

    @Override // defpackage.l16
    public final boolean b() {
        return c.e().booleanValue();
    }

    @Override // defpackage.l16
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.l16
    public final boolean zzb() {
        return a.e().booleanValue();
    }

    @Override // defpackage.l16
    public final boolean zzc() {
        return b.e().booleanValue();
    }
}
