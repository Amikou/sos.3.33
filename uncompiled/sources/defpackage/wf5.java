package defpackage;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.os.RemoteException;
import android.util.Pair;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import com.google.android.gms.internal.measurement.a0;
import com.google.android.gms.internal.measurement.b0;
import com.google.android.gms.internal.measurement.c0;
import com.google.android.gms.internal.measurement.d0;
import com.google.android.gms.internal.measurement.e0;
import com.google.android.gms.internal.measurement.f0;
import com.google.android.gms.internal.measurement.g0;
import com.google.android.gms.internal.measurement.h0;
import com.google.android.gms.internal.measurement.i;
import com.google.android.gms.internal.measurement.i0;
import com.google.android.gms.internal.measurement.j;
import com.google.android.gms.internal.measurement.q;
import com.google.android.gms.internal.measurement.r;
import com.google.android.gms.internal.measurement.s;
import com.google.android.gms.internal.measurement.t;
import com.google.android.gms.internal.measurement.u;
import com.google.android.gms.internal.measurement.v;
import com.google.android.gms.internal.measurement.w;
import com.google.android.gms.internal.measurement.x;
import com.google.android.gms.internal.measurement.y;
import com.google.android.gms.internal.measurement.z;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: wf5  reason: default package */
/* loaded from: classes.dex */
public final class wf5 {
    public static volatile wf5 i;
    public final String a;
    public final rz b;
    public final ExecutorService c;
    public final tf d;
    public final List<Pair<em5, oe5>> e;
    public int f;
    public boolean g;
    public volatile j h;

    public wf5(Context context, String str, String str2, String str3, Bundle bundle) {
        if (str != null && q(str2, str3)) {
            this.a = str;
        } else {
            this.a = "FA";
        }
        this.b = mi0.c();
        this.c = v95.a().a(new ud5(this), 1);
        this.d = new tf(this);
        this.e = new ArrayList();
        try {
            if (zp5.a(context, "google_app_id", sj5.a(context)) != null && !m()) {
                this.g = true;
                return;
            }
        } catch (IllegalStateException unused) {
        }
        q(str2, str3);
        n(new hc5(this, str2, str3, context, bundle));
        Application application = (Application) context.getApplicationContext();
        if (application == null) {
            return;
        }
        application.registerActivityLifecycleCallbacks(new uf5(this));
    }

    public static final boolean m() {
        return true;
    }

    public static final boolean q(String str, String str2) {
        return (str2 == null || str == null || m()) ? false : true;
    }

    public static wf5 r(Context context, String str, String str2, String str3, Bundle bundle) {
        zt2.j(context);
        if (i == null) {
            synchronized (wf5.class) {
                if (i == null) {
                    i = new wf5(context, str, str2, str3, bundle);
                }
            }
        }
        return i;
    }

    public final List<Bundle> A(String str, String str2) {
        aa5 aa5Var = new aa5();
        n(new s(this, str, str2, aa5Var));
        List<Bundle> list = (List) aa5.H1(aa5Var.G1(5000L), List.class);
        return list == null ? Collections.emptyList() : list;
    }

    public final void B(Activity activity, String str, String str2) {
        n(new t(this, activity, str, str2));
    }

    public final void C(String str) {
        n(new u(this, str));
    }

    public final void D(String str) {
        n(new v(this, str));
    }

    public final String E() {
        aa5 aa5Var = new aa5();
        n(new w(this, aa5Var));
        return aa5Var.F1(500L);
    }

    public final String F() {
        aa5 aa5Var = new aa5();
        n(new x(this, aa5Var));
        return aa5Var.F1(50L);
    }

    public final long G() {
        aa5 aa5Var = new aa5();
        n(new y(this, aa5Var));
        Long l = (Long) aa5.H1(aa5Var.G1(500L), Long.class);
        if (l == null) {
            long nextLong = new Random(System.nanoTime() ^ this.b.a()).nextLong();
            int i2 = this.f + 1;
            this.f = i2;
            return nextLong + i2;
        }
        return l.longValue();
    }

    public final String H() {
        aa5 aa5Var = new aa5();
        n(new z(this, aa5Var));
        return aa5Var.F1(500L);
    }

    public final String a() {
        aa5 aa5Var = new aa5();
        n(new a0(this, aa5Var));
        return aa5Var.F1(500L);
    }

    public final Map<String, Object> b(String str, String str2, boolean z) {
        aa5 aa5Var = new aa5();
        n(new b0(this, str, str2, z, aa5Var));
        Bundle G1 = aa5Var.G1(5000L);
        if (G1 != null && G1.size() != 0) {
            HashMap hashMap = new HashMap(G1.size());
            for (String str3 : G1.keySet()) {
                Object obj = G1.get(str3);
                if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                    hashMap.put(str3, obj);
                }
            }
            return hashMap;
        }
        return Collections.emptyMap();
    }

    public final void c(int i2, String str, Object obj, Object obj2, Object obj3) {
        n(new c0(this, false, 5, str, obj, null, null));
    }

    public final int d(String str) {
        aa5 aa5Var = new aa5();
        n(new d0(this, str, aa5Var));
        Integer num = (Integer) aa5.H1(aa5Var.G1(10000L), Integer.class);
        if (num == null) {
            return 25;
        }
        return num.intValue();
    }

    public final void e(boolean z) {
        n(new e0(this, z));
    }

    public final void n(i0 i0Var) {
        this.c.execute(i0Var);
    }

    public final void o(Exception exc, boolean z, boolean z2) {
        this.g |= z;
        if (!z && z2) {
            c(5, "Error with data collection. Data lost.", exc, null, null);
        }
    }

    public final void p(String str, String str2, Bundle bundle, boolean z, boolean z2, Long l) {
        n(new g0(this, l, str, str2, bundle, z, z2));
    }

    public final tf s() {
        return this.d;
    }

    public final j t(Context context, boolean z) {
        try {
            return i.asInterface(DynamiteModule.d(context, DynamiteModule.k, ModuleDescriptor.MODULE_ID).c("com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"));
        } catch (DynamiteModule.LoadingException e) {
            o(e, true, false);
            return null;
        }
    }

    public final void u(em5 em5Var) {
        zt2.j(em5Var);
        synchronized (this.e) {
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                if (em5Var.equals(this.e.get(i2).first)) {
                    return;
                }
            }
            oe5 oe5Var = new oe5(em5Var);
            this.e.add(new Pair<>(em5Var, oe5Var));
            if (this.h != null) {
                try {
                    this.h.registerOnMeasurementEventListener(oe5Var);
                    return;
                } catch (BadParcelableException | NetworkOnMainThreadException | RemoteException | IllegalArgumentException | IllegalStateException | NullPointerException | SecurityException | UnsupportedOperationException unused) {
                }
            }
            n(new f0(this, oe5Var));
        }
    }

    public final void v(String str, Bundle bundle) {
        p(null, str, bundle, false, true, null);
    }

    public final void w(String str, String str2, Bundle bundle) {
        p(str, str2, bundle, true, true, null);
    }

    public final void x(String str, String str2, Object obj, boolean z) {
        n(new h0(this, str, str2, obj, z));
    }

    public final void y(Bundle bundle) {
        n(new q(this, bundle));
    }

    public final void z(String str, String str2, Bundle bundle) {
        n(new r(this, str, str2, bundle));
    }
}
