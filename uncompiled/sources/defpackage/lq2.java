package defpackage;

import android.content.Context;

/* compiled from: PermissionUtil.java */
/* renamed from: lq2  reason: default package */
/* loaded from: classes3.dex */
public class lq2 {
    public static boolean a(Context context, String str) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr != null && strArr.length > 0) {
                for (String str2 : strArr) {
                    if (str2.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Exception unused) {
        }
        return false;
    }

    public static boolean b(Context context, String str) {
        return m70.a(context, str) == 0;
    }
}
