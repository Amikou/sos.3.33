package defpackage;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.EnumMap;
import java.util.HashSet;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: QRGEncoder.java */
/* renamed from: uw2  reason: default package */
/* loaded from: classes.dex */
public class uw2 {
    public int a;
    public String b = null;
    public BarcodeFormat c = null;
    public boolean d;

    public uw2(String str, Bundle bundle, String str2, int i) {
        this.a = Integer.MIN_VALUE;
        this.d = false;
        this.a = i;
        this.d = b(str, bundle, str2);
    }

    public static String d(String str) {
        if (str != null) {
            if (str.indexOf(58) >= 0 || str.indexOf(59) >= 0) {
                int length = str.length();
                StringBuilder sb = new StringBuilder(length);
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt == ':' || charAt == ';') {
                        sb.append('\\');
                    }
                    sb.append(charAt);
                }
                return sb.toString();
            }
            return str;
        }
        return str;
    }

    public static String e(CharSequence charSequence) {
        for (int i = 0; i < charSequence.length(); i++) {
            if (charSequence.charAt(i) > 255) {
                return "UTF-8";
            }
        }
        return null;
    }

    public static String f(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return null;
        }
        return trim;
    }

    public Bitmap a() throws WriterException {
        EnumMap enumMap = null;
        if (this.d) {
            String e = e(this.b);
            if (e != null) {
                enumMap = new EnumMap(EncodeHintType.class);
                enumMap.put((EnumMap) EncodeHintType.CHARACTER_SET, (EncodeHintType) e);
            }
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            String str = this.b;
            BarcodeFormat barcodeFormat = this.c;
            int i = this.a;
            BitMatrix encode = multiFormatWriter.encode(str, barcodeFormat, i, i, enumMap);
            int width = encode.getWidth();
            int height = encode.getHeight();
            int[] iArr = new int[width * height];
            for (int i2 = 0; i2 < height; i2++) {
                int i3 = i2 * width;
                for (int i4 = 0; i4 < width; i4++) {
                    iArr[i3 + i4] = encode.get(i4, i2) ? -16777216 : -1;
                }
            }
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            createBitmap.setPixels(iArr, 0, width, 0, 0, width, height);
            return createBitmap;
        }
        return null;
    }

    public final boolean b(String str, Bundle bundle, String str2) {
        BarcodeFormat barcodeFormat = BarcodeFormat.QR_CODE;
        this.c = barcodeFormat;
        this.c = barcodeFormat;
        c(str, bundle, str2);
        String str3 = this.b;
        return str3 != null && str3.length() > 0;
    }

    public final void c(String str, Bundle bundle, String str2) {
        str2.hashCode();
        int i = 0;
        char c = 65535;
        switch (str2.hashCode()) {
            case -1309271157:
                if (str2.equals("PHONE_TYPE")) {
                    c = 0;
                    break;
                }
                break;
            case -670199783:
                if (str2.equals("CONTACT_TYPE")) {
                    c = 1;
                    break;
                }
                break;
            case 709220992:
                if (str2.equals("SMS_TYPE")) {
                    c = 2;
                    break;
                }
                break;
            case 1349204356:
                if (str2.equals("LOCATION_TYPE")) {
                    c = 3;
                    break;
                }
                break;
            case 1778595596:
                if (str2.equals("TEXT_TYPE")) {
                    c = 4;
                    break;
                }
                break;
            case 1833351709:
                if (str2.equals("EMAIL_TYPE")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                String f = f(str);
                if (f != null) {
                    this.b = "tel:" + f;
                    PhoneNumberUtils.formatNumber(f);
                    return;
                }
                return;
            case 1:
                if (bundle == null) {
                    return;
                }
                StringBuilder sb = new StringBuilder(100);
                StringBuilder sb2 = new StringBuilder(100);
                sb.append("MECARD:");
                String f2 = f(bundle.getString(PublicResolver.FUNC_NAME));
                if (f2 != null) {
                    sb.append("N:");
                    sb.append(d(f2));
                    sb.append(';');
                    sb2.append(f2);
                }
                String f3 = f(bundle.getString("postal"));
                if (f3 != null) {
                    sb.append("ADR:");
                    sb.append(d(f3));
                    sb.append(';');
                    sb2.append('\n');
                    sb2.append(f3);
                }
                HashSet<String> hashSet = new HashSet(tw2.a.length);
                int i2 = 0;
                while (true) {
                    String[] strArr = tw2.a;
                    if (i2 < strArr.length) {
                        String f4 = f(bundle.getString(strArr[i2]));
                        if (f4 != null) {
                            hashSet.add(f4);
                        }
                        i2++;
                    } else {
                        for (String str3 : hashSet) {
                            sb.append("TEL:");
                            sb.append(d(str3));
                            sb.append(';');
                            sb2.append('\n');
                            sb2.append(PhoneNumberUtils.formatNumber(str3));
                        }
                        HashSet<String> hashSet2 = new HashSet(tw2.b.length);
                        while (true) {
                            String[] strArr2 = tw2.b;
                            if (i < strArr2.length) {
                                String f5 = f(bundle.getString(strArr2[i]));
                                if (f5 != null) {
                                    hashSet2.add(f5);
                                }
                                i++;
                            } else {
                                for (String str4 : hashSet2) {
                                    sb.append("EMAIL:");
                                    sb.append(d(str4));
                                    sb.append(';');
                                    sb2.append('\n');
                                    sb2.append(str4);
                                }
                                String f6 = f(bundle.getString("URL_KEY"));
                                if (f6 != null) {
                                    sb.append("URL:");
                                    sb.append(f6);
                                    sb.append(';');
                                    sb2.append('\n');
                                    sb2.append(f6);
                                }
                                String f7 = f(bundle.getString("NOTE_KEY"));
                                if (f7 != null) {
                                    sb.append("NOTE:");
                                    sb.append(d(f7));
                                    sb.append(';');
                                    sb2.append('\n');
                                    sb2.append(f7);
                                }
                                if (sb2.length() > 0) {
                                    sb.append(';');
                                    this.b = sb.toString();
                                    return;
                                }
                                this.b = null;
                                return;
                            }
                        }
                    }
                }
            case 2:
                String f8 = f(str);
                if (f8 != null) {
                    this.b = "sms:" + f8;
                    PhoneNumberUtils.formatNumber(f8);
                    return;
                }
                return;
            case 3:
                if (bundle != null) {
                    float f9 = bundle.getFloat("LAT", Float.MAX_VALUE);
                    float f10 = bundle.getFloat("LONG", Float.MAX_VALUE);
                    if (f9 == Float.MAX_VALUE || f10 == Float.MAX_VALUE) {
                        return;
                    }
                    this.b = "geo:" + f9 + ',' + f10;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(f9);
                    sb3.append(",");
                    sb3.append(f10);
                    return;
                }
                return;
            case 4:
                if (str == null || str.length() <= 0) {
                    return;
                }
                this.b = str;
                return;
            case 5:
                String f11 = f(str);
                if (f11 != null) {
                    this.b = "mailto:" + f11;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
