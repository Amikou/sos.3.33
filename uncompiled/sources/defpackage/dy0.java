package defpackage;

import java.io.Closeable;

/* compiled from: EventStore.java */
/* renamed from: dy0  reason: default package */
/* loaded from: classes.dex */
public interface dy0 extends Closeable {
    void E(Iterable<nq2> iterable);

    Iterable<nq2> H(ob4 ob4Var);

    nq2 b1(ob4 ob4Var, wx0 wx0Var);

    boolean d1(ob4 ob4Var);

    Iterable<ob4> j0();

    int k();

    long l0(ob4 ob4Var);

    void u1(ob4 ob4Var, long j);

    void x1(Iterable<nq2> iterable);
}
