package defpackage;

import java.util.Map;

/* compiled from: InternalProducerListener.java */
/* renamed from: ds1  reason: default package */
/* loaded from: classes.dex */
public class ds1 implements iv2 {
    public final jv2 a;
    public final iv2 b;

    public ds1(jv2 jv2Var, iv2 iv2Var) {
        this.a = jv2Var;
        this.b = iv2Var;
    }

    @Override // defpackage.iv2
    public void a(ev2 ev2Var, String str, Map<String, String> map) {
        jv2 jv2Var = this.a;
        if (jv2Var != null) {
            jv2Var.i(ev2Var.getId(), str, map);
        }
        iv2 iv2Var = this.b;
        if (iv2Var != null) {
            iv2Var.a(ev2Var, str, map);
        }
    }

    @Override // defpackage.iv2
    public void c(ev2 ev2Var, String str, Throwable th, Map<String, String> map) {
        jv2 jv2Var = this.a;
        if (jv2Var != null) {
            jv2Var.j(ev2Var.getId(), str, th, map);
        }
        iv2 iv2Var = this.b;
        if (iv2Var != null) {
            iv2Var.c(ev2Var, str, th, map);
        }
    }

    @Override // defpackage.iv2
    public void d(ev2 ev2Var, String str, Map<String, String> map) {
        jv2 jv2Var = this.a;
        if (jv2Var != null) {
            jv2Var.d(ev2Var.getId(), str, map);
        }
        iv2 iv2Var = this.b;
        if (iv2Var != null) {
            iv2Var.d(ev2Var, str, map);
        }
    }

    @Override // defpackage.iv2
    public void e(ev2 ev2Var, String str, boolean z) {
        jv2 jv2Var = this.a;
        if (jv2Var != null) {
            jv2Var.e(ev2Var.getId(), str, z);
        }
        iv2 iv2Var = this.b;
        if (iv2Var != null) {
            iv2Var.e(ev2Var, str, z);
        }
    }

    @Override // defpackage.iv2
    public void i(ev2 ev2Var, String str, String str2) {
        jv2 jv2Var = this.a;
        if (jv2Var != null) {
            jv2Var.h(ev2Var.getId(), str, str2);
        }
        iv2 iv2Var = this.b;
        if (iv2Var != null) {
            iv2Var.i(ev2Var, str, str2);
        }
    }

    @Override // defpackage.iv2
    public boolean j(ev2 ev2Var, String str) {
        iv2 iv2Var;
        jv2 jv2Var = this.a;
        boolean f = jv2Var != null ? jv2Var.f(ev2Var.getId()) : false;
        return (f || (iv2Var = this.b) == null) ? f : iv2Var.j(ev2Var, str);
    }

    @Override // defpackage.iv2
    public void k(ev2 ev2Var, String str) {
        jv2 jv2Var = this.a;
        if (jv2Var != null) {
            jv2Var.b(ev2Var.getId(), str);
        }
        iv2 iv2Var = this.b;
        if (iv2Var != null) {
            iv2Var.k(ev2Var, str);
        }
    }
}
