package defpackage;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import defpackage.gq2;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: a35  reason: default package */
/* loaded from: classes.dex */
public final class a35 implements gq2.a {
    public final /* synthetic */ BasePendingResult a;
    public final /* synthetic */ b35 b;

    public a35(b35 b35Var, BasePendingResult basePendingResult) {
        this.b = b35Var;
        this.a = basePendingResult;
    }

    @Override // defpackage.gq2.a
    public final void a(Status status) {
        Map map;
        map = this.b.a;
        map.remove(this.a);
    }
}
