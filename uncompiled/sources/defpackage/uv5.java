package defpackage;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: uv5  reason: default package */
/* loaded from: classes.dex */
public final class uv5 implements wg5 {
    public final /* synthetic */ fw5 a;

    public uv5(fw5 fw5Var) {
        this.a = fw5Var;
    }

    @Override // defpackage.wg5
    public final void a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.a.g(str, i, th, bArr, map);
    }
}
