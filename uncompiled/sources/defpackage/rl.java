package defpackage;

import androidx.media3.common.ParserException;
import defpackage.wc2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AvcConfig.java */
/* renamed from: rl  reason: default package */
/* loaded from: classes.dex */
public final class rl {
    public final List<byte[]> a;
    public final int b;
    public final int c;
    public final int d;
    public final float e;
    public final String f;

    public rl(List<byte[]> list, int i, int i2, int i3, float f, String str) {
        this.a = list;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = f;
        this.f = str;
    }

    public static byte[] a(op2 op2Var) {
        int J = op2Var.J();
        int e = op2Var.e();
        op2Var.Q(J);
        return h00.d(op2Var.d(), e, J);
    }

    public static rl b(op2 op2Var) throws ParserException {
        float f;
        String str;
        int i;
        try {
            op2Var.Q(4);
            int D = (op2Var.D() & 3) + 1;
            if (D != 3) {
                ArrayList arrayList = new ArrayList();
                int D2 = op2Var.D() & 31;
                for (int i2 = 0; i2 < D2; i2++) {
                    arrayList.add(a(op2Var));
                }
                int D3 = op2Var.D();
                for (int i3 = 0; i3 < D3; i3++) {
                    arrayList.add(a(op2Var));
                }
                int i4 = -1;
                if (D2 > 0) {
                    wc2.c l = wc2.l((byte[]) arrayList.get(0), D, ((byte[]) arrayList.get(0)).length);
                    int i5 = l.e;
                    int i6 = l.f;
                    float f2 = l.g;
                    str = h00.a(l.a, l.b, l.c);
                    i4 = i5;
                    i = i6;
                    f = f2;
                } else {
                    f = 1.0f;
                    str = null;
                    i = -1;
                }
                return new rl(arrayList, D, i4, i, f, str);
            }
            throw new IllegalStateException();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw ParserException.createForMalformedContainer("Error parsing AVC config", e);
        }
    }
}
