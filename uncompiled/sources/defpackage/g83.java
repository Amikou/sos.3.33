package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import android.util.TypedValue;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

/* compiled from: ResourcesCompat.java */
/* renamed from: g83  reason: default package */
/* loaded from: classes.dex */
public final class g83 {
    public static final ThreadLocal<TypedValue> a = new ThreadLocal<>();
    public static final WeakHashMap<c, SparseArray<b>> b = new WeakHashMap<>(0);
    public static final Object c = new Object();

    /* compiled from: ResourcesCompat.java */
    /* renamed from: g83$a */
    /* loaded from: classes.dex */
    public static class a {
        public static ColorStateList a(Resources resources, int i, Resources.Theme theme) {
            return resources.getColorStateList(i, theme);
        }
    }

    /* compiled from: ResourcesCompat.java */
    /* renamed from: g83$b */
    /* loaded from: classes.dex */
    public static class b {
        public final ColorStateList a;
        public final Configuration b;

        public b(ColorStateList colorStateList, Configuration configuration) {
            this.a = colorStateList;
            this.b = configuration;
        }
    }

    /* compiled from: ResourcesCompat.java */
    /* renamed from: g83$c */
    /* loaded from: classes.dex */
    public static final class c {
        public final Resources a;
        public final Resources.Theme b;

        public c(Resources resources, Resources.Theme theme) {
            this.a = resources;
            this.b = theme;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            return this.a.equals(cVar.a) && sl2.a(this.b, cVar.b);
        }

        public int hashCode() {
            return sl2.b(this.a, this.b);
        }
    }

    /* compiled from: ResourcesCompat.java */
    /* renamed from: g83$d */
    /* loaded from: classes.dex */
    public static abstract class d {

        /* compiled from: ResourcesCompat.java */
        /* renamed from: g83$d$a */
        /* loaded from: classes.dex */
        public class a implements Runnable {
            public final /* synthetic */ Typeface a;

            public a(Typeface typeface) {
                this.a = typeface;
            }

            @Override // java.lang.Runnable
            public void run() {
                d.this.e(this.a);
            }
        }

        /* compiled from: ResourcesCompat.java */
        /* renamed from: g83$d$b */
        /* loaded from: classes.dex */
        public class b implements Runnable {
            public final /* synthetic */ int a;

            public b(int i) {
                this.a = i;
            }

            @Override // java.lang.Runnable
            public void run() {
                d.this.d(this.a);
            }
        }

        public static Handler c(Handler handler) {
            return handler == null ? new Handler(Looper.getMainLooper()) : handler;
        }

        public final void a(int i, Handler handler) {
            c(handler).post(new b(i));
        }

        public final void b(Typeface typeface, Handler handler) {
            c(handler).post(new a(typeface));
        }

        public abstract void d(int i);

        public abstract void e(Typeface typeface);
    }

    /* compiled from: ResourcesCompat.java */
    /* renamed from: g83$e */
    /* loaded from: classes.dex */
    public static final class e {

        /* compiled from: ResourcesCompat.java */
        /* renamed from: g83$e$a */
        /* loaded from: classes.dex */
        public static class a {
            public static final Object a = new Object();
            public static Method b;
            public static boolean c;

            public static void a(Resources.Theme theme) {
                synchronized (a) {
                    if (!c) {
                        try {
                            Method declaredMethod = Resources.Theme.class.getDeclaredMethod("rebase", new Class[0]);
                            b = declaredMethod;
                            declaredMethod.setAccessible(true);
                        } catch (NoSuchMethodException unused) {
                        }
                        c = true;
                    }
                    Method method = b;
                    if (method != null) {
                        try {
                            method.invoke(theme, new Object[0]);
                        } catch (IllegalAccessException | InvocationTargetException unused2) {
                            b = null;
                        }
                    }
                }
            }
        }

        /* compiled from: ResourcesCompat.java */
        /* renamed from: g83$e$b */
        /* loaded from: classes.dex */
        public static class b {
            public static void a(Resources.Theme theme) {
                theme.rebase();
            }
        }

        public static void a(Resources.Theme theme) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                b.a(theme);
            } else if (i >= 23) {
                a.a(theme);
            }
        }
    }

    public static void a(c cVar, int i, ColorStateList colorStateList) {
        synchronized (c) {
            WeakHashMap<c, SparseArray<b>> weakHashMap = b;
            SparseArray<b> sparseArray = weakHashMap.get(cVar);
            if (sparseArray == null) {
                sparseArray = new SparseArray<>();
                weakHashMap.put(cVar, sparseArray);
            }
            sparseArray.append(i, new b(colorStateList, cVar.a.getConfiguration()));
        }
    }

    public static ColorStateList b(c cVar, int i) {
        b bVar;
        synchronized (c) {
            SparseArray<b> sparseArray = b.get(cVar);
            if (sparseArray != null && sparseArray.size() > 0 && (bVar = sparseArray.get(i)) != null) {
                if (bVar.b.equals(cVar.a.getConfiguration())) {
                    return bVar.a;
                }
                sparseArray.remove(i);
            }
            return null;
        }
    }

    public static Typeface c(Context context, int i) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return m(context, i, new TypedValue(), 0, null, null, false, true);
    }

    public static int d(Resources resources, int i, Resources.Theme theme) throws Resources.NotFoundException {
        if (Build.VERSION.SDK_INT >= 23) {
            return resources.getColor(i, theme);
        }
        return resources.getColor(i);
    }

    public static ColorStateList e(Resources resources, int i, Resources.Theme theme) throws Resources.NotFoundException {
        c cVar = new c(resources, theme);
        ColorStateList b2 = b(cVar, i);
        if (b2 != null) {
            return b2;
        }
        ColorStateList k = k(resources, i, theme);
        if (k != null) {
            a(cVar, i, k);
            return k;
        } else if (Build.VERSION.SDK_INT >= 23) {
            return a.a(resources, i, theme);
        } else {
            return resources.getColorStateList(i);
        }
    }

    public static Drawable f(Resources resources, int i, Resources.Theme theme) throws Resources.NotFoundException {
        if (Build.VERSION.SDK_INT >= 21) {
            return resources.getDrawable(i, theme);
        }
        return resources.getDrawable(i);
    }

    public static Typeface g(Context context, int i) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return m(context, i, new TypedValue(), 0, null, null, false, false);
    }

    public static Typeface h(Context context, int i, TypedValue typedValue, int i2, d dVar) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return m(context, i, typedValue, i2, dVar, null, true, false);
    }

    public static void i(Context context, int i, d dVar, Handler handler) throws Resources.NotFoundException {
        du2.e(dVar);
        if (context.isRestricted()) {
            dVar.a(-4, handler);
        } else {
            m(context, i, new TypedValue(), 0, dVar, handler, false, false);
        }
    }

    public static TypedValue j() {
        ThreadLocal<TypedValue> threadLocal = a;
        TypedValue typedValue = threadLocal.get();
        if (typedValue == null) {
            TypedValue typedValue2 = new TypedValue();
            threadLocal.set(typedValue2);
            return typedValue2;
        }
        return typedValue;
    }

    public static ColorStateList k(Resources resources, int i, Resources.Theme theme) {
        if (l(resources, i)) {
            return null;
        }
        try {
            return y20.a(resources, resources.getXml(i), theme);
        } catch (Exception unused) {
            return null;
        }
    }

    public static boolean l(Resources resources, int i) {
        TypedValue j = j();
        resources.getValue(i, j, true);
        int i2 = j.type;
        return i2 >= 28 && i2 <= 31;
    }

    public static Typeface m(Context context, int i, TypedValue typedValue, int i2, d dVar, Handler handler, boolean z, boolean z2) {
        Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        Typeface n = n(context, resources, typedValue, i, i2, dVar, handler, z, z2);
        if (n == null && dVar == null && !z2) {
            throw new Resources.NotFoundException("Font resource ID #0x" + Integer.toHexString(i) + " could not be retrieved.");
        }
        return n;
    }

    /* JADX WARN: Removed duplicated region for block: B:34:0x008c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static android.graphics.Typeface n(android.content.Context r13, android.content.res.Resources r14, android.util.TypedValue r15, int r16, int r17, defpackage.g83.d r18, android.os.Handler r19, boolean r20, boolean r21) {
        /*
            r2 = r14
            r0 = r15
            r3 = r16
            r4 = r17
            r8 = r18
            r9 = r19
            java.lang.CharSequence r1 = r0.string
            if (r1 == 0) goto L90
            java.lang.String r10 = r1.toString()
            java.lang.String r0 = "res/"
            boolean r0 = r10.startsWith(r0)
            r11 = -3
            r12 = 0
            if (r0 != 0) goto L22
            if (r8 == 0) goto L21
            r8.a(r11, r9)
        L21:
            return r12
        L22:
            android.graphics.Typeface r0 = defpackage.yd4.f(r14, r3, r4)
            if (r0 == 0) goto L2e
            if (r8 == 0) goto L2d
            r8.b(r0, r9)
        L2d:
            return r0
        L2e:
            if (r21 == 0) goto L31
            return r12
        L31:
            java.lang.String r0 = r10.toLowerCase()     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            java.lang.String r1 = ".xml"
            boolean r0 = r0.endsWith(r1)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            if (r0 == 0) goto L5e
            android.content.res.XmlResourceParser r0 = r14.getXml(r3)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            n81$a r1 = defpackage.n81.b(r0, r14)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            if (r1 != 0) goto L4d
            if (r8 == 0) goto L4c
            r8.a(r11, r9)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
        L4c:
            return r12
        L4d:
            r0 = r13
            r2 = r14
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r19
            r7 = r20
            android.graphics.Typeface r0 = defpackage.yd4.c(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            return r0
        L5e:
            r0 = r13
            android.graphics.Typeface r0 = defpackage.yd4.d(r13, r14, r3, r10, r4)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            if (r8 == 0) goto L6e
            if (r0 == 0) goto L6b
            r8.b(r0, r9)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
            goto L6e
        L6b:
            r8.a(r11, r9)     // Catch: java.io.IOException -> L6f org.xmlpull.v1.XmlPullParserException -> L7d
        L6e:
            return r0
        L6f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Failed to read xml resource "
            r0.append(r1)
            r0.append(r10)
            goto L8a
        L7d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Failed to parse xml resource "
            r0.append(r1)
            r0.append(r10)
        L8a:
            if (r8 == 0) goto L8f
            r8.a(r11, r9)
        L8f:
            return r12
        L90:
            android.content.res.Resources$NotFoundException r1 = new android.content.res.Resources$NotFoundException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Resource \""
            r4.append(r5)
            java.lang.String r2 = r14.getResourceName(r3)
            r4.append(r2)
            java.lang.String r2 = "\" ("
            r4.append(r2)
            java.lang.String r2 = java.lang.Integer.toHexString(r16)
            r4.append(r2)
            java.lang.String r2 = ") is not a Font: "
            r4.append(r2)
            r4.append(r15)
            java.lang.String r0 = r4.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.g83.n(android.content.Context, android.content.res.Resources, android.util.TypedValue, int, int, g83$d, android.os.Handler, boolean, boolean):android.graphics.Typeface");
    }
}
