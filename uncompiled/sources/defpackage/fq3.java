package defpackage;

import java.lang.annotation.Annotation;
import retrofit2.p;

/* compiled from: SkipCallbackExecutorImpl.java */
/* renamed from: fq3  reason: default package */
/* loaded from: classes3.dex */
public final class fq3 implements eq3 {
    public static final eq3 a = new fq3();

    public static Annotation[] a(Annotation[] annotationArr) {
        if (p.l(annotationArr, eq3.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[annotationArr.length + 1];
        annotationArr2[0] = a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    @Override // java.lang.annotation.Annotation
    public Class<? extends Annotation> annotationType() {
        return eq3.class;
    }

    @Override // java.lang.annotation.Annotation
    public boolean equals(Object obj) {
        return obj instanceof eq3;
    }

    @Override // java.lang.annotation.Annotation
    public int hashCode() {
        return 0;
    }

    @Override // java.lang.annotation.Annotation
    public String toString() {
        return "@" + eq3.class.getName() + "()";
    }
}
