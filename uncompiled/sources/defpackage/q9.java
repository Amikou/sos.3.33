package defpackage;

import org.web3j.abi.datatypes.Address;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: AddNewTokenItemDisplayModel.kt */
/* renamed from: q9  reason: default package */
/* loaded from: classes2.dex */
public final class q9 {
    public final String a;
    public final int b;
    public final String c;
    public final String d;
    public final String e;
    public boolean f;
    public String g;
    public int h;
    public int i;
    public boolean j;

    public q9(String str, int i, String str2, String str3, String str4, boolean z, String str5, int i2, int i3, boolean z2) {
        fs1.f(str, "symbolWithType");
        fs1.f(str2, "iconFile");
        fs1.f(str3, PublicResolver.FUNC_NAME);
        fs1.f(str4, "symbol");
        fs1.f(str5, Address.TYPE_NAME);
        this.a = str;
        this.b = i;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = z;
        this.g = str5;
        this.h = i2;
        this.i = i3;
        this.j = z2;
    }

    public final String a() {
        return this.g;
    }

    public final int b() {
        return this.h;
    }

    public final int c() {
        return this.i;
    }

    public final String d() {
        return this.c;
    }

    public final int e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof q9) {
            q9 q9Var = (q9) obj;
            return fs1.b(this.a, q9Var.a) && this.b == q9Var.b && fs1.b(this.c, q9Var.c) && fs1.b(this.d, q9Var.d) && fs1.b(this.e, q9Var.e) && this.f == q9Var.f && fs1.b(this.g, q9Var.g) && this.h == q9Var.h && this.i == q9Var.i && this.j == q9Var.j;
        }
        return false;
    }

    public final String f() {
        return this.d;
    }

    public final String g() {
        return this.e;
    }

    public final String h() {
        return this.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = ((((((((this.a.hashCode() * 31) + this.b) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31;
        boolean z = this.f;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        int hashCode2 = (((((((hashCode + i) * 31) + this.g.hashCode()) * 31) + this.h) * 31) + this.i) * 31;
        boolean z2 = this.j;
        return hashCode2 + (z2 ? 1 : z2 ? 1 : 0);
    }

    public final boolean i() {
        return this.f;
    }

    public final void j(boolean z) {
        this.f = z;
    }

    public String toString() {
        return "AddNewTokenItemDisplayModel(symbolWithType=" + this.a + ", iconResId=" + this.b + ", iconFile=" + this.c + ", name=" + this.d + ", symbol=" + this.e + ", isAdded=" + this.f + ", address=" + this.g + ", chainId=" + this.h + ", decimals=" + this.i + ", allowSwap=" + this.j + ')';
    }
}
