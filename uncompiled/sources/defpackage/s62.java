package defpackage;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import zendesk.belvedere.MediaIntent;
import zendesk.belvedere.MediaResult;
import zendesk.belvedere.i;

/* compiled from: MediaSource.java */
/* renamed from: s62  reason: default package */
/* loaded from: classes3.dex */
public class s62 {
    public final vt3 a;
    public final as1 b;
    public final Context c;

    public s62(Context context, vt3 vt3Var, as1 as1Var) {
        this.c = context;
        this.a = vt3Var;
        this.b = as1Var;
    }

    public final boolean a(Context context) {
        return g(context);
    }

    @SuppressLint({"NewApi"})
    public final List<Uri> b(Intent intent) {
        ArrayList arrayList = new ArrayList();
        if (Build.VERSION.SDK_INT >= 16 && intent.getClipData() != null) {
            ClipData clipData = intent.getClipData();
            int itemCount = clipData.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                ClipData.Item itemAt = clipData.getItemAt(i);
                if (itemAt.getUri() != null) {
                    arrayList.add(itemAt.getUri());
                }
            }
        } else if (intent.getData() != null) {
            arrayList.add(intent.getData());
        }
        return arrayList;
    }

    public jp2<MediaIntent, MediaResult> c(int i) {
        if (a(this.c)) {
            return j(this.c, i);
        }
        return new jp2<>(MediaIntent.e(), null);
    }

    @TargetApi(19)
    public final Intent d(String str, boolean z, List<String> list) {
        Intent intent;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            i.a("Belvedere", "Gallery Intent, using 'ACTION_OPEN_DOCUMENT'");
            intent = new Intent("android.intent.action.OPEN_DOCUMENT");
        } else {
            i.a("Belvedere", "Gallery Intent, using 'ACTION_GET_CONTENT'");
            intent = new Intent("android.intent.action.GET_CONTENT");
        }
        intent.setType(str);
        intent.addCategory("android.intent.category.OPENABLE");
        if (i >= 18) {
            intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", z);
        }
        if (list != null && !list.isEmpty()) {
            intent.putExtra("android.intent.extra.MIME_TYPES", (String[]) list.toArray(new String[0]));
        }
        return intent;
    }

    public void e(Context context, int i, int i2, Intent intent, uu<List<MediaResult>> uuVar, boolean z) {
        ArrayList arrayList = new ArrayList();
        MediaResult b = this.b.b(i);
        if (b != null) {
            if (b.e() != null && b.l() != null) {
                Locale locale = Locale.US;
                Object[] objArr = new Object[1];
                objArr[0] = Boolean.valueOf(i2 == -1);
                i.a("Belvedere", String.format(locale, "Parsing activity result - Camera - Ok: %s", objArr));
                this.a.m(context, b.l(), 3);
                if (i2 == -1) {
                    MediaResult j = vt3.j(context, b.l());
                    arrayList.add(new MediaResult(b.e(), b.l(), b.j(), b.h(), j.g(), j.k(), j.o(), j.f()));
                    i.a("Belvedere", String.format(locale, "Image from camera: %s", b.e()));
                }
                this.b.a(i);
            } else {
                Locale locale2 = Locale.US;
                Object[] objArr2 = new Object[1];
                objArr2[0] = Boolean.valueOf(i2 == -1);
                i.a("Belvedere", String.format(locale2, "Parsing activity result - Gallery - Ok: %s", objArr2));
                if (i2 == -1) {
                    List<Uri> b2 = b(intent);
                    i.a("Belvedere", String.format(locale2, "Number of items received from gallery: %s", Integer.valueOf(b2.size())));
                    if (z) {
                        i.a("Belvedere", "Resolving items");
                        q73.c(context, this.a, uuVar, b2);
                        return;
                    }
                    i.a("Belvedere", "Resolving items turned off");
                    for (Uri uri : b2) {
                        arrayList.add(vt3.j(context, uri));
                    }
                }
            }
        }
        if (uuVar != null) {
            uuVar.internalSuccess(arrayList);
        }
    }

    public MediaIntent f(int i, String str, boolean z, List<String> list) {
        if (h(this.c)) {
            return new MediaIntent(i, d(str, z, list), null, true, 1);
        }
        return MediaIntent.e();
    }

    public final boolean g(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.media.action.IMAGE_CAPTURE");
        PackageManager packageManager = context.getPackageManager();
        boolean z = packageManager.hasSystemFeature("android.hardware.camera") || packageManager.hasSystemFeature("android.hardware.camera.front");
        boolean i = i(intent, context);
        i.a("Belvedere", String.format(Locale.US, "Camera present: %b, Camera App present: %b", Boolean.valueOf(z), Boolean.valueOf(i)));
        return z && i;
    }

    public final boolean h(Context context) {
        return i(d("*/*", false, new ArrayList()), context);
    }

    public final boolean i(Intent intent, Context context) {
        return context.getPackageManager().queryIntentActivities(intent, 0).size() > 0;
    }

    public final jp2<MediaIntent, MediaResult> j(Context context, int i) {
        File e = this.a.e(context);
        if (e == null) {
            i.e("Belvedere", "Camera Intent: Image path is null. There's something wrong with the storage.");
            return null;
        }
        Uri i2 = this.a.i(context, e);
        if (i2 == null) {
            i.e("Belvedere", "Camera Intent: Uri to file is null. There's something wrong with the storage or FileProvider configuration.");
            return null;
        }
        i.a("Belvedere", String.format(Locale.US, "Camera Intent: Request Id: %s - File: %s - Uri: %s", Integer.valueOf(i), e, i2));
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", i2);
        this.a.l(context, intent, i2, 3);
        boolean z = lq2.a(context, "android.permission.CAMERA") && !lq2.b(context, "android.permission.CAMERA");
        MediaResult j = vt3.j(context, i2);
        return new jp2<>(new MediaIntent(i, intent, z ? "android.permission.CAMERA" : null, true, 2), new MediaResult(e, i2, i2, e.getName(), j.g(), j.k(), j.o(), j.f()));
    }
}
