package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import com.bumptech.glide.a;

/* compiled from: BitmapTransformation.java */
/* renamed from: qq  reason: default package */
/* loaded from: classes.dex */
public abstract class qq implements za4<Bitmap> {
    @Override // defpackage.za4
    public final s73<Bitmap> a(Context context, s73<Bitmap> s73Var, int i, int i2) {
        if (mg4.t(i, i2)) {
            jq f = a.c(context).f();
            Bitmap bitmap = s73Var.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap c = c(f, bitmap, i, i2);
            return bitmap.equals(c) ? s73Var : oq.f(c, f);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }

    public abstract Bitmap c(jq jqVar, Bitmap bitmap, int i, int i2);
}
