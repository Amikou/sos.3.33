package defpackage;

import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: u15  reason: default package */
/* loaded from: classes.dex */
public final class u15 extends m05 {
    public final /* synthetic */ Intent a;
    public final /* synthetic */ nz1 f0;
    public final /* synthetic */ int g0 = 2;

    public u15(Intent intent, nz1 nz1Var, int i) {
        this.a = intent;
        this.f0 = nz1Var;
    }

    @Override // defpackage.m05
    public final void c() {
        Intent intent = this.a;
        if (intent != null) {
            this.f0.startActivityForResult(intent, this.g0);
        }
    }
}
