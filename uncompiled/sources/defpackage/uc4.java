package defpackage;

import java.util.Collections;
import java.util.List;

/* compiled from: Tx3gSubtitle.java */
/* renamed from: uc4  reason: default package */
/* loaded from: classes.dex */
public final class uc4 implements qv3 {
    public static final uc4 f0 = new uc4();
    public final List<kb0> a;

    public uc4(kb0 kb0Var) {
        this.a = Collections.singletonList(kb0Var);
    }

    @Override // defpackage.qv3
    public int a(long j) {
        return j < 0 ? 0 : -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        ii.a(i == 0);
        return 0L;
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return j >= 0 ? this.a : Collections.emptyList();
    }

    @Override // defpackage.qv3
    public int f() {
        return 1;
    }

    public uc4() {
        this.a = Collections.emptyList();
    }
}
