package defpackage;

/* compiled from: KProperty.kt */
/* renamed from: yw1  reason: default package */
/* loaded from: classes2.dex */
public interface yw1<V> extends pw1<V> {

    /* compiled from: KProperty.kt */
    /* renamed from: yw1$a */
    /* loaded from: classes2.dex */
    public interface a<V> extends sw1<V> {
    }

    boolean isConst();

    boolean isLateinit();
}
