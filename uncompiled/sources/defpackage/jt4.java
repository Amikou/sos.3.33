package defpackage;

/* renamed from: jt4  reason: default package */
/* loaded from: classes2.dex */
public abstract class jt4 implements Runnable {
    public final tx4<?> a;

    public jt4() {
        this.a = null;
    }

    public jt4(tx4<?> tx4Var) {
        this.a = tx4Var;
    }

    public abstract void a();

    public final tx4<?> b() {
        return this.a;
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            a();
        } catch (Exception e) {
            tx4<?> tx4Var = this.a;
            if (tx4Var != null) {
                tx4Var.d(e);
            }
        }
    }
}
