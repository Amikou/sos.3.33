package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzs;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: g56  reason: default package */
/* loaded from: classes.dex */
public final class g56 implements Parcelable.Creator<zzs> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzs createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        long j = 0;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 2) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 3) {
                i2 = SafeParcelReader.E(parcel, C);
            } else if (v == 4) {
                i3 = SafeParcelReader.E(parcel, C);
            } else if (v == 5) {
                j = SafeParcelReader.F(parcel, C);
            } else if (v != 6) {
                SafeParcelReader.I(parcel, C);
            } else {
                i4 = SafeParcelReader.E(parcel, C);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzs(i, i2, i3, j, i4);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzs[] newArray(int i) {
        return new zzs[i];
    }
}
