package defpackage;

import androidx.media3.common.util.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: WebvttSubtitle.java */
/* renamed from: zo4  reason: default package */
/* loaded from: classes.dex */
public final class zo4 implements qv3 {
    public final List<to4> a;
    public final long[] f0;
    public final long[] g0;

    public zo4(List<to4> list) {
        this.a = Collections.unmodifiableList(new ArrayList(list));
        this.f0 = new long[list.size() * 2];
        for (int i = 0; i < list.size(); i++) {
            to4 to4Var = list.get(i);
            int i2 = i * 2;
            long[] jArr = this.f0;
            jArr[i2] = to4Var.b;
            jArr[i2 + 1] = to4Var.c;
        }
        long[] jArr2 = this.f0;
        long[] copyOf = Arrays.copyOf(jArr2, jArr2.length);
        this.g0 = copyOf;
        Arrays.sort(copyOf);
    }

    public static /* synthetic */ int c(to4 to4Var, to4 to4Var2) {
        return Long.compare(to4Var.b, to4Var2.b);
    }

    @Override // defpackage.qv3
    public int a(long j) {
        int e = b.e(this.g0, j, false, false);
        if (e < this.g0.length) {
            return e;
        }
        return -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        ii.a(i >= 0);
        ii.a(i < this.g0.length);
        return this.g0[i];
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i = 0; i < this.a.size(); i++) {
            long[] jArr = this.f0;
            int i2 = i * 2;
            if (jArr[i2] <= j && j < jArr[i2 + 1]) {
                to4 to4Var = this.a.get(i);
                kb0 kb0Var = to4Var.a;
                if (kb0Var.i0 == -3.4028235E38f) {
                    arrayList2.add(to4Var);
                } else {
                    arrayList.add(kb0Var);
                }
            }
        }
        Collections.sort(arrayList2, yo4.a);
        for (int i3 = 0; i3 < arrayList2.size(); i3++) {
            arrayList.add(((to4) arrayList2.get(i3)).a.b().h((-1) - i3, 1).a());
        }
        return arrayList;
    }

    @Override // defpackage.qv3
    public int f() {
        return this.g0.length;
    }
}
