package defpackage;

import android.os.RemoteException;
import com.google.android.gms.measurement.internal.d;
import com.google.android.gms.measurement.internal.p;
import com.google.android.gms.measurement.internal.zzp;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: er5  reason: default package */
/* loaded from: classes.dex */
public final class er5 implements Runnable {
    public final /* synthetic */ zzp a;
    public final /* synthetic */ p f0;

    public er5(p pVar, zzp zzpVar) {
        this.f0 = pVar;
        this.a = zzpVar;
    }

    @Override // java.lang.Runnable
    public final void run() {
        d dVar;
        dVar = this.f0.d;
        if (dVar == null) {
            this.f0.a.w().l().a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            zt2.j(this.a);
            dVar.f1(this.a);
            this.f0.a.I().t();
            this.f0.K(dVar, null, this.a);
            this.f0.D();
        } catch (RemoteException e) {
            this.f0.a.w().l().b("Failed to send app launch to the service", e);
        }
    }
}
