package defpackage;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import android.text.TextUtils;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/* compiled from: SystemJobScheduler.java */
/* renamed from: y24  reason: default package */
/* loaded from: classes.dex */
public class y24 implements cd3 {
    public static final String i0 = v12.f("SystemJobScheduler");
    public final Context a;
    public final JobScheduler f0;
    public final hq4 g0;
    public final x24 h0;

    public y24(Context context, hq4 hq4Var) {
        this(context, hq4Var, (JobScheduler) context.getSystemService("jobscheduler"), new x24(context));
    }

    public static void b(Context context) {
        List<JobInfo> g;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler == null || (g = g(context, jobScheduler)) == null || g.isEmpty()) {
            return;
        }
        for (JobInfo jobInfo : g) {
            c(jobScheduler, jobInfo.getId());
        }
    }

    public static void c(JobScheduler jobScheduler, int i) {
        try {
            jobScheduler.cancel(i);
        } catch (Throwable th) {
            v12.c().b(i0, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", Integer.valueOf(i)), th);
        }
    }

    public static List<Integer> f(Context context, JobScheduler jobScheduler, String str) {
        List<JobInfo> g = g(context, jobScheduler);
        if (g == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(2);
        for (JobInfo jobInfo : g) {
            if (str.equals(h(jobInfo))) {
                arrayList.add(Integer.valueOf(jobInfo.getId()));
            }
        }
        return arrayList;
    }

    public static List<JobInfo> g(Context context, JobScheduler jobScheduler) {
        List<JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            v12.c().b(i0, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ComponentName componentName = new ComponentName(context, SystemJobService.class);
        for (JobInfo jobInfo : list) {
            if (componentName.equals(jobInfo.getService())) {
                arrayList.add(jobInfo);
            }
        }
        return arrayList;
    }

    public static String h(JobInfo jobInfo) {
        PersistableBundle extras = jobInfo.getExtras();
        if (extras != null) {
            try {
                if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                    return extras.getString("EXTRA_WORK_SPEC_ID");
                }
                return null;
            } catch (NullPointerException unused) {
                return null;
            }
        }
        return null;
    }

    public static boolean i(Context context, hq4 hq4Var) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        List<JobInfo> g = g(context, jobScheduler);
        List<String> a = hq4Var.q().M().a();
        boolean z = false;
        HashSet hashSet = new HashSet(g != null ? g.size() : 0);
        if (g != null && !g.isEmpty()) {
            for (JobInfo jobInfo : g) {
                String h = h(jobInfo);
                if (!TextUtils.isEmpty(h)) {
                    hashSet.add(h);
                } else {
                    c(jobScheduler, jobInfo.getId());
                }
            }
        }
        Iterator<String> it = a.iterator();
        while (true) {
            if (it.hasNext()) {
                if (!hashSet.contains(it.next())) {
                    v12.c().a(i0, "Reconciling jobs", new Throwable[0]);
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (z) {
            WorkDatabase q = hq4Var.q();
            q.e();
            try {
                uq4 P = q.P();
                for (String str : a) {
                    P.b(str, -1L);
                }
                q.E();
            } finally {
                q.j();
            }
        }
        return z;
    }

    @Override // defpackage.cd3
    public boolean a() {
        return true;
    }

    @Override // defpackage.cd3
    public void d(String str) {
        List<Integer> f = f(this.a, this.f0, str);
        if (f == null || f.isEmpty()) {
            return;
        }
        for (Integer num : f) {
            c(this.f0, num.intValue());
        }
        this.g0.q().M().d(str);
    }

    @Override // defpackage.cd3
    public void e(tq4... tq4VarArr) {
        int d;
        List<Integer> f;
        int d2;
        WorkDatabase q = this.g0.q();
        kn1 kn1Var = new kn1(q);
        for (tq4 tq4Var : tq4VarArr) {
            q.e();
            try {
                tq4 l = q.P().l(tq4Var.a);
                if (l == null) {
                    v12.c().h(i0, "Skipping scheduling " + tq4Var.a + " because it's no longer in the DB", new Throwable[0]);
                    q.E();
                } else if (l.b != WorkInfo.State.ENQUEUED) {
                    v12.c().h(i0, "Skipping scheduling " + tq4Var.a + " because it is no longer enqueued", new Throwable[0]);
                    q.E();
                } else {
                    u24 c = q.M().c(tq4Var.a);
                    if (c != null) {
                        d = c.b;
                    } else {
                        d = kn1Var.d(this.g0.k().i(), this.g0.k().g());
                    }
                    if (c == null) {
                        this.g0.q().M().b(new u24(tq4Var.a, d));
                    }
                    j(tq4Var, d);
                    if (Build.VERSION.SDK_INT == 23 && (f = f(this.a, this.f0, tq4Var.a)) != null) {
                        int indexOf = f.indexOf(Integer.valueOf(d));
                        if (indexOf >= 0) {
                            f.remove(indexOf);
                        }
                        if (!f.isEmpty()) {
                            d2 = f.get(0).intValue();
                        } else {
                            d2 = kn1Var.d(this.g0.k().i(), this.g0.k().g());
                        }
                        j(tq4Var, d2);
                    }
                    q.E();
                }
                q.j();
            } catch (Throwable th) {
                q.j();
                throw th;
            }
        }
    }

    public void j(tq4 tq4Var, int i) {
        JobInfo a = this.h0.a(tq4Var, i);
        v12 c = v12.c();
        String str = i0;
        c.a(str, String.format("Scheduling work ID %s Job ID %s", tq4Var.a, Integer.valueOf(i)), new Throwable[0]);
        try {
            if (this.f0.schedule(a) == 0) {
                v12.c().h(str, String.format("Unable to schedule work ID %s", tq4Var.a), new Throwable[0]);
                if (tq4Var.q && tq4Var.r == OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST) {
                    tq4Var.q = false;
                    v12.c().a(str, String.format("Scheduling a non-expedited job (work ID %s)", tq4Var.a), new Throwable[0]);
                    j(tq4Var, i);
                }
            }
        } catch (IllegalStateException e) {
            List<JobInfo> g = g(this.a, this.f0);
            String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", Integer.valueOf(g != null ? g.size() : 0), Integer.valueOf(this.g0.q().P().f().size()), Integer.valueOf(this.g0.k().h()));
            v12.c().b(i0, format, new Throwable[0]);
            throw new IllegalStateException(format, e);
        } catch (Throwable th) {
            v12.c().b(i0, String.format("Unable to schedule %s", tq4Var), th);
        }
    }

    public y24(Context context, hq4 hq4Var, JobScheduler jobScheduler, x24 x24Var) {
        this.a = context;
        this.g0 = hq4Var;
        this.f0 = jobScheduler;
        this.h0 = x24Var;
    }
}
