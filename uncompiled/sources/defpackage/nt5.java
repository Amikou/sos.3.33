package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: nt5  reason: default package */
/* loaded from: classes.dex */
public final class nt5 {
    public final Object a;
    public final int b;

    public nt5(Object obj, int i) {
        this.a = obj;
        this.b = i;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof nt5) {
            nt5 nt5Var = (nt5) obj;
            return this.a == nt5Var.a && this.b == nt5Var.b;
        }
        return false;
    }

    public final int hashCode() {
        return (System.identityHashCode(this.a) * 65535) + this.b;
    }
}
