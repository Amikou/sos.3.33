package defpackage;

import android.content.Context;
import androidx.annotation.RecentlyNonNull;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
@Deprecated
/* renamed from: ql4  reason: default package */
/* loaded from: classes.dex */
public class ql4 {
    public static ql4 a = new ql4();

    @RecentlyNonNull
    public static ql4 a() {
        return a;
    }

    public void b(@RecentlyNonNull Context context, @RecentlyNonNull String str, @RecentlyNonNull int i, @RecentlyNonNull String str2, @RecentlyNonNull String str3, @RecentlyNonNull String str4, @RecentlyNonNull int i2, @RecentlyNonNull List<String> list) {
    }

    public void c(@RecentlyNonNull Context context, @RecentlyNonNull String str, @RecentlyNonNull int i, @RecentlyNonNull String str2, @RecentlyNonNull String str3, @RecentlyNonNull String str4, @RecentlyNonNull int i2, @RecentlyNonNull List<String> list, @RecentlyNonNull long j) {
    }
}
