package defpackage;

import androidx.media3.common.ParserException;
import java.io.IOException;

/* compiled from: EbmlProcessor.java */
/* renamed from: zt0  reason: default package */
/* loaded from: classes.dex */
public interface zt0 {
    void a(int i) throws ParserException;

    int b(int i);

    boolean c(int i);

    void d(int i, String str) throws ParserException;

    void e(int i, double d) throws ParserException;

    void f(int i, int i2, q11 q11Var) throws IOException;

    void g(int i, long j, long j2) throws ParserException;

    void h(int i, long j) throws ParserException;
}
