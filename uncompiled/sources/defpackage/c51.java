package defpackage;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.a;
import com.google.firebase.FirebaseCommonRegistrar;
import com.google.firebase.components.ComponentDiscoveryService;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Marker;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: FirebaseApp.java */
/* renamed from: c51  reason: default package */
/* loaded from: classes2.dex */
public class c51 {
    public static final Object i = new Object();
    public static final Executor j = new d();
    public static final Map<String, c51> k = new rh();
    public final Context a;
    public final String b;
    public final y51 c;
    public final l40 d;
    public final ty1<ce0> g;
    public final AtomicBoolean e = new AtomicBoolean(false);
    public final AtomicBoolean f = new AtomicBoolean();
    public final List<b> h = new CopyOnWriteArrayList();

    /* compiled from: FirebaseApp.java */
    /* renamed from: c51$b */
    /* loaded from: classes2.dex */
    public interface b {
        void a(boolean z);
    }

    /* compiled from: FirebaseApp.java */
    @TargetApi(14)
    /* renamed from: c51$c */
    /* loaded from: classes2.dex */
    public static class c implements a.InterfaceC0109a {
        public static AtomicReference<c> a = new AtomicReference<>();

        public static void c(Context context) {
            if (jr2.a() && (context.getApplicationContext() instanceof Application)) {
                Application application = (Application) context.getApplicationContext();
                if (a.get() == null) {
                    c cVar = new c();
                    if (a.compareAndSet(null, cVar)) {
                        com.google.android.gms.common.api.internal.a.c(application);
                        com.google.android.gms.common.api.internal.a.b().a(cVar);
                    }
                }
            }
        }

        @Override // com.google.android.gms.common.api.internal.a.InterfaceC0109a
        public void a(boolean z) {
            synchronized (c51.i) {
                Iterator it = new ArrayList(c51.k.values()).iterator();
                while (it.hasNext()) {
                    c51 c51Var = (c51) it.next();
                    if (c51Var.e.get()) {
                        c51Var.u(z);
                    }
                }
            }
        }
    }

    /* compiled from: FirebaseApp.java */
    /* renamed from: c51$d */
    /* loaded from: classes2.dex */
    public static class d implements Executor {
        public static final Handler a = new Handler(Looper.getMainLooper());

        public d() {
        }

        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            a.post(runnable);
        }
    }

    /* compiled from: FirebaseApp.java */
    @TargetApi(24)
    /* renamed from: c51$e */
    /* loaded from: classes2.dex */
    public static class e extends BroadcastReceiver {
        public static AtomicReference<e> b = new AtomicReference<>();
        public final Context a;

        public e(Context context) {
            this.a = context;
        }

        public static void b(Context context) {
            if (b.get() == null) {
                e eVar = new e(context);
                if (b.compareAndSet(null, eVar)) {
                    context.registerReceiver(eVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        public void c() {
            this.a.unregisterReceiver(this);
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            synchronized (c51.i) {
                for (c51 c51Var : c51.k.values()) {
                    c51Var.m();
                }
            }
            c();
        }
    }

    public c51(final Context context, String str, y51 y51Var) {
        new CopyOnWriteArrayList();
        this.a = (Context) zt2.j(context);
        this.b = zt2.f(str);
        this.c = (y51) zt2.j(y51Var);
        this.d = l40.i(j).d(d40.c(context, ComponentDiscoveryService.class).b()).c(new FirebaseCommonRegistrar()).b(a40.p(context, Context.class, new Class[0])).b(a40.p(this, c51.class, new Class[0])).b(a40.p(y51Var, y51.class, new Class[0])).e();
        this.g = new ty1<>(new fw2() { // from class: b51
            @Override // defpackage.fw2
            public final Object get() {
                ce0 s;
                s = c51.this.s(context);
                return s;
            }
        });
    }

    public static c51 i() {
        c51 c51Var;
        synchronized (i) {
            c51Var = k.get("[DEFAULT]");
            if (c51Var == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + com.google.android.gms.common.util.b.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return c51Var;
    }

    public static c51 n(Context context) {
        synchronized (i) {
            if (k.containsKey("[DEFAULT]")) {
                return i();
            }
            y51 a2 = y51.a(context);
            if (a2 == null) {
                return null;
            }
            return o(context, a2);
        }
    }

    public static c51 o(Context context, y51 y51Var) {
        return p(context, y51Var, "[DEFAULT]");
    }

    public static c51 p(Context context, y51 y51Var, String str) {
        c51 c51Var;
        c.c(context);
        String t = t(str);
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (i) {
            Map<String, c51> map = k;
            boolean z = !map.containsKey(t);
            zt2.n(z, "FirebaseApp name " + t + " already exists!");
            zt2.k(context, "Application context cannot be null.");
            c51Var = new c51(context, t, y51Var);
            map.put(t, c51Var);
        }
        c51Var.m();
        return c51Var;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ ce0 s(Context context) {
        return new ce0(context, l(), (ow2) this.d.a(ow2.class));
    }

    public static String t(String str) {
        return str.trim();
    }

    public boolean equals(Object obj) {
        if (obj instanceof c51) {
            return this.b.equals(((c51) obj).j());
        }
        return false;
    }

    public final void f() {
        zt2.n(!this.f.get(), "FirebaseApp was deleted");
    }

    public <T> T g(Class<T> cls) {
        f();
        return (T) this.d.a(cls);
    }

    public Context h() {
        f();
        return this.a;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String j() {
        f();
        return this.b;
    }

    public y51 k() {
        f();
        return this.c;
    }

    public String l() {
        return mm.c(j().getBytes(Charset.defaultCharset())) + Marker.ANY_NON_NULL_MARKER + mm.c(k().c().getBytes(Charset.defaultCharset()));
    }

    public final void m() {
        if (!yf4.a(this.a)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Device in Direct Boot Mode: postponing initialization of Firebase APIs for app ");
            sb.append(j());
            e.b(this.a);
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Device unlocked: initializing all Firebase APIs for app ");
        sb2.append(j());
        this.d.l(r());
    }

    public boolean q() {
        f();
        return this.g.get().b();
    }

    public boolean r() {
        return "[DEFAULT]".equals(j());
    }

    public String toString() {
        return pl2.c(this).a(PublicResolver.FUNC_NAME, this.b).a("options", this.c).toString();
    }

    public final void u(boolean z) {
        for (b bVar : this.h) {
            bVar.a(z);
        }
    }
}
