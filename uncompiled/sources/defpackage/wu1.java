package defpackage;

import com.google.firebase.encoders.EncodingException;
import com.google.firebase.encoders.c;
import com.google.firebase.encoders.d;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/* compiled from: JsonDataEncoderBuilder.java */
/* renamed from: wu1  reason: default package */
/* loaded from: classes2.dex */
public final class wu1 implements fv0<wu1> {
    public static final hl2<Object> e = tu1.a;
    public static final zg4<String> f = vu1.a;
    public static final zg4<Boolean> g = uu1.a;
    public static final b h = new b(null);
    public final Map<Class<?>, hl2<?>> a = new HashMap();
    public final Map<Class<?>, zg4<?>> b = new HashMap();
    public hl2<Object> c = e;
    public boolean d = false;

    /* compiled from: JsonDataEncoderBuilder.java */
    /* renamed from: wu1$a */
    /* loaded from: classes2.dex */
    public class a implements com.google.firebase.encoders.a {
        public a() {
        }

        @Override // com.google.firebase.encoders.a
        public void a(Object obj, Writer writer) throws IOException {
            lw1 lw1Var = new lw1(writer, wu1.this.a, wu1.this.b, wu1.this.c, wu1.this.d);
            lw1Var.i(obj, false);
            lw1Var.r();
        }

        @Override // com.google.firebase.encoders.a
        public String b(Object obj) {
            StringWriter stringWriter = new StringWriter();
            try {
                a(obj, stringWriter);
            } catch (IOException unused) {
            }
            return stringWriter.toString();
        }
    }

    /* compiled from: JsonDataEncoderBuilder.java */
    /* renamed from: wu1$b */
    /* loaded from: classes2.dex */
    public static final class b implements zg4<Date> {
        public static final DateFormat a;

        static {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            a = simpleDateFormat;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        public b() {
        }

        @Override // com.google.firebase.encoders.b
        /* renamed from: b */
        public void a(Date date, d dVar) throws IOException {
            dVar.b(a.format(date));
        }

        public /* synthetic */ b(a aVar) {
            this();
        }
    }

    public wu1() {
        p(String.class, f);
        p(Boolean.class, g);
        p(Date.class, h);
    }

    public static /* synthetic */ void l(Object obj, c cVar) throws IOException {
        throw new EncodingException("Couldn't find encoder for type " + obj.getClass().getCanonicalName());
    }

    public static /* synthetic */ void n(Boolean bool, d dVar) throws IOException {
        dVar.c(bool.booleanValue());
    }

    public com.google.firebase.encoders.a i() {
        return new a();
    }

    public wu1 j(b50 b50Var) {
        b50Var.a(this);
        return this;
    }

    public wu1 k(boolean z) {
        this.d = z;
        return this;
    }

    @Override // defpackage.fv0
    /* renamed from: o */
    public <T> wu1 a(Class<T> cls, hl2<? super T> hl2Var) {
        this.a.put(cls, hl2Var);
        this.b.remove(cls);
        return this;
    }

    public <T> wu1 p(Class<T> cls, zg4<? super T> zg4Var) {
        this.b.put(cls, zg4Var);
        this.a.remove(cls);
        return this;
    }
}
