package defpackage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;

/* compiled from: CardViewApi21Impl.java */
/* renamed from: bw  reason: default package */
/* loaded from: classes.dex */
public class bw implements ew {
    @Override // defpackage.ew
    public void a(dw dwVar, float f) {
        p(dwVar).h(f);
    }

    @Override // defpackage.ew
    public float b(dw dwVar) {
        return e(dwVar) * 2.0f;
    }

    @Override // defpackage.ew
    public void c(dw dwVar) {
        o(dwVar, i(dwVar));
    }

    @Override // defpackage.ew
    public void d(dw dwVar) {
        if (!dwVar.e()) {
            dwVar.a(0, 0, 0, 0);
            return;
        }
        float i = i(dwVar);
        float e = e(dwVar);
        int ceil = (int) Math.ceil(r93.c(i, e, dwVar.d()));
        int ceil2 = (int) Math.ceil(r93.d(i, e, dwVar.d()));
        dwVar.a(ceil, ceil2, ceil, ceil2);
    }

    @Override // defpackage.ew
    public float e(dw dwVar) {
        return p(dwVar).d();
    }

    @Override // defpackage.ew
    public ColorStateList f(dw dwVar) {
        return p(dwVar).b();
    }

    @Override // defpackage.ew
    public float g(dw dwVar) {
        return e(dwVar) * 2.0f;
    }

    @Override // defpackage.ew
    public void h(dw dwVar, float f) {
        dwVar.g().setElevation(f);
    }

    @Override // defpackage.ew
    public float i(dw dwVar) {
        return p(dwVar).c();
    }

    @Override // defpackage.ew
    public void j(dw dwVar) {
        o(dwVar, i(dwVar));
    }

    @Override // defpackage.ew
    public float k(dw dwVar) {
        return dwVar.g().getElevation();
    }

    @Override // defpackage.ew
    public void l() {
    }

    @Override // defpackage.ew
    public void m(dw dwVar, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        dwVar.c(new q93(colorStateList, f));
        View g = dwVar.g();
        g.setClipToOutline(true);
        g.setElevation(f2);
        o(dwVar, f3);
    }

    @Override // defpackage.ew
    public void n(dw dwVar, ColorStateList colorStateList) {
        p(dwVar).f(colorStateList);
    }

    @Override // defpackage.ew
    public void o(dw dwVar, float f) {
        p(dwVar).g(f, dwVar.e(), dwVar.d());
        d(dwVar);
    }

    public final q93 p(dw dwVar) {
        return (q93) dwVar.f();
    }
}
