package defpackage;

import android.content.Context;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import net.safemoon.androidwallet.model.token.abstraction.IToken;
import net.safemoon.androidwallet.ui.displayModel.UserTokenItemDisplayModel;

/* compiled from: UserTokenDisplayModelMapper.kt */
/* renamed from: dg4  reason: default package */
/* loaded from: classes2.dex */
public final class dg4 implements an1 {
    public final Context a;

    public dg4(Context context) {
        fs1.f(context, "context");
        this.a = context;
    }

    @Override // defpackage.an1
    public List<UserTokenItemDisplayModel> a(List<? extends IToken> list) {
        double d;
        fs1.f(list, "tokenList");
        ArrayList arrayList = new ArrayList();
        for (IToken iToken : list) {
            int identifier = cv3.l(iToken.getIconResName()) == null ? this.a.getResources().getIdentifier(iToken.getIconResName(), "drawable", this.a.getPackageName()) : 0;
            String iconResName = iToken.getIconResName();
            String name = iToken.getName();
            String symbol = iToken.getSymbol();
            String contractAddress = iToken.getContractAddress();
            int decimals = iToken.getDecimals();
            int chainId = iToken.getChainId();
            String symbolWithType = iToken.getSymbolWithType();
            boolean allowSwap = iToken.getAllowSwap();
            double priceInUsdt = iToken.getPriceInUsdt();
            double nativeBalance = iToken.getNativeBalance();
            double percentChange1h = iToken.getPercentChange1h();
            try {
                d = iToken.getPriceInUsdt() * iToken.getNativeBalance();
            } catch (Exception unused) {
                d = Utils.DOUBLE_EPSILON;
            }
            arrayList.add(new UserTokenItemDisplayModel(symbolWithType, identifier, name, symbol, contractAddress, decimals, chainId, allowSwap, priceInUsdt, nativeBalance, percentChange1h, d, iconResName, kt.b(iToken.getContractAddress(), iToken.getSymbol(), cv3.l(iToken.getIconResName()))));
        }
        return arrayList;
    }
}
