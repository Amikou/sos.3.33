package defpackage;

import java.io.IOException;
import java.math.BigInteger;
import org.web3j.protocol.exceptions.TransactionException;
import org.web3j.tx.exceptions.ContractCallException;

/* compiled from: TransactionManager.java */
/* renamed from: u84  reason: default package */
/* loaded from: classes3.dex */
public abstract class u84 {
    public static final int DEFAULT_POLLING_ATTEMPTS_PER_TX_HASH = 40;
    public static final long DEFAULT_POLLING_FREQUENCY = 15000;
    public static final String REVERT_ERR_STR = "Contract Call has been reverted by the EVM with the reason: '%s'.";
    private final String fromAddress;
    private final x84 transactionReceiptProcessor;

    public u84(x84 x84Var, String str) {
        this.transactionReceiptProcessor = x84Var;
        this.fromAddress = str;
    }

    public static void assertCallNotReverted(iw0 iw0Var) {
        if (iw0Var.isReverted()) {
            throw new ContractCallException(String.format(REVERT_ERR_STR, iw0Var.getRevertReason()));
        }
    }

    private w84 processResponse(hx0 hx0Var) throws IOException, TransactionException {
        if (!hx0Var.hasError()) {
            return this.transactionReceiptProcessor.waitForTransactionReceipt(hx0Var.getTransactionHash());
        }
        throw new RuntimeException("Error processing transaction request: " + hx0Var.getError().getMessage());
    }

    public w84 executeTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3) throws IOException, TransactionException {
        return executeTransaction(bigInteger, bigInteger2, str, str2, bigInteger3, false);
    }

    public w84 executeTransactionEIP1559(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2, BigInteger bigInteger4) throws IOException, TransactionException {
        return executeTransactionEIP1559(bigInteger, bigInteger2, bigInteger3, str, str2, bigInteger4, false);
    }

    public abstract vw0 getCode(String str, gi0 gi0Var) throws IOException;

    public String getFromAddress() {
        return this.fromAddress;
    }

    public abstract String sendCall(String str, String str2, gi0 gi0Var) throws IOException;

    public hx0 sendTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3) throws IOException {
        return sendTransaction(bigInteger, bigInteger2, str, str2, bigInteger3, false);
    }

    public abstract hx0 sendTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3, boolean z) throws IOException;

    public hx0 sendTransactionEIP1559(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2, BigInteger bigInteger4) throws IOException {
        return sendTransactionEIP1559(bigInteger, bigInteger2, bigInteger3, str, str2, bigInteger4, false);
    }

    public abstract hx0 sendTransactionEIP1559(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2, BigInteger bigInteger4, boolean z) throws IOException;

    public w84 executeTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3, boolean z) throws IOException, TransactionException {
        return processResponse(sendTransaction(bigInteger, bigInteger2, str, str2, bigInteger3, z));
    }

    public w84 executeTransactionEIP1559(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2, BigInteger bigInteger4, boolean z) throws IOException, TransactionException {
        return processResponse(sendTransactionEIP1559(bigInteger, bigInteger2, bigInteger3, str, str2, bigInteger4, z));
    }

    public u84(ko4 ko4Var, String str) {
        this(new ns2(ko4Var, DEFAULT_POLLING_FREQUENCY, 40), str);
    }

    public u84(ko4 ko4Var, int i, long j, String str) {
        this(new ns2(ko4Var, j, i), str);
    }
}
