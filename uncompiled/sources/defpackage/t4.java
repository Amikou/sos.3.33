package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;

/* compiled from: AbstractCoroutine.kt */
/* renamed from: t4  reason: default package */
/* loaded from: classes2.dex */
public abstract class t4<T> extends bu1 implements st1, q70<T> {
    public final CoroutineContext f0;

    public t4(CoroutineContext coroutineContext, boolean z, boolean z2) {
        super(z2);
        if (z) {
            d0((st1) coroutineContext.get(st1.f));
        }
        this.f0 = coroutineContext.plus(this);
    }

    public void H0(Object obj) {
        E(obj);
    }

    public void I0(Throwable th, boolean z) {
    }

    public void J0(T t) {
    }

    public final <R> void K0(CoroutineStart coroutineStart, R r, hd1<? super R, ? super q70<? super T>, ? extends Object> hd1Var) {
        coroutineStart.invoke(hd1Var, r, this);
    }

    @Override // defpackage.bu1
    public String N() {
        return fs1.l(ff0.a(this), " was cancelled");
    }

    @Override // defpackage.bu1, defpackage.st1
    public boolean b() {
        return super.b();
    }

    @Override // defpackage.bu1
    public final void c0(Throwable th) {
        z80.a(this.f0, th);
    }

    @Override // defpackage.q70
    public final CoroutineContext getContext() {
        return this.f0;
    }

    public CoroutineContext m() {
        return this.f0;
    }

    @Override // defpackage.bu1
    public String m0() {
        String b = x80.b(this.f0);
        if (b == null) {
            return super.m0();
        }
        return '\"' + b + "\":" + super.m0();
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.bu1
    public final void r0(Object obj) {
        if (obj instanceof t30) {
            t30 t30Var = (t30) obj;
            I0(t30Var.a, t30Var.a());
            return;
        }
        J0(obj);
    }

    @Override // defpackage.q70
    public final void resumeWith(Object obj) {
        Object k0 = k0(w30.d(obj, null, 1, null));
        if (k0 == cu1.b) {
            return;
        }
        H0(k0);
    }
}
