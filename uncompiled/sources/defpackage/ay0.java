package defpackage;

/* compiled from: EventLoop.common.kt */
/* renamed from: ay0  reason: default package */
/* loaded from: classes2.dex */
public final class ay0 {
    public static final k24 a = new k24("REMOVED_TASK");
    public static final k24 b = new k24("CLOSED_EMPTY");

    public static final long c(long j) {
        if (j <= 0) {
            return 0L;
        }
        if (j >= 9223372036854L) {
            return Long.MAX_VALUE;
        }
        return 1000000 * j;
    }
}
