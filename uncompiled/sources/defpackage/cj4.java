package defpackage;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import com.google.android.material.button.MaterialButton;
import java.util.Objects;

/* compiled from: View.kt */
/* renamed from: cj4  reason: default package */
/* loaded from: classes2.dex */
public final class cj4 {

    /* compiled from: TextView.kt */
    /* renamed from: cj4$a */
    /* loaded from: classes2.dex */
    public static final class a implements TextWatcher {
        public final /* synthetic */ TextView a;
        public final /* synthetic */ tc1 f0;

        public a(TextView textView, tc1 tc1Var) {
            this.a = textView;
            this.f0 = tc1Var;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            if (this.a.hasFocus()) {
                this.f0.invoke(editable);
            }
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: TextView.kt */
    /* renamed from: cj4$b */
    /* loaded from: classes2.dex */
    public static final class b implements TextWatcher {
        public final /* synthetic */ View a;
        public final /* synthetic */ EditText f0;

        public b(View view, EditText editText) {
            this.a = view;
            this.f0 = editText;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj;
            String str = "";
            if (editable != null && (obj = editable.toString()) != null) {
                str = obj;
            }
            View view = this.a;
            boolean z = true;
            if (!(str.length() > 0) || !(!dv3.w(str))) {
                z = false;
            }
            view.setVisibility(z ? 0 : 8);
            this.a.setOnClickListener(new c(this.f0));
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: View.kt */
    /* renamed from: cj4$c */
    /* loaded from: classes2.dex */
    public static final class c implements View.OnClickListener {
        public final /* synthetic */ EditText a;

        public c(EditText editText) {
            this.a = editText;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            this.a.setText("");
        }
    }

    /* compiled from: TextView.kt */
    /* renamed from: cj4$d */
    /* loaded from: classes2.dex */
    public static final class d implements TextWatcher {
        public final /* synthetic */ View a;
        public final /* synthetic */ EditText f0;

        public d(View view, EditText editText) {
            this.a = view;
            this.f0 = editText;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String obj;
            String str = "";
            if (editable != null && (obj = editable.toString()) != null) {
                str = obj;
            }
            View view = this.a;
            boolean z = true;
            if (!(str.length() > 0) || !(!dv3.w(str))) {
                z = false;
            }
            view.setVisibility(z ? 0 : 8);
            this.a.setOnClickListener(new e(this.f0, str));
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: View.kt */
    /* renamed from: cj4$e */
    /* loaded from: classes2.dex */
    public static final class e implements View.OnClickListener {
        public final /* synthetic */ EditText a;
        public final /* synthetic */ String f0;

        public e(EditText editText, String str) {
            this.a = editText;
            this.f0 = str;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Context context = this.a.getContext();
            fs1.e(context, "context");
            e30.h(context, this.f0);
        }
    }

    /* compiled from: TextView.kt */
    /* renamed from: cj4$f */
    /* loaded from: classes2.dex */
    public static final class f implements TextWatcher {
        public final /* synthetic */ EditText a;
        public final /* synthetic */ zh4 f0;
        public final /* synthetic */ rc1 g0;

        public f(EditText editText, zh4 zh4Var, rc1 rc1Var) {
            this.a = editText;
            this.f0 = zh4Var;
            this.g0 = rc1Var;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            boolean z = true;
            boolean z2 = this.a.getText().toString().length() == 0;
            this.f0.a.setVisibility(e30.l0(z2));
            MaterialButton materialButton = this.f0.b;
            fs1.e(materialButton, "btnPaste");
            materialButton.setVisibility(z2 ? 0 : 8);
            AppCompatImageView appCompatImageView = this.f0.c;
            fs1.e(appCompatImageView, "btnQr");
            if (!z2 || this.g0 == null) {
                z = false;
            }
            appCompatImageView.setVisibility(z ? 0 : 8);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* compiled from: TextView.kt */
    /* renamed from: cj4$g */
    /* loaded from: classes2.dex */
    public static final class g implements TextWatcher {
        public final /* synthetic */ EditText a;
        public final /* synthetic */ ij4 f0;

        public g(EditText editText, ij4 ij4Var) {
            this.a = editText;
            this.f0 = ij4Var;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            boolean z = this.a.getText().toString().length() == 0;
            this.f0.a.setVisibility(e30.l0(z));
            MaterialButton materialButton = this.f0.b;
            fs1.e(materialButton, "btnPaste");
            materialButton.setVisibility(z ? 0 : 8);
            AppCompatImageView appCompatImageView = this.f0.c;
            fs1.e(appCompatImageView, "btnQr");
            appCompatImageView.setVisibility(z ? 0 : 8);
            ImageView imageView = this.f0.d;
            fs1.e(imageView, "btnSelectContact");
            imageView.setVisibility(z ? 0 : 8);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public static final void i(TextView textView, tc1<? super Editable, te4> tc1Var) {
        fs1.f(textView, "<this>");
        fs1.f(tc1Var, "action");
        textView.addTextChangedListener(new a(textView, tc1Var));
    }

    public static final void j(CompoundButton compoundButton, final tc1<? super Boolean, te4> tc1Var) {
        fs1.f(compoundButton, "<this>");
        fs1.f(tc1Var, "callback");
        compoundButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: bj4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton2, boolean z) {
                cj4.k(tc1.this, compoundButton2, z);
            }
        });
    }

    public static final void k(tc1 tc1Var, CompoundButton compoundButton, boolean z) {
        fs1.f(tc1Var, "$callback");
        if (compoundButton.isPressed()) {
            tc1Var.invoke(Boolean.valueOf(z));
        }
    }

    public static final void l(EditText editText, View view) {
        fs1.f(editText, "<this>");
        fs1.f(view, "viewClear");
        editText.addTextChangedListener(new b(view, editText));
    }

    public static final void m(EditText editText, View view) {
        fs1.f(editText, "<this>");
        fs1.f(view, "viewCopy");
        editText.addTextChangedListener(new d(view, editText));
    }

    public static final void n(final zh4 zh4Var, final EditText editText, final rc1<te4> rc1Var) {
        fs1.f(zh4Var, "<this>");
        fs1.f(editText, "editText");
        AppCompatImageView appCompatImageView = zh4Var.c;
        fs1.e(appCompatImageView, "btnQr");
        appCompatImageView.setVisibility(rc1Var != null ? 0 : 8);
        Object systemService = editText.getContext().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        final ClipboardManager clipboardManager = (ClipboardManager) systemService;
        editText.addTextChangedListener(new f(editText, zh4Var, rc1Var));
        zh4Var.b.setOnClickListener(new View.OnClickListener() { // from class: wi4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                cj4.t(clipboardManager, editText, zh4Var, rc1Var, view);
            }
        });
        zh4Var.c.setOnClickListener(new View.OnClickListener() { // from class: aj4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                cj4.u(editText, rc1Var, view);
            }
        });
        zh4Var.a.setOnClickListener(new View.OnClickListener() { // from class: yi4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                cj4.v(editText, view);
            }
        });
        editText.setText("");
    }

    public static final void o(final ij4 ij4Var, final EditText editText, final rc1<te4> rc1Var, final rc1<te4> rc1Var2, final rc1<te4> rc1Var3) {
        fs1.f(ij4Var, "<this>");
        fs1.f(editText, "editText");
        AppCompatImageView appCompatImageView = ij4Var.c;
        fs1.e(appCompatImageView, "btnQr");
        appCompatImageView.setVisibility(rc1Var != null ? 0 : 8);
        Object systemService = editText.getContext().getSystemService("clipboard");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.content.ClipboardManager");
        final ClipboardManager clipboardManager = (ClipboardManager) systemService;
        editText.addTextChangedListener(new g(editText, ij4Var));
        ij4Var.b.setOnClickListener(new View.OnClickListener() { // from class: vi4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                cj4.p(clipboardManager, editText, rc1Var3, ij4Var, view);
            }
        });
        ij4Var.c.setOnClickListener(new View.OnClickListener() { // from class: zi4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                cj4.q(editText, rc1Var, view);
            }
        });
        ij4Var.a.setOnClickListener(new View.OnClickListener() { // from class: xi4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                cj4.r(editText, view);
            }
        });
        ImageView imageView = ij4Var.d;
        if (imageView != null) {
            imageView.setOnClickListener(new View.OnClickListener() { // from class: ui4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    cj4.s(rc1.this, view);
                }
            });
        }
        editText.setText("");
    }

    public static final void p(ClipboardManager clipboardManager, EditText editText, rc1 rc1Var, ij4 ij4Var, View view) {
        fs1.f(clipboardManager, "$manager");
        fs1.f(editText, "$editText");
        fs1.f(ij4Var, "$this_setUpDefaultView");
        ClipData primaryClip = clipboardManager.getPrimaryClip();
        if (primaryClip != null && primaryClip.getItemCount() > 0) {
            if (primaryClip.getItemAt(0).getText() != null) {
                ClipData.Item itemAt = primaryClip.getItemAt(0);
                MaterialButton materialButton = ij4Var.a;
                fs1.e(materialButton, "btnClear");
                materialButton.setVisibility(0);
                editText.setText(itemAt.getText());
                boolean z = editText.getText().toString().length() == 0;
                ij4Var.a.setVisibility(e30.l0(z));
                MaterialButton materialButton2 = ij4Var.b;
                fs1.e(materialButton2, "btnPaste");
                materialButton2.setVisibility(z ? 0 : 8);
                AppCompatImageView appCompatImageView = ij4Var.c;
                fs1.e(appCompatImageView, "btnQr");
                appCompatImageView.setVisibility(z ? 0 : 8);
                ImageView imageView = ij4Var.d;
                fs1.e(imageView, "btnSelectContact");
                imageView.setVisibility(z ? 0 : 8);
            }
        }
        pg4.f(editText.getContext());
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public static final void q(EditText editText, rc1 rc1Var, View view) {
        fs1.f(editText, "$editText");
        pg4.f(editText.getContext());
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public static final void r(EditText editText, View view) {
        fs1.f(editText, "$editText");
        editText.setText("");
    }

    public static final void s(rc1 rc1Var, View view) {
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public static final void t(ClipboardManager clipboardManager, EditText editText, zh4 zh4Var, rc1 rc1Var, View view) {
        fs1.f(clipboardManager, "$manager");
        fs1.f(editText, "$editText");
        fs1.f(zh4Var, "$this_setUpDefaultView");
        ClipData primaryClip = clipboardManager.getPrimaryClip();
        if (primaryClip != null && primaryClip.getItemCount() > 0) {
            if (primaryClip.getItemAt(0).getText() != null) {
                ClipData.Item itemAt = primaryClip.getItemAt(0);
                MaterialButton materialButton = zh4Var.a;
                fs1.e(materialButton, "btnClear");
                materialButton.setVisibility(0);
                editText.setText(itemAt.getText());
                boolean z = true;
                boolean z2 = editText.getText().toString().length() == 0;
                zh4Var.a.setVisibility(e30.l0(z2));
                MaterialButton materialButton2 = zh4Var.b;
                fs1.e(materialButton2, "btnPaste");
                materialButton2.setVisibility(z2 ? 0 : 8);
                AppCompatImageView appCompatImageView = zh4Var.c;
                fs1.e(appCompatImageView, "btnQr");
                if (!z2 || rc1Var == null) {
                    z = false;
                }
                appCompatImageView.setVisibility(z ? 0 : 8);
            }
        }
        pg4.f(editText.getContext());
    }

    public static final void u(EditText editText, rc1 rc1Var, View view) {
        fs1.f(editText, "$editText");
        pg4.f(editText.getContext());
        if (rc1Var == null) {
            return;
        }
        rc1Var.invoke();
    }

    public static final void v(EditText editText, View view) {
        fs1.f(editText, "$editText");
        editText.setText("");
    }

    public static final void w(EditText editText, Object obj) {
        String obj2;
        fs1.f(editText, "<this>");
        if (obj == null || (obj2 = obj.toString()) == null) {
            return;
        }
        boolean hasFocus = editText.hasFocus();
        int i = -99;
        if (hasFocus) {
            i = editText.getSelectionEnd();
            editText.clearFocus();
        }
        editText.setText(obj2);
        if (hasFocus) {
            editText.requestFocus();
            try {
                editText.setSelection(i);
            } catch (Exception unused) {
            }
        }
    }
}
