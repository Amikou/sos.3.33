package defpackage;

/* compiled from: CustomConfirmation.kt */
/* renamed from: gt  reason: default package */
/* loaded from: classes2.dex */
public final class gt {
    public final Integer a;
    public final String b;
    public final int c;
    public final boolean d;

    public gt(Integer num, String str, int i, boolean z) {
        this.a = num;
        this.b = str;
        this.c = i;
        this.d = z;
    }

    public final int a() {
        return this.c;
    }

    public final Integer b() {
        return this.a;
    }

    public final String c() {
        return this.b;
    }

    public final boolean d() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof gt) {
            gt gtVar = (gt) obj;
            return fs1.b(this.a, gtVar.a) && fs1.b(this.b, gtVar.b) && this.c == gtVar.c && this.d == gtVar.d;
        }
        return false;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        Integer num = this.a;
        int hashCode = (num == null ? 0 : num.hashCode()) * 31;
        String str = this.b;
        int hashCode2 = (((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.c) * 31;
        boolean z = this.d;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "CCButton(itemResource=" + this.a + ", itemTxt=" + ((Object) this.b) + ", actionId=" + this.c + ", isClickAble=" + this.d + ')';
    }

    public /* synthetic */ gt(Integer num, String str, int i, boolean z, int i2, qi0 qi0Var) {
        this((i2 & 1) != 0 ? null : num, (i2 & 2) != 0 ? null : str, i, (i2 & 8) != 0 ? false : z);
    }
}
