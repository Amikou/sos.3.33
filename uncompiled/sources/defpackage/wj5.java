package defpackage;

import com.google.android.gms.internal.measurement.c1;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: wj5  reason: default package */
/* loaded from: classes.dex */
public final class wj5 extends w1<c1, wj5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public wj5() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.c1 r0 = com.google.android.gms.internal.measurement.c1.y()
            r1.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.wj5.<init>():void");
    }

    public final wj5 v(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        c1.z((c1) this.f0, str);
        return this;
    }

    public final wj5 x(long j) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        c1.A((c1) this.f0, j);
        return this;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ wj5(defpackage.wi5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.c1 r1 = com.google.android.gms.internal.measurement.c1.y()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.wj5.<init>(wi5):void");
    }
}
