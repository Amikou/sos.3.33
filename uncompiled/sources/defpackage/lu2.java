package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;

/* compiled from: PreferenceUtils.java */
/* renamed from: lu2  reason: default package */
/* loaded from: classes.dex */
public class lu2 {
    public final WorkDatabase a;

    public lu2(WorkDatabase workDatabase) {
        this.a = workDatabase;
    }

    public static void b(Context context, sw3 sw3Var) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.preferences", 0);
        if (sharedPreferences.contains("reschedule_needed") || sharedPreferences.contains("last_cancel_all_time_ms")) {
            long j = sharedPreferences.getLong("last_cancel_all_time_ms", 0L);
            long j2 = sharedPreferences.getBoolean("reschedule_needed", false) ? 1L : 0L;
            sw3Var.B();
            try {
                sw3Var.u0("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"last_cancel_all_time_ms", Long.valueOf(j)});
                sw3Var.u0("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", Long.valueOf(j2)});
                sharedPreferences.edit().clear().apply();
                sw3Var.s0();
            } finally {
                sw3Var.N0();
            }
        }
    }

    public boolean a() {
        Long a = this.a.L().a("reschedule_needed");
        return a != null && a.longValue() == 1;
    }

    public void c(boolean z) {
        this.a.L().b(new hu2("reschedule_needed", z));
    }
}
