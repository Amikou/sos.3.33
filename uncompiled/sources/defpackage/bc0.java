package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: CurveFit.java */
/* renamed from: bc0  reason: default package */
/* loaded from: classes.dex */
public abstract class bc0 {

    /* compiled from: CurveFit.java */
    /* renamed from: bc0$a */
    /* loaded from: classes.dex */
    public static class a extends bc0 {
        public double a;
        public double[] b;

        public a(double d, double[] dArr) {
            this.a = d;
            this.b = dArr;
        }

        @Override // defpackage.bc0
        public double c(double d, int i) {
            return this.b[i];
        }

        @Override // defpackage.bc0
        public void d(double d, double[] dArr) {
            double[] dArr2 = this.b;
            System.arraycopy(dArr2, 0, dArr, 0, dArr2.length);
        }

        @Override // defpackage.bc0
        public void e(double d, float[] fArr) {
            int i = 0;
            while (true) {
                double[] dArr = this.b;
                if (i >= dArr.length) {
                    return;
                }
                fArr[i] = (float) dArr[i];
                i++;
            }
        }

        @Override // defpackage.bc0
        public double f(double d, int i) {
            return Utils.DOUBLE_EPSILON;
        }

        @Override // defpackage.bc0
        public void g(double d, double[] dArr) {
            for (int i = 0; i < this.b.length; i++) {
                dArr[i] = 0.0d;
            }
        }

        @Override // defpackage.bc0
        public double[] h() {
            return new double[]{this.a};
        }
    }

    public static bc0 a(int i, double[] dArr, double[][] dArr2) {
        if (dArr.length == 1) {
            i = 2;
        }
        if (i != 0) {
            if (i != 2) {
                return new xz1(dArr, dArr2);
            }
            return new a(dArr[0], dArr2[0]);
        }
        return new p92(dArr, dArr2);
    }

    public static bc0 b(int[] iArr, double[] dArr, double[][] dArr2) {
        return new fh(iArr, dArr, dArr2);
    }

    public abstract double c(double d, int i);

    public abstract void d(double d, double[] dArr);

    public abstract void e(double d, float[] fArr);

    public abstract double f(double d, int i);

    public abstract void g(double d, double[] dArr);

    public abstract double[] h();
}
