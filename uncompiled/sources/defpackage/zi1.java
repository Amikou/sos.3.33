package defpackage;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.b;
import retrofit2.e;

/* compiled from: GsonRequestBodyConverter.java */
/* renamed from: zi1  reason: default package */
/* loaded from: classes3.dex */
public final class zi1<T> implements e<T, RequestBody> {
    public static final MediaType c = MediaType.get("application/json; charset=UTF-8");
    public static final Charset d = Charset.forName("UTF-8");
    public final Gson a;
    public final TypeAdapter<T> b;

    public zi1(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @Override // retrofit2.e
    /* renamed from: b */
    public RequestBody a(T t) throws IOException {
        b bVar = new b();
        JsonWriter newJsonWriter = this.a.newJsonWriter(new OutputStreamWriter(bVar.D1(), d));
        this.b.write(newJsonWriter, t);
        newJsonWriter.close();
        return RequestBody.create(c, bVar.R0());
    }
}
