package defpackage;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: NotificationCompat.java */
/* renamed from: dh2  reason: default package */
/* loaded from: classes.dex */
public class dh2 {

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$a */
    /* loaded from: classes.dex */
    public static class a {
        public final Bundle a;
        public IconCompat b;
        public final n63[] c;
        public final n63[] d;
        public boolean e;
        public boolean f;
        public final int g;
        public final boolean h;
        @Deprecated
        public int i;
        public CharSequence j;
        public PendingIntent k;

        public a(int i, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i != 0 ? IconCompat.c(null, "", i) : null, charSequence, pendingIntent);
        }

        public PendingIntent a() {
            return this.k;
        }

        public boolean b() {
            return this.e;
        }

        public n63[] c() {
            return this.d;
        }

        public Bundle d() {
            return this.a;
        }

        public IconCompat e() {
            int i;
            if (this.b == null && (i = this.i) != 0) {
                this.b = IconCompat.c(null, "", i);
            }
            return this.b;
        }

        public n63[] f() {
            return this.c;
        }

        public int g() {
            return this.g;
        }

        public boolean h() {
            return this.f;
        }

        public CharSequence i() {
            return this.j;
        }

        public boolean j() {
            return this.h;
        }

        public a(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent) {
            this(iconCompat, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false);
        }

        public a(IconCompat iconCompat, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, n63[] n63VarArr, n63[] n63VarArr2, boolean z, int i, boolean z2, boolean z3) {
            this.f = true;
            this.b = iconCompat;
            if (iconCompat != null && iconCompat.i() == 2) {
                this.i = iconCompat.e();
            }
            this.j = e.e(charSequence);
            this.k = pendingIntent;
            this.a = bundle == null ? new Bundle() : bundle;
            this.c = n63VarArr;
            this.d = n63VarArr2;
            this.e = z;
            this.g = i;
            this.f = z2;
            this.h = z3;
        }
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$b */
    /* loaded from: classes.dex */
    public static class b extends h {
        public Bitmap e;
        public IconCompat f;
        public boolean g;
        public boolean h;

        /* compiled from: NotificationCompat.java */
        /* renamed from: dh2$b$a */
        /* loaded from: classes.dex */
        public static class a {
            public static void a(Notification.BigPictureStyle bigPictureStyle, Bitmap bitmap) {
                bigPictureStyle.bigLargeIcon(bitmap);
            }

            public static void b(Notification.BigPictureStyle bigPictureStyle, CharSequence charSequence) {
                bigPictureStyle.setSummaryText(charSequence);
            }
        }

        /* compiled from: NotificationCompat.java */
        /* renamed from: dh2$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0166b {
            public static void a(Notification.BigPictureStyle bigPictureStyle, Icon icon) {
                bigPictureStyle.bigLargeIcon(icon);
            }
        }

        /* compiled from: NotificationCompat.java */
        /* renamed from: dh2$b$c */
        /* loaded from: classes.dex */
        public static class c {
            public static void a(Notification.BigPictureStyle bigPictureStyle, boolean z) {
                bigPictureStyle.showBigPictureWhenCollapsed(z);
            }
        }

        @Override // defpackage.dh2.h
        public void b(ch2 ch2Var) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 16) {
                Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(ch2Var.a()).setBigContentTitle(this.b).bigPicture(this.e);
                if (this.g) {
                    IconCompat iconCompat = this.f;
                    if (iconCompat == null) {
                        a.a(bigPicture, null);
                    } else if (i >= 23) {
                        C0166b.a(bigPicture, this.f.q(ch2Var instanceof eh2 ? ((eh2) ch2Var).f() : null));
                    } else if (iconCompat.i() == 1) {
                        a.a(bigPicture, this.f.d());
                    } else {
                        a.a(bigPicture, null);
                    }
                }
                if (this.d) {
                    a.b(bigPicture, this.c);
                }
                if (i >= 31) {
                    c.a(bigPicture, this.h);
                }
            }
        }

        @Override // defpackage.dh2.h
        public String c() {
            return "androidx.core.app.NotificationCompat$BigPictureStyle";
        }

        public b h(Bitmap bitmap) {
            this.f = bitmap == null ? null : IconCompat.b(bitmap);
            this.g = true;
            return this;
        }

        public b i(Bitmap bitmap) {
            this.e = bitmap;
            return this;
        }

        public b j(CharSequence charSequence) {
            this.c = e.e(charSequence);
            this.d = true;
            return this;
        }
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$c */
    /* loaded from: classes.dex */
    public static class c extends h {
        public CharSequence e;

        @Override // defpackage.dh2.h
        public void a(Bundle bundle) {
            super.a(bundle);
            if (Build.VERSION.SDK_INT < 21) {
                bundle.putCharSequence("android.bigText", this.e);
            }
        }

        @Override // defpackage.dh2.h
        public void b(ch2 ch2Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(ch2Var.a()).setBigContentTitle(this.b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }

        @Override // defpackage.dh2.h
        public String c() {
            return "androidx.core.app.NotificationCompat$BigTextStyle";
        }

        public c h(CharSequence charSequence) {
            this.e = e.e(charSequence);
            return this;
        }
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$d */
    /* loaded from: classes.dex */
    public static final class d {
        public static Notification.BubbleMetadata a(d dVar) {
            return null;
        }
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$f */
    /* loaded from: classes.dex */
    public interface f {
        e a(e eVar);
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$g */
    /* loaded from: classes.dex */
    public static class g extends h {
        public ArrayList<CharSequence> e = new ArrayList<>();

        @Override // defpackage.dh2.h
        public void b(ch2 ch2Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.InboxStyle bigContentTitle = new Notification.InboxStyle(ch2Var.a()).setBigContentTitle(this.b);
                if (this.d) {
                    bigContentTitle.setSummaryText(this.c);
                }
                Iterator<CharSequence> it = this.e.iterator();
                while (it.hasNext()) {
                    bigContentTitle.addLine(it.next());
                }
            }
        }

        @Override // defpackage.dh2.h
        public String c() {
            return "androidx.core.app.NotificationCompat$InboxStyle";
        }

        public g h(CharSequence charSequence) {
            if (charSequence != null) {
                this.e.add(e.e(charSequence));
            }
            return this;
        }

        public g i(CharSequence charSequence) {
            this.b = e.e(charSequence);
            return this;
        }
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$h */
    /* loaded from: classes.dex */
    public static abstract class h {
        public e a;
        public CharSequence b;
        public CharSequence c;
        public boolean d = false;

        public void a(Bundle bundle) {
            if (this.d) {
                bundle.putCharSequence("android.summaryText", this.c);
            }
            CharSequence charSequence = this.b;
            if (charSequence != null) {
                bundle.putCharSequence("android.title.big", charSequence);
            }
            String c = c();
            if (c != null) {
                bundle.putString("androidx.core.app.extra.COMPAT_TEMPLATE", c);
            }
        }

        public abstract void b(ch2 ch2Var);

        public abstract String c();

        public RemoteViews d(ch2 ch2Var) {
            return null;
        }

        public RemoteViews e(ch2 ch2Var) {
            return null;
        }

        public RemoteViews f(ch2 ch2Var) {
            return null;
        }

        public void g(e eVar) {
            if (this.a != eVar) {
                this.a = eVar;
                if (eVar != null) {
                    eVar.C(this);
                }
            }
        }
    }

    public static Bundle a(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return fh2.c(notification);
        }
        return null;
    }

    public static boolean b(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 20) {
            return (notification.flags & RecyclerView.a0.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
        } else if (i >= 19) {
            return notification.extras.getBoolean("android.support.isGroupSummary");
        } else {
            if (i >= 16) {
                return fh2.c(notification).getBoolean("android.support.isGroupSummary");
            }
            return false;
        }
    }

    /* compiled from: NotificationCompat.java */
    /* renamed from: dh2$e */
    /* loaded from: classes.dex */
    public static class e {
        public boolean A;
        public boolean B;
        public String C;
        public Bundle D;
        public int E;
        public int F;
        public Notification G;
        public RemoteViews H;
        public RemoteViews I;
        public RemoteViews J;
        public String K;
        public int L;
        public String M;
        public long N;
        public int O;
        public int P;
        public boolean Q;
        public d R;
        public Notification S;
        public boolean T;
        public Icon U;
        @Deprecated
        public ArrayList<String> V;
        public Context a;
        public ArrayList<a> b;
        public ArrayList<oq2> c;
        public ArrayList<a> d;
        public CharSequence e;
        public CharSequence f;
        public PendingIntent g;
        public PendingIntent h;
        public RemoteViews i;
        public Bitmap j;
        public CharSequence k;
        public int l;
        public int m;
        public boolean n;
        public boolean o;
        public h p;
        public CharSequence q;
        public CharSequence r;
        public CharSequence[] s;
        public int t;
        public int u;
        public boolean v;
        public String w;
        public boolean x;
        public String y;
        public boolean z;

        public e(Context context, String str) {
            this.b = new ArrayList<>();
            this.c = new ArrayList<>();
            this.d = new ArrayList<>();
            this.n = true;
            this.z = false;
            this.E = 0;
            this.F = 0;
            this.L = 0;
            this.O = 0;
            this.P = 0;
            Notification notification = new Notification();
            this.S = notification;
            this.a = context;
            this.K = str;
            notification.when = System.currentTimeMillis();
            this.S.audioStreamType = -1;
            this.m = 0;
            this.V = new ArrayList<>();
            this.Q = true;
        }

        public static CharSequence e(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        public e A(int i) {
            this.S.icon = i;
            return this;
        }

        public e B(Uri uri) {
            Notification notification = this.S;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }

        public e C(h hVar) {
            if (this.p != hVar) {
                this.p = hVar;
                if (hVar != null) {
                    hVar.g(this);
                }
            }
            return this;
        }

        public e D(CharSequence charSequence) {
            this.S.tickerText = e(charSequence);
            return this;
        }

        public e E(long[] jArr) {
            this.S.vibrate = jArr;
            return this;
        }

        public e F(int i) {
            this.F = i;
            return this;
        }

        public e G(long j) {
            this.S.when = j;
            return this;
        }

        public e a(int i, CharSequence charSequence, PendingIntent pendingIntent) {
            this.b.add(new a(i, charSequence, pendingIntent));
            return this;
        }

        public Notification b() {
            return new eh2(this).c();
        }

        public e c(f fVar) {
            fVar.a(this);
            return this;
        }

        public Bundle d() {
            if (this.D == null) {
                this.D = new Bundle();
            }
            return this.D;
        }

        public final Bitmap f(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(ez2.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(ez2.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() > dimensionPixelSize || bitmap.getHeight() > dimensionPixelSize2) {
                double min = Math.min(dimensionPixelSize / Math.max(1, bitmap.getWidth()), dimensionPixelSize2 / Math.max(1, bitmap.getHeight()));
                return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(bitmap.getWidth() * min), (int) Math.ceil(bitmap.getHeight() * min), true);
            }
            return bitmap;
        }

        public e g(boolean z) {
            p(16, z);
            return this;
        }

        public e h(String str) {
            this.K = str;
            return this;
        }

        public e i(int i) {
            this.E = i;
            return this;
        }

        public e j(RemoteViews remoteViews) {
            this.S.contentView = remoteViews;
            return this;
        }

        public e k(PendingIntent pendingIntent) {
            this.g = pendingIntent;
            return this;
        }

        public e l(CharSequence charSequence) {
            this.f = e(charSequence);
            return this;
        }

        public e m(CharSequence charSequence) {
            this.e = e(charSequence);
            return this;
        }

        public e n(int i) {
            Notification notification = this.S;
            notification.defaults = i;
            if ((i & 4) != 0) {
                notification.flags |= 1;
            }
            return this;
        }

        public e o(PendingIntent pendingIntent) {
            this.S.deleteIntent = pendingIntent;
            return this;
        }

        public final void p(int i, boolean z) {
            if (z) {
                Notification notification = this.S;
                notification.flags = i | notification.flags;
                return;
            }
            Notification notification2 = this.S;
            notification2.flags = (~i) & notification2.flags;
        }

        public e q(String str) {
            this.w = str;
            return this;
        }

        public e r(int i) {
            this.O = i;
            return this;
        }

        public e s(boolean z) {
            this.x = z;
            return this;
        }

        public e t(Bitmap bitmap) {
            this.j = f(bitmap);
            return this;
        }

        public e u(int i, int i2, int i3) {
            Notification notification = this.S;
            notification.ledARGB = i;
            notification.ledOnMS = i2;
            notification.ledOffMS = i3;
            notification.flags = ((i2 == 0 || i3 == 0) ? 0 : 1) | (notification.flags & (-2));
            return this;
        }

        public e v(boolean z) {
            this.z = z;
            return this;
        }

        public e w(int i) {
            this.l = i;
            return this;
        }

        public e x(boolean z) {
            p(8, z);
            return this;
        }

        public e y(int i) {
            this.m = i;
            return this;
        }

        public e z(boolean z) {
            this.n = z;
            return this;
        }

        @Deprecated
        public e(Context context) {
            this(context, null);
        }
    }
}
