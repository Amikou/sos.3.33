package defpackage;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import androidx.constraintlayout.core.widgets.analyzer.a;
import androidx.constraintlayout.core.widgets.analyzer.b;
import androidx.constraintlayout.core.widgets.analyzer.c;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.f;
import com.github.mikephil.charting.utils.Utils;
import defpackage.jo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: DependencyGraph.java */
/* renamed from: lm0  reason: default package */
/* loaded from: classes.dex */
public class lm0 {
    public d a;
    public d d;
    public jo.b f;
    public jo.a g;
    public ArrayList<aa3> h;
    public boolean b = true;
    public boolean c = true;
    public ArrayList<WidgetRun> e = new ArrayList<>();

    public lm0(d dVar) {
        new ArrayList();
        this.f = null;
        this.g = new jo.a();
        this.h = new ArrayList<>();
        this.a = dVar;
        this.d = dVar;
    }

    public final void a(DependencyNode dependencyNode, int i, int i2, DependencyNode dependencyNode2, ArrayList<aa3> arrayList, aa3 aa3Var) {
        WidgetRun widgetRun = dependencyNode.d;
        if (widgetRun.c == null) {
            d dVar = this.a;
            if (widgetRun == dVar.d || widgetRun == dVar.e) {
                return;
            }
            if (aa3Var == null) {
                aa3Var = new aa3(widgetRun, i2);
                arrayList.add(aa3Var);
            }
            widgetRun.c = aa3Var;
            aa3Var.a(widgetRun);
            for (im0 im0Var : widgetRun.h.k) {
                if (im0Var instanceof DependencyNode) {
                    a((DependencyNode) im0Var, i, 0, dependencyNode2, arrayList, aa3Var);
                }
            }
            for (im0 im0Var2 : widgetRun.i.k) {
                if (im0Var2 instanceof DependencyNode) {
                    a((DependencyNode) im0Var2, i, 1, dependencyNode2, arrayList, aa3Var);
                }
            }
            if (i == 1 && (widgetRun instanceof androidx.constraintlayout.core.widgets.analyzer.d)) {
                for (im0 im0Var3 : ((androidx.constraintlayout.core.widgets.analyzer.d) widgetRun).k.k) {
                    if (im0Var3 instanceof DependencyNode) {
                        a((DependencyNode) im0Var3, i, 2, dependencyNode2, arrayList, aa3Var);
                    }
                }
            }
            for (DependencyNode dependencyNode3 : widgetRun.h.l) {
                a(dependencyNode3, i, 0, dependencyNode2, arrayList, aa3Var);
            }
            for (DependencyNode dependencyNode4 : widgetRun.i.l) {
                a(dependencyNode4, i, 1, dependencyNode2, arrayList, aa3Var);
            }
            if (i == 1 && (widgetRun instanceof androidx.constraintlayout.core.widgets.analyzer.d)) {
                for (DependencyNode dependencyNode5 : ((androidx.constraintlayout.core.widgets.analyzer.d) widgetRun).k.l) {
                    a(dependencyNode5, i, 2, dependencyNode2, arrayList, aa3Var);
                }
            }
        }
    }

    public final boolean b(d dVar) {
        int i;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        int i2;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour3;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour4;
        Iterator<ConstraintWidget> it = dVar.P0.iterator();
        while (it.hasNext()) {
            ConstraintWidget next = it.next();
            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = next.X;
            ConstraintWidget.DimensionBehaviour dimensionBehaviour5 = dimensionBehaviourArr[0];
            ConstraintWidget.DimensionBehaviour dimensionBehaviour6 = dimensionBehaviourArr[1];
            if (next.U() == 8) {
                next.a = true;
            } else {
                if (next.x < 1.0f && dimensionBehaviour5 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    next.s = 2;
                }
                if (next.A < 1.0f && dimensionBehaviour6 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    next.t = 2;
                }
                if (next.x() > Utils.FLOAT_EPSILON) {
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour7 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    if (dimensionBehaviour5 == dimensionBehaviour7 && (dimensionBehaviour6 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || dimensionBehaviour6 == ConstraintWidget.DimensionBehaviour.FIXED)) {
                        next.s = 3;
                    } else if (dimensionBehaviour6 == dimensionBehaviour7 && (dimensionBehaviour5 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || dimensionBehaviour5 == ConstraintWidget.DimensionBehaviour.FIXED)) {
                        next.t = 3;
                    } else if (dimensionBehaviour5 == dimensionBehaviour7 && dimensionBehaviour6 == dimensionBehaviour7) {
                        if (next.s == 0) {
                            next.s = 3;
                        }
                        if (next.t == 0) {
                            next.t = 3;
                        }
                    }
                }
                ConstraintWidget.DimensionBehaviour dimensionBehaviour8 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (dimensionBehaviour5 == dimensionBehaviour8 && next.s == 1 && (next.M.f == null || next.O.f == null)) {
                    dimensionBehaviour5 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                }
                ConstraintWidget.DimensionBehaviour dimensionBehaviour9 = dimensionBehaviour5;
                if (dimensionBehaviour6 == dimensionBehaviour8 && next.t == 1 && (next.N.f == null || next.P.f == null)) {
                    dimensionBehaviour6 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                }
                ConstraintWidget.DimensionBehaviour dimensionBehaviour10 = dimensionBehaviour6;
                c cVar = next.d;
                cVar.d = dimensionBehaviour9;
                int i3 = next.s;
                cVar.a = i3;
                androidx.constraintlayout.core.widgets.analyzer.d dVar2 = next.e;
                dVar2.d = dimensionBehaviour10;
                int i4 = next.t;
                dVar2.a = i4;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour11 = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                if ((dimensionBehaviour9 == dimensionBehaviour11 || dimensionBehaviour9 == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviour9 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) && (dimensionBehaviour10 == dimensionBehaviour11 || dimensionBehaviour10 == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviour10 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT)) {
                    int V = next.V();
                    if (dimensionBehaviour9 == dimensionBehaviour11) {
                        i = (dVar.V() - next.M.g) - next.O.g;
                        dimensionBehaviour = ConstraintWidget.DimensionBehaviour.FIXED;
                    } else {
                        i = V;
                        dimensionBehaviour = dimensionBehaviour9;
                    }
                    int z = next.z();
                    if (dimensionBehaviour10 == dimensionBehaviour11) {
                        i2 = (dVar.z() - next.N.g) - next.P.g;
                        dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.FIXED;
                    } else {
                        i2 = z;
                        dimensionBehaviour2 = dimensionBehaviour10;
                    }
                    l(next, dimensionBehaviour, i, dimensionBehaviour2, i2);
                    next.d.e.d(next.V());
                    next.e.e.d(next.z());
                    next.a = true;
                } else {
                    if (dimensionBehaviour9 == dimensionBehaviour8 && (dimensionBehaviour10 == (dimensionBehaviour4 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) || dimensionBehaviour10 == ConstraintWidget.DimensionBehaviour.FIXED)) {
                        if (i3 == 3) {
                            if (dimensionBehaviour10 == dimensionBehaviour4) {
                                l(next, dimensionBehaviour4, 0, dimensionBehaviour4, 0);
                            }
                            int z2 = next.z();
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour12 = ConstraintWidget.DimensionBehaviour.FIXED;
                            l(next, dimensionBehaviour12, (int) ((z2 * next.b0) + 0.5f), dimensionBehaviour12, z2);
                            next.d.e.d(next.V());
                            next.e.e.d(next.z());
                            next.a = true;
                        } else if (i3 == 1) {
                            l(next, dimensionBehaviour4, 0, dimensionBehaviour10, 0);
                            next.d.e.m = next.V();
                        } else if (i3 == 2) {
                            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr2 = dVar.X;
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour13 = dimensionBehaviourArr2[0];
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour14 = ConstraintWidget.DimensionBehaviour.FIXED;
                            if (dimensionBehaviour13 == dimensionBehaviour14 || dimensionBehaviourArr2[0] == dimensionBehaviour11) {
                                l(next, dimensionBehaviour14, (int) ((next.x * dVar.V()) + 0.5f), dimensionBehaviour10, next.z());
                                next.d.e.d(next.V());
                                next.e.e.d(next.z());
                                next.a = true;
                            }
                        } else {
                            ConstraintAnchor[] constraintAnchorArr = next.U;
                            if (constraintAnchorArr[0].f == null || constraintAnchorArr[1].f == null) {
                                l(next, dimensionBehaviour4, 0, dimensionBehaviour10, 0);
                                next.d.e.d(next.V());
                                next.e.e.d(next.z());
                                next.a = true;
                            }
                        }
                    }
                    if (dimensionBehaviour10 == dimensionBehaviour8 && (dimensionBehaviour9 == (dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) || dimensionBehaviour9 == ConstraintWidget.DimensionBehaviour.FIXED)) {
                        if (i4 == 3) {
                            if (dimensionBehaviour9 == dimensionBehaviour3) {
                                l(next, dimensionBehaviour3, 0, dimensionBehaviour3, 0);
                            }
                            int V2 = next.V();
                            float f = next.b0;
                            if (next.y() == -1) {
                                f = 1.0f / f;
                            }
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour15 = ConstraintWidget.DimensionBehaviour.FIXED;
                            l(next, dimensionBehaviour15, V2, dimensionBehaviour15, (int) ((V2 * f) + 0.5f));
                            next.d.e.d(next.V());
                            next.e.e.d(next.z());
                            next.a = true;
                        } else if (i4 == 1) {
                            l(next, dimensionBehaviour9, 0, dimensionBehaviour3, 0);
                            next.e.e.m = next.z();
                        } else if (i4 == 2) {
                            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr3 = dVar.X;
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour16 = dimensionBehaviourArr3[1];
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour17 = ConstraintWidget.DimensionBehaviour.FIXED;
                            if (dimensionBehaviour16 == dimensionBehaviour17 || dimensionBehaviourArr3[1] == dimensionBehaviour11) {
                                l(next, dimensionBehaviour9, next.V(), dimensionBehaviour17, (int) ((next.A * dVar.z()) + 0.5f));
                                next.d.e.d(next.V());
                                next.e.e.d(next.z());
                                next.a = true;
                            }
                        } else {
                            ConstraintAnchor[] constraintAnchorArr2 = next.U;
                            if (constraintAnchorArr2[2].f == null || constraintAnchorArr2[3].f == null) {
                                l(next, dimensionBehaviour3, 0, dimensionBehaviour10, 0);
                                next.d.e.d(next.V());
                                next.e.e.d(next.z());
                                next.a = true;
                            }
                        }
                    }
                    if (dimensionBehaviour9 == dimensionBehaviour8 && dimensionBehaviour10 == dimensionBehaviour8) {
                        if (i3 == 1 || i4 == 1) {
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour18 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                            l(next, dimensionBehaviour18, 0, dimensionBehaviour18, 0);
                            next.d.e.m = next.V();
                            next.e.e.m = next.z();
                        } else if (i4 == 2 && i3 == 2) {
                            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr4 = dVar.X;
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour19 = dimensionBehaviourArr4[0];
                            ConstraintWidget.DimensionBehaviour dimensionBehaviour20 = ConstraintWidget.DimensionBehaviour.FIXED;
                            if (dimensionBehaviour19 == dimensionBehaviour20 && dimensionBehaviourArr4[1] == dimensionBehaviour20) {
                                l(next, dimensionBehaviour20, (int) ((next.x * dVar.V()) + 0.5f), dimensionBehaviour20, (int) ((next.A * dVar.z()) + 0.5f));
                                next.d.e.d(next.V());
                                next.e.e.d(next.z());
                                next.a = true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public void c() {
        d(this.e);
        this.h.clear();
        aa3.c = 0;
        i(this.a.d, 0, this.h);
        i(this.a.e, 1, this.h);
        this.b = false;
    }

    public void d(ArrayList<WidgetRun> arrayList) {
        arrayList.clear();
        this.d.d.f();
        this.d.e.f();
        arrayList.add(this.d.d);
        arrayList.add(this.d.e);
        Iterator<ConstraintWidget> it = this.d.P0.iterator();
        HashSet hashSet = null;
        while (it.hasNext()) {
            ConstraintWidget next = it.next();
            if (next instanceof f) {
                arrayList.add(new bj1(next));
            } else {
                if (next.h0()) {
                    if (next.b == null) {
                        next.b = new gx(next, 0);
                    }
                    if (hashSet == null) {
                        hashSet = new HashSet();
                    }
                    hashSet.add(next.b);
                } else {
                    arrayList.add(next.d);
                }
                if (next.j0()) {
                    if (next.c == null) {
                        next.c = new gx(next, 1);
                    }
                    if (hashSet == null) {
                        hashSet = new HashSet();
                    }
                    hashSet.add(next.c);
                } else {
                    arrayList.add(next.e);
                }
                if (next instanceof mk1) {
                    arrayList.add(new b(next));
                }
            }
        }
        if (hashSet != null) {
            arrayList.addAll(hashSet);
        }
        Iterator<WidgetRun> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            it2.next().f();
        }
        Iterator<WidgetRun> it3 = arrayList.iterator();
        while (it3.hasNext()) {
            WidgetRun next2 = it3.next();
            if (next2.b != this.d) {
                next2.d();
            }
        }
    }

    public final int e(d dVar, int i) {
        int size = this.h.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            j = Math.max(j, this.h.get(i2).b(dVar, i));
        }
        return (int) j;
    }

    public boolean f(boolean z) {
        boolean z2;
        boolean z3 = true;
        boolean z4 = z & true;
        if (this.b || this.c) {
            Iterator<ConstraintWidget> it = this.a.P0.iterator();
            while (it.hasNext()) {
                ConstraintWidget next = it.next();
                next.p();
                next.a = false;
                next.d.r();
                next.e.q();
            }
            this.a.p();
            d dVar = this.a;
            dVar.a = false;
            dVar.d.r();
            this.a.e.q();
            this.c = false;
        }
        if (b(this.d)) {
            return false;
        }
        this.a.j1(0);
        this.a.k1(0);
        ConstraintWidget.DimensionBehaviour w = this.a.w(0);
        ConstraintWidget.DimensionBehaviour w2 = this.a.w(1);
        if (this.b) {
            c();
        }
        int W = this.a.W();
        int X = this.a.X();
        this.a.d.h.d(W);
        this.a.e.h.d(X);
        m();
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (w == dimensionBehaviour || w2 == dimensionBehaviour) {
            if (z4) {
                Iterator<WidgetRun> it2 = this.e.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    } else if (!it2.next().m()) {
                        z4 = false;
                        break;
                    }
                }
            }
            if (z4 && w == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.a.M0(ConstraintWidget.DimensionBehaviour.FIXED);
                d dVar2 = this.a;
                dVar2.h1(e(dVar2, 0));
                d dVar3 = this.a;
                dVar3.d.e.d(dVar3.V());
            }
            if (z4 && w2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.a.d1(ConstraintWidget.DimensionBehaviour.FIXED);
                d dVar4 = this.a;
                dVar4.I0(e(dVar4, 1));
                d dVar5 = this.a;
                dVar5.e.e.d(dVar5.z());
            }
        }
        d dVar6 = this.a;
        ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = dVar6.X;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[0];
        ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.FIXED;
        if (dimensionBehaviour2 == dimensionBehaviour3 || dimensionBehaviourArr[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int V = dVar6.V() + W;
            this.a.d.i.d(V);
            this.a.d.e.d(V - W);
            m();
            d dVar7 = this.a;
            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr2 = dVar7.X;
            if (dimensionBehaviourArr2[1] == dimensionBehaviour3 || dimensionBehaviourArr2[1] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                int z5 = dVar7.z() + X;
                this.a.e.i.d(z5);
                this.a.e.e.d(z5 - X);
            }
            m();
            z2 = true;
        } else {
            z2 = false;
        }
        Iterator<WidgetRun> it3 = this.e.iterator();
        while (it3.hasNext()) {
            WidgetRun next2 = it3.next();
            if (next2.b != this.a || next2.g) {
                next2.e();
            }
        }
        Iterator<WidgetRun> it4 = this.e.iterator();
        while (it4.hasNext()) {
            WidgetRun next3 = it4.next();
            if (z2 || next3.b != this.a) {
                if (!next3.h.j || ((!next3.i.j && !(next3 instanceof bj1)) || (!next3.e.j && !(next3 instanceof gx) && !(next3 instanceof bj1)))) {
                    z3 = false;
                    break;
                }
            }
        }
        this.a.M0(w);
        this.a.d1(w2);
        return z3;
    }

    public boolean g(boolean z) {
        if (this.b) {
            Iterator<ConstraintWidget> it = this.a.P0.iterator();
            while (it.hasNext()) {
                ConstraintWidget next = it.next();
                next.p();
                next.a = false;
                c cVar = next.d;
                cVar.e.j = false;
                cVar.g = false;
                cVar.r();
                androidx.constraintlayout.core.widgets.analyzer.d dVar = next.e;
                dVar.e.j = false;
                dVar.g = false;
                dVar.q();
            }
            this.a.p();
            d dVar2 = this.a;
            dVar2.a = false;
            c cVar2 = dVar2.d;
            cVar2.e.j = false;
            cVar2.g = false;
            cVar2.r();
            androidx.constraintlayout.core.widgets.analyzer.d dVar3 = this.a.e;
            dVar3.e.j = false;
            dVar3.g = false;
            dVar3.q();
            c();
        }
        if (b(this.d)) {
            return false;
        }
        this.a.j1(0);
        this.a.k1(0);
        this.a.d.h.d(0);
        this.a.e.h.d(0);
        return true;
    }

    public boolean h(boolean z, int i) {
        boolean z2;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        boolean z3 = true;
        boolean z4 = z & true;
        ConstraintWidget.DimensionBehaviour w = this.a.w(0);
        ConstraintWidget.DimensionBehaviour w2 = this.a.w(1);
        int W = this.a.W();
        int X = this.a.X();
        if (z4 && (w == (dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) || w2 == dimensionBehaviour)) {
            Iterator<WidgetRun> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                WidgetRun next = it.next();
                if (next.f == i && !next.m()) {
                    z4 = false;
                    break;
                }
            }
            if (i == 0) {
                if (z4 && w == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    this.a.M0(ConstraintWidget.DimensionBehaviour.FIXED);
                    d dVar = this.a;
                    dVar.h1(e(dVar, 0));
                    d dVar2 = this.a;
                    dVar2.d.e.d(dVar2.V());
                }
            } else if (z4 && w2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.a.d1(ConstraintWidget.DimensionBehaviour.FIXED);
                d dVar3 = this.a;
                dVar3.I0(e(dVar3, 1));
                d dVar4 = this.a;
                dVar4.e.e.d(dVar4.z());
            }
        }
        if (i == 0) {
            d dVar5 = this.a;
            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = dVar5.X;
            if (dimensionBehaviourArr[0] == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviourArr[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                int V = dVar5.V() + W;
                this.a.d.i.d(V);
                this.a.d.e.d(V - W);
                z2 = true;
            }
            z2 = false;
        } else {
            d dVar6 = this.a;
            ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr2 = dVar6.X;
            if (dimensionBehaviourArr2[1] == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviourArr2[1] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                int z5 = dVar6.z() + X;
                this.a.e.i.d(z5);
                this.a.e.e.d(z5 - X);
                z2 = true;
            }
            z2 = false;
        }
        m();
        Iterator<WidgetRun> it2 = this.e.iterator();
        while (it2.hasNext()) {
            WidgetRun next2 = it2.next();
            if (next2.f == i && (next2.b != this.a || next2.g)) {
                next2.e();
            }
        }
        Iterator<WidgetRun> it3 = this.e.iterator();
        while (it3.hasNext()) {
            WidgetRun next3 = it3.next();
            if (next3.f == i && (z2 || next3.b != this.a)) {
                if (!next3.h.j || !next3.i.j || (!(next3 instanceof gx) && !next3.e.j)) {
                    z3 = false;
                    break;
                }
            }
        }
        this.a.M0(w);
        this.a.d1(w2);
        return z3;
    }

    public final void i(WidgetRun widgetRun, int i, ArrayList<aa3> arrayList) {
        for (im0 im0Var : widgetRun.h.k) {
            if (im0Var instanceof DependencyNode) {
                a((DependencyNode) im0Var, i, 0, widgetRun.i, arrayList, null);
            } else if (im0Var instanceof WidgetRun) {
                a(((WidgetRun) im0Var).h, i, 0, widgetRun.i, arrayList, null);
            }
        }
        for (im0 im0Var2 : widgetRun.i.k) {
            if (im0Var2 instanceof DependencyNode) {
                a((DependencyNode) im0Var2, i, 1, widgetRun.h, arrayList, null);
            } else if (im0Var2 instanceof WidgetRun) {
                a(((WidgetRun) im0Var2).i, i, 1, widgetRun.h, arrayList, null);
            }
        }
        if (i == 1) {
            for (im0 im0Var3 : ((androidx.constraintlayout.core.widgets.analyzer.d) widgetRun).k.k) {
                if (im0Var3 instanceof DependencyNode) {
                    a((DependencyNode) im0Var3, i, 2, null, arrayList, null);
                }
            }
        }
    }

    public void j() {
        this.b = true;
    }

    public void k() {
        this.c = true;
    }

    public final void l(ConstraintWidget constraintWidget, ConstraintWidget.DimensionBehaviour dimensionBehaviour, int i, ConstraintWidget.DimensionBehaviour dimensionBehaviour2, int i2) {
        jo.a aVar = this.g;
        aVar.a = dimensionBehaviour;
        aVar.b = dimensionBehaviour2;
        aVar.c = i;
        aVar.d = i2;
        this.f.b(constraintWidget, aVar);
        constraintWidget.h1(this.g.e);
        constraintWidget.I0(this.g.f);
        constraintWidget.H0(this.g.h);
        constraintWidget.x0(this.g.g);
    }

    public void m() {
        a aVar;
        Iterator<ConstraintWidget> it = this.a.P0.iterator();
        while (it.hasNext()) {
            ConstraintWidget next = it.next();
            if (!next.a) {
                ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = next.X;
                boolean z = false;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = dimensionBehaviourArr[0];
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = dimensionBehaviourArr[1];
                int i = next.s;
                int i2 = next.t;
                ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                boolean z2 = dimensionBehaviour == dimensionBehaviour3 || (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && i == 1);
                if (dimensionBehaviour2 == dimensionBehaviour3 || (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && i2 == 1)) {
                    z = true;
                }
                a aVar2 = next.d.e;
                boolean z3 = aVar2.j;
                a aVar3 = next.e.e;
                boolean z4 = aVar3.j;
                if (z3 && z4) {
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = ConstraintWidget.DimensionBehaviour.FIXED;
                    l(next, dimensionBehaviour4, aVar2.g, dimensionBehaviour4, aVar3.g);
                    next.a = true;
                } else if (z3 && z) {
                    l(next, ConstraintWidget.DimensionBehaviour.FIXED, aVar2.g, dimensionBehaviour3, aVar3.g);
                    if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        next.e.e.m = next.z();
                    } else {
                        next.e.e.d(next.z());
                        next.a = true;
                    }
                } else if (z4 && z2) {
                    l(next, dimensionBehaviour3, aVar2.g, ConstraintWidget.DimensionBehaviour.FIXED, aVar3.g);
                    if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        next.d.e.m = next.V();
                    } else {
                        next.d.e.d(next.V());
                        next.a = true;
                    }
                }
                if (next.a && (aVar = next.e.l) != null) {
                    aVar.d(next.r());
                }
            }
        }
    }

    public void n(jo.b bVar) {
        this.f = bVar;
    }
}
