package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: gy  reason: default package */
/* loaded from: classes2.dex */
public final class gy extends tt1 implements fy {
    public final hy i0;

    public gy(hy hyVar) {
        this.i0 = hyVar;
    }

    @Override // defpackage.fy
    public boolean d(Throwable th) {
        return z().O(th);
    }

    @Override // defpackage.fy
    public st1 getParent() {
        return z();
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        y(th);
        return te4.a;
    }

    @Override // defpackage.v30
    public void y(Throwable th) {
        this.i0.n(z());
    }
}
