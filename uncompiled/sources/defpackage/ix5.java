package defpackage;

import com.google.android.gms.tasks.c;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-tasks@@17.2.0 */
/* renamed from: ix5  reason: default package */
/* loaded from: classes.dex */
public final class ix5<TResult> implements l46<TResult> {
    public final Executor a;
    public final Object b = new Object();
    public nm2 c;

    public ix5(Executor executor, nm2 nm2Var) {
        this.a = executor;
        this.c = nm2Var;
    }

    @Override // defpackage.l46
    public final void c(c<TResult> cVar) {
        if (cVar.p() || cVar.n()) {
            return;
        }
        synchronized (this.b) {
            if (this.c == null) {
                return;
            }
            this.a.execute(new yu5(this, cVar));
        }
    }
}
