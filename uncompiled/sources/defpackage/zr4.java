package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: zr4  reason: default package */
/* loaded from: classes2.dex */
public class zr4 extends h {
    public final g a;
    public final int f0;
    public final va g0;

    public zr4(int i, va vaVar) {
        this.a = new g(0L);
        this.f0 = i;
        this.g0 = vaVar;
    }

    public zr4(h4 h4Var) {
        this.a = g.z(h4Var.D(0));
        this.f0 = g.z(h4Var.D(1)).B().intValue();
        this.g0 = va.p(h4Var.D(2));
    }

    public static zr4 p(Object obj) {
        if (obj instanceof zr4) {
            return (zr4) obj;
        }
        if (obj != null) {
            return new zr4(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(this.a);
        d4Var.a(new g(this.f0));
        d4Var.a(this.g0);
        return new n0(d4Var);
    }

    public int o() {
        return this.f0;
    }

    public va q() {
        return this.g0;
    }
}
