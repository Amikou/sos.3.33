package defpackage;

import kotlin.Pair;

/* compiled from: Tuples.kt */
/* renamed from: qc4  reason: default package */
/* loaded from: classes2.dex */
public final class qc4 {
    public static final <A, B> Pair<A, B> a(A a, B b) {
        return new Pair<>(a, b);
    }
}
