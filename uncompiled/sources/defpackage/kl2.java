package defpackage;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.f;

/* compiled from: ObjectIdWriter.java */
/* renamed from: kl2  reason: default package */
/* loaded from: classes.dex */
public final class kl2 {
    public final JavaType a;
    public final yl3 b;
    public final ObjectIdGenerator<?> c;
    public final f<Object> d;
    public final boolean e;

    public kl2(JavaType javaType, yl3 yl3Var, ObjectIdGenerator<?> objectIdGenerator, f<?> fVar, boolean z) {
        this.a = javaType;
        this.b = yl3Var;
        this.c = objectIdGenerator;
        this.d = fVar;
        this.e = z;
    }

    public static kl2 a(JavaType javaType, PropertyName propertyName, ObjectIdGenerator<?> objectIdGenerator, boolean z) {
        return b(javaType, propertyName == null ? null : propertyName.getSimpleName(), objectIdGenerator, z);
    }

    @Deprecated
    public static kl2 b(JavaType javaType, String str, ObjectIdGenerator<?> objectIdGenerator, boolean z) {
        return new kl2(javaType, str == null ? null : new SerializedString(str), objectIdGenerator, null, z);
    }

    public kl2 c(boolean z) {
        return z == this.e ? this : new kl2(this.a, this.b, this.c, this.d, z);
    }

    public kl2 d(f<?> fVar) {
        return new kl2(this.a, this.b, this.c, fVar, this.e);
    }
}
