package defpackage;

import java.util.List;

/* compiled from: EventValues.java */
/* renamed from: ky0  reason: default package */
/* loaded from: classes2.dex */
public class ky0 {
    private final List<zc4> indexedValues;
    private final List<zc4> nonIndexedValues;

    public ky0(List<zc4> list, List<zc4> list2) {
        this.indexedValues = list;
        this.nonIndexedValues = list2;
    }

    public List<zc4> getIndexedValues() {
        return this.indexedValues;
    }

    public List<zc4> getNonIndexedValues() {
        return this.nonIndexedValues;
    }
}
