package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.PlaybackException;
import androidx.media3.common.h;
import androidx.media3.common.j;
import androidx.media3.common.m;
import androidx.media3.common.n;
import androidx.media3.common.p;
import androidx.media3.common.q;
import androidx.media3.common.x;
import androidx.media3.common.y;
import defpackage.tb;
import java.util.List;

/* compiled from: AnalyticsListener.java */
/* renamed from: sb  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class sb {
    public static void A(tb tbVar, tb.a aVar) {
    }

    public static void B(tb tbVar, tb.a aVar, int i, long j) {
    }

    public static void C(tb tbVar, tb.a aVar, boolean z) {
    }

    public static void D(tb tbVar, tb.a aVar, boolean z) {
    }

    public static void E(tb tbVar, tb.a aVar, u02 u02Var, g62 g62Var) {
    }

    public static void F(tb tbVar, tb.a aVar, u02 u02Var, g62 g62Var) {
    }

    public static void G(tb tbVar, tb.a aVar, u02 u02Var, g62 g62Var) {
    }

    @Deprecated
    public static void H(tb tbVar, tb.a aVar, boolean z) {
    }

    public static void I(tb tbVar, tb.a aVar, m mVar, int i) {
    }

    public static void J(tb tbVar, tb.a aVar, n nVar) {
    }

    public static void K(tb tbVar, tb.a aVar, Metadata metadata) {
    }

    public static void L(tb tbVar, tb.a aVar, boolean z, int i) {
    }

    public static void M(tb tbVar, tb.a aVar, p pVar) {
    }

    public static void N(tb tbVar, tb.a aVar, int i) {
    }

    public static void O(tb tbVar, tb.a aVar, int i) {
    }

    public static void P(tb tbVar, tb.a aVar, PlaybackException playbackException) {
    }

    public static void Q(tb tbVar, tb.a aVar) {
    }

    @Deprecated
    public static void R(tb tbVar, tb.a aVar, boolean z, int i) {
    }

    @Deprecated
    public static void S(tb tbVar, tb.a aVar, int i) {
    }

    public static void T(tb tbVar, tb.a aVar, Object obj, long j) {
    }

    public static void U(tb tbVar, tb.a aVar, int i) {
    }

    @Deprecated
    public static void V(tb tbVar, tb.a aVar) {
    }

    @Deprecated
    public static void W(tb tbVar, tb.a aVar) {
    }

    public static void X(tb tbVar, tb.a aVar, boolean z) {
    }

    public static void Y(tb tbVar, tb.a aVar, boolean z) {
    }

    public static void Z(tb tbVar, tb.a aVar, int i, int i2) {
    }

    public static void a(tb tbVar, tb.a aVar, Exception exc) {
    }

    public static void a0(tb tbVar, tb.a aVar, int i) {
    }

    @Deprecated
    public static void b(tb tbVar, tb.a aVar, String str, long j) {
    }

    public static void b0(tb tbVar, tb.a aVar, x xVar) {
    }

    public static void c(tb tbVar, tb.a aVar, String str, long j, long j2) {
    }

    public static void c0(tb tbVar, tb.a aVar, y yVar) {
    }

    public static void d(tb tbVar, tb.a aVar, String str) {
    }

    public static void d0(tb tbVar, tb.a aVar, g62 g62Var) {
    }

    public static void e(tb tbVar, tb.a aVar, hf0 hf0Var) {
    }

    public static void e0(tb tbVar, tb.a aVar, Exception exc) {
    }

    public static void f(tb tbVar, tb.a aVar, hf0 hf0Var) {
    }

    @Deprecated
    public static void f0(tb tbVar, tb.a aVar, String str, long j) {
    }

    @Deprecated
    public static void g(tb tbVar, tb.a aVar, j jVar) {
    }

    public static void g0(tb tbVar, tb.a aVar, String str, long j, long j2) {
    }

    public static void h(tb tbVar, tb.a aVar, j jVar, jf0 jf0Var) {
    }

    public static void h0(tb tbVar, tb.a aVar, String str) {
    }

    public static void i(tb tbVar, tb.a aVar, long j) {
    }

    public static void i0(tb tbVar, tb.a aVar, hf0 hf0Var) {
    }

    public static void j(tb tbVar, tb.a aVar, Exception exc) {
    }

    public static void j0(tb tbVar, tb.a aVar, long j, int i) {
    }

    public static void k(tb tbVar, tb.a aVar, int i, long j, long j2) {
    }

    @Deprecated
    public static void k0(tb tbVar, tb.a aVar, j jVar) {
    }

    public static void l(tb tbVar, tb.a aVar, q.b bVar) {
    }

    public static void l0(tb tbVar, tb.a aVar, j jVar, jf0 jf0Var) {
    }

    public static void m(tb tbVar, tb.a aVar, nb0 nb0Var) {
    }

    @Deprecated
    public static void m0(tb tbVar, tb.a aVar, int i, int i2, int i3, float f) {
    }

    @Deprecated
    public static void n(tb tbVar, tb.a aVar, List list) {
    }

    @Deprecated
    public static void o(tb tbVar, tb.a aVar, int i, hf0 hf0Var) {
    }

    @Deprecated
    public static void p(tb tbVar, tb.a aVar, int i, hf0 hf0Var) {
    }

    @Deprecated
    public static void q(tb tbVar, tb.a aVar, int i, String str, long j) {
    }

    @Deprecated
    public static void r(tb tbVar, tb.a aVar, int i, j jVar) {
    }

    public static void s(tb tbVar, tb.a aVar, h hVar) {
    }

    public static void t(tb tbVar, tb.a aVar, int i, boolean z) {
    }

    public static void u(tb tbVar, tb.a aVar) {
    }

    public static void v(tb tbVar, tb.a aVar) {
    }

    public static void w(tb tbVar, tb.a aVar) {
    }

    @Deprecated
    public static void x(tb tbVar, tb.a aVar) {
    }

    public static void y(tb tbVar, tb.a aVar, int i) {
    }

    public static void z(tb tbVar, tb.a aVar, Exception exc) {
    }
}
