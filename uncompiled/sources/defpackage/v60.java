package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.contact.abstraction.IContact;
import net.safemoon.androidwallet.model.contact.room.RoomContact;

/* compiled from: ContactDataSource.kt */
/* renamed from: v60  reason: default package */
/* loaded from: classes2.dex */
public final class v60 {
    public final t60 a;

    public v60(t60 t60Var) {
        fs1.f(t60Var, "dao");
        this.a = t60Var;
    }

    public Object a(IContact iContact, q70<? super te4> q70Var) {
        this.a.e(new RoomContact(iContact));
        return te4.a;
    }

    public Object b(String str, q70<? super te4> q70Var) {
        this.a.c(str);
        return te4.a;
    }

    public Object c(String str, int i, q70<? super List<RoomContact>> q70Var) {
        return this.a.b(str, i, q70Var);
    }

    public LiveData<List<RoomContact>> d() {
        return this.a.a();
    }

    public Object e(IContact iContact, q70<? super te4> q70Var) {
        Object f = this.a.f(new RoomContact(iContact), q70Var);
        return f == gs1.d() ? f : te4.a;
    }

    public Object f(int i, q70<? super te4> q70Var) {
        this.a.d(i, System.currentTimeMillis());
        return te4.a;
    }
}
