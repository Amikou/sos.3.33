package defpackage;

import android.os.Bundle;
import androidx.media3.common.e;
import androidx.media3.exoplayer.ExoPlaybackException;

/* renamed from: ez0  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class ez0 implements e.a {
    public static final /* synthetic */ ez0 a = new ez0();

    @Override // androidx.media3.common.e.a
    public final e a(Bundle bundle) {
        return ExoPlaybackException.d(bundle);
    }
}
