package defpackage;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* compiled from: MatrixDrawable.java */
/* renamed from: z42  reason: default package */
/* loaded from: classes.dex */
public class z42 extends d91 {
    public Matrix h0;
    public Matrix i0;
    public int j0;
    public int k0;

    @Override // defpackage.d91, defpackage.wa4
    public void c(Matrix matrix) {
        super.c(matrix);
        Matrix matrix2 = this.i0;
        if (matrix2 != null) {
            matrix.preConcat(matrix2);
        }
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        q();
        if (this.i0 != null) {
            int save = canvas.save();
            canvas.clipRect(getBounds());
            canvas.concat(this.i0);
            super.draw(canvas);
            canvas.restoreToCount(save);
            return;
        }
        super.draw(canvas);
    }

    @Override // defpackage.d91
    public Drawable n(Drawable drawable) {
        Drawable n = super.n(drawable);
        p();
        return n;
    }

    @Override // defpackage.d91, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        p();
    }

    public final void p() {
        Drawable current = getCurrent();
        Rect bounds = getBounds();
        int intrinsicWidth = current.getIntrinsicWidth();
        this.j0 = intrinsicWidth;
        int intrinsicHeight = current.getIntrinsicHeight();
        this.k0 = intrinsicHeight;
        if (intrinsicWidth > 0 && intrinsicHeight > 0) {
            current.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            this.i0 = this.h0;
            return;
        }
        current.setBounds(bounds);
        this.i0 = null;
    }

    public final void q() {
        if (this.j0 == getCurrent().getIntrinsicWidth() && this.k0 == getCurrent().getIntrinsicHeight()) {
            return;
        }
        p();
    }
}
