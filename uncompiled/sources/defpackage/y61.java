package defpackage;

/* compiled from: FlexByteArrayPool.java */
/* renamed from: y61  reason: default package */
/* loaded from: classes.dex */
public class y61 {
    public final d83<byte[]> a;
    public final b b;

    /* compiled from: FlexByteArrayPool.java */
    /* renamed from: y61$a */
    /* loaded from: classes.dex */
    public class a implements d83<byte[]> {
        public a() {
        }

        @Override // defpackage.d83
        /* renamed from: b */
        public void a(byte[] bArr) {
            y61.this.b(bArr);
        }
    }

    /* compiled from: FlexByteArrayPool.java */
    /* renamed from: y61$b */
    /* loaded from: classes.dex */
    public static class b extends com.facebook.imagepipeline.memory.a {
        public b(r72 r72Var, ys2 ys2Var, zs2 zs2Var) {
            super(r72Var, ys2Var, zs2Var);
        }

        @Override // com.facebook.imagepipeline.memory.BasePool
        public or<byte[]> w(int i) {
            return new jj2(o(i), this.c.e, 0);
        }
    }

    public y61(r72 r72Var, ys2 ys2Var) {
        xt2.b(Boolean.valueOf(ys2Var.e > 0));
        this.b = new b(r72Var, ys2Var, rg2.h());
        this.a = new a();
    }

    public com.facebook.common.references.a<byte[]> a(int i) {
        return com.facebook.common.references.a.A(this.b.get(i), this.a);
    }

    public void b(byte[] bArr) {
        this.b.a(bArr);
    }
}
