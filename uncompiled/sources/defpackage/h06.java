package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: h06  reason: default package */
/* loaded from: classes.dex */
public final class h06 implements yp5<i06> {
    public static final h06 f0 = new h06();
    public final yp5<i06> a = gq5.a(gq5.b(new j06()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final i06 zza() {
        return this.a.zza();
    }
}
