package defpackage;

import android.content.ClipDescription;
import android.net.Uri;
import android.os.Build;
import android.view.inputmethod.InputContentInfo;

/* compiled from: InputContentInfoCompat.java */
/* renamed from: wq1  reason: default package */
/* loaded from: classes.dex */
public final class wq1 {
    public final c a;

    /* compiled from: InputContentInfoCompat.java */
    /* renamed from: wq1$b */
    /* loaded from: classes.dex */
    public static final class b implements c {
        public final Uri a;
        public final ClipDescription b;
        public final Uri c;

        public b(Uri uri, ClipDescription clipDescription, Uri uri2) {
            this.a = uri;
            this.b = clipDescription;
            this.c = uri2;
        }

        @Override // defpackage.wq1.c
        public Uri a() {
            return this.a;
        }

        @Override // defpackage.wq1.c
        public void b() {
        }

        @Override // defpackage.wq1.c
        public Uri c() {
            return this.c;
        }

        @Override // defpackage.wq1.c
        public Object d() {
            return null;
        }

        @Override // defpackage.wq1.c
        public ClipDescription getDescription() {
            return this.b;
        }
    }

    /* compiled from: InputContentInfoCompat.java */
    /* renamed from: wq1$c */
    /* loaded from: classes.dex */
    public interface c {
        Uri a();

        void b();

        Uri c();

        Object d();

        ClipDescription getDescription();
    }

    public wq1(Uri uri, ClipDescription clipDescription, Uri uri2) {
        if (Build.VERSION.SDK_INT >= 25) {
            this.a = new a(uri, clipDescription, uri2);
        } else {
            this.a = new b(uri, clipDescription, uri2);
        }
    }

    public static wq1 f(Object obj) {
        if (obj != null && Build.VERSION.SDK_INT >= 25) {
            return new wq1(new a(obj));
        }
        return null;
    }

    public Uri a() {
        return this.a.a();
    }

    public ClipDescription b() {
        return this.a.getDescription();
    }

    public Uri c() {
        return this.a.c();
    }

    public void d() {
        this.a.b();
    }

    public Object e() {
        return this.a.d();
    }

    /* compiled from: InputContentInfoCompat.java */
    /* renamed from: wq1$a */
    /* loaded from: classes.dex */
    public static final class a implements c {
        public final InputContentInfo a;

        public a(Object obj) {
            this.a = (InputContentInfo) obj;
        }

        @Override // defpackage.wq1.c
        public Uri a() {
            return this.a.getContentUri();
        }

        @Override // defpackage.wq1.c
        public void b() {
            this.a.requestPermission();
        }

        @Override // defpackage.wq1.c
        public Uri c() {
            return this.a.getLinkUri();
        }

        @Override // defpackage.wq1.c
        public Object d() {
            return this.a;
        }

        @Override // defpackage.wq1.c
        public ClipDescription getDescription() {
            return this.a.getDescription();
        }

        public a(Uri uri, ClipDescription clipDescription, Uri uri2) {
            this.a = new InputContentInfo(uri, clipDescription, uri2);
        }
    }

    public wq1(c cVar) {
        this.a = cVar;
    }
}
