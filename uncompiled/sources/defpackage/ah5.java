package defpackage;

import com.google.android.gms.internal.measurement.u0;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ah5  reason: default package */
/* loaded from: classes.dex */
public final class ah5 extends w1<u0, ah5> implements xx5 {
    public ah5() {
        super(u0.G());
    }

    public final ah5 v(String str) {
        if (this.g0) {
            s();
            this.g0 = false;
        }
        u0.H((u0) this.f0, str);
        return this;
    }

    public /* synthetic */ ah5(zf5 zf5Var) {
        super(u0.G());
    }
}
