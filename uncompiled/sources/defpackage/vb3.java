package defpackage;

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: SafeIterableMap.java */
/* renamed from: vb3  reason: default package */
/* loaded from: classes.dex */
public class vb3<K, V> implements Iterable<Map.Entry<K, V>> {
    public c<K, V> a;
    public c<K, V> f0;
    public WeakHashMap<f<K, V>, Boolean> g0 = new WeakHashMap<>();
    public int h0 = 0;

    /* compiled from: SafeIterableMap.java */
    /* renamed from: vb3$a */
    /* loaded from: classes.dex */
    public static class a<K, V> extends e<K, V> {
        public a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @Override // defpackage.vb3.e
        public c<K, V> b(c<K, V> cVar) {
            return cVar.h0;
        }

        @Override // defpackage.vb3.e
        public c<K, V> c(c<K, V> cVar) {
            return cVar.g0;
        }
    }

    /* compiled from: SafeIterableMap.java */
    /* renamed from: vb3$b */
    /* loaded from: classes.dex */
    public static class b<K, V> extends e<K, V> {
        public b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @Override // defpackage.vb3.e
        public c<K, V> b(c<K, V> cVar) {
            return cVar.g0;
        }

        @Override // defpackage.vb3.e
        public c<K, V> c(c<K, V> cVar) {
            return cVar.h0;
        }
    }

    /* compiled from: SafeIterableMap.java */
    /* renamed from: vb3$c */
    /* loaded from: classes.dex */
    public static class c<K, V> implements Map.Entry<K, V> {
        public final K a;
        public final V f0;
        public c<K, V> g0;
        public c<K, V> h0;

        public c(K k, V v) {
            this.a = k;
            this.f0 = v;
        }

        @Override // java.util.Map.Entry
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof c) {
                c cVar = (c) obj;
                return this.a.equals(cVar.a) && this.f0.equals(cVar.f0);
            }
            return false;
        }

        @Override // java.util.Map.Entry
        public K getKey() {
            return this.a;
        }

        @Override // java.util.Map.Entry
        public V getValue() {
            return this.f0;
        }

        @Override // java.util.Map.Entry
        public int hashCode() {
            return this.a.hashCode() ^ this.f0.hashCode();
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        public String toString() {
            return this.a + "=" + this.f0;
        }
    }

    /* compiled from: SafeIterableMap.java */
    /* renamed from: vb3$d */
    /* loaded from: classes.dex */
    public class d implements Iterator<Map.Entry<K, V>>, f<K, V> {
        public c<K, V> a;
        public boolean f0 = true;

        public d() {
        }

        @Override // defpackage.vb3.f
        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = this.a;
            if (cVar == cVar2) {
                c<K, V> cVar3 = cVar2.h0;
                this.a = cVar3;
                this.f0 = cVar3 == null;
            }
        }

        @Override // java.util.Iterator
        /* renamed from: b */
        public Map.Entry<K, V> next() {
            if (this.f0) {
                this.f0 = false;
                this.a = vb3.this.a;
            } else {
                c<K, V> cVar = this.a;
                this.a = cVar != null ? cVar.g0 : null;
            }
            return this.a;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.f0) {
                return vb3.this.a != null;
            }
            c<K, V> cVar = this.a;
            return (cVar == null || cVar.g0 == null) ? false : true;
        }
    }

    /* compiled from: SafeIterableMap.java */
    /* renamed from: vb3$e */
    /* loaded from: classes.dex */
    public static abstract class e<K, V> implements Iterator<Map.Entry<K, V>>, f<K, V> {
        public c<K, V> a;
        public c<K, V> f0;

        public e(c<K, V> cVar, c<K, V> cVar2) {
            this.a = cVar2;
            this.f0 = cVar;
        }

        @Override // defpackage.vb3.f
        public void a(c<K, V> cVar) {
            if (this.a == cVar && cVar == this.f0) {
                this.f0 = null;
                this.a = null;
            }
            c<K, V> cVar2 = this.a;
            if (cVar2 == cVar) {
                this.a = b(cVar2);
            }
            if (this.f0 == cVar) {
                this.f0 = f();
            }
        }

        public abstract c<K, V> b(c<K, V> cVar);

        public abstract c<K, V> c(c<K, V> cVar);

        @Override // java.util.Iterator
        /* renamed from: d */
        public Map.Entry<K, V> next() {
            c<K, V> cVar = this.f0;
            this.f0 = f();
            return cVar;
        }

        public final c<K, V> f() {
            c<K, V> cVar = this.f0;
            c<K, V> cVar2 = this.a;
            if (cVar == cVar2 || cVar2 == null) {
                return null;
            }
            return c(cVar);
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            return this.f0 != null;
        }
    }

    /* compiled from: SafeIterableMap.java */
    /* renamed from: vb3$f */
    /* loaded from: classes.dex */
    public interface f<K, V> {
        void a(c<K, V> cVar);
    }

    public Iterator<Map.Entry<K, V>> descendingIterator() {
        b bVar = new b(this.f0, this.a);
        this.g0.put(bVar, Boolean.FALSE);
        return bVar;
    }

    public Map.Entry<K, V> e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof vb3) {
            vb3 vb3Var = (vb3) obj;
            if (size() != vb3Var.size()) {
                return false;
            }
            Iterator<Map.Entry<K, V>> it = iterator();
            Iterator<Map.Entry<K, V>> it2 = vb3Var.iterator();
            while (it.hasNext() && it2.hasNext()) {
                Map.Entry<K, V> next = it.next();
                Map.Entry<K, V> next2 = it2.next();
                if ((next == null && next2 != null) || (next != null && !next.equals(next2))) {
                    return false;
                }
            }
            return (it.hasNext() || it2.hasNext()) ? false : true;
        }
        return false;
    }

    public int hashCode() {
        Iterator<Map.Entry<K, V>> it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i += it.next().hashCode();
        }
        return i;
    }

    public c<K, V> i(K k) {
        c<K, V> cVar = this.a;
        while (cVar != null && !cVar.a.equals(k)) {
            cVar = cVar.g0;
        }
        return cVar;
    }

    @Override // java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        a aVar = new a(this.a, this.f0);
        this.g0.put(aVar, Boolean.FALSE);
        return aVar;
    }

    public vb3<K, V>.d k() {
        vb3<K, V>.d dVar = new d();
        this.g0.put(dVar, Boolean.FALSE);
        return dVar;
    }

    public Map.Entry<K, V> m() {
        return this.f0;
    }

    public c<K, V> n(K k, V v) {
        c<K, V> cVar = new c<>(k, v);
        this.h0++;
        c<K, V> cVar2 = this.f0;
        if (cVar2 == null) {
            this.a = cVar;
            this.f0 = cVar;
            return cVar;
        }
        cVar2.g0 = cVar;
        cVar.h0 = cVar2;
        this.f0 = cVar;
        return cVar;
    }

    public V o(K k, V v) {
        c<K, V> i = i(k);
        if (i != null) {
            return i.f0;
        }
        n(k, v);
        return null;
    }

    public V p(K k) {
        c<K, V> i = i(k);
        if (i == null) {
            return null;
        }
        this.h0--;
        if (!this.g0.isEmpty()) {
            for (f<K, V> fVar : this.g0.keySet()) {
                fVar.a(i);
            }
        }
        c<K, V> cVar = i.h0;
        if (cVar != null) {
            cVar.g0 = i.g0;
        } else {
            this.a = i.g0;
        }
        c<K, V> cVar2 = i.g0;
        if (cVar2 != null) {
            cVar2.h0 = cVar;
        } else {
            this.f0 = cVar;
        }
        i.g0 = null;
        i.h0 = null;
        return i.f0;
    }

    public int size() {
        return this.h0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<Map.Entry<K, V>> it = iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
