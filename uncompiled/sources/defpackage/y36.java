package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: y36  reason: default package */
/* loaded from: classes.dex */
public final class y36 implements x36 {
    public static final wo5<Boolean> a = new ro5(bo5.a("com.google.android.gms.measurement")).b("measurement.integration.disable_firebase_instance_id", false);

    @Override // defpackage.x36
    public final boolean zza() {
        return true;
    }

    @Override // defpackage.x36
    public final boolean zzb() {
        return a.e().booleanValue();
    }
}
