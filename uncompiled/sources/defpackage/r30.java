package defpackage;

/* compiled from: CancellableContinuationImpl.kt */
/* renamed from: r30  reason: default package */
/* loaded from: classes2.dex */
public final class r30 {
    public final Object a;
    public final iv b;
    public final tc1<Throwable, te4> c;
    public final Object d;
    public final Throwable e;

    /* JADX WARN: Multi-variable type inference failed */
    public r30(Object obj, iv ivVar, tc1<? super Throwable, te4> tc1Var, Object obj2, Throwable th) {
        this.a = obj;
        this.b = ivVar;
        this.c = tc1Var;
        this.d = obj2;
        this.e = th;
    }

    public static /* synthetic */ r30 b(r30 r30Var, Object obj, iv ivVar, tc1 tc1Var, Object obj2, Throwable th, int i, Object obj3) {
        if ((i & 1) != 0) {
            obj = r30Var.a;
        }
        if ((i & 2) != 0) {
            ivVar = r30Var.b;
        }
        iv ivVar2 = ivVar;
        tc1<Throwable, te4> tc1Var2 = tc1Var;
        if ((i & 4) != 0) {
            tc1Var2 = r30Var.c;
        }
        tc1 tc1Var3 = tc1Var2;
        if ((i & 8) != 0) {
            obj2 = r30Var.d;
        }
        Object obj4 = obj2;
        if ((i & 16) != 0) {
            th = r30Var.e;
        }
        return r30Var.a(obj, ivVar2, tc1Var3, obj4, th);
    }

    public final r30 a(Object obj, iv ivVar, tc1<? super Throwable, te4> tc1Var, Object obj2, Throwable th) {
        return new r30(obj, ivVar, tc1Var, obj2, th);
    }

    public final boolean c() {
        return this.e != null;
    }

    public final void d(pv<?> pvVar, Throwable th) {
        iv ivVar = this.b;
        if (ivVar != null) {
            pvVar.m(ivVar, th);
        }
        tc1<Throwable, te4> tc1Var = this.c;
        if (tc1Var == null) {
            return;
        }
        pvVar.p(tc1Var, th);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof r30) {
            r30 r30Var = (r30) obj;
            return fs1.b(this.a, r30Var.a) && fs1.b(this.b, r30Var.b) && fs1.b(this.c, r30Var.c) && fs1.b(this.d, r30Var.d) && fs1.b(this.e, r30Var.e);
        }
        return false;
    }

    public int hashCode() {
        Object obj = this.a;
        int hashCode = (obj == null ? 0 : obj.hashCode()) * 31;
        iv ivVar = this.b;
        int hashCode2 = (hashCode + (ivVar == null ? 0 : ivVar.hashCode())) * 31;
        tc1<Throwable, te4> tc1Var = this.c;
        int hashCode3 = (hashCode2 + (tc1Var == null ? 0 : tc1Var.hashCode())) * 31;
        Object obj2 = this.d;
        int hashCode4 = (hashCode3 + (obj2 == null ? 0 : obj2.hashCode())) * 31;
        Throwable th = this.e;
        return hashCode4 + (th != null ? th.hashCode() : 0);
    }

    public String toString() {
        return "CompletedContinuation(result=" + this.a + ", cancelHandler=" + this.b + ", onCancellation=" + this.c + ", idempotentResume=" + this.d + ", cancelCause=" + this.e + ')';
    }

    public /* synthetic */ r30(Object obj, iv ivVar, tc1 tc1Var, Object obj2, Throwable th, int i, qi0 qi0Var) {
        this(obj, (i & 2) != 0 ? null : ivVar, (i & 4) != 0 ? null : tc1Var, (i & 8) != 0 ? null : obj2, (i & 16) != 0 ? null : th);
    }
}
