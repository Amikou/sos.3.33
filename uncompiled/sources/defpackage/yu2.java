package defpackage;

import android.os.Process;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: PriorityThreadFactory.java */
/* renamed from: yu2  reason: default package */
/* loaded from: classes.dex */
public class yu2 implements ThreadFactory {
    public final int a;
    public final String f0;
    public final boolean g0;
    public final AtomicInteger h0 = new AtomicInteger(1);

    /* compiled from: PriorityThreadFactory.java */
    /* renamed from: yu2$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ Runnable a;

        public a(Runnable runnable) {
            this.a = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                Process.setThreadPriority(yu2.this.a);
            } catch (Throwable unused) {
            }
            this.a.run();
        }
    }

    public yu2(int i, String str, boolean z) {
        this.a = i;
        this.f0 = str;
        this.g0 = z;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        String str;
        a aVar = new a(runnable);
        if (this.g0) {
            str = this.f0 + "-" + this.h0.getAndIncrement();
        } else {
            str = this.f0;
        }
        return new Thread(aVar, str);
    }
}
