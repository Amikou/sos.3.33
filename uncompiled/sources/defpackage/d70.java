package defpackage;

import androidx.media3.common.j;
import androidx.media3.datasource.b;
import androidx.media3.datasource.f;
import defpackage.ny;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: ContainerMediaChunk.java */
/* renamed from: d70  reason: default package */
/* loaded from: classes.dex */
public class d70 extends ln {
    public final int o;
    public final long p;
    public final ny q;
    public long r;
    public volatile boolean s;
    public boolean t;

    public d70(b bVar, je0 je0Var, j jVar, int i, Object obj, long j, long j2, long j3, long j4, long j5, int i2, long j6, ny nyVar) {
        super(bVar, je0Var, jVar, i, obj, j, j2, j3, j4, j5);
        this.o = i2;
        this.p = j6;
        this.q = nyVar;
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public final void a() throws IOException {
        if (this.r == 0) {
            nn i = i();
            i.b(this.p);
            ny nyVar = this.q;
            ny.b k = k(i);
            long j = this.k;
            long j2 = j == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? -9223372036854775807L : j - this.p;
            long j3 = this.l;
            nyVar.c(k, j2, j3 == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED ? -9223372036854775807L : j3 - this.p);
        }
        try {
            je0 e = this.b.e(this.r);
            f fVar = this.i;
            hj0 hj0Var = new hj0(fVar, e.f, fVar.b(e));
            while (!this.s && this.q.b(hj0Var)) {
            }
            this.r = hj0Var.getPosition() - this.b.f;
            he0.a(this.i);
            this.t = !this.s;
        } catch (Throwable th) {
            he0.a(this.i);
            throw th;
        }
    }

    @Override // androidx.media3.exoplayer.upstream.Loader.e
    public final void c() {
        this.s = true;
    }

    @Override // defpackage.s52
    public long f() {
        return this.j + this.o;
    }

    @Override // defpackage.s52
    public boolean g() {
        return this.t;
    }

    public ny.b k(nn nnVar) {
        return nnVar;
    }
}
