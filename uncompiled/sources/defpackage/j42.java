package defpackage;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import androidx.core.graphics.drawable.a;
import com.google.android.material.button.MaterialButton;

/* compiled from: MaterialButtonHelper.java */
/* renamed from: j42  reason: default package */
/* loaded from: classes2.dex */
public class j42 {
    public static final boolean t;
    public final MaterialButton a;
    public pn3 b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public PorterDuff.Mode i;
    public ColorStateList j;
    public ColorStateList k;
    public ColorStateList l;
    public Drawable m;
    public boolean n = false;
    public boolean o = false;
    public boolean p = false;
    public boolean q;
    public LayerDrawable r;
    public int s;

    static {
        t = Build.VERSION.SDK_INT >= 21;
    }

    public j42(MaterialButton materialButton, pn3 pn3Var) {
        this.a = materialButton;
        this.b = pn3Var;
    }

    public void A(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            I();
        }
    }

    public void B(int i) {
        if (this.h != i) {
            this.h = i;
            I();
        }
    }

    public void C(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            if (f() != null) {
                a.o(f(), this.j);
            }
        }
    }

    public void D(PorterDuff.Mode mode) {
        if (this.i != mode) {
            this.i = mode;
            if (f() == null || this.i == null) {
                return;
            }
            a.p(f(), this.i);
        }
    }

    public final void E(int i, int i2) {
        int J = ei4.J(this.a);
        int paddingTop = this.a.getPaddingTop();
        int I = ei4.I(this.a);
        int paddingBottom = this.a.getPaddingBottom();
        int i3 = this.e;
        int i4 = this.f;
        this.f = i2;
        this.e = i;
        if (!this.o) {
            F();
        }
        ei4.H0(this.a, J, (paddingTop + i) - i3, I, (paddingBottom + i2) - i4);
    }

    public final void F() {
        this.a.setInternalBackground(a());
        o42 f = f();
        if (f != null) {
            f.Z(this.s);
        }
    }

    public final void G(pn3 pn3Var) {
        if (f() != null) {
            f().setShapeAppearanceModel(pn3Var);
        }
        if (n() != null) {
            n().setShapeAppearanceModel(pn3Var);
        }
        if (e() != null) {
            e().setShapeAppearanceModel(pn3Var);
        }
    }

    public void H(int i, int i2) {
        Drawable drawable = this.m;
        if (drawable != null) {
            drawable.setBounds(this.c, this.e, i2 - this.d, i - this.f);
        }
    }

    public final void I() {
        o42 f = f();
        o42 n = n();
        if (f != null) {
            f.k0(this.h, this.k);
            if (n != null) {
                n.j0(this.h, this.n ? l42.d(this.a, gy2.colorSurface) : 0);
            }
        }
    }

    public final InsetDrawable J(Drawable drawable) {
        return new InsetDrawable(drawable, this.c, this.e, this.d, this.f);
    }

    public final Drawable a() {
        o42 o42Var = new o42(this.b);
        o42Var.P(this.a.getContext());
        a.o(o42Var, this.j);
        PorterDuff.Mode mode = this.i;
        if (mode != null) {
            a.p(o42Var, mode);
        }
        o42Var.k0(this.h, this.k);
        o42 o42Var2 = new o42(this.b);
        o42Var2.setTint(0);
        o42Var2.j0(this.h, this.n ? l42.d(this.a, gy2.colorSurface) : 0);
        if (t) {
            o42 o42Var3 = new o42(this.b);
            this.m = o42Var3;
            a.n(o42Var3, -1);
            RippleDrawable rippleDrawable = new RippleDrawable(b93.d(this.l), J(new LayerDrawable(new Drawable[]{o42Var2, o42Var})), this.m);
            this.r = rippleDrawable;
            return rippleDrawable;
        }
        a93 a93Var = new a93(this.b);
        this.m = a93Var;
        a.o(a93Var, b93.d(this.l));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{o42Var2, o42Var, this.m});
        this.r = layerDrawable;
        return J(layerDrawable);
    }

    public int b() {
        return this.g;
    }

    public int c() {
        return this.f;
    }

    public int d() {
        return this.e;
    }

    public sn3 e() {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 1) {
            return null;
        }
        if (this.r.getNumberOfLayers() > 2) {
            return (sn3) this.r.getDrawable(2);
        }
        return (sn3) this.r.getDrawable(1);
    }

    public o42 f() {
        return g(false);
    }

    public final o42 g(boolean z) {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 0) {
            return null;
        }
        if (t) {
            return (o42) ((LayerDrawable) ((InsetDrawable) this.r.getDrawable(0)).getDrawable()).getDrawable(!z ? 1 : 0);
        }
        return (o42) this.r.getDrawable(!z ? 1 : 0);
    }

    public ColorStateList h() {
        return this.l;
    }

    public pn3 i() {
        return this.b;
    }

    public ColorStateList j() {
        return this.k;
    }

    public int k() {
        return this.h;
    }

    public ColorStateList l() {
        return this.j;
    }

    public PorterDuff.Mode m() {
        return this.i;
    }

    public final o42 n() {
        return g(true);
    }

    public boolean o() {
        return this.o;
    }

    public boolean p() {
        return this.q;
    }

    public void q(TypedArray typedArray) {
        this.c = typedArray.getDimensionPixelOffset(o23.MaterialButton_android_insetLeft, 0);
        this.d = typedArray.getDimensionPixelOffset(o23.MaterialButton_android_insetRight, 0);
        this.e = typedArray.getDimensionPixelOffset(o23.MaterialButton_android_insetTop, 0);
        this.f = typedArray.getDimensionPixelOffset(o23.MaterialButton_android_insetBottom, 0);
        int i = o23.MaterialButton_cornerRadius;
        if (typedArray.hasValue(i)) {
            int dimensionPixelSize = typedArray.getDimensionPixelSize(i, -1);
            this.g = dimensionPixelSize;
            y(this.b.w(dimensionPixelSize));
            this.p = true;
        }
        this.h = typedArray.getDimensionPixelSize(o23.MaterialButton_strokeWidth, 0);
        this.i = mk4.i(typedArray.getInt(o23.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.j = n42.b(this.a.getContext(), typedArray, o23.MaterialButton_backgroundTint);
        this.k = n42.b(this.a.getContext(), typedArray, o23.MaterialButton_strokeColor);
        this.l = n42.b(this.a.getContext(), typedArray, o23.MaterialButton_rippleColor);
        this.q = typedArray.getBoolean(o23.MaterialButton_android_checkable, false);
        this.s = typedArray.getDimensionPixelSize(o23.MaterialButton_elevation, 0);
        int J = ei4.J(this.a);
        int paddingTop = this.a.getPaddingTop();
        int I = ei4.I(this.a);
        int paddingBottom = this.a.getPaddingBottom();
        if (typedArray.hasValue(o23.MaterialButton_android_background)) {
            s();
        } else {
            F();
        }
        ei4.H0(this.a, J + this.c, paddingTop + this.e, I + this.d, paddingBottom + this.f);
    }

    public void r(int i) {
        if (f() != null) {
            f().setTint(i);
        }
    }

    public void s() {
        this.o = true;
        this.a.setSupportBackgroundTintList(this.j);
        this.a.setSupportBackgroundTintMode(this.i);
    }

    public void t(boolean z) {
        this.q = z;
    }

    public void u(int i) {
        if (this.p && this.g == i) {
            return;
        }
        this.g = i;
        this.p = true;
        y(this.b.w(i));
    }

    public void v(int i) {
        E(this.e, i);
    }

    public void w(int i) {
        E(i, this.f);
    }

    public void x(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            boolean z = t;
            if (z && (this.a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.a.getBackground()).setColor(b93.d(colorStateList));
            } else if (z || !(this.a.getBackground() instanceof a93)) {
            } else {
                ((a93) this.a.getBackground()).setTintList(b93.d(colorStateList));
            }
        }
    }

    public void y(pn3 pn3Var) {
        this.b = pn3Var;
        G(pn3Var);
    }

    public void z(boolean z) {
        this.n = z;
        I();
    }
}
