package defpackage;

import androidx.lifecycle.LiveData;
import java.util.Iterator;
import java.util.Map;

/* compiled from: MediatorLiveData.java */
/* renamed from: g72  reason: default package */
/* loaded from: classes.dex */
public class g72<T> extends gb2<T> {
    public vb3<LiveData<?>, a<?>> a = new vb3<>();

    /* compiled from: MediatorLiveData.java */
    /* renamed from: g72$a */
    /* loaded from: classes.dex */
    public static class a<V> implements tl2<V> {
        public final LiveData<V> a;
        public final tl2<? super V> b;
        public int c = -1;

        public a(LiveData<V> liveData, tl2<? super V> tl2Var) {
            this.a = liveData;
            this.b = tl2Var;
        }

        public void a() {
            this.a.observeForever(this);
        }

        public void b() {
            this.a.removeObserver(this);
        }

        @Override // defpackage.tl2
        public void onChanged(V v) {
            if (this.c != this.a.getVersion()) {
                this.c = this.a.getVersion();
                this.b.onChanged(v);
            }
        }
    }

    public <S> void a(LiveData<S> liveData, tl2<? super S> tl2Var) {
        a<?> aVar = new a<>(liveData, tl2Var);
        a<?> o = this.a.o(liveData, aVar);
        if (o != null && o.b != tl2Var) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        }
        if (o == null && hasActiveObservers()) {
            aVar.a();
        }
    }

    public <S> void b(LiveData<S> liveData) {
        a<?> p = this.a.p(liveData);
        if (p != null) {
            p.b();
        }
    }

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().getValue().a();
        }
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().getValue().b();
        }
    }
}
