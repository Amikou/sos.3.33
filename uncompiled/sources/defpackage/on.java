package defpackage;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;

/* compiled from: BaseMenuWrapper.java */
/* renamed from: on  reason: default package */
/* loaded from: classes.dex */
public abstract class on {
    public final Context a;
    public vo3<lw3, MenuItem> b;
    public vo3<xw3, SubMenu> c;

    public on(Context context) {
        this.a = context;
    }

    public final MenuItem c(MenuItem menuItem) {
        if (menuItem instanceof lw3) {
            lw3 lw3Var = (lw3) menuItem;
            if (this.b == null) {
                this.b = new vo3<>();
            }
            MenuItem menuItem2 = this.b.get(menuItem);
            if (menuItem2 == null) {
                u72 u72Var = new u72(this.a, lw3Var);
                this.b.put(lw3Var, u72Var);
                return u72Var;
            }
            return menuItem2;
        }
        return menuItem;
    }

    public final SubMenu d(SubMenu subMenu) {
        if (subMenu instanceof xw3) {
            xw3 xw3Var = (xw3) subMenu;
            if (this.c == null) {
                this.c = new vo3<>();
            }
            SubMenu subMenu2 = this.c.get(xw3Var);
            if (subMenu2 == null) {
                iv3 iv3Var = new iv3(this.a, xw3Var);
                this.c.put(xw3Var, iv3Var);
                return iv3Var;
            }
            return subMenu2;
        }
        return subMenu;
    }

    public final void e() {
        vo3<lw3, MenuItem> vo3Var = this.b;
        if (vo3Var != null) {
            vo3Var.clear();
        }
        vo3<xw3, SubMenu> vo3Var2 = this.c;
        if (vo3Var2 != null) {
            vo3Var2.clear();
        }
    }

    public final void f(int i) {
        if (this.b == null) {
            return;
        }
        int i2 = 0;
        while (i2 < this.b.size()) {
            if (this.b.i(i2).getGroupId() == i) {
                this.b.k(i2);
                i2--;
            }
            i2++;
        }
    }

    public final void g(int i) {
        if (this.b == null) {
            return;
        }
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            if (this.b.i(i2).getItemId() == i) {
                this.b.k(i2);
                return;
            }
        }
    }
}
