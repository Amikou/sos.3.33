package defpackage;

import java.util.HashSet;
import java.util.Set;

/* compiled from: LruBucketsPoolBackend.java */
/* renamed from: q22  reason: default package */
/* loaded from: classes.dex */
public abstract class q22<T> implements vs2<T> {
    public final Set<T> a = new HashSet();
    public final pr<T> b = new pr<>();

    public final T b(T t) {
        if (t != null) {
            synchronized (this) {
                this.a.remove(t);
            }
        }
        return t;
    }

    @Override // defpackage.vs2
    public void c(T t) {
        boolean add;
        synchronized (this) {
            add = this.a.add(t);
        }
        if (add) {
            this.b.e(a(t), t);
        }
    }

    @Override // defpackage.vs2
    public T get(int i) {
        return b(this.b.a(i));
    }

    @Override // defpackage.vs2
    public T pop() {
        return b(this.b.f());
    }
}
