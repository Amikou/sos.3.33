package defpackage;

import android.net.Uri;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: LoadEventInfo.java */
/* renamed from: u02  reason: default package */
/* loaded from: classes.dex */
public final class u02 {
    public static final AtomicLong a = new AtomicLong();

    public u02(long j, je0 je0Var, long j2) {
        this(j, je0Var, je0Var.a, Collections.emptyMap(), j2, 0L, 0L);
    }

    public static long a() {
        return a.getAndIncrement();
    }

    public u02(long j, je0 je0Var, Uri uri, Map<String, List<String>> map, long j2, long j3, long j4) {
    }
}
