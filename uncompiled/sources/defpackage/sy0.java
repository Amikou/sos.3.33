package defpackage;

import java.util.concurrent.Executor;

/* compiled from: ExecutionModule_ExecutorFactory.java */
/* renamed from: sy0  reason: default package */
/* loaded from: classes.dex */
public final class sy0 implements z11<Executor> {

    /* compiled from: ExecutionModule_ExecutorFactory.java */
    /* renamed from: sy0$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final sy0 a = new sy0();
    }

    public static sy0 a() {
        return a.a;
    }

    public static Executor b() {
        return (Executor) yt2.c(ry0.a(), "Cannot return null from a non-@Nullable @Provides method");
    }

    @Override // defpackage.ew2
    /* renamed from: c */
    public Executor get() {
        return b();
    }
}
