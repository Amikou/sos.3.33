package defpackage;

import android.content.Context;
import com.google.android.datatransport.runtime.backends.d;
import java.util.Objects;

/* compiled from: AutoValue_CreationContext.java */
/* renamed from: hl  reason: default package */
/* loaded from: classes.dex */
public final class hl extends d {
    public final Context a;
    public final qz b;
    public final qz c;
    public final String d;

    public hl(Context context, qz qzVar, qz qzVar2, String str) {
        Objects.requireNonNull(context, "Null applicationContext");
        this.a = context;
        Objects.requireNonNull(qzVar, "Null wallClock");
        this.b = qzVar;
        Objects.requireNonNull(qzVar2, "Null monotonicClock");
        this.c = qzVar2;
        Objects.requireNonNull(str, "Null backendName");
        this.d = str;
    }

    @Override // com.google.android.datatransport.runtime.backends.d
    public Context b() {
        return this.a;
    }

    @Override // com.google.android.datatransport.runtime.backends.d
    public String c() {
        return this.d;
    }

    @Override // com.google.android.datatransport.runtime.backends.d
    public qz d() {
        return this.c;
    }

    @Override // com.google.android.datatransport.runtime.backends.d
    public qz e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof d) {
            d dVar = (d) obj;
            return this.a.equals(dVar.b()) && this.b.equals(dVar.e()) && this.c.equals(dVar.d()) && this.d.equals(dVar.c());
        }
        return false;
    }

    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    public String toString() {
        return "CreationContext{applicationContext=" + this.a + ", wallClock=" + this.b + ", monotonicClock=" + this.c + ", backendName=" + this.d + "}";
    }
}
