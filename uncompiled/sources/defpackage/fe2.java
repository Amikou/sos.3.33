package defpackage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/* compiled from: NavigateToAndroidSettingsForLocation.kt */
/* renamed from: fe2  reason: default package */
/* loaded from: classes2.dex */
public final class fe2 {
    public static final fe2 a = new fe2();

    public final void a(Context context) {
        fs1.f(context, "context");
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }
}
