package defpackage;

/* renamed from: nd0  reason: default package */
/* loaded from: classes2.dex */
public class nd0 {
    public byte[] a;
    public int b;

    public nd0(byte[] bArr, int i) {
        this.a = bArr;
        this.b = i;
    }

    public boolean equals(Object obj) {
        if (obj instanceof nd0) {
            nd0 nd0Var = (nd0) obj;
            if (nd0Var.b != this.b) {
                return false;
            }
            return wh.a(this.a, nd0Var.a);
        }
        return false;
    }

    public int hashCode() {
        return this.b ^ wh.r(this.a);
    }
}
