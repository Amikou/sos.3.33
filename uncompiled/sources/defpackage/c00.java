package defpackage;

import android.graphics.Bitmap;
import com.facebook.common.references.a;

/* compiled from: CloseableStaticBitmap.java */
/* renamed from: c00  reason: default package */
/* loaded from: classes.dex */
public class c00 extends wz implements yj1 {
    public a<Bitmap> g0;
    public volatile Bitmap h0;
    public final xw2 i0;
    public final int j0;
    public final int k0;

    public c00(Bitmap bitmap, d83<Bitmap> d83Var, xw2 xw2Var, int i) {
        this(bitmap, d83Var, xw2Var, i, 0);
    }

    public static int i(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getHeight();
    }

    public static int j(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getWidth();
    }

    @Override // com.facebook.imagepipeline.image.a
    public xw2 a() {
        return this.i0;
    }

    @Override // com.facebook.imagepipeline.image.a
    public int b() {
        return rq.e(this.h0);
    }

    @Override // com.facebook.imagepipeline.image.a, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        a<Bitmap> h = h();
        if (h != null) {
            h.close();
        }
    }

    @Override // defpackage.wz
    public Bitmap f() {
        return this.h0;
    }

    public synchronized a<Bitmap> g() {
        return a.e(this.g0);
    }

    @Override // defpackage.ao1
    public int getHeight() {
        int i;
        if (this.j0 % 180 == 0 && (i = this.k0) != 5 && i != 7) {
            return i(this.h0);
        }
        return j(this.h0);
    }

    @Override // defpackage.ao1
    public int getWidth() {
        int i;
        if (this.j0 % 180 == 0 && (i = this.k0) != 5 && i != 7) {
            return j(this.h0);
        }
        return i(this.h0);
    }

    public final synchronized a<Bitmap> h() {
        a<Bitmap> aVar;
        aVar = this.g0;
        this.g0 = null;
        this.h0 = null;
        return aVar;
    }

    @Override // com.facebook.imagepipeline.image.a
    public synchronized boolean isClosed() {
        return this.g0 == null;
    }

    public int l() {
        return this.k0;
    }

    public int m() {
        return this.j0;
    }

    public c00(Bitmap bitmap, d83<Bitmap> d83Var, xw2 xw2Var, int i, int i2) {
        this.h0 = (Bitmap) xt2.g(bitmap);
        this.g0 = a.A(this.h0, (d83) xt2.g(d83Var));
        this.i0 = xw2Var;
        this.j0 = i;
        this.k0 = i2;
    }

    public c00(a<Bitmap> aVar, xw2 xw2Var, int i) {
        this(aVar, xw2Var, i, 0);
    }

    public c00(a<Bitmap> aVar, xw2 xw2Var, int i, int i2) {
        a<Bitmap> aVar2 = (a) xt2.g(aVar.d());
        this.g0 = aVar2;
        this.h0 = aVar2.j();
        this.i0 = xw2Var;
        this.j0 = i;
        this.k0 = i2;
    }
}
