package defpackage;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import okio.ByteString;
import okio.b;
import okio.c;
import okio.m;
import okio.n;
import okio.o;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: RealBufferedSink.kt */
/* renamed from: c43  reason: default package */
/* loaded from: classes2.dex */
public final class c43 implements c {
    public final b a;
    public boolean f0;
    public final m g0;

    public c43(m mVar) {
        fs1.f(mVar, "sink");
        this.g0 = mVar;
        this.a = new b();
    }

    @Override // okio.c
    public c B1(long j) {
        if (!this.f0) {
            this.a.B1(j);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public c C0(String str) {
        fs1.f(str, Utf8String.TYPE_NAME);
        if (!this.f0) {
            this.a.C0(str);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public OutputStream D1() {
        return new a();
    }

    @Override // okio.c
    public c M0(byte[] bArr, int i, int i2) {
        fs1.f(bArr, "source");
        if (!this.f0) {
            this.a.M0(bArr, i, i2);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public c O() {
        if (!this.f0) {
            long a0 = this.a.a0();
            if (a0 > 0) {
                this.g0.write(this.a, a0);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public c P(int i) {
        if (!this.f0) {
            this.a.P(i);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public long P0(n nVar) {
        fs1.f(nVar, "source");
        long j = 0;
        while (true) {
            long read = nVar.read(this.a, 8192);
            if (read == -1) {
                return j;
            }
            j += read;
            n0();
        }
    }

    @Override // okio.c
    public c Q0(long j) {
        if (!this.f0) {
            this.a.Q0(j);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public c V(int i) {
        if (!this.f0) {
            this.a.V(i);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (this.f0) {
            return;
        }
        Throwable th = null;
        try {
            if (this.a.a0() > 0) {
                m mVar = this.g0;
                b bVar = this.a;
                mVar.write(bVar, bVar.a0());
            }
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            this.g0.close();
        } catch (Throwable th3) {
            if (th == null) {
                th = th3;
            }
        }
        this.f0 = true;
        if (th != null) {
            throw th;
        }
    }

    @Override // okio.c
    public c d0(int i) {
        if (!this.f0) {
            this.a.d0(i);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c, okio.m, java.io.Flushable
    public void flush() {
        if (!this.f0) {
            if (this.a.a0() > 0) {
                m mVar = this.g0;
                b bVar = this.a;
                mVar.write(bVar, bVar.a0());
            }
            this.g0.flush();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return !this.f0;
    }

    @Override // okio.c
    public c l1(byte[] bArr) {
        fs1.f(bArr, "source");
        if (!this.f0) {
            this.a.l1(bArr);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public c n0() {
        if (!this.f0) {
            long d = this.a.d();
            if (d > 0) {
                this.g0.write(this.a, d);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public c n1(ByteString byteString) {
        fs1.f(byteString, "byteString");
        if (!this.f0) {
            this.a.n1(byteString);
            return n0();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.c
    public b o() {
        return this.a;
    }

    @Override // okio.m
    public o timeout() {
        return this.g0.timeout();
    }

    public String toString() {
        return "buffer(" + this.g0 + ')';
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) {
        fs1.f(byteBuffer, "source");
        if (!this.f0) {
            int write = this.a.write(byteBuffer);
            n0();
            return write;
        }
        throw new IllegalStateException("closed".toString());
    }

    /* compiled from: RealBufferedSink.kt */
    /* renamed from: c43$a */
    /* loaded from: classes2.dex */
    public static final class a extends OutputStream {
        public a() {
        }

        @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            c43.this.close();
        }

        @Override // java.io.OutputStream, java.io.Flushable
        public void flush() {
            c43 c43Var = c43.this;
            if (c43Var.f0) {
                return;
            }
            c43Var.flush();
        }

        public String toString() {
            return c43.this + ".outputStream()";
        }

        @Override // java.io.OutputStream
        public void write(int i) {
            c43 c43Var = c43.this;
            if (!c43Var.f0) {
                c43Var.a.d0((byte) i);
                c43.this.n0();
                return;
            }
            throw new IOException("closed");
        }

        @Override // java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) {
            fs1.f(bArr, "data");
            c43 c43Var = c43.this;
            if (!c43Var.f0) {
                c43Var.a.M0(bArr, i, i2);
                c43.this.n0();
                return;
            }
            throw new IOException("closed");
        }
    }

    @Override // okio.m
    public void write(b bVar, long j) {
        fs1.f(bVar, "source");
        if (!this.f0) {
            this.a.write(bVar, j);
            n0();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }
}
