package defpackage;

import android.content.SharedPreferences;

/* renamed from: h55  reason: default package */
/* loaded from: classes.dex */
public final class h55 extends r45<Boolean> {
    public h55(w55 w55Var, String str, Boolean bool) {
        super(w55Var, str, bool, null);
    }

    @Override // defpackage.r45
    public final /* synthetic */ Boolean j(String str) {
        if (v56.c.matcher(str).matches()) {
            return Boolean.TRUE;
        }
        if (v56.d.matcher(str).matches()) {
            return Boolean.FALSE;
        }
        String str2 = this.b;
        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 28 + String.valueOf(str).length());
        sb.append("Invalid boolean value for ");
        sb.append(str2);
        sb.append(": ");
        sb.append(str);
        return null;
    }

    @Override // defpackage.r45
    /* renamed from: r */
    public final Boolean c(SharedPreferences sharedPreferences) {
        try {
            return Boolean.valueOf(sharedPreferences.getBoolean(this.b, false));
        } catch (ClassCastException unused) {
            String valueOf = String.valueOf(this.b);
            if (valueOf.length() != 0) {
                "Invalid boolean value in SharedPreferences for ".concat(valueOf);
                return null;
            }
            return null;
        }
    }
}
