package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;

/* compiled from: Builders.common.kt */
/* renamed from: bz1  reason: default package */
/* loaded from: classes2.dex */
public final class bz1 extends ls3 {
    public final q70<te4> g0;

    public bz1(CoroutineContext coroutineContext, hd1<? super c90, ? super q70<? super te4>, ? extends Object> hd1Var) {
        super(coroutineContext, false);
        this.g0 = IntrinsicsKt__IntrinsicsJvmKt.b(hd1Var, this, this);
    }

    @Override // defpackage.bu1
    public void s0() {
        sv.a(this.g0, this);
    }
}
