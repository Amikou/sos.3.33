package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.gc4;
import defpackage.l4;
import java.util.Collections;
import zendesk.support.request.CellBase;

/* compiled from: LatmReader.java */
/* renamed from: my1  reason: default package */
/* loaded from: classes.dex */
public final class my1 implements ku0 {
    public final String a;
    public final op2 b;
    public final np2 c;
    public f84 d;
    public String e;
    public j f;
    public int g;
    public int h;
    public int i;
    public int j;
    public long k;
    public boolean l;
    public int m;
    public int n;
    public int o;
    public boolean p;
    public long q;
    public int r;
    public long s;
    public int t;
    public String u;

    public my1(String str) {
        this.a = str;
        op2 op2Var = new op2((int) RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE);
        this.b = op2Var;
        this.c = new np2(op2Var.d());
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static long b(np2 np2Var) {
        return np2Var.h((np2Var.h(2) + 1) * 8);
    }

    @Override // defpackage.ku0
    public void a(op2 op2Var) throws ParserException {
        ii.i(this.d);
        while (op2Var.a() > 0) {
            int i = this.g;
            if (i != 0) {
                if (i == 1) {
                    int D = op2Var.D();
                    if ((D & 224) == 224) {
                        this.j = D;
                        this.g = 2;
                    } else if (D != 86) {
                        this.g = 0;
                    }
                } else if (i == 2) {
                    int D2 = ((this.j & (-225)) << 8) | op2Var.D();
                    this.i = D2;
                    if (D2 > this.b.d().length) {
                        m(this.i);
                    }
                    this.h = 0;
                    this.g = 3;
                } else if (i == 3) {
                    int min = Math.min(op2Var.a(), this.i - this.h);
                    op2Var.j(this.c.a, this.h, min);
                    int i2 = this.h + min;
                    this.h = i2;
                    if (i2 == this.i) {
                        this.c.p(0);
                        g(this.c);
                        this.g = 0;
                    }
                } else {
                    throw new IllegalStateException();
                }
            } else if (op2Var.D() == 86) {
                this.g = 1;
            }
        }
    }

    @Override // defpackage.ku0
    public void c() {
        this.g = 0;
        this.k = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.l = false;
    }

    @Override // defpackage.ku0
    public void d() {
    }

    @Override // defpackage.ku0
    public void e(long j, int i) {
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.k = j;
        }
    }

    @Override // defpackage.ku0
    public void f(r11 r11Var, gc4.d dVar) {
        dVar.a();
        this.d = r11Var.f(dVar.c(), 1);
        this.e = dVar.b();
    }

    public final void g(np2 np2Var) throws ParserException {
        if (!np2Var.g()) {
            this.l = true;
            l(np2Var);
        } else if (!this.l) {
            return;
        }
        if (this.m == 0) {
            if (this.n == 0) {
                k(np2Var, j(np2Var));
                if (this.p) {
                    np2Var.r((int) this.q);
                    return;
                }
                return;
            }
            throw ParserException.createForMalformedContainer(null, null);
        }
        throw ParserException.createForMalformedContainer(null, null);
    }

    public final int h(np2 np2Var) throws ParserException {
        int b = np2Var.b();
        l4.b d = l4.d(np2Var, true);
        this.u = d.c;
        this.r = d.a;
        this.t = d.b;
        return b - np2Var.b();
    }

    public final void i(np2 np2Var) {
        int h = np2Var.h(3);
        this.o = h;
        if (h == 0) {
            np2Var.r(8);
        } else if (h == 1) {
            np2Var.r(9);
        } else if (h == 3 || h == 4 || h == 5) {
            np2Var.r(6);
        } else if (h != 6 && h != 7) {
            throw new IllegalStateException();
        } else {
            np2Var.r(1);
        }
    }

    public final int j(np2 np2Var) throws ParserException {
        int h;
        if (this.o == 0) {
            int i = 0;
            do {
                h = np2Var.h(8);
                i += h;
            } while (h == 255);
            return i;
        }
        throw ParserException.createForMalformedContainer(null, null);
    }

    public final void k(np2 np2Var, int i) {
        int e = np2Var.e();
        if ((e & 7) == 0) {
            this.b.P(e >> 3);
        } else {
            np2Var.i(this.b.d(), 0, i * 8);
            this.b.P(0);
        }
        this.d.a(this.b, i);
        long j = this.k;
        if (j != CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED) {
            this.d.b(j, 1, i, 0, null);
            this.k += this.s;
        }
    }

    public final void l(np2 np2Var) throws ParserException {
        boolean g;
        int h = np2Var.h(1);
        int h2 = h == 1 ? np2Var.h(1) : 0;
        this.m = h2;
        if (h2 == 0) {
            if (h == 1) {
                b(np2Var);
            }
            if (np2Var.g()) {
                this.n = np2Var.h(6);
                int h3 = np2Var.h(4);
                int h4 = np2Var.h(3);
                if (h3 == 0 && h4 == 0) {
                    if (h == 0) {
                        int e = np2Var.e();
                        int h5 = h(np2Var);
                        np2Var.p(e);
                        byte[] bArr = new byte[(h5 + 7) / 8];
                        np2Var.i(bArr, 0, h5);
                        j E = new j.b().S(this.e).e0("audio/mp4a-latm").I(this.u).H(this.t).f0(this.r).T(Collections.singletonList(bArr)).V(this.a).E();
                        if (!E.equals(this.f)) {
                            this.f = E;
                            this.s = 1024000000 / E.D0;
                            this.d.f(E);
                        }
                    } else {
                        np2Var.r(((int) b(np2Var)) - h(np2Var));
                    }
                    i(np2Var);
                    boolean g2 = np2Var.g();
                    this.p = g2;
                    this.q = 0L;
                    if (g2) {
                        if (h == 1) {
                            this.q = b(np2Var);
                        } else {
                            do {
                                g = np2Var.g();
                                this.q = (this.q << 8) + np2Var.h(8);
                            } while (g);
                        }
                    }
                    if (np2Var.g()) {
                        np2Var.r(8);
                        return;
                    }
                    return;
                }
                throw ParserException.createForMalformedContainer(null, null);
            }
            throw ParserException.createForMalformedContainer(null, null);
        }
        throw ParserException.createForMalformedContainer(null, null);
    }

    public final void m(int i) {
        this.b.L(i);
        this.c.n(this.b.d());
    }
}
