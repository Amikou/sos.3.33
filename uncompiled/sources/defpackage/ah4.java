package defpackage;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.deser.i;

/* compiled from: ValueInstantiators.java */
/* renamed from: ah4  reason: default package */
/* loaded from: classes.dex */
public interface ah4 {

    /* compiled from: ValueInstantiators.java */
    /* renamed from: ah4$a */
    /* loaded from: classes.dex */
    public static class a implements ah4 {
        @Override // defpackage.ah4
        public i findValueInstantiator(DeserializationConfig deserializationConfig, so soVar, i iVar) {
            return iVar;
        }
    }

    i findValueInstantiator(DeserializationConfig deserializationConfig, so soVar, i iVar);
}
