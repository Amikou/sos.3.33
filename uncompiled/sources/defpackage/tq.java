package defpackage;

import okio.b;
import okio.m;
import okio.o;

/* compiled from: Okio.kt */
/* renamed from: tq  reason: default package */
/* loaded from: classes2.dex */
public final class tq implements m {
    @Override // okio.m, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
    }

    @Override // okio.m, java.io.Flushable
    public void flush() {
    }

    @Override // okio.m
    public o timeout() {
        return o.NONE;
    }

    @Override // okio.m
    public void write(b bVar, long j) {
        fs1.f(bVar, "source");
        bVar.skip(j);
    }
}
