package defpackage;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.RecentlyNonNull;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: rg1  reason: default package */
/* loaded from: classes.dex */
public abstract class rg1 {
    public static int a = 4225;
    public static final Object b = new Object();
    public static rg1 c;

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* renamed from: rg1$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static final Uri f = new Uri.Builder().scheme(PublicResolver.FUNC_CONTENT).authority("com.google.android.gms.chimera").build();
        public final String a;
        public final String b;
        public final ComponentName c = null;
        public final int d;
        public final boolean e;

        public a(String str, String str2, int i, boolean z) {
            this.a = zt2.f(str);
            this.b = zt2.f(str2);
            this.d = i;
            this.e = z;
        }

        public final Intent a(Context context) {
            if (this.a != null) {
                Intent d = this.e ? d(context) : null;
                return d == null ? new Intent(this.a).setPackage(this.b) : d;
            }
            return new Intent().setComponent(this.c);
        }

        public final String b() {
            return this.b;
        }

        public final ComponentName c() {
            return this.c;
        }

        public final Intent d(Context context) {
            Bundle bundle;
            Bundle bundle2 = new Bundle();
            bundle2.putString("serviceActionBundleKey", this.a);
            try {
                bundle = context.getContentResolver().call(f, "serviceIntentCall", (String) null, bundle2);
            } catch (IllegalArgumentException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(valueOf.length() + 34);
                sb.append("Dynamic intent resolution failed: ");
                sb.append(valueOf);
                bundle = null;
            }
            Intent intent = bundle != null ? (Intent) bundle.getParcelable("serviceResponseIntentKey") : null;
            if (intent == null) {
                String valueOf2 = String.valueOf(this.a);
                if (valueOf2.length() != 0) {
                    "Dynamic lookup for intent failed for action: ".concat(valueOf2);
                }
            }
            return intent;
        }

        public final int e() {
            return this.d;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof a) {
                a aVar = (a) obj;
                return pl2.a(this.a, aVar.a) && pl2.a(this.b, aVar.b) && pl2.a(this.c, aVar.c) && this.d == aVar.d && this.e == aVar.e;
            }
            return false;
        }

        public final int hashCode() {
            return pl2.b(this.a, this.b, this.c, Integer.valueOf(this.d), Boolean.valueOf(this.e));
        }

        public final String toString() {
            String str = this.a;
            if (str == null) {
                zt2.j(this.c);
                return this.c.flattenToString();
            }
            return str;
        }
    }

    @RecentlyNonNull
    public static int a() {
        return a;
    }

    @RecentlyNonNull
    public static rg1 b(@RecentlyNonNull Context context) {
        synchronized (b) {
            if (c == null) {
                c = new rk5(context.getApplicationContext());
            }
        }
        return c;
    }

    public final void c(@RecentlyNonNull String str, @RecentlyNonNull String str2, @RecentlyNonNull int i, @RecentlyNonNull ServiceConnection serviceConnection, @RecentlyNonNull String str3, @RecentlyNonNull boolean z) {
        e(new a(str, str2, i, z), serviceConnection, str3);
    }

    public abstract boolean d(a aVar, ServiceConnection serviceConnection, String str);

    public abstract void e(a aVar, ServiceConnection serviceConnection, String str);
}
