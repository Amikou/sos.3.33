package defpackage;

import androidx.media3.exoplayer.drm.g;
import androidx.media3.exoplayer.drm.h;
import java.util.UUID;

/* renamed from: ec1  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class ec1 implements g.c {
    public static final /* synthetic */ ec1 a = new ec1();

    @Override // androidx.media3.exoplayer.drm.g.c
    public final g a(UUID uuid) {
        g A;
        A = h.A(uuid);
        return A;
    }
}
