package defpackage;

import androidx.savedstate.SavedStateRegistry;

/* compiled from: SavedStateRegistryOwner.java */
/* renamed from: fc3  reason: default package */
/* loaded from: classes.dex */
public interface fc3 extends rz1 {
    SavedStateRegistry getSavedStateRegistry();
}
