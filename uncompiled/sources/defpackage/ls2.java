package defpackage;

import androidx.media3.common.u;
import androidx.media3.common.util.b;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/* compiled from: PlaylistTimeline.java */
/* renamed from: ls2  reason: default package */
/* loaded from: classes.dex */
public final class ls2 extends s4 {
    public final int i0;
    public final int j0;
    public final int[] k0;
    public final int[] l0;
    public final u[] m0;
    public final Object[] n0;
    public final HashMap<Object, Integer> o0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ls2(Collection<? extends z62> collection, qo3 qo3Var) {
        super(false, qo3Var);
        int i = 0;
        int size = collection.size();
        this.k0 = new int[size];
        this.l0 = new int[size];
        this.m0 = new u[size];
        this.n0 = new Object[size];
        this.o0 = new HashMap<>();
        int i2 = 0;
        int i3 = 0;
        for (z62 z62Var : collection) {
            this.m0[i3] = z62Var.b();
            this.l0[i3] = i;
            this.k0[i3] = i2;
            i += this.m0[i3].p();
            i2 += this.m0[i3].i();
            this.n0[i3] = z62Var.a();
            this.o0.put(this.n0[i3], Integer.valueOf(i3));
            i3++;
        }
        this.i0 = i;
        this.j0 = i2;
    }

    @Override // defpackage.s4
    public int B(int i) {
        return this.k0[i];
    }

    @Override // defpackage.s4
    public int C(int i) {
        return this.l0[i];
    }

    @Override // defpackage.s4
    public u F(int i) {
        return this.m0[i];
    }

    public List<u> G() {
        return Arrays.asList(this.m0);
    }

    @Override // androidx.media3.common.u
    public int i() {
        return this.j0;
    }

    @Override // androidx.media3.common.u
    public int p() {
        return this.i0;
    }

    @Override // defpackage.s4
    public int u(Object obj) {
        Integer num = this.o0.get(obj);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    @Override // defpackage.s4
    public int v(int i) {
        return b.h(this.k0, i + 1, false, false);
    }

    @Override // defpackage.s4
    public int w(int i) {
        return b.h(this.l0, i + 1, false, false);
    }

    @Override // defpackage.s4
    public Object z(int i) {
        return this.n0[i];
    }
}
