package defpackage;

import android.util.Base64;
import android.util.JsonWriter;
import com.google.firebase.encoders.EncodingException;
import com.google.firebase.encoders.c;
import com.google.firebase.encoders.d;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/* compiled from: JsonValueObjectEncoderContext.java */
/* renamed from: lw1  reason: default package */
/* loaded from: classes2.dex */
public final class lw1 implements c, d {
    public lw1 a = null;
    public boolean b = true;
    public final JsonWriter c;
    public final Map<Class<?>, hl2<?>> d;
    public final Map<Class<?>, zg4<?>> e;
    public final hl2<Object> f;
    public final boolean g;

    public lw1(Writer writer, Map<Class<?>, hl2<?>> map, Map<Class<?>, zg4<?>> map2, hl2<Object> hl2Var, boolean z) {
        this.c = new JsonWriter(writer);
        this.d = map;
        this.e = map2;
        this.f = hl2Var;
        this.g = z;
    }

    @Override // com.google.firebase.encoders.c
    public c a(h31 h31Var, Object obj) throws IOException {
        return m(h31Var.b(), obj);
    }

    @Override // com.google.firebase.encoders.c
    public c d(h31 h31Var, boolean z) throws IOException {
        return n(h31Var.b(), z);
    }

    @Override // com.google.firebase.encoders.c
    public c e(h31 h31Var, int i) throws IOException {
        return k(h31Var.b(), i);
    }

    @Override // com.google.firebase.encoders.c
    public c f(h31 h31Var, long j) throws IOException {
        return l(h31Var.b(), j);
    }

    public lw1 g(int i) throws IOException {
        v();
        this.c.value(i);
        return this;
    }

    public lw1 h(long j) throws IOException {
        v();
        this.c.value(j);
        return this;
    }

    public lw1 i(Object obj, boolean z) throws IOException {
        int[] iArr;
        int i = 0;
        if (z && q(obj)) {
            Object[] objArr = new Object[1];
            objArr[0] = obj == null ? null : obj.getClass();
            throw new EncodingException(String.format("%s cannot be encoded inline", objArr));
        } else if (obj == null) {
            this.c.nullValue();
            return this;
        } else if (obj instanceof Number) {
            this.c.value((Number) obj);
            return this;
        } else if (obj.getClass().isArray()) {
            if (obj instanceof byte[]) {
                return p((byte[]) obj);
            }
            this.c.beginArray();
            if (obj instanceof int[]) {
                int length = ((int[]) obj).length;
                while (i < length) {
                    this.c.value(iArr[i]);
                    i++;
                }
            } else if (obj instanceof long[]) {
                long[] jArr = (long[]) obj;
                int length2 = jArr.length;
                while (i < length2) {
                    h(jArr[i]);
                    i++;
                }
            } else if (obj instanceof double[]) {
                double[] dArr = (double[]) obj;
                int length3 = dArr.length;
                while (i < length3) {
                    this.c.value(dArr[i]);
                    i++;
                }
            } else if (obj instanceof boolean[]) {
                boolean[] zArr = (boolean[]) obj;
                int length4 = zArr.length;
                while (i < length4) {
                    this.c.value(zArr[i]);
                    i++;
                }
            } else if (obj instanceof Number[]) {
                for (Number number : (Number[]) obj) {
                    i(number, false);
                }
            } else {
                for (Object obj2 : (Object[]) obj) {
                    i(obj2, false);
                }
            }
            this.c.endArray();
            return this;
        } else if (obj instanceof Collection) {
            this.c.beginArray();
            for (Object obj3 : (Collection) obj) {
                i(obj3, false);
            }
            this.c.endArray();
            return this;
        } else if (obj instanceof Map) {
            this.c.beginObject();
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                Object key = entry.getKey();
                try {
                    m((String) key, entry.getValue());
                } catch (ClassCastException e) {
                    throw new EncodingException(String.format("Only String keys are currently supported in maps, got %s of type %s instead.", key, key.getClass()), e);
                }
            }
            this.c.endObject();
            return this;
        } else {
            hl2<?> hl2Var = this.d.get(obj.getClass());
            if (hl2Var != null) {
                return s(hl2Var, obj, z);
            }
            zg4<?> zg4Var = this.e.get(obj.getClass());
            if (zg4Var != null) {
                zg4Var.a(obj, this);
                return this;
            } else if (obj instanceof Enum) {
                b(((Enum) obj).name());
                return this;
            } else {
                return s(this.f, obj, z);
            }
        }
    }

    @Override // com.google.firebase.encoders.d
    /* renamed from: j */
    public lw1 b(String str) throws IOException {
        v();
        this.c.value(str);
        return this;
    }

    public lw1 k(String str, int i) throws IOException {
        v();
        this.c.name(str);
        return g(i);
    }

    public lw1 l(String str, long j) throws IOException {
        v();
        this.c.name(str);
        return h(j);
    }

    public lw1 m(String str, Object obj) throws IOException {
        if (this.g) {
            return u(str, obj);
        }
        return t(str, obj);
    }

    public lw1 n(String str, boolean z) throws IOException {
        v();
        this.c.name(str);
        return c(z);
    }

    @Override // com.google.firebase.encoders.d
    /* renamed from: o */
    public lw1 c(boolean z) throws IOException {
        v();
        this.c.value(z);
        return this;
    }

    public lw1 p(byte[] bArr) throws IOException {
        v();
        if (bArr == null) {
            this.c.nullValue();
        } else {
            this.c.value(Base64.encodeToString(bArr, 2));
        }
        return this;
    }

    public final boolean q(Object obj) {
        return obj == null || obj.getClass().isArray() || (obj instanceof Collection) || (obj instanceof Date) || (obj instanceof Enum) || (obj instanceof Number);
    }

    public void r() throws IOException {
        v();
        this.c.flush();
    }

    public lw1 s(hl2<Object> hl2Var, Object obj, boolean z) throws IOException {
        if (!z) {
            this.c.beginObject();
        }
        hl2Var.a(obj, this);
        if (!z) {
            this.c.endObject();
        }
        return this;
    }

    public final lw1 t(String str, Object obj) throws IOException, EncodingException {
        v();
        this.c.name(str);
        if (obj == null) {
            this.c.nullValue();
            return this;
        }
        return i(obj, false);
    }

    public final lw1 u(String str, Object obj) throws IOException, EncodingException {
        if (obj == null) {
            return this;
        }
        v();
        this.c.name(str);
        return i(obj, false);
    }

    public final void v() throws IOException {
        if (this.b) {
            lw1 lw1Var = this.a;
            if (lw1Var != null) {
                lw1Var.v();
                this.a.b = false;
                this.a = null;
                this.c.endObject();
                return;
            }
            return;
        }
        throw new IllegalStateException("Parent context used since this context was created. Cannot use this context anymore.");
    }
}
