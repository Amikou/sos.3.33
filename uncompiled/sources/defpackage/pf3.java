package defpackage;

import defpackage.pt0;

/* renamed from: pf3  reason: default package */
/* loaded from: classes2.dex */
public class pf3 extends pt0.c {
    public pf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public pf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public pf3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        of3 of3Var = (of3) this.c;
        if (of3Var.i()) {
            return i.v();
        }
        of3 of3Var2 = (of3) this.b;
        of3 of3Var3 = (of3) this.d[0];
        int[] j = kd2.j(12);
        int[] j2 = kd2.j(12);
        int[] j3 = kd2.j(12);
        nf3.j(of3Var.f, j3);
        int[] j4 = kd2.j(12);
        nf3.j(j3, j4);
        boolean h = of3Var3.h();
        int[] iArr = of3Var3.f;
        if (!h) {
            nf3.j(iArr, j2);
            iArr = j2;
        }
        nf3.m(of3Var2.f, iArr, j);
        nf3.a(of3Var2.f, iArr, j2);
        nf3.f(j2, j, j2);
        nf3.i(kd2.c(12, j2, j2, j2), j2);
        nf3.f(j3, of3Var2.f, j3);
        nf3.i(kd2.G(12, j3, 2, 0), j3);
        nf3.i(kd2.H(12, j4, 3, 0, j), j);
        of3 of3Var4 = new of3(j4);
        nf3.j(j2, of3Var4.f);
        int[] iArr2 = of3Var4.f;
        nf3.m(iArr2, j3, iArr2);
        int[] iArr3 = of3Var4.f;
        nf3.m(iArr3, j3, iArr3);
        of3 of3Var5 = new of3(j3);
        nf3.m(j3, of3Var4.f, of3Var5.f);
        int[] iArr4 = of3Var5.f;
        nf3.f(iArr4, j2, iArr4);
        int[] iArr5 = of3Var5.f;
        nf3.m(iArr5, j, iArr5);
        of3 of3Var6 = new of3(j2);
        nf3.n(of3Var.f, of3Var6.f);
        if (!h) {
            int[] iArr6 = of3Var6.f;
            nf3.f(iArr6, of3Var3.f, iArr6);
        }
        return new pf3(i, of3Var4, of3Var5, new ct0[]{of3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        of3 of3Var = (of3) this.b;
        of3 of3Var2 = (of3) this.c;
        of3 of3Var3 = (of3) pt0Var.q();
        of3 of3Var4 = (of3) pt0Var.r();
        of3 of3Var5 = (of3) this.d[0];
        of3 of3Var6 = (of3) pt0Var.s(0);
        int[] j = kd2.j(24);
        int[] j2 = kd2.j(24);
        int[] j3 = kd2.j(12);
        int[] j4 = kd2.j(12);
        boolean h = of3Var5.h();
        if (h) {
            iArr = of3Var3.f;
            iArr2 = of3Var4.f;
        } else {
            nf3.j(of3Var5.f, j3);
            nf3.f(j3, of3Var3.f, j2);
            nf3.f(j3, of3Var5.f, j3);
            nf3.f(j3, of3Var4.f, j3);
            iArr = j2;
            iArr2 = j3;
        }
        boolean h2 = of3Var6.h();
        if (h2) {
            iArr3 = of3Var.f;
            iArr4 = of3Var2.f;
        } else {
            nf3.j(of3Var6.f, j4);
            nf3.f(j4, of3Var.f, j);
            nf3.f(j4, of3Var6.f, j4);
            nf3.f(j4, of3Var2.f, j4);
            iArr3 = j;
            iArr4 = j4;
        }
        int[] j5 = kd2.j(12);
        nf3.m(iArr3, iArr, j5);
        int[] j6 = kd2.j(12);
        nf3.m(iArr4, iArr2, j6);
        if (kd2.w(12, j5)) {
            return kd2.w(12, j6) ? J() : i.v();
        }
        nf3.j(j5, j3);
        int[] j7 = kd2.j(12);
        nf3.f(j3, j5, j7);
        nf3.f(j3, iArr3, j3);
        nf3.g(j7, j7);
        gd2.a(iArr4, j7, j);
        nf3.i(kd2.c(12, j3, j3, j7), j7);
        of3 of3Var7 = new of3(j4);
        nf3.j(j6, of3Var7.f);
        int[] iArr5 = of3Var7.f;
        nf3.m(iArr5, j7, iArr5);
        of3 of3Var8 = new of3(j7);
        nf3.m(j3, of3Var7.f, of3Var8.f);
        gd2.a(of3Var8.f, j6, j2);
        nf3.b(j, j2, j);
        nf3.h(j, of3Var8.f);
        of3 of3Var9 = new of3(j5);
        if (!h) {
            int[] iArr6 = of3Var9.f;
            nf3.f(iArr6, of3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = of3Var9.f;
            nf3.f(iArr7, of3Var6.f, iArr7);
        }
        return new pf3(i, of3Var7, of3Var8, new ct0[]{of3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new pf3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new pf3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
