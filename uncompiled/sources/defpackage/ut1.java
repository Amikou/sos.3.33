package defpackage;

/* compiled from: JobSupport.kt */
/* renamed from: ut1  reason: default package */
/* loaded from: classes2.dex */
public class ut1 extends bu1 implements q30 {
    public final boolean f0;

    public ut1(st1 st1Var) {
        super(true);
        d0(st1Var);
        this.f0 = H0();
    }

    public final boolean H0() {
        fy Z = Z();
        gy gyVar = Z instanceof gy ? (gy) Z : null;
        if (gyVar == null) {
            return false;
        }
        bu1 z = gyVar.z();
        while (!z.W()) {
            fy Z2 = z.Z();
            gy gyVar2 = Z2 instanceof gy ? (gy) Z2 : null;
            if (gyVar2 == null) {
                return false;
            }
            z = gyVar2.z();
        }
        return true;
    }

    @Override // defpackage.bu1
    public boolean W() {
        return this.f0;
    }

    @Override // defpackage.bu1
    public boolean X() {
        return true;
    }
}
