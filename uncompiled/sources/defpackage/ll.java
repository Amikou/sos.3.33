package defpackage;

import java.util.Objects;

/* compiled from: AutoValue_LibraryVersion.java */
/* renamed from: ll  reason: default package */
/* loaded from: classes2.dex */
public final class ll extends hz1 {
    public final String a;
    public final String b;

    public ll(String str, String str2) {
        Objects.requireNonNull(str, "Null libraryName");
        this.a = str;
        Objects.requireNonNull(str2, "Null version");
        this.b = str2;
    }

    @Override // defpackage.hz1
    public String b() {
        return this.a;
    }

    @Override // defpackage.hz1
    public String c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof hz1) {
            hz1 hz1Var = (hz1) obj;
            return this.a.equals(hz1Var.b()) && this.b.equals(hz1Var.c());
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    public String toString() {
        return "LibraryVersion{libraryName=" + this.a + ", version=" + this.b + "}";
    }
}
