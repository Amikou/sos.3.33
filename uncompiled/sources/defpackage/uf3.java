package defpackage;

import java.math.BigInteger;

/* renamed from: uf3  reason: default package */
/* loaded from: classes2.dex */
public class uf3 {
    public static void a(long[] jArr, long[] jArr2, long[] jArr3) {
        jArr3[0] = jArr[0] ^ jArr2[0];
        jArr3[1] = jArr2[1] ^ jArr[1];
    }

    public static void b(long[] jArr, long[] jArr2, long[] jArr3) {
        jArr3[0] = jArr[0] ^ jArr2[0];
        jArr3[1] = jArr[1] ^ jArr2[1];
        jArr3[2] = jArr[2] ^ jArr2[2];
        jArr3[3] = jArr2[3] ^ jArr[3];
    }

    public static void c(long[] jArr, long[] jArr2) {
        jArr2[0] = jArr[0] ^ 1;
        jArr2[1] = jArr[1];
    }

    public static long[] d(BigInteger bigInteger) {
        long[] l = ad2.l(bigInteger);
        l(l, 0);
        return l;
    }

    public static void e(long[] jArr, long[] jArr2, long[] jArr3) {
        long j = jArr[0];
        long j2 = ((jArr[1] << 7) ^ (j >>> 57)) & 144115188075855871L;
        long j3 = j & 144115188075855871L;
        long j4 = jArr2[0];
        long j5 = ((jArr2[1] << 7) ^ (j4 >>> 57)) & 144115188075855871L;
        long j6 = 144115188075855871L & j4;
        long[] jArr4 = new long[6];
        f(j3, j6, jArr4, 0);
        f(j2, j5, jArr4, 2);
        f(j3 ^ j2, j6 ^ j5, jArr4, 4);
        long j7 = jArr4[1] ^ jArr4[2];
        long j8 = jArr4[0];
        long j9 = jArr4[3];
        long j10 = (jArr4[4] ^ j8) ^ j7;
        long j11 = j7 ^ (jArr4[5] ^ j9);
        jArr3[0] = j8 ^ (j10 << 57);
        jArr3[1] = (j10 >>> 7) ^ (j11 << 50);
        jArr3[2] = (j11 >>> 14) ^ (j9 << 43);
        jArr3[3] = j9 >>> 21;
    }

    public static void f(long j, long j2, long[] jArr, int i) {
        long[] jArr2 = {0, j2, jArr2[1] << 1, jArr2[2] ^ j2, jArr2[2] << 1, jArr2[4] ^ j2, jArr2[3] << 1, jArr2[6] ^ j2};
        long j3 = jArr2[((int) j) & 7];
        long j4 = 0;
        int i2 = 48;
        do {
            int i3 = (int) (j >>> i2);
            long j5 = (jArr2[i3 & 7] ^ (jArr2[(i3 >>> 3) & 7] << 3)) ^ (jArr2[(i3 >>> 6) & 7] << 6);
            j3 ^= j5 << i2;
            j4 ^= j5 >>> (-i2);
            i2 -= 9;
        } while (i2 > 0);
        jArr[i] = 144115188075855871L & j3;
        jArr[i + 1] = (((((j & 72198606942111744L) & ((j2 << 7) >> 63)) >>> 8) ^ j4) << 7) ^ (j3 >>> 57);
    }

    public static void g(long[] jArr, long[] jArr2) {
        bs1.c(jArr[0], jArr2, 0);
        bs1.c(jArr[1], jArr2, 2);
    }

    public static void h(long[] jArr, long[] jArr2) {
        if (ad2.r(jArr)) {
            throw new IllegalStateException();
        }
        long[] f = ad2.f();
        long[] f2 = ad2.f();
        n(jArr, f);
        i(f, jArr, f);
        n(f, f);
        i(f, jArr, f);
        p(f, 3, f2);
        i(f2, f, f2);
        n(f2, f2);
        i(f2, jArr, f2);
        p(f2, 7, f);
        i(f, f2, f);
        p(f, 14, f2);
        i(f2, f, f2);
        p(f2, 28, f);
        i(f, f2, f);
        p(f, 56, f2);
        i(f2, f, f2);
        n(f2, jArr2);
    }

    public static void i(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] h = ad2.h();
        e(jArr, jArr2, h);
        k(h, jArr3);
    }

    public static void j(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] h = ad2.h();
        e(jArr, jArr2, h);
        b(jArr3, h, jArr3);
    }

    public static void k(long[] jArr, long[] jArr2) {
        long j = jArr[0];
        long j2 = jArr[1];
        long j3 = jArr[2];
        long j4 = jArr[3];
        long j5 = j3 ^ ((j4 >>> 40) ^ (j4 >>> 49));
        long j6 = j ^ ((j5 << 15) ^ (j5 << 24));
        long j7 = (j2 ^ ((j4 << 15) ^ (j4 << 24))) ^ ((j5 >>> 40) ^ (j5 >>> 49));
        long j8 = j7 >>> 49;
        jArr2[0] = (j6 ^ j8) ^ (j8 << 9);
        jArr2[1] = 562949953421311L & j7;
    }

    public static void l(long[] jArr, int i) {
        int i2 = i + 1;
        long j = jArr[i2];
        long j2 = j >>> 49;
        jArr[i] = (j2 ^ (j2 << 9)) ^ jArr[i];
        jArr[i2] = j & 562949953421311L;
    }

    public static void m(long[] jArr, long[] jArr2) {
        long e = bs1.e(jArr[0]);
        long e2 = bs1.e(jArr[1]);
        long j = (e >>> 32) | (e2 & (-4294967296L));
        jArr2[0] = ((j << 57) ^ ((4294967295L & e) | (e2 << 32))) ^ (j << 5);
        jArr2[1] = (j >>> 59) ^ (j >>> 7);
    }

    public static void n(long[] jArr, long[] jArr2) {
        long[] h = ad2.h();
        g(jArr, h);
        k(h, jArr2);
    }

    public static void o(long[] jArr, long[] jArr2) {
        long[] h = ad2.h();
        g(jArr, h);
        b(jArr2, h, jArr2);
    }

    public static void p(long[] jArr, int i, long[] jArr2) {
        long[] h = ad2.h();
        g(jArr, h);
        while (true) {
            k(h, jArr2);
            i--;
            if (i <= 0) {
                return;
            }
            g(jArr2, h);
        }
    }

    public static int q(long[] jArr) {
        return ((int) jArr[0]) & 1;
    }
}
