package defpackage;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: o25  reason: default package */
/* loaded from: classes.dex */
public final class o25 extends ThreadLocal<Boolean> {
    @Override // java.lang.ThreadLocal
    public final /* synthetic */ Boolean initialValue() {
        return Boolean.FALSE;
    }
}
