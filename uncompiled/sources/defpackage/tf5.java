package defpackage;

import com.google.android.gms.internal.clearcut.m;
import com.google.android.gms.internal.clearcut.o;

/* renamed from: tf5  reason: default package */
/* loaded from: classes.dex */
public final class tf5 implements ce5 {
    public final o a;
    public final vf5 b;

    public tf5(o oVar, String str, Object[] objArr) {
        this.a = oVar;
        this.b = new vf5(oVar.getClass(), str, objArr);
    }

    @Override // defpackage.ce5
    public final int a() {
        int i;
        i = this.b.d;
        return (i & 1) == 1 ? m.e.i : m.e.j;
    }

    @Override // defpackage.ce5
    public final boolean b() {
        int i;
        i = this.b.d;
        return (i & 2) == 2;
    }

    @Override // defpackage.ce5
    public final o c() {
        return this.a;
    }

    public final int d() {
        int i;
        i = this.b.e;
        return i;
    }

    public final vf5 e() {
        return this.b;
    }

    public final int f() {
        int i;
        i = this.b.h;
        return i;
    }

    public final int g() {
        int i;
        i = this.b.i;
        return i;
    }

    public final int h() {
        int i;
        i = this.b.j;
        return i;
    }

    public final int i() {
        int i;
        i = this.b.m;
        return i;
    }

    public final int[] j() {
        int[] iArr;
        iArr = this.b.n;
        return iArr;
    }

    public final int k() {
        int i;
        i = this.b.l;
        return i;
    }

    public final int l() {
        int i;
        i = this.b.k;
        return i;
    }
}
