package defpackage;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: ViewBindings.java */
/* renamed from: ai4  reason: default package */
/* loaded from: classes.dex */
public class ai4 {
    public static <T extends View> T a(View view, int i) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                T t = (T) viewGroup.getChildAt(i2).findViewById(i);
                if (t != null) {
                    return t;
                }
            }
            return null;
        }
        return null;
    }
}
