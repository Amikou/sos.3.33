package defpackage;

import java.util.concurrent.RejectedExecutionException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;
import kotlinx.coroutines.d;
import kotlinx.coroutines.scheduling.CoroutineScheduler;

/* compiled from: Dispatcher.kt */
/* renamed from: z01  reason: default package */
/* loaded from: classes2.dex */
public class z01 extends ExecutorCoroutineDispatcher {
    public final int f0;
    public final int g0;
    public final long h0;
    public final String i0;
    public CoroutineScheduler j0;

    public z01(int i, int i2, long j, String str) {
        this.f0 = i;
        this.g0 = i2;
        this.h0 = j;
        this.i0 = str;
        this.j0 = m();
    }

    public final void M(Runnable runnable, p34 p34Var, boolean z) {
        try {
            this.j0.e(runnable, p34Var, z);
        } catch (RejectedExecutionException unused) {
            d.k0.x0(this.j0.c(runnable, p34Var));
        }
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void h(CoroutineContext coroutineContext, Runnable runnable) {
        try {
            CoroutineScheduler.f(this.j0, runnable, null, false, 6, null);
        } catch (RejectedExecutionException unused) {
            d.k0.h(coroutineContext, runnable);
        }
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void i(CoroutineContext coroutineContext, Runnable runnable) {
        try {
            CoroutineScheduler.f(this.j0, runnable, null, true, 2, null);
        } catch (RejectedExecutionException unused) {
            d.k0.i(coroutineContext, runnable);
        }
    }

    public final CoroutineScheduler m() {
        return new CoroutineScheduler(this.f0, this.g0, this.h0, this.i0);
    }

    public /* synthetic */ z01(int i, int i2, String str, int i3, qi0 qi0Var) {
        this((i3 & 1) != 0 ? w34.b : i, (i3 & 2) != 0 ? w34.c : i2, (i3 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    public z01(int i, int i2, String str) {
        this(i, i2, w34.d, str);
    }
}
