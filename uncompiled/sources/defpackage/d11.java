package defpackage;

import net.safemoon.androidwallet.model.Coin;

/* compiled from: CMCListViewModel.kt */
/* renamed from: d11  reason: default package */
/* loaded from: classes2.dex */
public final class d11 {
    public final Coin a;
    public boolean b;

    public d11(Coin coin, boolean z) {
        fs1.f(coin, "coin");
        this.a = coin;
        this.b = z;
    }

    public static /* synthetic */ d11 b(d11 d11Var, Coin coin, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            coin = d11Var.a;
        }
        if ((i & 2) != 0) {
            z = d11Var.b;
        }
        return d11Var.a(coin, z);
    }

    public final d11 a(Coin coin, boolean z) {
        fs1.f(coin, "coin");
        return new d11(coin, z);
    }

    public final Coin c() {
        return this.a;
    }

    public final boolean d() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof d11) {
            d11 d11Var = (d11) obj;
            return fs1.b(this.a, d11Var.a) && this.b == d11Var.b;
        }
        return false;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        boolean z = this.b;
        int i = z;
        if (z != 0) {
            i = 1;
        }
        return hashCode + i;
    }

    public String toString() {
        return "ExtendCoin(coin=" + this.a + ", isSelected=" + this.b + ')';
    }
}
