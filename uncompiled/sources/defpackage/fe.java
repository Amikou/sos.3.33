package defpackage;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import defpackage.de;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: AnimationBackendDelegateWithInactivityCheck.java */
/* renamed from: fe  reason: default package */
/* loaded from: classes.dex */
public class fe<T extends de> extends ee<T> {
    public final o92 b;
    public final ScheduledExecutorService c;
    public boolean d;
    public long e;
    public long f;
    public long g;
    public b h;
    public final Runnable i;

    /* compiled from: AnimationBackendDelegateWithInactivityCheck.java */
    /* renamed from: fe$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (fe.this) {
                fe.this.d = false;
                if (fe.this.p()) {
                    if (fe.this.h != null) {
                        fe.this.h.f();
                    }
                } else {
                    fe.this.q();
                }
            }
        }
    }

    /* compiled from: AnimationBackendDelegateWithInactivityCheck.java */
    /* renamed from: fe$b */
    /* loaded from: classes.dex */
    public interface b {
        void f();
    }

    public fe(T t, b bVar, o92 o92Var, ScheduledExecutorService scheduledExecutorService) {
        super(t);
        this.d = false;
        this.f = 2000L;
        this.g = 1000L;
        this.i = new a();
        this.h = bVar;
        this.b = o92Var;
        this.c = scheduledExecutorService;
    }

    public static <T extends de> ee<T> n(T t, b bVar, o92 o92Var, ScheduledExecutorService scheduledExecutorService) {
        return new fe(t, bVar, o92Var, scheduledExecutorService);
    }

    public static <T extends de & b> ee<T> o(T t, o92 o92Var, ScheduledExecutorService scheduledExecutorService) {
        return n(t, (b) t, o92Var, scheduledExecutorService);
    }

    @Override // defpackage.ee, defpackage.de
    public boolean j(Drawable drawable, Canvas canvas, int i) {
        this.e = this.b.now();
        boolean j = super.j(drawable, canvas, i);
        q();
        return j;
    }

    public final boolean p() {
        return this.b.now() - this.e > this.f;
    }

    public final synchronized void q() {
        if (!this.d) {
            this.d = true;
            this.c.schedule(this.i, this.g, TimeUnit.MILLISECONDS);
        }
    }
}
