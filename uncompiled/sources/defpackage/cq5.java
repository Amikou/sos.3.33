package defpackage;

import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: cq5  reason: default package */
/* loaded from: classes.dex */
public final class cq5<T> implements yp5<T> {
    public volatile yp5<T> a;
    public volatile boolean f0;
    public T g0;

    public cq5(yp5<T> yp5Var) {
        Objects.requireNonNull(yp5Var);
        this.a = yp5Var;
    }

    public final String toString() {
        Object obj = this.a;
        if (obj == null) {
            String valueOf = String.valueOf(this.g0);
            StringBuilder sb = new StringBuilder(valueOf.length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(">");
            obj = sb.toString();
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(valueOf2.length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @Override // defpackage.yp5
    public final T zza() {
        if (!this.f0) {
            synchronized (this) {
                if (!this.f0) {
                    T zza = this.a.zza();
                    this.g0 = zza;
                    this.f0 = true;
                    this.a = null;
                    return zza;
                }
            }
        }
        return this.g0;
    }
}
