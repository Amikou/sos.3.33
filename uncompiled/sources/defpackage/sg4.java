package defpackage;

import org.bouncycastle.asn1.h0;

/* renamed from: sg4  reason: default package */
/* loaded from: classes2.dex */
public class sg4 {
    public static va a(String str) {
        if (str.equals("SHA-1")) {
            return new va(hj2.a, h0.a);
        }
        if (str.equals("SHA-224")) {
            return new va(tc2.f, h0.a);
        }
        if (str.equals("SHA-256")) {
            return new va(tc2.c, h0.a);
        }
        if (str.equals("SHA-384")) {
            return new va(tc2.d, h0.a);
        }
        if (str.equals("SHA-512")) {
            return new va(tc2.e, h0.a);
        }
        throw new IllegalArgumentException("unrecognised digest algorithm: " + str);
    }

    public static qo0 b(va vaVar) {
        if (vaVar.o().equals(hj2.a)) {
            return ro0.a();
        }
        if (vaVar.o().equals(tc2.f)) {
            return ro0.b();
        }
        if (vaVar.o().equals(tc2.c)) {
            return ro0.c();
        }
        if (vaVar.o().equals(tc2.d)) {
            return ro0.d();
        }
        if (vaVar.o().equals(tc2.e)) {
            return ro0.e();
        }
        throw new IllegalArgumentException("unrecognised OID in digest algorithm identifier: " + vaVar.o());
    }
}
