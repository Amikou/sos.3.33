package defpackage;

import android.database.Cursor;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;

/* compiled from: SupportSQLiteDatabase.java */
/* renamed from: sw3  reason: default package */
/* loaded from: classes.dex */
public interface sw3 extends Closeable {
    void B();

    Cursor D(vw3 vw3Var);

    Cursor E0(String str);

    List<Pair<String, String>> G();

    void K(String str) throws SQLException;

    void N0();

    ww3 U(String str);

    String getPath();

    boolean h1();

    boolean isOpen();

    boolean q1();

    void s0();

    Cursor t0(vw3 vw3Var, CancellationSignal cancellationSignal);

    void u0(String str, Object[] objArr) throws SQLException;

    void v0();
}
