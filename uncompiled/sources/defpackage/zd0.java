package defpackage;

import java.security.MessageDigest;

/* compiled from: DataCacheKey.java */
/* renamed from: zd0  reason: default package */
/* loaded from: classes.dex */
public final class zd0 implements fx1 {
    public final fx1 b;
    public final fx1 c;

    public zd0(fx1 fx1Var, fx1 fx1Var2) {
        this.b = fx1Var;
        this.c = fx1Var2;
    }

    @Override // defpackage.fx1
    public void b(MessageDigest messageDigest) {
        this.b.b(messageDigest);
        this.c.b(messageDigest);
    }

    @Override // defpackage.fx1
    public boolean equals(Object obj) {
        if (obj instanceof zd0) {
            zd0 zd0Var = (zd0) obj;
            return this.b.equals(zd0Var.b) && this.c.equals(zd0Var.c);
        }
        return false;
    }

    @Override // defpackage.fx1
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }
}
