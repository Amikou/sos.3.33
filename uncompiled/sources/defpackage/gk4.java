package defpackage;

import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintAttribute;
import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ViewTimeCycle.java */
/* renamed from: gk4  reason: default package */
/* loaded from: classes.dex */
public abstract class gk4 extends z54 {

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$a */
    /* loaded from: classes.dex */
    public static class a extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setAlpha(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$b */
    /* loaded from: classes.dex */
    public static class b extends gk4 {
        public String l;
        public SparseArray<ConstraintAttribute> m;
        public SparseArray<float[]> n = new SparseArray<>();
        public float[] o;
        public float[] p;

        public b(String str, SparseArray<ConstraintAttribute> sparseArray) {
            this.l = str.split(",")[1];
            this.m = sparseArray;
        }

        @Override // defpackage.z54
        public void b(int i, float f, float f2, int i2, float f3) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute,...)");
        }

        @Override // defpackage.z54
        public void e(int i) {
            float[] fArr;
            int size = this.m.size();
            int h = this.m.valueAt(0).h();
            double[] dArr = new double[size];
            int i2 = h + 2;
            this.o = new float[i2];
            this.p = new float[h];
            double[][] dArr2 = (double[][]) Array.newInstance(double.class, size, i2);
            for (int i3 = 0; i3 < size; i3++) {
                int keyAt = this.m.keyAt(i3);
                float[] valueAt = this.n.valueAt(i3);
                dArr[i3] = keyAt * 0.01d;
                this.m.valueAt(i3).f(this.o);
                int i4 = 0;
                while (true) {
                    if (i4 < this.o.length) {
                        dArr2[i3][i4] = fArr[i4];
                        i4++;
                    }
                }
                dArr2[i3][h] = valueAt[0];
                dArr2[i3][h + 1] = valueAt[1];
            }
            this.a = bc0.a(i, dArr, dArr2);
        }

        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            this.a.e(f, this.o);
            float[] fArr = this.o;
            float f2 = fArr[fArr.length - 2];
            float f3 = fArr[fArr.length - 1];
            long j2 = j - this.i;
            if (Float.isNaN(this.j)) {
                float a = gx1Var.a(view, this.l, 0);
                this.j = a;
                if (Float.isNaN(a)) {
                    this.j = Utils.FLOAT_EPSILON;
                }
            }
            float f4 = (float) ((this.j + ((j2 * 1.0E-9d) * f2)) % 1.0d);
            this.j = f4;
            this.i = j;
            float a2 = a(f4);
            this.h = false;
            int i = 0;
            while (true) {
                float[] fArr2 = this.p;
                if (i >= fArr2.length) {
                    break;
                }
                boolean z = this.h;
                float[] fArr3 = this.o;
                this.h = z | (((double) fArr3[i]) != Utils.DOUBLE_EPSILON);
                fArr2[i] = (fArr3[i] * a2) + f3;
                i++;
            }
            this.m.valueAt(0).k(view, this.p);
            if (f2 != Utils.FLOAT_EPSILON) {
                this.h = true;
            }
            return this.h;
        }

        public void j(int i, ConstraintAttribute constraintAttribute, float f, int i2, float f2) {
            this.m.append(i, constraintAttribute);
            this.n.append(i, new float[]{f, f2});
            this.b = Math.max(this.b, i2);
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$c */
    /* loaded from: classes.dex */
    public static class c extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setElevation(f(f, j, view, gx1Var));
            }
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$d */
    /* loaded from: classes.dex */
    public static class d extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            return this.h;
        }

        public boolean j(View view, gx1 gx1Var, float f, long j, double d, double d2) {
            view.setRotation(f(f, j, view, gx1Var) + ((float) Math.toDegrees(Math.atan2(d2, d))));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$e */
    /* loaded from: classes.dex */
    public static class e extends gk4 {
        public boolean l = false;

        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            if (view instanceof MotionLayout) {
                ((MotionLayout) view).setProgress(f(f, j, view, gx1Var));
            } else if (this.l) {
                return false;
            } else {
                Method method = null;
                try {
                    method = view.getClass().getMethod("setProgress", Float.TYPE);
                } catch (NoSuchMethodException unused) {
                    this.l = true;
                }
                if (method != null) {
                    try {
                        method.invoke(view, Float.valueOf(f(f, j, view, gx1Var)));
                    } catch (IllegalAccessException | InvocationTargetException unused2) {
                    }
                }
            }
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$f */
    /* loaded from: classes.dex */
    public static class f extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setRotation(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$g */
    /* loaded from: classes.dex */
    public static class g extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setRotationX(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$h */
    /* loaded from: classes.dex */
    public static class h extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setRotationY(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$i */
    /* loaded from: classes.dex */
    public static class i extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setScaleX(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$j */
    /* loaded from: classes.dex */
    public static class j extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setScaleY(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$k */
    /* loaded from: classes.dex */
    public static class k extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setTranslationX(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$l */
    /* loaded from: classes.dex */
    public static class l extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            view.setTranslationY(f(f, j, view, gx1Var));
            return this.h;
        }
    }

    /* compiled from: ViewTimeCycle.java */
    /* renamed from: gk4$m */
    /* loaded from: classes.dex */
    public static class m extends gk4 {
        @Override // defpackage.gk4
        public boolean i(View view, float f, long j, gx1 gx1Var) {
            if (Build.VERSION.SDK_INT >= 21) {
                view.setTranslationZ(f(f, j, view, gx1Var));
            }
            return this.h;
        }
    }

    public static gk4 g(String str, SparseArray<ConstraintAttribute> sparseArray) {
        return new b(str, sparseArray);
    }

    public static gk4 h(String str, long j2) {
        gk4 gVar;
        str.hashCode();
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1249320806:
                if (str.equals("rotationX")) {
                    c2 = 0;
                    break;
                }
                break;
            case -1249320805:
                if (str.equals("rotationY")) {
                    c2 = 1;
                    break;
                }
                break;
            case -1225497657:
                if (str.equals("translationX")) {
                    c2 = 2;
                    break;
                }
                break;
            case -1225497656:
                if (str.equals("translationY")) {
                    c2 = 3;
                    break;
                }
                break;
            case -1225497655:
                if (str.equals("translationZ")) {
                    c2 = 4;
                    break;
                }
                break;
            case -1001078227:
                if (str.equals("progress")) {
                    c2 = 5;
                    break;
                }
                break;
            case -908189618:
                if (str.equals("scaleX")) {
                    c2 = 6;
                    break;
                }
                break;
            case -908189617:
                if (str.equals("scaleY")) {
                    c2 = 7;
                    break;
                }
                break;
            case -40300674:
                if (str.equals("rotation")) {
                    c2 = '\b';
                    break;
                }
                break;
            case -4379043:
                if (str.equals("elevation")) {
                    c2 = '\t';
                    break;
                }
                break;
            case 37232917:
                if (str.equals("transitionPathRotate")) {
                    c2 = '\n';
                    break;
                }
                break;
            case 92909918:
                if (str.equals("alpha")) {
                    c2 = 11;
                    break;
                }
                break;
        }
        switch (c2) {
            case 0:
                gVar = new g();
                break;
            case 1:
                gVar = new h();
                break;
            case 2:
                gVar = new k();
                break;
            case 3:
                gVar = new l();
                break;
            case 4:
                gVar = new m();
                break;
            case 5:
                gVar = new e();
                break;
            case 6:
                gVar = new i();
                break;
            case 7:
                gVar = new j();
                break;
            case '\b':
                gVar = new f();
                break;
            case '\t':
                gVar = new c();
                break;
            case '\n':
                gVar = new d();
                break;
            case 11:
                gVar = new a();
                break;
            default:
                return null;
        }
        gVar.c(j2);
        return gVar;
    }

    public float f(float f2, long j2, View view, gx1 gx1Var) {
        this.a.e(f2, this.g);
        float[] fArr = this.g;
        float f3 = fArr[1];
        int i2 = (f3 > Utils.FLOAT_EPSILON ? 1 : (f3 == Utils.FLOAT_EPSILON ? 0 : -1));
        if (i2 == 0) {
            this.h = false;
            return fArr[2];
        }
        if (Float.isNaN(this.j)) {
            float a2 = gx1Var.a(view, this.f, 0);
            this.j = a2;
            if (Float.isNaN(a2)) {
                this.j = Utils.FLOAT_EPSILON;
            }
        }
        float f4 = (float) ((this.j + (((j2 - this.i) * 1.0E-9d) * f3)) % 1.0d);
        this.j = f4;
        gx1Var.b(view, this.f, 0, f4);
        this.i = j2;
        float f5 = this.g[0];
        float a3 = (a(this.j) * f5) + this.g[2];
        this.h = (f5 == Utils.FLOAT_EPSILON && i2 == 0) ? false : true;
        return a3;
    }

    public abstract boolean i(View view, float f2, long j2, gx1 gx1Var);
}
