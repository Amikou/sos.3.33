package defpackage;

import java.util.concurrent.TimeUnit;

/* compiled from: Tasks.kt */
/* renamed from: w34  reason: default package */
/* loaded from: classes2.dex */
public final class w34 {
    public static final long a;
    public static final int b;
    public static final int c;
    public static final long d;
    public static ed3 e;

    static {
        long e2;
        int d2;
        int d3;
        long e3;
        e2 = c34.e("kotlinx.coroutines.scheduler.resolution.ns", 100000L, 0L, 0L, 12, null);
        a = e2;
        c34.d("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, null);
        d2 = c34.d("kotlinx.coroutines.scheduler.core.pool.size", u33.b(a34.a(), 2), 1, 0, 8, null);
        b = d2;
        d3 = c34.d("kotlinx.coroutines.scheduler.max.pool.size", u33.f(a34.a() * 128, d2, 2097150), 0, 2097150, 4, null);
        c = d3;
        TimeUnit timeUnit = TimeUnit.SECONDS;
        e3 = c34.e("kotlinx.coroutines.scheduler.keep.alive.sec", 60L, 0L, 0L, 12, null);
        d = timeUnit.toNanos(e3);
        e = zc2.a;
    }
}
