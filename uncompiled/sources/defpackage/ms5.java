package defpackage;

import com.google.android.gms.internal.vision.l0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: ms5  reason: default package */
/* loaded from: classes.dex */
public final class ms5 implements aw5 {
    public static final ms5 a = new ms5();

    public static ms5 c() {
        return a;
    }

    @Override // defpackage.aw5
    public final boolean a(Class<?> cls) {
        return l0.class.isAssignableFrom(cls);
    }

    @Override // defpackage.aw5
    public final tv5 b(Class<?> cls) {
        if (!l0.class.isAssignableFrom(cls)) {
            String name = cls.getName();
            throw new IllegalArgumentException(name.length() != 0 ? "Unsupported message type: ".concat(name) : new String("Unsupported message type: "));
        }
        try {
            return (tv5) l0.n(cls.asSubclass(l0.class)).o(l0.f.c, null, null);
        } catch (Exception e) {
            String name2 = cls.getName();
            throw new RuntimeException(name2.length() != 0 ? "Unable to get message info for ".concat(name2) : new String("Unable to get message info for "), e);
        }
    }
}
