package defpackage;

import android.content.Context;
import com.google.android.datatransport.a;
import com.google.android.gms.tasks.c;
import com.google.firebase.crashlytics.internal.common.e;
import java.nio.charset.Charset;

/* compiled from: DataTransportCrashlyticsReportSender.java */
/* renamed from: ne0  reason: default package */
/* loaded from: classes2.dex */
public class ne0 {
    public static final z90 b = new z90();
    public static final String c = f("hts/cahyiseot-agolai.o/1frlglgc/aclg", "tp:/rsltcrprsp.ogepscmv/ieo/eaybtho");
    public static final String d = f("AzSBpY4F0rHiHFdinTvM", "IayrSTFL9eJ69YeSUO2");
    public static final fb4<r90, byte[]> e = le0.a;
    public final mb4<r90> a;

    public ne0(mb4<r90> mb4Var, fb4<r90, byte[]> fb4Var) {
        this.a = mb4Var;
    }

    public static ne0 c(Context context) {
        vb4.f(context);
        pb4 g = vb4.c().g(new ht(c, d));
        hv0 b2 = hv0.b("json");
        fb4<r90, byte[]> fb4Var = e;
        return new ne0(g.a("FIREBASE_CRASHLYTICS_REPORT", r90.class, b2, fb4Var), fb4Var);
    }

    public static /* synthetic */ void d(n34 n34Var, e eVar, Exception exc) {
        if (exc != null) {
            n34Var.d(exc);
        } else {
            n34Var.e(eVar);
        }
    }

    public static /* synthetic */ byte[] e(r90 r90Var) {
        return b.E(r90Var).getBytes(Charset.forName("UTF-8"));
    }

    public static String f(String str, String str2) {
        int length = str.length() - str2.length();
        if (length >= 0 && length <= 1) {
            StringBuilder sb = new StringBuilder(str.length() + str2.length());
            for (int i = 0; i < str.length(); i++) {
                sb.append(str.charAt(i));
                if (str2.length() > i) {
                    sb.append(str2.charAt(i));
                }
            }
            return sb.toString();
        }
        throw new IllegalArgumentException("Invalid input received");
    }

    public c<e> g(final e eVar) {
        r90 b2 = eVar.b();
        final n34 n34Var = new n34();
        this.a.b(a.e(b2), new yb4() { // from class: me0
            @Override // defpackage.yb4
            public final void a(Exception exc) {
                ne0.d(n34.this, eVar, exc);
            }
        });
        return n34Var.a();
    }
}
