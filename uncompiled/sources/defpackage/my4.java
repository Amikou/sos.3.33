package defpackage;

import android.content.Context;
import com.google.android.play.core.internal.d;

/* renamed from: my4  reason: default package */
/* loaded from: classes2.dex */
public final class my4 implements jw4<Context> {
    public final dy4 a;

    public my4(dy4 dy4Var) {
        this.a = dy4Var;
    }

    public static Context c(dy4 dy4Var) {
        Context a = dy4Var.a();
        d.k(a);
        return a;
    }

    @Override // defpackage.jw4
    /* renamed from: b */
    public final Context a() {
        return c(this.a);
    }
}
