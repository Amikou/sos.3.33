package defpackage;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.tasks.c;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: pf5 */
/* loaded from: classes.dex */
public final class pf5 {
    public static pf5 e;
    public final Context a;
    public final ScheduledExecutorService b;
    public zh5 c = new zh5(this);
    public int d = 1;

    public pf5(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    public static /* synthetic */ Context b(pf5 pf5Var) {
        return pf5Var.a;
    }

    public static synchronized pf5 e(Context context) {
        pf5 pf5Var;
        synchronized (pf5.class) {
            if (e == null) {
                e = new pf5(context, i35.a().a(1, new yc2("MessengerIpcClient"), fi5.b));
            }
            pf5Var = e;
        }
        return pf5Var;
    }

    public static /* synthetic */ ScheduledExecutorService g(pf5 pf5Var) {
        return pf5Var.b;
    }

    public final synchronized int a() {
        int i;
        i = this.d;
        this.d = i + 1;
        return i;
    }

    public final c<Void> c(int i, Bundle bundle) {
        return d(new yz5(a(), 2, bundle));
    }

    public final synchronized <T> c<T> d(n36<T> n36Var) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(n36Var);
            StringBuilder sb = new StringBuilder(valueOf.length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
        }
        if (!this.c.e(n36Var)) {
            zh5 zh5Var = new zh5(this);
            this.c = zh5Var;
            zh5Var.e(n36Var);
        }
        return n36Var.b.a();
    }

    public final c<Bundle> f(int i, Bundle bundle) {
        return d(new r46(a(), 1, bundle));
    }
}
