package defpackage;

/* compiled from: Booleans.java */
/* renamed from: cr  reason: default package */
/* loaded from: classes2.dex */
public final class cr {
    public static int a(boolean z, boolean z2) {
        if (z == z2) {
            return 0;
        }
        return z ? 1 : -1;
    }

    public static boolean b(boolean[] zArr, boolean z) {
        for (boolean z2 : zArr) {
            if (z2 == z) {
                return true;
            }
        }
        return false;
    }
}
