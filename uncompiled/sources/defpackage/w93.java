package defpackage;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.utils.Utils;
import java.util.Arrays;

/* compiled from: RoundedDrawable.java */
/* renamed from: w93  reason: default package */
/* loaded from: classes.dex */
public abstract class w93 extends Drawable implements s93, va4 {
    public Matrix A0;
    public wa4 G0;
    public final Drawable a;
    public float[] o0;
    public RectF t0;
    public Matrix z0;
    public boolean f0 = false;
    public boolean g0 = false;
    public float h0 = Utils.FLOAT_EPSILON;
    public final Path i0 = new Path();
    public boolean j0 = true;
    public int k0 = 0;
    public final Path l0 = new Path();
    public final float[] m0 = new float[8];
    public final float[] n0 = new float[8];
    public final RectF p0 = new RectF();
    public final RectF q0 = new RectF();
    public final RectF r0 = new RectF();
    public final RectF s0 = new RectF();
    public final Matrix u0 = new Matrix();
    public final Matrix v0 = new Matrix();
    public final Matrix w0 = new Matrix();
    public final Matrix x0 = new Matrix();
    public final Matrix y0 = new Matrix();
    public final Matrix B0 = new Matrix();
    public float C0 = Utils.FLOAT_EPSILON;
    public boolean D0 = false;
    public boolean E0 = false;
    public boolean F0 = true;

    public w93(Drawable drawable) {
        this.a = drawable;
    }

    @Override // defpackage.s93
    public void a(int i, float f) {
        if (this.k0 == i && this.h0 == f) {
            return;
        }
        this.k0 = i;
        this.h0 = f;
        this.F0 = true;
        invalidateSelf();
    }

    @Override // defpackage.s93
    public void b(boolean z) {
        this.f0 = z;
        this.F0 = true;
        invalidateSelf();
    }

    public boolean c() {
        return this.E0;
    }

    @Override // android.graphics.drawable.Drawable
    public void clearColorFilter() {
        this.a.clearColorFilter();
    }

    @Override // defpackage.s93
    public void d(boolean z) {
        if (this.E0 != z) {
            this.E0 = z;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (nc1.d()) {
            nc1.a("RoundedDrawable#draw");
        }
        this.a.draw(canvas);
        if (nc1.d()) {
            nc1.b();
        }
    }

    @Override // defpackage.s93
    public void e(boolean z) {
        if (this.D0 != z) {
            this.D0 = z;
            this.F0 = true;
            invalidateSelf();
        }
    }

    public boolean f() {
        return this.f0 || this.g0 || this.h0 > Utils.FLOAT_EPSILON;
    }

    public void g() {
        float[] fArr;
        if (this.F0) {
            this.l0.reset();
            RectF rectF = this.p0;
            float f = this.h0;
            rectF.inset(f / 2.0f, f / 2.0f);
            if (this.f0) {
                this.l0.addCircle(this.p0.centerX(), this.p0.centerY(), Math.min(this.p0.width(), this.p0.height()) / 2.0f, Path.Direction.CW);
            } else {
                int i = 0;
                while (true) {
                    fArr = this.n0;
                    if (i >= fArr.length) {
                        break;
                    }
                    fArr[i] = (this.m0[i] + this.C0) - (this.h0 / 2.0f);
                    i++;
                }
                this.l0.addRoundRect(this.p0, fArr, Path.Direction.CW);
            }
            RectF rectF2 = this.p0;
            float f2 = this.h0;
            rectF2.inset((-f2) / 2.0f, (-f2) / 2.0f);
            this.i0.reset();
            float f3 = this.C0 + (this.D0 ? this.h0 : Utils.FLOAT_EPSILON);
            this.p0.inset(f3, f3);
            if (this.f0) {
                this.i0.addCircle(this.p0.centerX(), this.p0.centerY(), Math.min(this.p0.width(), this.p0.height()) / 2.0f, Path.Direction.CW);
            } else if (this.D0) {
                if (this.o0 == null) {
                    this.o0 = new float[8];
                }
                for (int i2 = 0; i2 < this.n0.length; i2++) {
                    this.o0[i2] = this.m0[i2] - this.h0;
                }
                this.i0.addRoundRect(this.p0, this.o0, Path.Direction.CW);
            } else {
                this.i0.addRoundRect(this.p0, this.m0, Path.Direction.CW);
            }
            float f4 = -f3;
            this.p0.inset(f4, f4);
            this.i0.setFillType(Path.FillType.WINDING);
            this.F0 = false;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.a.getAlpha();
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        return this.a.getColorFilter();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.a.getIntrinsicHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.a.getIntrinsicWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.a.getOpacity();
    }

    @Override // defpackage.s93
    public void h(float f) {
        if (this.C0 != f) {
            this.C0 = f;
            this.F0 = true;
            invalidateSelf();
        }
    }

    public void i() {
        Matrix matrix;
        wa4 wa4Var = this.G0;
        if (wa4Var != null) {
            wa4Var.c(this.w0);
            this.G0.g(this.p0);
        } else {
            this.w0.reset();
            this.p0.set(getBounds());
        }
        this.r0.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, getIntrinsicWidth(), getIntrinsicHeight());
        this.s0.set(this.a.getBounds());
        this.u0.setRectToRect(this.r0, this.s0, Matrix.ScaleToFit.FILL);
        if (this.D0) {
            RectF rectF = this.t0;
            if (rectF == null) {
                this.t0 = new RectF(this.p0);
            } else {
                rectF.set(this.p0);
            }
            RectF rectF2 = this.t0;
            float f = this.h0;
            rectF2.inset(f, f);
            if (this.z0 == null) {
                this.z0 = new Matrix();
            }
            this.z0.setRectToRect(this.p0, this.t0, Matrix.ScaleToFit.FILL);
        } else {
            Matrix matrix2 = this.z0;
            if (matrix2 != null) {
                matrix2.reset();
            }
        }
        if (!this.w0.equals(this.x0) || !this.u0.equals(this.v0) || ((matrix = this.z0) != null && !matrix.equals(this.A0))) {
            this.j0 = true;
            this.w0.invert(this.y0);
            this.B0.set(this.w0);
            if (this.D0) {
                this.B0.postConcat(this.z0);
            }
            this.B0.preConcat(this.u0);
            this.x0.set(this.w0);
            this.v0.set(this.u0);
            if (this.D0) {
                Matrix matrix3 = this.A0;
                if (matrix3 == null) {
                    this.A0 = new Matrix(this.z0);
                } else {
                    matrix3.set(this.z0);
                }
            } else {
                Matrix matrix4 = this.A0;
                if (matrix4 != null) {
                    matrix4.reset();
                }
            }
        }
        if (this.p0.equals(this.q0)) {
            return;
        }
        this.F0 = true;
        this.q0.set(this.p0);
    }

    @Override // defpackage.va4
    public void j(wa4 wa4Var) {
        this.G0 = wa4Var;
    }

    @Override // defpackage.s93
    public void l(float[] fArr) {
        if (fArr == null) {
            Arrays.fill(this.m0, (float) Utils.FLOAT_EPSILON);
            this.g0 = false;
        } else {
            xt2.c(fArr.length == 8, "radii should have exactly 8 values");
            System.arraycopy(fArr, 0, this.m0, 0, 8);
            this.g0 = false;
            for (int i = 0; i < 8; i++) {
                this.g0 |= fArr[i] > Utils.FLOAT_EPSILON;
            }
        }
        this.F0 = true;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        this.a.setBounds(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.a.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(int i, PorterDuff.Mode mode) {
        this.a.setColorFilter(i, mode);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
    }
}
