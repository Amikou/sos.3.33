package defpackage;

import android.os.ParcelFileDescriptor;
import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.tasks.a;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

/* renamed from: su4  reason: default package */
/* loaded from: classes2.dex */
public final class su4 {
    public final cw4<zy4> a;

    public su4(cw4<zy4> cw4Var) {
        this.a = cw4Var;
    }

    public final InputStream a(int i, String str, String str2, int i2) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) a.b(this.a.a().e(i, str, str2, i2));
            if (parcelFileDescriptor == null || parcelFileDescriptor.getFileDescriptor() == null) {
                throw new bj(String.format("Corrupted ParcelFileDescriptor, session %s packName %s sliceId %s, chunkNumber %s", Integer.valueOf(i), str, str2, Integer.valueOf(i2)), i);
            }
            return new ParcelFileDescriptor.AutoCloseInputStream(parcelFileDescriptor);
        } catch (InterruptedException e) {
            throw new bj("Extractor was interrupted while waiting for chunk file.", e, i);
        } catch (ExecutionException e2) {
            throw new bj(String.format("Error opening chunk file, session %s packName %s sliceId %s, chunkNumber %s", Integer.valueOf(i), str, str2, Integer.valueOf(i2)), e2, i);
        }
    }
}
