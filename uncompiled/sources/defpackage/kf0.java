package defpackage;

import android.view.Menu;
import android.view.Window;
import androidx.appcompat.view.menu.i;

/* compiled from: DecorContentParent.java */
/* renamed from: kf0  reason: default package */
/* loaded from: classes.dex */
public interface kf0 {
    boolean a();

    boolean b();

    boolean c();

    boolean d();

    boolean e();

    void f(int i);

    void g();

    void setMenu(Menu menu, i.a aVar);

    void setMenuPrepared();

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
