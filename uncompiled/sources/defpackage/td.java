package defpackage;

import android.graphics.Bitmap;
import com.facebook.imagepipeline.animated.base.AnimatedDrawableFrameInfo;

/* compiled from: AnimatedImage.java */
/* renamed from: td  reason: default package */
/* loaded from: classes.dex */
public interface td {
    int a();

    int b();

    AnimatedDrawableFrameInfo c(int i);

    Bitmap.Config f();

    xd g(int i);

    int getHeight();

    int getWidth();

    boolean h();

    int[] i();

    int j();
}
