package defpackage;

import com.google.firebase.crashlytics.internal.common.i;
import defpackage.r90;
import java.util.Comparator;

/* renamed from: km3  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class km3 implements Comparator {
    public static final /* synthetic */ km3 a = new km3();

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        int l;
        l = i.l((r90.c) obj, (r90.c) obj2);
        return l;
    }
}
