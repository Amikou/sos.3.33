package defpackage;

import java.math.BigInteger;

/* renamed from: st0  reason: default package */
/* loaded from: classes2.dex */
public class st0 extends ft0 {
    public BigInteger f0;

    public st0(BigInteger bigInteger, at0 at0Var) {
        super(true, at0Var);
        this.f0 = bigInteger;
    }

    public BigInteger b() {
        return this.f0;
    }
}
