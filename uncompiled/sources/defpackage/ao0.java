package defpackage;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: DialogSwapSlipHelpPromptBinding.java */
/* renamed from: ao0  reason: default package */
/* loaded from: classes2.dex */
public final class ao0 {
    public final MaterialButton a;

    public ao0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialTextView materialTextView, MaterialTextView materialTextView2, MaterialTextView materialTextView3) {
        this.a = materialButton;
    }

    public static ao0 a(View view) {
        int i = R.id.dialog_cross;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.dialog_cross);
        if (materialButton != null) {
            i = R.id.dialog_title;
            MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.dialog_title);
            if (materialTextView != null) {
                i = R.id.txtDescribeSlippage;
                MaterialTextView materialTextView2 = (MaterialTextView) ai4.a(view, R.id.txtDescribeSlippage);
                if (materialTextView2 != null) {
                    i = R.id.txtDescribeSlippageUse;
                    MaterialTextView materialTextView3 = (MaterialTextView) ai4.a(view, R.id.txtDescribeSlippageUse);
                    if (materialTextView3 != null) {
                        return new ao0((ConstraintLayout) view, materialButton, materialTextView, materialTextView2, materialTextView3);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
