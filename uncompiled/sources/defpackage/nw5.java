package defpackage;

import com.google.android.gms.internal.measurement.zzjd;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: nw5  reason: default package */
/* loaded from: classes.dex */
public interface nw5 extends List {
    void M(zzjd zzjdVar);

    List<?> f();

    nw5 g();

    Object v1(int i);
}
