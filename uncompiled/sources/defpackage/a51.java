package defpackage;

import java.math.BigInteger;

/* renamed from: a51  reason: default package */
/* loaded from: classes2.dex */
public abstract class a51 {
    public static final z41 a = new su2(BigInteger.valueOf(2));
    public static final z41 b = new su2(BigInteger.valueOf(3));

    public static qs2 a(int[] iArr) {
        if (iArr[0] == 0) {
            for (int i = 1; i < iArr.length; i++) {
                if (iArr[i] <= iArr[i - 1]) {
                    throw new IllegalArgumentException("Polynomial exponents must be montonically increasing");
                }
            }
            return new bf1(a, new ie1(iArr));
        }
        throw new IllegalArgumentException("Irreducible polynomials in GF(2) must have constant term");
    }

    public static z41 b(BigInteger bigInteger) {
        int bitLength = bigInteger.bitLength();
        if (bigInteger.signum() <= 0 || bitLength < 2) {
            throw new IllegalArgumentException("'characteristic' must be >= 2");
        }
        if (bitLength < 3) {
            int intValue = bigInteger.intValue();
            if (intValue == 2) {
                return a;
            }
            if (intValue == 3) {
                return b;
            }
        }
        return new su2(bigInteger);
    }
}
