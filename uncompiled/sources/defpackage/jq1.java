package defpackage;

import androidx.media3.common.util.b;
import defpackage.wi3;

/* compiled from: IndexSeekMap.java */
/* renamed from: jq1  reason: default package */
/* loaded from: classes.dex */
public final class jq1 implements wi3 {
    public final long[] a;
    public final long[] b;
    public final long c;
    public final boolean d;

    public jq1(long[] jArr, long[] jArr2, long j) {
        ii.a(jArr.length == jArr2.length);
        int length = jArr2.length;
        boolean z = length > 0;
        this.d = z;
        if (z && jArr2[0] > 0) {
            int i = length + 1;
            long[] jArr3 = new long[i];
            this.a = jArr3;
            long[] jArr4 = new long[i];
            this.b = jArr4;
            System.arraycopy(jArr, 0, jArr3, 1, length);
            System.arraycopy(jArr2, 0, jArr4, 1, length);
        } else {
            this.a = jArr;
            this.b = jArr2;
        }
        this.c = j;
    }

    @Override // defpackage.wi3
    public boolean e() {
        return this.d;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        if (!this.d) {
            return new wi3.a(yi3.c);
        }
        int i = b.i(this.b, j, true, true);
        yi3 yi3Var = new yi3(this.b[i], this.a[i]);
        if (yi3Var.a != j && i != this.b.length - 1) {
            int i2 = i + 1;
            return new wi3.a(yi3Var, new yi3(this.b[i2], this.a[i2]));
        }
        return new wi3.a(yi3Var);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.c;
    }
}
