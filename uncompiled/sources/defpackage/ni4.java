package defpackage;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: ViewGroupOverlayApi14.java */
/* renamed from: ni4  reason: default package */
/* loaded from: classes.dex */
public class ni4 extends lj4 implements pi4 {
    public ni4(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    public static ni4 g(ViewGroup viewGroup) {
        return (ni4) lj4.e(viewGroup);
    }

    @Override // defpackage.pi4
    public void c(View view) {
        this.a.b(view);
    }

    @Override // defpackage.pi4
    public void d(View view) {
        this.a.g(view);
    }
}
