package defpackage;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: to2  reason: default package */
/* loaded from: classes.dex */
public class to2 {
    public final Context a;

    public to2(@RecentlyNonNull Context context) {
        this.a = context;
    }

    @RecentlyNonNull
    public int a(@RecentlyNonNull String str) {
        return this.a.checkCallingOrSelfPermission(str);
    }

    @RecentlyNonNull
    public int b(@RecentlyNonNull String str, @RecentlyNonNull String str2) {
        return this.a.getPackageManager().checkPermission(str, str2);
    }

    @RecentlyNonNull
    public ApplicationInfo c(@RecentlyNonNull String str, @RecentlyNonNull int i) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getApplicationInfo(str, i);
    }

    @RecentlyNonNull
    public CharSequence d(@RecentlyNonNull String str) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getApplicationLabel(this.a.getPackageManager().getApplicationInfo(str, 0));
    }

    @RecentlyNonNull
    public PackageInfo e(@RecentlyNonNull String str, @RecentlyNonNull int i) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getPackageInfo(str, i);
    }

    @RecentlyNonNull
    public boolean f() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return hr1.a(this.a);
        }
        if (!jr2.h() || (nameForUid = this.a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.a.getPackageManager().isInstantApp(nameForUid);
    }

    @RecentlyNonNull
    @TargetApi(19)
    public final boolean g(@RecentlyNonNull int i, @RecentlyNonNull String str) {
        if (jr2.d()) {
            try {
                AppOpsManager appOpsManager = (AppOpsManager) this.a.getSystemService("appops");
                if (appOpsManager != null) {
                    appOpsManager.checkPackage(i, str);
                    return true;
                }
                throw new NullPointerException("context.getSystemService(Context.APP_OPS_SERVICE) is null");
            } catch (SecurityException unused) {
                return false;
            }
        }
        String[] packagesForUid = this.a.getPackageManager().getPackagesForUid(i);
        if (str != null && packagesForUid != null) {
            for (String str2 : packagesForUid) {
                if (str.equals(str2)) {
                    return true;
                }
            }
        }
        return false;
    }
}
