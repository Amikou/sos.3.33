package defpackage;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SystemIdInfoDao_Impl.java */
/* renamed from: w24  reason: default package */
/* loaded from: classes.dex */
public final class w24 implements v24 {
    public final RoomDatabase a;
    public final zv0<u24> b;
    public final co3 c;

    /* compiled from: SystemIdInfoDao_Impl.java */
    /* renamed from: w24$a */
    /* loaded from: classes.dex */
    public class a extends zv0<u24> {
        public a(w24 w24Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`system_id`) VALUES (?,?)";
        }

        @Override // defpackage.zv0
        /* renamed from: k */
        public void g(ww3 ww3Var, u24 u24Var) {
            String str = u24Var.a;
            if (str == null) {
                ww3Var.Y0(1);
            } else {
                ww3Var.L(1, str);
            }
            ww3Var.q0(2, u24Var.b);
        }
    }

    /* compiled from: SystemIdInfoDao_Impl.java */
    /* renamed from: w24$b */
    /* loaded from: classes.dex */
    public class b extends co3 {
        public b(w24 w24Var, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @Override // defpackage.co3
        public String d() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    public w24(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
    }

    @Override // defpackage.v24
    public List<String> a() {
        k93 c = k93.c("SELECT DISTINCT work_spec_id FROM SystemIdInfo", 0);
        this.a.d();
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            ArrayList arrayList = new ArrayList(c2.getCount());
            while (c2.moveToNext()) {
                arrayList.add(c2.getString(0));
            }
            return arrayList;
        } finally {
            c2.close();
            c.f();
        }
    }

    @Override // defpackage.v24
    public void b(u24 u24Var) {
        this.a.d();
        this.a.e();
        try {
            this.b.h(u24Var);
            this.a.E();
        } finally {
            this.a.j();
        }
    }

    @Override // defpackage.v24
    public u24 c(String str) {
        k93 c = k93.c("SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            c.Y0(1);
        } else {
            c.L(1, str);
        }
        this.a.d();
        Cursor c2 = id0.c(this.a, c, false, null);
        try {
            return c2.moveToFirst() ? new u24(c2.getString(wb0.e(c2, "work_spec_id")), c2.getInt(wb0.e(c2, "system_id"))) : null;
        } finally {
            c2.close();
            c.f();
        }
    }

    @Override // defpackage.v24
    public void d(String str) {
        this.a.d();
        ww3 a2 = this.c.a();
        if (str == null) {
            a2.Y0(1);
        } else {
            a2.L(1, str);
        }
        this.a.e();
        try {
            a2.T();
            this.a.E();
        } finally {
            this.a.j();
            this.c.f(a2);
        }
    }
}
