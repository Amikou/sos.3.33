package defpackage;

import java.security.SecureRandom;

/* compiled from: Random.java */
/* renamed from: p33  reason: default package */
/* loaded from: classes2.dex */
public final class p33 {
    public static final ThreadLocal<SecureRandom> a = new a();

    /* compiled from: Random.java */
    /* renamed from: p33$a */
    /* loaded from: classes2.dex */
    public class a extends ThreadLocal<SecureRandom> {
        @Override // java.lang.ThreadLocal
        /* renamed from: a */
        public SecureRandom initialValue() {
            return p33.b();
        }
    }

    public static SecureRandom b() {
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextLong();
        return secureRandom;
    }

    public static byte[] c(int i) {
        byte[] bArr = new byte[i];
        a.get().nextBytes(bArr);
        return bArr;
    }
}
