package defpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ForwardingRequestListener2.java */
/* renamed from: j91  reason: default package */
/* loaded from: classes.dex */
public class j91 implements i73 {
    public final List<i73> a;

    public j91(Set<i73> set) {
        this.a = new ArrayList(set.size());
        for (i73 i73Var : set) {
            if (i73Var != null) {
                this.a.add(i73Var);
            }
        }
    }

    @Override // defpackage.iv2
    public void a(ev2 ev2Var, String str, Map<String, String> map) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).a(ev2Var, str, map);
            } catch (Exception e) {
                l("InternalListener exception in onProducerFinishWithSuccess", e);
            }
        }
    }

    @Override // defpackage.i73
    public void b(ev2 ev2Var) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).b(ev2Var);
            } catch (Exception e) {
                l("InternalListener exception in onRequestStart", e);
            }
        }
    }

    @Override // defpackage.iv2
    public void c(ev2 ev2Var, String str, Throwable th, Map<String, String> map) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).c(ev2Var, str, th, map);
            } catch (Exception e) {
                l("InternalListener exception in onProducerFinishWithFailure", e);
            }
        }
    }

    @Override // defpackage.iv2
    public void d(ev2 ev2Var, String str, Map<String, String> map) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).d(ev2Var, str, map);
            } catch (Exception e) {
                l("InternalListener exception in onProducerFinishWithCancellation", e);
            }
        }
    }

    @Override // defpackage.iv2
    public void e(ev2 ev2Var, String str, boolean z) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).e(ev2Var, str, z);
            } catch (Exception e) {
                l("InternalListener exception in onProducerFinishWithSuccess", e);
            }
        }
    }

    @Override // defpackage.i73
    public void f(ev2 ev2Var, Throwable th) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).f(ev2Var, th);
            } catch (Exception e) {
                l("InternalListener exception in onRequestFailure", e);
            }
        }
    }

    @Override // defpackage.i73
    public void g(ev2 ev2Var) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).g(ev2Var);
            } catch (Exception e) {
                l("InternalListener exception in onRequestSuccess", e);
            }
        }
    }

    @Override // defpackage.i73
    public void h(ev2 ev2Var) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).h(ev2Var);
            } catch (Exception e) {
                l("InternalListener exception in onRequestCancellation", e);
            }
        }
    }

    @Override // defpackage.iv2
    public void i(ev2 ev2Var, String str, String str2) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).i(ev2Var, str, str2);
            } catch (Exception e) {
                l("InternalListener exception in onIntermediateChunkStart", e);
            }
        }
    }

    @Override // defpackage.iv2
    public boolean j(ev2 ev2Var, String str) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            if (this.a.get(i).j(ev2Var, str)) {
                return true;
            }
        }
        return false;
    }

    @Override // defpackage.iv2
    public void k(ev2 ev2Var, String str) {
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            try {
                this.a.get(i).k(ev2Var, str);
            } catch (Exception e) {
                l("InternalListener exception in onProducerStart", e);
            }
        }
    }

    public final void l(String str, Throwable th) {
        v11.i("ForwardingRequestListener2", str, th);
    }
}
