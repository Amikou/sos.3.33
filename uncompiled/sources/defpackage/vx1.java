package defpackage;

import com.google.crypto.tink.g;
import com.google.crypto.tink.m;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.c0;
import com.google.crypto.tink.proto.d0;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: KmsEnvelopeAeadKeyManager.java */
/* renamed from: vx1  reason: default package */
/* loaded from: classes2.dex */
public class vx1 extends g<c0> {

    /* compiled from: KmsEnvelopeAeadKeyManager.java */
    /* renamed from: vx1$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, c0> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(c0 c0Var) throws GeneralSecurityException {
            String G = c0Var.G().G();
            return new ux1(c0Var.G().E(), m.a(G).b(G));
        }
    }

    /* compiled from: KmsEnvelopeAeadKeyManager.java */
    /* renamed from: vx1$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<d0, c0> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public c0 a(d0 d0Var) throws GeneralSecurityException {
            return c0.I().s(d0Var).t(vx1.this.j()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public d0 c(ByteString byteString) throws InvalidProtocolBufferException {
            return d0.H(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(d0 d0Var) throws GeneralSecurityException {
        }
    }

    public vx1() {
        super(c0.class, new a(com.google.crypto.tink.a.class));
    }

    public static void l(boolean z) throws GeneralSecurityException {
        q.q(new vx1(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, c0> e() {
        return new b(d0.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.REMOTE;
    }

    public int j() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: k */
    public c0 g(ByteString byteString) throws InvalidProtocolBufferException {
        return c0.J(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public void i(c0 c0Var) throws GeneralSecurityException {
        ug4.c(c0Var.H(), j());
    }
}
