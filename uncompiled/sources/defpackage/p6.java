package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktBinding.java */
/* renamed from: p6  reason: default package */
/* loaded from: classes2.dex */
public final class p6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final ImageView d;

    public p6(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, ImageView imageView, bq1 bq1Var, AppCompatTextView appCompatTextView) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = imageView;
    }

    public static p6 a(View view) {
        int i = R.id.btnLogin;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnLogin);
        if (materialButton != null) {
            i = R.id.btnRegister;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnRegister);
            if (materialButton2 != null) {
                i = R.id.imgBack;
                ImageView imageView = (ImageView) ai4.a(view, R.id.imgBack);
                if (imageView != null) {
                    i = R.id.layoutSafeMoonBrand;
                    View a = ai4.a(view, R.id.layoutSafeMoonBrand);
                    if (a != null) {
                        bq1 a2 = bq1.a(a);
                        i = R.id.orTxt;
                        AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.orTxt);
                        if (appCompatTextView != null) {
                            return new p6((ConstraintLayout) view, materialButton, materialButton2, imageView, a2, appCompatTextView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static p6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static p6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
