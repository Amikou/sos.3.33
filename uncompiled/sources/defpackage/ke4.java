package defpackage;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.c;
import com.fasterxml.jackson.core.io.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: UTF8StreamJsonParser.java */
/* renamed from: ke4  reason: default package */
/* loaded from: classes.dex */
public class ke4 extends qp2 {
    public static final int[] Z0 = a.i();
    public static final int[] a1 = a.g();
    public c O0;
    public final vs P0;
    public int[] Q0;
    public boolean R0;
    public int S0;
    public int T0;
    public int U0;
    public int V0;
    public InputStream W0;
    public byte[] X0;
    public boolean Y0;

    public ke4(jm1 jm1Var, int i, InputStream inputStream, c cVar, vs vsVar, byte[] bArr, int i2, int i3, boolean z) {
        super(jm1Var, i);
        this.Q0 = new int[16];
        this.W0 = inputStream;
        this.O0 = cVar;
        this.P0 = vsVar;
        this.X0 = bArr;
        this.j0 = i2;
        this.k0 = i3;
        this.n0 = i2;
        this.l0 = -i2;
        this.Y0 = z;
    }

    public static int[] w3(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        return Arrays.copyOf(iArr, iArr.length + i);
    }

    public static final int y3(int i, int i2) {
        return i2 == 4 ? i : i | ((-1) << (i2 << 3));
    }

    public final String A2(JsonToken jsonToken) {
        if (jsonToken == null) {
            return null;
        }
        int id = jsonToken.id();
        if (id != 5) {
            if (id != 6 && id != 7 && id != 8) {
                return jsonToken.asString();
            }
            return this.t0.j();
        }
        return this.r0.b();
    }

    public final String A3(int i, int i2, int i3) throws IOException {
        int[] iArr = this.Q0;
        iArr[0] = this.S0;
        iArr[1] = i2;
        iArr[2] = i3;
        byte[] bArr = this.X0;
        int[] iArr2 = a1;
        int i4 = i;
        int i5 = 3;
        while (true) {
            int i6 = this.j0;
            if (i6 + 4 <= this.k0) {
                int i7 = i6 + 1;
                this.j0 = i7;
                int i8 = bArr[i6] & 255;
                if (iArr2[i8] != 0) {
                    if (i8 == 34) {
                        return v3(this.Q0, i5, i4, 1);
                    }
                    return z3(this.Q0, i5, i4, i8, 1);
                }
                int i9 = (i4 << 8) | i8;
                int i10 = i7 + 1;
                this.j0 = i10;
                int i11 = bArr[i7] & 255;
                if (iArr2[i11] != 0) {
                    if (i11 == 34) {
                        return v3(this.Q0, i5, i9, 2);
                    }
                    return z3(this.Q0, i5, i9, i11, 2);
                }
                int i12 = (i9 << 8) | i11;
                int i13 = i10 + 1;
                this.j0 = i13;
                int i14 = bArr[i10] & 255;
                if (iArr2[i14] != 0) {
                    if (i14 == 34) {
                        return v3(this.Q0, i5, i12, 3);
                    }
                    return z3(this.Q0, i5, i12, i14, 3);
                }
                int i15 = (i12 << 8) | i14;
                this.j0 = i13 + 1;
                int i16 = bArr[i13] & 255;
                if (iArr2[i16] != 0) {
                    if (i16 == 34) {
                        return v3(this.Q0, i5, i15, 4);
                    }
                    return z3(this.Q0, i5, i15, i16, 4);
                }
                int[] iArr3 = this.Q0;
                if (i5 >= iArr3.length) {
                    this.Q0 = w3(iArr3, i5);
                }
                this.Q0[i5] = i15;
                i4 = i16;
                i5++;
            } else {
                return z3(this.Q0, i5, 0, i4, 0);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0045, code lost:
        if (r6 != 39) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0047, code lost:
        r10.t0.z(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x004e, code lost:
        return com.fasterxml.jackson.core.JsonToken.VALUE_STRING;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x004f, code lost:
        r5 = r1[r6];
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x0052, code lost:
        if (r5 == 1) goto L49;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x0055, code lost:
        if (r5 == 2) goto L48;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0058, code lost:
        if (r5 == 3) goto L44;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x005b, code lost:
        if (r5 == 4) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x005f, code lost:
        if (r6 >= 32) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x0061, code lost:
        L1(r6, "string value");
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x0066, code lost:
        S2(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x006a, code lost:
        r5 = w2(r6);
        r6 = r4 + 1;
        r0[r4] = (char) (55296 | (r5 >> 10));
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x007a, code lost:
        if (r6 < r0.length) goto L43;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x007c, code lost:
        r0 = r10.t0.n();
        r4 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x0084, code lost:
        r4 = r6;
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x0085, code lost:
        r6 = 56320 | (r5 & 1023);
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x008f, code lost:
        if ((r10.k0 - r7) < 2) goto L47;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x0091, code lost:
        r6 = v2(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x0096, code lost:
        r6 = u2(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x009b, code lost:
        r6 = t2(r6);
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00a0, code lost:
        r6 = R1();
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x00a5, code lost:
        if (r4 < r0.length) goto L36;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x00a7, code lost:
        r0 = r10.t0.n();
        r4 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x00ae, code lost:
        r0[r4] = (char) r6;
        r4 = r4 + 1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.core.JsonToken B2() throws java.io.IOException {
        /*
            r10 = this;
            com.fasterxml.jackson.core.util.c r0 = r10.t0
            char[] r0 = r0.k()
            int[] r1 = defpackage.ke4.Z0
            byte[] r2 = r10.X0
            r3 = 0
            r4 = r3
        Lc:
            int r5 = r10.j0
            int r6 = r10.k0
            if (r5 < r6) goto L15
            r10.G2()
        L15:
            int r5 = r0.length
            if (r4 < r5) goto L1f
            com.fasterxml.jackson.core.util.c r0 = r10.t0
            char[] r0 = r0.n()
            r4 = r3
        L1f:
            int r5 = r10.k0
            int r6 = r10.j0
            int r7 = r0.length
            int r7 = r7 - r4
            int r6 = r6 + r7
            if (r6 >= r5) goto L29
            r5 = r6
        L29:
            int r6 = r10.j0
            if (r6 >= r5) goto Lc
            int r7 = r6 + 1
            r10.j0 = r7
            r6 = r2[r6]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r8 = 39
            if (r6 == r8) goto L45
            r9 = r1[r6]
            if (r9 == 0) goto L3e
            goto L45
        L3e:
            int r7 = r4 + 1
            char r6 = (char) r6
            r0[r4] = r6
            r4 = r7
            goto L29
        L45:
            if (r6 != r8) goto L4f
            com.fasterxml.jackson.core.util.c r0 = r10.t0
            r0.z(r4)
            com.fasterxml.jackson.core.JsonToken r0 = com.fasterxml.jackson.core.JsonToken.VALUE_STRING
            return r0
        L4f:
            r5 = r1[r6]
            r8 = 1
            if (r5 == r8) goto La0
            r8 = 2
            if (r5 == r8) goto L9b
            r9 = 3
            if (r5 == r9) goto L8c
            r7 = 4
            if (r5 == r7) goto L6a
            r5 = 32
            if (r6 >= r5) goto L66
            java.lang.String r5 = "string value"
            r10.L1(r6, r5)
        L66:
            r10.S2(r6)
            goto La4
        L6a:
            int r5 = r10.w2(r6)
            int r6 = r4 + 1
            r7 = 55296(0xd800, float:7.7486E-41)
            int r8 = r5 >> 10
            r7 = r7 | r8
            char r7 = (char) r7
            r0[r4] = r7
            int r4 = r0.length
            if (r6 < r4) goto L84
            com.fasterxml.jackson.core.util.c r0 = r10.t0
            char[] r0 = r0.n()
            r4 = r3
            goto L85
        L84:
            r4 = r6
        L85:
            r6 = 56320(0xdc00, float:7.8921E-41)
            r5 = r5 & 1023(0x3ff, float:1.434E-42)
            r6 = r6 | r5
            goto La4
        L8c:
            int r5 = r10.k0
            int r5 = r5 - r7
            if (r5 < r8) goto L96
            int r6 = r10.v2(r6)
            goto La4
        L96:
            int r6 = r10.u2(r6)
            goto La4
        L9b:
            int r6 = r10.t2(r6)
            goto La4
        La0:
            char r6 = r10.R1()
        La4:
            int r5 = r0.length
            if (r4 < r5) goto Lae
            com.fasterxml.jackson.core.util.c r0 = r10.t0
            char[] r0 = r0.n()
            r4 = r3
        Lae:
            int r5 = r4 + 1
            char r6 = (char) r6
            r0[r4] = r6
            r4 = r5
            goto Lc
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.B2():com.fasterxml.jackson.core.JsonToken");
    }

    public final String B3(int i) throws IOException {
        byte[] bArr = this.X0;
        int[] iArr = a1;
        int i2 = this.j0;
        int i3 = i2 + 1;
        this.j0 = i3;
        int i4 = bArr[i2] & 255;
        if (iArr[i4] != 0) {
            if (i4 == 34) {
                return t3(this.S0, i, 1);
            }
            return E3(this.S0, i, i4, 1);
        }
        int i5 = (i << 8) | i4;
        int i6 = i3 + 1;
        this.j0 = i6;
        int i7 = bArr[i3] & 255;
        if (iArr[i7] != 0) {
            if (i7 == 34) {
                return t3(this.S0, i5, 2);
            }
            return E3(this.S0, i5, i7, 2);
        }
        int i8 = (i5 << 8) | i7;
        int i9 = i6 + 1;
        this.j0 = i9;
        int i10 = bArr[i6] & 255;
        if (iArr[i10] != 0) {
            if (i10 == 34) {
                return t3(this.S0, i8, 3);
            }
            return E3(this.S0, i8, i10, 3);
        }
        int i11 = (i8 << 8) | i10;
        this.j0 = i9 + 1;
        int i12 = bArr[i9] & 255;
        if (iArr[i12] != 0) {
            if (i12 == 34) {
                return t3(this.S0, i11, 4);
            }
            return E3(this.S0, i11, i12, 4);
        }
        return C3(i12, i11);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public Object C() {
        return this.W0;
    }

    public JsonToken C2(int i, boolean z) throws IOException {
        String str;
        while (i == 73) {
            if (this.j0 >= this.k0 && !F2()) {
                F1(JsonToken.VALUE_NUMBER_FLOAT);
            }
            byte[] bArr = this.X0;
            int i2 = this.j0;
            this.j0 = i2 + 1;
            i = bArr[i2];
            if (i != 78) {
                if (i != 110) {
                    break;
                }
                str = z ? "-Infinity" : "+Infinity";
            } else {
                str = z ? "-INF" : "+INF";
            }
            H2(str, 3);
            if (J0(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS)) {
                return n2(str, z ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);
            }
            w1("Non-standard token '" + str + "': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow");
        }
        l2(i, "expected digit (0-9) to follow minus sign, for valid numeric value");
        return null;
    }

    public final String C3(int i, int i2) throws IOException {
        byte[] bArr = this.X0;
        int[] iArr = a1;
        int i3 = this.j0;
        int i4 = i3 + 1;
        this.j0 = i4;
        int i5 = bArr[i3] & 255;
        if (iArr[i5] != 0) {
            if (i5 == 34) {
                return u3(this.S0, i2, i, 1);
            }
            return F3(this.S0, i2, i, i5, 1);
        }
        int i6 = (i << 8) | i5;
        int i7 = i4 + 1;
        this.j0 = i7;
        int i8 = bArr[i4] & 255;
        if (iArr[i8] != 0) {
            if (i8 == 34) {
                return u3(this.S0, i2, i6, 2);
            }
            return F3(this.S0, i2, i6, i8, 2);
        }
        int i9 = (i6 << 8) | i8;
        int i10 = i7 + 1;
        this.j0 = i10;
        int i11 = bArr[i7] & 255;
        if (iArr[i11] != 0) {
            if (i11 == 34) {
                return u3(this.S0, i2, i9, 3);
            }
            return F3(this.S0, i2, i9, i11, 3);
        }
        int i12 = (i9 << 8) | i11;
        this.j0 = i10 + 1;
        int i13 = bArr[i10] & 255;
        if (iArr[i13] != 0) {
            if (i13 == 34) {
                return u3(this.S0, i2, i12, 4);
            }
            return F3(this.S0, i2, i12, i13, 4);
        }
        return A3(i13, i2, i12);
    }

    public String D2(int i) throws IOException {
        if (i == 39 && J0(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return L2();
        }
        if (!J0(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)) {
            I1((char) s2(i), "was expecting double-quote to start field name");
        }
        int[] j = a.j();
        if (j[i] != 0) {
            I1(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int[] iArr = this.Q0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i2 < 4) {
                i2++;
                i4 = i | (i4 << 8);
            } else {
                if (i3 >= iArr.length) {
                    iArr = w3(iArr, iArr.length);
                    this.Q0 = iArr;
                }
                iArr[i3] = i4;
                i4 = i;
                i3++;
                i2 = 1;
            }
            if (this.j0 >= this.k0 && !F2()) {
                z1(" in field name", JsonToken.FIELD_NAME);
            }
            byte[] bArr = this.X0;
            int i5 = this.j0;
            i = bArr[i5] & 255;
            if (j[i] != 0) {
                break;
            }
            this.j0 = i5 + 1;
        }
        if (i2 > 0) {
            if (i3 >= iArr.length) {
                int[] w3 = w3(iArr, iArr.length);
                this.Q0 = w3;
                iArr = w3;
            }
            iArr[i3] = i4;
            i3++;
        }
        String F = this.P0.F(iArr, i3);
        return F == null ? r3(iArr, i3, i2) : F;
    }

    public final String D3(int i, int i2, int i3) throws IOException {
        return z3(this.Q0, 0, i, i2, i3);
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x001b, code lost:
        if (r4 != 44) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0048, code lost:
        if (r3.r0.e() == false) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0051, code lost:
        if (J0(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_MISSING_VALUES) == false) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x0053, code lost:
        r3.j0--;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x005a, code lost:
        return com.fasterxml.jackson.core.JsonToken.VALUE_NULL;
     */
    /* JADX WARN: Removed duplicated region for block: B:48:0x00a8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public com.fasterxml.jackson.core.JsonToken E2(int r4) throws java.io.IOException {
        /*
            r3 = this;
            r0 = 39
            if (r4 == r0) goto L95
            r0 = 73
            r1 = 1
            if (r4 == r0) goto L7b
            r0 = 78
            if (r4 == r0) goto L61
            r0 = 93
            if (r4 == r0) goto L42
            r0 = 125(0x7d, float:1.75E-43)
            if (r4 == r0) goto L5b
            r0 = 43
            if (r4 == r0) goto L1f
            r0 = 44
            if (r4 == r0) goto L4b
            goto La2
        L1f:
            int r4 = r3.j0
            int r0 = r3.k0
            if (r4 < r0) goto L30
            boolean r4 = r3.F2()
            if (r4 != 0) goto L30
            com.fasterxml.jackson.core.JsonToken r4 = com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_INT
            r3.F1(r4)
        L30:
            byte[] r4 = r3.X0
            int r0 = r3.j0
            int r1 = r0 + 1
            r3.j0 = r1
            r4 = r4[r0]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r0 = 0
            com.fasterxml.jackson.core.JsonToken r4 = r3.C2(r4, r0)
            return r4
        L42:
            hv1 r0 = r3.r0
            boolean r0 = r0.e()
            if (r0 != 0) goto L4b
            goto La2
        L4b:
            com.fasterxml.jackson.core.JsonParser$Feature r0 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_MISSING_VALUES
            boolean r0 = r3.J0(r0)
            if (r0 == 0) goto L5b
            int r4 = r3.j0
            int r4 = r4 - r1
            r3.j0 = r4
            com.fasterxml.jackson.core.JsonToken r4 = com.fasterxml.jackson.core.JsonToken.VALUE_NULL
            return r4
        L5b:
            java.lang.String r0 = "expected a value"
            r3.I1(r4, r0)
            goto L95
        L61:
            java.lang.String r0 = "NaN"
            r3.H2(r0, r1)
            com.fasterxml.jackson.core.JsonParser$Feature r1 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS
            boolean r1 = r3.J0(r1)
            if (r1 == 0) goto L75
            r1 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            com.fasterxml.jackson.core.JsonToken r4 = r3.n2(r0, r1)
            return r4
        L75:
            java.lang.String r0 = "Non-standard token 'NaN': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r3.w1(r0)
            goto La2
        L7b:
            java.lang.String r0 = "Infinity"
            r3.H2(r0, r1)
            com.fasterxml.jackson.core.JsonParser$Feature r1 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS
            boolean r1 = r3.J0(r1)
            if (r1 == 0) goto L8f
            r1 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            com.fasterxml.jackson.core.JsonToken r4 = r3.n2(r0, r1)
            return r4
        L8f:
            java.lang.String r0 = "Non-standard token 'Infinity': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow"
            r3.w1(r0)
            goto La2
        L95:
            com.fasterxml.jackson.core.JsonParser$Feature r0 = com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_SINGLE_QUOTES
            boolean r0 = r3.J0(r0)
            if (r0 == 0) goto La2
            com.fasterxml.jackson.core.JsonToken r4 = r3.B2()
            return r4
        La2:
            boolean r0 = java.lang.Character.isJavaIdentifierStart(r4)
            if (r0 == 0) goto Lbf
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = ""
            r0.append(r1)
            char r1 = (char) r4
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "('true', 'false' or 'null')"
            r3.X2(r0, r1)
        Lbf:
            java.lang.String r0 = "expected a valid value (number, String, array, object, 'true', 'false' or 'null')"
            r3.I1(r4, r0)
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.E2(int):com.fasterxml.jackson.core.JsonToken");
    }

    public final String E3(int i, int i2, int i3, int i4) throws IOException {
        int[] iArr = this.Q0;
        iArr[0] = i;
        return z3(iArr, 1, i2, i3, i4);
    }

    public final boolean F2() throws IOException {
        byte[] bArr;
        int length;
        int i = this.k0;
        this.l0 += i;
        this.n0 -= i;
        this.T0 -= i;
        InputStream inputStream = this.W0;
        if (inputStream == null || (length = (bArr = this.X0).length) == 0) {
            return false;
        }
        int read = inputStream.read(bArr, 0, length);
        if (read > 0) {
            this.j0 = 0;
            this.k0 = read;
            return true;
        }
        O1();
        if (read == 0) {
            throw new IOException("InputStream.read() returned 0 characters when trying to read " + this.X0.length + " bytes");
        }
        return false;
    }

    public final String F3(int i, int i2, int i3, int i4, int i5) throws IOException {
        int[] iArr = this.Q0;
        iArr[0] = i;
        iArr[1] = i2;
        return z3(iArr, 2, i3, i4, i5);
    }

    public void G2() throws IOException {
        if (F2()) {
            return;
        }
        y1();
    }

    public String G3() throws IOException {
        if (this.j0 >= this.k0 && !F2()) {
            z1(": was expecting closing '\"' for name", JsonToken.FIELD_NAME);
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        this.j0 = i + 1;
        int i2 = bArr[i] & 255;
        return i2 == 34 ? "" : z3(this.Q0, 0, 0, i2, 0);
    }

    public final void H2(String str, int i) throws IOException {
        int i2;
        int length = str.length();
        if (this.j0 + length >= this.k0) {
            I2(str, i);
            return;
        }
        do {
            if (this.X0[this.j0] != str.charAt(i)) {
                W2(str.substring(0, i));
            }
            i2 = this.j0 + 1;
            this.j0 = i2;
            i++;
        } while (i < length);
        int i3 = this.X0[i2] & 255;
        if (i3 < 48 || i3 == 93 || i3 == 125) {
            return;
        }
        q2(str, i, i3);
    }

    public final void I2(String str, int i) throws IOException {
        int i2;
        int i3;
        int length = str.length();
        do {
            if ((this.j0 >= this.k0 && !F2()) || this.X0[this.j0] != str.charAt(i)) {
                W2(str.substring(0, i));
            }
            i2 = this.j0 + 1;
            this.j0 = i2;
            i++;
        } while (i < length);
        if ((i2 < this.k0 || F2()) && (i3 = this.X0[this.j0] & 255) >= 48 && i3 != 93 && i3 != 125) {
            q2(str, i, i3);
        }
    }

    public final JsonToken J2() {
        this.v0 = false;
        JsonToken jsonToken = this.s0;
        this.s0 = null;
        if (jsonToken == JsonToken.START_ARRAY) {
            this.r0 = this.r0.l(this.p0, this.q0);
        } else if (jsonToken == JsonToken.START_OBJECT) {
            this.r0 = this.r0.m(this.p0, this.q0);
        }
        this.g0 = jsonToken;
        return jsonToken;
    }

    public final JsonToken K2(int i) throws IOException {
        if (i == 34) {
            this.R0 = true;
            JsonToken jsonToken = JsonToken.VALUE_STRING;
            this.g0 = jsonToken;
            return jsonToken;
        } else if (i == 45) {
            JsonToken O2 = O2();
            this.g0 = O2;
            return O2;
        } else if (i == 91) {
            this.r0 = this.r0.l(this.p0, this.q0);
            JsonToken jsonToken2 = JsonToken.START_ARRAY;
            this.g0 = jsonToken2;
            return jsonToken2;
        } else if (i == 102) {
            H2("false", 1);
            JsonToken jsonToken3 = JsonToken.VALUE_FALSE;
            this.g0 = jsonToken3;
            return jsonToken3;
        } else if (i == 110) {
            H2("null", 1);
            JsonToken jsonToken4 = JsonToken.VALUE_NULL;
            this.g0 = jsonToken4;
            return jsonToken4;
        } else if (i == 116) {
            H2("true", 1);
            JsonToken jsonToken5 = JsonToken.VALUE_TRUE;
            this.g0 = jsonToken5;
            return jsonToken5;
        } else if (i != 123) {
            switch (i) {
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                    JsonToken Q2 = Q2(i);
                    this.g0 = Q2;
                    return Q2;
                default:
                    JsonToken E2 = E2(i);
                    this.g0 = E2;
                    return E2;
            }
        } else {
            this.r0 = this.r0.m(this.p0, this.q0);
            JsonToken jsonToken6 = JsonToken.START_OBJECT;
            this.g0 = jsonToken6;
            return jsonToken6;
        }
    }

    public String L2() throws IOException {
        if (this.j0 >= this.k0 && !F2()) {
            z1(": was expecting closing ''' for field name", JsonToken.FIELD_NAME);
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        this.j0 = i + 1;
        int i2 = bArr[i] & 255;
        if (i2 == 39) {
            return "";
        }
        int[] iArr = this.Q0;
        int[] iArr2 = a1;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i2 != 39) {
            if (i2 != 34 && iArr2[i2] != 0) {
                if (i2 != 92) {
                    L1(i2, PublicResolver.FUNC_NAME);
                } else {
                    i2 = R1();
                }
                if (i2 > 127) {
                    if (i3 >= 4) {
                        if (i4 >= iArr.length) {
                            iArr = w3(iArr, iArr.length);
                            this.Q0 = iArr;
                        }
                        iArr[i4] = i5;
                        i5 = 0;
                        i4++;
                        i3 = 0;
                    }
                    if (i2 < 2048) {
                        i5 = (i5 << 8) | (i2 >> 6) | 192;
                        i3++;
                    } else {
                        int i6 = (i5 << 8) | (i2 >> 12) | 224;
                        int i7 = i3 + 1;
                        if (i7 >= 4) {
                            if (i4 >= iArr.length) {
                                iArr = w3(iArr, iArr.length);
                                this.Q0 = iArr;
                            }
                            iArr[i4] = i6;
                            i6 = 0;
                            i4++;
                            i7 = 0;
                        }
                        i5 = (i6 << 8) | ((i2 >> 6) & 63) | 128;
                        i3 = i7 + 1;
                    }
                    i2 = (i2 & 63) | 128;
                }
            }
            if (i3 < 4) {
                i3++;
                i5 = i2 | (i5 << 8);
            } else {
                if (i4 >= iArr.length) {
                    iArr = w3(iArr, iArr.length);
                    this.Q0 = iArr;
                }
                iArr[i4] = i5;
                i5 = i2;
                i4++;
                i3 = 1;
            }
            if (this.j0 >= this.k0 && !F2()) {
                z1(" in field name", JsonToken.FIELD_NAME);
            }
            byte[] bArr2 = this.X0;
            int i8 = this.j0;
            this.j0 = i8 + 1;
            i2 = bArr2[i8] & 255;
        }
        if (i3 > 0) {
            if (i4 >= iArr.length) {
                int[] w3 = w3(iArr, iArr.length);
                this.Q0 = w3;
                iArr = w3;
            }
            iArr[i4] = y3(i5, i3);
            i4++;
        }
        String F = this.P0.F(iArr, i4);
        return F == null ? r3(iArr, i4, i3) : F;
    }

    public final JsonToken M2(char[] cArr, int i, int i2, boolean z, int i3) throws IOException {
        int i4;
        boolean z2;
        int i5 = 0;
        if (i2 == 46) {
            if (i >= cArr.length) {
                cArr = this.t0.n();
                i = 0;
            }
            cArr[i] = (char) i2;
            i++;
            i4 = 0;
            while (true) {
                if (this.j0 >= this.k0 && !F2()) {
                    z2 = true;
                    break;
                }
                byte[] bArr = this.X0;
                int i6 = this.j0;
                this.j0 = i6 + 1;
                i2 = bArr[i6] & 255;
                if (i2 < 48 || i2 > 57) {
                    break;
                }
                i4++;
                if (i >= cArr.length) {
                    cArr = this.t0.n();
                    i = 0;
                }
                cArr[i] = (char) i2;
                i++;
            }
            z2 = false;
            if (i4 == 0) {
                l2(i2, "Decimal point not followed by a digit");
            }
        } else {
            i4 = 0;
            z2 = false;
        }
        if (i2 == 101 || i2 == 69) {
            if (i >= cArr.length) {
                cArr = this.t0.n();
                i = 0;
            }
            int i7 = i + 1;
            cArr[i] = (char) i2;
            if (this.j0 >= this.k0) {
                G2();
            }
            byte[] bArr2 = this.X0;
            int i8 = this.j0;
            this.j0 = i8 + 1;
            int i9 = bArr2[i8] & 255;
            if (i9 == 45 || i9 == 43) {
                if (i7 >= cArr.length) {
                    cArr = this.t0.n();
                    i7 = 0;
                }
                int i10 = i7 + 1;
                cArr[i7] = (char) i9;
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr3 = this.X0;
                int i11 = this.j0;
                this.j0 = i11 + 1;
                i9 = bArr3[i11] & 255;
                i7 = i10;
            }
            i2 = i9;
            int i12 = 0;
            while (i2 <= 57 && i2 >= 48) {
                i12++;
                if (i7 >= cArr.length) {
                    cArr = this.t0.n();
                    i7 = 0;
                }
                int i13 = i7 + 1;
                cArr[i7] = (char) i2;
                if (this.j0 >= this.k0 && !F2()) {
                    i5 = i12;
                    z2 = true;
                    i = i13;
                    break;
                }
                byte[] bArr4 = this.X0;
                int i14 = this.j0;
                this.j0 = i14 + 1;
                i2 = bArr4[i14] & 255;
                i7 = i13;
            }
            i5 = i12;
            i = i7;
            if (i5 == 0) {
                l2(i2, "Exponent indicator not followed by a digit");
            }
        }
        if (!z2) {
            this.j0--;
            if (this.r0.g()) {
                q3(i2);
            }
        }
        this.t0.z(i);
        return o2(z, i3, i4, i5);
    }

    public final String N2(int i) throws IOException {
        if (i != 34) {
            return D2(i);
        }
        int i2 = this.j0;
        if (i2 + 13 > this.k0) {
            return G3();
        }
        byte[] bArr = this.X0;
        int[] iArr = a1;
        int i3 = i2 + 1;
        this.j0 = i3;
        int i4 = bArr[i2] & 255;
        if (iArr[i4] != 0) {
            return i4 == 34 ? "" : D3(0, i4, 0);
        }
        int i5 = i3 + 1;
        this.j0 = i5;
        int i6 = bArr[i3] & 255;
        if (iArr[i6] != 0) {
            if (i6 == 34) {
                return s3(i4, 1);
            }
            return D3(i4, i6, 1);
        }
        int i7 = (i4 << 8) | i6;
        int i8 = i5 + 1;
        this.j0 = i8;
        int i9 = bArr[i5] & 255;
        if (iArr[i9] != 0) {
            if (i9 == 34) {
                return s3(i7, 2);
            }
            return D3(i7, i9, 2);
        }
        int i10 = (i7 << 8) | i9;
        int i11 = i8 + 1;
        this.j0 = i11;
        int i12 = bArr[i8] & 255;
        if (iArr[i12] != 0) {
            if (i12 == 34) {
                return s3(i10, 3);
            }
            return D3(i10, i12, 3);
        }
        int i13 = (i10 << 8) | i12;
        this.j0 = i11 + 1;
        int i14 = bArr[i11] & 255;
        if (iArr[i14] == 0) {
            this.S0 = i13;
            return B3(i14);
        } else if (i14 == 34) {
            return s3(i13, 4);
        } else {
            return D3(i13, i14, 4);
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String O0() throws IOException {
        JsonToken O2;
        this.y0 = 0;
        JsonToken jsonToken = this.g0;
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (jsonToken == jsonToken2) {
            J2();
            return null;
        }
        if (this.R0) {
            e3();
        }
        int k3 = k3();
        if (k3 < 0) {
            close();
            this.g0 = null;
            return null;
        }
        this.x0 = null;
        if (k3 == 93) {
            n3();
            if (!this.r0.e()) {
                Z1(k3, '}');
            }
            this.r0 = this.r0.k();
            this.g0 = JsonToken.END_ARRAY;
            return null;
        } else if (k3 == 125) {
            n3();
            if (!this.r0.f()) {
                Z1(k3, ']');
            }
            this.r0 = this.r0.k();
            this.g0 = JsonToken.END_OBJECT;
            return null;
        } else {
            if (this.r0.o()) {
                if (k3 != 44) {
                    I1(k3, "was expecting comma to separate " + this.r0.i() + " entries");
                }
                k3 = i3();
            }
            if (!this.r0.f()) {
                n3();
                K2(k3);
                return null;
            }
            o3();
            String N2 = N2(k3);
            this.r0.t(N2);
            this.g0 = jsonToken2;
            int a3 = a3();
            n3();
            if (a3 == 34) {
                this.R0 = true;
                this.s0 = JsonToken.VALUE_STRING;
                return N2;
            }
            if (a3 == 45) {
                O2 = O2();
            } else if (a3 == 91) {
                O2 = JsonToken.START_ARRAY;
            } else if (a3 == 102) {
                H2("false", 1);
                O2 = JsonToken.VALUE_FALSE;
            } else if (a3 == 110) {
                H2("null", 1);
                O2 = JsonToken.VALUE_NULL;
            } else if (a3 == 116) {
                H2("true", 1);
                O2 = JsonToken.VALUE_TRUE;
            } else if (a3 != 123) {
                switch (a3) {
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                        O2 = Q2(a3);
                        break;
                    default:
                        O2 = E2(a3);
                        break;
                }
            } else {
                O2 = JsonToken.START_OBJECT;
            }
            this.s0 = O2;
            return N2;
        }
    }

    @Override // defpackage.qp2
    public void O1() throws IOException {
        if (this.W0 != null) {
            if (this.h0.n() || J0(JsonParser.Feature.AUTO_CLOSE_SOURCE)) {
                this.W0.close();
            }
            this.W0 = null;
        }
    }

    public JsonToken O2() throws IOException {
        int i;
        int i2;
        char[] k = this.t0.k();
        k[0] = '-';
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i3 = this.j0;
        this.j0 = i3 + 1;
        int i4 = bArr[i3] & 255;
        if (i4 >= 48 && i4 <= 57) {
            if (i4 == 48) {
                i4 = p3();
            }
            int i5 = 2;
            k[1] = (char) i4;
            int length = (this.j0 + k.length) - 2;
            int i6 = this.k0;
            if (length > i6) {
                length = i6;
            }
            int i7 = 1;
            while (true) {
                int i8 = this.j0;
                if (i8 >= length) {
                    return P2(k, i5, true, i7);
                }
                byte[] bArr2 = this.X0;
                i = i8 + 1;
                this.j0 = i;
                i2 = bArr2[i8] & 255;
                if (i2 < 48 || i2 > 57) {
                    break;
                }
                i7++;
                k[i5] = (char) i2;
                i5++;
            }
            if (i2 != 46 && i2 != 101 && i2 != 69) {
                this.j0 = i - 1;
                this.t0.z(i5);
                if (this.r0.g()) {
                    q3(i2);
                }
                return p2(true, i7);
            }
            return M2(k, i5, i2, true, i7);
        }
        return C2(i4, true);
    }

    /* JADX WARN: Code restructure failed: missing block: B:19:0x0044, code lost:
        if (r3 == 46) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0048, code lost:
        if (r3 == 101) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x004c, code lost:
        if (r3 != 69) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x004f, code lost:
        r6.j0 = r10 - 1;
        r6.t0.z(r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x005e, code lost:
        if (r6.r0.g() == false) goto L30;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0060, code lost:
        r7 = r6.X0;
        r8 = r6.j0;
        r6.j0 = r8 + 1;
        q3(r7[r8] & 255);
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x0073, code lost:
        return p2(r9, r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x007a, code lost:
        return M2(r1, r2, r3, r9, r5);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final com.fasterxml.jackson.core.JsonToken P2(char[] r7, int r8, boolean r9, int r10) throws java.io.IOException {
        /*
            r6 = this;
            r1 = r7
            r2 = r8
            r5 = r10
        L3:
            int r7 = r6.j0
            int r8 = r6.k0
            if (r7 < r8) goto L19
            boolean r7 = r6.F2()
            if (r7 != 0) goto L19
            com.fasterxml.jackson.core.util.c r7 = r6.t0
            r7.z(r2)
            com.fasterxml.jackson.core.JsonToken r7 = r6.p2(r9, r5)
            return r7
        L19:
            byte[] r7 = r6.X0
            int r8 = r6.j0
            int r10 = r8 + 1
            r6.j0 = r10
            r7 = r7[r8]
            r3 = r7 & 255(0xff, float:3.57E-43)
            r7 = 57
            if (r3 > r7) goto L42
            r7 = 48
            if (r3 >= r7) goto L2e
            goto L42
        L2e:
            int r7 = r1.length
            if (r2 < r7) goto L39
            com.fasterxml.jackson.core.util.c r7 = r6.t0
            char[] r7 = r7.n()
            r2 = 0
            r1 = r7
        L39:
            int r7 = r2 + 1
            char r8 = (char) r3
            r1[r2] = r8
            int r5 = r5 + 1
            r2 = r7
            goto L3
        L42:
            r7 = 46
            if (r3 == r7) goto L74
            r7 = 101(0x65, float:1.42E-43)
            if (r3 == r7) goto L74
            r7 = 69
            if (r3 != r7) goto L4f
            goto L74
        L4f:
            int r10 = r10 + (-1)
            r6.j0 = r10
            com.fasterxml.jackson.core.util.c r7 = r6.t0
            r7.z(r2)
            hv1 r7 = r6.r0
            boolean r7 = r7.g()
            if (r7 == 0) goto L6f
            byte[] r7 = r6.X0
            int r8 = r6.j0
            int r10 = r8 + 1
            r6.j0 = r10
            r7 = r7[r8]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6.q3(r7)
        L6f:
            com.fasterxml.jackson.core.JsonToken r7 = r6.p2(r9, r5)
            return r7
        L74:
            r0 = r6
            r4 = r9
            com.fasterxml.jackson.core.JsonToken r7 = r0.M2(r1, r2, r3, r4, r5)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.P2(char[], int, boolean, int):com.fasterxml.jackson.core.JsonToken");
    }

    public JsonToken Q2(int i) throws IOException {
        int i2;
        int i3;
        int i4;
        int i5;
        char[] k = this.t0.k();
        if (i == 48) {
            i = p3();
        }
        k[0] = (char) i;
        int length = (this.j0 + k.length) - 1;
        int i6 = this.k0;
        if (length > i6) {
            i3 = 1;
            length = i6;
            i2 = 1;
        } else {
            i2 = 1;
            i3 = 1;
        }
        while (true) {
            int i7 = this.j0;
            if (i7 >= length) {
                return P2(k, i2, false, i3);
            }
            byte[] bArr = this.X0;
            i4 = i7 + 1;
            this.j0 = i4;
            i5 = bArr[i7] & 255;
            if (i5 < 48 || i5 > 57) {
                break;
            }
            i3++;
            k[i2] = (char) i5;
            i2++;
        }
        if (i5 != 46 && i5 != 101 && i5 != 69) {
            this.j0 = i4 - 1;
            this.t0.z(i2);
            if (this.r0.g()) {
                q3(i5);
            }
            return p2(false, i3);
        }
        return M2(k, i2, i5, false, i3);
    }

    @Override // defpackage.qp2
    public char R1() throws IOException {
        if (this.j0 >= this.k0 && !F2()) {
            z1(" in character escape sequence", JsonToken.VALUE_STRING);
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        this.j0 = i + 1;
        byte b = bArr[i];
        if (b == 34 || b == 47 || b == 92) {
            return (char) b;
        }
        if (b != 98) {
            if (b != 102) {
                if (b != 110) {
                    if (b != 114) {
                        if (b != 116) {
                            if (b != 117) {
                                return s1((char) s2(b));
                            }
                            int i2 = 0;
                            for (int i3 = 0; i3 < 4; i3++) {
                                if (this.j0 >= this.k0 && !F2()) {
                                    z1(" in character escape sequence", JsonToken.VALUE_STRING);
                                }
                                byte[] bArr2 = this.X0;
                                int i4 = this.j0;
                                this.j0 = i4 + 1;
                                byte b2 = bArr2[i4];
                                int b3 = a.b(b2);
                                if (b3 < 0) {
                                    I1(b2, "expected a hex-digit for character escape sequence");
                                }
                                i2 = (i2 << 4) | b3;
                            }
                            return (char) i2;
                        }
                        return '\t';
                    }
                    return '\r';
                }
                return '\n';
            }
            return '\f';
        }
        return '\b';
    }

    public int R2(Base64Variant base64Variant, OutputStream outputStream, byte[] bArr) throws IOException {
        int i;
        int length = bArr.length - 3;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (this.j0 >= this.k0) {
                G2();
            }
            byte[] bArr2 = this.X0;
            int i4 = this.j0;
            this.j0 = i4 + 1;
            int i5 = bArr2[i4] & 255;
            if (i5 > 32) {
                int decodeBase64Char = base64Variant.decodeBase64Char(i5);
                if (decodeBase64Char < 0) {
                    if (i5 == 34) {
                        break;
                    }
                    decodeBase64Char = Q1(base64Variant, i5, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                if (i2 > length) {
                    i3 += i2;
                    outputStream.write(bArr, 0, i2);
                    i2 = 0;
                }
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr3 = this.X0;
                int i6 = this.j0;
                this.j0 = i6 + 1;
                int i7 = bArr3[i6] & 255;
                int decodeBase64Char2 = base64Variant.decodeBase64Char(i7);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = Q1(base64Variant, i7, 1);
                }
                int i8 = (decodeBase64Char << 6) | decodeBase64Char2;
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr4 = this.X0;
                int i9 = this.j0;
                this.j0 = i9 + 1;
                int i10 = bArr4[i9] & 255;
                int decodeBase64Char3 = base64Variant.decodeBase64Char(i10);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (i10 == 34 && !base64Variant.usesPadding()) {
                            bArr[i2] = (byte) (i8 >> 4);
                            i2++;
                            break;
                        }
                        decodeBase64Char3 = Q1(base64Variant, i10, 2);
                    }
                    if (decodeBase64Char3 == -2) {
                        if (this.j0 >= this.k0) {
                            G2();
                        }
                        byte[] bArr5 = this.X0;
                        int i11 = this.j0;
                        this.j0 = i11 + 1;
                        int i12 = bArr5[i11] & 255;
                        if (base64Variant.usesPaddingChar(i12)) {
                            i = i2 + 1;
                            bArr[i2] = (byte) (i8 >> 4);
                            i2 = i;
                        } else {
                            throw h2(base64Variant, i12, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                    }
                }
                int i13 = (i8 << 6) | decodeBase64Char3;
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr6 = this.X0;
                int i14 = this.j0;
                this.j0 = i14 + 1;
                int i15 = bArr6[i14] & 255;
                int decodeBase64Char4 = base64Variant.decodeBase64Char(i15);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (i15 == 34 && !base64Variant.usesPadding()) {
                            int i16 = i13 >> 2;
                            int i17 = i2 + 1;
                            bArr[i2] = (byte) (i16 >> 8);
                            i2 = i17 + 1;
                            bArr[i17] = (byte) i16;
                            break;
                        }
                        decodeBase64Char4 = Q1(base64Variant, i15, 3);
                    }
                    if (decodeBase64Char4 == -2) {
                        int i18 = i13 >> 2;
                        int i19 = i2 + 1;
                        bArr[i2] = (byte) (i18 >> 8);
                        i2 = i19 + 1;
                        bArr[i19] = (byte) i18;
                    }
                }
                int i20 = (i13 << 6) | decodeBase64Char4;
                int i21 = i2 + 1;
                bArr[i2] = (byte) (i20 >> 16);
                int i22 = i21 + 1;
                bArr[i21] = (byte) (i20 >> 8);
                i = i22 + 1;
                bArr[i22] = (byte) i20;
                i2 = i;
            }
        }
        this.R0 = false;
        if (i2 > 0) {
            int i23 = i3 + i2;
            outputStream.write(bArr, 0, i2);
            return i23;
        }
        return i3;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String S0() throws IOException {
        if (this.g0 == JsonToken.FIELD_NAME) {
            this.v0 = false;
            JsonToken jsonToken = this.s0;
            this.s0 = null;
            this.g0 = jsonToken;
            if (jsonToken == JsonToken.VALUE_STRING) {
                if (this.R0) {
                    this.R0 = false;
                    return x2();
                }
                return this.t0.j();
            }
            if (jsonToken == JsonToken.START_ARRAY) {
                this.r0 = this.r0.l(this.p0, this.q0);
            } else if (jsonToken == JsonToken.START_OBJECT) {
                this.r0 = this.r0.m(this.p0, this.q0);
            }
            return null;
        } else if (T0() == JsonToken.VALUE_STRING) {
            return X();
        } else {
            return null;
        }
    }

    public void S2(int i) throws JsonParseException {
        if (i < 32) {
            K1(i);
        }
        T2(i);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonToken T0() throws IOException {
        JsonToken O2;
        JsonToken jsonToken = this.g0;
        JsonToken jsonToken2 = JsonToken.FIELD_NAME;
        if (jsonToken == jsonToken2) {
            return J2();
        }
        this.y0 = 0;
        if (this.R0) {
            e3();
        }
        int k3 = k3();
        if (k3 < 0) {
            close();
            this.g0 = null;
            return null;
        }
        this.x0 = null;
        if (k3 == 93) {
            n3();
            if (!this.r0.e()) {
                Z1(k3, '}');
            }
            this.r0 = this.r0.k();
            JsonToken jsonToken3 = JsonToken.END_ARRAY;
            this.g0 = jsonToken3;
            return jsonToken3;
        } else if (k3 == 125) {
            n3();
            if (!this.r0.f()) {
                Z1(k3, ']');
            }
            this.r0 = this.r0.k();
            JsonToken jsonToken4 = JsonToken.END_OBJECT;
            this.g0 = jsonToken4;
            return jsonToken4;
        } else {
            if (this.r0.o()) {
                if (k3 != 44) {
                    I1(k3, "was expecting comma to separate " + this.r0.i() + " entries");
                }
                k3 = i3();
            }
            if (!this.r0.f()) {
                n3();
                return K2(k3);
            }
            o3();
            this.r0.t(N2(k3));
            this.g0 = jsonToken2;
            int a3 = a3();
            n3();
            if (a3 == 34) {
                this.R0 = true;
                this.s0 = JsonToken.VALUE_STRING;
                return this.g0;
            }
            if (a3 == 45) {
                O2 = O2();
            } else if (a3 == 91) {
                O2 = JsonToken.START_ARRAY;
            } else if (a3 == 102) {
                H2("false", 1);
                O2 = JsonToken.VALUE_FALSE;
            } else if (a3 == 110) {
                H2("null", 1);
                O2 = JsonToken.VALUE_NULL;
            } else if (a3 == 116) {
                H2("true", 1);
                O2 = JsonToken.VALUE_TRUE;
            } else if (a3 != 123) {
                switch (a3) {
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                        O2 = Q2(a3);
                        break;
                    default:
                        O2 = E2(a3);
                        break;
                }
            } else {
                O2 = JsonToken.START_OBJECT;
            }
            this.s0 = O2;
            return this.g0;
        }
    }

    public void T2(int i) throws JsonParseException {
        w1("Invalid UTF-8 start byte 0x" + Integer.toHexString(i));
    }

    public void U2(int i) throws JsonParseException {
        w1("Invalid UTF-8 middle byte 0x" + Integer.toHexString(i));
    }

    public void V2(int i, int i2) throws JsonParseException {
        this.j0 = i2;
        U2(i);
    }

    public void W2(String str) throws IOException {
        X2(str, "'null', 'true', 'false' or NaN");
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public String X() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                return x2();
            }
            return this.t0.j();
        }
        return A2(jsonToken);
    }

    public void X2(String str, String str2) throws IOException {
        StringBuilder sb = new StringBuilder(str);
        while (sb.length() < 256 && (this.j0 < this.k0 || F2())) {
            byte[] bArr = this.X0;
            int i = this.j0;
            this.j0 = i + 1;
            char s2 = (char) s2(bArr[i]);
            if (!Character.isJavaIdentifierPart(s2)) {
                break;
            }
            sb.append(s2);
        }
        if (sb.length() == 256) {
            sb.append("...");
        }
        w1("Unrecognized token '" + sb.toString() + "': was expecting " + str2);
    }

    @Override // defpackage.qp2
    public void Y1() throws IOException {
        byte[] bArr;
        super.Y1();
        this.P0.M();
        if (!this.Y0 || (bArr = this.X0) == null) {
            return;
        }
        this.X0 = ms.j0;
        this.h0.r(bArr);
    }

    /* JADX WARN: Code restructure failed: missing block: B:26:0x0043, code lost:
        z1(" in a comment", null);
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x0049, code lost:
        return;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void Y2() throws java.io.IOException {
        /*
            r5 = this;
            int[] r0 = com.fasterxml.jackson.core.io.a.f()
        L4:
            int r1 = r5.j0
            int r2 = r5.k0
            if (r1 < r2) goto L10
            boolean r1 = r5.F2()
            if (r1 == 0) goto L43
        L10:
            byte[] r1 = r5.X0
            int r2 = r5.j0
            int r3 = r2 + 1
            r5.j0 = r3
            r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r2 = r0[r1]
            if (r2 == 0) goto L4
            r4 = 2
            if (r2 == r4) goto L6e
            r4 = 3
            if (r2 == r4) goto L6a
            r4 = 4
            if (r2 == r4) goto L66
            r4 = 10
            if (r2 == r4) goto L5d
            r4 = 13
            if (r2 == r4) goto L59
            r4 = 42
            if (r2 == r4) goto L39
            r5.S2(r1)
            goto L4
        L39:
            int r1 = r5.k0
            if (r3 < r1) goto L4a
            boolean r1 = r5.F2()
            if (r1 != 0) goto L4a
        L43:
            r0 = 0
            java.lang.String r1 = " in a comment"
            r5.z1(r1, r0)
            return
        L4a:
            byte[] r1 = r5.X0
            int r2 = r5.j0
            r1 = r1[r2]
            r3 = 47
            if (r1 != r3) goto L4
            int r2 = r2 + 1
            r5.j0 = r2
            return
        L59:
            r5.Z2()
            goto L4
        L5d:
            int r1 = r5.m0
            int r1 = r1 + 1
            r5.m0 = r1
            r5.n0 = r3
            goto L4
        L66:
            r5.h3(r1)
            goto L4
        L6a:
            r5.g3()
            goto L4
        L6e:
            r5.f3()
            goto L4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.Y2():void");
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int Z0(Base64Variant base64Variant, OutputStream outputStream) throws IOException {
        if (this.R0 && this.g0 == JsonToken.VALUE_STRING) {
            byte[] d = this.h0.d();
            try {
                return R2(base64Variant, outputStream, d);
            } finally {
                this.h0.o(d);
            }
        }
        byte[] j = j(base64Variant);
        outputStream.write(j);
        return j.length;
    }

    public final void Z2() throws IOException {
        if (this.j0 < this.k0 || F2()) {
            byte[] bArr = this.X0;
            int i = this.j0;
            if (bArr[i] == 10) {
                this.j0 = i + 1;
            }
        }
        this.m0++;
        this.n0 = this.j0;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public char[] a0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 5) {
                if (id != 6) {
                    if (id != 7 && id != 8) {
                        return this.g0.asCharArray();
                    }
                } else if (this.R0) {
                    this.R0 = false;
                    y2();
                }
                return this.t0.q();
            }
            if (!this.v0) {
                String b = this.r0.b();
                int length = b.length();
                char[] cArr = this.u0;
                if (cArr == null) {
                    this.u0 = this.h0.f(length);
                } else if (cArr.length < length) {
                    this.u0 = new char[length];
                }
                b.getChars(0, length, this.u0, 0);
                this.v0 = true;
            }
            return this.u0;
        }
        return null;
    }

    public final int a3() throws IOException {
        int i = this.j0;
        if (i + 4 >= this.k0) {
            return b3(false);
        }
        byte[] bArr = this.X0;
        byte b = bArr[i];
        if (b == 58) {
            int i2 = i + 1;
            this.j0 = i2;
            byte b2 = bArr[i2];
            if (b2 > 32) {
                if (b2 != 47 && b2 != 35) {
                    this.j0 = i2 + 1;
                    return b2;
                }
                return b3(true);
            }
            if (b2 == 32 || b2 == 9) {
                int i3 = i2 + 1;
                this.j0 = i3;
                byte b3 = bArr[i3];
                if (b3 > 32) {
                    if (b3 != 47 && b3 != 35) {
                        this.j0 = i3 + 1;
                        return b3;
                    }
                    return b3(true);
                }
            }
            return b3(true);
        }
        if (b == 32 || b == 9) {
            int i4 = i + 1;
            this.j0 = i4;
            b = bArr[i4];
        }
        if (b == 58) {
            int i5 = this.j0 + 1;
            this.j0 = i5;
            byte b4 = bArr[i5];
            if (b4 > 32) {
                if (b4 != 47 && b4 != 35) {
                    this.j0 = i5 + 1;
                    return b4;
                }
                return b3(true);
            }
            if (b4 == 32 || b4 == 9) {
                int i6 = i5 + 1;
                this.j0 = i6;
                byte b5 = bArr[i6];
                if (b5 > 32) {
                    if (b5 != 47 && b5 != 35) {
                        this.j0 = i6 + 1;
                        return b5;
                    }
                    return b3(true);
                }
            }
            return b3(true);
        }
        return b3(false);
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public int b0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != null) {
            int id = jsonToken.id();
            if (id != 5) {
                if (id != 6) {
                    if (id != 7 && id != 8) {
                        return this.g0.asCharArray().length;
                    }
                } else if (this.R0) {
                    this.R0 = false;
                    y2();
                }
                return this.t0.A();
            }
            return this.r0.b().length();
        }
        return 0;
    }

    public final int b3(boolean z) throws IOException {
        while (true) {
            if (this.j0 >= this.k0 && !F2()) {
                z1(" within/between " + this.r0.i() + " entries", null);
                return -1;
            }
            byte[] bArr = this.X0;
            int i = this.j0;
            int i2 = i + 1;
            this.j0 = i2;
            int i3 = bArr[i] & 255;
            if (i3 > 32) {
                if (i3 == 47) {
                    c3();
                } else if (i3 != 35 || !m3()) {
                    if (z) {
                        return i3;
                    }
                    if (i3 != 58) {
                        I1(i3, "was expecting a colon to separate field name and value");
                    }
                    z = true;
                }
            } else if (i3 != 32) {
                if (i3 == 10) {
                    this.m0++;
                    this.n0 = i2;
                } else if (i3 == 13) {
                    Z2();
                } else if (i3 != 9) {
                    K1(i3);
                }
            }
        }
    }

    public final void c3() throws IOException {
        if (!J0(JsonParser.Feature.ALLOW_COMMENTS)) {
            I1(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this.j0 >= this.k0 && !F2()) {
            z1(" in a comment", null);
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        this.j0 = i + 1;
        int i2 = bArr[i] & 255;
        if (i2 == 47) {
            d3();
        } else if (i2 == 42) {
            Y2();
        } else {
            I1(i2, "was expecting either '*' or '/' for a comment");
        }
    }

    public final void d3() throws IOException {
        int[] f = a.f();
        while (true) {
            if (this.j0 >= this.k0 && !F2()) {
                return;
            }
            byte[] bArr = this.X0;
            int i = this.j0;
            int i2 = i + 1;
            this.j0 = i2;
            int i3 = bArr[i] & 255;
            int i4 = f[i3];
            if (i4 != 0) {
                if (i4 == 2) {
                    f3();
                } else if (i4 == 3) {
                    g3();
                } else if (i4 == 4) {
                    h3(i3);
                } else if (i4 == 10) {
                    this.m0++;
                    this.n0 = i2;
                    return;
                } else if (i4 == 13) {
                    Z2();
                    return;
                } else if (i4 != 42 && i4 < 0) {
                    S2(i3);
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r0 != 8) goto L15;
     */
    @Override // com.fasterxml.jackson.core.JsonParser
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public int e0() throws java.io.IOException {
        /*
            r3 = this;
            com.fasterxml.jackson.core.JsonToken r0 = r3.g0
            r1 = 0
            if (r0 == 0) goto L24
            int r0 = r0.id()
            r2 = 6
            if (r0 == r2) goto L14
            r2 = 7
            if (r0 == r2) goto L1d
            r2 = 8
            if (r0 == r2) goto L1d
            goto L24
        L14:
            boolean r0 = r3.R0
            if (r0 == 0) goto L1d
            r3.R0 = r1
            r3.y2()
        L1d:
            com.fasterxml.jackson.core.util.c r0 = r3.t0
            int r0 = r0.r()
            return r0
        L24:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.e0():int");
    }

    public void e3() throws IOException {
        this.R0 = false;
        int[] iArr = Z0;
        byte[] bArr = this.X0;
        while (true) {
            int i = this.j0;
            int i2 = this.k0;
            if (i >= i2) {
                G2();
                i = this.j0;
                i2 = this.k0;
            }
            while (true) {
                if (i < i2) {
                    int i3 = i + 1;
                    int i4 = bArr[i] & 255;
                    if (iArr[i4] != 0) {
                        this.j0 = i3;
                        if (i4 == 34) {
                            return;
                        }
                        int i5 = iArr[i4];
                        if (i5 == 1) {
                            R1();
                        } else if (i5 == 2) {
                            f3();
                        } else if (i5 == 3) {
                            g3();
                        } else if (i5 == 4) {
                            h3(i4);
                        } else if (i4 < 32) {
                            L1(i4, "string value");
                        } else {
                            S2(i4);
                        }
                    } else {
                        i = i3;
                    }
                } else {
                    this.j0 = i;
                    break;
                }
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation f0() {
        Object m = this.h0.m();
        if (this.g0 == JsonToken.FIELD_NAME) {
            return new JsonLocation(m, (this.T0 - 1) + this.l0, -1L, this.U0, this.V0);
        }
        return new JsonLocation(m, this.o0 - 1, -1L, this.p0, this.q0);
    }

    public final void f3() throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        int i2 = i + 1;
        this.j0 = i2;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            V2(b & 255, i2);
        }
    }

    public final void g3() throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        int i2 = i + 1;
        this.j0 = i2;
        byte b = bArr[i];
        if ((b & 192) != 128) {
            V2(b & 255, i2);
        }
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr2 = this.X0;
        int i3 = this.j0;
        int i4 = i3 + 1;
        this.j0 = i4;
        byte b2 = bArr2[i3];
        if ((b2 & 192) != 128) {
            V2(b2 & 255, i4);
        }
    }

    public final void h3(int i) throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i2 = this.j0;
        int i3 = i2 + 1;
        this.j0 = i3;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            V2(b & 255, i3);
        }
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr2 = this.X0;
        int i4 = this.j0;
        int i5 = i4 + 1;
        this.j0 = i5;
        byte b2 = bArr2[i4];
        if ((b2 & 192) != 128) {
            V2(b2 & 255, i5);
        }
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr3 = this.X0;
        int i6 = this.j0;
        int i7 = i6 + 1;
        this.j0 = i7;
        byte b3 = bArr3[i6];
        if ((b3 & 192) != 128) {
            V2(b3 & 255, i7);
        }
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public int i0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_NUMBER_INT && jsonToken != JsonToken.VALUE_NUMBER_FLOAT) {
            return super.m0(0);
        }
        int i = this.y0;
        if ((i & 1) == 0) {
            if (i == 0) {
                return U1();
            }
            if ((i & 1) == 0) {
                d2();
            }
        }
        return this.z0;
    }

    public final int i3() throws IOException {
        while (true) {
            int i = this.j0;
            if (i < this.k0) {
                byte[] bArr = this.X0;
                int i2 = i + 1;
                this.j0 = i2;
                int i3 = bArr[i] & 255;
                if (i3 > 32) {
                    if (i3 == 47 || i3 == 35) {
                        this.j0 = i2 - 1;
                        return j3();
                    }
                    return i3;
                } else if (i3 != 32) {
                    if (i3 == 10) {
                        this.m0++;
                        this.n0 = i2;
                    } else if (i3 == 13) {
                        Z2();
                    } else if (i3 != 9) {
                        K1(i3);
                    }
                }
            } else {
                return j3();
            }
        }
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public byte[] j(Base64Variant base64Variant) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_STRING && (jsonToken != JsonToken.VALUE_EMBEDDED_OBJECT || this.x0 == null)) {
            w1("Current token (" + this.g0 + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this.R0) {
            try {
                this.x0 = r2(base64Variant);
                this.R0 = false;
            } catch (IllegalArgumentException e) {
                throw a("Failed to decode VALUE_STRING as base64 (" + base64Variant + "): " + e.getMessage());
            }
        } else if (this.x0 == null) {
            ms T1 = T1();
            o1(X(), T1, base64Variant);
            this.x0 = T1.n();
        }
        return this.x0;
    }

    public final int j3() throws IOException {
        int i;
        while (true) {
            if (this.j0 >= this.k0 && !F2()) {
                throw a("Unexpected end-of-input within/between " + this.r0.i() + " entries");
            }
            byte[] bArr = this.X0;
            int i2 = this.j0;
            int i3 = i2 + 1;
            this.j0 = i3;
            i = bArr[i2] & 255;
            if (i > 32) {
                if (i == 47) {
                    c3();
                } else if (i != 35 || !m3()) {
                    break;
                }
            } else if (i != 32) {
                if (i == 10) {
                    this.m0++;
                    this.n0 = i3;
                } else if (i == 13) {
                    Z2();
                } else if (i != 9) {
                    K1(i);
                }
            }
        }
        return i;
    }

    public final int k3() throws IOException {
        if (this.j0 >= this.k0 && !F2()) {
            return S1();
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        int i2 = i + 1;
        this.j0 = i2;
        int i3 = bArr[i] & 255;
        if (i3 > 32) {
            if (i3 == 47 || i3 == 35) {
                this.j0 = i2 - 1;
                return l3();
            }
            return i3;
        }
        if (i3 != 32) {
            if (i3 == 10) {
                this.m0++;
                this.n0 = i2;
            } else if (i3 == 13) {
                Z2();
            } else if (i3 != 9) {
                K1(i3);
            }
        }
        while (true) {
            int i4 = this.j0;
            if (i4 < this.k0) {
                byte[] bArr2 = this.X0;
                int i5 = i4 + 1;
                this.j0 = i5;
                int i6 = bArr2[i4] & 255;
                if (i6 > 32) {
                    if (i6 == 47 || i6 == 35) {
                        this.j0 = i5 - 1;
                        return l3();
                    }
                    return i6;
                } else if (i6 != 32) {
                    if (i6 == 10) {
                        this.m0++;
                        this.n0 = i5;
                    } else if (i6 == 13) {
                        Z2();
                    } else if (i6 != 9) {
                        K1(i6);
                    }
                }
            } else {
                return l3();
            }
        }
    }

    public final int l3() throws IOException {
        int i;
        while (true) {
            if (this.j0 >= this.k0 && !F2()) {
                return S1();
            }
            byte[] bArr = this.X0;
            int i2 = this.j0;
            int i3 = i2 + 1;
            this.j0 = i3;
            i = bArr[i2] & 255;
            if (i > 32) {
                if (i == 47) {
                    c3();
                } else if (i != 35 || !m3()) {
                    break;
                }
            } else if (i != 32) {
                if (i == 10) {
                    this.m0++;
                    this.n0 = i3;
                } else if (i == 13) {
                    Z2();
                } else if (i != 9) {
                    K1(i);
                }
            }
        }
        return i;
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public int m0(int i) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken != JsonToken.VALUE_NUMBER_INT && jsonToken != JsonToken.VALUE_NUMBER_FLOAT) {
            return super.m0(i);
        }
        int i2 = this.y0;
        if ((i2 & 1) == 0) {
            if (i2 == 0) {
                return U1();
            }
            if ((i2 & 1) == 0) {
                d2();
            }
        }
        return this.z0;
    }

    public final boolean m3() throws IOException {
        if (J0(JsonParser.Feature.ALLOW_YAML_COMMENTS)) {
            d3();
            return true;
        }
        return false;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public c n() {
        return this.O0;
    }

    public final void n3() {
        this.p0 = this.m0;
        int i = this.j0;
        this.o0 = this.l0 + i;
        this.q0 = i - this.n0;
    }

    public final void o3() {
        this.U0 = this.m0;
        int i = this.j0;
        this.T0 = i;
        this.V0 = i - this.n0;
    }

    public final int p3() throws IOException {
        int i;
        if ((this.j0 < this.k0 || F2()) && (i = this.X0[this.j0] & 255) >= 48 && i <= 57) {
            if (!J0(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS)) {
                i2("Leading zeroes not allowed");
            }
            this.j0++;
            if (i == 48) {
                do {
                    if (this.j0 >= this.k0 && !F2()) {
                        break;
                    }
                    byte[] bArr = this.X0;
                    int i2 = this.j0;
                    i = bArr[i2] & 255;
                    if (i < 48 || i > 57) {
                        return 48;
                    }
                    this.j0 = i2 + 1;
                } while (i == 48);
            }
            return i;
        }
        return 48;
    }

    @Override // com.fasterxml.jackson.core.JsonParser
    public JsonLocation q() {
        return new JsonLocation(this.h0.m(), this.l0 + this.j0, -1L, this.m0, (this.j0 - this.n0) + 1);
    }

    public final void q2(String str, int i, int i2) throws IOException {
        if (Character.isJavaIdentifierPart((char) s2(i2))) {
            W2(str.substring(0, i));
        }
    }

    public final void q3(int i) throws IOException {
        int i2 = this.j0 + 1;
        this.j0 = i2;
        if (i != 9) {
            if (i == 10) {
                this.m0++;
                this.n0 = i2;
            } else if (i == 13) {
                Z2();
            } else if (i != 32) {
                H1(i);
            }
        }
    }

    public final byte[] r2(Base64Variant base64Variant) throws IOException {
        ms T1 = T1();
        while (true) {
            if (this.j0 >= this.k0) {
                G2();
            }
            byte[] bArr = this.X0;
            int i = this.j0;
            this.j0 = i + 1;
            int i2 = bArr[i] & 255;
            if (i2 > 32) {
                int decodeBase64Char = base64Variant.decodeBase64Char(i2);
                if (decodeBase64Char < 0) {
                    if (i2 == 34) {
                        return T1.n();
                    }
                    decodeBase64Char = Q1(base64Variant, i2, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr2 = this.X0;
                int i3 = this.j0;
                this.j0 = i3 + 1;
                int i4 = bArr2[i3] & 255;
                int decodeBase64Char2 = base64Variant.decodeBase64Char(i4);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = Q1(base64Variant, i4, 1);
                }
                int i5 = (decodeBase64Char << 6) | decodeBase64Char2;
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr3 = this.X0;
                int i6 = this.j0;
                this.j0 = i6 + 1;
                int i7 = bArr3[i6] & 255;
                int decodeBase64Char3 = base64Variant.decodeBase64Char(i7);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (i7 == 34 && !base64Variant.usesPadding()) {
                            T1.b(i5 >> 4);
                            return T1.n();
                        }
                        decodeBase64Char3 = Q1(base64Variant, i7, 2);
                    }
                    if (decodeBase64Char3 == -2) {
                        if (this.j0 >= this.k0) {
                            G2();
                        }
                        byte[] bArr4 = this.X0;
                        int i8 = this.j0;
                        this.j0 = i8 + 1;
                        int i9 = bArr4[i8] & 255;
                        if (base64Variant.usesPaddingChar(i9)) {
                            T1.b(i5 >> 4);
                        } else {
                            throw h2(base64Variant, i9, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                    }
                }
                int i10 = (i5 << 6) | decodeBase64Char3;
                if (this.j0 >= this.k0) {
                    G2();
                }
                byte[] bArr5 = this.X0;
                int i11 = this.j0;
                this.j0 = i11 + 1;
                int i12 = bArr5[i11] & 255;
                int decodeBase64Char4 = base64Variant.decodeBase64Char(i12);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (i12 == 34 && !base64Variant.usesPadding()) {
                            T1.d(i10 >> 2);
                            return T1.n();
                        }
                        decodeBase64Char4 = Q1(base64Variant, i12, 3);
                    }
                    if (decodeBase64Char4 == -2) {
                        T1.d(i10 >> 2);
                    }
                }
                T1.c((i10 << 6) | decodeBase64Char4);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x00c2  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final java.lang.String r3(int[] r17, int r18, int r19) throws com.fasterxml.jackson.core.JsonParseException {
        /*
            Method dump skipped, instructions count: 265
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.r3(int[], int, int):java.lang.String");
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public int s2(int r7) throws java.io.IOException {
        /*
            r6 = this;
            r7 = r7 & 255(0xff, float:3.57E-43)
            r0 = 127(0x7f, float:1.78E-43)
            if (r7 <= r0) goto L68
            r0 = r7 & 224(0xe0, float:3.14E-43)
            r1 = 2
            r2 = 1
            r3 = 192(0xc0, float:2.69E-43)
            if (r0 != r3) goto L12
            r7 = r7 & 31
        L10:
            r0 = r2
            goto L2c
        L12:
            r0 = r7 & 240(0xf0, float:3.36E-43)
            r3 = 224(0xe0, float:3.14E-43)
            if (r0 != r3) goto L1c
            r7 = r7 & 15
            r0 = r1
            goto L2c
        L1c:
            r0 = r7 & 248(0xf8, float:3.48E-43)
            r3 = 240(0xf0, float:3.36E-43)
            if (r0 != r3) goto L26
            r7 = r7 & 7
            r0 = 3
            goto L2c
        L26:
            r0 = r7 & 255(0xff, float:3.57E-43)
            r6.T2(r0)
            goto L10
        L2c:
            int r3 = r6.x3()
            r4 = r3 & 192(0xc0, float:2.69E-43)
            r5 = 128(0x80, float:1.8E-43)
            if (r4 == r5) goto L3b
            r4 = r3 & 255(0xff, float:3.57E-43)
            r6.U2(r4)
        L3b:
            int r7 = r7 << 6
            r3 = r3 & 63
            r7 = r7 | r3
            if (r0 <= r2) goto L68
            int r2 = r6.x3()
            r3 = r2 & 192(0xc0, float:2.69E-43)
            if (r3 == r5) goto L4f
            r3 = r2 & 255(0xff, float:3.57E-43)
            r6.U2(r3)
        L4f:
            int r7 = r7 << 6
            r2 = r2 & 63
            r7 = r7 | r2
            if (r0 <= r1) goto L68
            int r0 = r6.x3()
            r1 = r0 & 192(0xc0, float:2.69E-43)
            if (r1 == r5) goto L63
            r1 = r0 & 255(0xff, float:3.57E-43)
            r6.U2(r1)
        L63:
            int r7 = r7 << 6
            r0 = r0 & 63
            r7 = r7 | r0
        L68:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ke4.s2(int):int");
    }

    public final String s3(int i, int i2) throws JsonParseException {
        int y3 = y3(i, i2);
        String C = this.P0.C(y3);
        if (C != null) {
            return C;
        }
        int[] iArr = this.Q0;
        iArr[0] = y3;
        return r3(iArr, 1, i2);
    }

    public final int t2(int i) throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i2 = this.j0;
        int i3 = i2 + 1;
        this.j0 = i3;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            V2(b & 255, i3);
        }
        return ((i & 31) << 6) | (b & 63);
    }

    public final String t3(int i, int i2, int i3) throws JsonParseException {
        int y3 = y3(i2, i3);
        String D = this.P0.D(i, y3);
        if (D != null) {
            return D;
        }
        int[] iArr = this.Q0;
        iArr[0] = i;
        iArr[1] = y3;
        return r3(iArr, 2, i3);
    }

    public final int u2(int i) throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        int i2 = i & 15;
        byte[] bArr = this.X0;
        int i3 = this.j0;
        int i4 = i3 + 1;
        this.j0 = i4;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            V2(b & 255, i4);
        }
        int i5 = (i2 << 6) | (b & 63);
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr2 = this.X0;
        int i6 = this.j0;
        int i7 = i6 + 1;
        this.j0 = i7;
        byte b2 = bArr2[i6];
        if ((b2 & 192) != 128) {
            V2(b2 & 255, i7);
        }
        return (i5 << 6) | (b2 & 63);
    }

    public final String u3(int i, int i2, int i3, int i4) throws JsonParseException {
        int y3 = y3(i3, i4);
        String E = this.P0.E(i, i2, y3);
        if (E != null) {
            return E;
        }
        int[] iArr = this.Q0;
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = y3(y3, i4);
        return r3(iArr, 3, i4);
    }

    public final int v2(int i) throws IOException {
        int i2 = i & 15;
        byte[] bArr = this.X0;
        int i3 = this.j0;
        int i4 = i3 + 1;
        this.j0 = i4;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            V2(b & 255, i4);
        }
        int i5 = (i2 << 6) | (b & 63);
        byte[] bArr2 = this.X0;
        int i6 = this.j0;
        int i7 = i6 + 1;
        this.j0 = i7;
        byte b2 = bArr2[i6];
        if ((b2 & 192) != 128) {
            V2(b2 & 255, i7);
        }
        return (i5 << 6) | (b2 & 63);
    }

    public final String v3(int[] iArr, int i, int i2, int i3) throws JsonParseException {
        if (i >= iArr.length) {
            iArr = w3(iArr, iArr.length);
            this.Q0 = iArr;
        }
        int i4 = i + 1;
        iArr[i] = y3(i2, i3);
        String F = this.P0.F(iArr, i4);
        return F == null ? r3(iArr, i4, i3) : F;
    }

    public final int w2(int i) throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i2 = this.j0;
        int i3 = i2 + 1;
        this.j0 = i3;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            V2(b & 255, i3);
        }
        int i4 = ((i & 7) << 6) | (b & 63);
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr2 = this.X0;
        int i5 = this.j0;
        int i6 = i5 + 1;
        this.j0 = i6;
        byte b2 = bArr2[i5];
        if ((b2 & 192) != 128) {
            V2(b2 & 255, i6);
        }
        int i7 = (i4 << 6) | (b2 & 63);
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr3 = this.X0;
        int i8 = this.j0;
        int i9 = i8 + 1;
        this.j0 = i9;
        byte b3 = bArr3[i8];
        if ((b3 & 192) != 128) {
            V2(b3 & 255, i9);
        }
        return ((i7 << 6) | (b3 & 63)) - 65536;
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public String x0() throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                return x2();
            }
            return this.t0.j();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        } else {
            return super.y0(null);
        }
    }

    public String x2() throws IOException {
        int i = this.j0;
        if (i >= this.k0) {
            G2();
            i = this.j0;
        }
        int i2 = 0;
        char[] k = this.t0.k();
        int[] iArr = Z0;
        int min = Math.min(this.k0, k.length + i);
        byte[] bArr = this.X0;
        while (true) {
            if (i >= min) {
                break;
            }
            int i3 = bArr[i] & 255;
            if (iArr[i3] == 0) {
                i++;
                k[i2] = (char) i3;
                i2++;
            } else if (i3 == 34) {
                this.j0 = i + 1;
                return this.t0.y(i2);
            }
        }
        this.j0 = i;
        z2(k, i2);
        return this.t0.j();
    }

    public final int x3() throws IOException {
        if (this.j0 >= this.k0) {
            G2();
        }
        byte[] bArr = this.X0;
        int i = this.j0;
        this.j0 = i + 1;
        return bArr[i] & 255;
    }

    @Override // defpackage.rp2, com.fasterxml.jackson.core.JsonParser
    public String y0(String str) throws IOException {
        JsonToken jsonToken = this.g0;
        if (jsonToken == JsonToken.VALUE_STRING) {
            if (this.R0) {
                this.R0 = false;
                return x2();
            }
            return this.t0.j();
        } else if (jsonToken == JsonToken.FIELD_NAME) {
            return r();
        } else {
            return super.y0(str);
        }
    }

    public void y2() throws IOException {
        int i = this.j0;
        if (i >= this.k0) {
            G2();
            i = this.j0;
        }
        int i2 = 0;
        char[] k = this.t0.k();
        int[] iArr = Z0;
        int min = Math.min(this.k0, k.length + i);
        byte[] bArr = this.X0;
        while (true) {
            if (i >= min) {
                break;
            }
            int i3 = bArr[i] & 255;
            if (iArr[i3] == 0) {
                i++;
                k[i2] = (char) i3;
                i2++;
            } else if (i3 == 34) {
                this.j0 = i + 1;
                this.t0.z(i2);
                return;
            }
        }
        this.j0 = i;
        z2(k, i2);
    }

    public final void z2(char[] cArr, int i) throws IOException {
        int[] iArr = Z0;
        byte[] bArr = this.X0;
        while (true) {
            int i2 = this.j0;
            if (i2 >= this.k0) {
                G2();
                i2 = this.j0;
            }
            int i3 = 0;
            if (i >= cArr.length) {
                cArr = this.t0.n();
                i = 0;
            }
            int min = Math.min(this.k0, (cArr.length - i) + i2);
            while (true) {
                if (i2 < min) {
                    int i4 = i2 + 1;
                    int i5 = bArr[i2] & 255;
                    if (iArr[i5] != 0) {
                        this.j0 = i4;
                        if (i5 == 34) {
                            this.t0.z(i);
                            return;
                        }
                        int i6 = iArr[i5];
                        if (i6 == 1) {
                            i5 = R1();
                        } else if (i6 == 2) {
                            i5 = t2(i5);
                        } else if (i6 == 3) {
                            i5 = this.k0 - i4 >= 2 ? v2(i5) : u2(i5);
                        } else if (i6 == 4) {
                            int w2 = w2(i5);
                            int i7 = i + 1;
                            cArr[i] = (char) (55296 | (w2 >> 10));
                            if (i7 >= cArr.length) {
                                cArr = this.t0.n();
                                i = 0;
                            } else {
                                i = i7;
                            }
                            i5 = (w2 & 1023) | 56320;
                        } else if (i5 < 32) {
                            L1(i5, "string value");
                        } else {
                            S2(i5);
                        }
                        if (i >= cArr.length) {
                            cArr = this.t0.n();
                        } else {
                            i3 = i;
                        }
                        i = i3 + 1;
                        cArr[i3] = (char) i5;
                    } else {
                        cArr[i] = (char) i5;
                        i2 = i4;
                        i++;
                    }
                } else {
                    this.j0 = i2;
                    break;
                }
            }
        }
    }

    public final String z3(int[] iArr, int i, int i2, int i3, int i4) throws IOException {
        int[] iArr2 = a1;
        while (true) {
            if (iArr2[i3] != 0) {
                if (i3 == 34) {
                    break;
                }
                if (i3 != 92) {
                    L1(i3, PublicResolver.FUNC_NAME);
                } else {
                    i3 = R1();
                }
                if (i3 > 127) {
                    int i5 = 0;
                    if (i4 >= 4) {
                        if (i >= iArr.length) {
                            iArr = w3(iArr, iArr.length);
                            this.Q0 = iArr;
                        }
                        iArr[i] = i2;
                        i++;
                        i2 = 0;
                        i4 = 0;
                    }
                    if (i3 < 2048) {
                        i2 = (i2 << 8) | (i3 >> 6) | 192;
                        i4++;
                    } else {
                        int i6 = (i2 << 8) | (i3 >> 12) | 224;
                        int i7 = i4 + 1;
                        if (i7 >= 4) {
                            if (i >= iArr.length) {
                                iArr = w3(iArr, iArr.length);
                                this.Q0 = iArr;
                            }
                            iArr[i] = i6;
                            i++;
                            i7 = 0;
                        } else {
                            i5 = i6;
                        }
                        i2 = (i5 << 8) | ((i3 >> 6) & 63) | 128;
                        i4 = i7 + 1;
                    }
                    i3 = (i3 & 63) | 128;
                }
            }
            if (i4 < 4) {
                i4++;
                i2 = (i2 << 8) | i3;
            } else {
                if (i >= iArr.length) {
                    iArr = w3(iArr, iArr.length);
                    this.Q0 = iArr;
                }
                iArr[i] = i2;
                i2 = i3;
                i++;
                i4 = 1;
            }
            if (this.j0 >= this.k0 && !F2()) {
                z1(" in field name", JsonToken.FIELD_NAME);
            }
            byte[] bArr = this.X0;
            int i8 = this.j0;
            this.j0 = i8 + 1;
            i3 = bArr[i8] & 255;
        }
        if (i4 > 0) {
            if (i >= iArr.length) {
                iArr = w3(iArr, iArr.length);
                this.Q0 = iArr;
            }
            iArr[i] = y3(i2, i4);
            i++;
        }
        String F = this.P0.F(iArr, i);
        return F == null ? r3(iArr, i, i4) : F;
    }
}
