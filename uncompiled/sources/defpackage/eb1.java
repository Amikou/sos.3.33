package defpackage;

import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentSwitchWalletBinding.java */
/* renamed from: eb1  reason: default package */
/* loaded from: classes2.dex */
public final class eb1 {
    public final ConstraintLayout a;
    public final AppCompatImageView b;
    public final ConstraintLayout c;
    public final RecyclerView d;
    public final SwipeRefreshLayout e;
    public final h74 f;
    public final TextView g;

    public eb1(ConstraintLayout constraintLayout, AppCompatImageView appCompatImageView, ConstraintLayout constraintLayout2, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, h74 h74Var, TextView textView) {
        this.a = constraintLayout;
        this.b = appCompatImageView;
        this.c = constraintLayout2;
        this.d = recyclerView;
        this.e = swipeRefreshLayout;
        this.f = h74Var;
        this.g = textView;
    }

    public static eb1 a(View view) {
        int i = R.id.imWalletsLink;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imWalletsLink);
        if (appCompatImageView != null) {
            i = R.id.layoutLinkAllWallets;
            ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.layoutLinkAllWallets);
            if (constraintLayout != null) {
                i = R.id.rvWallets;
                RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rvWallets);
                if (recyclerView != null) {
                    i = R.id.swipeRefreshLayout;
                    SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swipeRefreshLayout);
                    if (swipeRefreshLayout != null) {
                        i = R.id.toolbarParent;
                        View a = ai4.a(view, R.id.toolbarParent);
                        if (a != null) {
                            h74 a2 = h74.a(a);
                            i = R.id.txtWalletsLink;
                            TextView textView = (TextView) ai4.a(view, R.id.txtWalletsLink);
                            if (textView != null) {
                                return new eb1((ConstraintLayout) view, appCompatImageView, constraintLayout, recyclerView, swipeRefreshLayout, a2, textView);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
