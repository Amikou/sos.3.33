package defpackage;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: SharedPrefs.java */
/* renamed from: bo3  reason: default package */
/* loaded from: classes2.dex */
public class bo3 {
    public static String a = "safemoon_crypto";

    public static void a(Context context) {
        h(context).edit().clear().commit();
    }

    public static Boolean b(Context context, String str, String str2) {
        return Boolean.valueOf(h(context).edit().putString(str, str2).commit());
    }

    public static boolean c(Context context, String str) {
        return h(context).contains(str);
    }

    public static boolean d(Context context, String str) {
        return h(context).getBoolean(str, false);
    }

    public static boolean e(Context context, String str, boolean z) {
        return h(context).getBoolean(str, z);
    }

    public static int f(Context context, String str, int i) {
        return h(context).getInt(str, i);
    }

    public static long g(Context context, String str) {
        return h(context).getLong(str, 0L);
    }

    public static SharedPreferences h(Context context) {
        return context.getSharedPreferences(a, 0);
    }

    public static String i(Context context, String str) {
        return h(context).getString(str, "");
    }

    public static String j(Context context, String str, String str2) {
        return h(context).getString(str, str2);
    }

    public static void k(Context context, String str) {
        h(context).edit().remove(str).apply();
    }

    public static void l(Context context, String str, int i) {
        h(context).edit().putInt(str, i).apply();
    }

    public static void m(Context context, String str, long j) {
        h(context).edit().putLong(str, j).apply();
    }

    public static void n(Context context, String str, Boolean bool) {
        h(context).edit().putBoolean(str, bool.booleanValue()).apply();
    }

    public static void o(Context context, String str, String str2) {
        h(context).edit().putString(str, str2).apply();
    }

    public static void p(Context context, String str, boolean z) {
        h(context).edit().putBoolean(str, z).apply();
    }
}
