package defpackage;

import com.google.android.gms.internal.clearcut.o;
import java.util.Map;

/* renamed from: ac5  reason: default package */
/* loaded from: classes.dex */
public final class ac5<K> implements Map.Entry<K, Object> {
    public Map.Entry<K, wb5> a;

    public ac5(Map.Entry<K, wb5> entry) {
        this.a = entry;
    }

    public final wb5 a() {
        return this.a.getValue();
    }

    @Override // java.util.Map.Entry
    public final K getKey() {
        return this.a.getKey();
    }

    @Override // java.util.Map.Entry
    public final Object getValue() {
        if (this.a.getValue() == null) {
            return null;
        }
        return wb5.e();
    }

    @Override // java.util.Map.Entry
    public final Object setValue(Object obj) {
        if (obj instanceof o) {
            return this.a.getValue().c((o) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
