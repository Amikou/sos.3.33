package defpackage;

import java.util.ArrayList;
import java.util.List;

/* compiled from: LoggerHelper.java */
/* renamed from: y12  reason: default package */
/* loaded from: classes2.dex */
public class y12 {
    public static String a(String str) {
        return ru3.d(str) ? "Zendesk" : str.length() > 23 ? str.substring(0, 23) : str;
    }

    public static char b(int i) {
        if (i != 2) {
            if (i != 3) {
                if (i != 5) {
                    if (i != 6) {
                        return i != 7 ? 'I' : 'A';
                    }
                    return 'E';
                }
                return 'W';
            }
            return 'D';
        }
        return 'V';
    }

    public static List<String> c(String str, int i) {
        int min;
        ArrayList arrayList = new ArrayList();
        if (i < 1) {
            if (!ru3.b(str)) {
                arrayList.add("");
                return arrayList;
            }
            arrayList.add(str);
            return arrayList;
        } else if (!ru3.b(str)) {
            arrayList.add("");
            return arrayList;
        } else if (str.length() < i) {
            arrayList.add(str);
            return arrayList;
        } else {
            int i2 = 0;
            int length = str.length();
            while (i2 < length) {
                int indexOf = str.indexOf(ru3.b, i2);
                if (indexOf == -1) {
                    indexOf = length;
                }
                while (true) {
                    min = Math.min(indexOf, i2 + i);
                    arrayList.add(str.substring(i2, min));
                    if (min >= indexOf) {
                        break;
                    }
                    i2 = min;
                }
                i2 = min + 1;
            }
            return arrayList;
        }
    }
}
