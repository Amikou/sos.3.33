package defpackage;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.a;

/* compiled from: HoneycombBitmapCreator.java */
/* renamed from: bl1  reason: default package */
/* loaded from: classes.dex */
public class bl1 implements tp {
    public final ru0 a;
    public final y61 b;

    public bl1(xs2 xs2Var) {
        this.b = xs2Var.d();
        this.a = new ru0(xs2Var.h());
    }

    public static BitmapFactory.Options b(int i, Bitmap.Config config) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = true;
        options.inPreferredConfig = config;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inSampleSize = i;
        if (Build.VERSION.SDK_INT >= 11) {
            options.inMutable = true;
        }
        return options;
    }

    @Override // defpackage.tp
    @TargetApi(12)
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        zu0 zu0Var;
        a<PooledByteBuffer> a = this.a.a((short) i, (short) i2);
        a<byte[]> aVar = null;
        try {
            zu0Var = new zu0(a);
        } catch (Throwable th) {
            th = th;
            zu0Var = null;
        }
        try {
            zu0Var.a0(wj0.a);
            BitmapFactory.Options b = b(zu0Var.r(), config);
            int size = a.j().size();
            aVar = this.b.a(size + 2);
            byte[] j = aVar.j();
            a.j().s(0, j, 0, size);
            Bitmap bitmap = (Bitmap) xt2.g(BitmapFactory.decodeByteArray(j, 0, size, b));
            bitmap.setHasAlpha(true);
            bitmap.eraseColor(0);
            a.g(aVar);
            zu0.c(zu0Var);
            a.g(a);
            return bitmap;
        } catch (Throwable th2) {
            th = th2;
            a.g(aVar);
            zu0.c(zu0Var);
            a.g(a);
            throw th;
        }
    }
}
