package defpackage;

import java.util.concurrent.Executor;

/* compiled from: ArchTaskExecutor.java */
/* renamed from: gh  reason: default package */
/* loaded from: classes.dex */
public class gh extends r34 {
    public static volatile gh c;
    public static final Executor d;
    public r34 a;
    public r34 b;

    /* compiled from: ArchTaskExecutor.java */
    /* renamed from: gh$a */
    /* loaded from: classes.dex */
    public static class a implements Executor {
        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            gh.f().d(runnable);
        }
    }

    /* compiled from: ArchTaskExecutor.java */
    /* renamed from: gh$b */
    /* loaded from: classes.dex */
    public static class b implements Executor {
        @Override // java.util.concurrent.Executor
        public void execute(Runnable runnable) {
            gh.f().a(runnable);
        }
    }

    static {
        new a();
        d = new b();
    }

    public gh() {
        wk0 wk0Var = new wk0();
        this.b = wk0Var;
        this.a = wk0Var;
    }

    public static Executor e() {
        return d;
    }

    public static gh f() {
        if (c != null) {
            return c;
        }
        synchronized (gh.class) {
            if (c == null) {
                c = new gh();
            }
        }
        return c;
    }

    @Override // defpackage.r34
    public void a(Runnable runnable) {
        this.a.a(runnable);
    }

    @Override // defpackage.r34
    public boolean c() {
        return this.a.c();
    }

    @Override // defpackage.r34
    public void d(Runnable runnable) {
        this.a.d(runnable);
    }
}
