package defpackage;

import androidx.media3.common.u;
import androidx.media3.exoplayer.source.j;
import defpackage.tb;

/* compiled from: PlaybackSessionManager.java */
/* renamed from: mr2  reason: default package */
/* loaded from: classes.dex */
public interface mr2 {

    /* compiled from: PlaybackSessionManager.java */
    /* renamed from: mr2$a */
    /* loaded from: classes.dex */
    public interface a {
        void F(tb.a aVar, String str);

        void V(tb.a aVar, String str, String str2);

        void g(tb.a aVar, String str);

        void n(tb.a aVar, String str, boolean z);
    }

    String a();

    String b(u uVar, j.b bVar);

    void c(tb.a aVar, int i);

    void d(tb.a aVar);

    void e(tb.a aVar);

    void f(tb.a aVar);

    void g(a aVar);
}
