package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: lu5  reason: default package */
/* loaded from: classes.dex */
public final class lu5 {
    public long a;
    public long b;
    public final n55 c;
    public final /* synthetic */ qu5 d;

    public lu5(qu5 qu5Var) {
        this.d = qu5Var;
        this.c = new iu5(this, qu5Var.a);
        long b = qu5Var.a.a().b();
        this.a = b;
        this.b = b;
    }

    public final void a(long j) {
        this.d.e();
        this.c.d();
        this.a = j;
        this.b = j;
    }

    public final void b(long j) {
        this.c.d();
    }

    public final void c() {
        this.c.d();
        this.a = 0L;
        this.b = 0L;
    }

    public final boolean d(boolean z, boolean z2, long j) {
        this.d.e();
        this.d.g();
        q16.a();
        if (this.d.a.z().v(null, qf5.o0)) {
            if (this.d.a.h()) {
                this.d.a.A().o.b(this.d.a.a().a());
            }
        } else {
            this.d.a.A().o.b(this.d.a.a().a());
        }
        long j2 = j - this.a;
        if (!z && j2 < 1000) {
            this.d.a.w().v().b("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
            return false;
        }
        if (!z2) {
            j2 = j - this.b;
            this.b = j;
        }
        this.d.a.w().v().b("Recording user engagement, ms", Long.valueOf(j2));
        Bundle bundle = new Bundle();
        bundle.putLong("_et", j2);
        qq5.x(this.d.a.Q().r(!this.d.a.z().C()), bundle, true);
        q45 z3 = this.d.a.z();
        we5<Boolean> we5Var = qf5.U;
        if (!z3.v(null, we5Var) && z2) {
            bundle.putLong("_fr", 1L);
        }
        if (!this.d.a.z().v(null, we5Var) || !z2) {
            this.d.a.F().X("auto", "_e", bundle);
        }
        this.a = j;
        this.c.d();
        this.c.b(3600000L);
        return true;
    }
}
