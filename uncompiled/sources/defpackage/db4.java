package defpackage;

import java.util.Iterator;

/* compiled from: TransformedIterator.java */
/* renamed from: db4  reason: default package */
/* loaded from: classes2.dex */
public abstract class db4<F, T> implements Iterator<T> {
    public final Iterator<? extends F> a;

    public db4(Iterator<? extends F> it) {
        this.a = (Iterator) au2.k(it);
    }

    public abstract T a(F f);

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @Override // java.util.Iterator
    public final T next() {
        return a(this.a.next());
    }

    @Override // java.util.Iterator
    public final void remove() {
        this.a.remove();
    }
}
