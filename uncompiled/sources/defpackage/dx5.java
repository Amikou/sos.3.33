package defpackage;

import android.util.Log;
import com.google.android.gms.common.util.a;
import java.security.MessageDigest;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: dx5  reason: default package */
/* loaded from: classes.dex */
public class dx5 {
    public static final dx5 d = new dx5(true, null, null);
    public final boolean a;
    public final String b;
    public final Throwable c;

    public dx5(boolean z, String str, Throwable th) {
        this.a = z;
        this.b = str;
        this.c = th;
    }

    public static String a(String str, vc5 vc5Var, boolean z, boolean z2) {
        String str2 = z2 ? "debug cert rejected" : "not allowed";
        StringBuilder sb = new StringBuilder(14);
        sb.append("12451009.false");
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", str2, str, qk1.a(((MessageDigest) zt2.j(a.b("SHA-1"))).digest(vc5Var.G1())), Boolean.valueOf(z), sb.toString());
    }

    public static dx5 b() {
        return d;
    }

    public static dx5 c(String str) {
        return new dx5(false, str, null);
    }

    public static dx5 d(String str, Throwable th) {
        return new dx5(false, str, th);
    }

    public static dx5 e(Callable<String> callable) {
        return new sz5(callable);
    }

    public String f() {
        return this.b;
    }

    public final void g() {
        if (this.a || !Log.isLoggable("GoogleCertificatesRslt", 3)) {
            return;
        }
        if (this.c != null) {
            f();
        } else {
            f();
        }
    }
}
