package defpackage;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.d;
import defpackage.j92;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: ByteBufferFileLoader.java */
/* renamed from: qs  reason: default package */
/* loaded from: classes.dex */
public class qs implements j92<File, ByteBuffer> {

    /* compiled from: ByteBufferFileLoader.java */
    /* renamed from: qs$a */
    /* loaded from: classes.dex */
    public static final class a implements d<ByteBuffer> {
        public final File a;

        public a(File file) {
            this.a = file;
        }

        @Override // com.bumptech.glide.load.data.d
        public Class<ByteBuffer> a() {
            return ByteBuffer.class;
        }

        @Override // com.bumptech.glide.load.data.d
        public void b() {
        }

        @Override // com.bumptech.glide.load.data.d
        public void cancel() {
        }

        @Override // com.bumptech.glide.load.data.d
        public DataSource d() {
            return DataSource.LOCAL;
        }

        @Override // com.bumptech.glide.load.data.d
        public void e(Priority priority, d.a<? super ByteBuffer> aVar) {
            try {
                aVar.f(ts.a(this.a));
            } catch (IOException e) {
                aVar.c(e);
            }
        }
    }

    /* compiled from: ByteBufferFileLoader.java */
    /* renamed from: qs$b */
    /* loaded from: classes.dex */
    public static class b implements k92<File, ByteBuffer> {
        @Override // defpackage.k92
        public void a() {
        }

        @Override // defpackage.k92
        public j92<File, ByteBuffer> c(qa2 qa2Var) {
            return new qs();
        }
    }

    @Override // defpackage.j92
    /* renamed from: c */
    public j92.a<ByteBuffer> b(File file, int i, int i2, vn2 vn2Var) {
        return new j92.a<>(new ll2(file), new a(file));
    }

    @Override // defpackage.j92
    /* renamed from: d */
    public boolean a(File file) {
        return true;
    }
}
