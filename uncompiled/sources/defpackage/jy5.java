package defpackage;

import com.google.android.gms.internal.measurement.c2;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: jy5  reason: default package */
/* loaded from: classes.dex */
public final class jy5 {
    public static final jy5 c = new jy5();
    public final ConcurrentMap<Class<?>, c2<?>> b = new ConcurrentHashMap();
    public final py5 a = new lx5();

    public static jy5 a() {
        return c;
    }

    public final <T> c2<T> b(Class<T> cls) {
        cw5.b(cls, "messageType");
        c2<T> c2Var = (c2<T>) this.b.get(cls);
        if (c2Var == null) {
            c2Var = this.a.a(cls);
            cw5.b(cls, "messageType");
            cw5.b(c2Var, "schema");
            c2 putIfAbsent = this.b.putIfAbsent(cls, c2Var);
            if (putIfAbsent != null) {
                return putIfAbsent;
            }
        }
        return c2Var;
    }
}
