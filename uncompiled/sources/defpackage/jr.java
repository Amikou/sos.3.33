package defpackage;

import android.os.Bundle;
import org.json.JSONException;
import org.json.JSONObject;
import org.web3j.ens.contracts.generated.PublicResolver;

/* compiled from: BreadcrumbAnalyticsEventReceiver.java */
/* renamed from: jr  reason: default package */
/* loaded from: classes2.dex */
public class jr implements rb, lr {
    public kr a;

    public static String b(String str, Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (String str2 : bundle.keySet()) {
            jSONObject2.put(str2, bundle.get(str2));
        }
        jSONObject.put(PublicResolver.FUNC_NAME, str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }

    @Override // defpackage.lr
    public void a(kr krVar) {
        this.a = krVar;
        w12.f().b("Registered Firebase Analytics event receiver for breadcrumbs");
    }

    @Override // defpackage.rb
    public void d(String str, Bundle bundle) {
        kr krVar = this.a;
        if (krVar != null) {
            try {
                krVar.a("$A$:" + b(str, bundle));
            } catch (JSONException unused) {
                w12.f().k("Unable to serialize Firebase Analytics event to breadcrumb.");
            }
        }
    }
}
