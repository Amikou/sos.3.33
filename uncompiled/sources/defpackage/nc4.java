package defpackage;

import androidx.media3.common.util.b;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: TtmlSubtitle.java */
/* renamed from: nc4  reason: default package */
/* loaded from: classes.dex */
public final class nc4 implements qv3 {
    public final jc4 a;
    public final long[] f0;
    public final Map<String, mc4> g0;
    public final Map<String, kc4> h0;
    public final Map<String, String> i0;

    public nc4(jc4 jc4Var, Map<String, mc4> map, Map<String, kc4> map2, Map<String, String> map3) {
        this.a = jc4Var;
        this.h0 = map2;
        this.i0 = map3;
        this.g0 = map != null ? Collections.unmodifiableMap(map) : Collections.emptyMap();
        this.f0 = jc4Var.j();
    }

    @Override // defpackage.qv3
    public int a(long j) {
        int e = b.e(this.f0, j, false, false);
        if (e < this.f0.length) {
            return e;
        }
        return -1;
    }

    @Override // defpackage.qv3
    public long d(int i) {
        return this.f0[i];
    }

    @Override // defpackage.qv3
    public List<kb0> e(long j) {
        return this.a.h(j, this.g0, this.h0, this.i0);
    }

    @Override // defpackage.qv3
    public int f() {
        return this.f0.length;
    }
}
