package defpackage;

import com.onesignal.n1;
import org.json.JSONException;

/* compiled from: UserStatePush.java */
/* renamed from: bg4  reason: default package */
/* loaded from: classes2.dex */
public class bg4 extends n1 {
    public bg4(String str, boolean z) {
        super(str, z);
    }

    public final int A() {
        int d = i().d("subscribableStatus", 1);
        if (d < -2) {
            return d;
        }
        if (i().c("androidPermission", true)) {
            return !i().c("userSubscribePref", true) ? -2 : 1;
        }
        return 0;
    }

    @Override // com.onesignal.n1
    public void a() {
        try {
            t("notification_types", Integer.valueOf(A()));
        } catch (JSONException unused) {
        }
    }

    @Override // com.onesignal.n1
    public n1 p(String str) {
        return new bg4(str, false);
    }
}
