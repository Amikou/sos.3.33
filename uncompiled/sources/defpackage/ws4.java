package defpackage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: ws4  reason: default package */
/* loaded from: classes2.dex */
public final class ws4 {
    public final Map<String, Object> a = new HashMap();
    public final AtomicBoolean b = new AtomicBoolean(false);

    public final synchronized boolean a() {
        if (!this.b.get()) {
            b();
        }
        Object obj = this.a.get("assetOnlyUpdates");
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue();
        }
        return false;
    }

    public final synchronized void b() {
        this.a.put("assetOnlyUpdates", Boolean.FALSE);
    }
}
