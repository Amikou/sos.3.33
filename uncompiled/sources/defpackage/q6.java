package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktChangePasswordBinding.java */
/* renamed from: q6  reason: default package */
/* loaded from: classes2.dex */
public final class q6 {
    public final ConstraintLayout a;
    public final AppCompatButton b;
    public final ConstraintLayout c;
    public final AppCompatEditText d;
    public final AppCompatEditText e;
    public final AppCompatEditText f;
    public final qy1 g;
    public final rp3 h;
    public final TextView i;

    public q6(ConstraintLayout constraintLayout, AppCompatButton appCompatButton, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, MaterialCardView materialCardView, MaterialCardView materialCardView2, AppCompatEditText appCompatEditText, AppCompatEditText appCompatEditText2, AppCompatEditText appCompatEditText3, qy1 qy1Var, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, rp3 rp3Var, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
        this.a = constraintLayout;
        this.b = appCompatButton;
        this.c = constraintLayout2;
        this.d = appCompatEditText;
        this.e = appCompatEditText2;
        this.f = appCompatEditText3;
        this.g = qy1Var;
        this.h = rp3Var;
        this.i = textView4;
    }

    public static q6 a(View view) {
        int i = R.id.btnSave;
        AppCompatButton appCompatButton = (AppCompatButton) ai4.a(view, R.id.btnSave);
        if (appCompatButton != null) {
            i = R.id.content_layout;
            ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.content_layout);
            if (constraintLayout != null) {
                ConstraintLayout constraintLayout2 = (ConstraintLayout) view;
                i = R.id.cvCurrentPassword;
                MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.cvCurrentPassword);
                if (materialCardView != null) {
                    i = R.id.cvNewPassword;
                    MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.cvNewPassword);
                    if (materialCardView2 != null) {
                        i = R.id.etConfirmNewPassword;
                        AppCompatEditText appCompatEditText = (AppCompatEditText) ai4.a(view, R.id.etConfirmNewPassword);
                        if (appCompatEditText != null) {
                            i = R.id.etNewPassword;
                            AppCompatEditText appCompatEditText2 = (AppCompatEditText) ai4.a(view, R.id.etNewPassword);
                            if (appCompatEditText2 != null) {
                                i = R.id.etUsername;
                                AppCompatEditText appCompatEditText3 = (AppCompatEditText) ai4.a(view, R.id.etUsername);
                                if (appCompatEditText3 != null) {
                                    i = R.id.lPasswordRequirements;
                                    View a = ai4.a(view, R.id.lPasswordRequirements);
                                    if (a != null) {
                                        qy1 a2 = qy1.a(a);
                                        i = R.id.tilConfirmNewPassword;
                                        TextInputLayout textInputLayout = (TextInputLayout) ai4.a(view, R.id.tilConfirmNewPassword);
                                        if (textInputLayout != null) {
                                            i = R.id.tilNewPassword;
                                            TextInputLayout textInputLayout2 = (TextInputLayout) ai4.a(view, R.id.tilNewPassword);
                                            if (textInputLayout2 != null) {
                                                i = R.id.tilUsername;
                                                TextInputLayout textInputLayout3 = (TextInputLayout) ai4.a(view, R.id.tilUsername);
                                                if (textInputLayout3 != null) {
                                                    i = R.id.toolbar;
                                                    View a3 = ai4.a(view, R.id.toolbar);
                                                    if (a3 != null) {
                                                        rp3 a4 = rp3.a(a3);
                                                        i = R.id.tvConfirmNewPassTitle;
                                                        TextView textView = (TextView) ai4.a(view, R.id.tvConfirmNewPassTitle);
                                                        if (textView != null) {
                                                            i = R.id.tvNewPassTitle;
                                                            TextView textView2 = (TextView) ai4.a(view, R.id.tvNewPassTitle);
                                                            if (textView2 != null) {
                                                                i = R.id.tvUsernameTitle;
                                                                TextView textView3 = (TextView) ai4.a(view, R.id.tvUsernameTitle);
                                                                if (textView3 != null) {
                                                                    i = R.id.txt_username_invalid;
                                                                    TextView textView4 = (TextView) ai4.a(view, R.id.txt_username_invalid);
                                                                    if (textView4 != null) {
                                                                        return new q6(constraintLayout2, appCompatButton, constraintLayout, constraintLayout2, materialCardView, materialCardView2, appCompatEditText, appCompatEditText2, appCompatEditText3, a2, textInputLayout, textInputLayout2, textInputLayout3, a4, textView, textView2, textView3, textView4);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static q6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static q6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_change_password, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
