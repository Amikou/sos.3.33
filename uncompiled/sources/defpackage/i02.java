package defpackage;

/* compiled from: ListUpdateCallback.java */
/* renamed from: i02  reason: default package */
/* loaded from: classes.dex */
public interface i02 {
    void onChanged(int i, int i2, Object obj);

    void onInserted(int i, int i2);

    void onMoved(int i, int i2);

    void onRemoved(int i, int i2);
}
