package defpackage;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: FirebaseOptions.java */
/* renamed from: y51  reason: default package */
/* loaded from: classes2.dex */
public final class y51 {
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;

    /* compiled from: FirebaseOptions.java */
    /* renamed from: y51$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;

        public y51 a() {
            return new y51(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
        }

        public b b(String str) {
            this.a = zt2.g(str, "ApiKey must be set.");
            return this;
        }

        public b c(String str) {
            this.b = zt2.g(str, "ApplicationId must be set.");
            return this;
        }

        public b d(String str) {
            this.e = str;
            return this;
        }

        public b e(String str) {
            this.g = str;
            return this;
        }
    }

    public static y51 a(Context context) {
        pu3 pu3Var = new pu3(context);
        String a2 = pu3Var.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new y51(a2, pu3Var.a("google_api_key"), pu3Var.a("firebase_database_url"), pu3Var.a("ga_trackingId"), pu3Var.a("gcm_defaultSenderId"), pu3Var.a("google_storage_bucket"), pu3Var.a("project_id"));
    }

    public String b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.e;
    }

    public String e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (obj instanceof y51) {
            y51 y51Var = (y51) obj;
            return pl2.a(this.b, y51Var.b) && pl2.a(this.a, y51Var.a) && pl2.a(this.c, y51Var.c) && pl2.a(this.d, y51Var.d) && pl2.a(this.e, y51Var.e) && pl2.a(this.f, y51Var.f) && pl2.a(this.g, y51Var.g);
        }
        return false;
    }

    public int hashCode() {
        return pl2.b(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }

    public String toString() {
        return pl2.c(this).a("applicationId", this.b).a("apiKey", this.a).a("databaseUrl", this.c).a("gcmSenderId", this.e).a("storageBucket", this.f).a("projectId", this.g).toString();
    }

    public y51(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        zt2.n(!vu3.a(str), "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }
}
