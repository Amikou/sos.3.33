package defpackage;

import com.google.android.gms.internal.measurement.zzga;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: mk5  reason: default package */
/* loaded from: classes.dex */
public final class mk5 implements sv5 {
    public static final sv5 a = new mk5();

    @Override // defpackage.sv5
    public final boolean d(int i) {
        return zzga.zza(i) != null;
    }
}
