package defpackage;

import android.os.Build;
import com.zendesk.logger.Logger;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

/* compiled from: LocaleUtil.java */
/* renamed from: h12  reason: default package */
/* loaded from: classes2.dex */
public class h12 {
    public static final String a = "h12";
    public static final List<String> b = Arrays.asList("he", "yi", "id");

    public static Locale a(String str, String str2) {
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                Constructor declaredConstructor = Locale.class.getDeclaredConstructor(Boolean.TYPE, String.class, String.class);
                declaredConstructor.setAccessible(true);
                return (Locale) declaredConstructor.newInstance(Boolean.TRUE, str, str2);
            }
            Constructor declaredConstructor2 = Locale.class.getDeclaredConstructor(new Class[0]);
            declaredConstructor2.setAccessible(true);
            Locale locale = (Locale) declaredConstructor2.newInstance(new Object[0]);
            Class<?> cls = locale.getClass();
            Field declaredField = cls.getDeclaredField("languageCode");
            declaredField.setAccessible(true);
            declaredField.set(locale, str);
            Field declaredField2 = cls.getDeclaredField("countryCode");
            declaredField2.setAccessible(true);
            declaredField2.set(locale, str2);
            return locale;
        } catch (Exception e) {
            Logger.d(a, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }

    public static Locale b(String str, String str2) {
        try {
            Method declaredMethod = Locale.class.getDeclaredMethod("createConstant", String.class, String.class);
            declaredMethod.setAccessible(true);
            return (Locale) declaredMethod.invoke(null, str, str2);
        } catch (Exception e) {
            Logger.d(a, "Unable to create ISO-6390-Alpha3 per reflection", e, new Object[0]);
            return null;
        }
    }

    public static Locale c(String str) {
        Locale locale;
        String str2 = a;
        Logger.b(str2, "Assuming Locale.getDefault()", new Object[0]);
        Locale locale2 = Locale.getDefault();
        if (ru3.b(str)) {
            StringTokenizer stringTokenizer = new StringTokenizer(str, "-");
            int countTokens = stringTokenizer.countTokens();
            if (countTokens == 1 || countTokens == 2) {
                if ((countTokens != 1 ? 5 : 2) != str.length()) {
                    Logger.b(str2, "number of tokens is correct but the length of the locale string does not match the expected length", new Object[0]);
                    return locale2;
                }
                String nextToken = stringTokenizer.nextToken();
                String upperCase = (stringTokenizer.hasMoreTokens() ? stringTokenizer.nextToken() : "").toUpperCase(Locale.US);
                if (b.contains(nextToken)) {
                    Logger.b(str2, "New ISO-6390-Alpha3 locale detected trying to create new locale per reflection", new Object[0]);
                    locale = b(nextToken, upperCase);
                    if (locale == null) {
                        locale = a(nextToken, upperCase);
                    }
                    if (locale == null) {
                        locale = new Locale(nextToken, upperCase);
                    }
                } else {
                    locale = new Locale(nextToken, upperCase);
                }
                return locale;
            }
            Logger.k(str2, "Unexpected number of tokens, must be at least one and at most two", new Object[0]);
            return locale2;
        }
        return locale2;
    }

    public static String d(Locale locale) {
        if (locale != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(locale.getLanguage());
            if (ru3.b(locale.getCountry())) {
                sb.append("-");
                sb.append(locale.getCountry().toLowerCase(Locale.US));
            }
            return sb.toString();
        }
        return null;
    }
}
