package defpackage;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: mq5  reason: default package */
/* loaded from: classes.dex */
public final class mq5 {
    public final ConcurrentHashMap<kq5, List<Throwable>> a = new ConcurrentHashMap<>(16, 0.75f, 10);
    public final ReferenceQueue<Throwable> b = new ReferenceQueue<>();

    public final List<Throwable> a(Throwable th, boolean z) {
        ReferenceQueue<Throwable> referenceQueue = this.b;
        while (true) {
            Reference<? extends Throwable> poll = referenceQueue.poll();
            if (poll == null) {
                break;
            }
            this.a.remove(poll);
            referenceQueue = this.b;
        }
        List<Throwable> list = this.a.get(new kq5(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.a.putIfAbsent(new kq5(th, this.b), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
