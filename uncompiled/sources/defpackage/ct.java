package defpackage;

/* compiled from: BytesRange.java */
/* renamed from: ct  reason: default package */
/* loaded from: classes.dex */
public class ct {
    public final int a;
    public final int b;

    public ct(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public static ct b(int i) {
        xt2.b(Boolean.valueOf(i >= 0));
        return new ct(i, Integer.MAX_VALUE);
    }

    public static ct c(int i) {
        xt2.b(Boolean.valueOf(i > 0));
        return new ct(0, i);
    }

    public static String d(int i) {
        return i == Integer.MAX_VALUE ? "" : Integer.toString(i);
    }

    public boolean a(ct ctVar) {
        return ctVar != null && this.a <= ctVar.a && this.b >= ctVar.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ct) {
            ct ctVar = (ct) obj;
            return this.a == ctVar.a && this.b == ctVar.b;
        }
        return false;
    }

    public int hashCode() {
        return ck1.a(this.a, this.b);
    }

    public String toString() {
        return String.format(null, "%s-%s", d(this.a), d(this.b));
    }
}
