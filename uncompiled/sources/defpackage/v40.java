package defpackage;

/* compiled from: ConfigFeature.java */
/* renamed from: v40  reason: default package */
/* loaded from: classes.dex */
public interface v40 {
    boolean enabledByDefault();

    int getMask();
}
