package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import net.safemoon.androidwallet.R;

/* compiled from: ToolbarWithTitleAndActionBinding.java */
/* renamed from: g74  reason: default package */
/* loaded from: classes2.dex */
public final class g74 {
    public final AppCompatImageView a;
    public final AppCompatImageView b;
    public final ImageView c;
    public final AppCompatImageView d;
    public final TextView e;

    public g74(Toolbar toolbar, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, ImageView imageView, AppCompatImageView appCompatImageView3, TextView textView) {
        this.a = appCompatImageView;
        this.b = appCompatImageView2;
        this.c = imageView;
        this.d = appCompatImageView3;
        this.e = textView;
    }

    public static g74 a(View view) {
        int i = R.id.imgInfo;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.imgInfo);
        if (appCompatImageView != null) {
            i = R.id.ivToolbarAction;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.ivToolbarAction);
            if (appCompatImageView2 != null) {
                i = R.id.ivToolbarBack;
                ImageView imageView = (ImageView) ai4.a(view, R.id.ivToolbarBack);
                if (imageView != null) {
                    i = R.id.ivToolbarRefresh;
                    AppCompatImageView appCompatImageView3 = (AppCompatImageView) ai4.a(view, R.id.ivToolbarRefresh);
                    if (appCompatImageView3 != null) {
                        i = R.id.tvToolbarTitle;
                        TextView textView = (TextView) ai4.a(view, R.id.tvToolbarTitle);
                        if (textView != null) {
                            return new g74((Toolbar) view, appCompatImageView, appCompatImageView2, imageView, appCompatImageView3, textView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
