package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.extractor.metadata.id3.a;
import java.io.EOFException;
import java.io.IOException;

/* compiled from: Id3Peeker.java */
/* renamed from: in1  reason: default package */
/* loaded from: classes.dex */
public final class in1 {
    public final op2 a = new op2(10);

    public Metadata a(q11 q11Var, a.InterfaceC0044a interfaceC0044a) throws IOException {
        Metadata metadata = null;
        int i = 0;
        while (true) {
            try {
                q11Var.n(this.a.d(), 0, 10);
                this.a.P(0);
                if (this.a.G() != 4801587) {
                    break;
                }
                this.a.Q(3);
                int C = this.a.C();
                int i2 = C + 10;
                if (metadata == null) {
                    byte[] bArr = new byte[i2];
                    System.arraycopy(this.a.d(), 0, bArr, 0, 10);
                    q11Var.n(bArr, 10, C);
                    metadata = new a(interfaceC0044a).e(bArr, i2);
                } else {
                    q11Var.f(C);
                }
                i += i2;
            } catch (EOFException unused) {
            }
        }
        q11Var.j();
        q11Var.f(i);
        return metadata;
    }
}
