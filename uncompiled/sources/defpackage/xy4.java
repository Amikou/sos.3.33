package defpackage;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.play.core.internal.n;
import com.google.android.play.core.internal.o;
import com.google.android.play.core.internal.p;

/* renamed from: xy4  reason: default package */
/* loaded from: classes2.dex */
public abstract class xy4 extends n implements p {
    public static p F1(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.play.core.assetpacks.protocol.IAssetModuleService");
        return queryLocalInterface instanceof p ? (p) queryLocalInterface : new o(iBinder);
    }
}
