package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import java.util.Locale;

/* compiled from: PicassoTransformations.java */
/* renamed from: tq2  reason: default package */
/* loaded from: classes3.dex */
public class tq2 {

    /* compiled from: PicassoTransformations.java */
    /* renamed from: tq2$a */
    /* loaded from: classes3.dex */
    public static class a implements ya4 {
        public final int a;
        public final int b;
        public final int c;

        public a(int i, int i2, int i3) {
            this.a = i;
            this.c = i2;
            this.b = i3;
        }

        public final RectF a(int i, int i2, int i3) {
            float f = i3;
            return new RectF(f, f, i - i3, i2 - i3);
        }

        public final Paint b(Bitmap bitmap) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            paint.setShader(new BitmapShader(bitmap, tileMode, tileMode));
            return paint;
        }

        public final Paint c() {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(this.c);
            return paint;
        }

        @Override // defpackage.ya4
        public String key() {
            return String.format(Locale.US, "rounded-%s-%s-%s", Integer.valueOf(this.a), Integer.valueOf(this.c), Integer.valueOf(this.b));
        }

        @Override // defpackage.ya4
        public Bitmap transform(Bitmap bitmap) {
            if (this.b > 0) {
                Canvas canvas = new Canvas(bitmap);
                Paint c = c();
                Path path = new Path();
                path.setFillType(Path.FillType.INVERSE_EVEN_ODD);
                RectF a = a(bitmap.getWidth(), bitmap.getHeight(), this.b);
                int i = this.a;
                path.addRoundRect(a, i, i, Path.Direction.CW);
                canvas.drawPath(path, c);
            }
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap);
            Paint b = b(bitmap);
            RectF a2 = a(bitmap.getWidth(), bitmap.getHeight(), 0);
            int i2 = this.a;
            canvas2.drawRoundRect(a2, i2, i2, b);
            if (bitmap != createBitmap) {
                bitmap.recycle();
            }
            return createBitmap;
        }
    }

    public static ya4 a(int i, int i2, int i3) {
        return new a(i, i2, i3);
    }
}
