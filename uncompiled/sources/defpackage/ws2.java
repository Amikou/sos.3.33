package defpackage;

/* compiled from: PoolConfig.java */
/* renamed from: ws2  reason: default package */
/* loaded from: classes.dex */
public class ws2 {
    public final ys2 a;
    public final zs2 b;
    public final ys2 c;
    public final r72 d;
    public final ys2 e;
    public final zs2 f;
    public final ys2 g;
    public final zs2 h;
    public final String i;
    public final int j;
    public final int k;
    public final boolean l;
    public final boolean m;

    /* compiled from: PoolConfig.java */
    /* renamed from: ws2$b */
    /* loaded from: classes.dex */
    public static class b {
        public ys2 a;
        public zs2 b;
        public ys2 c;
        public r72 d;
        public ys2 e;
        public zs2 f;
        public ys2 g;
        public zs2 h;
        public String i;
        public int j;
        public int k;
        public boolean l;
        public boolean m;

        public ws2 m() {
            return new ws2(this);
        }

        public b() {
        }
    }

    public static b n() {
        return new b();
    }

    public int a() {
        return this.k;
    }

    public int b() {
        return this.j;
    }

    public ys2 c() {
        return this.a;
    }

    public zs2 d() {
        return this.b;
    }

    public String e() {
        return this.i;
    }

    public ys2 f() {
        return this.c;
    }

    public ys2 g() {
        return this.e;
    }

    public zs2 h() {
        return this.f;
    }

    public r72 i() {
        return this.d;
    }

    public ys2 j() {
        return this.g;
    }

    public zs2 k() {
        return this.h;
    }

    public boolean l() {
        return this.m;
    }

    public boolean m() {
        return this.l;
    }

    public ws2(b bVar) {
        ys2 ys2Var;
        zs2 zs2Var;
        ys2 ys2Var2;
        r72 r72Var;
        ys2 ys2Var3;
        zs2 zs2Var2;
        ys2 ys2Var4;
        zs2 zs2Var3;
        if (nc1.d()) {
            nc1.a("PoolConfig()");
        }
        if (bVar.a != null) {
            ys2Var = bVar.a;
        } else {
            ys2Var = ei0.a();
        }
        this.a = ys2Var;
        if (bVar.b != null) {
            zs2Var = bVar.b;
        } else {
            zs2Var = rg2.h();
        }
        this.b = zs2Var;
        if (bVar.c != null) {
            ys2Var2 = bVar.c;
        } else {
            ys2Var2 = lj0.b();
        }
        this.c = ys2Var2;
        if (bVar.d != null) {
            r72Var = bVar.d;
        } else {
            r72Var = qg2.b();
        }
        this.d = r72Var;
        if (bVar.e != null) {
            ys2Var3 = bVar.e;
        } else {
            ys2Var3 = fk0.a();
        }
        this.e = ys2Var3;
        if (bVar.f != null) {
            zs2Var2 = bVar.f;
        } else {
            zs2Var2 = rg2.h();
        }
        this.f = zs2Var2;
        if (bVar.g != null) {
            ys2Var4 = bVar.g;
        } else {
            ys2Var4 = ii0.a();
        }
        this.g = ys2Var4;
        if (bVar.h != null) {
            zs2Var3 = bVar.h;
        } else {
            zs2Var3 = rg2.h();
        }
        this.h = zs2Var3;
        this.i = bVar.i == null ? "legacy" : bVar.i;
        this.j = bVar.j;
        this.k = bVar.k > 0 ? bVar.k : 4194304;
        this.l = bVar.l;
        if (nc1.d()) {
            nc1.b();
        }
        this.m = bVar.m;
    }
}
