package defpackage;

import defpackage.kn2;

/* compiled from: OperationImpl.java */
/* renamed from: ln2  reason: default package */
/* loaded from: classes.dex */
public class ln2 implements kn2 {
    public final gb2<kn2.b> c = new gb2<>();
    public final wm3<kn2.b.c> d = wm3.t();

    public ln2() {
        a(kn2.b);
    }

    public void a(kn2.b bVar) {
        this.c.postValue(bVar);
        if (bVar instanceof kn2.b.c) {
            this.d.p((kn2.b.c) bVar);
        } else if (bVar instanceof kn2.b.a) {
            this.d.q(((kn2.b.a) bVar).a());
        }
    }
}
