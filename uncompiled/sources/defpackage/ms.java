package defpackage;

import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: ByteArrayBuilder.java */
/* renamed from: ms  reason: default package */
/* loaded from: classes.dex */
public final class ms extends OutputStream {
    public static final byte[] j0 = new byte[0];
    public final wr a;
    public final LinkedList<byte[]> f0;
    public int g0;
    public byte[] h0;
    public int i0;

    public ms() {
        this((wr) null);
    }

    public final void a() {
        int length = this.g0 + this.h0.length;
        if (length >= 0) {
            this.g0 = length;
            int max = Math.max(length >> 1, 1000);
            if (max > 262144) {
                max = 262144;
            }
            this.f0.add(this.h0);
            this.h0 = new byte[max];
            this.i0 = 0;
            return;
        }
        throw new IllegalStateException("Maximum Java array size (2GB) exceeded by `ByteArrayBuilder`");
    }

    public void b(int i) {
        if (this.i0 >= this.h0.length) {
            a();
        }
        byte[] bArr = this.h0;
        int i2 = this.i0;
        this.i0 = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public void c(int i) {
        int i2 = this.i0;
        int i3 = i2 + 2;
        byte[] bArr = this.h0;
        if (i3 < bArr.length) {
            int i4 = i2 + 1;
            this.i0 = i4;
            bArr[i2] = (byte) (i >> 16);
            int i5 = i4 + 1;
            this.i0 = i5;
            bArr[i4] = (byte) (i >> 8);
            this.i0 = i5 + 1;
            bArr[i5] = (byte) i;
            return;
        }
        b(i >> 16);
        b(i >> 8);
        b(i);
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public void d(int i) {
        int i2 = this.i0;
        int i3 = i2 + 1;
        byte[] bArr = this.h0;
        if (i3 < bArr.length) {
            int i4 = i2 + 1;
            this.i0 = i4;
            bArr[i2] = (byte) (i >> 8);
            this.i0 = i4 + 1;
            bArr[i4] = (byte) i;
            return;
        }
        b(i >> 8);
        b(i);
    }

    public byte[] e(int i) {
        this.i0 = i;
        return n();
    }

    public byte[] f() {
        a();
        return this.h0;
    }

    @Override // java.io.OutputStream, java.io.Flushable
    public void flush() {
    }

    public byte[] g() {
        return this.h0;
    }

    public int h() {
        return this.i0;
    }

    public void i() {
        byte[] bArr;
        j();
        wr wrVar = this.a;
        if (wrVar == null || (bArr = this.h0) == null) {
            return;
        }
        wrVar.i(2, bArr);
        this.h0 = null;
    }

    public void j() {
        this.g0 = 0;
        this.i0 = 0;
        if (this.f0.isEmpty()) {
            return;
        }
        this.f0.clear();
    }

    public byte[] l() {
        j();
        return this.h0;
    }

    public void m(int i) {
        this.i0 = i;
    }

    public byte[] n() {
        int i = this.g0 + this.i0;
        if (i == 0) {
            return j0;
        }
        byte[] bArr = new byte[i];
        Iterator<byte[]> it = this.f0.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            byte[] next = it.next();
            int length = next.length;
            System.arraycopy(next, 0, bArr, i2, length);
            i2 += length;
        }
        System.arraycopy(this.h0, 0, bArr, i2, this.i0);
        int i3 = i2 + this.i0;
        if (i3 == i) {
            if (!this.f0.isEmpty()) {
                j();
            }
            return bArr;
        }
        throw new RuntimeException("Internal error: total len assumed to be " + i + ", copied " + i3 + " bytes");
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public ms(wr wrVar) {
        this(wrVar, 500);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        while (true) {
            int min = Math.min(this.h0.length - this.i0, i2);
            if (min > 0) {
                System.arraycopy(bArr, i, this.h0, this.i0, min);
                i += min;
                this.i0 += min;
                i2 -= min;
            }
            if (i2 <= 0) {
                return;
            }
            a();
        }
    }

    public ms(int i) {
        this(null, i);
    }

    public ms(wr wrVar, int i) {
        this.f0 = new LinkedList<>();
        this.a = wrVar;
        this.h0 = wrVar == null ? new byte[i] : wrVar.a(2);
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        b(i);
    }
}
