package defpackage;

import android.content.Context;
import android.view.View;
import net.safemoon.androidwallet.R;

/* compiled from: AnchorCollectionsOption.kt */
/* renamed from: zb  reason: default package */
/* loaded from: classes2.dex */
public final class zb extends gc {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public zb(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        super(rc1Var, rc1Var2);
        fs1.f(rc1Var, "onShowListener");
        fs1.f(rc1Var2, "onDismissListener");
    }

    public static final void l(tc1 tc1Var, zb zbVar, View view) {
        fs1.f(tc1Var, "$callBack");
        fs1.f(zbVar, "this$0");
        tc1Var.invoke(Integer.valueOf(view.getId()));
        zbVar.d();
    }

    public static final void m(tc1 tc1Var, zb zbVar, View view) {
        fs1.f(tc1Var, "$callBack");
        fs1.f(zbVar, "this$0");
        tc1Var.invoke(Integer.valueOf(view.getId()));
        zbVar.d();
    }

    public final zb k(Context context, View view, final tc1<? super Integer, te4> tc1Var) {
        fs1.f(context, "context");
        fs1.f(view, "anchorView");
        fs1.f(tc1Var, "callBack");
        cn0 a = cn0.a(b(context, -2, R.layout.dialog_anchor_collection_option));
        a.a.setOnClickListener(new View.OnClickListener() { // from class: yb
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                zb.l(tc1.this, this, view2);
            }
        });
        a.b.setOnClickListener(new View.OnClickListener() { // from class: xb
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                zb.m(tc1.this, this, view2);
            }
        });
        g(view);
        return this;
    }
}
