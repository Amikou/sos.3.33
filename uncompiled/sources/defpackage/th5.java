package defpackage;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.n1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: th5  reason: default package */
/* loaded from: classes.dex */
public final class th5 {
    public final s65 a;
    public final wk5 b;
    public final wk5 c;
    public final ds5 d;

    public th5() {
        s65 s65Var = new s65();
        this.a = s65Var;
        wk5 wk5Var = new wk5(null, s65Var);
        this.c = wk5Var;
        this.b = wk5Var.c();
        ds5 ds5Var = new ds5();
        this.d = ds5Var;
        wk5Var.e("require", new f56(ds5Var));
        ds5Var.a("internal.platform", ye5.a);
        wk5Var.e("runtime.counter", new z45(Double.valueOf((double) Utils.DOUBLE_EPSILON)));
    }

    public final z55 a(wk5 wk5Var, n1... n1VarArr) {
        z55 z55Var = z55.X;
        for (n1 n1Var : n1VarArr) {
            z55Var = rp5.b(n1Var);
            vm5.k(this.c);
            if ((z55Var instanceof a65) || (z55Var instanceof u55)) {
                z55Var = this.a.b(wk5Var, z55Var);
            }
        }
        return z55Var;
    }
}
