package defpackage;

import com.google.crypto.tink.shaded.protobuf.ProtoSyntax;
import com.google.crypto.tink.shaded.protobuf.e0;
import com.google.crypto.tink.shaded.protobuf.q;

/* compiled from: StructuralMessageInfo.java */
/* renamed from: gv3  reason: default package */
/* loaded from: classes2.dex */
public final class gv3 implements a82 {
    public final ProtoSyntax a;
    public final boolean b;
    public final int[] c;
    public final q[] d;
    public final e0 e;

    @Override // defpackage.a82
    public boolean a() {
        return this.b;
    }

    @Override // defpackage.a82
    public e0 b() {
        return this.e;
    }

    @Override // defpackage.a82
    public ProtoSyntax c() {
        return this.a;
    }

    public int[] d() {
        return this.c;
    }

    public q[] e() {
        return this.d;
    }
}
