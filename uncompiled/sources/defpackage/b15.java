package defpackage;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.a;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: b15  reason: default package */
/* loaded from: classes.dex */
public final class b15 extends a.AbstractC0106a<so3, uo3> {
    @Override // com.google.android.gms.common.api.a.AbstractC0106a
    public final /* synthetic */ so3 d(Context context, Looper looper, kz kzVar, uo3 uo3Var, GoogleApiClient.b bVar, GoogleApiClient.c cVar) {
        uo3 uo3Var2 = uo3Var;
        if (uo3Var2 == null) {
            uo3Var2 = uo3.a;
        }
        return new so3(context, looper, true, kzVar, uo3Var2, bVar, cVar);
    }
}
