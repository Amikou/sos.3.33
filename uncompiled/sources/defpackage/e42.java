package defpackage;

import com.google.gson.JsonObject;
import net.safemoon.androidwallet.model.AllCryptoList;
import net.safemoon.androidwallet.model.MoonPaySignURL;
import net.safemoon.androidwallet.model.blackListAddress.BlackListAddress;
import net.safemoon.androidwallet.model.blackListTokens.BlackListTokens;
import net.safemoon.androidwallet.model.blackListTokens.Request;
import net.safemoon.androidwallet.model.fiat.gson.FiatInfor;
import net.safemoon.androidwallet.model.fiat.gson.FiatLatest;
import net.safemoon.androidwallet.model.nft.DeleteNftIdList;
import net.safemoon.androidwallet.model.nft.DeleteNfts;
import net.safemoon.androidwallet.model.swap.AllSwapTokens;
import net.safemoon.androidwallet.model.swap.BaseTokens;
import net.safemoon.androidwallet.model.swap.Pairs;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokenInfoResult;
import net.safemoon.androidwallet.model.tokensInfo.CurrencyTokensInfoResult;
import retrofit2.b;

/* compiled from: MarketDataAPIInterface.kt */
/* renamed from: e42  reason: default package */
/* loaded from: classes2.dex */
public interface e42 {
    @oo2("/api/nft/delete-nfts")
    b<DeleteNfts> a(@ar DeleteNftIdList deleteNftIdList);

    @ge1("/api/exchange/latest?")
    b<FiatLatest> b(@yw2("symbols") String str);

    @v81
    @oo2("/api/moonpay/sign-moonpay")
    b<MoonPaySignURL> c(@g31("originalUrl") String str);

    @ge1("/api/nft/delete-nfts")
    b<DeleteNfts> d(@yw2("walletAddress") String str);

    @ge1("/api/cryptocurrency/v7/quotes/latest?")
    b<JsonObject> e(@yw2("ids") String str);

    @ge1("/api/cryptocurrency/v3/quotes/latest?")
    b<JsonObject> f(@yw2("symbols") String str);

    @oo2("/api/nft/putback-nfts")
    b<DeleteNfts> g(@ar DeleteNftIdList deleteNftIdList);

    @ge1("/api/cryptocurrency/listings/recent?")
    b<AllCryptoList> h(@yw2("start") Integer num, @yw2("limit") Integer num2, @yw2("convert") String str, @yw2("sort") String str2, @yw2("sort_dir") String str3);

    @ge1("/api/cryptocurrency/listings/looser?")
    b<AllCryptoList> i(@yw2("start") Integer num, @yw2("limit") Integer num2, @yw2("convert") String str, @yw2("sortType") String str2, @yw2("sort_dir") String str3);

    @oo2("/api/swap/check-token-blacklist")
    b<BlackListTokens> j(@ar Request request);

    @oo2("/api/cryptocurrency/tokens-info?")
    b<CurrencyTokensInfoResult> k(@ar net.safemoon.androidwallet.model.tokensInfo.Request request);

    @ge1("/api/exchange/infor?")
    b<FiatInfor> l(@yw2("symbols") String str);

    @ge1("/api/swap/v7/tokens")
    b<AllSwapTokens> m();

    @ge1("/api/cryptocurrency/listings/latest?")
    b<AllCryptoList> n(@yw2("start") Integer num, @yw2("limit") Integer num2, @yw2("convert") String str);

    @oo2("/api/swap/check-walletaddress-blacklist")
    b<BlackListAddress> o(@ar net.safemoon.androidwallet.model.blackListAddress.Request request);

    @ge1("/api/cryptocurrency/token-info?")
    b<CurrencyTokenInfoResult> p(@yw2("tokenAddress") String str, @yw2("pairAddress") String str2);

    @ge1("/api/pair/v4/list")
    b<Pairs> q(@yw2("chainId") int i);

    @ge1("/api/cryptocurrency/listings/gainer?")
    b<AllCryptoList> r(@yw2("start") Integer num, @yw2("limit") Integer num2, @yw2("convert") String str, @yw2("sortType") String str2, @yw2("sort_dir") String str3);

    @ge1("/api/cryptocurrency/v2/base-token")
    b<BaseTokens> s(@yw2("chainId") int i);
}
