package defpackage;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* renamed from: sf5  reason: default package */
/* loaded from: classes.dex */
public final class sf5<E> extends n65<E> {
    public static final sf5<Object> g0;
    public final List<E> f0;

    static {
        sf5<Object> sf5Var = new sf5<>();
        g0 = sf5Var;
        sf5Var.v();
    }

    public sf5() {
        this(new ArrayList(10));
    }

    public sf5(List<E> list) {
        this.f0 = list;
    }

    public static <E> sf5<E> i() {
        return (sf5<E>) g0;
    }

    @Override // defpackage.rb5
    public final /* synthetic */ rb5 T0(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f0);
            return new sf5(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final void add(int i, E e) {
        e();
        this.f0.add(i, e);
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final E get(int i) {
        return this.f0.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public final E remove(int i) {
        e();
        E remove = this.f0.remove(i);
        ((AbstractList) this).modCount++;
        return remove;
    }

    @Override // java.util.AbstractList, java.util.List
    public final E set(int i, E e) {
        e();
        E e2 = this.f0.set(i, e);
        ((AbstractList) this).modCount++;
        return e2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.f0.size();
    }
}
