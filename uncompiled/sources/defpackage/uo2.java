package defpackage;

/* compiled from: PackageReference.kt */
/* renamed from: uo2  reason: default package */
/* loaded from: classes2.dex */
public final class uo2 implements az {
    public final Class<?> a;

    public uo2(Class<?> cls, String str) {
        fs1.f(cls, "jClass");
        fs1.f(str, "moduleName");
        this.a = cls;
    }

    @Override // defpackage.az
    public Class<?> a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        return (obj instanceof uo2) && fs1.b(a(), ((uo2) obj).a());
    }

    public int hashCode() {
        return a().hashCode();
    }

    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
