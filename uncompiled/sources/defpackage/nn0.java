package defpackage;

import android.view.View;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: DialogCustomConfirmationBinding.java */
/* renamed from: nn0  reason: default package */
/* loaded from: classes2.dex */
public final class nn0 {
    public final LinearLayoutCompat a;

    public nn0(ConstraintLayout constraintLayout, LinearLayoutCompat linearLayoutCompat) {
        this.a = linearLayoutCompat;
    }

    public static nn0 a(View view) {
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.childWrapper);
        if (linearLayoutCompat != null) {
            return new nn0((ConstraintLayout) view, linearLayoutCompat);
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.childWrapper)));
    }
}
