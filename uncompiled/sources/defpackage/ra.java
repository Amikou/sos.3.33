package defpackage;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.AlarmManagerSchedulerBroadcastReceiver;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;

/* compiled from: AlarmManagerScheduler.java */
/* renamed from: ra  reason: default package */
/* loaded from: classes.dex */
public class ra implements rq4 {
    public final Context a;
    public final dy0 b;
    public AlarmManager c;
    public final SchedulerConfig d;
    public final qz e;

    public ra(Context context, dy0 dy0Var, qz qzVar, SchedulerConfig schedulerConfig) {
        this(context, dy0Var, (AlarmManager) context.getSystemService("alarm"), qzVar, schedulerConfig);
    }

    @Override // defpackage.rq4
    public void a(ob4 ob4Var, int i) {
        b(ob4Var, i, false);
    }

    @Override // defpackage.rq4
    public void b(ob4 ob4Var, int i, boolean z) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("backendName", ob4Var.b());
        builder.appendQueryParameter("priority", String.valueOf(wu2.a(ob4Var.d())));
        if (ob4Var.c() != null) {
            builder.appendQueryParameter("extras", Base64.encodeToString(ob4Var.c(), 0));
        }
        Intent intent = new Intent(this.a, AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(builder.build());
        intent.putExtra("attemptNumber", i);
        if (!z && c(intent)) {
            z12.a("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", ob4Var);
            return;
        }
        long l0 = this.b.l0(ob4Var);
        long g = this.d.g(ob4Var.d(), l0, i);
        z12.b("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", ob4Var, Long.valueOf(g), Long.valueOf(l0), Integer.valueOf(i));
        this.c.set(3, this.e.a() + g, PendingIntent.getBroadcast(this.a, 0, intent, 0));
    }

    public boolean c(Intent intent) {
        return PendingIntent.getBroadcast(this.a, 0, intent, 536870912) != null;
    }

    public ra(Context context, dy0 dy0Var, AlarmManager alarmManager, qz qzVar, SchedulerConfig schedulerConfig) {
        this.a = context;
        this.b = dy0Var;
        this.c = alarmManager;
        this.e = qzVar;
        this.d = schedulerConfig;
    }
}
