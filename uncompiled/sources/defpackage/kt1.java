package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import net.safemoon.androidwallet.R;

/* compiled from: ItemWalletTokenCardBinding.java */
/* renamed from: kt1  reason: default package */
/* loaded from: classes2.dex */
public final class kt1 {
    public final CardView a;
    public final ImageView b;
    public final ImageView c;
    public final TextView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;
    public final TextView h;

    public kt1(CardView cardView, ImageView imageView, ImageView imageView2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, LinearLayout linearLayout) {
        this.a = cardView;
        this.b = imageView;
        this.c = imageView2;
        this.d = textView;
        this.e = textView2;
        this.f = textView3;
        this.g = textView4;
        this.h = textView5;
    }

    public static kt1 a(View view) {
        int i = R.id.ivPercentageDirection;
        ImageView imageView = (ImageView) ai4.a(view, R.id.ivPercentageDirection);
        if (imageView != null) {
            i = R.id.ivTokenIcon;
            ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivTokenIcon);
            if (imageView2 != null) {
                i = R.id.tvPercentage;
                TextView textView = (TextView) ai4.a(view, R.id.tvPercentage);
                if (textView != null) {
                    i = R.id.tvTokenBalance;
                    TextView textView2 = (TextView) ai4.a(view, R.id.tvTokenBalance);
                    if (textView2 != null) {
                        i = R.id.tvTokenName;
                        TextView textView3 = (TextView) ai4.a(view, R.id.tvTokenName);
                        if (textView3 != null) {
                            i = R.id.tvTokenNativeBalance;
                            TextView textView4 = (TextView) ai4.a(view, R.id.tvTokenNativeBalance);
                            if (textView4 != null) {
                                i = R.id.tvTokenPrice;
                                TextView textView5 = (TextView) ai4.a(view, R.id.tvTokenPrice);
                                if (textView5 != null) {
                                    i = R.id.vPercentageContainer;
                                    LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vPercentageContainer);
                                    if (linearLayout != null) {
                                        return new kt1((CardView) view, imageView, imageView2, textView, textView2, textView3, textView4, textView5, linearLayout);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public CardView b() {
        return this.a;
    }
}
