package defpackage;

import android.os.Build;
import android.text.TextUtils;
import java.util.Objects;

/* compiled from: MediaSessionManager.java */
/* renamed from: n62  reason: default package */
/* loaded from: classes.dex */
public final class n62 {
    public o62 a;

    public n62(String str, int i, int i2) {
        Objects.requireNonNull(str, "package shouldn't be null");
        if (!TextUtils.isEmpty(str)) {
            if (Build.VERSION.SDK_INT >= 28) {
                this.a = new p62(str, i, i2);
                return;
            } else {
                this.a = new q62(str, i, i2);
                return;
            }
        }
        throw new IllegalArgumentException("packageName should be nonempty");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof n62) {
            return this.a.equals(((n62) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
