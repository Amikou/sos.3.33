package defpackage;

import com.facebook.imagepipeline.memory.BasePool;

/* compiled from: NoOpPoolStatsTracker.java */
/* renamed from: rg2  reason: default package */
/* loaded from: classes.dex */
public class rg2 implements zs2 {
    public static rg2 a;

    public static synchronized rg2 h() {
        rg2 rg2Var;
        synchronized (rg2.class) {
            if (a == null) {
                a = new rg2();
            }
            rg2Var = a;
        }
        return rg2Var;
    }

    @Override // defpackage.zs2
    public void a(int i) {
    }

    @Override // defpackage.zs2
    public void b(int i) {
    }

    @Override // defpackage.zs2
    public void c(BasePool basePool) {
    }

    @Override // defpackage.zs2
    public void d() {
    }

    @Override // defpackage.zs2
    public void e(int i) {
    }

    @Override // defpackage.zs2
    public void f() {
    }

    @Override // defpackage.zs2
    public void g(int i) {
    }
}
