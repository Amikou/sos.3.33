package defpackage;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;

/* compiled from: FLogDefaultLoggingDelegate.java */
/* renamed from: w11  reason: default package */
/* loaded from: classes.dex */
public class w11 implements a22 {
    public static final w11 c = new w11();
    public String a = "unknown";
    public int b = 5;

    public static w11 k() {
        return c;
    }

    public static String l(String str, Throwable th) {
        return str + '\n' + m(th);
    }

    public static String m(Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    @Override // defpackage.a22
    public void a(String str, String str2) {
        o(5, str, str2);
    }

    @Override // defpackage.a22
    public void b(String str, String str2) {
        o(6, str, str2);
    }

    @Override // defpackage.a22
    public void c(String str, String str2) {
        o(3, str, str2);
    }

    @Override // defpackage.a22
    public void d(String str, String str2, Throwable th) {
        p(6, str, str2, th);
    }

    @Override // defpackage.a22
    public void e(String str, String str2, Throwable th) {
        p(6, str, str2, th);
    }

    @Override // defpackage.a22
    public void f(String str, String str2, Throwable th) {
        p(5, str, str2, th);
    }

    @Override // defpackage.a22
    public void g(String str, String str2) {
        o(2, str, str2);
    }

    @Override // defpackage.a22
    public void h(String str, String str2) {
        o(6, str, str2);
    }

    @Override // defpackage.a22
    public void i(String str, String str2, Throwable th) {
        p(3, str, str2, th);
    }

    @Override // defpackage.a22
    public boolean j(int i) {
        return this.b <= i;
    }

    public final String n(String str) {
        if (this.a != null) {
            return this.a + ":" + str;
        }
        return str;
    }

    public final void o(int i, String str, String str2) {
        Log.println(i, n(str), str2);
    }

    public final void p(int i, String str, String str2, Throwable th) {
        Log.println(i, n(str), l(str2, th));
    }
}
