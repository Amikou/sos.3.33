package defpackage;

import java.util.NoSuchElementException;

/* compiled from: BaseMediaChunkIterator.java */
/* renamed from: mn  reason: default package */
/* loaded from: classes.dex */
public abstract class mn implements t52 {
    public final long b;
    public final long c;
    public long d;

    public mn(long j, long j2) {
        this.b = j;
        this.c = j2;
        f();
    }

    public final void c() {
        long j = this.d;
        if (j < this.b || j > this.c) {
            throw new NoSuchElementException();
        }
    }

    public final long d() {
        return this.d;
    }

    public boolean e() {
        return this.d > this.c;
    }

    public void f() {
        this.d = this.b - 1;
    }

    @Override // defpackage.t52
    public boolean next() {
        this.d++;
        return !e();
    }
}
