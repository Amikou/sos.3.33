package defpackage;

import defpackage.a21;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: SafeKeyGenerator.java */
/* renamed from: wb3  reason: default package */
/* loaded from: classes.dex */
public class wb3 {
    public final s22<fx1, String> a = new s22<>(1000);
    public final et2<b> b = a21.d(10, new a(this));

    /* compiled from: SafeKeyGenerator.java */
    /* renamed from: wb3$a */
    /* loaded from: classes.dex */
    public class a implements a21.d<b> {
        public a(wb3 wb3Var) {
        }

        @Override // defpackage.a21.d
        /* renamed from: b */
        public b a() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* compiled from: SafeKeyGenerator.java */
    /* renamed from: wb3$b */
    /* loaded from: classes.dex */
    public static final class b implements a21.f {
        public final MessageDigest a;
        public final et3 f0 = et3.a();

        public b(MessageDigest messageDigest) {
            this.a = messageDigest;
        }

        @Override // defpackage.a21.f
        public et3 g() {
            return this.f0;
        }
    }

    public final String a(fx1 fx1Var) {
        b bVar = (b) wt2.d(this.b.b());
        try {
            fx1Var.b(bVar.a);
            return mg4.w(bVar.a.digest());
        } finally {
            this.b.a(bVar);
        }
    }

    public String b(fx1 fx1Var) {
        String g;
        synchronized (this.a) {
            g = this.a.g(fx1Var);
        }
        if (g == null) {
            g = a(fx1Var);
        }
        synchronized (this.a) {
            this.a.k(fx1Var, g);
        }
        return g;
    }
}
