package defpackage;

import java.io.Closeable;
import kotlin.coroutines.CoroutineContext;

/* compiled from: ViewModel.kt */
/* renamed from: xz  reason: default package */
/* loaded from: classes.dex */
public final class xz implements Closeable, c90 {
    public final CoroutineContext a;

    public xz(CoroutineContext coroutineContext) {
        fs1.f(coroutineContext, "context");
        this.a = coroutineContext;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        zt1.f(m(), null, 1, null);
    }

    @Override // defpackage.c90
    public CoroutineContext m() {
        return this.a;
    }
}
