package defpackage;

import com.onesignal.influence.domain.OSInfluenceType;
import com.onesignal.o0;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSOutcomeEventsV1Repository.kt */
/* renamed from: lk2  reason: default package */
/* loaded from: classes2.dex */
public final class lk2 extends jk2 {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public lk2(yj2 yj2Var, fk2 fk2Var, co2 co2Var) {
        super(yj2Var, fk2Var, co2Var);
        fs1.f(yj2Var, "logger");
        fs1.f(fk2Var, "outcomeEventsCache");
        fs1.f(co2Var, "outcomeEventsService");
    }

    @Override // defpackage.ik2
    public void g(String str, int i, dk2 dk2Var, ym2 ym2Var) {
        fs1.f(str, "appId");
        fs1.f(dk2Var, "eventParams");
        fs1.f(ym2Var, "responseHandler");
        o0 a = o0.a(dk2Var);
        fs1.e(a, "event");
        OSInfluenceType b = a.b();
        if (b == null) {
            return;
        }
        int i2 = kk2.a[b.ordinal()];
        if (i2 == 1) {
            l(str, i, a, ym2Var);
        } else if (i2 == 2) {
            m(str, i, a, ym2Var);
        } else if (i2 != 3) {
        } else {
            n(str, i, a, ym2Var);
        }
    }

    public final void l(String str, int i, o0 o0Var, ym2 ym2Var) {
        try {
            JSONObject put = o0Var.c().put("app_id", str).put("device_type", i).put("direct", true);
            co2 k = k();
            fs1.e(put, "jsonObject");
            k.a(put, ym2Var);
        } catch (JSONException e) {
            j().error("Generating direct outcome:JSON Failed.", e);
        }
    }

    public final void m(String str, int i, o0 o0Var, ym2 ym2Var) {
        try {
            JSONObject put = o0Var.c().put("app_id", str).put("device_type", i).put("direct", false);
            co2 k = k();
            fs1.e(put, "jsonObject");
            k.a(put, ym2Var);
        } catch (JSONException e) {
            j().error("Generating indirect outcome:JSON Failed.", e);
        }
    }

    public final void n(String str, int i, o0 o0Var, ym2 ym2Var) {
        try {
            JSONObject put = o0Var.c().put("app_id", str).put("device_type", i);
            co2 k = k();
            fs1.e(put, "jsonObject");
            k.a(put, ym2Var);
        } catch (JSONException e) {
            j().error("Generating unattributed outcome:JSON Failed.", e);
        }
    }
}
