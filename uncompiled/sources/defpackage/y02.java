package defpackage;

import androidx.paging.LoadType;

/* renamed from: y02  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class y02 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    static {
        int[] iArr = new int[LoadType.values().length];
        a = iArr;
        LoadType loadType = LoadType.APPEND;
        iArr[loadType.ordinal()] = 1;
        LoadType loadType2 = LoadType.PREPEND;
        iArr[loadType2.ordinal()] = 2;
        LoadType loadType3 = LoadType.REFRESH;
        iArr[loadType3.ordinal()] = 3;
        int[] iArr2 = new int[LoadType.values().length];
        b = iArr2;
        iArr2[loadType3.ordinal()] = 1;
        iArr2[loadType.ordinal()] = 2;
        iArr2[loadType2.ordinal()] = 3;
    }
}
