package defpackage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/* compiled from: WorkTimer.java */
/* renamed from: zq4  reason: default package */
/* loaded from: classes.dex */
public class zq4 {
    public static final String f = v12.f("WorkTimer");
    public final ThreadFactory a;
    public final ScheduledExecutorService b;
    public final Map<String, c> c;
    public final Map<String, b> d;
    public final Object e;

    /* compiled from: WorkTimer.java */
    /* renamed from: zq4$a */
    /* loaded from: classes.dex */
    public class a implements ThreadFactory {
        public int a = 0;

        public a(zq4 zq4Var) {
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
            newThread.setName("WorkManager-WorkTimer-thread-" + this.a);
            this.a = this.a + 1;
            return newThread;
        }
    }

    /* compiled from: WorkTimer.java */
    /* renamed from: zq4$b */
    /* loaded from: classes.dex */
    public interface b {
        void a(String str);
    }

    /* compiled from: WorkTimer.java */
    /* renamed from: zq4$c */
    /* loaded from: classes.dex */
    public static class c implements Runnable {
        public final zq4 a;
        public final String f0;

        public c(zq4 zq4Var, String str) {
            this.a = zq4Var;
            this.f0 = str;
        }

        @Override // java.lang.Runnable
        public void run() {
            synchronized (this.a.e) {
                if (this.a.c.remove(this.f0) != null) {
                    b remove = this.a.d.remove(this.f0);
                    if (remove != null) {
                        remove.a(this.f0);
                    }
                } else {
                    v12.c().a("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.f0), new Throwable[0]);
                }
            }
        }
    }

    public zq4() {
        a aVar = new a(this);
        this.a = aVar;
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new Object();
        this.b = Executors.newSingleThreadScheduledExecutor(aVar);
    }

    public void a() {
        if (this.b.isShutdown()) {
            return;
        }
        this.b.shutdownNow();
    }

    public void b(String str, long j, b bVar) {
        synchronized (this.e) {
            v12.c().a(f, String.format("Starting timer for %s", str), new Throwable[0]);
            c(str);
            c cVar = new c(this, str);
            this.c.put(str, cVar);
            this.d.put(str, bVar);
            this.b.schedule(cVar, j, TimeUnit.MILLISECONDS);
        }
    }

    public void c(String str) {
        synchronized (this.e) {
            if (this.c.remove(str) != null) {
                v12.c().a(f, String.format("Stopping timer for %s", str), new Throwable[0]);
                this.d.remove(str);
            }
        }
    }
}
