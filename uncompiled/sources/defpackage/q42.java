package defpackage;

import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateSelector;
import java.util.Iterator;

/* compiled from: MaterialTextInputPicker.java */
/* renamed from: q42  reason: default package */
/* loaded from: classes2.dex */
public final class q42<S> extends uq2<S> {
    public int f0;
    public DateSelector<S> g0;
    public CalendarConstraints h0;

    /* compiled from: MaterialTextInputPicker.java */
    /* renamed from: q42$a */
    /* loaded from: classes2.dex */
    public class a extends sm2<S> {
        public a() {
        }

        @Override // defpackage.sm2
        public void a() {
            Iterator<sm2<S>> it = q42.this.a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }

        @Override // defpackage.sm2
        public void b(S s) {
            Iterator<sm2<S>> it = q42.this.a.iterator();
            while (it.hasNext()) {
                it.next().b(s);
            }
        }
    }

    public static <T> q42<T> g(DateSelector<T> dateSelector, int i, CalendarConstraints calendarConstraints) {
        q42<T> q42Var = new q42<>();
        Bundle bundle = new Bundle();
        bundle.putInt("THEME_RES_ID_KEY", i);
        bundle.putParcelable("DATE_SELECTOR_KEY", dateSelector);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", calendarConstraints);
        q42Var.setArguments(bundle);
        return q42Var;
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.f0 = bundle.getInt("THEME_RES_ID_KEY");
        this.g0 = (DateSelector) bundle.getParcelable("DATE_SELECTOR_KEY");
        this.h0 = (CalendarConstraints) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.g0.S(layoutInflater.cloneInContext(new ContextThemeWrapper(getContext(), this.f0)), viewGroup, bundle, this.h0, new a());
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("THEME_RES_ID_KEY", this.f0);
        bundle.putParcelable("DATE_SELECTOR_KEY", this.g0);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.h0);
    }
}
