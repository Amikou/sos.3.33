package defpackage;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: j36  reason: default package */
/* loaded from: classes.dex */
public final class j36 extends tz5 {
    public final g26 a = new g26();

    @Override // defpackage.tz5
    public final void a(Throwable th, Throwable th2) {
        if (th2 != th) {
            this.a.a(th, true).add(th2);
            return;
        }
        throw new IllegalArgumentException("Self suppression is not allowed.", th2);
    }
}
