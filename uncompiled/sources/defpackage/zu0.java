package defpackage;

import android.graphics.ColorSpace;
import android.util.Pair;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.a;
import com.facebook.imageutils.HeifExifUtil;
import com.facebook.imageutils.d;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: EncodedImage.java */
/* renamed from: zu0  reason: default package */
/* loaded from: classes.dex */
public class zu0 implements Closeable {
    public static boolean q0;
    public final a<PooledByteBuffer> a;
    public final fw3<FileInputStream> f0;
    public wn1 g0;
    public int h0;
    public int i0;
    public int j0;
    public int k0;
    public int l0;
    public int m0;
    public ct n0;
    public ColorSpace o0;
    public boolean p0;

    public zu0(a<PooledByteBuffer> aVar) {
        this.g0 = wn1.b;
        this.h0 = -1;
        this.i0 = 0;
        this.j0 = -1;
        this.k0 = -1;
        this.l0 = 1;
        this.m0 = -1;
        xt2.b(Boolean.valueOf(a.u(aVar)));
        this.a = aVar.clone();
        this.f0 = null;
    }

    public static boolean A(zu0 zu0Var) {
        return zu0Var.h0 >= 0 && zu0Var.j0 >= 0 && zu0Var.k0 >= 0;
    }

    public static boolean F(zu0 zu0Var) {
        return zu0Var != null && zu0Var.C();
    }

    public static zu0 b(zu0 zu0Var) {
        if (zu0Var != null) {
            return zu0Var.a();
        }
        return null;
    }

    public static void c(zu0 zu0Var) {
        if (zu0Var != null) {
            zu0Var.close();
        }
    }

    public synchronized boolean C() {
        boolean z;
        if (!a.u(this.a)) {
            z = this.f0 != null;
        }
        return z;
    }

    public void M() {
        if (!q0) {
            x();
        } else if (this.p0) {
        } else {
            x();
            this.p0 = true;
        }
    }

    public final void N() {
        if (this.j0 < 0 || this.k0 < 0) {
            M();
        }
    }

    public final eo1 Q() {
        InputStream inputStream;
        try {
            inputStream = m();
            try {
                eo1 b = rq.b(inputStream);
                this.o0 = b.a();
                Pair<Integer, Integer> b2 = b.b();
                if (b2 != null) {
                    this.j0 = ((Integer) b2.first).intValue();
                    this.k0 = ((Integer) b2.second).intValue();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                    }
                }
                return b;
            } catch (Throwable th) {
                th = th;
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
        }
    }

    public final Pair<Integer, Integer> R() {
        Pair<Integer, Integer> g = d.g(m());
        if (g != null) {
            this.j0 = ((Integer) g.first).intValue();
            this.k0 = ((Integer) g.second).intValue();
        }
        return g;
    }

    public void S(ct ctVar) {
        this.n0 = ctVar;
    }

    public void W(int i) {
        this.i0 = i;
    }

    public void X(int i) {
        this.k0 = i;
    }

    public zu0 a() {
        zu0 zu0Var;
        fw3<FileInputStream> fw3Var = this.f0;
        if (fw3Var != null) {
            zu0Var = new zu0(fw3Var, this.m0);
        } else {
            a e = a.e(this.a);
            if (e == null) {
                zu0Var = null;
            } else {
                try {
                    zu0Var = new zu0(e);
                } finally {
                    a.g(e);
                }
            }
        }
        if (zu0Var != null) {
            zu0Var.d(this);
        }
        return zu0Var;
    }

    public void a0(wn1 wn1Var) {
        this.g0 = wn1Var;
    }

    public void b0(int i) {
        this.h0 = i;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        a.g(this.a);
    }

    public void d(zu0 zu0Var) {
        this.g0 = zu0Var.l();
        this.j0 = zu0Var.v();
        this.k0 = zu0Var.j();
        this.h0 = zu0Var.q();
        this.i0 = zu0Var.h();
        this.l0 = zu0Var.r();
        this.m0 = zu0Var.u();
        this.n0 = zu0Var.f();
        this.o0 = zu0Var.g();
        this.p0 = zu0Var.w();
    }

    public a<PooledByteBuffer> e() {
        return a.e(this.a);
    }

    public void e0(int i) {
        this.l0 = i;
    }

    public ct f() {
        return this.n0;
    }

    public void f0(int i) {
        this.j0 = i;
    }

    public ColorSpace g() {
        N();
        return this.o0;
    }

    public int h() {
        N();
        return this.i0;
    }

    public String i(int i) {
        a<PooledByteBuffer> e = e();
        if (e == null) {
            return "";
        }
        int min = Math.min(u(), i);
        byte[] bArr = new byte[min];
        try {
            PooledByteBuffer j = e.j();
            if (j == null) {
                return "";
            }
            j.s(0, bArr, 0, min);
            e.close();
            StringBuilder sb = new StringBuilder(min * 2);
            for (int i2 = 0; i2 < min; i2++) {
                sb.append(String.format("%02X", Byte.valueOf(bArr[i2])));
            }
            return sb.toString();
        } finally {
            e.close();
        }
    }

    public int j() {
        N();
        return this.k0;
    }

    public wn1 l() {
        N();
        return this.g0;
    }

    public InputStream m() {
        fw3<FileInputStream> fw3Var = this.f0;
        if (fw3Var != null) {
            return fw3Var.get();
        }
        a e = a.e(this.a);
        if (e != null) {
            try {
                return new bt2((PooledByteBuffer) e.j());
            } finally {
                a.g(e);
            }
        }
        return null;
    }

    public InputStream n() {
        return (InputStream) xt2.g(m());
    }

    public int q() {
        N();
        return this.h0;
    }

    public int r() {
        return this.l0;
    }

    public int u() {
        a<PooledByteBuffer> aVar = this.a;
        if (aVar != null && aVar.j() != null) {
            return this.a.j().size();
        }
        return this.m0;
    }

    public int v() {
        N();
        return this.j0;
    }

    public boolean w() {
        return this.p0;
    }

    public final void x() {
        Pair<Integer, Integer> b;
        wn1 c = xn1.c(m());
        this.g0 = c;
        if (wj0.b(c)) {
            b = R();
        } else {
            b = Q().b();
        }
        if (c == wj0.a && this.h0 == -1) {
            if (b != null) {
                int b2 = com.facebook.imageutils.a.b(m());
                this.i0 = b2;
                this.h0 = com.facebook.imageutils.a.a(b2);
            }
        } else if (c == wj0.k && this.h0 == -1) {
            int a = HeifExifUtil.a(m());
            this.i0 = a;
            this.h0 = com.facebook.imageutils.a.a(a);
        } else if (this.h0 == -1) {
            this.h0 = 0;
        }
    }

    public boolean z(int i) {
        wn1 wn1Var = this.g0;
        if ((wn1Var == wj0.a || wn1Var == wj0.l) && this.f0 == null) {
            xt2.g(this.a);
            PooledByteBuffer j = this.a.j();
            return j.p(i + (-2)) == -1 && j.p(i - 1) == -39;
        }
        return true;
    }

    public zu0(fw3<FileInputStream> fw3Var) {
        this.g0 = wn1.b;
        this.h0 = -1;
        this.i0 = 0;
        this.j0 = -1;
        this.k0 = -1;
        this.l0 = 1;
        this.m0 = -1;
        xt2.g(fw3Var);
        this.a = null;
        this.f0 = fw3Var;
    }

    public zu0(fw3<FileInputStream> fw3Var, int i) {
        this(fw3Var);
        this.m0 = i;
    }
}
