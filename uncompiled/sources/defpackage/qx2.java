package defpackage;

import android.os.Handler;
import android.os.Looper;

/* compiled from: QuietLiveDataObserver.kt */
/* renamed from: qx2  reason: default package */
/* loaded from: classes2.dex */
public final class qx2<T> implements tl2<T> {
    public final tl2<T> a;
    public final Handler b;
    public Runnable c;
    public T d;

    /* compiled from: QuietLiveDataObserver.kt */
    /* renamed from: qx2$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
    }

    public qx2(tl2<T> tl2Var) {
        fs1.f(tl2Var, "origin");
        this.a = tl2Var;
        this.b = new Handler(Looper.getMainLooper());
    }

    public static final void c(qx2 qx2Var) {
        fs1.f(qx2Var, "this$0");
        tl2<T> tl2Var = qx2Var.a;
        T t = qx2Var.d;
        fs1.d(t);
        tl2Var.onChanged(t);
        qx2Var.c = null;
    }

    public final boolean b() {
        return this.d == null;
    }

    @Override // defpackage.tl2
    public void onChanged(T t) {
        if (b()) {
            this.d = t;
            this.a.onChanged(t);
            return;
        }
        this.d = t;
        Runnable runnable = this.c;
        if (runnable != null) {
            Handler handler = this.b;
            fs1.d(runnable);
            handler.removeCallbacks(runnable);
        }
        Runnable runnable2 = new Runnable() { // from class: px2
            @Override // java.lang.Runnable
            public final void run() {
                qx2.c(qx2.this);
            }
        };
        this.c = runnable2;
        Handler handler2 = this.b;
        fs1.d(runnable2);
        handler2.postDelayed(runnable2, 500L);
    }
}
