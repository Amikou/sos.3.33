package defpackage;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Process;
import android.os.SystemClock;
import com.google.firebase.messaging.d;
import com.google.firebase.messaging.f;
import defpackage.dh2;
import defpackage.f30;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: wp0  reason: default package */
/* loaded from: classes2.dex */
public class wp0 {
    public final Executor a;
    public final Context b;
    public final f c;

    public wp0(Context context, f fVar, Executor executor) {
        this.a = executor;
        this.b = context;
        this.c = fVar;
    }

    public boolean a() {
        if (this.c.a("gcm.n.noui")) {
            return true;
        }
        if (b()) {
            return false;
        }
        d d = d();
        f30.a d2 = f30.d(this.b, this.c);
        e(d2.a, d);
        c(d2);
        return true;
    }

    public final boolean b() {
        if (((KeyguardManager) this.b.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            return false;
        }
        if (!jr2.f()) {
            SystemClock.sleep(10L);
        }
        int myPid = Process.myPid();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.b.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    if (next.importance == 100) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public final void c(f30.a aVar) {
        ((NotificationManager) this.b.getSystemService("notification")).notify(aVar.b, aVar.c, aVar.a.b());
    }

    public final d d() {
        d c = d.c(this.c.p("gcm.n.image"));
        if (c != null) {
            c.e(this.a);
        }
        return c;
    }

    public final void e(dh2.e eVar, d dVar) {
        if (dVar == null) {
            return;
        }
        try {
            Bitmap bitmap = (Bitmap) com.google.android.gms.tasks.d.b(dVar.d(), 5L, TimeUnit.SECONDS);
            eVar.t(bitmap);
            eVar.C(new dh2.b().i(bitmap).h(null));
        } catch (InterruptedException unused) {
            dVar.close();
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            String valueOf = String.valueOf(e.getCause());
            StringBuilder sb = new StringBuilder(valueOf.length() + 26);
            sb.append("Failed to download image: ");
            sb.append(valueOf);
        } catch (TimeoutException unused2) {
            dVar.close();
        }
    }
}
