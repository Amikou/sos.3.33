package defpackage;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;

/* compiled from: ViewUtils.java */
/* renamed from: nk4  reason: default package */
/* loaded from: classes.dex */
public class nk4 {
    public static final uk4 a;
    public static final Property<View, Float> b;
    public static final Property<View, Rect> c;

    /* compiled from: ViewUtils.java */
    /* renamed from: nk4$a */
    /* loaded from: classes.dex */
    public static class a extends Property<View, Float> {
        public a(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(nk4.c(view));
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, Float f) {
            nk4.h(view, f.floatValue());
        }
    }

    /* compiled from: ViewUtils.java */
    /* renamed from: nk4$b */
    /* loaded from: classes.dex */
    public static class b extends Property<View, Rect> {
        public b(Class cls, String str) {
            super(cls, str);
        }

        @Override // android.util.Property
        /* renamed from: a */
        public Rect get(View view) {
            return ei4.w(view);
        }

        @Override // android.util.Property
        /* renamed from: b */
        public void set(View view, Rect rect) {
            ei4.z0(view, rect);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            a = new tk4();
        } else if (i >= 23) {
            a = new sk4();
        } else if (i >= 22) {
            a = new rk4();
        } else if (i >= 21) {
            a = new qk4();
        } else if (i >= 19) {
            a = new pk4();
        } else {
            a = new uk4();
        }
        b = new a(Float.class, "translationAlpha");
        c = new b(Rect.class, "clipBounds");
    }

    public static void a(View view) {
        a.a(view);
    }

    public static qj4 b(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new oj4(view);
        }
        return lj4.e(view);
    }

    public static float c(View view) {
        return a.c(view);
    }

    public static ip4 d(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new hp4(view);
        }
        return new gp4(view.getWindowToken());
    }

    public static void e(View view) {
        a.d(view);
    }

    public static void f(View view, Matrix matrix) {
        a.e(view, matrix);
    }

    public static void g(View view, int i, int i2, int i3, int i4) {
        a.f(view, i, i2, i3, i4);
    }

    public static void h(View view, float f) {
        a.g(view, f);
    }

    public static void i(View view, int i) {
        a.h(view, i);
    }

    public static void j(View view, Matrix matrix) {
        a.i(view, matrix);
    }

    public static void k(View view, Matrix matrix) {
        a.j(view, matrix);
    }
}
