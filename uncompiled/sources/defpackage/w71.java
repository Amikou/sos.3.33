package defpackage;

import java.util.concurrent.Callable;

/* compiled from: FlowableError.java */
/* renamed from: w71  reason: default package */
/* loaded from: classes2.dex */
public final class w71<T> extends q71<T> {
    public final Callable<? extends Throwable> b;

    public w71(Callable<? extends Throwable> callable) {
        this.b = callable;
    }
}
