package defpackage;

import java.util.Set;

/* compiled from: OSSharedPreferences.java */
/* renamed from: vk2  reason: default package */
/* loaded from: classes2.dex */
public interface vk2 {
    void a(String str, String str2, int i);

    void b(String str, String str2, boolean z);

    Set<String> c(String str, String str2, Set<String> set);

    int d(String str, String str2, int i);

    String e(String str, String str2, String str3);

    String f();

    void g(String str, String str2, Set<String> set);

    String h();

    void i(String str, String str2, String str3);

    boolean j(String str, String str2, boolean z);
}
