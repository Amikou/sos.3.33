package defpackage;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AnimatedImageFactoryImpl.java */
/* renamed from: wd  reason: default package */
/* loaded from: classes.dex */
public class wd implements vd {
    public static ud c = g("com.facebook.animated.gif.GifImage");
    public static ud d = g("com.facebook.animated.webp.WebPImage");
    public final od a;
    public final br2 b;

    /* compiled from: AnimatedImageFactoryImpl.java */
    /* renamed from: wd$a */
    /* loaded from: classes.dex */
    public class a implements AnimatedImageCompositor.b {
        public a(wd wdVar) {
        }

        @Override // com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor.b
        public void a(int i, Bitmap bitmap) {
        }

        @Override // com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor.b
        public com.facebook.common.references.a<Bitmap> b(int i) {
            return null;
        }
    }

    /* compiled from: AnimatedImageFactoryImpl.java */
    /* renamed from: wd$b */
    /* loaded from: classes.dex */
    public class b implements AnimatedImageCompositor.b {
        public final /* synthetic */ List a;

        public b(wd wdVar, List list) {
            this.a = list;
        }

        @Override // com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor.b
        public void a(int i, Bitmap bitmap) {
        }

        @Override // com.facebook.imagepipeline.animated.impl.AnimatedImageCompositor.b
        public com.facebook.common.references.a<Bitmap> b(int i) {
            return com.facebook.common.references.a.e((com.facebook.common.references.a) this.a.get(i));
        }
    }

    public wd(od odVar, br2 br2Var) {
        this.a = odVar;
        this.b = br2Var;
    }

    public static ud g(String str) {
        try {
            return (ud) Class.forName(str).newInstance();
        } catch (Throwable unused) {
            return null;
        }
    }

    @Override // defpackage.vd
    public com.facebook.imagepipeline.image.a a(zu0 zu0Var, rn1 rn1Var, Bitmap.Config config) {
        td d2;
        if (d != null) {
            com.facebook.common.references.a<PooledByteBuffer> e = zu0Var.e();
            xt2.g(e);
            try {
                PooledByteBuffer j = e.j();
                if (j.t() != null) {
                    d2 = d.e(j.t(), rn1Var);
                } else {
                    d2 = d.d(j.y(), j.size(), rn1Var);
                }
                return f(rn1Var, d2, config);
            } finally {
                com.facebook.common.references.a.g(e);
            }
        }
        throw new UnsupportedOperationException("To encode animated webp please add the dependency to the animated-webp module");
    }

    @Override // defpackage.vd
    public com.facebook.imagepipeline.image.a b(zu0 zu0Var, rn1 rn1Var, Bitmap.Config config) {
        td d2;
        if (c != null) {
            com.facebook.common.references.a<PooledByteBuffer> e = zu0Var.e();
            xt2.g(e);
            try {
                PooledByteBuffer j = e.j();
                if (j.t() != null) {
                    d2 = c.e(j.t(), rn1Var);
                } else {
                    d2 = c.d(j.y(), j.size(), rn1Var);
                }
                return f(rn1Var, d2, config);
            } finally {
                com.facebook.common.references.a.g(e);
            }
        }
        throw new UnsupportedOperationException("To encode animated gif please add the dependency to the animated-gif module");
    }

    @SuppressLint({"NewApi"})
    public final com.facebook.common.references.a<Bitmap> c(int i, int i2, Bitmap.Config config) {
        com.facebook.common.references.a<Bitmap> c2 = this.b.c(i, i2, config);
        c2.j().eraseColor(0);
        if (Build.VERSION.SDK_INT >= 12) {
            c2.j().setHasAlpha(true);
        }
        return c2;
    }

    public final com.facebook.common.references.a<Bitmap> d(td tdVar, Bitmap.Config config, int i) {
        com.facebook.common.references.a<Bitmap> c2 = c(tdVar.getWidth(), tdVar.getHeight(), config);
        new AnimatedImageCompositor(this.a.a(yd.b(tdVar), null), new a(this)).g(i, c2.j());
        return c2;
    }

    public final List<com.facebook.common.references.a<Bitmap>> e(td tdVar, Bitmap.Config config) {
        kd a2 = this.a.a(yd.b(tdVar), null);
        ArrayList arrayList = new ArrayList(a2.a());
        AnimatedImageCompositor animatedImageCompositor = new AnimatedImageCompositor(a2, new b(this, arrayList));
        for (int i = 0; i < a2.a(); i++) {
            com.facebook.common.references.a<Bitmap> c2 = c(a2.getWidth(), a2.getHeight(), config);
            animatedImageCompositor.g(i, c2.j());
            arrayList.add(c2);
        }
        return arrayList;
    }

    public final com.facebook.imagepipeline.image.a f(rn1 rn1Var, td tdVar, Bitmap.Config config) {
        List<com.facebook.common.references.a<Bitmap>> list;
        com.facebook.common.references.a<Bitmap> aVar = null;
        try {
            int a2 = rn1Var.d ? tdVar.a() - 1 : 0;
            if (rn1Var.f) {
                c00 c00Var = new c00(d(tdVar, config, a2), jp1.d, 0);
                com.facebook.common.references.a.g(null);
                com.facebook.common.references.a.h(null);
                return c00Var;
            }
            if (rn1Var.e) {
                list = e(tdVar, config);
                try {
                    aVar = com.facebook.common.references.a.e(list.get(a2));
                } catch (Throwable th) {
                    th = th;
                    com.facebook.common.references.a.g(aVar);
                    com.facebook.common.references.a.h(list);
                    throw th;
                }
            } else {
                list = null;
            }
            if (rn1Var.c && aVar == null) {
                aVar = d(tdVar, config, a2);
            }
            vz vzVar = new vz(yd.e(tdVar).j(aVar).i(a2).h(list).g(rn1Var.j).a());
            com.facebook.common.references.a.g(aVar);
            com.facebook.common.references.a.h(list);
            return vzVar;
        } catch (Throwable th2) {
            th = th2;
            list = null;
        }
    }
}
