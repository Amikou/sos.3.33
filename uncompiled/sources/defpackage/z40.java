package defpackage;

import android.content.Intent;
import android.os.Bundle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import zendesk.configurations.Configuration;

/* compiled from: ConfigurationHelper.java */
/* renamed from: z40  reason: default package */
/* loaded from: classes3.dex */
public class z40 {
    public static String a = "ZENDESK_CONFIGURATION";
    public static z40 b = new z40();

    public static z40 h() {
        return b;
    }

    public List<Configuration> a(List<Configuration> list, Configuration configuration) {
        ArrayList arrayList = new ArrayList(list);
        if (e(list, configuration.getClass()) == null) {
            arrayList.add(configuration);
        }
        return arrayList;
    }

    public void b(Bundle bundle, Configuration configuration) {
        bundle.putSerializable(a, configuration);
    }

    public void c(Intent intent, Configuration configuration) {
        intent.putExtra(a, configuration);
    }

    public void d(Map<String, Object> map, Configuration configuration) {
        map.put(a, configuration);
    }

    public <E extends Configuration> E e(List<Configuration> list, Class<E> cls) {
        Iterator<Configuration> it = list.iterator();
        while (it.hasNext()) {
            E e = (E) it.next();
            if (cls.isInstance(e)) {
                return e;
            }
        }
        return null;
    }

    public <E extends Configuration> E f(Bundle bundle, Class<E> cls) {
        if (bundle == null || !bundle.containsKey(a)) {
            return null;
        }
        Serializable serializable = bundle.getSerializable(a);
        if (cls.isInstance(serializable)) {
            return (E) serializable;
        }
        return null;
    }

    public <E extends Configuration> E g(Map<String, Object> map, Class<E> cls) {
        if (map == null || !map.containsKey(a)) {
            return null;
        }
        Object obj = map.get(a);
        if (cls.isInstance(obj)) {
            return (E) obj;
        }
        return null;
    }
}
