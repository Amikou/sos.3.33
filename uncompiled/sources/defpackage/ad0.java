package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: CutCornerTreatment.java */
/* renamed from: ad0  reason: default package */
/* loaded from: classes2.dex */
public class ad0 extends w80 {
    public float a = -1.0f;

    @Override // defpackage.w80
    public void a(rn3 rn3Var, float f, float f2, float f3) {
        rn3Var.o(Utils.FLOAT_EPSILON, f3 * f2, 180.0f, 180.0f - f);
        double d = f3;
        double d2 = f2;
        rn3Var.m((float) (Math.sin(Math.toRadians(f)) * d * d2), (float) (Math.sin(Math.toRadians(90.0f - f)) * d * d2));
    }
}
