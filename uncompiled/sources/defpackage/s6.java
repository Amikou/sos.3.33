package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import net.safemoon.androidwallet.R;

/* compiled from: ActivityAktImportExistingWalletsBinding.java */
/* renamed from: s6  reason: default package */
/* loaded from: classes2.dex */
public final class s6 {
    public final ConstraintLayout a;
    public final MaterialButton b;
    public final MaterialButton c;
    public final MaterialButton d;
    public final MaterialCheckBox e;
    public final LinearLayoutCompat f;
    public final sp3 g;
    public final TextView h;
    public final TextView i;
    public final TextView j;
    public final TextView k;

    public s6(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialCheckBox materialCheckBox, LinearLayout linearLayout, LinearLayoutCompat linearLayoutCompat, AppCompatImageView appCompatImageView, sp3 sp3Var, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
        this.a = constraintLayout;
        this.b = materialButton;
        this.c = materialButton2;
        this.d = materialButton3;
        this.e = materialCheckBox;
        this.f = linearLayoutCompat;
        this.g = sp3Var;
        this.h = textView;
        this.i = textView2;
        this.j = textView3;
        this.k = textView4;
    }

    public static s6 a(View view) {
        int i = R.id.btnContinue;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnContinue);
        if (materialButton != null) {
            i = R.id.btnNo;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnNo);
            if (materialButton2 != null) {
                i = R.id.btnYes;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btnYes);
                if (materialButton3 != null) {
                    i = R.id.chkTermsAgree;
                    MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.chkTermsAgree);
                    if (materialCheckBox != null) {
                        i = R.id.content_layout;
                        LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.content_layout);
                        if (linearLayout != null) {
                            i = R.id.contentWrapper;
                            LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) ai4.a(view, R.id.contentWrapper);
                            if (linearLayoutCompat != null) {
                                i = R.id.img_logo;
                                AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.img_logo);
                                if (appCompatImageView != null) {
                                    i = R.id.toolbar;
                                    View a = ai4.a(view, R.id.toolbar);
                                    if (a != null) {
                                        sp3 a2 = sp3.a(a);
                                        i = R.id.tvExistingWalletsHeader;
                                        TextView textView = (TextView) ai4.a(view, R.id.tvExistingWalletsHeader);
                                        if (textView != null) {
                                            i = R.id.tvLoginConfirm;
                                            TextView textView2 = (TextView) ai4.a(view, R.id.tvLoginConfirm);
                                            if (textView2 != null) {
                                                i = R.id.tvLoginNote;
                                                TextView textView3 = (TextView) ai4.a(view, R.id.tvLoginNote);
                                                if (textView3 != null) {
                                                    i = R.id.tvNoticeContent;
                                                    TextView textView4 = (TextView) ai4.a(view, R.id.tvNoticeContent);
                                                    if (textView4 != null) {
                                                        return new s6((ConstraintLayout) view, materialButton, materialButton2, materialButton3, materialCheckBox, linearLayout, linearLayoutCompat, appCompatImageView, a2, textView, textView2, textView3, textView4);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static s6 c(LayoutInflater layoutInflater) {
        return d(layoutInflater, null, false);
    }

    public static s6 d(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.activity_akt_import_existing_wallets, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
