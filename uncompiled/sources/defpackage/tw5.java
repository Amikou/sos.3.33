package defpackage;

import com.google.android.gms.internal.measurement.l2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: tw5  reason: default package */
/* loaded from: classes.dex */
public final class tw5 extends ww5 {
    public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

    public /* synthetic */ tw5(qw5 qw5Var) {
        super(null);
    }

    @Override // defpackage.ww5
    public final void a(Object obj, long j) {
        Object unmodifiableList;
        List list = (List) l2.s(obj, j);
        if (list instanceof nw5) {
            unmodifiableList = ((nw5) list).g();
        } else if (c.isAssignableFrom(list.getClass())) {
            return;
        } else {
            if ((list instanceof hy5) && (list instanceof zv5)) {
                zv5 zv5Var = (zv5) list;
                if (zv5Var.zza()) {
                    zv5Var.zzb();
                    return;
                }
                return;
            }
            unmodifiableList = Collections.unmodifiableList(list);
        }
        l2.t(obj, j, unmodifiableList);
    }

    @Override // defpackage.ww5
    public final <E> void b(Object obj, Object obj2, long j) {
        ArrayList arrayList;
        List list = (List) l2.s(obj2, j);
        int size = list.size();
        List list2 = (List) l2.s(obj, j);
        if (list2.isEmpty()) {
            if (list2 instanceof nw5) {
                list2 = new kw5(size);
            } else if ((list2 instanceof hy5) && (list2 instanceof zv5)) {
                list2 = ((zv5) list2).Q(size);
            } else {
                list2 = new ArrayList(size);
            }
            l2.t(obj, j, list2);
        } else {
            if (c.isAssignableFrom(list2.getClass())) {
                ArrayList arrayList2 = new ArrayList(list2.size() + size);
                arrayList2.addAll(list2);
                l2.t(obj, j, arrayList2);
                arrayList = arrayList2;
            } else if (list2 instanceof nz5) {
                kw5 kw5Var = new kw5(list2.size() + size);
                kw5Var.addAll(kw5Var.size(), (nz5) list2);
                l2.t(obj, j, kw5Var);
                arrayList = kw5Var;
            } else if ((list2 instanceof hy5) && (list2 instanceof zv5)) {
                zv5 zv5Var = (zv5) list2;
                if (!zv5Var.zza()) {
                    list2 = zv5Var.Q(list2.size() + size);
                    l2.t(obj, j, list2);
                }
            }
            list2 = arrayList;
        }
        int size2 = list2.size();
        int size3 = list.size();
        if (size2 > 0 && size3 > 0) {
            list2.addAll(list);
        }
        if (size2 > 0) {
            list = list2;
        }
        l2.t(obj, j, list);
    }
}
