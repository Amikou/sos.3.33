package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: IncludeMoneyBagPercentageBinding.java */
/* renamed from: aq1  reason: default package */
/* loaded from: classes2.dex */
public final class aq1 {
    public final AppCompatSeekBar a;
    public final MaterialButton b;
    public final MaterialButton c;

    public aq1(LinearLayoutCompat linearLayoutCompat, AppCompatSeekBar appCompatSeekBar, MaterialButton materialButton, MaterialButton materialButton2, AppCompatTextView appCompatTextView) {
        this.a = appCompatSeekBar;
        this.b = materialButton;
        this.c = materialButton2;
    }

    public static aq1 a(View view) {
        int i = R.id.adjustPercentage;
        AppCompatSeekBar appCompatSeekBar = (AppCompatSeekBar) ai4.a(view, R.id.adjustPercentage);
        if (appCompatSeekBar != null) {
            i = R.id.btnConfirm;
            MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnConfirm);
            if (materialButton != null) {
                i = R.id.txtTextPercentage;
                MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.txtTextPercentage);
                if (materialButton2 != null) {
                    i = R.id.txtTitle;
                    AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtTitle);
                    if (appCompatTextView != null) {
                        return new aq1((LinearLayoutCompat) view, appCompatSeekBar, materialButton, materialButton2, appCompatTextView);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
