package defpackage;

import android.content.Context;
import com.google.android.gms.clearcut.zze;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.b;
import com.google.android.gms.internal.clearcut.p0;

/* renamed from: kf5  reason: default package */
/* loaded from: classes.dex */
public final class kf5 extends b<Object> implements j75 {
    public kf5(Context context) {
        super(context, (a<a.d>) com.google.android.gms.clearcut.a.o, (a.d) null, (nt3) new ye());
    }

    public static j75 m(Context context) {
        return new kf5(context);
    }

    @Override // defpackage.j75
    public final gq2<Status> a(zze zzeVar) {
        return e(new p0(zzeVar, c()));
    }
}
