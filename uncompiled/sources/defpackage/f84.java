package defpackage;

import androidx.media3.common.g;
import androidx.media3.common.j;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: TrackOutput.java */
/* renamed from: f84  reason: default package */
/* loaded from: classes.dex */
public interface f84 {

    /* compiled from: TrackOutput.java */
    /* renamed from: f84$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;
        public final byte[] b;
        public final int c;
        public final int d;

        public a(int i, byte[] bArr, int i2, int i3) {
            this.a = i;
            this.b = bArr;
            this.c = i2;
            this.d = i3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.c == aVar.c && this.d == aVar.d && Arrays.equals(this.b, aVar.b);
        }

        public int hashCode() {
            return (((((this.a * 31) + Arrays.hashCode(this.b)) * 31) + this.c) * 31) + this.d;
        }
    }

    void a(op2 op2Var, int i);

    void b(long j, int i, int i2, int i3, a aVar);

    void c(op2 op2Var, int i, int i2);

    int d(g gVar, int i, boolean z) throws IOException;

    int e(g gVar, int i, boolean z, int i2) throws IOException;

    void f(j jVar);
}
