package defpackage;

import androidx.media3.common.g;
import androidx.media3.common.util.b;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Arrays;

/* compiled from: DefaultExtractorInput.java */
/* renamed from: hj0  reason: default package */
/* loaded from: classes.dex */
public final class hj0 implements q11 {
    public final g b;
    public final long c;
    public long d;
    public int f;
    public int g;
    public byte[] e = new byte[65536];
    public final byte[] a = new byte[4096];

    static {
        f62.a("media3.extractor");
    }

    public hj0(g gVar, long j, long j2) {
        this.b = gVar;
        this.d = j;
        this.c = j2;
    }

    @Override // defpackage.q11
    public boolean a(byte[] bArr, int i, int i2, boolean z) throws IOException {
        int q = q(bArr, i, i2);
        while (q < i2 && q != -1) {
            q = r(bArr, i, i2, q, z);
        }
        o(q);
        return q != -1;
    }

    @Override // defpackage.q11
    public boolean d(byte[] bArr, int i, int i2, boolean z) throws IOException {
        if (l(i2, z)) {
            System.arraycopy(this.e, this.f - i2, bArr, i, i2);
            return true;
        }
        return false;
    }

    @Override // defpackage.q11
    public long e() {
        return this.d + this.f;
    }

    @Override // defpackage.q11
    public void f(int i) throws IOException {
        l(i, false);
    }

    @Override // defpackage.q11
    public int g(int i) throws IOException {
        int s = s(i);
        if (s == 0) {
            byte[] bArr = this.a;
            s = r(bArr, 0, Math.min(i, bArr.length), 0, true);
        }
        o(s);
        return s;
    }

    @Override // defpackage.q11
    public long getLength() {
        return this.c;
    }

    @Override // defpackage.q11
    public long getPosition() {
        return this.d;
    }

    @Override // defpackage.q11
    public int h(byte[] bArr, int i, int i2) throws IOException {
        int min;
        p(i2);
        int i3 = this.g;
        int i4 = this.f;
        int i5 = i3 - i4;
        if (i5 == 0) {
            min = r(this.e, i4, i2, 0, true);
            if (min == -1) {
                return -1;
            }
            this.g += min;
        } else {
            min = Math.min(i2, i5);
        }
        System.arraycopy(this.e, this.f, bArr, i, min);
        this.f += min;
        return min;
    }

    @Override // defpackage.q11
    public void j() {
        this.f = 0;
    }

    @Override // defpackage.q11
    public void k(int i) throws IOException {
        t(i, false);
    }

    @Override // defpackage.q11
    public boolean l(int i, boolean z) throws IOException {
        p(i);
        int i2 = this.g - this.f;
        while (i2 < i) {
            i2 = r(this.e, this.f, i, i2, z);
            if (i2 == -1) {
                return false;
            }
            this.g = this.f + i2;
        }
        this.f += i;
        return true;
    }

    @Override // defpackage.q11
    public void n(byte[] bArr, int i, int i2) throws IOException {
        d(bArr, i, i2, false);
    }

    public final void o(int i) {
        if (i != -1) {
            this.d += i;
        }
    }

    public final void p(int i) {
        int i2 = this.f + i;
        byte[] bArr = this.e;
        if (i2 > bArr.length) {
            this.e = Arrays.copyOf(this.e, b.q(bArr.length * 2, 65536 + i2, i2 + 524288));
        }
    }

    public final int q(byte[] bArr, int i, int i2) {
        int i3 = this.g;
        if (i3 == 0) {
            return 0;
        }
        int min = Math.min(i3, i2);
        System.arraycopy(this.e, 0, bArr, i, min);
        u(min);
        return min;
    }

    public final int r(byte[] bArr, int i, int i2, int i3, boolean z) throws IOException {
        if (!Thread.interrupted()) {
            int read = this.b.read(bArr, i + i3, i2 - i3);
            if (read == -1) {
                if (i3 == 0 && z) {
                    return -1;
                }
                throw new EOFException();
            }
            return i3 + read;
        }
        throw new InterruptedIOException();
    }

    @Override // defpackage.q11, androidx.media3.common.g
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int q = q(bArr, i, i2);
        if (q == 0) {
            q = r(bArr, i, i2, 0, true);
        }
        o(q);
        return q;
    }

    @Override // defpackage.q11
    public void readFully(byte[] bArr, int i, int i2) throws IOException {
        a(bArr, i, i2, false);
    }

    public final int s(int i) {
        int min = Math.min(this.g, i);
        u(min);
        return min;
    }

    public boolean t(int i, boolean z) throws IOException {
        int s = s(i);
        while (s < i && s != -1) {
            s = r(this.a, -s, Math.min(i, this.a.length + s), s, z);
        }
        o(s);
        return s != -1;
    }

    public final void u(int i) {
        int i2 = this.g - i;
        this.g = i2;
        this.f = 0;
        byte[] bArr = this.e;
        byte[] bArr2 = i2 < bArr.length - 524288 ? new byte[65536 + i2] : bArr;
        System.arraycopy(bArr, i, bArr2, 0, i2);
        this.e = bArr2;
    }
}
