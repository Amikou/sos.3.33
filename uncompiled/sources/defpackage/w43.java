package defpackage;

import android.content.DialogInterface;
import net.safemoon.androidwallet.activity.RecoverWalletActivity;

/* renamed from: w43  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class w43 implements DialogInterface.OnDismissListener {
    public static final /* synthetic */ w43 a = new w43();

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        RecoverWalletActivity.X(dialogInterface);
    }
}
