package defpackage;

/* compiled from: Function.java */
/* renamed from: ld1  reason: default package */
/* loaded from: classes2.dex */
public interface ld1<T, R> {
    R apply(T t) throws Exception;
}
