package defpackage;

import android.annotation.SuppressLint;
import java.util.HashSet;
import java.util.Set;

/* compiled from: AppBarConfiguration.java */
/* renamed from: af  reason: default package */
/* loaded from: classes.dex */
public final class af {
    public final Set<Integer> a;
    public final jn2 b;

    /* compiled from: AppBarConfiguration.java */
    /* renamed from: af$b */
    /* loaded from: classes.dex */
    public static final class b {
        public final Set<Integer> a = new HashSet();
        public jn2 b;
        public c c;

        public b(int... iArr) {
            for (int i : iArr) {
                this.a.add(Integer.valueOf(i));
            }
        }

        @SuppressLint({"SyntheticAccessor"})
        public af a() {
            return new af(this.a, this.b, this.c);
        }
    }

    /* compiled from: AppBarConfiguration.java */
    /* renamed from: af$c */
    /* loaded from: classes.dex */
    public interface c {
    }

    public jn2 a() {
        return this.b;
    }

    public Set<Integer> b() {
        return this.a;
    }

    public af(Set<Integer> set, jn2 jn2Var, c cVar) {
        this.a = set;
        this.b = jn2Var;
    }
}
