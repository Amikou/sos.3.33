package defpackage;

/* compiled from: BIG.java */
/* renamed from: zl  reason: default package */
/* loaded from: classes2.dex */
public class zl {
    public long[] a;

    public zl() {
        this.a = new long[5];
        for (int i = 0; i < 5; i++) {
            this.a[i] = 0;
        }
    }

    public static hd0 E(zl zlVar) {
        hd0 hd0Var = new hd0(0);
        int i = 0;
        while (i < 5) {
            int i2 = i + 1;
            long j = 0;
            for (int i3 = i2; i3 < 5; i3++) {
                long[] jArr = zlVar.a;
                int i4 = i + i3;
                long[] s = s(2 * jArr[i], jArr[i3], j, hd0Var.a[i4]);
                j = s[0];
                hd0Var.a[i4] = s[1];
            }
            hd0Var.a[i + 5] = j;
            i = i2;
        }
        for (int i5 = 0; i5 < 5; i5++) {
            long[] jArr2 = zlVar.a;
            int i6 = i5 * 2;
            long[] s2 = s(jArr2[i5], jArr2[i5], 0L, hd0Var.a[i6]);
            long[] jArr3 = hd0Var.a;
            int i7 = i6 + 1;
            jArr3[i7] = jArr3[i7] + s2[0];
            jArr3[i6] = s2[1];
        }
        hd0Var.b();
        return hd0Var;
    }

    public static int F(zl zlVar, zl zlVar2, zl zlVar3) {
        long[] jArr = zlVar3.a;
        jArr[0] = (jArr[0] >> 1) | ((jArr[1] << 55) & 72057594037927935L);
        long[] jArr2 = zlVar.a;
        jArr2[0] = zlVar2.a[0] - jArr[0];
        long j = jArr2[0] >> 56;
        jArr2[0] = jArr2[0] & 72057594037927935L;
        int i = 1;
        while (i < 4) {
            long[] jArr3 = zlVar3.a;
            int i2 = i + 1;
            jArr3[i] = (jArr3[i] >> 1) | ((jArr3[i2] << 55) & 72057594037927935L);
            long[] jArr4 = zlVar.a;
            jArr4[i] = (zlVar2.a[i] - jArr3[i]) + j;
            j = jArr4[i] >> 56;
            jArr4[i] = jArr4[i] & 72057594037927935L;
            i = i2;
        }
        long[] jArr5 = zlVar3.a;
        jArr5[4] = jArr5[4] >> 1;
        long[] jArr6 = zlVar.a;
        jArr6[4] = (zlVar2.a[4] - jArr5[4]) + j;
        return (int) ((jArr6[4] >> 63) & 1);
    }

    public static long b(int i) {
        return i;
    }

    public static int d(zl zlVar, zl zlVar2) {
        long j = 0;
        long j2 = 1;
        for (int i = 4; i >= 0; i--) {
            long[] jArr = zlVar2.a;
            long j3 = jArr[i];
            long[] jArr2 = zlVar.a;
            j |= ((j3 - jArr2[i]) >> 56) & j2;
            j2 &= ((jArr[i] ^ jArr2[i]) - 1) >> 56;
        }
        return (int) (((j + j) + j2) - 1);
    }

    public static zl h(byte[] bArr) {
        return i(bArr, 0);
    }

    public static zl i(byte[] bArr, int i) {
        zl zlVar = new zl(0);
        for (int i2 = 0; i2 < 32; i2++) {
            zlVar.j(8);
            long[] jArr = zlVar.a;
            jArr[0] = jArr[0] + (bArr[i2 + i] & 255);
        }
        return zlVar;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0057 A[LOOP:1: B:19:0x0049->B:21:0x0057, LOOP_END] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static defpackage.zl q(defpackage.zl r17, long r18, defpackage.hd0 r20) {
        /*
            r0 = r20
            r1 = 0
            r2 = r1
        L4:
            r3 = 5
            if (r2 < r3) goto L1f
            zl r4 = new zl
            r4.<init>(r1)
        Lc:
            if (r1 < r3) goto L12
            r4.v()
            return r4
        L12:
            long[] r2 = r4.a
            long[] r5 = r0.a
            int r6 = r1 + 5
            r6 = r5[r6]
            r2[r1] = r6
            int r1 = r1 + 1
            goto Lc
        L1f:
            r4 = -1
            int r4 = (r18 > r4 ? 1 : (r18 == r4 ? 0 : -1))
            r5 = 72057594037927935(0xffffffffffffff, double:7.291122019556397E-304)
            if (r4 != 0) goto L32
            long[] r4 = r0.a
            r7 = r4[r2]
            long r7 = -r7
        L2f:
            long r4 = r7 & r5
            goto L45
        L32:
            r7 = 1
            int r4 = (r18 > r7 ? 1 : (r18 == r7 ? 0 : -1))
            if (r4 != 0) goto L3e
            long[] r4 = r0.a
            r5 = r4[r2]
            r4 = r5
            goto L45
        L3e:
            long[] r4 = r0.a
            r7 = r4[r2]
            long r7 = r7 * r18
            goto L2f
        L45:
            r6 = 0
            r14 = r1
            r10 = r6
        L49:
            if (r14 < r3) goto L57
            long[] r3 = r0.a
            int r4 = r2 + 5
            r5 = r3[r4]
            long r5 = r5 + r10
            r3[r4] = r5
            int r2 = r2 + 1
            goto L4
        L57:
            r15 = r17
            long[] r6 = r15.a
            r8 = r6[r14]
            long[] r6 = r0.a
            int r16 = r2 + r14
            r12 = r6[r16]
            r6 = r4
            long[] r6 = s(r6, r8, r10, r12)
            r10 = r6[r1]
            long[] r7 = r0.a
            r8 = 1
            r8 = r6[r8]
            r7[r16] = r8
            int r14 = r14 + 1
            goto L49
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.zl.q(zl, long, hd0):zl");
    }

    public static hd0 r(zl zlVar, zl zlVar2) {
        hd0 hd0Var = new hd0(0);
        for (int i = 0; i < 5; i++) {
            long j = 0;
            for (int i2 = 0; i2 < 5; i2++) {
                int i3 = i + i2;
                long[] s = s(zlVar.a[i], zlVar2.a[i2], j, hd0Var.a[i3]);
                j = s[0];
                hd0Var.a[i3] = s[1];
            }
            hd0Var.a[i + 5] = j;
        }
        return hd0Var;
    }

    public static long[] s(long j, long j2, long j3, long j4) {
        long t = t(j, j2);
        long j5 = j * j2;
        long j6 = (j5 & 72057594037927935L) + j3 + j4;
        return new long[]{((j5 >>> 56) | (t << 8)) + (j6 >>> 56), 72057594037927935L & j6};
    }

    public static long t(long j, long j2) {
        if (j >= 0 && j2 >= 0) {
            long j3 = j >>> 32;
            long j4 = j2 >>> 32;
            long j5 = j & 4294967295L;
            long j6 = j2 & 4294967295L;
            long j7 = j3 * j4;
            long j8 = j5 * j6;
            return (((j8 >>> 32) + ((((j3 + j5) * (j4 + j6)) - j7) - j8)) >>> 32) + j7;
        }
        long j9 = j >> 32;
        long j10 = j & 4294967295L;
        long j11 = j2 >> 32;
        long j12 = j2 & 4294967295L;
        long j13 = (j12 * j9) + ((j10 * j12) >>> 32);
        return (j9 * j11) + (j13 >> 32) + (((j13 & 4294967295L) + (j10 * j11)) >> 32);
    }

    public long A(int i) {
        long j = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            long[] jArr = this.a;
            long j2 = jArr[i2];
            jArr[i2] = 0;
            long[] s = s(j2, i, j, jArr[i2]);
            j = s[0];
            this.a[i2] = s[1];
        }
        return j;
    }

    public void B(zl zlVar) {
        for (int i = 0; i < 5; i++) {
            long[] jArr = this.a;
            jArr[i] = zlVar.a[i] - jArr[i];
        }
    }

    public void C(int i) {
        int i2 = i % 56;
        int i3 = i / 56;
        long[] jArr = this.a;
        jArr[4] = jArr[4 - i3] << i2;
        if (5 >= i3 + 2) {
            jArr[4] = jArr[4] | (jArr[(5 - i3) - 2] >> (56 - i2));
        }
        for (int i4 = 3; i4 > i3; i4--) {
            long[] jArr2 = this.a;
            int i5 = i4 - i3;
            jArr2[i4] = (72057594037927935L & (jArr2[i5] << i2)) | (jArr2[i5 - 1] >> (56 - i2));
        }
        long[] jArr3 = this.a;
        jArr3[i3] = 72057594037927935L & (jArr3[0] << i2);
        for (int i6 = 0; i6 < i3; i6++) {
            this.a[i6] = 0;
        }
    }

    public void D(int i) {
        int i2;
        int i3;
        int i4 = i % 56;
        int i5 = i / 56;
        int i6 = 0;
        while (true) {
            i2 = 5 - i5;
            i3 = i2 - 1;
            if (i6 >= i3) {
                break;
            }
            long[] jArr = this.a;
            int i7 = i5 + i6;
            jArr[i6] = (jArr[i7] >> i4) | ((jArr[i7 + 1] << (56 - i4)) & 72057594037927935L);
            i6++;
        }
        if (5 > i5) {
            long[] jArr2 = this.a;
            jArr2[i3] = jArr2[4] >> i4;
        }
        while (i2 < 5) {
            this.a[i2] = 0;
            i2++;
        }
    }

    public void G(zl zlVar) {
        for (int i = 0; i < 5; i++) {
            long[] jArr = this.a;
            jArr[i] = jArr[i] - zlVar.a[i];
        }
    }

    public void H(byte[] bArr) {
        I(bArr, 0);
    }

    public void I(byte[] bArr, int i) {
        zl zlVar = new zl(this);
        zlVar.v();
        for (int i2 = 31; i2 >= 0; i2--) {
            bArr[i2 + i] = (byte) zlVar.a[0];
            zlVar.k(8);
        }
    }

    public void J() {
        for (int i = 0; i < 5; i++) {
            this.a[i] = 0;
        }
    }

    public void a(zl zlVar) {
        for (int i = 0; i < 5; i++) {
            long[] jArr = this.a;
            jArr[i] = jArr[i] + zlVar.a[i];
        }
    }

    public void c(zl zlVar, int i) {
        long j = -i;
        for (int i2 = 0; i2 < 5; i2++) {
            long[] jArr = this.a;
            jArr[i2] = jArr[i2] ^ ((jArr[i2] ^ zlVar.a[i2]) & j);
        }
    }

    public void e(zl zlVar) {
        for (int i = 0; i < 5; i++) {
            this.a[i] = zlVar.a[i];
        }
    }

    public void f(zl zlVar, int i) {
        zl zlVar2 = new zl(0);
        zl zlVar3 = new zl(zlVar);
        v();
        zlVar3.C(i);
        while (true) {
            zlVar2.e(this);
            zlVar2.G(zlVar3);
            zlVar2.v();
            c(zlVar2, (int) (1 - ((zlVar2.a[4] >> 63) & 1)));
            if (i == 0) {
                return;
            }
            zlVar3.k(1);
            i--;
        }
    }

    public void g(int i) {
        v();
        long[] jArr = this.a;
        jArr[0] = jArr[0] - i;
    }

    public int j(int i) {
        long[] jArr = this.a;
        int i2 = 56 - i;
        jArr[4] = (jArr[4] << i) | (jArr[3] >> i2);
        for (int i3 = 3; i3 > 0; i3--) {
            long[] jArr2 = this.a;
            jArr2[i3] = (72057594037927935L & (jArr2[i3] << i)) | (jArr2[i3 - 1] >> i2);
        }
        long[] jArr3 = this.a;
        jArr3[0] = 72057594037927935L & (jArr3[0] << i);
        return (int) (jArr3[4] >> 32);
    }

    public int k(int i) {
        int i2 = 0;
        int b = (int) (this.a[0] & ((b(1) << i) - 1));
        while (i2 < 4) {
            long[] jArr = this.a;
            int i3 = i2 + 1;
            jArr[i2] = (jArr[i2] >> i) | ((jArr[i3] << (56 - i)) & 72057594037927935L);
            i2 = i3;
        }
        long[] jArr2 = this.a;
        jArr2[4] = jArr2[4] >> i;
        return b;
    }

    public void l(int i) {
        v();
        long[] jArr = this.a;
        jArr[0] = jArr[0] + i;
    }

    public boolean m() {
        long j = 0;
        for (int i = 1; i < 5; i++) {
            j |= this.a[i];
        }
        return ((((j - 1) >> 56) & 1) & (((this.a[0] ^ 1) - 1) >> 56)) != 0;
    }

    public boolean n() {
        long j = 0;
        for (int i = 0; i < 5; i++) {
            j |= this.a[i];
        }
        return (((j - 1) >> 56) & 1) != 0;
    }

    public int o(int i) {
        v();
        return ((1 << i) - 1) & ((int) this.a[0]);
    }

    public void p(zl zlVar) {
        int u = u() - zlVar.u();
        if (u < 0) {
            u = 0;
        }
        f(zlVar, u);
    }

    public String toString() {
        zl zlVar;
        int u = u();
        int i = u % 4 == 0 ? u / 4 : (u / 4) + 1;
        if (i < 64) {
            i = 64;
        }
        String str = "";
        for (int i2 = i - 1; i2 >= 0; i2--) {
            new zl(this).D(i2 * 4);
            str = String.valueOf(str) + Long.toHexString(15 & zlVar.a[0]);
        }
        return str;
    }

    public int u() {
        zl zlVar = new zl(this);
        zlVar.v();
        int i = 4;
        while (i >= 0 && zlVar.a[i] == 0) {
            i--;
        }
        if (i < 0) {
            return 0;
        }
        int i2 = i * 56;
        long j = zlVar.a[i];
        while (j != 0) {
            j /= 2;
            i2++;
        }
        return i2;
    }

    public long v() {
        long j = 0;
        for (int i = 0; i < 4; i++) {
            long[] jArr = this.a;
            long j2 = jArr[i] + j;
            jArr[i] = 72057594037927935L & j2;
            j = j2 >> 56;
        }
        long[] jArr2 = this.a;
        jArr2[4] = jArr2[4] + j;
        return jArr2[4] >> 32;
    }

    public void w() {
        this.a[0] = 1;
        for (int i = 1; i < 5; i++) {
            this.a[i] = 0;
        }
    }

    public void x(zl zlVar) {
        for (int i = 0; i < 5; i++) {
            long[] jArr = this.a;
            jArr[i] = jArr[i] | zlVar.a[i];
        }
    }

    public int y() {
        return (int) (this.a[0] % 2);
    }

    public zl z(zl zlVar) {
        zl zlVar2 = new zl(0);
        for (int i = 0; i < 5; i++) {
            zlVar2.a[i] = this.a[i] + zlVar.a[i];
        }
        zlVar2.v();
        return zlVar2;
    }

    public zl(int i) {
        long[] jArr = new long[5];
        this.a = jArr;
        jArr[0] = i;
        for (int i2 = 1; i2 < 5; i2++) {
            this.a[i2] = 0;
        }
    }

    public zl(zl zlVar) {
        this.a = new long[5];
        for (int i = 0; i < 5; i++) {
            this.a[i] = zlVar.a[i];
        }
    }

    public zl(long[] jArr) {
        this.a = new long[5];
        for (int i = 0; i < 5; i++) {
            this.a[i] = jArr[i];
        }
    }
}
