package defpackage;

import android.graphics.Rect;
import android.view.ViewGroup;
import androidx.transition.Transition;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: CircularPropagation.java */
/* renamed from: zy  reason: default package */
/* loaded from: classes.dex */
public class zy extends al4 {
    public float b = 3.0f;

    public static float h(float f, float f2, float f3, float f4) {
        float f5 = f3 - f;
        float f6 = f4 - f2;
        return (float) Math.sqrt((f5 * f5) + (f6 * f6));
    }

    @Override // defpackage.jb4
    public long c(ViewGroup viewGroup, Transition transition, kb4 kb4Var, kb4 kb4Var2) {
        int i;
        int[] iArr;
        int round;
        int i2;
        if (kb4Var == null && kb4Var2 == null) {
            return 0L;
        }
        if (kb4Var2 == null || e(kb4Var) == 0) {
            i = -1;
        } else {
            kb4Var = kb4Var2;
            i = 1;
        }
        int f = f(kb4Var);
        int g = g(kb4Var);
        Rect y = transition.y();
        if (y != null) {
            i2 = y.centerX();
            round = y.centerY();
        } else {
            viewGroup.getLocationOnScreen(new int[2]);
            int round2 = Math.round(iArr[0] + (viewGroup.getWidth() / 2) + viewGroup.getTranslationX());
            round = Math.round(iArr[1] + (viewGroup.getHeight() / 2) + viewGroup.getTranslationY());
            i2 = round2;
        }
        float h = h(f, g, i2, round) / h(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, viewGroup.getWidth(), viewGroup.getHeight());
        long x = transition.x();
        if (x < 0) {
            x = 300;
        }
        return Math.round((((float) (x * i)) / this.b) * h);
    }
}
