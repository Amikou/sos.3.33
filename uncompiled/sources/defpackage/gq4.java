package defpackage;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ExistingWorkPolicy;
import androidx.work.a;
import androidx.work.c;
import androidx.work.d;
import java.util.Collections;
import java.util.List;

/* compiled from: WorkManager.java */
@SuppressLint({"AddedAbstractMethod"})
/* renamed from: gq4  reason: default package */
/* loaded from: classes.dex */
public abstract class gq4 {
    public static gq4 f(Context context) {
        return hq4.m(context);
    }

    public static void g(Context context, a aVar) {
        hq4.g(context, aVar);
    }

    public abstract kn2 a(String str);

    public final kn2 b(d dVar) {
        return c(Collections.singletonList(dVar));
    }

    public abstract kn2 c(List<? extends d> list);

    public kn2 d(String str, ExistingWorkPolicy existingWorkPolicy, c cVar) {
        return e(str, existingWorkPolicy, Collections.singletonList(cVar));
    }

    public abstract kn2 e(String str, ExistingWorkPolicy existingWorkPolicy, List<c> list);
}
