package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.RoomCoinPriceAlert;

/* compiled from: CoinPriceAlertDataSource.kt */
/* renamed from: m00  reason: default package */
/* loaded from: classes2.dex */
public final class m00 {
    public final l00 a;

    public m00(l00 l00Var) {
        fs1.f(l00Var, "dao");
        this.a = l00Var;
    }

    public final Object a(int i, q70<? super te4> q70Var) {
        Object c = this.a.c(i, q70Var);
        return c == gs1.d() ? c : te4.a;
    }

    public final Object b(q70<? super List<RoomCoinPriceAlert>> q70Var) {
        return this.a.b(q70Var);
    }

    public final LiveData<List<RoomCoinPriceAlert>> c() {
        return this.a.a();
    }

    public final Object d(RoomCoinPriceAlert roomCoinPriceAlert, q70<? super te4> q70Var) {
        Object d = this.a.d(roomCoinPriceAlert, q70Var);
        return d == gs1.d() ? d : te4.a;
    }
}
