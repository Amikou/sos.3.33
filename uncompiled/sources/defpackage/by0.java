package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.extractor.metadata.emsg.EventMessage;
import java.nio.ByteBuffer;
import java.util.Arrays;

/* compiled from: EventMessageDecoder.java */
/* renamed from: by0  reason: default package */
/* loaded from: classes.dex */
public final class by0 extends kp3 {
    @Override // defpackage.kp3
    public Metadata b(n82 n82Var, ByteBuffer byteBuffer) {
        return new Metadata(c(new op2(byteBuffer.array(), byteBuffer.limit())));
    }

    public EventMessage c(op2 op2Var) {
        return new EventMessage((String) ii.e(op2Var.x()), (String) ii.e(op2Var.x()), op2Var.w(), op2Var.w(), Arrays.copyOfRange(op2Var.d(), op2Var.e(), op2Var.f()));
    }
}
