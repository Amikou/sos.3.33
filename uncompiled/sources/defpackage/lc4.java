package defpackage;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import androidx.media3.common.util.b;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.ArrayDeque;
import java.util.Map;

/* compiled from: TtmlRenderUtil.java */
/* renamed from: lc4  reason: default package */
/* loaded from: classes.dex */
public final class lc4 {
    public static void a(Spannable spannable, int i, int i2, mc4 mc4Var, jc4 jc4Var, Map<String, mc4> map, int i3) {
        jc4 e;
        mc4 f;
        int i4;
        if (mc4Var.l() != -1) {
            spannable.setSpan(new StyleSpan(mc4Var.l()), i, i2, 33);
        }
        if (mc4Var.s()) {
            spannable.setSpan(new StrikethroughSpan(), i, i2, 33);
        }
        if (mc4Var.t()) {
            spannable.setSpan(new UnderlineSpan(), i, i2, 33);
        }
        if (mc4Var.q()) {
            hr3.a(spannable, new ForegroundColorSpan(mc4Var.c()), i, i2, 33);
        }
        if (mc4Var.p()) {
            hr3.a(spannable, new BackgroundColorSpan(mc4Var.b()), i, i2, 33);
        }
        if (mc4Var.d() != null) {
            hr3.a(spannable, new TypefaceSpan(mc4Var.d()), i, i2, 33);
        }
        if (mc4Var.o() != null) {
            j44 j44Var = (j44) ii.e(mc4Var.o());
            int i5 = j44Var.a;
            if (i5 == -1) {
                i5 = (i3 == 2 || i3 == 1) ? 3 : 1;
                i4 = 1;
            } else {
                i4 = j44Var.b;
            }
            int i6 = j44Var.c;
            if (i6 == -2) {
                i6 = 1;
            }
            hr3.a(spannable, new k44(i5, i4, i6), i, i2, 33);
        }
        int j = mc4Var.j();
        if (j == 2) {
            jc4 d = d(jc4Var, map);
            if (d != null && (e = e(d, map)) != null) {
                if (e.g() == 1 && e.f(0).b != null) {
                    String str = (String) b.j(e.f(0).b);
                    mc4 f2 = f(e.f, e.l(), map);
                    int i7 = f2 != null ? f2.i() : -1;
                    if (i7 == -1 && (f = f(d.f, d.l(), map)) != null) {
                        i7 = f.i();
                    }
                    spannable.setSpan(new z93(str, i7), i, i2, 33);
                } else {
                    p12.f("TtmlRenderUtil", "Skipping rubyText node without exactly one text child.");
                }
            }
        } else if (j == 3 || j == 4) {
            spannable.setSpan(new dm0(), i, i2, 33);
        }
        if (mc4Var.n()) {
            hr3.a(spannable, new dl1(), i, i2, 33);
        }
        int f3 = mc4Var.f();
        if (f3 == 1) {
            hr3.a(spannable, new AbsoluteSizeSpan((int) mc4Var.e(), true), i, i2, 33);
        } else if (f3 == 2) {
            hr3.a(spannable, new RelativeSizeSpan(mc4Var.e()), i, i2, 33);
        } else if (f3 != 3) {
        } else {
            hr3.a(spannable, new RelativeSizeSpan(mc4Var.e() / 100.0f), i, i2, 33);
        }
    }

    public static String b(String str) {
        return str.replaceAll("\r\n", "\n").replaceAll(" *\n *", "\n").replaceAll("\n", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).replaceAll("[ \t\\x0B\f\r]+", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
    }

    public static void c(SpannableStringBuilder spannableStringBuilder) {
        int length = spannableStringBuilder.length() - 1;
        while (length >= 0 && spannableStringBuilder.charAt(length) == ' ') {
            length--;
        }
        if (length < 0 || spannableStringBuilder.charAt(length) == '\n') {
            return;
        }
        spannableStringBuilder.append('\n');
    }

    public static jc4 d(jc4 jc4Var, Map<String, mc4> map) {
        while (jc4Var != null) {
            mc4 f = f(jc4Var.f, jc4Var.l(), map);
            if (f != null && f.j() == 1) {
                return jc4Var;
            }
            jc4Var = jc4Var.j;
        }
        return null;
    }

    public static jc4 e(jc4 jc4Var, Map<String, mc4> map) {
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(jc4Var);
        while (!arrayDeque.isEmpty()) {
            jc4 jc4Var2 = (jc4) arrayDeque.pop();
            mc4 f = f(jc4Var2.f, jc4Var2.l(), map);
            if (f != null && f.j() == 3) {
                return jc4Var2;
            }
            for (int g = jc4Var2.g() - 1; g >= 0; g--) {
                arrayDeque.push(jc4Var2.f(g));
            }
        }
        return null;
    }

    public static mc4 f(mc4 mc4Var, String[] strArr, Map<String, mc4> map) {
        int i = 0;
        if (mc4Var == null) {
            if (strArr == null) {
                return null;
            }
            if (strArr.length == 1) {
                return map.get(strArr[0]);
            }
            if (strArr.length > 1) {
                mc4 mc4Var2 = new mc4();
                int length = strArr.length;
                while (i < length) {
                    mc4Var2.a(map.get(strArr[i]));
                    i++;
                }
                return mc4Var2;
            }
        } else if (strArr != null && strArr.length == 1) {
            return mc4Var.a(map.get(strArr[0]));
        } else {
            if (strArr != null && strArr.length > 1) {
                int length2 = strArr.length;
                while (i < length2) {
                    mc4Var.a(map.get(strArr[i]));
                    i++;
                }
            }
        }
        return mc4Var;
    }
}
