package defpackage;

/* compiled from: CancellableContinuation.kt */
/* renamed from: rv  reason: default package */
/* loaded from: classes2.dex */
public final class rv {
    public static final void a(ov<?> ovVar, yp0 yp0Var) {
        ovVar.f(new zp0(yp0Var));
    }

    public static final <T> pv<T> b(q70<? super T> q70Var) {
        if (!(q70Var instanceof np0)) {
            return new pv<>(q70Var, 1);
        }
        pv<T> m = ((np0) q70Var).m();
        if (m == null || !m.J()) {
            m = null;
        }
        return m == null ? new pv<>(q70Var, 2) : m;
    }

    public static final void c(ov<?> ovVar, l12 l12Var) {
        ovVar.f(new r63(l12Var));
    }
}
