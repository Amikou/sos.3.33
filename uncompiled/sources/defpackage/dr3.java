package defpackage;

import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.PopupWindow;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

/* compiled from: SoftKeyboardHeightProvider.kt */
/* renamed from: dr3  reason: default package */
/* loaded from: classes2.dex */
public final class dr3 extends PopupWindow implements ViewTreeObserver.OnGlobalLayoutListener {
    public final Fragment a;
    public View f0;
    public a g0;
    public int h0;

    /* compiled from: SoftKeyboardHeightProvider.kt */
    /* renamed from: dr3$a */
    /* loaded from: classes2.dex */
    public interface a {
        void a(int i);
    }

    public dr3(Fragment fragment) {
        fs1.f(fragment, "fragment");
        this.a = fragment;
        View view = new View(fragment.requireContext());
        this.f0 = view;
        setContentView(view);
        this.f0.getViewTreeObserver().addOnGlobalLayoutListener(this);
        setBackgroundDrawable(new ColorDrawable(0));
        setWidth(0);
        setHeight(-1);
        setInputMethodMode(1);
    }

    public static final void c(dr3 dr3Var, View view) {
        fs1.f(dr3Var, "this$0");
        dr3Var.showAtLocation(view, 0, 0, 0);
    }

    public final dr3 b() {
        Window window;
        if (!isShowing()) {
            FragmentActivity activity = this.a.getActivity();
            final View view = null;
            if (activity != null && (window = activity.getWindow()) != null) {
                view = window.getDecorView();
            }
            fs1.d(view);
            view.post(new Runnable() { // from class: cr3
                @Override // java.lang.Runnable
                public final void run() {
                    dr3.c(dr3.this, view);
                }
            });
        }
        return this;
    }

    public final dr3 d(a aVar) {
        this.g0 = aVar;
        return this;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        if (this.g0 == null) {
            return;
        }
        Rect rect = new Rect();
        this.f0.getWindowVisibleDisplayFrame(rect);
        int i = rect.bottom;
        if (i > this.h0) {
            this.h0 = i;
        }
        int i2 = this.h0 - i;
        a aVar = this.g0;
        if (aVar == null) {
            return;
        }
        aVar.a(i2);
    }
}
