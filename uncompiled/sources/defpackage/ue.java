package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeBindings;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

/* compiled from: Annotated.java */
/* renamed from: ue  reason: default package */
/* loaded from: classes.dex */
public abstract class ue {
    public abstract Iterable<Annotation> annotations();

    public abstract boolean equals(Object obj);

    public abstract we getAllAnnotations();

    public abstract AnnotatedElement getAnnotated();

    public abstract <A extends Annotation> A getAnnotation(Class<A> cls);

    @Deprecated
    public Type getGenericType() {
        return getRawType();
    }

    public abstract int getModifiers();

    public abstract String getName();

    public abstract Class<?> getRawType();

    public abstract JavaType getType();

    @Deprecated
    public final JavaType getType(TypeBindings typeBindings) {
        return getType();
    }

    public abstract boolean hasAnnotation(Class<?> cls);

    public abstract boolean hasOneOf(Class<? extends Annotation>[] clsArr);

    public abstract int hashCode();

    public final boolean isPublic() {
        return Modifier.isPublic(getModifiers());
    }

    public abstract String toString();

    public abstract ue withAnnotations(we weVar);

    public final ue withFallBackAnnotationsFrom(ue ueVar) {
        return withAnnotations(we.h(getAllAnnotations(), ueVar.getAllAnnotations()));
    }
}
