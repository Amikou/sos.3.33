package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.internal.vision.zzab;
import com.google.android.gms.internal.vision.zzah;
import com.google.android.gms.internal.vision.zzao;

/* compiled from: com.google.android.gms:play-services-vision@@20.1.3 */
/* renamed from: w45  reason: default package */
/* loaded from: classes.dex */
public final class w45 implements Parcelable.Creator<zzah> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzah createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        int i = 0;
        boolean z = false;
        int i2 = 0;
        int i3 = 0;
        zzao[] zzaoVarArr = null;
        zzab zzabVar = null;
        zzab zzabVar2 = null;
        zzab zzabVar3 = null;
        String str = null;
        String str2 = null;
        float f = 0.0f;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            switch (SafeParcelReader.v(C)) {
                case 2:
                    zzaoVarArr = (zzao[]) SafeParcelReader.s(parcel, C, zzao.CREATOR);
                    break;
                case 3:
                    zzabVar = (zzab) SafeParcelReader.o(parcel, C, zzab.CREATOR);
                    break;
                case 4:
                    zzabVar2 = (zzab) SafeParcelReader.o(parcel, C, zzab.CREATOR);
                    break;
                case 5:
                    zzabVar3 = (zzab) SafeParcelReader.o(parcel, C, zzab.CREATOR);
                    break;
                case 6:
                    str = SafeParcelReader.p(parcel, C);
                    break;
                case 7:
                    f = SafeParcelReader.A(parcel, C);
                    break;
                case 8:
                    str2 = SafeParcelReader.p(parcel, C);
                    break;
                case 9:
                    i = SafeParcelReader.E(parcel, C);
                    break;
                case 10:
                    z = SafeParcelReader.w(parcel, C);
                    break;
                case 11:
                    i2 = SafeParcelReader.E(parcel, C);
                    break;
                case 12:
                    i3 = SafeParcelReader.E(parcel, C);
                    break;
                default:
                    SafeParcelReader.I(parcel, C);
                    break;
            }
        }
        SafeParcelReader.u(parcel, J);
        return new zzah(zzaoVarArr, zzabVar, zzabVar2, zzabVar3, str, f, str2, i, z, i2, i3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ zzah[] newArray(int i) {
        return new zzah[i];
    }
}
