package defpackage;

import com.github.mikephil.charting.utils.Utils;

/* compiled from: SpringStopEngine.java */
/* renamed from: bs3  reason: default package */
/* loaded from: classes.dex */
public class bs3 implements rt3 {
    public double b;
    public double c;
    public float d;
    public float e;
    public float f;
    public float g;
    public float h;
    public double a = 0.5d;
    public int i = 0;

    @Override // defpackage.rt3
    public boolean a() {
        double d = this.e - this.c;
        double d2 = this.b;
        double d3 = this.f;
        return Math.sqrt((((d3 * d3) * ((double) this.g)) + ((d2 * d) * d)) / d2) <= ((double) this.h);
    }

    @Override // defpackage.rt3
    public float b() {
        return Utils.FLOAT_EPSILON;
    }

    public final void c(double d) {
        double d2 = this.b;
        double d3 = this.a;
        int sqrt = (int) ((9.0d / ((Math.sqrt(d2 / this.g) * d) * 4.0d)) + 1.0d);
        double d4 = d / sqrt;
        int i = 0;
        while (i < sqrt) {
            float f = this.e;
            double d5 = this.c;
            float f2 = this.f;
            double d6 = d2;
            float f3 = this.g;
            double d7 = d3;
            double d8 = f2 + ((((((-d2) * (f - d5)) - (f2 * d3)) / f3) * d4) / 2.0d);
            double d9 = ((((-((f + ((d4 * d8) / 2.0d)) - d5)) * d6) - (d8 * d7)) / f3) * d4;
            float f4 = (float) (f2 + d9);
            this.f = f4;
            float f5 = (float) (f + ((f2 + (d9 / 2.0d)) * d4));
            this.e = f5;
            int i2 = this.i;
            if (i2 > 0) {
                if (f5 < Utils.FLOAT_EPSILON && (i2 & 1) == 1) {
                    this.e = -f5;
                    this.f = -f4;
                }
                float f6 = this.e;
                if (f6 > 1.0f && (i2 & 2) == 2) {
                    this.e = 2.0f - f6;
                    this.f = -this.f;
                }
            }
            i++;
            d2 = d6;
            d3 = d7;
        }
    }

    public void d(float f, float f2, float f3, float f4, float f5, float f6, float f7, int i) {
        this.c = f2;
        this.a = f6;
        this.e = f;
        this.b = f5;
        this.g = f4;
        this.h = f7;
        this.i = i;
        this.d = Utils.FLOAT_EPSILON;
    }

    @Override // defpackage.rt3
    public float getInterpolation(float f) {
        c(f - this.d);
        this.d = f;
        return this.e;
    }
}
