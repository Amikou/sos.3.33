package defpackage;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/* compiled from: DiskLruCache.java */
/* renamed from: ep0  reason: default package */
/* loaded from: classes2.dex */
public final class ep0 implements Closeable {
    public static final Pattern s0 = Pattern.compile("[a-z0-9_-]{1,64}");
    public static final OutputStream t0 = new b();
    public final File a;
    public final File f0;
    public final File g0;
    public final File h0;
    public final int i0;
    public long j0;
    public final int k0;
    public Writer m0;
    public int o0;
    public long l0 = 0;
    public final LinkedHashMap<String, d> n0 = new LinkedHashMap<>(0, 0.75f, true);
    public long p0 = 0;
    public final ThreadPoolExecutor q0 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
    public final Callable<Void> r0 = new a();

    /* compiled from: DiskLruCache.java */
    /* renamed from: ep0$a */
    /* loaded from: classes2.dex */
    public class a implements Callable<Void> {
        public a() {
        }

        @Override // java.util.concurrent.Callable
        /* renamed from: a */
        public Void call() throws Exception {
            synchronized (ep0.this) {
                if (ep0.this.m0 == null) {
                    return null;
                }
                ep0.this.Q();
                if (ep0.this.w()) {
                    ep0.this.F();
                    ep0.this.o0 = 0;
                }
                return null;
            }
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: ep0$b */
    /* loaded from: classes2.dex */
    public static class b extends OutputStream {
        @Override // java.io.OutputStream
        public void write(int i) throws IOException {
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: ep0$c */
    /* loaded from: classes2.dex */
    public final class c {
        public final d a;
        public final boolean[] b;
        public boolean c;

        /* compiled from: DiskLruCache.java */
        /* renamed from: ep0$c$a */
        /* loaded from: classes2.dex */
        public class a extends FilterOutputStream {
            public /* synthetic */ a(c cVar, OutputStream outputStream, a aVar) {
                this(outputStream);
            }

            @Override // java.io.FilterOutputStream, java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
            public void close() {
                try {
                    ((FilterOutputStream) this).out.close();
                } catch (IOException unused) {
                    c.this.c = true;
                }
            }

            @Override // java.io.FilterOutputStream, java.io.OutputStream, java.io.Flushable
            public void flush() {
                try {
                    ((FilterOutputStream) this).out.flush();
                } catch (IOException unused) {
                    c.this.c = true;
                }
            }

            @Override // java.io.FilterOutputStream, java.io.OutputStream
            public void write(int i) {
                try {
                    ((FilterOutputStream) this).out.write(i);
                } catch (IOException unused) {
                    c.this.c = true;
                }
            }

            public a(OutputStream outputStream) {
                super(outputStream);
            }

            @Override // java.io.FilterOutputStream, java.io.OutputStream
            public void write(byte[] bArr, int i, int i2) {
                try {
                    ((FilterOutputStream) this).out.write(bArr, i, i2);
                } catch (IOException unused) {
                    c.this.c = true;
                }
            }
        }

        public /* synthetic */ c(ep0 ep0Var, d dVar, a aVar) {
            this(dVar);
        }

        public void a() throws IOException {
            ep0.this.l(this, false);
        }

        public void e() throws IOException {
            if (this.c) {
                ep0.this.l(this, false);
                ep0.this.M(this.a.a);
                return;
            }
            ep0.this.l(this, true);
        }

        public OutputStream f(int i) throws IOException {
            FileOutputStream fileOutputStream;
            a aVar;
            synchronized (ep0.this) {
                if (this.a.d == this) {
                    if (!this.a.c) {
                        this.b[i] = true;
                    }
                    File k = this.a.k(i);
                    try {
                        fileOutputStream = new FileOutputStream(k);
                    } catch (FileNotFoundException unused) {
                        ep0.this.a.mkdirs();
                        try {
                            fileOutputStream = new FileOutputStream(k);
                        } catch (FileNotFoundException unused2) {
                            return ep0.t0;
                        }
                    }
                    aVar = new a(this, fileOutputStream, null);
                } else {
                    throw new IllegalStateException();
                }
            }
            return aVar;
        }

        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.c ? null : new boolean[ep0.this.k0];
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: ep0$d */
    /* loaded from: classes2.dex */
    public final class d {
        public final String a;
        public final long[] b;
        public boolean c;
        public c d;
        public long e;

        public /* synthetic */ d(ep0 ep0Var, String str, a aVar) {
            this(str);
        }

        public File j(int i) {
            File file = ep0.this.a;
            return new File(file, this.a + "." + i);
        }

        public File k(int i) {
            File file = ep0.this.a;
            return new File(file, this.a + "." + i + ".tmp");
        }

        public String l() throws IOException {
            long[] jArr;
            StringBuilder sb = new StringBuilder();
            for (long j : this.b) {
                sb.append(' ');
                sb.append(j);
            }
            return sb.toString();
        }

        public final IOException m(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        public final void n(String[] strArr) throws IOException {
            if (strArr.length == ep0.this.k0) {
                for (int i = 0; i < strArr.length; i++) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                    } catch (NumberFormatException unused) {
                        throw m(strArr);
                    }
                }
                return;
            }
            throw m(strArr);
        }

        public d(String str) {
            this.a = str;
            this.b = new long[ep0.this.k0];
        }
    }

    /* compiled from: DiskLruCache.java */
    /* renamed from: ep0$e */
    /* loaded from: classes2.dex */
    public final class e implements Closeable {
        public final InputStream[] a;
        public final long[] f0;

        public /* synthetic */ e(ep0 ep0Var, String str, long j, InputStream[] inputStreamArr, long[] jArr, a aVar) {
            this(ep0Var, str, j, inputStreamArr, jArr);
        }

        public InputStream a(int i) {
            return this.a[i];
        }

        public long b(int i) {
            return this.f0[i];
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            for (InputStream inputStream : this.a) {
                ng4.a(inputStream);
            }
        }

        public e(ep0 ep0Var, String str, long j, InputStream[] inputStreamArr, long[] jArr) {
            this.a = inputStreamArr;
            this.f0 = jArr;
        }
    }

    public ep0(File file, int i, int i2, long j) {
        this.a = file;
        this.i0 = i;
        this.f0 = new File(file, "journal");
        this.g0 = new File(file, "journal.tmp");
        this.h0 = new File(file, "journal.bkp");
        this.k0 = i2;
        this.j0 = j;
    }

    public static void N(File file, File file2, boolean z) throws IOException {
        if (z) {
            n(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    public static void n(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    public static ep0 x(File file, int i, int i2, long j) throws IOException {
        if (j > 0) {
            if (i2 > 0) {
                File file2 = new File(file, "journal.bkp");
                if (file2.exists()) {
                    File file3 = new File(file, "journal");
                    if (file3.exists()) {
                        file2.delete();
                    } else {
                        N(file2, file3, false);
                    }
                }
                ep0 ep0Var = new ep0(file, i, i2, j);
                if (ep0Var.f0.exists()) {
                    try {
                        ep0Var.A();
                        ep0Var.z();
                        ep0Var.m0 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ep0Var.f0, true), ng4.a));
                        return ep0Var;
                    } catch (IOException e2) {
                        PrintStream printStream = System.out;
                        printStream.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                        ep0Var.m();
                    }
                }
                file.mkdirs();
                ep0 ep0Var2 = new ep0(file, i, i2, j);
                ep0Var2.F();
                return ep0Var2;
            }
            throw new IllegalArgumentException("valueCount <= 0");
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    public final void A() throws IOException {
        ju3 ju3Var = new ju3(new FileInputStream(this.f0), ng4.a);
        try {
            String c2 = ju3Var.c();
            String c3 = ju3Var.c();
            String c4 = ju3Var.c();
            String c5 = ju3Var.c();
            String c6 = ju3Var.c();
            if (!"libcore.io.DiskLruCache".equals(c2) || !"1".equals(c3) || !Integer.toString(this.i0).equals(c4) || !Integer.toString(this.k0).equals(c5) || !"".equals(c6)) {
                throw new IOException("unexpected journal header: [" + c2 + ", " + c3 + ", " + c5 + ", " + c6 + "]");
            }
            int i = 0;
            while (true) {
                try {
                    C(ju3Var.c());
                    i++;
                } catch (EOFException unused) {
                    this.o0 = i - this.n0.size();
                    ng4.a(ju3Var);
                    return;
                }
            }
        } catch (Throwable th) {
            ng4.a(ju3Var);
            throw th;
        }
    }

    public final void C(String str) throws IOException {
        String substring;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(32, i);
            if (indexOf2 == -1) {
                substring = str.substring(i);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.n0.remove(substring);
                    return;
                }
            } else {
                substring = str.substring(i, indexOf2);
            }
            d dVar = this.n0.get(substring);
            if (dVar == null) {
                dVar = new d(this, substring, null);
                this.n0.put(substring, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                dVar.c = true;
                dVar.d = null;
                dVar.n(split);
                return;
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.d = new c(this, dVar, null);
                return;
            } else if (indexOf2 == -1 && indexOf == 4 && str.startsWith("READ")) {
                return;
            } else {
                throw new IOException("unexpected journal line: " + str);
            }
        }
        throw new IOException("unexpected journal line: " + str);
    }

    public final synchronized void F() throws IOException {
        Writer writer = this.m0;
        if (writer != null) {
            writer.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.g0), ng4.a));
        bufferedWriter.write("libcore.io.DiskLruCache");
        bufferedWriter.write("\n");
        bufferedWriter.write("1");
        bufferedWriter.write("\n");
        bufferedWriter.write(Integer.toString(this.i0));
        bufferedWriter.write("\n");
        bufferedWriter.write(Integer.toString(this.k0));
        bufferedWriter.write("\n");
        bufferedWriter.write("\n");
        for (d dVar : this.n0.values()) {
            if (dVar.d != null) {
                bufferedWriter.write("DIRTY " + dVar.a + '\n');
            } else {
                bufferedWriter.write("CLEAN " + dVar.a + dVar.l() + '\n');
            }
        }
        bufferedWriter.close();
        if (this.f0.exists()) {
            N(this.f0, this.h0, true);
        }
        N(this.g0, this.f0, false);
        this.h0.delete();
        this.m0 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f0, true), ng4.a));
    }

    public synchronized boolean M(String str) throws IOException {
        j();
        R(str);
        d dVar = this.n0.get(str);
        if (dVar != null && dVar.d == null) {
            for (int i = 0; i < this.k0; i++) {
                File j = dVar.j(i);
                if (j.exists() && !j.delete()) {
                    throw new IOException("failed to delete " + j);
                }
                this.l0 -= dVar.b[i];
                dVar.b[i] = 0;
            }
            this.o0++;
            this.m0.append((CharSequence) ("REMOVE " + str + '\n'));
            this.n0.remove(str);
            if (w()) {
                this.q0.submit(this.r0);
            }
            return true;
        }
        return false;
    }

    public final void Q() throws IOException {
        while (this.l0 > this.j0) {
            M(this.n0.entrySet().iterator().next().getKey());
        }
    }

    public final void R(String str) {
        if (s0.matcher(str).matches()) {
            return;
        }
        throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,64}: \"" + str + "\"");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        if (this.m0 == null) {
            return;
        }
        Iterator it = new ArrayList(this.n0.values()).iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.d != null) {
                dVar.d.a();
            }
        }
        Q();
        this.m0.close();
        this.m0 = null;
    }

    public final void j() {
        if (this.m0 == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    public final synchronized void l(c cVar, boolean z) throws IOException {
        d dVar = cVar.a;
        if (dVar.d == cVar) {
            if (z && !dVar.c) {
                for (int i = 0; i < this.k0; i++) {
                    if (cVar.b[i]) {
                        if (!dVar.k(i).exists()) {
                            cVar.a();
                            return;
                        }
                    } else {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                    }
                }
            }
            for (int i2 = 0; i2 < this.k0; i2++) {
                File k = dVar.k(i2);
                if (z) {
                    if (k.exists()) {
                        File j = dVar.j(i2);
                        k.renameTo(j);
                        long j2 = dVar.b[i2];
                        long length = j.length();
                        dVar.b[i2] = length;
                        this.l0 = (this.l0 - j2) + length;
                    }
                } else {
                    n(k);
                }
            }
            this.o0++;
            dVar.d = null;
            if (dVar.c | z) {
                dVar.c = true;
                this.m0.write("CLEAN " + dVar.a + dVar.l() + '\n');
                if (z) {
                    long j3 = this.p0;
                    this.p0 = 1 + j3;
                    dVar.e = j3;
                }
            } else {
                this.n0.remove(dVar.a);
                this.m0.write("REMOVE " + dVar.a + '\n');
            }
            this.m0.flush();
            if (this.l0 > this.j0 || w()) {
                this.q0.submit(this.r0);
            }
            return;
        }
        throw new IllegalStateException();
    }

    public void m() throws IOException {
        close();
        ng4.b(this.a);
    }

    public c q(String str) throws IOException {
        return r(str, -1L);
    }

    public final synchronized c r(String str, long j) throws IOException {
        j();
        R(str);
        d dVar = this.n0.get(str);
        if (j == -1 || (dVar != null && dVar.e == j)) {
            if (dVar != null) {
                if (dVar.d != null) {
                    return null;
                }
            } else {
                dVar = new d(this, str, null);
                this.n0.put(str, dVar);
            }
            c cVar = new c(this, dVar, null);
            dVar.d = cVar;
            Writer writer = this.m0;
            writer.write("DIRTY " + str + '\n');
            this.m0.flush();
            return cVar;
        }
        return null;
    }

    public synchronized e u(String str) throws IOException {
        j();
        R(str);
        d dVar = this.n0.get(str);
        if (dVar == null) {
            return null;
        }
        if (dVar.c) {
            InputStream[] inputStreamArr = new InputStream[this.k0];
            for (int i = 0; i < this.k0; i++) {
                try {
                    inputStreamArr[i] = new FileInputStream(dVar.j(i));
                } catch (FileNotFoundException unused) {
                    for (int i2 = 0; i2 < this.k0 && inputStreamArr[i2] != null; i2++) {
                        ng4.a(inputStreamArr[i2]);
                    }
                    return null;
                }
            }
            this.o0++;
            this.m0.append((CharSequence) ("READ " + str + '\n'));
            if (w()) {
                this.q0.submit(this.r0);
            }
            return new e(this, str, dVar.e, inputStreamArr, dVar.b, null);
        }
        return null;
    }

    public File v() {
        return this.a;
    }

    public final boolean w() {
        int i = this.o0;
        return i >= 2000 && i >= this.n0.size();
    }

    public final void z() throws IOException {
        n(this.g0);
        Iterator<d> it = this.n0.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i = 0;
            if (next.d != null) {
                next.d = null;
                while (i < this.k0) {
                    n(next.j(i));
                    n(next.k(i));
                    i++;
                }
                it.remove();
            } else {
                while (i < this.k0) {
                    this.l0 += next.b[i];
                    i++;
                }
            }
        }
    }
}
