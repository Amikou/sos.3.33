package defpackage;

import com.google.android.play.core.assetpacks.bj;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.k;
import com.google.android.play.core.assetpacks.n;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/* renamed from: fx4  reason: default package */
/* loaded from: classes2.dex */
public final class fx4 {
    public static final it4 b = new it4("VerifySliceTaskHandler");
    public final c a;

    public fx4(c cVar) {
        this.a = cVar;
    }

    public final void a(ex4 ex4Var) {
        File v = this.a.v(ex4Var.b, ex4Var.c, ex4Var.d, ex4Var.e);
        if (!v.exists()) {
            throw new bj(String.format("Cannot find unverified files for slice %s.", ex4Var.e), ex4Var.a);
        }
        b(ex4Var, v);
        File w = this.a.w(ex4Var.b, ex4Var.c, ex4Var.d, ex4Var.e);
        if (!w.exists()) {
            w.mkdirs();
        }
        if (!v.renameTo(w)) {
            throw new bj(String.format("Failed to move slice %s after verification.", ex4Var.e), ex4Var.a);
        }
    }

    public final void b(ex4 ex4Var, File file) {
        try {
            File C = this.a.C(ex4Var.b, ex4Var.c, ex4Var.d, ex4Var.e);
            if (!C.exists()) {
                throw new bj(String.format("Cannot find metadata files for slice %s.", ex4Var.e), ex4Var.a);
            }
            try {
                if (!k.a(n.a(file, C)).equals(ex4Var.f)) {
                    throw new bj(String.format("Verification failed for slice %s.", ex4Var.e), ex4Var.a);
                }
                b.d("Verification of slice %s of pack %s successful.", ex4Var.e, ex4Var.b);
            } catch (IOException e) {
                throw new bj(String.format("Could not digest file during verification for slice %s.", ex4Var.e), e, ex4Var.a);
            } catch (NoSuchAlgorithmException e2) {
                throw new bj("SHA256 algorithm not supported.", e2, ex4Var.a);
            }
        } catch (IOException e3) {
            throw new bj(String.format("Could not reconstruct slice archive during verification for slice %s.", ex4Var.e), e3, ex4Var.a);
        }
    }
}
