package defpackage;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: m05  reason: default package */
/* loaded from: classes.dex */
public abstract class m05 implements DialogInterface.OnClickListener {
    public static m05 a(nz1 nz1Var, Intent intent, int i) {
        return new u15(intent, nz1Var, 2);
    }

    public static m05 b(Activity activity, Intent intent, int i) {
        return new r15(intent, activity, i);
    }

    public abstract void c();

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            c();
        } catch (ActivityNotFoundException unused) {
            if (Build.FINGERPRINT.contains("generic")) {
                "Failed to start resolution intent.".concat(" This may occur when resolving Google Play services connection issues on emulators with Google APIs but not Google Play Store.");
            }
        } finally {
            dialogInterface.dismiss();
        }
    }
}
