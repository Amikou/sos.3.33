package defpackage;

import com.facebook.fresco.animation.factory.AnimatedFactoryV2Impl;
import com.facebook.imagepipeline.image.a;
import java.util.concurrent.ExecutorService;

/* compiled from: AnimatedFactoryProvider.java */
/* renamed from: rd  reason: default package */
/* loaded from: classes.dex */
public class rd {
    public static boolean a;
    public static qd b;

    public static qd a(br2 br2Var, wy0 wy0Var, i90<wt, a> i90Var, boolean z, ExecutorService executorService) {
        if (!a) {
            try {
                b = (qd) AnimatedFactoryV2Impl.class.getConstructor(br2.class, wy0.class, i90.class, Boolean.TYPE, xl3.class).newInstance(br2Var, wy0Var, i90Var, Boolean.valueOf(z), executorService);
            } catch (Throwable unused) {
            }
            if (b != null) {
                a = true;
            }
        }
        return b;
    }
}
