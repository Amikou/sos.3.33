package defpackage;

import android.os.Bundle;
import com.google.android.gms.cloudmessaging.zzp;

/* compiled from: com.google.android.gms:play-services-cloud-messaging@@16.0.0 */
/* renamed from: yz5  reason: default package */
/* loaded from: classes.dex */
public final class yz5 extends n36<Void> {
    public yz5(int i, int i2, Bundle bundle) {
        super(i, 2, bundle);
    }

    @Override // defpackage.n36
    public final void a(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            c(null);
        } else {
            b(new zzp(4, "Invalid response to one way request"));
        }
    }

    @Override // defpackage.n36
    public final boolean d() {
        return true;
    }
}
