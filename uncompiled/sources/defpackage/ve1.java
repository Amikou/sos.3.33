package defpackage;

import androidx.paging.LoadType;
import kotlin.NoWhenBranchMatchedException;

/* compiled from: PageFetcherSnapshot.kt */
/* renamed from: ve1  reason: default package */
/* loaded from: classes.dex */
public final class ve1 {
    public final int a;
    public final xk4 b;

    public ve1(int i, xk4 xk4Var) {
        fs1.f(xk4Var, "hint");
        this.a = i;
        this.b = xk4Var;
    }

    public final int a() {
        return this.a;
    }

    public final xk4 b() {
        return this.b;
    }

    public final int c(LoadType loadType) {
        fs1.f(loadType, "loadType");
        int i = ue1.a[loadType.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i == 3) {
                    return this.b.c();
                }
                throw new NoWhenBranchMatchedException();
            }
            return this.b.d();
        }
        throw new IllegalArgumentException("Cannot get presentedItems for loadType: REFRESH");
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ve1) {
                ve1 ve1Var = (ve1) obj;
                return this.a == ve1Var.a && fs1.b(this.b, ve1Var.b);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = this.a * 31;
        xk4 xk4Var = this.b;
        return i + (xk4Var != null ? xk4Var.hashCode() : 0);
    }

    public String toString() {
        return "GenerationalViewportHint(generationId=" + this.a + ", hint=" + this.b + ")";
    }
}
