package defpackage;

import android.content.Context;
import android.view.SubMenu;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.g;

/* compiled from: NavigationMenu.java */
/* renamed from: ke2  reason: default package */
/* loaded from: classes2.dex */
public class ke2 extends e {
    public ke2(Context context) {
        super(context);
    }

    @Override // androidx.appcompat.view.menu.e, android.view.Menu
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        g gVar = (g) a(i, i2, i3, charSequence);
        ne2 ne2Var = new ne2(w(), this, gVar);
        gVar.x(ne2Var);
        return ne2Var;
    }
}
