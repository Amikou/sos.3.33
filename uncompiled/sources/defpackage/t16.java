package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: t16  reason: default package */
/* loaded from: classes.dex */
public final class t16 implements yp5<u16> {
    public static final t16 f0 = new t16();
    public final yp5<u16> a = gq5.a(gq5.b(new v16()));

    public static boolean a() {
        return f0.zza().zza();
    }

    @Override // defpackage.yp5
    /* renamed from: b */
    public final u16 zza() {
        return this.a.zza();
    }
}
