package defpackage;

import kotlinx.coroutines.channels.BufferOverflow;
import kotlinx.coroutines.flow.FlowKt__BuildersKt;
import kotlinx.coroutines.flow.FlowKt__ChannelsKt;
import kotlinx.coroutines.flow.FlowKt__CollectKt;
import kotlinx.coroutines.flow.FlowKt__DistinctKt;
import kotlinx.coroutines.flow.FlowKt__EmittersKt;
import kotlinx.coroutines.flow.FlowKt__ErrorsKt;
import kotlinx.coroutines.flow.FlowKt__MergeKt;
import kotlinx.coroutines.flow.FlowKt__TransformKt;

/* renamed from: n71  reason: default package */
/* loaded from: classes2.dex */
public final class n71 {
    public static final <T> j71<T> a(j71<? extends T> j71Var, int i, BufferOverflow bufferOverflow) {
        return o71.a(j71Var, i, bufferOverflow);
    }

    public static final <T> j71<T> c(j71<? extends T> j71Var, kd1<? super k71<? super T>, ? super Throwable, ? super q70<? super te4>, ? extends Object> kd1Var) {
        return FlowKt__ErrorsKt.a(j71Var, kd1Var);
    }

    public static final <T> Object d(j71<? extends T> j71Var, k71<? super T> k71Var, q70<? super Throwable> q70Var) {
        return FlowKt__ErrorsKt.b(j71Var, k71Var, q70Var);
    }

    public static final Object e(j71<?> j71Var, q70<? super te4> q70Var) {
        return FlowKt__CollectKt.a(j71Var, q70Var);
    }

    public static final <T> Object f(j71<? extends T> j71Var, hd1<? super T, ? super q70<? super te4>, ? extends Object> hd1Var, q70<? super te4> q70Var) {
        return FlowKt__CollectKt.b(j71Var, hd1Var, q70Var);
    }

    public static final <T> j71<T> g(j71<? extends T> j71Var) {
        return o71.c(j71Var);
    }

    public static final <T> j71<T> h(f43<? extends T> f43Var) {
        return FlowKt__ChannelsKt.b(f43Var);
    }

    public static final <T> j71<T> i(j71<? extends T> j71Var) {
        return FlowKt__DistinctKt.a(j71Var);
    }

    public static final <T> j71<T> j(j71<? extends T> j71Var, int i) {
        return p71.a(j71Var, i);
    }

    public static final <T> Object k(k71<? super T> k71Var, f43<? extends T> f43Var, q70<? super te4> q70Var) {
        return FlowKt__ChannelsKt.c(k71Var, f43Var, q70Var);
    }

    public static final void l(k71<?> k71Var) {
        FlowKt__EmittersKt.b(k71Var);
    }

    public static final <T> j71<T> m(j71<? extends T> j71Var) {
        return FlowKt__TransformKt.a(j71Var);
    }

    public static final <T> j71<T> n(hd1<? super k71<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return FlowKt__BuildersKt.a(hd1Var);
    }

    public static final <T> j71<T> o(T t) {
        return FlowKt__BuildersKt.b(t);
    }

    public static final <T> j71<T> p(T... tArr) {
        return FlowKt__BuildersKt.c(tArr);
    }

    public static final <T> st1 q(j71<? extends T> j71Var, c90 c90Var) {
        return FlowKt__CollectKt.c(j71Var, c90Var);
    }

    public static final <T, R> j71<R> r(j71<? extends T> j71Var, hd1<? super T, ? super q70<? super R>, ? extends Object> hd1Var) {
        return FlowKt__MergeKt.a(j71Var, hd1Var);
    }

    public static final <T> j71<T> s(j71<? extends T> j71Var, kd1<? super k71<? super T>, ? super Throwable, ? super q70<? super te4>, ? extends Object> kd1Var) {
        return FlowKt__EmittersKt.d(j71Var, kd1Var);
    }

    public static final <T> j71<T> t(j71<? extends T> j71Var, hd1<? super T, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return FlowKt__TransformKt.b(j71Var, hd1Var);
    }

    public static final <T> j71<T> u(j71<? extends T> j71Var, hd1<? super k71<? super T>, ? super q70<? super te4>, ? extends Object> hd1Var) {
        return FlowKt__EmittersKt.e(j71Var, hd1Var);
    }

    public static final <T, R> j71<R> v(j71<? extends T> j71Var, kd1<? super k71<? super R>, ? super T, ? super q70<? super te4>, ? extends Object> kd1Var) {
        return FlowKt__MergeKt.b(j71Var, kd1Var);
    }

    public static final <T> j71<lq1<T>> w(j71<? extends T> j71Var) {
        return FlowKt__TransformKt.c(j71Var);
    }
}
