package defpackage;

import kotlin.Result;

/* compiled from: Result.kt */
/* renamed from: o83  reason: default package */
/* loaded from: classes2.dex */
public final class o83 {
    public static final Object a(Throwable th) {
        fs1.f(th, "exception");
        return new Result.Failure(th);
    }

    public static final void b(Object obj) {
        if (obj instanceof Result.Failure) {
            throw ((Result.Failure) obj).exception;
        }
    }
}
