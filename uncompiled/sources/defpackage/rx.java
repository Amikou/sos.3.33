package defpackage;

import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: Channel.kt */
/* renamed from: rx */
/* loaded from: classes2.dex */
public final class rx {
    public static final <E> kx<E> a(int i, BufferOverflow bufferOverflow, tc1<? super E, te4> tc1Var) {
        if (i == -2) {
            return new mh(bufferOverflow == BufferOverflow.SUSPEND ? kx.c.a() : 1, bufferOverflow, tc1Var);
        } else if (i == -1) {
            if ((bufferOverflow != BufferOverflow.SUSPEND ? 0 : 1) != 0) {
                return new s50(tc1Var);
            }
            throw new IllegalArgumentException("CONFLATED capacity cannot be used with non-default onBufferOverflow".toString());
        } else if (i == 0) {
            if (bufferOverflow == BufferOverflow.SUSPEND) {
                return new x63(tc1Var);
            }
            return new mh(1, bufferOverflow, tc1Var);
        } else if (i != Integer.MAX_VALUE) {
            if (i == 1 && bufferOverflow == BufferOverflow.DROP_OLDEST) {
                return new s50(tc1Var);
            }
            return new mh(i, bufferOverflow, tc1Var);
        } else {
            return new e02(tc1Var);
        }
    }

    public static /* synthetic */ kx b(int i, BufferOverflow bufferOverflow, tc1 tc1Var, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        if ((i2 & 2) != 0) {
            bufferOverflow = BufferOverflow.SUSPEND;
        }
        if ((i2 & 4) != 0) {
            tc1Var = null;
        }
        return a(i, bufferOverflow, tc1Var);
    }
}
