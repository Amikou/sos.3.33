package defpackage;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: di5  reason: default package */
/* loaded from: classes.dex */
public abstract class di5 extends t75 implements jf5 {
    public static jf5 b(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
        return queryLocalInterface instanceof jf5 ? (jf5) queryLocalInterface : new sk5(iBinder);
    }
}
