package defpackage;

import androidx.media3.extractor.flv.TagPayloadReader;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zendesk.support.request.CellBase;

/* compiled from: ScriptTagPayloadReader.java */
/* renamed from: xd3  reason: default package */
/* loaded from: classes.dex */
public final class xd3 extends TagPayloadReader {
    public long b;
    public long[] c;
    public long[] d;

    public xd3() {
        super(new ks0());
        this.b = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        this.c = new long[0];
        this.d = new long[0];
    }

    public static Boolean g(op2 op2Var) {
        return Boolean.valueOf(op2Var.D() == 1);
    }

    public static Object h(op2 op2Var, int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 8) {
                            if (i != 10) {
                                if (i != 11) {
                                    return null;
                                }
                                return i(op2Var);
                            }
                            return m(op2Var);
                        }
                        return k(op2Var);
                    }
                    return l(op2Var);
                }
                return n(op2Var);
            }
            return g(op2Var);
        }
        return j(op2Var);
    }

    public static Date i(op2 op2Var) {
        Date date = new Date((long) j(op2Var).doubleValue());
        op2Var.Q(2);
        return date;
    }

    public static Double j(op2 op2Var) {
        return Double.valueOf(Double.longBitsToDouble(op2Var.w()));
    }

    public static HashMap<String, Object> k(op2 op2Var) {
        int H = op2Var.H();
        HashMap<String, Object> hashMap = new HashMap<>(H);
        for (int i = 0; i < H; i++) {
            String n = n(op2Var);
            Object h = h(op2Var, o(op2Var));
            if (h != null) {
                hashMap.put(n, h);
            }
        }
        return hashMap;
    }

    public static HashMap<String, Object> l(op2 op2Var) {
        HashMap<String, Object> hashMap = new HashMap<>();
        while (true) {
            String n = n(op2Var);
            int o = o(op2Var);
            if (o == 9) {
                return hashMap;
            }
            Object h = h(op2Var, o);
            if (h != null) {
                hashMap.put(n, h);
            }
        }
    }

    public static ArrayList<Object> m(op2 op2Var) {
        int H = op2Var.H();
        ArrayList<Object> arrayList = new ArrayList<>(H);
        for (int i = 0; i < H; i++) {
            Object h = h(op2Var, o(op2Var));
            if (h != null) {
                arrayList.add(h);
            }
        }
        return arrayList;
    }

    public static String n(op2 op2Var) {
        int J = op2Var.J();
        int e = op2Var.e();
        op2Var.Q(J);
        return new String(op2Var.d(), e, J);
    }

    public static int o(op2 op2Var) {
        return op2Var.D();
    }

    @Override // androidx.media3.extractor.flv.TagPayloadReader
    public boolean b(op2 op2Var) {
        return true;
    }

    @Override // androidx.media3.extractor.flv.TagPayloadReader
    public boolean c(op2 op2Var, long j) {
        if (o(op2Var) == 2 && "onMetaData".equals(n(op2Var)) && op2Var.a() != 0 && o(op2Var) == 8) {
            HashMap<String, Object> k = k(op2Var);
            Object obj = k.get("duration");
            if (obj instanceof Double) {
                double doubleValue = ((Double) obj).doubleValue();
                if (doubleValue > Utils.DOUBLE_EPSILON) {
                    this.b = (long) (doubleValue * 1000000.0d);
                }
            }
            Object obj2 = k.get("keyframes");
            if (obj2 instanceof Map) {
                Map map = (Map) obj2;
                Object obj3 = map.get("filepositions");
                Object obj4 = map.get("times");
                if ((obj3 instanceof List) && (obj4 instanceof List)) {
                    List list = (List) obj3;
                    List list2 = (List) obj4;
                    int size = list2.size();
                    this.c = new long[size];
                    this.d = new long[size];
                    for (int i = 0; i < size; i++) {
                        Object obj5 = list.get(i);
                        Object obj6 = list2.get(i);
                        if ((obj6 instanceof Double) && (obj5 instanceof Double)) {
                            this.c[i] = (long) (((Double) obj6).doubleValue() * 1000000.0d);
                            this.d[i] = ((Double) obj5).longValue();
                        } else {
                            this.c = new long[0];
                            this.d = new long[0];
                            break;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }

    public long d() {
        return this.b;
    }

    public long[] e() {
        return this.d;
    }

    public long[] f() {
        return this.c;
    }
}
