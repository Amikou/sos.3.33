package defpackage;

import android.os.Bundle;

/* compiled from: ActionOnlyNavDirections.java */
/* renamed from: l6  reason: default package */
/* loaded from: classes.dex */
public final class l6 implements ce2 {
    public final int a;

    public l6(int i) {
        this.a = i;
    }

    @Override // defpackage.ce2
    public Bundle a() {
        return new Bundle();
    }

    @Override // defpackage.ce2
    public int b() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && l6.class == obj.getClass() && b() == ((l6) obj).b();
    }

    public int hashCode() {
        return 31 + b();
    }

    public String toString() {
        return "ActionOnlyNavDirections(actionId=" + b() + ")";
    }
}
