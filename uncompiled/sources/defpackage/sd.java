package defpackage;

import defpackage.i90;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* compiled from: AnimatedFrameCache.java */
/* renamed from: sd  reason: default package */
/* loaded from: classes.dex */
public class sd {
    public final wt a;
    public final i90<wt, com.facebook.imagepipeline.image.a> b;
    public final LinkedHashSet<wt> d = new LinkedHashSet<>();
    public final i90.b<wt> c = new a();

    /* compiled from: AnimatedFrameCache.java */
    /* renamed from: sd$a */
    /* loaded from: classes.dex */
    public class a implements i90.b<wt> {
        public a() {
        }

        @Override // defpackage.i90.b
        /* renamed from: b */
        public void a(wt wtVar, boolean z) {
            sd.this.f(wtVar, z);
        }
    }

    /* compiled from: AnimatedFrameCache.java */
    /* renamed from: sd$b */
    /* loaded from: classes.dex */
    public static class b implements wt {
        public final wt a;
        public final int b;

        public b(wt wtVar, int i) {
            this.a = wtVar;
            this.b = i;
        }

        @Override // defpackage.wt
        public boolean a() {
            return false;
        }

        @Override // defpackage.wt
        public String b() {
            return null;
        }

        @Override // defpackage.wt
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof b) {
                b bVar = (b) obj;
                return this.b == bVar.b && this.a.equals(bVar.a);
            }
            return false;
        }

        @Override // defpackage.wt
        public int hashCode() {
            return (this.a.hashCode() * 1013) + this.b;
        }

        public String toString() {
            return ol2.c(this).b("imageCacheKey", this.a).a("frameIndex", this.b).toString();
        }
    }

    public sd(wt wtVar, i90<wt, com.facebook.imagepipeline.image.a> i90Var) {
        this.a = wtVar;
        this.b = i90Var;
    }

    public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> a(int i, com.facebook.common.references.a<com.facebook.imagepipeline.image.a> aVar) {
        return this.b.e(e(i), aVar, this.c);
    }

    public boolean b(int i) {
        return this.b.contains(e(i));
    }

    public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> c(int i) {
        return this.b.get(e(i));
    }

    public com.facebook.common.references.a<com.facebook.imagepipeline.image.a> d() {
        com.facebook.common.references.a<com.facebook.imagepipeline.image.a> d;
        do {
            wt g = g();
            if (g == null) {
                return null;
            }
            d = this.b.d(g);
        } while (d == null);
        return d;
    }

    public final b e(int i) {
        return new b(this.a, i);
    }

    public synchronized void f(wt wtVar, boolean z) {
        if (z) {
            this.d.add(wtVar);
        } else {
            this.d.remove(wtVar);
        }
    }

    public final synchronized wt g() {
        wt wtVar;
        wtVar = null;
        Iterator<wt> it = this.d.iterator();
        if (it.hasNext()) {
            wtVar = it.next();
            it.remove();
        }
        return wtVar;
    }
}
