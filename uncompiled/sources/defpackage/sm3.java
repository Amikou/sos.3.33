package defpackage;

import java.util.Collections;
import java.util.Set;

/* compiled from: SetsJVM.kt */
/* renamed from: sm3  reason: default package */
/* loaded from: classes2.dex */
public class sm3 {
    public static final <T> Set<T> a(T t) {
        Set<T> singleton = Collections.singleton(t);
        fs1.e(singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }
}
