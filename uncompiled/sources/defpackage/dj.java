package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import com.rd.animation.type.AnimationType;
import com.rd.draw.data.Orientation;
import com.rd.draw.data.RtlMode;

/* compiled from: AttributeController.java */
/* renamed from: dj  reason: default package */
/* loaded from: classes2.dex */
public class dj {
    public mq1 a;

    public dj(mq1 mq1Var) {
        this.a = mq1Var;
    }

    public final AnimationType a(int i) {
        switch (i) {
            case 0:
                return AnimationType.NONE;
            case 1:
                return AnimationType.COLOR;
            case 2:
                return AnimationType.SCALE;
            case 3:
                return AnimationType.WORM;
            case 4:
                return AnimationType.SLIDE;
            case 5:
                return AnimationType.FILL;
            case 6:
                return AnimationType.THIN_WORM;
            case 7:
                return AnimationType.DROP;
            case 8:
                return AnimationType.SWAP;
            case 9:
                return AnimationType.SCALE_DOWN;
            default:
                return AnimationType.NONE;
        }
    }

    public final RtlMode b(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    return RtlMode.Auto;
                }
                return RtlMode.Auto;
            }
            return RtlMode.Off;
        }
        return RtlMode.On;
    }

    public void c(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, r23.PageIndicatorView, 0, 0);
        f(obtainStyledAttributes);
        e(obtainStyledAttributes);
        d(obtainStyledAttributes);
        g(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }

    public final void d(TypedArray typedArray) {
        boolean z = typedArray.getBoolean(r23.PageIndicatorView_piv_interactiveAnimation, false);
        long j = typedArray.getInt(r23.PageIndicatorView_piv_animationDuration, 350);
        if (j < 0) {
            j = 0;
        }
        AnimationType a = a(typedArray.getInt(r23.PageIndicatorView_piv_animationType, AnimationType.NONE.ordinal()));
        RtlMode b = b(typedArray.getInt(r23.PageIndicatorView_piv_rtl_mode, RtlMode.Off.ordinal()));
        boolean z2 = typedArray.getBoolean(r23.PageIndicatorView_piv_fadeOnIdle, false);
        long j2 = typedArray.getInt(r23.PageIndicatorView_piv_idleDuration, 3000);
        this.a.A(j);
        this.a.J(z);
        this.a.B(a);
        this.a.S(b);
        this.a.F(z2);
        this.a.I(j2);
    }

    public final void e(TypedArray typedArray) {
        int color = typedArray.getColor(r23.PageIndicatorView_piv_unselectedColor, Color.parseColor("#33ffffff"));
        int color2 = typedArray.getColor(r23.PageIndicatorView_piv_selectedColor, Color.parseColor("#ffffff"));
        this.a.Y(color);
        this.a.U(color2);
    }

    public final void f(TypedArray typedArray) {
        int resourceId = typedArray.getResourceId(r23.PageIndicatorView_piv_viewPager, -1);
        boolean z = typedArray.getBoolean(r23.PageIndicatorView_piv_autoVisibility, true);
        int i = 0;
        boolean z2 = typedArray.getBoolean(r23.PageIndicatorView_piv_dynamicCount, false);
        int i2 = typedArray.getInt(r23.PageIndicatorView_piv_count, -1);
        if (i2 == -1) {
            i2 = 3;
        }
        int i3 = typedArray.getInt(r23.PageIndicatorView_piv_select, 0);
        if (i3 >= 0 && (i2 <= 0 || i3 <= i2 - 1)) {
            i = i3;
        }
        this.a.Z(resourceId);
        this.a.C(z);
        this.a.E(z2);
        this.a.D(i2);
        this.a.V(i);
        this.a.W(i);
        this.a.K(i);
    }

    public final void g(TypedArray typedArray) {
        int i = r23.PageIndicatorView_piv_orientation;
        Orientation orientation = Orientation.HORIZONTAL;
        if (typedArray.getInt(i, orientation.ordinal()) != 0) {
            orientation = Orientation.VERTICAL;
        }
        int dimension = (int) typedArray.getDimension(r23.PageIndicatorView_piv_radius, fm0.a(6));
        if (dimension < 0) {
            dimension = 0;
        }
        int dimension2 = (int) typedArray.getDimension(r23.PageIndicatorView_piv_padding, fm0.a(8));
        if (dimension2 < 0) {
            dimension2 = 0;
        }
        float f = typedArray.getFloat(r23.PageIndicatorView_piv_scaleFactor, 0.7f);
        if (f < 0.3f) {
            f = 0.3f;
        } else if (f > 1.0f) {
            f = 1.0f;
        }
        int dimension3 = (int) typedArray.getDimension(r23.PageIndicatorView_piv_strokeWidth, fm0.a(1));
        if (dimension3 > dimension) {
            dimension3 = dimension;
        }
        int i2 = this.a.b() == AnimationType.FILL ? dimension3 : 0;
        this.a.R(dimension);
        this.a.L(orientation);
        this.a.M(dimension2);
        this.a.T(f);
        this.a.X(i2);
    }
}
