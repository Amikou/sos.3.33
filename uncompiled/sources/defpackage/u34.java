package defpackage;

/* compiled from: Tasks.kt */
/* renamed from: u34  reason: default package */
/* loaded from: classes2.dex */
public final class u34 extends k34 {
    public final Runnable g0;

    public u34(Runnable runnable, long j, p34 p34Var) {
        super(j, p34Var);
        this.g0 = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.g0.run();
        } finally {
            this.f0.c();
        }
    }

    public String toString() {
        return "Task[" + ff0.a(this.g0) + '@' + ff0.b(this.g0) + ", " + this.a + ", " + this.f0 + ']';
    }
}
