package defpackage;

import android.app.Application;
import defpackage.pe0;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.NotificationRead;
import net.safemoon.androidwallet.model.common.FooterHistoryItem;
import net.safemoon.androidwallet.model.common.HeaderItemHistory;
import net.safemoon.androidwallet.model.common.HistoryListItem;
import net.safemoon.androidwallet.model.notificationHistory.NotificationDeleteRequest;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistory;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryData;
import net.safemoon.androidwallet.model.notificationHistory.NotificationHistoryResult;
import net.safemoon.androidwallet.model.notificationHistory.NotificationMarkReadRequest;
import net.safemoon.androidwallet.model.notificationHistory.ResultItemHistory;
import okhttp3.ResponseBody;
import retrofit2.n;

/* compiled from: NotificationHistoryViewModel.kt */
/* renamed from: qi2  reason: default package */
/* loaded from: classes2.dex */
public final class qi2 extends gd {
    public final gb2<NotificationHistory> b;
    public final gb2<String> c;

    /* compiled from: NotificationHistoryViewModel.kt */
    /* renamed from: qi2$a */
    /* loaded from: classes2.dex */
    public static final class a implements wu<ResponseBody> {
        public final /* synthetic */ rc1<te4> a;
        public final /* synthetic */ rc1<te4> b;
        public final /* synthetic */ qi2 c;

        public a(rc1<te4> rc1Var, rc1<te4> rc1Var2, qi2 qi2Var) {
            this.a = rc1Var;
            this.b = rc1Var2;
            this.c = qi2Var;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<ResponseBody> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            this.b.invoke();
            this.c.d().setValue(th.getLocalizedMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<ResponseBody> bVar, n<ResponseBody> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                this.a.invoke();
                return;
            }
            this.b.invoke();
            try {
                this.c.d().setValue(xe2.a(nVar).message);
            } catch (Exception e) {
                this.c.d().setValue(e.getLocalizedMessage());
            }
        }
    }

    /* compiled from: NotificationHistoryViewModel.kt */
    /* renamed from: qi2$b */
    /* loaded from: classes2.dex */
    public static final class b implements wu<ResponseBody> {
        public final /* synthetic */ rc1<te4> a;
        public final /* synthetic */ rc1<te4> b;
        public final /* synthetic */ qi2 c;

        public b(rc1<te4> rc1Var, rc1<te4> rc1Var2, qi2 qi2Var) {
            this.a = rc1Var;
            this.b = rc1Var2;
            this.c = qi2Var;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<ResponseBody> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            this.b.invoke();
            this.c.d().setValue(th.getLocalizedMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<ResponseBody> bVar, n<ResponseBody> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                this.a.invoke();
                return;
            }
            this.b.invoke();
            try {
                this.c.d().setValue(xe2.a(nVar).message);
            } catch (Exception e) {
                this.c.d().setValue(e.getLocalizedMessage());
            }
        }
    }

    /* compiled from: NotificationHistoryViewModel.kt */
    /* renamed from: qi2$c */
    /* loaded from: classes2.dex */
    public static final class c implements wu<NotificationHistory> {
        public c() {
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<NotificationHistory> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            qi2.this.d().setValue(th.getLocalizedMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<NotificationHistory> bVar, n<NotificationHistory> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                qi2.this.e().setValue(nVar.a());
                return;
            }
            try {
                qi2.this.d().setValue(xe2.a(nVar).message);
            } catch (Exception e) {
                qi2.this.d().setValue(e.getLocalizedMessage());
            }
        }
    }

    /* compiled from: NotificationHistoryViewModel.kt */
    /* renamed from: qi2$d */
    /* loaded from: classes2.dex */
    public static final class d implements wu<ResponseBody> {
        public final /* synthetic */ rc1<te4> a;
        public final /* synthetic */ rc1<te4> b;
        public final /* synthetic */ qi2 c;

        public d(rc1<te4> rc1Var, rc1<te4> rc1Var2, qi2 qi2Var) {
            this.a = rc1Var;
            this.b = rc1Var2;
            this.c = qi2Var;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<ResponseBody> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            this.b.invoke();
            this.c.d().setValue(th.getLocalizedMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<ResponseBody> bVar, n<ResponseBody> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                this.a.invoke();
                return;
            }
            this.b.invoke();
            try {
                this.c.d().setValue(xe2.a(nVar).message);
            } catch (Exception e) {
                this.c.d().setValue(e.getLocalizedMessage());
            }
        }
    }

    /* compiled from: NotificationHistoryViewModel.kt */
    /* renamed from: qi2$e */
    /* loaded from: classes2.dex */
    public static final class e implements wu<ResponseBody> {
        public final /* synthetic */ rc1<te4> a;
        public final /* synthetic */ rc1<te4> b;
        public final /* synthetic */ qi2 c;

        public e(rc1<te4> rc1Var, rc1<te4> rc1Var2, qi2 qi2Var) {
            this.a = rc1Var;
            this.b = rc1Var2;
            this.c = qi2Var;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<ResponseBody> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
            this.b.invoke();
            this.c.d().setValue(th.getLocalizedMessage());
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<ResponseBody> bVar, n<ResponseBody> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                this.a.invoke();
                return;
            }
            this.b.invoke();
            try {
                this.c.d().setValue(xe2.a(nVar).message);
            } catch (Exception e) {
                this.c.d().setValue(e.getLocalizedMessage());
            }
        }
    }

    /* compiled from: NotificationHistoryViewModel.kt */
    /* renamed from: qi2$f */
    /* loaded from: classes2.dex */
    public static final class f implements wu<NotificationRead> {
        public final /* synthetic */ String b;

        public f(String str) {
            this.b = str;
        }

        @Override // defpackage.wu
        public void a(retrofit2.b<NotificationRead> bVar, Throwable th) {
            fs1.f(bVar, "call");
            fs1.f(th, "t");
        }

        @Override // defpackage.wu
        public void b(retrofit2.b<NotificationRead> bVar, n<NotificationRead> nVar) {
            fs1.f(bVar, "call");
            fs1.f(nVar, "response");
            if (nVar.e()) {
                NotificationRead a = nVar.a();
                NotificationHistoryResult notificationHistoryResult = null;
                if ((a == null ? null : a.data) != null) {
                    try {
                        NotificationHistory value = qi2.this.e().getValue();
                        fs1.d(value);
                        ArrayList<NotificationHistoryResult> result = value.getData().getResult();
                        fs1.e(result, "notificationData\n       …             .data.result");
                        String str = this.b;
                        Iterator<T> it = result.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Object next = it.next();
                            if (dv3.t(((NotificationHistoryResult) next).id, str, true)) {
                                notificationHistoryResult = next;
                                break;
                            }
                        }
                        fs1.d(notificationHistoryResult);
                        notificationHistoryResult.read = true;
                        qi2.this.e().postValue(qi2.this.e().getValue());
                    } catch (Exception unused) {
                    }
                }
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public qi2(Application application) {
        super(application);
        fs1.f(application, "application");
        this.b = new gb2<>();
        this.c = new gb2<>();
        f();
    }

    public final void b(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        fs1.f(rc1Var, "onSuccess");
        fs1.f(rc1Var2, "onFailed");
        String i = bo3.i(a(), "SAFEMOON_ADDRESS");
        NotificationDeleteRequest notificationDeleteRequest = new NotificationDeleteRequest();
        notificationDeleteRequest.setAddress(i);
        notificationDeleteRequest.setDeleteAll(Boolean.TRUE);
        a4.m().l(notificationDeleteRequest).n(new a(rc1Var, rc1Var2, this));
    }

    public final void c(List<String> list, rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        fs1.f(list, "ids");
        fs1.f(rc1Var, "onSuccess");
        fs1.f(rc1Var2, "onFailed");
        String i = bo3.i(a(), "SAFEMOON_ADDRESS");
        NotificationDeleteRequest notificationDeleteRequest = new NotificationDeleteRequest();
        notificationDeleteRequest.setAddress(i);
        notificationDeleteRequest.setDeleteAll(Boolean.FALSE);
        notificationDeleteRequest.setIds(list);
        a4.m().l(notificationDeleteRequest).n(new b(rc1Var, rc1Var2, this));
    }

    public final gb2<String> d() {
        return this.c;
    }

    public final gb2<NotificationHistory> e() {
        return this.b;
    }

    public final void f() {
        String i = bo3.i(a(), "SAFEMOON_ADDRESS");
        ac3 m = a4.m();
        b30 b30Var = b30.a;
        Application a2 = a();
        fs1.e(a2, "getApplication()");
        m.a(b30.g(b30Var, a2, null, 2, null), i, 100, 0).n(new c());
    }

    public final List<HistoryListItem> g() {
        Set<Date> keySet;
        NotificationHistoryData data;
        ArrayList<NotificationHistoryResult> result;
        ArrayList arrayList = new ArrayList();
        NotificationHistory value = this.b.getValue();
        LinkedHashMap linkedHashMap = null;
        if (value != null && (data = value.getData()) != null && (result = data.getResult()) != null) {
            linkedHashMap = new LinkedHashMap();
            for (Object obj : result) {
                Calendar calendar = Calendar.getInstance();
                Date parse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).parse(((NotificationHistoryResult) obj).createdAt);
                fs1.d(parse);
                calendar.setTime(parse);
                calendar.set(11, 0);
                calendar.set(12, 0);
                calendar.set(13, 0);
                calendar.set(14, 0);
                Date time = calendar.getTime();
                Object obj2 = linkedHashMap.get(time);
                if (obj2 == null) {
                    obj2 = new ArrayList();
                    linkedHashMap.put(time, obj2);
                }
                ((List) obj2).add(obj);
            }
        }
        if (linkedHashMap != null && (keySet = linkedHashMap.keySet()) != null) {
            for (Date date : keySet) {
                HeaderItemHistory headerItemHistory = new HeaderItemHistory();
                Calendar calendar2 = Calendar.getInstance();
                calendar2.set(11, 0);
                calendar2.set(12, 0);
                calendar2.set(13, 0);
                calendar2.set(14, 0);
                calendar2.getTime();
                pe0.a aVar = pe0.a;
                Application a2 = a();
                fs1.e(a2, "getApplication()");
                SimpleDateFormat b2 = aVar.b(a2);
                headerItemHistory.setTitle(fs1.b(b2.format(date), b2.format(new Date())) ? a().getString(R.string.today) : b2.format(date));
                arrayList.add(headerItemHistory);
                List<NotificationHistoryResult> list = (List) linkedHashMap.get(date);
                if (list != null) {
                    for (NotificationHistoryResult notificationHistoryResult : list) {
                        ResultItemHistory resultItemHistory = new ResultItemHistory();
                        resultItemHistory.setResult(notificationHistoryResult);
                        arrayList.add(resultItemHistory);
                    }
                }
                arrayList.add(new FooterHistoryItem());
            }
        }
        return arrayList;
    }

    public final void h(rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        fs1.f(rc1Var, "onSuccess");
        fs1.f(rc1Var2, "onFailed");
        String i = bo3.i(a(), "SAFEMOON_ADDRESS");
        NotificationMarkReadRequest notificationMarkReadRequest = new NotificationMarkReadRequest();
        notificationMarkReadRequest.setAddress(i);
        notificationMarkReadRequest.setReadAll(Boolean.TRUE);
        a4.m().k(notificationMarkReadRequest).n(new d(rc1Var, rc1Var2, this));
    }

    public final void i(List<String> list, rc1<te4> rc1Var, rc1<te4> rc1Var2) {
        fs1.f(list, "ids");
        fs1.f(rc1Var, "onSuccess");
        fs1.f(rc1Var2, "onFailed");
        String i = bo3.i(a(), "SAFEMOON_ADDRESS");
        NotificationMarkReadRequest notificationMarkReadRequest = new NotificationMarkReadRequest();
        notificationMarkReadRequest.setAddress(i);
        notificationMarkReadRequest.setReadAll(Boolean.FALSE);
        notificationMarkReadRequest.setIds(list);
        a4.m().k(notificationMarkReadRequest).n(new e(rc1Var, rc1Var2, this));
    }

    public final void j(String str) {
        fs1.f(str, "notificationId");
        a4.m().i(str).n(new f(str));
    }
}
