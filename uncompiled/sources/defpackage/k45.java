package defpackage;

import android.database.ContentObserver;
import android.os.Handler;

/* renamed from: k45  reason: default package */
/* loaded from: classes.dex */
public final class k45 extends ContentObserver {
    public final /* synthetic */ g45 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k45(g45 g45Var, Handler handler) {
        super(null);
        this.a = g45Var;
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        this.a.d();
        this.a.f();
    }
}
