package defpackage;

import androidx.media3.common.g;
import androidx.media3.common.j;
import defpackage.f84;
import java.io.EOFException;
import java.io.IOException;

/* compiled from: DummyTrackOutput.java */
/* renamed from: ks0  reason: default package */
/* loaded from: classes.dex */
public final class ks0 implements f84 {
    public final byte[] a = new byte[4096];

    @Override // defpackage.f84
    public /* synthetic */ void a(op2 op2Var, int i) {
        e84.b(this, op2Var, i);
    }

    @Override // defpackage.f84
    public void b(long j, int i, int i2, int i3, f84.a aVar) {
    }

    @Override // defpackage.f84
    public void c(op2 op2Var, int i, int i2) {
        op2Var.Q(i);
    }

    @Override // defpackage.f84
    public /* synthetic */ int d(g gVar, int i, boolean z) {
        return e84.a(this, gVar, i, z);
    }

    @Override // defpackage.f84
    public int e(g gVar, int i, boolean z, int i2) throws IOException {
        int read = gVar.read(this.a, 0, Math.min(this.a.length, i));
        if (read == -1) {
            if (z) {
                return -1;
            }
            throw new EOFException();
        }
        return read;
    }

    @Override // defpackage.f84
    public void f(j jVar) {
    }
}
