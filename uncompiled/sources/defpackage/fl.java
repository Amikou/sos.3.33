package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_Session_OperatingSystem.java */
/* renamed from: fl  reason: default package */
/* loaded from: classes2.dex */
public final class fl extends r90.e.AbstractC0277e {
    public final int a;
    public final String b;
    public final String c;
    public final boolean d;

    /* compiled from: AutoValue_CrashlyticsReport_Session_OperatingSystem.java */
    /* renamed from: fl$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.e.AbstractC0277e.a {
        public Integer a;
        public String b;
        public String c;
        public Boolean d;

        @Override // defpackage.r90.e.AbstractC0277e.a
        public r90.e.AbstractC0277e a() {
            String str = "";
            if (this.a == null) {
                str = " platform";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (this.c == null) {
                str = str + " buildVersion";
            }
            if (this.d == null) {
                str = str + " jailbroken";
            }
            if (str.isEmpty()) {
                return new fl(this.a.intValue(), this.b, this.c, this.d.booleanValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.e.AbstractC0277e.a
        public r90.e.AbstractC0277e.a b(String str) {
            Objects.requireNonNull(str, "Null buildVersion");
            this.c = str;
            return this;
        }

        @Override // defpackage.r90.e.AbstractC0277e.a
        public r90.e.AbstractC0277e.a c(boolean z) {
            this.d = Boolean.valueOf(z);
            return this;
        }

        @Override // defpackage.r90.e.AbstractC0277e.a
        public r90.e.AbstractC0277e.a d(int i) {
            this.a = Integer.valueOf(i);
            return this;
        }

        @Override // defpackage.r90.e.AbstractC0277e.a
        public r90.e.AbstractC0277e.a e(String str) {
            Objects.requireNonNull(str, "Null version");
            this.b = str;
            return this;
        }
    }

    @Override // defpackage.r90.e.AbstractC0277e
    public String b() {
        return this.c;
    }

    @Override // defpackage.r90.e.AbstractC0277e
    public int c() {
        return this.a;
    }

    @Override // defpackage.r90.e.AbstractC0277e
    public String d() {
        return this.b;
    }

    @Override // defpackage.r90.e.AbstractC0277e
    public boolean e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.e.AbstractC0277e) {
            r90.e.AbstractC0277e abstractC0277e = (r90.e.AbstractC0277e) obj;
            return this.a == abstractC0277e.c() && this.b.equals(abstractC0277e.d()) && this.c.equals(abstractC0277e.b()) && this.d == abstractC0277e.e();
        }
        return false;
    }

    public int hashCode() {
        return ((((((this.a ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ (this.d ? 1231 : 1237);
    }

    public String toString() {
        return "OperatingSystem{platform=" + this.a + ", version=" + this.b + ", buildVersion=" + this.c + ", jailbroken=" + this.d + "}";
    }

    public fl(int i, String str, String str2, boolean z) {
        this.a = i;
        this.b = str;
        this.c = str2;
        this.d = z;
    }
}
