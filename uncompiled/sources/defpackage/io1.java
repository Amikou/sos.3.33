package defpackage;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import defpackage.l80;
import java.io.Closeable;

/* compiled from: ImagePerfControllerListener2.java */
/* renamed from: io1  reason: default package */
/* loaded from: classes.dex */
public class io1 extends dn<ao1> implements Closeable {
    public final o92 a;
    public final po1 f0;
    public final no1 g0;
    public final fw3<Boolean> h0;
    public final fw3<Boolean> i0;
    public Handler j0;

    /* compiled from: ImagePerfControllerListener2.java */
    /* renamed from: io1$a */
    /* loaded from: classes.dex */
    public static class a extends Handler {
        public final no1 a;

        public a(Looper looper, no1 no1Var) {
            super(looper);
            this.a = no1Var;
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            po1 po1Var = (po1) xt2.g(message.obj);
            int i = message.what;
            if (i == 1) {
                this.a.b(po1Var, message.arg1);
            } else if (i != 2) {
            } else {
                this.a.a(po1Var, message.arg1);
            }
        }
    }

    public io1(o92 o92Var, po1 po1Var, no1 no1Var, fw3<Boolean> fw3Var, fw3<Boolean> fw3Var2) {
        this.a = o92Var;
        this.f0 = po1Var;
        this.g0 = no1Var;
        this.h0 = fw3Var;
        this.i0 = fw3Var2;
    }

    @Override // defpackage.dn, defpackage.l80
    public void c(String str, l80.a aVar) {
        long now = this.a.now();
        po1 h = h();
        h.m(aVar);
        h.h(str);
        int a2 = h.a();
        if (a2 != 3 && a2 != 5 && a2 != 6) {
            h.e(now);
            r(h, 4);
        }
        l(h, now);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        n();
    }

    @Override // defpackage.dn, defpackage.l80
    public void d(String str, Throwable th, l80.a aVar) {
        long now = this.a.now();
        po1 h = h();
        h.m(aVar);
        h.f(now);
        h.h(str);
        h.l(th);
        r(h, 5);
        l(h, now);
    }

    @Override // defpackage.dn, defpackage.l80
    public void e(String str, Object obj, l80.a aVar) {
        long now = this.a.now();
        po1 h = h();
        h.c();
        h.k(now);
        h.h(str);
        h.d(obj);
        h.m(aVar);
        r(h, 0);
        m(h, now);
    }

    public final synchronized void g() {
        if (this.j0 != null) {
            return;
        }
        HandlerThread handlerThread = new HandlerThread("ImagePerfControllerListener2Thread");
        handlerThread.start();
        this.j0 = new a((Looper) xt2.g(handlerThread.getLooper()), this.g0);
    }

    public final po1 h() {
        return this.i0.get().booleanValue() ? new po1() : this.f0;
    }

    @Override // defpackage.dn, defpackage.l80
    /* renamed from: i */
    public void b(String str, ao1 ao1Var, l80.a aVar) {
        long now = this.a.now();
        po1 h = h();
        h.m(aVar);
        h.g(now);
        h.r(now);
        h.h(str);
        h.n(ao1Var);
        r(h, 3);
    }

    @Override // defpackage.dn, defpackage.l80
    /* renamed from: j */
    public void a(String str, ao1 ao1Var) {
        long now = this.a.now();
        po1 h = h();
        h.j(now);
        h.h(str);
        h.n(ao1Var);
        r(h, 2);
    }

    public final void l(po1 po1Var, long j) {
        po1Var.A(false);
        po1Var.t(j);
        u(po1Var, 2);
    }

    public void m(po1 po1Var, long j) {
        po1Var.A(true);
        po1Var.z(j);
        u(po1Var, 1);
    }

    public void n() {
        h().b();
    }

    public final boolean q() {
        boolean booleanValue = this.h0.get().booleanValue();
        if (booleanValue && this.j0 == null) {
            g();
        }
        return booleanValue;
    }

    public final void r(po1 po1Var, int i) {
        if (q()) {
            Message obtainMessage = ((Handler) xt2.g(this.j0)).obtainMessage();
            obtainMessage.what = 1;
            obtainMessage.arg1 = i;
            obtainMessage.obj = po1Var;
            this.j0.sendMessage(obtainMessage);
            return;
        }
        this.g0.b(po1Var, i);
    }

    public final void u(po1 po1Var, int i) {
        if (q()) {
            Message obtainMessage = ((Handler) xt2.g(this.j0)).obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = i;
            obtainMessage.obj = po1Var;
            this.j0.sendMessage(obtainMessage);
            return;
        }
        this.g0.a(po1Var, i);
    }
}
