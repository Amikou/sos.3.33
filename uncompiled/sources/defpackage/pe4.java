package defpackage;

import android.os.Bundle;

/* compiled from: UnavailableAnalyticsEventLogger.java */
/* renamed from: pe4  reason: default package */
/* loaded from: classes2.dex */
public class pe4 implements qb {
    @Override // defpackage.qb
    public void a(String str, Bundle bundle) {
        w12.f().b("Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
    }
}
