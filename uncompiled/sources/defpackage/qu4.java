package defpackage;

import com.google.android.play.core.internal.bf;
import com.google.android.play.core.internal.d;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: qu4  reason: default package */
/* loaded from: classes2.dex */
public final class qu4 implements fu4 {
    public final /* synthetic */ int a = 0;

    public qu4() {
    }

    public qu4(byte[] bArr) {
    }

    public qu4(char[] cArr) {
    }

    public qu4(float[] fArr) {
    }

    public qu4(int[] iArr) {
    }

    public qu4(short[] sArr) {
    }

    public qu4(boolean[] zArr) {
    }

    public qu4(byte[][] bArr) {
    }

    public static void c(ClassLoader classLoader, Set<File> set) {
        if (set.isEmpty()) {
            return;
        }
        HashSet hashSet = new HashSet();
        for (File file : set) {
            String valueOf = String.valueOf(file.getParentFile().getAbsolutePath());
            if (valueOf.length() != 0) {
                "Adding native library parent directory: ".concat(valueOf);
            }
            hashSet.add(file.getParentFile());
        }
        zu4 e = d.e(e(classLoader), "nativeLibraryDirectories", File.class);
        hashSet.removeAll(Arrays.asList((File[]) e.a()));
        synchronized (xx4.class) {
            int size = hashSet.size();
            StringBuilder sb = new StringBuilder(30);
            sb.append("Adding directories ");
            sb.append(size);
            e.e(hashSet);
        }
    }

    public static boolean d(ClassLoader classLoader, File file, File file2, boolean z, ku4 ku4Var, String str, iu4 iu4Var) {
        ArrayList<IOException> arrayList = new ArrayList<>();
        Object e = e(classLoader);
        zu4 e2 = d.e(e, "dexElements", Object.class);
        List<Object> asList = Arrays.asList((Object[]) e2.a());
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : asList) {
            arrayList2.add((File) d.d(obj, str, File.class).a());
        }
        if (arrayList2.contains(file2)) {
            return true;
        }
        if (!z && !iu4Var.a(e, file2, file)) {
            String valueOf = String.valueOf(file2.getPath());
            if (valueOf.length() != 0) {
                "Should be optimized ".concat(valueOf);
            }
            return false;
        }
        e2.d(Arrays.asList(ku4Var.a(e, new ArrayList<>(Collections.singleton(file2)), file, arrayList)));
        if (arrayList.isEmpty()) {
            return true;
        }
        bf bfVar = new bf("DexPathList.makeDexElement failed");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            yv4.a(bfVar, arrayList.get(i));
        }
        d.e(e, "dexElementsSuppressedExceptions", IOException.class).d(arrayList);
        throw bfVar;
    }

    public static Object e(ClassLoader classLoader) {
        return d.d(classLoader, "pathList", Object.class).a();
    }

    public static ku4 f() {
        return new ru4(null);
    }

    public static iu4 g() {
        return new wu4((byte[]) null);
    }

    public static void h(ClassLoader classLoader, Set<File> set, tu4 tu4Var) {
        if (set.isEmpty()) {
            return;
        }
        HashSet hashSet = new HashSet();
        for (File file : set) {
            hashSet.add(file.getParentFile());
        }
        Object e = e(classLoader);
        zu4 d = d.d(e, "nativeLibraryDirectories", List.class);
        synchronized (xx4.class) {
            ArrayList arrayList = new ArrayList((Collection) d.a());
            hashSet.removeAll(arrayList);
            arrayList.addAll(hashSet);
            d.b(arrayList);
        }
        ArrayList arrayList2 = new ArrayList();
        Object[] a = tu4Var.a(e, new ArrayList(hashSet), arrayList2);
        if (arrayList2.isEmpty()) {
            synchronized (xx4.class) {
                d.e(e, "nativeLibraryPathElements", Object.class).e(Arrays.asList(a));
            }
            return;
        }
        bf bfVar = new bf("Error in makePathElements");
        int size = arrayList2.size();
        for (int i = 0; i < size; i++) {
            yv4.a(bfVar, arrayList2.get(i));
        }
        throw bfVar;
    }

    public static ku4 i() {
        return new ru4();
    }

    public static tu4 j() {
        return new uu4(null);
    }

    public static boolean k(ClassLoader classLoader, File file, File file2, boolean z) {
        return d(classLoader, file, file2, z, i(), "zip", g());
    }

    public static void l(ClassLoader classLoader, Set<File> set) {
        h(classLoader, set, new uu4());
    }

    public static boolean m(ClassLoader classLoader, File file, File file2, boolean z) {
        return d(classLoader, file, file2, z, i(), "path", new wu4());
    }

    @Override // defpackage.fu4
    public final boolean a(ClassLoader classLoader, File file, File file2, boolean z) {
        ku4 f;
        iu4 g;
        String str;
        switch (this.a) {
            case 0:
            case 1:
                f = f();
                g = g();
                str = "zip";
                break;
            case 2:
                return k(classLoader, file, file2, z);
            case 3:
                return k(classLoader, file, file2, z);
            case 4:
                return k(classLoader, file, file2, z);
            case 5:
                return m(classLoader, file, file2, z);
            case 6:
                return m(classLoader, file, file2, z);
            default:
                f = i();
                g = new wu4((char[]) null);
                str = "path";
                break;
        }
        return d(classLoader, file, file2, z, f, str, g);
    }

    @Override // defpackage.fu4
    public final void b(ClassLoader classLoader, Set set) {
        switch (this.a) {
            case 0:
                c(classLoader, set);
                return;
            case 1:
                c(classLoader, set);
                return;
            case 2:
            case 3:
            case 4:
                h(classLoader, set, j());
                return;
            case 5:
                l(classLoader, set);
                return;
            case 6:
                l(classLoader, set);
                return;
            default:
                l(classLoader, set);
                return;
        }
    }
}
