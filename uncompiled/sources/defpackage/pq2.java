package defpackage;

import androidx.media3.common.ParserException;
import defpackage.gc4;
import zendesk.support.request.CellBase;

/* compiled from: PesReader.java */
/* renamed from: pq2  reason: default package */
/* loaded from: classes.dex */
public final class pq2 implements gc4 {
    public final ku0 a;
    public final np2 b = new np2(new byte[10]);
    public int c = 0;
    public int d;
    public h64 e;
    public boolean f;
    public boolean g;
    public boolean h;
    public int i;
    public int j;
    public boolean k;
    public long l;

    public pq2(ku0 ku0Var) {
        this.a = ku0Var;
    }

    @Override // defpackage.gc4
    public final void a(op2 op2Var, int i) throws ParserException {
        ii.i(this.e);
        if ((i & 1) != 0) {
            int i2 = this.c;
            if (i2 != 0 && i2 != 1) {
                if (i2 == 2) {
                    p12.i("PesReader", "Unexpected start indicator reading extended header");
                } else if (i2 == 3) {
                    if (this.j != -1) {
                        p12.i("PesReader", "Unexpected start indicator: expected " + this.j + " more bytes");
                    }
                    this.a.d();
                } else {
                    throw new IllegalStateException();
                }
            }
            g(1);
        }
        while (op2Var.a() > 0) {
            int i3 = this.c;
            if (i3 != 0) {
                if (i3 != 1) {
                    if (i3 == 2) {
                        if (d(op2Var, this.b.a, Math.min(10, this.i)) && d(op2Var, null, this.i)) {
                            f();
                            i |= this.k ? 4 : 0;
                            this.a.e(this.l, i);
                            g(3);
                        }
                    } else if (i3 == 3) {
                        int a = op2Var.a();
                        int i4 = this.j;
                        int i5 = i4 != -1 ? a - i4 : 0;
                        if (i5 > 0) {
                            a -= i5;
                            op2Var.O(op2Var.e() + a);
                        }
                        this.a.a(op2Var);
                        int i6 = this.j;
                        if (i6 != -1) {
                            int i7 = i6 - a;
                            this.j = i7;
                            if (i7 == 0) {
                                this.a.d();
                                g(1);
                            }
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                } else if (d(op2Var, this.b.a, 9)) {
                    g(e() ? 2 : 0);
                }
            } else {
                op2Var.Q(op2Var.a());
            }
        }
    }

    @Override // defpackage.gc4
    public void b(h64 h64Var, r11 r11Var, gc4.d dVar) {
        this.e = h64Var;
        this.a.f(r11Var, dVar);
    }

    @Override // defpackage.gc4
    public final void c() {
        this.c = 0;
        this.d = 0;
        this.h = false;
        this.a.c();
    }

    public final boolean d(op2 op2Var, byte[] bArr, int i) {
        int min = Math.min(op2Var.a(), i - this.d);
        if (min <= 0) {
            return true;
        }
        if (bArr == null) {
            op2Var.Q(min);
        } else {
            op2Var.j(bArr, this.d, min);
        }
        int i2 = this.d + min;
        this.d = i2;
        return i2 == i;
    }

    public final boolean e() {
        this.b.p(0);
        int h = this.b.h(24);
        if (h != 1) {
            p12.i("PesReader", "Unexpected start code prefix: " + h);
            this.j = -1;
            return false;
        }
        this.b.r(8);
        int h2 = this.b.h(16);
        this.b.r(5);
        this.k = this.b.g();
        this.b.r(2);
        this.f = this.b.g();
        this.g = this.b.g();
        this.b.r(6);
        int h3 = this.b.h(8);
        this.i = h3;
        if (h2 == 0) {
            this.j = -1;
        } else {
            int i = ((h2 + 6) - 9) - h3;
            this.j = i;
            if (i < 0) {
                p12.i("PesReader", "Found negative packet payload size: " + this.j);
                this.j = -1;
            }
        }
        return true;
    }

    public final void f() {
        this.b.p(0);
        this.l = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        if (this.f) {
            this.b.r(4);
            this.b.r(1);
            this.b.r(1);
            long h = (this.b.h(3) << 30) | (this.b.h(15) << 15) | this.b.h(15);
            this.b.r(1);
            if (!this.h && this.g) {
                this.b.r(4);
                this.b.r(1);
                this.b.r(1);
                this.b.r(1);
                this.e.b((this.b.h(3) << 30) | (this.b.h(15) << 15) | this.b.h(15));
                this.h = true;
            }
            this.l = this.e.b(h);
        }
    }

    public final void g(int i) {
        this.c = i;
        this.d = 0;
    }
}
