package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: hq5  reason: default package */
/* loaded from: classes.dex */
public final class hq5 implements Runnable {
    public final /* synthetic */ qq5 a;

    public hq5(qq5 qq5Var) {
        this.a = qq5Var;
    }

    @Override // java.lang.Runnable
    public final void run() {
        bq5 bq5Var;
        qq5 qq5Var = this.a;
        bq5Var = qq5Var.j;
        qq5Var.e = bq5Var;
    }
}
