package defpackage;

import android.util.SparseIntArray;

/* compiled from: PoolParams.java */
/* renamed from: ys2  reason: default package */
/* loaded from: classes.dex */
public class ys2 {
    public final int a;
    public final int b;
    public final SparseIntArray c;
    public boolean d;
    public final int e;

    public ys2(int i, int i2, SparseIntArray sparseIntArray) {
        this(i, i2, sparseIntArray, 0, Integer.MAX_VALUE, -1);
    }

    public ys2(int i, int i2, SparseIntArray sparseIntArray, int i3, int i4, int i5) {
        xt2.i(i >= 0 && i2 >= i);
        this.b = i;
        this.a = i2;
        this.c = sparseIntArray;
        this.e = i5;
    }
}
