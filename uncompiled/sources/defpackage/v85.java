package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.List;
import org.web3j.abi.datatypes.Utf8String;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: v85  reason: default package */
/* loaded from: classes.dex */
public final class v85 extends o65 {
    public v85() {
        this.a.add(zzbl.ASSIGN);
        this.a.add(zzbl.CONST);
        this.a.add(zzbl.CREATE_ARRAY);
        this.a.add(zzbl.CREATE_OBJECT);
        this.a.add(zzbl.EXPRESSION_LIST);
        this.a.add(zzbl.GET);
        this.a.add(zzbl.GET_INDEX);
        this.a.add(zzbl.GET_PROPERTY);
        this.a.add(zzbl.NULL);
        this.a.add(zzbl.SET_PROPERTY);
        this.a.add(zzbl.TYPEOF);
        this.a.add(zzbl.UNDEFINED);
        this.a.add(zzbl.VAR);
    }

    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        String str2;
        zzbl zzblVar = zzbl.ADD;
        int ordinal = vm5.e(str).ordinal();
        int i = 0;
        if (ordinal == 3) {
            vm5.a(zzbl.ASSIGN.name(), 2, list);
            z55 a = wk5Var.a(list.get(0));
            if (a instanceof f65) {
                if (wk5Var.d(a.zzc())) {
                    z55 a2 = wk5Var.a(list.get(1));
                    wk5Var.e(a.zzc(), a2);
                    return a2;
                }
                throw new IllegalArgumentException(String.format("Attempting to assign undefined value %s", a.zzc()));
            }
            throw new IllegalArgumentException(String.format("Expected string for assign var. got %s", a.getClass().getCanonicalName()));
        } else if (ordinal == 14) {
            vm5.b(zzbl.CONST.name(), 2, list);
            if (list.size() % 2 == 0) {
                for (int i2 = 0; i2 < list.size() - 1; i2 += 2) {
                    z55 a3 = wk5Var.a(list.get(i2));
                    if (a3 instanceof f65) {
                        wk5Var.g(a3.zzc(), wk5Var.a(list.get(i2 + 1)));
                    } else {
                        throw new IllegalArgumentException(String.format("Expected string for const name. got %s", a3.getClass().getCanonicalName()));
                    }
                }
                return z55.X;
            }
            throw new IllegalArgumentException(String.format("CONST requires an even number of arguments, found %s", Integer.valueOf(list.size())));
        } else if (ordinal == 24) {
            vm5.b(zzbl.EXPRESSION_LIST.name(), 1, list);
            z55 z55Var = z55.X;
            while (i < list.size()) {
                z55Var = wk5Var.a(list.get(i));
                if (z55Var instanceof v45) {
                    throw new IllegalStateException("ControlValue cannot be in an expression list");
                }
                i++;
            }
            return z55Var;
        } else if (ordinal == 33) {
            vm5.a(zzbl.GET.name(), 1, list);
            z55 a4 = wk5Var.a(list.get(0));
            if (a4 instanceof f65) {
                return wk5Var.h(a4.zzc());
            }
            throw new IllegalArgumentException(String.format("Expected string for get var. got %s", a4.getClass().getCanonicalName()));
        } else if (ordinal == 49) {
            vm5.a(zzbl.NULL.name(), 0, list);
            return z55.Y;
        } else if (ordinal == 58) {
            vm5.a(zzbl.SET_PROPERTY.name(), 3, list);
            z55 a5 = wk5Var.a(list.get(0));
            z55 a6 = wk5Var.a(list.get(1));
            z55 a7 = wk5Var.a(list.get(2));
            if (a5 != z55.X && a5 != z55.Y) {
                if ((a5 instanceof p45) && (a6 instanceof z45)) {
                    ((p45) a5).y(a6.b().intValue(), a7);
                } else if (a5 instanceof m55) {
                    ((m55) a5).k(a6.zzc(), a7);
                }
                return a7;
            }
            throw new IllegalStateException(String.format("Can't set property %s of %s", a6.zzc(), a5.zzc()));
        } else if (ordinal == 17) {
            if (list.isEmpty()) {
                return new p45();
            }
            p45 p45Var = new p45();
            for (z55 z55Var2 : list) {
                z55 a8 = wk5Var.a(z55Var2);
                if (!(a8 instanceof v45)) {
                    p45Var.y(i, a8);
                    i++;
                } else {
                    throw new IllegalStateException("Failed to evaluate array element");
                }
            }
            return p45Var;
        } else if (ordinal == 18) {
            if (list.isEmpty()) {
                return new p55();
            }
            if (list.size() % 2 == 0) {
                p55 p55Var = new p55();
                while (i < list.size() - 1) {
                    z55 a9 = wk5Var.a(list.get(i));
                    z55 a10 = wk5Var.a(list.get(i + 1));
                    if (!(a9 instanceof v45) && !(a10 instanceof v45)) {
                        p55Var.k(a9.zzc(), a10);
                        i += 2;
                    } else {
                        throw new IllegalStateException("Failed to evaluate map entry");
                    }
                }
                return p55Var;
            }
            throw new IllegalArgumentException(String.format("CREATE_OBJECT requires an even number of arguments, found %s", Integer.valueOf(list.size())));
        } else if (ordinal != 35 && ordinal != 36) {
            switch (ordinal) {
                case 62:
                    vm5.a(zzbl.TYPEOF.name(), 1, list);
                    z55 a11 = wk5Var.a(list.get(0));
                    if (a11 instanceof i65) {
                        str2 = "undefined";
                    } else if (a11 instanceof s45) {
                        str2 = "boolean";
                    } else if (a11 instanceof z45) {
                        str2 = "number";
                    } else if (a11 instanceof f65) {
                        str2 = Utf8String.TYPE_NAME;
                    } else if (a11 instanceof u55) {
                        str2 = "function";
                    } else if ((a11 instanceof a65) || (a11 instanceof v45)) {
                        throw new IllegalArgumentException(String.format("Unsupported value type %s in typeof", a11));
                    } else {
                        str2 = "object";
                    }
                    return new f65(str2);
                case 63:
                    vm5.a(zzbl.UNDEFINED.name(), 0, list);
                    return z55.X;
                case 64:
                    vm5.b(zzbl.VAR.name(), 1, list);
                    for (z55 z55Var3 : list) {
                        z55 a12 = wk5Var.a(z55Var3);
                        if (a12 instanceof f65) {
                            wk5Var.f(a12.zzc(), z55.X);
                        } else {
                            throw new IllegalArgumentException(String.format("Expected string for var name. got %s", a12.getClass().getCanonicalName()));
                        }
                    }
                    return z55.X;
                default:
                    return super.b(str);
            }
        } else {
            vm5.a(zzbl.GET_PROPERTY.name(), 2, list);
            z55 a13 = wk5Var.a(list.get(0));
            z55 a14 = wk5Var.a(list.get(1));
            if ((a13 instanceof p45) && vm5.d(a14)) {
                return ((p45) a13).w(a14.b().intValue());
            }
            if (a13 instanceof m55) {
                return ((m55) a13).e(a14.zzc());
            }
            if (a13 instanceof f65) {
                if ("length".equals(a14.zzc())) {
                    return new z45(Double.valueOf(a13.zzc().length()));
                }
                if (vm5.d(a14) && a14.b().doubleValue() < a13.zzc().length()) {
                    return new f65(String.valueOf(a13.zzc().charAt(a14.b().intValue())));
                }
            }
            return z55.X;
        }
    }
}
