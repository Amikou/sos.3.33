package defpackage;

import defpackage.la2;

/* compiled from: ConstantBitrateSeeker.java */
/* renamed from: a60  reason: default package */
/* loaded from: classes.dex */
public final class a60 extends z50 implements zi3 {
    public a60(long j, long j2, la2.a aVar, boolean z) {
        super(j, j2, aVar.f, aVar.c, z);
    }

    @Override // defpackage.zi3
    public long b(long j) {
        return c(j);
    }

    @Override // defpackage.zi3
    public long d() {
        return -1L;
    }
}
