package defpackage;

import com.bumptech.glide.Registry;
import defpackage.j92;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: MultiModelLoaderFactory.java */
/* renamed from: qa2  reason: default package */
/* loaded from: classes.dex */
public class qa2 {
    public static final c e = new c();
    public static final j92<Object, Object> f = new a();
    public final List<b<?, ?>> a;
    public final c b;
    public final Set<b<?, ?>> c;
    public final et2<List<Throwable>> d;

    /* compiled from: MultiModelLoaderFactory.java */
    /* renamed from: qa2$a */
    /* loaded from: classes.dex */
    public static class a implements j92<Object, Object> {
        @Override // defpackage.j92
        public boolean a(Object obj) {
            return false;
        }

        @Override // defpackage.j92
        public j92.a<Object> b(Object obj, int i, int i2, vn2 vn2Var) {
            return null;
        }
    }

    /* compiled from: MultiModelLoaderFactory.java */
    /* renamed from: qa2$b */
    /* loaded from: classes.dex */
    public static class b<Model, Data> {
        public final Class<Model> a;
        public final Class<Data> b;
        public final k92<? extends Model, ? extends Data> c;

        public b(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var) {
            this.a = cls;
            this.b = cls2;
            this.c = k92Var;
        }

        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }

        public boolean b(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }
    }

    /* compiled from: MultiModelLoaderFactory.java */
    /* renamed from: qa2$c */
    /* loaded from: classes.dex */
    public static class c {
        public <Model, Data> pa2<Model, Data> a(List<j92<Model, Data>> list, et2<List<Throwable>> et2Var) {
            return new pa2<>(list, et2Var);
        }
    }

    public qa2(et2<List<Throwable>> et2Var) {
        this(et2Var, e);
    }

    public static <Model, Data> j92<Model, Data> f() {
        return (j92<Model, Data>) f;
    }

    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var, boolean z) {
        b<?, ?> bVar = new b<>(cls, cls2, k92Var);
        List<b<?, ?>> list = this.a;
        list.add(z ? list.size() : 0, bVar);
    }

    public synchronized <Model, Data> void b(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var) {
        a(cls, cls2, k92Var, true);
    }

    public final <Model, Data> j92<Model, Data> c(b<?, ?> bVar) {
        return (j92) wt2.d(bVar.c.c(this));
    }

    public synchronized <Model, Data> j92<Model, Data> d(Class<Model> cls, Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (b<?, ?> bVar : this.a) {
                if (this.c.contains(bVar)) {
                    z = true;
                } else if (bVar.b(cls, cls2)) {
                    this.c.add(bVar);
                    arrayList.add(c(bVar));
                    this.c.remove(bVar);
                }
            }
            if (arrayList.size() > 1) {
                return this.b.a(arrayList, this.d);
            } else if (arrayList.size() == 1) {
                return (j92) arrayList.get(0);
            } else if (z) {
                return f();
            } else {
                throw new Registry.NoModelLoaderAvailableException((Class<?>) cls, (Class<?>) cls2);
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
    }

    public synchronized <Model> List<j92<Model, ?>> e(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (b<?, ?> bVar : this.a) {
                if (!this.c.contains(bVar) && bVar.a(cls)) {
                    this.c.add(bVar);
                    arrayList.add(c(bVar));
                    this.c.remove(bVar);
                }
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
        return arrayList;
    }

    public synchronized List<Class<?>> g(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (b<?, ?> bVar : this.a) {
            if (!arrayList.contains(bVar.b) && bVar.a(cls)) {
                arrayList.add(bVar.b);
            }
        }
        return arrayList;
    }

    public final <Model, Data> k92<Model, Data> h(b<?, ?> bVar) {
        return (k92<Model, Data>) bVar.c;
    }

    public synchronized <Model, Data> List<k92<? extends Model, ? extends Data>> i(Class<Model> cls, Class<Data> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        Iterator<b<?, ?>> it = this.a.iterator();
        while (it.hasNext()) {
            b<?, ?> next = it.next();
            if (next.b(cls, cls2)) {
                it.remove();
                arrayList.add(h(next));
            }
        }
        return arrayList;
    }

    public synchronized <Model, Data> List<k92<? extends Model, ? extends Data>> j(Class<Model> cls, Class<Data> cls2, k92<? extends Model, ? extends Data> k92Var) {
        List<k92<? extends Model, ? extends Data>> i;
        i = i(cls, cls2);
        b(cls, cls2, k92Var);
        return i;
    }

    public qa2(et2<List<Throwable>> et2Var, c cVar) {
        this.a = new ArrayList();
        this.c = new HashSet();
        this.d = et2Var;
        this.b = cVar;
    }
}
