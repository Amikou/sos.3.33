package defpackage;

import java.math.BigInteger;

/* renamed from: ur4  reason: default package */
/* loaded from: classes2.dex */
public class ur4 {
    public int a(xs0 xs0Var) {
        return (xs0Var.u() + 7) / 8;
    }

    public int b(ct0 ct0Var) {
        return (ct0Var.f() + 7) / 8;
    }

    public byte[] c(BigInteger bigInteger, int i) {
        byte[] byteArray = bigInteger.toByteArray();
        if (i < byteArray.length) {
            byte[] bArr = new byte[i];
            System.arraycopy(byteArray, byteArray.length - i, bArr, 0, i);
            return bArr;
        } else if (i > byteArray.length) {
            byte[] bArr2 = new byte[i];
            System.arraycopy(byteArray, 0, bArr2, i - byteArray.length, byteArray.length);
            return bArr2;
        } else {
            return byteArray;
        }
    }
}
