package defpackage;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import java.util.Locale;

/* compiled from: Extensions.kt */
/* renamed from: m11  reason: default package */
/* loaded from: classes2.dex */
public final class m11 {
    public static final Locale a(Configuration configuration) {
        Locale locale;
        String str;
        fs1.g(configuration, "$this$getLocaleCompat");
        if (b(24)) {
            locale = configuration.getLocales().get(0);
            str = "locales.get(0)";
        } else {
            locale = configuration.locale;
            str = "locale";
        }
        fs1.c(locale, str);
        return locale;
    }

    public static final boolean b(int i) {
        return Build.VERSION.SDK_INT >= i;
    }

    public static final void c(Activity activity) {
        fs1.g(activity, "$this$resetTitle");
        try {
            int i = activity.getPackageManager().getActivityInfo(activity.getComponentName(), 128).labelRes;
            if (i != 0) {
                activity.setTitle(i);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
