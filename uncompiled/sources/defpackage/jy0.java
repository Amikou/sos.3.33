package defpackage;

import androidx.media3.extractor.metadata.emsg.EventMessage;

/* compiled from: EventStream.java */
/* renamed from: jy0  reason: default package */
/* loaded from: classes.dex */
public final class jy0 {
    public final EventMessage[] a;
    public final long[] b;
    public final String c;
    public final String d;

    public jy0(String str, String str2, long j, long[] jArr, EventMessage[] eventMessageArr) {
        this.c = str;
        this.d = str2;
        this.b = jArr;
        this.a = eventMessageArr;
    }

    public String a() {
        return this.c + "/" + this.d;
    }
}
