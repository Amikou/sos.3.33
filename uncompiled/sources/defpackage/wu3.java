package defpackage;

/* compiled from: Appendable.kt */
/* renamed from: wu3  reason: default package */
/* loaded from: classes2.dex */
public class wu3 {
    public static final <T> void a(Appendable appendable, T t, tc1<? super T, ? extends CharSequence> tc1Var) {
        fs1.f(appendable, "$this$appendElement");
        if (tc1Var != null) {
            appendable.append(tc1Var.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append((CharSequence) t);
        } else if (t instanceof Character) {
            appendable.append(((Character) t).charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
