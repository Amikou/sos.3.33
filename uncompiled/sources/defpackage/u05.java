package defpackage;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: u05  reason: default package */
/* loaded from: classes.dex */
public interface u05 {
    void a(ConnectionResult connectionResult);

    void b(int i, boolean z);

    void d(Bundle bundle);
}
