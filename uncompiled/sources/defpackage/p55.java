package defpackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: p55  reason: default package */
/* loaded from: classes.dex */
public class p55 implements z55, m55 {
    public final Map<String, z55> a = new HashMap();

    public final List<String> a() {
        return new ArrayList(this.a.keySet());
    }

    @Override // defpackage.z55
    public final Double b() {
        return Double.valueOf(Double.NaN);
    }

    @Override // defpackage.z55
    public final Boolean c() {
        return Boolean.TRUE;
    }

    @Override // defpackage.m55
    public final z55 e(String str) {
        return this.a.containsKey(str) ? this.a.get(str) : z55.X;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof p55) {
            return this.a.equals(((p55) obj).a);
        }
        return false;
    }

    public final int hashCode() {
        return this.a.hashCode();
    }

    @Override // defpackage.z55
    public final Iterator<z55> i() {
        return g55.b(this.a);
    }

    @Override // defpackage.m55
    public final void k(String str, z55 z55Var) {
        if (z55Var == null) {
            this.a.remove(str);
        } else {
            this.a.put(str, z55Var);
        }
    }

    @Override // defpackage.z55
    public final z55 m() {
        p55 p55Var = new p55();
        for (Map.Entry<String, z55> entry : this.a.entrySet()) {
            if (entry.getValue() instanceof m55) {
                p55Var.a.put(entry.getKey(), entry.getValue());
            } else {
                p55Var.a.put(entry.getKey(), entry.getValue().m());
            }
        }
        return p55Var;
    }

    @Override // defpackage.z55
    public z55 n(String str, wk5 wk5Var, List<z55> list) {
        if ("toString".equals(str)) {
            return new f65(toString());
        }
        return g55.a(this, new f65(str), wk5Var, list);
    }

    @Override // defpackage.m55
    public final boolean o(String str) {
        return this.a.containsKey(str);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("{");
        if (!this.a.isEmpty()) {
            for (String str : this.a.keySet()) {
                sb.append(String.format("%s: %s,", str, this.a.get(str)));
            }
            sb.deleteCharAt(sb.lastIndexOf(","));
        }
        sb.append("}");
        return sb.toString();
    }

    @Override // defpackage.z55
    public final String zzc() {
        return "[object Object]";
    }
}
