package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import net.safemoon.androidwallet.R;

/* compiled from: ItemMyTokensScreenBinding.java */
/* renamed from: et1  reason: default package */
/* loaded from: classes2.dex */
public final class et1 {
    public final ConstraintLayout a;
    public final ImageView b;
    public final ImageView c;
    public final TextView d;
    public final TextView e;
    public final TextView f;
    public final TextView g;
    public final TextView h;
    public final View i;

    public et1(ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, ConstraintLayout constraintLayout2, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, View view, LinearLayout linearLayout) {
        this.a = constraintLayout;
        this.b = imageView;
        this.c = imageView2;
        this.d = textView;
        this.e = textView2;
        this.f = textView3;
        this.g = textView4;
        this.h = textView5;
        this.i = view;
    }

    public static et1 a(View view) {
        int i = R.id.ivPercentageDirection;
        ImageView imageView = (ImageView) ai4.a(view, R.id.ivPercentageDirection);
        if (imageView != null) {
            i = R.id.ivTokenIcon;
            ImageView imageView2 = (ImageView) ai4.a(view, R.id.ivTokenIcon);
            if (imageView2 != null) {
                i = R.id.rowFG;
                ConstraintLayout constraintLayout = (ConstraintLayout) ai4.a(view, R.id.rowFG);
                if (constraintLayout != null) {
                    i = R.id.tvTokenBalance;
                    TextView textView = (TextView) ai4.a(view, R.id.tvTokenBalance);
                    if (textView != null) {
                        i = R.id.tvTokenName;
                        TextView textView2 = (TextView) ai4.a(view, R.id.tvTokenName);
                        if (textView2 != null) {
                            i = R.id.tvTokenNativeBalance;
                            TextView textView3 = (TextView) ai4.a(view, R.id.tvTokenNativeBalance);
                            if (textView3 != null) {
                                i = R.id.tvTokenPercent;
                                TextView textView4 = (TextView) ai4.a(view, R.id.tvTokenPercent);
                                if (textView4 != null) {
                                    i = R.id.tvTokenSymbol;
                                    TextView textView5 = (TextView) ai4.a(view, R.id.tvTokenSymbol);
                                    if (textView5 != null) {
                                        i = R.id.vDivider;
                                        View a = ai4.a(view, R.id.vDivider);
                                        if (a != null) {
                                            i = R.id.vTokenNameContainer;
                                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.vTokenNameContainer);
                                            if (linearLayout != null) {
                                                return new et1((ConstraintLayout) view, imageView, imageView2, constraintLayout, textView, textView2, textView3, textView4, textView5, a, linearLayout);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
