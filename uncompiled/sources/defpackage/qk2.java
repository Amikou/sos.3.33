package defpackage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OSOutcomeSourceBody.kt */
/* renamed from: qk2  reason: default package */
/* loaded from: classes2.dex */
public final class qk2 {
    public JSONArray a;
    public JSONArray b;

    public qk2() {
        this(null, null, 3, null);
    }

    public qk2(JSONArray jSONArray, JSONArray jSONArray2) {
        this.a = jSONArray;
        this.b = jSONArray2;
    }

    public final JSONArray a() {
        return this.b;
    }

    public final JSONArray b() {
        return this.a;
    }

    public final void c(JSONArray jSONArray) {
        this.b = jSONArray;
    }

    public final void d(JSONArray jSONArray) {
        this.a = jSONArray;
    }

    public final JSONObject e() throws JSONException {
        JSONObject put = new JSONObject().put("notification_ids", this.a).put("in_app_message_ids", this.b);
        fs1.e(put, "JSONObject()\n        .pu…AM_IDS, inAppMessagesIds)");
        return put;
    }

    public String toString() {
        return "OSOutcomeSourceBody{notificationIds=" + this.a + ", inAppMessagesIds=" + this.b + '}';
    }

    public /* synthetic */ qk2(JSONArray jSONArray, JSONArray jSONArray2, int i, qi0 qi0Var) {
        this((i & 1) != 0 ? new JSONArray() : jSONArray, (i & 2) != 0 ? new JSONArray() : jSONArray2);
    }
}
