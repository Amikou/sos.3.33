package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: wx5  reason: default package */
/* loaded from: classes.dex */
public final class wx5 implements Iterator<Map.Entry<K, V>> {
    public int a;
    public Iterator<Map.Entry<K, V>> f0;
    public final /* synthetic */ ux5 g0;

    public wx5(ux5 ux5Var) {
        List list;
        this.g0 = ux5Var;
        list = ux5Var.f0;
        this.a = list.size();
    }

    public final Iterator<Map.Entry<K, V>> a() {
        Map map;
        if (this.f0 == null) {
            map = this.g0.j0;
            this.f0 = map.entrySet().iterator();
        }
        return this.f0;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        List list;
        int i = this.a;
        if (i > 0) {
            list = this.g0.f0;
            if (i <= list.size()) {
                return true;
            }
        }
        return a().hasNext();
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        List list;
        if (!a().hasNext()) {
            list = this.g0.f0;
            int i = this.a - 1;
            this.a = i;
            return (Map.Entry) list.get(i);
        }
        return (Map.Entry) a().next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ wx5(ux5 ux5Var, sx5 sx5Var) {
        this(ux5Var);
    }
}
