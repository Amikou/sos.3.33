package defpackage;

import java.util.Map;

/* compiled from: StatefulProducerRunnable.java */
/* renamed from: gt3  reason: default package */
/* loaded from: classes.dex */
public abstract class gt3<T> extends ht3<T> {
    public final l60<T> f0;
    public final iv2 g0;
    public final String h0;
    public final ev2 i0;

    public gt3(l60<T> l60Var, iv2 iv2Var, ev2 ev2Var, String str) {
        this.f0 = l60Var;
        this.g0 = iv2Var;
        this.h0 = str;
        this.i0 = ev2Var;
        iv2Var.k(ev2Var, str);
    }

    @Override // defpackage.ht3
    public void d() {
        iv2 iv2Var = this.g0;
        ev2 ev2Var = this.i0;
        String str = this.h0;
        iv2Var.d(ev2Var, str, iv2Var.j(ev2Var, str) ? g() : null);
        this.f0.b();
    }

    @Override // defpackage.ht3
    public void e(Exception exc) {
        iv2 iv2Var = this.g0;
        ev2 ev2Var = this.i0;
        String str = this.h0;
        iv2Var.c(ev2Var, str, exc, iv2Var.j(ev2Var, str) ? h(exc) : null);
        this.f0.a(exc);
    }

    @Override // defpackage.ht3
    public void f(T t) {
        iv2 iv2Var = this.g0;
        ev2 ev2Var = this.i0;
        String str = this.h0;
        iv2Var.a(ev2Var, str, iv2Var.j(ev2Var, str) ? i(t) : null);
        this.f0.d(t, 1);
    }

    public Map<String, String> g() {
        return null;
    }

    public Map<String, String> h(Exception exc) {
        return null;
    }

    public Map<String, String> i(T t) {
        return null;
    }
}
