package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.textview.MaterialTextView;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentCollectiblesBinding.java */
/* renamed from: u91  reason: default package */
/* loaded from: classes2.dex */
public final class u91 {
    public final LinearLayout a;
    public final AppCompatImageView b;
    public final AppCompatImageView c;
    public final TextView d;
    public final LinearLayout e;
    public final LinearLayout f;
    public final ry1 g;
    public final TextView h;
    public final RecyclerView i;
    public final SwipeRefreshLayout j;
    public final TextView k;
    public final MaterialTextView l;
    public final TextView m;

    public u91(LinearLayout linearLayout, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, LinearLayout linearLayout2, TextView textView, LinearLayout linearLayout3, LinearLayout linearLayout4, ry1 ry1Var, TextView textView2, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, TextView textView3, MaterialTextView materialTextView, TextView textView4) {
        this.a = linearLayout;
        this.b = appCompatImageView;
        this.c = appCompatImageView2;
        this.d = textView;
        this.e = linearLayout3;
        this.f = linearLayout4;
        this.g = ry1Var;
        this.h = textView2;
        this.i = recyclerView;
        this.j = swipeRefreshLayout;
        this.k = textView3;
        this.l = materialTextView;
        this.m = textView4;
    }

    public static u91 a(View view) {
        int i = R.id.btnForHide;
        AppCompatImageView appCompatImageView = (AppCompatImageView) ai4.a(view, R.id.btnForHide);
        if (appCompatImageView != null) {
            i = R.id.btnInfo;
            AppCompatImageView appCompatImageView2 = (AppCompatImageView) ai4.a(view, R.id.btnInfo);
            if (appCompatImageView2 != null) {
                LinearLayout linearLayout = (LinearLayout) view;
                i = R.id.emptyPlaceholder;
                TextView textView = (TextView) ai4.a(view, R.id.emptyPlaceholder);
                if (textView != null) {
                    i = R.id.errorWrapper;
                    LinearLayout linearLayout2 = (LinearLayout) ai4.a(view, R.id.errorWrapper);
                    if (linearLayout2 != null) {
                        i = R.id.headerWrapper;
                        LinearLayout linearLayout3 = (LinearLayout) ai4.a(view, R.id.headerWrapper);
                        if (linearLayout3 != null) {
                            i = R.id.lSelectTokenType;
                            View a = ai4.a(view, R.id.lSelectTokenType);
                            if (a != null) {
                                ry1 a2 = ry1.a(a);
                                i = R.id.learn_more;
                                TextView textView2 = (TextView) ai4.a(view, R.id.learn_more);
                                if (textView2 != null) {
                                    i = R.id.nftCollections;
                                    RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.nftCollections);
                                    if (recyclerView != null) {
                                        i = R.id.swRefresh;
                                        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swRefresh);
                                        if (swipeRefreshLayout != null) {
                                            i = R.id.title;
                                            TextView textView3 = (TextView) ai4.a(view, R.id.title);
                                            if (textView3 != null) {
                                                i = R.id.tvMainWallet;
                                                MaterialTextView materialTextView = (MaterialTextView) ai4.a(view, R.id.tvMainWallet);
                                                if (materialTextView != null) {
                                                    i = R.id.whatAreCollectibles;
                                                    TextView textView4 = (TextView) ai4.a(view, R.id.whatAreCollectibles);
                                                    if (textView4 != null) {
                                                        return new u91(linearLayout, appCompatImageView, appCompatImageView2, linearLayout, textView, linearLayout2, linearLayout3, a2, textView2, recyclerView, swipeRefreshLayout, textView3, materialTextView, textView4);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public LinearLayout b() {
        return this.a;
    }
}
