package defpackage;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: GlobalLibraryVersionRegistrar.java */
/* renamed from: og1  reason: default package */
/* loaded from: classes2.dex */
public class og1 {
    public static volatile og1 b;
    public final Set<hz1> a = new HashSet();

    public static og1 a() {
        og1 og1Var = b;
        if (og1Var == null) {
            synchronized (og1.class) {
                og1Var = b;
                if (og1Var == null) {
                    og1Var = new og1();
                    b = og1Var;
                }
            }
        }
        return og1Var;
    }

    public Set<hz1> b() {
        Set<hz1> unmodifiableSet;
        synchronized (this.a) {
            unmodifiableSet = Collections.unmodifiableSet(this.a);
        }
        return unmodifiableSet;
    }
}
