package defpackage;

import android.app.Activity;

/* compiled from: RecentAppsThumbnailHidingListener.kt */
/* renamed from: p43  reason: default package */
/* loaded from: classes.dex */
public interface p43 {

    /* compiled from: RecentAppsThumbnailHidingListener.kt */
    /* renamed from: p43$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static void a(p43 p43Var, Activity activity, boolean z) {
            fs1.f(p43Var, "this");
            fs1.f(activity, "activity");
            m43.a(activity, z);
        }
    }

    void o(Activity activity, boolean z);
}
