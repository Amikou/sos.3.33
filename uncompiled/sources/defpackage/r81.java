package defpackage;

import android.content.Context;

/* compiled from: ForceTokenLogoUpdateUseCaseProvider.kt */
/* renamed from: r81  reason: default package */
/* loaded from: classes2.dex */
public final class r81 {
    public static final r81 a = new r81();
    public static am1 b;

    public final am1 a(Context context) {
        fs1.f(context, "context");
        if (b == null) {
            synchronized (this) {
                if (b == null) {
                    b = new q81(gg4.a.b(context), e53.a.a(context), qg1.a);
                }
                te4 te4Var = te4.a;
            }
        }
        am1 am1Var = b;
        fs1.d(am1Var);
        return am1Var;
    }
}
