package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: vf3  reason: default package */
/* loaded from: classes2.dex */
public class vf3 extends ct0.a {
    public long[] f;

    public vf3() {
        this.f = ad2.f();
    }

    public vf3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 113) {
            throw new IllegalArgumentException("x value invalid for SecT113FieldElement");
        }
        this.f = uf3.d(bigInteger);
    }

    public vf3(long[] jArr) {
        this.f = jArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        long[] f = ad2.f();
        uf3.a(this.f, ((vf3) ct0Var).f, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        long[] f = ad2.f();
        uf3.c(this.f, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        return j(ct0Var.g());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof vf3) {
            return ad2.j(this.f, ((vf3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return 113;
    }

    @Override // defpackage.ct0
    public ct0 g() {
        long[] f = ad2.f();
        uf3.h(this.f, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ad2.p(this.f);
    }

    public int hashCode() {
        return wh.v(this.f, 0, 2) ^ 113009;
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ad2.r(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        long[] f = ad2.f();
        uf3.i(this.f, ((vf3) ct0Var).f, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return l(ct0Var, ct0Var2, ct0Var3);
    }

    @Override // defpackage.ct0
    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        long[] jArr = this.f;
        long[] jArr2 = ((vf3) ct0Var).f;
        long[] jArr3 = ((vf3) ct0Var2).f;
        long[] jArr4 = ((vf3) ct0Var3).f;
        long[] h = ad2.h();
        uf3.j(jArr, jArr2, h);
        uf3.j(jArr3, jArr4, h);
        long[] f = ad2.f();
        uf3.k(h, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        return this;
    }

    @Override // defpackage.ct0
    public ct0 n() {
        long[] f = ad2.f();
        uf3.m(this.f, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 o() {
        long[] f = ad2.f();
        uf3.n(this.f, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        long[] jArr = this.f;
        long[] jArr2 = ((vf3) ct0Var).f;
        long[] jArr3 = ((vf3) ct0Var2).f;
        long[] h = ad2.h();
        uf3.o(jArr, h);
        uf3.j(jArr2, jArr3, h);
        long[] f = ad2.f();
        uf3.k(h, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 q(int i) {
        if (i < 1) {
            return this;
        }
        long[] f = ad2.f();
        uf3.p(this.f, i, f);
        return new vf3(f);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        return a(ct0Var);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return (this.f[0] & 1) != 0;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ad2.y(this.f);
    }

    @Override // defpackage.ct0.a
    public int u() {
        return uf3.q(this.f);
    }
}
