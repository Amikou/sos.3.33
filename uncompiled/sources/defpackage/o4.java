package defpackage;

import android.graphics.RectF;
import java.util.Arrays;

/* compiled from: AbsoluteCornerSize.java */
/* renamed from: o4  reason: default package */
/* loaded from: classes2.dex */
public final class o4 implements v80 {
    public final float a;

    public o4(float f) {
        this.a = f;
    }

    @Override // defpackage.v80
    public float a(RectF rectF) {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof o4) && this.a == ((o4) obj).a;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.a)});
    }
}
