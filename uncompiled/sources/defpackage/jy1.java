package defpackage;

/* compiled from: LanguageProviderAppDefined.java */
/* renamed from: jy1  reason: default package */
/* loaded from: classes2.dex */
public class jy1 implements iy1 {
    public final vk2 a;

    public jy1(vk2 vk2Var) {
        this.a = vk2Var;
    }

    @Override // defpackage.iy1
    public String a() {
        vk2 vk2Var = this.a;
        return vk2Var.e(vk2Var.f(), "PREFS_OS_LANGUAGE", "en");
    }
}
