package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: bh3  reason: default package */
/* loaded from: classes2.dex */
public class bh3 extends ct0.a {
    public long[] f;

    public bh3() {
        this.f = ed2.i();
    }

    public bh3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 239) {
            throw new IllegalArgumentException("x value invalid for SecT239FieldElement");
        }
        this.f = ah3.d(bigInteger);
    }

    public bh3(long[] jArr) {
        this.f = jArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        long[] i = ed2.i();
        ah3.a(this.f, ((bh3) ct0Var).f, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        long[] i = ed2.i();
        ah3.c(this.f, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        return j(ct0Var.g());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof bh3) {
            return ed2.n(this.f, ((bh3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return 239;
    }

    @Override // defpackage.ct0
    public ct0 g() {
        long[] i = ed2.i();
        ah3.j(this.f, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ed2.u(this.f);
    }

    public int hashCode() {
        return wh.v(this.f, 0, 4) ^ 23900158;
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ed2.w(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        long[] i = ed2.i();
        ah3.k(this.f, ((bh3) ct0Var).f, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return l(ct0Var, ct0Var2, ct0Var3);
    }

    @Override // defpackage.ct0
    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        long[] jArr = this.f;
        long[] jArr2 = ((bh3) ct0Var).f;
        long[] jArr3 = ((bh3) ct0Var2).f;
        long[] jArr4 = ((bh3) ct0Var3).f;
        long[] k = ed2.k();
        ah3.l(jArr, jArr2, k);
        ah3.l(jArr3, jArr4, k);
        long[] i = ed2.i();
        ah3.m(k, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        return this;
    }

    @Override // defpackage.ct0
    public ct0 n() {
        long[] i = ed2.i();
        ah3.o(this.f, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 o() {
        long[] i = ed2.i();
        ah3.p(this.f, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        long[] jArr = this.f;
        long[] jArr2 = ((bh3) ct0Var).f;
        long[] jArr3 = ((bh3) ct0Var2).f;
        long[] k = ed2.k();
        ah3.q(jArr, k);
        ah3.l(jArr2, jArr3, k);
        long[] i = ed2.i();
        ah3.m(k, i);
        return new bh3(i);
    }

    @Override // defpackage.ct0
    public ct0 q(int i) {
        if (i < 1) {
            return this;
        }
        long[] i2 = ed2.i();
        ah3.r(this.f, i, i2);
        return new bh3(i2);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        return a(ct0Var);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return (this.f[0] & 1) != 0;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ed2.K(this.f);
    }

    @Override // defpackage.ct0.a
    public int u() {
        return ah3.s(this.f);
    }
}
