package defpackage;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.bumptech.glide.e;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: GifFrameLoader.java */
/* renamed from: ag1  reason: default package */
/* loaded from: classes.dex */
public class ag1 {
    public final tf1 a;
    public final Handler b;
    public final List<b> c;
    public final k73 d;
    public final jq e;
    public boolean f;
    public boolean g;
    public boolean h;
    public e<Bitmap> i;
    public a j;
    public boolean k;
    public a l;
    public Bitmap m;
    public za4<Bitmap> n;
    public a o;
    public d p;
    public int q;
    public int r;
    public int s;

    /* compiled from: GifFrameLoader.java */
    /* renamed from: ag1$a */
    /* loaded from: classes.dex */
    public static class a extends wc0<Bitmap> {
        public final Handler h0;
        public final int i0;
        public final long j0;
        public Bitmap k0;

        public a(Handler handler, int i, long j) {
            this.h0 = handler;
            this.i0 = i;
            this.j0 = j;
        }

        public Bitmap d() {
            return this.k0;
        }

        @Override // defpackage.i34
        /* renamed from: i */
        public void j(Bitmap bitmap, hb4<? super Bitmap> hb4Var) {
            this.k0 = bitmap;
            this.h0.sendMessageAtTime(this.h0.obtainMessage(1, this), this.j0);
        }

        @Override // defpackage.i34
        public void m(Drawable drawable) {
            this.k0 = null;
        }
    }

    /* compiled from: GifFrameLoader.java */
    /* renamed from: ag1$b */
    /* loaded from: classes.dex */
    public interface b {
        void a();
    }

    /* compiled from: GifFrameLoader.java */
    /* renamed from: ag1$c */
    /* loaded from: classes.dex */
    public class c implements Handler.Callback {
        public c() {
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                ag1.this.m((a) message.obj);
                return true;
            } else if (i == 2) {
                ag1.this.d.o((a) message.obj);
                return false;
            } else {
                return false;
            }
        }
    }

    /* compiled from: GifFrameLoader.java */
    /* renamed from: ag1$d */
    /* loaded from: classes.dex */
    public interface d {
        void a();
    }

    public ag1(com.bumptech.glide.a aVar, tf1 tf1Var, int i, int i2, za4<Bitmap> za4Var, Bitmap bitmap) {
        this(aVar.f(), com.bumptech.glide.a.t(aVar.h()), tf1Var, null, i(com.bumptech.glide.a.t(aVar.h()), i, i2), za4Var, bitmap);
    }

    public static fx1 g() {
        return new ll2(Double.valueOf(Math.random()));
    }

    public static e<Bitmap> i(k73 k73Var, int i, int i2) {
        return k73Var.i().a(n73.x0(bp0.a).t0(true).o0(true).d0(i, i2));
    }

    public void a() {
        this.c.clear();
        n();
        q();
        a aVar = this.j;
        if (aVar != null) {
            this.d.o(aVar);
            this.j = null;
        }
        a aVar2 = this.l;
        if (aVar2 != null) {
            this.d.o(aVar2);
            this.l = null;
        }
        a aVar3 = this.o;
        if (aVar3 != null) {
            this.d.o(aVar3);
            this.o = null;
        }
        this.a.clear();
        this.k = true;
    }

    public ByteBuffer b() {
        return this.a.getData().asReadOnlyBuffer();
    }

    public Bitmap c() {
        a aVar = this.j;
        return aVar != null ? aVar.d() : this.m;
    }

    public int d() {
        a aVar = this.j;
        if (aVar != null) {
            return aVar.i0;
        }
        return -1;
    }

    public Bitmap e() {
        return this.m;
    }

    public int f() {
        return this.a.a();
    }

    public int h() {
        return this.s;
    }

    public int j() {
        return this.a.h() + this.q;
    }

    public int k() {
        return this.r;
    }

    public final void l() {
        if (!this.f || this.g) {
            return;
        }
        if (this.h) {
            wt2.a(this.o == null, "Pending target must be null when starting from the first frame");
            this.a.f();
            this.h = false;
        }
        a aVar = this.o;
        if (aVar != null) {
            this.o = null;
            m(aVar);
            return;
        }
        this.g = true;
        long uptimeMillis = SystemClock.uptimeMillis() + this.a.d();
        this.a.c();
        this.l = new a(this.b, this.a.g(), uptimeMillis);
        this.i.a(n73.z0(g())).Q0(this.a).D0(this.l);
    }

    public void m(a aVar) {
        d dVar = this.p;
        if (dVar != null) {
            dVar.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f) {
            if (this.h) {
                this.b.obtainMessage(2, aVar).sendToTarget();
            } else {
                this.o = aVar;
            }
        } else {
            if (aVar.d() != null) {
                n();
                a aVar2 = this.j;
                this.j = aVar;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (aVar2 != null) {
                    this.b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            l();
        }
    }

    public final void n() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.c(bitmap);
            this.m = null;
        }
    }

    public void o(za4<Bitmap> za4Var, Bitmap bitmap) {
        this.n = (za4) wt2.d(za4Var);
        this.m = (Bitmap) wt2.d(bitmap);
        this.i = this.i.a(new n73().p0(za4Var));
        this.q = mg4.h(bitmap);
        this.r = bitmap.getWidth();
        this.s = bitmap.getHeight();
    }

    public final void p() {
        if (this.f) {
            return;
        }
        this.f = true;
        this.k = false;
        l();
    }

    public final void q() {
        this.f = false;
    }

    public void r(b bVar) {
        if (!this.k) {
            if (!this.c.contains(bVar)) {
                boolean isEmpty = this.c.isEmpty();
                this.c.add(bVar);
                if (isEmpty) {
                    p();
                    return;
                }
                return;
            }
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
        throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
    }

    public void s(b bVar) {
        this.c.remove(bVar);
        if (this.c.isEmpty()) {
            q();
        }
    }

    public ag1(jq jqVar, k73 k73Var, tf1 tf1Var, Handler handler, e<Bitmap> eVar, za4<Bitmap> za4Var, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = k73Var;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.e = jqVar;
        this.b = handler;
        this.i = eVar;
        this.a = tf1Var;
        o(za4Var, bitmap);
    }
}
