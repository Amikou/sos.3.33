package defpackage;

import defpackage.ck3;
import java.util.Objects;

/* compiled from: AutoValue_SendRequest.java */
/* renamed from: ol  reason: default package */
/* loaded from: classes.dex */
public final class ol extends ck3 {
    public final ob4 a;
    public final String b;
    public final com.google.android.datatransport.a<?> c;
    public final fb4<?, byte[]> d;
    public final hv0 e;

    /* compiled from: AutoValue_SendRequest.java */
    /* renamed from: ol$b */
    /* loaded from: classes.dex */
    public static final class b extends ck3.a {
        public ob4 a;
        public String b;
        public com.google.android.datatransport.a<?> c;
        public fb4<?, byte[]> d;
        public hv0 e;

        @Override // defpackage.ck3.a
        public ck3 a() {
            String str = "";
            if (this.a == null) {
                str = " transportContext";
            }
            if (this.b == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " event";
            }
            if (this.d == null) {
                str = str + " transformer";
            }
            if (this.e == null) {
                str = str + " encoding";
            }
            if (str.isEmpty()) {
                return new ol(this.a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.ck3.a
        public ck3.a b(hv0 hv0Var) {
            Objects.requireNonNull(hv0Var, "Null encoding");
            this.e = hv0Var;
            return this;
        }

        @Override // defpackage.ck3.a
        public ck3.a c(com.google.android.datatransport.a<?> aVar) {
            Objects.requireNonNull(aVar, "Null event");
            this.c = aVar;
            return this;
        }

        @Override // defpackage.ck3.a
        public ck3.a d(fb4<?, byte[]> fb4Var) {
            Objects.requireNonNull(fb4Var, "Null transformer");
            this.d = fb4Var;
            return this;
        }

        @Override // defpackage.ck3.a
        public ck3.a e(ob4 ob4Var) {
            Objects.requireNonNull(ob4Var, "Null transportContext");
            this.a = ob4Var;
            return this;
        }

        @Override // defpackage.ck3.a
        public ck3.a f(String str) {
            Objects.requireNonNull(str, "Null transportName");
            this.b = str;
            return this;
        }
    }

    @Override // defpackage.ck3
    public hv0 b() {
        return this.e;
    }

    @Override // defpackage.ck3
    public com.google.android.datatransport.a<?> c() {
        return this.c;
    }

    @Override // defpackage.ck3
    public fb4<?, byte[]> e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ck3) {
            ck3 ck3Var = (ck3) obj;
            return this.a.equals(ck3Var.f()) && this.b.equals(ck3Var.g()) && this.c.equals(ck3Var.c()) && this.d.equals(ck3Var.e()) && this.e.equals(ck3Var.b());
        }
        return false;
    }

    @Override // defpackage.ck3
    public ob4 f() {
        return this.a;
    }

    @Override // defpackage.ck3
    public String g() {
        return this.b;
    }

    public int hashCode() {
        return ((((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003) ^ this.e.hashCode();
    }

    public String toString() {
        return "SendRequest{transportContext=" + this.a + ", transportName=" + this.b + ", event=" + this.c + ", transformer=" + this.d + ", encoding=" + this.e + "}";
    }

    public ol(ob4 ob4Var, String str, com.google.android.datatransport.a<?> aVar, fb4<?, byte[]> fb4Var, hv0 hv0Var) {
        this.a = ob4Var;
        this.b = str;
        this.c = aVar;
        this.d = fb4Var;
        this.e = hv0Var;
    }
}
