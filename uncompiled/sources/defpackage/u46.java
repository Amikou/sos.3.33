package defpackage;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: u46  reason: default package */
/* loaded from: classes.dex */
public final class u46 extends c55 {
    public final j46 g0;

    public u46(j46 j46Var) {
        super("internal.logger");
        this.g0 = j46Var;
        this.f0.put("log", new p46(this, false, true));
        this.f0.put("silent", new e26(this, "silent"));
        ((c55) this.f0.get("silent")).k("log", new p46(this, true, true));
        this.f0.put("unmonitored", new k36(this, "unmonitored"));
        ((c55) this.f0.get("unmonitored")).k("log", new p46(this, false, false));
    }

    @Override // defpackage.c55
    public final z55 a(wk5 wk5Var, List<z55> list) {
        return z55.X;
    }
}
