package defpackage;

/* renamed from: a34  reason: default package */
/* loaded from: classes2.dex */
public final class a34 {
    public static final int a() {
        return b34.a();
    }

    public static final int b(String str, int i, int i2, int i3) {
        return c34.a(str, i, i2, i3);
    }

    public static final long c(String str, long j, long j2, long j3) {
        return c34.b(str, j, j2, j3);
    }

    public static final String d(String str) {
        return b34.b(str);
    }

    public static final boolean e(String str, boolean z) {
        return c34.c(str, z);
    }
}
