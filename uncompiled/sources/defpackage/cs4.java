package defpackage;

import java.io.IOException;
import java.util.Objects;
import org.bouncycastle.pqc.crypto.xmss.BDSStateMap;
import org.bouncycastle.pqc.crypto.xmss.f;
import org.bouncycastle.pqc.crypto.xmss.i;

/* renamed from: cs4  reason: default package */
/* loaded from: classes2.dex */
public final class cs4 extends pi {
    public final f a;
    public final long f0;
    public final byte[] g0;
    public final byte[] h0;
    public final byte[] i0;
    public final byte[] j0;
    public final BDSStateMap k0;

    /* renamed from: cs4$b */
    /* loaded from: classes2.dex */
    public static class b {
        public final f a;
        public long b = 0;
        public byte[] c = null;
        public byte[] d = null;
        public byte[] e = null;
        public byte[] f = null;
        public BDSStateMap g = null;
        public byte[] h = null;
        public fs4 i = null;

        public b(f fVar) {
            this.a = fVar;
        }

        public cs4 j() {
            return new cs4(this);
        }

        public b k(BDSStateMap bDSStateMap) {
            this.g = bDSStateMap;
            return this;
        }

        public b l(long j) {
            this.b = j;
            return this;
        }

        public b m(byte[] bArr) {
            this.e = i.c(bArr);
            return this;
        }

        public b n(byte[] bArr) {
            this.f = i.c(bArr);
            return this;
        }

        public b o(byte[] bArr) {
            this.d = i.c(bArr);
            return this;
        }

        public b p(byte[] bArr) {
            this.c = i.c(bArr);
            return this;
        }
    }

    public cs4(b bVar) {
        super(true);
        f fVar = bVar.a;
        this.a = fVar;
        Objects.requireNonNull(fVar, "params == null");
        int b2 = fVar.b();
        byte[] bArr = bVar.h;
        if (bArr != null) {
            Objects.requireNonNull(bVar.i, "xmss == null");
            int c = fVar.c();
            int i = (c + 7) / 8;
            long a2 = i.a(bArr, 0, i);
            this.f0 = a2;
            if (!i.l(c, a2)) {
                throw new IllegalArgumentException("index out of bounds");
            }
            int i2 = i + 0;
            this.g0 = i.g(bArr, i2, b2);
            int i3 = i2 + b2;
            this.h0 = i.g(bArr, i3, b2);
            int i4 = i3 + b2;
            this.i0 = i.g(bArr, i4, b2);
            int i5 = i4 + b2;
            this.j0 = i.g(bArr, i5, b2);
            int i6 = i5 + b2;
            try {
                BDSStateMap bDSStateMap = (BDSStateMap) i.f(i.g(bArr, i6, bArr.length - i6), BDSStateMap.class);
                bDSStateMap.setXMSS(bVar.i);
                this.k0 = bDSStateMap;
                return;
            } catch (IOException e) {
                throw new IllegalArgumentException(e.getMessage(), e);
            } catch (ClassNotFoundException e2) {
                throw new IllegalArgumentException(e2.getMessage(), e2);
            }
        }
        this.f0 = bVar.b;
        byte[] bArr2 = bVar.c;
        if (bArr2 == null) {
            this.g0 = new byte[b2];
        } else if (bArr2.length != b2) {
            throw new IllegalArgumentException("size of secretKeySeed needs to be equal size of digest");
        } else {
            this.g0 = bArr2;
        }
        byte[] bArr3 = bVar.d;
        if (bArr3 == null) {
            this.h0 = new byte[b2];
        } else if (bArr3.length != b2) {
            throw new IllegalArgumentException("size of secretKeyPRF needs to be equal size of digest");
        } else {
            this.h0 = bArr3;
        }
        byte[] bArr4 = bVar.e;
        if (bArr4 == null) {
            this.i0 = new byte[b2];
        } else if (bArr4.length != b2) {
            throw new IllegalArgumentException("size of publicSeed needs to be equal size of digest");
        } else {
            this.i0 = bArr4;
        }
        byte[] bArr5 = bVar.f;
        if (bArr5 == null) {
            this.j0 = new byte[b2];
        } else if (bArr5.length != b2) {
            throw new IllegalArgumentException("size of root needs to be equal size of digest");
        } else {
            this.j0 = bArr5;
        }
        BDSStateMap bDSStateMap2 = bVar.g;
        if (bDSStateMap2 == null) {
            if (!i.l(fVar.c(), bVar.b) || bArr4 == null || bArr2 == null) {
                this.k0 = new BDSStateMap();
                return;
            }
            bDSStateMap2 = new BDSStateMap(fVar, bVar.b, bArr4, bArr2);
        }
        this.k0 = bDSStateMap2;
    }

    public f a() {
        return this.a;
    }

    public byte[] b() {
        int b2 = this.a.b();
        int c = (this.a.c() + 7) / 8;
        byte[] bArr = new byte[c + b2 + b2 + b2 + b2];
        i.e(bArr, i.p(this.f0, c), 0);
        int i = c + 0;
        i.e(bArr, this.g0, i);
        int i2 = i + b2;
        i.e(bArr, this.h0, i2);
        int i3 = i2 + b2;
        i.e(bArr, this.i0, i3);
        i.e(bArr, this.j0, i3 + b2);
        try {
            return wh.i(bArr, i.o(this.k0));
        } catch (IOException e) {
            throw new IllegalStateException("error serializing bds state: " + e.getMessage(), e);
        }
    }
}
