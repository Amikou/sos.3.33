package defpackage;

import java.util.Objects;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: fr5  reason: default package */
/* loaded from: classes.dex */
public final class fr5 {
    public int a;
    public long b;
    public Object c;
    public final qt5 d;

    public fr5(qt5 qt5Var) {
        Objects.requireNonNull(qt5Var);
        this.d = qt5Var;
    }
}
