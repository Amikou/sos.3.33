package defpackage;

/* compiled from: StartOffsetExtractorInput.java */
/* renamed from: ps3  reason: default package */
/* loaded from: classes.dex */
public final class ps3 extends e91 {
    public final long b;

    public ps3(q11 q11Var, long j) {
        super(q11Var);
        ii.a(q11Var.getPosition() >= j);
        this.b = j;
    }

    @Override // defpackage.e91, defpackage.q11
    public long e() {
        return super.e() - this.b;
    }

    @Override // defpackage.e91, defpackage.q11
    public long getLength() {
        return super.getLength() - this.b;
    }

    @Override // defpackage.e91, defpackage.q11
    public long getPosition() {
        return super.getPosition() - this.b;
    }
}
