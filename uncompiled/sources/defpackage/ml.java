package defpackage;

import com.google.android.datatransport.cct.internal.k;

/* compiled from: AutoValue_LogResponse.java */
/* renamed from: ml  reason: default package */
/* loaded from: classes.dex */
public final class ml extends k {
    public final long a;

    public ml(long j) {
        this.a = j;
    }

    @Override // com.google.android.datatransport.cct.internal.k
    public long c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof k) && this.a == ((k) obj).c();
    }

    public int hashCode() {
        long j = this.a;
        return ((int) (j ^ (j >>> 32))) ^ 1000003;
    }

    public String toString() {
        return "LogResponse{nextRequestWaitMillis=" + this.a + "}";
    }
}
