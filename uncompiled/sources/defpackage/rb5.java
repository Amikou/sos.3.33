package defpackage;

import java.util.List;
import java.util.RandomAccess;

/* renamed from: rb5  reason: default package */
/* loaded from: classes.dex */
public interface rb5<E> extends List<E>, RandomAccess {
    rb5<E> T0(int i);

    boolean u();

    void v();
}
