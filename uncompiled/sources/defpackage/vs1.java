package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.imageview.ShapeableImageView;
import net.safemoon.androidwallet.R;

/* compiled from: ItemCheckTokenBinding.java */
/* renamed from: vs1  reason: default package */
/* loaded from: classes2.dex */
public final class vs1 {
    public final ConstraintLayout a;
    public final MaterialCheckBox b;
    public final ImageView c;
    public final MaterialCardView d;
    public final MaterialCardView e;
    public final ShapeableImageView f;
    public final TextView g;

    public vs1(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialCheckBox materialCheckBox, ImageView imageView, MaterialCardView materialCardView, MaterialCardView materialCardView2, ShapeableImageView shapeableImageView, TextView textView) {
        this.a = constraintLayout;
        this.b = materialCheckBox;
        this.c = imageView;
        this.d = materialCardView;
        this.e = materialCardView2;
        this.f = shapeableImageView;
        this.g = textView;
    }

    public static vs1 a(View view) {
        int i = R.id.btnDelete;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnDelete);
        if (materialButton != null) {
            i = R.id.checkableIcon;
            MaterialCheckBox materialCheckBox = (MaterialCheckBox) ai4.a(view, R.id.checkableIcon);
            if (materialCheckBox != null) {
                i = R.id.ivTokenIcon;
                ImageView imageView = (ImageView) ai4.a(view, R.id.ivTokenIcon);
                if (imageView != null) {
                    i = R.id.rowBG;
                    MaterialCardView materialCardView = (MaterialCardView) ai4.a(view, R.id.rowBG);
                    if (materialCardView != null) {
                        i = R.id.rowFG;
                        MaterialCardView materialCardView2 = (MaterialCardView) ai4.a(view, R.id.rowFG);
                        if (materialCardView2 != null) {
                            i = R.id.statusForCheck;
                            ShapeableImageView shapeableImageView = (ShapeableImageView) ai4.a(view, R.id.statusForCheck);
                            if (shapeableImageView != null) {
                                i = R.id.tvTokenNameAndSymbol;
                                TextView textView = (TextView) ai4.a(view, R.id.tvTokenNameAndSymbol);
                                if (textView != null) {
                                    return new vs1((ConstraintLayout) view, materialButton, materialCheckBox, imageView, materialCardView, materialCardView2, shapeableImageView, textView);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static vs1 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(R.layout.item_check_token, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    public ConstraintLayout b() {
        return this.a;
    }
}
