package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.util.b;
import java.io.IOException;

/* compiled from: FlacFrameReader.java */
/* renamed from: o61  reason: default package */
/* loaded from: classes.dex */
public final class o61 {

    /* compiled from: FlacFrameReader.java */
    /* renamed from: o61$a */
    /* loaded from: classes.dex */
    public static final class a {
        public long a;
    }

    public static boolean a(op2 op2Var, s61 s61Var, int i) {
        int j = j(op2Var, i);
        return j != -1 && j <= s61Var.b;
    }

    public static boolean b(op2 op2Var, int i) {
        return op2Var.D() == b.t(op2Var.d(), i, op2Var.e() - 1, 0);
    }

    public static boolean c(op2 op2Var, s61 s61Var, boolean z, a aVar) {
        try {
            long K = op2Var.K();
            if (!z) {
                K *= s61Var.b;
            }
            aVar.a = K;
            return true;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public static boolean d(op2 op2Var, s61 s61Var, int i, a aVar) {
        int e = op2Var.e();
        long F = op2Var.F();
        long j = F >>> 16;
        if (j != i) {
            return false;
        }
        return g((int) ((F >> 4) & 15), s61Var) && f((int) ((F >> 1) & 7), s61Var) && !(((F & 1) > 1L ? 1 : ((F & 1) == 1L ? 0 : -1)) == 0) && c(op2Var, s61Var, ((j & 1) > 1L ? 1 : ((j & 1) == 1L ? 0 : -1)) == 0, aVar) && a(op2Var, s61Var, (int) ((F >> 12) & 15)) && e(op2Var, s61Var, (int) ((F >> 8) & 15)) && b(op2Var, e);
    }

    public static boolean e(op2 op2Var, s61 s61Var, int i) {
        int i2 = s61Var.e;
        if (i == 0) {
            return true;
        }
        if (i <= 11) {
            return i == s61Var.f;
        } else if (i == 12) {
            return op2Var.D() * 1000 == i2;
        } else if (i <= 14) {
            int J = op2Var.J();
            if (i == 14) {
                J *= 10;
            }
            return J == i2;
        } else {
            return false;
        }
    }

    public static boolean f(int i, s61 s61Var) {
        return i == 0 || i == s61Var.i;
    }

    public static boolean g(int i, s61 s61Var) {
        return i <= 7 ? i == s61Var.g - 1 : i <= 10 && s61Var.g == 2;
    }

    public static boolean h(q11 q11Var, s61 s61Var, int i, a aVar) throws IOException {
        long e = q11Var.e();
        byte[] bArr = new byte[2];
        q11Var.n(bArr, 0, 2);
        if ((((bArr[0] & 255) << 8) | (bArr[1] & 255)) != i) {
            q11Var.j();
            q11Var.f((int) (e - q11Var.getPosition()));
            return false;
        }
        op2 op2Var = new op2(16);
        System.arraycopy(bArr, 0, op2Var.d(), 0, 2);
        op2Var.O(s11.c(q11Var, op2Var.d(), 2, 14));
        q11Var.j();
        q11Var.f((int) (e - q11Var.getPosition()));
        return d(op2Var, s61Var, i, aVar);
    }

    public static long i(q11 q11Var, s61 s61Var) throws IOException {
        q11Var.j();
        q11Var.f(1);
        byte[] bArr = new byte[1];
        q11Var.n(bArr, 0, 1);
        boolean z = (bArr[0] & 1) == 1;
        q11Var.f(2);
        int i = z ? 7 : 6;
        op2 op2Var = new op2(i);
        op2Var.O(s11.c(q11Var, op2Var.d(), 0, i));
        q11Var.j();
        a aVar = new a();
        if (c(op2Var, s61Var, z, aVar)) {
            return aVar.a;
        }
        throw ParserException.createForMalformedContainer(null, null);
    }

    public static int j(op2 op2Var, int i) {
        switch (i) {
            case 1:
                return 192;
            case 2:
            case 3:
            case 4:
            case 5:
                return 576 << (i - 2);
            case 6:
                return op2Var.D() + 1;
            case 7:
                return op2Var.J() + 1;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return 256 << (i - 8);
            default:
                return -1;
        }
    }
}
