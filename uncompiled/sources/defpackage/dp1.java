package defpackage;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.widget.ImageView;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: ImageViewHelper.java */
/* renamed from: dp1  reason: default package */
/* loaded from: classes.dex */
public class dp1 {
    public static final RectF a = new RectF();
    public static final RectF b = new RectF();

    /* compiled from: ImageViewHelper.java */
    /* renamed from: dp1$a */
    /* loaded from: classes.dex */
    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        static {
            int[] iArr = new int[ImageView.ScaleType.values().length];
            a = iArr;
            try {
                iArr[ImageView.ScaleType.FIT_XY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                a[ImageView.ScaleType.FIT_START.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                a[ImageView.ScaleType.FIT_END.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public static void a(ImageView.ScaleType scaleType, int i, int i2, int i3, int i4, Matrix matrix, Matrix matrix2) {
        float f;
        float f2;
        if (ImageView.ScaleType.CENTER == scaleType) {
            matrix2.setTranslate((i3 - i) * 0.5f, (i4 - i2) * 0.5f);
            return;
        }
        ImageView.ScaleType scaleType2 = ImageView.ScaleType.CENTER_CROP;
        float f3 = Utils.FLOAT_EPSILON;
        if (scaleType2 == scaleType) {
            if (i * i4 > i3 * i2) {
                f = i4 / i2;
                f2 = 0.0f;
                f3 = (i3 - (i * f)) * 0.5f;
            } else {
                f = i3 / i;
                f2 = (i4 - (i2 * f)) * 0.5f;
            }
            matrix2.setScale(f, f);
            matrix2.postTranslate(f3, f2);
        } else if (ImageView.ScaleType.CENTER_INSIDE == scaleType) {
            float min = (i > i3 || i2 > i4) ? Math.min(i3 / i, i4 / i2) : 1.0f;
            matrix2.setScale(min, min);
            matrix2.postTranslate((i3 - (i * min)) * 0.5f, (i4 - (i2 * min)) * 0.5f);
        } else {
            Matrix.ScaleToFit b2 = b(scaleType);
            if (b2 == null) {
                matrix2.set(matrix);
                return;
            }
            RectF rectF = a;
            rectF.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, i, i2);
            RectF rectF2 = b;
            rectF2.set(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, i3, i4);
            matrix2.setRectToRect(rectF, rectF2, b2);
        }
    }

    public static Matrix.ScaleToFit b(ImageView.ScaleType scaleType) {
        int i = a.a[scaleType.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        return null;
                    }
                    return Matrix.ScaleToFit.END;
                }
                return Matrix.ScaleToFit.CENTER;
            }
            return Matrix.ScaleToFit.START;
        }
        return Matrix.ScaleToFit.FILL;
    }
}
