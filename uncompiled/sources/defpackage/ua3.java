package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;

/* renamed from: ua3  reason: default package */
/* loaded from: classes2.dex */
public class ua3 extends h {
    public final g a;
    public final va f0;

    public ua3(h4 h4Var) {
        this.a = g.z(h4Var.D(0));
        this.f0 = va.p(h4Var.D(1));
    }

    public ua3(va vaVar) {
        this.a = new g(0L);
        this.f0 = vaVar;
    }

    public static final ua3 o(Object obj) {
        if (obj instanceof ua3) {
            return (ua3) obj;
        }
        if (obj != null) {
            return new ua3(h4.z(obj));
        }
        return null;
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(this.a);
        d4Var.a(this.f0);
        return new n0(d4Var);
    }

    public va p() {
        return this.f0;
    }
}
