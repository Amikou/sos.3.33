package defpackage;

import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import androidx.media3.common.util.b;
import androidx.media3.extractor.text.SubtitleDecoderException;
import defpackage.kb0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import zendesk.support.request.CellBase;

/* compiled from: Cea608Decoder.java */
/* renamed from: ow  reason: default package */
/* loaded from: classes.dex */
public final class ow extends sw {
    public final int h;
    public final int i;
    public final int j;
    public final long k;
    public List<kb0> n;
    public List<kb0> o;
    public int p;
    public int q;
    public boolean r;
    public boolean s;
    public byte t;
    public byte u;
    public boolean w;
    public long x;
    public static final int[] y = {11, 1, 3, 12, 14, 5, 7, 9};
    public static final int[] z = {0, 4, 8, 12, 16, 20, 24, 28};
    public static final int[] A = {-1, -16711936, -16776961, -16711681, -65536, -256, -65281};
    public static final int[] B = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 225, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 233, 93, 237, 243, 250, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 231, 247, 209, 241, 9632};
    public static final int[] C = {174, 176, 189, 191, 8482, 162, 163, 9834, 224, 32, 232, 226, 234, 238, 244, 251};
    public static final int[] D = {193, 201, 211, 218, 220, 252, 8216, 161, 42, 39, 8212, 169, 8480, 8226, 8220, 8221, 192, 194, 199, 200, 202, 203, 235, 206, 207, 239, 212, 217, 249, 219, 171, 187};
    public static final int[] E = {195, 227, 205, 204, 236, 210, 242, 213, 245, 123, 125, 92, 94, 95, 124, 126, 196, 228, 214, 246, 223, 165, 164, 9474, 197, 229, 216, 248, 9484, 9488, 9492, 9496};
    public static final boolean[] F = {false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false, false, true, true, false, true, false, false, true, false, true, true, false, true, false, false, true, true, false, false, true, false, true, true, false};
    public final op2 g = new op2();
    public final ArrayList<a> l = new ArrayList<>();
    public a m = new a(0, 4);
    public int v = 0;

    /* compiled from: Cea608Decoder.java */
    /* renamed from: ow$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final List<C0259a> a = new ArrayList();
        public final List<SpannableString> b = new ArrayList();
        public final StringBuilder c = new StringBuilder();
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;

        /* compiled from: Cea608Decoder.java */
        /* renamed from: ow$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0259a {
            public final int a;
            public final boolean b;
            public int c;

            public C0259a(int i, boolean z, int i2) {
                this.a = i;
                this.b = z;
                this.c = i2;
            }
        }

        public a(int i, int i2) {
            j(i);
            this.h = i2;
        }

        public static void n(SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3) {
            if (i3 == -1) {
                return;
            }
            spannableStringBuilder.setSpan(new ForegroundColorSpan(i3), i, i2, 33);
        }

        public static void o(SpannableStringBuilder spannableStringBuilder, int i, int i2) {
            spannableStringBuilder.setSpan(new StyleSpan(2), i, i2, 33);
        }

        public static void q(SpannableStringBuilder spannableStringBuilder, int i, int i2) {
            spannableStringBuilder.setSpan(new UnderlineSpan(), i, i2, 33);
        }

        public void e(char c) {
            if (this.c.length() < 32) {
                this.c.append(c);
            }
        }

        public void f() {
            int length = this.c.length();
            if (length > 0) {
                this.c.delete(length - 1, length);
                for (int size = this.a.size() - 1; size >= 0; size--) {
                    C0259a c0259a = this.a.get(size);
                    int i = c0259a.c;
                    if (i != length) {
                        return;
                    }
                    c0259a.c = i - 1;
                }
            }
        }

        public kb0 g(int i) {
            float f;
            int i2 = this.e + this.f;
            int i3 = 32 - i2;
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            for (int i4 = 0; i4 < this.b.size(); i4++) {
                spannableStringBuilder.append(b.T0(this.b.get(i4), i3));
                spannableStringBuilder.append('\n');
            }
            spannableStringBuilder.append(b.T0(h(), i3));
            if (spannableStringBuilder.length() == 0) {
                return null;
            }
            int length = i3 - spannableStringBuilder.length();
            int i5 = i2 - length;
            if (i == Integer.MIN_VALUE) {
                if (this.g != 2 || (Math.abs(i5) >= 3 && length >= 0)) {
                    i = (this.g != 2 || i5 <= 0) ? 0 : 2;
                } else {
                    i = 1;
                }
            }
            if (i != 1) {
                if (i == 2) {
                    i2 = 32 - length;
                }
                f = ((i2 / 32.0f) * 0.8f) + 0.1f;
            } else {
                f = 0.5f;
            }
            int i6 = this.d;
            if (i6 > 7) {
                i6 = (i6 - 15) - 2;
            } else if (this.g == 1) {
                i6 -= this.h - 1;
            }
            return new kb0.b().o(spannableStringBuilder).p(Layout.Alignment.ALIGN_NORMAL).h(i6, 1).k(f).l(i).a();
        }

        public final SpannableString h() {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.c);
            int length = spannableStringBuilder.length();
            int i = 0;
            int i2 = 0;
            boolean z = false;
            int i3 = -1;
            int i4 = -1;
            int i5 = -1;
            int i6 = -1;
            while (i < this.a.size()) {
                C0259a c0259a = this.a.get(i);
                boolean z2 = c0259a.b;
                int i7 = c0259a.a;
                if (i7 != 8) {
                    boolean z3 = i7 == 7;
                    if (i7 != 7) {
                        i6 = ow.A[i7];
                    }
                    z = z3;
                }
                int i8 = c0259a.c;
                i++;
                if (i8 != (i < this.a.size() ? this.a.get(i).c : length)) {
                    if (i3 != -1 && !z2) {
                        q(spannableStringBuilder, i3, i8);
                        i3 = -1;
                    } else if (i3 == -1 && z2) {
                        i3 = i8;
                    }
                    if (i4 != -1 && !z) {
                        o(spannableStringBuilder, i4, i8);
                        i4 = -1;
                    } else if (i4 == -1 && z) {
                        i4 = i8;
                    }
                    if (i6 != i5) {
                        n(spannableStringBuilder, i2, i8, i5);
                        i5 = i6;
                        i2 = i8;
                    }
                }
            }
            if (i3 != -1 && i3 != length) {
                q(spannableStringBuilder, i3, length);
            }
            if (i4 != -1 && i4 != length) {
                o(spannableStringBuilder, i4, length);
            }
            if (i2 != length) {
                n(spannableStringBuilder, i2, length, i5);
            }
            return new SpannableString(spannableStringBuilder);
        }

        public boolean i() {
            return this.a.isEmpty() && this.b.isEmpty() && this.c.length() == 0;
        }

        public void j(int i) {
            this.g = i;
            this.a.clear();
            this.b.clear();
            this.c.setLength(0);
            this.d = 15;
            this.e = 0;
            this.f = 0;
        }

        public void k() {
            this.b.add(h());
            this.c.setLength(0);
            this.a.clear();
            int min = Math.min(this.h, this.d);
            while (this.b.size() >= min) {
                this.b.remove(0);
            }
        }

        public void l(int i) {
            this.g = i;
        }

        public void m(int i) {
            this.h = i;
        }

        public void p(int i, boolean z) {
            this.a.add(new C0259a(i, z, this.c.length()));
        }
    }

    public ow(String str, int i, long j) {
        this.k = j > 0 ? j * 1000 : -9223372036854775807L;
        this.h = "application/x-mp4-cea-608".equals(str) ? 2 : 3;
        if (i == 1) {
            this.j = 0;
            this.i = 0;
        } else if (i == 2) {
            this.j = 1;
            this.i = 0;
        } else if (i == 3) {
            this.j = 0;
            this.i = 1;
        } else if (i != 4) {
            p12.i("Cea608Decoder", "Invalid channel. Defaulting to CC1.");
            this.j = 0;
            this.i = 0;
        } else {
            this.j = 1;
            this.i = 1;
        }
        N(0);
        M();
        this.w = true;
        this.x = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    public static boolean A(byte b) {
        return (b & 224) == 0;
    }

    public static boolean B(byte b, byte b2) {
        return (b & 246) == 18 && (b2 & 224) == 32;
    }

    public static boolean C(byte b, byte b2) {
        return (b & 247) == 17 && (b2 & 240) == 32;
    }

    public static boolean D(byte b, byte b2) {
        return (b & 246) == 20 && (b2 & 240) == 32;
    }

    public static boolean E(byte b, byte b2) {
        return (b & 240) == 16 && (b2 & 192) == 64;
    }

    public static boolean F(byte b) {
        return (b & 240) == 16;
    }

    public static boolean H(byte b) {
        return (b & 247) == 20;
    }

    public static boolean I(byte b, byte b2) {
        return (b & 247) == 17 && (b2 & 240) == 48;
    }

    public static boolean J(byte b, byte b2) {
        return (b & 247) == 23 && b2 >= 33 && b2 <= 35;
    }

    public static boolean K(byte b) {
        return 1 <= b && b <= 15;
    }

    public static char q(byte b) {
        return (char) B[(b & Byte.MAX_VALUE) - 32];
    }

    public static int r(byte b) {
        return (b >> 3) & 1;
    }

    public static char t(byte b) {
        return (char) D[b & 31];
    }

    public static char u(byte b) {
        return (char) E[b & 31];
    }

    public static char v(byte b, byte b2) {
        if ((b & 1) == 0) {
            return t(b2);
        }
        return u(b2);
    }

    public static char w(byte b) {
        return (char) C[b & 15];
    }

    public final boolean G(boolean z2, byte b, byte b2) {
        if (z2 && F(b)) {
            if (this.s && this.t == b && this.u == b2) {
                this.s = false;
                return true;
            }
            this.s = true;
            this.t = b;
            this.u = b2;
        } else {
            this.s = false;
        }
        return false;
    }

    public final void L(byte b, byte b2) {
        if (K(b)) {
            this.w = false;
        } else if (H(b)) {
            if (b2 != 32 && b2 != 47) {
                switch (b2) {
                    case 37:
                    case 38:
                    case 39:
                        break;
                    default:
                        switch (b2) {
                            case 41:
                                break;
                            case 42:
                            case 43:
                                this.w = false;
                                return;
                            default:
                                return;
                        }
                }
            }
            this.w = true;
        }
    }

    public final void M() {
        this.m.j(this.p);
        this.l.clear();
        this.l.add(this.m);
    }

    public final void N(int i) {
        int i2 = this.p;
        if (i2 == i) {
            return;
        }
        this.p = i;
        if (i == 3) {
            for (int i3 = 0; i3 < this.l.size(); i3++) {
                this.l.get(i3).l(i);
            }
            return;
        }
        M();
        if (i2 == 3 || i == 1 || i == 0) {
            this.n = Collections.emptyList();
        }
    }

    public final void O(int i) {
        this.q = i;
        this.m.m(i);
    }

    public final boolean P() {
        return (this.k == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || this.x == CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED || k() - this.x < this.k) ? false : true;
    }

    public final boolean Q(byte b) {
        if (A(b)) {
            this.v = r(b);
        }
        return this.v == this.j;
    }

    @Override // defpackage.sw, androidx.media3.decoder.a
    public void a() {
    }

    @Override // defpackage.sw
    public qv3 f() {
        List<kb0> list = this.n;
        this.o = list;
        return new tw((List) ii.e(list));
    }

    @Override // defpackage.sw, androidx.media3.decoder.a
    public void flush() {
        super.flush();
        this.n = null;
        this.o = null;
        N(0);
        O(4);
        M();
        this.r = false;
        this.s = false;
        this.t = (byte) 0;
        this.u = (byte) 0;
        this.v = 0;
        this.w = true;
        this.x = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
    }

    /* JADX WARN: Removed duplicated region for block: B:76:0x006e A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:86:0x0018 A[SYNTHETIC] */
    @Override // defpackage.sw
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void g(defpackage.tv3 r10) {
        /*
            Method dump skipped, instructions count: 268
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ow.g(tv3):void");
    }

    @Override // defpackage.sw, androidx.media3.decoder.a
    /* renamed from: i */
    public uv3 c() throws SubtitleDecoderException {
        uv3 j;
        uv3 c = super.c();
        if (c != null) {
            return c;
        }
        if (!P() || (j = j()) == null) {
            return null;
        }
        this.n = Collections.emptyList();
        this.x = CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED;
        j.v(k(), f(), Long.MAX_VALUE);
        return j;
    }

    @Override // defpackage.sw
    public boolean l() {
        return this.n != this.o;
    }

    public final List<kb0> s() {
        int size = this.l.size();
        ArrayList arrayList = new ArrayList(size);
        int i = 2;
        for (int i2 = 0; i2 < size; i2++) {
            kb0 g = this.l.get(i2).g(Integer.MIN_VALUE);
            arrayList.add(g);
            if (g != null) {
                i = Math.min(i, g.m0);
            }
        }
        ArrayList arrayList2 = new ArrayList(size);
        for (int i3 = 0; i3 < size; i3++) {
            kb0 kb0Var = (kb0) arrayList.get(i3);
            if (kb0Var != null) {
                if (kb0Var.m0 != i) {
                    kb0Var = (kb0) ii.e(this.l.get(i3).g(i));
                }
                arrayList2.add(kb0Var);
            }
        }
        return arrayList2;
    }

    public final void x(byte b) {
        this.m.e(' ');
        this.m.p((b >> 1) & 7, (b & 1) == 1);
    }

    public final void y(byte b) {
        if (b == 32) {
            N(2);
        } else if (b != 41) {
            switch (b) {
                case 37:
                    N(1);
                    O(2);
                    return;
                case 38:
                    N(1);
                    O(3);
                    return;
                case 39:
                    N(1);
                    O(4);
                    return;
                default:
                    int i = this.p;
                    if (i == 0) {
                        return;
                    }
                    if (b != 33) {
                        switch (b) {
                            case 44:
                                this.n = Collections.emptyList();
                                int i2 = this.p;
                                if (i2 == 1 || i2 == 3) {
                                    M();
                                    return;
                                }
                                return;
                            case 45:
                                if (i != 1 || this.m.i()) {
                                    return;
                                }
                                this.m.k();
                                return;
                            case 46:
                                M();
                                return;
                            case 47:
                                this.n = s();
                                M();
                                return;
                            default:
                                return;
                        }
                    }
                    this.m.f();
                    return;
            }
        } else {
            N(3);
        }
    }

    public final void z(byte b, byte b2) {
        int i = y[b & 7];
        if ((b2 & 32) != 0) {
            i++;
        }
        if (i != this.m.d) {
            if (this.p != 1 && !this.m.i()) {
                a aVar = new a(this.p, this.q);
                this.m = aVar;
                this.l.add(aVar);
            }
            this.m.d = i;
        }
        boolean z2 = (b2 & 16) == 16;
        boolean z3 = (b2 & 1) == 1;
        int i2 = (b2 >> 1) & 7;
        this.m.p(z2 ? 8 : i2, z3);
        if (z2) {
            this.m.e = z[i2];
        }
    }
}
