package defpackage;

import defpackage.gc4;
import defpackage.wi3;
import java.io.IOException;
import okhttp3.internal.http2.Http2;
import zendesk.support.request.CellBase;

/* compiled from: Ac4Extractor.java */
/* renamed from: v5  reason: default package */
/* loaded from: classes.dex */
public final class v5 implements p11 {
    public final w5 a = new w5();
    public final op2 b = new op2((int) Http2.INITIAL_MAX_FRAME_SIZE);
    public boolean c;

    static {
        u5 u5Var = u5.b;
    }

    public static /* synthetic */ p11[] d() {
        return new p11[]{new v5()};
    }

    @Override // defpackage.p11
    public void a() {
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        this.c = false;
        this.a.c();
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        int read = q11Var.read(this.b.d(), 0, Http2.INITIAL_MAX_FRAME_SIZE);
        if (read == -1) {
            return -1;
        }
        this.b.P(0);
        this.b.O(read);
        if (!this.c) {
            this.a.e(0L, 4);
            this.c = true;
        }
        this.a.a(this.b);
        return 0;
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        op2 op2Var = new op2(10);
        int i = 0;
        while (true) {
            q11Var.n(op2Var.d(), 0, 10);
            op2Var.P(0);
            if (op2Var.G() != 4801587) {
                break;
            }
            op2Var.Q(3);
            int C = op2Var.C();
            i += C + 10;
            q11Var.f(C);
        }
        q11Var.j();
        q11Var.f(i);
        int i2 = 0;
        int i3 = i;
        while (true) {
            q11Var.n(op2Var.d(), 0, 7);
            op2Var.P(0);
            int J = op2Var.J();
            if (J == 44096 || J == 44097) {
                i2++;
                if (i2 >= 4) {
                    return true;
                }
                int e = x5.e(op2Var.d(), J);
                if (e == -1) {
                    return false;
                }
                q11Var.f(e - 7);
            } else {
                q11Var.j();
                i3++;
                if (i3 - i >= 8192) {
                    return false;
                }
                q11Var.f(i3);
                i2 = 0;
            }
        }
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.a.f(r11Var, new gc4.d(0, 1));
        r11Var.m();
        r11Var.p(new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
    }
}
