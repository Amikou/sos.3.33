package defpackage;

import android.database.ContentObserver;
import android.os.Handler;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: om5  reason: default package */
/* loaded from: classes.dex */
public final class om5 extends ContentObserver {
    public final /* synthetic */ bn5 a;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public om5(bn5 bn5Var, Handler handler) {
        super(null);
        this.a = bn5Var;
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        this.a.d();
    }
}
