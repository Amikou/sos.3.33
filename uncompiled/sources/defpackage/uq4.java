package defpackage;

import android.annotation.SuppressLint;
import androidx.work.WorkInfo;
import androidx.work.b;
import defpackage.tq4;
import java.util.List;

/* compiled from: WorkSpecDao.java */
@SuppressLint({"UnknownNullness"})
/* renamed from: uq4  reason: default package */
/* loaded from: classes.dex */
public interface uq4 {
    int a(WorkInfo.State state, String... strArr);

    int b(String str, long j);

    List<tq4.b> c(String str);

    List<tq4> d(long j);

    void delete(String str);

    List<tq4> e(int i);

    List<tq4> f();

    void g(String str, b bVar);

    List<tq4> h();

    boolean i();

    List<String> j(String str);

    WorkInfo.State k(String str);

    tq4 l(String str);

    int m(String str);

    void n(tq4 tq4Var);

    List<String> o(String str);

    List<b> p(String str);

    int q(String str);

    void r(String str, long j);

    List<tq4> s(int i);

    int t();
}
