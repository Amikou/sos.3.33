package defpackage;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ConstraintTracker.java */
/* renamed from: g60  reason: default package */
/* loaded from: classes.dex */
public abstract class g60<T> {
    public static final String f = v12.f("ConstraintTracker");
    public final q34 a;
    public final Context b;
    public final Object c = new Object();
    public final Set<f60<T>> d = new LinkedHashSet();
    public T e;

    /* compiled from: ConstraintTracker.java */
    /* renamed from: g60$a */
    /* loaded from: classes.dex */
    public class a implements Runnable {
        public final /* synthetic */ List a;

        public a(List list) {
            this.a = list;
        }

        @Override // java.lang.Runnable
        public void run() {
            for (f60 f60Var : this.a) {
                f60Var.a(g60.this.e);
            }
        }
    }

    public g60(Context context, q34 q34Var) {
        this.b = context.getApplicationContext();
        this.a = q34Var;
    }

    public void a(f60<T> f60Var) {
        synchronized (this.c) {
            if (this.d.add(f60Var)) {
                if (this.d.size() == 1) {
                    this.e = b();
                    v12.c().a(f, String.format("%s: initial state = %s", getClass().getSimpleName(), this.e), new Throwable[0]);
                    e();
                }
                f60Var.a(this.e);
            }
        }
    }

    public abstract T b();

    public void c(f60<T> f60Var) {
        synchronized (this.c) {
            if (this.d.remove(f60Var) && this.d.isEmpty()) {
                f();
            }
        }
    }

    public void d(T t) {
        synchronized (this.c) {
            T t2 = this.e;
            if (t2 != t && (t2 == null || !t2.equals(t))) {
                this.e = t;
                this.a.a().execute(new a(new ArrayList(this.d)));
            }
        }
    }

    public abstract void e();

    public abstract void f();
}
