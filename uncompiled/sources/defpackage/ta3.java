package defpackage;

import defpackage.pt0;

/* renamed from: ta3  reason: default package */
/* loaded from: classes2.dex */
public class ta3 extends pt0.c {
    public ta3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public ta3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public ta3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        sa3 sa3Var = (sa3) this.c;
        if (sa3Var.i()) {
            return i.v();
        }
        sa3 sa3Var2 = (sa3) this.b;
        sa3 sa3Var3 = (sa3) this.d[0];
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        int[] h3 = ed2.h();
        ra3.j(sa3Var.f, h3);
        int[] h4 = ed2.h();
        ra3.j(h3, h4);
        boolean h5 = sa3Var3.h();
        int[] iArr = sa3Var3.f;
        if (!h5) {
            ra3.j(iArr, h2);
            iArr = h2;
        }
        ra3.m(sa3Var2.f, iArr, h);
        ra3.a(sa3Var2.f, iArr, h2);
        ra3.e(h2, h, h2);
        ra3.i(ed2.b(h2, h2, h2), h2);
        ra3.e(h3, sa3Var2.f, h3);
        ra3.i(kd2.G(8, h3, 2, 0), h3);
        ra3.i(kd2.H(8, h4, 3, 0, h), h);
        sa3 sa3Var4 = new sa3(h4);
        ra3.j(h2, sa3Var4.f);
        int[] iArr2 = sa3Var4.f;
        ra3.m(iArr2, h3, iArr2);
        int[] iArr3 = sa3Var4.f;
        ra3.m(iArr3, h3, iArr3);
        sa3 sa3Var5 = new sa3(h3);
        ra3.m(h3, sa3Var4.f, sa3Var5.f);
        int[] iArr4 = sa3Var5.f;
        ra3.e(iArr4, h2, iArr4);
        int[] iArr5 = sa3Var5.f;
        ra3.m(iArr5, h, iArr5);
        sa3 sa3Var6 = new sa3(h2);
        ra3.n(sa3Var.f, sa3Var6.f);
        if (!h5) {
            int[] iArr6 = sa3Var6.f;
            ra3.e(iArr6, sa3Var3.f, iArr6);
        }
        return new ta3(i, sa3Var4, sa3Var5, new ct0[]{sa3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        sa3 sa3Var = (sa3) this.b;
        sa3 sa3Var2 = (sa3) this.c;
        sa3 sa3Var3 = (sa3) pt0Var.q();
        sa3 sa3Var4 = (sa3) pt0Var.r();
        sa3 sa3Var5 = (sa3) this.d[0];
        sa3 sa3Var6 = (sa3) pt0Var.s(0);
        int[] j = ed2.j();
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        int[] h3 = ed2.h();
        boolean h4 = sa3Var5.h();
        if (h4) {
            iArr = sa3Var3.f;
            iArr2 = sa3Var4.f;
        } else {
            ra3.j(sa3Var5.f, h2);
            ra3.e(h2, sa3Var3.f, h);
            ra3.e(h2, sa3Var5.f, h2);
            ra3.e(h2, sa3Var4.f, h2);
            iArr = h;
            iArr2 = h2;
        }
        boolean h5 = sa3Var6.h();
        if (h5) {
            iArr3 = sa3Var.f;
            iArr4 = sa3Var2.f;
        } else {
            ra3.j(sa3Var6.f, h3);
            ra3.e(h3, sa3Var.f, j);
            ra3.e(h3, sa3Var6.f, h3);
            ra3.e(h3, sa3Var2.f, h3);
            iArr3 = j;
            iArr4 = h3;
        }
        int[] h6 = ed2.h();
        ra3.m(iArr3, iArr, h6);
        ra3.m(iArr4, iArr2, h);
        if (ed2.v(h6)) {
            return ed2.v(h) ? J() : i.v();
        }
        ra3.j(h6, h2);
        int[] h7 = ed2.h();
        ra3.e(h2, h6, h7);
        ra3.e(h2, iArr3, h2);
        ra3.g(h7, h7);
        ed2.y(iArr4, h7, j);
        ra3.i(ed2.b(h2, h2, h7), h7);
        sa3 sa3Var7 = new sa3(h3);
        ra3.j(h, sa3Var7.f);
        int[] iArr5 = sa3Var7.f;
        ra3.m(iArr5, h7, iArr5);
        sa3 sa3Var8 = new sa3(h7);
        ra3.m(h2, sa3Var7.f, sa3Var8.f);
        ra3.f(sa3Var8.f, h, j);
        ra3.h(j, sa3Var8.f);
        sa3 sa3Var9 = new sa3(h6);
        if (!h4) {
            int[] iArr6 = sa3Var9.f;
            ra3.e(iArr6, sa3Var5.f, iArr6);
        }
        if (!h5) {
            int[] iArr7 = sa3Var9.f;
            ra3.e(iArr7, sa3Var6.f, iArr7);
        }
        return new ta3(i, sa3Var7, sa3Var8, new ct0[]{sa3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new ta3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new ta3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
