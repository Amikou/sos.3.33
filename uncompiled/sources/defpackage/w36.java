package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: w36  reason: default package */
/* loaded from: classes.dex */
public final class w36 implements yp5<x36> {
    public static final w36 f0 = new w36();
    public final yp5<x36> a = gq5.a(gq5.b(new y36()));

    public static boolean a() {
        f0.zza().zza();
        return true;
    }

    public static boolean b() {
        return f0.zza().zzb();
    }

    @Override // defpackage.yp5
    /* renamed from: c */
    public final x36 zza() {
        return this.a.zza();
    }
}
