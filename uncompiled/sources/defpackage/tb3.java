package defpackage;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.CoroutineSingletons;

/* compiled from: SafeContinuationJvm.kt */
/* renamed from: tb3  reason: default package */
/* loaded from: classes2.dex */
public final class tb3<T> implements q70<T>, e90 {
    @Deprecated
    public static final AtomicReferenceFieldUpdater<tb3<?>, Object> f0;
    public final q70<T> a;
    private volatile Object result;

    /* compiled from: SafeContinuationJvm.kt */
    /* renamed from: tb3$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public a() {
        }

        public /* synthetic */ a(qi0 qi0Var) {
            this();
        }
    }

    static {
        new a(null);
        f0 = AtomicReferenceFieldUpdater.newUpdater(tb3.class, Object.class, "result");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public tb3(q70<? super T> q70Var, Object obj) {
        fs1.f(q70Var, "delegate");
        this.a = q70Var;
        this.result = obj;
    }

    public final Object a() {
        Object obj = this.result;
        CoroutineSingletons coroutineSingletons = CoroutineSingletons.UNDECIDED;
        if (obj == coroutineSingletons) {
            if (f0.compareAndSet(this, coroutineSingletons, gs1.d())) {
                return gs1.d();
            }
            obj = this.result;
        }
        if (obj == CoroutineSingletons.RESUMED) {
            return gs1.d();
        }
        if (obj instanceof Result.Failure) {
            throw ((Result.Failure) obj).exception;
        }
        return obj;
    }

    @Override // defpackage.e90
    public e90 getCallerFrame() {
        q70<T> q70Var = this.a;
        if (!(q70Var instanceof e90)) {
            q70Var = null;
        }
        return (e90) q70Var;
    }

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        return this.a.getContext();
    }

    @Override // defpackage.e90
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.result;
            CoroutineSingletons coroutineSingletons = CoroutineSingletons.UNDECIDED;
            if (obj2 == coroutineSingletons) {
                if (f0.compareAndSet(this, coroutineSingletons, obj)) {
                    return;
                }
            } else if (obj2 != gs1.d()) {
                throw new IllegalStateException("Already resumed");
            } else {
                if (f0.compareAndSet(this, gs1.d(), CoroutineSingletons.RESUMED)) {
                    this.a.resumeWith(obj);
                    return;
                }
            }
        }
    }

    public String toString() {
        return "SafeContinuation for " + this.a;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public tb3(q70<? super T> q70Var) {
        this(q70Var, CoroutineSingletons.UNDECIDED);
        fs1.f(q70Var, "delegate");
    }
}
