package defpackage;

import androidx.media3.common.ParserException;
import androidx.media3.common.j;
import com.google.common.collect.ImmutableList;
import defpackage.dl4;
import defpackage.fu3;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/* compiled from: VorbisReader.java */
/* renamed from: cl4  reason: default package */
/* loaded from: classes.dex */
public final class cl4 extends fu3 {
    public a n;
    public int o;
    public boolean p;
    public dl4.d q;
    public dl4.b r;

    /* compiled from: VorbisReader.java */
    /* renamed from: cl4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final dl4.d a;
        public final dl4.b b;
        public final byte[] c;
        public final dl4.c[] d;
        public final int e;

        public a(dl4.d dVar, dl4.b bVar, byte[] bArr, dl4.c[] cVarArr, int i) {
            this.a = dVar;
            this.b = bVar;
            this.c = bArr;
            this.d = cVarArr;
            this.e = i;
        }
    }

    public static void n(op2 op2Var, long j) {
        if (op2Var.b() < op2Var.f() + 4) {
            op2Var.M(Arrays.copyOf(op2Var.d(), op2Var.f() + 4));
        } else {
            op2Var.O(op2Var.f() + 4);
        }
        byte[] d = op2Var.d();
        d[op2Var.f() - 4] = (byte) (j & 255);
        d[op2Var.f() - 3] = (byte) ((j >>> 8) & 255);
        d[op2Var.f() - 2] = (byte) ((j >>> 16) & 255);
        d[op2Var.f() - 1] = (byte) ((j >>> 24) & 255);
    }

    public static int o(byte b, a aVar) {
        if (!aVar.d[p(b, aVar.e, 1)].a) {
            return aVar.a.e;
        }
        return aVar.a.f;
    }

    public static int p(byte b, int i, int i2) {
        return (b >> i2) & (255 >>> (8 - i));
    }

    public static boolean r(op2 op2Var) {
        try {
            return dl4.m(1, op2Var, true);
        } catch (ParserException unused) {
            return false;
        }
    }

    @Override // defpackage.fu3
    public void e(long j) {
        super.e(j);
        this.p = j != 0;
        dl4.d dVar = this.q;
        this.o = dVar != null ? dVar.e : 0;
    }

    @Override // defpackage.fu3
    public long f(op2 op2Var) {
        if ((op2Var.d()[0] & 1) == 1) {
            return -1L;
        }
        int o = o(op2Var.d()[0], (a) ii.i(this.n));
        long j = this.p ? (this.o + o) / 4 : 0;
        n(op2Var, j);
        this.p = true;
        this.o = o;
        return j;
    }

    @Override // defpackage.fu3
    public boolean i(op2 op2Var, long j, fu3.b bVar) throws IOException {
        if (this.n != null) {
            ii.e(bVar.a);
            return false;
        }
        a q = q(op2Var);
        this.n = q;
        if (q == null) {
            return true;
        }
        dl4.d dVar = q.a;
        ArrayList arrayList = new ArrayList();
        arrayList.add(dVar.g);
        arrayList.add(q.c);
        bVar.a = new j.b().e0("audio/vorbis").G(dVar.d).Z(dVar.c).H(dVar.a).f0(dVar.b).T(arrayList).X(dl4.c(ImmutableList.copyOf(q.b.a))).E();
        return true;
    }

    @Override // defpackage.fu3
    public void l(boolean z) {
        super.l(z);
        if (z) {
            this.n = null;
            this.q = null;
            this.r = null;
        }
        this.o = 0;
        this.p = false;
    }

    public a q(op2 op2Var) throws IOException {
        dl4.d dVar = this.q;
        if (dVar == null) {
            this.q = dl4.k(op2Var);
            return null;
        }
        dl4.b bVar = this.r;
        if (bVar == null) {
            this.r = dl4.i(op2Var);
            return null;
        }
        byte[] bArr = new byte[op2Var.f()];
        System.arraycopy(op2Var.d(), 0, bArr, 0, op2Var.f());
        dl4.c[] l = dl4.l(op2Var, dVar.a);
        return new a(dVar, bVar, bArr, l, dl4.a(l.length - 1));
    }
}
