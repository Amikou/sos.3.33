package defpackage;

import com.google.crypto.tink.KeyTemplate;
import com.google.crypto.tink.g;
import com.google.crypto.tink.proto.KeyData;
import com.google.crypto.tink.proto.l;
import com.google.crypto.tink.proto.m;
import com.google.crypto.tink.q;
import com.google.crypto.tink.shaded.protobuf.ByteString;
import com.google.crypto.tink.shaded.protobuf.InvalidProtocolBufferException;
import com.google.crypto.tink.shaded.protobuf.n;
import java.security.GeneralSecurityException;

/* compiled from: AesGcmKeyManager.java */
/* renamed from: la  reason: default package */
/* loaded from: classes2.dex */
public final class la extends g<l> {

    /* compiled from: AesGcmKeyManager.java */
    /* renamed from: la$a */
    /* loaded from: classes2.dex */
    public class a extends g.b<com.google.crypto.tink.a, l> {
        public a(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.b
        /* renamed from: c */
        public com.google.crypto.tink.a a(l lVar) throws GeneralSecurityException {
            return new ka(lVar.G().toByteArray());
        }
    }

    /* compiled from: AesGcmKeyManager.java */
    /* renamed from: la$b */
    /* loaded from: classes2.dex */
    public class b extends g.a<m, l> {
        public b(Class cls) {
            super(cls);
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: e */
        public l a(m mVar) throws GeneralSecurityException {
            return l.I().s(ByteString.copyFrom(p33.c(mVar.E()))).t(la.this.l()).build();
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: f */
        public m c(ByteString byteString) throws InvalidProtocolBufferException {
            return m.H(byteString, n.b());
        }

        @Override // com.google.crypto.tink.g.a
        /* renamed from: g */
        public void d(m mVar) throws GeneralSecurityException {
            ug4.a(mVar.E());
        }
    }

    public la() {
        super(l.class, new a(com.google.crypto.tink.a.class));
    }

    public static final KeyTemplate j() {
        return k(32, KeyTemplate.OutputPrefixType.TINK);
    }

    public static KeyTemplate k(int i, KeyTemplate.OutputPrefixType outputPrefixType) {
        return KeyTemplate.a(new la().c(), m.G().s(i).build().toByteArray(), outputPrefixType);
    }

    public static void n(boolean z) throws GeneralSecurityException {
        q.q(new la(), z);
    }

    @Override // com.google.crypto.tink.g
    public String c() {
        return "type.googleapis.com/google.crypto.tink.AesGcmKey";
    }

    @Override // com.google.crypto.tink.g
    public g.a<?, l> e() {
        return new b(m.class);
    }

    @Override // com.google.crypto.tink.g
    public KeyData.KeyMaterialType f() {
        return KeyData.KeyMaterialType.SYMMETRIC;
    }

    public int l() {
        return 0;
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: m */
    public l g(ByteString byteString) throws InvalidProtocolBufferException {
        return l.J(byteString, n.b());
    }

    @Override // com.google.crypto.tink.g
    /* renamed from: o */
    public void i(l lVar) throws GeneralSecurityException {
        ug4.c(lVar.H(), l());
        ug4.a(lVar.G().size());
    }
}
