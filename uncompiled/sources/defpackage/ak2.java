package defpackage;

import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: OSNotificationIntentExtras.kt */
/* renamed from: ak2  reason: default package */
/* loaded from: classes2.dex */
public final class ak2 {
    public JSONArray a;
    public JSONObject b;

    public ak2(JSONArray jSONArray, JSONObject jSONObject) {
        this.a = jSONArray;
        this.b = jSONObject;
    }

    public final JSONArray a() {
        return this.a;
    }

    public final JSONObject b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ak2) {
                ak2 ak2Var = (ak2) obj;
                return fs1.b(this.a, ak2Var.a) && fs1.b(this.b, ak2Var.b);
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        JSONArray jSONArray = this.a;
        int hashCode = (jSONArray != null ? jSONArray.hashCode() : 0) * 31;
        JSONObject jSONObject = this.b;
        return hashCode + (jSONObject != null ? jSONObject.hashCode() : 0);
    }

    public String toString() {
        return "OSNotificationIntentExtras(dataArray=" + this.a + ", jsonData=" + this.b + ")";
    }
}
