package defpackage;

import android.os.IBinder;
import android.os.IInterface;
import java.util.List;

/* renamed from: tt4  reason: default package */
/* loaded from: classes2.dex */
public final class tt4 extends jt4 {
    public final /* synthetic */ IBinder f0;
    public final /* synthetic */ xt4 g0;

    public tt4(xt4 xt4Var, IBinder iBinder) {
        this.g0 = xt4Var;
        this.f0 = iBinder;
    }

    @Override // defpackage.jt4
    public final void a() {
        rt4 rt4Var;
        List<Runnable> list;
        List list2;
        zt4 zt4Var = this.g0.a;
        rt4Var = zt4Var.g;
        zt4Var.k = (IInterface) rt4Var.a(this.f0);
        zt4.j(this.g0.a);
        this.g0.a.e = false;
        list = this.g0.a.d;
        for (Runnable runnable : list) {
            runnable.run();
        }
        list2 = this.g0.a.d;
        list2.clear();
    }
}
