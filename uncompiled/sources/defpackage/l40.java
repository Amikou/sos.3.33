package defpackage;

import com.google.firebase.components.InvalidRegistrarException;
import com.google.firebase.components.MissingDependencyException;
import defpackage.l40;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: ComponentRuntime.java */
/* renamed from: l40  reason: default package */
/* loaded from: classes2.dex */
public class l40 extends r4 implements f40 {
    public static final fw2<Set<Object>> g = i40.a;
    public final Map<a40<?>, fw2<?>> a;
    public final Map<Class<?>, fw2<?>> b;
    public final Map<Class<?>, az1<?>> c;
    public final List<fw2<g40>> d;
    public final sx0 e;
    public final AtomicReference<Boolean> f;

    /* compiled from: ComponentRuntime.java */
    /* renamed from: l40$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public final Executor a;
        public final List<fw2<g40>> b = new ArrayList();
        public final List<a40<?>> c = new ArrayList();

        public b(Executor executor) {
            this.a = executor;
        }

        public static /* synthetic */ g40 f(g40 g40Var) {
            return g40Var;
        }

        public b b(a40<?> a40Var) {
            this.c.add(a40Var);
            return this;
        }

        public b c(final g40 g40Var) {
            this.b.add(new fw2() { // from class: m40
                @Override // defpackage.fw2
                public final Object get() {
                    g40 f;
                    f = l40.b.f(g40.this);
                    return f;
                }
            });
            return this;
        }

        public b d(Collection<fw2<g40>> collection) {
            this.b.addAll(collection);
            return this;
        }

        public l40 e() {
            return new l40(this.a, this.b, this.c);
        }
    }

    public static b i(Executor executor) {
        return new b(executor);
    }

    public static <T> List<T> m(Iterable<T> iterable) {
        ArrayList arrayList = new ArrayList();
        for (T t : iterable) {
            arrayList.add(t);
        }
        return arrayList;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ Object n(a40 a40Var) {
        return a40Var.f().a(new j83(a40Var, this));
    }

    @Override // defpackage.b40
    public synchronized <T> fw2<T> b(Class<T> cls) {
        bu2.c(cls, "Null interface requested.");
        return (fw2<T>) this.b.get(cls);
    }

    @Override // defpackage.b40
    public synchronized <T> fw2<Set<T>> c(Class<T> cls) {
        az1<?> az1Var = this.c.get(cls);
        if (az1Var != null) {
            return az1Var;
        }
        return (fw2<Set<T>>) g;
    }

    @Override // defpackage.b40
    public <T> rl0<T> e(Class<T> cls) {
        fw2<T> b2 = b(cls);
        if (b2 == null) {
            return tn2.e();
        }
        if (b2 instanceof tn2) {
            return (tn2) b2;
        }
        return tn2.i(b2);
    }

    public final void j(List<a40<?>> list) {
        ArrayList<Runnable> arrayList = new ArrayList();
        synchronized (this) {
            Iterator<fw2<g40>> it = this.d.iterator();
            while (it.hasNext()) {
                try {
                    g40 g40Var = it.next().get();
                    if (g40Var != null) {
                        list.addAll(g40Var.getComponents());
                        it.remove();
                    }
                } catch (InvalidRegistrarException unused) {
                    it.remove();
                }
            }
            if (this.a.isEmpty()) {
                cd0.a(list);
            } else {
                ArrayList arrayList2 = new ArrayList(this.a.keySet());
                arrayList2.addAll(list);
                cd0.a(arrayList2);
            }
            for (final a40<?> a40Var : list) {
                this.a.put(a40Var, new ty1(new fw2() { // from class: h40
                    @Override // defpackage.fw2
                    public final Object get() {
                        Object n;
                        n = l40.this.n(a40Var);
                        return n;
                    }
                }));
            }
            arrayList.addAll(s(list));
            arrayList.addAll(t());
            r();
        }
        for (Runnable runnable : arrayList) {
            runnable.run();
        }
        q();
    }

    public final void k(Map<a40<?>, fw2<?>> map, boolean z) {
        for (Map.Entry<a40<?>, fw2<?>> entry : map.entrySet()) {
            a40<?> key = entry.getKey();
            fw2<?> value = entry.getValue();
            if (key.k() || (key.l() && z)) {
                value.get();
            }
        }
        this.e.d();
    }

    public void l(boolean z) {
        HashMap hashMap;
        if (this.f.compareAndSet(null, Boolean.valueOf(z))) {
            synchronized (this) {
                hashMap = new HashMap(this.a);
            }
            k(hashMap, z);
        }
    }

    public final void q() {
        Boolean bool = this.f.get();
        if (bool != null) {
            k(this.a, bool.booleanValue());
        }
    }

    public final void r() {
        for (a40<?> a40Var : this.a.keySet()) {
            for (hm0 hm0Var : a40Var.e()) {
                if (hm0Var.g() && !this.c.containsKey(hm0Var.c())) {
                    this.c.put(hm0Var.c(), az1.b(Collections.emptySet()));
                } else if (this.b.containsKey(hm0Var.c())) {
                    continue;
                } else if (!hm0Var.f()) {
                    if (!hm0Var.g()) {
                        this.b.put(hm0Var.c(), tn2.e());
                    }
                } else {
                    throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", a40Var, hm0Var.c()));
                }
            }
        }
    }

    public final List<Runnable> s(List<a40<?>> list) {
        ArrayList arrayList = new ArrayList();
        for (a40<?> a40Var : list) {
            if (a40Var.m()) {
                final fw2<?> fw2Var = this.a.get(a40Var);
                for (Class<? super Object> cls : a40Var.g()) {
                    if (!this.b.containsKey(cls)) {
                        this.b.put(cls, fw2Var);
                    } else {
                        final tn2 tn2Var = (tn2) this.b.get(cls);
                        arrayList.add(new Runnable() { // from class: k40
                            @Override // java.lang.Runnable
                            public final void run() {
                                tn2.this.j(fw2Var);
                            }
                        });
                    }
                }
            }
        }
        return arrayList;
    }

    public final List<Runnable> t() {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        for (Map.Entry<a40<?>, fw2<?>> entry : this.a.entrySet()) {
            a40<?> key = entry.getKey();
            if (!key.m()) {
                fw2<?> value = entry.getValue();
                for (Class<? super Object> cls : key.g()) {
                    if (!hashMap.containsKey(cls)) {
                        hashMap.put(cls, new HashSet());
                    }
                    ((Set) hashMap.get(cls)).add(value);
                }
            }
        }
        for (Map.Entry entry2 : hashMap.entrySet()) {
            if (!this.c.containsKey(entry2.getKey())) {
                this.c.put((Class) entry2.getKey(), az1.b((Collection) entry2.getValue()));
            } else {
                final az1<?> az1Var = this.c.get(entry2.getKey());
                for (final fw2 fw2Var : (Set) entry2.getValue()) {
                    arrayList.add(new Runnable() { // from class: j40
                        @Override // java.lang.Runnable
                        public final void run() {
                            az1.this.a(fw2Var);
                        }
                    });
                }
            }
        }
        return arrayList;
    }

    public l40(Executor executor, Iterable<fw2<g40>> iterable, Collection<a40<?>> collection) {
        this.a = new HashMap();
        this.b = new HashMap();
        this.c = new HashMap();
        this.f = new AtomicReference<>();
        sx0 sx0Var = new sx0(executor);
        this.e = sx0Var;
        ArrayList arrayList = new ArrayList();
        arrayList.add(a40.p(sx0Var, sx0.class, mv3.class, ow2.class));
        arrayList.add(a40.p(this, f40.class, new Class[0]));
        for (a40<?> a40Var : collection) {
            if (a40Var != null) {
                arrayList.add(a40Var);
            }
        }
        this.d = m(iterable);
        j(arrayList);
    }
}
