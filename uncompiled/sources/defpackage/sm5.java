package defpackage;

import android.content.SharedPreferences;
import com.google.android.gms.flags.impl.d;

/* renamed from: sm5  reason: default package */
/* loaded from: classes.dex */
public final class sm5 extends g35<String> {
    public static String a(SharedPreferences sharedPreferences, String str, String str2) {
        try {
            return (String) of5.a(new d(sharedPreferences, str, str2));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            if (valueOf.length() != 0) {
                "Flag value not available, returning default: ".concat(valueOf);
            }
            return str2;
        }
    }
}
