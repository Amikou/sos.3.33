package defpackage;

import java.math.BigInteger;

/* renamed from: ne1  reason: default package */
/* loaded from: classes2.dex */
public class ne1 {
    public final BigInteger a;
    public final BigInteger b;
    public final BigInteger c;
    public final BigInteger d;
    public final BigInteger e;
    public final BigInteger f;
    public final BigInteger g;
    public final int h;

    public ne1(BigInteger bigInteger, BigInteger bigInteger2, BigInteger[] bigIntegerArr, BigInteger[] bigIntegerArr2, BigInteger bigInteger3, BigInteger bigInteger4, int i) {
        a(bigIntegerArr, "v1");
        a(bigIntegerArr2, "v2");
        this.a = bigInteger;
        this.b = bigIntegerArr[0];
        this.c = bigIntegerArr[1];
        this.d = bigIntegerArr2[0];
        this.e = bigIntegerArr2[1];
        this.f = bigInteger3;
        this.g = bigInteger4;
        this.h = i;
    }

    public static void a(BigInteger[] bigIntegerArr, String str) {
        if (bigIntegerArr == null || bigIntegerArr.length != 2 || bigIntegerArr[0] == null || bigIntegerArr[1] == null) {
            throw new IllegalArgumentException("'" + str + "' must consist of exactly 2 (non-null) values");
        }
    }

    public BigInteger b() {
        return this.a;
    }

    public int c() {
        return this.h;
    }

    public BigInteger d() {
        return this.f;
    }

    public BigInteger e() {
        return this.g;
    }

    public BigInteger f() {
        return this.b;
    }

    public BigInteger g() {
        return this.c;
    }

    public BigInteger h() {
        return this.d;
    }

    public BigInteger i() {
        return this.e;
    }
}
