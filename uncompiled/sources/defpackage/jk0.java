package defpackage;

import android.util.Base64;
import androidx.media3.common.u;
import androidx.media3.common.util.b;
import androidx.media3.exoplayer.source.j;
import defpackage.mr2;
import defpackage.tb;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

/* compiled from: DefaultPlaybackSessionManager.java */
/* renamed from: jk0  reason: default package */
/* loaded from: classes.dex */
public final class jk0 implements mr2 {
    public static final dw3<String> h = ik0.a;
    public static final Random i = new Random();
    public final u.c a;
    public final u.b b;
    public final HashMap<String, a> c;
    public final dw3<String> d;
    public mr2.a e;
    public u f;
    public String g;

    /* compiled from: DefaultPlaybackSessionManager.java */
    /* renamed from: jk0$a */
    /* loaded from: classes.dex */
    public final class a {
        public final String a;
        public int b;
        public long c;
        public j.b d;
        public boolean e;
        public boolean f;

        public a(String str, int i, j.b bVar) {
            this.a = str;
            this.b = i;
            this.c = bVar == null ? -1L : bVar.d;
            if (bVar == null || !bVar.b()) {
                return;
            }
            this.d = bVar;
        }

        public boolean i(int i, j.b bVar) {
            if (bVar == null) {
                return i == this.b;
            }
            j.b bVar2 = this.d;
            return bVar2 == null ? !bVar.b() && bVar.d == this.c : bVar.d == bVar2.d && bVar.b == bVar2.b && bVar.c == bVar2.c;
        }

        public boolean j(tb.a aVar) {
            long j = this.c;
            if (j == -1) {
                return false;
            }
            j.b bVar = aVar.d;
            if (bVar == null) {
                return this.b != aVar.c;
            } else if (bVar.d > j) {
                return true;
            } else {
                if (this.d == null) {
                    return false;
                }
                int b = aVar.b.b(bVar.a);
                int b2 = aVar.b.b(this.d.a);
                j.b bVar2 = aVar.d;
                if (bVar2.d < this.d.d || b < b2) {
                    return false;
                }
                if (b > b2) {
                    return true;
                }
                if (bVar2.b()) {
                    j.b bVar3 = aVar.d;
                    int i = bVar3.b;
                    int i2 = bVar3.c;
                    j.b bVar4 = this.d;
                    int i3 = bVar4.b;
                    return i > i3 || (i == i3 && i2 > bVar4.c);
                }
                int i4 = aVar.d.e;
                return i4 == -1 || i4 > this.d.b;
            }
        }

        public void k(int i, j.b bVar) {
            if (this.c == -1 && i == this.b && bVar != null) {
                this.c = bVar.d;
            }
        }

        public final int l(u uVar, u uVar2, int i) {
            if (i < uVar.p()) {
                uVar.n(i, jk0.this.a);
                for (int i2 = jk0.this.a.s0; i2 <= jk0.this.a.t0; i2++) {
                    int b = uVar2.b(uVar.m(i2));
                    if (b != -1) {
                        return uVar2.f(b, jk0.this.b).g0;
                    }
                }
                return -1;
            } else if (i < uVar2.p()) {
                return i;
            } else {
                return -1;
            }
        }

        public boolean m(u uVar, u uVar2) {
            int l = l(uVar, uVar2, this.b);
            this.b = l;
            if (l == -1) {
                return false;
            }
            j.b bVar = this.d;
            return bVar == null || uVar2.b(bVar.a) != -1;
        }
    }

    public jk0() {
        this(h);
    }

    public static String k() {
        byte[] bArr = new byte[12];
        i.nextBytes(bArr);
        return Base64.encodeToString(bArr, 10);
    }

    @Override // defpackage.mr2
    public synchronized String a() {
        return this.g;
    }

    @Override // defpackage.mr2
    public synchronized String b(u uVar, j.b bVar) {
        return l(uVar.h(bVar.a, this.b).g0, bVar).a;
    }

    @Override // defpackage.mr2
    public synchronized void c(tb.a aVar, int i2) {
        ii.e(this.e);
        boolean z = i2 == 0;
        Iterator<a> it = this.c.values().iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (next.j(aVar)) {
                it.remove();
                if (next.e) {
                    boolean equals = next.a.equals(this.g);
                    boolean z2 = z && equals && next.f;
                    if (equals) {
                        this.g = null;
                    }
                    this.e.n(aVar, next.a, z2);
                }
            }
        }
        m(aVar);
    }

    @Override // defpackage.mr2
    public synchronized void d(tb.a aVar) {
        mr2.a aVar2;
        this.g = null;
        Iterator<a> it = this.c.values().iterator();
        while (it.hasNext()) {
            a next = it.next();
            it.remove();
            if (next.e && (aVar2 = this.e) != null) {
                aVar2.n(aVar, next.a, false);
            }
        }
    }

    @Override // defpackage.mr2
    public synchronized void e(tb.a aVar) {
        ii.e(this.e);
        u uVar = this.f;
        this.f = aVar.b;
        Iterator<a> it = this.c.values().iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!next.m(uVar, this.f) || next.j(aVar)) {
                it.remove();
                if (next.e) {
                    if (next.a.equals(this.g)) {
                        this.g = null;
                    }
                    this.e.n(aVar, next.a, false);
                }
            }
        }
        m(aVar);
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x00e1 A[Catch: all -> 0x0118, TryCatch #0 {, blocks: (B:4:0x0005, B:8:0x0014, B:11:0x0025, B:13:0x0030, B:16:0x003a, B:23:0x004b, B:25:0x0057, B:26:0x005d, B:28:0x0061, B:30:0x0067, B:32:0x0080, B:34:0x00db, B:36:0x00e1, B:38:0x00f7, B:40:0x0103, B:42:0x0109), top: B:48:0x0005 }] */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00f3  */
    @Override // defpackage.mr2
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public synchronized void f(defpackage.tb.a r25) {
        /*
            Method dump skipped, instructions count: 283
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.jk0.f(tb$a):void");
    }

    @Override // defpackage.mr2
    public void g(mr2.a aVar) {
        this.e = aVar;
    }

    public final a l(int i2, j.b bVar) {
        int i3;
        a aVar = null;
        long j = Long.MAX_VALUE;
        for (a aVar2 : this.c.values()) {
            aVar2.k(i2, bVar);
            if (aVar2.i(i2, bVar)) {
                long j2 = aVar2.c;
                if (j2 == -1 || j2 < j) {
                    aVar = aVar2;
                    j = j2;
                } else if (i3 == 0 && ((a) b.j(aVar)).d != null && aVar2.d != null) {
                    aVar = aVar2;
                }
            }
        }
        if (aVar == null) {
            String str = this.d.get();
            a aVar3 = new a(str, i2, bVar);
            this.c.put(str, aVar3);
            return aVar3;
        }
        return aVar;
    }

    public final void m(tb.a aVar) {
        if (aVar.b.q()) {
            this.g = null;
            return;
        }
        a aVar2 = this.c.get(this.g);
        a l = l(aVar.c, aVar.d);
        this.g = l.a;
        f(aVar);
        j.b bVar = aVar.d;
        if (bVar == null || !bVar.b()) {
            return;
        }
        if (aVar2 != null && aVar2.c == aVar.d.d && aVar2.d != null && aVar2.d.b == aVar.d.b && aVar2.d.c == aVar.d.c) {
            return;
        }
        j.b bVar2 = aVar.d;
        this.e.V(aVar, l(aVar.c, new j.b(bVar2.a, bVar2.d)).a, l.a);
    }

    public jk0(dw3<String> dw3Var) {
        this.d = dw3Var;
        this.a = new u.c();
        this.b = new u.b();
        this.c = new HashMap<>();
        this.f = u.a;
    }
}
