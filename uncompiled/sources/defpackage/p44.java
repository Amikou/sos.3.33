package defpackage;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;

/* compiled from: TextScale.java */
/* renamed from: p44  reason: default package */
/* loaded from: classes2.dex */
public class p44 extends Transition {

    /* compiled from: TextScale.java */
    /* renamed from: p44$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ TextView a;

        public a(p44 p44Var, TextView textView) {
            this.a = textView;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.setScaleX(floatValue);
            this.a.setScaleY(floatValue);
        }
    }

    @Override // androidx.transition.Transition
    public void h(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public void l(kb4 kb4Var) {
        s0(kb4Var);
    }

    @Override // androidx.transition.Transition
    public Animator s(ViewGroup viewGroup, kb4 kb4Var, kb4 kb4Var2) {
        if (kb4Var == null || kb4Var2 == null || !(kb4Var.b instanceof TextView)) {
            return null;
        }
        View view = kb4Var2.b;
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            Map<String, Object> map = kb4Var.a;
            Map<String, Object> map2 = kb4Var2.a;
            float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
            float floatValue2 = map2.get("android:textscale:scale") != null ? ((Float) map2.get("android:textscale:scale")).floatValue() : 1.0f;
            if (floatValue == floatValue2) {
                return null;
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat(floatValue, floatValue2);
            ofFloat.addUpdateListener(new a(this, textView));
            return ofFloat;
        }
        return null;
    }

    public final void s0(kb4 kb4Var) {
        View view = kb4Var.b;
        if (view instanceof TextView) {
            kb4Var.a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }
}
