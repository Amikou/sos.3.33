package defpackage;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Locale;

/* compiled from: FileUtils.java */
/* renamed from: a41  reason: default package */
/* loaded from: classes2.dex */
public class a41 {
    public static String a(String str) {
        int lastIndexOf;
        return (!ru3.b(str) || (lastIndexOf = str.lastIndexOf(".")) == -1) ? "" : str.substring(lastIndexOf + 1).toLowerCase(Locale.US).trim();
    }

    public static String b(Long l) {
        return c(l, true);
    }

    public static String c(Long l, boolean z) {
        if (l == null || l.longValue() < 0) {
            return "";
        }
        int i = z ? 1000 : RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE;
        if (l.longValue() < i) {
            return l + " B";
        }
        double d = i;
        int log = (int) (Math.log(l.longValue()) / Math.log(d));
        StringBuilder sb = new StringBuilder();
        sb.append((z ? "kMGTPE" : "KMGTPE").charAt(log - 1));
        sb.append(z ? "" : "i");
        return String.format(Locale.US, "%.1f %sB", Double.valueOf(l.longValue() / Math.pow(d, log)), sb.toString());
    }
}
