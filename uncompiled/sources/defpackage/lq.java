package defpackage;

import android.graphics.Bitmap;

/* compiled from: BitmapPoolBackend.java */
/* renamed from: lq  reason: default package */
/* loaded from: classes.dex */
public class lq extends q22<Bitmap> {
    @Override // defpackage.q22, defpackage.vs2
    /* renamed from: d */
    public Bitmap get(int i) {
        Bitmap bitmap = (Bitmap) super.get(i);
        if (bitmap == null || !f(bitmap)) {
            return null;
        }
        bitmap.eraseColor(0);
        return bitmap;
    }

    @Override // defpackage.vs2
    /* renamed from: e */
    public int a(Bitmap bitmap) {
        return rq.e(bitmap);
    }

    public boolean f(Bitmap bitmap) {
        if (bitmap == null) {
            return false;
        }
        if (bitmap.isRecycled()) {
            v11.A("BitmapPoolBackend", "Cannot reuse a recycled bitmap: %s", bitmap);
            return false;
        } else if (bitmap.isMutable()) {
            return true;
        } else {
            v11.A("BitmapPoolBackend", "Cannot reuse an immutable bitmap: %s", bitmap);
            return false;
        }
    }

    @Override // defpackage.q22, defpackage.vs2
    /* renamed from: g */
    public void c(Bitmap bitmap) {
        if (f(bitmap)) {
            super.c(bitmap);
        }
    }
}
