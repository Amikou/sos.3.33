package defpackage;

import kotlin.Result;

/* compiled from: JobSupport.kt */
/* renamed from: r83  reason: default package */
/* loaded from: classes2.dex */
public final class r83<T> extends au1 {
    public final pv<T> i0;

    /* JADX WARN: Multi-variable type inference failed */
    public r83(pv<? super T> pvVar) {
        this.i0 = pvVar;
    }

    @Override // defpackage.tc1
    public /* bridge */ /* synthetic */ te4 invoke(Throwable th) {
        y(th);
        return te4.a;
    }

    @Override // defpackage.v30
    public void y(Throwable th) {
        Object a0 = z().a0();
        if (ze0.a() && !(!(a0 instanceof dq1))) {
            throw new AssertionError();
        }
        if (a0 instanceof t30) {
            pv<T> pvVar = this.i0;
            Throwable th2 = ((t30) a0).a;
            Result.a aVar = Result.Companion;
            pvVar.resumeWith(Result.m52constructorimpl(o83.a(th2)));
            return;
        }
        pv<T> pvVar2 = this.i0;
        Object h = cu1.h(a0);
        Result.a aVar2 = Result.Companion;
        pvVar2.resumeWith(Result.m52constructorimpl(h));
    }
}
