package defpackage;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/* compiled from: ActionMode.java */
/* renamed from: k6  reason: default package */
/* loaded from: classes.dex */
public abstract class k6 {
    public Object a;
    public boolean f0;

    /* compiled from: ActionMode.java */
    /* renamed from: k6$a */
    /* loaded from: classes.dex */
    public interface a {
        boolean a(k6 k6Var, MenuItem menuItem);

        void b(k6 k6Var);

        boolean c(k6 k6Var, Menu menu);

        boolean d(k6 k6Var, Menu menu);
    }

    public abstract void c();

    public abstract View d();

    public abstract Menu e();

    public abstract MenuInflater f();

    public abstract CharSequence g();

    public Object h() {
        return this.a;
    }

    public abstract CharSequence i();

    public boolean j() {
        return this.f0;
    }

    public abstract void k();

    public abstract boolean l();

    public abstract void m(View view);

    public abstract void n(int i);

    public abstract void o(CharSequence charSequence);

    public void p(Object obj) {
        this.a = obj;
    }

    public abstract void q(int i);

    public abstract void r(CharSequence charSequence);

    public void s(boolean z) {
        this.f0 = z;
    }
}
