package defpackage;

import org.bouncycastle.asn1.a1;
import org.bouncycastle.asn1.n0;
import org.bouncycastle.asn1.p0;
import org.bouncycastle.asn1.z0;

/* renamed from: ld0  reason: default package */
/* loaded from: classes2.dex */
public class ld0 {
    public static final h4 a = new n0();
    public static final j4 b = new p0();

    public static h4 a(d4 d4Var) {
        return d4Var.c() < 1 ? a : new z0(d4Var);
    }

    public static j4 b(d4 d4Var) {
        return d4Var.c() < 1 ? b : new a1(d4Var);
    }
}
