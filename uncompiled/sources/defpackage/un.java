package defpackage;

import com.facebook.common.internal.ImmutableSet;
import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.image.EncodedImageOrigin;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: BaseProducerContext.java */
/* renamed from: un  reason: default package */
/* loaded from: classes.dex */
public class un implements ev2 {
    public static final Set<String> n = ImmutableSet.of((Object[]) new String[]{"id", "uri_source"});
    public final ImageRequest a;
    public final String b;
    public final String c;
    public final iv2 d;
    public final Object e;
    public final ImageRequest.RequestLevel f;
    public final Map<String, Object> g;
    public boolean h;
    public Priority i;
    public boolean j;
    public boolean k;
    public final List<fv2> l;
    public final so1 m;

    public un(ImageRequest imageRequest, String str, iv2 iv2Var, Object obj, ImageRequest.RequestLevel requestLevel, boolean z, boolean z2, Priority priority, so1 so1Var) {
        this(imageRequest, str, null, iv2Var, obj, requestLevel, z, z2, priority, so1Var);
    }

    public static void p(List<fv2> list) {
        if (list == null) {
            return;
        }
        for (fv2 fv2Var : list) {
            fv2Var.a();
        }
    }

    public static void q(List<fv2> list) {
        if (list == null) {
            return;
        }
        for (fv2 fv2Var : list) {
            fv2Var.b();
        }
    }

    public static void r(List<fv2> list) {
        if (list == null) {
            return;
        }
        for (fv2 fv2Var : list) {
            fv2Var.d();
        }
    }

    public static void s(List<fv2> list) {
        if (list == null) {
            return;
        }
        for (fv2 fv2Var : list) {
            fv2Var.c();
        }
    }

    @Override // defpackage.ev2
    public Object a() {
        return this.e;
    }

    @Override // defpackage.ev2
    public void b(String str, Object obj) {
        if (n.contains(str)) {
            return;
        }
        this.g.put(str, obj);
    }

    @Override // defpackage.ev2
    public ImageRequest c() {
        return this.a;
    }

    @Override // defpackage.ev2
    public so1 d() {
        return this.m;
    }

    @Override // defpackage.ev2
    public void e(EncodedImageOrigin encodedImageOrigin) {
    }

    @Override // defpackage.ev2
    public void f(String str, String str2) {
        this.g.put("origin", str);
        this.g.put("origin_sub", str2);
    }

    @Override // defpackage.ev2
    public void g(Map<String, ?> map) {
        if (map == null) {
            return;
        }
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            b(entry.getKey(), entry.getValue());
        }
    }

    @Override // defpackage.ev2
    public Map<String, Object> getExtras() {
        return this.g;
    }

    @Override // defpackage.ev2
    public String getId() {
        return this.b;
    }

    @Override // defpackage.ev2
    public synchronized Priority getPriority() {
        return this.i;
    }

    @Override // defpackage.ev2
    public synchronized boolean h() {
        return this.h;
    }

    @Override // defpackage.ev2
    public <T> T i(String str) {
        return (T) this.g.get(str);
    }

    @Override // defpackage.ev2
    public String j() {
        return this.c;
    }

    @Override // defpackage.ev2
    public void k(String str) {
        f(str, "default");
    }

    @Override // defpackage.ev2
    public iv2 l() {
        return this.d;
    }

    @Override // defpackage.ev2
    public synchronized boolean m() {
        return this.j;
    }

    @Override // defpackage.ev2
    public ImageRequest.RequestLevel n() {
        return this.f;
    }

    @Override // defpackage.ev2
    public void o(fv2 fv2Var) {
        boolean z;
        synchronized (this) {
            this.l.add(fv2Var);
            z = this.k;
        }
        if (z) {
            fv2Var.a();
        }
    }

    public void t() {
        p(u());
    }

    public synchronized List<fv2> u() {
        if (this.k) {
            return null;
        }
        this.k = true;
        return new ArrayList(this.l);
    }

    public synchronized List<fv2> v(boolean z) {
        if (z == this.j) {
            return null;
        }
        this.j = z;
        return new ArrayList(this.l);
    }

    public synchronized List<fv2> w(boolean z) {
        if (z == this.h) {
            return null;
        }
        this.h = z;
        return new ArrayList(this.l);
    }

    public synchronized List<fv2> x(Priority priority) {
        if (priority == this.i) {
            return null;
        }
        this.i = priority;
        return new ArrayList(this.l);
    }

    public un(ImageRequest imageRequest, String str, String str2, iv2 iv2Var, Object obj, ImageRequest.RequestLevel requestLevel, boolean z, boolean z2, Priority priority, so1 so1Var) {
        EncodedImageOrigin encodedImageOrigin = EncodedImageOrigin.NOT_SET;
        this.a = imageRequest;
        this.b = str;
        HashMap hashMap = new HashMap();
        this.g = hashMap;
        hashMap.put("id", str);
        hashMap.put("uri_source", imageRequest == null ? "null-request" : imageRequest.u());
        this.c = str2;
        this.d = iv2Var;
        this.e = obj;
        this.f = requestLevel;
        this.h = z;
        this.i = priority;
        this.j = z2;
        this.k = false;
        this.l = new ArrayList();
        this.m = so1Var;
    }
}
