package defpackage;

import android.util.Patterns;
import java.util.Locale;
import java.util.regex.Pattern;
import kotlin.text.StringsKt__StringsKt;

/* compiled from: TextValidationUtil.kt */
/* renamed from: s44  reason: default package */
/* loaded from: classes2.dex */
public final class s44 {
    public static final s44 a = new s44();

    public static final boolean b(String str) {
        return a.a(str, "(?=.*[A-Z])(?=\\S+$).+$");
    }

    public static final boolean d(String str) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (str == null) {
            str = "";
        }
        return pattern.matcher(str).matches();
    }

    public static final boolean e(String str) {
        return a.a(str, "^(?=.*[0-9])(?=\\S+$).+$");
    }

    public static final boolean g(String str) {
        return a.a(str, "^(?=.*[0-9])(?=.*[@#$%^_&+=!])(?=\\S+$).{4,}$");
    }

    public static final boolean h(String str) {
        return a.a(str, "(?=.*[_@#$%^&+=!])(?=\\S+$).+$");
    }

    public final boolean a(String str, String str2) {
        Pattern compile = Pattern.compile(str2);
        if (str == null) {
            str = "";
        }
        return compile.matcher(str).matches();
    }

    public final boolean c(String str) {
        fs1.f(str, "<this>");
        return StringsKt__StringsKt.K0(str).toString().length() > 0;
    }

    public final boolean f(String str) {
        fs1.f(str, "<this>");
        try {
            String lowerCase = str.toLowerCase(Locale.ROOT);
            fs1.e(lowerCase, "(this as java.lang.Strin….toLowerCase(Locale.ROOT)");
            if (!fs1.b(lowerCase, "0x")) {
                ej2.toBigInt(ej2.cleanHexPrefix(str));
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public final boolean i(String str) {
        fs1.f(str, "<this>");
        return dv3.H(str, "0x", false, 2, null) && f(str) && str.length() == 42;
    }
}
