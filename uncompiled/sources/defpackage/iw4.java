package defpackage;

import android.content.Context;
import com.google.android.play.core.assetpacks.c;
import com.google.android.play.core.assetpacks.j;
import com.google.android.play.core.internal.d;

/* renamed from: iw4  reason: default package */
/* loaded from: classes2.dex */
public final class iw4 implements jw4<hw4> {
    public final jw4 a;
    public final jw4 b;
    public final jw4 c;
    public final /* synthetic */ int d = 0;

    public iw4(jw4<zv4> jw4Var, jw4<c> jw4Var2, jw4<su4> jw4Var3) {
        this.a = jw4Var;
        this.b = jw4Var2;
        this.c = jw4Var3;
    }

    public iw4(jw4<Context> jw4Var, jw4<st4> jw4Var2, jw4<j> jw4Var3, byte[] bArr) {
        this.c = jw4Var;
        this.b = jw4Var2;
        this.a = jw4Var3;
    }

    /* JADX WARN: Type inference failed for: r0v10, types: [zy4, hw4, java.lang.Object] */
    @Override // defpackage.jw4
    public final /* bridge */ /* synthetic */ hw4 a() {
        if (this.d == 0) {
            return new hw4((zv4) this.a.a(), (c) this.b.a(), (su4) this.c.a());
        }
        ?? r0 = (zy4) (dy4.b(((my4) this.c).a()) == null ? gw4.c(this.b).a() : gw4.c(this.a).a());
        d.k(r0);
        return r0;
    }
}
