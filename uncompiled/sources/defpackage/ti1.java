package defpackage;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.WorkInfo;
import androidx.work.a;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: GreedyScheduler.java */
/* renamed from: ti1  reason: default package */
/* loaded from: classes.dex */
public class ti1 implements cd3, vp4, qy0 {
    public static final String m0 = v12.f("GreedyScheduler");
    public final Context a;
    public final hq4 f0;
    public final wp4 g0;
    public am0 i0;
    public boolean j0;
    public Boolean l0;
    public final Set<tq4> h0 = new HashSet();
    public final Object k0 = new Object();

    public ti1(Context context, a aVar, q34 q34Var, hq4 hq4Var) {
        this.a = context;
        this.f0 = hq4Var;
        this.g0 = new wp4(context, q34Var, this);
        this.i0 = new am0(this, aVar.k());
    }

    @Override // defpackage.cd3
    public boolean a() {
        return false;
    }

    @Override // defpackage.vp4
    public void b(List<String> list) {
        for (String str : list) {
            v12.c().a(m0, String.format("Constraints not met: Cancelling work ID %s", str), new Throwable[0]);
            this.f0.z(str);
        }
    }

    @Override // defpackage.qy0
    public void c(String str, boolean z) {
        i(str);
    }

    @Override // defpackage.cd3
    public void d(String str) {
        if (this.l0 == null) {
            g();
        }
        if (!this.l0.booleanValue()) {
            v12.c().d(m0, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        h();
        v12.c().a(m0, String.format("Cancelling work ID %s", str), new Throwable[0]);
        am0 am0Var = this.i0;
        if (am0Var != null) {
            am0Var.b(str);
        }
        this.f0.z(str);
    }

    @Override // defpackage.cd3
    public void e(tq4... tq4VarArr) {
        if (this.l0 == null) {
            g();
        }
        if (!this.l0.booleanValue()) {
            v12.c().d(m0, "Ignoring schedule request in a secondary process", new Throwable[0]);
            return;
        }
        h();
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (tq4 tq4Var : tq4VarArr) {
            long a = tq4Var.a();
            long currentTimeMillis = System.currentTimeMillis();
            if (tq4Var.b == WorkInfo.State.ENQUEUED) {
                if (currentTimeMillis < a) {
                    am0 am0Var = this.i0;
                    if (am0Var != null) {
                        am0Var.a(tq4Var);
                    }
                } else if (tq4Var.b()) {
                    int i = Build.VERSION.SDK_INT;
                    if (i >= 23 && tq4Var.j.h()) {
                        v12.c().a(m0, String.format("Ignoring WorkSpec %s, Requires device idle.", tq4Var), new Throwable[0]);
                    } else if (i >= 24 && tq4Var.j.e()) {
                        v12.c().a(m0, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", tq4Var), new Throwable[0]);
                    } else {
                        hashSet.add(tq4Var);
                        hashSet2.add(tq4Var.a);
                    }
                } else {
                    v12.c().a(m0, String.format("Starting work for %s", tq4Var.a), new Throwable[0]);
                    this.f0.w(tq4Var.a);
                }
            }
        }
        synchronized (this.k0) {
            if (!hashSet.isEmpty()) {
                v12.c().a(m0, String.format("Starting tracking for [%s]", TextUtils.join(",", hashSet2)), new Throwable[0]);
                this.h0.addAll(hashSet);
                this.g0.d(this.h0);
            }
        }
    }

    @Override // defpackage.vp4
    public void f(List<String> list) {
        for (String str : list) {
            v12.c().a(m0, String.format("Constraints met: Scheduling work ID %s", str), new Throwable[0]);
            this.f0.w(str);
        }
    }

    public final void g() {
        this.l0 = Boolean.valueOf(av2.b(this.a, this.f0.k()));
    }

    public final void h() {
        if (this.j0) {
            return;
        }
        this.f0.o().d(this);
        this.j0 = true;
    }

    public final void i(String str) {
        synchronized (this.k0) {
            Iterator<tq4> it = this.h0.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                tq4 next = it.next();
                if (next.a.equals(str)) {
                    v12.c().a(m0, String.format("Stopping tracking for %s", str), new Throwable[0]);
                    this.h0.remove(next);
                    this.g0.d(this.h0);
                    break;
                }
            }
        }
    }
}
