package defpackage;

import android.content.Context;
import com.google.android.gms.measurement.internal.h;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: ql5  reason: default package */
/* loaded from: classes.dex */
public class ql5 implements tl5 {
    public final ck5 a;

    public ql5(ck5 ck5Var) {
        zt2.j(ck5Var);
        this.a = ck5Var;
    }

    @Override // defpackage.tl5
    public final rz a() {
        throw null;
    }

    @Override // defpackage.tl5
    public final c66 b() {
        throw null;
    }

    public void d() {
        this.a.q().d();
    }

    public void e() {
        this.a.q().e();
    }

    @Override // defpackage.tl5
    public final Context m() {
        throw null;
    }

    @Override // defpackage.tl5
    public final h q() {
        throw null;
    }

    @Override // defpackage.tl5
    public final og5 w() {
        throw null;
    }
}
