package defpackage;

import java.util.concurrent.Callable;

/* compiled from: FlowableDefer.java */
/* renamed from: t71  reason: default package */
/* loaded from: classes2.dex */
public final class t71<T> extends q71<T> {
    public final Callable<? extends nw2<? extends T>> b;

    public t71(Callable<? extends nw2<? extends T>> callable) {
        this.b = callable;
    }
}
