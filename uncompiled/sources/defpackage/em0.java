package defpackage;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import kotlin.Pair;

/* compiled from: Strings.kt */
/* renamed from: em0  reason: default package */
/* loaded from: classes2.dex */
public final class em0 implements ol3<sr1> {
    public final CharSequence a;
    public final int b;
    public final int c;
    public final hd1<CharSequence, Integer, Pair<Integer, Integer>> d;

    /* compiled from: Strings.kt */
    /* renamed from: em0$a */
    /* loaded from: classes2.dex */
    public static final class a implements Iterator<sr1>, tw1 {
        public int a = -1;
        public int f0;
        public int g0;
        public sr1 h0;
        public int i0;

        public a() {
            int f = u33.f(em0.this.b, 0, em0.this.a.length());
            this.f0 = f;
            this.g0 = f;
        }

        /* JADX WARN: Code restructure failed: missing block: B:8:0x0021, code lost:
            if (r0 < r6.j0.c) goto L13;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        public final void a() {
            /*
                r6 = this;
                int r0 = r6.g0
                r1 = 0
                if (r0 >= 0) goto Lc
                r6.a = r1
                r0 = 0
                r6.h0 = r0
                goto L9e
            Lc:
                em0 r0 = defpackage.em0.this
                int r0 = defpackage.em0.d(r0)
                r2 = -1
                r3 = 1
                if (r0 <= 0) goto L23
                int r0 = r6.i0
                int r0 = r0 + r3
                r6.i0 = r0
                em0 r4 = defpackage.em0.this
                int r4 = defpackage.em0.d(r4)
                if (r0 >= r4) goto L31
            L23:
                int r0 = r6.g0
                em0 r4 = defpackage.em0.this
                java.lang.CharSequence r4 = defpackage.em0.c(r4)
                int r4 = r4.length()
                if (r0 <= r4) goto L47
            L31:
                int r0 = r6.f0
                sr1 r1 = new sr1
                em0 r4 = defpackage.em0.this
                java.lang.CharSequence r4 = defpackage.em0.c(r4)
                int r4 = kotlin.text.StringsKt__StringsKt.T(r4)
                r1.<init>(r0, r4)
                r6.h0 = r1
                r6.g0 = r2
                goto L9c
            L47:
                em0 r0 = defpackage.em0.this
                hd1 r0 = defpackage.em0.b(r0)
                em0 r4 = defpackage.em0.this
                java.lang.CharSequence r4 = defpackage.em0.c(r4)
                int r5 = r6.g0
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                java.lang.Object r0 = r0.invoke(r4, r5)
                kotlin.Pair r0 = (kotlin.Pair) r0
                if (r0 != 0) goto L77
                int r0 = r6.f0
                sr1 r1 = new sr1
                em0 r4 = defpackage.em0.this
                java.lang.CharSequence r4 = defpackage.em0.c(r4)
                int r4 = kotlin.text.StringsKt__StringsKt.T(r4)
                r1.<init>(r0, r4)
                r6.h0 = r1
                r6.g0 = r2
                goto L9c
            L77:
                java.lang.Object r2 = r0.component1()
                java.lang.Number r2 = (java.lang.Number) r2
                int r2 = r2.intValue()
                java.lang.Object r0 = r0.component2()
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r4 = r6.f0
                sr1 r4 = defpackage.u33.k(r4, r2)
                r6.h0 = r4
                int r2 = r2 + r0
                r6.f0 = r2
                if (r0 != 0) goto L99
                r1 = r3
            L99:
                int r2 = r2 + r1
                r6.g0 = r2
            L9c:
                r6.a = r3
            L9e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: defpackage.em0.a.a():void");
        }

        @Override // java.util.Iterator
        /* renamed from: b */
        public sr1 next() {
            if (this.a == -1) {
                a();
            }
            if (this.a != 0) {
                sr1 sr1Var = this.h0;
                Objects.requireNonNull(sr1Var, "null cannot be cast to non-null type kotlin.ranges.IntRange");
                this.h0 = null;
                this.a = -1;
                return sr1Var;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.a == -1) {
                a();
            }
            return this.a == 1;
        }

        @Override // java.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public em0(CharSequence charSequence, int i, int i2, hd1<? super CharSequence, ? super Integer, Pair<Integer, Integer>> hd1Var) {
        fs1.f(charSequence, "input");
        fs1.f(hd1Var, "getNextMatch");
        this.a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = hd1Var;
    }

    @Override // defpackage.ol3
    public Iterator<sr1> iterator() {
        return new a();
    }
}
