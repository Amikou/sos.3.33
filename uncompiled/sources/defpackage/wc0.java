package defpackage;

import android.graphics.drawable.Drawable;

/* compiled from: CustomTarget.java */
/* renamed from: wc0  reason: default package */
/* loaded from: classes.dex */
public abstract class wc0<T> implements i34<T> {
    public final int a;
    public final int f0;
    public d73 g0;

    public wc0() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @Override // defpackage.i34
    public final void a(dq3 dq3Var) {
        dq3Var.f(this.a, this.f0);
    }

    @Override // defpackage.pz1
    public void b() {
    }

    @Override // defpackage.i34
    public final void c(d73 d73Var) {
        this.g0 = d73Var;
    }

    @Override // defpackage.pz1
    public void e() {
    }

    @Override // defpackage.i34
    public final void f(dq3 dq3Var) {
    }

    @Override // defpackage.i34
    public void g(Drawable drawable) {
    }

    @Override // defpackage.pz1
    public void h() {
    }

    @Override // defpackage.i34
    public void k(Drawable drawable) {
    }

    @Override // defpackage.i34
    public final d73 l() {
        return this.g0;
    }

    public wc0(int i, int i2) {
        if (mg4.t(i, i2)) {
            this.a = i;
            this.f0 = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }
}
