package defpackage;

import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.image.EncodedImageOrigin;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;

/* compiled from: ProducerContext.java */
/* renamed from: ev2  reason: default package */
/* loaded from: classes.dex */
public interface ev2 {
    Object a();

    <E> void b(String str, E e);

    ImageRequest c();

    so1 d();

    void e(EncodedImageOrigin encodedImageOrigin);

    void f(String str, String str2);

    void g(Map<String, ?> map);

    Map<String, Object> getExtras();

    String getId();

    Priority getPriority();

    boolean h();

    <E> E i(String str);

    String j();

    void k(String str);

    iv2 l();

    boolean m();

    ImageRequest.RequestLevel n();

    void o(fv2 fv2Var);
}
