package defpackage;

import defpackage.pt0;

/* renamed from: df3  reason: default package */
/* loaded from: classes2.dex */
public class df3 extends pt0.c {
    public df3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2) {
        this(xs0Var, ct0Var, ct0Var2, false);
    }

    public df3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, boolean z) {
        super(xs0Var, ct0Var, ct0Var2);
        if ((ct0Var == null) != (ct0Var2 == null)) {
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }
        this.e = z;
    }

    public df3(xs0 xs0Var, ct0 ct0Var, ct0 ct0Var2, ct0[] ct0VarArr, boolean z) {
        super(xs0Var, ct0Var, ct0Var2, ct0VarArr);
        this.e = z;
    }

    @Override // defpackage.pt0
    public pt0 H() {
        return (u() || this.c.i()) ? this : J().a(this);
    }

    @Override // defpackage.pt0
    public pt0 J() {
        if (u()) {
            return this;
        }
        xs0 i = i();
        cf3 cf3Var = (cf3) this.c;
        if (cf3Var.i()) {
            return i.v();
        }
        cf3 cf3Var2 = (cf3) this.b;
        cf3 cf3Var3 = (cf3) this.d[0];
        int[] e = dd2.e();
        int[] e2 = dd2.e();
        int[] e3 = dd2.e();
        bf3.j(cf3Var.f, e3);
        int[] e4 = dd2.e();
        bf3.j(e3, e4);
        boolean h = cf3Var3.h();
        int[] iArr = cf3Var3.f;
        if (!h) {
            bf3.j(iArr, e2);
            iArr = e2;
        }
        bf3.m(cf3Var2.f, iArr, e);
        bf3.a(cf3Var2.f, iArr, e2);
        bf3.e(e2, e, e2);
        bf3.i(dd2.b(e2, e2, e2), e2);
        bf3.e(e3, cf3Var2.f, e3);
        bf3.i(kd2.G(7, e3, 2, 0), e3);
        bf3.i(kd2.H(7, e4, 3, 0, e), e);
        cf3 cf3Var4 = new cf3(e4);
        bf3.j(e2, cf3Var4.f);
        int[] iArr2 = cf3Var4.f;
        bf3.m(iArr2, e3, iArr2);
        int[] iArr3 = cf3Var4.f;
        bf3.m(iArr3, e3, iArr3);
        cf3 cf3Var5 = new cf3(e3);
        bf3.m(e3, cf3Var4.f, cf3Var5.f);
        int[] iArr4 = cf3Var5.f;
        bf3.e(iArr4, e2, iArr4);
        int[] iArr5 = cf3Var5.f;
        bf3.m(iArr5, e, iArr5);
        cf3 cf3Var6 = new cf3(e2);
        bf3.n(cf3Var.f, cf3Var6.f);
        if (!h) {
            int[] iArr6 = cf3Var6.f;
            bf3.e(iArr6, cf3Var3.f, iArr6);
        }
        return new df3(i, cf3Var4, cf3Var5, new ct0[]{cf3Var6}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 K(pt0 pt0Var) {
        return this == pt0Var ? H() : u() ? pt0Var : pt0Var.u() ? J() : this.c.i() ? pt0Var : J().a(pt0Var);
    }

    @Override // defpackage.pt0
    public pt0 a(pt0 pt0Var) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (u()) {
            return pt0Var;
        }
        if (pt0Var.u()) {
            return this;
        }
        if (this == pt0Var) {
            return J();
        }
        xs0 i = i();
        cf3 cf3Var = (cf3) this.b;
        cf3 cf3Var2 = (cf3) this.c;
        cf3 cf3Var3 = (cf3) pt0Var.q();
        cf3 cf3Var4 = (cf3) pt0Var.r();
        cf3 cf3Var5 = (cf3) this.d[0];
        cf3 cf3Var6 = (cf3) pt0Var.s(0);
        int[] f = dd2.f();
        int[] e = dd2.e();
        int[] e2 = dd2.e();
        int[] e3 = dd2.e();
        boolean h = cf3Var5.h();
        if (h) {
            iArr = cf3Var3.f;
            iArr2 = cf3Var4.f;
        } else {
            bf3.j(cf3Var5.f, e2);
            bf3.e(e2, cf3Var3.f, e);
            bf3.e(e2, cf3Var5.f, e2);
            bf3.e(e2, cf3Var4.f, e2);
            iArr = e;
            iArr2 = e2;
        }
        boolean h2 = cf3Var6.h();
        if (h2) {
            iArr3 = cf3Var.f;
            iArr4 = cf3Var2.f;
        } else {
            bf3.j(cf3Var6.f, e3);
            bf3.e(e3, cf3Var.f, f);
            bf3.e(e3, cf3Var6.f, e3);
            bf3.e(e3, cf3Var2.f, e3);
            iArr3 = f;
            iArr4 = e3;
        }
        int[] e4 = dd2.e();
        bf3.m(iArr3, iArr, e4);
        bf3.m(iArr4, iArr2, e);
        if (dd2.l(e4)) {
            return dd2.l(e) ? J() : i.v();
        }
        bf3.j(e4, e2);
        int[] e5 = dd2.e();
        bf3.e(e2, e4, e5);
        bf3.e(e2, iArr3, e2);
        bf3.g(e5, e5);
        dd2.m(iArr4, e5, f);
        bf3.i(dd2.b(e2, e2, e5), e5);
        cf3 cf3Var7 = new cf3(e3);
        bf3.j(e, cf3Var7.f);
        int[] iArr5 = cf3Var7.f;
        bf3.m(iArr5, e5, iArr5);
        cf3 cf3Var8 = new cf3(e5);
        bf3.m(e2, cf3Var7.f, cf3Var8.f);
        bf3.f(cf3Var8.f, e, f);
        bf3.h(f, cf3Var8.f);
        cf3 cf3Var9 = new cf3(e4);
        if (!h) {
            int[] iArr6 = cf3Var9.f;
            bf3.e(iArr6, cf3Var5.f, iArr6);
        }
        if (!h2) {
            int[] iArr7 = cf3Var9.f;
            bf3.e(iArr7, cf3Var6.f, iArr7);
        }
        return new df3(i, cf3Var7, cf3Var8, new ct0[]{cf3Var9}, this.e);
    }

    @Override // defpackage.pt0
    public pt0 d() {
        return new df3(null, f(), g());
    }

    @Override // defpackage.pt0
    public pt0 z() {
        return u() ? this : new df3(this.a, this.b, this.c.m(), this.d, this.e);
    }
}
