package defpackage;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.nft.NFTData;

/* compiled from: NftDetailFragmentDirections.java */
/* renamed from: dg2  reason: default package */
/* loaded from: classes2.dex */
public class dg2 {

    /* compiled from: NftDetailFragmentDirections.java */
    /* renamed from: dg2$b */
    /* loaded from: classes2.dex */
    public static class b implements ce2 {
        public final HashMap a;

        @Override // defpackage.ce2
        public Bundle a() {
            Bundle bundle = new Bundle();
            if (this.a.containsKey("nftData")) {
                NFTData nFTData = (NFTData) this.a.get("nftData");
                if (!Parcelable.class.isAssignableFrom(NFTData.class) && nFTData != null) {
                    if (Serializable.class.isAssignableFrom(NFTData.class)) {
                        bundle.putSerializable("nftData", (Serializable) Serializable.class.cast(nFTData));
                    } else {
                        throw new UnsupportedOperationException(NFTData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                    }
                } else {
                    bundle.putParcelable("nftData", (Parcelable) Parcelable.class.cast(nFTData));
                }
            }
            return bundle;
        }

        @Override // defpackage.ce2
        public int b() {
            return R.id.action_nft_detail_to_sendto;
        }

        public NFTData c() {
            return (NFTData) this.a.get("nftData");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.containsKey("nftData") != bVar.a.containsKey("nftData")) {
                return false;
            }
            if (c() == null ? bVar.c() == null : c().equals(bVar.c())) {
                return b() == bVar.b();
            }
            return false;
        }

        public int hashCode() {
            return (((c() != null ? c().hashCode() : 0) + 31) * 31) + b();
        }

        public String toString() {
            return "ActionNftDetailToSendto(actionId=" + b() + "){nftData=" + c() + "}";
        }

        public b(NFTData nFTData) {
            HashMap hashMap = new HashMap();
            this.a = hashMap;
            if (nFTData != null) {
                hashMap.put("nftData", nFTData);
                return;
            }
            throw new IllegalArgumentException("Argument \"nftData\" is marked as non-null but was passed a null value.");
        }
    }

    public static b a(NFTData nFTData) {
        return new b(nFTData);
    }
}
