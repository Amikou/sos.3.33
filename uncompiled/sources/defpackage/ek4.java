package defpackage;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ViewTarget.java */
@Deprecated
/* renamed from: ek4  reason: default package */
/* loaded from: classes.dex */
public abstract class ek4<T extends View, Z> extends yn<Z> {
    public static int j0 = a03.glide_custom_view_target_tag;
    public final T a;
    public final a f0;
    public View.OnAttachStateChangeListener g0;
    public boolean h0;
    public boolean i0;

    /* compiled from: ViewTarget.java */
    /* renamed from: ek4$a */
    /* loaded from: classes.dex */
    public static final class a {
        public static Integer e;
        public final View a;
        public final List<dq3> b = new ArrayList();
        public boolean c;
        public ViewTreeObserver$OnPreDrawListenerC0168a d;

        /* compiled from: ViewTarget.java */
        /* renamed from: ek4$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class ViewTreeObserver$OnPreDrawListenerC0168a implements ViewTreeObserver.OnPreDrawListener {
            public final WeakReference<a> a;

            public ViewTreeObserver$OnPreDrawListenerC0168a(a aVar) {
                this.a = new WeakReference<>(aVar);
            }

            @Override // android.view.ViewTreeObserver.OnPreDrawListener
            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("OnGlobalLayoutListener called attachStateListener=");
                    sb.append(this);
                }
                a aVar = this.a.get();
                if (aVar != null) {
                    aVar.a();
                    return true;
                }
                return true;
            }
        }

        public a(View view) {
            this.a = view;
        }

        public static int c(Context context) {
            if (e == null) {
                Display defaultDisplay = ((WindowManager) wt2.d((WindowManager) context.getSystemService("window"))).getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        public void a() {
            if (this.b.isEmpty()) {
                return;
            }
            int g = g();
            int f = f();
            if (i(g, f)) {
                j(g, f);
                b();
            }
        }

        public void b() {
            ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        public void d(dq3 dq3Var) {
            int g = g();
            int f = f();
            if (i(g, f)) {
                dq3Var.f(g, f);
                return;
            }
            if (!this.b.contains(dq3Var)) {
                this.b.add(dq3Var);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
                ViewTreeObserver$OnPreDrawListenerC0168a viewTreeObserver$OnPreDrawListenerC0168a = new ViewTreeObserver$OnPreDrawListenerC0168a(this);
                this.d = viewTreeObserver$OnPreDrawListenerC0168a;
                viewTreeObserver.addOnPreDrawListener(viewTreeObserver$OnPreDrawListenerC0168a);
            }
        }

        public final int e(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            return c(this.a.getContext());
        }

        public final int f() {
            int paddingTop = this.a.getPaddingTop() + this.a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return e(this.a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        public final int g() {
            int paddingLeft = this.a.getPaddingLeft() + this.a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return e(this.a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        public final boolean h(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        public final boolean i(int i, int i2) {
            return h(i) && h(i2);
        }

        public final void j(int i, int i2) {
            Iterator it = new ArrayList(this.b).iterator();
            while (it.hasNext()) {
                ((dq3) it.next()).f(i, i2);
            }
        }

        public void k(dq3 dq3Var) {
            this.b.remove(dq3Var);
        }
    }

    public ek4(T t) {
        this.a = (T) wt2.d(t);
        this.f0 = new a(t);
    }

    @Override // defpackage.i34
    public void a(dq3 dq3Var) {
        this.f0.d(dq3Var);
    }

    @Override // defpackage.i34
    public void c(d73 d73Var) {
        o(d73Var);
    }

    public final Object d() {
        return this.a.getTag(j0);
    }

    @Override // defpackage.i34
    public void f(dq3 dq3Var) {
        this.f0.k(dq3Var);
    }

    public final void i() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.g0;
        if (onAttachStateChangeListener == null || this.i0) {
            return;
        }
        this.a.addOnAttachStateChangeListener(onAttachStateChangeListener);
        this.i0 = true;
    }

    @Override // defpackage.yn, defpackage.i34
    public void k(Drawable drawable) {
        super.k(drawable);
        i();
    }

    @Override // defpackage.i34
    public d73 l() {
        Object d = d();
        if (d != null) {
            if (d instanceof d73) {
                return (d73) d;
            }
            throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
        }
        return null;
    }

    @Override // defpackage.yn, defpackage.i34
    public void m(Drawable drawable) {
        super.m(drawable);
        this.f0.b();
        if (this.h0) {
            return;
        }
        n();
    }

    public final void n() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.g0;
        if (onAttachStateChangeListener == null || !this.i0) {
            return;
        }
        this.a.removeOnAttachStateChangeListener(onAttachStateChangeListener);
        this.i0 = false;
    }

    public final void o(Object obj) {
        this.a.setTag(j0, obj);
    }

    public String toString() {
        return "Target for: " + this.a;
    }
}
