package defpackage;

import com.google.android.gms.internal.measurement.zzbl;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: z75  reason: default package */
/* loaded from: classes.dex */
public final class z75 extends o65 {
    public z75() {
        this.a.add(zzbl.AND);
        this.a.add(zzbl.NOT);
        this.a.add(zzbl.OR);
    }

    @Override // defpackage.o65
    public final z55 a(String str, wk5 wk5Var, List<z55> list) {
        zzbl zzblVar = zzbl.ADD;
        int ordinal = vm5.e(str).ordinal();
        if (ordinal == 1) {
            vm5.a(zzbl.AND.name(), 2, list);
            z55 a = wk5Var.a(list.get(0));
            return !a.c().booleanValue() ? a : wk5Var.a(list.get(1));
        } else if (ordinal == 47) {
            vm5.a(zzbl.NOT.name(), 1, list);
            return new s45(Boolean.valueOf(!wk5Var.a(list.get(0)).c().booleanValue()));
        } else if (ordinal != 50) {
            return super.b(str);
        } else {
            vm5.a(zzbl.OR.name(), 2, list);
            z55 a2 = wk5Var.a(list.get(0));
            return a2.c().booleanValue() ? a2 : wk5Var.a(list.get(1));
        }
    }
}
