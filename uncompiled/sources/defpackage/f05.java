package defpackage;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: f05  reason: default package */
/* loaded from: classes.dex */
public final class f05 implements c25 {
    public final /* synthetic */ d05 a;

    public f05(d05 d05Var) {
        this.a = d05Var;
    }

    @Override // defpackage.c25
    public final boolean c() {
        return this.a.h();
    }

    @Override // defpackage.c25
    public final Bundle v() {
        return null;
    }
}
