package defpackage;

import com.facebook.common.memory.PooledByteBuffer;

/* compiled from: EncodedMemoryCacheFactory.java */
/* renamed from: av0  reason: default package */
/* loaded from: classes.dex */
public class av0 {

    /* compiled from: EncodedMemoryCacheFactory.java */
    /* renamed from: av0$a */
    /* loaded from: classes.dex */
    public static class a implements n72<wt> {
        public final /* synthetic */ qn1 a;

        public a(qn1 qn1Var) {
            this.a = qn1Var;
        }

        @Override // defpackage.n72
        /* renamed from: d */
        public void a(wt wtVar) {
            this.a.f(wtVar);
        }

        @Override // defpackage.n72
        /* renamed from: e */
        public void b(wt wtVar) {
            this.a.d(wtVar);
        }

        @Override // defpackage.n72
        /* renamed from: f */
        public void c(wt wtVar) {
            this.a.k(wtVar);
        }
    }

    public static jr1<wt, PooledByteBuffer> a(l72<wt, PooledByteBuffer> l72Var, qn1 qn1Var) {
        qn1Var.h(l72Var);
        return new jr1<>(l72Var, new a(qn1Var));
    }
}
