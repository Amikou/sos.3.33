package defpackage;

import java.util.Collections;
import java.util.List;

/* compiled from: SimpleProgressiveJpegConfig.java */
/* renamed from: np3  reason: default package */
/* loaded from: classes.dex */
public class np3 implements qv2 {
    public final c a;

    /* compiled from: SimpleProgressiveJpegConfig.java */
    /* renamed from: np3$b */
    /* loaded from: classes.dex */
    public static class b implements c {
        public b() {
        }

        @Override // defpackage.np3.c
        public List<Integer> a() {
            return Collections.EMPTY_LIST;
        }

        @Override // defpackage.np3.c
        public int b() {
            return 0;
        }
    }

    /* compiled from: SimpleProgressiveJpegConfig.java */
    /* renamed from: np3$c */
    /* loaded from: classes.dex */
    public interface c {
        List<Integer> a();

        int b();
    }

    public np3() {
        this(new b());
    }

    @Override // defpackage.qv2
    public xw2 a(int i) {
        return jp1.d(i, i >= this.a.b(), false);
    }

    @Override // defpackage.qv2
    public int b(int i) {
        List<Integer> a2 = this.a.a();
        if (a2 == null || a2.isEmpty()) {
            return i + 1;
        }
        for (int i2 = 0; i2 < a2.size(); i2++) {
            if (a2.get(i2).intValue() > i) {
                return a2.get(i2).intValue();
            }
        }
        return Integer.MAX_VALUE;
    }

    public np3(c cVar) {
        this.a = (c) xt2.g(cVar);
    }
}
