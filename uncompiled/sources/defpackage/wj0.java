package defpackage;

/* compiled from: DefaultImageFormats.java */
/* renamed from: wj0  reason: default package */
/* loaded from: classes.dex */
public final class wj0 {
    public static final wn1 a = new wn1("JPEG", "jpeg");
    public static final wn1 b = new wn1("PNG", "png");
    public static final wn1 c = new wn1("GIF", "gif");
    public static final wn1 d = new wn1("BMP", "bmp");
    public static final wn1 e = new wn1("ICO", "ico");
    public static final wn1 f = new wn1("WEBP_SIMPLE", "webp");
    public static final wn1 g = new wn1("WEBP_LOSSLESS", "webp");
    public static final wn1 h = new wn1("WEBP_EXTENDED", "webp");
    public static final wn1 i = new wn1("WEBP_EXTENDED_WITH_ALPHA", "webp");
    public static final wn1 j = new wn1("WEBP_ANIMATED", "webp");
    public static final wn1 k = new wn1("HEIF", "heif");
    public static final wn1 l = new wn1("DNG", "dng");

    public static boolean a(wn1 wn1Var) {
        return wn1Var == f || wn1Var == g || wn1Var == h || wn1Var == i;
    }

    public static boolean b(wn1 wn1Var) {
        return a(wn1Var) || wn1Var == j;
    }
}
