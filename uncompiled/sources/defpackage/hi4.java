package defpackage;

import android.view.View;
import androidx.constraintlayout.helper.widget.Flow;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: ViewCustomKeyboardBinding.java */
/* renamed from: hi4  reason: default package */
/* loaded from: classes2.dex */
public final class hi4 {
    public final MaterialButton a;
    public final MaterialButton b;

    public hi4(View view, MaterialButton materialButton, MaterialButton materialButton2, MaterialButton materialButton3, MaterialButton materialButton4, MaterialButton materialButton5, MaterialButton materialButton6, MaterialButton materialButton7, MaterialButton materialButton8, MaterialButton materialButton9, MaterialButton materialButton10, MaterialButton materialButton11, MaterialButton materialButton12, MaterialButton materialButton13, MaterialButton materialButton14, MaterialButton materialButton15, MaterialButton materialButton16, Flow flow) {
        this.a = materialButton12;
        this.b = materialButton13;
    }

    public static hi4 a(View view) {
        int i = R.id.btn_0;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btn_0);
        if (materialButton != null) {
            i = R.id.btn_1;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btn_1);
            if (materialButton2 != null) {
                i = R.id.btn_2;
                MaterialButton materialButton3 = (MaterialButton) ai4.a(view, R.id.btn_2);
                if (materialButton3 != null) {
                    i = R.id.btn_3;
                    MaterialButton materialButton4 = (MaterialButton) ai4.a(view, R.id.btn_3);
                    if (materialButton4 != null) {
                        i = R.id.btn_4;
                        MaterialButton materialButton5 = (MaterialButton) ai4.a(view, R.id.btn_4);
                        if (materialButton5 != null) {
                            i = R.id.btn_5;
                            MaterialButton materialButton6 = (MaterialButton) ai4.a(view, R.id.btn_5);
                            if (materialButton6 != null) {
                                i = R.id.btn_6;
                                MaterialButton materialButton7 = (MaterialButton) ai4.a(view, R.id.btn_6);
                                if (materialButton7 != null) {
                                    i = R.id.btn_7;
                                    MaterialButton materialButton8 = (MaterialButton) ai4.a(view, R.id.btn_7);
                                    if (materialButton8 != null) {
                                        i = R.id.btn_8;
                                        MaterialButton materialButton9 = (MaterialButton) ai4.a(view, R.id.btn_8);
                                        if (materialButton9 != null) {
                                            i = R.id.btn_9;
                                            MaterialButton materialButton10 = (MaterialButton) ai4.a(view, R.id.btn_9);
                                            if (materialButton10 != null) {
                                                i = R.id.btn_comma;
                                                MaterialButton materialButton11 = (MaterialButton) ai4.a(view, R.id.btn_comma);
                                                if (materialButton11 != null) {
                                                    i = R.id.btn_delete;
                                                    MaterialButton materialButton12 = (MaterialButton) ai4.a(view, R.id.btn_delete);
                                                    if (materialButton12 != null) {
                                                        i = R.id.btn_done;
                                                        MaterialButton materialButton13 = (MaterialButton) ai4.a(view, R.id.btn_done);
                                                        if (materialButton13 != null) {
                                                            i = R.id.btn_dot;
                                                            MaterialButton materialButton14 = (MaterialButton) ai4.a(view, R.id.btn_dot);
                                                            if (materialButton14 != null) {
                                                                i = R.id.btn_fake_1;
                                                                MaterialButton materialButton15 = (MaterialButton) ai4.a(view, R.id.btn_fake_1);
                                                                if (materialButton15 != null) {
                                                                    i = R.id.btn_fake_2;
                                                                    MaterialButton materialButton16 = (MaterialButton) ai4.a(view, R.id.btn_fake_2);
                                                                    if (materialButton16 != null) {
                                                                        i = R.id.flow;
                                                                        Flow flow = (Flow) ai4.a(view, R.id.flow);
                                                                        if (flow != null) {
                                                                            return new hi4(view, materialButton, materialButton2, materialButton3, materialButton4, materialButton5, materialButton6, materialButton7, materialButton8, materialButton9, materialButton10, materialButton11, materialButton12, materialButton13, materialButton14, materialButton15, materialButton16, flow);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
