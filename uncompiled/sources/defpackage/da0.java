package defpackage;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: da0  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class da0 implements FilenameFilter {
    public static final /* synthetic */ da0 a = new da0();

    @Override // java.io.FilenameFilter
    public final boolean accept(File file, String str) {
        boolean startsWith;
        startsWith = str.startsWith("event");
        return startsWith;
    }
}
