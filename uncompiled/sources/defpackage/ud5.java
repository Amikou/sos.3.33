package defpackage;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@19.0.0 */
/* renamed from: ud5  reason: default package */
/* loaded from: classes.dex */
public final class ud5 implements ThreadFactory {
    public final ThreadFactory a = Executors.defaultThreadFactory();

    public ud5(wf5 wf5Var) {
    }

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.a.newThread(runnable);
        newThread.setName("ScionFrontendApi");
        return newThread;
    }
}
