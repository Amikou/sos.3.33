package defpackage;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Layout;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import androidx.media3.common.e;

/* compiled from: Cue.java */
/* renamed from: kb0  reason: default package */
/* loaded from: classes.dex */
public final class kb0 implements e {
    public static final kb0 v0 = new b().o("").a();
    public static final e.a<kb0> w0 = jb0.a;
    public final CharSequence a;
    public final Layout.Alignment f0;
    public final Layout.Alignment g0;
    public final Bitmap h0;
    public final float i0;
    public final int j0;
    public final int k0;
    public final float l0;
    public final int m0;
    public final float n0;
    public final float o0;
    public final boolean p0;
    public final int q0;
    public final int r0;
    public final float s0;
    public final int t0;
    public final float u0;

    /* compiled from: Cue.java */
    /* renamed from: kb0$b */
    /* loaded from: classes.dex */
    public static final class b {
        public CharSequence a;
        public Bitmap b;
        public Layout.Alignment c;
        public Layout.Alignment d;
        public float e;
        public int f;
        public int g;
        public float h;
        public int i;
        public int j;
        public float k;
        public float l;
        public float m;
        public boolean n;
        public int o;
        public int p;
        public float q;

        public kb0 a() {
            return new kb0(this.a, this.c, this.d, this.b, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q);
        }

        public b b() {
            this.n = false;
            return this;
        }

        public int c() {
            return this.g;
        }

        public int d() {
            return this.i;
        }

        public CharSequence e() {
            return this.a;
        }

        public b f(Bitmap bitmap) {
            this.b = bitmap;
            return this;
        }

        public b g(float f) {
            this.m = f;
            return this;
        }

        public b h(float f, int i) {
            this.e = f;
            this.f = i;
            return this;
        }

        public b i(int i) {
            this.g = i;
            return this;
        }

        public b j(Layout.Alignment alignment) {
            this.d = alignment;
            return this;
        }

        public b k(float f) {
            this.h = f;
            return this;
        }

        public b l(int i) {
            this.i = i;
            return this;
        }

        public b m(float f) {
            this.q = f;
            return this;
        }

        public b n(float f) {
            this.l = f;
            return this;
        }

        public b o(CharSequence charSequence) {
            this.a = charSequence;
            return this;
        }

        public b p(Layout.Alignment alignment) {
            this.c = alignment;
            return this;
        }

        public b q(float f, int i) {
            this.k = f;
            this.j = i;
            return this;
        }

        public b r(int i) {
            this.p = i;
            return this;
        }

        public b s(int i) {
            this.o = i;
            this.n = true;
            return this;
        }

        public b() {
            this.a = null;
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = -3.4028235E38f;
            this.f = Integer.MIN_VALUE;
            this.g = Integer.MIN_VALUE;
            this.h = -3.4028235E38f;
            this.i = Integer.MIN_VALUE;
            this.j = Integer.MIN_VALUE;
            this.k = -3.4028235E38f;
            this.l = -3.4028235E38f;
            this.m = -3.4028235E38f;
            this.n = false;
            this.o = -16777216;
            this.p = Integer.MIN_VALUE;
        }

        public b(kb0 kb0Var) {
            this.a = kb0Var.a;
            this.b = kb0Var.h0;
            this.c = kb0Var.f0;
            this.d = kb0Var.g0;
            this.e = kb0Var.i0;
            this.f = kb0Var.j0;
            this.g = kb0Var.k0;
            this.h = kb0Var.l0;
            this.i = kb0Var.m0;
            this.j = kb0Var.r0;
            this.k = kb0Var.s0;
            this.l = kb0Var.n0;
            this.m = kb0Var.o0;
            this.n = kb0Var.p0;
            this.o = kb0Var.q0;
            this.p = kb0Var.t0;
            this.q = kb0Var.u0;
        }
    }

    public static final kb0 c(Bundle bundle) {
        b bVar = new b();
        CharSequence charSequence = bundle.getCharSequence(d(0));
        if (charSequence != null) {
            bVar.o(charSequence);
        }
        Layout.Alignment alignment = (Layout.Alignment) bundle.getSerializable(d(1));
        if (alignment != null) {
            bVar.p(alignment);
        }
        Layout.Alignment alignment2 = (Layout.Alignment) bundle.getSerializable(d(2));
        if (alignment2 != null) {
            bVar.j(alignment2);
        }
        Bitmap bitmap = (Bitmap) bundle.getParcelable(d(3));
        if (bitmap != null) {
            bVar.f(bitmap);
        }
        if (bundle.containsKey(d(4)) && bundle.containsKey(d(5))) {
            bVar.h(bundle.getFloat(d(4)), bundle.getInt(d(5)));
        }
        if (bundle.containsKey(d(6))) {
            bVar.i(bundle.getInt(d(6)));
        }
        if (bundle.containsKey(d(7))) {
            bVar.k(bundle.getFloat(d(7)));
        }
        if (bundle.containsKey(d(8))) {
            bVar.l(bundle.getInt(d(8)));
        }
        if (bundle.containsKey(d(10)) && bundle.containsKey(d(9))) {
            bVar.q(bundle.getFloat(d(10)), bundle.getInt(d(9)));
        }
        if (bundle.containsKey(d(11))) {
            bVar.n(bundle.getFloat(d(11)));
        }
        if (bundle.containsKey(d(12))) {
            bVar.g(bundle.getFloat(d(12)));
        }
        if (bundle.containsKey(d(13))) {
            bVar.s(bundle.getInt(d(13)));
        }
        if (!bundle.getBoolean(d(14), false)) {
            bVar.b();
        }
        if (bundle.containsKey(d(15))) {
            bVar.r(bundle.getInt(d(15)));
        }
        if (bundle.containsKey(d(16))) {
            bVar.m(bundle.getFloat(d(16)));
        }
        return bVar.a();
    }

    public static String d(int i) {
        return Integer.toString(i, 36);
    }

    public b b() {
        return new b();
    }

    public boolean equals(Object obj) {
        Bitmap bitmap;
        Bitmap bitmap2;
        if (this == obj) {
            return true;
        }
        if (obj == null || kb0.class != obj.getClass()) {
            return false;
        }
        kb0 kb0Var = (kb0) obj;
        return TextUtils.equals(this.a, kb0Var.a) && this.f0 == kb0Var.f0 && this.g0 == kb0Var.g0 && ((bitmap = this.h0) != null ? !((bitmap2 = kb0Var.h0) == null || !bitmap.sameAs(bitmap2)) : kb0Var.h0 == null) && this.i0 == kb0Var.i0 && this.j0 == kb0Var.j0 && this.k0 == kb0Var.k0 && this.l0 == kb0Var.l0 && this.m0 == kb0Var.m0 && this.n0 == kb0Var.n0 && this.o0 == kb0Var.o0 && this.p0 == kb0Var.p0 && this.q0 == kb0Var.q0 && this.r0 == kb0Var.r0 && this.s0 == kb0Var.s0 && this.t0 == kb0Var.t0 && this.u0 == kb0Var.u0;
    }

    public int hashCode() {
        return ql2.b(this.a, this.f0, this.g0, this.h0, Float.valueOf(this.i0), Integer.valueOf(this.j0), Integer.valueOf(this.k0), Float.valueOf(this.l0), Integer.valueOf(this.m0), Float.valueOf(this.n0), Float.valueOf(this.o0), Boolean.valueOf(this.p0), Integer.valueOf(this.q0), Integer.valueOf(this.r0), Float.valueOf(this.s0), Integer.valueOf(this.t0), Float.valueOf(this.u0));
    }

    @Override // androidx.media3.common.e
    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putCharSequence(d(0), this.a);
        bundle.putSerializable(d(1), this.f0);
        bundle.putSerializable(d(2), this.g0);
        bundle.putParcelable(d(3), this.h0);
        bundle.putFloat(d(4), this.i0);
        bundle.putInt(d(5), this.j0);
        bundle.putInt(d(6), this.k0);
        bundle.putFloat(d(7), this.l0);
        bundle.putInt(d(8), this.m0);
        bundle.putInt(d(9), this.r0);
        bundle.putFloat(d(10), this.s0);
        bundle.putFloat(d(11), this.n0);
        bundle.putFloat(d(12), this.o0);
        bundle.putBoolean(d(14), this.p0);
        bundle.putInt(d(13), this.q0);
        bundle.putInt(d(15), this.t0);
        bundle.putFloat(d(16), this.u0);
        return bundle;
    }

    public kb0(CharSequence charSequence, Layout.Alignment alignment, Layout.Alignment alignment2, Bitmap bitmap, float f, int i, int i2, float f2, int i3, int i4, float f3, float f4, float f5, boolean z, int i5, int i6, float f6) {
        if (charSequence == null) {
            ii.e(bitmap);
        } else {
            ii.a(bitmap == null);
        }
        if (charSequence instanceof Spanned) {
            this.a = SpannedString.valueOf(charSequence);
        } else if (charSequence != null) {
            this.a = charSequence.toString();
        } else {
            this.a = null;
        }
        this.f0 = alignment;
        this.g0 = alignment2;
        this.h0 = bitmap;
        this.i0 = f;
        this.j0 = i;
        this.k0 = i2;
        this.l0 = f2;
        this.m0 = i3;
        this.n0 = f4;
        this.o0 = f5;
        this.p0 = z;
        this.q0 = i5;
        this.r0 = i4;
        this.s0 = f3;
        this.t0 = i6;
        this.u0 = f6;
    }
}
