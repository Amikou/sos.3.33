package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: mo5  reason: default package */
/* loaded from: classes.dex */
public final class mo5 extends wo5<Double> {
    public mo5(ro5 ro5Var, String str, Double d, boolean z) {
        super(ro5Var, "measurement.test.double_flag", d, true, null);
    }

    @Override // defpackage.wo5
    public final /* bridge */ /* synthetic */ Double a(Object obj) {
        try {
            return Double.valueOf(Double.parseDouble((String) obj));
        } catch (NumberFormatException unused) {
            String d = super.d();
            String str = (String) obj;
            StringBuilder sb = new StringBuilder(String.valueOf(d).length() + 27 + str.length());
            sb.append("Invalid double value for ");
            sb.append(d);
            sb.append(": ");
            sb.append(str);
            return null;
        }
    }
}
