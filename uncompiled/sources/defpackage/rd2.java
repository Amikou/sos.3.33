package defpackage;

import com.facebook.soloader.SoLoader;

/* compiled from: NativeLoaderToSoLoaderDelegate.java */
/* renamed from: rd2  reason: default package */
/* loaded from: classes.dex */
public class rd2 implements qd2 {
    @Override // defpackage.qd2
    public boolean a(String str, int i) {
        return SoLoader.i(str, ((i & 1) != 0 ? 16 : 0) | 0);
    }
}
