package defpackage;

import com.google.android.material.textfield.TextInputLayout;

/* compiled from: CustomEndIconDelegate.java */
/* renamed from: fc0  reason: default package */
/* loaded from: classes2.dex */
public class fc0 extends kv0 {
    public fc0(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @Override // defpackage.kv0
    public void a() {
        this.a.setEndIconOnClickListener(null);
        this.a.setEndIconOnLongClickListener(null);
    }
}
