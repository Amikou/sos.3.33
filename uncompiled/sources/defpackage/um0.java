package defpackage;

import com.google.crypto.tink.d;
import com.google.crypto.tink.o;
import com.google.crypto.tink.p;
import com.google.crypto.tink.q;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.logging.Logger;

/* compiled from: DeterministicAeadWrapper.java */
/* renamed from: um0  reason: default package */
/* loaded from: classes2.dex */
public class um0 implements p<d, d> {
    public static final Logger a = Logger.getLogger(um0.class.getName());

    /* compiled from: DeterministicAeadWrapper.java */
    /* renamed from: um0$a */
    /* loaded from: classes2.dex */
    public static class a implements d {
        public o<d> a;

        public a(o<d> oVar) {
            this.a = oVar;
        }

        @Override // com.google.crypto.tink.d
        public byte[] a(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            return at.a(this.a.b().a(), this.a.b().c().a(bArr, bArr2));
        }

        @Override // com.google.crypto.tink.d
        public byte[] b(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            if (bArr.length > 5) {
                byte[] copyOfRange = Arrays.copyOfRange(bArr, 0, 5);
                byte[] copyOfRange2 = Arrays.copyOfRange(bArr, 5, bArr.length);
                for (o.b<d> bVar : this.a.c(copyOfRange)) {
                    try {
                        return bVar.c().b(copyOfRange2, bArr2);
                    } catch (GeneralSecurityException e) {
                        Logger logger = um0.a;
                        logger.info("ciphertext prefix matches a key, but cannot decrypt: " + e.toString());
                    }
                }
            }
            for (o.b<d> bVar2 : this.a.e()) {
                try {
                    return bVar2.c().b(bArr, bArr2);
                } catch (GeneralSecurityException unused) {
                }
            }
            throw new GeneralSecurityException("decryption failed");
        }
    }

    public static void e() throws GeneralSecurityException {
        q.r(new um0());
    }

    @Override // com.google.crypto.tink.p
    public Class<d> a() {
        return d.class;
    }

    @Override // com.google.crypto.tink.p
    public Class<d> b() {
        return d.class;
    }

    @Override // com.google.crypto.tink.p
    /* renamed from: f */
    public d c(o<d> oVar) {
        return new a(oVar);
    }
}
