package defpackage;

import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.button.MaterialButton;
import net.safemoon.androidwallet.R;

/* compiled from: DialogAnchorCollectionOptionBinding.java */
/* renamed from: cn0  reason: default package */
/* loaded from: classes2.dex */
public final class cn0 {
    public final MaterialButton a;
    public final MaterialButton b;

    public cn0(ConstraintLayout constraintLayout, MaterialButton materialButton, MaterialButton materialButton2, AppCompatTextView appCompatTextView) {
        this.a = materialButton;
        this.b = materialButton2;
    }

    public static cn0 a(View view) {
        int i = R.id.btnHiddenCollections;
        MaterialButton materialButton = (MaterialButton) ai4.a(view, R.id.btnHiddenCollections);
        if (materialButton != null) {
            i = R.id.btnHideCollections;
            MaterialButton materialButton2 = (MaterialButton) ai4.a(view, R.id.btnHideCollections);
            if (materialButton2 != null) {
                i = R.id.txtTitle;
                AppCompatTextView appCompatTextView = (AppCompatTextView) ai4.a(view, R.id.txtTitle);
                if (appCompatTextView != null) {
                    return new cn0((ConstraintLayout) view, materialButton, materialButton2, appCompatTextView);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
