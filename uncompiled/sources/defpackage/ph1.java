package defpackage;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.RecentlyNonNull;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: ph1  reason: default package */
/* loaded from: classes.dex */
public final class ph1 extends qh1 {
    @RecentlyNonNull
    public static Resources e(@RecentlyNonNull Context context) {
        return qh1.e(context);
    }
}
