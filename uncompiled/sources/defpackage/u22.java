package defpackage;

import android.os.SystemClock;
import defpackage.i90;
import defpackage.l72;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.WeakHashMap;

/* compiled from: LruCountingMemoryCache.java */
/* renamed from: u22  reason: default package */
/* loaded from: classes.dex */
public class u22<K, V> implements i90<K, V>, l72<K, V> {
    public final i90.b<K> a;
    public final h90<K, i90.a<K, V>> b;
    public final h90<K, i90.a<K, V>> c;
    public final yg4<V> d;
    public final fw3<m72> e;
    public m72 f;
    public long g;
    public final boolean h;
    public final boolean i;

    /* compiled from: LruCountingMemoryCache.java */
    /* renamed from: u22$a */
    /* loaded from: classes.dex */
    public class a implements yg4<i90.a<K, V>> {
        public final /* synthetic */ yg4 a;

        public a(yg4 yg4Var) {
            this.a = yg4Var;
        }

        @Override // defpackage.yg4
        /* renamed from: b */
        public int a(i90.a<K, V> aVar) {
            if (u22.this.h) {
                return aVar.f;
            }
            return this.a.a(aVar.b.j());
        }
    }

    /* compiled from: LruCountingMemoryCache.java */
    /* renamed from: u22$b */
    /* loaded from: classes.dex */
    public class b implements d83<V> {
        public final /* synthetic */ i90.a a;

        public b(i90.a aVar) {
            this.a = aVar;
        }

        @Override // defpackage.d83
        public void a(V v) {
            u22.this.x(this.a);
        }
    }

    public u22(yg4<V> yg4Var, l72.a aVar, fw3<m72> fw3Var, i90.b<K> bVar, boolean z, boolean z2) {
        new WeakHashMap();
        this.d = yg4Var;
        this.b = new h90<>(z(yg4Var));
        this.c = new h90<>(z(yg4Var));
        this.e = fw3Var;
        this.f = (m72) xt2.h(fw3Var.get(), "mMemoryCacheParamsSupplier returned null");
        this.g = SystemClock.uptimeMillis();
        this.a = bVar;
        this.h = z;
        this.i = z2;
    }

    public static <K, V> void r(i90.a<K, V> aVar) {
        i90.b<K> bVar;
        if (aVar == null || (bVar = aVar.e) == null) {
            return;
        }
        bVar.a(aVar.a, true);
    }

    public static <K, V> void s(i90.a<K, V> aVar) {
        i90.b<K> bVar;
        if (aVar == null || (bVar = aVar.e) == null) {
            return;
        }
        bVar.a(aVar.a, false);
    }

    @Override // defpackage.l72
    public void b(K k) {
        xt2.g(k);
        synchronized (this) {
            i90.a<K, V> h = this.b.h(k);
            if (h != null) {
                this.b.g(k, h);
            }
        }
    }

    @Override // defpackage.l72
    public com.facebook.common.references.a<V> c(K k, com.facebook.common.references.a<V> aVar) {
        return e(k, aVar, this.a);
    }

    @Override // defpackage.l72
    public synchronized boolean contains(K k) {
        return this.c.a(k);
    }

    @Override // defpackage.i90
    public com.facebook.common.references.a<V> d(K k) {
        i90.a<K, V> h;
        boolean z;
        com.facebook.common.references.a<V> aVar;
        xt2.g(k);
        synchronized (this) {
            h = this.b.h(k);
            z = true;
            if (h != null) {
                i90.a<K, V> h2 = this.c.h(k);
                xt2.g(h2);
                xt2.i(h2.c == 0);
                aVar = h2.b;
            } else {
                aVar = null;
                z = false;
            }
        }
        if (z) {
            s(h);
        }
        return aVar;
    }

    @Override // defpackage.i90
    public com.facebook.common.references.a<V> e(K k, com.facebook.common.references.a<V> aVar, i90.b<K> bVar) {
        i90.a<K, V> h;
        com.facebook.common.references.a<V> aVar2;
        com.facebook.common.references.a<V> aVar3;
        i90.a<K, V> b2;
        xt2.g(k);
        xt2.g(aVar);
        u();
        synchronized (this) {
            h = this.b.h(k);
            i90.a<K, V> h2 = this.c.h(k);
            aVar2 = null;
            if (h2 != null) {
                m(h2);
                aVar3 = w(h2);
            } else {
                aVar3 = null;
            }
            int a2 = this.d.a(aVar.j());
            if (h(a2)) {
                if (this.h) {
                    b2 = i90.a.a(k, aVar, a2, bVar);
                } else {
                    b2 = i90.a.b(k, aVar, bVar);
                }
                this.c.g(k, b2);
                aVar2 = v(b2);
            }
        }
        com.facebook.common.references.a.g(aVar3);
        s(h);
        q();
        return aVar2;
    }

    @Override // defpackage.l72
    public com.facebook.common.references.a<V> get(K k) {
        i90.a<K, V> h;
        com.facebook.common.references.a<V> v;
        xt2.g(k);
        synchronized (this) {
            h = this.b.h(k);
            i90.a<K, V> b2 = this.c.b(k);
            v = b2 != null ? v(b2) : null;
        }
        s(h);
        u();
        q();
        return v;
    }

    /* JADX WARN: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (k() <= (r3.f.a - r4)) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final synchronized boolean h(int r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            m72 r0 = r3.f     // Catch: java.lang.Throwable -> L22
            int r0 = r0.e     // Catch: java.lang.Throwable -> L22
            r1 = 1
            if (r4 > r0) goto L1f
            int r0 = r3.j()     // Catch: java.lang.Throwable -> L22
            m72 r2 = r3.f     // Catch: java.lang.Throwable -> L22
            int r2 = r2.b     // Catch: java.lang.Throwable -> L22
            int r2 = r2 - r1
            if (r0 > r2) goto L1f
            int r0 = r3.k()     // Catch: java.lang.Throwable -> L22
            m72 r2 = r3.f     // Catch: java.lang.Throwable -> L22
            int r2 = r2.a     // Catch: java.lang.Throwable -> L22
            int r2 = r2 - r4
            if (r0 > r2) goto L1f
            goto L20
        L1f:
            r1 = 0
        L20:
            monitor-exit(r3)
            return r1
        L22:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.u22.h(int):boolean");
    }

    public final synchronized void i(i90.a<K, V> aVar) {
        xt2.g(aVar);
        xt2.i(aVar.c > 0);
        aVar.c--;
    }

    public synchronized int j() {
        return this.c.c() - this.b.c();
    }

    public synchronized int k() {
        return this.c.e() - this.b.e();
    }

    public final synchronized void l(i90.a<K, V> aVar) {
        xt2.g(aVar);
        xt2.i(!aVar.d);
        aVar.c++;
    }

    public final synchronized void m(i90.a<K, V> aVar) {
        xt2.g(aVar);
        xt2.i(!aVar.d);
        aVar.d = true;
    }

    public final synchronized void n(ArrayList<i90.a<K, V>> arrayList) {
        if (arrayList != null) {
            Iterator<i90.a<K, V>> it = arrayList.iterator();
            while (it.hasNext()) {
                m(it.next());
            }
        }
    }

    public final synchronized boolean o(i90.a<K, V> aVar) {
        if (aVar.d || aVar.c != 0) {
            return false;
        }
        this.b.g(aVar.a, aVar);
        return true;
    }

    public final void p(ArrayList<i90.a<K, V>> arrayList) {
        if (arrayList != null) {
            Iterator<i90.a<K, V>> it = arrayList.iterator();
            while (it.hasNext()) {
                com.facebook.common.references.a.g(w(it.next()));
            }
        }
    }

    public void q() {
        ArrayList<i90.a<K, V>> y;
        synchronized (this) {
            m72 m72Var = this.f;
            int min = Math.min(m72Var.d, m72Var.b - j());
            m72 m72Var2 = this.f;
            y = y(min, Math.min(m72Var2.c, m72Var2.a - k()));
            n(y);
        }
        p(y);
        t(y);
    }

    public final void t(ArrayList<i90.a<K, V>> arrayList) {
        if (arrayList != null) {
            Iterator<i90.a<K, V>> it = arrayList.iterator();
            while (it.hasNext()) {
                s(it.next());
            }
        }
    }

    public final synchronized void u() {
        if (this.g + this.f.f > SystemClock.uptimeMillis()) {
            return;
        }
        this.g = SystemClock.uptimeMillis();
        this.f = (m72) xt2.h(this.e.get(), "mMemoryCacheParamsSupplier returned null");
    }

    public final synchronized com.facebook.common.references.a<V> v(i90.a<K, V> aVar) {
        l(aVar);
        return com.facebook.common.references.a.A(aVar.b.j(), new b(aVar));
    }

    public final synchronized com.facebook.common.references.a<V> w(i90.a<K, V> aVar) {
        xt2.g(aVar);
        return (aVar.d && aVar.c == 0) ? aVar.b : null;
    }

    public final void x(i90.a<K, V> aVar) {
        boolean o;
        com.facebook.common.references.a<V> w;
        xt2.g(aVar);
        synchronized (this) {
            i(aVar);
            o = o(aVar);
            w = w(aVar);
        }
        com.facebook.common.references.a.g(w);
        if (!o) {
            aVar = null;
        }
        r(aVar);
        u();
        q();
    }

    public final synchronized ArrayList<i90.a<K, V>> y(int i, int i2) {
        int max = Math.max(i, 0);
        int max2 = Math.max(i2, 0);
        if (this.b.c() > max || this.b.e() > max2) {
            ArrayList<i90.a<K, V>> arrayList = new ArrayList<>();
            while (true) {
                if (this.b.c() <= max && this.b.e() <= max2) {
                    break;
                }
                K d = this.b.d();
                if (d == null) {
                    if (this.i) {
                        this.b.i();
                    } else {
                        throw new IllegalStateException(String.format("key is null, but exclusiveEntries count: %d, size: %d", Integer.valueOf(this.b.c()), Integer.valueOf(this.b.e())));
                    }
                } else {
                    this.b.h(d);
                    arrayList.add(this.c.h(d));
                }
            }
            return arrayList;
        }
        return null;
    }

    public final yg4<i90.a<K, V>> z(yg4<V> yg4Var) {
        return new a(yg4Var);
    }
}
