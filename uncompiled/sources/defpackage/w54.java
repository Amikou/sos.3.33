package defpackage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: TileDrawer.kt */
/* renamed from: w54  reason: default package */
/* loaded from: classes2.dex */
public final class w54 {
    public final hd1<Path, x54, te4> a;
    public final Path b;

    /* JADX WARN: Multi-variable type inference failed */
    public w54(hd1<? super Path, ? super x54, te4> hd1Var) {
        fs1.f(hd1Var, "drawer");
        this.a = hd1Var;
        this.b = new Path();
    }

    public final void a(Canvas canvas, x54 x54Var, int i, Paint paint, Paint paint2) {
        fs1.f(canvas, "canvas");
        fs1.f(x54Var, "measures");
        fs1.f(paint, "bgPaint");
        fs1.f(paint2, "fgPaint");
        canvas.drawRect(Utils.FLOAT_EPSILON, Utils.FLOAT_EPSILON, x54Var.m(), x54Var.f(), paint);
        this.b.reset();
        int i2 = i % 360;
        if (i2 != 0) {
            canvas.save();
            canvas.rotate(i2, x54Var.l(), x54Var.e());
            this.a.invoke(this.b, x54Var);
            canvas.drawPath(this.b, paint2);
            canvas.restore();
            return;
        }
        this.a.invoke(this.b, x54Var);
        canvas.drawPath(this.b, paint2);
    }
}
