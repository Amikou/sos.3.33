package defpackage;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/* renamed from: il4  reason: default package */
/* loaded from: classes2.dex */
public final class il4 implements es4 {
    public static final Map<String, il4> b;
    public final String a;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(a("SHA-256", 32, 16, 67), new il4(16777217, "WOTSP_SHA2-256_W16"));
        hashMap.put(a("SHA-512", 64, 16, 131), new il4(33554434, "WOTSP_SHA2-512_W16"));
        hashMap.put(a("SHAKE128", 32, 16, 67), new il4(50331651, "WOTSP_SHAKE128_W16"));
        hashMap.put(a("SHAKE256", 64, 16, 131), new il4(67108868, "WOTSP_SHAKE256_W16"));
        b = Collections.unmodifiableMap(hashMap);
    }

    public il4(int i, String str) {
        this.a = str;
    }

    public static String a(String str, int i, int i2, int i3) {
        Objects.requireNonNull(str, "algorithmName == null");
        return str + "-" + i + "-" + i2 + "-" + i3;
    }

    public static il4 b(String str, int i, int i2, int i3) {
        Objects.requireNonNull(str, "algorithmName == null");
        return b.get(a(str, i, i2, i3));
    }

    public String toString() {
        return this.a;
    }
}
