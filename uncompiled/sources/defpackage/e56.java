package defpackage;

import com.google.android.gms.internal.firebase_messaging.i;
import com.google.android.gms.internal.firebase_messaging.zzy;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: e56  reason: default package */
/* loaded from: classes.dex */
public final class e56 {
    public int a;
    public final zzy b = zzy.DEFAULT;

    public final e56 a(int i) {
        this.a = i;
        return this;
    }

    public final i b() {
        return new z46(this.a, this.b);
    }
}
