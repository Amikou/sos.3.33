package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.g;
import androidx.recyclerview.widget.o;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.button.MaterialButton;
import defpackage.g53;
import net.safemoon.androidwallet.R;
import net.safemoon.androidwallet.model.reflections.RoomReflectionsToken;

/* compiled from: ReflectionTokenAdapter.kt */
/* renamed from: g53  reason: default package */
/* loaded from: classes2.dex */
public final class g53 extends o<RoomReflectionsToken, b> {
    public tc1<? super RoomReflectionsToken, te4> a;
    public hd1<? super RoomReflectionsToken, ? super Button, te4> b;

    /* compiled from: ReflectionTokenAdapter.kt */
    /* renamed from: g53$a */
    /* loaded from: classes2.dex */
    public static final class a extends g.f<RoomReflectionsToken> {
        public static final a a = new a();

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: a */
        public boolean areContentsTheSame(RoomReflectionsToken roomReflectionsToken, RoomReflectionsToken roomReflectionsToken2) {
            fs1.f(roomReflectionsToken, "oldItem");
            fs1.f(roomReflectionsToken2, "newItem");
            return roomReflectionsToken.getEnableAdvanceMode() == roomReflectionsToken2.getEnableAdvanceMode() && fs1.b(roomReflectionsToken.getDifferenceBalance(), roomReflectionsToken2.getDifferenceBalance());
        }

        @Override // androidx.recyclerview.widget.g.f
        /* renamed from: b */
        public boolean areItemsTheSame(RoomReflectionsToken roomReflectionsToken, RoomReflectionsToken roomReflectionsToken2) {
            fs1.f(roomReflectionsToken, "oldItem");
            fs1.f(roomReflectionsToken2, "newItem");
            return fs1.b(roomReflectionsToken.getId(), roomReflectionsToken2.getId());
        }
    }

    /* compiled from: ReflectionTokenAdapter.kt */
    /* renamed from: g53$b */
    /* loaded from: classes2.dex */
    public final class b extends RecyclerView.a0 {
        public final yk1 a;
        public final /* synthetic */ g53 b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(g53 g53Var, yk1 yk1Var) {
            super(yk1Var.b());
            fs1.f(g53Var, "this$0");
            fs1.f(yk1Var, "binding");
            this.b = g53Var;
            this.a = yk1Var;
        }

        public static final void d(yk1 yk1Var, g53 g53Var, RoomReflectionsToken roomReflectionsToken, View view) {
            fs1.f(yk1Var, "$this_apply");
            fs1.f(g53Var, "this$0");
            fs1.f(roomReflectionsToken, "$it");
            yk1Var.b.setEnabled(false);
            hd1 hd1Var = g53Var.b;
            if (hd1Var == null) {
                return;
            }
            MaterialButton materialButton = yk1Var.b;
            fs1.e(materialButton, "btnEnableAdvancedDataMode");
            hd1Var.invoke(roomReflectionsToken, materialButton);
        }

        public static final void e(g53 g53Var, RoomReflectionsToken roomReflectionsToken, View view) {
            fs1.f(g53Var, "this$0");
            fs1.f(roomReflectionsToken, "$it");
            tc1 tc1Var = g53Var.a;
            if (tc1Var == null) {
                return;
            }
            tc1Var.invoke(roomReflectionsToken);
        }

        public final void c(int i) {
            final RoomReflectionsToken c = g53.c(this.b, i);
            if (c == null) {
                return;
            }
            final g53 g53Var = this.b;
            final yk1 f = f();
            ImageView imageView = f.c;
            fs1.e(imageView, "ivTokenIcon");
            Integer iconResId = c.getIconResId();
            e30.Q(imageView, iconResId == null ? 0 : iconResId.intValue(), c.getIconResName(), c.getSymbol());
            if (e30.H(c.getSymbol())) {
                ImageView imageView2 = f.c;
                fs1.e(imageView2, "ivTokenIcon");
                e30.P(imageView2, String.valueOf(c.getCmcId()), c.getSymbol());
            }
            f.e.setText(c.getName());
            f.h.setText(c.getSymbol());
            double doubleValue = c.getDifferenceBalance().doubleValue();
            TextView textView = f.f;
            fs1.e(textView, "tvTokenNativeBalance");
            e30.R(textView, doubleValue);
            Double priceUsd = c.getPriceUsd();
            double doubleValue2 = priceUsd == null ? 0.0d : priceUsd.doubleValue();
            double d = doubleValue * doubleValue2;
            if (doubleValue2 > Utils.DOUBLE_EPSILON) {
                TextView textView2 = f.g;
                fs1.e(textView2, "tvTokenNativeBalanceInFiat");
                e30.N(textView2, d, true);
            } else {
                TextView textView3 = f.g;
                fs1.e(textView3, "tvTokenNativeBalanceInFiat");
                e30.R(textView3, doubleValue);
            }
            String displayDate = c.getDisplayDate();
            if (displayDate != null) {
                f.d.setText(displayDate);
            }
            boolean enableAdvanceMode = c.getEnableAdvanceMode();
            f.b.setIconResource(!enableAdvanceMode ? R.drawable.ic_baseline_add_24 : R.drawable.ic_baseline_remove_24);
            f.b.setText(!enableAdvanceMode ? R.string.mr_enable_advanced_data_mode : R.string.mr_disable_advanced_data_mode);
            f.b.setOnClickListener(new View.OnClickListener() { // from class: h53
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    g53.b.d(yk1.this, g53Var, c, view);
                }
            });
            f.b().setOnClickListener(new View.OnClickListener() { // from class: i53
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    g53.b.e(g53.this, c, view);
                }
            });
        }

        public final yk1 f() {
            return this.a;
        }
    }

    public g53() {
        super(a.a);
    }

    public static final /* synthetic */ RoomReflectionsToken c(g53 g53Var, int i) {
        return g53Var.getItem(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public void onBindViewHolder(b bVar, int i) {
        fs1.f(bVar, "holder");
        bVar.c(i);
    }

    public final void e(tc1<? super RoomReflectionsToken, te4> tc1Var) {
        fs1.f(tc1Var, "clickListener");
        this.a = tc1Var;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: f */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        yk1 a2 = yk1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_reflection_token, viewGroup, false));
        fs1.e(a2, "bind(\n            Layout…on_token, parent, false))");
        return new b(this, a2);
    }

    public final void g(hd1<? super RoomReflectionsToken, ? super Button, te4> hd1Var) {
        fs1.f(hd1Var, "enableListener");
        this.b = hd1Var;
    }
}
