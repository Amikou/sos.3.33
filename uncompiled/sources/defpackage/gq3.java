package defpackage;

import android.animation.IntEvaluator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.xg4;

/* compiled from: SlideAnimation.java */
/* renamed from: gq3  reason: default package */
/* loaded from: classes2.dex */
public class gq3 extends nm<ValueAnimator> {
    public hq3 d;
    public int e;
    public int f;

    /* compiled from: SlideAnimation.java */
    /* renamed from: gq3$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            gq3.this.j(valueAnimator);
        }
    }

    public gq3(xg4.a aVar) {
        super(aVar);
        this.e = -1;
        this.f = -1;
        this.d = new hq3();
    }

    @Override // defpackage.nm
    /* renamed from: g */
    public ValueAnimator a() {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(350L);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.addUpdateListener(new a());
        return valueAnimator;
    }

    public final PropertyValuesHolder h() {
        PropertyValuesHolder ofInt = PropertyValuesHolder.ofInt("ANIMATION_COORDINATE", this.e, this.f);
        ofInt.setEvaluator(new IntEvaluator());
        return ofInt;
    }

    public final boolean i(int i, int i2) {
        return (this.e == i && this.f == i2) ? false : true;
    }

    public final void j(ValueAnimator valueAnimator) {
        this.d.b(((Integer) valueAnimator.getAnimatedValue("ANIMATION_COORDINATE")).intValue());
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.d);
        }
    }

    @Override // defpackage.nm
    /* renamed from: k */
    public gq3 d(float f) {
        T t = this.c;
        if (t != 0) {
            long j = f * ((float) this.a);
            if (((ValueAnimator) t).getValues() != null && ((ValueAnimator) this.c).getValues().length > 0) {
                ((ValueAnimator) this.c).setCurrentPlayTime(j);
            }
        }
        return this;
    }

    public gq3 l(int i, int i2) {
        if (this.c != 0 && i(i, i2)) {
            this.e = i;
            this.f = i2;
            ((ValueAnimator) this.c).setValues(h());
        }
        return this;
    }
}
