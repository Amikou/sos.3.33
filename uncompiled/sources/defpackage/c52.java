package defpackage;

import android.net.Uri;
import java.util.Map;

/* renamed from: c52  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class c52 implements u11 {
    public static final /* synthetic */ c52 b = new c52();

    @Override // defpackage.u11
    public final p11[] a() {
        p11[] B;
        B = d52.B();
        return B;
    }

    @Override // defpackage.u11
    public /* synthetic */ p11[] b(Uri uri, Map map) {
        return t11.a(this, uri, map);
    }
}
