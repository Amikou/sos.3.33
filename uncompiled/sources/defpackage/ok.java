package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_CustomAttribute.java */
/* renamed from: ok  reason: default package */
/* loaded from: classes2.dex */
public final class ok extends r90.c {
    public final String a;
    public final String b;

    /* compiled from: AutoValue_CrashlyticsReport_CustomAttribute.java */
    /* renamed from: ok$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.c.a {
        public String a;
        public String b;

        @Override // defpackage.r90.c.a
        public r90.c a() {
            String str = "";
            if (this.a == null) {
                str = " key";
            }
            if (this.b == null) {
                str = str + " value";
            }
            if (str.isEmpty()) {
                return new ok(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.c.a
        public r90.c.a b(String str) {
            Objects.requireNonNull(str, "Null key");
            this.a = str;
            return this;
        }

        @Override // defpackage.r90.c.a
        public r90.c.a c(String str) {
            Objects.requireNonNull(str, "Null value");
            this.b = str;
            return this;
        }
    }

    @Override // defpackage.r90.c
    public String b() {
        return this.a;
    }

    @Override // defpackage.r90.c
    public String c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.c) {
            r90.c cVar = (r90.c) obj;
            return this.a.equals(cVar.b()) && this.b.equals(cVar.c());
        }
        return false;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    public String toString() {
        return "CustomAttribute{key=" + this.a + ", value=" + this.b + "}";
    }

    public ok(String str, String str2) {
        this.a = str;
        this.b = str2;
    }
}
