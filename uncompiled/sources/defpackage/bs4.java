package defpackage;

import org.bouncycastle.asn1.g;
import org.bouncycastle.asn1.h;
import org.bouncycastle.asn1.j0;
import org.bouncycastle.asn1.k;
import org.bouncycastle.asn1.n0;
import org.bouncycastle.asn1.s0;

/* renamed from: bs4  reason: default package */
/* loaded from: classes2.dex */
public class bs4 extends h {
    public final int a;
    public final byte[] f0;
    public final byte[] g0;
    public final byte[] h0;
    public final byte[] i0;
    public final byte[] j0;

    public bs4(int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5) {
        this.a = i;
        this.f0 = wh.e(bArr);
        this.g0 = wh.e(bArr2);
        this.h0 = wh.e(bArr3);
        this.i0 = wh.e(bArr4);
        this.j0 = wh.e(bArr5);
    }

    @Override // org.bouncycastle.asn1.h, defpackage.c4
    public k i() {
        d4 d4Var = new d4();
        d4Var.a(new g(0L));
        d4 d4Var2 = new d4();
        d4Var2.a(new g(this.a));
        d4Var2.a(new j0(this.f0));
        d4Var2.a(new j0(this.g0));
        d4Var2.a(new j0(this.h0));
        d4Var2.a(new j0(this.i0));
        d4Var.a(new n0(d4Var2));
        d4Var.a(new s0(true, 0, new j0(this.j0)));
        return new n0(d4Var);
    }
}
