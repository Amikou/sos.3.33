package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.server.response.SafeParcelResponse;
import com.google.android.gms.common.server.response.zal;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: p25  reason: default package */
/* loaded from: classes.dex */
public final class p25 implements Parcelable.Creator<SafeParcelResponse> {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SafeParcelResponse createFromParcel(Parcel parcel) {
        int J = SafeParcelReader.J(parcel);
        Parcel parcel2 = null;
        int i = 0;
        zal zalVar = null;
        while (parcel.dataPosition() < J) {
            int C = SafeParcelReader.C(parcel);
            int v = SafeParcelReader.v(C);
            if (v == 1) {
                i = SafeParcelReader.E(parcel, C);
            } else if (v == 2) {
                parcel2 = SafeParcelReader.m(parcel, C);
            } else if (v != 3) {
                SafeParcelReader.I(parcel, C);
            } else {
                zalVar = (zal) SafeParcelReader.o(parcel, C, zal.CREATOR);
            }
        }
        SafeParcelReader.u(parcel, J);
        return new SafeParcelResponse(i, parcel2, zalVar);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SafeParcelResponse[] newArray(int i) {
        return new SafeParcelResponse[i];
    }
}
