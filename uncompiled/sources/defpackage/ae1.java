package defpackage;

import java.util.Comparator;
import java.util.concurrent.Callable;

/* compiled from: Functions.java */
/* renamed from: ae1  reason: default package */
/* loaded from: classes2.dex */
public final class ae1 {

    /* compiled from: Functions.java */
    /* renamed from: ae1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public String toString() {
            return "EmptyAction";
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$b */
    /* loaded from: classes2.dex */
    public static final class b {
        public String toString() {
            return "EmptyConsumer";
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$c */
    /* loaded from: classes2.dex */
    public static final class c {
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$d */
    /* loaded from: classes2.dex */
    public static final class d implements Runnable {
        @Override // java.lang.Runnable
        public void run() {
        }

        public String toString() {
            return "EmptyRunnable";
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$e */
    /* loaded from: classes2.dex */
    public static final class e {
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$f */
    /* loaded from: classes2.dex */
    public static final class f implements eu2<Object> {
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$g */
    /* loaded from: classes2.dex */
    public static final class g implements ld1<Object, Object> {
        @Override // defpackage.ld1
        public Object apply(Object obj) {
            return obj;
        }

        public String toString() {
            return "IdentityFunction";
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$h */
    /* loaded from: classes2.dex */
    public static final class h<T, U> implements Callable<U>, ld1<T, U> {
        public final U a;

        public h(U u) {
            this.a = u;
        }

        @Override // defpackage.ld1
        public U apply(T t) throws Exception {
            return this.a;
        }

        @Override // java.util.concurrent.Callable
        public U call() throws Exception {
            return this.a;
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$i */
    /* loaded from: classes2.dex */
    public static final class i {
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$j */
    /* loaded from: classes2.dex */
    public static final class j implements Comparator<Object> {
        @Override // java.util.Comparator
        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo(obj2);
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$k */
    /* loaded from: classes2.dex */
    public static final class k implements Callable<Object> {
        @Override // java.util.concurrent.Callable
        public Object call() {
            return null;
        }
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$l */
    /* loaded from: classes2.dex */
    public static final class l {
    }

    /* compiled from: Functions.java */
    /* renamed from: ae1$m */
    /* loaded from: classes2.dex */
    public static final class m implements eu2<Object> {
    }

    static {
        new g();
        new d();
        new a();
        new b();
        new e();
        new l();
        new c();
        new m();
        new f();
        new k();
        new j();
        new i();
    }

    public static <T> Callable<T> a(T t) {
        return new h(t);
    }
}
