package defpackage;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.zas;
import com.google.android.gms.signin.internal.b;
import com.google.android.gms.signin.internal.zam;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: e15  reason: default package */
/* loaded from: classes.dex */
public final class e15 extends b implements GoogleApiClient.b, GoogleApiClient.c {
    public static a.AbstractC0106a<? extends p15, uo3> h = lz4.c;
    public final Context a;
    public final Handler b;
    public final a.AbstractC0106a<? extends p15, uo3> c;
    public Set<Scope> d;
    public kz e;
    public p15 f;
    public h15 g;

    public e15(Context context, Handler handler, kz kzVar) {
        this(context, handler, kzVar, h);
    }

    public final void H1() {
        p15 p15Var = this.f;
        if (p15Var != null) {
            p15Var.b();
        }
    }

    public final void J1(h15 h15Var) {
        p15 p15Var = this.f;
        if (p15Var != null) {
            p15Var.b();
        }
        this.e.g(Integer.valueOf(System.identityHashCode(this)));
        a.AbstractC0106a<? extends p15, uo3> abstractC0106a = this.c;
        Context context = this.a;
        Looper looper = this.b.getLooper();
        kz kzVar = this.e;
        this.f = abstractC0106a.d(context, looper, kzVar, kzVar.i(), this, this);
        this.g = h15Var;
        Set<Scope> set = this.d;
        if (set != null && !set.isEmpty()) {
            this.f.f();
        } else {
            this.b.post(new g15(this));
        }
    }

    public final void K1(zam zamVar) {
        ConnectionResult I1 = zamVar.I1();
        if (I1.M1()) {
            zas zasVar = (zas) zt2.j(zamVar.J1());
            ConnectionResult J1 = zasVar.J1();
            if (!J1.M1()) {
                String valueOf = String.valueOf(J1);
                StringBuilder sb = new StringBuilder(valueOf.length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.g.a(J1);
                this.f.b();
                return;
            }
            this.g.c(zasVar.I1(), this.d);
        } else {
            this.g.a(I1);
        }
        this.f.b();
    }

    @Override // com.google.android.gms.signin.internal.a
    public final void d1(zam zamVar) {
        this.b.post(new f15(this, zamVar));
    }

    @Override // defpackage.u50
    public final void onConnected(Bundle bundle) {
        this.f.j(this);
    }

    @Override // defpackage.jm2
    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.g.a(connectionResult);
    }

    @Override // defpackage.u50
    public final void onConnectionSuspended(int i) {
        this.f.b();
    }

    public e15(Context context, Handler handler, kz kzVar, a.AbstractC0106a<? extends p15, uo3> abstractC0106a) {
        this.a = context;
        this.b = handler;
        this.e = (kz) zt2.k(kzVar, "ClientSettings must not be null");
        this.d = kzVar.e();
        this.c = abstractC0106a;
    }
}
