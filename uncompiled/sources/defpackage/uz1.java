package defpackage;

import java.io.InputStream;
import org.bouncycastle.asn1.e1;

/* renamed from: uz1  reason: default package */
/* loaded from: classes2.dex */
public abstract class uz1 extends InputStream {
    public final InputStream a;
    public int f0;

    public uz1(InputStream inputStream, int i) {
        this.a = inputStream;
        this.f0 = i;
    }

    public int a() {
        return this.f0;
    }

    public void b(boolean z) {
        InputStream inputStream = this.a;
        if (inputStream instanceof e1) {
            ((e1) inputStream).d(z);
        }
    }
}
