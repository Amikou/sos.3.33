package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: kf3  reason: default package */
/* loaded from: classes2.dex */
public class kf3 extends ct0.b {
    public static final BigInteger g = if3.j;
    public int[] f;

    public kf3() {
        this.f = ed2.h();
    }

    public kf3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(g) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP256R1FieldElement");
        }
        this.f = jf3.d(bigInteger);
    }

    public kf3(int[] iArr) {
        this.f = iArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        int[] h = ed2.h();
        jf3.a(this.f, ((kf3) ct0Var).f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        int[] h = ed2.h();
        jf3.b(this.f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        int[] h = ed2.h();
        g92.d(jf3.a, ((kf3) ct0Var).f, h);
        jf3.e(h, this.f, h);
        return new kf3(h);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof kf3) {
            return ed2.m(this.f, ((kf3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return g.bitLength();
    }

    @Override // defpackage.ct0
    public ct0 g() {
        int[] h = ed2.h();
        g92.d(jf3.a, this.f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return ed2.t(this.f);
    }

    public int hashCode() {
        return g.hashCode() ^ wh.u(this.f, 0, 8);
    }

    @Override // defpackage.ct0
    public boolean i() {
        return ed2.v(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        int[] h = ed2.h();
        jf3.e(this.f, ((kf3) ct0Var).f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        int[] h = ed2.h();
        jf3.g(this.f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public ct0 n() {
        int[] iArr = this.f;
        if (ed2.v(iArr) || ed2.t(iArr)) {
            return this;
        }
        int[] h = ed2.h();
        int[] h2 = ed2.h();
        jf3.j(iArr, h);
        jf3.e(h, iArr, h);
        jf3.k(h, 2, h2);
        jf3.e(h2, h, h2);
        jf3.k(h2, 4, h);
        jf3.e(h, h2, h);
        jf3.k(h, 8, h2);
        jf3.e(h2, h, h2);
        jf3.k(h2, 16, h);
        jf3.e(h, h2, h);
        jf3.k(h, 32, h);
        jf3.e(h, iArr, h);
        jf3.k(h, 96, h);
        jf3.e(h, iArr, h);
        jf3.k(h, 94, h);
        jf3.j(h, h2);
        if (ed2.m(iArr, h2)) {
            return new kf3(h);
        }
        return null;
    }

    @Override // defpackage.ct0
    public ct0 o() {
        int[] h = ed2.h();
        jf3.j(this.f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        int[] h = ed2.h();
        jf3.m(this.f, ((kf3) ct0Var).f, h);
        return new kf3(h);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return ed2.q(this.f, 0) == 1;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return ed2.J(this.f);
    }
}
