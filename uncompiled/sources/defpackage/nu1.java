package defpackage;

import androidx.media3.common.Metadata;
import androidx.media3.common.j;
import androidx.media3.extractor.metadata.mp4.MotionPhotoMetadata;
import androidx.recyclerview.widget.RecyclerView;
import defpackage.wi3;
import java.io.IOException;
import zendesk.support.request.CellBase;

/* compiled from: JpegExtractor.java */
/* renamed from: nu1  reason: default package */
/* loaded from: classes.dex */
public final class nu1 implements p11 {
    public r11 b;
    public int c;
    public int d;
    public int e;
    public MotionPhotoMetadata g;
    public q11 h;
    public ps3 i;
    public ha2 j;
    public final op2 a = new op2(6);
    public long f = -1;

    public static MotionPhotoMetadata e(String str, long j) throws IOException {
        y92 a;
        if (j == -1 || (a = ks4.a(str)) == null) {
            return null;
        }
        return a.a(j);
    }

    @Override // defpackage.p11
    public void a() {
        ha2 ha2Var = this.j;
        if (ha2Var != null) {
            ha2Var.a();
        }
    }

    public final void b(q11 q11Var) throws IOException {
        this.a.L(2);
        q11Var.n(this.a.d(), 0, 2);
        q11Var.f(this.a.J() - 2);
    }

    @Override // defpackage.p11
    public void c(long j, long j2) {
        if (j == 0) {
            this.c = 0;
            this.j = null;
        } else if (this.c == 5) {
            ((ha2) ii.e(this.j)).c(j, j2);
        }
    }

    public final void d() {
        h(new Metadata.Entry[0]);
        ((r11) ii.e(this.b)).m();
        this.b.p(new wi3.b(CellBase.ID_SYSTEM_MESSAGE_REQUEST_CLOSED));
        this.c = 6;
    }

    @Override // defpackage.p11
    public int f(q11 q11Var, ot2 ot2Var) throws IOException {
        int i = this.c;
        if (i == 0) {
            k(q11Var);
            return 0;
        } else if (i == 1) {
            m(q11Var);
            return 0;
        } else if (i == 2) {
            l(q11Var);
            return 0;
        } else if (i == 4) {
            long position = q11Var.getPosition();
            long j = this.f;
            if (position != j) {
                ot2Var.a = j;
                return 1;
            }
            n(q11Var);
            return 0;
        } else if (i != 5) {
            if (i == 6) {
                return -1;
            }
            throw new IllegalStateException();
        } else {
            if (this.i == null || q11Var != this.h) {
                this.h = q11Var;
                this.i = new ps3(q11Var, this.f);
            }
            int f = ((ha2) ii.e(this.j)).f(this.i, ot2Var);
            if (f == 1) {
                ot2Var.a += this.f;
            }
            return f;
        }
    }

    @Override // defpackage.p11
    public boolean g(q11 q11Var) throws IOException {
        if (i(q11Var) != 65496) {
            return false;
        }
        int i = i(q11Var);
        this.d = i;
        if (i == 65504) {
            b(q11Var);
            this.d = i(q11Var);
        }
        if (this.d != 65505) {
            return false;
        }
        q11Var.f(2);
        this.a.L(6);
        q11Var.n(this.a.d(), 0, 6);
        return this.a.F() == 1165519206 && this.a.J() == 0;
    }

    public final void h(Metadata.Entry... entryArr) {
        ((r11) ii.e(this.b)).f(RecyclerView.a0.FLAG_ADAPTER_FULLUPDATE, 4).f(new j.b().K("image/jpeg").X(new Metadata(entryArr)).E());
    }

    public final int i(q11 q11Var) throws IOException {
        this.a.L(2);
        q11Var.n(this.a.d(), 0, 2);
        return this.a.J();
    }

    @Override // defpackage.p11
    public void j(r11 r11Var) {
        this.b = r11Var;
    }

    public final void k(q11 q11Var) throws IOException {
        this.a.L(2);
        q11Var.readFully(this.a.d(), 0, 2);
        int J = this.a.J();
        this.d = J;
        if (J == 65498) {
            if (this.f != -1) {
                this.c = 4;
            } else {
                d();
            }
        } else if ((J < 65488 || J > 65497) && J != 65281) {
            this.c = 1;
        }
    }

    public final void l(q11 q11Var) throws IOException {
        String x;
        if (this.d == 65505) {
            op2 op2Var = new op2(this.e);
            q11Var.readFully(op2Var.d(), 0, this.e);
            if (this.g == null && "http://ns.adobe.com/xap/1.0/".equals(op2Var.x()) && (x = op2Var.x()) != null) {
                MotionPhotoMetadata e = e(x, q11Var.getLength());
                this.g = e;
                if (e != null) {
                    this.f = e.h0;
                }
            }
        } else {
            q11Var.k(this.e);
        }
        this.c = 0;
    }

    public final void m(q11 q11Var) throws IOException {
        this.a.L(2);
        q11Var.readFully(this.a.d(), 0, 2);
        this.e = this.a.J() - 2;
        this.c = 2;
    }

    public final void n(q11 q11Var) throws IOException {
        if (!q11Var.d(this.a.d(), 0, 1, true)) {
            d();
            return;
        }
        q11Var.j();
        if (this.j == null) {
            this.j = new ha2();
        }
        ps3 ps3Var = new ps3(q11Var, this.f);
        this.i = ps3Var;
        if (this.j.g(ps3Var)) {
            this.j.j(new qs3(this.f, (r11) ii.e(this.b)));
            o();
            return;
        }
        d();
    }

    public final void o() {
        h((Metadata.Entry) ii.e(this.g));
        this.c = 5;
    }
}
