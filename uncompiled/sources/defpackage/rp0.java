package defpackage;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.internal.ThreadContextKt;

/* compiled from: DispatchedTask.kt */
/* renamed from: rp0  reason: default package */
/* loaded from: classes2.dex */
public final class rp0 {
    public static final <T> void a(qp0<? super T> qp0Var, int i) {
        if (ze0.a()) {
            if (!(i != -1)) {
                throw new AssertionError();
            }
        }
        q70<? super T> b = qp0Var.b();
        boolean z = i == 4;
        if (!z && (b instanceof np0) && b(i) == b(qp0Var.g0)) {
            CoroutineDispatcher coroutineDispatcher = ((np0) b).h0;
            CoroutineContext context = b.getContext();
            if (coroutineDispatcher.j(context)) {
                coroutineDispatcher.h(context, qp0Var);
                return;
            } else {
                e(qp0Var);
                return;
            }
        }
        d(qp0Var, b, z);
    }

    public static final boolean b(int i) {
        return i == 1 || i == 2;
    }

    public static final boolean c(int i) {
        return i == 2;
    }

    public static final <T> void d(qp0<? super T> qp0Var, q70<? super T> q70Var, boolean z) {
        Object e;
        boolean M0;
        Object h = qp0Var.h();
        Throwable d = qp0Var.d(h);
        if (d != null) {
            Result.a aVar = Result.Companion;
            e = o83.a(d);
        } else {
            Result.a aVar2 = Result.Companion;
            e = qp0Var.e(h);
        }
        Object m52constructorimpl = Result.m52constructorimpl(e);
        if (z) {
            np0 np0Var = (np0) q70Var;
            q70<T> q70Var2 = np0Var.i0;
            Object obj = np0Var.k0;
            CoroutineContext context = q70Var2.getContext();
            Object c = ThreadContextKt.c(context, obj);
            qe4<?> e2 = c != ThreadContextKt.a ? x80.e(q70Var2, context, c) : null;
            try {
                np0Var.i0.resumeWith(m52constructorimpl);
                te4 te4Var = te4.a;
                if (e2 != null) {
                    if (!M0) {
                        return;
                    }
                }
                return;
            } finally {
                if (e2 == null || e2.M0()) {
                    ThreadContextKt.a(context, c);
                }
            }
        }
        q70Var.resumeWith(m52constructorimpl);
    }

    public static final void e(qp0<?> qp0Var) {
        xx0 b = j54.a.b();
        if (b.W()) {
            b.N(qp0Var);
            return;
        }
        b.R(true);
        try {
            d(qp0Var, qp0Var.b(), true);
            do {
            } while (b.b0());
        } finally {
            try {
            } finally {
            }
        }
    }
}
