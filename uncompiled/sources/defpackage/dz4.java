package defpackage;

import java.io.PrintStream;

/* compiled from: uiscm.java */
/* renamed from: dz4  reason: default package */
/* loaded from: classes.dex */
public class dz4 {
    public static final String v = new String("1.1");
    public static final String w = new String("1.3");
    public static final String x = new String("3.1");
    public static final String y = new String("3.92");
    public static final byte[] z = {7, 35, 51, 67, 71, 73, -110};
    public String e;
    public int q;
    public int r;
    public long s;
    public long t;
    public int b = 0;
    public String c = "System OK";
    public byte[] d = null;
    public byte[] f = null;
    public byte[] g = null;
    public byte[] h = null;
    public byte[] i = null;
    public byte[] j = null;
    public byte[] k = null;
    public byte[] l = null;
    public byte[] m = null;
    public byte[] n = null;
    public boolean o = false;
    public boolean p = false;
    public boolean u = false;
    public bz4 a = new bz4(false);

    public dz4() {
        this.s = 0L;
        this.t = 0L;
        r();
        this.q = 2;
        this.t = 0L;
        this.s = 0L;
    }

    public boolean a() {
        return this.t == 0;
    }

    public String b(String str) {
        if (!a()) {
            this.s = this.t;
            return new String();
        }
        try {
            this.a.i(true);
            String b = this.a.b(str);
            if (b.length() == 0) {
                this.s = this.a.f();
                new StringBuilder("Text decryption failed - ").append(this.s);
                return new String();
            }
            this.s = 0L;
            return b;
        } catch (Exception e) {
            this.s = st.n;
            new StringBuilder("System error: ").append(e.getMessage());
            return new String();
        }
    }

    public String c(String str) {
        if (!a()) {
            this.s = this.t;
            return new String();
        }
        try {
            this.a.i(true);
            String d = this.a.d(str);
            if (d.length() == 0) {
                this.s = this.a.f();
                new StringBuilder("Text encryption failed - ").append(this.s);
                return new String();
            }
            this.s = 0L;
            return d;
        } catch (Exception e) {
            this.s = st.n;
            new StringBuilder("System error: ").append(e.getMessage());
            return new String();
        }
    }

    public final int d() {
        this.s = 0L;
        int i = this.q;
        if ((i & 248) > 0) {
            return st.u;
        }
        if ((i & 4) > 0) {
            return st.t;
        }
        if ((i & 2) > 0) {
            return st.s;
        }
        this.s = st.d;
        return st.r;
    }

    public String e() {
        byte[] bArr = this.h;
        if (bArr != null) {
            return lm.f(bArr);
        }
        return null;
    }

    public long f() {
        return this.t;
    }

    public String g() {
        if (!a() && f() != st.o) {
            this.s = this.t;
            return new String();
        }
        try {
            if (!l()) {
                System.out.println("GetU5 696 MakeU5 failed");
                return new String();
            }
            return lm.f(this.f);
        } catch (Exception e) {
            this.s = st.n;
            new StringBuilder("System error: ").append(e.getMessage());
            return new String();
        }
    }

    public final boolean h() {
        int i;
        boolean z2;
        if (this.j != null) {
            int i2 = 0;
            while (true) {
                byte[] bArr = this.j;
                if (i2 >= bArr.length) {
                    break;
                }
                bArr[i2] = 0;
                i2++;
            }
        }
        this.j = null;
        if ((this.q & 16) > 0) {
            byte[] bArr2 = this.n;
            if (bArr2 != null && bArr2.length != 0) {
                i = bArr2.length;
                z2 = true;
            } else {
                this.s = st.l;
                return false;
            }
        } else {
            i = 0;
            z2 = false;
        }
        if (!z2) {
            this.s = st.k;
            return false;
        }
        byte[] bArr3 = new byte[i];
        this.j = bArr3;
        if (i > 0) {
            System.arraycopy(this.n, 0, bArr3, 0, 20);
        }
        this.s = 0L;
        return true;
    }

    public final boolean i() {
        byte[] bArr;
        if (this.k != null) {
            int i = 0;
            while (true) {
                byte[] bArr2 = this.k;
                if (i >= bArr2.length) {
                    break;
                }
                bArr2[i] = 0;
                i++;
            }
        }
        this.k = null;
        if (h() && (bArr = this.i) != null) {
            byte[] bArr3 = new byte[this.j.length + 20];
            System.arraycopy(bArr, 0, bArr3, 0, 20);
            byte[] bArr4 = this.j;
            System.arraycopy(bArr4, 0, bArr3, 20, bArr4.length);
            new cz4();
            this.k = new byte[20];
            if (cz4.h(bArr3) && cz4.g(this.k)) {
                this.s = 0L;
                return true;
            }
            long f = cz4.f();
            int i2 = 0;
            while (true) {
                byte[] bArr5 = this.k;
                if (i2 >= bArr5.length) {
                    this.k = null;
                    this.s = f;
                    return false;
                }
                bArr5[i2] = 0;
                i2++;
            }
        } else {
            this.s = st.k;
            return false;
        }
    }

    public final boolean j() {
        byte[] bArr = this.d;
        if (bArr == null) {
            System.out.println("MakeCValue 739 username is null");
            this.s = st.h;
            return false;
        }
        byte[] bArr2 = new byte[20];
        if (cz4.h(bArr) && cz4.g(bArr2)) {
            byte[] bArr3 = new byte[12];
            this.g = bArr3;
            System.arraycopy(bArr2, 0, bArr3, 0, 12);
            byte[] bArr4 = new byte[32];
            int d = d();
            this.r = d;
            if (d == st.s) {
                if (!k()) {
                    System.out.println("uiscm 803 MakeK5 failed");
                    return false;
                }
                System.arraycopy(this.h, 0, bArr4, 0, 20);
            } else if (d == st.t) {
                byte[] bArr5 = this.i;
                if (bArr5 == null) {
                    this.s = st.j;
                    return false;
                }
                System.arraycopy(bArr5, 0, bArr4, 0, 20);
            } else if (d == st.u) {
                if (!i()) {
                    return false;
                }
                System.arraycopy(this.k, 0, bArr4, 0, 20);
            } else {
                this.s = st.m;
                return false;
            }
            System.arraycopy(this.g, 0, bArr4, 20, 12);
            bz4 bz4Var = new bz4(true);
            if (!bz4Var.g(bArr4)) {
                this.s = st.g;
                return false;
            }
            bz4Var.e(z);
            this.s = 0L;
            return true;
        }
        long f = cz4.f();
        for (int i = 0; i < 20; i++) {
            bArr2[i] = 0;
        }
        this.f = null;
        this.s = f;
        return false;
    }

    public final boolean k() {
        if (!this.p) {
            if (this.h != null) {
                int i = 0;
                while (true) {
                    byte[] bArr = this.h;
                    if (i >= bArr.length) {
                        break;
                    }
                    bArr[i] = 0;
                    i++;
                }
            }
            this.h = null;
            if (this.b == 1) {
                System.out.println("uiscm 449 pass=" + this.e);
            }
            if (this.e.length() < 1) {
                this.s = st.i;
                return false;
            }
            byte[] bArr2 = new byte[40];
            new ez4();
            if (this.b == 1) {
                System.out.println("uiscm 460 pass=" + this.e);
            }
            byte[] c = ez4.c(this.d);
            byte[] c2 = ez4.c(ay1.a(this.e));
            System.arraycopy(c, 0, bArr2, 0, 20);
            System.arraycopy(c2, 0, bArr2, 20, 20);
            String d = ay1.d(bArr2);
            if (this.b == 1) {
                System.out.println("uiscm 471 seed=" + d);
            }
            this.h = new byte[20];
            if (!cz4.h(bArr2) || !cz4.g(this.h)) {
                long f = cz4.f();
                System.out.println("MakeK5 465 failed nResult=" + f);
                int i2 = 0;
                while (true) {
                    byte[] bArr3 = this.h;
                    if (i2 >= bArr3.length) {
                        this.h = null;
                        this.s = f;
                        return false;
                    }
                    bArr3[i2] = 0;
                    i2++;
                }
            }
        }
        this.p = true;
        this.s = 0L;
        return true;
    }

    public final boolean l() {
        if (!this.o) {
            if (this.d == null || !k()) {
                return false;
            }
            bz4 bz4Var = new bz4(false);
            if (!bz4Var.g(this.h)) {
                return false;
            }
            byte[] c = bz4Var.c(this.d);
            if (c.length == 0) {
                return false;
            }
            this.f = new byte[20];
            new cz4();
            if (!cz4.h(c) || !cz4.g(this.f)) {
                cz4.f();
                System.out.println("MakeU5 508 failure");
                int i = 0;
                while (true) {
                    byte[] bArr = this.f;
                    if (i >= bArr.length) {
                        this.f = null;
                        return false;
                    }
                    bArr[i] = 0;
                    i++;
                }
            }
        }
        this.o = true;
        return true;
    }

    public void m(boolean z2) {
        this.u = z2;
    }

    public void n() {
    }

    public boolean o() {
        try {
            if (!j()) {
                new StringBuilder("Verification failed - ").append(this.s);
                System.out.println("SetInitialAuthentication 399 makeCValue failed");
                return false;
            }
            n();
            this.s = 0L;
            return true;
        } catch (Exception e) {
            this.s = st.n;
            new StringBuilder("uiscm 406 System error: ").append(e.getMessage());
            return false;
        }
    }

    public void p(String str) {
        this.e = str;
        this.s = 0L;
    }

    public void q(String str) {
        if (this.b == 1) {
            this.t = 0L;
        }
        byte[] a = ay1.a(str);
        this.d = a;
        String d = ay1.d(a);
        if (this.b == 1) {
            PrintStream printStream = System.out;
            printStream.println("uiscm 354 username=" + d);
        }
        this.s = 0L;
    }

    public void r() {
        s();
        this.d = null;
        this.e = "";
        this.q = 2;
        this.r = st.r;
        if (this.f != null) {
            int i = 0;
            while (true) {
                byte[] bArr = this.f;
                if (i >= bArr.length) {
                    break;
                }
                bArr[i] = 0;
                i++;
            }
        }
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                byte[] bArr2 = this.g;
                if (i2 >= bArr2.length) {
                    break;
                }
                bArr2[i2] = 0;
                i2++;
            }
        }
        if (this.h != null) {
            int i3 = 0;
            while (true) {
                byte[] bArr3 = this.h;
                if (i3 >= bArr3.length) {
                    break;
                }
                bArr3[i3] = 0;
                i3++;
            }
        }
        if (this.i != null) {
            int i4 = 0;
            while (true) {
                byte[] bArr4 = this.i;
                if (i4 >= bArr4.length) {
                    break;
                }
                bArr4[i4] = 0;
                i4++;
            }
        }
        if (this.j != null) {
            int i5 = 0;
            while (true) {
                byte[] bArr5 = this.j;
                if (i5 >= bArr5.length) {
                    break;
                }
                bArr5[i5] = 0;
                i5++;
            }
        }
        if (this.k != null) {
            int i6 = 0;
            while (true) {
                byte[] bArr6 = this.k;
                if (i6 >= bArr6.length) {
                    break;
                }
                bArr6[i6] = 0;
                i6++;
            }
        }
        if (this.m != null) {
            int i7 = 0;
            while (true) {
                byte[] bArr7 = this.m;
                if (i7 >= bArr7.length) {
                    break;
                }
                bArr7[i7] = 0;
                i7++;
            }
        }
        if (this.n != null) {
            int i8 = 0;
            while (true) {
                byte[] bArr8 = this.n;
                if (i8 >= bArr8.length) {
                    break;
                }
                bArr8[i8] = 0;
                i8++;
            }
        }
        if (this.l != null) {
            int i9 = 0;
            while (true) {
                byte[] bArr9 = this.l;
                if (i9 >= bArr9.length) {
                    break;
                }
                bArr9[i9] = 0;
                i9++;
            }
        }
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.m = null;
        this.n = null;
        this.l = null;
        this.s = 0L;
    }

    public void s() {
        this.a.j();
    }

    public boolean t(String str) {
        int length = str.length();
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) str.charAt(i);
        }
        if (!this.a.g(bArr)) {
            this.s = this.a.f();
            return false;
        }
        this.s = 0L;
        return true;
    }
}
