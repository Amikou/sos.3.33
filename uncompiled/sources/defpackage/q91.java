package defpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import net.safemoon.androidwallet.R;

/* compiled from: FragmentAlltokensListBinding.java */
/* renamed from: q91  reason: default package */
/* loaded from: classes2.dex */
public final class q91 {
    public final zd3 a;
    public final SwipeRefreshLayout b;
    public final LinearLayout c;
    public final wf d;
    public final TextView e;

    public q91(LinearLayout linearLayout, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextView textView5, TextView textView6, RecyclerView recyclerView, zd3 zd3Var, SwipeRefreshLayout swipeRefreshLayout, LinearLayout linearLayout2, wf wfVar, TextView textView7, TextView textView8) {
        this.a = zd3Var;
        this.b = swipeRefreshLayout;
        this.c = linearLayout2;
        this.d = wfVar;
        this.e = textView7;
    }

    public static q91 a(View view) {
        int i = R.id.btn1h;
        TextView textView = (TextView) ai4.a(view, R.id.btn1h);
        if (textView != null) {
            i = R.id.btn24h;
            TextView textView2 = (TextView) ai4.a(view, R.id.btn24h);
            if (textView2 != null) {
                i = R.id.btn7d;
                TextView textView3 = (TextView) ai4.a(view, R.id.btn7d);
                if (textView3 != null) {
                    i = R.id.btnRecent;
                    TextView textView4 = (TextView) ai4.a(view, R.id.btnRecent);
                    if (textView4 != null) {
                        i = R.id.btnTopGainers;
                        TextView textView5 = (TextView) ai4.a(view, R.id.btnTopGainers);
                        if (textView5 != null) {
                            i = R.id.btnTopLosers;
                            TextView textView6 = (TextView) ai4.a(view, R.id.btnTopLosers);
                            if (textView6 != null) {
                                i = R.id.rView;
                                RecyclerView recyclerView = (RecyclerView) ai4.a(view, R.id.rView);
                                if (recyclerView != null) {
                                    i = R.id.searchBar;
                                    View a = ai4.a(view, R.id.searchBar);
                                    if (a != null) {
                                        zd3 a2 = zd3.a(a);
                                        i = R.id.swRefresh;
                                        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) ai4.a(view, R.id.swRefresh);
                                        if (swipeRefreshLayout != null) {
                                            i = R.id.timesSelectLayout;
                                            LinearLayout linearLayout = (LinearLayout) ai4.a(view, R.id.timesSelectLayout);
                                            if (linearLayout != null) {
                                                i = R.id.topBar;
                                                View a3 = ai4.a(view, R.id.topBar);
                                                if (a3 != null) {
                                                    wf a4 = wf.a(a3);
                                                    i = R.id.tvNoDataMsg;
                                                    TextView textView7 = (TextView) ai4.a(view, R.id.tvNoDataMsg);
                                                    if (textView7 != null) {
                                                        i = R.id.tv_title;
                                                        TextView textView8 = (TextView) ai4.a(view, R.id.tv_title);
                                                        if (textView8 != null) {
                                                            return new q91((LinearLayout) view, textView, textView2, textView3, textView4, textView5, textView6, recyclerView, a2, swipeRefreshLayout, linearLayout, a4, textView7, textView8);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
