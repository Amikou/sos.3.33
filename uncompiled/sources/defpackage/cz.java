package defpackage;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.ResolvedRecursiveType;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ClassStack.java */
/* renamed from: cz  reason: default package */
/* loaded from: classes.dex */
public final class cz {
    public final cz a;
    public final Class<?> b;
    public ArrayList<ResolvedRecursiveType> c;

    public cz(Class<?> cls) {
        this(null, cls);
    }

    public void a(ResolvedRecursiveType resolvedRecursiveType) {
        if (this.c == null) {
            this.c = new ArrayList<>();
        }
        this.c.add(resolvedRecursiveType);
    }

    public cz b(Class<?> cls) {
        return new cz(this, cls);
    }

    public cz c(Class<?> cls) {
        if (this.b == cls) {
            return this;
        }
        for (cz czVar = this.a; czVar != null; czVar = czVar.a) {
            if (czVar.b == cls) {
                return czVar;
            }
        }
        return null;
    }

    public void d(JavaType javaType) {
        ArrayList<ResolvedRecursiveType> arrayList = this.c;
        if (arrayList != null) {
            Iterator<ResolvedRecursiveType> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().setReference(javaType);
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[ClassStack (self-refs: ");
        ArrayList<ResolvedRecursiveType> arrayList = this.c;
        sb.append(arrayList == null ? "0" : String.valueOf(arrayList.size()));
        sb.append(')');
        for (cz czVar = this; czVar != null; czVar = czVar.a) {
            sb.append(' ');
            sb.append(czVar.b.getName());
        }
        sb.append(']');
        return sb.toString();
    }

    public cz(cz czVar, Class<?> cls) {
        this.a = czVar;
        this.b = cls;
    }
}
