package defpackage;

import defpackage.gw0;
import io.reactivex.BackpressureStrategy;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java8.util.stream.h;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.filters.a;
import org.web3j.protocol.core.filters.b;
import org.web3j.protocol.core.filters.c;
import org.web3j.protocol.core.filters.d;

/* compiled from: JsonRpc2_0Rx.java */
/* renamed from: yv1  reason: default package */
/* loaded from: classes3.dex */
public class yv1 {
    private final ScheduledExecutorService scheduledExecutorService;
    private final bd3 scheduler;
    private final ko4 web3j;

    public yv1(ko4 ko4Var, ScheduledExecutorService scheduledExecutorService) {
        this.web3j = ko4Var;
        this.scheduledExecutorService = scheduledExecutorService;
        this.scheduler = fd3.a(scheduledExecutorService);
    }

    private BigInteger getBlockNumber(gi0 gi0Var) throws IOException {
        if (gi0Var instanceof hi0) {
            return ((hi0) gi0Var).getBlockNumber();
        }
        return this.web3j.ethGetBlockByNumber(gi0Var, false).send().getBlock().getNumber();
    }

    private BigInteger getLatestBlockNumber() throws IOException {
        return getBlockNumber(DefaultBlockParameterName.LATEST);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ nw2 lambda$blockFlowable$10(boolean z, String str) throws Exception {
        return this.web3j.ethGetBlockByHash(str, z).flowable();
    }

    private /* synthetic */ void lambda$ethBlockHashFlowable$1(long j, final u71 u71Var) throws Exception {
        run(new a(this.web3j, new vu() { // from class: pv1
            @Override // defpackage.vu
            public final void onEvent(Object obj) {
                u71.this.c((String) obj);
            }
        }), u71Var, j);
    }

    private /* synthetic */ void lambda$ethLogFlowable$5(qw0 qw0Var, long j, final u71 u71Var) throws Exception {
        run(new c(this.web3j, new vu() { // from class: qv1
            @Override // defpackage.vu
            public final void onEvent(Object obj) {
                u71.this.c((q12) obj);
            }
        }, qw0Var), u71Var, j);
    }

    private /* synthetic */ void lambda$ethPendingTransactionHashFlowable$3(long j, final u71 u71Var) throws Exception {
        run(new d(this.web3j, new vu() { // from class: iv1
            @Override // defpackage.vu
            public final void onEvent(Object obj) {
                u71.this.c((String) obj);
            }
        }), u71Var, j);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ nw2 lambda$pendingTransactionFlowable$7(String str) throws Exception {
        return this.web3j.ethGetTransactionByHash(str).flowable();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ o84 lambda$pendingTransactionFlowable$9(nx0 nx0Var) throws Exception {
        return nx0Var.getTransaction().b();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ org.web3j.protocol.core.c lambda$replayBlocksFlowableSync$11(boolean z, hi0 hi0Var) throws Exception {
        return this.web3j.ethGetBlockByNumber(hi0Var, z);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public /* synthetic */ nw2 lambda$replayPastBlocksFlowableSync$12(BigInteger bigInteger, boolean z, q71 q71Var) throws Exception {
        return replayPastBlocksFlowableSync(new hi0(bigInteger.add(BigInteger.ONE)), z, q71Var);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static /* synthetic */ o84 lambda$toTransactions$13(gw0.f fVar) {
        return (o84) fVar.get();
    }

    private q71<gw0> replayBlocksFlowableSync(gi0 gi0Var, gi0 gi0Var2, boolean z) {
        return replayBlocksFlowableSync(gi0Var, gi0Var2, z, true);
    }

    private q71<gw0> replayPastBlocksFlowableSync(gi0 gi0Var, final boolean z, final q71<gw0> q71Var) {
        try {
            BigInteger blockNumber = getBlockNumber(gi0Var);
            final BigInteger latestBlockNumber = getLatestBlockNumber();
            return blockNumber.compareTo(latestBlockNumber) > -1 ? q71Var : q71.b(replayBlocksFlowableSync(new hi0(blockNumber), new hi0(latestBlockNumber), z), q71.e(new Callable() { // from class: ov1
                @Override // java.util.concurrent.Callable
                public final Object call() {
                    nw2 lambda$replayPastBlocksFlowableSync$12;
                    lambda$replayPastBlocksFlowableSync$12 = yv1.this.lambda$replayPastBlocksFlowableSync$12(latestBlockNumber, z, q71Var);
                    return lambda$replayPastBlocksFlowableSync$12;
                }
            }));
        } catch (IOException e) {
            return q71.g(e);
        }
    }

    private <T> void run(final b<T> bVar, u71<? super T> u71Var, long j) {
        bVar.run(this.scheduledExecutorService, j);
        u71Var.b(new mv() { // from class: rv1
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static List<o84> toTransactions(gw0 gw0Var) {
        return (List) h.a(gw0Var.getBlock().getTransactions()).c(mv1.a).f(java8.util.stream.b.e());
    }

    public q71<gw0> blockFlowable(final boolean z, long j) {
        return ethBlockHashFlowable(j).j(new ld1() { // from class: vv1
            @Override // defpackage.ld1
            public final Object apply(Object obj) {
                nw2 lambda$blockFlowable$10;
                lambda$blockFlowable$10 = yv1.this.lambda$blockFlowable$10(z, (String) obj);
                return lambda$blockFlowable$10;
            }
        });
    }

    public q71<String> ethBlockHashFlowable(long j) {
        return q71.d(new sv1(this, j), BackpressureStrategy.BUFFER);
    }

    public q71<q12> ethLogFlowable(final qw0 qw0Var, final long j) {
        return q71.d(new d81(this, qw0Var, j) { // from class: tv1
        }, BackpressureStrategy.BUFFER);
    }

    public q71<String> ethPendingTransactionHashFlowable(long j) {
        return q71.d(new sv1(this, j), BackpressureStrategy.BUFFER);
    }

    public q71<o84> pendingTransactionFlowable(long j) {
        return ethPendingTransactionHashFlowable(j).j(new ld1() { // from class: uv1
            @Override // defpackage.ld1
            public final Object apply(Object obj) {
                nw2 lambda$pendingTransactionFlowable$7;
                lambda$pendingTransactionFlowable$7 = yv1.this.lambda$pendingTransactionFlowable$7((String) obj);
                return lambda$pendingTransactionFlowable$7;
            }
        }).i(nv1.a).p(kv1.a);
    }

    public q71<gw0> replayBlocksFlowable(gi0 gi0Var, gi0 gi0Var2, boolean z) {
        return replayBlocksFlowable(gi0Var, gi0Var2, z, true);
    }

    public q71<gw0> replayPastAndFutureBlocksFlowable(gi0 gi0Var, boolean z, long j) {
        return replayPastBlocksFlowable(gi0Var, z, blockFlowable(z, j));
    }

    public q71<o84> replayPastAndFutureTransactionsFlowable(gi0 gi0Var, long j) {
        return replayPastAndFutureBlocksFlowable(gi0Var, true, j).l(jv1.a);
    }

    public q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, boolean z, q71<gw0> q71Var) {
        return replayPastBlocksFlowableSync(gi0Var, z, q71Var).q(this.scheduler);
    }

    public q71<o84> replayPastTransactionsFlowable(gi0 gi0Var) {
        return replayPastBlocksFlowable(gi0Var, true, q71.f()).l(jv1.a);
    }

    public q71<o84> replayTransactionsFlowable(gi0 gi0Var, gi0 gi0Var2) {
        return replayBlocksFlowable(gi0Var, gi0Var2, true).l(jv1.a);
    }

    public q71<o84> transactionFlowable(long j) {
        return blockFlowable(true, j).l(jv1.a);
    }

    private q71<gw0> replayBlocksFlowableSync(gi0 gi0Var, gi0 gi0Var2, final boolean z, boolean z2) {
        try {
            return org.web3j.utils.a.range(getBlockNumber(gi0Var), getBlockNumber(gi0Var2), z2).p(lv1.a).p(new ld1() { // from class: wv1
                @Override // defpackage.ld1
                public final Object apply(Object obj) {
                    org.web3j.protocol.core.c lambda$replayBlocksFlowableSync$11;
                    lambda$replayBlocksFlowableSync$11 = yv1.this.lambda$replayBlocksFlowableSync$11(z, (hi0) obj);
                    return lambda$replayBlocksFlowableSync$11;
                }
            }).j(xv1.a);
        } catch (IOException e) {
            return q71.g(e);
        }
    }

    public q71<gw0> replayBlocksFlowable(gi0 gi0Var, gi0 gi0Var2, boolean z, boolean z2) {
        return replayBlocksFlowableSync(gi0Var, gi0Var2, z, z2).q(this.scheduler);
    }

    public q71<gw0> replayPastBlocksFlowable(gi0 gi0Var, boolean z) {
        return replayPastBlocksFlowable(gi0Var, z, q71.f());
    }
}
