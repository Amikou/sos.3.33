package defpackage;

import org.json.JSONObject;

/* compiled from: ImmutableJSONObject.java */
/* renamed from: hp1  reason: default package */
/* loaded from: classes2.dex */
public class hp1 {
    public final JSONObject a;

    public hp1() {
        this.a = new JSONObject();
    }

    public boolean a(String str) {
        return this.a.has(str);
    }

    public boolean b(String str) {
        return this.a.optBoolean(str);
    }

    public boolean c(String str, boolean z) {
        return this.a.optBoolean(str, z);
    }

    public int d(String str, int i) {
        return this.a.optInt(str, i);
    }

    public JSONObject e(String str) {
        return this.a.optJSONObject(str);
    }

    public String f(String str) {
        return this.a.optString(str);
    }

    public String g(String str, String str2) {
        return this.a.optString(str, str2);
    }

    public String toString() {
        return "ImmutableJSONObject{jsonObject=" + this.a + '}';
    }

    public hp1(JSONObject jSONObject) {
        this.a = jSONObject;
    }
}
