package defpackage;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: Component.java */
/* renamed from: a40  reason: default package */
/* loaded from: classes2.dex */
public final class a40<T> {
    public final Set<Class<? super T>> a;
    public final Set<hm0> b;
    public final int c;
    public final int d;
    public final e40<T> e;
    public final Set<Class<?>> f;

    /* compiled from: Component.java */
    /* renamed from: a40$b */
    /* loaded from: classes2.dex */
    public static class b<T> {
        public final Set<Class<? super T>> a;
        public final Set<hm0> b;
        public int c;
        public int d;
        public e40<T> e;
        public Set<Class<?>> f;

        public b<T> b(hm0 hm0Var) {
            bu2.c(hm0Var, "Null dependency");
            i(hm0Var.c());
            this.b.add(hm0Var);
            return this;
        }

        public b<T> c() {
            return h(1);
        }

        public a40<T> d() {
            bu2.d(this.e != null, "Missing required property: factory.");
            return new a40<>(new HashSet(this.a), new HashSet(this.b), this.c, this.d, this.e, this.f);
        }

        public b<T> e() {
            return h(2);
        }

        public b<T> f(e40<T> e40Var) {
            this.e = (e40) bu2.c(e40Var, "Null factory");
            return this;
        }

        public final b<T> g() {
            this.d = 1;
            return this;
        }

        public final b<T> h(int i) {
            bu2.d(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        public final void i(Class<?> cls) {
            bu2.a(!this.a.contains(cls), "Components are not allowed to depend on interfaces they themselves provide.");
        }

        public b(Class<T> cls, Class<? super T>... clsArr) {
            HashSet hashSet = new HashSet();
            this.a = hashSet;
            this.b = new HashSet();
            this.c = 0;
            this.d = 0;
            this.f = new HashSet();
            bu2.c(cls, "Null interface");
            hashSet.add(cls);
            for (Class<? super T> cls2 : clsArr) {
                bu2.c(cls2, "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }
    }

    public static <T> b<T> c(Class<T> cls) {
        return new b<>(cls, new Class[0]);
    }

    public static <T> b<T> d(Class<T> cls, Class<? super T>... clsArr) {
        return new b<>(cls, clsArr);
    }

    public static <T> a40<T> i(final T t, Class<T> cls) {
        return j(cls).f(new e40() { // from class: y30
            @Override // defpackage.e40
            public final Object a(b40 b40Var) {
                Object n;
                n = a40.n(t, b40Var);
                return n;
            }
        }).d();
    }

    public static <T> b<T> j(Class<T> cls) {
        return c(cls).g();
    }

    public static /* synthetic */ Object n(Object obj, b40 b40Var) {
        return obj;
    }

    public static /* synthetic */ Object o(Object obj, b40 b40Var) {
        return obj;
    }

    public static <T> a40<T> p(final T t, Class<T> cls, Class<? super T>... clsArr) {
        return d(cls, clsArr).f(new e40() { // from class: z30
            @Override // defpackage.e40
            public final Object a(b40 b40Var) {
                Object o;
                o = a40.o(t, b40Var);
                return o;
            }
        }).d();
    }

    public Set<hm0> e() {
        return this.b;
    }

    public e40<T> f() {
        return this.e;
    }

    public Set<Class<? super T>> g() {
        return this.a;
    }

    public Set<Class<?>> h() {
        return this.f;
    }

    public boolean k() {
        return this.c == 1;
    }

    public boolean l() {
        return this.c == 2;
    }

    public boolean m() {
        return this.d == 0;
    }

    public String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", type=" + this.d + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }

    public a40(Set<Class<? super T>> set, Set<hm0> set2, int i, int i2, e40<T> e40Var, Set<Class<?>> set3) {
        this.a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = i2;
        this.e = e40Var;
        this.f = Collections.unmodifiableSet(set3);
    }
}
