package defpackage;

import com.google.firebase.encoders.EncodingException;
import com.google.firebase.encoders.c;

/* compiled from: com.google.firebase:firebase-messaging@@22.0.0 */
/* renamed from: h45  reason: default package */
/* loaded from: classes.dex */
public final /* synthetic */ class h45 implements hl2 {
    public static final hl2 a = new h45();

    @Override // com.google.firebase.encoders.b
    public final void a(Object obj, c cVar) {
        int i = l45.e;
        String valueOf = String.valueOf(obj.getClass().getCanonicalName());
        throw new EncodingException(valueOf.length() != 0 ? "Couldn't find encoder for type ".concat(valueOf) : new String("Couldn't find encoder for type "));
    }
}
