package defpackage;

import java.io.IOException;
import java.math.BigInteger;
import org.web3j.crypto.d;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.tx.exceptions.TxHashMismatchException;

/* compiled from: RawTransactionManager.java */
/* renamed from: z33  reason: default package */
/* loaded from: classes3.dex */
public class z33 extends u84 {
    private final long chainId;
    public final ma0 credentials;
    public vc4 txHashVerifier;
    private final ko4 web3j;

    public z33(ko4 ko4Var, ma0 ma0Var, long j) {
        super(ko4Var, ma0Var.getAddress());
        this.txHashVerifier = new vc4();
        this.web3j = ko4Var;
        this.credentials = ma0Var;
        this.chainId = j;
    }

    @Override // defpackage.u84
    public vw0 getCode(String str, gi0 gi0Var) throws IOException {
        return this.web3j.ethGetCode(str, gi0Var).send();
    }

    public BigInteger getNonce() throws IOException {
        return this.web3j.ethGetTransactionCount(this.credentials.getAddress(), DefaultBlockParameterName.PENDING).send().getTransactionCount();
    }

    public vc4 getTxHashVerifier() {
        return this.txHashVerifier;
    }

    @Override // defpackage.u84
    public String sendCall(String str, String str2, gi0 gi0Var) throws IOException {
        iw0 send = this.web3j.ethCall(p84.createEthCallTransaction(getFromAddress(), str, str2), gi0Var).send();
        u84.assertCallNotReverted(send);
        return send.getValue();
    }

    @Override // defpackage.u84
    public hx0 sendTransaction(BigInteger bigInteger, BigInteger bigInteger2, String str, String str2, BigInteger bigInteger3, boolean z) throws IOException {
        return signAndSend(y33.createTransaction(getNonce(), bigInteger, bigInteger2, str, bigInteger3, str2));
    }

    @Override // defpackage.u84
    public hx0 sendTransactionEIP1559(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, String str, String str2, BigInteger bigInteger4, boolean z) throws IOException {
        return signAndSend(y33.createTransaction(getNonce(), null, bigInteger3, str, bigInteger4, str2, bigInteger, bigInteger2));
    }

    public void setTxHashVerifier(vc4 vc4Var) {
        this.txHashVerifier = vc4Var;
    }

    public String sign(y33 y33Var) {
        byte[] signMessage;
        long j = this.chainId;
        if (j > -1) {
            signMessage = d.signMessage(y33Var, j, this.credentials);
        } else {
            signMessage = d.signMessage(y33Var, this.credentials);
        }
        return ej2.toHexString(signMessage);
    }

    public hx0 signAndSend(y33 y33Var) throws IOException {
        String sign = sign(y33Var);
        hx0 send = this.web3j.ethSendRawTransaction(sign).send();
        if (send != null && !send.hasError()) {
            String sha3 = ak1.sha3(sign);
            String transactionHash = send.getTransactionHash();
            if (!this.txHashVerifier.verify(sha3, transactionHash)) {
                throw new TxHashMismatchException(sha3, transactionHash);
            }
        }
        return send;
    }

    public z33(ko4 ko4Var, ma0 ma0Var, long j, x84 x84Var) {
        super(x84Var, ma0Var.getAddress());
        this.txHashVerifier = new vc4();
        this.web3j = ko4Var;
        this.credentials = ma0Var;
        this.chainId = j;
    }

    public z33(ko4 ko4Var, ma0 ma0Var, long j, int i, long j2) {
        super(ko4Var, i, j2, ma0Var.getAddress());
        this.txHashVerifier = new vc4();
        this.web3j = ko4Var;
        this.credentials = ma0Var;
        this.chainId = j;
    }

    public z33(ko4 ko4Var, ma0 ma0Var) {
        this(ko4Var, ma0Var, -1L);
    }

    public z33(ko4 ko4Var, ma0 ma0Var, int i, int i2) {
        this(ko4Var, ma0Var, -1L, i, i2);
    }
}
