package defpackage;

import defpackage.st1;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.KotlinNothingValueException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CompletionHandlerException;
import kotlinx.coroutines.CoroutineDispatcher;

/* compiled from: CancellableContinuationImpl.kt */
/* renamed from: pv  reason: default package */
/* loaded from: classes2.dex */
public class pv<T> extends qp0<T> implements ov<T>, e90 {
    public static final /* synthetic */ AtomicIntegerFieldUpdater k0 = AtomicIntegerFieldUpdater.newUpdater(pv.class, "_decision");
    public static final /* synthetic */ AtomicReferenceFieldUpdater l0 = AtomicReferenceFieldUpdater.newUpdater(pv.class, Object.class, "_state");
    private volatile /* synthetic */ int _decision;
    private volatile /* synthetic */ Object _state;
    public final q70<T> h0;
    public final CoroutineContext i0;
    public yp0 j0;

    /* JADX WARN: Multi-variable type inference failed */
    public pv(q70<? super T> q70Var, int i) {
        super(i);
        this.h0 = q70Var;
        if (ze0.a()) {
            if (!(i != -1)) {
                throw new AssertionError();
            }
        }
        this.i0 = q70Var.getContext();
        this._decision = 0;
        this._state = n6.a;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void L(pv pvVar, Object obj, int i, tc1 tc1Var, int i2, Object obj2) {
        if (obj2 != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: resumeImpl");
        }
        if ((i2 & 4) != 0) {
            tc1Var = null;
        }
        pvVar.K(obj, i, tc1Var);
    }

    public void A() {
        yp0 B = B();
        if (B != null && C()) {
            B.a();
            this.j0 = vg2.a;
        }
    }

    public final yp0 B() {
        st1 st1Var = (st1) getContext().get(st1.f);
        if (st1Var == null) {
            return null;
        }
        yp0 d = st1.a.d(st1Var, true, false, new ey(this), 2, null);
        this.j0 = d;
        return d;
    }

    public boolean C() {
        return !(y() instanceof yg2);
    }

    public final boolean D() {
        q70<T> q70Var = this.h0;
        return (q70Var instanceof np0) && ((np0) q70Var).q(this);
    }

    public final iv E(tc1<? super Throwable, te4> tc1Var) {
        return tc1Var instanceof iv ? (iv) tc1Var : new ms1(tc1Var);
    }

    public final void F(tc1<? super Throwable, te4> tc1Var, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + tc1Var + ", already has " + obj).toString());
    }

    public String G() {
        return "CancellableContinuation";
    }

    public final void H(Throwable th) {
        if (r(th)) {
            return;
        }
        q(th);
        u();
    }

    public final void I() {
        q70<T> q70Var = this.h0;
        np0 np0Var = q70Var instanceof np0 ? (np0) q70Var : null;
        Throwable u = np0Var != null ? np0Var.u(this) : null;
        if (u == null) {
            return;
        }
        t();
        q(u);
    }

    public final boolean J() {
        if (ze0.a()) {
            if (!(this.g0 == 2)) {
                throw new AssertionError();
            }
        }
        if (ze0.a()) {
            if (!(this.j0 != vg2.a)) {
                throw new AssertionError();
            }
        }
        Object obj = this._state;
        if (!ze0.a() || (!(obj instanceof yg2))) {
            if ((obj instanceof r30) && ((r30) obj).d != null) {
                t();
                return false;
            }
            this._decision = 0;
            this._state = n6.a;
            return true;
        }
        throw new AssertionError();
    }

    public final void K(Object obj, int i, tc1<? super Throwable, te4> tc1Var) {
        Object obj2;
        do {
            obj2 = this._state;
            if (obj2 instanceof yg2) {
            } else {
                if (obj2 instanceof wv) {
                    wv wvVar = (wv) obj2;
                    if (wvVar.c()) {
                        if (tc1Var == null) {
                            return;
                        }
                        p(tc1Var, wvVar.a);
                        return;
                    }
                }
                l(obj);
                throw new KotlinNothingValueException();
            }
        } while (!l0.compareAndSet(this, obj2, M((yg2) obj2, obj, i, tc1Var, null)));
        u();
        v(i);
    }

    public final Object M(yg2 yg2Var, Object obj, int i, tc1<? super Throwable, te4> tc1Var, Object obj2) {
        if (obj instanceof t30) {
            if (ze0.a()) {
                if (!(obj2 == null)) {
                    throw new AssertionError();
                }
            }
            if (ze0.a()) {
                if (tc1Var == null) {
                    return obj;
                }
                throw new AssertionError();
            }
            return obj;
        } else if (rp0.b(i) || obj2 != null) {
            if (tc1Var != null || (((yg2Var instanceof iv) && !(yg2Var instanceof zo)) || obj2 != null)) {
                return new r30(obj, yg2Var instanceof iv ? (iv) yg2Var : null, tc1Var, obj2, null, 16, null);
            }
            return obj;
        } else {
            return obj;
        }
    }

    public final boolean N() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!k0.compareAndSet(this, 0, 2));
        return true;
    }

    public final k24 O(Object obj, Object obj2, tc1<? super Throwable, te4> tc1Var) {
        Object obj3;
        do {
            obj3 = this._state;
            if (obj3 instanceof yg2) {
            } else if (!(obj3 instanceof r30) || obj2 == null) {
                return null;
            } else {
                r30 r30Var = (r30) obj3;
                if (r30Var.d == obj2) {
                    if (!ze0.a() || fs1.b(r30Var.a, obj)) {
                        return qv.a;
                    }
                    throw new AssertionError();
                }
                return null;
            }
        } while (!l0.compareAndSet(this, obj3, M((yg2) obj3, obj, this.g0, tc1Var, obj2)));
        u();
        return qv.a;
    }

    public final boolean P() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!k0.compareAndSet(this, 0, 1));
        return true;
    }

    @Override // defpackage.qp0
    public void a(Object obj, Throwable th) {
        while (true) {
            Object obj2 = this._state;
            if (!(obj2 instanceof yg2)) {
                if (obj2 instanceof t30) {
                    return;
                }
                if (obj2 instanceof r30) {
                    r30 r30Var = (r30) obj2;
                    if (!r30Var.c()) {
                        if (l0.compareAndSet(this, obj2, r30.b(r30Var, null, null, null, null, th, 15, null))) {
                            r30Var.d(this, th);
                            return;
                        }
                    } else {
                        throw new IllegalStateException("Must be called at most once".toString());
                    }
                } else if (l0.compareAndSet(this, obj2, new r30(obj2, null, null, null, th, 14, null))) {
                    return;
                }
            } else {
                throw new IllegalStateException("Not completed".toString());
            }
        }
    }

    @Override // defpackage.qp0
    public final q70<T> b() {
        return this.h0;
    }

    @Override // defpackage.ov
    public Object c(T t, Object obj) {
        return O(t, obj, null);
    }

    @Override // defpackage.qp0
    public Throwable d(Object obj) {
        Throwable j;
        Throwable d = super.d(obj);
        if (d == null) {
            return null;
        }
        q70<T> b = b();
        if (ze0.d() && (b instanceof e90)) {
            j = hs3.j(d, (e90) b);
            return j;
        }
        return d;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // defpackage.qp0
    public <T> T e(Object obj) {
        return obj instanceof r30 ? (T) ((r30) obj).a : obj;
    }

    @Override // defpackage.ov
    public void f(tc1<? super Throwable, te4> tc1Var) {
        iv E = E(tc1Var);
        while (true) {
            Object obj = this._state;
            if (obj instanceof n6) {
                if (l0.compareAndSet(this, obj, E)) {
                    return;
                }
            } else if (obj instanceof iv) {
                F(tc1Var, obj);
            } else {
                boolean z = obj instanceof t30;
                if (z) {
                    t30 t30Var = (t30) obj;
                    if (!t30Var.b()) {
                        F(tc1Var, obj);
                    }
                    if (obj instanceof wv) {
                        if (!z) {
                            t30Var = null;
                        }
                        n(tc1Var, t30Var != null ? t30Var.a : null);
                        return;
                    }
                    return;
                } else if (obj instanceof r30) {
                    r30 r30Var = (r30) obj;
                    if (r30Var.b != null) {
                        F(tc1Var, obj);
                    }
                    if (E instanceof zo) {
                        return;
                    }
                    if (r30Var.c()) {
                        n(tc1Var, r30Var.e);
                        return;
                    } else {
                        if (l0.compareAndSet(this, obj, r30.b(r30Var, null, E, null, null, null, 29, null))) {
                            return;
                        }
                    }
                } else if (E instanceof zo) {
                    return;
                } else {
                    if (l0.compareAndSet(this, obj, new r30(obj, E, null, null, null, 28, null))) {
                        return;
                    }
                }
            }
        }
    }

    @Override // defpackage.e90
    public e90 getCallerFrame() {
        q70<T> q70Var = this.h0;
        if (q70Var instanceof e90) {
            return (e90) q70Var;
        }
        return null;
    }

    @Override // defpackage.q70
    public CoroutineContext getContext() {
        return this.i0;
    }

    @Override // defpackage.e90
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @Override // defpackage.qp0
    public Object h() {
        return y();
    }

    @Override // defpackage.ov
    public Object i(Throwable th) {
        return O(new t30(th, false, 2, null), null, null);
    }

    @Override // defpackage.ov
    public void j(T t, tc1<? super Throwable, te4> tc1Var) {
        K(t, this.g0, tc1Var);
    }

    @Override // defpackage.ov
    public void k(CoroutineDispatcher coroutineDispatcher, T t) {
        q70<T> q70Var = this.h0;
        np0 np0Var = q70Var instanceof np0 ? (np0) q70Var : null;
        L(this, t, (np0Var != null ? np0Var.h0 : null) == coroutineDispatcher ? 4 : this.g0, null, 4, null);
    }

    public final Void l(Object obj) {
        throw new IllegalStateException(fs1.l("Already resumed, but proposed with update ", obj).toString());
    }

    public final void m(iv ivVar, Throwable th) {
        try {
            ivVar.a(th);
        } catch (Throwable th2) {
            z80.a(getContext(), new CompletionHandlerException(fs1.l("Exception in invokeOnCancellation handler for ", this), th2));
        }
    }

    public final void n(tc1<? super Throwable, te4> tc1Var, Throwable th) {
        try {
            tc1Var.invoke(th);
        } catch (Throwable th2) {
            z80.a(getContext(), new CompletionHandlerException(fs1.l("Exception in invokeOnCancellation handler for ", this), th2));
        }
    }

    @Override // defpackage.ov
    public Object o(T t, Object obj, tc1<? super Throwable, te4> tc1Var) {
        return O(t, obj, tc1Var);
    }

    public final void p(tc1<? super Throwable, te4> tc1Var, Throwable th) {
        try {
            tc1Var.invoke(th);
        } catch (Throwable th2) {
            z80.a(getContext(), new CompletionHandlerException(fs1.l("Exception in resume onCancellation handler for ", this), th2));
        }
    }

    public boolean q(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof yg2)) {
                return false;
            }
            z = obj instanceof iv;
        } while (!l0.compareAndSet(this, obj, new wv(this, th, z)));
        iv ivVar = z ? (iv) obj : null;
        if (ivVar != null) {
            m(ivVar, th);
        }
        u();
        v(this.g0);
        return true;
    }

    public final boolean r(Throwable th) {
        if (rp0.c(this.g0) && D()) {
            return ((np0) this.h0).r(th);
        }
        return false;
    }

    @Override // defpackage.q70
    public void resumeWith(Object obj) {
        L(this, w30.b(obj, this), this.g0, null, 4, null);
    }

    @Override // defpackage.ov
    public void s(Object obj) {
        if (ze0.a()) {
            if (!(obj == qv.a)) {
                throw new AssertionError();
            }
        }
        v(this.g0);
    }

    public final void t() {
        yp0 yp0Var = this.j0;
        if (yp0Var == null) {
            return;
        }
        yp0Var.a();
        this.j0 = vg2.a;
    }

    public String toString() {
        return G() + '(' + ff0.c(this.h0) + "){" + z() + "}@" + ff0.b(this);
    }

    public final void u() {
        if (D()) {
            return;
        }
        t();
    }

    public final void v(int i) {
        if (N()) {
            return;
        }
        rp0.a(this, i);
    }

    public Throwable w(st1 st1Var) {
        return st1Var.g();
    }

    public final Object x() {
        st1 st1Var;
        Throwable j;
        Throwable j2;
        boolean D = D();
        if (P()) {
            if (this.j0 == null) {
                B();
            }
            if (D) {
                I();
            }
            return gs1.d();
        }
        if (D) {
            I();
        }
        Object y = y();
        if (y instanceof t30) {
            Throwable th = ((t30) y).a;
            if (ze0.d()) {
                j2 = hs3.j(th, this);
                throw j2;
            }
            throw th;
        } else if (rp0.b(this.g0) && (st1Var = (st1) getContext().get(st1.f)) != null && !st1Var.b()) {
            CancellationException g = st1Var.g();
            a(y, g);
            if (ze0.d()) {
                j = hs3.j(g, this);
                throw j;
            }
            throw g;
        } else {
            return e(y);
        }
    }

    public final Object y() {
        return this._state;
    }

    public final String z() {
        Object y = y();
        return y instanceof yg2 ? "Active" : y instanceof wv ? "Cancelled" : "Completed";
    }
}
