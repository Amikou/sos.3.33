package defpackage;

import defpackage.r90;
import java.util.Objects;

/* compiled from: AutoValue_CrashlyticsReport_FilesPayload.java */
/* renamed from: pk  reason: default package */
/* loaded from: classes2.dex */
public final class pk extends r90.d {
    public final ip1<r90.d.b> a;
    public final String b;

    /* compiled from: AutoValue_CrashlyticsReport_FilesPayload.java */
    /* renamed from: pk$b */
    /* loaded from: classes2.dex */
    public static final class b extends r90.d.a {
        public ip1<r90.d.b> a;
        public String b;

        @Override // defpackage.r90.d.a
        public r90.d a() {
            String str = "";
            if (this.a == null) {
                str = " files";
            }
            if (str.isEmpty()) {
                return new pk(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @Override // defpackage.r90.d.a
        public r90.d.a b(ip1<r90.d.b> ip1Var) {
            Objects.requireNonNull(ip1Var, "Null files");
            this.a = ip1Var;
            return this;
        }

        @Override // defpackage.r90.d.a
        public r90.d.a c(String str) {
            this.b = str;
            return this;
        }
    }

    @Override // defpackage.r90.d
    public ip1<r90.d.b> b() {
        return this.a;
    }

    @Override // defpackage.r90.d
    public String c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof r90.d) {
            r90.d dVar = (r90.d) obj;
            if (this.a.equals(dVar.b())) {
                String str = this.b;
                if (str == null) {
                    if (dVar.c() == null) {
                        return true;
                    }
                } else if (str.equals(dVar.c())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        String str = this.b;
        return hashCode ^ (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return "FilesPayload{files=" + this.a + ", orgId=" + this.b + "}";
    }

    public pk(ip1<r90.d.b> ip1Var, String str) {
        this.a = ip1Var;
        this.b = str;
    }
}
