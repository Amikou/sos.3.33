package defpackage;

import android.os.IBinder;

/* renamed from: if5  reason: default package */
/* loaded from: classes.dex */
public final class if5 extends k35 implements ja5 {
    public if5(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.flags.IFlagProvider");
    }
}
