package defpackage;

import org.bouncycastle.util.MemoableResetException;

/* renamed from: oa3  reason: default package */
/* loaded from: classes2.dex */
public class oa3 extends g22 {
    public int p;
    public long q;
    public long r;
    public long s;
    public long t;
    public long u;
    public long v;
    public long w;
    public long x;

    public oa3(int i) {
        if (i >= 512) {
            throw new IllegalArgumentException("bitLength cannot be >= 512");
        }
        if (i % 8 != 0) {
            throw new IllegalArgumentException("bitLength needs to be a multiple of 8");
        }
        if (i == 384) {
            throw new IllegalArgumentException("bitLength cannot be 384 use SHA384 instead");
        }
        int i2 = i / 8;
        this.p = i2;
        w(i2 * 8);
        reset();
    }

    public oa3(oa3 oa3Var) {
        super(oa3Var);
        this.p = oa3Var.p;
        d(oa3Var);
    }

    public static void u(int i, byte[] bArr, int i2, int i3) {
        int min = Math.min(4, i3);
        while (true) {
            min--;
            if (min < 0) {
                return;
            }
            bArr[i2 + min] = (byte) (i >>> ((3 - min) * 8));
        }
    }

    public static void v(long j, byte[] bArr, int i, int i2) {
        if (i2 > 0) {
            u((int) (j >>> 32), bArr, i, i2);
            if (i2 > 4) {
                u((int) (j & 4294967295L), bArr, i + 4, i2 - 4);
            }
        }
    }

    @Override // defpackage.qo0
    public int a(byte[] bArr, int i) {
        q();
        v(this.e, bArr, i, this.p);
        v(this.f, bArr, i + 8, this.p - 8);
        v(this.g, bArr, i + 16, this.p - 16);
        v(this.h, bArr, i + 24, this.p - 24);
        v(this.i, bArr, i + 32, this.p - 32);
        v(this.j, bArr, i + 40, this.p - 40);
        v(this.k, bArr, i + 48, this.p - 48);
        v(this.l, bArr, i + 56, this.p - 56);
        reset();
        return this.p;
    }

    @Override // defpackage.j72
    public j72 copy() {
        return new oa3(this);
    }

    @Override // defpackage.j72
    public void d(j72 j72Var) {
        oa3 oa3Var = (oa3) j72Var;
        if (this.p != oa3Var.p) {
            throw new MemoableResetException("digestLength inappropriate in other");
        }
        super.p(oa3Var);
        this.q = oa3Var.q;
        this.r = oa3Var.r;
        this.s = oa3Var.s;
        this.t = oa3Var.t;
        this.u = oa3Var.u;
        this.v = oa3Var.v;
        this.w = oa3Var.w;
        this.x = oa3Var.x;
    }

    @Override // defpackage.qo0
    public String g() {
        return "SHA-512/" + Integer.toString(this.p * 8);
    }

    @Override // defpackage.qo0
    public int h() {
        return this.p;
    }

    @Override // defpackage.g22, defpackage.qo0
    public void reset() {
        super.reset();
        this.e = this.q;
        this.f = this.r;
        this.g = this.s;
        this.h = this.t;
        this.i = this.u;
        this.j = this.v;
        this.k = this.w;
        this.l = this.x;
    }

    /* JADX WARN: Code restructure failed: missing block: B:7:0x007f, code lost:
        if (r4 > 10) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public final void w(int r4) {
        /*
            r3 = this;
            r0 = -3482333909917012819(0xcfac43c256196cad, double:-6.392239886847908E75)
            r3.e = r0
            r0 = 2216346199247487646(0x1ec20b20216f029e, double:1.604250256667292E-160)
            r3.f = r0
            r0 = -7364697282686394994(0x99cb56d75b315d8e, double:-2.0106609494103695E-184)
            r3.g = r0
            r0 = 65953792586715988(0xea509ffab89354, double:2.9978976005667514E-304)
            r3.h = r0
            r0 = -816286391624063116(0xf4abf7da08432774, double:-1.0252515228978657E254)
            r3.i = r0
            r0 = 4512832404995164602(0x3ea0cd298e9bc9ba, double:5.007211971427005E-7)
            r3.j = r0
            r0 = -5033199132376557362(0xba267c0e5ee418ce, double:-1.418977391716189E-28)
            r3.k = r0
            r0 = -124578254951840548(0xfe4568bcb6db84dc, double:-1.7921927020935902E300)
            r3.l = r0
            r0 = 83
            r3.c(r0)
            r0 = 72
            r3.c(r0)
            r0 = 65
            r3.c(r0)
            r0 = 45
            r3.c(r0)
            r0 = 53
            r3.c(r0)
            r0 = 49
            r3.c(r0)
            r0 = 50
            r3.c(r0)
            r0 = 47
            r3.c(r0)
            r0 = 100
            r1 = 10
            if (r4 <= r0) goto L7f
            int r2 = r4 / 100
            int r2 = r2 + 48
            byte r2 = (byte) r2
            r3.c(r2)
            int r4 = r4 % r0
        L6f:
            int r0 = r4 / 10
            int r0 = r0 + 48
            byte r0 = (byte) r0
            r3.c(r0)
            int r4 = r4 % r1
        L78:
            int r4 = r4 + 48
            byte r4 = (byte) r4
            r3.c(r4)
            goto L82
        L7f:
            if (r4 <= r1) goto L78
            goto L6f
        L82:
            r3.q()
            long r0 = r3.e
            r3.q = r0
            long r0 = r3.f
            r3.r = r0
            long r0 = r3.g
            r3.s = r0
            long r0 = r3.h
            r3.t = r0
            long r0 = r3.i
            r3.u = r0
            long r0 = r3.j
            r3.v = r0
            long r0 = r3.k
            r3.w = r0
            long r0 = r3.l
            r3.x = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.oa3.w(int):void");
    }
}
