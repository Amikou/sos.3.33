package defpackage;

import java.math.BigInteger;

/* renamed from: yb0  reason: default package */
/* loaded from: classes2.dex */
public class yb0 {
    public static final int[] a = {-19, -1, -1, -1, -1, -1, -1, Integer.MAX_VALUE};
    public static final int[] b = {361, 0, 0, 0, 0, 0, 0, 0, -19, -1, -1, -1, -1, -1, -1, 1073741823};

    public static void a(int[] iArr, int[] iArr2, int[] iArr3) {
        ed2.a(iArr, iArr2, iArr3);
        if (ed2.s(iArr3, a)) {
            m(iArr3);
        }
    }

    public static void b(int[] iArr, int[] iArr2) {
        kd2.s(8, iArr, iArr2);
        if (ed2.s(iArr2, a)) {
            m(iArr2);
        }
    }

    public static int c(int[] iArr) {
        long j = (iArr[0] & 4294967295L) - 19;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            j2 = kd2.m(7, iArr, 1);
        }
        long j3 = j2 + (4294967295L & iArr[7]) + 2147483648L;
        iArr[7] = (int) j3;
        return (int) (j3 >> 32);
    }

    public static int[] d(BigInteger bigInteger) {
        int[] o = ed2.o(bigInteger);
        while (true) {
            int[] iArr = a;
            if (!ed2.s(o, iArr)) {
                return o;
            }
            ed2.I(iArr, o);
        }
    }

    public static void e(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] j = ed2.j();
        ed2.y(iArr, iArr2, j);
        h(j, iArr3);
    }

    public static void f(int[] iArr, int[] iArr2, int[] iArr3) {
        ed2.C(iArr, iArr2, iArr3);
        if (kd2.q(16, iArr3, b)) {
            l(iArr3);
        }
    }

    public static void g(int[] iArr, int[] iArr2) {
        if (ed2.v(iArr)) {
            ed2.L(iArr2);
        } else {
            ed2.H(a, iArr, iArr2);
        }
    }

    public static void h(int[] iArr, int[] iArr2) {
        int i = iArr[7];
        kd2.D(8, iArr, 8, i, iArr2, 0);
        int i2 = iArr2[7];
        iArr2[7] = (i2 & Integer.MAX_VALUE) + kd2.g(7, ((ed2.D(19, iArr, iArr2) << 1) + ((i2 >>> 31) - (i >>> 31))) * 19, iArr2);
        if (ed2.s(iArr2, a)) {
            m(iArr2);
        }
    }

    public static void i(int i, int[] iArr) {
        int i2 = iArr[7];
        iArr[7] = (i2 & Integer.MAX_VALUE) + kd2.g(7, ((i << 1) | (i2 >>> 31)) * 19, iArr);
        if (ed2.s(iArr, a)) {
            m(iArr);
        }
    }

    public static void j(int[] iArr, int[] iArr2) {
        int[] j = ed2.j();
        ed2.F(iArr, j);
        h(j, iArr2);
    }

    public static void k(int[] iArr, int i, int[] iArr2) {
        int[] j = ed2.j();
        ed2.F(iArr, j);
        while (true) {
            h(j, iArr2);
            i--;
            if (i <= 0) {
                return;
            }
            ed2.F(iArr2, j);
        }
    }

    public static int l(int[] iArr) {
        int[] iArr2 = b;
        long j = (iArr[0] & 4294967295L) - (iArr2[0] & 4294967295L);
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            j2 = kd2.m(8, iArr, 1);
        }
        long j3 = j2 + (iArr[8] & 4294967295L) + 19;
        iArr[8] = (int) j3;
        long j4 = j3 >> 32;
        if (j4 != 0) {
            j4 = kd2.t(15, iArr, 9);
        }
        long j5 = j4 + ((iArr[15] & 4294967295L) - (4294967295L & (iArr2[15] + 1)));
        iArr[15] = (int) j5;
        return (int) (j5 >> 32);
    }

    public static int m(int[] iArr) {
        long j = (iArr[0] & 4294967295L) + 19;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            j2 = kd2.t(7, iArr, 1);
        }
        long j3 = j2 + ((4294967295L & iArr[7]) - 2147483648L);
        iArr[7] = (int) j3;
        return (int) (j3 >> 32);
    }

    public static void n(int[] iArr, int[] iArr2, int[] iArr3) {
        if (ed2.H(iArr, iArr2, iArr3) != 0) {
            c(iArr3);
        }
    }

    public static void o(int[] iArr, int[] iArr2) {
        kd2.E(8, iArr, 0, iArr2);
        if (ed2.s(iArr2, a)) {
            m(iArr2);
        }
    }
}
