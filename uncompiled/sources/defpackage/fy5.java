package defpackage;

import com.google.protobuf.r0;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: fy5  reason: default package */
/* loaded from: classes.dex */
public final class fy5 {
    public static final dy5 a;
    public static final dy5 b;

    static {
        dy5 dy5Var;
        try {
            dy5Var = (dy5) r0.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            dy5Var = null;
        }
        a = dy5Var;
        b = new dy5();
    }

    public static dy5 a() {
        return a;
    }

    public static dy5 b() {
        return b;
    }
}
