package defpackage;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* compiled from: TypeReference.java */
/* renamed from: ud4  reason: default package */
/* loaded from: classes.dex */
public abstract class ud4<T> implements Comparable<ud4<T>> {
    public final Type _type;

    public ud4() {
        Type genericSuperclass = getClass().getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            this._type = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
            return;
        }
        throw new IllegalArgumentException("Internal error: TypeReference constructed without actual type information");
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return compareTo((ud4) ((ud4) obj));
    }

    public int compareTo(ud4<T> ud4Var) {
        return 0;
    }

    public Type getType() {
        return this._type;
    }
}
