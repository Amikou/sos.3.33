package defpackage;

import java.util.List;
import net.safemoon.androidwallet.common.TokenType;
import net.safemoon.androidwallet.model.token.abstraction.IToken;

/* compiled from: IAllTokenDataSource.kt */
/* renamed from: ol1  reason: default package */
/* loaded from: classes2.dex */
public interface ol1 {
    List<IToken> a(TokenType tokenType);
}
