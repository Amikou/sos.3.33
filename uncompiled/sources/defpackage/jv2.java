package defpackage;

import java.util.Map;

/* compiled from: ProducerListener.java */
/* renamed from: jv2  reason: default package */
/* loaded from: classes.dex */
public interface jv2 {
    void b(String str, String str2);

    void d(String str, String str2, Map<String, String> map);

    void e(String str, String str2, boolean z);

    boolean f(String str);

    void h(String str, String str2, String str3);

    void i(String str, String str2, Map<String, String> map);

    void j(String str, String str2, Throwable th, Map<String, String> map);
}
