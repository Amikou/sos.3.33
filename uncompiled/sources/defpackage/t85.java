package defpackage;

/* renamed from: t85  reason: default package */
/* loaded from: classes.dex */
public final class t85 implements j85 {
    public t85() {
    }

    public /* synthetic */ t85(b85 b85Var) {
        this();
    }

    @Override // defpackage.j85
    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
