package defpackage;

import defpackage.ct0;
import java.math.BigInteger;

/* renamed from: lh3  reason: default package */
/* loaded from: classes2.dex */
public class lh3 extends ct0.a {
    public long[] f;

    public lh3() {
        this.f = hd2.b();
    }

    public lh3(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 409) {
            throw new IllegalArgumentException("x value invalid for SecT409FieldElement");
        }
        this.f = kh3.d(bigInteger);
    }

    public lh3(long[] jArr) {
        this.f = jArr;
    }

    @Override // defpackage.ct0
    public ct0 a(ct0 ct0Var) {
        long[] b = hd2.b();
        kh3.a(this.f, ((lh3) ct0Var).f, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 b() {
        long[] b = hd2.b();
        kh3.c(this.f, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 d(ct0 ct0Var) {
        return j(ct0Var.g());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof lh3) {
            return hd2.d(this.f, ((lh3) obj).f);
        }
        return false;
    }

    @Override // defpackage.ct0
    public int f() {
        return 409;
    }

    @Override // defpackage.ct0
    public ct0 g() {
        long[] b = hd2.b();
        kh3.j(this.f, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public boolean h() {
        return hd2.f(this.f);
    }

    public int hashCode() {
        return wh.v(this.f, 0, 7) ^ 4090087;
    }

    @Override // defpackage.ct0
    public boolean i() {
        return hd2.g(this.f);
    }

    @Override // defpackage.ct0
    public ct0 j(ct0 ct0Var) {
        long[] b = hd2.b();
        kh3.k(this.f, ((lh3) ct0Var).f, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 k(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        return l(ct0Var, ct0Var2, ct0Var3);
    }

    @Override // defpackage.ct0
    public ct0 l(ct0 ct0Var, ct0 ct0Var2, ct0 ct0Var3) {
        long[] jArr = this.f;
        long[] jArr2 = ((lh3) ct0Var).f;
        long[] jArr3 = ((lh3) ct0Var2).f;
        long[] jArr4 = ((lh3) ct0Var3).f;
        long[] k = kd2.k(13);
        kh3.l(jArr, jArr2, k);
        kh3.l(jArr3, jArr4, k);
        long[] b = hd2.b();
        kh3.m(k, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 m() {
        return this;
    }

    @Override // defpackage.ct0
    public ct0 n() {
        long[] b = hd2.b();
        kh3.o(this.f, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 o() {
        long[] b = hd2.b();
        kh3.p(this.f, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 p(ct0 ct0Var, ct0 ct0Var2) {
        long[] jArr = this.f;
        long[] jArr2 = ((lh3) ct0Var).f;
        long[] jArr3 = ((lh3) ct0Var2).f;
        long[] k = kd2.k(13);
        kh3.q(jArr, k);
        kh3.l(jArr2, jArr3, k);
        long[] b = hd2.b();
        kh3.m(k, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 q(int i) {
        if (i < 1) {
            return this;
        }
        long[] b = hd2.b();
        kh3.r(this.f, i, b);
        return new lh3(b);
    }

    @Override // defpackage.ct0
    public ct0 r(ct0 ct0Var) {
        return a(ct0Var);
    }

    @Override // defpackage.ct0
    public boolean s() {
        return (this.f[0] & 1) != 0;
    }

    @Override // defpackage.ct0
    public BigInteger t() {
        return hd2.h(this.f);
    }

    @Override // defpackage.ct0.a
    public int u() {
        return kh3.s(this.f);
    }
}
