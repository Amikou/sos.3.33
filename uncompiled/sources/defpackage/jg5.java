package defpackage;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@19.0.0 */
/* renamed from: jg5  reason: default package */
/* loaded from: classes.dex */
public final class jg5 {
    public final int a;
    public final boolean b;
    public final boolean c;
    public final /* synthetic */ og5 d;

    public jg5(og5 og5Var, int i, boolean z, boolean z2) {
        this.d = og5Var;
        this.a = i;
        this.b = z;
        this.c = z2;
    }

    public final void a(String str) {
        this.d.y(this.a, this.b, this.c, str, null, null, null);
    }

    public final void b(String str, Object obj) {
        this.d.y(this.a, this.b, this.c, str, obj, null, null);
    }

    public final void c(String str, Object obj, Object obj2) {
        this.d.y(this.a, this.b, this.c, str, obj, obj2, null);
    }

    public final void d(String str, Object obj, Object obj2, Object obj3) {
        this.d.y(this.a, this.b, this.c, str, obj, obj2, obj3);
    }
}
