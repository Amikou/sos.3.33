package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import net.safemoon.androidwallet.R;

/* compiled from: TokenListAdapter.kt */
/* renamed from: z64  reason: default package */
/* loaded from: classes2.dex */
public final class z64 extends RecyclerView.Adapter<a> {
    public final List<q9> a;
    public final tc1<q9, te4> b;

    /* compiled from: TokenListAdapter.kt */
    /* renamed from: z64$a */
    /* loaded from: classes2.dex */
    public static final class a extends RecyclerView.a0 {
        public final zk1 a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(zk1 zk1Var) {
            super(zk1Var.b());
            fs1.f(zk1Var, "binding");
            this.a = zk1Var;
        }

        public final void a(q9 q9Var) {
            fs1.f(q9Var, "model");
            zk1 zk1Var = this.a;
            ImageView imageView = zk1Var.b;
            fs1.e(imageView, "ivTokenIcon");
            e30.Q(imageView, q9Var.e(), q9Var.d(), q9Var.g());
            if (e30.H(q9Var.g())) {
                String c = kt.c(q9Var.a(), q9Var.g(), null, 2, null);
                ImageView imageView2 = zk1Var.b;
                fs1.e(imageView2, "ivTokenIcon");
                e30.P(imageView2, c, q9Var.g());
            }
            zk1Var.c.setText(q9Var.f());
            zk1Var.d.setText(q9Var.g());
        }

        public final zk1 b() {
            return this.a;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public z64(List<q9> list, tc1<? super q9, te4> tc1Var) {
        fs1.f(list, "items");
        fs1.f(tc1Var, "callBack");
        this.a = list;
        this.b = tc1Var;
    }

    public static final void c(z64 z64Var, q9 q9Var, View view) {
        fs1.f(z64Var, "this$0");
        fs1.f(q9Var, "$item");
        z64Var.b.invoke(q9Var);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: b */
    public void onBindViewHolder(a aVar, int i) {
        fs1.f(aVar, "holder");
        final q9 q9Var = this.a.get(i);
        aVar.a(q9Var);
        zk1 b = aVar.b();
        b.e.setVisibility(e30.m0(i < this.a.size() - 1));
        b.b().setOnClickListener(new View.OnClickListener() { // from class: y64
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                z64.c(z64.this, q9Var, view);
            }
        });
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /* renamed from: d */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        fs1.f(viewGroup, "parent");
        zk1 a2 = zk1.a(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_token_list, viewGroup, false));
        fs1.e(a2, "bind(LayoutInflater.from…ken_list, parent, false))");
        return new a(a2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.size();
    }
}
