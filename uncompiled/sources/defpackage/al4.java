package defpackage;

import android.view.View;

/* compiled from: VisibilityPropagation.java */
/* renamed from: al4  reason: default package */
/* loaded from: classes.dex */
public abstract class al4 extends jb4 {
    public static final String[] a = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    public static int d(kb4 kb4Var, int i) {
        int[] iArr;
        if (kb4Var == null || (iArr = (int[]) kb4Var.a.get("android:visibilityPropagation:center")) == null) {
            return -1;
        }
        return iArr[i];
    }

    @Override // defpackage.jb4
    public void a(kb4 kb4Var) {
        View view = kb4Var.b;
        Integer num = (Integer) kb4Var.a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        kb4Var.a.put("android:visibilityPropagation:visibility", num);
        view.getLocationOnScreen(r2);
        int[] iArr = {iArr[0] + Math.round(view.getTranslationX())};
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = iArr[1] + (view.getHeight() / 2);
        kb4Var.a.put("android:visibilityPropagation:center", iArr);
    }

    @Override // defpackage.jb4
    public String[] b() {
        return a;
    }

    public int e(kb4 kb4Var) {
        Integer num;
        if (kb4Var == null || (num = (Integer) kb4Var.a.get("android:visibilityPropagation:visibility")) == null) {
            return 8;
        }
        return num.intValue();
    }

    public int f(kb4 kb4Var) {
        return d(kb4Var, 0);
    }

    public int g(kb4 kb4Var) {
        return d(kb4Var, 1);
    }
}
