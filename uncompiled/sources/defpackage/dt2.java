package defpackage;

import androidx.annotation.RecentlyNonNull;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
@Deprecated
/* renamed from: dt2  reason: default package */
/* loaded from: classes.dex */
public class dt2 {
    public static a a;

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* renamed from: dt2$a */
    /* loaded from: classes.dex */
    public interface a {
        @RecentlyNonNull
        @Deprecated
        ScheduledExecutorService a();
    }

    @RecentlyNonNull
    @Deprecated
    public static synchronized a a() {
        a aVar;
        synchronized (dt2.class) {
            if (a == null) {
                a = new x35();
            }
            aVar = a;
        }
        return aVar;
    }
}
