package defpackage;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import defpackage.dr4;
import defpackage.xg4;

/* compiled from: ThinWormAnimation.java */
/* renamed from: d54  reason: default package */
/* loaded from: classes2.dex */
public class d54 extends dr4 {
    public e54 k;

    /* compiled from: ThinWormAnimation.java */
    /* renamed from: d54$a */
    /* loaded from: classes2.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            d54.this.r(valueAnimator);
        }
    }

    public d54(xg4.a aVar) {
        super(aVar);
        this.k = new e54();
    }

    @Override // defpackage.dr4
    public dr4 n(int i, int i2, int i3, boolean z) {
        if (k(i, i2, i3, z)) {
            this.c = a();
            this.d = i;
            this.e = i2;
            this.f = i3;
            this.g = z;
            int i4 = i3 * 2;
            int i5 = i - i3;
            this.h = i5;
            this.i = i + i3;
            this.k.d(i5);
            this.k.c(this.i);
            this.k.f(i4);
            dr4.b h = h(z);
            long j = this.a;
            long j2 = (long) (j * 0.8d);
            long j3 = (long) (j * 0.2d);
            long j4 = (long) (j * 0.5d);
            long j5 = (long) (j * 0.5d);
            ValueAnimator i6 = i(h.a, h.b, j2, false, this.k);
            ValueAnimator i7 = i(h.c, h.d, j2, true, this.k);
            i7.setStartDelay(j3);
            ValueAnimator p = p(i4, i3, j4);
            ValueAnimator p2 = p(i3, i4, j4);
            p2.setStartDelay(j5);
            ((AnimatorSet) this.c).playTogether(i6, i7, p, p2);
        }
        return this;
    }

    public final ValueAnimator p(int i, int i2, long j) {
        ValueAnimator ofInt = ValueAnimator.ofInt(i, i2);
        ofInt.setInterpolator(new AccelerateDecelerateInterpolator());
        ofInt.setDuration(j);
        ofInt.addUpdateListener(new a());
        return ofInt;
    }

    @Override // defpackage.dr4
    /* renamed from: q */
    public d54 j(long j) {
        super.j(j);
        return this;
    }

    public final void r(ValueAnimator valueAnimator) {
        this.k.f(((Integer) valueAnimator.getAnimatedValue()).intValue());
        xg4.a aVar = this.b;
        if (aVar != null) {
            aVar.a(this.k);
        }
    }

    @Override // defpackage.dr4
    /* renamed from: s */
    public d54 m(float f) {
        T t = this.c;
        if (t != 0) {
            long j = f * ((float) this.a);
            int size = ((AnimatorSet) t).getChildAnimations().size();
            for (int i = 0; i < size; i++) {
                ValueAnimator valueAnimator = (ValueAnimator) ((AnimatorSet) this.c).getChildAnimations().get(i);
                long startDelay = j - valueAnimator.getStartDelay();
                long duration = valueAnimator.getDuration();
                if (startDelay > duration) {
                    startDelay = duration;
                } else if (startDelay < 0) {
                    startDelay = 0;
                }
                if ((i != size - 1 || startDelay > 0) && valueAnimator.getValues() != null && valueAnimator.getValues().length > 0) {
                    valueAnimator.setCurrentPlayTime(startDelay);
                }
            }
        }
        return this;
    }
}
