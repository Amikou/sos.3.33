package defpackage;

import java.util.concurrent.CancellationException;

/* compiled from: Channel.kt */
/* renamed from: f43  reason: default package */
/* loaded from: classes2.dex */
public interface f43<E> {
    void a(CancellationException cancellationException);

    Object e(q70<? super tx<? extends E>> q70Var);

    qx<E> iterator();
}
