package defpackage;

import androidx.annotation.RecentlyNonNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: pl2  reason: default package */
/* loaded from: classes.dex */
public final class pl2 {

    /* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
    /* renamed from: pl2$a */
    /* loaded from: classes.dex */
    public static final class a {
        public final List<String> a;
        public final Object b;

        public a(Object obj) {
            this.b = zt2.j(obj);
            this.a = new ArrayList();
        }

        @RecentlyNonNull
        public final a a(@RecentlyNonNull String str, Object obj) {
            List<String> list = this.a;
            String str2 = (String) zt2.j(str);
            String valueOf = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 1 + valueOf.length());
            sb.append(str2);
            sb.append("=");
            sb.append(valueOf);
            list.add(sb.toString());
            return this;
        }

        @RecentlyNonNull
        public final String toString() {
            StringBuilder sb = new StringBuilder(100);
            sb.append(this.b.getClass().getSimpleName());
            sb.append('{');
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                sb.append(this.a.get(i));
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }

    @RecentlyNonNull
    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @RecentlyNonNull
    public static int b(@RecentlyNonNull Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    @RecentlyNonNull
    public static a c(@RecentlyNonNull Object obj) {
        return new a(obj);
    }
}
