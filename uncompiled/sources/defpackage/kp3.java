package defpackage;

import androidx.media3.common.Metadata;
import java.nio.ByteBuffer;

/* compiled from: SimpleMetadataDecoder.java */
/* renamed from: kp3  reason: default package */
/* loaded from: classes.dex */
public abstract class kp3 implements l82 {
    @Override // defpackage.l82
    public final Metadata a(n82 n82Var) {
        ByteBuffer byteBuffer = (ByteBuffer) ii.e(n82Var.g0);
        ii.a(byteBuffer.position() == 0 && byteBuffer.hasArray() && byteBuffer.arrayOffset() == 0);
        if (n82Var.o()) {
            return null;
        }
        return b(n82Var, byteBuffer);
    }

    public abstract Metadata b(n82 n82Var, ByteBuffer byteBuffer);
}
