package defpackage;

import com.google.firebase.crashlytics.internal.common.e;
import defpackage.r90;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: CrashlyticsReportPersistence.java */
/* renamed from: ga0  reason: default package */
/* loaded from: classes2.dex */
public class ga0 {
    public static final Charset g = Charset.forName("UTF-8");
    public static final int h = 15;
    public static final z90 i = new z90();
    public static final Comparator<? super File> j = ea0.a;
    public static final FilenameFilter k = da0.a;
    public final AtomicInteger a = new AtomicInteger(0);
    public final File b;
    public final File c;
    public final File d;
    public final File e;
    public final en3 f;

    public ga0(File file, en3 en3Var) {
        File file2 = new File(file, "report-persistence");
        this.b = new File(file2, "sessions");
        this.c = new File(file2, "priority-reports");
        this.d = new File(file2, "reports");
        this.e = new File(file2, "native-reports");
        this.f = en3Var;
    }

    public static /* synthetic */ boolean A(String str, File file) {
        return file.isDirectory() && !file.getName().equals(str);
    }

    public static /* synthetic */ int C(File file, File file2) {
        return file2.getName().compareTo(file.getName());
    }

    public static boolean G(File file) {
        return file.exists() || file.mkdirs();
    }

    public static int H(File file, File file2) {
        return s(file.getName()).compareTo(s(file2.getName()));
    }

    public static File K(File file) throws IOException {
        if (G(file)) {
            return file;
        }
        throw new IOException("Could not create directory " + file);
    }

    public static String L(File file) throws IOException {
        byte[] bArr = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        FileInputStream fileInputStream = new FileInputStream(file);
        while (true) {
            try {
                int read = fileInputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    String str = new String(byteArrayOutputStream.toByteArray(), g);
                    fileInputStream.close();
                    return str;
                }
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
    }

    public static void M(File file) {
        if (file == null) {
            return;
        }
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                M(file2);
            }
        }
        file.delete();
    }

    public static List<File> N(List<File>... listArr) {
        for (List<File> list : listArr) {
            Collections.sort(list, j);
        }
        return j(listArr);
    }

    public static void O(File file, File file2, r90.d dVar, String str) {
        try {
            z90 z90Var = i;
            S(new File(K(file2), str), z90Var.E(z90Var.D(L(file)).m(dVar)));
        } catch (IOException e) {
            w12 f = w12.f();
            f.l("Could not synthesize final native report file for " + file, e);
        }
    }

    public static void Q(File file, File file2, List<r90.e.d> list, long j2, boolean z, String str) {
        try {
            z90 z90Var = i;
            r90 l = z90Var.D(L(file)).n(j2, z, str).l(ip1.e(list));
            r90.e j3 = l.j();
            if (j3 == null) {
                return;
            }
            S(new File(K(file2), j3.h()), z90Var.E(l));
        } catch (IOException e) {
            w12 f = w12.f();
            f.l("Could not synthesize final report file for " + file, e);
        }
    }

    public static int R(File file, int i2) {
        List<File> u = u(file, ca0.a);
        Collections.sort(u, fa0.a);
        return h(u, i2);
    }

    public static void S(File file, String str) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), g);
        try {
            outputStreamWriter.write(str);
            outputStreamWriter.close();
        } catch (Throwable th) {
            try {
                outputStreamWriter.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static void T(File file, String str, long j2) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), g);
        try {
            outputStreamWriter.write(str);
            file.setLastModified(k(j2));
            outputStreamWriter.close();
        } catch (Throwable th) {
            try {
                outputStreamWriter.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static int h(List<File> list, int i2) {
        int size = list.size();
        for (File file : list) {
            if (size <= i2) {
                return size;
            }
            M(file);
            size--;
        }
        return size;
    }

    public static List<File> j(List<File>... listArr) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        for (List<File> list : listArr) {
            i2 += list.size();
        }
        arrayList.ensureCapacity(i2);
        for (List<File> list2 : listArr) {
            arrayList.addAll(list2);
        }
        return arrayList;
    }

    public static long k(long j2) {
        return j2 * 1000;
    }

    public static String p(int i2, boolean z) {
        String format = String.format(Locale.US, "%010d", Integer.valueOf(i2));
        String str = z ? "_" : "";
        return "event" + format + str;
    }

    public static List<File> q(File file) {
        return t(file, null);
    }

    public static String s(String str) {
        return str.substring(0, h);
    }

    public static List<File> t(File file, FileFilter fileFilter) {
        if (!file.isDirectory()) {
            return Collections.emptyList();
        }
        File[] listFiles = fileFilter == null ? file.listFiles() : file.listFiles(fileFilter);
        return listFiles != null ? Arrays.asList(listFiles) : Collections.emptyList();
    }

    public static List<File> u(File file, FilenameFilter filenameFilter) {
        if (!file.isDirectory()) {
            return Collections.emptyList();
        }
        File[] listFiles = filenameFilter == null ? file.listFiles() : file.listFiles(filenameFilter);
        return listFiles != null ? Arrays.asList(listFiles) : Collections.emptyList();
    }

    public static boolean y(String str) {
        return str.startsWith("event") && str.endsWith("_");
    }

    public static boolean z(File file, String str) {
        return str.startsWith("event") && !str.endsWith("_");
    }

    public List<String> E() {
        List<File> q = q(this.b);
        Collections.sort(q, j);
        ArrayList arrayList = new ArrayList();
        for (File file : q) {
            arrayList.add(file.getName());
        }
        return arrayList;
    }

    public List<e> F() {
        List<File> r = r();
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(r.size());
        for (File file : r()) {
            try {
                arrayList.add(e.a(i.D(L(file)), file.getName()));
            } catch (IOException e) {
                w12 f = w12.f();
                f.l("Could not load report file " + file + "; deleting", e);
                file.delete();
            }
        }
        return arrayList;
    }

    public void I(r90.e.d dVar, String str, boolean z) {
        int i2 = this.f.b().b().a;
        File v = v(str);
        try {
            S(new File(v, p(this.a.getAndIncrement(), z)), i.h(dVar));
        } catch (IOException e) {
            w12 f = w12.f();
            f.l("Could not persist event for session " + str, e);
        }
        R(v, i2);
    }

    public void J(r90 r90Var) {
        r90.e j2 = r90Var.j();
        if (j2 == null) {
            w12.f().b("Could not get session for report");
            return;
        }
        String h2 = j2.h();
        try {
            File K = K(v(h2));
            S(new File(K, "report"), i.E(r90Var));
            T(new File(K, "start-time"), "", j2.k());
        } catch (IOException e) {
            w12 f = w12.f();
            f.c("Could not persist report for session " + h2, e);
        }
    }

    public final void P(File file, long j2) {
        boolean z;
        List<File> u = u(file, k);
        if (u.isEmpty()) {
            w12.f().i("Session " + file.getName() + " has no events.");
            return;
        }
        Collections.sort(u);
        ArrayList arrayList = new ArrayList();
        loop0: while (true) {
            z = false;
            for (File file2 : u) {
                try {
                    arrayList.add(i.g(L(file2)));
                } catch (IOException e) {
                    w12.f().l("Could not add event to report for " + file2, e);
                }
                if (z || y(file2.getName())) {
                    z = true;
                }
            }
        }
        if (arrayList.isEmpty()) {
            w12.f().k("Could not parse event files for session " + file.getName());
            return;
        }
        String str = null;
        File file3 = new File(file, "user");
        if (file3.isFile()) {
            try {
                str = L(file3);
            } catch (IOException e2) {
                w12.f().l("Could not read user ID file in " + file.getName(), e2);
            }
        }
        Q(new File(file, "report"), z ? this.c : this.d, arrayList, j2, z, str);
    }

    public final List<File> g(final String str) {
        List<File> t = t(this.b, new FileFilter() { // from class: aa0
            @Override // java.io.FileFilter
            public final boolean accept(File file) {
                boolean A;
                A = ga0.A(str, file);
                return A;
            }
        });
        Collections.sort(t, j);
        if (t.size() <= 8) {
            return t;
        }
        for (File file : t.subList(8, t.size())) {
            M(file);
        }
        return t.subList(0, 8);
    }

    public final void i() {
        int i2 = this.f.b().b().b;
        List<File> r = r();
        int size = r.size();
        if (size <= i2) {
            return;
        }
        for (File file : r.subList(i2, size)) {
            file.delete();
        }
    }

    public void l() {
        for (File file : r()) {
            file.delete();
        }
    }

    public void m(final String str) {
        FilenameFilter filenameFilter = new FilenameFilter() { // from class: ba0
            @Override // java.io.FilenameFilter
            public final boolean accept(File file, String str2) {
                boolean startsWith;
                startsWith = str2.startsWith(str);
                return startsWith;
            }
        };
        for (File file : j(u(this.c, filenameFilter), u(this.e, filenameFilter), u(this.d, filenameFilter))) {
            file.delete();
        }
    }

    public void n(String str, long j2) {
        for (File file : g(str)) {
            w12 f = w12.f();
            f.i("Finalizing report for session " + file.getName());
            P(file, j2);
            M(file);
        }
        i();
    }

    public void o(String str, r90.d dVar) {
        O(new File(v(str), "report"), this.e, dVar, str);
    }

    public final List<File> r() {
        return N(j(q(this.c), q(this.e)), q(this.d));
    }

    public final File v(String str) {
        return new File(this.b, str);
    }

    public long w(String str) {
        return new File(v(str), "start-time").lastModified();
    }

    public boolean x() {
        return !r().isEmpty();
    }
}
