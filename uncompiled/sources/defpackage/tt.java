package defpackage;

import android.graphics.Bitmap;

/* compiled from: Cache.java */
/* renamed from: tt  reason: default package */
/* loaded from: classes2.dex */
public interface tt {
    int a();

    void b(String str, Bitmap bitmap);

    Bitmap get(String str);

    int size();
}
