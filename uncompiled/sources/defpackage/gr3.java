package defpackage;

import com.google.common.collect.Ordering;
import java.util.Comparator;
import java.util.SortedSet;

/* compiled from: SortedIterables.java */
/* renamed from: gr3  reason: default package */
/* loaded from: classes2.dex */
public final class gr3 {
    public static <E> Comparator<? super E> a(SortedSet<E> sortedSet) {
        Comparator<? super E> comparator = sortedSet.comparator();
        return comparator == null ? Ordering.natural() : comparator;
    }

    public static boolean b(Comparator<?> comparator, Iterable<?> iterable) {
        Comparator comparator2;
        au2.k(comparator);
        au2.k(iterable);
        if (iterable instanceof SortedSet) {
            comparator2 = a((SortedSet) iterable);
        } else if (!(iterable instanceof fr3)) {
            return false;
        } else {
            comparator2 = ((fr3) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }
}
