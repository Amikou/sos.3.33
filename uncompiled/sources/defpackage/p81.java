package defpackage;

import java8.util.stream.ForEachOps;

/* renamed from: p81  reason: default package */
/* loaded from: classes2.dex */
public final /* synthetic */ class p81 implements nr1 {
    public static final p81 a = new p81();

    public static nr1 b() {
        return a;
    }

    @Override // defpackage.nr1
    public Object a(int i) {
        return ForEachOps.ForEachOrderedTask.lambda$doCompute$80(i);
    }
}
