package defpackage;

import android.graphics.Bitmap;

/* compiled from: Transformation.java */
/* renamed from: ya4  reason: default package */
/* loaded from: classes2.dex */
public interface ya4 {
    String key();

    Bitmap transform(Bitmap bitmap);
}
