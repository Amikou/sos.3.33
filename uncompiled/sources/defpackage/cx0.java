package defpackage;

import java.util.List;

/* compiled from: EthGetWork.java */
/* renamed from: cx0  reason: default package */
/* loaded from: classes3.dex */
public class cx0 extends i83<List<String>> {
    public String getBoundaryCondition() {
        return getResult().get(2);
    }

    public String getCurrentBlockHeaderPowHash() {
        return getResult().get(0);
    }

    public String getSeedHashForDag() {
        return getResult().get(1);
    }
}
