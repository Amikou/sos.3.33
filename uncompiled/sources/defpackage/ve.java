package defpackage;

import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;

/* compiled from: AnnotatedMethodMap.java */
/* renamed from: ve  reason: default package */
/* loaded from: classes.dex */
public final class ve implements Iterable<AnnotatedMethod> {
    public LinkedHashMap<h72, AnnotatedMethod> a;

    public void e(AnnotatedMethod annotatedMethod) {
        if (this.a == null) {
            this.a = new LinkedHashMap<>();
        }
        this.a.put(new h72(annotatedMethod.getAnnotated()), annotatedMethod);
    }

    public AnnotatedMethod i(String str, Class<?>[] clsArr) {
        LinkedHashMap<h72, AnnotatedMethod> linkedHashMap = this.a;
        if (linkedHashMap == null) {
            return null;
        }
        return linkedHashMap.get(new h72(str, clsArr));
    }

    public boolean isEmpty() {
        LinkedHashMap<h72, AnnotatedMethod> linkedHashMap = this.a;
        return linkedHashMap == null || linkedHashMap.size() == 0;
    }

    @Override // java.lang.Iterable
    public Iterator<AnnotatedMethod> iterator() {
        LinkedHashMap<h72, AnnotatedMethod> linkedHashMap = this.a;
        if (linkedHashMap != null) {
            return linkedHashMap.values().iterator();
        }
        return Collections.emptyList().iterator();
    }

    public AnnotatedMethod k(Method method) {
        LinkedHashMap<h72, AnnotatedMethod> linkedHashMap = this.a;
        if (linkedHashMap == null) {
            return null;
        }
        return linkedHashMap.get(new h72(method));
    }

    public AnnotatedMethod m(Method method) {
        LinkedHashMap<h72, AnnotatedMethod> linkedHashMap = this.a;
        if (linkedHashMap != null) {
            return linkedHashMap.remove(new h72(method));
        }
        return null;
    }
}
