package defpackage;

/* compiled from: PoolBackend.java */
/* renamed from: vs2  reason: default package */
/* loaded from: classes.dex */
public interface vs2<T> {
    int a(T t);

    void c(T t);

    T get(int i);

    T pop();
}
