package defpackage;

import com.bumptech.glide.load.engine.g;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Jobs.java */
/* renamed from: du1  reason: default package */
/* loaded from: classes.dex */
public final class du1 {
    public final Map<fx1, g<?>> a = new HashMap();
    public final Map<fx1, g<?>> b = new HashMap();

    public g<?> a(fx1 fx1Var, boolean z) {
        return b(z).get(fx1Var);
    }

    public final Map<fx1, g<?>> b(boolean z) {
        return z ? this.b : this.a;
    }

    public void c(fx1 fx1Var, g<?> gVar) {
        b(gVar.p()).put(fx1Var, gVar);
    }

    public void d(fx1 fx1Var, g<?> gVar) {
        Map<fx1, g<?>> b = b(gVar.p());
        if (gVar.equals(b.get(fx1Var))) {
            b.remove(fx1Var);
        }
    }
}
