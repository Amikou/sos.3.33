package defpackage;

import android.net.Uri;
import com.google.android.gms.internal.clearcut.b;

/* renamed from: w55  reason: default package */
/* loaded from: classes.dex */
public final class w55 {
    public final String a;
    public final Uri b;
    public final String c;
    public final String d;
    public final boolean e;
    public final boolean f;

    public w55(Uri uri) {
        this(null, uri, "", "", false, false);
    }

    public w55(String str, Uri uri, String str2, String str3, boolean z, boolean z2) {
        this.a = str;
        this.b = uri;
        this.c = str2;
        this.d = str3;
        this.e = z;
        this.f = z2;
    }

    public final <T> r45<T> b(String str, T t, b<T> bVar) {
        return r45.k(this, str, t, bVar);
    }

    public final r45<String> c(String str, String str2) {
        return r45.l(this, str, null);
    }

    public final r45<Boolean> f(String str, boolean z) {
        return r45.m(this, str, false);
    }

    public final w55 g(String str) {
        boolean z = this.e;
        if (z) {
            throw new IllegalStateException("Cannot set GServices prefix and skip GServices");
        }
        return new w55(this.a, this.b, str, this.d, z, this.f);
    }

    public final w55 i(String str) {
        return new w55(this.a, this.b, this.c, str, this.e, this.f);
    }
}
