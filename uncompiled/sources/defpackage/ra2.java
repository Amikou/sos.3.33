package defpackage;

import android.net.Uri;

/* compiled from: MultiUriHelper.java */
/* renamed from: ra2  reason: default package */
/* loaded from: classes.dex */
public abstract class ra2 {
    public static <T> Uri a(T t, T t2, T[] tArr, i81<T, Uri> i81Var) {
        Uri apply;
        Uri apply2;
        if (t == null || (apply2 = i81Var.apply(t)) == null) {
            if (tArr == null || tArr.length <= 0 || tArr[0] == null || (apply = i81Var.apply(tArr[0])) == null) {
                if (t2 != null) {
                    return i81Var.apply(t2);
                }
                return null;
            }
            return apply;
        }
        return apply2;
    }
}
