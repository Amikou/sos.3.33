package defpackage;

import android.os.IBinder;
import androidx.annotation.RecentlyNonNull;
import defpackage.lm1;
import java.lang.reflect.Field;

/* compiled from: com.google.android.gms:play-services-basement@@17.4.0 */
/* renamed from: nl2  reason: default package */
/* loaded from: classes.dex */
public final class nl2<T> extends lm1.a {
    public final T a;

    public nl2(T t) {
        this.a = t;
    }

    @RecentlyNonNull
    public static <T> T G1(@RecentlyNonNull lm1 lm1Var) {
        if (lm1Var instanceof nl2) {
            return ((nl2) lm1Var).a;
        }
        IBinder asBinder = lm1Var.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        Field field = null;
        int i = 0;
        for (Field field2 : declaredFields) {
            if (!field2.isSynthetic()) {
                i++;
                field = field2;
            }
        }
        if (i == 1) {
            if (!((Field) zt2.j(field)).isAccessible()) {
                field.setAccessible(true);
                try {
                    return (T) field.get(asBinder);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException("Could not access the field in remoteBinder.", e);
                } catch (NullPointerException e2) {
                    throw new IllegalArgumentException("Binder object is null.", e2);
                }
            }
            throw new IllegalArgumentException("IObjectWrapper declared field not private!");
        }
        int length = declaredFields.length;
        StringBuilder sb = new StringBuilder(64);
        sb.append("Unexpected number of IObjectWrapper declared fields: ");
        sb.append(length);
        throw new IllegalArgumentException(sb.toString());
    }

    @RecentlyNonNull
    public static <T> lm1 H1(@RecentlyNonNull T t) {
        return new nl2(t);
    }
}
