package defpackage;

import androidx.paging.DiffingChangePayload;

/* compiled from: NullPaddedListDiffHelper.kt */
/* renamed from: aq0  reason: default package */
/* loaded from: classes.dex */
public final class aq0 {
    public static final aq0 a = new aq0();

    public final void a(i02 i02Var, int i, int i2, int i3, int i4, Object obj) {
        int i5 = i - i3;
        if (i5 > 0) {
            i02Var.onChanged(i3, i5, obj);
        }
        int i6 = i4 - i2;
        if (i6 > 0) {
            i02Var.onChanged(i2, i6, obj);
        }
    }

    public final <T> void b(i02 i02Var, vi2<T> vi2Var, vi2<T> vi2Var2) {
        fs1.f(i02Var, "callback");
        fs1.f(vi2Var, "oldList");
        fs1.f(vi2Var2, "newList");
        int max = Math.max(vi2Var.c(), vi2Var2.c());
        int min = Math.min(vi2Var.c() + vi2Var.b(), vi2Var2.c() + vi2Var2.b());
        int i = min - max;
        if (i > 0) {
            i02Var.onRemoved(max, i);
            i02Var.onInserted(max, i);
        }
        int min2 = Math.min(max, min);
        int max2 = Math.max(max, min);
        a(i02Var, min2, max2, u33.d(vi2Var.c(), vi2Var2.a()), u33.d(vi2Var.c() + vi2Var.b(), vi2Var2.a()), DiffingChangePayload.ITEM_TO_PLACEHOLDER);
        a(i02Var, min2, max2, u33.d(vi2Var2.c(), vi2Var.a()), u33.d(vi2Var2.c() + vi2Var2.b(), vi2Var.a()), DiffingChangePayload.PLACEHOLDER_TO_ITEM);
        int a2 = vi2Var2.a() - vi2Var.a();
        if (a2 > 0) {
            i02Var.onInserted(vi2Var.a(), a2);
        } else if (a2 < 0) {
            i02Var.onRemoved(vi2Var.a() + a2, -a2);
        }
    }
}
