package defpackage;

import android.os.Bundle;
import android.os.Parcel;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;

/* compiled from: CueDecoder.java */
/* renamed from: lb0  reason: default package */
/* loaded from: classes.dex */
public final class lb0 {
    public ImmutableList<kb0> a(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        Bundle readBundle = obtain.readBundle(Bundle.class.getClassLoader());
        obtain.recycle();
        return is.b(kb0.w0, (ArrayList) ii.e(readBundle.getParcelableArrayList("c")));
    }
}
