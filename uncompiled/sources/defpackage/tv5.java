package defpackage;

import com.google.android.gms.internal.vision.n0;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: tv5  reason: default package */
/* loaded from: classes.dex */
public interface tv5 {
    int zza();

    boolean zzb();

    n0 zzc();
}
