package defpackage;

import androidx.lifecycle.LiveData;
import java.util.List;
import net.safemoon.androidwallet.model.fiat.room.RoomFiat;

/* compiled from: FiatListDao.kt */
/* renamed from: a31  reason: default package */
/* loaded from: classes2.dex */
public interface a31 {
    LiveData<List<RoomFiat>> a();

    void b(String str, double d);

    void c(RoomFiat... roomFiatArr);

    void d(String str, String str2);

    boolean e(String str);
}
