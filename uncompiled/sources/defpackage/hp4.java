package defpackage;

import android.view.View;
import android.view.WindowId;

/* compiled from: WindowIdApi18.java */
/* renamed from: hp4  reason: default package */
/* loaded from: classes.dex */
public class hp4 implements ip4 {
    public final WindowId a;

    public hp4(View view) {
        this.a = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof hp4) && ((hp4) obj).a.equals(this.a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
