package defpackage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-vision-common@@19.1.3 */
/* renamed from: gy5  reason: default package */
/* loaded from: classes.dex */
public final class gy5 implements Iterator<Map.Entry<K, V>> {
    public int a;
    public boolean f0;
    public Iterator<Map.Entry<K, V>> g0;
    public final /* synthetic */ ux5 h0;

    public gy5(ux5 ux5Var) {
        this.h0 = ux5Var;
        this.a = -1;
    }

    public final Iterator<Map.Entry<K, V>> a() {
        Map map;
        if (this.g0 == null) {
            map = this.h0.g0;
            this.g0 = map.entrySet().iterator();
        }
        return this.g0;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        List list;
        Map map;
        int i = this.a + 1;
        list = this.h0.f0;
        if (i >= list.size()) {
            map = this.h0.g0;
            if (map.isEmpty() || !a().hasNext()) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        List list;
        List list2;
        this.f0 = true;
        int i = this.a + 1;
        this.a = i;
        list = this.h0.f0;
        if (i < list.size()) {
            list2 = this.h0.f0;
            return (Map.Entry) list2.get(this.a);
        }
        return (Map.Entry) a().next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        List list;
        if (this.f0) {
            this.f0 = false;
            this.h0.p();
            int i = this.a;
            list = this.h0.f0;
            if (i < list.size()) {
                ux5 ux5Var = this.h0;
                int i2 = this.a;
                this.a = i2 - 1;
                ux5Var.k(i2);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    public /* synthetic */ gy5(ux5 ux5Var, sx5 sx5Var) {
        this(ux5Var);
    }
}
