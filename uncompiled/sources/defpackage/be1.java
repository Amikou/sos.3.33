package defpackage;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.channels.BufferOverflow;

/* compiled from: ChannelFlow.kt */
/* renamed from: be1  reason: default package */
/* loaded from: classes2.dex */
public interface be1<T> extends j71<T> {

    /* compiled from: ChannelFlow.kt */
    /* renamed from: be1$a */
    /* loaded from: classes2.dex */
    public static final class a {
        public static /* synthetic */ j71 a(be1 be1Var, CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow, int i2, Object obj) {
            if (obj == null) {
                if ((i2 & 1) != 0) {
                    coroutineContext = EmptyCoroutineContext.INSTANCE;
                }
                if ((i2 & 2) != 0) {
                    i = -3;
                }
                if ((i2 & 4) != 0) {
                    bufferOverflow = BufferOverflow.SUSPEND;
                }
                return be1Var.c(coroutineContext, i, bufferOverflow);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: fuse");
        }
    }

    j71<T> c(CoroutineContext coroutineContext, int i, BufferOverflow bufferOverflow);
}
