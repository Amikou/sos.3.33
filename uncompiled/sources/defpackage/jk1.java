package defpackage;

import android.content.Context;
import android.content.SharedPreferences;
import java.text.SimpleDateFormat;
import java.util.Date;

/* compiled from: HeartBeatInfoStorage.java */
/* renamed from: jk1  reason: default package */
/* loaded from: classes2.dex */
public class jk1 {
    public static jk1 b;
    public static final SimpleDateFormat c = new SimpleDateFormat("dd/MM/yyyy z");
    public final SharedPreferences a;

    public jk1(Context context) {
        this.a = context.getSharedPreferences("FirebaseAppHeartBeat", 0);
        context.getSharedPreferences("FirebaseAppHeartBeatStorage", 0);
    }

    public static synchronized jk1 a(Context context) {
        jk1 jk1Var;
        synchronized (jk1.class) {
            if (b == null) {
                b = new jk1(context);
            }
            jk1Var = b;
        }
        return jk1Var;
    }

    public static boolean b(long j, long j2) {
        Date date = new Date(j);
        Date date2 = new Date(j2);
        SimpleDateFormat simpleDateFormat = c;
        return !simpleDateFormat.format(date).equals(simpleDateFormat.format(date2));
    }

    public synchronized boolean c(long j) {
        return d("fire-global", j);
    }

    public synchronized boolean d(String str, long j) {
        if (this.a.contains(str)) {
            if (b(this.a.getLong(str, -1L), j)) {
                this.a.edit().putLong(str, j).apply();
                return true;
            }
            return false;
        }
        this.a.edit().putLong(str, j).apply();
        return true;
    }
}
