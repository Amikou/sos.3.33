package defpackage;

import androidx.media3.common.util.b;
import defpackage.s61;
import defpackage.wi3;

/* compiled from: FlacSeekTableSeekMap.java */
/* renamed from: r61  reason: default package */
/* loaded from: classes.dex */
public final class r61 implements wi3 {
    public final s61 a;
    public final long b;

    public r61(s61 s61Var, long j) {
        this.a = s61Var;
        this.b = j;
    }

    public final yi3 a(long j, long j2) {
        return new yi3((j * 1000000) / this.a.e, this.b + j2);
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        ii.i(this.a.k);
        s61 s61Var = this.a;
        s61.a aVar = s61Var.k;
        long[] jArr = aVar.a;
        long[] jArr2 = aVar.b;
        int i = b.i(jArr, s61Var.i(j), true, false);
        yi3 a = a(i == -1 ? 0L : jArr[i], i != -1 ? jArr2[i] : 0L);
        if (a.a != j && i != jArr.length - 1) {
            int i2 = i + 1;
            return new wi3.a(a, a(jArr[i2], jArr2[i2]));
        }
        return new wi3.a(a);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.a.f();
    }
}
