package defpackage;

import java.util.concurrent.Executor;

/* compiled from: Subscriber.java */
/* renamed from: mv3  reason: default package */
/* loaded from: classes2.dex */
public interface mv3 {
    <T> void a(Class<T> cls, Executor executor, vx0<? super T> vx0Var);

    <T> void b(Class<T> cls, vx0<? super T> vx0Var);
}
