package defpackage;

import androidx.media3.common.util.b;
import defpackage.wi3;

/* compiled from: WavSeekMap.java */
/* renamed from: fo4  reason: default package */
/* loaded from: classes.dex */
public final class fo4 implements wi3 {
    public final do4 a;
    public final int b;
    public final long c;
    public final long d;
    public final long e;

    public fo4(do4 do4Var, int i, long j, long j2) {
        this.a = do4Var;
        this.b = i;
        this.c = j;
        long j3 = (j2 - j) / do4Var.d;
        this.d = j3;
        this.e = a(j3);
    }

    public final long a(long j) {
        return b.J0(j * this.b, 1000000L, this.a.c);
    }

    @Override // defpackage.wi3
    public boolean e() {
        return true;
    }

    @Override // defpackage.wi3
    public wi3.a h(long j) {
        long r = b.r((this.a.c * j) / (this.b * 1000000), 0L, this.d - 1);
        long j2 = this.c + (this.a.d * r);
        long a = a(r);
        yi3 yi3Var = new yi3(a, j2);
        if (a < j && r != this.d - 1) {
            long j3 = r + 1;
            return new wi3.a(yi3Var, new yi3(a(j3), this.c + (this.a.d * j3)));
        }
        return new wi3.a(yi3Var);
    }

    @Override // defpackage.wi3
    public long i() {
        return this.e;
    }
}
