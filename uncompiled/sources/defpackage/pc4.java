package defpackage;

/* compiled from: Tuple3.java */
/* renamed from: pc4  reason: default package */
/* loaded from: classes3.dex */
public final class pc4<T1, T2, T3> {
    private static final int SIZE = 3;
    private final T1 value1;
    private final T2 value2;
    private final T3 value3;

    public pc4(T1 t1, T2 t2, T3 t3) {
        this.value1 = t1;
        this.value2 = t2;
        this.value3 = t3;
    }

    public T1 component1() {
        return this.value1;
    }

    public T2 component2() {
        return this.value2;
    }

    public T3 component3() {
        return this.value3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || pc4.class != obj.getClass()) {
            return false;
        }
        pc4 pc4Var = (pc4) obj;
        T1 t1 = this.value1;
        if (t1 == null ? pc4Var.value1 == null : t1.equals(pc4Var.value1)) {
            T2 t2 = this.value2;
            if (t2 == null ? pc4Var.value2 == null : t2.equals(pc4Var.value2)) {
                T3 t3 = this.value3;
                T3 t32 = pc4Var.value3;
                return t3 != null ? t3.equals(t32) : t32 == null;
            }
            return false;
        }
        return false;
    }

    public int getSize() {
        return 3;
    }

    @Deprecated
    public T1 getValue1() {
        return this.value1;
    }

    @Deprecated
    public T2 getValue2() {
        return this.value2;
    }

    @Deprecated
    public T3 getValue3() {
        return this.value3;
    }

    public int hashCode() {
        int hashCode = this.value1.hashCode() * 31;
        T2 t2 = this.value2;
        int hashCode2 = (hashCode + (t2 != null ? t2.hashCode() : 0)) * 31;
        T3 t3 = this.value3;
        return hashCode2 + (t3 != null ? t3.hashCode() : 0);
    }

    public String toString() {
        return "Tuple3{value1=" + this.value1 + ", value2=" + this.value2 + ", value3=" + this.value3 + "}";
    }
}
