package defpackage;

import com.google.android.gms.internal.measurement.t0;
import com.google.android.gms.internal.measurement.w1;

/* compiled from: com.google.android.gms:play-services-measurement@@19.0.0 */
/* renamed from: ng5  reason: default package */
/* loaded from: classes.dex */
public final class ng5 extends w1<t0, ng5> implements xx5 {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public /* synthetic */ ng5(defpackage.zf5 r1) {
        /*
            r0 = this;
            com.google.android.gms.internal.measurement.t0 r1 = com.google.android.gms.internal.measurement.t0.I()
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ng5.<init>(zf5):void");
    }
}
