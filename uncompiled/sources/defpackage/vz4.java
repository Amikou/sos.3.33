package defpackage;

import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-base@@17.4.0 */
/* renamed from: vz4  reason: default package */
/* loaded from: classes.dex */
public final class vz4 extends q05 {
    public final /* synthetic */ ConnectionResult b;
    public final /* synthetic */ wz4 c;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public vz4(wz4 wz4Var, j05 j05Var, ConnectionResult connectionResult) {
        super(j05Var);
        this.c = wz4Var;
        this.b = connectionResult;
    }

    @Override // defpackage.q05
    public final void a() {
        this.c.g0.q(this.b);
    }
}
