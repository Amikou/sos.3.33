package defpackage;

import defpackage.r90;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;

/* compiled from: BytesBackedNativeSessionFile.java */
/* renamed from: bt  reason: default package */
/* loaded from: classes2.dex */
public class bt implements td2 {
    public final byte[] a;
    public final String b;
    public final String c;

    public bt(String str, String str2, byte[] bArr) {
        this.b = str;
        this.c = str2;
        this.a = bArr;
    }

    public final byte[] a() {
        if (b()) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            try {
                gZIPOutputStream.write(this.a);
                gZIPOutputStream.finish();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                gZIPOutputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            } catch (Throwable th) {
                try {
                    gZIPOutputStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        } catch (IOException unused) {
            return null;
        }
    }

    public final boolean b() {
        byte[] bArr = this.a;
        return bArr == null || bArr.length == 0;
    }

    @Override // defpackage.td2
    public InputStream c() {
        if (b()) {
            return null;
        }
        return new ByteArrayInputStream(this.a);
    }

    @Override // defpackage.td2
    public String d() {
        return this.c;
    }

    @Override // defpackage.td2
    public r90.d.b e() {
        byte[] a = a();
        if (a == null) {
            return null;
        }
        return r90.d.b.a().b(a).c(this.b).a();
    }
}
