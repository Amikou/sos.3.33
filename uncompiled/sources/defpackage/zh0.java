package defpackage;

import android.os.Handler;
import java.util.concurrent.Executor;

/* renamed from: zh0  reason: default package */
/* loaded from: classes3.dex */
public final /* synthetic */ class zh0 implements Executor {
    public final /* synthetic */ Handler a;

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        this.a.post(runnable);
    }
}
