package defpackage;

import android.graphics.Matrix;
import com.facebook.common.internal.ImmutableList;

/* compiled from: JpegTranscoderUtils.java */
/* renamed from: ou1  reason: default package */
/* loaded from: classes.dex */
public class ou1 {
    public static final ImmutableList<Integer> a = ImmutableList.of((Object[]) new Integer[]{2, 7, 4, 5});

    public static int a(int i) {
        return Math.max(1, 8 / i);
    }

    public static int b(zu0 zu0Var) {
        int q = zu0Var.q();
        if (q == 90 || q == 180 || q == 270) {
            return zu0Var.q();
        }
        return 0;
    }

    public static int c(p93 p93Var, zu0 zu0Var) {
        int h = zu0Var.h();
        ImmutableList<Integer> immutableList = a;
        int indexOf = immutableList.indexOf(Integer.valueOf(h));
        if (indexOf >= 0) {
            return immutableList.get((indexOf + ((p93Var.f() ? 0 : p93Var.d()) / 90)) % immutableList.size()).intValue();
        }
        throw new IllegalArgumentException("Only accepts inverted exif orientations");
    }

    public static int d(p93 p93Var, zu0 zu0Var) {
        if (p93Var.e()) {
            int b = b(zu0Var);
            return p93Var.f() ? b : (b + p93Var.d()) % 360;
        }
        return 0;
    }

    public static int e(p93 p93Var, p73 p73Var, zu0 zu0Var, boolean z) {
        return 8;
    }

    public static Matrix f(zu0 zu0Var, p93 p93Var) {
        if (a.contains(Integer.valueOf(zu0Var.h()))) {
            return g(c(p93Var, zu0Var));
        }
        int d = d(p93Var, zu0Var);
        if (d != 0) {
            Matrix matrix = new Matrix();
            matrix.setRotate(d);
            return matrix;
        }
        return null;
    }

    public static Matrix g(int i) {
        Matrix matrix = new Matrix();
        if (i == 2) {
            matrix.setScale(-1.0f, 1.0f);
        } else if (i == 7) {
            matrix.setRotate(-90.0f);
            matrix.postScale(-1.0f, 1.0f);
        } else if (i == 4) {
            matrix.setRotate(180.0f);
            matrix.postScale(-1.0f, 1.0f);
        } else if (i != 5) {
            return null;
        } else {
            matrix.setRotate(90.0f);
            matrix.postScale(-1.0f, 1.0f);
        }
        return matrix;
    }

    public static boolean h(int i) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return true;
            default:
                return false;
        }
    }

    public static boolean i(int i) {
        return i >= 0 && i <= 270 && i % 90 == 0;
    }
}
