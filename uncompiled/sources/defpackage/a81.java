package defpackage;

import java.util.concurrent.Callable;

/* compiled from: FlowableFromCallable.java */
/* renamed from: a81  reason: default package */
/* loaded from: classes2.dex */
public final class a81<T> extends q71<T> implements Callable<T> {
    public final Callable<? extends T> b;

    public a81(Callable<? extends T> callable) {
        this.b = callable;
    }

    @Override // java.util.concurrent.Callable
    public T call() throws Exception {
        return (T) il2.a(this.b.call(), "The callable returned a null value");
    }
}
