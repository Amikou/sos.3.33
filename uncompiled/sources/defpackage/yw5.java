package defpackage;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-base@@19.0.0 */
/* renamed from: yw5  reason: default package */
/* loaded from: classes.dex */
public final class yw5 extends br5<Long> implements RandomAccess, xv5, hy5 {
    public static final yw5 h0;
    public long[] f0;
    public int g0;

    static {
        yw5 yw5Var = new yw5(new long[0], 0);
        h0 = yw5Var;
        yw5Var.zzb();
    }

    public yw5() {
        this(new long[10], 0);
    }

    public static yw5 i() {
        return h0;
    }

    @Override // defpackage.zv5
    /* renamed from: C */
    public final xv5 Q(int i) {
        if (i >= this.g0) {
            return new yw5(Arrays.copyOf(this.f0, i), this.g0);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ void add(int i, Object obj) {
        int i2;
        long longValue = ((Long) obj).longValue();
        e();
        if (i >= 0 && i <= (i2 = this.g0)) {
            long[] jArr = this.f0;
            if (i2 < jArr.length) {
                System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
            } else {
                long[] jArr2 = new long[((i2 * 3) / 2) + 1];
                System.arraycopy(jArr, 0, jArr2, 0, i);
                System.arraycopy(this.f0, i, jArr2, i + 1, this.g0 - i);
                this.f0 = jArr2;
            }
            this.f0[i] = longValue;
            this.g0++;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException(n(i));
    }

    @Override // defpackage.br5, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean addAll(Collection<? extends Long> collection) {
        e();
        cw5.a(collection);
        if (!(collection instanceof yw5)) {
            return super.addAll(collection);
        }
        yw5 yw5Var = (yw5) collection;
        int i = yw5Var.g0;
        if (i == 0) {
            return false;
        }
        int i2 = this.g0;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.f0;
            if (i3 > jArr.length) {
                this.f0 = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(yw5Var.f0, 0, this.f0, this.g0, yw5Var.g0);
            this.g0 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.Collection, java.util.List
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof yw5)) {
            return super.equals(obj);
        }
        yw5 yw5Var = (yw5) obj;
        if (this.g0 != yw5Var.g0) {
            return false;
        }
        long[] jArr = yw5Var.f0;
        for (int i = 0; i < this.g0; i++) {
            if (this.f0[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object get(int i) {
        m(i);
        return Long.valueOf(this.f0[i]);
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.Collection, java.util.List
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g0; i2++) {
            i = (i * 31) + cw5.e(this.f0[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Long) {
            long longValue = ((Long) obj).longValue();
            int i = this.g0;
            for (int i2 = 0; i2 < i; i2++) {
                if (this.f0[i2] == longValue) {
                    return i2;
                }
            }
            return -1;
        }
        return -1;
    }

    public final void k(long j) {
        e();
        int i = this.g0;
        long[] jArr = this.f0;
        if (i == jArr.length) {
            long[] jArr2 = new long[((i * 3) / 2) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.f0 = jArr2;
        }
        long[] jArr3 = this.f0;
        int i2 = this.g0;
        this.g0 = i2 + 1;
        jArr3[i2] = j;
    }

    public final void m(int i) {
        if (i < 0 || i >= this.g0) {
            throw new IndexOutOfBoundsException(n(i));
        }
    }

    public final String n(int i) {
        int i2 = this.g0;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object remove(int i) {
        int i2;
        e();
        m(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        if (i < this.g0 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.g0--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        e();
        if (i2 >= i) {
            long[] jArr = this.f0;
            System.arraycopy(jArr, i2, jArr, i, this.g0 - i2);
            this.g0 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        e();
        m(i);
        long[] jArr = this.f0;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public final int size() {
        return this.g0;
    }

    @Override // defpackage.xv5
    public final long z0(int i) {
        m(i);
        return this.f0[i];
    }

    public yw5(long[] jArr, int i) {
        this.f0 = jArr;
        this.g0 = i;
    }

    @Override // defpackage.br5, java.util.AbstractList, java.util.AbstractCollection, java.util.Collection, java.util.List
    public final /* bridge */ /* synthetic */ boolean add(Object obj) {
        k(((Long) obj).longValue());
        return true;
    }
}
