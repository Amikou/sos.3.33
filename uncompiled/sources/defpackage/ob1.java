package defpackage;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.d;
import androidx.lifecycle.f;
import androidx.lifecycle.j;
import androidx.lifecycle.l;
import androidx.savedstate.SavedStateRegistry;
import androidx.savedstate.a;

/* compiled from: FragmentViewLifecycleOwner.java */
/* renamed from: ob1  reason: default package */
/* loaded from: classes.dex */
public class ob1 implements d, fc3, hj4 {
    public final Fragment a;
    public final gj4 f0;
    public l.b g0;
    public f h0 = null;
    public a i0 = null;

    public ob1(Fragment fragment, gj4 gj4Var) {
        this.a = fragment;
        this.f0 = gj4Var;
    }

    public void a(Lifecycle.Event event) {
        this.h0.h(event);
    }

    public void b() {
        if (this.h0 == null) {
            this.h0 = new f(this);
            this.i0 = a.a(this);
        }
    }

    public boolean c() {
        return this.h0 != null;
    }

    public void d(Bundle bundle) {
        this.i0.c(bundle);
    }

    public void e(Bundle bundle) {
        this.i0.d(bundle);
    }

    public void f(Lifecycle.State state) {
        this.h0.o(state);
    }

    @Override // androidx.lifecycle.d
    public l.b getDefaultViewModelProviderFactory() {
        l.b defaultViewModelProviderFactory = this.a.getDefaultViewModelProviderFactory();
        if (!defaultViewModelProviderFactory.equals(this.a.mDefaultFactory)) {
            this.g0 = defaultViewModelProviderFactory;
            return defaultViewModelProviderFactory;
        }
        if (this.g0 == null) {
            Application application = null;
            Context applicationContext = this.a.requireContext().getApplicationContext();
            while (true) {
                if (!(applicationContext instanceof ContextWrapper)) {
                    break;
                } else if (applicationContext instanceof Application) {
                    application = applicationContext;
                    break;
                } else {
                    applicationContext = ((ContextWrapper) applicationContext).getBaseContext();
                }
            }
            this.g0 = new j(application, this, this.a.getArguments());
        }
        return this.g0;
    }

    @Override // defpackage.rz1
    public Lifecycle getLifecycle() {
        b();
        return this.h0;
    }

    @Override // defpackage.fc3
    public SavedStateRegistry getSavedStateRegistry() {
        b();
        return this.i0.b();
    }

    @Override // defpackage.hj4
    public gj4 getViewModelStore() {
        b();
        return this.f0;
    }
}
